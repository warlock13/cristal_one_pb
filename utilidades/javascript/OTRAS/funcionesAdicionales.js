var mensaje = "";
var valores_a_mandar = "";
var paginaActual = ""; // ubicar el nombre de la pagina actual
var setResultados = new Array();
var posicionActual = 0; //definir en que posicion de la busqueda me encuentro 
var totalRegistros = 0;
var parametros = new Array();
var totalRegistros = 0;
var tabActivo = '1';
var rutaFotos = '/clinica/utilidades/fotos/';
var rutaFirmas = '/clinica/utilidades/firmas/';
var rutaNombres = '/clinica/utilidades/firmas/nombres/';
var idQuery = '';
var elemInputDestino = '';
var version = 1.0;
loaderFormulasFolio = false


function mostrarListaMedicamentos(checkbox) {
    var $checkbox = $(checkbox);

    if ($checkbox.is(':checked')) {
        buscarHC('listMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
    }
}

function EsCirugia(selectElement){

    var selectedValue = selectElement.value;
    var selectedText = selectElement.options[selectElement.selectedIndex].text;

    if (selectedText.toLowerCase().includes('cirugia') || selectedText.toLowerCase().includes('cirugía')) {
        $('#tbCirugia').show()  
        $('#IdTdProcedimientos').text('PROCEDIMIENTOS CIRUGÍA')
    }else{
        $('#tbCirugia').hide()  
        $('#IdTdProcedimientos').text('PROCEDIMIENTOS')

    }
}

function mostrarListaProcedimientos(checkbox) {
    var $checkbox = $(checkbox);

    if ($checkbox.is(':checked')) {
        buscarHC('listaProcedimientoConductaTratamiento', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
    }
}


function toggleAllCheckboxes() {
    var checkboxes = document.querySelectorAll('input[name^="chkEstadoLE"]');
    var todoCheckbox = document.getElementById('chkEstadoLE');

    checkboxes.forEach(function (checkbox) {
        checkbox.checked = todoCheckbox.checked;
    });
}

function updateTodoCheckbox() {
    var checkboxes = document.querySelectorAll('input[name^="chkEstadoLE"]');
    var todoCheckbox = document.getElementById('chkEstadoLE');

    // Verificar si todos los checkboxes están marcados
    var allChecked = true;
    checkboxes.forEach(function (checkbox) {
        if (checkbox.id !== 'chkEstadoLE' && !checkbox.checked) {
            allChecked = false;
        }
    });

    // Actualizar el estado del checkbox "todo"
    todoCheckbox.checked = allChecked;
}

async function imprimirFormatosImpresion() {
    idPaciente = valorAtributo('txtIdBusPaciente').split('-')[1] + ' ' + valorAtributo('txtIdBusPaciente').split('-')[2];
    evo = traerDatoFilaSeleccionada('divParaContenedorFoliosPaciente', 'ID_FOLIO');
    idReporte = traerDatoFilaSeleccionada('divParaContenedorFoliosPaciente', 'TIPO');
    nombrepdf = "HC-" + idPaciente + ".pdf";

    if(evo==""){
        alert2("Falta seleccionar un folio de historia clínica")
        return
    }

    var checkboxes = document.querySelectorAll('table input[type="checkbox"]');

    var valores = {};

    checkboxes.forEach(function (checkbox) {
        valores[checkbox.name] = checkbox.checked;
    });

    var URL = 'hc/mostrarFormatosHC.jsp?';
    for (var key in valores) {
        URL += key + '=' + encodeURIComponent(valores[key]) + '&';
    }

    let tablaProcedimientos = $("#listaProcedimientoConductaTratamiento").getDataIDs();
    if (tablaProcedimientos.length > 0) {
        const laboratoriosContratados = await listaLaboratoriosImpresionSolicitudes("contratados")
        const laboratoriosNoContratados = await listaLaboratoriosImpresionSolicitudes("")

        URL += 'procedimientosSolicitud=' + listaProcedimientosImpresionSolicitudes().toString() + '&'
        URL += 'laboratoriosContratados=' + laboratoriosContratados.toString() + '&'
        URL += 'laboratoriosNoContratados=' + laboratoriosNoContratados.toString() + '&'
    }

    let tablaMedicamentos = $("#listMedicacion").getDataIDs();
    if (tablaMedicamentos.length > 0) {
        URL += 'listaMedicamentos=' + listaMedicamentosImpresion().toString() + '&'
    }

    if ($("#chkEstadoLE_consentimiento_disentimiento").is(':checked')) {
        const consentimientos = await listaConsentimientosImpresion();
        URL += 'listaConsentimientos=' + consentimientos.toString() + '&'
    }

    if ($("#chkEstadoLE_remisiones").is(':checked')) {
        const remisiones = await listaRemisionesImpresion();
        URL += 'listaRemisiones=' + remisiones.toString() + '&'
    }

    if ($("#chkEstadoLE_incapacidad").is(':checked')) {
        const incapacidades = await listaIncapacidadesImpresion();
        URL += 'listaIncapacidades=' + incapacidades.toString() + '&'
    }

    URL += 'idPaciente=' + idPaciente + '&'
    URL += 'evo=' + evo + '&'
    URL += 'idReporte=' + idReporte + '&'
    URL += 'nombrepdf=' + nombrepdf


    var dimension = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90';
    window.open(URL, '', dimension);
}

function listaIncapacidadesImpresion() {
    return new Promise(function(resolve, reject) {
        var lista_incapacidades = [];

        $.ajax({
            type: "GET",
            url: "/clinica/paginas/accionesXml/ejecutar_sql.jsp",
            data: { "idQuery": 9026, "parametros": [valorAtributo("lblIdDocumento")]},
            dataType: "json",
            success: function (data) {
                $.each(data, function (index, item) {
                    lista_incapacidades.push(item.id);
                });
            },
            error: function (xhr, status, error) {
                reject(error);
            },
            complete: function () {
                resolve(lista_incapacidades);
            }
        });
    });
}

function listaRemisionesImpresion() {
    return new Promise(function(resolve, reject) {
        var lista_remisiones = [];

        $.ajax({
            type: "GET",
            url: "/clinica/paginas/accionesXml/ejecutar_sql.jsp",
            data: { "idQuery": 1400, "parametros": [valorAtributo("lblIdDocumento")]},
            dataType: "json",
            success: function (data) {
                $.each(data, function (index, item) {
                    lista_remisiones.push(item.id);
                });
            },
            error: function (xhr, status, error) {
                reject(error);
            },
            complete: function () {
                resolve(lista_remisiones);
            }
        });
    });
}


function listaConsentimientosImpresion() {
    return new Promise(function(resolve, reject) {
        let id_admision = valorAtributo("lblIdAdmision");
        var lista_consentimientos = [];

        $.ajax({
            type: "GET",
            url: "/clinica/paginas/accionesXml/ejecutar_sql.jsp",
            data: { "idQuery": 218, "parametros": [id_admision] },
            dataType: "json",
            success: function (data) {
                $.each(data, function (index, item) {
                    lista_consentimientos.push(item.id);
                });
            },
            error: function (xhr, status, error) {
                reject(error);
            },
            complete: function () {
                resolve(lista_consentimientos);
            }
        });
    });
}


function listaMedicamentosImpresion() {
    let lista_medicamentos = []
    let ids = $("#listMedicacion").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listMedicacion_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#listMedicacion").getRowData(ids[i]);
            lista_medicamentos.push(datosRow.ID);
        }
    }

    return lista_medicamentos
}

function listaProcedimientosImpresionSolicitudes() {

    let lista_procedimientos = []
    let ids = $("#listaProcedimientoConductaTratamiento").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listaProcedimientoConductaTratamiento_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#listaProcedimientoConductaTratamiento").getRowData(ids[i]);
            if (datosRow.CLASE != 'Laboratorio') {
                lista_procedimientos.push(datosRow.ID);
            }
        }
    }
    return lista_procedimientos
}

function listaLaboratoriosImpresionSolicitudes(tipo) {

    return new Promise(function (resolve, reject) {
        let lista_laboratorios = []

        let ids = $("#listaProcedimientoConductaTratamiento").getDataIDs();

        for (var i = 0; i < ids.length; i++) {
            var c = "jqg_listaProcedimientoConductaTratamiento_" + ids[i];
            if ($("#" + c).is(":checked")) {
                datosRow = $("#listaProcedimientoConductaTratamiento").getRowData(ids[i]);
                if (datosRow.CLASE == 'Laboratorio') {
                    lista_laboratorios.push(datosRow.ID);
                }
            }
        }

        $.ajax({
            type: "GET",
            url: "/clinica/paginas/accionesXml/ejecutar_sql.jsp",
            data: { "idQuery": tipo == "contratados" ? 2667 : 2668, "parametros": [valorAtributo('lblIdDocumento'), lista_laboratorios.toString()]},
            dataType: "json",
            beforeSend: function (){
                lista_laboratorios = []
            },
            success: function (data) {
                console.log(data)
                $.each(data, function (index, item) {
                    lista_laboratorios.push(item.id);
                });
            },
            error: function (xhr, status, error) {
                reject(error);
            },
            complete: function () {
                resolve(lista_laboratorios);
            }
        });
    });
}

/**
* funcionalidad minimizar ventanita.
* @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
*/
function minimizarVentanita(idVentanita) {
    const card = document.getElementById(idVentanita);
    const bubble = document.getElementById('bubble');
    card.style.transform = 'scale(0)';
    bubble.style.display = 'block';
}
/**
* funcionalidad maximizar ventanita.
* @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
*/
function maximizarVentanita(idVentanita) {
    const card = document.getElementById(idVentanita);
    const bubble = document.getElementById('bubble');
    card.style.transform = 'scale(1)';
    bubble.style.display = 'none';
}
/*
 @Miguel Pasaje
 funcion para mover la tarjeta infoPaciente
*/
function moverTarjeta() {


    let tarjeta = document.getElementById("divInfoPacienteVentana");
    let isDragging = false;
    let offsetX = 0, offsetY = 0;
    let ventanitaPaciente = document.getElementById('divInfoPacienteVentana')

    ventanitaPaciente.classList.remove('desIzqHC')
    ventanitaPaciente.classList.remove('desDerHC')


    tarjeta.addEventListener("mousedown", (e) => {
        // Evitar que el clic tenga un efecto no deseado
        e.preventDefault();
        isDragging = true;
        offsetX = e.clientX - tarjeta.getBoundingClientRect().left;
        offsetY = e.clientY - tarjeta.getBoundingClientRect().top;
        tarjeta.style.cursor = "grabbing";


    });

    document.addEventListener("mousemove", (e) => {
        var anchoVentana = window.innerWidth;
        if (anchoVentana < 1400) {
            return
        }

        if (e.buttons === 1) {

            let left = e.clientX - offsetX;
            let top = e.clientY - offsetY;

            // Verificar que la tarjeta no se salga de la ventana
            if (left < 0) left = 0;
            if (top < 0) top = 0;
            if (left + tarjeta.clientWidth > window.innerWidth) {
                left = window.innerWidth - tarjeta.clientWidth;
            }
            if (top + tarjeta.clientHeight > window.innerHeight) {
                top = window.innerHeight - tarjeta.clientHeight;
            }


            if (isDragging) {
                tarjeta.style.left = `${e.clientX - offsetX}px`;
                tarjeta.style.top = `${e.clientY - offsetY}px`;
            }
        }

    });

    document.addEventListener("mouseup", () => {
        isDragging = false;
        tarjeta.style.cursor = "grab";
    });
}



/*MP  */
async function traerDatoPlantillaDiagnostico_hc(idEvolucion) {
    valores_a_mandar = ''
    add_valores_a_mandar(idEvolucion)
    return await new Promise((resolve = () => { }, reject = () => { }) => {
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 4063,
                "parametros": valores_a_mandar,
                "_search": false,
                "nd": 1611873687449,
                "rows": -1,
                "page": 1,
                "sidx": "NO FACT",
                "sord": "asc"
            },
            beforeSend: function () {
                //mostrarLoader(urlParams.get("accion"))
            },
            success: function (data) {
                let diagnosticos = []
                cells = data.getElementsByTagName("cell");
                for (let i = 1; i < cells.length; i += 2) {
                    diagnosticos.push(cells[i].firstChild.data)
                }
                resolve(diagnosticos);
            },
            complete: function (jqXHR, textStatus) {
                //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                reject();
                swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            }
        });
    });
}
async function consultarCamposEpoc(idEvolucion, campo, plantilla) {
    //console.log(idEvolucion)
    valores_a_mandar = '';
    add_valores_a_mandar(campo);
    add_valores_a_mandar(plantilla);
    add_valores_a_mandar(idEvolucion);
    const respuesta = await traerDatoPlantilla(valores_a_mandar)
    //console.log(respuesta, 'console')
    return respuesta
}
/*end MP  */

function dataClap(datos) {
    let campos = [
        "C1",
        "ID_FOLIO",
        "TIPO",
        "IDPACIENTE",
        "FECHA",
        "EDAD_GESTACIONAL",
        "PESO",
        "PA",
        "ALTURA_UTERINA",
        "PRESENTACION",
        "FCF(ipm)",
        "MOV. FETALES",
        "PROTEINURIA",
        "SIG.ALARMA",
        "INICIALES_TECNICO",
        "PROXIMA_CITA"
    ]

    return new Promise((resolve = () => { }, reject = () => { }) => {
        valores_a_mandar = ""
        let idFolio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO')
        add_valores_a_mandar(datos.idPaciente)
        add_valores_a_mandar(idFolio)
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 2797, /* 2794, */
                "parametros": valores_a_mandar,
                "rows": -1,
                "page": 1,
                "sord": "asc"
            },
            success: function (data) {

                let rows = data.getElementsByTagName("row")


                let size = data.getElementsByTagName('row').length;
                let datos = []
                for (let i = 0; i < size; i++) {

                    let cells = rows[i].getElementsByTagName('cell');
                    let rowObject = {};

                    for (let j = 0; j < cells.length; j++) {
                        let cellData = cells[j].textContent;
                        let cellKey = "C" + (j + 1)
                        //let cellKey = campos[j]
                        res = "Fila: " + (i + 1) + ", Celda: " + (j + 1) + ", Valor: " + cellData
                        rowObject[cellKey] = cellData
                        // Aquí puedes realizar cualquier otra acción con los datos de la celda
                    }
                    //console.log(rowObject,'rowObject');

                    datos.push(rowObject)

                }


                //console.log(datos,'dataPROMISE');
                resolve(datos)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("error Al cargar la pagina intente recargar la pagina");
            }
        });
    })



}
function dataClapAll() {
    return new Promise((resolve = () => { }, reject = () => { }) => {
        valores_a_mandar = ""
        let idFolio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO')
        add_valores_a_mandar(idFolio)
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 2796,
                "parametros": valores_a_mandar,
                "rows": -1,
                "page": 1,
                "sord": "asc"
            },
            success: function (data) {

                let rows = data.getElementsByTagName("row")


                let size = data.getElementsByTagName('row').length;
                let datos = []
                for (let i = 0; i < size; i++) {

                    let cells = rows[i].getElementsByTagName('cell');
                    let rowObject = {};

                    for (let j = 0; j < cells.length; j++) {
                        let cellData = cells[j].textContent;
                        index = j + 1;
                        let cellKey = ''
                        if (index == 2) {
                            cellKey = "idFolio";
                        } else if (index == 4) {
                            cellKey = "userMod";
                        }
                        else {
                            cellKey = "Celda" + (index);
                        }
                        //let cellKey = campos[j]
                        res = "Fila: " + (i + 1) + ", Celda: " + (j + 1) + ", Valor: " + cellData
                        rowObject[cellKey] = cellData
                        // Aquí puedes realizar cualquier otra acción con los datos de la celda
                    }
                    //console.log(rowObject,'rowObject');

                    datos.push(rowObject)

                }



                resolve(datos)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("error Al cargar la pagina intente recargar la pagina");
            }
        });

    })
}

function traerDatosGrillaClap(query, idPaciente) {
    return new Promise((resolve = () => { }, reject = () => { }) => {
        valores_a_mandar = ""
        let idFolio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO')
        add_valores_a_mandar(idPaciente)
        add_valores_a_mandar(idFolio)
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": query,
                "parametros": valores_a_mandar,
                "rows": -1,
                "page": 1,
                "sord": "asc"
            },
            success: function (data) {

                let rows = data.getElementsByTagName("row")


                let size = data.getElementsByTagName('row').length;
                let datos = []
                for (let i = 0; i < size; i++) {

                    let cells = rows[i].getElementsByTagName('cell');
                    let rowObject = {};

                    for (let j = 0; j < cells.length; j++) {
                        let cellData = cells[j].textContent;
                        index = j + 1;
                        let cellKey = "C" + (j + 1)
                        //let cellKey = campos[j]
                        res = "Fila: " + (i + 1) + ", Celda: " + (j + 1) + ", Valor: " + cellData
                        rowObject[cellKey] = cellData
                        // Aquí puedes realizar cualquier otra acción con los datos de la celda
                    }
                    //console.log(rowObject,'rowObject');

                    datos.push(rowObject)

                }



                resolve(datos)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("error Al cargar la pagina intente recargar la pagina");
            }
        });

    })
}

function seleccionarPrimeraOpcionSelect(selectId) {
    const select = document.getElementById(selectId);
    const options = select.options;
    options[0].selected = true;
    console.log({ selectId, options });
}


/* ---------------------------------------------------------------------------------------------------------- */
/**
 * Realiza una petición AJAX para cargar un combo utilizando el plugin Select2 con paginación.
 * @param {Object} data - Los datos a enviar en la petición AJAX.
 * @param {string} idCombo - El ID del elemento combo/select donde se cargarán los resultados.
 * @returns {Promise<string>} Una promesa que se resuelve con el ID del combo/select una vez que se ha cargado exitosamente.
 */
function pagerSelect2(data, idCombo) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp',
            data: data,
            contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
            beforeSend: function () {
                //mostrarLoader(urlParams.get("accion"))
                desHabilitar(idCombo, 1)
            },
            success: function (data) {
                raiz = data;
                let resultId = [];
                let resultNombre = [];
                let result = {};
                c = raiz.getElementsByTagName('id');
                n = raiz.getElementsByTagName('nom');
                d = raiz.getElementsByTagName('title');
                for (let i = 0; i < c.length; i++) {
                    resultId.push(c[i].firstChild.data);
                    resultNombre.push(n[i].firstChild.data)
                    result[n[i].firstChild.data] = c[i].firstChild.data
                }
                x = _.map(result, function (i, j) {
                    return {
                        id: i,
                        text: j
                    }
                });
                $(`#${idCombo}`).select2({
                    data: x,
                    placeholder: 'search',
                    //multiple: false,
                    // query with pagination
                    query: function (q) {
                        var pageSize, results, that = this;
                        pageSize = 20; // or whatever pagesize
                        results = [];
                        if (q.term && q.term !== '') {
                            // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                            results = _.filter(that.data, function (e) {
                                return e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0;
                            });
                        } else if (q.term === '') {
                            results = that.data;
                        }
                        q.callback({
                            results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                            more: results.length >= q.page * pageSize,
                        });
                    },
                    dropdownCssClass: "set_ddl_size"
                });
                resolve(idCombo)
            },
            complete: function (jqXHR, textStatus) {
                //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
                try {
                    document.getElementById('select2-drop-mask').style = ''
                } catch (error) {
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            }
        });
    })
}
/* ---------------------------------------------------------------------------------------------------------- */


function mostrarLoaderSw2() {
    Swal.fire({
        html: 'Esperando respuesta, por favor espere...',
        allowOutsideClick: false,
        didOpen: () => {
            Swal.showLoading()
        },
    })
}

function ocultarSw2() {
    Swal.close()
}

function alert2(mensaje) {
    Swal.fire({
        html: mensaje,
        icon: "info",
        confirmButtonText: "Continuar",
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#999999',
        position: "top",
    })
}

function traerDatoFilaSeleccionada(grilla, nomColumna) {
    let idRowSel = $('#drag' + ventanaActual.num).find('#' + grilla).jqGrid('getGridParam', 'selrow');
    let idFolioSel = $('#drag' + ventanaActual.num).find('#' + grilla).getRowData(idRowSel)[nomColumna];

    return idFolioSel ? idFolioSel : '';
}

function traerTituloFilaSeleccionada(grilla, nomColumna) {
    let idRowSel = $('#' + grilla).jqGrid('getGridParam', 'selrow');

    if (idRowSel) {
        let cellValue = $('#' + grilla).jqGrid('getCell', idRowSel, nomColumna);
        let titleValue = $('#' + grilla).find('tr#' + idRowSel).find('td[aria-describedby="' + grilla + '_' + nomColumna + '"]').attr('title');

        // Si la celda tiene un valor y el atributo title, devuelve el atributo title (eliminando espacios antes y después), de lo contrario, devuelve una cadena vacía
        return (cellValue && titleValue) ? titleValue.trim() : '';
    } else {
        // Si no hay fila seleccionada, devuelve una cadena vacía
        return '';
    }
}



function calcularEdadCompleta(fechaSrc, parte = "todo") {
    //parte = meses, años, dias, por defecto todo
    let fechaInicial = new Date(new Date(fechaSrc.split('/').reverse()).toISOString().substr(0, 10));
    let fechaFinal = new Date(new Date().toISOString().substr(0, 10));

    let anoInicio = fechaInicial.getFullYear();
    let february = (anoInicio % 4 === 0 && anoInicio % 100 !== 0) || anoInicio % 400 === 0 ? 29 : 28;
    let daysInMonth = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let anoDif = fechaFinal.getFullYear() - anoInicio;
    let mesDif = fechaFinal.getMonth() - fechaInicial.getMonth();
    if (mesDif < 0) {
        anoDif--;
        mesDif += 12;
    }
    let diaDif = fechaFinal.getDate() - fechaInicial.getDate();
    if (diaDif < 0) {
        if (mesDif > 0) {
            mesDif--;
        } else {
            anoDif--;
            mesDif = 11;
        }
        diaDif += daysInMonth[fechaInicial.getMonth()];
    }

    let respuesta = {
        "todo": `${anoDif}&nbsp;A&ntildeos ${mesDif}&nbsp;Meses ${diaDif}&nbsp;D&iacuteas`,
        "años": anoDif,
        "meses": mesDif,
        "dias": diaDif
    }

    return respuesta[parte];
}

function sumarDuracionaHora(horaSrc, duracionSrc) {
    console.log("hora :", horaSrc, "  duaracion: ", duracionSrc);
    let horaOut = horaSrc.replace(/\s+(AM|PM)/, '');
    let horas = parseInt(horaOut.split(':')[0]);
    let minutos = parseInt(horaOut.split(":")[1]);
    duracionSrc = parseInt(duracionSrc);

    if (horaSrc.includes('PM') && horas != 12) horas += 12;

    horas += Math.floor(duracionSrc / 60);
    minutos += duracionSrc % 60;

    if (minutos >= 60) {
        horas += 1;
        minutos -= 60;
    }

    let rango = horas >= 12 ? 'PM' : 'AM';
    if (horas > 12) horas -= 12;

    horaOut = `${horas < 10 ? '0' : ''}${horas}:${minutos < 10 ? '0' : ''}${minutos} ${rango}`;
    return horaOut;
}

function mostrarLoader(accion) {
    loader = '<img id="loader_aux001" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">';
    switch (accion) {
        case "finalizarOrdenExternaAdmision":
            window.finalizarOrdenExternaAdmision = $('#btnFinalizarOrdenExterna')
            $("#btnFinalizarOrdenExterna").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;

        case "btnCrearFacturasBaseNominal":
            asignaAtributo('lblTotNotifVentanillaAlert', 'Por favor, espere unos segundos hasta que las facturas se generen completamente', 0);
            $("#loaderBaseNom").show();
            break;

        case "eliminarFacturasBaseNominal":
            asignaAtributo('lblTotNotifVentanillaAlert', 'Por favor, espere unos segundos hasta que las facturas se generen completamente', 0);
            $("#loaderBaseNom").show();
            break;

        case 'tipificarFacturasBaseNominal':
            window.tipificarFacturasBaseNominal = $('#tipificarFacturasBaseNominal')
            $("#tipificarFacturasBaseNominal").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;

        case 'actualizarCausaExternaHc':
            $('#cmbCausaExterna-ok').hide();
            $('#cmbCausaExterna-loader').show();
            $('#cmbCausaExterna-error').hide();
            break;

        case 'actualizarFinalidadHc':
            $('#cmbFinalidadConsulta-ok').hide();
            $('#cmbFinalidadConsulta-loader').show();
            $('#cmbFinalidadConsulta-error').hide();
            break;

        case 'actualizarDxIngresoHC':
            $('#txtIdDxIngreso-ok').hide();
            $('#txtIdDxIngreso-loader').show();
            $('#txtIdDxIngreso-error').hide();
            break;

        case 'eliminarRegistroLaboratorioExterno':
            window.botoneliminarRegistroLaboratorioExterno = $('#btnEliminarOtroExamenEx')
            $("#btnEliminarOtroExamenEx").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;
        case 'ingresoResultadoExterno':
            window.botonIngresoResultadoExterno = $('#btnIngresoOtrosResultadosEx')
            $("#btnIngresoOtrosResultadosEx").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;
        case 'registrarArchivoHelp':
            window.botonRegistrarArchivoHelp = $('#btnSubirAdjuntosE')
            $("#btnSubirAdjuntosE").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;
        case 'cerrarEncuestaPaciente':
            window.btnCerrarEncuesta = $('#btnCerrarEncuesta');
            $("#btnCerrarEncuesta").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;

        case "crearFolio":
            window.botonCrearFolio = $("#btnCrearFolio");
            $("#btnCrearFolio").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;

        case "eliminarFacturasRips":
            window.botonEliminarFacturasRips = $("#eliminar_facturas_rips");
            $("#eliminar_facturas_rips").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;

        case "firmarFolio":
            window.botonFirmarFolio = $("#btnFirmarFolio");
            $("#btnFirmarFolio").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;

        case "crearArchivosRips":
            window.botonCrearRips = $("#btnCrearRips");
            $("#btnCrearRips").replaceWith(`<img id="loader_${accion}" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">`);
            break;

        case "crearFacturasBaseNominalNefro":
            mostrarLoaderSw2()
            break;

        case 'numerarFacturasBloque':
            mostrarLoaderSw2()
            break;

        case 'accionesAlEstadoFolio':
            $(`#${accion}`).show();
            break;

        case 'eliminarFolio':
            $(`#accionesAlEstadoFolio`).show();
            break;

        default:
            break;
    }
}

function ocultarLoader(accion) {
    switch (accion) {
        case "finalizarOrdenExternaAdmision":
            $("#loader_" + accion).replaceWith(window.finalizarOrdenExternaAdmision);
            break;

        case "btnCrearFacturasBaseNominal":
            $("#loaderBaseNom").hide();
            break;

        case "eliminarFacturasBaseNominal":
            $("#loaderBaseNom").hide();
            break;

        case 'tipificarFacturasBaseNominal':
            $("#loader_" + accion).replaceWith(window.tipificarFacturasBaseNominal);
            break;

        case 'actualizarCausaExternaHc':
            $('#cmbCausaExterna-ok').show();
            $('#cmbCausaExterna-loader').hide();
            $('#cmbCausaExterna-error').hide();
            break;

        case 'actualizarFinalidadHc':
            $('#cmbFinalidadConsulta-ok').show();
            $('#cmbFinalidadConsulta-loader').hide();
            $('#cmbFinalidadConsulta-error').hide();
            break;

        case 'actualizarDxIngresoHC':
            $('#txtIdDxIngreso-ok').show();
            $('#txtIdDxIngreso-loader').hide();
            $('#txtIdDxIngreso-error').hide();
            break;

        case 'eliminarRegistroLaboratorioExterno':
            $("#loader_" + accion).replaceWith(window.botoneliminarRegistroLaboratorioExterno);
            break;

        case 'ingresoResultadoExterno':
            $("#loader_" + accion).replaceWith(window.botonIngresoResultadoExterno);
            break;

        case 'registrarArchivoHelp':
            $("#loader_" + accion).replaceWith(window.botonRegistrarArchivoHelp);
            break;

        case 'cerrarEncuestaPaciente':
            $("#loader_" + accion).replaceWith(window.btnCerrarEncuesta);
            break;
        case "crearFolio":
            $("#loader_" + accion).replaceWith(window.botonCrearFolio);
            break;

        case "eliminarFacturasRips":
            $("#loader_" + accion).replaceWith(window.botonEliminarFacturasRips);
            break;

        case "firmarFolio":
            $("#loader_" + accion).replaceWith(window.botonFirmarFolio);
            break;

        case "crearArchivosRips":
            $("#loader_" + accion).replaceWith(window.botonCrearRips);
            break;

        case "crearFacturasBaseNominalNefro":
            ocultarSw2()
            break;

        case 'numerarFacturasBloque':
            ocultarSw2()
            break;

        case 'accionesAlEstadoFolio':
            $(`#${accion}`).hide();
            break;

        case 'eliminarFolio':
            $(`#accionesAlEstadoFolio`).hide();
            break;

        default:
            break;
    }
}

function obtenerValoresCheckList(className) {
    let checksList = document.querySelectorAll(`.${className}`);
    let checksArr = [];
    for (let i = 0; i < checksList.length; i++) {
        if (checksList[i].checked) {
            checksArr.push(checksList[i].value);
        }
    }
    return checksArr;
}

function botonOrdenesPendientes() {
    valores_a_mandar = ""
    add_valores_a_mandar(traerDatoFilaSeleccionada("listAdmisionCuenta", "ID"));
    $.ajax({
        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        type: "POST",
        data: {
            "idQuery": 2643,
            "parametros": valores_a_mandar,
            "rows": -1,
            "page": 1,
            "sord": "asc"
        },
        success: function (data) {
            //alert(new XMLSerializer().serializeToString(data));

            let respuesta = data.getElementsByTagName('cell');
            try {
                let pendiente = respuesta[1].firstChild.data;
                if (pendiente == 'SI') {
                    //console.log("Debe parpadear");
                    $('#btnProgramcacionPendiente').addClass("btn-parpadea");
                } else {
                    $('#btnProgramcacionPendiente').removeClass("btn-parpadea");
                }
            } catch (error) {
                $('#btnProgramcacionPendiente').removeClass("btn-parpadea");
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log("error Al verificar ordenes pendientes para pintar boton.");
        }
    });
}

function mostrarVentanaHcAdmision() {
    asignaAtributo('lblIdAuxiliar', IdSesion(), 0);
    asignaAtributo('lblIdPaciente', valorAtributoIdAutoCompletar('txtIdBusPaciente'), 0);
    asignaAtributo('lblIdAdministradora', valorAtributo('lblIdPlan'), 0);
    mostrar('divVentanitaHistoricosAtencion');
    setTimeout(() => {
        buscarHC('listDocumentosHistoricosAdm', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
    }, 200);
}

function mostrarVentanaOrdenExterna() {
    let admision = $("#listAdmisionCuenta").jqGrid('getGridParam', 'selrow')
    if (admision != null) {
        limpiaAtributo("txtIdProcedimientoOrdenExt", 0)
        limpiaAtributo("txtCantidadOrdenExt", 0)
        limpiaAtributo("cmbNecesidadOrdenExt", 0)
        limpiaAtributo("txtIndicacionOrdenExt", 0)
        limpiaAtributo("txtDxRelacionadoOrdenExt", 0)
        mostrar("divOrdenesExternas");
        setTimeout(() => {
            buscarHC("listaProcedimientosOrdenExternaAdmision");
        }, 200);
    } else {
        alert("FALTA SELECCIONAR UNA ADMISION")
    }
}

function validarMax(obj, max) {
    //console.log("ENTRO")
    var maximo = parseInt(max)
    if (obj.value.length === maximo) return false;
}

function buscarFolioAyudasDx() {
    $('#listaOrdenesApoyoDx').jqGrid('clearGridData');
    ocultar("#tabsSubirDocumentos");
    setTimeout(() => {
        buscarHC('listaOrdenesApoyoDx')
    }, 100);
}

function traerVentanaCitasPaciente() {
    if (valorAtributoIdAutoCompletar("txtIdBusPaciente") == "") {
        alert("NO SE HA BUSCADO NINGUN PACIENTE.")
    } else {
        mostrar('divVentanitaPacienteYaExiste')
        limpTablas('listDocumentosHistoricos');
        ocultar('divArchivosAdjuntos')
        asignaAtributo('lblIdPacienteExiste', decodeURI(valorAtributo("txtIdBusPaciente")), 0)
        asignaAtributo('lblLetrero', ' PACIENTE  YA EXISTE  !!!', 0)
        asignaAtributo("cmbIdEspecialidadCitasPaciente", valorAtributo("cmbIdEspecialidad"), 0)
        setTimeout(() => {
            buscarAGENDA("listaCitasPaciente")
            setTimeout(() => {
                buscarAGENDA('listHistoricoListaEsperaEnAgenda')
            }, 400);
        }, 200);
        //document.getElementById('divContenidoListaEsperaHist').innerHTML = '<table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>';
    }

}

$("#upload-form").live("submit", function (e) {
    e.preventDefault()

    var form = $('#upload-form')[0];

    var data = new FormData(document.getElementById("upload-form"));

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/clinica/paginas/hc/hc/subir_archivo.jsp', true);
    xhr.send(data);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                alert(this.responseText)
            } else {
                alert("No ha sido posible subir el archivo. Por favor intente nuevamente o pongase en contacto con el equipo de soporte.")
            }
        } else {

        }
    };

    /*$.ajax({
        type: "POST",
        method: "POST",
        url: "/clinica/paginas/hc/hc/subir_archivo.jsp",
        data: data,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache : false,
        beforeSend: function (){
        },
        success: function (data){
            alert(data.respuesta)
            console.log("SUCCESS : ", data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(XMLHttpRequest.responseText)
            console.log("ERROR : ", e);

        }
    });*/
});


function traerFormularioArchivo() {
    var configuracion_ventana = "width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230";
    window.open("http://190.60.243.113:8081/upload/", "Pagina_CNN", configuracion_ventana);
    /*$.ajax({
        url: "http://190.60.243.113:8081/upload/",
        type: "GET",
        beforeSend: function () { },
        success: function (data) {
            $("#divSubirArchivo").html(data)
        },
        complete: function (jqXHR, String) {
            buscarHistoria('listLaboratoriosPdf')
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus)
        }
    });*/
}

function autocompletarNombreUsuario() {
    var pnombre = valorAtributo("txtPNombre").charAt().trim()
    var snombre = valorAtributo("txtSNombre").charAt().trim()
    var papellido = valorAtributo("txtPApellido").trim()
    var sapellido = valorAtributo("txtSApellido").charAt().trim()

    asignaAtributo("txtUSuario", pnombre + snombre + papellido + sapellido, 0)
}

function autocompletarNombrePersonal() {
    var pnombre = valorAtributo("txtPNombre").trim()
    var snombre = valorAtributo("txtSNombre").trim()
    var papellido = valorAtributo("txtPApellido").trim()
    var sapellido = valorAtributo("txtSApellido").trim()

    asignaAtributo("txtNomPersonal", pnombre + " " + snombre + " " + papellido + " " + sapellido, 0)
}


function cargarInfoOrdenProgramacion() {
    var id_orden = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');

    var condicion1 = valorAtributo("lblIdSedeAgendaOrdenes")
    var condicion2 = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(id_orden).ID_PROCEDIMIENTO
    var condicion3 = valorAtributoIdAutoCompletar("txtAdministradoraPacienteOrdenes")

    $.ajax({
        url: "/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp",
        type: "POST",
        data: { "idQueryCombo": 209, "cantCondiciones": "tres", "condicion1": condicion1, "condicion2": condicion2, "condicion3": condicion3 },
        beforeSend: function () { },
        success: function (data) {
            cod = data.getElementsByTagName('id');
            text = data.getElementsByTagName('nom');

            if (cod[0] !== undefined) {
                asignaAtributo("txtAdministradora1Ordenes", cod[0].firstChild.data + "-" + text[0].firstChild.data, 0)
            } else {
                limpiaAtributo("txtAdministradora1Ordenes", 0)
            }
        },
        complete: function (jqXHR, String) {
            buscarAGENDA("listaProgramacionTerapiasAgenda");
            setTimeout(() => {
                buscarAGENDA("listaProgramacionLE");
            }, 300);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus)
        }
    });
}

function traerPlanesDeContratacionAgendaOrdenes() {
    terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');
    var id_procedimiento = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).ID_PROCEDIMIENTO

    id_administradora = valorAtributoIdAutoCompletar("txtAdministradora1Ordenes")
    id_regimen = valorAtributo("cmbIdTipoRegimenOrdenes").split("-")[0]
    id_tipo_plan = valorAtributo("cmbIdTipoRegimenOrdenes").split("-")[1]
    sede = valorAtributo("lblIdSedeAgendaOrdenes")

    cargarComboGRALCondicion5('',
        '',
        "cmbIdPlanOrdenes",
        202,
        id_administradora,
        id_regimen,
        id_tipo_plan,
        id_procedimiento,
        sede)
}

function cargarComboRegimenOrdenes() {
    terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');

    var id_procedimiento = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).ID_PROCEDIMIENTO

    cargarComboGRALCondicion3('',
        '',
        'cmbIdTipoRegimenOrdenes',
        201,
        valorAtributoIdAutoCompletar('txtAdministradora1Ordenes'),
        valorAtributo('cmbSede'),
        id_procedimiento);
}

function mostrarOrdenesProgramadas() {
    mostrar("divElementosPendientes");
    setTimeout(() => {
        $('#tabsElementosPendientes').tabs('select', 0);
        buscarFacturacion("listaHcSinFactura")
    }, 100);
}

function prepararVentana() {
    document.getElementById('divContenidoListaEsperaHist').innerHTML = '<table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>';
}


function copiarCitas() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.onreadystatechange = respuestaCopiarCitas;

    if (valorAtributo('cmbTipoCopiado') == 1) {
        fechas = [];
        ids = $("#listDias").getDataIDs();

        for (var i = 0; i < ids.length; i++) {
            var c = "jqg_listDias_" + ids[i];
            if ($("#" + c).is(":checked")) {
                datosRow = $("#listDias").getRowData(ids[i]);
                fechas.push(datosRow.Fecha2);
            }
        }

        citas = [];
        ids = $("#listAgenda").getDataIDs();

        for (var i = 0; i < ids.length; i++) {
            var c = "jqg_listAgenda_" + ids[i];
            if ($("#" + c).is(":checked")) {
                datosRow = $("#listAgenda").getRowData(ids[i]);
                citas.push(datosRow.ID);
            }
        }

        if (citas.length <= 0) {
            alert("DEBE SELECCIONAR AL MENOS UNA CITA")
            return
        }

        if (fechas.length <= 0) {
            alert("DEBE SELECCIONAR AL MENOS UN DIA")
            return
        }
        valores_a_mandar = "accion=copiarCitas";
        valores_a_mandar = valores_a_mandar + "&fechas=" + fechas + "&citas=" + citas;
        varajaxInit.send(valores_a_mandar);
    } else if (valorAtributo('cmbTipoCopiado') == 2) {
        modificarCRUD('copiarCitasFrecuencia')
    } else {
        alert("Ninguna accion para el tipo seleccionado");
    }
}

function respuestaCopiarCitas() {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            alert("Accion terminada.")
            buscarAGENDA("listAgenda")
            $("#copiarCitas").removeAttr('checked');
            ocultar('divOpcionesCopiado')
            ocultar('divOpcionesFrecuencia')
            setTimeout(() => {
                buscarAGENDA("listDias")
            }, 200);
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
}

function verificarEnvioFactura() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=auditarFactura";
    valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
    varajaxInit.send(valores_a_mandar);
}

function tabActivoOrdenesAgenda(idDiv) {
    switch (idDiv) {
        case 'divOrdenesPendientes':
            setTimeout(() => {
                buscarAGENDA('listCoordinacionTerapiaOrdenadosAgenda')
                setTimeout(() => {
                    buscarAGENDA('listaProgramacionTerapiasAgenda')
                    setTimeout(() => {
                        buscarAGENDA('listaProgramacionLE')
                    }, 300);
                }, 300);
            }, 200);
            break;

        case 'divOrdenesHistoricas':
            setTimeout(() => {
                buscarAGENDA('listCoordinacionTerapiaOrdenadosAgenda')
                setTimeout(() => {
                    buscarAGENDA('listaProgramacionTerapiasAgenda')
                    setTimeout(() => {
                        buscarAGENDA('listaProgramacionLE')
                    }, 300);
                }, 300);
            }, 200);
            break;

        case 'divOrdenesExternas':
            buscarHC("listaProcedimientosOrdenExterna");
            break;
    }
}


function tabActivoFoliosParametros(idDiv) {
    switch (idDiv) {
        case 'divFoliosTipoAdmision':
            setTimeout(() => {
                buscarHC('listGrillaTiposCita', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                setTimeout(() => {
                    buscarHC('listGrillaFolioProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                }, 200);
            }, 500);
            break;

        case 'divFoliosProfesion':
            buscarHC('listGrillaProfesiones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;

        case 'divSolicitudes':
            buscarHC('listGrillaAutorizaciones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;
    }
}

function tabActivoPlanesContratacion(idDiv) {
    switch (idDiv) {
        case 'divProcedimientosPlan':
            buscarFacturacion('listGrillaPlanesDetalle')
            setTimeout(() => {
                buscarFacturacion('listGrillaPlanExcepcion')
            }, 300);
            break;

        case 'divArticulosPlan':
            buscarFacturacion('listGrillaPlanesMedicamentos')
            break;

        case 'divRangos':
            buscarFacturacion('listGrillaRangos')
            break;

        case 'divLiquidacion':
            buscarFacturacion('listGrillaContratos')
            break;

        case 'divLiquidacionProcedimientos':
            buscarFacturacion('listGrillaProcedimientos')
            break;

        case 'divMunicipiosPlan':
            buscarFacturacion('listGrillaMunicipios')
            break;

        case 'divSedesPlan':
            buscarFacturacion('listSedePlan')
            break;
    }
}

function reiniciarElementosNuevoPacienteEncuesta() {
    $("#sw_nuevo_paciente").removeAttr("checked");
    ocultar("elementosNuevoPaciente")
    limpiaAtributo("cmbTipoIdUrgencias", 0)
    limpiaAtributo("txtIdentificacionUrgencias", 0)
}

function elementosNuevoPacienteEncuesta() {
    if ($('#sw_nuevo_paciente').attr('checked')) {
        mostrar("elementosNuevoPaciente")
    } else {
        ocultar("elementosNuevoPaciente")
    }
}

function abrirvideollamada() {
    var url = "";
    url = "http://18.211.197.195:8383/clinica/login_crm,jsp"
    window.open(url);
}

function reaccionAEvento(idElemento) {
    switch (idElemento) {
        case 'guardarCitasBloque':
            if (valorAtributo('cmbIdProfesionales') != '') {
                setTimeout(() => {
                    buscarAGENDA("listDias")
                }, 200);

                if ($('#guardarCitasBloque').attr('checked')) {
                    mostrar('divOpcionesCopiado');
                    if (valorAtributo('cmbTipoCopiado') == '2') {
                        mostrar('divOpcionesFrecuencia')
                    } else {
                        ocultar('divOpcionesFrecuencia')
                    }
                } else {
                    ocultar('divOpcionesCopiado');
                    ocultar('divOpcionesFrecuencia')
                }
            } else {
                alert("FALTA SELECCIONAR AGENDA")
                if ($('#guardarCitasBloque').attr('checked')) {
                    $("#guardarCitasBloque").attr('checked', false)
                } else {
                    $("#guardarCitasBloque").attr('checked', true)
                }
            }
            break;

        case "cmbBloqueCita":
            if (valorAtributo(idElemento) == "S") {
                $("[id='tdElementosCitasBloque']").show();
            } else {
                $("[id='tdElementosCitasBloque']").hide();
            }
            break;

        case "cmbIdIdProceso":
            if (valorAtributo(idElemento) == "3") {
                $("[id='tdModuloHelp']").show();
                document.getElementById("cmbIdIModulo").disabled = false;
            } else {
                $("[id='tdModuloHelp']").hide();
                document.getElementById("cmbIdIModulo").value = ' ';
            }
            break;

        case 'chkFrecuencia':
            if ($('#chkFrecuencia').attr('checked')) {
                mostrar("tdFrecuencia")
            } else {
                ocultar("tdFrecuencia")
            }
            break;

        case 'tipoCopia':
            if ($('input:radio[name=tipoCopia]:checked').val() == "1") {
                mostrar('divOpcionesFrecuencia')
            } else {
                ocultar('divOpcionesFrecuencia')
            }
            break;

        case 'copiarCitas':
            if (valorAtributo('cmbIdProfesionales') != '') {
                setTimeout(() => {
                    buscarAGENDA("listDias")
                    buscarAGENDA("listAgenda")
                }, 200);

                if ($('#copiarCitas').attr('checked')) {
                    mostrar('divOpcionesCopiado');
                    if (valorAtributo('cmbTipoCopiado') == '2') {
                        mostrar('divOpcionesFrecuencia')
                    } else {
                        ocultar('divOpcionesFrecuencia')
                    }
                } else {
                    ocultar('divOpcionesCopiado');
                    ocultar('divOpcionesFrecuencia')
                }
            } else {
                alert("FALTA SELECCIONAR AGENDA")
                if ($('#copiarCitas').attr('checked')) {
                    $("#copiarCitas").attr('checked', false)
                } else {
                    $("#copiarCitas").attr('checked', true)
                }
            }
            break;

        case 'copiarCitasAgenda':
            if (valorAtributo('cmbIdProfesionales') != '') {
                setTimeout(() => {
                    buscarAGENDA("listDias")
                }, 200);

                if ($('#copiarCitasAgenda').attr('checked')) {
                    mostrar('divOpcionesCopiado');
                    if (valorAtributo('cmbTipoCopiado') == '2') {
                        mostrar('divOpcionesFrecuencia')
                    } else {
                        ocultar('divOpcionesFrecuencia')
                    }
                } else {
                    ocultar('divOpcionesCopiado');
                    ocultar('divOpcionesFrecuencia')
                }
            } else {
                alert("FALTA SELECCIONAR AGENDA")
                if ($('#copiarCitasAgenda').attr('checked')) {
                    $("#copiarCitasAgenda").attr('checked', false)
                } else {
                    $("#copiarCitasAgenda").attr('checked', true)
                }
            }
            break;

        case 'cmbTipoAlta':
            if (valorAtributo("cmbTipoAlta") == "6") {
                mostrar("trInfoMuerte")
            } else {
                ocultar("trInfoMuerte")
            }
            break;

        case 'cmbOrdenenesAdmisionURG':
            if (valorAtributo("cmbOrdenenesAdmisionURG") == "1") {
                cargarComboGRAL('', '', 'cmbTipoAlta', 161);
            } else {
                asignaAtributoCombo("cmbTipoAlta", "", "");
            }
            ocultar("trInfoMuerte");
            break;

        case 'sw_fecha_actual':
            if ($('#sw_fecha_actual').attr('checked')) {
                $("#txtFechaMuerte").attr('disabled', 'disabled');
                $("#txtHoraMuerte").attr('disabled', 'disabled');
                $("#txtMinutoMuerte").attr('disabled', 'disabled');
                $("#cmbPeriodoMuerte").attr('disabled', 'disabled');
            } else {
                $("#txtFechaMuerte").removeAttr('disabled');
                $("#txtHoraMuerte").removeAttr('disabled');
                $("#txtMinutoMuerte").removeAttr('disabled');
                $("#cmbPeriodoMuerte").removeAttr('disabled');
            }
            break;
    }
}

function limpiarCamposURG(arg) {
    switch (arg) {
        case "limpiarDatosPaciente":
            limpiaAtributo("txtIdBusPacienteUrgencias", 0)
            limpiaAtributo("lblIdPacienteUrgencias", 0)
            break;

        case "limpiarDatosBusquedaURG":
            limpiaAtributo("cmbTipoIdUrgencias", 0)
            limpiaAtributo("txtIdentificacionUrgencias", 0)
            limpiaAtributo("txtIdBusPacienteUrgencias", 0)
            limpiaAtributo("lblIdPacienteUrgencias", 0)
            limpiaAtributo("cmbIdTipoServicioUrgencias", 0)
            limpiaAtributo("cmbEstadoAdmisionURG", 0)
            limpiaAtributo("cmbPreferencialURG", 0)
            break;

        default:
            break;
    }
}

function traerIdUltimoPacienteURG(tipo_id) {
    cantColumnasP = 1;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarReportes_xml.jsp', true);
    varajaxInit.onreadystatechange = respuestaTraerIdUltimoPacienteURG;

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=listadoReporteExcel";
    valores_a_mandar = valores_a_mandar + "&id=3";
    valores_a_mandar = valores_a_mandar + "&parametro1=" + tipo_id;
    varajaxInit.send(valores_a_mandar);
}

function respuestaTraerIdUltimoPacienteURG() {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('rows') != null) {
                max_id = raiz.getElementsByTagName('c1')[1].firstChild.data;
                asignaAtributo("txtIdentificacionUrgencias", max_id, 0)
            } else {
                alert("Error al crear paciente. Intente nuevamente")
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
}




function traerAdmisionPacienteURG() {
    ocultar("divmodificarAdmisionPacienteURG")
    ocultar("divVentanitaAdmisionUrgencias")
    asignaAtributo("txtIdBusPaciente", valorAtributo("lblIdPacienteModificarURG") + "-" + valorAtributo("lblNombrePacienteModificarURG"), 0)
    asignaAtributo("lblIdAdmision", valorAtributo("lblIdAdmisionModificarURG"), 0)
    asignaAtributo("lblIdFactura", valorAtributo("lblIdFacturaModificarURG"), 0)
    setTimeout(() => {
        buscarInformacionBasicaPaciente()
    }, 200);

}

function traerInfoServicio() {
    if (true) {
        informacionBasicaServicio(valorAtributo("cmbIdTipoServicio"));
    }
}

function informacionBasicaServicio(servicio) {
    cantColumnasP = 1;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarReportes_xml.jsp', true);
    varajaxInit.onreadystatechange = respuestaInformacionBasicaServicio;

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=listadoReporteExcel";
    valores_a_mandar = valores_a_mandar + "&id=2";
    valores_a_mandar = valores_a_mandar + "&parametro1=" + servicio;
    varajaxInit.send(valores_a_mandar);
}

function traerPlanesDeContratacionListaEspera(idElemDestino, idQuery, condicion1, condicion2) {
    numero_cita_seleccionada = $("#listAgenda").jqGrid('getGridParam', 'selrow')

    id_administradora = valorAtributoIdAutoCompletar(condicion1)
    id_regimen = valorAtributo(condicion2).split("-")[0]
    id_tipo_plan = valorAtributo(condicion2).split("-")[1]
    sede = valorAtributo("cmbSede")

    cargarComboGRALCondicion4('', '', idElemDestino, idQuery, id_administradora, id_regimen, id_tipo_plan, sede)
}


function traerPlanesDeContratacionAgenda(idElemDestino, idQuery, condicion1, condicion2) {
    numero_cita_seleccionada = $("#listAgenda").jqGrid('getGridParam', 'selrow')

    id_administradora = valorAtributoIdAutoCompletar(condicion1)
    id_regimen = valorAtributo(condicion2).split("-")[0]
    id_tipo_plan = valorAtributo(condicion2).split("-")[1]
    sede = $("#listAgenda").getRowData(numero_cita_seleccionada).ID_SEDE
    if (sede == undefined) {
        sede = valorAtributo("cmbSede")
    }

    cargarComboGRALCondicion4('', '', idElemDestino, idQuery, id_administradora, id_regimen, id_tipo_plan, sede)
}

/**
 //funcion actualizada de traerPlanesDeContratacionAgenda()
 * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>  
*/
async function traerPlanesDeContratacionAgenda_1(idElemDestino, idQuery, condicion1, condicion2) {
    try {
        const numero_cita_seleccionada = $("#listAgenda").jqGrid('getGridParam', 'selrow');
        const id_administradora = valorAtributoIdAutoCompletar(condicion1);
        const id_regimen = valorAtributo(condicion2).split("-")[0];
        const id_tipo_plan = valorAtributo(condicion2).split("-")[1];

        let sede = $("#listAgenda").getRowData(numero_cita_seleccionada).ID_SEDE;
        if (sede === undefined) {
            sede = valorAtributo("cmbSede");
        }

        await cargarComboGRALCondicion4_1('', '', idElemDestino, idQuery, id_administradora, id_regimen, id_tipo_plan, sede);
    } catch (error) {
        console.error(error);
    }
}


function traerPlanesDeContratacion(idElemDestino, idQuery, condicion1, condicion2, facturacionEnBloque) {
    id_administradora = valorAtributoIdAutoCompletar(condicion1)
    id_regimen = valorAtributo(condicion2).split("-")[0]
    id_tipo_plan = valorAtributo(condicion2).split("-")[1]
    if (facturacionEnBloque) {
        sede = valorAtributo('cmbSede')
    } else {
        sede = IdSede()
    }


    cargarComboGRALCondicion4('', '', idElemDestino, idQuery, id_administradora, id_regimen, id_tipo_plan, sede)
}

function cargarComboGRALCondicion5(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2, condicion3, condicion4, condicion5) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=5&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4 + "&condicion5=" + condicion5;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);

}

function cargarComboGRALCondicion4(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2, condicion3, condicion4) {
    if (condicion1 != '' && condicion2 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=4&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarComboGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    } else {
        $("#" + comboDestino).empty();
        $("#" + comboDestino).append(new Option("[ SELECCIONE ]", "", true, true));
    }
}

/**
 //funcion actualizada de cargarComboGRALCondicion4()
 * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>  
*/
function cargarComboGRALCondicion4_1(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2, condicion3, condicion4) {
    if (condicion1 != '' && condicion2 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=4&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4;

        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp',
                data: valores_a_mandar,
                contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
                beforeSend: function () {

                },
                success: function (data) {
                    respuestacargarComboGRAL_1(data)
                    resolve()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            })
        })

    } else {
        $("#" + comboDestino).empty();
        $("#" + comboDestino).append(new Option("[ SELECCIONE ]", "", true, true));
    }
}


function respuestaInformacionBasicaServicio() {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('rows') != null) {

                totalRegistros = raiz.getElementsByTagName('c1').length;

                if (totalRegistros) {
                    agregarDatosTablaHistoricosAtencion(raiz, totalRegistros);
                } else {
                    alert("Sin datos en el reporte, cierre y vuelva a generarlo");
                }

            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
}

function copiarContenido(idElemento) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(valorAtributo(idElemento).replace(/%20/g, " ")).select();
    document.execCommand("copy");
    $temp.remove();

    $("#dialog").dialog({
        open: setTimeout(() => {
            $("#dialog").dialog("destroy");
        }, 1000),
        title: "Elemento copiado",
        height: 0,
        minHeight: 0
    });
}

function mostrarMotivosAbrir() {
    if (valorAtributo('cmbAccionFolio') != 0) {
        document.getElementById('cmbMotivosAbrir').innerHTML = '<option value=""></option>';
    } else {
        cargarComboGRALCondicion1('cmbPadre', '1', 'cmbMotivosAbrir', 155, valorAtributo('lblIdAdmision'));
    }
}

function fe() {
    select = document.getElementById("año");
    for (i = 2000; i <= 2050; i++) {
        option = document.createElement("option");
        option.value = i;
        option.text = i;
        select.appendChild(option);
    }
}

function mostrarDivVentanaDescuento() {

    if (verificarCamposGuardar('mostrarDivVentanaDescuento')) {

        if (valorAtributo('cmbIdTipoRegimen').split('-')[0] == 'A') {
            asignaAtributo('lblValorBaseDescuento', valorAtributo('lblValorCubierto'), 0)
        } else {
            asignaAtributo('lblValorBaseDescuento', valorAtributo('lblValorNoCubierto'), 0)
        }
        mostrar('divVentanitaDescuentoFactura')
    }

}

//funcion repetida linea 6379
/* function checkKey2(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }

    if (unicode == 13) {
        guardarYtraerDatoAlListado('buscarIdentificacionPaciente')
    }
} */

function checkKey3(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }

    if (unicode == 13) {
        if (verificarCamposGuardar("nuevoIdPacienteUrgencias")) {
            guardarYtraerDatoAlListado('buscarIdentificacionPacienteUrgencias')
        }
    }
}

function checkKeyHistoricos(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }

    if (unicode == 13) {
        buscarHC('listDocumentosHistoricos')
    }

}

function nuevoPacienteEncuesta() {
    if (verificarCamposGuardar("nuevoIdPacienteUrgencias")) {
        guardarYtraerDatoAlListado('buscarIdPacienteEncuesta')
    }
}

function mostrarDivVentanaDescuentoArticulo() {
    if (verificarCamposGuardar('mostrarDivVentanaDescuento')) {
        asignaAtributo('lblValorBaseDescuento', valorAtributo('lblValorCubierto'), 0)
        mostrar('divVentanitaDescuentoFactura')
    }
}

function numerarFactura() {
    if (valorAtributo('lblIdFactura') != '') {
        var idFactura = valorAtributo('lblIdFactura');
        var idEstadoFactura = valorAtributo('lblIdEstadoFactura');
        var idDoc = "";
        formatoPDFFactura(idFactura, idEstadoFactura, idDoc);
    } else {
        alert('SELECCIONE UNA ADMISION CON FACTURA');
    }
}

function numerarFacturaArticulo() {
    if (valorAtributo('lblIdFactura') != '') {

        var ids = jQuery("#listArticulosDeFactura").getDataIDs();

        if (ids.length != 0) {

            var idFactura = valorAtributo('lblIdFactura');
            var idEstadoFactura = valorAtributo('lblIdEstadoFactura');
            //var idCuenta = valorAtributo('lblIdCuenta');

            //formatoPDFFactura(idFactura, idEstadoFactura,idCuenta);
            formatoPDFFactura(idFactura, idEstadoFactura);
        } else {
            alert('DEBE AGREGAR ALGUN ARTICULO PARA VER LA FACTURA');
        }
    } else {
        alert('SELECCIONE UN DOCUMENTO');
    }
}


function eliminarElementoGrilla(list, rowid) {

    var ids = jQuery('#' + list).getDataIDs();
    var data = [''];
    var p;

    for (var i = 0; i < ids.length; i++) {
        if (ids[i] === rowid) {
            p = i;
        } else {
            data.push(jQuery('#' + list).getRowData(ids[i]));
        }
    }

    ids.splice(p, 1)

    jQuery('#' + list).clearGridData()

    for (var i = 0; i < ids.length; i++) {
        jQuery('#' + list).addRowData(i, data[i + 1]);
    }
}


function verificaTipo() {

    if (valorAtributo('cmbIdTipoDocumento') == '36') {
        asignaAtributo('txtDocEgreso', '', 0);
    } else {
        asignaAtributo('txtDocEgreso', '', 1);
    }
}

function llamarIndicacionAdx() {
    var texto = "CON MEDIO DE CONTRASTE ( FLUORESCEINA SODICA)";
    var valor = valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
    if (valor == '951201') {
        asignaAtributo('txtIndicacionAD', texto, 0);
    }
}

function enviarConsultaCita() { /*ESTA FUNCION SE ENCUENTRA DUPLICADA POR CONCEPTOS DE BODY, EN LA PAGINA DE consultaCitas.jsp*/
    var nuevaURL = "";
    //    if( document.getElementById('cmbTipoId').value =='' && document.getElementById('txtIdentificacionPaciente').value =='' ){ 

    var tipoId = document.getElementById('cmbTipoId').value;
    var identificacion = document.getElementById('txtIdentificacionPaciente').value;

    nuevaURL = "/clinica/paginas/ireports/citas/generaCitas.jsp?reporte=consultaCitas&tipoId=" + tipoId + "&id=" + identificacion.trim();
    window.open(nuevaURL, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");

    //	    }		else alert('DEBE ESCRIBIR SU TIPO Y NUMERO DE IDENTIDAD')
}

function consultaSolicitudAutorizacion(caso) {
    var nuevaURL = "";
    switch (caso) {
        case 'solicitud':
            if (valorAtributo('lblIdSolicitud') != '') {
                var id = valorAtributo('lblIdSolicitud')
                nuevaURL = "/clinica/paginas/ireports/solicitudes/generaIntercambio.jsp?reporte=solicitud&id=" + id;
                window.open(nuevaURL, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");
            } else alert('DEBE SELECCIONAR LA SOLICITUD')
            break;
        case 'autorizacion':
            if (valorAtributo('lblIdAutorizacion') != '') {
                var id = valorAtributo('lblIdAutorizacion')
                nuevaURL = "/clinica/paginas/ireports/solicitudes/generaIntercambio.jsp?reporte=autorizacion&id=" + id;
                window.open(nuevaURL, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");
            } else alert('DEBE SELECCIONAR LA AUTORIZACION')
            break;

    }

}

function formatoReportPDFIntercambio() {
    if (valorAtributo('lblIdEstadoFolio') == '1') {
        var openedWindow;
        var idReporte = valorAtributo('lblIdDocumento');
        var reporte = valorAtributo('lblTipoDocumento');
        var nuevaURL = "/clinica/paginas/ireports/hc/to_pdf.jsp?reporte=" + reporte + "&evo=" + idReporte;
        //openedWindow=window.open(nuevaURL);  ../../ireports/hc/to_pdf.jsp	
        window.open(nuevaURL, '_blank');
        window.open(this.href, '_self');
    } else alert('FOLIO DEBE ESTAR FINALIZADO')

}

function formatoReportPDFIntercambio2() {
    //    if (valorAtributo('lblIdEstadoFolio') == '1') {
    var openedWindow;
    var idReporte = valorAtributo('lblIdDocumento');
    var reporte = valorAtributo('lblTipoDocumento');
    var imagenEnvio = valorAtributo('imgCanvas');
    alert(imagenEnvio)
    var nuevaURL = "/clinica/paginas/hc/guardaFirma.jsp?reporte=" + reporte + "&evo=" + idReporte + "&imgEnvio=" + imagenEnvio;

    window.open(nuevaURL, '_blank');
    window.open(this.href, '_self');
    //  } else alert('FOLIO DEBE ESTAR FINALIZADO')

}

function verificarSerialMod() {
    var serial = valorAtributo('txtSerial');
    if (serial != '') {
        cargarElementCondicionDosCodBarras('txtRespuestaSerial', 540, serial, 'N');
    }
}

function respuestaVSerialMod() {
    vSerial = valorAtributo('txtRespuestaSerial');
    if (vSerial == 0) {
        modificarCRUD('modificaTransaccionSerial')
    } else {
        alert('El serial ya se encuentra registrado.. ! ');
    }
}
/* fn llamada desde entrada btn crear y desde fn registrarSerial */
function verificarSerial() {
    var serial = valorAtributo('txtSerial');
    if (serial != '') {
        cargarElementCondicionDosCodigoBarras('txtRespuestaSerial', 540, serial, 'N');
    } else {
        modificarCRUD('crearTransaccion');
    }
}

/* fn llamada en la respuesta de cargarElementCondicionDosCodigoBarras */
function respuestaVSerial() {
    vSerial = valorAtributo('txtRespuestaSerial');
    if (vSerial == 0) {
        modificarCRUD('crearTransaccion');
    } else {
        alert('El serial ya se encuentra registrado ! ');
    }
}

/* funcion para entradas accionada desde txtSerial */
function registrarSerial() {
    if (valorAtributo('lblIdEstado') == '0') {
        if (valorAtributo('lblIdTransaccion') != '') {
            verificarSerialMod()
        } else {
            verificarSerial()
        }
    } else alert('EL DOCUMENTO DEBE ESTAR ABIERTO')
}
/* solo para salidas llamada desde cargarElementoCondicionDosCodigoBarras*/
function codigoBarras() {
    valorCodigo = valorAtributo('lblTraidosCodBarras');
    if (valorCodigo != '') {
        var codigoB = valorCodigo.split('_-_');
        id_art = codigoB[0];
        articulo = codigoB[1];
        lot = codigoB[2];
        existencia = codigoB[3];
        vlr_unitario = codigoB[4];
        f_venc = codigoB[5];
        medida = codigoB[6];

        if (valorAtributo('txtVenCodBarras') == 1) {
            asignaAtributo('txtIdArticulo', id_art + '-' + articulo, 0);
            asignaAtributo('lblValorUnitario', vlr_unitario, 0);

            // valores en div oculto para el registro de la salida
            asignaAtributo('txtIdArticulo_dos', id_art, 0);
            asignaAtributo('txtIdLote_dos', lot, 0);
            asignaAtributo('txtValorUnitario_dos', vlr_unitario, 0);
            asignaAtributo('txtFV_dos', f_venc, 0);
            asignaAtributo('lblMedida', medida, 0);

        } else {
            asignaAtributo('lblIdArticulo', articulo, 0);

            //1
            asignaAtributo('lblValorUnitarioCodBarra', vlr_unitario, 0);
            //2
            var vlrSinIva = (Math.round((vlr_unitario / 1.19).toFixed(1)));
            asignaAtributo('lblSinIvaCodBarra', vlrSinIva + '', 0);
            //3
            var vlrIva = (Math.round(((vlrSinIva * 19) / 100).toFixed(1)));
            asignaAtributo('lblValorIvaCodBarra', vlrIva + '', 0);

        }

        if ($('#txtSerial').length) {
            asignaAtributo('txtSerial', valorAtributo('txtIdBarCode'));
        }

        asignaAtributo('txtIdLote1', lot, 0);
        asignaAtributo('txtIdLoteBan', 'SI', 0);
        asignaAtributo('txtCantidad', '1', 0);
        asignaAtributo('lblIva', '0', 0);
        asignaAtributo('txtFV', f_venc, 0);
        var cantidad = parseInt(valorAtributo('txtCantidad'));
        var valorUnitario = valorAtributo('txtValorUnitario_dos');
        var val_iva = valorAtributo('lblIva');
        var subTotal = (valorUnitario / ((val_iva / 100) + 1)) * cantidad;
        var lblValorImpuesto = subTotal.toFixed(2) * ((val_iva / 100));
        asignaAtributo('lblValorImpuesto', lblValorImpuesto.toFixed(2), 0);
        setTimeout("$('#txtIdBarCode').focus();", 400);
        setTimeout("modificarCRUD('crearTransaccion_dos')", 300);

        if ($('#txtCodBarrasBus').length) {
            limpiaAtributo('txtCodBarrasBus', 0);
        }
    }
}

function codigoBarrasVentas() {
    valorCodigo = valorAtributo('lblDatosCodigoBarraVentas');
    if (valorCodigo != '') {
        var codigoB = valorCodigo.split('_-_');
        id_art = codigoB[0];
        articulo = codigoB[1];
        lot = codigoB[2];
        existencia = codigoB[3];
        vlr_unitario = codigoB[4];
        f_venc = codigoB[5];
        medida = codigoB[6];


        asignaAtributo('lblIdArticulo', id_art + '-' + articulo, 0);
        asignaAtributo('lblCantidad', '1', 0);
        asignaAtributo('lblValorUnitario', vlr_unitario, 0);
        asignaAtributo('lblIva', '0', 0);
        asignaAtributo('lblValorImpuesto', vlr_unitario, 0);
        asignaAtributo('lblIdLote', lot, 0);
        asignaAtributo('lblFechaVencimiento', f_venc, 0);
        asignaAtributo('lblMedida', medida, 0);
        asignaAtributo('lblNaturaleza', 'S', 0);



        setTimeout("$('#txtIdCodigoBarrasVentas').focus();", 400);
        setTimeout("modificarCRUD('crearTransaccionVentas')", 300);

    }
}

function mostrarVentanaTipificacionRips(id_factura) {
    asignaAtributo("lblIdFactura");
    setTimeout(() => {
        mostrar('divVentanitaArchivosTipificacion');
        buscarFacturacion('listArchivosTificacion')
    }, 100);
}

function obtenerListaFacturasTabla(idTabla) {
    var lista_facturas = [];

    var ids = $("#" + idTabla).getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + idTabla + "_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#" + idTabla).getRowData(ids[i]);
            lista_facturas.push(datosRow.ID_FACTURA);
        }
    }
    return lista_facturas;
}

function obtenerListaSedes(idTabla) {
    var lista_sedes = [];
    var ids = $("#" + idTabla).getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + idTabla + "_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosSedeRow = $("#" + idTabla).getRowData(ids[i]);
            lista_sedes.push(datosSedeRow.id_sede);
        }
    }
    return lista_sedes;
}

function agregarSede(lista_sedes) {
    tam = lista_sedes.length;
    if (lista_sedes <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UNA SEDE");
    }
    else {
        varajaxInit = crearAjax();
        valores_a_mandar = "";
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.onreadystatechange = function () { respuestaAgregarSede() }
        valores_a_mandar = 'accion=agregarsede';
        valores_a_mandar = valores_a_mandar + "&idQuery=939&parametros=";
        add_valores_a_mandar(valorAtributo('txtIdentificacion'));
        add_valores_a_mandar(lista_sedes);
        varajaxInit.send(valores_a_mandar);
    }
}
function agregarSedeBodega(lista_sedes) {
    tam = lista_sedes.length;
    if (lista_sedes <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UNA SEDEAA");
    }
    else {
        varajaxInit = crearAjax();
        valores_a_mandar = "";
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.onreadystatechange = function () { respuestaAgregarSedeB() }
        valores_a_mandar = 'accion=agregarsedeBodega';
        valores_a_mandar = valores_a_mandar + "&idQuery=994&parametros=";
        add_valores_a_mandar(valorAtributo('txtIdentificacion'));
        add_valores_a_mandar(lista_sedesB);
        varajaxInit.send(valores_a_mandar);
    }
}

function obtenerListaEspe(idTabla) {
    var lista_plan = [];
    var ids = $("#" + idTabla).getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + idTabla + "_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosSedeRow = $("#" + idTabla).getRowData(ids[i]);
            lista_plan.push(datosSedeRow.NOMBRE);
        }
    }
    return lista_plan;
}

function agregarEspe(lista_plan) {
    tam = lista_plan.length
    var totalplan = ""
    if (lista_plan <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UN OBJETIVO ESPECIFICO");
    }
    else {

        for (var i = 0; i < tam; i++) {
            totalplan = totalplan + lista_plan[i]
        }
        asignaAtributo("txtEspe", totalplan, 0)
    }
}

function obtenerListaPlan(idTabla) {
    var lista_plan = [];
    var ids = $("#" + idTabla).getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + idTabla + "_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosSedeRow = $("#" + idTabla).getRowData(ids[i]);
            lista_plan.push(datosSedeRow.PLAN);
        }
    }
    return lista_plan;
}

function agregarPlanT(lista_plan) {
    tam = lista_plan.length
    var totalplan = ""
    if (lista_plan <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UN OBJETIVO ESPECIFICO");
    }
    else {

        for (var i = 0; i < tam; i++) {
            totalplan = totalplan + lista_plan[i]
        }
        asignaAtributo("txtPlanTr", totalplan, 0)
    }
}

function respuestaAgregarSede() {
    if (varajaxInit.readyState == 4) {
        buscarUsuario('sede');
        setTimeout(() => {
            buscarUsuario('sedes');
        }, 200);
        alert('Sedes Agregadas');
        limpiaAtributo('lblIdSede');
    }
}

function respuestaAgregarSedeB() {
    if (varajaxInit.readyState == 4) {
        buscarUsuario('sedeB');
        setTimeout(() => {
            buscarUsuario('sedesB');
        }, 200);
        alert('Sedes Agregadas');
        limpiaAtributo('lblIdSede');
    }
}

function obtenerListaFacturasTipificacionRips() {
    var lista_facturas = [];

    var ids = $("#listaFacturasCuenta").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listaFacturasCuenta_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#listaFacturasCuenta").getRowData(ids[i]);
            lista_facturas.push(datosRow.ID_FACTURA);
        }
    }

    var ids = $("#listaFacturasPendientes").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listaFacturasPendientes_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosRow = $("#listaFacturasPendientes").getRowData(ids[i]);
            lista_facturas.push(datosRow.ID_FACTURA);
        }
    }
    return lista_facturas
}

function cancelarTipificacion() {
    if (confirm("ESTA SEGURO DE TRUNCAR LOS PROCESOS ACTIVOS ?")) {
        $.ajax({
            url: "/clinica/paginas/accionesXml/guardarHc_xml.jsp",
            type: "POST",
            data: { "accion": "cancelarTipificacion" },
            beforeSend: function () { },
            success: function (data) {
                alert(data.getElementsByTagName("dato")[0].firstChild.data);
            },
            complete: function (jqXHR, String) {
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("HA OCURRIDO UN PROBLEMA AL EJECUTAR LA ACCION. POR FAVOR INTENTE NUEVAMENTE.")
            }
        });
    }
}

function tipificarFacturasPorBloqueEnEnvios() {
    if (verificarCamposGuardar("tipificarFacturasPorBloqueEnEnvios")) {
        var lista_envios = [];
        var ids = $("#listaCuentas").getDataIDs();

        for (var i = 0; i < ids.length; i++) {
            var c = "jqg_listaCuentas_" + ids[i];
            if ($("#" + c).is(":checked")) {
                datosRow = $("#listaCuentas").getRowData(ids[i]);
                lista_envios.push(datosRow.ID_ENVIO);
            }
        }

        valores_a_mandar = ""
        add_valores_a_mandar(lista_envios)
        add_valores_a_mandar(valorAtributo("cmbEstadoTipificacionFacturas"))
        add_valores_a_mandar(valorAtributo("cmbEstadoTipificacionFacturas"))
        add_valores_a_mandar(valorAtributo("cmbEstadoTipificacionFacturas"))

        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 1088,
                "parametros": valores_a_mandar,
                "_search": false,
                "nd": 1611873687449,
                "rows": -1,
                "page": 1,
                "sidx": "NO FACT",
                "sord": "asc"
            },
            beforeSend: function () {
                $("[id='btnTipificacion']").hide();
                $("[id='loaderTipificacion']").show();
            },
            success: function (data) {
                var respuesta = data.getElementsByTagName('cell');
                if (respuesta.length > 0) {
                    var lista_facturas = respuesta[1].firstChild.data;
                    if (lista_facturas != "") {
                        if (confirm("CANTIDAD DE FACTURAS A TIPIFICAR: " + lista_facturas.split(",").length)) {
                            tipificarFacturas(lista_facturas)
                        }
                    } else {
                        alert("NO SE HA ENCONTRADO NINGUNA FACTURA PARA TIPIFICAR")
                    }
                } else {
                    alert("NO HA SIDO POSIBLE OBTENER LOS ELEMENTOS PARA TIPIFICAR. POR FAVOR PONGASE EN CONTACTO CON SOPORTE.")
                }
                //tipificarFacturas(lista_facturas);
            },
            complete: function (xhr, status) {

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("NO HA SIDO POSIBLE OBTENER LA LISTA DE FACTURAS A TIPIFICAR. POR FAVOR INTENTE NUEVAMENTE O PONGASE EN CONTACTO CON SOPORTE.")
            }
        });
    }
}

function tipificarFacturaAdmision() {
    if (confirm("Esta seguro de tipificar factura ?")) {
        var admision = $("#listAdmisionCuenta").jqGrid('getGridParam', 'selrow');
        var id_factura = $("#listAdmisionCuenta").getRowData(admision).ID_FACTURA;
        tipificarFacturas([id_factura])
    }
}

function tipificacionPersonalizadaNefro() {
    $.ajax({
        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        type: "POST",
        data: {
            "idQuery": 3000,
            "_search": false,
            "parametros": "",
            "nd": 1611873687449,
            "rows": -1,
            "page": 1,
            "sidx": "NO FACT",
            "sord": "asc"
        },
        beforeSend: function () { },
        success: function (data) {
            var respuesta = data.getElementsByTagName('row')
            var lista_facturas = []

            for (i = 0; i < respuesta.length; i++) {
                lista_facturas.push(respuesta.item(i).getElementsByTagName('cell')[1].firstChild.data)
            }

            $.ajax({
                url: "/clinica/paginas/accionesXml/guardarHc_xml.jsp",
                type: "POST",
                data: {
                    "accion": "iniciarProcesoTipificacionPacientesNefro",
                    "lblIdFacturas": lista_facturas.toString()
                },
                success: function (data) {
                    alert("EL PROCESO DE TIPIFICACION DE PACIENTES CONTRIBUTIVOS DE NARIÑO HA TERMINADO")
                },
                complete: function (xhr, status) { },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("HA OCURRIDO UN ERROR AL REALIZAR LA ACCION. POR FAVOR COMUNICATE CON EL EQUIPO DE SOPORTE.")
                }
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("ERROR")
        }
    });
}

function iniciarProcesoTipificacionExterna(hilos) {
    var count_process = 0;
    for (let i = 0; i < hilos; i++) {
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 3000,
                "parametros": "_-" + i,
                "nd": 1611873687449,
                "rows": -1,
                "page": 1,
                "sidx": "NO FACT",
                "sord": "asc"
            },
            beforeSend: function () { },
            success: function (data) {
                var respuesta = data.getElementsByTagName('row')
                var lista_facturas = []

                for (let j = 0; j < respuesta.length; j++) {
                    lista_facturas.push(respuesta.item(j).getElementsByTagName('cell')[1].firstChild.data)
                }

                if (lista_facturas.length > 0) {
                    count_process += 1
                    asignaAtributo("lblCantidadProcesos", count_process)
                    $.ajax({
                        url: "/clinica/TipificacionExternaServlet",
                        type: "POST",
                        data: {
                            "lblIdFacturas": lista_facturas.toString(),
                            "grupo": i
                        },
                        beforeSend: function () { },
                        success: function (data) { },
                        complete: function (xhr, status) { },
                        error: function (XMLHttpRequest, textStatus, errorThrown) { }
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { }
        });
    }
    setTimeout(() => {
        alert("El proceso de tipificacion ya se ha iniciado")
    }, 1000);
}

function tipificarFacturas(lista_facturas) {
    if (lista_facturas <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UNA FACTURA");
        return;
    }

    if (!confirm("CANTIDAD DE FACTURAS A TIPIFICAR: " + lista_facturas.length)) {
        return;
    }

    $.ajax({
        url: "/clinica/paginas/accionesXml/guardarHc_xml.jsp",
        type: "POST",
        data: { "accion": "estaTipificando" },
        beforeSend: function () {
            $("[id='loaderTipificacion']").show();
        },
        success: function (data) {
            var respuesta = data.getElementsByTagName('dato');
            var accion = "";
            if (respuesta[0].firstChild.data == "NO") {
                accion = "iniciarProcesoTipificacion";
            } else {
                accion = "adicionarFaturasProcesoTipificacionActivo";
            }
            $.ajax({
                url: "/clinica/paginas/accionesXml/guardarHc_xml.jsp",
                type: "POST",
                data: {
                    "accion": accion,
                    "lblIdFacturas": lista_facturas.toString()
                },
                success: function (data) {
                    var respuesta = data.getElementsByTagName('dato');
                    alert(respuesta[0].firstChild.data);
                },
                complete: function (xhr, status) {
                    $.ajax({
                        url: "/clinica/paginas/accionesXml/guardarHc_xml.jsp",
                        type: "POST",
                        data: { "accion": "estaTipificando" },
                        success: function (data) {
                            var respuesta = data.getElementsByTagName('dato');
                            var accion = "";
                            if (respuesta[0].firstChild.data == "NO") {
                                $("[id='loaderTipificacion']").hide();
                            }
                        },
                        complete: function (xhr, status) {
                            switch (ventanaActual.opc) {
                                case 'rips':
                                    $('#listaFacturasCuenta').trigger('reloadGrid');
                                    setTimeout(() => {
                                        buscarFacturacion("listArchivosTificacion");
                                    }, 200);
                                    break;

                                case 'Facturas':
                                    buscarFacturacion('listGrillaFacturas')
                                    break;

                                case 'admisiones':
                                    buscarFacturacion("listArchivosTificacion");
                                    setTimeout(() => {
                                        buscarAGENDA("listAdmisionCuenta");
                                    }, 500);
                                    break;
                            }
                        }
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("[id='loaderTipificacion']").hide();
                    alert("HA OCURRIDO UN ERROR AL REALIZAR LA ACCION. POR FAVOR COMUNICATE CON EL EQUIPO DE SOPORTE.")
                }
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("HA OCURRIDO UN ERROR AL REALIZAR LA ACCION. POR FAVOR COMUNICATE CON EL EQUIPO DE SOPORTE.")
        }
    });
}

function tipificarFacturas_bk(lista_facturas, unidadProgreso, progreso) {
    if (lista_facturas <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UNA FACTURA");
    } else {
        $("[id='btnTipificacion']").hide();
        $("[id='loaderTipificacion']").show();

        if (unidadProgreso === undefined) {
            unidadProgreso = Math.ceil(100 / lista_facturas.length)
        }

        if (progreso === undefined) {
            progreso = 0
        }

        //id_factura = lista_facturas.shift();

        varajaxTipificacion = crearAjax();
        valores_a_mandar = "";
        varajaxTipificacion.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
        varajaxTipificacion.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxTipificacion.onreadystatechange = function () { respuestaTipificacion() }
        valores_a_mandar = "accion=tipificarFactura";
        valores_a_mandar = valores_a_mandar + "&lblIdFacturas=" + lista_facturas + "&idEnvio=" + valorAtributo("lblIdEnvioVentanita");
        varajaxTipificacion.send(valores_a_mandar);
    }
}

function respuestaTipificacion() {
    if (varajaxTipificacion.readyState == 4) {
        var data = varajaxTipificacion.responseXML.documentElement;

        alert(data.getElementsByTagName("dato")[0].firstChild.data);

        $("[id='btnTipificacion']").show();
        $("[id='loaderTipificacion']").hide();
        $("#progreso").text(0);


        switch (ventanaActual.opc) {
            case 'rips':
                jQuery('#listaFacturasCuenta').trigger('reloadGrid');
                break;

            case 'Facturas':
                buscarFacturacion('listGrillaFacturas')
                break;
        }
    }
}

function respuestaTipificarFacturas(lista_facturas, unidadProgreso, progreso) {
    console.log(varajaxInit.readyState)
    if (varajaxInit.readyState == 4) {

        progreso = progreso + unidadProgreso
        if (progreso > 100) {
            progreso = 100
        }

        $("#progreso").text(progreso);

        if (lista_facturas.length > 0) {
            setTimeout(() => {
                tipificarFacturas(lista_facturas, unidadProgreso, progreso)
            }, 500);
        } else {
            alert("PROCESO DE TIPIFICACION TERMINADO.")
            $("#btnTipificacion").show();
            $("#loaderTipificacion").hide();
            $("#progreso").text(0);

            var progreso;
            var unidadProgreso;

            switch (ventanaActual.opc) {
                case 'rips':
                    buscarFacturacion("listGrillaDetallesArchivoRips",
                        buscarFacturacion("listGrillaDetallesPendientesArchivoRips"))
                    break;

                case 'Facturas':
                    buscarFacturacion('listGrillaFacturas')
                    break;
            }
        }
    }
}

function tipificarFactura(id_factura, callback) {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=tipificarFactura";
    valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + id_factura;
    varajaxInit.send(valores_a_mandar);
}

function verificarEnvioFactura() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=auditarFactura";
    valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
    varajaxInit.send(valores_a_mandar);
}

function verificarEnvioRecibo() {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=auditarRecibo";
    valores_a_mandar = valores_a_mandar + "&lblIdRecibo=" + valorAtributo('lblIdRecibo');
    varajaxInit.send(valores_a_mandar);
}


function buscarInformacionBasicaPacienteParaAdmisiones(tipoServicio) {
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestabuscarInformacionBasicaPacienteParaAdmisiones(tipoServicio) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=InformacionBasicaDelPaciente";
    valores_a_mandar = valores_a_mandar + "&id=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
    varajaxInit.send(valores_a_mandar);
}

function respuestabuscarInformacionBasicaPacienteParaAdmisiones(tipoServicio) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('infoBasicaPaciente') != null) {
                totalRegistros = raiz.getElementsByTagName('identificacion').length;

                if (totalRegistros > 0) {
                    TipoIdPaciente = raiz.getElementsByTagName('TipoIdPaciente')[0].firstChild.data;
                    identificacion = raiz.getElementsByTagName('identificacion')[0].firstChild.data;
                    nombre1 = raiz.getElementsByTagName('Nombre1')[0].firstChild.data;
                    nombre2 = raiz.getElementsByTagName('Nombre2')[0].firstChild.data;
                    apellido1 = raiz.getElementsByTagName('Apellido1')[0].firstChild.data;
                    apellido2 = raiz.getElementsByTagName('Apellido2')[0].firstChild.data;
                    Nacionalidad = raiz.getElementsByTagName('nacionalidad')[0].firstChild.data;
                    idMunicipio = raiz.getElementsByTagName('idMunicipio')[0].firstChild.data;
                    nomMunicipio = raiz.getElementsByTagName('nomMunicipio')[0].firstChild.data;
                    Direccion = raiz.getElementsByTagName('Direccion')[0].firstChild.data;
                    Telefonos = raiz.getElementsByTagName('Telefonos')[0].firstChild.data;
                    Celular1 = raiz.getElementsByTagName('Celular1')[0].firstChild.data;
                    Celular2 = raiz.getElementsByTagName('Celular2')[0].firstChild.data;
                    email = raiz.getElementsByTagName('email')[0].firstChild.data;
                    Etnia = raiz.getElementsByTagName('Etnia')[0].firstChild.data;
                    NivelEscolar = raiz.getElementsByTagName('NivelEscolar')[0].firstChild.data;
                    idOcupacion = raiz.getElementsByTagName('idOcupacion')[0].firstChild.data;
                    Ocupacion = raiz.getElementsByTagName('Ocupacion')[0].firstChild.data;
                    Estrato = raiz.getElementsByTagName('Estrato')[0].firstChild.data;

                    fechaNac = raiz.getElementsByTagName('fechaNac')[0].firstChild.data;
                    edad = raiz.getElementsByTagName('edad')[0].firstChild.data;
                    sexo = raiz.getElementsByTagName('sexo')[0].firstChild.data;
                    acompanante = raiz.getElementsByTagName('acompanante')[0].firstChild.data;
                    administradora = raiz.getElementsByTagName('administradora')[0].firstChild.data;

                    asignaAtributo('cmbTipoId', TipoIdPaciente, 0)
                    asignaAtributo('txtIdentificacion', identificacion, 0)
                    asignaAtributo('txtNombre1', nombre1, 0)
                    asignaAtributo('txtNombre2', nombre2, 0)
                    asignaAtributo('txtApellido1', apellido1, 0)
                    asignaAtributo('txtApellido2', apellido2, 0)
                    asignaAtributo('txtNacionalidad', Nacionalidad, 0)
                    asignaAtributo('txtMunicipio', idMunicipio + '-' + nomMunicipio, 0)
                    asignaAtributo('txtDireccionRes', Direccion, 0)
                    asignaAtributo('txtTelefonos', Telefonos, 0)
                    asignaAtributo('txtCelular1', Celular1, 0)
                    asignaAtributo('txtCelular2', Celular2, 0)
                    asignaAtributo('txtEmail', email, 0)
                    asignaAtributo('cmbIdEtnia', Etnia, 0)
                    asignaAtributo('cmbIdNivelEscolaridad', NivelEscolar, 0)
                    asignaAtributo('txtIdOcupacion', idOcupacion + '-' + Ocupacion, 0)
                    asignaAtributo('cmbIdEstrato', Estrato, 0)

                    asignaAtributo('txtNomAcompanante', acompanante, 0);
                    asignaAtributo('txtFechaNac', fechaNac, 0)
                    asignaAtributo('cmbSexo', sexo, 0)
                    asignaAtributo('txtAdministradoraPaciente', administradora, 0)
                    asignaAtributo('lblEdad', edad, 0)
                    $("#txtEdadCompleta").html(calcularEdadCompleta(fechaNac));


                    //verificarNotificacionAntesDeGuardar('validarIdentificacionPaciente')
                    setTimeout(() => {
                        buscarAGENDA('listAdmisionCuenta')
                    }, 200);
                } else {
                    alert('PACIENTE NO EXISTE')
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}

function calcularValorTotalRecibo(idElemento) {
    var valor = parseInt(valorAtributo('txtValorUnidadRecibo'));
    var canti = parseInt(valorAtributo('cmbCantidadRecibo'));
    subTotal = valor * canti;
    subTotal = subTotal.toFixed(0)
    asignaAtributo('lblValorRecibo', subTotal, 0);
}

function cargarSitioLab() {
    $('#cmbIdSitioQuirurgicoLaboratorio > option[value="10"]').attr('selected', 'selected');
}

function cargarSitioTer() {
    $('#cmbIdSitioQuirurgicoTerapia > option[value="10"]').attr('selected', 'selected');
}

function cargarSitioADX() {
    switch (valorAtributo('cmbCantidadAD')) {
        case '2':
            $('#cmbIdSitioQuirurgicoAD > option[value="3"]').attr('selected', 'selected');
            break;
    }
}

function cargarCantidad() {

    switch (valorAtributo('cmbIdSitioQuirurgicoAD')) {
        case '3':
            $('#cmbCantidadAD > option[value="2"]').attr('selected', 'selected');
            break;
    }
}

function setIndicacion() {
    if (valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica') == '95140101') {
        asignaAtributo('txtIndicacionAD', 'OCT ', 0)
    }
}

function calcularValorExedenteCuenta() {
    var noCubi = parseInt(valorAtributo('lblValorNoCubierto'));
    var valMax = parseInt(valorAtributo('txtValorMaximoEvento'));
    if (noCubi > valMax)
        asignaAtributo('lblValorExedente', parseFloat(noCubi - valMax).toFixed(0), 0)
    else {
        alert('EL VALOR NO PUEDE SER MAYOR DEL VALOR NO CUBIERTO')
        asignaAtributo('txtValorMaximoEvento', '', 0)
        asignaAtributo('lblValorExedente', '', 0)
    }
}

function calculoDescuentoPorcentajeCuenta() {

    if (valorAtributo('cmbPorcentajeDescuentoCuenta') == '') {
        asignaAtributo('txtValorDescuentoCuenta', '', 0)
    } else {

        var tot = parseInt(valorAtributo('lblValorBaseDescuento'));
        var porc = parseInt(valorAtributo('cmbPorcentajeDescuentoCuenta'));
        asignaAtributo('txtValorDescuentoCuenta', parseFloat(tot * (porc / 100)).toFixed(2), 1)
    }
}


function calculoDescuentoPorcentajeVentas() {

    if (valorAtributo('cmbPorcentajeDescuentoVentanita') == '') {
        asignaAtributo('txtValorDescuentoVentanita', '', 1)
    } else {

        var tot = parseInt(valorAtributo('lblValorBaseVentanita'));
        var porc = parseInt(valorAtributo('cmbPorcentajeDescuentoVentanita'));
        asignaAtributo('txtValorDescuentoVentanita', parseFloat(tot * (porc / 100)).toFixed(2), 1)
    }
}

function calculoDescuentoPorcentajeVentasOtro() {

    if (valorAtributo('cmbPorcentajeDescuentoOtroVentanita') == '') {
        asignaAtributo('txtValorDescuentoOtroVentanita', '', 0)
    } else {

        var tot = parseInt(valorAtributo('lblValorBaseOtroVentanita'));
        var porc = parseInt(valorAtributo('cmbPorcentajeDescuentoOtroVentanita'));
        asignaAtributo('txtValorDescuentoOtroVentanita', parseFloat(tot * (porc / 100)).toFixed(2), 1)
    }
}


function consultarGrillasDiasPrincipalDespachoProgramacion() {
    buscarAGENDA('listDiasProgramacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
}

function selectConciliacionMedicamentosa() {
    if (valorAtributo('cmbExiste') == 'SI') {
        asignaAtributo('txtConciliacionDescripcion', '', 0)
    } else asignaAtributo('txtConciliacionDescripcion', 'LOS MEDICAMNETOS FORMULADOS, NO GENERAN NINGUNA INTERACCION MEDICAMENTOSA', 0)

}

function guardarContenidoDocumentoAYD() {

    if (IdSesion() == '1020401863' && valorAtributo('lblTipoDocumento') == 'ADAN') { /*DR ROJAS*/
        asignaAtributo('txtIdProfesionalElaboro', '1020401863')
        //		alert('2.txtIdProfesionalElaboroJJJJ='+  valorAtributo('txtIdProfesionalElaboro')   +' IdSesion= '+IdSesion() )		
    }
    if (IdSesion() == '13061897' && valorAtributo('lblTipoDocumento') == 'ADAN') { /*DR CARLOS*/
        asignaAtributo('txtIdProfesionalElaboro', '13061897')
        //		alert('2.txtIdProfesionalElaboroJJJJ='+  valorAtributo('txtIdProfesionalElaboro')   +' IdSesion= '+IdSesion() )		
    }
    if (IdSesion() == '13061897' && valorAtributo('lblTipoDocumento') == 'ADAG') {
        asignaAtributo('txtIdProfesionalElaboro', '13061897')
        //		alert('2.txtIdProfesionalElaboroJJJJ='+  valorAtributo('txtIdProfesionalElaboro')   +' IdSesion= '+IdSesion() )		
    }



    if (valorAtributo('txtIdProfesionalElaboro') == IdSesion()) {

        if (valorAtributo('txtIdEsdadoDocumento') == 1) {
            alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
            alert('EL ESTADO FINALIZADO URGENTE DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 7) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 9) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else guardarContenidoDocumento()

    } else alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA GUARDAR LOS CAMBIOS')

}

function copiarJustificacionAnexo() {
    //    document.getElementById("lbl_JUST_NOPOS").value=document.getElementById("txt_JUST").value;

    asignaAtributo('lbl_JUST_NOPOS', '', 0)
    asignaAtributo('lbl_JUST_NOPOS', $('#drag' + ventanaActual.num).find('#txt_JUST').val().trim(), 0)

}

function traerTextoPredefinidoBiometria() {
    asignaAtributo('txt_gestionarPaciente', '\nOJO DERECHO\n1.Constante=\n2.Observaciones=  \n\nOJO IZQUIERDO\n1.Constante=\n2.Observaciones=\n-', 0)
}

function traerTextoPredefinidoAngio(idTextArea) {

    switch (idTextArea) {
        case '10':
            asignaAtributo('txt_ADAN_C10', 'DISCO ROSADO, BORDES DEFINIDOS EXCAVACION 2/10  VASOS DE EMERGENCIA CENTRAL DE FORMA Y CONFIGURACION NORMAL RETINA APLICADA, MACULA NORMAL.', 0)
            break;
        case '11':
            asignaAtributo('txt_ADAN_C11', 'CONFIRMA LOS HALLAZGOS.', 0)
            break;
        case '12':
            asignaAtributo('txt_ADAN_C12', 'TIEMPO DE LLENADO VENOSO Y COROIDEO EN LIMITES NORMALES.  DESDE ETAPAS ARTERIOVENOSAS INICIALES SE APRECIA FOCOS PUNTIFORMES DE HIPERFLUORESCENCIA PERIMACULAR QUE NO AUMENTAN NI FUGAN EN ETAPAS TARDIAS SECUNDARIO A ATROFIA DEL EPR PERIMACULAR', 0)
            break;
        case '13':
            asignaAtributo('txt_ADAN_C13', '', 0)
            break;
        case '14':
            asignaAtributo('txt_ADAN_C14', '', 0)
            break;
        case '15':
            asignaAtributo('txt_ADAN_C15', '', 0)
            break;
        case '16':
            asignaAtributo('txt_ADAN_C16', 'SE SIGIERE CORRACIONAR CON LA CLINICA', 0)
            break;
    }
}

function getAbsoluteElementPositionX(element) {

    var elemento = document.getElementById(element)

    if (typeof element == "string")
        element = document.getElementById(element)

    if (!element) return { top: 0, left: 0 };

    var y = 0;
    var x = 0;
    while (element.offsetParent) {
        x += element.offsetLeft;
        y += element.offsetTop;
        element = element.offsetParent;
    }
    return { left: x };
}

function getAbsoluteElementPositionY(element) {

    var elemento = document.getElementById(element)

    if (typeof element == "string")
        element = document.getElementById(element)

    if (!element) return { top: 0, left: 0 };

    var y = 0;
    var x = 0;
    while (element.offsetParent) {
        x += element.offsetLeft;
        y += element.offsetTop;
        element = element.offsetParent;
    }
    return { top: y };
}

function traerVentanitaDx(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaDx.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaHos(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {

    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hospitalizacion/ventanita.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaHos(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}


function llenartraerVentanitaHos(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function llenartraerVentanitaFuncionesHCPlantilla(divPaVentanita, id, name, value) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            guardarDatosPlantilla(id, name, value);
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}



function llenartraerVentanitaFuncionesHC(divPaVentanita, accion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            asignaAtributo("txtAccionVentintaHC", accion, 0)
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function traerVentanitaUrgencias(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/facturacion/admisiones/ventanita.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaFuncionesHcPlantilla(idLupitaVentanita, accion, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFuncionesHCPlantilla.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaFuncionesHC(divPaVentanita, accion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaBuscarHC(idLupitaVentanita, divPaVentanita, elemInputDes, arg) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaBuscarHC.jsp?arg=' + arg, true);

    varajaxMenu.onreadystatechange = function () { llenarVentanitaBuscarHC(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarVentanitaBuscarHC(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function traerVentanitaFuncionesHc(idLupitaVentanita, accion, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFuncionesHC.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaFuncionesHC(divPaVentanita, accion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

/* paredes */

function traerVentanitaPT(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaPT.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

///7

function traerVentanitaProcedimientosListaEspera(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaProcedimientoListaEspera.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaProcedimientosListaEspera(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}




function llenartraerVentanitaProcedimientosListaEspera(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;

            mostrar(divPaVentanita);
            mostrar('divVentanitaProcedimientoListaEspera');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}


///

/* fn */

function traerVentanitaPT(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaPT.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanita(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanita.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}


function traerVentanitaPaciente(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaPaciente.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaPaciente(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaPerfiles(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#cmbEstado').focus();
            limpiarListadosTotales('listGrillaOpcionRoles');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}


function traerVentanitaPerfiles(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaPerfiles.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaPerfiles(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}


function traerVentanitaFirmaCliente(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {

    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFirmaCliente.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaHos(divPaVentanita, accion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            asignaAtributo("txtAccionVentintaHC", accion, 0)
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function traerVentanitaHistoricosListaEspera(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaHisticosListaEspera.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaHistoricosListaEspera(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}



function llenartraerVentanitaHistoricosListaEspera(divPaVentanita) {

    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;

            mostrar(divPaVentanita);
            mostrar('divVentanitaHistoricosListaEspera');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}


function llenartraerVentanita(divPaVentanita) {

    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;

            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function llenartraerVentanitaPaciente(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');
            asignaAtributo("lblVentanita", divPaVentanita, 0)

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtIdBus').focus();
            limpiarListadosTotales('listGrillaPaciente');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function traerVentanitaCondicion1Autocompletar(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top - 200;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaCondicion.jsp?condicion1=' + valorAtributoIdAutoCompletar(filtro), true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaCondicion1(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaCondicion1(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaCondicion.jsp?condicion1=' + valorAtributo(filtro), true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaCondicion1(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function traerVentanitaProcedimientos(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    $.ajax({
        url: '/clinica/paginas/hc/ventanitaProcedimientos.jsp',
        data: {
            'condicion1': valorAtributo(filtro)
        },
        contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
        beforeSend: function () {
            //mostrarLoader(urlParams.get("accion"))
        },
        success: function (data) {
            document.getElementById(divPaVentanita).innerHTML = data;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuestaProcedimientos');

            var d = document.getElementById('ventanitaHijaProcedimientos');
            d.style.position = "fixed";
            //alert(d.style.top)
            //d.style.position = "absolute";
            //d.style.top = topY + 'px';

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        },
        complete: function (jqXHR, textStatus) {
            //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('PROBLEMAS CON LA VENTANA DE PROCEDIMIENTOS, POR FAVOR CERRARLA Y VOLVER A ABRIR');
        }
    });
}

function traerVentanitaEmpresa(idLupitaVentanita, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaCondicionEmpresa.jsp?condicion1=' + IdEmpresa(), true);
    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaCondicion1(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaCondicion1(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuestaCondicion1');

            var d = document.getElementById('ventanitaHijaCondicion1');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

var idArticulo;

function traerVentanitaArticulo(elemInputDes, query) {
    if (valorAtributo('cmbIdBodega') != '6') {
        idQuery = query;
        elemInputDestino = elemInputDes;
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaArticulo.jsp', true);
        varajaxMenu.onreadystatechange = llenartraerVentanitaArticulo;
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
    } else alert('SOLO SE PERMITE A BODEGAS DIFERENTES DE OPTICA')
}

function llenartraerVentanitaArticulo() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaVentanitaArticulo").innerHTML = varajaxMenu.responseText;
            asignaAtributo('txtCodBus', idArticulo, 0);
            mostrar('divParaVentanitaArticulo');
            mostrar('divVentanitaPuestaArticulo');
            if (valorAtributo('txtCodBus') != '') { setTimeout("buscarHistoria('listGrillaVentanaArticulo')", 400); }
            // document.getElementById("txtNomBus").onfocus
            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function abrirVentanaAnexo1() {
    if ((valorAtributoIdAutoCompletar("txtIdBusPaciente") != '' || valorAtributo("txtIdPac") != '') && valorAtributo('txtIdentificacion') != '' && valorAtributo('txtNombre1') != '') {
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        varajaxMenu.open("POST", '/clinica/paginas/facturacion/admisiones/ventanaAnexo1.jsp', true);
        varajaxMenu.onreadystatechange = function () { llenarAbrirVentanaAnexo1() };
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
    }
    else {
        alert("SELECCIONE UN PACIENTE");
        return;
    }
}

function llenarAbrirVentanaAnexo1() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById('divParaVentanita').innerHTML = varajaxMenu.responseText;
            mostrar('divParaVentanita');
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";
            if (valorAtributo("txtIdBusPaciente") != '') {
                asignaAtributo('lblIdP', valorAtributoIdAutoCompletar("txtIdBusPaciente"), 0)
            }
            else {
                asignaAtributo('lblIdP', valorAtributo("txtIdPac"), 0)
            }
            asignaAtributo('lblTipoId', valorAtributo('cmbTipoId'), 0)
            asignaAtributo('lblNumeroId', valorAtributo('txtIdentificacion'), 0)
            asignaAtributo('lblPrimerNom', valorAtributo('txtNombre1'), 0)
            asignaAtributo('lblSegundoNom', valorAtributo('txtNombre2'), 0)
            asignaAtributo('lblPrimerApe', valorAtributo('txtApellido1'), 0)
            asignaAtributo('lblSegundoApe', valorAtributo('txtApellido2'), 0)
            asignaAtributo('lblFecha', valorAtributo('txtFechaNac'), 0)
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function abrirVistaPreviaEvolucion(idEvolucion) {
    alert(idEvolucion)
}

function agregarTablaHistoricosAtencion(idReporte, parametro1, cantColumnasQuery, tablaReportesDestino) {
    mostrar('divDocumentosHistoricos')
    cantColumnasP = cantColumnasQuery;
    idTablaReportesExcel = tablaReportesDestino;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarReportes_xml.jsp', true);
    varajaxInit.onreadystatechange = respuestallenarTablaHistoricosAtencion;

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=listadoReporteExcel";
    valores_a_mandar = valores_a_mandar + "&id=" + idReporte;
    valores_a_mandar = valores_a_mandar + "&parametro1=" + parametro1; //+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
    varajaxInit.send(valores_a_mandar);
}

function respuestallenarTablaHistoricosAtencion() {
    if (varajaxInit.readyState == 4) {
        //   VentanaModal.cerrar();
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('rows') != null) {

                totalRegistros = raiz.getElementsByTagName('c1').length;

                if (totalRegistros) {
                    agregarDatosTablaHistoricosAtencion(raiz, totalRegistros);
                } else {
                    alert("Sin datos en el reporte, cierre y vuelva a generarlo");
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //	abrirVentana(260, 100);
        //  VentanaModal.setSombra(true);
    }
}

function agregarTablaDeVistaPrevia() {

    borrarRegistrosTabla('listDocumentosHistoricos');
    var cantFilas = 0,
        estilo = 'inputBlanco';

    tablabody = document.getElementById('listDocumentosHistoricos').lastChild;
    Nregistro = document.createElement("TR");
    TD = document.createElement("TD");
    Ncampo1 = document.createElement("IMG");
    Ncampo1.setAttribute('src', '/clinica/utilidades/imagenes/acciones/abajoFlecha.png');
    Ncampo1.setAttribute('TITLE', 'VER VISTA PREVIA');
    Ncampo1.setAttribute('width', '16');
    Ncampo1.setAttribute('height', '16');
    Ncampo1.setAttribute('align', 'left');
    TD.appendChild(Ncampo1);
    Nregistro.appendChild(TD);
    tablabody.appendChild(Nregistro);


    var ids = jQuery("#listDocumentosHistoricos").getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = ids[i];
        var datosRow = jQuery("#listDocumentosHistoricos").getRowData(c);


        Nregistro = document.createElement("TR");
        Nregistro.id = datosRow.id_evolucion;
        Nregistro.className = estilo;

        TD = document.createElement("TD");
        Ncampo1 = document.createElement("IMG");
        Ncampo1.setAttribute('src', '/clinica/utilidades/imagenes/acciones/buscar.png');
        Ncampo1.setAttribute('width', '16');
        Ncampo1.setAttribute('TITLE', datosRow.id_evolucion);
        Ncampo1.setAttribute('height', '16');
        Ncampo1.setAttribute('align', 'left');
        Ncampo1.setAttribute('onclick', "abrirVistaPreviaEvolucion(" + datosRow.id_evolucion + ");");
        TD.appendChild(Ncampo1);
        Nregistro.appendChild(TD);
        tablabody.appendChild(Nregistro);
    }

}



/* Para la impresion de los documentos con el array */
var ordenImpresion = new Array();
var POSICION_VECTOR = 0;
var POSICION_VECTOR_MAX = 0;
/*la impresion empieza cn esta funcion que es llamada en la linea 1205*/
function llenarArregloImpresion() {

    POSICION_VECTOR = 0;
    if (bandFormOftal == 'SI') {
        POSICION_VECTOR_MAX = 6;
    } else POSICION_VECTOR_MAX = 14;


    ordenImpresion[1] = 1;
    ordenImpresion[2] = 2;
    ordenImpresion[3] = 3;
    ordenImpresion[4] = 4;


    if (valorAtributo('txtIdDocumentoVistaPrevia') == '')
        TIPO_DOCUMENTO = valorAtributo('lblTipoDocumento')
    else
        TIPO_DOCUMENTO = tipo_evolucion

    //alert('TIPO_DOCUMENTO= '+TIPO_DOCUMENTO)

    //TIPO_DOCUMENTO = valorAtributo('lblTipoDocumento') 

    if (TIPO_DOCUMENTO == 'HQUI' || TIPO_DOCUMENTO == 'HQLA' || TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF' || TIPO_DOCUMENTO == 'RANA' || TIPO_DOCUMENTO == 'RPRE' || TIPO_DOCUMENTO == 'EPIC') {
        ordenImpresion[5] = 5;

    } else ordenImpresion[5] = 55; /* validar el query de hqui y hqla cuando hay dos hc*/

    ordenImpresion[6] = 6;

    if (TIPO_DOCUMENTO == 'HQUI' || TIPO_DOCUMENTO == 'HQLA' || TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF' || TIPO_DOCUMENTO == 'RANA' || TIPO_DOCUMENTO == 'RPRE' || TIPO_DOCUMENTO == 'EPIC' || TIPO_DOCUMENTO == 'EVME')
        ordenImpresion[7] = 111;
    else ordenImpresion[7] = 7;
    ordenImpresion[8] = 8;

    if (TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF') {
        ordenImpresion[9] = 111;

    } else ordenImpresion[9] = 9;


    if (TIPO_DOCUMENTO == 'LICH' || TIPO_DOCUMENTO == 'NENF') {
        ordenImpresion[10] = 111;
    } else ordenImpresion[10] = 10;

    ordenImpresion[11] = 11;
    ordenImpresion[12] = 12;
    ordenImpresion[13] = 13;

    ocuparCandadoPaReporte(); /*inserta en candado y llama a  recorrerImpresion()*/
    //recorrerImpresion();	
}

function recorrerImpresion() {

    POSICION_VECTOR++; //alert(POSICION_VECTOR)
    if (POSICION_VECTOR < POSICION_VECTOR_MAX) {
        if (bandFormOftal == 'SI') {
            // alert('impr anexo, ban: '+bandFormOftal);
            imprimirSincronicoPdfAnexoOptalmica(ordenImpresion[POSICION_VECTOR]);
        } else imprimirSincronicoPdf(ordenImpresion[POSICION_VECTOR]);
    } else {
        LiberarCandadoPaReporte();
        POSICION_VECTOR = 0;
        POSICION_VECTOR_MAX = 0;
        asignaAtributo('txtIdDocumentoVistaPrevia', '')
        asignaAtributo('lblIdAdmisionVistaPrevia', '')


        if (totalRegistrosTabla('listSistemasPDF') == 1)
            ocultar('idDivRevisionSistemas')
        if (totalRegistrosTabla('listDiagnosticosPDF') == 1)
            ocultar('idDivDiagnosticos')

        if (totalRegistrosTabla('listPlanPDF') == 1)
            ocultar('idDivPlan')
        if (totalRegistrosTabla('listMedicamentosPDF') == 1)
            ocultar('idDivMedicamentos')

    }
}

/**/


function cargarDxHistoricos() {
    if (valorAtributo('lblIdDocumento') != '') {
        mostrar('divVentanitaDxHistorico')
        buscarHC('listDiagnosticosHistorico', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
    } else alert('SELECCIONE DOCUMENTO CLINICO')
}

function cargarDxFrecuentes() {
        mostrar('divVentanitaDxFrecuente')
        buscarHC('listDiagnosticosFrecuente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
}

function cargarProcedimientosHistoricos() {
    if (valorAtributo('lblIdDocumento') != '') {
        mostrar('divVentanitaProcedimCuentaientosHistoricos')
        buscarHC('listProcedimientosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
    } else alert('SELECCIONE DOCUMENTO CLINICO')

}

function cargarRefrescaDocClinico() { // para  enviar archivo  UPC
    if (valorAtributo('lblIdDocumento') != '') {
        mostrar('divContenidos')
        llenarTablaAlAcordion(valorAtributo('lblIdDocumento'), valorAtributo('lblTipoDocumento'), valorAtributo('lblDescripcionTipoDocumento'))
    } else alert('SELECCIONE DOCUMENTO CLINICO')

}

function ventanaVerificacionAdmision() {
    limpiaAtributo('cmbTipoIdVerifica', 0)
    limpiaAtributo('txtIdentificacionVerifica', 0)
    mostrar('divVentanitaVerificacion')
}

function ventanaVerificacionAdmisionFactura() { 
    if (verificarCamposGuardar('crearAdmisionFactura')) {
        numero_cita_seleccionada = $("#listAgendaAdmision").jqGrid('getGridParam', 'selrow')

        limpiaAtributo('cmbTipoIdVerifica', 0)
        limpiaAtributo('txtIdentificacionVerifica', 0)

        /*asignaAtributo('cmbTipoIdVerifica', $("#listAgendaAdmision").getRowData(numero_cita_seleccionada).TIPO_ID, 0)
        asignaAtributo('txtIdentificacionVerifica', $("#listAgendaAdmision").getRowData(numero_cita_seleccionada).IDENTIFICACION, 0)*/

        asignaAtributo('cmbTipoIdVerifica', valorAtributo("txtIdBusPaciente").split("-")[1], 0)
        asignaAtributo('txtIdentificacionVerifica', valorAtributo("txtIdBusPaciente").split("-")[2], 0)

        verificarNotificacionAntesDeGuardar('validarTipoCitaFactura')
    }
}

function verificacionCrearAdmision() {

    if (valorAtributo('cmbTipoId') == valorAtributo('cmbTipoIdVerifica')) {
        if (valorAtributo('txtIdentificacion') == valorAtributo('txtIdentificacionVerifica')) {

            var lista = jQuery("#listCitaCirugiaProcedimiento").getDataIDs().length;

            if (lista == 0) {
                verificarNotificacionAntesDeGuardar('crearAdmision')
            } else {
                verificarNotificacionAntesDeGuardar('crearAdmisionCirugia')
            }
            ocultar('divVentanitaVerificacion')
        } else {
            alert('RECTIFIQUE NUMERO IDENTIFICACION')
            limpiaAtributo('txtIdentificacion', 0)
            ponerFoco('txtIdentificacion', 0)
            ocultar('divVentanitaVerificacion')
        }
    } else {
        alert('RECTIFIQUE TIPO DE DOCUMENTO')
        limpiaAtributo('cmbTipoId', 0)
        ponerFoco('cmbTipoId', 0)
        ocultar('divVentanitaVerificacion')
    }

}

function crearAdmisionSinFactura() {

    if (valorAtributo('cmbTipoId') === valorAtributo('cmbTipoIdVerifica') &&
        valorAtributo('txtIdentificacion') === valorAtributo('txtIdentificacionVerifica')) {
        //verificarNotificacionAntesDeGuardar('crearAdmisionSinFactura');    
        modificarCRUD('crearAdmisionSinFactura');
    } else {
        alert('RECTIFIQUE NUMERO DE IDENTIFICACION')
        ponerFoco('txtIdentificacion', 0)
    }

    ocultar('divVentanitaVerificacion')
}

function crearAdmisionConFactura() {
    if (valorAtributo('cmbTipoId') === valorAtributo('cmbTipoIdVerifica') &&
        valorAtributo('txtIdentificacion') === valorAtributo('txtIdentificacionVerifica')) {
        verificarNotificacionAntesDeGuardar('crearAdmisionFactura');
    } else {
        alert('RECTIFIQUE NUMERO DE IDENTIFICACION')
        ponerFoco('txtIdentificacion', 0)
    }
    ocultar('divVentanitaVerificacion')
}

function limpiarArchivoAdjunto() { // para  eliminar archivo  UPC
    if (confirm('Seguro de LIMPIAR :: ' + valorAtributo('cmbTipoArchivoSubir') + ' de la base de datos ?')) {
        switch (tabActivo) {
            case 'divArchivosAdjuntos':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=limpia' + '&subCarpeta=fisico';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divArchivosAdjuntosEvento':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=limpia' + '&subCarpeta=fisico';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;


            case 'divListadoIdentificacion':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=limpia' + '&subCarpeta=identificacion';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divListadoRemision':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=limpia' + '&subCarpeta=remision';
                    document.forms['formUpcElimina'].submit();
                } else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divListadoCarnet':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=limpia' + '&subCarpeta=carnet';
                    document.forms['formUpcElimina'].submit();
                } else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
            case 'divListadoAyudasDiagnosticas':
                if (valorAtributo('txtIdEsdadoDocumento') == 1) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.1')
                } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.8')
                } else {
                    if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                        //if(valorAtributo('txtIdProfesionalElaboro')==IdSesion()){ 
                        document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=limpia' + '&subCarpeta=ayudaDx';
                        document.forms['formUpcElimina'].submit();

                    } else alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA LIMPIAR')

                }
                break;
            case 'divListadoOtrosDocumentos':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=limpia' + '&subCarpeta=otros';
                    document.forms['formUpcElimina'].submit();
                } else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA LIMPIAR');
                break;
        }
    }
}

function formatearFecha(fecha) {
    var f;
    if (fecha != '') {
        f = fecha.split('-');
        return f[2] + '/' + f[1] + '/' + f[0];
    } else {
        return '';
    }
}

function desformatearFecha(fecha) {
    var f;
    if (fecha != '') {
        f = fecha.split('/');
        return f[2] + '-' + f[1] + '-' + f[0];
    } else {
        return '';
    }
}

function deDDMMYYYY_a_MMDDYYYY(fecha) {
    var f;
    if (fecha != '') {
        f = fecha.split('/');
        return f[1] + '/' + f[0] + '/' + f[2];
    } else {
        return '';
    }
}

function calcula_utilizado(id) {
    alert(id)
}

function calculoIMC() {
    if (valorAtributo('txtPeso') != '' && valorAtributo('txtTalla')) {
        var peso = parseInt(valorAtributo('txtPeso'));
        var talla = parseInt(valorAtributo('txtTalla'));
        asignaAtributo('lblIMC', parseFloat(peso / ((talla * 0.01) * (talla * 0.01))).toFixed(2), 0)
    } else asignaAtributo('lblIMC', '', 0)
}


function llenarTablaAlAcordion_(idEvolucion, tipoEvolucion, nombreEvolucion) {
    //buscarHC('listAntFarmacologicos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
    ruta = '/clinica/paginas/hc/documentosHistoriaClinica/'
    pagina = tipoEvolucion + '.jsp';
    idDivTitulo = 'divContenidos';
    $('#drag' + ventanaActual.num).find('#' + idDivTitulo + idDivTitulo).html($.trim(nombreEvolucion)); /*para el titulo del acordion*/
    cargarMenuTable(ruta + pagina, tipoEvolucion, idEvolucion);
}

function cargarMenuTable(pagina, tipoEvolucion, idEvolucion) {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", pagina, true);
    varajaxMenu.onreadystatechange = function () { llenarinfoPagina(tipoEvolucion, idEvolucion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);

}

function llenarinfoPagina(tipoEvolucion, idEvolucion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {

            document.getElementById("divParaPagina").innerHTML = varajaxMenu.responseText;

            traerContenidoDocumentoSeleccionado(tipoEvolucion, idEvolucion, 'txt_')


            if (tipoEvolucion == 'ADAN') {
                calendario('txt_ADAN_C18', 0);
            }

            if (tipoEvolucion == 'ECOO') {
                calendario('txt_ECOO_C1', 0);
                calendario('txt_ECOO_C2', 0);
            }

            if (tipoEvolucion == 'PSIE') {
                calendario('txt_PSIE_C2', 0);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

/**************************************************************/

function cargarFormulas(tipoEvolucion, idEvolucion) {
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp";
    valores_a_mandar = valores_a_mandar + "?idQuery=1801&parametros=";
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'));
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'));
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'));
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
    $("#drag" + ventanaActual.num)
        .find("#listGrillaFormulas")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CAMPO", "VALOR", "FORMULAS"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 15) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 40) },
                { name: "FORMULAS", index: "FORMULAS", width: anchoP(ancho, 40) }
            ],
            height: 250,
            width: ancho + 40,
            loadComplete: function (data) {
                cargarValidaciones(tipoEvolucion, idEvolucion);
            },
            loadBeforeSend: function (xhr, settings) {
                loaderFormulasFolio = true;
                $("#loaderPlantilla").show()
                $("#divParaTabsContenidoFolioPlantilla").hide();
            }
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaFormulas")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

function cargarFormulasEncuesta(idEncuesta, idEncuestaPaciente) {
    //pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=1805&parametros=";
    add_valores_a_mandar(idEncuesta);
    add_valores_a_mandar(idEncuesta);
    add_valores_a_mandar(idEncuestaPaciente);
    $("#drag" + ventanaActual.num)
        .find("#listGrillaFormulasE")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CAMPO", "VALOR", "FORMULAS"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 15) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 40) },
                { name: "FORMULAS", index: "FORMULAS", width: anchoP(ancho, 40) }
            ],

            //  pager: jQuery('#pagerGrilla'),
            height: 250,
            width: ancho + 40,
            gridComplete: function (data) {
                cargarValidacionesEncuesta(idEncuesta, idEncuestaPaciente);
            },

            /*onSelectRow: function (rowid) {
                var datosRow = jQuery("#drag" + ventanaActual.num)
                    .find("#listGrillaFolios")
                    .getRowData(rowid);
            },*/
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaFormulasE")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

/**************************************************************/

function cargarValidaciones(tipoEvolucion, idEvolucion) {
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp";
    valores_a_mandar = valores_a_mandar + "?idQuery=1802&parametros=";
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'));
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValidaciones")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CODIGO", "CAMPO", "OPERADOR", "VALOR", "CAMPO_COMPARAR", "TIPO"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CODIGO", index: "CODIGO", width: anchoP(ancho, 15) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 15) },
                { name: "OPERADOR", index: "OPERADOR", width: anchoP(ancho, 15) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 15) },
                { name: "CAMPO_COMPARAR", index: "CAMPO_COMPARAR", width: anchoP(ancho, 15) },
                { name: "TIPO", index: "TIPO", width: anchoP(ancho, 15) }
            ],
            height: 250,
            width: ancho + 40,
            loadComplete: function (data) {
                cargarValoresValidaciones(tipoEvolucion, idEvolucion);
            },
            gridview: true,
            rowattr: function (rd) {
                return { "class": rd.CODIGO }
            }
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValidaciones")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

function cargarValidacionesEncuesta(idEncuesta, idEncuestaPaciente) {
    //pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=1806&parametros=";
    add_valores_a_mandar(idEncuesta);
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValidacionesE")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CODIGO", "CAMPO", "OPERADOR", "VALOR", "CAMPO_COMPARAR", "TIPO"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CODIGO", index: "CODIGO", width: anchoP(ancho, 15) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 15) },
                { name: "OPERADOR", index: "OPERADOR", width: anchoP(ancho, 15) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 15) },
                { name: "CAMPO_COMPARAR", index: "CAMPO_COMPARAR", width: anchoP(ancho, 15) },
                { name: "TIPO", index: "TIPO", width: anchoP(ancho, 15) }
            ],
            height: 250,
            width: ancho + 40,
            gridview: true,
            rowattr: function (rd) {
                /*if (rd.GroupHeader === "1") { // verify that the testing is correct in your case
                    return {"class": "myAltRowClass"};
                }*/
                return { "class": rd.CODIGO }
            },

            gridComplete: function () {
                cargarValoresValidacionesEncuesta(idEncuesta, idEncuestaPaciente);
            }
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValidacionesE")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

/**************************************************************/

function cargarValoresValidaciones(tipoEvolucion, idEvolucion) {
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp";
    valores_a_mandar = valores_a_mandar + "?idQuery=1804&parametros=";
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'));
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'));
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValoresValidaciones")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CAMPO", "VALOR"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 25) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 70) }
            ],
            loadComplete: function () {
                $("#loaderPlantilla").hide()
                $("#divParaTabsContenidoFolioPlantilla").show();

                loaderFormulasFolio = false
            },
            height: 250,
            width: ancho + 40,
            gridview: true
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValoresValidaciones")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

/*function cargarFormulasValidaciones(tipoEvolucion, idEvolucion) {
    return new Promise((resolve, reject) => {
        try {
            cargarFormulas(tipoEvolucion, idEvolucion)
            resolve();
        } catch (error) {
            console.log(error.message);
            reject();
        }
    })
}*/

function cargarValoresValidacionesEncuesta(idEncuesta, idEncuestaPaciente) {
    //pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 50;
    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=1807&parametros=";
    add_valores_a_mandar(idEncuesta);
    add_valores_a_mandar(idEncuestaPaciente);
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValoresValidacionesE")
        .jqGrid({
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",

            colNames: [
                "Cont", "CAMPO", "VALOR"
            ],
            colModel: [
                { name: "contador", index: "contador", width: anchoP(ancho, 5) },
                { name: "CAMPO", index: "CAMPO", width: anchoP(ancho, 25) },
                { name: "VALOR", index: "VALOR", width: anchoP(ancho, 70) }
            ],
            height: 250,
            width: ancho + 40,
            gridview: true
        });
    $("#drag" + ventanaActual.num)
        .find("#listGrillaValoresValidacionesE")
        .setGridParam({ url: valores_a_mandar })
        .trigger("reloadGrid");
}

/**************************************************************/

function tabActivoDireccionamiento(idTab) {
    switch (idTab) {
        case "divListaEspera":
            calendario('txtFechaCita', 0);
            buscarHC("listaEsperaProgramacion", "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp")
            setTimeout(() => {
                buscarHC("citaProgramacion", "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp")
            }, 200);
            break;
    }
}

function TabActivoConductaTratamiento(idTab) {
    tabActivo = idTab;
    switch (idTab) {
        case "divLogTipificacion":
            setTimeout(function () {
                valores_a_mandar = ""
                add_valores_a_mandar(valorAtributo("lblIdFacturaAdjuntos"))
                $.ajax({
                    url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                    type: "POST",
                    data: {
                        "idQuery": 1099,
                        "parametros": valores_a_mandar,
                        "_search": false,
                        "nd": 1611873687449,
                        "rows": -1,
                        "page": 1,
                        "sidx": "NO FACT",
                        "sord": "asc"
                    },
                    beforeSend: function () { },
                    success: function (data) {
                        var respuesta = data.getElementsByTagName('cell');
                        $("#textoLogTipificacion").text(respuesta[1].firstChild.data);
                    },
                });
            }, 200)
            break;

        case "listaFacturaDetalleEvolucion":
            buscarFacturacion("listaFacturaDetalleEvolucion")
            break;

        case 'divProgramacionFactura':
            buscarFacturacion("listaPlanEvolucionFactura")
            break;

        case 'divAdministracionPaciente':
            buscarHC('listOrdenesPacienteURG', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            calendario("txtFechaMuerte", 1)
            setTimeout(() => {
                cargarComboGRALCondicion1('', '', 'cmbOrdenenesAdmisionURG', 160, valorAtributo('lblIdAdmision'))
            }, 200);
            break;

        case 'divConsentimientos':
            if (valorAtributo('txtIdEsdadoDocumento') == 0 || valorAtributo('txtIdEsdadoDocumento') == 12) {
                document.getElementById('btnCrearConsentimiento').disabled = false;
            } else {
                document.getElementById('btnCrearConsentimiento').disabled = true;
            }
            buscarHC('listConsentimientosPaciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'divProcedimiento':
            buscarHC('listaProcedimientoConductaTratamiento', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoP','lblIdDocumento')", 500);
            break;

        // case 'divIncapacidad':
        //     buscarHC('listProcedimientosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
        //     setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoP','lblIdDocumento')", 500);
        //     break;
        case 'divAyudasDiagnosticas':
            buscarHC('listAyudasDiagnosticasDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoA','lblIdDocumento')", 500);
            break;

        case 'divConciliacionMedicamento':
            guardarYtraerDatoAlListado('buscarConciliacion')
            break;

        case 'divMedicacion':
            buscarHC('listMedicacion', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;

        case 'divLaboratorioClinico':
            buscarHC('listLaboratoriosDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoL','lblIdDocumento')", 500);
            break;

        case 'divTerapias':
            buscarHC('listTerapiaDetalle', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            setTimeout("cargarDiagnosticoRelacionado('cmbIdDxRelacionadoT','lblIdDocumento')", 500);
            break;
        case 'listRemision':
            buscarReferencia('listRemision')
            break;
        // case 'listIncapacidad':
        //     buscarParametros('listIncapacidad')
        //     break;
        case 'divControl':
            cargarTableListaEspera();
            break;

        case 'divEventosAnestesia':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/anestesia.jsp', 'divParaPaginaEventosAnestesia');
            break;

        case 'divMonitoreoAnestesia':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/monitoreoAnestesia.jsp', 'divParaPaginaMonitoreoAnestesia');
            break;

        case 'divAdjuntos':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/adjuntos.jsp', 'divParaAdjuntos');
            break;

        case 'divArchivosAdjuntos':
            /*RANA CIRUGIA ANASTESIA*/
            buscarHistoria('listArchivosAdjuntos')
            break;
        case 'divArchivosAdjuntosEvento':
            /*RANA CIRUGIA ANASTESIA*/
            buscarHistoria('listArchivosAdjuntosEvento')
            break;

        case 'divListadoIdentificacion':
            buscarHistoria('listArchivosAdjuntosIdentificacion')
            break;

        case 'divListadoRemision':
            buscarHistoria('listArchivosAdjuntosRemision')
            break;

        case 'divListadoCarnet':
            buscarHistoria('listArchivosAdjuntosCarnet')
            break;
        case 'divListadoAyudasDiagnosticas':
            buscarHistoria('listAyudasDiagnost')
            break;
        case 'divListadoOtrosDocumentos':
            buscarHistoria('listArchivosAdjuntosVarios')
            break;

        case 'divListadoOtrosDocumentosHC':
            buscarHistoria('listArchivosAdjuntosVariosHC')
            break;

        case 'listaProcedimientosProgramados':
            /*RANA CIRUGIA ANASTESIA*/
            //cargarUnaPagina('/clinica/paginas/hc/cirugia/preOperatorio.jsp', 'divParaPreOperatorio');
            setTimeout(() => {
                buscarHistoria('listaProcedimientosProgramados')
            }, 200);
            break;

        case 'divPreOperatorio':
            /*RANA CIRUGIA ANASTESIA*/
            //cargarUnaPagina('/clinica/paginas/hc/cirugia/preOperatorio.jsp', 'divParaPreOperatorio');
            buscarHistoria('listaProcedimientosProgramados')
            break;

        case 'divPosOperatorio':
            /*RANA CIRUGIA ANASTESIA*/
            //cargarUnaPagina('/clinica/paginas/hc/cirugia/posOperatorio.jsp', 'divParaPosOperatorio');
            buscarHistoria('listPosOperatorio')
            break;

        case 'listaProcedimientosEjecutados':
            /*RANA CIRUGIA ANASTESIA*/
            //cargarUnaPagina('/clinica/paginas/hc/cirugia/posOperatorio.jsp', 'divParaPosOperatorio');
            buscarHistoria('listaProcedimientosEjecutados')
            break;

        case 'divPersonalProc':
            /*RANA CIRUGIA ANASTESIA*/
            cargarUnaPagina('/clinica/paginas/hc/cirugia/personalProc.jsp', 'divParaPersonalProc');
            break;

        case 'divOrdenesSubidas':
            buscarHistoria('listOrdenesArchivo');
            break;

        case 'divInterpretacion':
            buscarHistoria('listInterpretaciones');
            break;

        case 'divNuevaInterpretacion':
            buscarHistoria('listInterpretaciones');
            //buscarHistoria('listOrdenesArchivo');
            break;

        case 'divProgramacionTerapias':
            buscarAGENDA('listaProgramacionTerapias');
            calendario('txtFechaInicioTerapia', 1);
            calendario('txtFechaInicioTerapiaEditar', 1);
            break;

        case 'divTipificacion':
            buscarFacturacion("listArchivosTificacion");
            break;

        case 'divResultadosExternos':
            buscarHC('listaProcedimientoResultadosExternos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'clap2':
            buscarHC('listaConsultasAntenatales', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;


        case 'divResultadosExternosVIH':
            buscarHC('listaProcedimientoResultadosExternos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'divGlucometriasAfinamientos':
            $('#registroGlucometriasEnfe').hide();
            buscarHC('glucometriasEnfe', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'divGlucometrias':
            $('#registroGlucometriasEnfe').hide();
            buscarHC('glucometriasEnfe', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'divAfinamientos':
            $('#registroAfinamientosEnfe').hide();
            buscarHC('afinamientosEnfe', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;

        case 'divIngresoResultadosExternos':
            buscarHC('tablaOtrosResultadosExternosIngresados', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            break;


        case 'divSolicitudes':
            asignaAtributo("lblPacienteAdjuntos", valorAtributo("lblIdPaciente") + "-" + valorAtributo("lblIdentificacionPaciente") + "-" + valorAtributo("lblNombrePaciente"), 0)
            asignaAtributo("lblIdAdmisionAdjuntos", valorAtributo("lblIdAdmision"), 0)
            asignaAtributo("lblIdEvolucionAdjuntos", valorAtributo("lblIdDocumento"), 0)
            buscarHC('listaProcedimientoConductaTratamientoS', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
            buscarHistoria('listArchivosAdjuntosAnexos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;

        case 'divPlanTratmientoNut':
            $.ajax({
                url: 'http://10.0.0.3:9010/plant/' + valorAtributo('lblIdDocumento'),
                dataType: 'json',
                success: function (object) {
                    asignaAtributo('txtObjGeneral', object[0], 0);
                    asignaAtributo('txtEspe', object[1], 0);
                    asignaAtributo('txtPlanTr', object[3], 0);
                    asignaAtributo('txtDx', object[2], 0);
                    asignaAtributo('txtIdObjGeneral', object[4], 0);
                    asignaAtributo('cmbDxTipoPT', object[5], 0);
                    buscarHC('listEspecificos');
                }
            });
            break;

        case 'divRemision':

            let remision = document.getElementById('txtDxRemision').value
            document.getElementById("txtDXRemision").value = (remision.split('-')[0]).replace(/\s/g, "") + " " + remision.split('-')[1]
            //asignaAtributoCombo('txtDXRemision', (remision.split('-')[0]).replace(/\s/g, ""), remision.split('-')[1])
            asignaAtributoCombo('txtDXRemite', (remision.split('-')[0]).replace(/\s/g, ""), remision.split('-')[1])

            document.getElementById("cmbEspecialidadRemite_opcion").value = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_ESPECIALIDAD');
            document.getElementById("cmbEspecialidadRemite_opcion").innerHTML = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ESPECIALIDAD');

            $.ajax({
                url: 'http://10.0.0.3:9010/medico/' + IdSesion(),
                dataType: 'json',
                success: function (object) {
                    asignaAtributo('txtMedicoRemiteR', object[0][0].nompersonal, 1);
                    asignaAtributo('txtMedicoRemite', object[0][0].nompersonal, 1);
                }
            });
            var f = new Date();
            asignaAtributo('txtIdMedicoRemiteR', IdSesion(), 1);
            asignaAtributo('txtFechaRemitida', f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear(), 1);
            asignaAtributo('txtHoraRemitida', f.getHours() + ":" + f.getMinutes(), 1);
            asignaAtributo('txtHoraRemite', f.getHours() + ":" + f.getMinutes(), 1);
            if (document.getElementById("txtRemitidoDesde").checked == false) {
                desHabilitar('txtIpsRemisora', 1);
                desHabilitar('txtMedicoRemite', 1);
                desHabilitar('cmbEspecialidadRemite', 1);
                desHabilitar('txtFechaRemite', 1);
                desHabilitar('txtHoraRemite', 1);
                desHabilitar('txtDXRemite', 1);
            }
            if (document.getElementById("txtRemitidoa").checked == false) {
                desHabilitar('txtIpsRemitida', 1);
                desHabilitar('txtMedicoRemiteR', 1);
                desHabilitar('cmbEspecialidadRemite', 1);
                desHabilitar('txtFechaRemitida', 1);
                desHabilitar('txtHoraRemitida', 1);
                desHabilitar('cmbIdDxRemision', 1);
                desHabilitar('cmbPrioridadRemision', 1);
            }
            buscarHistoria('listRemisiones')
            break;

        case "divIndicacionMedica":
            buscarHC("listIndicaciones")
            break;


        // case 'divEducacionPaciente':
        //     buscarParametros('listGrillaEducacionPac')
        //     break;
    }
}

function cargarDisentimiento(aux) {
    ocultar('disentimiento')

    if (aux == 1) {
        mostrar('disentimiento')
    }
    else {
        ocultar('disentimiento')
    }
}
/***************************** cargar una pagina ******************************************/

function cargarUnaPagina(rutaPagina, divDestino) {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", rutaPagina, true);
    varajaxMenu.onreadystatechange = llenarcargarUnaPagina;
    varajaxMenu.onreadystatechange = function () { llenarcargarUnaPagina(divDestino) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarUnaPagina(divDestino) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divDestino).innerHTML = varajaxMenu.responseText;

            switch (divDestino) {
                case 'divParaAdjuntos':
                    buscarHistoria('listAdjuntos')
                    break;

                case 'divParaPaginaEventosAnestesia':
                    buscarHistoria('listAnestesia')
                    break;

                case 'divParaPaginaMonitoreoAnestesia':
                    buscarHistoria('listMonitoreo')
                    break;

                case 'divParaArchivosAdjuntos':
                    $("#tabsArchivosAdjuntos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    buscarHistoria('listArchivosAdjuntosVarios')
                    if(ventanaActual.opc == 'listaEspera'){
                        $("#idTificacion").hide();
                        $("#logTipificacion").hide();
                        
                    }
                    else {
                        $("#idTificacion").show();
                        $("#logTipificacion").show();
    
                    }
                    // LIMPIAR CAMPOS
                    limpiaAtributo("lblPacienteAdjuntos", 0)
                    limpiaAtributo("lblIdCitaAdjuntos", 0)
                    limpiaAtributo("lblIdAdmisionAdjuntos", 0)
                    limpiaAtributo("lblIdFacturaAdjuntos", 0)
                    limpiaAtributo("lblIdEvolucionAdjuntos", 0)
                    switch (ventanaActual.opc) {
                        case "nominalNefro":
                            let nominalIdFactura = traerDatoFilaSeleccionada("listGrillaNominalNefro", "id_factura");
                            let nominalPaciente = `${traerDatoFilaSeleccionada("listGrillaNominalNefro", "id")}-${traerDatoFilaSeleccionada("listGrillaNominalNefro", "identificacion_paciente")}-${traerDatoFilaSeleccionada("listGrillaNominalNefro", "nombre")}`;
                            asignaAtributo("lblIdFacturaAdjuntos", nominalIdFactura, 0);
                            asignaAtributo("lblPacienteAdjuntos", nominalPaciente, 0);
                            break;

                        case "rips":
                            setTimeout(function () {
                                var factura = $("#listaFacturasCuenta").jqGrid('getGridParam', 'selrow')
                                var id_factura = $("#listaFacturasCuenta").getRowData(factura).ID_FACTURA;
                                var id_admision = $("#listaFacturasCuenta").getRowData(factura).ID_ADMISION;
                                var id_paciente = $("#listaFacturasCuenta").getRowData(factura).ID_PACIENTE;
                                var identificacion = $("#listaFacturasCuenta").getRowData(factura).IDENTIFICACION;
                                var nombre_paciente = $("#listaFacturasCuenta").getRowData(factura).NOMBRE;

                                asignaAtributo("lblPacienteAdjuntos", id_paciente + "-" + identificacion + "-" + nombre_paciente, 0)
                                asignaAtributo("lblIdAdmisionAdjuntos", id_admision, 0)
                                asignaAtributo("lblIdFacturaAdjuntos", id_factura, 0)
                            }, 100)
                            break;

                        case "soloFactura":
                            var factura = $("#listSoloFacturas").jqGrid('getGridParam', 'selrow')
                            if (factura != null) {
                                id_factura = $("#listSoloFacturas").getRowData(factura).ID_FACTURA
                                asignaAtributo("lblPacienteAdjuntos", valorAtributo("txtIdBusPaciente"), 0)
                                asignaAtributo("lblIdFacturaAdjuntos", id_factura, 0)
                                //buscarHistoria('listArchivosAdjuntosVarios')
                            } else {
                                alert("FALTA SELECCIONAR LA FACTURA")
                                ocultar("divVentanitaAdjuntos")
                            }
                            break;

                        case "Facturas":
                            asignaAtributo("lblPacienteAdjuntos", valorAtributo("lblIdPaciente") + "-" + valorAtributo("lblIdentificacion") + "-" + valorAtributo("lblNomPaciente"), 0)
                            asignaAtributo("lblIdAdmisionAdjuntos", valorAtributo("lblIdAdmision"), 0)
                            asignaAtributo("lblIdFacturaAdjuntos", valorAtributo("lblIdFactura"), 0)
                            //buscarHistoria('listArchivosAdjuntosVarios')
                            break;

                        case "admisiones":
                            var admision = $("#listAdmisionCuenta").jqGrid('getGridParam', 'selrow')
                            if (admision != null) {
                                asignaAtributo("lblPacienteAdjuntos", decodeURI(valorAtributo("txtIdBusPaciente")), 0)
                                asignaAtributo("lblIdCitaAdjuntos", valorAtributo("lblIdCita"), 0)
                                asignaAtributo("lblIdAdmisionAdjuntos", valorAtributo("lblIdAdmision"), 0)
                                asignaAtributo("lblIdEvolucionAdjuntos", "", 0)
                                asignaAtributo("lblIdFacturaAdjuntos", valorAtributo("lblIdFactura"), 0)
                            } else {
                                alert("FALTA SELECCIONAR UNA ADMISION")
                                ocultar("divVentanitaAdjuntos")
                            }
                            break;

                        case "agenda":
                            asignaAtributo("lblPacienteAdjuntos", decodeURI(valorAtributo("txtIdBusPaciente")), 0)
                            asignaAtributo("lblIdCitaAdjuntos", valorAtributo("lblIdAgendaDetalle"), 0)
                            break;

                        case 'listaEspera':
                            asignaAtributo("lblPacienteAdjuntos", decodeURI(valorAtributo("txtIdBusPaciente")), 0)
                            break;

                        case 'principalHC':
                            let id_evolucion = valorAtributo("lblIdDocumento")
                            let paciente = valorAtributo("lblIdPaciente") + "-" + valorAtributo("lblIdentificacionPaciente") + "-" + valorAtributo("lblNombrePaciente")
                            let id_cita = ""
                            let id_admision = ""
                            if (id_evolucion == "") {
                                id_cita = valorAtributo("lblIdCita")
                                id_admision = valorAtributo("lblIdAdmisionAgen")
                            } else {
                                id_cita = valorAtributo("lblIdCitaFolio")
                                id_admision = valorAtributo("lblIdAdmision")
                            }
                            asignaAtributo("lblPacienteAdjuntos", paciente, 0)
                            asignaAtributo("lblIdCitaAdjuntos", id_cita, 0)
                            asignaAtributo("lblIdAdmisionAdjuntos", id_admision, 0)
                            asignaAtributo("lblIdEvolucionAdjuntos", id_evolucion, 0)
                            break;

                        case 'principalOrdenes':
                            asignaAtributo("lblPacienteAdjuntos", valorAtributo("lblIdPaciente") + "-" + valorAtributo("lblIdentificacionPaciente") + "-" + valorAtributo("lblNombrePaciente"), 0)
                            asignaAtributo("lblIdAdmisionAdjuntos", valorAtributo("lblIdAdmision"), 0)
                            asignaAtributo("lblIdEvolucionAdjuntos", valorAtributo("lblIdDocumento"), 0)
                            break;

                    }
                    break;

                case 'divParaArchivosAdjuntosEvento':
                    $("#tabsArchivosAdjuntosEvento").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    buscarHistoria('listArchivosAdjuntosEvento')
                    break;

                case 'divParaPreOperatorio':
                    buscarHistoria('listPreOperatorio')
                    break;

                case 'divParaPosOperatorio':
                    buscarHistoria('listPosOperatorio')
                    break;

                case 'divParaPersonalProc':
                    buscarHistoria('listPersonalProc')
                    break;

                case 'divParaArchivosAdjuntosOrdenExterna':
                    $("#tabsArchivosAdjuntos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    if ($("#tabsCoordinacionTerapia").tabs("option", "selected") == 0) {
                        var orden = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow')
                        var id_admision = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(orden).ID_ADMISION
                        var id_evolucion = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(orden).ID_EVOLUCION
                    } else if ($("#tabsCoordinacionTerapia").tabs("option", "selected") == 1) {
                        var orden = $("#listaOrdenesHistoricas").jqGrid('getGridParam', 'selrow')
                        var id_admision = $("#listaOrdenesHistoricas").getRowData(orden).ID_ADMISION
                        var id_evolucion = $("#listaOrdenesHistoricas").getRowData(orden).ID_EVOLUCION
                    }
                    asignaAtributo("lblPacienteAdjuntos", decodeURI(valorAtributo("txtIdBusPacienteOrdenes")), 0)
                    asignaAtributo("lblIdCitaAdjuntos", "", 0)
                    asignaAtributo("lblIdAdmisionAdjuntos", id_admision, 0)
                    asignaAtributo("lblIdEvolucionAdjuntos", id_evolucion, 0)
                    asignaAtributo("lblIdFacturaAdjuntos", "", 0)
                    break;
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}
/***************************** fin cargar una pagina ******************************************/


function adicionarSolicitudesADocumentos(id_Paciente, NomCompleto, id_articulo, nombre_articulo, id_orden, Fecha_Programada, Hora, esdatoOrden, Vigencia) {


    asignaAtributo('lblIdPaciente', id_Paciente, 1);
    asignaAtributo('lblNombrePaciente', NomCompleto, 1);

    asignaAtributo('lblIdArticulo', id_articulo, 1);
    asignaAtributo('lblNombreArticulo', nombre_articulo, 1);

    asignaAtributo('lblIdOrden', id_orden, 1);
    asignaAtributo('lblFechaProgramada', Fecha_Programada, 1);
    asignaAtributo('lblHoraProgramada', Hora, 1);
    asignaAtributo('lblEstadoOrden', esdatoOrden, 1);
    asignaAtributo('lblVigencia', Vigencia, 1);



    mostrar('divAdicionOrdenADocumento');

}


function abrirVentanitaAuditoria() {
    mostrar('divNovedadesAuditoria');
    // BORRAR   buscarHC('listDocumentosHistoricosTraerAuditoria' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )           

    modificarCRUD('meterDatosParaListarLaProyeccion');

}

function cerrarVentanitaAuditoria() {
    ocultar('divNovedadesTransaccion')
    limpiaAtributo('lblCodItem', 0)
    limpiaAtributo('lblNombreItem', 0)
    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('lblFechaProgramada', 0)
    limpiaAtributo('lblIdHoraMilitar', 0)
    limpiaAtributo('cmbIdConcepto', 0)
    limpiaAtributo('cmbIdCantidadNovedad', 0)
    limpiaAtributo('txtObservacionNovedad', 0)
}

function calendario(idElemento, conFechaDeSesion) {
    if (valorAtributo(idElemento) != 'undefined') {
        $("#drag" + ventanaActual.num).find('#' + idElemento).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-78:+78"
        });

        if (conFechaDeSesion == 1) {
            asignaAtributo(idElemento, document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0);
        }
    }

    /*if (valorAtributo(idElemento) != 'undefined' && valorAtributo(idElemento) != 'null') {
        var y = document.getElementById(idElemento);
       y.setAttribute("type", "date");
        if (conFechaDeSesion == 1) {
            var today = new Date();
            var fecha_hoy = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

            y.setAttribute("value", fecha_hoy);
        }
    }*/
}

function chequeadoInmediato() {
    if ($('#drag' + ventanaActual.num).find('#chkInmediato').attr('checked') == true) abrirVentanitaInmediatoMedicamentos()
}

function abrirVentanitaInmediatoMedicamentos() {
    mostrar('divInmediatoMedicamentos')
}

function cerrarVentanitaInmediatoMedicamentos() {
    ocultar('divInmediatoMedicamentos')
    $('#drag' + ventanaActual.num).find('#chkInmediato').attr('checked', '');
    limpiaAtributo('cmb_inmediato_horas', 0)
    limpiaAtributo('cmb_inmediato_dosis', 0)

}


function cerrarVentanita(idVentanita) {
    ocultar(idVentanita)
}

function abrirVentanita(idVentanita) {
    mostrar(idVentanita)
    //	document.getElementById('divInmediatoMedicamentos').style.top = topY;  

}

function cerrarDocumentClinico() {
    if (confirm("Esta seguro de FINALIZAR EL DOCUMENTO No:" + valorAtributo('lblIdDocumento') + "? \n Quedar� de su autor�a ")) {
        modificarCRUD('cerrarDocumento');
    }
}

function cerrarDocumentClinicoSinImp() {
    if (confirm("Esta seguro de FINALIZAR EL DOCUMENTO No:" + valorAtributo('lblIdDocumento') + "? \n Quedar� de su autor�a, \n la fecha de finalizacion se registrar� este momento al igual que el auxiliar que finaliza")) {
        modificarCRUD('cerrarDocumentoSinImp');
    }
}

function cerrarDocumentSinCambioAutor() {
    if (confirm("ESTA SEGURO DE FINALIZAR EL FOLIO? \nLa fecha de finalizacion se registrara este momento al igual que Ud sera el auxiliar que finaliza")) {
        modificarCRUD('cerrarDocumentoSinCambioAutor');
    }
}

function finalizaFolioConFecha() {
    if (confirm("ESTA SEGURO DE FINALIZAR EL FOLIO? \nLa fecha de finalizacion se registrara este momento al igual que el estado de finaliza")) {
        modificarCRUD('finalizaFolioConFecha');
    }
}

function finalizarFolio() {
    if (confirm("Esta seguro de FINALIZAR EL FOLIO No:" + valorAtributo('lblIdFolioJasper') + "? \n Quedara de su autoria, \n la fecha de finalizacion se registrara este momento al igual que el auxiliar que finaliza")) {
        modificarCRUD('cerrarFolioJasper');
    }
}


function cambioEstadoFolio() {
    if (confirm("SEGURO DE CAMBIAR EL ESTADO DEL FOLIO? \n DESEA CAMBIARLO A: " + valorAtributoCombo('cmbIdEstadoFolioEdit'))) {
        //	if(valorAtributo('txtIdProfesionalElaboro')==IdSesion())
        //	else alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA CAMBIAR DE ESTADO')	

        if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 10) {
            alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
            alert('EL ESTADO FINALIZADO URGENTE DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 7) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else if (valorAtributo('txtIdEsdadoDocumento') == 9) {
            alert('EL ESTADO CANCELADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR')
        } else modificarCRUD('cambioEstadoFolio');

    }
}

function eliminaConsumo() {
    if (valorAtributo('txtUConsumo') != 'S') {
        modificarCRUD('eliminarListHojaGasto');
    } else {
        modificarCRUD('eliminarUltimoConsumo');
    }

}

function eliminarTransaccionDevolucion() {
    comboB = valorAtributo('cmbIdBodega');
    if (comboB == '1') {
        modificarCRUD('eliminaTransaccion');
    } else {
        modificarCRUD('eliminarTransaccionDevHG');


    }
}

function insertarTransaccionDevolucion() {
    comboB = valorAtributo('cmbIdBodega');
    if (comboB == '1') {
        modificarCRUD('crearTransaccion');
    } else {
        modificarCRUD('crearTransaccionDevHG');


    }
}

function cargarHG(valor) {
    if (valor != '1') {
        $('#listaDevolucionesHG').show();
        buscarSuministros('listGrillaDevolucionHG');
    } else {
        $('#listaDevolucionesHG').hide();
    }

    cargarCanasta(valorAtributo('cmbIdBodegaDestino'));

}

function cargarCanasta(valor) {
    if (valorAtributo('cmbIdBodega') == '1' && valor == '3') {
        $('#listaCanastaTraslado').show();
    } else {
        $('#listaCanastaTraslado').hide();
    }
}




function cambiarOpcionIdQuery(id) {
    idQuery = id;

}



function comboCargarBodegaVentas(combo) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, 57, IdSesion())
    //cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidadEditar', 124, valorAtributo('lblIdElementoEditar'))
}

function comboCargarElementos1(combo, idQuery, condicion) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, idQuery, valorAtributo(condicion))
}

function comboBodegasConsumos(combo) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, 515, IdSesion())
}

function comboBodegasDespachos(combo, idQuery) {
    cargarComboGRALCondicion1('lblId', 'xxx', combo, idQuery, IdSesion())
}

async function comboCreacionDedocumentos() {
    try {
        await cargarComboGRALCondicion2Async('cmbPadre', '1', 'cmbTipoDocumento', 80, valorAtributo('lblIdTipoAdmision'), document.getElementById('lblIdProfesion').lastChild.nodeValue);
    } catch (error) {
        // Manejar el error si es necesario
        console.error(error);
    }
}


function comboTiposDeFolios() {
    cargarComboGRALCondicion1('lblId', 'xxx', 'cmbTipoFolio', 507, valorAtributo('cmbIdTipoServicio'))
}

function comboFormaFarmaceutica() {
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidad', 124, valorAtributoIdAutoCompletar('txtIdArticulo'))
}

function comboFormaFarmaceuticaEditar() {
    asignaAtributoCombo2('cmbUnidadEditar', '', '');
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidadEditar', 124, valorAtributo('lblIdElementoEditar'))
}



function comboFormaFarmaceuticaAdm() {
    cargarComboGRALCondicion1('cmbPadre', '1', 'cmbUnidadAdm', 124, valorAtributoIdAutoCompletar('txtIdMedicamento'))
}

function activarODesactivarElemServicio(idSelec) {
    limpiaAtributo('cmbTipoId', 0)
    limpiaAtributo('txtIdentificacion', 0)
    limpiaAtributo('txtIdBusPaciente', 0)
    limpiaAtributo('cmbIdProfesionales', 0)
    limpiaAtributo('cmbIdArea', 0)
    limpiaAtributo('cmbIdHabitacion', 0)
    limpiaAtributo('cmbIdEstadoCama', 0)

    ocultar('idElemHOSDOM')
    ocultar('idElemHOS')
    ocultar('idElemAYD')
    ocultar('idElementoCEX')
    ocultar('idElementosVIH')

    if (valorAtributo('cmbIdTipoServicio') == 'AYD' || valorAtributo('cmbIdTipoServicio') == 'CPR' || valorAtributo('cmbIdTipoServicio') == 'LAB') {
        mostrar('idElemAYD')
    }

    if (["HDP", "HDC"].indexOf(valorAtributo('cmbIdTipoServicio')) != -1) {
        limpiaAtributo('cmbIdDepartamento', 0)
        limpiaAtributo('cmbIdMunicipio', 0)
        limpiaAtributo('cmbIdLocalidad', 0)
        setTimeout(() => {
            mostrar('idElemHOSDOM')
        }, 100);
    } else if (["CEX"].indexOf(valorAtributo('cmbIdTipoServicio')) != -1) {
        limpiaAtributo('cmbIdEspecialidadCEX', 0)
        limpiaAtributo('cmbIdProfesionalCEX', 0)
        setTimeout(() => {
            mostrar('idElementoCEX')
        }, 100);
    } else if (["HOS"].indexOf(valorAtributo('cmbIdTipoServicio')) != -1) {
        mostrar('idElemHOS')
    } else if (["AVH"].indexOf(valorAtributo('cmbIdTipoServicio')) != -1) {
        mostrar('idElementosVIH')
    }

    setTimeout(() => {
        contenedorBuscarPaciente()
    }, 100);

}

function cambiarEstadoFolioAyudaDiagnostica() {

    if (valorAtributo('cmbIdProfesionales') != '') {
        limpiarListadosTotales('listGrillaPacientes');

        if (valorAtributo('cmbIdEstadoFolio') != '') {
            asignaAtributo('txtFechaDesdeFolio', '01/01/2016', 0)
            buscarHistoria('ordenesAYD')
        }
    } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO.')
}

function contenedorBuscarPaciente() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/hc/contenidoBuscarPacientes.jsp?', true);
    varajaxMenu.onreadystatechange = llenarcontenedorBuscarPaciente;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcontenedorBuscarPaciente() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaContenedorBuscarPaciente").innerHTML = varajaxMenu.responseText;
            BuscarYcambiarIdQueryPorServicio()
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}


function BuscarYcambiarIdQueryPorServicio() { //alert(valorAtributo('cmbIdTipoServicio'))
    $("#divListado table").remove();
    contenedorDiv = document.getElementById('divListado');
    tabla = document.createElement('table');  // se crean los div hijos
    tabla.id = 'listGrillaPacientes'
    tabla.className = 'scroll';
    contenedorDiv.appendChild(tabla);
    //document.getElementById('divListado').innerHTML = '<table id="listGrillaPacientes" class="scroll"></table>';

    habilitar('cmbIdUnidad', 1)
    if (valorAtributo('txtIdBusPaciente') == '') {
        if (valorAtributo('cmbIdTipoServicio') == 'LAP') {
            if (valorAtributo('cmbIdTipoServicio') == 'LAP') {
                buscarHistoria('ordenesLAP')
                habilitar('cmbIdUnidad', 0)
            }
        } else if (valorAtributo('cmbIdTipoServicio') == 'CEX') {
            buscarHistoria('ordenesCEX')
            habilitar('cmbIdUnidad', 0)
        } else if (valorAtributo('cmbIdTipoServicio') == 'HME') {
            buscarHistoria('ordenesCEX')
            habilitar('cmbIdUnidad', 0)
        } else if (valorAtributo('cmbIdTipoServicio') == 'CIR') {
            buscarHistoria('ordenesCIR')
            habilitar('cmbIdUnidad', 0)
        } else if (valorAtributo('cmbIdTipoServicio') == 'AYD') {
            if (valorAtributo('cmbIdProfesionales') != '') {
                buscarHistoria('ordenesAYD')
                habilitar('cmbIdUnidad', 0)
            } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO')
        } else if (valorAtributo('cmbIdTipoServicio') == 'CPR') {
            if (valorAtributo('cmbIdProfesionales') != '') {
                buscarHistoria('ordenesCPR')
                habilitar('cmbIdUnidad', 0)
            } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO')
        } else if (valorAtributo('cmbIdTipoServicio') == 'LAB') {
            if (valorAtributo('cmbIdProfesionales') != '') {
                buscarHistoria('ordenesAYD')
                habilitar('cmbIdUnidad', 0)
            } else alert('DEBE SELECCIONAR EL PROFESIONAL QUE ELABORO')
        } else if (['HOS'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesHOS')
        } else if (['HDP', 'HDC'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesHD')
        } else if (['URG', 'ORI', 'TRG', 'TRC'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesURG')
        } else if (['CPP'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesCPP')
        } else if (['TMC'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesTMC')
        } else if (['AVH'].indexOf(valorAtributo("cmbIdTipoServicio")) != -1) {
            buscarHistoria('ordenesAVH')
        }
    } else {
        if (valorAtributo('cmbIdTipoServicio') == 'INT') {
            if (valorAtributo('txtIdBusPaciente') != '') {
                buscarHistoria('ordenesINT')
            } else {
                alert('SELECCIONE UN PACIENTE')
            }
        } else {
            buscarHistoria('ordenesIndividuales', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        }

    }

}

function BuscarYcambiarIdQueryPorServicioAuditoria() {

    limpiarListadosTotales('listGrillaPacientes');
    habilitar('cmbIdUnidad', 1)

    if (valorAtributo('txtBusIdPaciente') == '') {
        if (valorAtributo('cmbIdTipoServicio') == 'HSP' && valorAtributo('cmbIdUnidad') != '') {
            buscarHC('ordenesAuditoria', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else if (valorAtributo('cmbIdTipoServicio') == 'URG' && valorAtributo('cmbIdUnidad') != '') {
            buscarHC('ordenesAuditoria', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else if (valorAtributo('cmbIdTipoServicio') == 'OTR' && valorAtributo('cmbIdUnidad') != '') {
            buscarHC('ordenes', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
        } else if (valorAtributo('cmbIdTipoServicio') == 'CEX') {
            buscarHC('ordenesCEX', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            habilitar('cmbIdUnidad', 0)
        }
    } else { buscarHC('ordenesIndividualesAuditoria', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp') }

}



function abrirVentanitaArticulosBodega() {

    if (valorAtributo('txtVentanaTrasladoBodegas') != 'undefined') { // solo entra desde la ventana de traslado de bodegas

        cargarComboGRALCondicion1('cmbPadre', 2, 'cmbBodegaDestino', 71, valorAtributo('lblId')); // id bodega origen que se excluira

        mostrar('divArticulosBodega')
        document.getElementById('divArticulosBodega').style.top = topY;


    }
}


function abrirVentanitaOrdenInsumos() {
    mostrar('divNovedadesOrdenInsumos')
    document.getElementById('divNovedadesOrdenInsumos').style.top = topY;

}

function cerrarVentanitaOrdenInsumos() {
    ocultar('divNovedadesOrdenInsumos')

    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('idArticulo', 0)
    limpiaAtributo('lblNombreItem', 0)
}

function abrirVentanitaOrdenMedicamentos() {
    mostrar('divNovedadesOrdenMedicamentos')
    document.getElementById('divNovedadesOrdenMedicamentos').style.top = topY;
    buscarHC('listTrazabilidadMedicamento', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
}

function cerrarVentanitaOrdenMedicamentos() {
    ocultar('divNovedadesOrdenMedicamentos')

    //   limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('idArticulo', 0)
    limpiaAtributo('lblNombreItem', 0)
}

function abrirVentanitaNovedadesAdministracion(codItem, NomItem) {
    mostrar('divNovedadesAdministracion')
    $('#drag' + ventanaActual.num).find('#lblCodItem').html(codItem)
    $('#drag' + ventanaActual.num).find('#lblNombreItem').html(NomItem)

    $('#drag' + ventanaActual.num).find('#txtValorConteo').val('');
    $('#drag' + ventanaActual.num).find('#txtValorConteo').focus();

    document.getElementById('divNovedadesAdministracion').style.top = topY;

}

function cerrarVentanitaNovedadesAdministracion() {
    ocultar('divNovedadesAdministracion')
    limpiaAtributo('lblCodItem', 0)
    limpiaAtributo('lblNombreItem', 0)
    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('lblFechaProgramada', 0)
    limpiaAtributo('lblIdHoraMilitar', 0)
    limpiaAtributo('cmbIdConcepto', 0)
    limpiaAtributo('cmbIdCantidadNovedad', 0)
    limpiaAtributo('txtObservacionNovedad', 0)
}

function abrirVentanitaNovedadesTransaccion(codItem, NomItem) {
    mostrar('divNovedadesTransaccion');

    $('#drag' + ventanaActual.num).find('#lblCodItem').html(codItem)
    $('#drag' + ventanaActual.num).find('#lblNombreItem').html(NomItem)

    //   setTimeout("buscarHC('listProyeccionOrden' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )",500);   
}

function cerrarVentanitaNovedadesTransaccion() {
    ocultar('divNovedadesTransaccion')
    limpiaAtributo('lblCodItem', 0)
    limpiaAtributo('lblNombreItem', 0)
    limpiaAtributo('lblIdOrden', 0)
    limpiaAtributo('lblFechaProgramada', 0)
    limpiaAtributo('lblIdHoraMilitar', 0)
    limpiaAtributo('cmbIdConcepto', 0)
    limpiaAtributo('cmbIdCantidadNovedad', 0)
    limpiaAtributo('txtObservacionNovedad', 0)
}



function horasDosis() {
    this.idCombo;
    this.valueCombo;
}
var horasCombo = new Array();



function sumarALaDosisOrdenMedica() { //alert( idElem );
    suma = 0;
    for (i = 0; i <= 23; i++) {
        valorElemento = $('#drag' + ventanaActual.num).find('#cmb_' + i + ' option:selected').text();
        horasCombo[i] = new horasDosis();
        if (valorElemento > 0) {
            horasCombo[i].idCombo = i;
            horasCombo[i].valueCombo = parseFloat(valorElemento);
            suma = suma + horasCombo[i].valueCombo;
        } else {
            horasCombo[i].idCombo = i;
            horasCombo[i].valueCombo = 0;
        }
        //alert( horasCombo[i].idCombo +' -- '+horasCombo[i].valueCombo);				

    }

    $('#drag' + ventanaActual.num).find('#lblDosis').html(suma); // asigna valor a sumatoria de la dosisi
    $('#drag' + ventanaActual.num).find('#lblTotUnidades').html($.trim($('#drag' + ventanaActual.num).find('#cmbIdRepeticionProgramada').val()) * suma); //asigna valor a sumatoria de la dosis por el numero de dias repetidos



}

function limpiarTodosLosCombosDeLaDosisOrdenMedica() { //alert( idElem );

    for (i = 0; i <= 23; i++) {
        $('#drag' + ventanaActual.num).find('#cmb_' + i).val('');
    }
}

function sumarALaDosisOrdenMedicaInsumo() { //alert( idElem );
    suma = 0;
    $('#drag' + ventanaActual.num).find('#lblTotUnidadesInsumo').html($.trim($('#drag' + ventanaActual.num).find('#cmbDosisCantInsumo').val()) * $.trim($('#drag' + ventanaActual.num).find('#cmbIdRepeticionProgramadaInsumo').val())); //asigna valor a sumatoria de la dosis por el numero de dias repetidos

}

function seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica() {
    for (i = 0; i <= 23; i++) {
        if ($('#drag' + ventanaActual.num).find('#cmb_' + i).val() != '') // al menos uno
            return true;
    }
    return false;

}



function limpiaCache() {
    if ('Navigator' == navigator.appName)
        document.forms[0].reset()

}

function abrirTutorial(paginaAbrir) {
    window.open(paginaAbrir, "", "width=1000, height=700, top=80, left=40, scrollbars=yes, Resizable=No, Directories=No, Location=No, Menubar=No, Status=No, Titlebar=No, Toolbar=No, fullscreen=yes");

}



function respuestaeliminarAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {
                    listadoTablaHC(opcion);
                    limpiarElementosDeSeleccionParaLista(opcion);
                } else {
                    alert("No se pudo ingresar la informaci�n");
                }

            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}

function add_parametros(parametros) {
    let aux = "";
    for (let i = 0; i < parametros.length; i++) {
        if (parametros[i] == "") {
            aux += "_-x.X.x"
        } else {
            aux += "_-" + parametros[i]
        }
    }

    return aux
}

function add_valores_a_mandar(valor) {

    if (tipoElem == 'cmb') {
        switch (valor) {
            case '':
                valores_a_mandar = valores_a_mandar + '_-x.X.x';
                break;
            default:
                valores_a_mandar = valores_a_mandar + '_-' + valor;
        }
    }
    if (tipoElem == 'txt') {
        switch (valor) {
            case '':
                valores_a_mandar = valores_a_mandar + '_-x.X.x';
                break;
            default:
                valores_a_mandar = valores_a_mandar + '_-' + valor;
        }
    }
    if (tipoElem == 'lbl') {
        switch (valor) {
            case '':
                valores_a_mandar = valores_a_mandar + '_-x.X.x';
                break;
            default:
                valores_a_mandar = valores_a_mandar + '_-' + valor;
        }
    }
    if (tipoElem == 'chk') { //alert('aqui llega='+tipoElem); alert('aqui valor='+valor)
        switch (valor) {
            case 'true':
                valores_a_mandar = valores_a_mandar + '_-1'; //alert('aqui valores_a_mandar='+valores_a_mandar)
                break;
            case 'false':
                valores_a_mandar = valores_a_mandar + '_-0';
                break;
        }
    }

}


function guardarAlListado(opcion) {
    banderita = 1;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaguardarAlListado(opcion) };

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    valores_a_mandar = "accion=" + opcion;


    switch (opcion) {
        /***************************************************************************** INICIO  HISTORIA CLINICA *************************************************/
        case 'listOrdenesMedicamentos':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
                valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
                valores_a_mandar = valores_a_mandar + "&cmbIdVia=" + valorAtributo('cmbIdVia');
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDosis=" + valorAtributo('cmbIdTipoDosis');
                valores_a_mandar = valores_a_mandar + "&cmbIdRepeticionProgramada=" + valorAtributo('cmbIdRepeticionProgramada');
                valores_a_mandar = valores_a_mandar + "&cmbIdIntervalo=" + valorAtributo('cmbIdIntervalo');
                valores_a_mandar = valores_a_mandar + "&txtObservaciones=" + valorAtributo('txtObservaciones');
                valores_a_mandar = valores_a_mandar + "&chkInmediato=" + valorAtributo('chkInmediato');
                valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');



                valores_a_mandar = valores_a_mandar + "&HorasCombo="

                if (valorAtributo('chkInmediato') == 'false') {
                    for (i = 0; i <= 23; i++) { // lee todos los valores de los combos de dosis de medicamento
                        if (horasCombo[i].valueCombo > 0) {
                            // alert( horasCombo[i].idCombo +' -- '+horasCombo[i].valueCombo);		
                            valores_a_mandar = valores_a_mandar + horasCombo[i].idCombo + '_-' + horasCombo[i].valueCombo + ',-';
                        }
                    }
                    valores_a_mandar = valores_a_mandar + "&txtFechaOrdenMed=" + valorAtributo('txtFechaOrdenMed');
                    valores_a_mandar = valores_a_mandar + "&cmbHoraOrdenMed=" + valorAtributo('cmbHoraOrdenMed');

                } else {
                    valores_a_mandar = valores_a_mandar + valorAtributo('cmb_inmediato_horas') + '_-' + valorAtributo('cmb_inmediato_dosis') + ',-';
                    valores_a_mandar = valores_a_mandar + "&txtFechaOrdenMed=" + valorAtributo('txtFechaOrdenMed');
                    valores_a_mandar = valores_a_mandar + "&cmbHoraOrdenMed=" + valorAtributoCombo('cmb_inmediato_horas');


                }

            } else banderita = 0;
            break;
        case 'listOrdenesMedicamentosModificar':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
                valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
                valores_a_mandar = valores_a_mandar + "&cmbIdVia=" + valorAtributo('cmbIdVia');
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDosis=" + valorAtributo('cmbIdTipoDosis');
                valores_a_mandar = valores_a_mandar + "&cmbIdRepeticionProgramada=" + valorAtributo('cmbIdRepeticionProgramada');
                valores_a_mandar = valores_a_mandar + "&cmbIdIntervalo=" + valorAtributo('cmbIdIntervalo');
                valores_a_mandar = valores_a_mandar + "&txtObservaciones=" + valorAtributo('txtObservaciones');
                valores_a_mandar = valores_a_mandar + "&chkInmediato=" + valorAtributo('chkInmediato');
                valores_a_mandar = valores_a_mandar + "&txtFechaOrdenMed=" + valorAtributo('txtFechaOrdenMed');
                valores_a_mandar = valores_a_mandar + "&cmbHoraOrdenMed=" + valorAtributo('cmbHoraOrdenMed');
                valores_a_mandar = valores_a_mandar + "&lblIdSolicitud=" + valorAtributo('lblIdOrden');

                valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');

                //alert(valores_a_mandar);
                valores_a_mandar = valores_a_mandar + "&HorasCombo="

                if (valorAtributo('chkInmediato') == 'false') {
                    for (i = 0; i <= 23; i++) { // lee todos los valores de los combos de dosis de medicamento
                        if (horasCombo[i].valueCombo > 0) {
                            // alert( horasCombo[i].idCombo +' -- '+horasCombo[i].valueCombo);		
                            valores_a_mandar = valores_a_mandar + horasCombo[i].idCombo + '_-' + horasCombo[i].valueCombo + ',-';
                        }
                    }
                } else {
                    valores_a_mandar = valores_a_mandar + valorAtributo('cmb_inmediato_horas') + '_-' + valorAtributo('cmb_inmediato_dosis') + ',-';
                }
                //	alert('listOrdenesMedicamentos '+valores_a_mandar);
            } else banderita = 0;
            break;

        /***************************************************************************** FIN   HISTORIA CLINICA *************************************************/
        case 'listProfesionalesFicha':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + valorAtributo('cmbIdAreaDerecho');
            valores_a_mandar = valores_a_mandar + "&idProfesional=" + valorAtributo('cmbProfesional');
            break;
        case 'listDxIntegralInicial':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&dxIntegralInicial=" + valorAtributo('txtDxIntegralInicial');
            break;
        case 'listSituaActual':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
                valores_a_mandar = valores_a_mandar + "&descripcion=" + valorAtributo('txtSituacionActual_' + tabActivo);
            }
            break;
        case 'listObjMeta':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&objetivo=" + valorAtributo('txtObjetivo_' + tabActivo);
            valores_a_mandar = valores_a_mandar + "&meta=" + valorAtributo('txtMeta_' + tabActivo);
            break;
        case 'listActividad':
            valores_a_mandar = valores_a_mandar + "&idPlatin=" + valorAtributo('lblIdPlatin');
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&descripcion=" + valorAtributo('txtActividad_' + tabActivo);
            break;


    }
    //	   alert('guardarAlListado...'+valores_a_mandar);  
    if (banderita == '1')
        varajaxInit.send(valores_a_mandar);

}

function respuestaguardarAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) { //  alert('respuesta= '+raiz.getElementsByTagName('respuesta')[0].firstChild.data)
                    listadoTablaHC(opcion);
                    limpiarElementosDeSeleccionParaLista(opcion);
                } else {
                    alert("No se pudo ingresar la informaci�n");
                }

            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}


/*function generarFactura(){

    if(valorAtributo('lblIdCuenta') !=''){

        var ids = jQuery("#listProcedimientosDeFactura").getDataIDs();			  
    	
        if(ids.length != 0){

        if(valorAtributo('lblIdFactura') == ''){
            guardarYtraerDatoAlListado('validaNumeracionFactura');
        }else{
            alert('LA FACTURA YA SE ENCUENTRA GENERADA.');
        }


        }else{
            alert('DEBE AGREGAR ALGUN PROCEDIMIENTO.');
        }

        }else{
            alert('SELECCIONE UNA CUENTA PARA GENERAR LA FACTURA.');
	

    }

}*/


function guardarYtraerDatoAlListado(opcion) { //para guardar en la grilla 

    valores_a_mandar = "accion=" + opcion;

    switch (opcion) {
        case 'auditarNotaCreditoFE':
            if (verificarCamposGuardar(opcion)) {
                var lista_facturas = [];

                var ids = $("#listGrillaFacturas").getDataIDs();

                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_listGrillaFacturas_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listGrillaFacturas").getRowData(ids[i]);
                        lista_facturas.push(datosRow.ID_FACTURA);
                    }
                }
                valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
            }
            break;

        case 'auditarFE':
            if (verificarCamposGuardar(opcion)) {
                var lista_facturas = [];

                var ids = $("#listGrillaFacturas").getDataIDs();

                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_listGrillaFacturas_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listGrillaFacturas").getRowData(ids[i]);
                        lista_facturas.push(datosRow.ID_FACTURA);
                    }
                }
                valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + lista_facturas.toString();
            }
            break;

        case 'enviarSF':
            if (valorAtributo('lblIdEvolucion') == '') {
                idFolioSel = document.getElementById('lblIdDocumento').innerHTML;
                valores_a_mandar = valores_a_mandar + "&lblIdEvolucion=" + idFolioSel;
            } else {
                valores_a_mandar = valores_a_mandar + "&lblIdEvolucion=" + valorAtributo('lblIdEvolucion');
            }
            break;

        case 'consultarSF':
            valores_a_mandar = valores_a_mandar + "&lblIdEvolucion=" + valorAtributo('lblIdEvolucion');
            break

        case 'auditarRecibo':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdRecibo=" + valorAtributo('lblIdRecibo');
            }
            break;

        case 'validaNumeracionFactura':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
                valores_a_mandar = valores_a_mandar + "&txtIdPaciente=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            }
            break;

        case 'auditarFactura':
            if (verificarCamposGuardar(opcion)) {
                valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
            }
            break;

        case 'anularFacturaWS':
            valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo('lblIdFactura');
            break;


        case 'cerrarDocumentoProgramacion':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') != '1') {
                    valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'cerrarDocumentoSolicitudesInventario':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') != '1') {
                    valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'modificarTransaccionDocumento':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') == '1' && valorAtributo('lblNaturaleza') == 'I') {
                    valores_a_mandar = valores_a_mandar + "&lblIdTr=" + valorAtributo('lblIdTr');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidadAnt=" + valorAtributo('txtTrCantidadAnt');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidad=" + valorAtributo('txtTrCantidad');
                    valores_a_mandar = valores_a_mandar + "&txtVlrTr=" + valorAtributo('txtVlrTr');
                    valores_a_mandar = valores_a_mandar + "&lblVlrImpuesto=" + valorAtributo('lblVlrImpuesto');
                    valores_a_mandar = valores_a_mandar + "&cmbVlrIva=" + valorAtributo('cmbVlrIva');
                    valores_a_mandar = valores_a_mandar + "&txtLoteTr=" + valorAtributo('txtLoteTr');
                    valores_a_mandar = valores_a_mandar + "&txtFechaTr=" + valorAtributo('txtFechaTr');
                    valores_a_mandar = valores_a_mandar + "&txtLoteAnt=" + valorAtributo('txtLoteAnt');
                } else { alert('EL DOCUMENTO NO ESTA FINALIZADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'eliminarTransaccionDocumento':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') == '1' && valorAtributo('lblNaturaleza') == 'I') {
                    valores_a_mandar = valores_a_mandar + "&lblIdTr=" + valorAtributo('lblIdTr');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidadAnt=" + valorAtributo('txtTrCantidadAnt');
                    valores_a_mandar = valores_a_mandar + "&txtTrCantidad=" + valorAtributo('txtTrCantidad');
                    valores_a_mandar = valores_a_mandar + "&txtVlrTr=" + valorAtributo('txtVlrTr');
                    valores_a_mandar = valores_a_mandar + "&lblVlrImpuesto=" + valorAtributo('lblVlrImpuesto');
                    valores_a_mandar = valores_a_mandar + "&cmbVlrIva=" + valorAtributo('cmbVlrIva');
                    valores_a_mandar = valores_a_mandar + "&txtLoteTr=" + valorAtributo('txtLoteTr');
                    valores_a_mandar = valores_a_mandar + "&txtFechaTr=" + valorAtributo('txtFechaTr');
                    valores_a_mandar = valores_a_mandar + "&txtLoteAnt=" + valorAtributo('txtLoteAnt');
                } else { alert('EL DOCUMENTO NO ESTA FINALIZADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'verificaTransaccionesInventario':
            if (valorAtributo('lblIdEstado') != '') {
                if (valorAtributo('lblIdEstado') != '1') {
                    valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            } else { alert('SELECCIONE EL DOCUMENTO'); return false; }
            break;
        case 'verificaTransaccionesInventarioTrasladoBodega':
            if (valorAtributo('lblIdDoc') != '') {
                if (valorAtributo('cmbIdBodegaDestino') != '') {
                    if (valorAtributo('cmbIdBodega') != valorAtributo('cmbIdBodegaDestino')) {

                        if (valorAtributo('lblIdEstado') != '1') {

                            if (confirm('SE REALIZARA EL TRASLADO A : ' + valorAtributoCombo('cmbIdBodegaDestino') +
                                "\nDESEA CONTINUAR?")) {
                                valores_a_mandar = valores_a_mandar + "&lblIdDoc=" + valorAtributo('lblIdDoc');
                                valores_a_mandar = valores_a_mandar + "&cmbIdBodegaDestino=" + valorAtributo('cmbIdBodegaDestino');
                            } else {
                                return false;
                            }


                        } else { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }

                    } else { alert('NO PUEDEN SER ORIGEN Y DESTINO LA MISMA BODEGA'); return false; }
                } else { alert('SELECCIONE BODEGA DESTINO'); return false; }
            } else { alert('SELECCIONE UN DOCUMENTO'); return false; }
            break;



        case 'consultaExamenFisico':
            if (valorAtributo('lblIdDocumento') != '') {
                valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
            } else {
                return;
            }
            break;


        case 'elementosDelFolio':
            if (valorAtributo('lblIdDocumento') != '') {
                if (valorAtributo('txtIdEsdadoDocumento') != '1') {
                    if (valorAtributo('lblTipoDocumento') == 'CRNC' || valorAtributo('lblTipoDocumento') == 'CRNH') {
                        if (confirm('ESTA SEGURO DE FIRMAR EL FOLIO?')) {
                            valores_a_mandar = valores_a_mandar + "&lblTipoDocumento=" + valorAtributo('lblTipoDocumento');
                            valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        valores_a_mandar = valores_a_mandar + "&lblTipoDocumento=" + valorAtributo('lblTipoDocumento');
                        valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
                    }
                } else { alert('EL FOLIO YA SE ENCUENTRA FINALIZADO'); return false; }
            } else { alert('SELECCIONE EL FOLIO'); return false; }
            break;

        case 'buscarIdentificacionPaciente':
            if (verificarCamposGuardar('buscarIdentificacionPaciente')) {
                valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoId');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
            }
            break;

        case 'buscarIdPacienteEncuesta':
            valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoIdUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            break;

        case 'buscarIdentificacionPacienteUrgencias':
            valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoIdUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacionUrgencias');
            break;

        case 'creaValorCuentaFacturaCirugia':
            if (verificarCamposGuardar('buscarIdentificacionPaciente')) {
                valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoId');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
            }
            break;

        case 'buscarSiEsNoPos':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            break;

        case 'buscarSiEsNoPosInsumos':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticuloInsumo');
            break;

        case 'buscarConciliacion':
            valores_a_mandar = valores_a_mandar + "&lblIdDocumento=" + valorAtributo('lblIdDocumento');
            break;

        case 'buscarSiEsNoPosProcedim':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdProcedimiento');
            break;

        case 'buscarSiEsNoPosProcedimientoNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdProcedimiento');
            break;

        case 'buscarSiEsNoPosAyudaDiagnostica':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
            break;
        case 'buscarSiEsNoPosAyudaDiagnosticaNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
            break;
        case 'buscarSiEsNoPosLaboratorio':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdLaboratorio');
            break;
        case 'buscarSiEsNoPosLaboratorioNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdLaboratorio');
            break;
        case 'buscarSiEsNoPosTerapia':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdTerapia');
            break;
        case 'buscarSiEsNoPosTerapiaNotificacion':
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdTerapia');
            break;
        case 'consultarDisponibleAgendaSinLE':
            valores_a_mandar = valores_a_mandar + "&lblIdAgendaDetalle=" + valorAtributo('lblIdAgendaDetalle');
            break;

        case 'consultarDisponibleDesdeAgenda':
            //				  valores_a_mandar=valores_a_mandar+"&txtEstadoCita="+valorAtributo('txtEstadoCita');		  		  
            valores_a_mandar = valores_a_mandar + "&lblIdAgendaDetalle=" + valorAtributo('lblIdAgendaDetalle');
            valores_a_mandar = valores_a_mandar + "&txtIdBusPaciente=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            valores_a_mandar = valores_a_mandar + "&cmbTipoCita=" + valorAtributo('cmbTipoCita');
            valores_a_mandar = valores_a_mandar + "&lblIdListaEspera=" + valorAtributo('lblIdListaEspera');
            break;

        case 'buscarPendienteLE_LE':
            valores_a_mandar = "accion=consultarDisponibleDesdeListaEspera";
            valores_a_mandar = valores_a_mandar + "&txtIdBusPaciente=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            valores_a_mandar = valores_a_mandar + "&cmbTipoCita=" + valorAtributo('cmbTipoCita');
            break;

        case 'nuevoDocumentoHC':
            if (valorAtributo('lblIdTipoAdmision') != '') {
                if (valorAtributo('cmbIdTipoServicio') != 'INT') {
                    valores_a_mandar = valores_a_mandar + '&opcionTipo=MI_AGENDA'
                    valores_a_mandar = valores_a_mandar + "&lblIdAdmision=" + valorAtributo('lblIdAdmisionAgen');
                    valores_a_mandar = valores_a_mandar + "&lblIdTipoAdmision=" + valorAtributo('lblIdTipoAdmision');
                    valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');
                    valores_a_mandar = valores_a_mandar + "&TipoFolio=" + valorAtributo('cmbTipoDocumento');
                    valores_a_mandar = valores_a_mandar + "&lblIdProfesion=" + document.getElementById('lblIdProfesion').lastChild.nodeValue;
                    valores_a_mandar = valores_a_mandar + "&lblIdAuxiliar=" + valorAtributo('lblIdAuxiliar');
                    valores_a_mandar = valores_a_mandar + "&lblIdCita=" + valorAtributo('lblIdCita');
                } else guardarYtraerDatoAlListado('nuevoDocumentoInterconsulta')
            } else {
                alert('Debe seleccionar un paciente con Admision')
                return;
            }
            break;
        case 'nuevoDocumentoHC02':
            pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

            if (valorAtributo('lblIdTipoAdmision') != '') {
                if (valorAtributo('cmbIdTipoServicio') != 'INT') {
                    valores_a_mandar = 'accion=' + option;
                    valores_a_mandar = valores_a_mandar + "&idQuery=803&parametros=";
                    valores_a_mandar = valores_a_mandar + '&opcionTipo=MI_AGENDA'
                    valores_a_mandar = valores_a_mandar + "&lblIdAdmision=" + valorAtributo('lblIdAdmisionAgen');
                    valores_a_mandar = valores_a_mandar + "&lblIdTipoAdmision=" + valorAtributo('lblIdTipoAdmision');
                    valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');
                    valores_a_mandar = valores_a_mandar + "&TipoFolio=" + valorAtributo('cmbTipoDocumento');
                    valores_a_mandar = valores_a_mandar + "&lblIdProfesion=" + document.getElementById('lblIdProfesion').lastChild.nodeValue;
                    valores_a_mandar = valores_a_mandar + "&lblIdAuxiliar=" + valorAtributo('lblIdAuxiliar');
                    valores_a_mandar = valores_a_mandar + "&lblIdCita=" + valorAtributo('lblIdCita');
                } else guardarYtraerDatoAlListado('nuevoDocumentoInterconsulta')
            } else {
                alert('Debe seleccionar un paciente con Admision')
                return;
            }
            break;

        case 'nuevoDocumentoInterconsulta':
            if (valorAtributo('lblIdTipoAdmision') != '') {
                valores_a_mandar = 'accion=nuevoDocumentoHC'
                valores_a_mandar = valores_a_mandar + '&opcionTipo=INTERCONSULTA'
                valores_a_mandar = valores_a_mandar + "&lblIdPaciente=" + valorAtributo('lblIdPaciente');
                valores_a_mandar = valores_a_mandar + "&TipoFolio=" + valorAtributo('cmbTipoDocumento');
                valores_a_mandar = valores_a_mandar + "&lblIdProfesion=" + document.getElementById('lblIdProfesion').lastChild.nodeValue;
                valores_a_mandar = valores_a_mandar + "&lblIdEspecialidad=" + valorAtributo('lblIdEspecialidad');
                valores_a_mandar = valores_a_mandar + "&lblIdAuxiliar=" + valorAtributo('lblIdAuxiliar');
                opcion = 'nuevoDocumentoHC'
            } else {
                alert('Debe seleccionar un paciente con Admision')
                return;
            }
            break;


        case 'nuevaAdmisionCuenta':
            valores_a_mandar = valores_a_mandar + "&txtNoAutorizacion=" + valorAtributo('txtNoAutorizacion');
            break;


        case 'consultarValoresCuenta':
            /*if(traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == null || traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == ""){
                return
            }*/
            valores_a_mandar = valores_a_mandar + "&idQuery=682";
            valores_a_mandar = valores_a_mandar + "&lblIdFactura=" + valorAtributo("lblIdFactura");
            break;


        case 'verificaCantidadInventario':
            if (valorAtributo('txtCantidad') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;
        case 'verificaCantidadInventarioMinimo':
            if (valorAtributo('txtCantidad') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;
        case 'verificaValorInventario':
            if (valorAtributo('txtValorU') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;
        case 'verificaValorInventarioMinimo':
            if (valorAtributo('txtValorU') != '') {
                valores_a_mandar = valores_a_mandar + "&txtCantidad=" + valorAtributoIdAutoCompletar('txtIdArticulo');
            } else {
                return;
            }
            break;

        case 'nuevoDocumentoInventarioBodegaConsumo':
            if (verificarCamposGuardar('nuevoDocumentoInventarioBodegaConsumo')) {
                valores_a_mandar = valores_a_mandar + '&accion=nuevoDocumentoInventarioProgramacion';
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');
                valores_a_mandar = valores_a_mandar + "&idAdmision=" + valorAtributo('lblIdAdmisionAgen');
                valores_a_mandar = valores_a_mandar + "&sw_origen=" + valorAtributo('cmbIdTipoDevolucion');

            } else { alert('DEBE SELECCIONAR BODEGA Y ADMISION'); return false; }
            break;
        case 'nuevoDocumentoInventarioProgramacion':
            if (valorAtributo('cmbIdBodega') != '') {
                valores_a_mandar = valores_a_mandar + '&accion=nuevoDocumentoInventarioProgramacion';
                valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');
                valores_a_mandar = valores_a_mandar + "&idCita=" + valorAtributo('lblIdAgendaDetalle'); //cita
            } else { alert('SELECCIONE UNA BODEGA '); return false; }
            break;
        case 'crearDocInvBodega':
            if (valorAtributo('cmbIdBodegaDestino') != '') {
                if (valorAtributo('cmbIdBodega') != valorAtributo('cmbIdBodegaDestino')) {
                    valores_a_mandar = ""
                    valores_a_mandar = valores_a_mandar + '&accion=crearDocInvBodega';
                    valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                    valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');
                } else { alert('NO PUEDEN SER ORIGEN Y DESTINO LA MISMA BODEGA'); return false; }
            } else { alert('SELECCIONE BODEGA DESTINO'); return false; }
            break;
        case 'existeDocumentoInventarioBodega':
            if (valorAtributo('cmbIdBodega') != '') {
                if (valorAtributo('cmbIdTipoDocumento') != '') {
                    if (verificarCamposGuardar('nuevoDocumentoInventarioBodega')) {
                        valores_a_mandar = valores_a_mandar + '&accion=existeDocumentoInventarioBodega';

                        valores_a_mandar = valores_a_mandar + "&txtNumero=" + valorAtributo('txtNumero');
                        valores_a_mandar = valores_a_mandar + "&txtIdTercero=" + valorAtributoIdAutoCompletar('txtIdTercero');

                    } else { return false; }
                } else { alert('SELECCIONE EL TIPO DE DOCUMENTO'); return false; }
            } else { alert('SELECCIONE UNA BODEGA'); return false; }
            break;
        case 'nuevoDocumentoInventarioBodega':
            if (valorAtributo('cmbIdBodega') != '') {
                if (valorAtributo('cmbIdTipoDocumento') != '') {
                    if (verificarCamposGuardar('nuevoDocumentoInventarioBodega')) {
                        valores_a_mandar = valores_a_mandar + '&accion=nuevoDocumentoInventarioBodega';
                        valores_a_mandar = valores_a_mandar + "&cmbIdTipoDocumento=" + valorAtributo('cmbIdTipoDocumento');
                        valores_a_mandar = valores_a_mandar + "&idBodega=" + valorAtributo('cmbIdBodega');

                        valores_a_mandar = valores_a_mandar + "&txtObservacion=" + valorAtributo('txtObservacion');
                        valores_a_mandar = valores_a_mandar + "&txtNumero=" + valorAtributo('txtNumero');
                        valores_a_mandar = valores_a_mandar + "&txtIdTercero=" + valorAtributoIdAutoCompletar('txtIdTercero');
                        valores_a_mandar = valores_a_mandar + "&txtFechaDocumento=" + valorAtributo('txtFechaDocumento');
                        valores_a_mandar = valores_a_mandar + "&txtValorFlete=" + valorAtributo('txtValorFlete');

                    } else { return false; }
                } else { alert('SELECCIONE EL TIPO DE DOCUMENTO'); return false; }
            } else { alert('SELECCIONE UNA BODEGA'); return false; }
            break;

        case 'ocuparCandadoReporte':
            alert('ocuparCandadoReporte')
            break;
        case 'liberarCandadoReporte':
            alert('liberarCandadoReporte')
            break;

        case 'agregarTurno':
            if (verificarCamposGuardar('agregarTurno')) {

                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + valorAtributo('cmbTurno');
            } else { return false; }
            break;

        case 'eliminarTurno':
            if (verificarCamposGuardar('eliminarTurno')) {
                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + "0";
            } else { return false; }
            break;

        case 'llamarTurno':
            if (verificarCamposGuardar('llamarTurno')) {
                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + "0";
            } else { return false; }
            break;

        case 'modificarTurno':
            if (verificarCamposGuardar('modificarTurno')) {
                valores_a_mandar = valores_a_mandar + "&txtIdTurno=" + valorAtributo('lblIdPacienteParlante') + '_' + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblPacienteParlante=" + valorAtributo('lblPacienteParlante');
                valores_a_mandar = valores_a_mandar + "&lblConsultorioParlante=" + valorAtributo('lblConsultorioParlante');
                valores_a_mandar = valores_a_mandar + "&lblEstadoParlante=" + valorAtributo('lblEstadoParlante');
                valores_a_mandar = valores_a_mandar + "&cmbTurno=" + "0";

            } else { return false; }
            break;


        /*  case 'nuevoDocumentoInventario':  
       if( valorAtributo('cmbUbicacion')!='' && valorAtributo('cmbConcepto')!='' &&  valorAtributo('txtFechaConsumo')!=''  ){
          if(valorAtributo('cmbConcepto')=='UD'){ 				   
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':00'; /* no cambiar el 00 en punto
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
          }
          else{alert('El concepto debe ser UNIDOSIS'); return false;}
       }
       else{ alert('Debe seleccionar Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;	

  case 'nuevoDocumentoInventarioIndividual':  
  
       if( valorAtributo('cmbUbicacion')!=''  &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtFechaConsumo2')!='' &&  valorAtributoIdAutoCompletar('txtBusIdPaciente')!=''  ){
          if(valorAtributo('cmbConcepto')=='SP'){ 
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':59';
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
               valores_a_mandar=valores_a_mandar+"&txtBusIdPaciente="+valorAtributoIdAutoCompletar('txtBusIdPaciente');	
          }
        else  alert('El concepto debe ser SP-Despacho Individual')

       }
       else{ alert('Debe seleccionar PACIENTE Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;	
  case 'nuevoDocumentoInventarioVacio':  
  
       if( valorAtributo('cmbUbicacion')!=''  &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtFechaConsumo2')!='' &&  valorAtributoIdAutoCompletar('txtBusIdPaciente')!=''  ){
          if(valorAtributo('cmbConcepto')=='SP'){ 
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':59';
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
               valores_a_mandar=valores_a_mandar+"&txtBusIdPaciente="+valorAtributoIdAutoCompletar('txtBusIdPaciente');	
          }
        else  alert('El concepto debe ser SP-Despacho Individual')

       }
       else{ alert('Debe seleccionar PACIENTE Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;		  
  case 'nuevoDocumentoInventarioIndividualStock':  
  
       if( valorAtributo('cmbUbicacion')!=''  &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtFechaConsumo2')!='' &&  valorAtributoIdAutoCompletar('txtBusIdPaciente')!=''  ){
          if(valorAtributo('cmbConcepto')=='CS'){ 
               limpiaAtributo('lblIdDocumento');
               valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
               valores_a_mandar=valores_a_mandar+"&cmbIdBodega="+valorAtributo('cmbIdBodegaStock');					   
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo')+' '+valorAtributo('cmbHoraConsumoDesde')+':00';
               valores_a_mandar=valores_a_mandar+"&txtFechaConsumo2="+valorAtributo('txtFechaConsumo2')+' '+valorAtributo('cmbHoraConsumoHasta')+':59';
               valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('cmbUbicacion');	
               valores_a_mandar=valores_a_mandar+"&txtBusIdPaciente="+valorAtributoIdAutoCompletar('txtBusIdPaciente');	
               valores_a_mandar=valores_a_mandar+"&cmbIdInmediato="+valorAtributo('cmbIdInmediato');						   
          }
        else  alert('El concepto debe ser CS-Despacho Stock')

       }
       else{ alert('Debe seleccionar PACIENTE Unidad, Concepto y Fecha de Consumo')
           return;
       }			   
  break;	
                  
          
  case 'dePendienteAAdministrado':  
                 valores_a_mandar=valores_a_mandar+"&lblIdTnsFarmacia="+valorAtributo('lblIdTnsFarmacia');		  
                 valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
           valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');	
           valores_a_mandar=valores_a_mandar+"&txtProyectable="+valorAtributo('txtProyectable');					   
  break; 			
  case 'deAdministradoAPendiente':  
                 valores_a_mandar=valores_a_mandar+"&lblIdTnsFarmacia="+valorAtributo('lblIdTnsFarmacia');		  		  
                 valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
           valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');	
           valores_a_mandar=valores_a_mandar+"&txtProyectable="+valorAtributo('txtProyectable');					   				   
  break;		  
  case 'dePendienteAAdministradoNovedad':  
           if(verificarCamposGuardar(opcion)){
                   valores_a_mandar=valores_a_mandar+"&lblIdTnsFarmacia="+valorAtributo('lblIdTnsFarmacia');		  					   
             valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
             valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');
             valores_a_mandar=valores_a_mandar+"&cmbIdConcepto="+valorAtributo('cmbIdConcepto');					   				   				   
             valores_a_mandar=valores_a_mandar+"&cmbIdCantidadNovedad="+valorAtributo('cmbIdCantidadNovedad');		
             valores_a_mandar=valores_a_mandar+"&txtObservacionNovedad="+valorAtributo('txtObservacionNovedad');
             valores_a_mandar=valores_a_mandar+"&txtProyectable="+valorAtributo('txtProyectable');					   				   					 					 						   
           }else  return false;
  break;	
  case 'deDevolucion':  
           if(verificarCamposGuardar(opcion)){
             valores_a_mandar=valores_a_mandar+"&lblIdOrden="+valorAtributo('lblIdOrden');
             valores_a_mandar=valores_a_mandar+"&lblFechaProgramada="+valorAtributo('lblFechaProgramada');		
             valores_a_mandar=valores_a_mandar+"&txtIdHora="+valorAtributo('txtIdHora');
             valores_a_mandar=valores_a_mandar+"&cmbIdConcepto="+valorAtributo('cmbIdConcepto');					   				   				   
             valores_a_mandar=valores_a_mandar+"&cmbIdCantidadNovedad="+valorAtributo('cmbIdCantidadNovedad');		
             valores_a_mandar=valores_a_mandar+"&txtObservacionNovedad="+valorAtributo('txtObservacionNovedad');						   
           }else  return false;
  break;	
  case 'cambioDeBodega':  
           if(verificarCamposGuardar(opcion)){
             valores_a_mandar=valores_a_mandar+"&lblIdTransaccion="+valorAtributo('lblIdTransaccion');
             valores_a_mandar=valores_a_mandar+"&cmbIdBodega="+valorAtributo('cmbIdBodega');					 
          alert('cambioDeBodega= '+valores_a_mandar);
           }else  return false;
  break;		  
  case 'dejarEnPendiente':  
           if(verificarCamposGuardar(opcion)){
             valores_a_mandar=valores_a_mandar+"&lblIdTransaccion="+valorAtributo('lblIdTransaccion');
//				  alert('dejarEnPendiente= '+valores_a_mandar);
           }else  return false;
  break;
  */

        /***************************************************************************** FIN   HISTORIA CLINICA *************************************************/
        /*	  case 'listTransaccionBodega':  alert('cmbIdPresentacion= '+valorAtributo('cmbIdPresentacion'))
       if( valorAtributo('txtIdArticulo')!='' && valorAtributo('cmbIdConcepto')!='' &&  valorAtributo('txtCantidad')!='' &&  valorAtributo('txtValor')!='' &&  valorAtributo('lblIdDocumento')!='' &&  valorAtributo('cmbIdPresentacion')!=''  ){
//				   limpiaAtributo('lblIdDocumento');
                 valores_a_mandar=valores_a_mandar+"&cmbIdConcepto="+valorAtributo('cmbIdConcepto');		
                 valores_a_mandar=valores_a_mandar+"&lblIdDocumento="+valorAtributo('lblIdDocumento');
                 valores_a_mandar=valores_a_mandar+"&IdBodega="+valorAtributo('lblId');						   
                 valores_a_mandar=valores_a_mandar+"&txtIdArticulo="+valorAtributoIdAutoCompletar('txtIdArticulo');
           valores_a_mandar=valores_a_mandar+"&txtCantidad="+valorAtributo('txtCantidad');	
           valores_a_mandar=valores_a_mandar+"&cmbIdPresentacion="+valorAtributo('cmbIdPresentacion');					   
           valores_a_mandar=valores_a_mandar+"&txtValor="+valorAtributo('txtValor');					   
       }
       else{ alert('Debe seleccionar Documento Art�culo, Concepto, Cantidad, Presentaci�n y Valor')
           return;
       }			   
  break;
/* case 'nuevoDocumentoInventarioDevoluciones':  

               if( valorAtributo('cmbConcepto')!='' && valorAtributo('cmbCentroCostoSolicita')!='' && valorAtributo('txtBusFechaConsumo')!='' ){
                   limpiaAtributo('lblIdDocumento');
                   valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
                   valores_a_mandar=valores_a_mandar+"&txtIdCentroCosto="+valorAtributo('cmbCentroCostoSolicita');	
                   valores_a_mandar=valores_a_mandar+"&txtBusFechaConsumo="+valorAtributo('txtBusFechaConsumo');							   
               }
               else{ alert('Debe seleccionar  Concepto y Centro de Costos')
                   return;
               }					 
       
  break;
  case 'nuevoDocumentoInventarioEntrada':  

               if( valorAtributo('txtIdCentroCosto')!='' && valorAtributo('cmbConcepto')!='' &&  valorAtributo('txtFechaConsumo')!='' &&  valorAtributo('txtIdProveedor')!='' &&  valorAtributo('cmbIdTipoReferencia')!='' &&  valorAtributo('txtNoReferencia')!=''){
                   limpiaAtributo('lblIdDocumento');
                   valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
                   valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo');		
                   valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('txtIdCentroCosto');	

                   valores_a_mandar=valores_a_mandar+"&txtIdProveedor="+valorAtributoIdAutoCompletar('txtIdProveedor');	
                   valores_a_mandar=valores_a_mandar+"&txtFechaReferencia="+valorAtributo('txtFechaReferencia');	
                   valores_a_mandar=valores_a_mandar+"&cmbIdTipoReferencia="+valorAtributo('cmbIdTipoReferencia');							   
                   valores_a_mandar=valores_a_mandar+"&txtNoReferencia="+valorAtributo('txtNoReferencia');	
                   valores_a_mandar=valores_a_mandar+"&txtObservacionReferencia="+valorAtributo('txtObservacionReferencia');							   
               
               }
               else{ alert('Debe seleccionar  Concepto y Datos completos de Referencia')
                   return;
               }	
  break;	
  
  case 'nuevoDocumentoInventarioTraslado':  

               if( valorAtributo('txtIdCentroCosto')!='' &&   valorAtributo('txtCantidad')!='' ){
                   limpiaAtributo('lblIdDocumento');
                   valores_a_mandar=valores_a_mandar+"&cmbConcepto="+valorAtributo('cmbConcepto');
                   valores_a_mandar=valores_a_mandar+"&idBodegaOrigen="+valorAtributo('lblId');						   						   						   					   
                   valores_a_mandar=valores_a_mandar+"&lblIdArticulo="+valorAtributo('lblIdArticulo');
                   valores_a_mandar=valores_a_mandar+"&txtCantidad="+valorAtributo('txtCantidad');	
                   valores_a_mandar=valores_a_mandar+"&cmbBodegaDestino="+valorAtributo('cmbBodegaDestino');							   					   
                   

                   
                   valores_a_mandar=valores_a_mandar+"&txtFechaConsumo="+valorAtributo('txtFechaConsumo');		
                   valores_a_mandar=valores_a_mandar+"&cmbUbicacion="+valorAtributo('txtIdCentroCosto');	

               }
               else{ alert('Debe seleccionar Bodega Origen y Cantidad')
                   return;
               }	
  break;	*/

    }

    $.ajax({
        url: "/clinica/paginas/accionesXml/guardarHc_xml.jsp",
        type: "POST",
        data: valores_a_mandar,
        contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
        beforeSend: function () { },
        success: function (raiz) {
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {

                    switch (opcion) {
                        case 'validaNumeracionFactura':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {

                                var noticia = raiz.getElementsByTagName('NOTICIA')[0].firstChild.data;

                                if (noticia != 'PERMITE_NUMERAR') {
                                    alert('1. ATENCION FACTURADOR:\n\n' + noticia);
                                }


                                console.log('Ajuste: ' + raiz.getElementsByTagName('estadoAjuste')[0].firstChild.data)
                                switch (raiz.getElementsByTagName('estadoAjuste')[0].firstChild.data) {

                                    case 'NUMERAR':
                                        //modificarCRUD('actualizarFacturaGenerada');
                                        break;

                                    case 'MAXIMO_CUENTA':
                                        modificarCRUD('ajustarMaximoCuenta')
                                        break;

                                    case 'MAXIMO_ANUAL':
                                        modificarCRUD('ajustarCuentaAnual')
                                        break;

                                }

                                setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')", 200);
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }

                            break;


                        case 'enviarSF':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listaPanelMedicamentos');
                            }
                            break;

                        case 'consultarSF':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listaPanelMedicamentos');
                            }
                            break;

                        case 'auditarRecibo':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                limpiarDivEditarJuan('auditarRecibo');
                                buscarFacturacion('listGrillaRecibos');
                            }
                            break;

                        case 'auditarFactura':

                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                limpiarDivEditarJuan('auditarFactura');
                                buscarFacturacion('listGrillaFacturas');
                            }


                            break;

                        case 'anularFacturaWS':

                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarFacturacion('listGrillaFacturas');
                                alert('FACTURA ANULADA EXISTOSAMENTE');
                            }

                            break;

                        case 'cerrarDocumentoProgramacion':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()																
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodegaProgramacion');
                                /* limpiar campos de las transacciones */

                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'cerrarDocumentoSolicitudesInventario':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()																
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodegaConsumo');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'modificarTransaccionDocumento':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                ocultar('divVentanitaModTransaccion')
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'eliminarTransaccionDocumento':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                ocultar('divVentanitaModTransaccion')
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'verificaTransaccionesInventario':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()								
                                /*CAMBIAR EL LABEL DEL ESTADO A 1*/
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodega');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;

                        case 'verificaTransaccionesInventarioTrasladoBodega':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()								
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosTrasladoBodega');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;



                        case 'consultaExamenFisico':
                            asignaAtributo('txtExamenFisicoDescripcion', raiz.getElementsByTagName('examenFisicoDescripcion')[0].firstChild.data, 0);
                            break;


                        case 'elementosDelFolio':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                imprimirHCPdf_paraFinalizar()
                                //cerrarDocumentClinicoSinImp()
                            } else {
                                alert('ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'buscarSiEsNoPos':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                //codigos = ['7527', '7528', '7529', '7530', '7531', '7532', '7533', '7534', '7535', '7536', '7537','43053']
                                //codigo = valorAtributoIdAutoCompletar('txtIdArticulo')
                                //if (codigos.includes(codigo)) {
                                alert('Por favor Diligenciar en MIPRES ')
                                modificarCRUD('listMedicacion');
                                //return false;
                                //}

                                //if (verificarCamposGuardar('listMedicacion')) {
                                //mostrar('divVentanitaMedicamentosNoPos')
                                //cargarTableVistaPreviaNoPos('noPos');
                                //}
                            } else {
                                modificarCRUD('listMedicacion');
                            }
                            break;

                        case 'buscarSiEsNoPosInsumos':
                            modificarCRUD('listInsumos');
                            break;

                        case 'buscarSiEsNoPosProcedim':
                            if (verificarCamposGuardar('listProcedimientosDetalle')) {
                                // verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups')
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listProcedimientosDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listProcedimientosDetalle');
                            }*/
                            break;

                        case 'buscarSiEsNoPosProcedimientoNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listProcedimientosDetalle');
                            }

                            break

                        case 'buscarSiEsNoPosAyudaDiagnostica':
                            if (verificarCamposGuardar('listAyudasDiagnosticasDetalle')) {
                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoAyudaDiagnostica');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups2');
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listAyudasDiagnosticasDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listAyudasDiagnosticasDetalle');
                            }*/

                            break;

                        case 'buscarSiEsNoPosAyudaDiagnosticaNotificacion':

                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {

                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');

                            } else {
                                modificarCRUD('listAyudasDiagnosticasDetalle');
                            }
                            break;


                        case 'buscarSiEsNoPosLaboratorio':

                            if (verificarCamposGuardar('listLaboratoriosDetalle')) {
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups4');

                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoLaboratorio');
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listLaboratoriosDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listLaboratoriosDetalle');
                            }*/
                            break;


                        case 'buscarSiEsNoPosLaboratorioNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listLaboratoriosDetalle');
                            }
                            break;


                        case 'buscarSiEsNoPosTerapia':


                            if (verificarCamposGuardar('listTerapiaDetalle')) {
                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoTerapia');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups3')

                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listTerapiaDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listTerapiaDetalle');
                            }*/
                            break;


                        case 'buscarSiEsNoPosTerapiaNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listTerapiaDetalle');
                            }
                            break;


                        case 'buscarPendienteLE':

                            //alert('.111..'+raiz.getElementsByTagName('existe')[0].firstChild.data)

                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK')
                                        modificarCRUD('crearCitaCirugia')
                                    else modificarCRUD('crearCita');
                                } else {
                                    if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'S') {
                                        alert('PACIENTE YA TIENE UNA CITA O LISTA DE ESPERA DE ESTA ESPECIALIDAD Y SUB ESPECIALIDAD !!!')
                                    } else if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'O') { /*CITA OCUPADA*/
                                        alert('1. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    }

                                }
                            }
                            break;
                        case 'consultarDisponibleDesdeAgenda':

                            switch (raiz.getElementsByTagName('existe')[0].firstChild.data) {
                                case 'C':
                                    alert('1-. PACIENTE YA TIENE CITA A FUTURO')
                                    break;
                                case 'S':
                                    alert('2-. PACIENTE TIENE LISTA DE ESPERA A FUTURO')
                                    break;
                                case 'O':
                                    alert('3-. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    break;
                                case 'N':
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                        verificarNotificacionAntesDeGuardar('crearCitaCirugia')
                                        //modificarCRUD('crearCitaCirugia')
                                    } else {
                                        verificarNotificacionAntesDeGuardar('crearCita')
                                        //modificarCRUD('crearCita')
                                    }


                                    break;
                            }
                            break;

                        case 'buscarPendienteLE_LE':
                            switch (raiz.getElementsByTagName('existe')[0].firstChild.data) {
                                case 'C':
                                    alert('1-. PACIENTE YA TIENE CITA A FUTURO')
                                    break;
                                case 'S':
                                    alert('2-. PACIENTE TIENE LISTA DE ESPERA A FUTURO')
                                    break;
                                case 'O':
                                    alert('3-. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    break;
                                case 'N':
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                        if (valorAtributo('txt_banderaOpcionCirugiaGestion') == 'OKGESTIONPROC') {
                                            modificarCRUD('crearListaEsperaCirugiaProgramacion')
                                        } else {
                                            modificarCRUD('crearListaEsperaCirugia')
                                        }
                                    } else if (valorAtributo('txt_banderaOpcionCirugia') == 'NO') {
                                        modificarCRUD('crearListaEspera');
                                    }
                                    break;
                            }
                            break;

                        case 'buscarIdentificacionPaciente':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    alert('PACIENTE NO EXISTE !!! \n DEBE USTED BUSCAR POR APELLIDOS SEGUIDO DE NOMBRES, O CON DIFERENTE TIPO DE IDENTIFICACION EN LA SIGUIENTE VENTANA ')
                                    traerVentanitaPaciente('txtIdBusPaciente', '', 'divParaVentanita', 'txtIdBusPaciente', '27')

                                } else {
                                    mostrar('divVentanitaPacienteYaExiste')
                                    asignaAtributo('lblIdPacienteExiste', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('lblLetrero', ' PACIENTE  YA EXISTE  !!!', 0)
                                    asignaAtributo("cmbIdEspecialidadCitasPaciente", valorAtributo("cmbIdEspecialidad"), 0)
                                    setTimeout(() => {
                                        buscarAGENDA("listaCitasPaciente")
                                        setTimeout(() => {
                                            buscarAGENDA('listHistoricoListaEsperaEnAgenda')
                                        }, 400);
                                    }, 200);
                                    setTimeout(() => {
                                        switch (ventanaActual.opc) {
                                            case "agenda":
                                                verificarNotificacionAntesDeGuardar("verificarCitasAFuturoPaciente")
                                                break;

                                            case "admisiones":
                                                $('#listAdmisionCuenta').jqGrid('clearGridData')
                                                $('#listCitaCirugiaProcedimiento').jqGrid('clearGridData')
                                                $('#listArticulosDeFactura').jqGrid('clearGridData')
                                                $('#listProcedimientosDeFactura').jqGrid('clearGridData')
                                                setTimeout(() => {
                                                    buscarAGENDA('listAdmisionCuenta');
                                                }, 100);
                                                break;

                                            case "listaEspera":

                                                asignaAtributo("txtAdministradora1", decodeURI(valorAtributo("txtAdministradoraPaciente")), 0)
                                                buscarAGENDA("listHistoricoListaEspera");
                                                buscarAGENDA("listDocumentosHistoricos");
                                                buscarInformacionBasicaPaciente()
                                                break;

                                            case "principalHC":
                                                contenedorBuscarPaciente();
                                                break;

                                            case 'modificarHistoria':
                                                var datoscadena = raiz.getElementsByTagName('dato')[0].firstChild.data;
                                                datoscadena = datoscadena.split('-');
                                                var identificacion = datoscadena[1].split(" ")
                                                asignaAtributo("lblIdPaciente", datoscadena[0], 0)
                                                asignaAtributo("lblIdentificacionPaciente", identificacion[0], 0)
                                                asignaAtributo("lblNombrePaciente", identificacion[1] + " " + identificacion[2] + " " + identificacion[3] + " " + identificacion[4])
                                                edadPacienteModificarHistoria()
                                                setTimeout(buscarHC('listDocumentosHistoricos'), 200)
                                                break;

                                            default:
                                                break;
                                        }
                                    }, 600);
                                    //document.getElementById('divContenidoListaEsperaHist').innerHTML = '<table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>';
                                }
                            }
                            break;

                        case 'buscarIdPacienteEncuesta':

                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {

                                    asignaAtributo("lblTipoIdCrearPacienteURG", valorAtributo("cmbTipoIdUrgencias"), 1)
                                    asignaAtributo("lblIdCrearPacienteURG", valorAtributo("txtIdentificacionUrgencias"), 1)
                                    if (valorAtributo("cmbTipoIdUrgencias") == "MS") {
                                        calendario("txtFechaNacimientoUrgencias", 1)
                                    } else if (valorAtributo("cmbTipoIdUrgencias") == "AS") {
                                        fecha = new Date();
                                        fecha.setFullYear(fecha.getFullYear() - 18)
                                        a = fecha.toISOString().slice(0, 10)
                                        asignaAtributo("txtFechaNacimientoUrgencias", a, 0)
                                    }
                                    limpiaAtributo("txtNombre1Urgencias", 0)
                                    limpiaAtributo("txtNombre2Urgencias", 0)
                                    limpiaAtributo("txtApellido1Urgencias", 0)
                                    limpiaAtributo("txtApellido2Urgencias", 0)

                                    asignaAtributo("cmbSexoUrgencias", "O", 0)
                                    limpiaAtributo("txtFechaNacimientoUrgencias", 0)
                                    limpiaAtributo("txtTelefonoUrgencias", 0)
                                    limpiaAtributo("txtCelular1Urgencias", 0)
                                    limpiaAtributo("txtCelular2Urgencias", 0)
                                    asignaAtributo("txtAdministradoraUrgencias", "65-1055 PARTICULAR ADSCRITO", 0)

                                    asignaAtributo("txtMunicipioResidenciaUrgencias", "52001-PASTO", 0)
                                    limpiaAtributo("txtDireccionUrgencias", 0)
                                    limpiaAtributo("txtEmailUrgencias", 0)

                                    mostrar("divCrearPacienteUrgencias")
                                } else {
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    setTimeout(() => {
                                        buscarInformacionBasicaPaciente()
                                        setTimeout(() => {
                                            cargarInformacionContactoPaciente()
                                            $("#sw_nuevo_paciente").removeAttr("checked");
                                            ocultar("elementosNuevoPaciente")
                                            limpiaAtributo("cmbTipoIdUrgencias", 0)
                                            limpiaAtributo("txtIdentificacionUrgencias", 0)
                                        }, 200);
                                    }, 200);
                                }
                            }
                            break;

                        case 'buscarIdentificacionPacienteUrgencias':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    if (confirm("PACIENTE NO ENCONTRADO \n DESEA CREAR NUEVO PACIENTE CON NUMERO DE DOCUMENTO \n " + valorAtributo("cmbTipoIdUrgencias") + valorAtributo("txtIdentificacionUrgencias") + "?")) {
                                        asignaAtributo("lblTipoIdCrearPacienteURG", valorAtributo("cmbTipoIdUrgencias"), 1)
                                        asignaAtributo("lblIdCrearPacienteURG", valorAtributo("txtIdentificacionUrgencias"), 1)
                                        if (valorAtributo("cmbTipoIdUrgencias") == "MS") {
                                            calendario("txtFechaNacimientoUrgencias", 1)
                                        } else if (valorAtributo("cmbTipoIdUrgencias") == "AS") {
                                            fecha = new Date();
                                            fecha.setFullYear(fecha.getFullYear() - 18)
                                            a = fecha.toISOString().slice(0, 10)
                                            asignaAtributo("txtFechaNacimientoUrgencias", a, 0)
                                        }
                                        limpiaAtributo("txtNombre1Urgencias", 0)
                                        limpiaAtributo("txtNombre2Urgencias", 0)
                                        limpiaAtributo("txtApellido1Urgencias", 0)
                                        limpiaAtributo("txtApellido2Urgencias", 0)

                                        asignaAtributo("cmbSexoUrgencias", "O", 0)
                                        limpiaAtributo("txtFechaNacimientoUrgencias", 0)
                                        limpiaAtributo("txtTelefonoUrgencias", 0)
                                        limpiaAtributo("txtCelular1Urgencias", 0)
                                        limpiaAtributo("txtCelular2Urgencias", 0)
                                        asignaAtributo("txtAdministradoraUrgencias", "65-1055 PARTICULAR ADSCRITO", 0)

                                        asignaAtributo("txtMunicipioResidenciaUrgencias", "52001-PASTO", 0)
                                        limpiaAtributo("txtDireccionUrgencias", 0)
                                        limpiaAtributo("txtEmailUrgencias", 0)

                                        mostrar("divCrearPacienteUrgencias")
                                    }
                                } else {
                                    asignaAtributo('txtIdBusPacienteUrgencias', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('lblIdPacienteUrgencias', raiz.getElementsByTagName('dato')[0].firstChild.data.split("-")[0], 0)
                                }
                            }
                            break;

                        case 'nuevoPaciente':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N')
                                    alert(raiz.getElementsByTagName('dato')[0].firstChild.data + '\n \n                           CREADO SATISFACTORIAMENTE  !!!')
                                else alert(raiz.getElementsByTagName('dato')[0].firstChild.data + '\n \n                           YA EXISTE  !!!')
                                asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                            }
                            break;

                        case 'nuevoDocumentoHC':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') {
                                asignaAtributo('lblIdDocumento', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdAdmision', raiz.getElementsByTagName('id_admision')[0].firstChild.data, 1);

                                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');

                                setTimeout("llenarTablaAlAcordion(valorAtributo('lblIdDocumento'), valorAtributo('cmbTipoDocumento'), valorAtributoCombo('cmbTipoDocumento') ) ", 500)

                                mostrar('divContenidos')
                                asignaAtributo('lblTipoDocumento', valorAtributo('cmbTipoDocumento'), 0)
                                asignaAtributo('lblDescripcionTipoDocumento', valorAtributoCombo('cmbTipoDocumento'), 0)
                                asignaAtributo('txtIdEsdadoDocumento', '0', 0)
                                asignaAtributo('lblNomEstadoDocumento', 'Abierto', 0)

                                habilitar('btn_cerrarDocumentoHC', 1)
                                habilitar('btnEliminarMedicamentoListado', 1)
                                habilitar('btnEliminarProcedimientos', 1)
                                habilitar('btnProcedimiento', 1)
                            }
                            break;
                        case 'nuevaAdmisionCuenta':
                            if (raiz.getElementsByTagName('no_autorizacion')[0].firstChild.data != '') {
                                alert('NUMERO DE AUTORIZACION YA EXISTE')
                            }
                            break;

                        case 'consultarValoresCuenta':
                            if (raiz.getElementsByTagName('id_factura')[0].firstChild.data == null || raiz.getElementsByTagName('id_factura')[0].firstChild.data == "") {
                                return;
                            }
                            asignaAtributo('lblValorNoCubierto', raiz.getElementsByTagName('valor_nocubierto')[0].firstChild.data, 0);
                            asignaAtributo('lblValorCubierto', raiz.getElementsByTagName('valor_cubierto')[0].firstChild.data, 0);
                            asignaAtributo('lblDescuento', raiz.getElementsByTagName('valor_descuento')[0].firstChild.data, 0);
                            asignaAtributo('lblTotalFactura', raiz.getElementsByTagName('total_factura')[0].firstChild.data, 0);
                            asignaAtributo('lblIdEstadoFactura', raiz.getElementsByTagName('id_estado')[0].firstChild.data, 0);
                            asignaAtributo('lblEstadoFactura', raiz.getElementsByTagName('estado')[0].firstChild.data, 0);
                            asignaAtributo('lblIdFactura', raiz.getElementsByTagName('id_factura')[0].firstChild.data, 0);
                            asignaAtributo('lblNumeroFactura', raiz.getElementsByTagName('numero_factura')[0].firstChild.data, 0);

                            /*if (raiz.getElementsByTagName('medio_pago')[0].firstChild.data == '1') {
                                $("#efectivo").attr('checked', true);
                            } else if (raiz.getElementsByTagName('medio_pago')[0].firstChild.data == '2') {
                                $("#tarjeta").attr('checked', true);
                            } else {
                                alert("EL MEDIO DE PAGO NO SE PUEDE MOSTRAR")
                            }*/

                            if (raiz.getElementsByTagName('forma_pago')[0].firstChild.data == '1') {
                                $("#contado").attr('checked', true);
                            } else if (raiz.getElementsByTagName('forma_pago')[0].firstChild.data == '2') {
                                $("#credito").attr('checked', true);
                            } else {
                                alert("LA FORMA DE PAGO NO SE PUEDE MOSTRAR")
                            }

                            if (raiz.getElementsByTagName('id_estado')[0].firstChild.data != 'P') {
                                $("input:radio[name=medioPago]").each(function () {
                                    $(this).attr('disabled', 'disabled')
                                });
                                $("input:radio[name=formaPago]").each(function () {
                                    $(this).attr('disabled', 'disabled')
                                });
                            } else {
                                if (raiz.getElementsByTagName('prefijo_factura')[0].firstChild.data == 'FS') {
                                    $("input:radio[name=medioPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                    $("input:radio[name=formaPago]").each(function () {
                                        $(this).attr('disabled', 'disabled')
                                    });
                                } else {
                                    $("input:radio[name=medioPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                    $("input:radio[name=formaPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                }
                            }
                            break;

                        case 'verificaCantidadInventario':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var cantidad = parseInt(valorAtributo('txtCantidad'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (cantidad > cantMax) {
                                    if (!confirm('LA MAXIMA CANTIDAD QUE SE HA INGRESADO HISTORICAMENTE ES : ' + cantMax + '\n Desea continuar ?')) {
                                        asignaAtributo('txtCantidad', '', 0);
                                    }
                                } else guardarYtraerDatoAlListado('verificaCantidadInventarioMinimo')
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaCantidadInventarioMinimo':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var cantidad = parseInt(valorAtributo('txtCantidad'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);
                                if (cantidad < cantMax) {
                                    if (!confirm('LA MINIMA CANTIDAD QUE SE HA INGRESADO HISTORICAMENTE ES : ' + cantMax + '\n Desea continuar ?')) {
                                        asignaAtributo('txtCantidad', '', 0);
                                    }
                                }
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaValorInventario':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var txtValorU = parseInt(valorAtributo('txtValorU'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (txtValorU > cantMax) {
                                    if (!confirm('EL MAXIMO VALOR UNITARIO QUE SE HA INGRESADO HISTORICAMENTE ES = ' + raiz.getElementsByTagName('cantidad')[0].firstChild.data + '\n Desea continuar ?')) {
                                        asignaAtributo('txtValorU', '', 0);
                                    }
                                } else guardarYtraerDatoAlListado('verificaValorInventarioMinimo')
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaValorInventarioMinimo':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var txtValorU = parseInt(valorAtributo('txtValorU'));
                                var cant = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (txtValorU < cant) {
                                    if (!confirm('EL MINIMO VALOR UNITARIO QUE SE HA INGRESADO HISTORICAMENTE ES = ' + raiz.getElementsByTagName('cantidad')[0].firstChild.data + '\n Desea continuar ?')) {
                                        asignaAtributo('txtValorU', '', 0);
                                    }
                                }
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'nuevoDocumentoDespachoSolicitud':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                alert('despacho de solicitud');
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodega')
                            }
                            break;
                        case 'nuevoDocumentoInventarioBodegaConsumo':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto							  
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodegaConsumo');
                                /* cargar elementos de bodega de consumo en hoja de gasto */
                                if (valorAtributo('cmbIdTipoDocumento') == '21') {
                                    if (valorAtributo('cmbIdBodega') != '') {
                                        setTimeout("buscarSuministros('listGrillaElementosBodegaConsumo')", 400);
                                    }
                                }
                            }
                            break;
                        case 'buscarConciliacion':
                            if (raiz.getElementsByTagName('dato1')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('cmbExiste', raiz.getElementsByTagName('dato1')[0].firstChild.data, 0);
                                asignaAtributo('txtConciliacionDescripcion', raiz.getElementsByTagName('dato2')[0].firstChild.data, 0);
                                buscarHC('listAntFarmacologicos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                                setTimeout("buscarHC('listMedicacionOrdenada' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);


                            }
                            break;
                        case 'nuevoDocumentoInventarioProgramacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodegaProgramacion')
                            }
                            break;
                        case 'crearDocInvBodega':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosTrasladoBodega')
                            }
                            break;
                        case 'existeDocumentoInventarioBodega':
                            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto							  

                                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != 'true') {
                                    alert("EL NUMERO DE FACTURA CON IGUAL TERCERO YA EXISTE !!! ")
                                } else {
                                    guardarYtraerDatoAlListado('nuevoDocumentoInventarioBodega');
                                }
                            } else {
                                alert("Ya se encuentra registrado un documento con igual numero de factura y proveedor ")
                            }
                            break;
                        case 'nuevoDocumentoInventarioBodega':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodega')
                                /* cargar transacciones del documento */
                                limpiarDivEditarJuan('limpCamposTransaccion')
                                if (valorAtributo('txtNaturaleza') == 'I') {
                                    setTimeout("buscarSuministros('listGrillaTransaccionDocumento')", 500);
                                } else {
                                    setTimeout("buscarSuministros('listGrillaTransaccionSalida')", 500);
                                }
                            }
                            break;
                        case 'ocuparCandadoReporte':
                            vanOcuparCandadoReporte = raiz.getElementsByTagName('dato')[0].firstChild.data;
                            alert('respueta=' + vanOcuparCandadoReporte)
                            return vanOcuparCandadoReporte;
                            break;


                        case 'agregarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('agregarTurno');
                            }
                            break;

                        case 'eliminarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('eliminarTurno');
                            }
                            break;

                        case 'modificarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('modificarTurno');
                            }
                            break;
                    }


                } else {
                    alert("No se pudo ingresar la informacion");
                }
            }
        },
        complete: function (jqXHR, textStatus) { },
        error: function (XMLHttpRequest, textStatus, errorThrown) { }
    });

}

function pacienteSearch(key) {
    //busca paciente en atencion al usuario -> paciente
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        buscarPaciente('paciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
    }
}

function checkKey2(key) {

    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        guardarYtraerDatoAlListado('buscarIdentificacionPaciente')
    }

}

function checkKeyVentanita(key, evento) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        switch (evento) {
            case "listGrillaPaciente":
                buscarHistoria('listGrillaPaciente')
                break;

            default:
                break;
        }
    }

}

function checkKeyVentanitaCondicion1(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        buscarHistoria('listGrillaVentanaCondicion1')
    }

}

function checkKeyVentanitaProcedimientos(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        buscarHistoria('listGrillaVentanaProcedimientos')
    }

}

function checkKeyVentanitaDx(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        buscarHistoria('listGrillaVentanaDX')
    }
}

/*NO POS*/
function buscarInformacionNoPos(tipoFormato) {
    // borrarRegistrosTabla('tablaCitas');	  

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestabuscarInformacionNoPos(tipoFormato) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    if (tipoFormato == 'noPos') {
        valores_a_mandar = "accion=noPos";
        valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdArticulo');
    } else {
        valores_a_mandar = "accion=noPosProcedimientos";

        if (tabActivo == 'divProcedimiento')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdProcedimiento');
        else if (tabActivo == 'divAyudasDiagnosticas')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica');
        else if (tabActivo == 'divTerapias')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdTerapia');
        else if (tabActivo == 'divLaboratorioClinico')
            valores_a_mandar = valores_a_mandar + "&txtIdArticulo=" + valorAtributoIdAutoCompletar('txtIdLaboratorio');



    }

    varajaxInit.send(valores_a_mandar);
}


function respuestabuscarInformacionNoPos(tipoFormato) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            if (raiz.getElementsByTagName('infoVAR') != null) {
                totalRegistros = raiz.getElementsByTagName('VAR1').length;

                if (totalRegistros > 0) {
                    if (tipoFormato == 'noPos') {
                        asignaAtributo('txt_ResumenHC', raiz.getElementsByTagName('VAR1')[0].firstChild.data, 0)
                        asignaAtributo('txt_MedicamentoDelPOS1', raiz.getElementsByTagName('VAR2')[0].firstChild.data, 0)
                        asignaAtributo('txt_PresentacionConcentracion1', raiz.getElementsByTagName('VAR3')[0].firstChild.data, 0)
                        asignaAtributo('txt_DosisFrecuencia1', raiz.getElementsByTagName('VAR4')[0].firstChild.data, 0)
                        asignaAtributo('txt_IndicacionTerapeutica', raiz.getElementsByTagName('VAR5')[0].firstChild.data, 0)
                        asignaAtributo('txt_ExpliquePos', raiz.getElementsByTagName('VAR6')[0].firstChild.data, 0)
                    }
                    if (tipoFormato == 'noPosProcedimientos') {
                        asignaAtributo('txt_ResumenHC', raiz.getElementsByTagName('VAR1')[0].firstChild.data, 0)
                        asignaAtributo('txt_AlternativaPos', raiz.getElementsByTagName('VAR2')[0].firstChild.data, 0)
                        asignaAtributo('txt_PosibilidadTerapeutica', raiz.getElementsByTagName('VAR3')[0].firstChild.data, 0)
                        asignaAtributo('txt_MotivoUso', raiz.getElementsByTagName('VAR4')[0].firstChild.data, 0)
                        asignaAtributo('txt_DiagnostProcPos', raiz.getElementsByTagName('VAR5')[0].firstChild.data, 0)
                        asignaAtributo('txt_JustificacionPos', raiz.getElementsByTagName('VAR6')[0].firstChild.data, 0)
                    }

                } else {
                    asignaAtributo('txt_ResumenHC', valorAtributo('txtEnfermedadActual'), 0)
                    //alert('ELEMENTO NO TIENE PARAMETROS ANTERIORES POR DEFECTO')
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}
/*FIN NO POS*/

function respuestaguardarYtraerDatoAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {

                    switch (opcion) {
                        case 'auditarFE':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarFacturacion('listGrillaFacturas');
                            }
                            break;

                        case 'validaNumeracionFactura':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {

                                var noticia = raiz.getElementsByTagName('NOTICIA')[0].firstChild.data;

                                if (noticia != 'PERMITE_NUMERAR') {
                                    alert('1. ATENCION FACTURADOR:\n\n' + noticia);
                                }


                                console.log('Ajuste: ' + raiz.getElementsByTagName('estadoAjuste')[0].firstChild.data)
                                switch (raiz.getElementsByTagName('estadoAjuste')[0].firstChild.data) {

                                    case 'NUMERAR':
                                        //modificarCRUD('actualizarFacturaGenerada');
                                        break;

                                    case 'MAXIMO_CUENTA':
                                        modificarCRUD('ajustarMaximoCuenta')
                                        break;

                                    case 'MAXIMO_ANUAL':
                                        modificarCRUD('ajustarCuentaAnual')
                                        break;

                                }

                                setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')", 200);
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }

                            break;


                        case 'enviarSF':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listaPanelMedicamentos');
                            }
                            break;

                        case 'consultarSF':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listaPanelMedicamentos');
                            }
                            break;

                        case 'auditarRecibo':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                limpiarDivEditarJuan('auditarRecibo');
                                buscarFacturacion('listGrillaRecibos');
                            }
                            break;

                        case 'auditarFactura':

                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                limpiarDivEditarJuan('auditarFactura');
                                buscarFacturacion('listGrillaFacturas');
                            }


                            break;

                        case 'anularFacturaWS':

                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarFacturacion('listGrillaFacturas');
                                alert('FACTURA ANULADA EXISTOSAMENTE');
                            }

                            break;

                        case 'cerrarDocumentoProgramacion':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()																
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodegaProgramacion');
                                /* limpiar campos de las transacciones */

                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'cerrarDocumentoSolicitudesInventario':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()																
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodegaConsumo');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'modificarTransaccionDocumento':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                ocultar('divVentanitaModTransaccion')
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'eliminarTransaccionDocumento':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                ocultar('divVentanitaModTransaccion')
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'verificaTransaccionesInventario':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()								
                                /*CAMBIAR EL LABEL DEL ESTADO A 1*/
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosBodega');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;

                        case 'verificaTransaccionesInventarioTrasladoBodega':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                alert('1. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                                //cerrarDocumentClinicoSinImp()								
                                asignaAtributo('lblIdEstado', '1', 0);
                                buscarSuministros('listGrillaDocumentosTrasladoBodega');
                            } else {
                                alert('2. ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;



                        case 'consultaExamenFisico':
                            asignaAtributo('txtExamenFisicoDescripcion', raiz.getElementsByTagName('examenFisicoDescripcion')[0].firstChild.data, 0);
                            break;


                        case 'elementosDelFolio':
                            if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
                                imprimirHCPdf_paraFinalizar()
                                //cerrarDocumentClinicoSinImp()
                            } else {
                                alert('ATENCION !!!\n\n ' + raiz.getElementsByTagName('NOTICIA')[0].firstChild.data);
                            }
                            break;
                        case 'buscarSiEsNoPos':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                //codigos = ['7527', '7528', '7529', '7530', '7531', '7532', '7533', '7534', '7535', '7536', '7537','43053']
                                //codigo = valorAtributoIdAutoCompletar('txtIdArticulo')
                                //if (codigos.includes(codigo)) {
                                alert('Por favor Diligenciar en MIPRES ')
                                modificarCRUD('listMedicacion');
                                //return false;
                                //}

                                //if (verificarCamposGuardar('listMedicacion')) {
                                //mostrar('divVentanitaMedicamentosNoPos')
                                //cargarTableVistaPreviaNoPos('noPos');
                                //}
                            } else {
                                modificarCRUD('listMedicacion');
                            }
                            break;

                        case 'buscarSiEsNoPosProcedim':
                            if (verificarCamposGuardar('listProcedimientosDetalle')) {
                                // verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups')
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listProcedimientosDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listProcedimientosDetalle');
                            }*/
                            break;

                        case 'buscarSiEsNoPosProcedimientoNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listProcedimientosDetalle');
                            }

                            break

                        case 'buscarSiEsNoPosAyudaDiagnostica':
                            if (verificarCamposGuardar('listAyudasDiagnosticasDetalle')) {
                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoAyudaDiagnostica');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups2');
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listAyudasDiagnosticasDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listAyudasDiagnosticasDetalle');
                            }*/

                            break;

                        case 'buscarSiEsNoPosAyudaDiagnosticaNotificacion':

                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {

                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');

                            } else {
                                modificarCRUD('listAyudasDiagnosticasDetalle');
                            }
                            break;


                        case 'buscarSiEsNoPosLaboratorio':

                            if (verificarCamposGuardar('listLaboratoriosDetalle')) {
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups4');

                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoLaboratorio');
                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listLaboratoriosDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listLaboratoriosDetalle');
                            }*/
                            break;


                        case 'buscarSiEsNoPosLaboratorioNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listLaboratoriosDetalle');
                            }
                            break;


                        case 'buscarSiEsNoPosTerapia':


                            if (verificarCamposGuardar('listTerapiaDetalle')) {
                                //verificarNotificacionAntesDeGuardar('verificarEmbarazoTerapia');
                                verificarNotificacionAntesDeGuardar('verificarCantidadCups3')

                            }
                            /*if(raiz.getElementsByTagName('dato')[0].firstChild.data =='NOPOS'){																
                                if(verificarCamposGuardar('listTerapiaDetalle')){ 
                                  mostrar('divVentanitaMedicamentosNoPos')
                                  cargarTableVistaPreviaNoPos('noPosProcedimientos');
                                }
                            }   
                            else{ 
                               modificarCRUD('listTerapiaDetalle');
                            }*/
                            break;


                        case 'buscarSiEsNoPosTerapiaNotificacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'NOPOS') {
                                mostrar('divVentanitaMedicamentosNoPos')
                                cargarTableVistaPreviaNoPos('noPosProcedimientos');
                            } else {
                                modificarCRUD('listTerapiaDetalle');
                            }
                            break;


                        case 'buscarPendienteLE':

                            //alert('.111..'+raiz.getElementsByTagName('existe')[0].firstChild.data)

                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK')
                                        modificarCRUD('crearCitaCirugia')
                                    else modificarCRUD('crearCita');
                                } else {
                                    if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'S') {
                                        alert('PACIENTE YA TIENE UNA CITA O LISTA DE ESPERA DE ESTA ESPECIALIDAD Y SUB ESPECIALIDAD !!!')
                                    } else if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'O') { /*CITA OCUPADA*/
                                        alert('1. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    }

                                }
                            }
                            break;
                        case 'consultarDisponibleDesdeAgenda':

                            switch (raiz.getElementsByTagName('existe')[0].firstChild.data) {
                                case 'C':
                                    alert('1-. PACIENTE YA TIENE CITA A FUTURO')
                                    break;
                                case 'S':
                                    alert('2-. PACIENTE TIENE LISTA DE ESPERA A FUTURO')
                                    break;
                                case 'O':
                                    alert('3-. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    break;
                                case 'N':
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                        verificarNotificacionAntesDeGuardar('crearCitaCirugia')
                                        //modificarCRUD('crearCitaCirugia')
                                    } else {
                                        verificarNotificacionAntesDeGuardar('crearCita')
                                        //modificarCRUD('crearCita')
                                    }


                                    break;
                            }
                            break;

                        case 'buscarPendienteLE_LE':
                            switch (raiz.getElementsByTagName('existe')[0].firstChild.data) {
                                case 'C':
                                    alert('1-. PACIENTE YA TIENE CITA A FUTURO')
                                    break;
                                case 'S':
                                    alert('2-. PACIENTE TIENE LISTA DE ESPERA A FUTURO')
                                    break;
                                case 'O':
                                    alert('3-. EL ESPACIO DE LA CITA YA SE ENCUENTRA OCUPADO')
                                    break;
                                case 'N':
                                    if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                                        if (valorAtributo('txt_banderaOpcionCirugiaGestion') == 'OKGESTIONPROC') {
                                            modificarCRUD('crearListaEsperaCirugiaProgramacion')
                                        } else {
                                            modificarCRUD('crearListaEsperaCirugia')
                                        }
                                    } else if (valorAtributo('txt_banderaOpcionCirugia') == 'NO') {
                                        modificarCRUD('crearListaEspera');
                                    }
                                    break;
                            }
                            break;

                        case 'buscarIdentificacionPaciente':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    alert('PACIENTE NO EXISTE !!! \n DEBE USTED BUSCAR POR APELLIDOS SEGUIDO DE NOMBRES, O CON DIFERENTE TIPO DE IDENTIFICACION EN LA SIGUIENTE VENTANA ')
                                    traerVentanitaPaciente('txtIdBusPaciente', '', 'divParaVentanita', 'txtIdBusPaciente', '27')

                                } else {
                                    mostrar('divVentanitaPacienteYaExiste')
                                    asignaAtributo('lblIdPacienteExiste', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('lblLetrero', ' PACIENTE  YA EXISTE  !!!', 0)
                                    asignaAtributo("cmbIdEspecialidadCitasPaciente", valorAtributo("cmbIdEspecialidad"), 0)
                                    setTimeout(() => {
                                        buscarAGENDA("listaCitasPaciente")
                                        setTimeout(() => {
                                            buscarAGENDA('listHistoricoListaEsperaEnAgenda')
                                        }, 400);
                                    }, 200);
                                    setTimeout(() => {
                                        switch (ventanaActual.opc) {
                                            case "agenda":
                                                verificarNotificacionAntesDeGuardar("verificarCitasAFuturoPaciente")
                                                break;

                                            case "admisiones":
                                                $('#listAdmisionCuenta').jqGrid('clearGridData')
                                                $('#listCitaCirugiaProcedimiento').jqGrid('clearGridData')
                                                $('#listArticulosDeFactura').jqGrid('clearGridData')
                                                $('#listProcedimientosDeFactura').jqGrid('clearGridData')
                                                setTimeout(() => {
                                                    buscarAGENDA('listAdmisionCuenta');
                                                }, 100);
                                                break;

                                            case "listaEspera":
                                                asignaAtributo("txtAdministradora1", decodeURI(valorAtributo("txtAdministradoraPaciente")), 0)
                                                buscarAGENDA("listHistoricoListaEspera");
                                                break;

                                            case "principalHC":
                                                contenedorBuscarPaciente();
                                                break;

                                            case 'modificarHistoria':
                                                var datoscadena = raiz.getElementsByTagName('dato')[0].firstChild.data;
                                                datoscadena = datoscadena.split('-');
                                                var identificacion = datoscadena[1].split(" ")
                                                asignaAtributo("lblIdPaciente", datoscadena[0], 0)
                                                asignaAtributo("lblIdentificacionPaciente", identificacion[0], 0)
                                                asignaAtributo("lblNombrePaciente", identificacion[1] + " " + identificacion[2] + " " + identificacion[3] + " " + identificacion[4])
                                                edadPacienteModificarHistoria()
                                                setTimeout(buscarHC('listDocumentosHistoricos'), 200)
                                                break;

                                            default:
                                                break;
                                        }
                                    }, 600);
                                    //document.getElementById('divContenidoListaEsperaHist').innerHTML = '<table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>';
                                }
                            }
                            break;

                        case 'buscarIdPacienteEncuesta':

                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {

                                    asignaAtributo("lblTipoIdCrearPacienteURG", valorAtributo("cmbTipoIdUrgencias"), 1)
                                    asignaAtributo("lblIdCrearPacienteURG", valorAtributo("txtIdentificacionUrgencias"), 1)
                                    if (valorAtributo("cmbTipoIdUrgencias") == "MS") {
                                        calendario("txtFechaNacimientoUrgencias", 1)
                                    } else if (valorAtributo("cmbTipoIdUrgencias") == "AS") {
                                        fecha = new Date();
                                        fecha.setFullYear(fecha.getFullYear() - 18)
                                        a = fecha.toISOString().slice(0, 10)
                                        asignaAtributo("txtFechaNacimientoUrgencias", a, 0)
                                    }
                                    limpiaAtributo("txtNombre1Urgencias", 0)
                                    limpiaAtributo("txtNombre2Urgencias", 0)
                                    limpiaAtributo("txtApellido1Urgencias", 0)
                                    limpiaAtributo("txtApellido2Urgencias", 0)

                                    asignaAtributo("cmbSexoUrgencias", "O", 0)
                                    limpiaAtributo("txtFechaNacimientoUrgencias", 0)
                                    limpiaAtributo("txtTelefonoUrgencias", 0)
                                    limpiaAtributo("txtCelular1Urgencias", 0)
                                    limpiaAtributo("txtCelular2Urgencias", 0)
                                    asignaAtributo("txtAdministradoraUrgencias", "65-1055 PARTICULAR ADSCRITO", 0)

                                    asignaAtributo("txtMunicipioResidenciaUrgencias", "52001-PASTO", 0)
                                    limpiaAtributo("txtDireccionUrgencias", 0)
                                    limpiaAtributo("txtEmailUrgencias", 0)

                                    mostrar("divCrearPacienteUrgencias")
                                } else {
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    setTimeout(() => {
                                        buscarInformacionBasicaPaciente()
                                        setTimeout(() => {
                                            cargarInformacionContactoPaciente()
                                            $("#sw_nuevo_paciente").removeAttr("checked");
                                            ocultar("elementosNuevoPaciente")
                                            limpiaAtributo("cmbTipoIdUrgencias", 0)
                                            limpiaAtributo("txtIdentificacionUrgencias", 0)
                                        }, 200);
                                    }, 200);
                                }
                            }
                            break;

                        case 'buscarIdentificacionPacienteUrgencias':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    if (confirm("PACIENTE NO ENCONTRADO \n DESEA CREAR NUEVO PACIENTE CON NUMERO DE DOCUMENTO \n " + valorAtributo("cmbTipoIdUrgencias") + valorAtributo("txtIdentificacionUrgencias") + "?")) {
                                        asignaAtributo("lblTipoIdCrearPacienteURG", valorAtributo("cmbTipoIdUrgencias"), 1)
                                        asignaAtributo("lblIdCrearPacienteURG", valorAtributo("txtIdentificacionUrgencias"), 1)
                                        if (valorAtributo("cmbTipoIdUrgencias") == "MS") {
                                            calendario("txtFechaNacimientoUrgencias", 1)
                                        } else if (valorAtributo("cmbTipoIdUrgencias") == "AS") {
                                            fecha = new Date();
                                            fecha.setFullYear(fecha.getFullYear() - 18)
                                            a = fecha.toISOString().slice(0, 10)
                                            asignaAtributo("txtFechaNacimientoUrgencias", a, 0)
                                        }
                                        limpiaAtributo("txtNombre1Urgencias", 0)
                                        limpiaAtributo("txtNombre2Urgencias", 0)
                                        limpiaAtributo("txtApellido1Urgencias", 0)
                                        limpiaAtributo("txtApellido2Urgencias", 0)

                                        asignaAtributo("cmbSexoUrgencias", "O", 0)
                                        limpiaAtributo("txtFechaNacimientoUrgencias", 0)
                                        limpiaAtributo("txtTelefonoUrgencias", 0)
                                        limpiaAtributo("txtCelular1Urgencias", 0)
                                        limpiaAtributo("txtCelular2Urgencias", 0)
                                        asignaAtributo("txtAdministradoraUrgencias", "65-1055 PARTICULAR ADSCRITO", 0)

                                        asignaAtributo("txtMunicipioResidenciaUrgencias", "52001-PASTO", 0)
                                        limpiaAtributo("txtDireccionUrgencias", 0)
                                        limpiaAtributo("txtEmailUrgencias", 0)

                                        mostrar("divCrearPacienteUrgencias")
                                    }
                                } else {
                                    asignaAtributo('txtIdBusPacienteUrgencias', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    asignaAtributo('lblIdPacienteUrgencias', raiz.getElementsByTagName('dato')[0].firstChild.data.split("-")[0], 0)
                                }
                            }
                            break;

                        case 'nuevoPaciente':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N')
                                    alert(raiz.getElementsByTagName('dato')[0].firstChild.data + '\n \n                           CREADO SATISFACTORIAMENTE  !!!')
                                else alert(raiz.getElementsByTagName('dato')[0].firstChild.data + '\n \n                           YA EXISTE  !!!')
                                asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                            }
                            break;

                        case 'nuevoDocumentoHC':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') {
                                asignaAtributo('lblIdDocumento', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdAdmision', raiz.getElementsByTagName('id_admision')[0].firstChild.data, 1);

                                buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');

                                setTimeout("llenarTablaAlAcordion(valorAtributo('lblIdDocumento'), valorAtributo('cmbTipoDocumento'), valorAtributoCombo('cmbTipoDocumento') ) ", 500)

                                mostrar('divContenidos')
                                asignaAtributo('lblTipoDocumento', valorAtributo('cmbTipoDocumento'), 0)
                                asignaAtributo('lblDescripcionTipoDocumento', valorAtributoCombo('cmbTipoDocumento'), 0)
                                asignaAtributo('txtIdEsdadoDocumento', '0', 0)
                                asignaAtributo('lblNomEstadoDocumento', 'Abierto', 0)

                                habilitar('btn_cerrarDocumentoHC', 1)
                                habilitar('btnEliminarMedicamentoListado', 1)
                                habilitar('btnEliminarProcedimientos', 1)
                                habilitar('btnProcedimiento', 1)
                            }
                            break;
                        case 'nuevaAdmisionCuenta':
                            if (raiz.getElementsByTagName('no_autorizacion')[0].firstChild.data != '') {
                                alert('NUMERO DE AUTORIZACION YA EXISTE')
                            }
                            break;

                        case 'consultarValoresCuenta':
                            asignaAtributo('lblValorNoCubierto', raiz.getElementsByTagName('valor_nocubierto')[0].firstChild.data, 0);
                            asignaAtributo('lblValorCubierto', raiz.getElementsByTagName('valor_cubierto')[0].firstChild.data, 0);
                            asignaAtributo('lblDescuento', raiz.getElementsByTagName('valor_descuento')[0].firstChild.data, 0);
                            asignaAtributo('lblTotalFactura', raiz.getElementsByTagName('total_factura')[0].firstChild.data, 0);
                            asignaAtributo('lblIdEstadoFactura', raiz.getElementsByTagName('id_estado')[0].firstChild.data, 0);
                            asignaAtributo('lblEstadoFactura', raiz.getElementsByTagName('estado')[0].firstChild.data, 0);
                            asignaAtributo('lblIdFactura', raiz.getElementsByTagName('id_factura')[0].firstChild.data, 0);
                            asignaAtributo('lblNumeroFactura', raiz.getElementsByTagName('numero_factura')[0].firstChild.data, 0);

                            if (raiz.getElementsByTagName('medio_pago')[0].firstChild.data == '1') {
                                $("#efectivo").attr('checked', true);
                            } else if (raiz.getElementsByTagName('medio_pago')[0].firstChild.data == '2') {
                                $("#tarjeta").attr('checked', true);
                            } else {
                                alert("EL MEDIO DE PAGO NO SE PUEDE MOSTRAR")
                            }

                            if (raiz.getElementsByTagName('forma_pago')[0].firstChild.data == '1') {
                                $("#contado").attr('checked', true);
                            } else if (raiz.getElementsByTagName('forma_pago')[0].firstChild.data == '2') {
                                $("#credito").attr('checked', true);
                            } else {
                                alert("LA FORMA DE PAGO NO SE PUEDE MOSTRAR")
                            }

                            if (raiz.getElementsByTagName('id_estado')[0].firstChild.data != 'P') {
                                $("input:radio[name=medioPago]").each(function () {
                                    $(this).attr('disabled', 'disabled')
                                });
                                $("input:radio[name=formaPago]").each(function () {
                                    $(this).attr('disabled', 'disabled')
                                });
                            } else {
                                if (raiz.getElementsByTagName('prefijo_factura')[0].firstChild.data == 'FS') {
                                    $("input:radio[name=medioPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                    $("input:radio[name=formaPago]").each(function () {
                                        $(this).attr('disabled', 'disabled')
                                    });
                                } else {
                                    $("input:radio[name=medioPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                    $("input:radio[name=formaPago]").each(function () {
                                        $(this).removeAttr('disabled')
                                    });
                                }
                            }
                            break;

                        case 'verificaCantidadInventario':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var cantidad = parseInt(valorAtributo('txtCantidad'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (cantidad > cantMax) {
                                    if (!confirm('LA MAXIMA CANTIDAD QUE SE HA INGRESADO HISTORICAMENTE ES : ' + cantMax + '\n Desea continuar ?')) {
                                        asignaAtributo('txtCantidad', '', 0);
                                    }
                                } else guardarYtraerDatoAlListado('verificaCantidadInventarioMinimo')
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaCantidadInventarioMinimo':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var cantidad = parseInt(valorAtributo('txtCantidad'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);
                                if (cantidad < cantMax) {
                                    if (!confirm('LA MINIMA CANTIDAD QUE SE HA INGRESADO HISTORICAMENTE ES : ' + cantMax + '\n Desea continuar ?')) {
                                        asignaAtributo('txtCantidad', '', 0);
                                    }
                                }
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaValorInventario':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var txtValorU = parseInt(valorAtributo('txtValorU'));
                                var cantMax = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (txtValorU > cantMax) {
                                    if (!confirm('EL MAXIMO VALOR UNITARIO QUE SE HA INGRESADO HISTORICAMENTE ES = ' + raiz.getElementsByTagName('cantidad')[0].firstChild.data + '\n Desea continuar ?')) {
                                        asignaAtributo('txtValorU', '', 0);
                                    }
                                } else guardarYtraerDatoAlListado('verificaValorInventarioMinimo')
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'verificaValorInventarioMinimo':
                            if (raiz.getElementsByTagName('cantidad')[0].firstChild.data != '') {

                                var txtValorU = parseInt(valorAtributo('txtValorU'));
                                var cant = parseInt(raiz.getElementsByTagName('cantidad')[0].firstChild.data);

                                if (txtValorU < cant) {
                                    if (!confirm('EL MINIMO VALOR UNITARIO QUE SE HA INGRESADO HISTORICAMENTE ES = ' + raiz.getElementsByTagName('cantidad')[0].firstChild.data + '\n Desea continuar ?')) {
                                        asignaAtributo('txtValorU', '', 0);
                                    }
                                }
                            } else { /* no hay datos */
                                alert('NO EXISTE CANTIDADES')
                                //asignaAtributo('lblIdCuentaAntigua', '', 1);							  						  
                            }
                            break;
                        case 'nuevoDocumentoDespachoSolicitud':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                alert('despacho de solicitud');
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodega')
                            }
                            break;
                        case 'nuevoDocumentoInventarioBodegaConsumo':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto							  
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodegaConsumo');
                                /* cargar elementos de bodega de consumo en hoja de gasto */
                                if (valorAtributo('cmbIdTipoDocumento') == '21') {
                                    if (valorAtributo('cmbIdBodega') != '') {
                                        setTimeout("buscarSuministros('listGrillaElementosBodegaConsumo')", 400);
                                    }
                                }
                            }
                            break;
                        case 'buscarConciliacion':
                            if (raiz.getElementsByTagName('dato1')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('cmbExiste', raiz.getElementsByTagName('dato1')[0].firstChild.data, 0);
                                asignaAtributo('txtConciliacionDescripcion', raiz.getElementsByTagName('dato2')[0].firstChild.data, 0);
                                buscarHC('listAntFarmacologicos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                                setTimeout("buscarHC('listMedicacionOrdenada' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )", 500);


                            }
                            break;
                        case 'nuevoDocumentoInventarioProgramacion':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodegaProgramacion')
                            }
                            break;
                        case 'crearDocInvBodega':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosTrasladoBodega')
                            }
                            break;
                        case 'existeDocumentoInventarioBodega':
                            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto							  

                                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != 'true') {
                                    alert("EL NUMERO DE FACTURA CON IGUAL TERCERO YA EXISTE !!! ")
                                } else {
                                    guardarYtraerDatoAlListado('nuevoDocumentoInventarioBodega');
                                }
                            } else {
                                alert("Ya se encuentra registrado un documento con igual numero de factura y proveedor ")
                            }
                            break;
                        case 'nuevoDocumentoInventarioBodega':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data != '') { // si esta limpio es porque paso algun error, lo contrario trae el codigo del documneto
                                asignaAtributo('lblIdDoc', raiz.getElementsByTagName('dato')[0].firstChild.data, 1);
                                asignaAtributo('lblIdEstado', '0', 1);
                                buscarSuministros('listGrillaDocumentosBodega')
                                /* cargar transacciones del documento */
                                limpiarDivEditarJuan('limpCamposTransaccion')
                                if (valorAtributo('txtNaturaleza') == 'I') {
                                    setTimeout("buscarSuministros('listGrillaTransaccionDocumento')", 500);
                                } else {
                                    setTimeout("buscarSuministros('listGrillaTransaccionSalida')", 500);
                                }
                            }
                            break;
                        case 'ocuparCandadoReporte':
                            vanOcuparCandadoReporte = raiz.getElementsByTagName('dato')[0].firstChild.data;
                            alert('respueta=' + vanOcuparCandadoReporte)
                            return vanOcuparCandadoReporte;
                            break;


                        case 'agregarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('agregarTurno');
                            }
                            break;

                        case 'eliminarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('eliminarTurno');
                            }
                            break;

                        case 'modificarTurno':
                            if (raiz.getElementsByTagName('dato')[0].firstChild.data == 'true') {
                                modificarCRUD('modificarTurno');
                            }
                            break;
                    }


                } else {
                    alert("No se pudo ingresar la informacion");
                }

            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}


function eliminarAlListado(opcion) {

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/eliminarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaeliminarAlListado(opcion) };

    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    valores_a_mandar = "accion=" + opcion;
    valores_a_mandar = valores_a_mandar + "&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();

    switch (opcion) {

        case 'listProfesionalesFicha':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + valorAtributo('cmbIdAreaDerecho');
            valores_a_mandar = valores_a_mandar + "&idProfesional=" + valorAtributo('cmbProfesional');

            break;
        case 'listDxIntegralInicial':
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdDx').html();
            break;
        case 'listSituaActual':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdSituacionActual_' + tabActivo).html();
            break;
        case 'listObjMeta':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdObjetivo_' + tabActivo).html();
            break;
        case 'listActividad':
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&id=" + $('#drag' + ventanaActual.num).find('#lblIdActividad_' + tabActivo).html();
            break;


    }

    //alert('eliminarAlListado...'+valores_a_mandar);  
    varajaxInit.send(valores_a_mandar);

}

function respuestaeliminarAlListado(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {
                    listadoTablaHC(opcion);
                    limpiarElementosDeSeleccionParaLista(opcion);
                } else {
                    alert("No se pudo ingresar la informaci�n");
                }

            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}

var tipoDocuHc = '';

function listadoTablaHC(caso) {

    var pag = '/clinica/paginas/accionesXml/buscarHc_xml.jsp';
    //	tabActivo= $('#drag'+ventanaActual.num).find('#IdTabActivo').val();			
    var ancho = ($('#drag' + ventanaActual.num).find("#divEditar").width()) - 60;
    switch (caso) {
        case 'listOrdenesMedicamentos':
            buscarHC(caso, '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

            if (valorAtributo('cmbTipoDocumento') == 'RCOD') {
                habilitar('chkInmediato', 1)
                ocultar('idDivReglaHoras')
                mostrar('idDivInmediato')
            } else {
                habilitar('chkInmediato', 0)
                mostrar('idDivReglaHoras');
                ocultar('idDivInmediato')
            }

            break;
        case 'listOrdenesMedicamentosModificar':
            buscarHC('listOrdenesMedicamentos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;
        case 'listOrdenesInsumos':
            buscarHC(caso, '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
            break;

        case 'listProfesionalesFicha':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            $('#drag' + ventanaActual.num).find('#' + caso).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'idAreaDerecho', 'Area Derecho', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'idAreaDerecho', index: 'idAreaDerecho', hidden: true },
                    { name: 'AreaDerecho', index: 'AreaDerecho', width: anchoP(ancho, 20) },
                    { name: 'Servicio', index: 'Servicio', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: anchoP(ancho, 40), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: anchoP(ancho, 20), align: 'left' },
                ],
                ondblClickRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + caso).getRowData(rowid);

                    asignaAtributo('cmbProfesional', datosRow.idProfesional, 0);
                    asignaAtributo('cmbIdAreaDerecho', datosRow.idAreaDerecho, 0);
                },
                height: 111,
                width: 700,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listDxIntegralInicial':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            $('#drag' + ventanaActual.num).find('#' + caso).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Descripci�n del Diagn�stico inicial', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Descripcion', index: 'Descripcion', width: ((ancho * 77) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 7) / 100), align: 'left' },
                ],

                pager: jQuery('#pager' + caso),
                rowNum: 10,
                rowList: [20, 50, 100],
                sortname: 'FechaHora',
                sortorder: "FechaHora",
                viewrecords: true,
                hidegrid: false,

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso).getRowData(rowid);
                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val()) {

                        $('#drag' + ventanaActual.num).find('#txtDxIntegralInicial').val($.trim(datosDelRegistro.Descripcion));
                        $('#drag' + ventanaActual.num).find('#lblIdDx').html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor')

                },
                height: 90,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listSituaActual':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            //alert('Area Derechossss '+valores_a_mandar);
            $('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Descripci�n', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Descripcion', index: 'Descripcion', width: ((ancho * 77) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 7) / 100), align: 'left' },
                ],

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).getRowData(rowid);

                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val() || $('#drag' + ventanaActual.num).find('#IdUsuario').val() == 'CC000098392395') {
                        $('#drag' + ventanaActual.num).find('#txtSituacionActual_' + tabActivo).val($.trim(datosDelRegistro.Descripcion));
                        $('#drag' + ventanaActual.num).find('#lblIdSituacionActual_' + tabActivo).html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor.')
                },
                height: 111,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso + '_' + tabActivo).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listObjMeta':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            $('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Objetivo', 'Meta', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Objetivo', index: 'Objetivo', width: ((ancho * 40) / 100) },
                    { name: 'Meta', index: 'Meta', width: ((ancho * 40) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 5) / 100), align: 'left' },
                ],

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).getRowData(rowid);
                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val()) {
                        $('#drag' + ventanaActual.num).find('#txtObjetivo_' + tabActivo).val($.trim(datosDelRegistro.Objetivo));
                        $('#drag' + ventanaActual.num).find('#txtMeta_' + tabActivo).val($.trim(datosDelRegistro.Meta));
                        $('#drag' + ventanaActual.num).find('#lblIdObjetivo_' + tabActivo).html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor')

                },
                height: 111,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso + '_' + tabActivo).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listActividad':
            valores_a_mandar = pag + "?accion=" + caso;
            valores_a_mandar = valores_a_mandar + "&idAreaDerecho=" + tabActivo;
            valores_a_mandar = valores_a_mandar + "&sidx=1&idPlatin=" + $('#drag' + ventanaActual.num).find('#lblIdPlatin').html();
            //alert('Area Derechossss '+valores_a_mandar);
            $('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Descripci�n', 'Servicio', 'idProfesional', 'Profesional', 'Fecha Elaboro'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Descripcion', index: 'Descripcion', width: ((ancho * 77) / 100) },
                    { name: 'Servicio', index: 'Servicio', width: ((ancho * 5) / 100), align: 'left' },
                    { name: 'idProfesional', index: 'idProfesional', hidden: true },
                    { name: 'nomProfesional', index: 'nomProfesional', width: ((ancho * 10) / 100), align: 'left' },
                    { name: 'fechaElaboro', index: 'fechaElaboro', width: ((ancho * 7) / 100), align: 'left' },
                ],

                ondblClickRow: function (rowid) {
                    var datosDelRegistro = jQuery('#drag' + ventanaActual.num).find('#' + caso + '_' + tabActivo).getRowData(rowid);
                    if (datosDelRegistro.idProfesional == $('#drag' + ventanaActual.num).find('#IdUsuario').val()) {
                        $('#drag' + ventanaActual.num).find('#txtActividad_' + tabActivo).val($.trim(datosDelRegistro.Descripcion));
                        $('#drag' + ventanaActual.num).find('#lblIdActividad_' + tabActivo).html($.trim(datosDelRegistro.id));
                    } else alert('Ud no puede editar porque no es el autor')

                },
                height: 111,
                width: ancho,
            }); //.navGrid("#pagerAntF",{edit:false,add:false,del:false});		
            $('#drag' + ventanaActual.num).find("#" + caso + '_' + tabActivo).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

    }
}


function llamarAutocomIdDescripcion(idCampoInput, idQuery) {
    //limpiarDivEditarJuan('reportarEventoAdverso');

    if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") { // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery;
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}

function llamarAutocomProcedimientoFactura(idCampoInput, idQuery, parametro1) {
    if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") { // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery + "&parametro1=" + valorAtributo(parametro1);
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestallamarAutocomProcedimientoFactura(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}

function respuestallamarAutocomProcedimientoFactura(idCampoInput) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('item') != null) {
                cod = raiz.getElementsByTagName('cod');
                text = raiz.getElementsByTagName('text');
                data = new Array();
                for (a = 0; a < text.length; a++) {
                    data[a] = cod[a].firstChild.data + '-' + text[a].firstChild.data;
                }

                $('#drag' + ventanaActual.num).find('#' + idCampoInput).flushCache();
                $('#drag' + ventanaActual.num).find('#' + idCampoInput).autocomplete(data, {
                    multiple: false,
                    matchContains: true, //sirve para buscar en cualquier parte de la frase
                    cacheLength: 1,
                    width: 600,
                    height: 1500,
                    deferRequestBy: 0, //miliseconds
                    selSeparator: '|', //separador de campos '|' por defecto
                    minChars: 1 //minimo de caracteres para empezar a desplegar la lista
                });
                buscarFacturacion('listProcedimientosDeFactura')
            } else {
                alert("No se puede Autocompletar");
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) { }

    $('#drag' + ventanaActual.num).find('#' + idCampoInput).focus();
}

var idCampoInputGral = '',
    idQueryGral = '',
    idCampoInput2 = '';

function llamarAdministradorasProgramacionOrden() {
    var terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');

    var id_procedimiento = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).ID_PROCEDIMIENTO

    llenarElementosAutoCompletarKey('txtAdministradora1Ordenes',
        200,
        valorAtributo("lblIdSedeAgendaOrdenes"),
        id_procedimiento
    );
}


function llenarValidarCheck(id, value) {

    if (document.getElementById("chkInterfazLab").checked) {
        llenarElementosAutoCompletarKey(id, 10366, value, '32', 'PLANTILLA ANNARLAB')
    } else {
        llenarElementosAutoCompletarKey(id, 221, value)
    }
}

function llenarValidarCheckVentanita(id) {

    if (document.getElementById("chkInterfazLab").checked) {
        traerVentanitaProcedimientos(id, 'lblIdAdministradora', 'divParaVentanitaCondicion1VentanitaProcmto', 'txtIdProcedimiento', '61')
    } else {
        traerVentanitaProcedimientos(id, 'lblIdAdministradora', 'divParaVentanitaCondicion1VentanitaProcmto', 'txtIdProcedimiento', '25')
    }
}

function llenarElementosAutoCompletarKey(idCampoInput, idQuery, condicion1, condicion2, condicion3, condicion4) {
    var cantCondiciones;

    if (condicion1 === undefined) {
        cantCondiciones = 0
    } else {
        cantCondiciones = 1
    }

    if (condicion2 !== undefined) {
        cantCondiciones = 2
    }

    if (condicion3 !== undefined) {
        cantCondiciones = 3
    }

    if (condicion4 !== undefined) {
        cantCondiciones = 4
    }

    valores_a_mandar = "idQueryCombo=" + idQuery + "&cantCondiciones=" + cantCondiciones + "&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { RespuestallenarElementosAutoCompletarKey(idCampoInput) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.send(valores_a_mandar);
}



function llenarElementosAutoCompletarexternos(idCampoInput, idQuery, condicion1, condicion2) {
    var cantCondiciones = 3;
    var paquete = 32;
    var plantilla = 'PLANTILLA ANNARLAB';

    /*
    switch (condicion2){
        case '455':
            paquete = '32'
            plantilla = 'PLANTILLA ANNARLAB' 
            break;
        case '160':
            paquete = '33'
            plantilla = 'PLANTILLA CLINIZAD' 
            break;
    }
    */

    valores_a_mandar = "idQueryCombo=" + idQuery + "&cantCondiciones=" + cantCondiciones + "&condicion1=" + condicion1 + "&condicion2=" + paquete + "&condicion3=" + plantilla;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { RespuestallenarElementosAutoCompletarKey(idCampoInput) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.send(valores_a_mandar);
}




function RespuestallenarElementosAutoCompletarKey(idCampoInput) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            cod = raiz.getElementsByTagName('id');
            text = raiz.getElementsByTagName('nom');

            data = new Array();

            for (a = 0; a < text.length; a++) {
                data[a] = cod[a].firstChild.data + '-' + text[a].firstChild.data;
            }

            $('#drag' + ventanaActual.num).find('#' + idCampoInput).flushCache();
            $('#drag' + ventanaActual.num).find('#' + idCampoInput).autocomplete(data, {
                multiple: false,
                matchContains: true,
                cacheLength: 1,
                width: 600,
                height: 1500,
                deferRequestBy: 0,
                selSeparator: '|',
                minChars: 1
            });


        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) { }
    $('#drag' + ventanaActual.num).find('#' + idCampoInput).focus();
}

/*function llenarElementosAutoCompletarKey(idCampoInput, idQuery, parametro1) {
    datos_a_mandar = "accion=idQuery";
    datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery + "&parametro1=" + valorAtributo(parametro1);
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.send(datos_a_mandar);
}*/

function llamarAutocomIdDescripcionParametro(idCampoInput, idQuery, parametro1) {
    //if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") {
    if (true) {
        // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery + "&parametro1=" + valorAtributo(parametro1);
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}

function llamarAutocomIdDescripcionParametroEmpresa(idCampoInput, idQuery, parametro1) {
    if ($('#drag' + ventanaActual.num).find('#' + idCampoInput).val() == "") { // para que el escoger del listado no se vuelva a lanzar el autocompleteAjax
        datos_a_mandar = "accion=idQuery";
        datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery + "&parametro1=" + parametro1;
        varajaxInit = crearAjax();
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
        varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput) };
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.send(datos_a_mandar);
    }
}


function llamarAutocompletarEmpresa(idCampoInput, idQuery) {
    idCampoInput2 = idCampoInput;
    if (valorAtributo(idCampoInput).length == 0) {
        //setTimeout( 'llamarAutocomIdDescripcionParametro('+idCampoInput+','+idQuery+','+  parametro1 +')');
        llamarAutocomIdDescripcionParametroEmpresa(idCampoInput, idQuery, IdEmpresa())
    }
}

function llamarAutocompletarParametro1(idCampoInput, idQuery, parametro1) {
    idCampoInput2 = idCampoInput;
    console.log("lblIdAdministradora: " + valorAtributo(parametro1));
    if (valorAtributo(parametro1) == '' || valorAtributo(parametro1) < 1) {
        alert("Paciente sin asignacion de plan\n     por favor agregar plan")
    } else {
        if (valorAtributo(idCampoInput).length == 0) {
            //setTimeout( 'llamarAutocomIdDescripcionParametro('+idCampoInput+','+idQuery+','+  parametro1 +')');
            llamarAutocomIdDescripcionParametro(idCampoInput, idQuery, parametro1)
        }
    }
}

/**
 * Función para limpiar campos que tienen la libreria select2.
 * 
 * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
 * @param {String} idCampo - id del campo a limpiar.
 * @returns {void}
 */
function limpiarSelect2(idCampo) {
    $(`#${idCampo}`).select2('data', { id: 0, text: '' }).trigger('change')
    $(`#${idCampo}`).val("").trigger("change");
}

function llamarAutocomIdDescripcionConDato(idCampoInput, idQuery) {

    idCampoInput2 = idCampoInput;
    if (valorAtributo(idCampoInput).length == 0) {
        setTimeout('llamarAutocomIdDescripcionConDatoRetardo(' + idCampoInput + ',' + idQuery + ')', 20);
    }
}

function llamarAutocomIdDescripcionConDatoRetardo(idCampoInput, idQuery) {
    //alert(idCampoInput2+'--'+idQuery)
    datos_a_mandar = "accion=idQuery";
    datos_a_mandar = datos_a_mandar + "&idQuery=" + idQuery;
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarAutocompletar_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestaLlamarAutocomIdDescripcion(idCampoInput2) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.send(datos_a_mandar);
}



function respuestaLlamarAutocomIdDescripcion(idCampoInput) {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('item') != null) {
                cod = raiz.getElementsByTagName('cod');
                text = raiz.getElementsByTagName('text');
                data = new Array();
                for (a = 0; a < text.length; a++) {
                    data[a] = cod[a].firstChild.data + '-' + text[a].firstChild.data;
                }

                $('#drag' + ventanaActual.num).find('#' + idCampoInput).flushCache();
                $('#drag' + ventanaActual.num).find('#' + idCampoInput).autocomplete(data, {
                    multiple: false,
                    matchContains: true, //sirve para buscar en cualquier parte de la frase
                    // autofill:false,
                    //selectedFirst:false, // ordena segun los caracteres coinidentes
                    cacheLength: 1,
                    width: 600,
                    height: 1500,
                    deferRequestBy: 0, //miliseconds
                    //formatItem:formatItem,
                    //formatResult:formatResult,											  
                    selSeparator: '|', //separador de campos '|' por defecto
                    // lineSeparator:'\n' //separador de lineas (registros) '\n' por defecto
                    minChars: 1 //minimo de caracteres para empezar a desplegar la lista
                });
            } else {
                alert("No se puede Autocompletar");
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) { }
    $('#drag' + ventanaActual.num).find('#' + idCampoInput).focus();
}


function anchoP(ancho, anchoPorciento) {
    return ((ancho * anchoPorciento) / 100)
    // return anchoPorciento;
}


function cargarDatosIdPaciente() {
    setTimeout("llamarAutocomIdDescripcion('txtBusIdPacienteListaEspera',15)", 500);
}

function cambiarElTabActivo(idTab) {

    //	$('#drag'+ventanaActual.num).find('#IdTabActivo').val(idTab);
    tabActivo = idTab;
    listadoTablaHC('listSituaActual');

    //copiarDeListAList('listSituaActual');		
    setTimeout("listadoTablaHC('listObjMeta')", 500);
    setTimeout("listadoTablaHC('listActividad')", 1900);

}

function alert_xxxx(texto) { // para el funcionamiento real dejar= alert
    jAlert(texto, "ATENCION ! ! !");
}

function alertMal(texto) {
    jAlertMal(texto, "ATENCION ! ! !");
}

function alertBien(texto) {
    jAlertBien(texto, "ATENCION ! ! !");
}

function confirmar(texto) {
    jConfirm(texto, "");
}



var headImpCC = " <head>";
headImpCC += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /> ";
headImpCC += "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\"> ";
headImpCC += "<META HTTP-EQUIV=\"Cache-Control\" CONTENT=\"no-cache\"> ";
headImpCC += "<title> . </title> ";
headImpCC += "<link href=\"/clinica/paginas/hc/documentosHistoriaClinicaPDF/pdf.css\" rel=\"stylesheet\" type=\"text/css\"> ";
headImpCC += "<link href=\"/clinica/paginas/hc/documentosHistoriaClinicaPDF/pdf.css\" rel=\"stylesheet\" type=\"text/css\" media=\"print\"></head> ";


//headImpCC += "<link href=\"/clinica/paginas/reportes/documentosHistoriaClinicaPDF\pdf.css" rel=\"stylesheet\" type=\"text/css\"> ";
//headImpCC += "<link href=\"/clinica/paginas/reportes/documentosHistoriaClinicaPDF\pdf.css" rel=\"stylesheet\" type=\"text/css\" media=\"print\"></head> ";



function imprimirCC(que) {
    var ventana = window.open("", "", "");
    var contenido = "<html>" + headImpCC + "<body onload='window.print();window.close();'>" + document.getElementById(que).innerHTML + "</body></html>";
    ventana.document.open();
    ventana.document.write(contenido);
    //ventana.document.close();
}
//





function copiarDeListAList(areaDerecho) {

    var registros0 = '',
        registros1 = '',
        registros2 = '',
        registros3 = '';

    var ids = jQuery("#listSituaActual_" + areaDerecho).getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = ids[i];
        var datosDelRegistro = jQuery("#listSituaActual_" + areaDerecho).getRowData(c);

        registros0 = registros0 + '- ' + $.trim(datosDelRegistro.Descripcion) + ' <p>';
    }


    ids2 = '';
    ids2 = jQuery("#listObjMeta_" + areaDerecho).getDataIDs();
    for (var i = 0; i < ids2.length; i++) {
        var c2 = ids[i];
        var datosDelRegistro2 = jQuery("#listObjMeta_" + areaDerecho).getRowData(c2);

        registros1 = registros1 + '- ' + $.trim(datosDelRegistro2.Objetivo) + '<p>';
        registros2 = registros2 + '- ' + $.trim(datosDelRegistro2.Meta) + '<p>';
    }

    ids3 = '';
    ids3 = jQuery("#listActividad_" + areaDerecho).getDataIDs();
    for (var i = 0; i < ids3.length; i++) {
        var c3 = ids3[i];
        var datosDelRegistro3 = jQuery("#listActividad_" + areaDerecho).getRowData(c3);
        registros3 = registros3 + '- ' + $.trim(datosDelRegistro3.Descripcion) + '<p>';
    }
    // -------------------------------ingresar a las tablas
    borrarRegistrosTabla('listSituaActual_' + areaDerecho + '_doc');
    tablabody = document.getElementById('listSituaActual_' + areaDerecho + '_doc').lastChild;
    tr = document.createElement("TR");
    th = document.createElement("Td");
    th.innerHTML = registros0;
    tr.appendChild(th);
    tablabody.appendChild(tr);


    borrarRegistrosTabla('listObjMeta_' + areaDerecho + '_doc');
    tablabody = document.getElementById('listObjMeta_' + areaDerecho + '_doc').lastChild;
    tr = document.createElement("TR");
    th = document.createElement("Td");
    th.width = '33%';
    th.innerHTML = registros1;
    tr.appendChild(th);

    th = document.createElement("Td");
    th.width = '33%';
    th.innerHTML = registros3;
    tr.appendChild(th);

    th = document.createElement("Td");
    th.width = '33%';
    th.innerHTML = registros2;
    tr.appendChild(th);
    tablabody.appendChild(tr);


}



function vertanasMoviblesConPunteroMouse(idDiv, zIndex2) {

    $('#drag' + ventanaActual.num).find('#' + idDiv).draggable({
        zIndex: zIndex2,
        ghosting: true,
        opacity: 0.7,
        //containment : 'parent',  para que se mueva dependiendo solo de su parent padre
        handle: '#tituloForma'
    });

}

function ira(opc) { //funcion que recarga valores si se da click en flechas
    switch (opc) {
        case 'P':
            posicionActual = 0;
            document.getElementById('ri').style.display = 'none';
            document.getElementById('ra').style.display = 'none';
            document.getElementById('rs').style.display = 'block';
            document.getElementById('rf').style.display = 'block';
            break;
        case 'A':
            posicionActual--;
            if (posicionActual <= 0) {
                document.getElementById('ri').style.display = 'none';
                document.getElementById('ra').style.display = 'none';
            }
            document.getElementById('rs').style.display = 'block';
            document.getElementById('rf').style.display = 'block';
            break;
        case 'S':
            posicionActual++;
            if (posicionActual >= totalRegistros - 1) {
                document.getElementById('rs').style.display = 'none';
                document.getElementById('rf').style.display = 'none';
            }
            document.getElementById('ra').style.display = 'block';
            document.getElementById('ri').style.display = 'block';
            break;
        case 'U':
            posicionActual = totalRegistros - 1;
            document.getElementById('rs').style.display = 'none';
            document.getElementById('rf').style.display = 'none';
            document.getElementById('ri').style.display = 'block';
            document.getElementById('ra').style.display = 'block';
            break;
    }
    cargarFormulariosBusqueda();
}


function presionaEnter(oEvento) {

    var ttt = document.getElementById('txtPassword').value;

    if (document.getElementById('txtLogin').value != '' && ttt.length > 8)
        validar2();


    var iAscii;
    if (oEvento.keyCode)
        iAscii = oEvento.keyCode;
    else if (oEvento.which)
        iAscii = oEvento.which;
    else return false;
    if (iAscii == 13)
        return true;
    else return false;

}


function cargarFormulariosBusqueda() {
    switch (paginaActual) {
        case 'opcion1':
            break;
        case 'opcion2':
            break;
    }
    //cargarFormulariosBusquedaAng();	
}

//cargar imagenes intercambiables
function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
}

function MM_preloadImages() { //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p) d.MM_p = new Array();
        var i, j = d.MM_p.length,
            a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}

function MM_findObj(n, d) { //v4.01
    var p, i, x;
    if (!d) d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n);
    return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0,
        x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc) x.oSrc = x.src;
            x.src = a[i + 2];
        }
}

function desactivarboton(boton) {
    if ($(boton).checked == true) {
        $(boton).checked = false;
    }
}

function imprimirAA(que) {
    var ventana = window.open("", "", "");
    var contenido = "<html>" + headImpCC + "<body onload='window.print();window.close();'>" + document.getElementById(que).innerHTML + "</body></html>";
    ventana.document.open();
    ventana.document.write(contenido);
    ventana.document.close();
}

function imprimirAgenda() {
    if (valorAtributo("lblIdSedeAgendaOrdenes") == '') var p1 = '%';
    else var p1 = valorAtributo("lblIdSedeAgendaOrdenes");
    if (valorAtributo("lblIdEspecialidadAgendaOrdenes") == '') var p2 = '%';
    else var p2 = valorAtributo("lblIdEspecialidadAgendaOrdenes");
    if (valorAtributo("lblIdProfesionalAgendaOrdenes") == '') var p3 = '%';
    else var p3 = valorAtributo("lblIdProfesionalAgendaOrdenes");
    if (valorAtributo("txtFechaInicioA") == '') var p4 = '%';
    else var p4 = valorAtributo("txtFechaInicioA")
    if (valorAtributo("txtFechaFinA") == '') var p5 = '%';
    else var p5 = valorAtributo("txtFechaFinA")
    if (valorAtributo("cmbExitoAgenda") == '') var p6 = '%';
    else var p6 = valorAtributo("cmbExitoAgenda");
    //if (valorAtributo("txtIdLocalidadA") == '') var p7 = '%';
    //else var p7 = valorAtributo("txtIdLocalidadA");
    var nombrepdf = "AGENDA.pdf"
    var Url = "/clinica/paginas/ireports/hc/generaAgenda.jsp?p1=" + p1 + "&p2=" + p2 + "&p3=" + p3 + "&p4=" + p4 + "&p5=" + p5 + "&p6=" + p6 + "&nombre=" + nombrepdf;
    var dimen = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90';
    window.open(Url, '', dimen);
}

//Funciones para manejo de codigos en los combos
function cambiarCombo(idcombo, texto, idfoco) {
    band = false;
    for (var i = 0; i < $(idcombo).length; i++) {
        if (trim($(idcombo).options[i].value.toUpperCase()) == trim($(texto).value.toUpperCase())) {
            $(idcombo).selectedIndex = i;
            i = $(idcombo).length;
            band = true;
            $(texto).style.backgroundColor = "#99CCFF";
            $(idcombo).style.backgroundColor = '#99CCFF';
            $(idfoco).focus();
        }
    }
    if (!band) {
        $(texto).value = "NaN->";
        $(texto).style.backgroundColor = "#9999CC";
        $(idcombo).style.backgroundColor = '#FFFFFF';
        $(idcombo).selectedIndex = 0;
    }
    return band;
}

function mandarCombo(idtexto, combo) {
    document.getElementById(idtexto).value = document.getElementById(combo).options[document.getElementById(combo).selectedIndex].value;
    document.getElementById(idtexto).style.backgroundColor = "#99CCFF";
    document.getElementById(combo).style.backgroundColor = '#99CCFF';
}
////////////////////Fin de funciones para codigos y combos


// ubicaciones
function cargarDepartamentos(codPais, pagina) {
    valores_a_mandar = "codPais=" + codPais;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarDeptos;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}



function cargarDepartamentos1(codPais, pagina) {

    valores_a_mandar = "codPais=" + codPais;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarDeptos1;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}

function cargarDepartamentos2(codPais, pagina) {

    valores_a_mandar = "codPais=" + codPais;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarDeptos2;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}

function respuestaCargarDeptos() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbDepartamento').options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbDepartamento').options[(document.getElementById('cmbDepartamento').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarDeptos1() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbDepartamento1').options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbDepartamento1').options[(document.getElementById('cmbDepartamento1').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarDeptos2() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbDepartamento2').options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbDepartamento2').options[(document.getElementById('cmbDepartamento2').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarCiudades(codDepto, pagina) {
    valores_a_mandar = "codDepto=" + codDepto;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarCiudades;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}

function cargarCiudades1(codDepto, pagina) {
    valores_a_mandar = "codDepto=" + codDepto;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarCiudades1;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}

function cargarCiudades2(codDepto, pagina) {
    valores_a_mandar = "codDepto=" + codDepto;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarCiudades2;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}

function respuestaCargarCiudades() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbMunicipio').options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbMunicipio').options[(document.getElementById('cmbMunicipio').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarCiudades1() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbMunicipio1').options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbMunicipio1').options[(document.getElementById('cmbMunicipio1').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestaCargarCiudades2() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbMunicipio2').options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            for (i = 0; i < c.length; i++) {
                document.getElementById('cmbMunicipio2').options[(document.getElementById('cmbMunicipio2').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}



function cargarBarrios(codMunici, pagina) {
    valores_a_mandar = "codMunici=" + codMunici;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaCargarBarrios;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}


var banderaBarrios = false;

function respuestaCargarBarrios() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            document.getElementById('cmbBarrios').options.length = 1;
            c = raiz.getElementsByTagName('codBarrio');
            n = raiz.getElementsByTagName('nomBarrio');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById('cmbBarrios').options[(document.getElementById('cmbBarrios').options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                }
            } else {
                if (banderaBarrios == true) {
                    alert('Tenga en cuenta que este municipio no tiene barrrios registrados.\nRegistre nuevos barrios entrando por la opcion de Ubicaciones \ndel menu de Administracion-Seguridad/Parametros generales. ');
                    banderaBarrios = false;
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) { }
}

function cargarDepartamentosIns(deptoDefecto, comboDep, codPais, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codPais=" + codPais;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarDeptosIns;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarDeptosIns() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('codDepto');
            n = raiz.getElementsByTagName('nomDepto');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                    }
                }
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione un departamento]', '', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarCiudadesIns(ciuDefecto, comboCiu, codDepto, pagina) {
    idcombo = comboCiu;
    porDefecto = ciuDefecto;
    valores_a_mandar = "codDepto=" + codDepto;
    varajax2 = crearAjax();
    varajax2.open("POST", pagina, true);
    varajax2.onreadystatechange = respuestaCargarCiudadesIns;
    varajax2.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax2.send(valores_a_mandar);
}
var bandera = 0;

function respuestaCargarCiudadesIns() {
    if (varajax2.readyState == 4) {
        if (varajax2.status == 200) {
            raiz = varajax2.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('codMunicipio');
            n = raiz.getElementsByTagName('nomMunicipio');
            if (c.length > 0) { //  alert("a "+bandera); 
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                    }
                }

            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione un municipio]', '', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax2.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarBarriosIns(codBarrio, comboBarr, codMunici, pagina) { // alert("b "+bandera);   // alert(varajax1.readyState+" "+ varajax2.readyState);

    idcombo = comboBarr;
    porDefecto = codBarrio;
    valores_a_mandar = "codMunici=" + codMunici;
    varajax3 = crearAjax();
    varajax3.open("POST", pagina, true);
    varajax3.onreadystatechange = respuestaCargarBarriosIns;
    varajax3.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax3.send(valores_a_mandar);

}

var banderaBarrios = false;

function hola() {
    alert("falta retrazar el ajax");

}

function respuestaCargarBarriosIns() { //setTimeout('hola()',50);
    if (varajax3.readyState == 4) {
        if (varajax3.status == 200) {
            raiz = varajax3.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('codBarrio');
            n = raiz.getElementsByTagName('nomBarrio');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, true, false);
                    }
                }

            } else {
                if (banderaBarrios == true) {
                    alert('Tenga en cuenta que este municipio no tiene barrrios registrados.\nRegistre nuevos barrios entrando por la opcion de Ubicaciones \ndel menu de Administracion-Seguridad/Parametros generales. ');
                    banderaBarrios = false;
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax3.readyState == 1) { }
}

function cargarCriterioFactor(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestacargarCriterioFactor;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarCriterioFactor() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione criterio]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione criterio]', '00', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarNoConformidades(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarNoConformidades;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarNoConformidades() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarOtroCombo(deptoDefecto, comboHijo, codPadre, pagina) {
    idcombo = comboHijo;
    porDefecto = deptoDefecto;
    valores_a_mandar = "accion=" + comboHijo;
    valores_a_mandar = valores_a_mandar + "&codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestacargarOtroCombo;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);

}

function respuestacargarOtroCombo() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');

            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a..', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione]', '00', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

//******************************************************************************************
function cargarComboGRALCondicionDocumentosBodega(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRALOrdenesMedicaVia;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
    return true;
}

/*function cargarSubEspecialidadDesdeEspecialidad(comboOrigen, comboDestino) {
    cargarCombo2Tecnica('cmbTecnicaDos', '1', comboDestino, 111, valorAtributo(comboOrigen));
}*/

function cargarRadioGRALX(chkDefecto, idDivRadioDestino, idQueryCombo, tabla = true, ...condiciones) {
    let numCondiciones = condiciones.length;
    idcombo = idDivRadioDestino;
    porDefecto = chkDefecto;
    valores_a_mandar = `idQueryCombo=${idQueryCombo}&cantCondiciones=${numCondiciones}`;
    divTable = tabla;
    for (let i = 0; i < condiciones.length; i++) {
        valores_a_mandar += `&condicion${i + 1}=${condiciones[i]}`;
    }
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarRadioGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarRadioGRAL() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            let ids = raiz.getElementsByTagName('id');
            let nombres = raiz.getElementsByTagName('nom');
            let titulos = raiz.getElementsByTagName('title');

            let divRadioDestino = $(`#${idcombo}`)
            divRadioDestino.empty();
            let divTr = null;
            if (divTable) {
                for (let i = 0; i < ids.length; i++) {

                    divRadioDestino.append(
                        $('<tr>').append(
                            $('<td>').append(
                                $('<input>', {
                                    'type': 'radio',
                                    'id': `radioMod>${ids[i].firstChild.data}`,
                                    'name': 'checkModalidad',
                                    'value': ids[i].firstChild.data,
                                    'checked': (porDefecto && porDefecto == ids[i].firstChild.data)
                                })
                            ),
                            $('<td>').append(
                                $('<label>', {
                                    'for': `radioMod>${ids[i].firstChild.data}`,
                                    'title': titulos[i].firstChild.data,
                                }).html(nombres[i].firstChild.data)
                            )
                        )
                    );
                }
            } else {
                for (let i = 0; i < ids.length; i++) {
                    divRadioDestino.append(
                        $('<input>', {
                            'type': 'radio',
                            'id': `radioMod>${ids[i].firstChild.data}`,
                            'name': 'checkModalidad',
                            'value': ids[i].firstChild.data
                        })
                    );
                    divRadioDestino.append(
                        $('<label>', {
                            'for': `radioMod>${ids[i].firstChild.data}`,
                            'title': titulos[i].firstChild.data,
                        }).html(nombres[i].firstChild.data)
                    );
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
}

function cargarComboGRAL(cmbPadre, deptoDefecto, comboDestino, idQueryCombo) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=0";
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarComboGRALCondicion1(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;
    return new Promise((resolve = () => { }, reject = () => { }) => {
        $.ajax({
            url: '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp',
            data: valores_a_mandar,
            contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
            beforeSend: function () {
                //mostrarLoader(urlParams.get("accion"))
            },
            success: function (data) {
                raiz = data;
                if (document.getElementById(idcombo) != null) { document.getElementById(idcombo).options.length = 1; }
                c = raiz.getElementsByTagName('id');
                n = raiz.getElementsByTagName('nom');
                d = raiz.getElementsByTagName('title');

                selected = false;
                $("#" + idcombo).empty();
                if (porDefecto == '') {
                    $("#" + idcombo).append(new Option("[ SELECCIONE ]", "", true, true));
                    selected = true;
                } else {
                    $("#" + idcombo).append(new Option("[ SELECCIONE ]", ""));
                }

                if (c.length > 0) {
                    if (c.length <= 1) {
                        if (selected) {
                            $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data));
                            document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                        } else {
                            $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data, true, true));
                            document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                        }
                    } else {
                        for (i = 0; i < c.length; i++) {
                            if (c[i].firstChild.data == porDefecto) {
                                $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data, true, true));
                                document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                            } else {
                                $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data));
                                document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                            }
                        }
                    }

                }

                /*if (c.length > 0) {
                    for (i = 0; i < c.length; i++) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                    if (idcombo == 'cmbRefCampo') {
                        $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
                    } else {
                        document.getElementById(idcombo).options[0] = new Option('[ Seleccione ]', '', true, false);
                        $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
                    }
                } else {
                    document.getElementById(idcombo).options[0] = new Option('[ Seleccione ]', '', true, false);
                }*/
                resolve();
            },
            complete: function (jqXHR, textStatus) {
                //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                reject();
            }
        });
    });
}

function cargarComboGRALCondicion1_(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;

    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    //alert(valores_a_mandar)
    varajax1.send(valores_a_mandar);
}

async function cargarComboGRALCondicion2Async(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2) {
    return new Promise((resolve, reject) => {
        if (condicion1 != '' && condicion2 != '') {
            idcombo = comboDestino;
            porDefecto = deptoDefecto;
            valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=2&condicion1=" + condicion1 + "&condicion2=" + condicion2;
            varajax1 = crearAjax();
            varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
            varajax1.onreadystatechange = async function () {
                try {
                    await respuestacargarComboGRALAsync();  // Espera la respuesta asíncrona antes de resolver la promesa
                    resolve();
                } catch (error) {
                    reject(error);
                }
            };
            varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            varajax1.send(valores_a_mandar);
        } else {
            // ... (resto del código)
            resolve();  // Resuelve la promesa inmediatamente si no hay condiciones
        }
    });
}


function cargarComboGRALCondicion2(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2) {
    if (condicion1 != '' && condicion2 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=2&condicion1=" + condicion1 + "&condicion2=" + condicion2;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarComboGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    } else {
        if (comboDestino == 'cmbIdRango') {
            asignaAtributoCombo2('cmbIdRango', '', '[ Seleccione ]');
            //console.log('Ingreso a limpiar')
            //limpiaAtributo('cmbIdRango',0);
        } else if (comboDestino == 'cmbMotivoConsultaClase') {
            asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
        }

        $("#" + comboDestino).empty();
        $("#" + comboDestino).append(new Option("[ SELECCIONE ]", "", true, true));
    }
}

function cargarComboGRALCondicion2_1(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2) {
    return new Promise((resolve, reject) => {
        if (condicion1 !== '' && condicion2 !== '') {
            valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=2&condicion1=" + condicion1 + "&condicion2=" + condicion2;

            $.ajax({
                url: '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp',
                data: valores_a_mandar,
                contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
                success: function (data) {
                    // Procesa la respuesta aquí si es necesario
                    respuestaCargarComboDestino(data, comboDestino);
                    resolve(); // Resuelve la promesa si la solicitud es exitosa
                },
                error: function (xhr, textStatus, errorThrown) {
                    // Maneja errores en caso de que ocurra un problema con la solicitud Ajax
                    console.error('Error en la solicitud Ajax:', errorThrown);
                    reject(errorThrown); // Rechaza la promesa en caso de error
                },
            });
        } else {
            if (comboDestino === 'cmbIdRango') {
                asignaAtributoCombo2('cmbIdRango', '', '[ Seleccione ]');
            } else if (comboDestino === 'cmbMotivoConsultaClase') {
                asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
            }

            $("#" + comboDestino).empty();
            $("#" + comboDestino).append(new Option("[ SELECCIONE ]", "", true, true));
            resolve(); // Resuelve la promesa si no se requiere la solicitud Ajax
        }
    });
}

/**
//funcion actualizada de respuestacargarComboGRAL()
* @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>  
*/
function respuestaCargarComboDestino(data, comboDestino) {

    const raiz = data.documentElement;
    const $idcombo = $("#" + comboDestino);
    if ($idcombo.length) {
        $idcombo.empty();
    }
    const c = raiz.getElementsByTagName('id');
    const n = raiz.getElementsByTagName('nom');
    const d = raiz.getElementsByTagName('title');
    const selected = porDefecto === '';
    $idcombo.append(new Option("[ SELECCIONE ]", selected ? "" : "", true, selected));
    for (let i = 0; i < c.length; i++) {
        const option = new Option(n[i].firstChild.data, c[i].firstChild.data, c[i].firstChild.data === porDefecto, c[i].firstChild.data === porDefecto);
        option.title = 'DEFINICION : ' + d[i].firstChild.data;
        $idcombo.append(option);
    }
}



function cargarComboGRALCondicion3(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2, condicion3) {
    if (condicion1 != '' && condicion2 != '' && condicion3 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarComboGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    } else {

        if (comboDestino == 'cmbIdRango') {
            asignaAtributoCombo2('cmbIdRango', '', '[ Seleccione ]');
            //console.log('Ingreso a limpiar')
            //limpiaAtributo('cmbIdRango',0);
        } else if (comboDestino == 'cmbMotivoConsultaClase') {
            asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
        }
    }
}

// se actualiza la funcion cargarComboGRALCondicion3
function cargarComboGRALCondicion3_1(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1, condicion2, condicion3) {
    if (condicion1 != '' && condicion2 != '' && condicion3 != '') {
        idcombo = comboDestino;
        porDefecto = deptoDefecto;
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp',
                data: valores_a_mandar,
                contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
                beforeSend: function () {

                },
                success: function (data) {
                    respuestacargarComboGRAL_1(data)
                    resolve()
                }
            })
        })
    } else {

        if (comboDestino == 'cmbIdRango') {
            asignaAtributoCombo2('cmbIdRango', '', '[ Seleccione ]');
        } else if (comboDestino == 'cmbMotivoConsultaClase') {
            asignaAtributoCombo2('cmbMotivoConsultaClase', '', '[ Seleccione ]');
        }
    }

}
/**
 //funcion actualizada de respuestacargarComboGRAL()
 * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>  
*/
function respuestacargarComboGRAL_1(data) {
    raiz = data.documentElement;
    if ($("#" + idcombo).length) {
        $("#" + idcombo).empty();
    }

    c = raiz.getElementsByTagName('id');
    n = raiz.getElementsByTagName('nom');
    d = raiz.getElementsByTagName('title');

    var selected = false;
    if (porDefecto === '') {
        $("#" + idcombo).append(new Option("[ SELECCIONE ]", "", true, true));
        selected = true;
    } else {
        $("#" + idcombo).append(new Option("[ SELECCIONE ]", ""));
    }

    if (c.length > 0) {
        if (c.length === 1) {
            if (selected) {
                $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data));
                $("#" + idcombo).find('option[value="' + c[0].firstChild.data + '"]').attr('title', 'DEFINICION : ' + d[0].firstChild.data);
            } else {
                $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data, true, true));
                $("#" + idcombo).find('option[value="' + c[0].firstChild.data + '"]').attr('title', 'DEFINICION : ' + d[0].firstChild.data);
            }
        } else {
            for (var i = 0; i < c.length; i++) {
                if (c[i].firstChild.data == porDefecto) {
                    $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data, true, true));
                    $("#" + idcombo).find('option[value="' + c[i].firstChild.data + '"]').attr('title', 'DEFINICION : ' + d[i].firstChild.data);
                } else {
                    $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data));
                    $("#" + idcombo).find('option[value="' + c[i].firstChild.data + '"]').attr('title', 'DEFINICION : ' + d[i].firstChild.data);
                }
            }
        }
    }
}

async function respuestacargarComboGRALAsync() {
    return new Promise((resolve, reject) => {
        if (varajax1.readyState == 4) {
            if (varajax1.status == 200) {
                raiz = varajax1.responseXML.documentElement;
                if (document.getElementById(idcombo) != null) { document.getElementById(idcombo).options.length = 1; }
                c = raiz.getElementsByTagName('id');
                n = raiz.getElementsByTagName('nom');
                d = raiz.getElementsByTagName('title');

                selected = false;
                $("#" + idcombo).empty();
                if (porDefecto == '') {
                    $("#" + idcombo).append(new Option("[ SELECCIONE ]", "", true, true));
                    selected = true;
                } else {
                    $("#" + idcombo).append(new Option("[ SELECCIONE ]", ""));
                }

                if (c.length > 0) {
                    if (c.length <= 1) {
                        if (selected) {
                            $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data));
                            document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                        } else {
                            $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data, true, true));
                            document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                        }
                    } else {
                        for (i = 0; i < c.length; i++) {
                            if (c[i].firstChild.data == porDefecto) {
                                $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data, true, true));
                                document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                            } else {
                                $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data));
                                document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                            }
                        }
                    }

                }
                resolve();  // Resuelve la promesa cuando la operación asíncrona se completa
            } else {
                swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                reject();  // Rechaza la promesa en caso de error
            }
        }
        if (varajax1.readyState == 1) {
            // document.getElementById('inicial').innerHTML = crearMensaje();
        }
    });
}

function respuestacargarComboGRAL() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            if (document.getElementById(idcombo) != null) { document.getElementById(idcombo).options.length = 1; }
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');

            selected = false;
            $("#" + idcombo).empty();
            if (porDefecto == '') {
                $("#" + idcombo).append(new Option("[ SELECCIONE ]", "", true, true));
                selected = true;
            } else {
                $("#" + idcombo).append(new Option("[ SELECCIONE ]", ""));
            }

            if (c.length > 0) {
                if (c.length <= 1) {
                    if (selected) {
                        $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data));
                        document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                    } else {
                        $("#" + idcombo).append(new Option(n[0].firstChild.data, c[0].firstChild.data, true, true));
                        document.getElementById(idcombo).options[1].title = 'DEFINICION : ' + d[0].firstChild.data;
                    }
                } else {
                    for (i = 0; i < c.length; i++) {
                        if (c[i].firstChild.data == porDefecto) {
                            $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data, true, true));
                            document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                        } else {
                            $("#" + idcombo).append(new Option(n[i].firstChild.data, c[i].firstChild.data));
                            document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                        }
                    }
                }

            }

            /*if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                    document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                }

                if (idcombo == 'cmbRefCampo') {
                    $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
                } else {
                    document.getElementById(idcombo).options[0] = new Option('[ Seleccione ]', '', true, false);
                    $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
                }
            } else {
                document.getElementById(idcombo).options[0] = new Option('[ Seleccione ]', '', true, false);
            }*/
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarLabelPlanContratacionAdmisiones(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionAdmisiones(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarLabelPlanContratacionEmpresa(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, condicion4, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=cuatro&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionEmpresa(idLabelDestino, nomLabelDestino) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarLabelPlanContratacionListaEspera(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionListaEspera(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarLabelPlanContratacionAgenda(idLabelDestino, nomLabelDestino, condicion1, condicion2, condicion3, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelPlanContratacionAgenda(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}



function respuestacargarLabelPlanContratacionAdmisiones(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)
            asignaAtributo('txtDiagnostico', raiz.getElementsByTagName('title')[0].firstChild.data)

            if (valorAtributo('cmbIdTipoServicio') == 'CIR') {
                buscarAGENDA('listADMISIONESCirugiaProcedimientoTraer')
            } else {
                buscarAGENDA('listAdmisionCEXProcedimientoTraer')
            }



        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function respuestacargarLabelPlanContratacionEmpresa(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)
            asignaAtributo('txtDiagnostico', raiz.getElementsByTagName('title')[0].firstChild.data)
            if (valorAtributo('cmbIdTipoServicio') == 'CIR') {
                buscarAGENDA('listADMISIONESCirugiaProcedimientoTraer')
            } else {
                buscarAGENDA('listAdmisionCEXProcedimientoTraer')
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function respuestacargarLabelPlanContratacionListaEspera(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);

            let id = ''
            let nom = ''
            try {
                id = raiz.getElementsByTagName('id')[0].firstChild.data                
            } catch (error) {
            }
            try {
                nom = raiz.getElementsByTagName('nom')[0].firstChild.data                
            } catch (error) {
                
            }

            asignaAtributo(idLabelDestino, id)
            asignaAtributo(nomLabelDestino, nom)

            buscarAGENDA('listEsperaCEXProcedimientoTraer')

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}


function respuestacargarLabelPlanContratacionAgenda(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)


            if (valorAtributo('txt_banderaOpcionCirugia') == 'OK') {
                setTimeout("buscarAGENDA('listProcedimientosAgendaCirugia')", 800);

            } else {
                buscarAGENDA('listProcedimientosAgenda')
                setTimeout("buscarAGENDA('listCitaCexProcedimientoTraer')", 800)
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) { }
}




function cargarLabelCondicion2(idLabelDestino, nomLabelDestino, condicion1, condicion2, idQueryCombo) {
    idcombo = idLabelDestino;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=2&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargarLabelCondicion2(idLabelDestino, nomLabelDestino) };

    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}


function respuestacargarLabelCondicion2(idLabelDestino, nomLabelDestino) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino);
            limpiaAtributo(nomLabelDestino);
            asignaAtributo(idLabelDestino, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(nomLabelDestino, raiz.getElementsByTagName('nom')[0].firstChild.data)

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


function cargarElementoCondicionDosCodigoBarras(elementoDestino, idQueryCombo, condicion1, condicion2) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementoGRALCodigoBarras;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarElementoGRALCodigoBarras() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');


            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                //

                if ($('#txtIdBarCode').length) {
                    verificarNotificacionAntesDeGuardar('existenciaBodegaOptica');
                    // si existe
                } else {
                    // no existe
                    codigoBarras();
                }


            } else {

                alert('CODIGO NO REGISTRADO !!')
                //cargarMenu('<%= response.encodeURL("suministros/articulo/codigoBarras.jsp")%>','9-4-0-22','codigoBarras','codigoBarras','s','s','s','s','s','s');
                /*if ($('#txtCodBarrasBus').length) {		  
                    limpiaAtributo('txtCodBarrasBus',0);

                }*/
                limpiaAtributo('txtIdBarCode', 0);
                limpiaAtributo('txtCodBarrasBus', 0);
                asignaAtributo('txtCodBarrasBus', '', 0)


            }
        } else {
            //swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            document.getElementById('txtCodBarrasBus').focus()
        }
    }


}



function cargarElementoCondicionCodigoBarrasVentas(elementoDestino, idQueryCombo, condicion1, condicion2, condicion3) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarCodigoBarrasVentas;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}






function respuestacargarCodigoBarrasVentas() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');


            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                console.log(c[0].firstChild.data)

                if ($('#txtIdCodigoBarrasVentas').length) {
                    verificarNotificacionAntesDeGuardar('existenciaBodegaVentas');
                    // si existe
                } else {
                    // no existe
                    codigoBarrasVentas();
                }

            } else {
                alert('CODIGO NO REGISTRADO !!')
                limpiaAtributo('txtIdCodigoBarrasVentas', 0);
                limpiaAtributo('lblDatosCodigoBarraVentas', 0);


            }
        } else {
            document.getElementById('txtIdCodigoBarrasVentas').focus()
        }
    }


}






function cargarElementCondicionDosCodigoBarras(elementoDestino, idQueryCombo, condicion1, condicion2) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementGRALCodigoBarras;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarElementGRALCodigoBarras() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');
            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                respuestaVSerial();
            } else {

                alert('CODIGO NO REGISTRADO !!')

            }
        } else {
            //swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            document.getElementById('txtCodBarrasBus').focus()
        }
    }
}

function cargarElementCondicionDosCodBarras(elementoDestino, idQueryCombo, condicion1, condicion2) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementGRALCodBarras;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarElementGRALCodBarras() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');
            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data)
                respuestaVSerialMod(); //
            } else {
                alert('CODIGO NO REGISTRADO !!')
            }
        } else {
            //swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            document.getElementById('txtCodBarrasBus').focus()
        }
    }


}
//

function cargarElementoCondicionUno(elementoDestino, idQueryCombo, condicion1) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementoGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarElementoCondicionDos(elementoDestino, idQueryCombo, condicion1, condicion2) {
    if (condicion1 != '' && condicion2 != '') {
        idcombo = elementoDestino;
        porDefecto = '';
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarElementoGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    }
}


function cargarElementoCondicionTres(elementoDestino, idQueryCombo, condicion1, condicion2, condicion3) {
    if (condicion1 != '' && condicion2 != '' && condicion3 != '') {
        idcombo = elementoDestino;
        porDefecto = '';
        valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=tres&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3;
        varajax1 = crearAjax();
        varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
        varajax1.onreadystatechange = respuestacargarElementoGRAL;
        varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajax1.send(valores_a_mandar);
    }
}

function cargarElementoCondicionCinco(elementoDestino, idQueryCombo, condicion1, condicion2, condicion3, condicion4, condicion5) {
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=5&condicion1=" + condicion1 + "&condicion2=" + condicion2 + "&condicion3=" + condicion3 + "&condicion4=" + condicion4 + "&condicion5=" + condicion5
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarElementoGRAL;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargarElementoGRAL() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            //document.getElementById(idcombo).options.length=1;	
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');

            // alert(c.length);
            if (c.length > 0) {
                asignaAtributo(idcombo, c[0].firstChild.data, 0)

            } else {
                alert('Problema al cargar respuestacargarElementoGRAL..')
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


function cargarComboGRALCondicionOrdenesMedicaVia(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRALOrdenesMedicaVia;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
    return true;
}

function respuestacargarComboGRALOrdenesMedicaVia() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');

            // alert(c.length);
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                }
            }
            if (c.length > 1) {
                document.getElementById(idcombo).options[0] = new Option('', '', true, false);
                $("#" + idcombo + " option[value='']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
            }
            // cargarComboGRALCondicionOrdenesMedicaDosis('txtIdArticulo', 'xxx', 'cmbIdTipoDosis', 51, valorAtributoIdAutoCompletar('txtIdArticulo')) 

        } else swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

function cargarComboGRALCondicionOrdenesMedicaDosis(cmbPadre, deptoDefecto, comboDestino, idQueryCombo, condicion1) {
    idcombo = comboDestino;
    porDefecto = deptoDefecto;
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=1&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacargarComboGRALOrdenesMedicaDosis;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
    return true;
}

function respuestacargarComboGRALOrdenesMedicaDosis() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');


            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                }
                $("#" + idcombo + " option[value='" + porDefecto + "']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
            }
            if (c.length > 1) {
                document.getElementById(idcombo).options[0] = new Option('', '', true, false);
                $("#" + idcombo + " option[value='']").attr('selected', 'selected'); // pa que quede seleccionado el id combo
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}




//******************************************************************************************


function cargarEAHospitalConClasePadre(cmbClaseEA, deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + $('#drag' + ventanaActual.num).find('#' + cmbClaseEA).val() + '&idProceso=' + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarEAHospitalConClase;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarEAHospitalConClase(cmbProceso, deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre + '&idProceso=' + $('#drag' + ventanaActual.num).find('#' + cmbProceso).val();
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarEAHospitalConClase;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargarEAHospitalConClaseEAeIncidentes(cmbProceso, deptoDefecto, comboDep, codPadre, pagina) { //LAMADO DESDE buscarJuan ->administrarEventoAdverso
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=00" + '&idProceso=00';
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarEAHospitalConClase;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarEAHospitalConClase() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            // alert(c.length);
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'DEFINICION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


function cargarAspectosAMejorar(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarAspectosAMejorar;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarAspectosAMejorar() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda acci�n que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'INFORMACION : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[Seleccione Evento]', '00', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}
/*en reportar eventoAdverso, partiendo de que se escoja la unidad de servicio donde se encuentre el paciente*/
function cargarPaciente(deptoDefecto, comboDep, codPadre, pagina) {
    idcombo = comboDep;
    porDefecto = deptoDefecto;
    valores_a_mandar = "codComboPadre=" + codPadre;
    varajax1 = crearAjax();
    varajax1.open("POST", pagina, true);
    varajax1.onreadystatechange = respuestaCargarPaciente;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestaCargarPaciente() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            document.getElementById(idcombo).options.length = 1;
            c = raiz.getElementsByTagName('cod');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('defini');
            t = raiz.getElementsByTagName('tratam');
            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (c[i].firstChild.data == porDefecto) {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda PACIENTE que pueda...', true, true);
                    } else {
                        document.getElementById(idcombo).options[(document.getElementById(idcombo).options.length)] = new Option(n[i].firstChild.data, c[i].firstChild.data, 'se refiere a toda PACIENTE que pueda...', true, false);
                        document.getElementById(idcombo).options[i + 1].title = 'ANOTACI�N : ' + d[i].firstChild.data;
                    }

                }
                document.getElementById(idcombo).options[0] = new Option('[SELECCIONE PACIENTE]', '00', true, false);
                document.getElementById(idcombo).options[0].selected = 'selected'; //para que quede seleccionado este elemento 00
            } else {
                document.getElementById(idcombo).options[0] = new Option('[SELECCIONE PACIENTE]', '00', true, false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}






///funiconesd adciones Angelica

function verificarfechaturno(valor, divdatos, id, numid, pagina) {
    // alert(valor+" "+id+" "+numid+" "+pagina);
    if (document.getElementById(valor).value != "") {
        tid = document.getElementById(id).innerHTML;
        nid = document.getElementById(numid).innerHTML;
        dia = document.getElementById(valor).value;
        $('#drag' + ventanaActual.num).find('#divContenido').css('height', '350px');
        mostrar(divdatos);
        valores_a_mandar = "id=" + tid + "&nid=" + nid + "&diat=" + dia;
        varajax = crearAjax();
        varajax.open("POST", pagina, true);
        varajax.onreadystatechange = respuestaVerificarfechaturno;
        varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        varajax.send(valores_a_mandar);
    }
}



function limpiarCampos(divdatos, fechaturno) {
    ocultar(divdatos);
    $('#drag' + ventanaActual.num).find('#divContenido').css('height', '200px');
    document.getElementById(fechaturno).value = "";
}


function calcularDuracionCita(txtCantidadP, txtDuracionC, horafin, horainicio, minfin, mininicio, ampminicio, ampmfin) {
    ban = 0;
    //alert("hola"+document.getElementById(horainicio).value+"  "+document.getElementById(horafin).value);		 
    if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'A.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'P.M' && document.getElementById(ampmfin).value == 'P.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'P.M') {
        horas1 = 12 - parseInt(document.getElementById(horainicio).value);
        horas = parseInt(horas1) + parseInt(document.getElementById(horafin).value);
        ban = 1;
    }
    if (ban == 1) {
        minutos = parseInt(document.getElementById(minfin).value) + parseInt(document.getElementById(mininicio).value);
        totalminutos = (parseInt(horas) * 60) + parseInt(minutos);
        duracioncita = parseInt(totalminutos) / parseInt(document.getElementById(txtCantidadP).value);
        if (duracioncita < 0) {
            duracioncita = duracioncita * (-1);
        }
        document.getElementById(txtDuracionC).value = duracioncita;
        vec = document.getElementById(txtDuracionC).value.split(".");
        document.getElementById(txtDuracionC).value = vec[0];
    }
}

function calcularCantidadPacientes(txtCantidadP, txtDuracionC, horafin, horainicio, minfin, mininicio, ampminicio, ampmfin) {
    ban1 = 0;
    if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'A.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban1 = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'P.M' && document.getElementById(ampmfin).value == 'P.M') {
        if (document.getElementById(horainicio).value < document.getElementById(horafin).value) {
            horas = document.getElementById(horafin).value - document.getElementById(horainicio).value;
            ban1 = 1;
        }
    } else if (document.getElementById(ampminicio).value == 'A.M' && document.getElementById(ampmfin).value == 'P.M') {
        horas1 = 12 - parseInt(document.getElementById(horainicio).value);
        horas = parseInt(horas1) + parseInt(document.getElementById(horafin).value);
        ban1 = 1;
    }
    if (ban1 == 1) {
        minutos = parseInt(document.getElementById(minfin).value) + parseInt(document.getElementById(mininicio).value);
        totalminutos = (parseInt(horas) * 60) + parseInt(minutos);
        duracioncita = parseInt(totalminutos) / parseInt(document.getElementById(txtDuracionC).value);
        if (duracioncita < 0) {
            duracioncita = duracioncita * (-1);
        }
        document.getElementById(txtCantidadP).value = duracioncita;
        vec = document.getElementById(txtCantidadP).value.split(".");
        document.getElementById(txtCantidadP).value = vec[0];
    }
}


function LlenarServicios(tipoid, identificacion, pagina) {
    valores_a_mandar = "tipoid=" + tipoid + "&identificacion=" + identificacion;
    varajax = crearAjax();
    varajax.open("POST", pagina, true);
    varajax.onreadystatechange = respuestaLlenarServicios;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}


function respuestaLlenarServicios() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            codigos = raiz.getElementsByTagName('codigos');
            nombres = raiz.getElementsByTagName('nombres');
            if (codigos.length > 0) {
                for (i = 0; i < codigos.length; i++) {
                    document.getElementById('cmbTipoServ1').options[(document.getElementById('cmbTipoServ1').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ2').options[(document.getElementById('cmbTipoServ2').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ3').options[(document.getElementById('cmbTipoServ3').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ4').options[(document.getElementById('cmbTipoServ4').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                    document.getElementById('cmbTipoServ5').options[(document.getElementById('cmbTipoServ5').options.length)] = new Option(nombres[i].firstChild.data, codigos[i].firstChild.data, true, false);
                }
            } else {
                //document.getElementById(idcombo).options[0]=new Option('[Seleccione un municipio]','',true,false);
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}


/* Funciones de citologia para las opciones de los botones */
function verificaropcion(boton) {
    if (boton == 'chediu' || boton == 'cheano') {
        document.getElementById('txtCualp').disabled = true;
        document.getElementById('txtCualp').value = "";
    }
    if (boton == 'cheotro') {
        document.getElementById('txtCualp').disabled = false;
    }
}

function verificaropcion1(boton) {
    if (boton == 'checkHiste' || boton == 'checkRadioT' || boton == 'checkTto') {
        document.getElementById('txtCualPro').disabled = true;
    }
    if (boton == 'checkotro') {
        document.getElementById('txtCualPro').disabled = false;
    }
    if (document.getElementById('checkotro').checked == false) {
        document.getElementById('txtCualPro').disabled = true;
        document.getElementById('txtCualPro').value = "";
    }
}
/*
function verificarbotonesr(boton){
    if(boton=='cheSatisfactorio'){
     document.getElementById('chePresentes').checked=true; 	
     document.getElementById('txtMuestraPro').disabled=true;  
     document.getElementById('txtMuestraRech').disabled=true;
     document.getElementById('txtMuestraPro').value="";  
     document.getElementById('txtMuestraRech').value="";
    }
    if(boton=='checkNegativa'){
      document.getElementById('chePresentes').checked=false;
      document.getElementById('chesinErupcionars').checked=false;
      document.getElementById('txtMuestraPro').disabled=false;  
      document.getElementById('txtMuestraRech').disabled=false;
    }
    if(boton=='chesinErupcionars' || boton=='chePresentes'){
        if(document.getElementById('cheSatisfactorio').checked==false){
           document.getElementById('cheSatisfactorio').checked=true;
        }		
    }	
  }*/

function verificarbotonescamb(boton) {
    if (boton == 'checkCambiosCelulares') {
        if (document.getElementById('checkCambiosCelulares').checked == true) {
            document.getElementById('checkInflamacion').checked = true;
            document.getElementById('cheL').checked = true;
        }
        if (document.getElementById('checkCambiosCelulares').checked == false) {
            document.getElementById('checkInflamacion').checked = false;
            document.getElementById('checkRadiacion').checked = false;
            document.getElementById('checkContracepcion').checked = false;
            document.getElementById('cheL').checked = false;
            document.getElementById('cheM').checked = false;
            document.getElementById('cheS').checked = false;
        }
    }
    if (boton == 'cheL' || boton == 'cheM' || boton == 'cheS') {
        if (document.getElementById('checkInflamacion').checked == false) {
            document.getElementById('checkInflamacion').checked = true;
        }
        if (document.getElementById('checkCambiosCelulares').checked == false) {
            document.getElementById('checkCambiosCelulares').checked = true;
        }

    }
    if (boton == 'checkInflamacion') {
        if (document.getElementById('checkInflamacion').checked == false) {
            if (document.getElementById('cheL').checked == true || document.getElementById('cheM').checked == true || document.getElementById('cheS').checked == true) {
                document.getElementById('cheL').checked = false;
                document.getElementById('cheM').checked = false;
                document.getElementById('cheS').checked = false;
            }
        }
        if (document.getElementById('checkInflamacion').checked == true) {
            if (document.getElementById('cheL').checked == false && document.getElementById('cheM').checked == false && document.getElementById('cheS').checked == false) {
                document.getElementById('cheL').checked = true;
            }
        }
    }

    if (boton == 'checkInflamacion' || boton == 'checkRadiacion' || boton == 'checkContracepcion') {
        if (document.getElementById('checkCambiosCelulares').checked == false) {
            document.getElementById('checkCambiosCelulares').checked = true;
        }
    }
    if (document.getElementById('checkInflamacion').checked == false && document.getElementById('checkRadiacion').checked == false && document.getElementById('checkContracepcion').checked == false) {
        document.getElementById('checkCambiosCelulares').checked = false;
    }

}



function getValorRadio(name) {
    if (document.getElementsByName(name) != null) {
        var inputs = document.getElementsByName(name);
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked == true) return inputs[i].value;
        }
    }
    return null;
}

/// --------------------------para cambiar usuarios del sistema con todo y roles juan

function verificarContrasenaAxiliar(param1) {

    varajaxReporte = crearAjax();
    valores_a_mandar = "accion=verificarContrasena";
    valores_a_mandar = valores_a_mandar + "&contrasena=" + param1;
    varajaxReporte.open("POST", '/clinica/paginas/accionesXml/verificarInformacionAuxiliar_xml.jsp', true);
    varajaxReporte.onreadystatechange = llenarverificarContrasenaAxiliar;
    varajaxReporte.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxReporte.send(valores_a_mandar);
}
var _verificacionInformacionAx2;

function llenarverificarContrasenaAxiliar() {
    if (varajaxReporte.readyState == 4) {
        //	VentanaModal.cerrar();
        if (varajaxReporte.status == 200) {
            raiz = varajaxReporte.responseXML.documentElement;
            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data != '') {

                // alert('IDENTIFICACION DEL AUXILIAR = '+raiz.getElementsByTagName('respuesta')[0].firstChild.data)	
                asignaAtributo('lblIdAuxiliar', raiz.getElementsByTagName('respuesta')[0].firstChild.data)
                ocultar('divAuxiliar')


            } else {
                alert("NO EXISTE USUARIO PARA ESTA CONTRASE�A");

                document.getElementById('txtContrasenaAdm').focus()
                asignaAtributo('txtContrasenaAdm', '', 0)
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxReporte.readyState == 1) {
        //document.getElementById('listado').innerHTML =	crearMensaje();
        //	 abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}
/// --------------------------para cambiar usuarios del sistema con todo y roles juan

function verificarContrasenaAx(param1, param2, pagina) {

    varajaxReporte = crearAjax();
    valores_a_mandar = "accion=verificarContrasena";
    valores_a_mandar = valores_a_mandar + "&login=" + param1;
    valores_a_mandar = valores_a_mandar + "&contrasena=" + param2;
    varajaxReporte.open("POST", '/clinica/paginas/accionesXml/verificarInformacion_xml.jsp', true);
    varajaxReporte.onreadystatechange = llenarinfoVerificaContrasenaAx;
    varajaxReporte.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxReporte.send(valores_a_mandar);
}
var _verificacionInformacionAx;

function llenarinfoVerificaContrasenaAx() {
    //
    _verificacionInformacionAx = false;
    //
    if (varajaxReporte.readyState == 4) {
        //	VentanaModal.cerrar();
        if (varajaxReporte.status == 200) {
            raiz = varajaxReporte.responseXML.documentElement;
            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                switch (paginaActual) {
                    case 'usuarios':
                        //alert("verificacion correcta contrase�a ");
                        // se sobreentiende
                        break;
                    default:
                        //alert("verificacion correcta ");
                        break;
                }
                _verificacionInformacionAx = true;

            } else {
                alert(" Su contrase�a es erronea .");
                _verificacionInformacionAx = false;
            }
            //cargarFocos();
            //if(focos.length>0)
            //document.getElementById(focos[1]).focus();

            if (_verificacionInformacionAx) {
                //ocultar('divcontrasena');
                //mostrar('contenido');
                $('#drag' + ventanaActual.num + ' #divBuscarUsua').hide();
                $('#drag' + ventanaActual.num + ' #divEditarUsua').show();
                $('#drag' + ventanaActual.num + ' #divContenidoUsua').css('height', '200px');
                //$('txtId').focus();
            } else {
                //alert('Su contrase�a es incorrecta');
                $('txtContrasenaAdm').value = '';
                $('txtContrasenaAdm').focus();
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxReporte.readyState == 1) {
        //document.getElementById('listado').innerHTML =	crearMensaje();
        //	 abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}

function consultarNombrePersona(codPersona, tipoid, pagina) {
    codPersona = valorAtributo('txtId');
    if (codPersona != '' && tipoid != '00') {
        varajaxReporte = crearAjax();
        valores_a_mandar = "id=" + codPersona;
        valores_a_mandar = valores_a_mandar + "&tipoId=" + tipoid;
        varajaxReporte.open("POST", pagina, true);
        varajaxReporte.onreadystatechange = llenarinfoNombrePersona;
        varajaxReporte.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxReporte.send(valores_a_mandar);
    }
}


/****** Ajax para leer nombre persona con identificacion *****/
function llenarinfoNombrePersona() {
    //
    var esLaPersona; //
    if (varajaxReporte.readyState == 4) {
        //	VentanaModal.cerrar();
        if (varajaxReporte.status == 200) {
            raiz = varajaxReporte.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nombres');


            if (c.length > 0) {
                for (i = 0; i < c.length; i++) {
                    if (i == 1) {
                        esLaPersona = " Parece existir mas de una persona ";
                        document.getElementById('txtId').focus(); //por siaca
                    } else {
                        esLaPersona = raiz.getElementsByTagName('nombres')[i].firstChild.data;
                        document.getElementById('txtLogin').focus();
                        $('#drag' + ventanaActual.num).find('#txtLogin').val(raiz.getElementsByTagName('login')[i].firstChild.data);
                        $('#drag' + ventanaActual.num).find('#txtContrasena').val(raiz.getElementsByTagName('contrasena')[i].firstChild.data);
                        $('#drag' + ventanaActual.num).find('#txtContrasena2').val(raiz.getElementsByTagName('contrasena')[i].firstChild.data);
                        $("#cmbRol option[value='" + $.trim(raiz.getElementsByTagName('rol')[i].firstChild.data) + "']").attr('selected', 'selected');
                        $("#cmbEstado option[value='" + $.trim(raiz.getElementsByTagName('estado')[i].firstChild.data) + "']").attr('selected', 'selected');

                    }
                }
            } else {
                esLaPersona = '[No existe ninguna persona con esa identificacion y tipo de id]';
                document.getElementById('txtId').focus();
            }

            $('#drag' + ventanaActual.num).find('#txtLogin').html(esLaPersona);
            $('#drag' + ventanaActual.num).find('#txtnomPersona').html(esLaPersona);

            //  document.getElementById('nomPersona').innerHTML = esLaPersona;
            //cargarFocos();
            //actualizarMensajeReporte();
            //if(focos.length>0)
            //document.getElementById(focos[1]).focus();
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxReporte.readyState == 1) {
        //document.getElementById('listado').innerHTML =	crearMensaje();
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}



function cargarUsuario(tipoId, id) {
    valores_a_mandar = "accion=consultaUsuario";
    valores_a_mandar = valores_a_mandar + "&tipoId=" + tipoId;
    valores_a_mandar = valores_a_mandar + "&id=" + id;
    varajax = crearAjax();
    varajax.open("POST", '/clinica/paginas/accionesXml/cargarPersonas_xml.jsp', true);
    varajax.onreadystatechange = respuestaCargarUsuario;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}


function respuestaCargarUsuario() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;

            $('#drag' + ventanaActual.num).find('#lblNomPersona').empty();
            $('#drag' + ventanaActual.num).find('#txtLogin').val('');
            $('#drag' + ventanaActual.num).find('#hddId').val('');
            $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val('');
            $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
            $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
            $("#cmbRol option[value='']").attr('selected', 'selected');

            tipoId = raiz.getElementsByTagName('tipoId')[0].firstChild.data;
            id = raiz.getElementsByTagName('id')[0].firstChild.data;
            nombres = raiz.getElementsByTagName('nombres')[0].firstChild.data;
            login = raiz.getElementsByTagName('login')[0].firstChild.data;
            contrasena = raiz.getElementsByTagName('contrasena')[0].firstChild.data;
            rol = $.trim(raiz.getElementsByTagName('rol')[0].firstChild.data);
            estadoCuenta = $.trim(raiz.getElementsByTagName('estado')[0].firstChild.data);
            cuantos = raiz.getElementsByTagName('cuantos')[0].firstChild.data;

            if (cuantos == 1) { //si esta en hoja de vida y tiene cuenta de usuario
                $('#drag' + ventanaActual.num).find('#hddId').val(tipoId);
                $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val(id);
                $('#drag' + ventanaActual.num).find('#lblNomPersona').append(nombres);
                $('#drag' + ventanaActual.num).find('#txtLogin').val(login);
                $('#drag' + ventanaActual.num).find('#txtContrasena').val(contrasena);
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val(contrasena);

                $("#cmbRol option[value='" + rol + "']").attr('selected', 'selected');
                $("#cmbEstado option[value='" + estadoCuenta + "']").attr('selected', 'selected');
            } else if (cuantos == 2) { //si esta en hoja de vida pero NO tiene cuenta de usuario	   
                $('#drag' + ventanaActual.num).find('#hddId').val(tipoId);
                $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val(id);
                $('#drag' + ventanaActual.num).find('#lblNomPersona').empty();
                $('#drag' + ventanaActual.num).find('#txtLogin').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $("#cmbRol option[value='']").attr('selected', 'selected');
                $("#cmbEstado option[value='1']").attr('selected', 'selected');

                $('#drag' + ventanaActual.num).find('#lblNomPersona').append(nombres);
                alert(nombres + " No posee cuenta en este Sistama");
            } else if (cuantos == 0) {
                $('#drag' + ventanaActual.num).find('#lblNomPersona').empty();
                $('#drag' + ventanaActual.num).find('#txtLogin').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $("#cmbRol option[value='']").attr('selected', 'selected');
                $("#cmbEstado option[value='1']").attr('selected', 'selected');

                alert("Empleado no se encuentra registrado en las hojas de vida de la Instituci�n, o est� Inactivo");
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) { }
}




function sacarElMaximo(arg) {
    var ids = jQuery("#" + arg).getDataIDs();
    cant = ids.length;
    var maxim = 0;

    for (var i = 0; i < ids.length; i++) {
        var datosDelRegistro = jQuery("#" + arg).getRowData(ids[i]);


        if (datosDelRegistro.idRegistro > maxim)
            maxim = datosDelRegistro.idRegistro;
        else maxim = maxim;


    }
    maximo = parseInt(maxim);
    maximo = (maximo + 1);
    return maximo;
}


function reglaRutasCirugia() {
    var ids = jQuery("#listCitaCirugiaProcedimiento").getDataIDs();
    var idEps = valorAtributo('lblIdEpsPlan');


    if (ids.length == 1) { /*SI SOLO ES UN PROCEDIMIENTO*/
        var c = ids[0];
        var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);

        if (datosRow.Contratado == 'SI') {
            if (datosRow.lateralidad == 'U') {

                alert('A. PARA UN SOLO PROCEDIMIENTO UNILATERAL CONTRATADO \n DEBE FACTURARSE DIRECTAMENTE');
                return false;

            }
            if (datosRow.lateralidad == 'B') {
                if (datosRow.Contratado == 'SI') {

                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('B. PARA UN SOLO PROCEDIMIENTO BILATERAL CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }

                }
            }
        } else {
            if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                alert('A1. PARA UN SOLO PROCEDIMIENTO UNILATERAL O BILATERAL NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                return false;
            }
        }
    } else { /*DOS O MAS PROCEDIMIENTO*/
        contUnilater = 0;
        contBilater = 0;
        todosContratados = 0;
        validaEpsIdProcedimiento = 0;
        for (var i = 0; i < ids.length; i++) {
            var c = ids[i];
            var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);
            if (datosRow.lateralidad == 'U') {
                contUnilater = contUnilater + 1;
            }
            if (datosRow.lateralidad == 'B') {
                contBilater = contBilater + 1;
            }
            if (datosRow.Contratado == 'SI') {
                todosContratados = todosContratados + 1;
            }

        }


        for (var i = 0; i < ids.length; i++) { /*UNA DETERMINADA EPS CON UNOS PROCEDIMIENTOS*/
            var c = ids[i];
            var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);

            if (datosRow.idProcedimiento == '147101' || datosRow.idProcedimiento == '147404' || datosRow.idProcedimiento == '147401' || datosRow.idProcedimiento == '147402' || datosRow.idProcedimiento == '147403') {
                if (idEps == '5')
                    validaEpsIdProcedimiento = 1;
                //  alert('algun procedimiento es de estos')
            }
        }

        if (validaEpsIdProcedimiento == 0) {

            alert('todosContratados=' + todosContratados + ' contBilater=' + contBilater + ' contUnilater=' + contUnilater + ' lengt=' + ids.length);

            if (todosContratados == ids.length) { /*TODOS SON CONTRATADOS*/
                if (contUnilater == ids.length) {
                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('C. EXISTE MAS DE UN PROCEDIMIENTO CON DIFERENTE VIA CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contBilater == ids.length) {
                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('D. EXISTE MAS DE UN PROCEDIMIENTO BILATERALES Y CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contUnilater < ids.length && contUnilater > 0) {
                    if (idEps == '10' || idEps == '26' || idEps == '27' || idEps == '29' || idEps == '39' || idEps == '13' || idEps == '50' || idEps == '57' || idEps == '15' || idEps == '3' || idEps == '5') {
                        alert('E. EXISTE MAS DE UN PROCEDIMIENTO UNILATERAL O BILATERALES CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\n DEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
            } else if (todosContratados == 0) { /*TODOS SON NO CONTRATADOS*/
                alert('TODOS SON NO CONTRATADOS....')
                if (contUnilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('C1. EXISTE MAS DE UN PROCEDIMIENTO CON DIFERENTE VIA CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contBilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('D1. EXISTE MAS DE UN PROCEDIMIENTO BILATERALES Y CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contUnilater < ids.length && contUnilater > 0) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('E1. EXISTE MAS DE UN PROCEDIMIENTO UNILATERAL O BILATERALES CONTRATADOS PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\n DEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }

            } else { /*CONTRATADOS Y NO CONTRATADOS*/
                alert('MIX')

                if (contUnilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('C1.1 EXISTE MAS DE UN PROCEDIMIENTO CON DIFERENTE VIA CONTRATADO Y NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contBilater == ids.length) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('D1.1 EXISTE MAS DE UN PROCEDIMIENTO BILATERALES Y CONTRATADOS Y NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\nDEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
                if (contUnilater < ids.length && contUnilater > 0) {
                    if (idEps == '1' || idEps == '3' || idEps == '26' || idEps == '13' || idEps == '50' || idEps == '52' || idEps == '57' || idEps == '15') {
                        alert('E1.1 EXISTE MAS DE UN PROCEDIMIENTO UNILATERAL O BILATERALES CONTRATADOS Y NO CONTRATADO PARA LA EPS ' + $('#drag' + ventanaActual.num).find('#txtAdministradora1').val().trim() + '\n DEBE FACTURARSE DIRECTAMENTE');
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

function verificarCamposDuplicadoGrilla(arg,nuevaFila) {


    var datosFilas = jQuery('#drag' + ventanaActual.num).find('#' + arg).jqGrid('getRowData');
    var existeRepetido = false;

    // Iterar sobre las filas existentes
    $.each(datosFilas, function(index, filaExistente) {
        if (
            nuevaFila.edad_minima === filaExistente.EDAD_MIN &&
            nuevaFila.edad_maxima === filaExistente.EDAD_MAX &&
            nuevaFila.id_citas_tipo === filaExistente.ID_TIPO_CITA
        ) {
            existeRepetido = true;
            return false; // Salir del bucle cuando se encuentra un duplicado
        }
    });

    return existeRepetido;
        
}

function verificarCamposGuardar(arg) {
    switch (arg) {
        case 'agregarRangoEdad':
            console.log('agregarRangoEdad');
            let mensaje = ''
            let nuevaFila = {
                edad_minima: valorAtributo('txtIdCheckMesesPerodicidadMin'),
                edad_maxima: valorAtributo('txtIdCheckMesesPerodicidadMax'),
                id_citas_tipo: traerDatoFilaSeleccionada('listGrillaTipoCita','ID')
            }
            if (verificarCamposDuplicadoGrilla('listEdadMinMax',nuevaFila)) {
                swAlert('info','Los datos ya existen para el tipo de cita.','Datos Repetidos')
                return false
            }  

            if (valorAtributo('txtIdCheckMesesPerodicidadMin') === '' || valorAtributo('txtIdCheckMesesPerodicidadMax') === '') {
                mensaje += 'Los campos de rango no estan diligenciados correctamente.'
                swAlert('warning',mensaje,'campos obligatorios','')
                return false
            }
            break

        case'eliminarRangoEdad':
            let idEdadMinMax = traerDatoFilaSeleccionada('listEdadMinMax','ID')

            if(!idEdadMinMax){
                swAlert('warning','Debe seleccionar el dato a eliminar.','','')
                return false
            }
            break
        case "crearNotaFactura":
            var lista_facturas = [];

            var ids = $("#listGrillaFacturas").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listGrillaFacturas_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listGrillaFacturas").getRowData(ids[i]);
                    lista_facturas.push(datosRow.ID_FACTURA);
                }
            }

            if (lista_facturas.length == 0) {
                swAlert('info', 'Debe seleccionar una factura');
                return false;
            }

            if (valorAtributo("cmbTipoDocumento") == "") {
                swAlert('info', 'Seleccione el tipo de documento');
                return false;
            }

            if (valorAtributo("cmbTipoNota") == "") {
                swAlert('info', 'Seleccione el tipo de nota');
                return false;
            }

            if (valorAtributo("txtValorNota") <= 0) {
                swAlert('info', 'El valor de la nota debe ser mayor a 0');
                return false;
            }

            if (valorAtributo("txtValorNota") <= 0) {
                swAlert('info', 'El valor de la nota debe ser mayor a 0');
                return false;
            }

            if (valorAtributo("txtValorNota") > traerDatoFilaSeleccionada("listGrillaFacturas", "TOTAL_FACTURA")) {
                swAlert('info', 'El valor de la nota no debe ser mayor al total de la factura');
                return false;
            }
            break;

        case 'agregarRemision':
            if (document.getElementById('txtMotivoRemision').value == '') {
                swAlert('info', 'EL CAMPO MOTIVO REMISION ES OBLIGATORIO');
                return false;
            }
            break;

        case 'auditarNotaCreditoFE':
            var lista_facturas = [];

            var ids = $("#listGrillaFacturas").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listGrillaFacturas_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listGrillaFacturas").getRowData(ids[i]);
                    lista_facturas.push(datosRow.ID_FACTURA);
                }
            }

            if (lista_facturas.length == 0) {
                alert('SELECCIONE UNA FACTURA PARA AUDITAR');
                return false;
            }

            if (valorAtributo('txtNoDevolucion') === '') {
                alert('LA FACTURA DEBE TENER UNA NOTA DE DEVOLUCION');
                return false;
            }
            if (!confirm('ESTA SEGURO QUE DESEA AUDITAR LA NOTA CREDITO?')) {
                return false;
            }
            break;

        case 'auditarFE':
            var lista_facturas = [];

            var ids = $("#listGrillaFacturas").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listGrillaFacturas_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listGrillaFacturas").getRowData(ids[i]);
                    lista_facturas.push(datosRow.ID_FACTURA);
                }
            }

            if (lista_facturas.length == 0) {
                alert('SELECCIONE UNA FACTURA PARA AUDITAR');
                return false;
            }

            if (!confirm('ESTA SEGURO QUE DESEA ENVIAR LAS FACTURAS ?')) {
                return false;
            }
            break;

        case "guardarObservacionFactura":
            if (valorAtributo('lblIdFactura') == '') { alert("DEBE SELECCIONAR UNA ADMISION CON FACTURA."); return false; }
            break;

        case 'guardarRegistroIngresoResultadoExterno':
            if (valorAtributo('txtResultadoInterpretacionExterno') == '') { swAlert('info', 'DEBE INGRESAR EL RESULTADO'); return false; }
            if (valorAtributo('txtUnidadesResultadoExterno') == '') { swAlert('info', 'DEBE INGRESAR LAS UNIDADES'); return false; }
            if (valorAtributo('txtFechaResultadoExterno') == '') { swAlert('info', 'DEBE INGRESAR LA FECHA DEL RESULTADO'); return false; }
            return true
            break;

        case "eliminarProcedimientosHC":
            return confirm("Esta seguro de continuar ?. Esta accion no se puede revertir !!")
            break;

        case "anularProcedimientosHc":
            folios = $("#listaHcSinFactura").jqGrid('getGridParam', 'selrow')
            if (folios == null) { alert('DEBE SELECCIONAR AL MENOS UN ELEMENTO PARA ANULAR'); return false; }
            return confirm("ESTA SEGURO DE CONTINUAR ? ESTA ACCION NO SE PUEDE REVERTIR !!")
            break;

        case 'crearEvento':
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert("DEBE DILIGENCIAR A SEDE QUE REPORTA "); return false; }
            if (valorAtributo("txtDescripcion") == '') { alert("DEBE DILIGENCIAR DESCRIPCION DEL EVENTO "); return false; }

            return true;

        case 'eliminarEvento':
            if (traerDatoFilaSeleccionada('listEvento', 'ID_ESTADO') == '') { alert('SELECCIONE UN TICKET A MODIFICAR'); return false }
            if (traerDatoFilaSeleccionada('listEvento', 'ID_ESTADO') != 0) { alert('NO PUEDE ELIMINAR ESTE TICKET'); return false }
            return true;

        case 'modificarEvento':
            if (traerDatoFilaSeleccionada('listEvento', 'ID_ESTADO') == '') { alert('SELECCIONE UN TICKET A MODIFICAR'); return false }
            if (traerDatoFilaSeleccionada('listEvento', 'ID_ESTADO') != 0) { alert('NO PUEDE MODIFICAR ESTE TICKET'); return false }
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert("DEBE DILIGENCIAR A SEDE QUE REPORTA "); return false; }
            if (valorAtributo('txtDescripcion') == '') { alert("DEBE DILIGENCIAR DESCRIPCION DEL EVENTO "); return false; }
            return true;

        case 'eliminarEncuestaPaciente':
            if (!traerDatoFilaSeleccionada('listEncuestaPaciente', 'id_estado_encuesta')) { alert('POR FAVOR, SELECCIONA UNA ENCUESTA ANTES DE ELIMINAR.'); return false; }
            if (traerDatoFilaSeleccionada('listEncuestaPaciente', 'id_estado_encuesta') == 'C') { alert('NO PUEDE ELIMINAR UNA ENCUESTA CERRADA'); return false; }
            return true;

        case 'crearFolio':
            if (valorAtributo("lblIdAdmisionAgen") == '') { alert('SELECCIONE UN PACIENTE CON ADMISION ACTUAL'); return false; }
            if (valorAtributo("cmbTipoDocumento") == '') { alert('FALTA SELECCIONAR EL TIPO DE FOLIO A CREAR'); return false; }
            return true;

        case 'modificarResultadoExterno': case 'agregarResultadoExterno':

            // para VIH
            if (parseInt(valorAtributo("txtIdOrden")) == '20' && parseInt(valorAtributo("txtIdPlantilla")) == 97492) {
                if (valorAtributo("txtResultadoAnalito").toLowerCase() == 'negativo' || valorAtributo("txtResultadoAnalito").toLowerCase() == 'positivo') {
                    return true;
                }
                else {
                    alert("SOLO SE REGISTRA POSITIVO O NEGATIVO");
                    return false;
                }
            }
            /*
            if (parseInt(valorAtributo("txtIdOrden")) == '29' && parseInt(valorAtributo("txtIdPlantilla")) == 97449) {
                try {
                    if (
                        document.getElementById('txtResultadoAnalito').value.toLowerCase() == 'aspecto: claro' 
                        || document.getElementById('txtResultadoAnalito').value.toLowerCase() == 'nitritos: negativo'
                        || document.getElementById('txtResultadoAnalito').value.toLowerCase() == 'glucosa: negativo'
                        || document.getElementById('txtResultadoAnalito').value.toLowerCase() == 'cetonas: negativo'
                        || document.getElementById('txtResultadoAnalito').value.toLowerCase() == 'bilirrubinas: negativo'
                        || document.getElementById('txtResultadoAnalito').value.toLowerCase() == 'bacterias:++'
                        ) {
                        return true;
                    }
                    else {
                        alert("SOLO SE REGISTRA: \nASPECTO: CLARO \nNITRITOS: NEGATIVO \nGLUCOSA: NEGATIVO \nCETONAS: NEGATIVO \nBILIRRUBINAS: NEGATIVO \nBACTERIAS: ++");
                        return false;
                    }
                } catch (error) {}
            }
            
            if (parseInt(valorAtributo("txtIdOrden")) == '32' && parseInt(valorAtributo("txtIdPlantilla")) == 97492){
                expreg = /^(?:([<])(?!.*\1))[0-9]/
                console.log(expreg.test(document.getElementById('txtResultadoAnalito').value), '.... exprexion regular')
                try {
                    if (expreg.test(document.getElementById('txtResultadoAnalito').value)) {
                        return true;
                    }
                    else {
                        alert("EXPRECION INCORRECTA, \n LA EXPRECION DEBE SER DE LA SIGUIENTE FORMA: <NUMERO");
                        return false;
                    }
                } catch (error) {}
            } */


            if (parseInt(valorAtributo("txtIdOrden")) == '21' && parseInt(valorAtributo("txtIdPlantilla")) == 97492) {
                //  expreg = /[<]{1}[0-9]/
                expreg = /^(?:([<])(?!.*\1))[0-9]/

                console.log(expreg.test(document.getElementById('txtResultadoAnalito').value), '.... exprexion regular')
                try {
                    if (expreg.test(document.getElementById('txtResultadoAnalito').value)) {
                        return true;
                    }
                    else {
                        alert("EXPRESION INCORRECTA, \n LA EXPRESION DEBE SER DE LA SIGUIENTE FORMA: <NUMERO");
                        return false;
                    }
                } catch (error) { }
            }
            // ********

            if (valorAtributo("txtResultadoNumericoAnalito") == '' && valorAtributo("txtTipoResultado") != 'texto') { alert("DIGITE UN RESULTADO"); return false; }
            if (valorAtributo("txtResultadoAnalito") == '' && valorAtributo("txtTipoResultado") == 'texto') { alert("DIGITE UN RESULTADO"); return false; }
            if (valorAtributo("txtFechaResultadoAnalito") == '') {
                /* MP cambio de alert */
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    showConfirmButton: false,
                    showCloseButton: true,
                    html: '<p style="color:black">DIGITE UNA FECHA </style>',
                });
                return false;
                //alert("DIGITE UNA FECHA"); 
            }
            if (parseInt(valorAtributo("txtIdOrden")) == '45' && parseInt(valorAtributo("txtIdPlantilla")) == 943) {
                if (valorAtributo("txtResultadoAnalito").toLowerCase() == 'negativo' || valorAtributo("txtResultadoAnalito").toLowerCase() == 'positivo') {
                    return true;
                }
                else {
                    alert("SOLO SE REGISTRA POSITIVO O NEGATIVO");
                    return false;
                }
            }


            var fechaNacPaciente = traerDatoFilaSeleccionada('listGrillaPacientes', 'fechaNac');
            fechaNacPaciente = fechaNacPaciente.split('-');
            fechaNacPaciente = new Date(parseInt(fechaNacPaciente[0]), parseInt(fechaNacPaciente[1]), parseInt(fechaNacPaciente[2]));
            var fechaResultado = $("#txtFechaResultadoAnalito").val().split('-');
            fechaResultado = new Date(parseInt(fechaResultado[0]), parseInt(fechaResultado[1]), parseInt(fechaResultado[2]));
            var fechaActual = fecha_actual_laboratorios().split('-');
            fechaActual = new Date(parseInt(fechaActual[0]), parseInt(fechaActual[1]), parseInt(fechaActual[2]));
            if (fechaResultado < fechaNacPaciente) {
                alert("LA FECHA REGISTRADA DEBE SER MAYOR A LA FECHA DE NACIMIENTO DEL PACIENTE");
                return false;
            }
            if (fechaResultado > fechaActual) {
                alert("LA FECHA REGISTRADA DEBE SER MENOR O IGUAL A LA FECHA ACTUAL");
                return false;
            }

            break;

        case 'eliminarFacturasRips':

            lista_facturas = [];
            ids = $("#listaFacturasCuenta").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                c = "jqg_listaFacturasCuenta_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listaFacturasCuenta").getRowData(ids[i]);
                    lista_facturas.push(datosRow.ID_FACTURA);
                    break;
                }
            }

            if (lista_facturas.length < 1) {
                alert("DEBE SELECCIONAR ALMENOS UNA FACTURA");
                return false;
            }
            break;
        case 'generarNuevaSolicitud':
            if (valorAtributo('txtJustificacionClinica') == '') { alert("DIGITE UNA JUSTIFICACION CLINICA"); return false; }
            var ids = jQuery("#listProcedimientosAuxiliares").getDataIDs();
            if (ids.length < 1) { alert("DEBE HABER UN PROCEDIMIENTO ASOCIADO A LA SOLICITUD"); return false; }
            if (valorAtributo('cmbDiasEsperarSolicitud') == '') { alert("SELECCIONE LOS DIAS DE ESPERA DE LA SOLICITUD"); return false; }
            break;

        case 'EliminarProcedimientoSolicitudNueva':
            if (valorAtributo('lblIdProcedimientoAgregar') == '') { alert("SELECCIONE UN PROCEDIMIENTO"); return false; }
            break;

        case 'AgregarProcedimientoSolicitudNueva':
            if (valorAtributo('txtProcedimientoAgregarSolicitud') == '') { alert("DIGITE UN PROCEDIMIENTO"); return false; }
            if (valorAtributo('txtProcedimientoCantidadAgregarSolicitud') == '') { alert("DIGITE UNA CANTIDAD"); return false; }
            if (valorAtributo('cmbProcedimientoDiagnotiscoAgregarSolicitud') == '') { alert("SELECCIONE UN DIAGNOSTICO ASOCIADO"); return false; }
            break;

        case 'modificarProcedimientoSolicitud':
            if (valorAtributo('txtProcedimientoCantidadEditarSolicitud') == '') { alert('DIGITE LA CANTIDAD ORDENADA'); return false; };
            if (valorAtributo('cmbProcedimientoDiagnotiscoEditarSolicitud') == '') { alert('SELECCIONE UN DIAGNOSTICO ASOCIADO'); return false; };
            break;

        case 'numeraFacEnBloqueExterna':
            if (valorAtributo('cmbFacturadoBaseNom') != 'SI') { alert('LAS FACTURAS A NUMERAR DEBEN ESTAR CREADAS POR BASE NOMINAL'); return false; };
            if (valorAtributo('cmbEstadoFactura') != '3') { alert('LAS FACTURAS A NUMERAR DEBEN ESTAR EN ESTADO PENDIENTE'); return false; };
            break;

        case 'listProcedimientosFacBloque':
            var num = $('#' + arg).getDataIDs().length;
            if (valorAtributo('cmbIdPlan') == '') { alert('SELECCIONE UN PLAN DE CONTRATACION'); return false; }
            if (valorAtributo('cmbIdProcedimientoFacBN') == '') { alert('SELECCIONE UN PLAN DE CONTRATACION'); return false; }
            if (num === 1) { alert('NO ES POSIBLE AGREGAR MAS PROCEDIMIENTOS'); return false; }
            traerValorUnitarioProcedimientoCuentaFacturacionBN();
            break;

        case 'crearFacturasBaseNominalNefro':
            if (valorAtributo('txtAdministradora1') === '') { alert('DEBE INGRESAR UNA ADMINISTRADORA DE FACTURA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') === '') { alert('DEBE ELEGIR UN TIPO DE REGIMEN'); return false; }
            if (valorAtributo('cmbIdPlan') === '') { alert('DEBE ELEGIR UN PLAN DE CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') === '') { alert('DEBE ELEGIR UN TIPO DE AFILIACION'); return false; }
            if (valorAtributo('cmbIdRango') === '') { alert('DEBE ELEGIR UN RANGO'); return false; }
            if (valorAtributo('txtIdDx') === '') { alert('DEBE ELEGIR UN DIAGNOSTICO'); return false; }
            // if (valorAtributo('cmbIdProfesionalesFactura') === '') { alert('DEBE ELEGIR EL PROFESIONAL QUE FACTURA'); return false; }
            if (valorAtributo('cmbCantidad') === '') { alert('DEBE INGRESAR LA CANTIDAD DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbEjecutado') === '') { alert('DEBE SELECCIONAR SI EL PROCEDIMIENTO ES EJECUTADO'); return false; }
            break;

        case "firmarFolio":
            fila_seleccionada = $("#listDocumentosHistoricos").jqGrid('getGridParam', 'selrow')
            id_estado = $("#listDocumentosHistoricos").getRowData(fila_seleccionada).ID_ESTADO
            if (valorAtributo('txtc1Rc') == '' && traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO') == 'B24X' && valorAtributo('lblMesesEdadPaciente') >= 12) {
                swAlert('error', 'FALTA EL RESULTADO DE RIESGO CARDIOVASCULAR'.toLowerCase());
                //mostrarAlerta('divFirmarFolio', 'ALERTA AL FIRMAR FOLIO', 'FALTA EL RESULTADO DE RIESGO CARDIOVASCULAR'); 
                return false;
            }
            if (fila_seleccionada == null) {
                swAlert('error', 'FALTA SELECCIONAR EL FOLIO A FIRMAR'.toLowerCase());
                //mostrarAlerta('divFirmarFolio', 'ALERTA AL FIRMAR FOLIO', '')
                return false;
            }
            if (id_estado != 0) {
                swAlert('error', 'EL FOLIO SELECCIONADO DEBE ESTAR EN ESTADO BORRADOR'.toLowerCase());
                //mostrarAlerta('divFirmarFolio', 'ALERTA AL FIRMAR FOLIO', '')
                return false;
            }
            return true;
            break;

        case "auditarFolioModificar":
            fila_seleccionada = $("#listDocumentosHistoricos").jqGrid('getGridParam', 'selrow')
            id_estado = $("#listDocumentosHistoricos").getRowData(fila_seleccionada).ID_ESTADO
            if (valorAtributo('txtc1Rc') == '' && traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO') == 'B24X' && valorAtributo('lblMesesEdadPaciente') >= 12) {
                swAlert('error', 'FALTA EL RESULTADO DE RIESGO CARDIOVASCULAR'.toLowerCase());
                //mostrarAlerta('divFirmarFolio', 'ALERTA AL FIRMAR FOLIO', 'FALTA EL RESULTADO DE RIESGO CARDIOVASCULAR'); 
                return false;
            }
            if (fila_seleccionada == null) {
                swAlert('error', 'FALTA SELECCIONAR EL FOLIO A FIRMAR'.toLowerCase());
                //mostrarAlerta('divFirmarFolio', 'ALERTA AL FIRMAR FOLIO', 'FALTA SELECCIONAR EL FOLIO A FIRMAR')
                return false;
            }
            if (id_estado != 1) {
                swAlert('error', 'EL FOLIO SELECCIONADO DEBE ESTAR EN ESTADO BORRADOR'.toLowerCase());
                //mostrarAlerta('divFirmarFolio', 'ALERTA AL FIRMAR FOLIO', 'EL FOLIO SELECCIONADO DEBE ESTAR EN ESTADO FIRMADO')
                return false;
            }
            break;

        case "editarIndicacionMedica":
            if (valorAtributo("lblProfesionalCreaIndicacion") != IdSesion()) { alert("USTED NO CREO LA INDICACION. NO PUIEDE MODIFICAR"); return false; }
            break;

        case "eliminarIndicacionMedica":
            if (valorAtributo("lblProfesionalCreaIndicacion") != IdSesion()) { alert("USTED NO CREO LA INDICACION. NO PUIEDE ELIMINAR"); return false; }
            break;

        case "eliminarArchivoOtros":
            var aux = $("#listArchivosAdjuntosVarios").jqGrid('getGridParam', 'selrow')
            var usuario = $("#listArchivosAdjuntosVarios").getRowData(aux).usuario_elaboro
            if (usuario != IdSesion()) { alert('USTED NO ES EL AUTOR DE ESTE REGISTRO. NO PUEDE ELIMINAR'); return false; }
            return confirm("ESTA SEGURO DE ELIMINAR ESTE ARCHIVO ?")
            break;

        case "firmarNota":
            if (valorAtributo('lblIdEstado') == '1') { alert('LA NOTA YA ESTA FIRMADA'); return false; }
            if (valorAtributo('lblIdProfesional') != IdSesion()) { alert('SOLO SE PUEDE FIRMAR UNA NOTA DE SU CREACION'); return false; }
            break;

        case "crearNotaNefroproteccion":
            if (valorAtributo('txtIdBusPaciente') == '') { alert('SELECCIONE UN PACIENTE'); return false; }
            break;

        case "eliminarAdmision":
            if (valorAtributo('lblIdAdmision') == '') { alert('FALTA SELCCIONAR ADMISION'); return false; }
            //if (IdSesion() != valorAtributo("lblUsuarioCrea")) { alert('USTED NO ES EL AUTOR DE ESTE REGISTRO. NO PUEDE ELIMINAR'); return false; }
            return confirm("ESTA SEGURO DE ELIMINAR LA ADMSION ?")
            break;

        case 'noDisponible':
            if ($("#listDiasC").jqGrid('getGridParam', 'selrow') === null) { alert('SELECCIONE AL MENOS UN DIA DE LA AGENDA'); return false; }
            if (valorAtributo('txtHoraInicioNoDisponibles') == '') { alert('SELECCIONE HORA DE INICIO.'); return false; }
            if (valorAtributo('txtHoraFinNoDisponibles') == '') { alert('SELECCIONE HORA DE FIN.'); return false; }
            if (valorAtributo('txtObservacionNoDisponible') == '') { alert('DEBE AGREGAR UNA OBSERVACION.'); return false; }
            return true
            break;


        case "tipificarFacturasPorBloqueEnEnvios":
            var cuentas = $("#listaCuentas").jqGrid('getGridParam', 'selrow')
            if (cuentas == null) { alert("DEBE SELECCIONAR AL MENOS UNA CUENTA PARA TIPIFICAR"); return false }
            if (valorAtributo('cmbEstadoTipificacionFacturas') == '') { alert("FALTA SELECCIONAR EL ESTADO DE LAS FACTURAS"); return false }
            break;

        case "guardarValorCuenta":
            if (valorAtributo('txtNuevoValorCuenta') == '') { alert("DEBE DILIGENCIAR ALGUN VALOR."); return false }
            break;

        case "eliminarEnvioRips":
            var cuenta = $("#listaCuentas").jqGrid('getGridParam', 'selrow')
            var id_estado = $("#listaCuentas").getRowData(cuenta).ID_ESTADO

            if (id_estado != 0) { alert("LA CUENTA NO SE PUEDE MODIFICAR"); return false }
            return confirm("ESTA SEGURO DE ELIMINAR ESTA CUENTA ?")
            break;

        case "cerrarCuenta":
            if (valorAtributo('txtFechaRadicacion') == '') { alert("FALTA INGRESAR FECHA DE RADICACION"); return false }
            if (valorAtributo('txtNumeroRadicacion') == '') { alert("FALTA INGRESAR NUMERO DE RADICACION"); return false }
            break;

        case "planTratamiento":
            if (valorAtributo("txtDx") == '') { alert("SELECCIONE UN DIAGNOSTICO"); return false }
            if (valorAtributo("cmbDxTipoPT") == '') { alert("SELECCIONE EL TIPO DEL DIAGNOSTICO"); return false }
            /*if(valorAtributo("txtObjGeneral") == ''){alert("ESCRIBA UN OBJETIVO GENERAL"); return false}
            if(valorAtributo("txtEspe") == ''){alert("ESCRIBA OBJETIVOS ESPECIFICOS"); return false}
            if(valorAtributo("txtPlanTr") == ''){alert("ESCRIBA UN PLAN DE TRATAMIENTO"); return false}*/
            break;

        case "modificarCitasBloqueAgenda":
            var citas = $("#listaCitasPaciente").jqGrid('getGridParam', 'selrow')
            if (citas == null) { alert("DEBE SELECCIONAR AL MENOS UNA CITA"); return false }
            if (valorAtributo('cmbEstadoCitaPacienteExiste') == '') { alert("FALTA SELECCIONAR EL ESTADO DE LAS CITAS"); return false }
            if (valorAtributo('cmbMotivoConsultaPacienteExiste') == '') { alert("FALTA SELECCIONAR LA VIA"); return false }
            if (["R", "L", "N"].indexOf(valorAtributo("cmbEstadoCitaPacienteExiste")) != -1) {
                if (valorAtributo('cmbMotivoConsultaClasePacienteExiste') == '') { alert("FALTA SELECCIONAR LA CLACIFICACION"); return false }
            }
            break;

        case "modificarEstadoProgramacionCitasBloque":
            if (valorAtributo('cmbEstadoCitaModificarBloque') == '') { alert("FALTA SELECCIONAR EL ESTADO DE LAS CITAS"); return false }
            if (valorAtributo('cmbMotivoConsulModificarBloque') == '') { alert("FALTA SELECCIONAR LA VIA DE COMUNICACION"); return false }
            if (["R", "L", "N"].indexOf(valorAtributo("cmbEstadoCitaModificarBloque")) != -1) {
                if (valorAtributo('cmbMotivoConsultaClaseModificarBloque') == '') { alert("FALTA SELECCIONAR LA CLACIFICACION"); return false }
            }
            break;

        case "modificarProgramacionCitasBloque":
            if (valorAtributo('cmbIdEspecialidadProgramacioCitasEditarBloque') == '') { alert("FALTA SELECCIONAR LA ESPECIALIDAD"); return false }
            if (valorAtributo('cmbTipoCitaProgramacionCitasEditarBloque') == '') { alert("FALTA SELECCIONAR EL TIPO DE CITA"); return false }
            if (valorAtributo('cmbIdProfesionalProgramacionCitasEditarBloque') == '') { alert("FALTA SELECCIONAR EL PROFESIONAL"); return false }
            break;

        case "modificarEstadoCitaProgramacion":
            if (valorAtributo('cmbEstadoCitaModificarCitaProgramacion') == '') { alert("FALTA SELECCIONAR EL ESTADO DE LAS CITAS"); return false }
            if (valorAtributo('cmbMotivoConsulModificarCitaProgramacion') == '') { alert("FALTA SELECCIONAR LA VIA DE COMUNICACION"); return false }
            if (["R", "L", "N"].indexOf(valorAtributo("cmbEstadoCitaModificarCitaProgramacion")) != -1) {
                if (valorAtributo('cmbMotivoConsultaClaseModificarCitaProgramacion') == '') { alert("FALTA SELECCIONAR LA CLACIFICACION"); return false }
            }
            break;

        case "modificarProgramacionLEBloque":
            if (valorAtributo('cmbIdEspecialidadProgramacioLEEditarBloque') == '') { alert("FALTA SELECCIONAR LA ESPECIALIDAD"); return false }
            if (valorAtributo('cmbTipoCitaProgramacionLEEditarBloque') == '') { alert("FALTA SELECCIONAR EL TIPO DE CITA"); return false }
            if (valorAtributo('cmbIdProfesionalProgramacionLEEditarBloque') == '') { alert("FALTA SELECCIONAR EL PROFESIONAL"); return false }
            break;

        case "insertarFacturaDetalleOrden":
            var ordenes = $("#listaOrdenesSinFactura").jqGrid('getGridParam', 'selrow')

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            if (ordenes == null) { alert('DEBE SELECCIONAR AL MENOS UNA ORDEN'); return false; }
            return confirm("ESTA SEGURO DE CONTINUAR ?")
            break;

        case "insertarFacturaDetalleEvolucion":
            var folios = $("#listaHcSinFactura").jqGrid('getGridParam', 'selrow')

            if (traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == null || traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == "") { alert('DEBE SELECCIONAR UNA ADMISION CON FACTURA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            if (folios == null) { alert('DEBE SELECCIONAR AL MENOS UN FOLIO'); return false; }
            return confirm("ESTA SEGURO DE CONTINUAR ?")
            break;

        case "eliminarPlanEvolucionFactura":
            var ordenes = $("#listaPlanEvolucionFactura").jqGrid('getGridParam', 'selrow')

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            if (ordenes == null) { alert('DEBE SELECCIONAR AL MENOS UNA ORDEN'); return false; }
            return confirm("ESTA SEGURO DE CONTINUAR ?")
            break;

        case "eliminarFacturaDetalleEvolucion":
            var folios = $("#listaFacturaDetalleEvolucion").jqGrid('getGridParam', 'selrow')

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            if (folios == null) { alert('DEBE SELECCIONAR AL MENOS UN FOLIO'); return false; }
            return confirm("ESTA SEGURO DE CONTINUAR ?")
            break;

        case 'crearProcedimientoEjecutado':
            if (valorAtributoIdAutoCompletar('txtIdProcedimientoEjecutado') == '') { alert('FALTA BUSCAR PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbCantidadEjecutado') == '') { alert('FALTA CANTIDAD'); return false; }
            break;

        case 'adicionarFolioProcedimientoTipoCitas':
            let idRowCita = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
            let idTipoCita = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCita)['ID'];

            let idRowFormulario = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').jqGrid('getGridParam', 'selrow');
            let idTipoFormulario = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').getRowData(idRowFormulario)['ID'];

            if (idTipoFormulario == null || idTipoFormulario == "") { alert('FALTA SELECCIONAR UN TIPO DE FOLIO'); return false; }
            if (idTipoCita == null || idTipoCita == '') { alert('FALTA SELECCIONAR EL TIPO DE CITA'); return false; }
            if (valorAtributoIdAutoCompletar("txtIdProcedimiento") == "") { alert('FALTA BUSCAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo("cmbIdModalidad") == "") { alert("FALTA SELECCIONAR MODALIDAD DE PROCEDIMIENTO"); return false; }
            break;

        case 'adicionarFolioProcedimiento':
            tipo_cita = $("#listGrillaTiposCita").jqGrid('getGridParam', 'selrow');
            tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

            if (tipo_formulario == null) { alert('FALTA SELECCIONAR EL TIPO DE FORUMULARIO'); return false; }
            if (tipo_cita == null) { alert('FALTA SELECCIONAR EL TIPO DE CITA'); return false; }
            if (valorAtributoIdAutoCompletar("txtIdProcedimiento") == "") { alert('FALTA BUSCAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo("cmbIdModalidad") == "") { alert("FALTA SELECCIONAR MODALIDAD DE PROCEDIMIENTO"); return false; }
            break;

        case 'finalizarOrdenExternaAdmision':
            var id_evolucion = $("#listaProcedimientosOrdenExternaAdmision").getRowData(1).ID_EVOLUCION

            if (id_evolucion == undefined) { alert('DEBE AGREGAR AL MENOS UN PROCEDIMIENTO'); return false; }
            return confirm("ESTA SEGURO DE FINALIZAR ESTA ORDEN ?")
            break;

        case 'guardarOrdenesExternas':
            var id_evolucion = $("#listaProcedimientosOrdenExterna").getRowData(1).ID_EVOLUCION

            if (id_evolucion == undefined) { alert('DEBE AGREGAR AL MENOS UN PROCEDIMIENTO'); return false; }
            return confirm("ESTA SEGURO DE FINALIZAR ESTA ORDEN ?")
            break;

        case 'crearOrdenExterna':
            if (valorAtributoIdAutoCompletar('txtIdBusPacienteOrdenes') == '') { alert('FALTA BUSCAR PACIENTE'); return false; }
            if (valorAtributo('txtIdProcedimientoOrdenExt') == '') { alert('FALTA SELECCIONAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCantidadOrdenExt') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('txtCantidadOrdenExt') <= 0) { alert('LA CANTIDAD DEBE SER MAYO A CERO'); return false; }
            if (valorAtributo('cmbNecesidadOrdenExt') == '') { alert('FALTA EL GRADO DE NECESIDAD'); return false; }
            if (valorAtributo('txtDxRelacionadoOrdenExt') == '') { alert('FALTA EL DX RELACIONADO'); return false; }
            if ($("#ckLaboratoriosNefro").is(":checked")) {
                if (valorAtributo("lblIdPlanContratacionLaboratoriosNefroproteccion") == '') {
                    alert('FALTA ESCOGER EL PLAN DE CONTRATACION');
                    return false;
                }
            }
            break;

        case 'modificarProgramacionLE':
            if (valorAtributo('cmbIdEspecialidadProgramacioLEEditar') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbIdEspecialidadProgramacioLEEditar') == '') { alert('FALTA SELECCIONAR UN TIPO DE CITA'); return false; }
            if (valorAtributo('cmbIdProfesionalProgramacionLEEditar') == '') { alert('FALTA SELECCIONAR UN PROFESIONAL'); return false; }
            if (valorAtributo('txtFechaProgramacionLEEditar') == '') { alert('FALTA LA FECHA DE PROGRAMACION'); return false; }
            break;

        case 'modificarProgramacionTerapiaAgenda':
            if (valorAtributo('cmbIdEspecialidadProgramacionEditar') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbIdEspecialidadProgramacionEditar') == '') { alert('FALTA SELECCIONAR UN TIPO DE CITA'); return false; }
            if (valorAtributo('cmbIdProfesionalProgramacionEditar') == '') { alert('FALTA SELECCIONAR UN PROFESIONAL'); return false; }
            if (valorAtributo('txtFechaTerapiaEditar') == '') { alert('FALTA SELECCIONAR FECHA'); return false; }
            if (valorAtributo('txtHoraTerapiaEditar') == '') { alert('FALTA SELECCIONAR HORA'); return false; }
            break;

        case 'guardarProgramacionLE':
            var count = 0;
            var ids = $("#listaProgramacionLE").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionLE_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    count += 1;
                }
            }

            if (count == 0) { alert('FALTA SELEECIONAR LA LISTA DE ESPERA A GUARDAR'); return false; }
            break;

        case 'guardarCitasProgramadas':
            var count = 0;
            var ids = $("#listaProgramacionTerapiasAgenda").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionTerapiasAgenda_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    count += 1;
                }
            }

            if (valorAtributo('txtMunicipioOrdenes') == '') { alert('FALTA MUNICIPIO PACIENTE'); return false; }
            if (valorAtributo('txtDireccionResOrdenes') == '') { alert('FALTA DIRECCION PACIENTE'); return false; }
            //if (valorAtributo('txtTelefonosOrdenes') == '') { alert('FALTA TELEFONO PACIENTE'); return false; }
            if (valorAtributo('txtTelefonosOrdenes') == '' && valorAtributo('txtCelular1Ordenes') == '') { alert('DEBE DILIGENCIAR EL CELULAR O TELEFONO DEL PACIENTE'); return false; }
            if (valorAtributo('txtCelular1Ordenes') == '') { alert('FALTA CELULAR PACIENTE'); return false; }

            if (count == 0) { alert('DEBE SELECCIONAR AL MENOS UNA CITA'); return false; }
            if (valorAtributo('cmbEstadoCitaTerapias') == '') { alert('FALTA ESTADO DE LA(S) CITA(S)'); return false; }
            if (valorAtributo('cmbMotivoConsultaTerapias') == '') { alert('FALTA VIA DE COMUNICACION'); return false; }
            break;

        case 'crearProgramacionLE':
            terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');
            var count = 0;

            var ids = $("#listDias").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listDias_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    count += 1;
                }
            }

            if (valorAtributo('txtMunicipioOrdenes') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionResOrdenes') == '') { alert('FALTA DIRECCION'); return false; }
            //if (valorAtributo('txtTelefonosOrdenes') == '') { alert('FALTA TELEFONO'); return false; }
            if (valorAtributo('txtCelular1Ordenes') == '') { alert('FALTA CELULAR1'); return false; }

            if (valorAtributo('cmbIdEspecialidadOrdenes') == '') { alert('FALTA ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoCitaTerapia') == '') { alert("DEBE SELECCIONAR EL TIPO DE CITA"); return false; }
            if (valorAtributo('cmbIdProfesionalesOrdenes') == '') { alert('FALTA PROFESIONAL'); return false; }

            if (valorAtributo('txtAdministradora1Ordenes') == '') { alert('FALTA ADMINISTRADORA AGENDA'); return false; }
            if (valorAtributo('cmbIdTipoRegimenOrdenes') == '') { alert('FALTA TIPO DE REGIMEN'); return false; }
            if (valorAtributo('cmbIdPlanOrdenes') == '') { alert('FALTA PLAN'); return false; }

            if (valorAtributoIdAutoCompletar('txtIPSRemiteOrdenes') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }

            if (terapia_seleccionada === null) { alert("DEBE SELECCIONAR UNA ORDEN"); return false; }
            if (valorAtributo('cmbCantidadCitasTerapia') == '') { alert("FALTA SELECCIONAR CANTIDAD AL DIA"); return false; }

            if ($('#chkFrecuencia').attr('checked')) {
                if (valorAtributo('cmbFrecuenciaTerapia') == '') { alert("FALTA SELECCIONAR LA FRECUENCIA DE LAS CITAS"); return false; }
                if (valorAtributo('cmbRepeticiones') == '') { alert("FALTA SELECCIONAR LA CANTIDAD DE REPETICIONES"); return false; }
            }
            if (count == 0) { alert("FALTA SELECCIONAR LOS DIAS DE LA AGENDA"); return false; }
            break;

        case 'crearProgramacionOrdenes':
            terapia_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');
            var count = 0;

            var ids = $("#listDias").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listDias_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    count += 1;
                }
            }
            /*if (valorAtributo('txtMunicipioOrdenes') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionResOrdenes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonosOrdenes') == '') { alert('FALTA TELEFONO'); return false; }
            if (valorAtributo('txtCelular1Ordenes') == '') { alert('FALTA CELULAR1'); return false; }*/

            if (valorAtributo('cmbIdEspecialidadOrdenes') == '') { alert('FALTA ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoCitaTerapia') == '') { alert("DEBE SELECCIONAR EL TIPO DE CITA"); return false; }
            if (valorAtributo('cmbIdProfesionalesOrdenes') == '') { alert('FALTA PROFESIONAL'); return false; }

            if (valorAtributo('txtAdministradora1Ordenes') == '') { alert('FALTA ADMINISTRADORA AGENDA'); return false; }
            if (valorAtributo('cmbIdTipoRegimenOrdenes') == '') { alert('FALTA TIPO DE REGIMEN'); return false; }
            if (valorAtributo('cmbIdPlanOrdenes') == '') { alert('FALTA PLAN'); return false; }

            //if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA FECHA PACIENTE'); return false; }
            //if (Date.parse(valorAtributo('lblFechaCita')) < Date.parse(valorAtributo('txtFechaPacienteCita'))) { alert('FECHA DE PACIENTE NO ES VALIDA. DEBE SER MAYOR O IGUAL A LA FECHA DE LA CITA'); return false; }
            if (valorAtributoIdAutoCompletar('txtIPSRemiteOrdenes') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            //if (valorAtributo('cmbEstadoCitaTerapias') == '') { alert('FALTA ESTADO DE LA(S) CITA(S)'); return false; }
            //if (valorAtributo('cmbMotivoConsultaTerapias') == '') { alert('FALTA VIA DE COMUNICACION'); return false; }
            if (valorAtributo('cmbDuracionMinutosOrdenes') == '') { alert('FALTA LA DURACION DE CITA(S)'); return false; }
            if (valorAtributo('cmbConsultorio') == '') { alert('FALTA SELECCIONAR EL CONSULTORIO'); return false; }

            if (terapia_seleccionada === null) { alert("DEBE SELECCIONAR UNA ORDEN"); return false; }
            //if (valorAtributo('cmbEstadoCitaTerapias') == '') { alert("FALTA SELECCIONAR EL ESTADO DEL CITAS") ; return false; }
            //if (valorAtributo('cmbMotivoConsultaTerapias') == '') { alert("FALTA SELECCIONAR VIA DE COMUNICACION") ; return false; }
            if (valorAtributo('txtHoraCitas') == '') { alert("NO SE HA ESPECIFICADO LA HORA DE LAS CITAS"); return false; }
            if (valorAtributo('cmbCantidadCitasTerapia') == '') { alert("FALTA SELECCIONAR CANTIDAD AL DIA"); return false; }
            //if (valorAtributo('cmbDuracionMinutosOrdenes') == '') { alert("FALTA SELECCIONAR DURACI&Oacute;N") ; return false; }
            if ($('#chkFrecuencia').attr('checked')) {
                if (valorAtributo('cmbFrecuenciaTerapia') == '') { alert("FALTA SELECCIONAR LA FRECUENCIA DE LAS CITAS"); return false; }
                if (valorAtributo('cmbRepeticiones') == '') { alert("FALTA SELECCIONAR LA CANTIDAD DE REPETICIONES"); return false; }
            }
            if (count == 0) { alert("FALTA SELECCIONAR LOS DIAS DE LA AGENDA"); return false; }
            break;

        case 'crearCopago':
            admision = $("#listAdmisionCuenta").jqGrid('getGridParam', 'selrow')
            id_factura = $("#listAdmisionCuenta").getRowData(admision).ID_FACTURA
            id_estado_factura = $("#listAdmisionCuenta").getRowData(admision).ID_ESTADO
            if (id_factura === undefined) { alert("DEBE SELECCIONAR UNA ADMISION CON FACTURA"); return false; }
            if (id_estado_factura == 'F') { alert("EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR."); return false; }
            if (isNaN(valorAtributo('txtValorRecibo')) || valorAtributo('txtValorRecibo') <= 0) { alert('EL VALOR DEBE SER UN VALOR NUMERICO MAYOR A 0'); return false; }
            break;

        case 'anexo1':
            if (valorAtributo('txtPrimerApellidoI') == '' && valorAtributo('txtSegundoApellidoI') == '' && valorAtributo('txtPrimerNombreI') == '' &&
                valorAtributo('txtSegundoNombreI') == '' && valorAtributo('cmbTipoIdI') == '' && valorAtributo('txtNumeroIdI') == '' && valorAtributo('txtFechaNI') == '') { alert("DEBE ESCRIBIR AL MENOS UN ITEM DEL FORMULARIO"); return false; }
            break;

        case 'asociarCitasAdmision':
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            return confirm("Esta seguro de registrar las citas a esta admision ?")
            break;

        case 'asociarNotaFactura':
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            return confirm("Esta seguro de registrar las notas a esta factura ?")
            break;

        case 'eliminarTipoCita':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE TIPO DE CITA!'); return false; }
            break;

        case 'modificarTipoProcedim':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE TIPO DE CITA!'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('SELECCIONE NOMBRE!'); return false; }
            if (valorAtributo('txtRecomendaciones') == '') { alert('SELECCIONE RECOMENDACIONES!'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE ESPECIALIDAD!'); return false; }
            if (valorAtributo('cmbProgramaTipoCita') == '') { alert('FALTA ANADIR EL PROGRAMA ASOCIADO'); return false; }
            break;

        case 'adicionarTipoCitaFormulario':
            if (valorAtributo('lblIdFormulario') == '') { alert('SELECCIONE UN FORMULARIO'); return false }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE UNA ESPECIALIDAD'); return false }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE UN TIPO DE CITA'); return false }
            break;

        case 'adicionarTipoCitaFormularioTipoCitas':
            let idRowCitaAf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
            let idTipoCitaAf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCitaAf)['ID'];

            if (valorAtributo('cmbFormulario') == '') { alert('SELECCIONE UN FORMULARIO'); return false }
            if (!idTipoCitaAf || idTipoCitaAf == '') { alert('SELECCIONE UN TIPO DE CITA'); return false }
            break;

        case 'modificarTipoCitaFormulario':
            if (valorAtributo('lblIdFormulario') == '') { alert('SELECCIONE UN FORMULARIO'); return false }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE UN TIPO DE CITA'); return false }
            break;
        case 'eliminarEspecialidadSede':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA ESPECIALIDAD.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert('SELECCIONE UNA SEDE.'); return false; }
            break;

        case 'asignarSedeEspecialidad':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA ESPECIALIDAD.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert('SELECCIONE UNA SEDE.'); return false; }
            break;
        case 'eliminarSedeBodega':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA BODEGA.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert('SELECCIONE UNA SEDE.'); return false; }
            break;

        case 'asignarSedeBodega':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA BODEGA.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdSede') == '') { alert('SELECCIONE UNA SEDE.'); return false; }
            break;

        case 'modificarEspecialidad':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA ESPECIALIDAD.'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA ASIGNAR NOMBRE.'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('SELECCIONE EL SERVICIO.'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA ASIGNAR VIGENCIA.'); return false; }
            break;

        case 'crearEspecialidadSede':
            if (valorAtributo('txtNombre') == '') { alert('FALTA ASIGNAR NOMBRE.'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('SELECCIONE EL SERVICIO.'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA ASIGNAR VIGENCIA.'); return false; }
            break;

        case 'eliminarDisponibles':
            if ($("#listDiasC").jqGrid('getGridParam', 'selrow') === null) { alert('SELECCIONE AL MENOS UN DIA DE LA AGENDA'); return false; }
            if (valorAtributo('txtHoraInicioEliminar') == '') { alert('SELECCIONE HORA DE INICIO.'); return false; }
            if (valorAtributo('txtHoraFinEliminar') == '') { alert('SELECCIONE HORA DE FIN.'); return false; }
            return true;
            break;

        case 'inhabilitarElemento':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR ELEMENTO'); return false; }
            return confirm("Esta seguro de inhabilitar elemento ?") ? true : false;

        case 'copiarCitasFrecuencia':
            if (valorAtributo('txtCantidadCitas') == '') { alert('FALTA CANDTIDAD'); return false; }
            if (valorAtributo('txtFrecuenciaCitas') == '') { alert('FALTA FRECUENCIA'); return false; }
            break;

        case 'insertarProcedimientoResultadosExternos':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA SELECCIONAR FOLIO.'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
            if (valorAtributo('txtIdProcedimientoResultado') == '') { alert('FALTA ESCOGER ESPECIALIDAD'); return false; }
            break;

        case 'eliminarPermisoPerfil':
            if (valorAtributo('lblIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR UNA OPCION DEL SISTEMA'); return false; }
            return confirm("Esta seguro de eliminar elemento ?") ? true : false;

            break;

        case 'finalizarHistoriaClinicaLaboratorio':
            if (valorAtributo('txtIdHCL') == '') { alert("DEBE SELECCIONAR UNA HISTORIA CLINICA"); return false };
            if (valorAtributo('txtIdHCLE') == '1') { alert("LA HISTORIA CLINICA YA FUE FINALIZADA"); return false };
            var ids = $("#listaOrdenesHistoriaClinicaLaboratorio").getDataIDs();
            if (ids.length == 0) { alert('DEBE AGREGAR ALGUN LABORATORIO PARA FINALIZAR.'); return false };

            break;

        case 'crearProcedimientoHistoriaClinicaLaboratorio':
            if (valorAtributo('txtIdHCL') == '') { alert("DEBE SELECCIONAR UNA HISTORIA CLINICA"); return false };
            if (valorAtributo('txtIdHCLE') == '1') { alert("LA HISTORIA CLINICA YA FUE FINALIZADA"); return false };
            if (valorAtributo('txtIdProcedimientoLaboratorio') == '') { alert("DEBE SELECCIONAR UN PROCEDIMIENTO"); return false };
            if (valorAtributo('cmbCantidadLaboratorio') == '') { alert("DEBE SELECCIONAR LA CANTIDAD"); return false };

            break;


        case 'modificarDocumentoTipificacionFormulario':
            if (valorAtributo('lblNombreFormulario') == '') { alert("DEBE SELECCIONAR UN FORMULARIO"); return false };
            break;

        case 'traerTarifarioPlanContratacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN DE CONTRATACION'); return false };
            if (valorAtributo('cmbIdTarifario') == '') { alert('FALTA SELECCIONAR TARIFARIO BASE'); return false };
            if (valorAtributo('txtPorcentajeIncremento') == '' || isNaN(valorAtributo('txtPorcentajeIncremento'))) { alert('INGRESE UN VALOR NUMERICO EN PORCETANJE DE INCREMENTO'); return false };
            break;

        case 'crearAdmisionSinEncuesta':
            if (valorAtributo('lblIdPaciente') == '') { alert('DEBE SELECCIONAR UN PACIENTE'); return false };
            if (valorAtributo('cmbServicioEntrada') == '') { alert("FALTA SELECCIONAR EL SERVICIO"); return false };
            break;

        case 'crearCitasTerapias':

            console.log($("#listaProgramacionTerapias").getGridParam("reccount"));
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('DEBE SELECCIONAR UN PROCEDIMIENTO'); return false };
            //if (valorAtributo('lblIdClaseLab') != '9') { alert('EL PROCEDIMIENTO DEBE SER CLASE TERAPEUTICO'); return false };
            if (valorAtributo('cmbSedeTerapia') == '') { alert('DEBE SELECCIONAR UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidadTerapia') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCitaTerapia') == '') { alert('DEBE SELECCIONAR UN TIPO DE CITA'); return false };
            if (valorAtributo('cmbProfesionalesTerapia') == '') { alert('DEBE SELECCIONAR UN PROFESIONAL'); return false };
            if (valorAtributo('txtFechaInicioTerapia') == '') { alert('DEBE SELECCIONAR UNA FECHA INICIO'); return false };
            if (valorAtributo('cmbFrecuenciaTerapia') == '') { alert('DEBE SELECCIONAR UNA FRECUENCIA'); return false };
            if (valorAtributo('cmbCantidadCitasTerapia') == '') { alert('DEBE SELECCIONAR UNA CANTIDAD'); return false };
            if ((parseInt(valorAtributo('cmbCantidadCitasTerapia')) + $("#listaProgramacionTerapias").getGridParam("reccount")
            ) > parseInt(valorAtributo('lblCantidadLab'))) { alert('LA CANTIDAD SUPERA EL NUMERO DE TERAPIAS ORDENADAS'); return false }
            break;

        case 'modificarProgramacionTerapia':
            if (valorAtributo('lblIdProgramacionTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA TERAPIA'); return false };
            if (valorAtributo('cmbSedeTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidadTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCitaTerapiaEditar') == '') { alert('DEBE SELECCIONAR UN TIPO DE CITA'); return false };
            if (valorAtributo('cmbProfesionalesTerapiaEditar') == '') { alert('DEBE SELECCIONAR UN PROFESIONAL'); return false };
            if (valorAtributo('txtFechaInicioTerapiaEditar') == '') { alert('DEBE SELECCIONAR UNA FECHA INICIO'); return false };

            break;

        case 'eliminarProgramacionTerapia':
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR LA TERAPIA?')) { return false; }

            break;



        case 'modificaPlanContratacion':
            if (valorAtributo('txtIdPlan') == '') { alert('DEBE SELECCIONAR UN PLAN'); return false };
            //if (valorAtributo('cmbIdModalidadContrato') == '') { alert('FALTA MODALIDAD DE CONTRATO'); return false };
            if (valorAtributo('txtDescPlan') == '') { alert('FALTA DESCRIPCION'); return false };
            if (valorAtributoIdAutoCompletar('txtIdAdministradora') == '') { alert('FALTA ADMISTRADORA O EL FORMATO NO ES VALIDO'); return false };
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FATA TIPO DE REGIMEN'); return false };
            if (valorAtributo('cmbIdTipoCliente') == '') { alert('FALTA TIPO DE CLIENTE'); return false };
            if (valorAtributo('txtNumeroContrato') == '') { alert('FALTA NUMERO DE CONTRATO'); return false };
            if (valorAtributo('txtFechaInicio') == '') { alert('FALTA FECHA INICIAL'); return false };
            if (valorAtributo('txtFechaFinal') == '') { alert('FALTA FECHA FINAL'); return false };
            if (valorAtributo('txtMontoContrato') == '' || isNaN(valorAtributo('txtMontoContrato'))) { alert('FALTA MONTO CONTRATO O EL VALOR NO ES NUMERICO'); return false };
            if (valorAtributo('cmbIdTipoPlan') == '') { alert('FALTA TIPO DE PLAN'); return false };
            if (valorAtributo('txtCodigoAdministradora') == '') { alert('FALTA CODIGO ADMINISTRADORA'); return false };


            //if (valorAtributo('cmbIdTarifario') == '') { alert('FALTA TARIFARIO BASE'); return false };
            //if (valorAtributo('txtPorcentajeIncremento') == '') { alert('FALTA PORCENTAJE DE INCREMENTO'); return false };
            break;

        case 'crearPlanContratacion':
            if (valorAtributo('txtDescPlan') == '') { alert('FALTA DESCRIPCION'); return false };
            if (valorAtributo('cmbIdModalidadContrato') == '') { alert('FALTA MODALIDAD DE CONTRATO'); return false };
            if (valorAtributoIdAutoCompletar('txtIdAdministradora') == '') { alert('FALTA ADMISTRADORA O EL FORMATO NO ES VALIDO'); return false };
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FATA TIPO DE REGIMEN'); return false };
            if (valorAtributo('cmbIdTipoCliente') == '') { alert('FALTA TIPO DE CLIENTE'); return false };
            if (valorAtributo('txtNumeroContrato') == '') { alert('FALTA NUMERO DE CONTRATO'); return false };
            if (valorAtributo('txtFechaInicio') == '') { alert('FALTA FECHA INICIAL'); return false };
            if (valorAtributo('txtFechaFinal') == '') { alert('FALTA FECHA FINAL'); return false };
            if (valorAtributo('txtMontoContrato') == '' || isNaN(valorAtributo('txtMontoContrato'))) { alert('FALTA MONTO CONTRATO O EL VALOR NO ES NUMERICO'); return false };
            if (valorAtributo('cmbIdTipoPlan') == '') { alert('FALTA TIPO DE PLAN'); return false };
            if (valorAtributo('txtCodigoAdministradora') == '') { alert('FALTA CODIGO ADMINISTRADORA'); return false };
            if (valorAtributo('cmbIdCoberturaBeneficios') == '') { alert('FALTA COBERTURA DE BENEFICIOS'); return false };
            break;

        case 'modificarPacienteEncuesta':
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA ESCOGER TIPO DE DOCUMENTO'); return false };
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA DIGITAR IDENTIFICACION'); return false };
            if (valorAtributo('txtApellido1') == '') { alert('FALTA DIGITAR APELLIDO 1'); return false };
            if (valorAtributo('txtNombre1') == '') { alert('FALTA DIGITAR NOMBRE 1'); return false };
            if (valorAtributo('txtFechaNac') == '') { alert('FALTA DIGITAR FECHA NACIMIENTO'); return false };
            if (valorAtributo('cmbSexo') == '') { alert('FALTA ESCOGER SEXO'); return false };
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA DIGITAR MUNICIPIO'); return false };
            break;
        case 'modificarPaciente':
            if (!validarEdad(valorAtributo('txtFechaNac'))) { return false; }
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA ESCOGER TIPO DE DOCUMENTO'); return false };
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA DIGITAR IDENTIFICACION'); return false };
            if (valorAtributo('txtApellido1') == '') { alert('FALTA DIGITAR APELLIDO 1'); return false };
            if (valorAtributo('txtNombre1') == '') { alert('FALTA DIGITAR NOMBRE 1'); return false };
            if (valorAtributo('txtFechaNac') == '') { alert('FALTA DIGITAR FECHA NACIMIENTO'); return false };
            if (valorAtributo('cmbSexo') == '') { alert('FALTA ESCOGER SEXO'); return false };
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA DIGITAR MUNICIPIO'); return false };
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIGITAR DIRECCION'); return false };
            if (valorAtributo('txtCelular1') == '') { alert('FALTA DIGITAR Celular 1'); return false };
            if (valorAtributo('cmbIdEtnia') == '') { alert('FALTA DIGITAR ETNIA '); return false };
            if (valorAtributo('cmbIdNivelEscolaridad') == '') { alert('FALTA DIGITAR ESCOLARIDAD'); return false };
            if (valorAtributo('cmbIdEstrato') == '') { alert('FALTA DIGITAR ESTRATO'); return false };
            if (valorAtributo('txtIdOcupacion') == '') { alert('FALTA DIGITAR OCUPACION'); return false };
            if (valorAtributo('txtAdministradoraPaciente') == '') { alert('FALTA DIGITAR ADMINISTRADORA PACIENTE'); return false };
            if (valorAtributo('txtIpsPaciente') == '') { alert('FALTA DIGITAR IPS PRIMARIA'); return false };
            if (valorAtributo('cmbTipoRegimen') == '') { alert('FALTA DIGITAR REGIMEN'); return false };
            if (valorAtributo('cmbNivelSisben') == '') { alert('FALTA DIGITAR NIVEL SISVEN'); return false };
            break;

        case 'nuevoIdPacienteEncuesta':
            if (valorAtributo('cmbTipoIdNuevoPacienteEncuesta') == '') { alert('FALTA ESCOGER TIPO DE DOCUMENTO'); return false };
            if (valorAtributo('txtNumeroDocNuevoPacienEncuesta') == '') { alert('FALTA NUMERO DE DOCUMENTO'); return false };
            break;

        case 'interpretarProcedimiento':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            if (valorAtributo('lblIdOrden') == '') { alert('FALTA ESCOGER PROCEDIMIENTO'); return false };
            if (valorAtributo('lblIdEstadoOrden') != '3' && valorAtributo('lblIdEstadoOrden') != '4') { alert('EL PROCEDIMIENTO DEBE ESTAR REALIZADO PARA INTERPRETAR'); return false };
            if (valorAtributo('txtInterpretacion') === '') { alert('LA INTERPRETACION NO PUEDE ESTAR VACIA'); return false };

            break;

        case 'modificarInterpretacionProcedimiento':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            if (valorAtributo('lblIdOrden') == '') { alert('FALTA ESCOGER PROCEDIMIENTO'); return false };
            if (valorAtributo('txtInterpretacion') === '') { alert('LA INTERPRETACION NO PUEDE ESTAR VACIA'); return false };

            break;

        case 'listGrillaPaciente':
            if ((valorAtributo('txtCodBus') == '' && valorAtributo('txtIdBus') == '' && valorAtributo('txtNomBus') == '')) {
                alert('INGRESE UN PARAMETRO DE BUSQUEDA');
                return false;
            }

            break;

        case 'listGrillaVentana':
            if ((valorAtributo('txtCodBus') == '' && valorAtributo('txtNomBus') == '')) {
                alert('INGRESE UN PARAMETRO DE BUSQUEDA');
                return false;
            }
            break;

        case 'direccionarLaboratorio':
            if (!(valorAtributo('lblIdClaseLab') === '4' || valorAtributo('lblIdClaseLab') === '5')) {
                alert('SOLO PUEDE DIRECCIONAR DIAGNOSTICOS O LABORATORIOS'); return false
            };
            if (valorAtributo('lblIdEstadoLab') == '2') { alert('EL PROCEDIMIENTO YA SE ENCUENTRA DIRECCIONADO'); return false };
            if (valorAtributo('lblIdEstadoLab') != '1') { alert('SOLO PUEDE DIRECCIONAR EN ESTADO SOLICITADO'); return false };
            if (valorAtributo('txtIdLaboratorioEmpresa') == '') { alert('FALTA LABORATORIO'); return false };
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('FALTA ESCOGER PROCEDIMIENTO'); return false };

            break;

        case 'crearCitaProgramacion':
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('DEBE SELECCIONAR UNA ORDEN'); return false };
            if (valorAtributo('cmbSede') == '') { alert('SELECCIONE UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbProfesionales') == '') { alert('SELECCIONE UN PROFESIONAL'); return false };
            if (valorAtributo('txtFechaCita') == '') { alert('FALTA SELECCIONAR FECHA'); return false };
            ids = $("#listaEsperaProgramacion").getDataIDs();
            if (ids.length > 0) { alert('LA ORDEN YA TIENE UNA CITA O LISTA DE ESPERA ASOCIADA.'); return false };
            break;

        case 'crearListaEsperaProgramacion':
            if (valorAtributo('lblIdProcedimientoAutoriza') == '') { alert('DEBE SELECCIONAR UNA ORDEN'); return false };
            if (valorAtributo('cmbSede') == '') { alert('SELECCIONE UNA SEDE'); return false };
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE UNA ESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE UN TIPO DE CITA'); return false };
            if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE DISCAPACIDAD FISICA'); return false };
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE GRADO DE NECESIDAD'); return false };
            ids = $("#listaEsperaProgramacion").getDataIDs();
            if (ids.length > 0) { alert('LA ORDEN YA TIENE UNA CITA O LISTA DE ESPERA ASOCIADA.'); return false };
            break;

        case 'modificarAdmisionCama':
            if (valorAtributo('lblIdAdmisionUbicacionPaciente') == '') { alert('FALTA SELECCIONAR PACIENTE'); return false };
            break;

        case 'enviarOrdenMedicaURG':
            // if (valorAtributo('lblIdAdmisionAgen') == '') { alert('DEBE SELECCIONAR UN PACIENTE CON UNA ADMISION ACTIVA'); return false };
            if (valorAtributo('cmbOrdenenesAdmisionURG') == '') { alert('DEBE SELECCIONAR LA ORDEN DEL SERVICIO'); return false };
            if (valorAtributo('cmbOrdenenesAdmisionURG') == "1") {
                if (valorAtributo('cmbTipoAlta') == "") { alert('SELECCIONE EL TIPO DE ALTA MEDICA'); return false };
                if (valorAtributo('cmbTipoAlta') == "6") {
                    if (valorAtributo('txtIdDxMuerte') == "") { alert('INGRESE LA CAUSA BÁSICA DE MUERTE'); return false };
                    if (!$('#sw_fecha_actual').attr('checked')) {
                        if (valorAtributo('txtFechaMuerte') == "") { alert('INGRESE LA FECHA DE MUERTE'); return false };
                        if (valorAtributo('txtHoraMuerte') == "") { alert('INGRESE LA HORA DE MUERTE'); return false };
                        if (valorAtributo('txtMinutoMuerte') == "") { alert('INGRESE EL MINUTO DE MUERTE'); return false };
                        if (valorAtributo('cmbPeriodoMuerte') == "") { alert('INGRESE EL PERIODO (AM - PM)'); return false };
                        if (valorAtributo('txtHoraMuerte') <= 0 || valorAtributo('txtHoraMuerte') > 12) { alert('FORMATO DE HORA NO ES VALIDO'); return false };
                        if (valorAtributo('txtMinutoMuerte') < 0 || valorAtributo('txtHoraMuerte') > 59) { alert('FORMATO DE HORA NO ES VALIDO'); return false };
                    }
                }
            }
            break;

        case 'modificarServicioPacienteURG':
            if (valorAtributo('cmbIdTipoAdmisionModificarURG') == '') { alert('DEBE SELECCIONAR UN SERVICIO'); return false };
            break;

        case 'ingresarPacienteURG':
            if (valorAtributo('lblIdPacienteUrgencias') == '') { alert('FALTA SELECCIONAR PACIENTE'); return false };
            if (valorAtributo('cmbIdTipoServicioUrgencias') == '') { alert('FALTA SELECCIONAR SERVICIO'); return false };
            tipo_admision_ingreso = $("#cmbIdTipoServicioUrgencias option:selected").attr("title").split(":")[1].trim();
            //if (tipo_admision_ingreso == '') { alert('NO ES POSIBLE INGRESAR PACIENTE AL SERVICIO'); return false };
            //if (valorAtributo('cmbIdTipoServicioUrgencias') != 'ORI') { alert('PACIENTE DEBE INGRESAR A ORIENTACION'); return false };
            break;

        case 'subirExcelFacturacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false };
            if (!confirm('SE ELIMINARAN TODOS LOS PROCEDIMIENTOS DEL PLAN \n\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'crearPacienteUrgencias':
            if (valorAtributo('txtApellido1Urgencias') == '' && valorAtributo('txtApellido2Urgencias') == '') { alert('DEBE INGRESAR AL MENOS UN APELLIDO'); return false };
            if (valorAtributo('txtNombre1Urgencias') == '' && valorAtributo('txtNombre2Urgencias') == '') { alert('DEBE INGRESAR AL MENOS UN NOMBRE'); return false };
            if (valorAtributo('cmbSexoUrgencias') == '') { alert('FALTA SEXO PACIENTE'); return false };
            if (valorAtributo('txtFechaNacimientoUrgencias') == '' && valorAtributo('txtEdadUrgencias') == '') { alert('INGRESE FECHA DE NACIMIENTO'); return false };
            if (valorAtributoIdAutoCompletar('txtAdministradoraUrgencias') == '') { alert('FALTA ADMINISTRADORA'); return false };
            if (valorAtributoIdAutoCompletar('txtMunicipioResidenciaUrgencias') == '') { alert('FALTA MUNICIPIO'); return false };
            break;

        case 'nuevoIdPacienteUrgencias':
            if (valorAtributo('cmbTipoIdUrgencias') == '') { alert('DEBE SELECCIONAR UN TIPO DE DOCUMENTO'); return false };
            if (valorAtributo('txtIdentificacionUrgencias') == '' && ["AS", "MS"].indexOf(valorAtributo('cmbTipoIdUrgencias')) == -1) { alert('FALTA DIGITAR NUMERO DE IDENTIFICAICON'); return false };
            break;

        case 'modificaRuta':
            if (valorAtributo('txtId') == '') { alert('DIGITE EL ID'); return false };
            if (valorAtributo('txtNombre') == '') { alert('DIGITE NOMBRE'); return false };
            if (valorAtributo('txtDuracion') == '') { alert('DIGITE DURACION'); return false };
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false };
            if (valorAtributo('cmbPlan') == '') { alert('SELECCIONE PLAN'); return false };
            if (valorAtributo('cmbCiclica') == '') { alert('SELECCIONE CICLICA'); return false };
            break;

        case 'pacienteConsulta':
            if (valorAtributo('lblIdTipoAdmision') == '') { alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert(' DEBE SELECCIONAR UN FOLIO'); return false; }
            break;

        case 'modificarTipoCitaARutaPaciente':
            if (valorAtributo('cmbIdEspecialidadEdit') == '') { alert('SELECCIONE UNA ESPECIALIDAD'); return false };
            //if (valorAtributo('cmbIdSubEspecialidadEdit') == '') { alert('SELECCIONE UNA SUBESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCitaEdit') == '') { alert('SELECCIONE TIPO DE CITA'); return false };
            if (valorAtributo('cmbEsperarEdit') == '') { alert('SELECCIONE UN TIEMPO DE ESPERA'); return false };
            if (valorAtributo('txtOrdenEdit') == '') { alert('DIGITE ORDEN'); return false };
            if (valorAtributo('cmbNecesidadEdit') == '') { alert('SELECCIONE GRADO DE NECESIDAD'); return false };
            if (valorAtributo('txtPrestadorAsignadoEdit') == '') { alert('SELECCIONE PRESTADOR ASIGNADO'); return false };
            break;

        case 'adicionarTipoCitaARutaPaciente':
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONAR ESPECIALIDAD'); return false };
            // if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('SELECCIONAR SUBESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONAR TIPO CITA'); return false };
            if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE UN PERIODO DE ESPERA'); return false };
            if (valorAtributo('txtOrden') == '') { alert('DEBE DIGITAR ORDEN'); return false };
            if (valorAtributo('cmbNecesidad') == '') { alert('DEBE DIGITAR GRADO DE NECESIDAD'); return false };
            if (valorAtributo('txtPrestadorAsignado') == '') { alert('ESCOJA EL PRESTADOR ASIGNADO'); return false };
            break;

        case 'eliminarRutaTipoCita':
            if (valorAtributo('lblIdParametr') == '') { alert('SELECCIONE UN PARAMETRO PARA ELIMINAR'); return false };
            break;

        case 'crearPacienteRuta':
            if (valorAtributo('lblIdPaciente') == '') { alert('SELECCIONE UN PACIENTE'); return false };
            if (valorAtributo('cmbPlan') == '') { alert('SELECCIONE UN PLAN DE ATENCION'); return false };
            if (valorAtributo('cmbRuta') == '') { alert('SELECCIONE UNA RUTA DEL PLAN'); return false };
            if (valorAtributo('txtPrestadorAsignadoRuta') == '') { alert('SELECCIONE UN PRESTADOR ASIGNADO A LA RUTA'); return false };
            break;

        case 'adicionarRutaTipoCita':

            if (valorAtributo('txtId') == '') { alert('SELECCIONE EL PLAN AL CUAL DESEA BORRARLE EL PARAMETRO'); return false };
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('DEBE SELECCIONAR UNA ESPECIALIDAD'); return false };
            //if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('DEBE SELECCIONAR UNA SUBESPECIALIDAD'); return false };
            if (valorAtributo('cmbTipoCita') == '') { alert('DEBE SELECCIONAR EL TIPO DE CITA'); return false };
            if (valorAtributo('cmbEsperar') == '') { alert('DEBE SELECCIONAR UN TIEMPO DE ESPERA'); return false };
            if (valorAtributo('txtOrden') == '') { alert('DEBE DIGITAR UNA ORDEN'); return false };
            if (valorAtributo('cmbEstado') == '') { alert('DEBE SELECCIONAR UN ESTADO'); return false };
            break;

        case 'crearRuta':
            if (valorAtributo('txtId') == '') { alert('DEBE DIGITAR ID'); return false };
            if (valorAtributo('txtNombre') == '') { alert('DEBE DIGITAR UN NOMBRE'); return false };
            if (valorAtributo('txtDuracion') == '') { alert('DEBE DIGITAR LA DURACION'); return false };
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE UNA OPCION EN VIGENTE'); return false };
            if (valorAtributo('cmbPlan') == '') { alert('SELECCIONE UNA OPCION EN PLAN'); return false };
            if (valorAtributo('cmbCiclica') == '') { alert('SELECCIONE UNA OPCION EN CICLICA'); return false };
            break;

        case 'modificarReferencia':
            if (valorAtributo('lblIdArea') == '') { alert('DEBE SELECCIONAR UN AREA'); return false; }
            if (valorAtributo('txtOrdenReferencia') == '') { alert('DEBE SELECCIONAR UN ORDEN'); return false; }
            if (valorAtributo('txtLatitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LATITUD'); return false; }
            if (valorAtributo('txtLongitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LONGITUD'); return false; }
            break;
        case 'adicionarReferencia':
            if (valorAtributo('lblIdArea') == '') { alert('DEBE SELECCIONAR UN AREA'); return false; }
            if (valorAtributo('txtOrdenReferencia') == '') { alert('DEBE SELECCIONAR UN ORDEN'); return false; }
            if (valorAtributo('txtLatitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LATITUD'); return false; }
            if (valorAtributo('txtLongitudReferencia') == '') { alert('DEBE SELECCIONAR UNA LONGITUD'); return false; }
            break;
        case 'cambiarMedioPago':
            if (valorAtributo('lblIdFactura') == '') { alert('DEBE SELECCIONAR UNA FACTURA'); return false; }
            break;

        case 'modificarFormaPago':
            if (valorAtributo('lblIdFactura') == '') { alert('DEBE SELECCIONAR UNA FACTURA'); return false; }
            break;

        case 'guardarDatosEncuesta':
            if (valorAtributo('lblIdEncuestaPaciente') == '') { alert('DEBE SELECCIONAR UN paciente'); return false; }
            break;

        case 'guardarDatosPlantilla':
            if (valorAtributo('lblIdDocumento') == '') { alert('NO SE ENCUENTRA ID EVOLUCION'); return false; }
            break;

        case 'crearEncuestaPaciente':
            if (valorAtributo('lblIdPaciente') == '') { alert('DEBE SELECCIONAR UN PACIENTE.'); return false; }
            if (document.getElementById('cmbEncuestaParaCrear').value == '' || document.getElementById('cmbEncuestaParaCrear').value == '0') { alert('DEBE SELECCIONAR UNA ENCUESTA.'); return false; }
            if (document.getElementById('cmbTipoRecepccionEnc').value == '' || document.getElementById('cmbTipoRecepccionEnc').value == '0') { alert('DEBE SELECCIONAR UNA TIPO DE RECEPCION.'); return false; }
            //if (valorAtributo('lblDiligenciado') == 'Diligenciada -Cerrada') { alert('DEBE SELECCIONAR UNA ENCUESTA SIN DILIGENCIAR.'); return false; }
            //if (valorAtributo('lblDiligenciado') == 'Diligenciada - Abierta') { alert('DEBE SELECCIONAR UNA ENCUESTA SIN DILIGENCIAR.'); return false; }
            break;

        case 'cerrarEncuestaPaciente':
            try {
                if (document.getElementById('formulario.enc_pa.c1').value == '' || document.getElementById('formulario.enc_pa.c2').value == '' || document.getElementById('formulario.enc_pa.c3').value == '') {
                    alert('DEBE DILIGENCIAR TODOS LOS CAMPOS DE LA PESTA\u00D1A GENERAL 256');
                    return false;
                }
            } catch (error) {
                console.error('No se enceuntra en la encuesta de percepcion');
            }
            if (valorAtributo('lblIdEncuestaPaciente') == '') { alert('DEBE SELECCIONAR UNA ENCUESTA.'); return false; }
            if (valorAtributo('lblDiligenciado') == 'Sin Diligenciar') { alert('DEBE SELECCIONAR UNA ENCUESTA DILIGENCIADA O ABIERTA.'); return false; }
            if (valorAtributo('lblDiligenciado') == 'Cerrada') { alert('DEBE SELECCIONAR UNA ENCUESTA DILIGENCIADA ABIERTA.'); return false; }
            break;

        case 'cambiarFormaPago':
            if (valorAtributo('lblIdFactura') == '') { alert('DEBE SELECCIONAR UNA FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'P') { alert('LA FACTURA NO SE PUEDE MODIFICAR'); return false; }
            break;

        case 'accionesAlEstadoFolio':
            if ((valorAtributo('cmbAccionFolio') === '3' || valorAtributo('cmbAccionFolio') === '4' || valorAtributo('cmbAccionFolio') === '14' || valorAtributo('cmbAccionFolio') === '8') && valorAtributo('lblIdDocumento') == '') { alert('DEBE SELECCIONAR UN FOLIO'); return false; }
            if (valorAtributo('cmbAccionFolio') === '3' && traerDatoFilaSeleccionada("listDocumentosHistoricos", "ADMISION") == '') { alert('DEBE SELECCIONAR UN FOLIO CON ADMISION'); return false; }
            if (valorAtributo('cmbAccionFolio') === '3' && traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == 'NOAC') { alert('NO SE PUEDE CREAR UNA NOTA ACLARATORIA A PARTIR DE OTRA NOTA ACLARATORIA'); return false; }
            if (valorAtributo('cmbAccionFolio') == 7 && valorAtributo('txtIdEsdadoDocumento') != 0) { alert('EL ESTADO DEL FOLIO ACTUAL NO PERMITE MODIFICACIONES.'); return false; }
            //if (valorAtributo('lblIdDocumento') == '') { alert('NO SE PUEDE ATENDER PORQUE DEBE TENER UNA ADMISION.'); return false; }
            if (valorAtributo('cmbAccionFolio') == 0 && valorAtributo('cmbMotivosAbrir') == '') { alert('DEBE SELECCIONAR UN MOTIVO PARA ABRIR.'); return false; }
            if (valorAtributo('txtMotivoAccion') == '' && valorAtributo("cmbAccionFolio") != '9' && valorAtributo("cmbAccionFolio") != '1' && valorAtributo("cmbAccionFolio") != '11' && valorAtributo("cmbAccionFolio") != '14') { alert('ESCRIBA JUSTIFICACION DE LA ACCION.'); return false; }
            if (valorAtributo('lblIdAuxiliar') == '') { alert('FALTA CLAVE AUXILIAR.'); return false; }

            if (!confirm('SOLO MODIFICARA EL ESTADO DEL FOLIO \nSE REGISTRARA ESTA ACCION A SU NOMBRE CON FECHA Y HORA \n\nESTA SEGURO?')) { return false; }

            break;

        case 'crearConsentimiento':
            if (valorAtributo("lblIdAdmisionAgen") == '') { swAlert('info', 'FALTA SELECCIONAR UNA ADMISION ACTUAL'); return false; }
            //if (valorAtributo('lblIdDocumento') == '') { swAlert('info', 'FALTA SELECCIONAR FOLIO.'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { swAlert('info', 'EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
            if (valorAtributo('cmbTipoConsentimiento') == '') { swAlert('info', 'FALTA TIPO DE CONSENTIMIENTO.'); return false; }
            break;

        case 'guardarInfoConsentimiento':
            if (traerDatoFilaSeleccionada('listConsentimientosPaciente', 'ID') == '') { swAlert('info', 'FALTA SELECCIONAR CONSENTIMIENTO'); return false }
            return true;

        case 'modificarPlanAtencion':
            if (valorAtributo('txtId') == '') { alert('FALTA ID PLAN.'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('txtObservaciones') == '') { alert('FALTA OBSERVACIONES'); return false; }
            if (valorAtributo('txtDuracion') == '') { alert('FALTA DURACION'); return false; }
            break;

        case 'modificarPacienteAdmision':
            if (valorAtributo('txtFechaNac') == '') { alert('FALTA FECHA DE NACIMIENTO'); return false; }
            if (valorAtributo('cmbSexo') == '') { alert('FALTA SEXO'); return false; }
            if (valorAtributoIdAutoCompletar('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            break;

        case 'agregarTurno':
            if (valorAtributo('cmbTurno') == '') { alert('FALTA TURNO'); return false; }

            var array = valorAtributo('lblIds').split(',');

            for (i = 0; i < array.length; i++) {
                var turno = array[i].split('::')[1];
                if (valorAtributo('cmbTurno') === turno) {
                    alert('EL TURNO ' + valorAtributo('cmbTurno') + ' YA SE ENCUENTRA ASIGNADO'); return false;
                }
            }

            break;

        case 'llamarTurno':
            if (valorAtributo('lblTurno') == '') { alert('NO TIENE TURNO'); return false; }
            if (valorAtributo('lblTurno') != '1') { alert('SOLO PUEDE LLAMAR AL TURNO 1'); return false; }
            break;

        case 'eliminarTurno':
            //if (valorAtributo('lblTurno') == '') { alert('NO TIENE TURNO'); return false; }
            break;

        case 'modificarTurno':
            if (valorAtributo('lblTurno') == '') { alert('NO TIENE TURNO'); return false; }
            if (valorAtributo('lblTurno') != '1') { alert('SOLO PUEDE INGRESAR AL TURNO 1'); return false; }
            break;


        case 'procedimientoAdd':
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbIdTipoRips') == '') { alert('FALTA TIPO RIPS'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbIdGrupoCentroCosto') == '') { alert('FALTA GRUPO CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbNivel') == '') { alert('FALTA NIVEL'); return false; }
            if (valorAtributo('txtCups') == '') { alert('FALTA CUPS'); return false; }
            if (valorAtributo('txtCosto') == '') { alert('FALTA COSTO'); return false; }
            if (valorAtributo('cmbClase') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbTipoServicio') == '') { alert('FALTA TIPO DE SERVICIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA VIGENTE'); return false; }
            if (valorAtributo('cmbMostrarHc') == '') { alert('FALTA MOSTRAR HISTORIA CLINICA'); return false; }
            if (valorAtributo('cmbMostrarV') == '') { alert('FALTA VIGENTE PARA MAS DE UN PROCEDIMIENTO'); return false; }
            break;

        case 'procedimientosEdit':
            if (valorAtributo('txtId') == '') { alert('FALTA ID'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbIdTipoRips') == '') { alert('FALTA TIPO RIPS'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbIdGrupoCentroCosto') == '') { alert('FALTA GRUPO CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbNivel') == '') { alert('FALTA NIVEL'); return false; }
            if (valorAtributo('txtCups') == '') { alert('FALTA CUPS'); return false; }
            if (valorAtributo('txtCosto') == '') { alert('FALTA COSTO'); return false; }
            if (valorAtributo('cmbClase') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbTipoServicio') == '') { alert('FALTA TIPO DE SERVICIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA VIGENTE'); return false; }
            if (valorAtributo('cmbMostrarHc') == '') { alert('FALTA MOSTRAR HISTORIA CLINICA'); return false; }
            if (valorAtributo('cmbMostrarV') == '') { alert('FALTA VIGENTE PARA MAS DE UN PROCEDIMIENTO'); return false; }
            break;

        case 'crearArchivosRips':
            if (valorAtributoIdAutoCompletar('txtPlan') == '') { alert('FALTA SELECCIONAR PLAN DE CONTRATACION'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('FALTA SELECCIONAR LA SEDE'); return false; }
            if (valorAtributo('cmbIdAnio') == '') { alert('FALTA SELECCIONAR EL PERIODO DE FACTURACION'); return false; }
            if (valorAtributo('cmbIdMes') == '') { alert('FALTA SELECCIONAR EL PERIODO DE FACTURACION'); return false; }
            if (valorAtributo('cmbIdAnioPrestacion') == '') { alert('FALTA SELECCIONAR EL PERIODO DE PRESTACION'); return false; }
            if (valorAtributo('cmbIdMesPrestacion') == '') { alert('FALTA SELECCIONAR EL PERIODO DE PRESTACION'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('FALTA SELECCIONAR EL TIPO'); return false; }
            break;

        case 'asociarReciboFactura':
            if (valorAtributo('lblIdReciboAsociar') == 'F') { alert('FALTA SELECCIONAR RECIBO.'); return false; }
            if (valorAtributo('lblIdFacturaAsociar') == 'F') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE ASOCIAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE ASOCIAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'T') { alert('NO SE PUEDE ASOCIAR PORQUE LA FACTURA SE ENCUENTRA ABIERTA.'); return false; }
            if (valorAtributo('lblIdEstadoRecibo') != 'C') { alert('EL RECIBO DEBE ESTAR CERRADO.'); return false; }
            break;

        case 'desAsociarReciboFactura':
            if (valorAtributo('lblIdReciboAsociar') == 'F') { alert('FALTA SELECCIONAR RECIBO.'); return false; }
            if (valorAtributo('lblIdFacturaAsociar') == 'F') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE DESASOCIAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE DESASOCIAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'T') { alert('NO SE PUEDE DESASOCIAR PORQUE LA FACTURA SE ENCUENTRA ABIERTA.'); return false; }
            if (valorAtributo('lblIdEstadoRecibo') != 'C') { alert('EL RECIBO DEBE ESTAR CERRADO.'); return false; }
            break;


        case 'crearFacturaDesdeAdmision':
            if (valorAtributo('lblIdAdmision') == '') { alert('FALTA SELECCIONAR LA ADMISION'); return false; }
            if (!confirm('DESEA CREAR OTRA FACTURA Y ASOCIARLA A LA ADMISION SELECCIONADA?')) { return false; }
            break;

        case 'auditarRecibo':
            if (valorAtributo('lblIdRecibo') == '') { alert('SELECCIONE UN RECIBO PARA AUDITAR'); return false; }
            if (valorAtributo('lblIdEstadoRecibo') != 'C') { alert('EL RECIBO DEBE ESTAR CERRADO'); return false; }
            if (valorAtributo('txtIdEstadoAuditoria') == '2') { alert('EL RECIBO YA ESTA AUDITADO'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA AUDITAR EL RECIBO?')) { return false; }
            break;

        case 'auditarFactura':
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA AUDITAR'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('LA FACTURA DEBE ESTAR FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoAuditoria') == '2') { alert('NO SE PUEDE AUDITAR, YA SE ENCUENTRA EN ESTE ESTADO'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA AUDITAR LA FACTURA?')) { return false; }
            break;

        case 'modificarListaEsperaCirugia':

            if (valorAtributo('cmbGradoNecesidadMod') == '') { alert('FALTA SELECCIONAR GRADO NESECIDAD'); return false; }
            if (valorAtributo('txtAdministradoraLE') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimenLE') == '') { alert('FALTA SELECCIONAR REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacionLE') == '') { alert('FALTA SELECCIONAR  REGIMEN - PLAN'); return false; }

            break;

        case 'modificarObservacionListaEspera':

            if (valorAtributo('cmbGradoNecesidadMod') == '') { alert('FALTA SELECCIONAR GRADO NESECIDAD'); return false; }
            if (valorAtributo('txtAdministradoraLE') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimenLE') == '') { alert('FALTA SELECCIONAR REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacionLE') == '') { alert('FALTA SELECCIONAR  REGIMEN - PLAN'); return false; }

            break;

        case 'firmarPaciente':
            if (valorAtributo('lblIdTrabajo') == '') { alert('FALTA SELECCIONAR EL TRABAJO'); return false; }
            break;

        case 'crearRecibo':

            if (valorAtributo('txtIdEditPacienteRecibo') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA PLAN DE CONTRATACION'); return false; }
            if (valorAtributo('txtValorUnidadRecibo') == '') { alert('FALTA VALOR'); return false; }
            if (valorAtributo('cmbIdConceptoRecibo') == '') { alert('FALTA CONCEPTO'); return false; }
            if (valorAtributo('cmbIdMedioPago') == '') { alert('FALTA MEDIO DE PAGO'); return false; }
            break;

        case 'adicionarMunicipio':
            if (valorAtributo('lblPlanM') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA SELECCIONAR EL MUNICIPIO'); return false; }
            break;

        case 'eliminarMunicipio':
            if (valorAtributo('lblPlanM') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('lblMuni') == '') { alert('FALTA SELECCIONAR EL MUNICIPIO'); return false; }
            break;

        case 'modificarMunicipioPlan':
            if (valorAtributo('lblPlanM') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA SELECCIONAR EL MUNICIPIO'); return false; }
            break;

        case 'facturarLiquidacionCirugia':
            if (valorAtributo('lblIdLiquidacion') == '') { alert('FALTA SELECCIONAR LA LIQUIDACION'); return false; }
            break;

        case 'crearFacturaArticulo':

            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') != '1') { alert('EL DOCUMENTO DEBE ESTAR FINALIZADO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }

            break;
        case 'adicionarFuncionarioVenta':

            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA EL TIPO DE IDENTIFICACION!!'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL FUNCIONARIO'); return false; }
            if (valorAtributo('lblIdTercero') == '') { alert('SELECCIONE UN TERCERO!!'); return false; }

            break;

        case 'modificarFuncionarioVenta':


            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }

            break;
        case 'eliminarFuncionarioVenta':


            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }

            break;
        case 'eliminarFuncionarioVenta':


            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION DEL FUNCIONARIO'); return false; }

            break;

        case 'adicionarTercero':

            if (valorAtributo('txtTipoId') == '') { alert('FALTA EL TIPO IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtIdTercero') == '') { alert('FALTA IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtNombreTercero') == '') { alert('FALTA EL NOMBRE DEL TERCERO'); return false; }
            if (valorAtributo('cmbProveedor') == '') { alert('SELECCIONE SI ES PROVEEDOR!!'); return false; }

            break;

        case 'eliminarTercero':

            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR EL TERCERO'); return false; }

            break;
        case 'modificarTercero':

            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR EL TERCERO'); return false; }
            if (valorAtributo('txtTipoId') == '') { alert('FALTA EL TIPO IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtIdTercero') == '') { alert('FALTA IDENTIFICACION DEL TERCERO'); return false; }
            if (valorAtributo('txtNombreTercero') == '') { alert('FALTA EL NOMBRE DEL TERCERO'); return false; }
            if (valorAtributo('cmbProveedor') == '') { alert('SELECCIONE SI ES PROVEEDOR!!'); return false; }

            break;
        case 'adicionarTrabajoControl':
            if (valorAtributo('txtIdFactura') == '') { alert('SELECCIONE LA FACTURA'); return false; }
            if (valorAtributo('txtNumeroOrdenN') == '') { alert('FALTA EL NUMERO DE ORDEN!!'); return false; }
            if (valorAtributo('cmbNombreLabo') == '') { alert('SELECCIONE EL PROVEEDOR'); return false; }
            if (valorAtributo('cmbNombreFuncionario1') == '') { alert('SELECCIONE FUNCIONARIO QUE RECIBE!!'); return false; }
            if (valorAtributo('txtFechaRecibeF') == '') { alert('SELECCIONE FECHA RECIBE!!'); return false; }
            if (valorAtributo('txtFechaCumpli') == '') { alert('SELECCIONE FECHA CUMPLIMIENTO!!'); return false; }
            break;

        case 'eliminarTrabajo':
            if (valorAtributo('lblIdTrabajo') == '') { alert('SELECCIONE EL TRABAJO'); return false; }
            if (valorAtributo('txtNumeroOrdenN') == '') { alert('SELECCIONE EL TRABAJO'); return false; }
            if (valorAtributo('lblFirma1') != '') { alert('NO SE PUEDE ELIMINAR, YA ESTA FIRMADO EL TRABAJO'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR EL TRABAJO?')) { return false; }

            break;


        case 'modificarDatosEnvioTrabajo':
            if (valorAtributo('lblIdTrabajo') == '') { alert('SELECCIONE EL TRABAJO'); return false; }
            if (valorAtributo('lblFirma1') != '') { alert('NO SE PUEDE MODIFICAR, YA ESTA FIRMADO ENVIADO'); return false; }
            if (valorAtributo('txtNumeroOrdenN') == '') { alert('FALTA EL NUMERO DE ORDEN!!'); return false; }
            if (valorAtributo('cmbNombreLabo') == '') { alert('SELECCIONE EL PROVEEDOR'); return false; }
            if (valorAtributo('cmbNombreFuncionario1') == '') { alert('SELECCIONE FUNCIONARIO QUE RECIBE!!'); return false; }
            if (valorAtributo('txtFechaRecibeF') == '') { alert('SELECCIONE FECHA RECIBE!!'); return false; }
            if (valorAtributo('txtFechaCumpli') == '') { alert('SELECCIONE FECHA CUMPLIMIENTO!!'); return false; }



            break;

        case 'recepcionarTrabajo':
            if (valorAtributo('txtIdEstado') != 'E') { alert('EL TRABAJO NO SE HA ENVIADO!!'); return false; }
            if (valorAtributo('lblFirma1') === '') { alert('FALTA FIRMA ENTREGADO'); return false; }
            if (valorAtributo('cmbNombreFuncionario2') == '') { alert('FALTA EL FUNCIONARIO'); return false; }
            break;
        case 'entregarTrabajo':
            if (valorAtributo('txtIdEstado') != 'C') { alert('NO SE TIENEN TRABAJOS POR ENTREGAR!!'); return false; }
            if (valorAtributo('txtNombreQuienRep') == '') { alert('FALTA EL NOMBRE DEL CLIENTE QUE RECIBE'); return false; }
            if (valorAtributo('lblFirma2') === '') { alert('FALTA FIRMA CUMPLIDO'); return false; }
            break;


        case 'agregarDetalleArticuloTrabajo':
            if (valorAtributo('lblIdTrabajoVentanita') != '') { alert('ARTICULO YA TIENE TRABAJO.'); return false; }
            break;

        case 'eliminarDetalleArticuloTrabajo':
            if (valorAtributo('lblIdTrabajoVentanita') === '') { alert('ARTICULO NO TIENE TRABAJO PARA ELIMINAR.'); return false; }
            break;

        case 'adicionarPiePagina':
            if (valorAtributo('txtNombre') == '') { alert('EL PIE DE PAGINA NO DEBE ESTAR VACIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('SELECCIONE UN TIPO'); return false; }
            break;


        case 'modificarPiePagina':
            if (valorAtributo('lblId') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('EL PIE DE PAGINA NO DEBE ESTAR VACIO'); return false; }
            if (valorAtributo('cmbTipo') == '') { alert('SELECCIONE UN TIPO'); return false; }
            break;

        case 'cambioEstadoFolio':

            if (valorAtributo('cmbIdEstadoFolioEdit') == '') { alert('FALTA ESTADO'); return false; }

            if (valorAtributo('cmbIdEstadoFolioEdit') == '7' || valorAtributo('cmbIdEstadoFolioEdit') == '10') {
                if (valorAtributo('cmbIdMotivoEstadoEdit') == '') { alert('FALTA MOTIVO'); return false; }
                if (valorAtributo('cmbMotivoClaseFolioEdit') == '') { alert('FALTA CLASIFICACION'); return false; }
            };

            break;

        case 'modificarTipoProcedim':

            if (valorAtributo('txtId') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtRecomendaciones') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }

            break;

        case 'crearTipoCitaProcedimiento':

            if (valorAtributo('txtId') == '') { alert('FALTA EL ID TIPO DE CITA DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA DEFINIR EL PROCEDIMIENTO'); return false; }

            break;

        case 'eliminarTipoCitaProcedimiento':

            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }

            break;

        case 'adicionarCentroCosto':
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            //if (valorAtributo('cmbPadre') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }

            break;

        case 'eliminarCentroCosto':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            break;

        case 'modificarCentroCosto':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            //if (valorAtributo('cmbPadre') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }
            break;
        case 'adicionarProcesosHD':
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }

            break;
        case 'modificarProcesosHD':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }

            break;

        case 'adicionarModuloHD':
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }

            break;
        case 'modificarModuloHD':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA NOMBRE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('SELECCIONE VIGENCIA'); return false; }

            break;



        case 'crearTipoCita':

            if (valorAtributo('txtNombre') == '') { alert('FALTA DEFINIR EL NOMBRE DEL TIPO DE CITA'); return false; }
            if (valorAtributo('txtRecomendaciones') == '') { alert('FALTA ANADIR RECOMENDACIONES'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA ANADIR EL ESTADO DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbProgramaTipoCita') == '') { alert('FALTA ANADIR EL PROGRAMA ASOCIADO'); return false; }

            break;

        case 'crearArticuloTemporal':

            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('txtNombreComercial') == '') { alert('FALTA NOMBRE COMERCIAL DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR PRESENTACION'); return false; }
            if (valorAtributo('txtConcentracion') == '') { alert('FALTA SELECCIONAR CONCENTRACION'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (jQuery("#listGrillaViaArticulo").getDataIDs().length == 0) { alert('FALTA AGREGAR VIA'); return false; }

            break;

        case 'agregarListaArticuloTemporal':

            if (valorAtributo('cmbViaArticulo') == '') { return false; }

            var ids = jQuery("#listGrillaViaArticulo").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaViaArticulo").getRowData(i);
                if (c == valorAtributo('cmbViaArticulo')) {
                    alert('YA EXISTE EN LA LISTA');
                    return false;
                }
            }


            break;


        case 'crearPlantillaEvolucion':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            break;

        case 'modificarPlantillaEvolucion':
            if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
                alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
                return false;
            }
            break;

        case 'crearPlantillaElemento':
            if (valorAtributo('lblCodigo') == '') { alert('SELECCIONE UNA PLANTILLA'); return false; }
            if (valorAtributo('txtDescripcion') == '') { alert('FALTA DESCRIPCION'); return false; }
            break;

        case 'modificarPlantillaElemento':
            if (valorAtributo('txtPlantilla') == '') { alert('SELECCIONE UNA PLANTILLA'); return false; }
            if (valorAtributo('txtOrden') == '') { alert('SELECCIONE UN ELEMENTO DE LA PLANTILLA'); return false; }
            break;


        case 'eliminarTratamientoPorDiente':
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR'); return false; }
            if (valorAtributo("lblIdTnsDienteTratamien") == '') { alert('SELECCIONE UNA EVOLUCION'); return false; }
            if (valorAtributo('txtIdEvolucionTratamiento') != valorAtributo('lblIdDocumento')) { alert('SOLO SE PUEDE MODIFICAR LA EVOLUCION DEL MISMO FOLIO SELECCIONADO'); return false; }
            break;

        case 'modificarTratamientoPorDiente':
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR'); return false; }
            if (valorAtributo("lblIdTnsDienteTratamien") == '') { alert('SELECCIONE UNA EVOLUCION'); return false; }
            if (valorAtributo('txtIdEvolucionTratamiento') != valorAtributo('lblIdDocumento')) { alert('SOLO SE PUEDE MODIFICAR LA EVOLUCION DEL MISMO FOLIO SELECCIONADO'); return false; }
            if (valorAtributo('txtDesTratamientoPorDiente') == '') { alert('DIGITE UNA DESCRIPCION DEL TRATAMIENTO'); return false; }
            if (valorAtributo('txtIdProcedimientoEjecutadoOdontograma') != '' && valorAtributo("txtCantidadOdontograma") == '') { alert('DIGITE UNA CANTIDAD'); return false; }
            if (valorAtributo("txtIdProcedimientoEjecutado") != '' && valorAtributo("txtIdProcedimientoEjecutadoOdontograma") == '') { alert('DIGITE UN PROCEDIMIENTO EJECUTADO'); return false; }
            if (valorAtributo("txtIdProcedimientoEjecutado") != '' && valorAtributo("txtCantidadOdontograma") == '') { alert('DIGITE UNA CANTIDAD'); return false; }
            break;
        case 'tratamientoPorDiente':
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('EL ESTADO DEL FOLIO NO PERMITE EDITAR'); return false; }
            if (valorAtributo('txtDesTratamientoPorDiente') == '') { alert('DIGITE UNA DESCRIPCION DEL TRATAMIENTO'); return false; }
            if (valorAtributo("txtIdProcedimientoEjecutadoOdontograma") != '' && valorAtributo("txtCantidadOdontograma") == '') { alert("DIGITE UNA CANTIDAD"); return false; }
            break;

        case 'eliminarAdmisionFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA ADMISION PARA ELIMINAR'); return false; }
            if (LoginSesion() != valorAtributo('lblUsuarioCrea')) { alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE ELIMINAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO SE PUEDE ELIMINAR PORQUE LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'T') { alert('NO SE PUEDE ELIMINAR PORQUE LA FACTURA SE ENCUENTRA ABIERTA.'); return false; }
            if (valorAtributo('lblIdAdmision') == '') { alert('NO HA SELECCIONADO LA ADMISION.'); return false; }
            if (!confirm('SI ELIMINA LA ADMISION, SE ELIMINARA TODA LA INFORMACION DE LA FACTURA\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'eliminarAdmisionSinFactura':
            if (LoginSesion() != valorAtributo('lblUsuarioCrea')) { alert('NO SE PUEDE ELIMINAR PORQUE USTED NO ES EL AUTOR DE ESTE REGISTRO'); return false; }
            if (valorAtributo('lblIdAdmision') == '') { alert('NO HA SELECCIONADO LA ADMISION.'); return false; }
            if (!confirm('SI LA ADMISION TIENE ASOCIADO FOLIOS NO PERMITIRA ELIMINAR\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'modificarProcedimientoCuenta':
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE MODIFICAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributoIdAutoCompletar('txtProcedimientoEditar') == '') { alert('FALTA BUSCAR EL PROCEDIMIENTO.'); return false; }
            if (valorAtributo('txtCantidadProcedimientoEditar') == '') { alert('FALTA CANTIDAD.'); return false; }
            return confirm("ESTA SEGURO DE MODIFICAR ?")
            break;

        case 'modificarAdmisionFactura':
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PUEDE MODIFICAR PORQUE LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            //if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            //if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA SELECCIONAR ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO ADMISION'); return false; }
            //if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA PROFESIONAL'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (!confirm('ESTA SEGURO DE MODIFICAR LA ADMISION ?')) { return false; }
            break;

        case 'modificarAdmisionSinFactura':
            //if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA SELECCIONAR ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO ADMISION'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (!confirm('ESTA SEGURO DE MODIFICAR LA ADMISION ?')) { return false; }
            break;

        case 'listMedicamentosEditar':
            if (valorAtributo('lblIdEditar') == '') { alert('FALTA ELEMENTO PARA EDITAR'); return false; }
            if (valorAtributo('cmbIdViaEditar') == '') { alert('FALTA VIA'); return false; }
            if (valorAtributo('cmbUnidadEditar') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('cmbCantEditar') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('cmbFrecuenciaEditar') == '') { alert('FALTA FRECUENCIA'); return false; }
            if (valorAtributo('cmbDiasEditar') == '') { alert('FALTA DIAS'); return false; }
            if (valorAtributo('txtCantidadEditar') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('cmbUnidadFarmaceuticaEditar') == '') { alert('FALTA UNIDAD FARMACEUTICA'); return false; }


            break;
        case 'listInsumosEditar':
            if (valorAtributo('lblIdEditar') == '') { alert('FALTA ELEMENTO PARA EDITAR'); return false; }
            if (valorAtributo('txtCantidadEditar') == '') { alert('FALTA CANTIDAD'); return false; }

            break;
        case 'adicionarProcedimientoE':

            if (valorAtributo('txtIdProcedimientoe') == '') { alert('FALTA EL PROCEDIMIENTO'); return false; }


            break;

        case 'anularFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ANULAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ANULAR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAnulacion') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            if (valorAtributo('txtNoDevolucion') != '') { alert('LA FACTURA YA TIENE UNA NOTA DE DEVOLUCION'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ANULAR LA FACTURA?,\nESTA OPCION NO SE PODRA REVERTIR.')) { return false; }
            break;


        case 'soloAnularFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ANULAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ANULAR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAnulacion') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            //if (valorAtributo('txtNoDevolucion') != '') { alert('LA FACTURA YA TIENE UNA NOTA DE DEVOLUCION'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ANULAR LA FACTURA?,\nESTA OPCION NO SE PODRA REVERTIR.')) { return false; }
            break;

        case 'anularFacturaVentas':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ANULAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ANULAR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAnulacion') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('LA FACTURA DEBE TENER DOCUMENTO.'); return false; }
            if (valorAtributo('txtNoDevolucion') != '') { alert('LA FACTURA YA TIENE UNA NOTA DE DEVOLUCION'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ANULAR LA FACTURA?,\nESTA OPCION NO SE PODRA REVERTIR.')) { return false; }
            break;

        case 'abrirFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ABRIR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'F') { alert('PARA ABRIR LA FACTURA, ESTA DEBE ESTAR FINALIZADA.'); return false; }
            if (valorAtributo('cmbIdMotivoAbrir') == '') { alert('SELECCIONE MOTIVO.'); return false; }
            if (valorAtributo('lblIdEstadoAuditoria') == '2') { alert('NO PUEDE ABRIR FACTURA, SE ENCUENTRA AUDITADA'); return false; }
            if (valorAtributo('lblIdEstadoFe') !== '100'  && valorAtributo('lblIdEstadoFe') !== '0' ) { alert('NO PUEDE ABRIR FACTURA, SE ENCUENTRA AUDITADA. '); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ABRIR LA FACTURA?')) { return false; }
            break;

        case 'cerrarFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA CERRAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'T') { alert('PARA CERRAR LA FACTURA, ESTA DEBE ESTAR ABIERTA.'); return false; }
            //if(valorAtributo('cmbIdMotivoAbrir') == ''){ alert('SELECCIONE MOTIVO.');  return false; }
            if (valorAtributo('lblIdEstadoFe') !== '100' && valorAtributo('lblIdEstadoFe') !== '0' ) { alert('NO PUEDE CERRAR FACTURA, SE ENCUENTRA AUDITADA .'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA CERRAR LA FACTURA?')) { return false; }
            break;

        case 'modificarFacturaPgp':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE EDITAR, LA FACTURA ESTA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE EDITAR, LA FACTURA ESTA ANULADA.'); return false; }
            if (valorAtributo('cmbNumeracionFactura') == '') { alert('FALTA NUMERACION.'); return false; }
            if (valorAtributo('txtValorNoCubierto') == '') { alert('FALTA VALOR NO CUBIERTO.'); return false; }
            if (valorAtributo('txtValorDescuento') == '') { alert('FALTA VALOR DESCUENTO.'); return false; }
            break;

        case 'modificarFechaAdmision':
            if (valorAtributo('txtFechaAdmision') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            break;

        case 'mostrarDivVentanaDescuento':

            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE FACTURA PARA REALIZAR DESCUENTO.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('LA FACTURA SE ENCUENTRA ANULADA.'); return false; }

            break;

        case 'validaNumeracionFactura':

            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA ADMISION CON FACTURA.'); return false; }
            //if(valorAtributo('lblIdEstadoCuenta')=='A'){ alert('LA CUENTA DEBE ESTAR ABIERTA.');  return false; }


            break;


        case 'reemplazarCodigoBarras':
            if (valorAtributo('txtCodBarrasNuevo') == '') { alert('SELECCIONE CODIGO A REEMPLAZAR NUEVO'); return false; }
            if (valorAtributo('txtCodBarrasBus') == '') { alert('SELECCIONE CODIGO ANTERIOR'); return false; }
            break;
        case 'listProcedimientosDetalleEditar':
            if (valorAtributo('cmbCantidadProcedimientoEditar') == '') { alert('NO HA SELECCIONADO UNA CANTIDAD'); return false; }
            if (valorAtributo('cmbNecesidadProcedimientoEditar') == '') { alert('NO HA SELECCIONADO UNA NECESIDAD'); return false; }
            if (valorAtributo('cmbProcedimientoDiagnotiscoEditar') == '') { alert('NO HA SELECCIONADO UN DIAGNOSTICO'); return false; }
            if (valorAtributo('lblIdProcedimientoEditar') == '') { alert('NO HA SELECCIONADO UN PROCEDIMIENTO'); return false; }
            break;
        case 'modificarEstadoCita':
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }

            break;


        case 'modificarEstadoCitaCuenta':
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }
            if ((valorAtributo('cmbModalidadCitaEdit') == '')) { alert('NO HA SELECCIONADO LA MODALIDAD'); return false; }
            break;


        case 'modificarEstadoCitaPrefactura':

            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }
            if (!confirm('SI CAMBIA EL ESTADO DE LA CITA, SE ELIMINARA LA PREFACTURA\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'modificarEstadoCitaPrefactura':
            if (valorAtributo('txtEstadoFacturaVentanitaAgenda') == 'F') { alert('NO SE PUEDE ELIMINAR. LA FACTURA SE ENCUENTRA FINALIZA'); return false; }
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            if (valorAtributo('cmbEstadoCitaEdit') == '') { alert('NO HA SELECCIONADO ESTADO'); return false; }
            if (valorAtributo('cmbMotivoConsultaEdit') == '') { alert('NO HA SELECCIONADO MOTIVO'); return false; }
            if ((valorAtributo('cmbEstadoCitaEdit') == 'L' || valorAtributo('cmbEstadoCitaEdit') == 'R') &&
                valorAtributo('cmbMotivoConsultaClaseEdit') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }
            if (!confirm('SI CAMBIA EL ESTADO DE LA CITA, SE ELIMINARA LA PREFACTURA\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'agregarIncapacidad':
            //if (valorAtributo('txtDiasIncapacidad') == '') { alert('NO HA SELECCIONADO LOS DIAS DE INCAPACIDAD'); return false; }
            if (valorAtributo('txtFechaIncaInicio') == '') { alert('NO HA SELECCIONADO LA FECHA DESDE'); return false; }
            //if (valorAtributo('txtFechaIncaFin') == '') { alert('NO HA SELECCIONADO LA FECHA HASTA'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO EL DOCUMENTO'); return false; }

            break;

        case 'agregarEducacionPaciente':
            if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO EL DOCUMENTO'); return false; }
            if (valorAtributo('txtObservacionesEducacion').trim() == '') { alert('SE DEBE DILIGENCIAR EL TEXTO \n DE EDUCACION AL PACIENTE'); return false; }
            break;
        case 'editaAsistenciaPaciente':
            if (valorAtributo('lblIdCitaEdit') == '') { alert('NO HA SELECCIONADO UN PACIENTE'); return false; }
            break;


        case 'guardarSeguimientoTarea':
            if (valorAtributo('lblIdEstadoTareaEdita') == '0') { alert('LA TAREA DEBE ESTAR EJECUTADA'); return false; }
            if (valorAtributo('cmbCumple') == '') { alert('SELECCIONE SI CUMPLE'); return false; }
            if (valorAtributo('txtSeguimiento') == '') { alert('DILIGENCIE EL SEGUIMIENTO'); return false; }
            break;

        case 'guardarSiguienteTarea':

            if (valorAtributo('txtTarea') == '') { alert('DILIGENCIE LA TAREA'); return false; }
            if (valorAtributo('txtFechaTarea') == '') { alert('INGRESE LA FECHA DE ENTREGA'); return false; }
            if (valorAtributo('cmbTareaResponsable') == '') { alert('SELECCIONE AL RESPONSABLE DE LA TAREA'); return false; }

            break;

        case 'eliminarDescuento':
            if (valorAtributo('lblIdArtDescuento') == '') { alert('SELECCIONE UN ARTICULO'); return false; }
            if (valorAtributo('lblSerialDescuento') == '') { alert('NO ESTA EL SERIAL DEL ARTICULO'); return false; }
            break;
        case 'crearnotificacionPersona':
            if (valorAtributo('lblIdNotificacion') == '') { alert('SELECCIONE LA NOTIFICACION'); return false; }
            break;
        case 'crearDescuento':
            if (valorAtributo('txtDescuentoArt') == '') { alert('FALTA INGRESAR EL VALOR DEL DESCUENTO'); return false; }
            if (valorAtributo('txtSerial') == '') { alert('FALTA EL SERIAL DEL ARTICULO'); return false; }

            break;
        case 'traerTrEgresos':
            if (valorAtributo('txtDocEgreso') == '') { alert('FALTA INGRESAR ID DE DOCUMENTO DE SALIDA'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO DE ENTRADA'); return false; }

            break;
        case 'eliminarGrupoUsuario':
            if (valorAtributo('lblIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA SELECCIONAR UN USUARIO'); return false; }

            break;
        case 'crearGrupoUsuario':
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR OPCION'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA SELECCIONAR UN ROL'); return false; }
            break;

        case 'agregarPermisoPerfil':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdOpcion') == '') { alert('FALTA SELECCIONAR UNA OPCION DEL SISTEMA'); return false; }
            break;

        case 'asignarAntecedenteFolio':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR UN TIPO DE FOLIO'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR UN ANTECEDENTE'); return false; }
            break;

        case 'eliminarAntecedenteFolio':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR UN TIPO DE FOLIO'); return false; }
            if (valorAtributo('lblIdAntec') == '') { alert('FALTA SELECCIONAR UN TIPO DE ANTECEDENTE'); return false; }
            break;

        case 'agregarEspProfesional':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR UNA ESPECIALIDAD'); return false; }
            if (valorAtributo('txtIdGrupo') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            break;

        case 'eliminarProfesionalEspecialidad':
            if (valorAtributo('lblIdGrupo') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            break;
        case 'agregaUsuarioBodega':
            if (valorAtributo('txtId') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('txtIdGrupo') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            break;

        case 'eliminarProfesionalBodega':
            if (valorAtributo('lblIdGrupo') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            break;
        case 'agregarSede':
            if (valorAtributo('txtIdentificacion') == '') { alert('SELECCIONE UN PROFESIONAL'); return false; }
            if (valorAtributo('cmbIdSede') == '') { alert('SELECCIONE UNA SEDE'); return false; }
            break;

        case 'agregarSedeBodega':
            if (valorAtributo('txtIdentificacion') == '') { alert('SELECCIONE UN PROFESIONAL'); return false; }
            if (valorAtributo('cmbIdSede') == '') { alert('SELECCIONE UNA SEDE DE LA TABLA'); return false; }
            break;

        case 'modificarUsuario':
            if (valorAtributo('txtPNombre') == '') { alert('FALTA PRIMER NOMBRE'); return false; }
            if (valorAtributo('txtPApellido') == '') { alert('FALTA PRIMER APELLIDO'); return false; }
            if (valorAtributo('txtNomPersonal') == '') { alert('FALTA NOMBRE COMPLETO'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION'); return false; }
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA TIPO DE IDENTIFICACION'); return false; }
            if (valorAtributo('txtUSuario') == '') { alert('FALTA NOMBRE DE USUARIO'); return false; }
            if (valorAtributo('cmbIdTipoProfesion') == '') { alert('FALTA PROFESION'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('FALTA LA SEDE'); return false; }

            break;
        case 'crearUsuario':
            if (valorAtributo('txtPNombre') == '') { alert('FALTA PRIMER NOMBRE'); return false; }
            if (valorAtributo('txtPApellido') == '') { alert('FALTA PRIMER APELLIDO'); return false; }
            if (valorAtributo('txtNomPersonal') == '') { alert('FALTA NOMBRE COMPLETO'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA LA IDENTIFICACION'); return false; }
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA TIPO DE IDENTIFICACION'); return false; }
            if (valorAtributo('txtUSuario') == '') { alert('FALTA NOMBRE DE USUARIO'); return false; }
            if (valorAtributo('cmbIdTipoProfesion') == '') { alert('FALTA PROFESION'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('FALTA SEDE PRINCIPAL'); return false; }

            break;
        case 'modificarFechaDocumentoInventario':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') == '0') { alert('EL DOCUMENTO DEBE ESTAR FINALIZADO'); return false; }
            if (valorAtributo('lblNaturaleza') != 'I') { alert('EL DOCUMENTO DEBE SER DE ENTRADA'); return false; }
            if (valorAtributo('lblIdBodega') != '1') { alert('LA BODEGA DEBE SER FARMACIA'); return false; }
            if (valorAtributo('txtFechaModifica') == '') { alert('FALTA FECHA'); return false; }

            break;
        case 'adicionarMedicamentoPlan':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdPrecioArticulo') == '') { alert('FALTA SELECCIONAR PRECIO'); return false; }
            break;
        case 'modificarMedicamentoPlan':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdPrecioArticulo') == '') { alert('FALTA SELECCIONAR PRECIO'); return false; }
            break;
        case 'eliminarMedicamentoPlan':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            break;

        case 'adicionarExcepcion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCantidadExcepcion') == '') { alert('FALTA SELECCIONAR LA CANTIDAD'); return false; }
            if (valorAtributo('cmbmes') == '') { alert('FALTA SELECCIONAR MES'); return false; }
            if (valorAtributo('cmbyear') == '') { alert('FALTA SELECCIONAR AÑO'); return false; }

            break;
        case 'modificarExcepcion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCantidadExcepcion') == '') { alert('FALTA SELECCIONAR LA CANTIDAD'); return false; }
            if (valorAtributo('cmbmes') == '') { alert('FALTA SELECCIONAR MES'); return false; }
            if (valorAtributo('cmbyear') == '') { alert('FALTA SELECCIONAR AÑO'); return false; }

            break;
        case 'eliminarExcepcion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR EL PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR EL PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCantidadExcepcion') == '') { alert('FALTA SELECCIONAR LA CANTIDAD'); return false; }
            if (valorAtributo('cmbmes') == '') { alert('FALTA SELECCIONAR MES'); return false; }
            if (valorAtributo('cmbyear') == '') { alert('FALTA SELECCIONAR AÑO'); return false; }

            break;
        case 'adicionarLiquidacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR UN PLAN'); return false; }
            if (valorAtributo('cmbOrdenProcedimiento') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcentaje') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLateralidad') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'eliminarLiquidacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR UN PLAN'); return false; }
            if (valorAtributo('cmbOrdenProcedimiento') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcentaje') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLateralidad') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'modificaLiquidacion':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR UN PLAN'); return false; }
            if (valorAtributo('cmbOrdenProcedimiento') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcentaje') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLateralidad') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'adicionarProcedimientoLiquidacion':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'eliminarLiquidacionProcedimiento':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'modificaLiquidacionProcedimiento':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'eliminarLiquidacion':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;

        case 'modificaLiquidacion':
            if (valorAtributo('txtIdProcedimientoLiquida') == '') { alert('FALTA SELECCIONAR EL ID DEL PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbOrdenProcedimientto') == '') { alert('FALTA SELECCIONAR UN ORDEN PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPorcen') == '') { alert('FALTA SELECCIONAR UN PORCENTAJE'); return false; }
            if (valorAtributo('cmbLate') == '') { alert('FALTA SELECCIONAR LATERALIDAD'); return false; }
            if (valorAtributo('cmbVigentte') == '') { alert('FALTA SELECCIONAR EL ESTADO VIGENTE'); return false; }
            break;
        case 'adicionarRangoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA SELECCIONAR EL TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA SELECCIONAR EL ID DEL RANGO'); return false; }
            if (valorAtributo('txtCopago') == '') { alert('FALTA DILIGENCIAR EL COPAGO'); return false; }
            break;

        case 'eliminaRangoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA SELECCIONAR EL TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA SELECCIONAR EL ID DEL RANGO'); return false; }
            if (valorAtributo('txtCopago') == '') { alert('FALTA DILIGENCIAR EL COPAGO'); return false; }
            break;

        case 'modificaRangoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA SELECCIONAR EL TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA SELECCIONAR EL ID DEL RANGO'); return false; }
            if (valorAtributo('txtCopago') == '') { alert('FALTA DILIGENCIAR EL COPAGO'); return false; }
            break;


        case 'modificaTransaccionSerial':
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR TRANSACCION'); return false; }
            if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
            break;

        case 'crearTransaccionDevHG':
            if (valorAtributo('txtFV') == '') { alert('SELECCIONE UN LOTE'); return false; }
            break;
        case 'eliminarLiquidacionCirugia':
            if (valorAtributo('lblIdLiquidacion') == '') { alert('SELECCIONE UNA LIQUIDACION PARA ESTA CUENTA'); return false; }
            break;
        case 'crearLiquidacionCirugia':
            //if(valorAtributo('lblIdEstadoFactura') !='A' ){ alert('EL ESTADO DE LA CUENTA DEBE ESTAR EN ABIERTA');  return false; }
            //if(valorAtributo('lblIdLiquidacion') !='' ){ alert('YA EXISTE UNA LIQUIDACION PARA ESTA CUENTA');  return false; }			  
            //if(valorAtributo('cmbIdAnestesiologo') =='' ){ alert('FALTA ANESTESIOLOGO ');  return false; }	
            if (valorAtributo('cmbIdTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }
            if (valorAtributo('cmbIdFinalidad') == '') { alert('FALTA FINALIDAD'); return false; }
            if (valorAtributo('cmbIdTipoAtencion') == '') { alert('FALTA TIPO ATENCION'); return false; }

            if (valorAtributo('cmbIdTipoAnestesia') == '2' && valorAtributo('chk_Anestesiologo') == 'true') { alert('TIPO ANESTESIA LOCAL NO PUEDE TENER SELECCIONADO ANESTESIOLOGO'); return false; }
            if (valorAtributo('chk_Anestesiologo') == 'true' && valorAtributo('cmbIdAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO '); return false; }


            break;

        case 'crearLiquidacionCirugiaContratadas':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA'); return false; }
            if (valorAtributo('lblIdPlan') == '') { alert('FALTA EL PLAN'); return false; }
            if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR CITA DE ADMISION'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }

            break;


        case 'eliminarLiquidacionCirugiaContratadas':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA'); return false; }
            if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR CITA DE ADMISION'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }


            break;

        case 'editarProcedimientoLiquidacionCirugiaContratadas':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA'); return false; }
            if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR CITA DE ADMISION'); return false; }
            if (valorAtributo('lblIdProcedimientoLiquidacionVentanita') == '') { alert('FALTA PROCEDIMIENTO'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE LIQUIDAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }



            break;


        case 'modificarLiquidacionCirugia':
            //if(valorAtributo('lblIdEstadoCuenta') !='A' ){ alert('EL ESTADO DE LA CUENTA DEBE ESTAR EN ABIERTA');  return false; }
            if (valorAtributo('lblIdLiquidacion') == '') { alert('DEBE EXISTIR Y SELECCIONAR UNA LIQUIDACION'); return false; }
            if (valorAtributo('cmbIdTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }
            if (valorAtributo('cmbIdFinalidad') == '') { alert('FALTA FINALIDAD'); return false; }
            if (valorAtributo('cmbIdTipoAtencion') == '') { alert('FALTA TIPO ATENCION'); return false; }

            if (valorAtributo('cmbIdTipoAnestesia') == '2' && valorAtributo('chk_Anestesiologo') == 'true') { alert('TIPO ANESTESIA LOCAL NO PUEDE TENER SELECCIONADO ANESTESIOLOGO'); return false; }
            if (valorAtributo('chk_Anestesiologo') == 'true' && valorAtributo('cmbIdAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO '); return false; }

            break;
        case 'reportarEvento':
            if (valorAtributo('txtIdEvento') == '') { alert('SELECCIONE UN EVENTO'); return false; }
            break;
        case 'priorizarEvento':
            if (valorAtributo('txtIdEvento') == '') { alert('SELECCIONE UN EVENTO'); return false; }
            if (valorAtributo('cmbIdTipoEvento') == '') { alert('SELECCIONE UN TIPO DE EVENTO'); return false; }
            // if (valorAtributo('cmbIdIModulo') == '') { alert('SELECCIONE UN MODULO'); return false; }
            if (valorAtributo('cmbIdIdProceso') == '') { alert('SELECCIONE UN PRROCESO AL QUE APLICA'); return false; }
            if (valorAtributo('cmbIdIdProceso') === '3') {
                if (valorAtributo('cmbIdIModulo') == '') { alert('SELECCIONE UN MODULO'); return false; }
            }

            if (valorAtributo('cmbIdIdResponsable') == '') { alert('FALTA RESPONSABLE DE '); return false; }
            if (valorAtributo('txtPriorizacion') == '') { alert('FALTA PRIORIZACION'); return false; }
            break;
        case 'anuladoEvento':
            if (valorAtributo('txtIdEvento') == '') { alert('SELECCIONE UN EVENTO'); return false; }
            if (valorAtributo('txtObservacion') == '') { alert('REGISTRE EN LA OBSERVACION EL MOTIVO DE ANULACION'); return false; }
            break;

        case 'entregarEvento':
            if (valorAtributo('txtIdEvento') == '') { alert('SELECCIONE UN EVENTO'); return false; }
            if (valorAtributo('txtObservacionRecibe') == '') { alert('REGISTRE EL SEGUIMIENTO'); return false; }
            break;
        case 'calificarEvento':
            if (valorAtributo('txtIdEvento') == '') { alert('SELECCIONE UN EVENTO'); return false; }
            if (valorAtributo('cmbIdCalificacion') == '') { alert('SELECCIONE UNA OPCION'); return false; }
            break;


        case 'aplazarEvento':
            if (valorAtributo('txtIdEvento') == '') { alert('SELECCIONE UN EVENTO'); return false; }
            if (valorAtributo('cmbIdMotivoAplaza') == '') { alert('SELECCIONE MOTIVO APLAZAMIENTO'); return false; }
            if (valorAtributo('txtFechaAplazada1') == '') { alert('FALTA SELECCIONAR FECHA DE APLAZAMIENTO'); return false; }
            if (valorAtributo('txtObservacionAplaza') == '') { alert('DILIGENCIE LA OBSERVACION DEL MOTIVO DE APLAZAMIENTO'); return false; }

            break;
        case 'adicionaDescuentoFactura':

            if (valorAtributo('txtValorDescuentoCuenta') == '') { alert('VALOR DEL DESCUENTO NO PUEDE SER VACIO.'); return false; }
            if (valorAtributo('cmbIdQuienAutoriza') == '') { alert('FALTA QUIEN AUTORIZA.'); return false; }
            if (valorAtributo('cmbIdMotivoAutoriza') == '') { alert('FALTA MOTIVO.'); return false; }

            if (parseInt(valorAtributo('txtValorDescuentoCuenta')) <= 0) {
                alert('EL DESCUENTO DEBE SER MAYOR A CERO.');
                return false;
            }

            if (parseInt(valorAtributo('txtValorDescuentoCuenta')) > parseInt(valorAtributo('lblValorBaseDescuento'))) {
                alert('EL DESCUENTO NO PUEDE SER MAYOR AL TOTAL.');
                return false;
            }

            break;

        case 'revertirDescuentoFactura':
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA FACTURA PARA REVERTIR DESCUENTO.'); return false; }
            break;

        case 'crearAdmisionFactura':
            var valorAtributoCmbTipoAdmision = valorAtributo("cmbTipoAdmision");
            // id de admisiones que son procedimiento para enfermeria
            var tipoAdmision = ["495", "466", "465", "496"];
            if (
                (valorAtributo('lblIdCita') == "" && valorAtributo("cmbTipoAdmision") != "LA") && valorAtributo("cmbTipoAdmision") != "539"
                && (valorAtributo('cmbIdEspecialidad') != "305" || valorAtributo("cmbTipoAdmision") != "470")
                && (valorAtributo("cmbIdEspecialidad") != "218" && !tipoAdmision.includes(valorAtributoCmbTipoAdmision))
                && (valorAtributo('cmbIdEspecialidad') != "311" || valorAtributo("cmbTipoAdmision") != "519")
                && (valorAtributo('cmbIdEspecialidad') != "311" || valorAtributo("cmbTipoAdmision") != "500")
                && (valorAtributo('cmbIdEspecialidad') != "312" || (valorAtributo("cmbTipoAdmision") != "458" && valorAtributo("cmbTipoAdmision") != "459" && valorAtributo("cmbTipoAdmision") != "460"))
            ) {
                if (valorAtributo('lblIdCita') == '') { alert('FALTA SELECCIONAR UNA CITA DE AGENDA.'); return false; }
            }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            //if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            if (
                (valorAtributo('lblIdCita') === '' && 
                 (valorAtributo('cmbIdEspecialidad') === '316' || 
                  valorAtributo('cmbIdEspecialidad') === '222' ||
                  valorAtributo('cmbIdEspecialidad') === '225' ||
                  valorAtributo('cmbIdEspecialidad') === '218')) &&
                valorAtributo('cmbCausaExterna') === ''
              ) {
                alert('FALTA CAUSA EXTERNA EN INFORMACION DE LA ADMISION ADICIONAL');
                return false;
              }
            if (
                (valorAtributo('lblIdCita') === '' &&
                 (valorAtributo('cmbIdEspecialidad') === '316' || 
                  valorAtributo('cmbIdEspecialidad') === '222' ||
                  valorAtributo('cmbIdEspecialidad') === '225' ||
                  valorAtributo('cmbIdEspecialidad') === '218')) && 
                valorAtributo('cmbFinalidad') === ''
              ) {
                alert('FALTA FINALIDAD EN INFORMACION DE LA ADMISION ADICIONAL');
                return false;
              }
            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA SELECCIONAR ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO ADMISION'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA PROFESIONAL'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            //  if (valorAtributo('cmbCausaExterna') == '') { alert('FALTA CAUSA EXTERNA EN INFORMACION DE LA ADMISION ADICIONAL '); return false; }
            //if (valorAtributo('cmbFinalidad') == '') { alert('FALTA FINALIDAD EN INFORMACION DE LA ADMISION ADICIONAL'); return false; }
            //if (valorAtributo('txtNoAutorizacion') == '' || isNaN(valorAtributo('txtNoAutorizacion'))) { alert('FALTA NUMERO AUTORIZACION O EL CAMPO DEBE SER NUMERICO'); return false; }
            break;

        case 'crearPlantilla':
            if (valorAtributo('txtAnCol1') == '' || isNaN(valorAtributo('txtAnCol1'))) { alert('NO SE A INGRESADO NINGUN VALOR, EL VALOR DEBE SER NUMERICO'); return false; }
            break

        case 'modificarPlantilla':
            if (valorAtributo('txtAnCol1') == '' || isNaN(valorAtributo('txtAnCol1'))) { alert('NO SE A INGRESADO NINGUN VALOR, EL VALOR DEBE SER NUMERICO'); return false; }

            break

        case 'crearSoloFactura':
            // if (valorAtributo('txtIdDx') == '' && valorAtributo('txtDiagnostico') == 'S') { alert('FALTA DIAGNOSTICO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR EPS'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            // if (valorAtributo('txtNoAutorizacion') == '') { alert('FALTA NUMERO AUTORIZACION O EL CAMPO DEBE SER NUMERICO'); return false; }
            // if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO '); return false; }
            break;


        case 'crearFacturaDocumentoVentas':

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (valorAtributo('cmbBodegaVentas') == '') { alert('FALTA SELECCIONAR BODEGA'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }

            break;

        case 'modificarFacturaDocumentoVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA SELECCIONAR TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA SELECCIONAR PLAN CONTRATACION'); return false; }
            if (valorAtributo('cmbIdTipoAfiliado') == '') { alert('FALTA TIPO AFILIADO'); return false; }
            if (valorAtributo('cmbIdRango') == '') { alert('FALTA RANGO'); return false; }
            if (valorAtributo('cmbBodegaVentas') == '') { alert('FALTA SELECCIONAR BODEGA'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA SELECCIONAR PROFESIONAL'); return false; }
            if (!confirm('SI MODIFICA LA FACTURA SE ELIMINARAN TODOS LOS ELEMENTOS AGREGADOS\nDESEA CONTINUAR?')) { return false; }

            break;


        case 'traerElementosCodigoBarrasVentas':

            if (valorAtributo('txtIdCodigoBarrasVentas') == '') { alert('EL CODIGO NO DEBE SER VACIO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE AGREGAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }

            break;


        case 'crearTransaccionVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('cmbIdArticuloOtros') == '') { alert('SELECCIONE UN ARTICULO'); return false; }
            if (valorAtributo('txtObservacionVentasOtros') == '') { alert('FALTA OBSERVACION'); return false; }
            if (valorAtributo('cmbCantidadVentasOtros') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('txtValorUnitarioOtros') == '') { alert('FALTA VALOR'); return false; }

            break;


        case 'eliminarTransaccionVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('lblIdTransaccionVentasVentanita') == '') { alert('FALTA SELECCIONAR TRANSACCION'); return false; }

            break;


        case 'modificarTransaccionVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtValorDescuentoVentanita') == '') { alert('FALTA VALOR DESCUENTO'); return false; }
            if (parseInt(valorAtributo('txtValorDescuentoVentanita')) >= parseInt(valorAtributo('lblValorBaseVentanita'))) { alert('EL DESCUENTO NO PUEDE SER MAYOR AL VALOR DEL ARTICULO.'); return false; }


            break;


        case 'revertirTransaccionVentas':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }

            break;


        case 'modificarTransaccionVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasOtroVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtValorDescuentoOtroVentanita') == '') { alert('FALTA VALOR DESCUENTO'); return false; }
            if (parseInt(valorAtributo('txtValorDescuentoOtroVentanita')) >= parseInt(valorAtributo('lblValorBaseOtroVentanita'))) { alert('EL DESCUENTO NO PUEDE SER MAYOR AL VALOR DEL ARTICULO.'); return false; }
            break;

        case 'revertirTransaccionVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasOtroVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }

            break;

        case 'eliminarArticuloVentasOtro':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR, LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE MODIFICAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (valorAtributo('lblIdDetalleVentasOtroVentanita') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }

            break;

        case 'eliminarFacturaDocumentoOptica':

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ELIMINAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE ELIMINAR, LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE ELIMINAR ,LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (valorAtributo('lblIdEstadoDoc') == '1') { alert('NO PUEDE ELIMINAR, EL DOCUMENTO SE ENCUENTRA CERRADO.'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR LA FACTURA?')) { return false; }

            break;

        case 'eliminarSoloFactura':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA ELIMINAR.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE ELIMINAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE ELIMINAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            if (!confirm('ESTA SEGURO QUE DESEA ELIMINAR LA FACTURA?')) { return false; }
            break;

        case 'modificarSoloFactura':
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA PLAN DE CONTRATACION.'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA PARA MODIFICAR.'); return false; }
            // if (valorAtributo('txtIdDx') == '') { alert('FALTA DIAGNOSTICO '); return false; }
            // if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }

            if (!confirm('SI MODIFICA LA FACTURA SE ELIMINARAN TODOS LOS ELEMENTOS AGREGADOS\nDESEA CONTINUAR?')) { return false; }
            break;

        case 'modificarFormaPago':
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdEstadoFactura') != 'P') { alert('EL ESTADO DE LA FACTURA NO PERMITE MODIFICAR'); return false; }
            break;

        case 'crearReciboInicial':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdReciboInicial') != '') { alert('NO PUEDE CREAR ABONO INICIAL PORQUE YA EXISTE.'); return false; }
            if (valorAtributo('txtValorInicial') == '') { alert('FALTA VALOR.'); return false; }
            if (parseInt(valorAtributo('txtValorInicial')) >= parseInt(valorAtributo('lblValorCubierto'))) { alert('EL VALOR NO PUEDE SER MAYOR A VALOR CUBIERTO.'); return false; }
            if (parseInt(valorAtributo('txtValorInicial')) <= 0) { alert('EL VALOR DEBE SER MAYOR A 0.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            break;


        case 'modificarFacturaContado':

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            if (valorAtributo('lblIdReciboInicial') == '') { alert('NO PUEDE MODIFICAR, LA FACTURA YA ES DE CONTADO.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA FINALIZADA.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE MODIFICAR LA FACTURA SE ENCUENTRA ANULADA.'); return false; }
            break;

        case 'modificarFactura':
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO.'); return false; }
            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            //if (valorAtributo('txtNoAutorizacion') == '') { alert('FALTA NUMERO DE AUTORIZACION.'); return false; }
            //if (valorAtributo('cmbIdProfesionalesFactura') == '') { alert('FALTA PROFESIONAL FACTURA.'); return false; }
            //if (valorAtributo('txtFechaIngreso') == '') { alert('FALTA FECHA DE INGRESO.'); return false; }
            if (valorAtributo('txtNacimiento') == '') { alert('FALTA FECHA DE NACIMIENTO.'); return false; }
            if (valorAtributoIdAutoCompletar('txtMunicipio') == '') { alert('FALTA MUNICIPIO.'); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert('FALTA PACIENTE'); return false; }
            break;


        case 'crearReciboCartera':

            if (valorAtributo('lblIdFactura') == '') { alert('FALTA SELECCIONAR FACTURA.'); return false; }
            //if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('LA FACTURA NO SE ENCUENTRA FINALIZADA.'); return false; }
            if (valorAtributo('lblIdTipoPago') == '1') { alert('LA FACTURA DEBE SER DE TIPO CREDITO.'); return false; }
            if (valorAtributo('txtValorUnidadRecibo') == '') { alert('FALTA VALOR.'); return false; }
            if (valorAtributo('cmbIdConceptoRecibo') == '') { alert('FALTA CONCEPTO.'); return false; }

            if (parseInt(valorAtributo('txtValorUnidadRecibo')) > parseInt(valorAtributo('lblSaldoFactura'))) { alert('EL VALOR NO PUEDE SER MAYOR AL SALDO.'); return false; }

            break;

        case 'pendienteFactura':
            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA'); return false; }
            break;


        case 'crearCanastaTransaccion':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblCodigoCanasta') == '') { alert('FALTA SELECCIONAR UNA CANASTA'); return false; }
            break;

        case 'crearCanastaTrasladoBodega':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblCodigoCanasta') == '') { alert('FALTA SELECCIONAR UNA CANASTA'); return false; }
            break;

        case 'crearViaArticulo':
            if (valorAtributo('cmbViaArticulo') == '') { alert('SELECCIONE VIA DE ADMINISTRACION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            break;
        case 'eliminarViaArticulo':
            if (valorAtributo('txtIdViaArticulo') == '') { alert('SELECCIONE VIA DE ADMINISTRACION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            break;
        case 'guardaConciliacion':
            if (valorAtributo('cmbExiste') == '') { alert('SELECCIONE SI EXISTE ITERACCION'); return false; }
            if (valorAtributo('txtConciliacionDescripcion') == '') { alert('FALTA DESCRIPCION DE ITERACCION'); return false; }
            break;


        case 'eliminarRemision':
            if (valorAtributo('lblIdAdmision') != "NO_HC") {

                if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO UN FOLIO'); return false; }

                if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '9') { alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '8') { alert('EL ESTADO DEL FOLIO NO PERMITE ELIMINAR'); return false; }

            }


            break;

        case 'listRemision':


            if (valorAtributo('lblIdAdmision') != "NO_HC") {

                if (valorAtributo('lblIdDocumento') == '') { alert('NO HA SELECCIONADO UN FOLIO'); return false; }

                if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '9') { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }
                if (valorAtributo('txtIdEsdadoDocumento') == '8') { alert('EL ESTADO DEL FOLIO NO PERMITE CREAR'); return false; }

            }

            if (valorAtributo('cmbTipoEvento') == '') { alert('FALTA TIPO DE EVENTO'); return false; }
            if (valorAtributo('cmbServicioSolicita') == '') { alert('FALTA SERVICIO QUE SOLICITA'); return false; }
            if (valorAtributo('cmbServicioParaCual') == '') { alert('FALTA SERVICIO PARA EL CUAL SE SOLICITA'); return false; }
            if (valorAtributo('cmbPrioridad') == '') { alert('FALTA PRIORIDAD'); return false; }
            if (valorAtributo('cmbNivelRemis') == '') { alert('FALTA NIVEL'); return false; }

            if (valorAtributo('cmbTipoDocResponsable') == '') { alert('FALTA TIPO DOCUMENTO RESPONSABLE'); return false; }
            if (valorAtributo('txtDocResponsable') == '') { alert('FALTA DOCUMENTO DEL RESPONSABLE'); return false; }
            if (valorAtributo('txtPrimerApeResponsable') == '') { alert('FALTA PRIMER APELLIDO RESPONSABLE'); return false; }

            if (valorAtributo('txtPrimerNomResponsable') == '') { alert('FALTA PRIMER NOMBRE RESPONSABLE'); return false; }
            if (valorAtributo('txtTelefonoResponsable') == '') { alert('FALTA TELEFONO RESPONSABLE'); return false; }
            if (valorAtributo('txtDireccionResponsable') == '') { alert('FALTA DIRECCION RESPONSABLE'); return false; }
            if (valorAtributo('txtDeparResponsable') == '') { alert('FALTA DEPARTAMENTO RESPONSABLE'); return false; }
            if (valorAtributo('txtMunicResponsable') == '') { alert('FALTA MUNICIPIO RESPONSABLE'); return false; }

            if (valorAtributo('txt_InformacionRemision') == '') { alert('FALTA INFORMACION CLINICA RELEVANTE'); return false; }
            if (valorAtributo('cmbIdProfesionalRemision') == '') { alert('FALTA PROFESIONAL REALIZA'); return false; }


            if (valorAtributo('cmbVerificacionPaciente') == '') { alert('FALTA VERIFICACION PACIENTE'); return false; }
            if (valorAtributo('cmbFormatoDiligenciado') == '') { alert('FALTA FORMATO DILIGENCIADO'); return false; }
            if (valorAtributo('cmbResumenHc') == '') { alert('FALTA RESUMEN HISTORIA CLINICA'); return false; }
            if (valorAtributo('cmbDocumentoIdentidad') == '') { alert('FALTA DOCUMENTO IDENTIDAD'); return false; }
            if (valorAtributo('cmbImagenDiagnostica') == '') { alert('FALTA IMAGEN DIAGNOSTICA'); return false; }
            if (valorAtributo('cmbPacienteManilla') == '') { alert('FALTA PACIENTE MANILLA'); return false; }


            break;

        case 'listAntecedentesFarmacologicos':
            if (valorAtributo('lblIdDocumento') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('txtAntecedenteFarmacologico') == '') { alert('FALTA SELECCIONAR UN ANTECEDENTE'); return false; }
            break;

        case 'eliminaTransaccionDevolucion':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            break;
        case 'modificaTransaccionSolicitud':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            //if(valorAtributo('lblIdSolicitud') =='' ){ alert('FALTA SELECCIONAR UNA SOLICITUD');  return false; }								  						  						 
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'crearTransaccionDevolucion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdDevolucion') == '') { alert('FALTA SELECCIONAR UNA DEVOLUCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'crearTransaccionSolicitud':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdSolicitud') == '') { alert('FALTA SELECCIONAR UNA SOLICITUD'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL IMPUESTO.'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'eliminaTransaccionSolicitud':
            /* inventario */
            if (valorAtributo('lblIdAdmisionAgen') == '') {
                if (valorAtributo('cmbIdGastos') == 'S') {
                    asignaAtributo('lblIdAdmisionAgen', '0', 0);
                    return true;
                } else {
                    alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA');
                    return false;
                }
            }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL  DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            break;
        case 'eliminaSolicitud':
            /* hc */
            if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            /*if(valorAtributo('txtIdArticuloSolicitudes') =='' ){ alert('FALTA SELECCIONAR UN ARTICULO');  return false; }*/
            if (valorAtributo('txtIdArticuloSolicitudes') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidadSolicitud') == '') { alert('FALTA CANTIDAD'); return false; }
            break;
        case 'modificaSolicitud':
            if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            if (valorAtributo('txtIdArticuloSolicitudes') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidadSolicitud') == '') { alert('FALTA CANTIDAD'); return false; }
            break;
        case 'crearSolicitud':
            if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            if (valorAtributo('txtIdArticuloSolicitudes') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidadSolicitud') == '') { alert('FALTA CANTIDAD'); return false; }

            var ids = jQuery("#listGrillaSolicitudFarmacia").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaSolicitudFarmacia").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticuloSolicitudes')) {
                    alert('Atencion!!!! \nYA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'nuevoDocumentoInventarioBodegaConsumo':
            if (valorAtributo('cmbIdGastos') == 'N') {
                if (valorAtributo('lblIdAdmisionAgen') == '') { alert('FALTA SELECCIONAR PACIENTE CON ADMISION VALIDA'); return false; }
            } else {
                asignaAtributo('lblIdAdmisionAgen', '0', 0);
            }
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            break;
        case 'traerArticulo':
            /*CONSUMO DE ARTICULO A HOJA DE GASTOS*/
            //if(valorAtributo('lblIdDocumento') =='' ){ alert('FALTA SELECCIONAR UN DOCUMENTO CLINICO');  return false; }								  						  						 
            if (valorAtributo('lblIdAdmision') == '') { alert('FALTA SELECCIONAR UN PACIENTE CON UNA ADMISI�N V�LIDA'); return false; }
            if (valorAtributo('cmbNovedad') == '') { alert('FALTA SELECCIONAR UNA NOVEDAD'); return false; }
            if (valorAtributo('lblCostoHG') == '' || valorAtributo('lblCostoHG') == '0') { alert('FALTA CALCULAR EL COSTO DEL ARTICULO'); return false; }
            //if(valorAtributo('txtIdEsdadoDocumento') =='1' ){ alert('EL DOCUMENTO YA ESTA FINALIZADO');  return false; }	activar_condicion							  						  						 
            break;
        case 'traerUltimoArticulo':
            /*CONSUMO DE ULTIMO ARTICULO A HOJA DE GASTOS*/
            //if(valorAtributo('lblIdDocumento') =='' ){ alert('FALTA SELECCIONAR UN DOCUMENTO CLINICO');  return false; }								  						  						 
            if (valorAtributo('lblIdAdmision') == '') { alert('FALTA SELECCIONAR UN PACIENTE CON UNA ADMISI�N V�LIDA'); return false; }
            /* if(valorAtributo('cmbNovedad') =='' ){ alert('FALTA SELECCIONAR UNA NOVEDAD');  return false; }								  						  						  */
            if (valorAtributo('lblCostoHG') == '' || valorAtributo('lblCostoHG') == '0') { alert('FALTA CALCULAR EL COSTO DEL ARTICULO'); return false; }
            //if(valorAtributo('txtIdEsdadoDocumento') =='1' ){ alert('EL DOCUMENTO YA ESTA FINALIZADO');  return false; }	activar_condicion							  						  						 
            break;
        case 'eliminarArticuloCanasta':
            if (valorAtributo('lblIdDetalle') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            break;
        case 'adicionarArticuloCanasta':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            /* --- --- -- */
            var ids = jQuery("#listGrillaCanastaDetalle").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaCanastaDetalle").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo')) {
                    alert('YA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'modificarPlantillaCanasta':
            if (valorAtributo('txtNombreCanasta') == '') { alert('FALTA EL NOMBRE DE LA CANASTA'); return false; }
            if (valorAtributo('cmbTipoCanasta') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            break;
        case 'crearPlantillaCanasta':
            if (valorAtributo('txtNombreCanasta') == '') { alert('FALTA EL NOMBRE DE LA CANASTA'); return false; }
            if (valorAtributo('cmbTipoCanasta') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            break;
        case 'cerrarDocumentoDevolucion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'cerrarDocumentoBodega':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            //if(valorAtributo('cmbIdTipoDocumento') =='' ){ alert('FALTA SELECCIONAR TIPO DE DOCUMENTO');  return false; }					
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'eliminaTransaccion':
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'crearTransaccion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL VALOR DEL IMPUESTO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('txtValorU') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
                if (valorAtributo('txtFechaVencimiento') == '') { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if (valorAtributo('txtLote') == '') { alert('FALTA LOTE '); return false; }

                var lote = valorAtributo('txtLote');
            } else {
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('lblIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE '); return false; }
                var lote = valorAtributo('cmbIdLote');
            }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }

            if (valorAtributo('cmbIdBodega') == '6') {
                var ids = jQuery("#listGrillaTransaccionDocumento").getDataIDs();
                for (var i = 0; i < ids.length; i++) {
                    var c = ids[i];
                    var datosRow = jQuery("#listGrillaTransaccionDocumento").getRowData(c);
                    if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo') && datosRow.LOTE == lote && datosRow.SERIAL == serial) {
                        alert('YA EXISTE ESTE ARTICULO EN EL LISTADO');
                        return false;
                    }
                }
            }

            break;


        case 'crearTransaccionVentas':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }


            break;



        case 'crearTransaccion_dos':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL VALOR DEL IMPUESTO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('txtValorU') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
                if ((valorAtributo('txtFechaVencimiento') == '') && (valorAtributo('txtLote') != '')) { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if ((valorAtributo('txtFechaVencimiento') != '') && (valorAtributo('txtLote') == '')) { alert('FALTA LOTE '); return false; }
                var lote = decodeURIComponent(valorAtributo('txtLote'));
                var serial = valorAtributo('txtSerial');
            } else {
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('lblIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }


                var lote = decodeURIComponent(valorAtributo('txtIdLote_dos'));
                var serial = valorAtributo('txtSerial');
            }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }


            var ids = jQuery("#listGrillaTransaccionDocumento").getDataIDs();
            console.log('crearTransaccion_dos: ' + ids.length)
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaTransaccionDocumento").getRowData(c);

                console.log('tabla: ' + datosRow.ID_ARTICULO + ' - ' + datosRow.LOTE + ' - ' + datosRow.SERIAL);
                console.log('nuevo: ' + valorAtributo('txtIdArticulo_dos') + ' - ' + lote + ' - ' + serial);

                if (datosRow.ID_ARTICULO == valorAtributo('txtIdArticulo_dos') && datosRow.LOTE == lote && datosRow.SERIAL == serial) {



                    alert('YA EXISTE ESTE ARTICULO EN EL LISTADO, NO SE PUEDE AGREGAR');
                    return false;
                }
            }

            break;
        case 'crearTransaccionDevolucionCompra':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('lblValorImpuesto') == '') { alert('FALTA CALCULAR EL VALOR DEL IMPUESTO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('txtValorU') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtPrecioVenta') == '') { alert('FALTA PRECIO DE VENTA'); return false; }
                if ((valorAtributo('txtFechaVencimiento') == '') && (valorAtributo('txtLote') != '')) { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if ((valorAtributo('txtFechaVencimiento') != '') && (valorAtributo('txtLote') == '')) { alert('FALTA LOTE '); return false; }
                var lote = valorAtributo('txtLote');
            } else {
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('lblIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                var lote = valorAtributo('lblIdLote');
            }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }

            /* --- --- -- */
            var ids = jQuery("#listGrillaTransaccionDocumento").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listGrillaTransaccionDocumento").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo') && datosRow.LOTE == lote) {
                    alert('YA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }

            break;
        case 'creaSerialesTransaccion':
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO DEBE ESTAR ABIERTO'); return false; }
            break;

        case 'modificaTransaccion':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtFechaVencimiento') == '') { alert('FALTA FECHA DE VENCIMIENTO '); return false; }
                if (valorAtributo('txtLote') == '') { alert('FALTA LOTE '); return false; }
            } else {
                if (valorAtributo('lblIva') == '') { alert('FALTA IVA'); return false; }
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE '); return false; }
            }
            if (valorAtributo('txtSerial') == '') { alert('ERROR AL MODIFICAR!!!  \nSI EL ARTICULO TIENE SERIAL, DEBE ESCRIBIRLO Y DARLE ENTER'); return false; }
            break;



        case 'modificaTransaccionTraslado':

            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
                if (valorAtributo('txtLote') == '') { alert('FALTA LOTE '); return false; }
            } else {
                if (valorAtributo('lblIva') == '') { alert('FALTA IVA'); return false; }
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE '); return false; }
            }

            break;

        case 'modificaTransaccionOptica':
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR UN DOCUMENTO'); return false; }
            if (valorAtributo('lblIdTransaccion') == '') { alert('FALTA SELECCIONAR UNA TRANSACCION'); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR UN ARTICULO'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA LA CANTIDAD'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            if (valorAtributo('lblNaturaleza') == 'I') {
                if (valorAtributo('cmbIva') == '') { alert('FALTA SELECCIONAR IVA'); return false; }
            } else {
                if (valorAtributo('lblIva') == '') { alert('FALTA IVA'); return false; }
                if (valorAtributo('lblValorUnitario') == '') { alert('FALTA VALOR UNITARIO'); return false; }
                if (valorAtributo('cmbIdLote') == '') { alert('FALTA LOTE'); return false; }
            }
            if (valorAtributo('txtSerial') == '') { alert('ERROR AL MODIFICAR!!!  \nSI EL ARTICULO TIENE SERIAL, DEBE ESCRIBIRLO Y DARLE ENTER'); return false; }
            break;
        case 'nuevoDocumentoInventarioBodega':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('cmbIdTipoDocumento') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO'); return false; }
            if (valorAtributo('txtIdTercero') == '') { alert('FALTA SELECCIONAR TERCERO'); return false; }
            if (valorAtributo('txtValorFlete') == '') { alert('FALTA VALOR DEL FLETE (CERO SI NO EXISTE)'); return false; }
            break;
        case 'crearDocumento':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('cmbIdTipoDocumento') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO'); return false; }
            //if(valorAtributo('cmbIdEstado') =='' ){ alert('FALTA SELECCIONAR EL ESTADO');  return false; }								 			  			  
            break;
        case 'modificaDocumento':
            if (valorAtributo('cmbIdBodega') == '') { alert('FALTA SELECCIONAR UNA BODEGA'); return false; }
            if (valorAtributo('lblIdDoc') == '') { alert('FALTA SELECCIONAR DOCUMENTO'); return false; }
            if (valorAtributo('cmbIdTipoDocumento') == '') { alert('FALTA SELECCIONAR TIPO DE DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstado') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA CERRADO'); return false; }
            break;
        case 'crearBodega':
            if (valorAtributo('txtDescripcion') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdIdResponsable') == '') { alert('FALTA SELECCIONAR RESPONSABLE'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('FALTA SELECCIONAR ESTADO'); return false; }
            if (valorAtributo('cmbSWRestitucion') == '') { alert('FALTA SELECCIONAR RESTITUCION'); return false; }
            if (valorAtributo('txtAutorizacion') == '') { alert('FALTA AUTORIZACION RECIBIR COMPRAS'); return false; }
            if (valorAtributo('cmbSWRestriccionStock') == '') { alert('FALTA SELECCIONAR RESTRICCION STOCK'); return false; }
            if (valorAtributo('cmbIdBodegaTipo') == '') { alert('FALTA SELECCIONAR TIPO BODEGA'); return false; }
            if (valorAtributo('cmbSWConsignacion') == '') { alert('FALTA SELECCIONAR CONSIGNACION'); return false; }
            if (valorAtributo('cmbSWAprovechamiento') == '') { alert('FALTA SELECCIONAR APROVECHAMIENTO'); return false; }
            if (valorAtributo('cmbSWAfectaCosto') == '') { alert('FALTA SELECCIONAR AFECTA COSTO'); return false; }

            break;
        case 'modificaBodega':
            if (valorAtributo('txtDescripcion') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdCentroCosto') == '') { alert('FALTA DESCRIPCION DE LA BODEGA'); return false; }
            if (valorAtributo('cmbIdIdResponsable') == '') { alert('FALTA SELECCIONAR RESPONSABLE'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('FALTA SELECCIONAR ESTADO'); return false; }
            if (valorAtributo('cmbSWRestitucion') == '') { alert('FALTA SELECCIONAR RESTITUCION'); return false; }
            if (valorAtributo('txtAutorizacion') == '') { alert('FALTA AUTORIZACION RECIBIR COMPRAS'); return false; }
            if (valorAtributo('cmbSWRestriccionStock') == '') { alert('FALTA SELECCIONAR RESTRICCION STOCK'); return false; }
            if (valorAtributo('cmbIdBodegaTipo') == '') { alert('FALTA SELECCIONAR TIPO BODEGA'); return false; }
            if (valorAtributo('cmbSWConsignacion') == '') { alert('FALTA SELECCIONAR CONSIGNACION'); return false; }
            if (valorAtributo('cmbSWAprovechamiento') == '') { alert('FALTA SELECCIONAR APROVECHAMIENTO'); return false; }
            if (valorAtributo('cmbSWAfectaCosto') == '') { alert('FALTA SELECCIONAR AFECTA COSTO'); return false; }
            break;
        case 'modificaMedicamento':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR PRESENTACION'); return false; }
            if (valorAtributo('txtConcentracion') == '') { alert('FALTA CONCENTRACION DEL MEDICAMENTO'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('txtATC') == '') { alert('FALTA CODIGO ATC'); return false; }
            if (valorAtributo('cmbAnatomofarmacologico') == '') { alert('FALTA ANATOMOFARMACOLOGICO'); return false; }
            if (valorAtributo('cmbPrincipioActivo') == '') { alert('FALTA PRINCIPIO ACTIVO'); return false; }
            if (valorAtributo('cmbFormaFarmacologica') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'modificaArticulo':
            if (valorAtributo('txtIdArticulo') == '') { alert('FALTA SELECCIONAR ARTICULO'); return false; }
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            //if(valorAtributo('cmbIdSubTipo') =='' ){ alert('FALTA SELECCIONAR SUBTIPO');  return false; }			  
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'crearMedicamento':
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR PRESENTACION'); return false; }
            if (valorAtributo('txtIVA') == '') { alert('FALTA IVA'); return false; }
            if (valorAtributo('cmbIVAVenta') == '') { alert('FALTA IVA VENTA'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('txtATC') == '') { alert('FALTA CUM'); return false; }
            if (valorAtributo('cmbAnatomofarmacologico') == '') { alert('FALTA ANATOMOFARMACOLOGICO'); return false; }
            if (valorAtributo('cmbPrincipioActivo') == '') { alert('FALTA PRINCIPIO ACTIVO'); return false; }
            if (valorAtributo('cmbFormaFarmacologica') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'crearArticulo':
            if (valorAtributo('txtNombreArticulo') == '') { alert('FALTA NOMBRE DEL ARTICULO'); return false; }
            if (valorAtributo('cmbIdClase') == '') { alert('FALTA SELECCIONAR CLASE'); return false; }
            if (valorAtributo('cmbIdGrupo') == '') { alert('FALTA SELECCIONAR GRUPO'); return false; }
            if (valorAtributo('cmbPresentacion') == '') { alert('FALTA SELECCIONAR TIPO'); return false; }
            if (valorAtributo('txtIVA') == '') { alert('FALTA IVA'); return false; }
            if (valorAtributo('cmbIVAVenta') == '') { alert('FALTA IVA VENTA'); return false; }
            if (valorAtributo('cmbPOS') == '') { alert('FALTA SELECCIONAR POS'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('FALTA SELECCIONAR VIGENTE'); return false; }
            if (valorAtributo('cmbIdUnidadAdministracion') == '') { alert('FALTA FACTOR DE CONVERSION'); return false; }
            break;
        case 'modificaCitaArchivo':
            if (valorAtributo('cmbTipoCita') == '') { alert('FALTA SELECCIONAR TIPO DE CITA'); return false; }
            if (valorAtributo('cmbMotivoConsulta') == '') { alert('FALTA SELECCIONAR MOTIVO'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA SELECCIONAR ADMINISTRADORA'); return false; }
            if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA SELECCIONAR FECHA PACIENTE'); return false; }
            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA SELECCIONAR INSTITUCION QUE REMITE'); return false; }
            break;
        case 'modificarProcedimientoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPrecioProcedimiento') == '') { alert('FALTA SELECCIONAR PRECIO PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCodigoExterno') == '') { alert('FALTA INGRESAR CODIGO EXTERNO'); return false; }
            break;
        case 'adicionarProcedimientoPlan':
            if (valorAtributo('txtIdPlan') == '') { alert('FALTA SELECCIONAR PLAN'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA SELECCIONAR PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtPrecioProcedimiento') == '') { alert('FALTA SELECCIONAR PRECIO PROCEDIMIENTO'); return false; }
            if (valorAtributo('txtCodigoExterno') == '') { alert('FALTA INGRESAR CODIGO EXTERNO'); return false; }
            break;
        case 'cerrarDocumentoSinImp':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL DOCUMENTO YA ESTA FINALIZADO'); return false; }
            break;
        case 'cerrarFolioJasper':
            if (valorAtributo('lblIdFolioJasper') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL DOCUMENTO YA ESTA FINALIZADO'); return false; }
            break;

        case 'adicionarProcedimientosDeFactura':
            if (valorAtributo('lblIdEstadoFactura') == '') { alert('SELECCIONE UNA ADMISION CON FACTURA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO SE PERMITE EDITAR FACTURAS FINALIZADAS'); return false; }
            var ids = jQuery("#listProcedimientosDeFactura").getDataIDs();
            /*for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listProcedimientosDeFactura").getRowData(c);
                if (datosRow.CODIGO == valorAtributoIdAutoCompletar('txtIdProcedimientoCex')) {
                    alert('A1. YA EXISTE ESTE PROCEDIMIENTO EN EL LISTADO');
                    return false;
                }
            }*/
            if (valorAtributo('lblIdAdmision') == '') { alert('SELECCIONE ADMISION'); return false; }
            //if(valorAtributo('lblIdEstadoCuenta') !='A' ){ alert('LA CUENTA DEBE ESTAR ABIERTA');  return false; }			  
            if (valorAtributo('lblIdTipoAdmision') == '') { alert('SELECCIONE TIPO ADMISION'); return false; }
            if (valorAtributo('lblIdRango') == '') { alert('SELECCIONE RANGO'); return false; }
            if (valorAtributo('txtIdProcedimientoCex') == '') { alert('SELECCIONE PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('SELECCIONE CANTIDAD'); return false; }
            break;

        case 'adicionarProcedimientosSoloFactura':
            //if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA ADMISION CON FACTURA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }

            var ids = jQuery("#listProcedimientosDeFactura").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listProcedimientosDeFactura").getRowData(c);
                if (datosRow.CODIGO == valorAtributoIdAutoCompletar('txtIdProcedimientoCex')) {
                    alert('A1. YA EXISTE ESTE PROCEDIMIENTO EN EL LISTADO');
                    return false;
                }
            }

            if (valorAtributo('txtIdProcedimientoCex') == '') { alert('SELECCIONE PROCEDIMIENTO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('SELECCIONE CANTIDAD'); return false; }
            break;

        case 'adicionarArticulosFacturaInventario':

            if (valorAtributo('lblIdDoc') == '') { alert('SELECCIONE UNA DOCUMENTO'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }

            var ids = jQuery("#listArticulosDeFactura").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listArticulosDeFactura").getRowData(c);
                if (datosRow.ID == valorAtributoIdAutoCompletar('txtIdArticuloFactura')) {
                    alert('A1. YA EXISTE ESTE ARTICULO EN EL LISTADO');
                    return false;
                }
            }

            if (valorAtributo('txtIdArticuloFactura') == '') { alert('SELECCIONE ARTICULO'); return false; }
            if (valorAtributo('txtValorUnitarioArticuloFactura') == '') { alert('SELECCIONE VALOR UNITARIO'); return false; }
            if (valorAtributo('cmbCantidad') == '') { alert('SELECCIONE CANTIDAD'); return false; }


            break;

        case 'adicionarArticulosFactura':

            if (valorAtributo('lblIdFactura') == '') { alert('SELECCIONE UNA FACTURA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }
            if (valorAtributo('txtIdArticuloFactura') == '') { alert('SELECCIONE ARTICULO'); return false; }
            if (valorAtributo('lblValorUnitarioArticuloFactura') == '') { alert('FALTA VALOR UNITARIO'); return false; }
            if (valorAtributo('cmbCantidadArticuloFactura') == '') { alert('SELECCIONE CANTIDAD'); return false; }


            break;


        case 'eliminarProcedimientoCuenta':

            if (valorAtributo('lblIdEstadoFactura') == 'F') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA FINALIZADA'); return false; }
            if (valorAtributo('lblIdEstadoFactura') == 'A') { alert('NO PUEDE AGREGAR, LA FACTURA ESTA ANULADA'); return false; }

            break;


        case 'eliminaCargoTarifario':
            if (valorAtributo('lblCargo') == '') { alert('SELECCIONE CARGO'); return false; }
            if (valorAtributo('lblIdTarifario') == '') { alert('SELECCIONE TARIFARIO'); return false; }
            break;
        case 'modificaCargoTarifario':
            if (valorAtributo('lblCargo') == '') { alert('SELECCIONE CARGO'); return false; }
            if (valorAtributo('txtDescCargoTarifario') == '') { alert('FALTA DESCRIPCION CARGO'); return false; }
            if (valorAtributo('cmbIdGrupoTarifario') == '') { alert('FALTA GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdSubGrupoTarifario') == '') { alert('FALTA SUB-GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }

            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA GRUPO TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdConceptoRips') == '') { alert('FALTA CONCEPTO RIPS'); return false; }
            if (valorAtributo('cmbIdNivelAtencion') == '') { alert('FALTA NIVEL ATENCION'); return false; }
            if (valorAtributo('cmbUnidadPrecio') == '') { alert('FALTA UNIDAD PRECIO'); return false; }

            if (valorAtributo('cmbIdTipoUnidad') == '') { alert('FALTA TIPO UNIDAD PRECIO'); return false; }
            if (valorAtributo('txtGravamen') == '') { alert('FALTA GRAVAMEN'); return false; }
            if (valorAtributo('cmbHonorarios') == '') { alert('FALTA HONORARIOS'); return false; }
            if (valorAtributo('cmbExigeCantidad') == '') { alert('FALTA EXIGE CANTIDAD'); return false; }
            break;
        case 'crearCargoTarifario':
            if (valorAtributo('lblCargo') != '') { alert('PRIMERO LIMPIE'); return false; }
            if (valorAtributo('lblIdTarifario') == '') { alert('SELECCIONE TARIFARIO'); return false; }
            if (valorAtributo('txtIdCargoNuevo') == '') { alert('FALTA NUEVO CARGO'); return false; }
            if (valorAtributo('txtDescCargoTarifario') == '') { alert('FALTA DESCRIPCION CARGO'); return false; }
            if (valorAtributo('cmbIdGrupoTarifario') == '') { alert('FALTA GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdSubGrupoTarifario') == '') { alert('FALTA SUB-GRUPO TARIFARIO'); return false; }
            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }

            if (valorAtributo('cmbIdGrupoTipoCargo') == '') { alert('FALTA GRUPO TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdTipoCargo') == '') { alert('FALTA TIPO CARGO'); return false; }
            if (valorAtributo('cmbIdConceptoRips') == '') { alert('FALTA CONCEPTO RIPS'); return false; }
            if (valorAtributo('cmbIdNivelAtencion') == '') { alert('FALTA NIVEL ATENCION'); return false; }
            if (valorAtributo('txtUnidadPrecio') == '') { alert('FALTA UNIDAD PRECIO'); return false; }

            if (valorAtributo('cmbIdTipoUnidad') == '') { alert('FALTA TIPO UNIDAD PRECIO'); return false; }
            if (valorAtributo('txtGravamen') == '') { alert('FALTA GRAVAMEN'); return false; }
            if (valorAtributo('cmbHonorarios') == '') { alert('FALTA HONORARIOS'); return false; }
            if (valorAtributo('cmbExigeCantidad') == '') { alert('FALTA EXIGE CANTIDAD'); return false; }
            break;
        case 'listGrillaTarifarioEquivalencia':
            if (valorAtributo('txtIdEquivalenciaCups') == '') { alert('FALTA CODIGO CUPS'); return false; }
            if (valorAtributo('lblCargo') == '') { alert('SELECCIONE CARGO'); return false; }
            if (valorAtributo('lblIdTarifario') == '') { alert('SELECCIONE TARIFARIO'); return false; }

            break;

        case 'adicionarPersonalProc':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != '0' && valorAtributo('txtIdEsdadoDocumento') != '12') { alert('ESTADO DEL DOCUMENTO NO LO PERMITE'); return false; }
            if (valorAtributo('cmbIdProfesiones') == '') { alert('FALTA ESCOJER PROFESION'); return false; }
            if (valorAtributo('cmbIdProfesionalesProc') == '') { alert('FALTA ESCOJER PROFESIONAL'); return false; }
            /* var ids = jQuery("#listPersonalProc").getDataIDs();
             for (var i = 0; i < ids.length; i++) {
                 var c = ids[i];
                 var datosRow = jQuery("#listPersonalProc").getRowData(c);
                 if (datosRow.ID_PERSONAL == valorAtributo('cmbIdProfesionalesProc')) {
                     alert('YA EXISTE ESTE PROFESIONAL EN EL LISTADO');
                     return false;
                 }
             }*/
            break;



        case 'adicionarPersonalCirugia':
            if (valorAtributo('lblIdAgendaDetalle') == '') { alert('FALTA ESCOJER AGENDA'); return false; }
            if (valorAtributo('cmbIdProfesiones') == '') { alert('FALTA ESCOJER PROFESION'); return false; }
            if (valorAtributo('cmbIdProfesionalesProc') == '') { alert('FALTA ESCOJER PROFESIONAL'); return false; }
            break;


        case 'listPersonalProcEliminar':
            if (valorAtributo('txtIdEsdadoDocumento') != '0' && valorAtributo('txtIdEsdadoDocumento') != '12') { alert('ESTADO DEL DOCUMENTO NO LO PERMITE'); return false; }
            break;

        case 'adicionarHcCirugiaProcedimientosPos':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('ESTADO DEL DOCUMENTO DEBE SER ABIERTO'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoPosO') == '') { alert('FALTA ESCOJER SITIO'); return false; }
            if (valorAtributo('txtIdProcedimientoPosO') == '') { alert('FALTA ESCOJER PROCEDIMIENTO'); return false; }
            if (valorAtributo('lblTipoDocumento') != 'HQLA' &&
                valorAtributo('lblTipoDocumento') != 'HQUI' && valorAtributo('lblTipoDocumento') != 'HPRO'
            ) { alert('SOLO SE PUEDE AGREGAR A UNA DESCRIPCION QUIRURGICA'); return false; }

            var ids = jQuery("#listPosOperatorio").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listPosOperatorio").getRowData(c);
                if (datosRow.ID_PROCEDIMIENTO == valorAtributoIdAutoCompletar('txtIdProcedimientoPosO') && datosRow.ID_SITIO == valorAtributo('cmbIdSitioQuirurgicoPosO')) {
                    alert('YA EXISTE ESTE PROCEDIMIENTO A ESTE SITIO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'traerHcCirugiaProcedimientosPreAPos':
            if (valorAtributo('lblIdDocumento') == '') { alert('FALTA ESCOJA DOCUMENTO CLINICO'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != '0') { alert('ESTADO DEL DOCUMENTO DEBE SER ABIERTO'); return false; }
            if (valorAtributo('lblTipoDocumento') != 'HQLA' &&
                valorAtributo('lblTipoDocumento') != 'HQUI'
            ) { alert('SOLO SE PUEDE AGREGAR A UNA DESCRIPCION QUIRURGICA'); return false; }

            var ids = jQuery("#listPosOperatorio").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listPosOperatorio").getRowData(c);
                if (datosRow.ID_PROCEDIMIENTO == valorAtributo('lblIdProcedimiento') && datosRow.ID_SITIO == valorAtributo('lblIdSitioPre')) {
                    alert('YA EXISTE ESTE PROCEDIMIENTO A ESTE SITIO EN EL LISTADO');
                    return false;
                }
            }
            break;

        case 'procedimientosGestionEdit':
            if (valorAtributo('txt_observacionGestion') == '') { alert('FALTA OBSERVACION'); return false; }
            if (valorAtributo('cmbEstadoEditProc') == '') { alert('FALTA ESTADO'); return false; }
            break;
        case 'listAnestesia':
            if (valorAtributo('cmbAnestesiasGrupo') == '') { alert('FALTA GRUPO ANESTESIA'); return false; }
            if (valorAtributo('txtHoraAnestesia') == '' || valorAtributo('txtHoraAnestesia') > 12 || valorAtributo('txtHoraAnestesia') < 0) { alert('FALTA HORA EVENTO O ES MAYOR QUE 12'); return false; }
            if (valorAtributo('txtMinAnestesia') == '' || valorAtributo('txtMinAnestesia') > 59 || valorAtributo('txtMinAnestesia') < 0) { alert('FALTA MINUTOS'); return false; }
            break;
        case 'listMedicacionConNoPOS':
            verificarCamposGuardar('listMedicacion')
            if (valorAtributo('txt_ResumenHC') == '') { alert('FALTA RESUMEN ENFERMEDAD ACTUAL'); return false; }
            if (valorAtributo('txt_MedicamentoDelPOS1') == '') { alert('FALTA MEDICAMENTO DEL POST UTILIZADO'); return false; }
            if (valorAtributo('txt_PresentacionConcentracion1') == '') { alert('FALTA PRESENTACION DEL MEDICAMENTO 1 NO POS'); return false; }
            if (valorAtributo('txt_DosisFrecuencia1') == '') { alert('FALTA FRECUENCIA DEL MEDICAMENTO 1 NO POS'); return false; }
            if (valorAtributo('txt_IndicacionTerapeutica') == '') { alert('FALTA INDICACION TERAPEUTICA DEL MEDICAMENTO 1 NO POS'); return false; }
            //if (valorAtributo('txt_ExpliquePos') == '') { alert('FALTA EXPLICACION NO POS'); return false; }
            if (valorAtributo('cmbUnidadFarmaceutica') == '') { alert('FALTA UNIDAD FARMACEUTICA'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }
            break;
        case 'listMedicacion':
            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO CLINICO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticulo') == '') { alert('SELECCIONE MEDICAMENTO'); return false; }
            if (valorAtributo('cmbIdVia') == '') { alert('FALTA VIA'); return false; }
            if (valorAtributo('cmbUnidad') == '') { alert('FALTA FORMA FARMACEUTICA'); return false; }
            if (valorAtributo('txtCant') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('txtFrecuencia') == '') { alert('FALTA FRECUENCIA'); return false; }
            if (valorAtributo('txtDias') == '') { alert('FALTA DIAS'); return false; }
            if (valorAtributo('cmbUnidadFarmaceutica') == '') { alert('FALTA UNIDAD FARMACEUTICA'); return false; }
            if (valorAtributo('txtCantidad') == '') { alert('FALTA CANTIDAD'); return false; }

            var ids = jQuery("#listMedicacion").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listMedicacion").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticulo')) {
                    alert('YA EXISTE ESTE MEDICAMENTO EN EL LISTADO');
                    return false;
                }
            }
            break;
        case 'listInsumos':
            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO CLINICO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdArticuloInsumo') == '') { alert('SELECCIONE MEDICAMENTO'); return false; }
            if (valorAtributo('txtCantidadInsumos') == '') { alert('FALTA CANTIDAD'); return false; }
            var ids = jQuery("#listMedicacion").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listMedicacion").getRowData(c);
                if (datosRow.ID_ARTICULO == valorAtributoIdAutoCompletar('txtIdArticuloInsumo')) {
                    alert('YA EXISTE ESTE INSUMO EN EL LISTADO');
                    return false;
                }
            }
            break;


        case 'listSistemas':
            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtSistema') == '') { alert('SELECCIONE SISTEMA'); return false; }
            if (valorAtributo('txtHallazgo') == '') { alert('FALTA HALLAZGO'); return false; }

            var ids = jQuery("#listSistemas").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listSistemas").getRowData(c);
                if (datosRow.id == valorAtributoIdAutoCompletar('txtSistema')) {
                    alert('YA EXISTE ESTE SISTEMA EN EL LISTADO DE ESTE DOCUMENTO');
                    return false;
                }
            }
            break;

        case 'listExamenFisicoSistemas':

            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtExamenSistema') == '') { alert('SELECCIONE SISTEMA'); return false; }
            if (valorAtributo('txtExamenHallazgo') == '') { alert('FALTA HALLAZGO'); return false; }

            var ids = jQuery("#listExamenFisicoSistemas").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listExamenFisicoSistemas").getRowData(c);
                if (datosRow.id == valorAtributoIdAutoCompletar('txtExamenSistema')) {
                    alert('YA EXISTE ESTE SISTEMA EN EL LISTADO DE ESTE DOCUMENTO');
                    return false;
                }
            }
            break;

        case 'modificarDx':
            if (valorAtributoIdAutoCompletar('txtIdDxVentanita') == '') { alert('SELECCIONE DIAGNOSTICO'); return false; }
            if (valorAtributo('cmbDxTipoVentanita') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbDxClaseVentanita') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoDxVentanita') == '') { alert('FALTA SITIO'); return false; }

            var ids = jQuery("#listDiagnosticos").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                if (datosRow.id != valorAtributo('txtIdTrans')) {
                    if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDxVentanita') && datosRow.Sitio == valorAtributoCombo('cmbIdSitioQuirurgicoDxVentanita')) {
                        alert('YA EXISTE ESTE DIAGNOSTICO CON SITIO EN EL LISTADO DE ESTE DOCUMENTO');
                        return false;
                    }
                }
            }

            if (datosRow.id != valorAtributo('txtIdTrans')) {
                for (var i = 0; i < ids.length; i++) {
                    var c = ids[i];
                    var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                    if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDxVentanita')) {
                        alert('Atencion!!!! \nYA EXISTE ESTE DIAGNOSTICO EN EL LISTADO');
                        return true;
                    }
                }
            }
            break;

        case 'listDiagnosticos':
            if (valorAtributo('lblIdDocumento') == '') { alert('SELECCIONE DOCUMENTO'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdDx') == '') { alert('SELECCIONE DIAGNOSTICO'); return false; }
            if (valorAtributo('cmbDxTipo') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbDxClase') == '') { alert('FALTA CLASE'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoDx') == '') { alert('FALTA SITIO'); return false; }

            var ids = jQuery("#listDiagnosticos").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDx') && datosRow.Sitio == valorAtributoCombo('cmbIdSitioQuirurgicoDx')) {
                    alert('YA EXISTE ESTE DIAGNOSTICO CON SITIO EN EL LISTADO DE ESTE DOCUMENTO');
                    return false;
                }
            }
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                if (datosRow.id_cie == valorAtributoIdAutoCompletar('txtIdDx')) {
                    alert('Atencion!!!! \nYA EXISTE ESTE DIAGNOSTICO EN EL LISTADO');
                    return false;
                }
            }
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listDiagnosticos").getRowData(c);
                if (datosRow.id_clase == valorAtributo('cmbDxClase') && valorAtributo('cmbDxClase') == "1") {
                    alert('Atencion!!!! \nYA EXISTE DIAGNOSTICO PRINCIPAL');
                    return false;
                }
            }
            break;

        case 'crearAdmision':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }

            if (valorAtributo('cmbIdEtnia') == '') { alert('FALTA ETNIA'); return false; }
            if (valorAtributo('cmbIdNivelEscolaridad') == '') { alert('FALTA NIVEL ESCOLARIDAD'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO REGIMEN'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdOcupacion') == '') { alert('FALTA OCUPACION'); return false; }
            if (valorAtributo('cmbIdEstrato') == '') { alert('FALTA ESTRATO'); return false; }
            if (valorAtributoIdAutoCompletar('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('txtNoAutorizacion') == '') { alert('FALTA NUMERO DE AUTORIZACION'); return false; }
            if (valorAtributoIdAutoCompletar('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdDx') == '') { alert('FALTA DIAGNOSTICO'); return false; }
            if (valorAtributo('cmbTipoAdmision') == '') { alert('FALTA TIPO'); return false; }
            if (valorAtributo('cmbCausaExterna') == '') { alert('FALTA CAUSA EXTERNA'); return false; }
            if (valorAtributo('cmbIdEspecialidad') == '') { alert('FALTA ESPECIALIDAD'); return false; }
            //if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('FALTA SUB ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('FALTA MEDICO'); return false; }
            if (valorAtributo('txtObservacion') == '') { alert('OJO !!!  LE FALTA EL NUMERO DE FACTURA'); return false; }

            /* if(valorAtributo('lblIdCita')!=''){
                  var ids = jQuery("#listAdmision").getDataIDs();			  
                  for(var i=0;i<ids.length;i++){ 
                      var c = ids[i];
                      var datosRow = jQuery("#listAdmision").getRowData(c); 
                      if(datosRow.id_cita == valorAtributo('lblIdCita')){ 
                            alert('NO SE PUEDE CREAR ADMISION PORQUE YA EXISTE UNA CITA EN EL LISTADO DE OTRA ADMISION'); return false; 
                      }
                  }	
              }*/

            break;
        case 'listCitaCirugiaProcedimiento':
            if (valorAtributo('cmbIdSitioQuirurgico') == '') { alert('FALTA SITIO QUIRURGICO'); return false; }
            if (valorAtributo('txtIdProcedimiento') == '') { alert('FALTA PROCEDIMIENTO'); return false; }
            var ids = jQuery("#listCitaCirugiaProcedimiento").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listCitaCirugiaProcedimiento").getRowData(c);

                if (datosRow.idSitioQuirur == valorAtributo('cmbIdSitioQuirurgico')) {
                    alert('ATENCION !!!! YA EXISTE UN SITIO EN EL LISTADO, \n IGUAL SE ADICIONARA');
                    if (datosRow.idProcedimiento == valorAtributoIdAutoCompletar('txtIdProcedimiento')) {
                        alert('YA EXISTE UN PROCEDIMIENTO PARA ESTE SITIO EN EL LISTADO');
                        return false;

                    }
                    alert('Asignar un procedimiento a sitio diferente?')
                }
            }
            break;
        case 'listCitaCirugiaProcedimientoLE':
            if (valorAtributo('cmbIdSitioQuirurgicoLE') == '') { alert('FALTA SITIO QUIRURGICO'); return false; }
            if (valorAtributo('txtIdProcedimientoLE') == '') { alert('FALTA PROCEDIMIENTO'); return false; }
            var ids = jQuery("#listCitaCirugiaProcedimientoLE").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listCitaCirugiaProcedimientoLE").getRowData(c);

                if (datosRow.idSitioQuirur == valorAtributo('cmbIdSitioQuirurgicoLE')) {
                    alert('ATENCION !!!! YA EXISTE UN SITIO EN EL LISTADO, \n IGUAL SE ADICIONARA');
                    if (datosRow.idProcedimiento == valorAtributoIdAutoCompletar('txtIdProcedimientoLE')) {
                        alert('YA EXISTE UN PROCEDIMIENTO PARA ESTE SITIO EN EL LISTADO');
                        return false;

                    }

                }
            }
            break;
        case 'adicionarListaEsperaProcedimientos':
            if (valorAtributo('cmbIdSitioQuirurgico') == '') { alert('FALTA SITIO QUIRURGICO'); return false; }
            break;
        case 'eliminaListaEspera':

            var ids = jQuery("#listListaEsperaProcedimiento").getDataIDs();
            if (ids.length > 0) {
                alert('NO SE PUEDE ELIMINAR PORQUE TIENE PROCEDIMIENTOS RELACIONADOS')
                return false;
            }
            break;
        case 'crearListaEsperaCirugia':
            var ids = jQuery("#listCitaCirugiaProcedimientoLE").getDataIDs();
            if (ids.length < 1) { alert('AL MENOS DEBE HABER UN PROCEDIMIENTO ! ! !'); return false; }

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR1'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO REGIMEN - PLAN'); return false; }

            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE Especialidad'); return false; }
            //if (valorAtributo('cmbIdSubEspecialidad') == '') { alert('SELECCIONE SubEspecialidad'); return false; }
            if (valorAtributo('cmbIdProfesionales') == '') { alert('SELECCIONE PROFESIONAL'); return false; }
            if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE Discapacidad Fisica'); return false; }
            if (valorAtributo('cmbEmbarazo') == '') { alert('SELECCIONE Embarazo'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE Tipo Cita'); return false; }
            if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE ESPERAR'); return false; }
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE Necesidad'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('SELECCIONE Estado'); return false; }

            break;
        case 'crearListaEsperaCirugiaProgramacion':

            var ids = jQuery("#listCitaCirugiaProcedimientoLEGestion").getDataIDs();
            if (ids.length < 1) { alert('AL MENOS DEBE HABER UN PROCEDIMIENTO ! ! !'); return false; }

            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR1'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }

            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE Especialidad'); return false; }
            if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE Discapacidad Fisica'); return false; }
            if (valorAtributo('cmbEmbarazo') == '') { alert('SELECCIONE Embarazo'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE Tipo Cita'); return false; }
            if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE ESPERAR'); return false; }
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE Necesidad'); return false; }
            if (valorAtributo('cmbEstado') == '') { alert('SELECCIONE Estado'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('SELECCIONE TIPO REGIMEN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('SELECCIONE TIPO REGIMEN'); return false; }
            break;
        case 'modificarListaEspera':
            if (valorAtributo('txtAdministradora1LE') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            //  if(valorAtributo('cmbIdEspecialidadLE') ==''  ){ alert('SELECCIONE Especialidad'); return false; }
            //if (valorAtributo('cmbIdSubEspecialidadLE') == '') { alert('SELECCIONE SubEspecialidad'); return false; }
            break;

        case 'crearListaEspera':

            var messageAlert = []
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') {
                swAlert('info', '', 'SELECCIONE PACIENTE');
                return false;
            }

            if (valorAtributo('cmbSede') === '') {
                messageAlert.push('Sede')
            }
            if (valorAtributo('cmbIdEspecialidad') === '') {
                messageAlert.push('Especialidad')
            }
            /* if (valorAtributo('cmbIdSubEspecialidad') === '') {
                messageAlert.push('Sub Especialidad')
            } */
            if (valorAtributo('cmbTipoCita') === '') {
                messageAlert.push('Tipo Cita')
            }
            if (valorAtributo('cmbIdTipoRegimen') === '') {
                messageAlert.push('Tipo Regimen')
            }
            if (valorAtributo('cmbIdPlan') === '') {
                messageAlert.push('Plan de Contratacion')
            }
            if (valorAtributo('cmbEsperar') === '') {
                messageAlert.push('Esperar Dias')
            }
            if (valorAtributo('cmbNecesidad') === '') {
                messageAlert.push('Grado de Necesidad')
            }

            msjhtml = messageAlert.join(' <br> ')
            console.log({ msjhtml }, { messageAlert });
            if (messageAlert.length > 0) {
                swAlert('info', 'los siguientes campos son obligatorios', 'campos obligatorios', msjhtml)
                return false;
            } else {
                return true
            }
            /*
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            //if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            //if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            //if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            //if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR1'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA REGIMEN - PLAN'); return false; }
            //if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA REGIMEN - PLAN'); return false; }

            if (valorAtributo('cmbIdEspecialidad') == '') { alert('SELECCIONE Especialidad'); return false; }
            //if (valorAtributo('cmbDiscapaciFisica') == '') { alert('SELECCIONE Discapacidad Fisica'); return false; }
            //if (valorAtributo('cmbEmbarazo') == '') { alert('SELECCIONE Embarazo'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE Tipo Cita'); return false; }
            //if (valorAtributo('cmbEsperar') == '') { alert('SELECCIONE ESPERAR'); return false; }
            if (valorAtributo('cmbNecesidad') == '') { alert('SELECCIONE Necesidad'); return false; }
            //if (valorAtributo('cmbEstado') == '') { alert('SELECCIONE Estado'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('SELECCIONE SEDE'); return false; }
            */
            break;

        case 'crearNuevoPaciente':
            //if(validarFecha('txtFechaNac',110)==false){ return false;}
            if (valorAtributo('cmbTipoId') == '') { alert('FALTA TIPO ID'); return false; }
            if (valorAtributo('txtIdentificacion') == '') { alert('FALTA IDENTIFICACION'); return false; }
            if (valorAtributo('txtApellido1') == '') { alert('FALTA PRIMER APELLIDO'); return false; }
            if (valorAtributo('txtNombre1') == '') { alert('FALTA PRIMER NOMBRE'); return false; }
            if (valorAtributo('txtFechaNac') == '') { alert('FALTA FECHA NACIMIENTO'); return false; }
            if (validarFecha('txtFechaNac', 110) == false) { return false; }
            if (valorAtributo('cmbSexo') == '') { alert('FALTA SEXO'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONO 1'); return false; }
            if (valorAtributo('txtCelular1') == '') { alert('FALTA CELULAR 1'); return false; }
            if (valorAtributo('txtAdministradoraPaciente') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            break;

        case 'crearNuevoPacienteT':

            //if(validarFecha('txtFechaNac',110)==false){ return false;}
            if (valorAtributo('cmbTipoIdOrdenes') == '') { alert('FALTA TIPO ID'); return false; }
            if (valorAtributo('txtIdentificacionOrdenes') == '') { alert('FALTA IDENTIFICACION'); return false; }
            if (valorAtributo('txtApellido1Ordenes') == '') { alert('FALTA PRIMER APELLIDO'); return false; }
            if (valorAtributo('txtNombre1Ordenes') == '') { alert('FALTA PRIMER NOMBRE'); return false; }
            if (valorAtributo('txtFechaNacOrdenes') == '') { alert('FALTA FECHA NACIMIENTO'); return false; }
            if (validarFecha('txtFechaNacOrdenes', 110) == false) { return false; }
            if (valorAtributo('cmbSexoOrdenes') == '') { alert('FALTA SEXO'); return false; }
            if (valorAtributo('txtMunicipioOrdenes') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionResOrdenes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtTelefonosOrdenes') == '') { alert('FALTA TELEFONO 1'); return false; }
            if (valorAtributo('txtCelular1Ordenes') == '') { alert('FALTA CELULAR 1'); return false; }
            if (valorAtributo('txtAdministradoraPacienteOrdenes') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            break;

        case 'crearCita':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE TIPO CITA'); return false; }
            if (valorAtributo('cmbEstadoCita') == '') { alert('SELECCIONE ESTADO CITA'); return false; }
            if (valorAtributo('cmbMotivoConsulta') == '') { alert('SELECCIONE MEDIO DE AGENDAMIENTO'); return false; }
            if (valorAtributo('txtCelular1') == '' && valorAtributo('txtTelefonos') == '') { alert('FALTA ADICIONAR EL TELEFONO O CEULAR'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }

            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }

            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }

            if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA FECHA PACIENTE'); return false; }
            //if (Date.parse(valorAtributo('lblFechaCita')) < Date.parse(valorAtributo('txtFechaPacienteCita'))) { alert('FECHA DE PACIENTE NO ES VALIDA. DEBE SER MAYOR O IGUAL A LA FECHA DE LA CITA'); return false; }

            if ((valorAtributo('cmbEstadoCita') == 'L' || valorAtributo('cmbEstadoCita') == 'R') &&
                valorAtributo('cmbMotivoConsultaClase') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }

            var gr1 = jQuery("#listProcedimientosAgenda").getDataIDs();
            var gr2 = jQuery("#listCitaCexProcedimientoTraer").getDataIDs();

            if ((gr1.length + 0) > 0 && (gr2.length + 0) === 0 && valorAtributo('cmbIdTipoRegimen').split('-')[1] === '2') { alert('DEBE AGREGAR ALGUN PROCEDIMIENTO!'); return false; }
            //var gr1 = jQuery("#listProcedimientosAgenda").getDataIDs();
            if (valorAtributo('cmbProgramaCita') == '') { aler('FALTA SELECCIONAR PROGRAMA'); return false; }

            let chkModlElementos = document.querySelectorAll("input[name='checkModalidad']");
            let banChk = false;
            for (let i = 0; i < chkModlElementos.length; i++) {
                if (chkModlElementos[i]) {
                    banChk = true;
                    break;
                }
            }
            if (!banChk) { alert('FALTA SELECCIONAR LA MODALIDAD DE CONSULTA'); return false; }
            break;

        case 'crearCitaCirugia':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') == '') { alert('SELECCIONE PACIENTE'); return false; }
            if (valorAtributo('cmbTipoCita') == '') { alert('SELECCIONE TIPO CITA'); return false; }
            if (valorAtributo('cmbEstadoCita') == '') { alert('SELECCIONE ESTADO CITA'); return false; }
            if (valorAtributo('cmbMotivoConsulta') == '') { alert('SELECCIONE MOTIVO'); return false; }
            if (valorAtributo('txtTelefonos') == '') { alert('FALTA TELEFONOS'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('cmbIdTipoRegimen') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }
            if (valorAtributo('lblIdPlanContratacion') == '') { alert('FALTA TIPO DE REGIMEN - PLAN'); return false; }

            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtAdministradora1') == '') { alert('FALTA ADMINISTRADORA'); return false; }
            if (valorAtributo('txtFechaPacienteCita') == '') { alert('FALTA FECHA PACIENTE'); return false; }
            if (valorAtributo('txtIPSRemite') == '') { alert('FALTA INSTITUCION QUE REMITE'); return false; }

            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('cmbTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('cmbTipoAnestesia') == '') { alert('FALTA TIPO ANESTESIA'); return false; }

            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtAnestesiologo') == '') { alert('FALTA ANESTESIOLOGO'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtInstrumentador') == '') { alert('FALTA INSTRUMENTADOR'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtInstrumentador') == '') { alert('FALTA INSTRUMENTADOR'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtCirculante') == '') { alert('FALTA CIRCULANTE'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtCirculante') == '') { alert('FALTA CIRCULANTE'); return false; }
            // if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtNoLente') == '') { alert('FALTA NUMERO LENTE'); return false; }
            // if (valorAtributo('cmbEstadoCita') == 'A' && valorAtributo('txtNoLente') == '') { alert('FALTA NUMERO LENTE'); return false; }

            if ((valorAtributo('cmbEstadoCita') == 'L' || valorAtributo('cmbEstadoCita') == 'R') &&
                valorAtributo('cmbMotivoConsultaClase') == '') { alert('FALTA CLASIFICACION MOTIVO'); return false; }

            if (valorAtributo('cmbEstadoCita') == 'C' && valorAtributo('txtNoAutorizacion') == '') { alert('PARA CONFIRMAR DEBE DILIGENCIAR:   NUMERO DE AUTORIZACION'); return false; }
            if (valorAtributo('cmbEstadoCita') == 'F' && valorAtributo('txtNoAutorizacion') == '') { alert('PARA FACTURADO DEBE DILIGENCIAR:   NUMERO DE AUTORIZACION'); return false; }

            var ids = jQuery("#listCitaCexProcedimientoTraer").getDataIDs();
            if (ids.length < 1) { alert('AL MENOS DEBE HABER UN PROCEDIMIENTO ! ! !'); return false; }

            if (parseInt($('#txtFechaPacienteCita').val().split('-')[0]) > 9999 || $('#txtFechaPacienteCita').val().length > 10) { alert('FORMATO DE FECHA PACIENTE INCORRECTA'); return false; }
            if (parseInt($('#txtFechaVigencia').val().split('-')[0]) > 9999 || $('#txtFechaVigencia').val().length > 10) { alert('FORMATO DE FECHA VIGENCIA AUTORIZACION INCORRECTA'); return false; }

            break;
        /******* AGENDA */

        /******* AGENDA */
        case 'listAgenda':
            var dias = $("#listDiasC").jqGrid('getGridParam', 'selrow')

            if (dias == null) { alert('DEBE SELECCIONAR AL MENOS UN DIA DE LA AGENDA.'); return false; }
            if (valorAtributo('txtHoraInicio') == '') { alert('SELECCIONE HORA DE INICIO.'); return false; }
            if (valorAtributo('txtHoraFin') == '') { alert('SELECCIONE HORA DE FIN.'); return false; }
            if (valorAtributo('cmbDuracionMinutos') == '') { alert('SELECCIONE DURACION EN MINUTOS'); return false; }
            if (valorAtributo("cmbBloqueCita") == "S" && valorAtributo("txtCantidadCitasBloque") == "") { alert('FALTA DILIGENCIAR LA CANTIDAD DE CITAS POR BLOQUE'); return false; }
            if (valorAtributo('cmbConsultorio') == '') { alert('FALTA CONSULTORIO'); return false; }
            break;

        case 'listAgendaCopiarDia':
            if ($("#listAgenda").jqGrid('getGridParam', 'selrow') === null) { alert('DEBE SELECCIONAR AL MENOS UNA CITA PARA COPIAR.'); return false; }
            if (valorAtributo('txtFechaDestino') == '') { alert('SELECCIONE FECHA DESTINO'); return false; }
            if (valorAtributo('cmbIdEspecialidadMover') == '') { alert('SELECCIONE LA ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbIdProfesionalesMov') == '') { alert('SELECCIONE PROFESIONAL'); return false; }
            break;

        case 'MoverAgenda':
            if ($("#listAgenda").jqGrid('getGridParam', 'selrow') === null) { alert('DEBE SELECCIONAR AL MENOS UNA CITA PARA MOVER.'); return false; }
            if (valorAtributo('txtFechaDestino') == '') { alert('SELECCIONE FECHA DESTINO'); return false; }
            if (valorAtributo('cmbIdEspecialidadMover') == '') { alert('SELECCIONE LA ESPECIALIDAD'); return false; }
            if (valorAtributo('cmbIdProfesionalesMov') == '') { alert('SELECCIONE PROFESIONAL'); return false; }
            break;

        case 'MoverAgendaporProfesional':
            if ($("#listDiasC").jqGrid('getGridParam', 'selrow') === null) { alert('SELECCIONE UN DIA DE LA AGENDA'); return false; }
            if (valorAtributo('txtFechaDestino') == '') { alert('SELECCIONE FECHA DESTINO'); return false; }
            if (valorAtributo('cmbIdProfesionalesMov') == '') { alert('SELECCIONE PROFESIONAL PARA MODIFICAR LA AGENDA'); return false; }
            break;
        /*************FIN CLINICA*/
        case 'adicionarSolicitudADocumento':
            if (valorAtributo('lblIdEstadoDocumento') != 0) { alert(' DOCUMENTO DEBE ESTAR ABIERTO ! ! !'); return false; }
            if (valorAtributo('lblIdEstadoDocumento') == '') { alert(' FALTA SELECCIONAR DOCUMENTO ! ! !'); return false; }
            break;

        case 'pdfAnexo3':
            if (valorAtributo('lblMunicipio') == 'null') { alert(' FALTA DILIGENCIAR MUNICIPIO ! ! !'); return false; }
            if (valorAtributo('lblTipoUsuario') == 'null') { alert(' FALTA DILIGENCIAR TIPO PACIENTE ! ! !'); return false; }
            break;

        case 'pdfAnexo2':
            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblMunicipio') == 'null') { alert(' FALTA DILIGENCIAR MUNICIPIO ! ! !'); return false; }
            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblClasTriage') == 'null') { alert(' FALTA DILIGENCIAR CLASE TRIAGE ! ! !'); return false; }
            if (valorAtributo('lbl_DestinoPaciente') == 'null') { alert(' FALTA DILIGENCIAR DESTINO PACIENTE ! ! !'); return false; }
            if (valorAtributo('lblTipoUsuario') == 'null') { alert(' FALTA DILIGENCIAR TIPO PACIENTE ! ! !'); return false; }

            break;
        case 'pdfAnexo':


            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblMunicipio') == 'null') { alert(' FALTA DILIGENCIAR MUNICIPIO ! ! !'); return false; }
            if (valorAtributo('lblOrigenAtencion') == 'null') { alert(' FALTA DILIGENCIAR ORIGEN DE LA ANECION ! ! !'); return false; }
            if (valorAtributo('lblClasTriage') == 'null') { alert(' FALTA DILIGENCIAR CLASE TRIAGE ! ! !'); return false; }
            if (valorAtributo('lbl_DestinoPaciente') == 'null') { alert(' FALTA DILIGENCIAR DESTINO PACIENTE ! ! !'); return false; }
            if (valorAtributo('lblTipoUsuario') == 'null') { alert(' FALTA DILIGENCIAR TIPO PACIENTE ! ! !'); return false; }

            break;


        case 'proyectarDocumentoInventario':
            if (valorAtributo('cmbIdBodega') == '') { alert(' FALTA ESCOGER BODEGA ! ! !'); return false; }
            if (valorAtributo('cmbIdPresentacion') == '') { alert(' FALTA ESCOGER PRESENTACION ! ! !'); return false; }
            break;
        case 'cerrarTransaccionesDocumentoInvUD':

            var ids = jQuery("#listTransaccionesInventarioConsolidado").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listTransaccionesInventarioConsolidado").getRowData(c);
                if (datosRow.id_permitido == 'N') { alert('No se puede Cerrar porque existe una cantidad NO permitida'); return false; }
            }

            var ids = jQuery("#listTransaccionesInventario").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listTransaccionesInventario").getRowData(c);
                if (datosRow.Proyectable == 'S' && datosRow.Proyectable) {
                    if (datosRow.Proyectable != 'S') {
                        alert('No se puede Cerrar porque existen elementos proyectables');
                        return false;
                    }
                }
            }
            return true;
            break;
        case 'cerrarTransaccionesDocumentoInvUDReportesExcel':

            if (valorAtributo('lblIdEstadoDocumento') == 0) {
                alert('NO SE PERMITE IMPRIMIR PORQUE EL DOCUMENTO ESTA ABIERTO');
                return false;
            } else return true;
            break;

        case 'MoverCitaPaciente':
            if (valorAtributo('txtFechaDestinoCita') == '') { alert(' DEBE SELECCIONAR FECHA DESTINO! ! !'); return false; }
            if (valorAtributo('cmbIdProfesionalesAg') == '') { alert(' DEBE SELECCIONAR PROFESIONAL! ! !'); return false; }
            if (valorAtributo('cmbConsultorioAg') == '') { alert('DEBE SELECCIONAR UN CONSULTORIO'); return false; }
            break;

        case 'listProcedimientos':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitado') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridad') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuia') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacion') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;
        case 'listProcedimientosDetalle':
            if (valorAtributo("cmbSitio") === '') { swAlert('warning', 'falta seleccionar Sitio', 'campo vacio'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO !!!'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('cmbIdDxRelacionadoP') == '') {

                idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO");

                if (idFolioSel != 'NTSE') {

                    alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO !!!'); return false;
                }



            }
            if (valorAtributoIdAutoCompletar('txtIdProcedimiento') == '') { alert(' FALTA ESCOGER PROCEDIMIENTO !!!'); return false; }
            if (valorAtributo('txtPrincipalHC') == 'OK') {
                if (valorAtributo('cmbCantidad') == '') { alert(' FALTA ESCOGER CANTIDAD !!!'); return false; }
            } else {
                if (valorAtributo('cmbCantidadFactur') == '') { alert(' FALTA ESCOGER CANTIDAD FACTURADOR !!!'); return false; }
            }
            if (valorAtributo('cmbNecesidad') == '') { alert(' FALTA GRADO NECESIDAD !!!'); return false; }

            break;
        case 'listAyudasDiagnosticas':

            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitadoAD') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridadAD') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuiaAD') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacionAD') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;
        case 'listAyudasDiagnosticasDetalle':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdDxRelacionadoA') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO ! ! !'); return false; }
            if (valorAtributo('txtIdAyudaDiagnostica') == '') { alert('FALTA AYUDA DIAGNOSTICA'); return false; }
            if (valorAtributo('cmbCantidadAD') == '') { alert('FALTA CANTIDAD'); return false; }
            if (valorAtributo('cmbIdSitioQuirurgicoAD') == '') { alert('FALTA SITIO'); return false; }
            break;
        case 'listLaboratorios':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitadoLaboratorio') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridadLaboratorio') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuiaLaboratorio') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacionLaboratorio') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;
        case 'listLaboratoriosDetalle':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdDxRelacionadoL') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO ! ! !'); return false; }
            if (valorAtributo('txtIdLaboratorio') == '') { alert('FALTA LABORATORIO'); return false; }
            if (valorAtributo('cmbCantidadLaboratorio') == '') { alert('FALTA CANTIDAD'); return false; }
            break;

        case 'listTerapia':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('cmbIdTipoServicioSolicitadoTerapia') == '') { alert(' FALTA ESCOGER SERVICIO SOLICITADO ! ! !'); return false; }
            if (valorAtributo('cmbIdPrioridadTerapia') == '') { alert(' FALTA ESCOGER PRIORIDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdGuiaTerapia') == '') { alert(' FALTA ESCOGER GUIA DE MANEJO INTEGRAL ! ! !'); return false; }
            if (valorAtributo('txtJustificacionTerapia') == '') { alert(' FALTA ESCOGER JUSTIFICACION ! ! !'); return false; }
            break;

        case 'listTerapiaDetalle':
            if (valorAtributo('cmbIdDxRelacionadoT') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ASOCIADO ! ! !'); return false; }
            if (valorAtributo('txtIdTerapia') == '') { alert('FALTA TERAPIA'); return false; }
            if (valorAtributo('cmbCantidadTerapia') == '') { alert('FALTA CANTIDAD'); return false; }
            break;

        case 'listDx':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributoIdAutoCompletar('txtIdDx') == '') { alert(' FALTA ESCOGER DIAGNOSTICO ! ! !'); return false; }
            if (valorAtributo('cmbDxTipo') == '') { alert(' FALTA ESCOGER TIPO ! ! !'); return false; }
            if (valorAtributo('cmbDxClase') == '') { alert(' FALTA ESCOGER CLASE ! ! !'); return false; }
            break;

        case 'listAntecedentes':
            if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_ESTADO") == '1') { alert('EL DOCUMENTO YA SE ENCUENTRA FINALIZADO'); return false; }
            if (valorAtributo('cmbAntecedenteGrupo') == '') { alert(' FALTA ESCOGER GRUPO DE ANTECEDENTES ! ! !'); return false; }
            if (valorAtributo('cmbTiene') == '') { alert(' FALTA ESCOGER TIENE'); return false; }
            if (valorAtributo('cmbControlada') == '') { alert(' FALTA ESCOGER CONTROLADA ! ! !'); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO CLINICO ! ! !'); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert(' FALTA ESCOGER PACIENTE ! ! !'); return false; }
            /*var ids = jQuery("#listAntecedentes").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listAntecedentes").getRowData(c);
                if (datosRow.id_grupo == valorAtributo('cmbAntecedenteGrupo')) {
                    alert('YA EXISTE UN ANTECEDENTE EN EL LISTADO');
                    return false;
                }
            }*/
            break;

        case 'modificarAntecedente':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO CLINICO ! ! !'); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert(' FALTA ESCOGER PACIENTE ! ! !'); return false; }
            /*if (valorAtributo('lblIdDocumento') != valorAtributo('lblIdEvolucionAntecedente')) { alert('NO PUEDES MODIFICAR SOBRE EL FOLIO ACTUAL'); return false; }*/
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('ESTADO DE FOLIO NO PERMITE MODIFICAR'); return false; }
            /*var ids = jQuery("#listAntecedentes").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listAntecedentes").getRowData(c);
                if (datosRow.id_grupo == valorAtributo('cmbAntecedenteGrupo')) {
                    alert('YA EXISTE UN ANTECEDENTE EN EL LISTADO');
                    return false;
                }
            }*/
            break;

        case 'eliminarAntecedentes':
            if (valorAtributo('lblIdDocumento') != valorAtributo('lblIdEvolucionAntecedente')) { alert('NO PUEDES ELIMINAR SOBRE EL FOLIO ACTUAL'); return false; }
            if (valorAtributo('txtIdEsdadoDocumento') != 0) { alert('ESTADO DE FOLIO NO PERMITE ELIMINAR'); return false; }
            /*var ids = jQuery("#listAntecedentes").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var c = ids[i];
                var datosRow = jQuery("#listAntecedentes").getRowData(c);
                if (datosRow.id_grupo == valorAtributo('cmbAntecedenteGrupo')) {
                    alert('YA EXISTE UN ANTECEDENTE EN EL LISTADO');
                    return false;
                }
            }*/
            break;

        case 'listSignosVitales':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            if (valorAtributo('txtTemperatura') == '') { alert(' FALTA ESCOGER TEMPERATURA ! ! !'); return false; }
            if (valorAtributo('txtSistolica') == '') { alert(' FALTA ESCOGER SISTOLICA ! ! !'); return false; }
            if (valorAtributo('txtDiastolica') == '') { alert(' FALTA ESCOGER DIASTOLICA ! ! !'); return false; }
            if (valorAtributo('txtPulso') == '') { alert(' FALTA ESCOGER PULSO ! ! !'); return false; }
            if (valorAtributo('txtRespiracion') == '') { alert(' FALTA ESCOGER RESPIRACION ! ! !'); return false; }
            if (valorAtributo('txtPeso') == '') { alert(' FALTA ESCOGER PESO ! ! !'); return false; }
            if (valorAtributo('txtTalla') == '') { alert(' FALTA ESCOGER TALLA ! ! !'); return false; }

            var ids = jQuery("#listSignosVitales").getDataIDs(); /*PARA CONSULTA EXTERNA SOLO UNO REGISTRO*/
            if (ids.length > 0) { alert('YA EXISTEN REGISTRO DE UN EXAMEN FISICO ! ! !'); return false; }


            break;

        case 'cerrarTransaccionesDocumentoInv':
            if (valorAtributo('lblIdDocumento') == '') { alert(' FALTA ESCOGER DOCUMENTO ! ! !'); return false; }
            break;

        case 'deDevolucion':
            if (valorAtributo('cmbIdCantidadNovedad') == '') { alert(' FALTA ESCOGER LA CANTIDAD CON NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdConcepto') == '') { alert(' FALTA ESCOGER EL TIPO DE NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdCantidadNovedad') > valorAtributo('lblCantidad')) { alert(' LA CANTIDAD CON NOVEDAD NO PUEDE SER MAYOR QUE LA ORDENADA ! ! !'); return false; }

            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        case 'dePendienteAAdministradoNovedad':
            if (valorAtributo('cmbIdCantidadNovedad') == '') { alert(' FALTA ESCOGER LA CANTIDAD CON NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdConcepto') == '') { alert(' FALTA ESCOGER EL TIPO DE NOVEDAD ! ! !'); return false; }
            if (valorAtributo('cmbIdCantidadNovedad') > valorAtributo('lblCantidad')) { alert(' LA CANTIDAD CON NOVEDAD NO PUEDE SER MAYOR QUE LA ORDENADA ! ! !'); return false; }

            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        /*        case 'adicionarTrabajoControl':
                    if (valorAtributo('txtNumeroFactura') == '') { alert(' FALTA EL NUMERO DE FACTURA! ! !'); return false; }
                    if (valorAtributo('txtIdPacientee') == '') { alert(' FALTA LA IDENTIFICACION DEL PACIENTE ! ! !'); return false; }
                    if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN ! ! !'); return false; }
                    if (valorAtributo('txtIdMedico') == '') { alert(' FALTA EL MEDICO ! ! !'); return false; }
    
                    break;*/
        case 'tramitar':
            if (valorAtributo('txtPersonaRecibe') == '') { alert(' FALTA CLIENTE ! ! !'); return false; }
            if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN! ! !'); return false; }
            if (valorAtributo('txtIdEvento') == '') { alert(' FALTA SELECCIONAR EL EVENTO ! ! !'); return false; }

            break;
        case 'recibirPedido':
            if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN! ! !'); return false; }

            break;
        case 'entregarAcliente':
            if (valorAtributo('txtClienteRecibe') == '') { alert(' FALTA EL CLIENTE QUE RECIBE! ! !'); return false; }
            if (valorAtributo('txtIdEvento') == '') { alert(' FALTA EL EVENTO! ! !'); return false; }
            if (valorAtributo('txtNumeroOrden') == '') { alert(' FALTA EL NUMERO DE ORDEN! ! !'); return false; }

            break;
        case 'auditarDocumentoClinico':
            if (valorAtributo('lblIdDocumento') == "") { alert('Falta Escojer Documento'); return false; }
            if (valorAtributo('txtAuditor') != '') { alert('Documento ya se encuentra Auditado '); return false; }
            if (valorAtributo('cmbIdEstadoAuditado') == '2') {
                if (document.getElementById('txtObservacionAuditoria').value == '') {
                    alert('DEBE ESCRIBIR UNA OBSERVACION CUANDO SE AUDITA CON ERROR ');
                    return false;
                }
            }
            break;
        case 'cerrarDocumentoClinico':
            if (valorAtributo('lblIdDocumento') == "") { alert('Falta Escojer Documento'); return false; }
            //  if(jQuery("#listOrdenesMedicamentos").getDataIDs().length==0){ alert('Al menos debe haber un Insumo o Medicamento, en listado'); return false;}	 			  


            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        case 'listOrdenesMedicamentos':

            if (valorAtributo('lblIdDocumento') == '') { alert('Le falta seleccionar DOCUMENTO CL�NICO '); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('Le falta seleccionar MEDICAMENTO '); return false; }
            if (valorAtributo('cmbIdVia') == '') { alert('Le falta seleccionar  VIA  '); return false; }

            //  if( valorAtributo('lblDosis') == '' ){ alert('Le falta seleccionar cantidad en DOSIS Y ESPECIFICAR HORAS  ');	return false; }	 			  			  			  			  
            if (valorAtributo('cmbIdTipoDosis') == '') { alert('Le falta seleccionar tipo DOSIS  '); return false; }
            if (valorAtributo('cmbIdRepeticionProgramada') == '') { alert('Le falta seleccionar DURACION  '); return false; }
            if (valorAtributo('cmbIdIntervalo') == '') { alert('Le falta seleccionar Intervalo  '); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert('Le falta seleccionar Paciente '); return false; }
            if (valorAtributo('txtFechaOrdenMed') == '') { alert('Le falta seleccionar FECHA INICIO '); return false; }
            if (valorAtributo('cmbHoraOrdenMed') == '') { alert('Le falta seleccionar HORA DE INICIO  '); return false; }
            //  
            //  if(  valorAtributo('cmbIdIntervalo') > valorAtributo('cmbIdRepeticionProgramada') ){ alert('El Intervalo NO puede ser mayor que la Duracion ::   ');	return false; }					  
            //  		   
            if (valorAtributo('chkInmediato') == 'true') {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    if (valorAtributo('cmbIdRepeticionProgramada') == '1') {
                        if (confirm('Ha seleccionado Inmediato, no se registrar� horas y repetici�n programada ser� un dia, ESTA SEGURO ?')) {
                            $("#cmbIdRepeticionProgramada option[value='1']").attr('selected', 'selected');
                            return true;
                        } else return false;
                    } else {
                        alert('La Duraci�n para Inmediato debe ser igual a UN DIA');
                        return false;
                    }

                } else { alert('Para Inmediato no se debe escoger horas'); return false; }

            } else {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    alert('Debe seleccionar al menos una Hora con Cantidad');
                    return false;

                }

            }
            break;
        case 'listOrdenesMedicamentosModificar':

            if (valorAtributo('lblIdOrden') == '') { alert('Le falta seleccionar La Solicitud a Modificar '); return false; }
            if (valorAtributo('lblIdDocumento') == '') { alert('Le falta seleccionar DOCUMENTO CL�NICO '); return false; }
            if (valorAtributo('txtIdArticulo') == '') { alert('Le falta seleccionar MEDICAMENTO '); return false; }
            if (valorAtributo('cmbIdVia') == '') { alert('Le falta seleccionar  VIA  '); return false; }

            if (valorAtributo('lblDosis') == '') { alert('Le falta seleccionar cantidad en DOSIS Y ESPECIFICAR HORAS  '); return false; }
            if (valorAtributo('cmbIdTipoDosis') == '') { alert('Le falta seleccionar tipo DOSIS  '); return false; }
            if (valorAtributo('cmbIdRepeticionProgramada') == '') { alert('Le falta seleccionar DURACION  '); return false; }
            if (valorAtributo('cmbIdIntervalo') == '') { alert('Le falta seleccionar Intervalo  '); return false; }
            if (valorAtributo('lblIdPaciente') == '') { alert('Le falta seleccionar Paciente '); return false; }
            if (valorAtributo('txtFechaOrdenMed') == '') { alert('Le falta seleccionar FECHA INICIO '); return false; }
            if (valorAtributo('cmbHoraOrdenMed') == '') { alert('Le falta seleccionar HORA DE INICIO  '); return false; }
            // REGLA PARA VALIDAR EL INTERVALO POR:JUAN if( valorAtributo('cmbIdIntervalo') > valorAtributo('cmbIdRepeticionProgramada') ){ alert('El Intervalo NO puede ser mayor que la Duraci�n  ');	return false; }					  			  			   			  			   
            if (valorAtributo('chkInmediato') == 'true') {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    if (valorAtributo('cmbIdRepeticionProgramada') == '1') {
                        if (confirm('Ha seleccionado Inmediato, no se registrar� horas y repetici�n programada ser� un dia, ESTA SEGURO ?')) {
                            $("#cmbIdRepeticionProgramada option[value='1']").attr('selected', 'selected');
                            return true;
                        } else return false;
                    } else {
                        alert('La repetici�n programada para Inmediato debe ser igual a UN DIA');
                        return false;
                    }

                } else { alert('Para Inmediato no se debe escoger horas'); return false; }

            } else {
                if (!seleccionadoAlgunoTodosLosCombosDeLaDosisOrdenMedica()) {
                    alert('Debe seleccionar al menos una Hora con Cantidad');
                    return false;

                }

            }
            break;

        case 'reportarNoConformidad':
            if ($('#drag' + ventanaActual.num).find('#txtDescripEvento').val() == "") { alert('Falta ingresar Descripcion evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbNoConformidades').val() == "00") { alert('Falta escoger la no conformidad del proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUbicacionArea').val() == "00") { alert('Falta escoger el Area donde se presenta el evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaEvento').val() == "") { alert('Falta ingresar fecha del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbHoraEvent').val() == "--") { alert('Falta ingresar hora del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMinutEvent').val() == "") { alert('Falta ingresar minuto del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMinutEvent').val() > 59 || $('#drag' + ventanaActual.num).find('#txtMinutEvent').val() < 1) { alert('El valor del minuto debe estar entre 1 y 59 '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtEstadoEvent').val() != "") { alert('Evento ya existe, pruebe con Modificar '); return false; }
            //if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="" ){  alert('El estado del Evento no permite Adicionar, pruebe con NUEVO');  return false; }		
            break;
        case 'administrarNoConformidad':
            if ($('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val() == "") { alert('Falta ingresar concepto'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsaDestinoEvento').val() == "00") { alert('Falta escoger el persona destinatario'); return false; }
            if ($('#drag' + ventanaActual.num).find('#lblEstadoEvent').html() != "Reportada" && $('#drag' + ventanaActual.num).find('#lblEstadoEvent').html() != "Asignado") { alert('1e. El estado del Evento no permite Modificarlo'); return false; }
            // if( $('#drag'+ventanaActual.num).find('#cmbTipoPlanAccion').val()=="00" ){  alert('Falta escoger plan de acci�n');  return false; }
            //if( $('#drag'+ventanaActual.num).find('#txtFechaIniPlanAccion').val()==""){  alert('Falta ingresar fecha inicio plan de acci�n');  return false; }
            break;
        case 'listPlanAccion':
            // if( $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()!=""){ alert('Para adicionar informaci�n pruebe con modificar'); return false; }									 
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlanAccion').val() == "00") { alert('Falta escoger el Tipo plan acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAnalisisCausas').val() == "") { alert('Falta ingresar An�lisis de causas'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val() == "") { alert('Falta ingresar fecha inicio plan acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMetaPlanAccion').val() == "") { alert('Falta ingresar Meta del plan acci�n'); return false; }
            if (jQuery("#" + arg).getDataIDs().length == 0) { alert('Al menos debe haber una tarea en el plan de acci�n'); return false; }
            if (jQuery("#listPersonasPlanAccion").getDataIDs().length == 0) { alert('Al menos debe haber un individuo,  en Personas Tareas del plan de acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val() == "") { alert('Falta escribir concepto a la no conformidad '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsaDestinoEvento').val() == "00") { alert('Falta escoger el Responsable'); return false; }


            break;
        case 'listTareasPlanAccion':
            // if( $('#drag'+ventanaActual.num).find('#lblIdEventoDoc').html()!=""){ alert('Para adicionar informaci�n pruebe con modificar'); return false; }									 
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlanAccion').val() == "00") { alert('Falta escoger el Tipo plan acci�n'); return false; }
            //if( $('#drag'+ventanaActual.num).find('#txtAnalisisCausas').val()=="" ){  alert('Falta ingresar An�lisis de causas');  return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaIniPlanAccion').val() == "") { alert('Falta ingresar fecha inicio plan acci�n'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtMetaPlanAccion').val() == "") { alert('Falta ingresar Meta del plan acci�n'); return false; }
            if (jQuery("#" + arg).getDataIDs().length == 0) { alert('Al menos debe haber una tarea en el plan de acci�n'); return false; }
            if (jQuery("#listPersonasPlanAccionGral").getDataIDs().length == 0) { alert('Al menos debe haber un individuo,  en Personas Tareas del plan de acci�n'); return false; }
            if (jQuery("#listEspinaPescado").getDataIDs().length == 0) { alert('Al menos debe haber un An�lisis,  en espina de pescado'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtConcepNoConforCalidad').val() == "") { alert('Falta escribir concepto a la no conformidad '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsaDestinoEvento').val() == "00") { alert('Falta escoger el Responsable'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbResponsable').val() == "00") { alert('Escoja responsable del plan acci�n'); return false; }

            break;

        case 'reportarEventoAdverso':


            var datoscadena = $('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val().split('-');
            if (!(datoscadena.length == 2 && datoscadena[0] != '' && datoscadena[1] != '')) { alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Alfredo '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUnidadPertenece').val() == "00") { alert('Falta escoger Unidad a la que Pertenece el paciente'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtDescripEvento').val() == "") { alert('Falta ingresar Descripcion evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtTratamiento').val() == "") { alert('Falta ingresar tratamiento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbClaseEA').val() == "00") { alert('Falta escoger clasificaci�n del evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEAProceso').val() == "00") { alert('Falta escoger Listado Evento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUbicacionArea').val() == "00") { alert('Falta escoger el Area donde se presenta el evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaEvento').val() == "") { alert('Falta ingresar fecha del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbHoraEvent').val() == "--") { alert('Falta ingresar Turno del evento '); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtEstadoEvent').val() != "") { alert('Evento ya existe, pruebe con Modificar '); return false; }

            var mes = $('#drag' + ventanaActual.num).find('#txtFechaEvento').val().split('/');
            if (mes[1] != $('#drag' + ventanaActual.num).find('#txt_mes_reporte').val()) { alert('Solo puede reportar eventos del mes en curso '); return false; }



            break;
        case 'administrarEventoAdverso':
            //			  if( $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="Reportada" && $('#drag'+ventanaActual.num).find('#lblEstadoEvent').html()!="Asignado"  ){  alert('1e El estado del Evento no permite Modificarlo porque se encuentra en estado diferente a Reportado o Asignado ');  return false; }		
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbNoConformidades').val() == "00") { alert('Falta escoger el evento del proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEventoProceso').val() == "00") { alert('Falta escoger el evento del proceso'); return false; }

            break;
        case 'administrarPlanMejora':
            if ($('#drag' + ventanaActual.num).find('#cmbproceso').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlan').val() == "00") { alert('Falta escoger elTipo de plan'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAspectoMejorar').val() == "") { alert('Falta ingresar aspecto a mejorar'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbRiesgo').val() == "00") { alert('Falta escoger Riesgo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbCosto').val() == "00") { alert('Falta escoger Costo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbVolumen').val() == "00") { alert('Falta escoger Volumen'); return false; }
            break;
        case 'usuario':
            if ($('#drag' + ventanaActual.num).find('#cmbTipoid').val() == "" || $('#drag' + ventanaActual.num).find('#txtId').val() == "" || $('#drag' + ventanaActual.num).find('#txtLogin').val() == "" || $('#drag' + ventanaActual.num).find('#txtContrasena').val() == "" || $('#drag' + ventanaActual.num).find('#txtContrasena2').val() == "" || $('#drag' + ventanaActual.num).find('#cmbRol').val() == "") {
                alert("Debe de ingresar datos Completos");
                return false;
            } else if ($('#drag' + ventanaActual.num).find('#txtContrasena').val() != $('#drag' + ventanaActual.num).find('#txtContrasena2').val()) {
                alert("La confirmacion de la contraseña no es igual a la contraseña");
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').focus();
                return false;
            } else if ($('#drag' + ventanaActual.num).find('#lblNomPersona').html() == '') {
                alert("Antes de editar informacion, debe de buscar un usuario");
                $('#drag' + ventanaActual.num).find('#txtId').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena').val('');
                $('#drag' + ventanaActual.num).find('#txtContrasena2').val('');
                $('#drag' + ventanaActual.num).find('#txtLogin').val('');
                $("#cmbRol option[value='']").attr('selected', 'selected');
                $('#drag' + ventanaActual.num).find('#txtId').focus();
                return false;
            } else if ($('#drag' + ventanaActual.num).find('#hddId').val() != $('#drag' + ventanaActual.num).find('#cmbTipoIdUsua').val() || $('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val() != $('#drag' + ventanaActual.num).find('#txtId').val()) {
                alert("El Tipo o la Identificacion, no pertenecen a " + $('#drag' + ventanaActual.num).find('#lblNomPersona').html());

                $('#drag' + ventanaActual.num).find('#cmbTipoIdUsua').val($('#drag' + ventanaActual.num).find('#hddId').val());
                $('#drag' + ventanaActual.num).find('#txtId').val($('#drag' + ventanaActual.num).find('#hddTipoIdUsua').val())

                return false;
            }
            break;
        case 'listFactContribu':
            if ($('#drag' + ventanaActual.num).find('#cmbBarreraYDefensa').val() == "00") { alert('Falta escoger barrera y defensa'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbAccionInsegura').val() == "00") { alert('Falta escoger accion insegura'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbAmbito').val() == "00") { alert('Falta escoger el ambito '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbPrevenible').val() == "00") { alert('Falta ingresar prevenible '); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbOrganizCultura').val() == "00") { alert('Falta ingresar organizacion y cultura '); return false; }



            if (jQuery("#listFactContribu").getDataIDs().length == 0) { alert('Falta ingresar al listado de factores contributivos '); return false; }
            if (jQuery("#listPersonasOportMejora").getDataIDs().length == 0) { alert('Falta ingresar ACCIONES CORRECTIVAS '); return false; }


            break;
        case 'administrarPlanMejoraEA':
            if ($('#drag' + ventanaActual.num).find('#cmbprocesoEA').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoPlan').val() == "00") { alert('Falta escoger elTipo de plan'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAspectoMejorar').val() == "") { alert('Falta ingresar aspecto a mejorar'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbRiesgo').val() == "00") { alert('Falta escoger Riesgo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbCosto').val() == "00") { alert('Falta escoger Costo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbVolumen').val() == "00") { alert('Falta escoger Volumen'); return false; }
            break;
        case 'relacionEventoOM':
            if ($('#drag' + ventanaActual.num).find('#cmbprocesoEA').val() == "00") { alert('Falta escoger el proceso'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtAspectoMejorar').val() == "") { alert('Falta ingresar aspecto a mejorar'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbRiesgo').val() == "00") { alert('Falta escoger Riesgo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbCosto').val() == "00") { alert('Falta escoger Costo'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbVolumen').val() == "00") { alert('Falta escoger Volumen'); return false; }

            verificaAtributo('cmbProcesoOM');
            verificaAtributo('cmbTipoPlanOM');
            verificaAtributo('txtAspectoMejorarOM');
            verificaAtributo('cmbRiesgoOM');
            verificaAtributo('cmbCostoOM');
            verificaAtributo('cmbVolumenOM');


            break;

        case 'adminItem':

            // if( $('#drag'+ventanaActual.num).find('#lblCodItem').html()!='' ){alert('Item ya creado e identificado, pruebe con modificar'); 	 return false; }	
            if ($('#drag' + ventanaActual.num).find('#txtNomItem').val() == "") { alert('Falta ingresar nombre del Item'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbUnidMed').val() == "00") { alert('Falta escoger unidad de medida'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbServicio').val() == "00") { alert('Falta escoger servicio'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbGrupoItem').val() == "00") { alert('Falta escoger Grupo item'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbClase').val() == "00") { alert('Falta escoger Clase'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEstado').val() == "00") { alert('Falta escoger Estado'); return false; }
            break;
        case 'guardarAsignacionCitaNoPlaneada':

            $('#drag' + ventanaActual.num).find('#hidd_IdPaciente').val('');
            if ($('#drag' + ventanaActual.num).find('#txtNomItem').val() == "") { alert('Falta ingresar nombre del Item'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoUsuario').val() == "00") { alert('Falta el Tipo Usuario'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoCita').val() == "00") { alert('Falta el Tipo Cita'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEstadoCita').val() == "00") { alert('Falta el Estado Cita'); return false; }

            if (verificarCamposAutocompletar('txtMunicipio') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora1') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora2') == false) { return false; }


            if ($('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val() != '') {
                var datoscadena = $('#drag' + ventanaActual.num).find('#txtBusIdPaciente').val().split('-');
                if (!(datoscadena.length == 2 && datoscadena[0] != '' && datoscadena[1] != '')) {
                    alert('Falta ingresar correctamente: Identificaci�n, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Alfredo ');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPaciente').val(datoscadena[0]);

            } else {
                if ($('#drag' + ventanaActual.num).find('#txtNombre1').val() == "") { alert('Falta ingresar Primer Nombre'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtApellido1').val() == "") { alert('Falta ingresar Primer Apellido'); return false; }
                if ($('#drag' + ventanaActual.num).find('#cmbTipoId').val() == "00") { alert('Falta el Tipo Documento'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtIdPaciente').val() == "") {
                    alert('Falta ingresar Identificaci�n');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPaciente').val($('#drag' + ventanaActual.num).find('#cmbTipoId').val() + $('#drag' + ventanaActual.num).find('#txtIdPaciente').val());
            }
            break;
        case 'guardarAsignacionCitaNoPlaneadaListaEspera':

            $('#drag' + ventanaActual.num).find('#hidd_IdPacienteListaEspera').val('');
            if ($('#drag' + ventanaActual.num).find('#cmbTipoUsuarioListaEspera').val() == "00") { alert('Falta el Tipo Usuario'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbTipoCitaListaEspera').val() == "00") { alert('Falta el Tipo Cita'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbEstadoCitaListaEspera').val() == "00") { alert('Falta el Estado Cita'); return false; }

            if ($('#drag' + ventanaActual.num).find('#cmbProfesionalesListaEspera').val() == "00") { alert('Falta el profesional'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbZonaResidenciaListaEspera').val() == "00") { alert('Falta el Zona Residencia'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbNecesidadListaEspera').val() == "00") { alert('Falta el Grado Necesidad'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbDisponibleListaEspera').val() == "00") { alert('Falta el Disponible'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaNacimientoListaEspera').val() == 'null/null/null') { alert('Falta Fecha nacimiento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtFechaNacimientoListaEspera').val() == '') { alert('Falta Fecha nacimiento'); return false; }
            if ($('#drag' + ventanaActual.num).find('#cmbSexoListaEspera').val() == '00') { alert('Falta Sexo'); return false; }


            if (verificarCamposAutocompletar('txtMunicipioListaEspera') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora1ListaEspera') == false) { return false; }
            if (verificarCamposAutocompletar('txtAdministradora2ListaEspera') == false) { return false; }


            if ($('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').val() != '') {
                var datoscadena = $('#drag' + ventanaActual.num).find('#txtBusIdPacienteListaEspera').val().split('-');
                if (!(datoscadena.length == 2 && datoscadena[0] != '' && datoscadena[1] != '')) {
                    alert('Falta ingresar correctamente: Identificacion, Apellidos con nombres separados por el signo - \n \nEjemplo:  CC98400895-Andrade Jesus Alfredo ');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPacienteListaEspera').val(datoscadena[0]);
            } else {
                if ($('#drag' + ventanaActual.num).find('#txtNombre1ListaEspera').val() == "") { alert('Falta ingresar Primer Nombre'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtApellido1ListaEspera').val() == "") { alert('Falta ingresar Primer Apellido'); return false; }
                if ($('#drag' + ventanaActual.num).find('#cmbTipoIdListaEspera').val() == "00") { alert('Falta el Tipo Documento'); return false; }
                if ($('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').val() == "") {
                    alert('Falta ingresar Identificaci�n');
                    return false;
                } else $('#drag' + ventanaActual.num).find('#hidd_IdPacienteListaEspera').val($('#drag' + ventanaActual.num).find('#cmbTipoIdListaEspera').val() + $('#drag' + ventanaActual.num).find('#txtIdPacienteListaEspera').val());
            }
            break;
        case 'listSituaActual':
            if ($('#drag' + ventanaActual.num).find('#txtSituacionActual_' + tabActivo).val() == "") { alert('Campo vacio'); return false; }
            break;
        case 'fichaTecnica':
            if (verificaAtributo('txtNombre') == false) return false;
            if (verificaAtributo('cmbTipo') == false) return false;
            if (verificaAtributo('cmbProceso') == false) return false;
            if (verificaAtributo('txtObjetivo') == false) return false;
            if (verificaAtributo('cmbEstado') == false) return false;
            if (verificaAtributo('cmbPeriodicidad') == false) return false;
            if (verificaAtributo('txtFechaVigenciaIni') == false) return false;
            if (verificaAtributo('cmbOrigenInfo') == false) return false;
            if (verificaAtributo('cmbUnidadMedida') == false) return false;
            if (verificaAtributo('txtDescripcionResponsable') == false) return false;
            if (verificaAtributo('txtLineaBase') == false) return false;
            if (verificaAtributo('txtMeta') == false) return false;

            if ($('#drag' + ventanaActual.num).find('#chkDenominadorRequerido').attr('checked') == true)
                if (verificaAtributo('txtDescripcionDenominador') == false) return false;
            if (verificaAtributo('txtDescripcionNumerador') == false) return false;
            if ($('#drag' + ventanaActual.num).find('#lblTotIndicadores').html() > 1) { alert('! ! !  ATENCION \n Tiene asociado m�s de un indicador, ya no se puede modificar'); return false; }
            break;
        case 'indicador':
            if (verificaAtributo('txtValNumerador') == false) return false;
            if (verificaAtributo('txtValDenominador') == false) return false;
            if (verificaAtributo('txtAnalisis') == false) return false;
            if ($('#drag' + ventanaActual.num).find('#txtValDenominador').val() < 1) { alert('!!!  Denominador no puede ser CERO'); return false; }
            if ($('#drag' + ventanaActual.num).find('#txtIdEstado').val() == '1') { alert('!!!  Indicador cerrado, no se puede modificar'); return false; }
            break;
        case 'fichaPlatin':
            if (verificaAtributo('cmbPrograma') == false) return false;
            if (verificaAtributo('txtIdPaciente') == false) return false;
            if (verificaAtributo('txtNomAutCompetente') == false) return false;
            if (verificaAtributo('cmbTipoPlan') == false) return false;
            if (verificaAtributo('txtFechaIniPlatin') == false) return false;
            if (verificaAtributo('txtFechaFinPlatin') == false) return false;
            if (verificaAtributo('cmbTipoPlan') == false) return false;
            break;
        case 'paciente':
            if (verificaAtributo('cmbTipoIdEdit') == false) return false;
            if (verificaAtributo('txtIdPaciente') == false) return false;
            if (verificaAtributo('txtNombre1') == false) return false;
            if (verificaAtributo('txtApellido1') == false) return false;
            if (verificaAtributo('cmbSexo') == false) return false;
            if (verificaAtributo('txtFechaNacimiento') == false) return false;

            if (validarFecha('txtFechaNacimiento', 110) == false) { return false; }

            if (verificaAtributo('txtTelefono') == false) return false;
            if (verificaAtributo('txtMunicipioResi') == false) return false;
            if (verificaAtributo('txtAdministradora1') == false) return false;
            if (verificaAtributo('txtDireccion') == false) return false;
            if (verificaAtributo('txtTelefono') == false) return false;
            break;

        case 'crearPacienteNuevo':

            if (valorAtributo('lblId') != '') {
                alert('DEBE CREAR EL USUARIO DESDE CERO');
                return false;
            }
            if (verificaAtributo('cmbTipoIdEdit') == false) return false;
            if (verificaAtributo('txtIdPaciente') == false) return false;
            if (verificaAtributo('txtNombre1') == false) return false;
            if (verificaAtributo('txtApellido1') == false) return false;
            if (verificaAtributo('cmbSexo') == false) return false;
            if (verificaAtributo('txtFechaNacimiento') == false) return false;

            if (validarFecha('txtFechaNacimiento', 110) == false) { return false; }

            if (verificaAtributo('txtTelefono') == false) return false;
            if (verificaAtributo('txtAdministradora1') == false) return false;
            if (verificaAtributo('txtMunicipioResi') == false) return false;
            if (verificaAtributo('txtDireccion') == false) return false;
            if (verificaAtributo('txtTelefono') == false) return false;
            break;

        case 'pacienteModifica':

            if (valorAtributo('txtIdBusPaciente') == '') { alert('FALTA SELECCIONAR PACIENTE'); return false; }
            if (valorAtributo('txtMunicipio') == '') { alert('FALTA MUNICIPIO'); return false; }
            if (valorAtributo('txtDireccionRes') == '') { alert('FALTA DIRECCION'); return false; }
            if (valorAtributo('txtAdministradoraPaciente') == '') { alert('FALTA ADMINISTRADORA'); return false; }

            break;

        case 'btn_nueva_Id':
            if (valorAtributo('lblId') == "") {
                alert('No se puede modificar porque aun no ha sido creado');
                return false;
            }
            if (verificaAtributo('cmbTipoIdNueva') == false) return false;
            if (verificaAtributo('txtIdentificacionNueva') == false) return false;
            if (!confirm('Esta seguro ?')) return false;
            break;

        case 'correccion_id_historico':
            if (valorAtributo('lblId') == "") {
                alert('No se puede modificar porque aun no ha sido creado');
                return false;
            }
            if (verificaAtributo('cmbTipoIdNueva') == false) return false;
            if (verificaAtributo('txtIdentificacionNueva') == false) return false;
            break;
        case 'crearSede':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA SEDE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('txtDireccion') == '') { alert('DEBE INGRESAR LA DIRECCION'); return false; }
            if (valorAtributo('txtTelefono') == '') { alert('INGRESE TELEFONO'); return false; }
            if (valorAtributo('txtCelular') == '') { alert('INGRESE CELULAR'); return false; }
            if (valorAtributo('txtCodHabilitacion') == '') { alert('INGRESE CODIGO DE HABILITACION'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR  TIENE SERVICIO DE HOMECARE'); return false; }
            if (valorAtributo('cmbSeccional') == '') { alert('DEBE SELECCIONAR LA SECCIONAL'); return false; }
            break;

        case 'crearArea':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL AREA'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR EL SERVICIO'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('DEBE SELECCIONAR LA SEDE'); return false; }
            break;
        case 'crearProfesion':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA PROFESION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR TIPO DE PROFESION'); return false; }
            if (valorAtributo('cmbFolio') == '') { alert('DEBE SELECCIONAR SI REALIZA IMPRESION DE FOLIOS'); return false; }
            break;
        case 'modificarProfesion':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE EL ELEMENTO A MODIFICAR'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA PROFESION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR TIPO DE PROFESION'); return false; }
            if (valorAtributo('cmbFolio') == '') { alert('DEBE SELECCIONAR SI REALIZA IMPRESION DE FOLIOS'); return false; }
            break;
        case 'modificarSede':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE LA SEDE'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA SEDE'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('txtDireccion') == '') { alert('DEBE INGRESAR LA DIRECCION'); return false; }
            if (valorAtributo('txtTelefono') == '') { alert('INGRESE TELEFONO'); return false; }
            if (valorAtributo('txtCodHabilitacion') == '') { alert('INGRESE CODIGO DE HABILITACION'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR  TIENE SERVICIO DE HOMECARE'); return false; }
            if (valorAtributo('cmbSeccional') == '') { alert('DEBE SELECCIONAR LA SECCIONAL'); return false; }
            break;

        case 'modificarArea':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA AREA'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL AREA'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            if (valorAtributo('cmbServicio') == '') { alert('DEBE SELECCIONAR EL SERVICIO'); return false; }
            if (valorAtributo('cmbSede') == '') { alert('DEBE SELECCIONAR LA SEDE'); return false; }
            break;


        case 'crearHabitacion':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA HABITACION'); return false; }
            if (valorAtributo('cmbIdArea') == '') { alert('FALTA SELECCIONAR AREA'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('DEBE SELECCIONAR EL TIPO'); return false; }
            if (valorAtributo('cmbCosto') == '') { alert('DEBE SELECCIONAR EL COSTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'modificarHabitacion':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UNA HABITACION'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DE LA HABITACION'); return false; }
            if (valorAtributo('cmbIdArea') == '') { alert('FALTA AREA'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('DEBE SELECCIONAR EL TIPO'); return false; }
            if (valorAtributo('cmbCosto') == '') { alert('DEBE SELECCIONAR EL CENTRO DE COSTO'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            //if (valorAtributo('cmbIdAdmision') == '') { alert('DEBE SELECCIONAR TIPO DE ADMISION'); return false; }
            break;

        case 'crearElemento':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL ELEMENTO'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR EL ESTADO'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('DEBE SELECCIONAR EL TIPO'); return false; }
            if (valorAtributo('cmbHabitacion') == '') { alert('DEBE SELECCIONAR LA HABITACION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'modificarElemento':
            if (valorAtributo('txtId') == '') { alert('SELECCIONE UN ELEMENTO'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL ELEMENTO'); return false; }
            if (valorAtributo('cmbIdEstado') == '') { alert('DEBE SELECCIONAR EL ESTADO'); return false; }
            if (valorAtributo('cmbIdTipo') == '') { alert('FALTA TIPO DE ELEMENTO'); return false; }
            if (valorAtributo('cmbHabitacion') == '') { alert('DEBE SELECCIONAR LA HABITACION'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'crearPerfil':
            if (valorAtributo('txtId') == '') { alert('FALTA EL ID DEL ROL'); return false; }
            if (valorAtributo('txtNombre') == '') { alert('INGRESE  NOMBRE DEL ROL'); return false; }
            if (valorAtributo('txtdefinicion') == '') { alert('DEBE SELECCIONAR  DESCRIPCION DEL ROL'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

        case 'modificarPerfil':
            if (valorAtributo('txtNombre') == '') { alert('FALTA EL NOMBRE DEL ROL'); return false; }
            if (valorAtributo('txtdefinicion') == '') { alert('DEBE SELECCIONAR EL ESTADODESCRIPCION DEL ROL'); return false; }
            if (valorAtributo('cmbVigente') == '') { alert('DEBE SELECCIONAR LA VIGENCIA'); return false; }
            break;

    }
    return true;
}

function profesionUsuario() {
    return document.getElementById('lblIdProfesion').lastChild.nodeValue;
}


function ponerFoco(idElem) {
    return $('#drag' + ventanaActual.num).find('#' + idElem).focus();
}

function valorAtributoIdAutoCompletar(idElem) {
    if (valorAtributo(idElem) != '') { //alert(valorAtributo(idElem)+' idElem  '+idElem)
        var datoscadena = valorAtributo(idElem).split('-'); //alert('tiene2: '+datoscadena[1].length)
        if (datoscadena[1] == undefined) {
            //	  alert('ERROR EN EL CAMPO:   '+$('#drag'+ventanaActual.num).find('#'+idElem).val().trim() +'\n\nDEL ELEMENTO:   '+idElem+'\n\nEJEMPLO:    codigo-Nombre elemento') 	
            asignaAtributo(idElem, '', 0)
            ponerFoco(idElem)
            return '';
        } else {
            return datoscadena[0];

        }

    } else return '';
}

function valorAtributoNomAutoCompletar(idElem) {
    if (valorAtributo(idElem) != '') { //alert(valorAtributo(idElem)+' idElem  '+idElem)
        //   	   var datoscadena = valorAtributo(idElem).split('-'); //alert('tiene2: '+datoscadena[1].length)
        var datoscadena = $('#drag' + ventanaActual.num).find('#' + idElem).val().trim().split('-');

        if (datoscadena[1] == undefined) {
            alert('ERROR EN EL CAMPO:   ' + $('#drag' + ventanaActual.num).find('#' + idElem).val().trim() + '\n\nDEL ELEMENTO:   ' + idElem + '\n\nEJEMPLO:    codigo-Nombre elemento')
            asignaAtributo(idElem, '', 0)
            ponerFoco(idElem)
            return '';
        } else {
            return datoscadena[1];

        }
    } else return '';
}

function valorAtributoCombo(idElem) { /*el valor que se ve en el combo escogido*/
    return $('#drag' + ventanaActual.num).find('#' + idElem + ' option:selected').text();
}

function asignaAtributoCombo(idElem, value, text) { /*asignar un nuevo valor al combo*/
    $('#drag' + ventanaActual.num).find('#' + idElem).html('<option value="' + value + '">' + text + '</option>');
    $('#drag' + ventanaActual.num).find('#' + idElem).append('<option value=""></option>');
}


function asignaAtributoCombo2(idElem, value, text) { /*asignar un nuevo valor al combo*/
    $('#drag' + ventanaActual.num).find('#' + idElem).html('<option value="' + value + '">' + text + '</option>');
    //$('#drag'+ventanaActual.num).find('#'+idElem).append('<option value=""></option>' );
}



function IdSesion() {
    return document.getElementById('lblIdUsuarioSesion').lastChild.nodeValue;
}

function IdAuxiliar() {
    return document.getElementById('lblIdAuxiliar').lastChild.nodeValue;
}

function LoginSesion() {
    return document.getElementById('lblLoginUsuarioSesion').lastChild.nodeValue;
}

function IdSede() {
    return document.getElementById('lblIdSede').lastChild.nodeValue;
}
function IdEmpresa() {
    return document.getElementById('lblIdEmpresa').lastChild.nodeValue;
}

function NombreEmpresa() {
    return document.getElementById('lblNomEmpresa').lastChild.nodeValue;
}

function valorAtributoValueCheck(idElem) {
    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {

        case 'chk':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).attr('checked'))
                return '1';
            else return '0';
            break;

    }
    return true;
}

function valorAtributo(idElem) {

    if ($('#drag' + ventanaActual.num).find('#' + idElem).val() != undefined) {
        tipoElem = idElem.substring(0, 3);
        switch (tipoElem) {
            case 'lbl':
                let resp = ($('#drag' + ventanaActual.num).find('#' + idElem).html()).trim();
                if (resp == '') {
                    resp = document.getElementById(idElem).textContent
                }
                return resp
                break;
            case 'txt':
                return encodeURIComponent($('#drag' + ventanaActual.num).find('#' + idElem).val().trim());
                break;
            case 'cmb':
                return ($('#drag' + ventanaActual.num).find('#' + idElem).val()).trim();
                break;
            case 'chk':
                if ($('#drag' + ventanaActual.num).find('#' + idElem).attr('checked'))
                    return 'true';
                else return 'false';
                break;

        }


    } else {
        console.log('No existe elemento: ' + idElem)
        return '';
    }



    return true;
}



function valorAtributoSU(idElem) {
    var elemento = document.getElementById(idElem);
    elemento.id = idElem.replace(/\./g, '');
    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {
        case 'for':
            return $('#drag' + ventanaActual.num).find('#' + idElem).val().trim();
        case 'lbl':
            return ($('#drag' + ventanaActual.num).find('#' + idElem).html()).trim();
            break;
        case 'txt':
            return $('#drag' + ventanaActual.num).find('#' + idElem).val().trim();
            break;
        case 'cmb':
            return ($('#drag' + ventanaActual.num).find('#' + idElem).val()).trim();
            break;
        case 'chk':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).attr('checked'))
                return 'true';
            else return 'false';
            break;
        default:
            return $("#" + idElem).val()

    }
    return true;
}

function verificaAtributo(idElem) {
    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {
        case 'lbl':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).html() == '') { alert('Campo ' + idElem.substring(3, 20) + ' esta vacio'); return false; }
            break;
        case 'txt':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).val() == "") { alert('Campo ' + idElem.substring(3, 20) + ' esta vacio'); return false; }
            break;
        case 'cmb':
            if ($('#drag' + ventanaActual.num).find('#' + idElem).val() == '00' || $('#drag' + ventanaActual.num).find('#' + idElem).val() == '') { alert('Campo ' + idElem.substring(3, 20) + ' esta vacio'); return false; }
            break;
    }
    return true;
}


function asignaAtributo(idElem, registro, activo, callback) {

    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {

        case 'lbl':
            $('#drag' + ventanaActual.num).find('#' + idElem).html($.trim(registro));
            break;
        case 'txt':
            // id element que tenda como en id fecha se le va a cambiar el tipo y valor
            /*if (idElem.indexOf('Fecha') !== -1) {
                var y = document.getElementById(idElem);
                y.setAttribute("type", "date");
                var date = registro.split("/");
                var date_complete = date[2] + "-" + date[1] + "-" + date[0];
                y.setAttribute("value", date_complete);
            } else {*/
            $('#drag' + ventanaActual.num).find('#' + idElem).val($.trim(registro));
            //}
            break;
        case 'for':
            $('#drag' + ventanaActual.num).find('#' + idElem).val($.trim(registro));
            break;
        case 'cmb':
            $('#drag' + ventanaActual.num).find('#' + idElem).val($.trim(registro));
            $("#" + idElem + " option[value='" + $.trim(registro) + "']").attr('selected', 'selected');
            break;
        case 'chk':
            if (registro == 0)
                $('#drag' + ventanaActual.num).find('#' + idElem).attr('checked', '');
            else $('#drag' + ventanaActual.num).find('#' + idElem).attr('checked', 'checked');
            break;
    }

    if (activo == 0) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}


function busquedaExternos() {

    valor_busqueda = document.getElementById("cmbTipoProcedimientoExterno").checked
    if (valor_busqueda == true) {
        limpiarCombosExternos()
        document.getElementById("txtIdProcedimientolabOrdenExt").removeAttribute("hidden")
        document.getElementById("txtIdProcedimientoOrdenExt").setAttribute("hidden", true)
    } else {
        limpiarCombosExternos()
        document.getElementById("txtIdProcedimientoOrdenExt").removeAttribute("hidden")
        document.getElementById("txtIdProcedimientolabOrdenExt").setAttribute("hidden", true)
    }

}

function limpiarCombosExternos() {
    document.getElementById("txtIdProcedimientoOrdenExt").value = ''
    document.getElementById("txtIdProcedimientolabOrdenExt").value = ''
}

function limpiaAtributo(idElem, activo) { // si es 1 desabilitado

    tipoElem = idElem.substring(0, 3);
    switch (tipoElem) {
        case 'lbl':
            $('#drag' + ventanaActual.num).find('#' + idElem).html('');
            break;
        case 'txt':
            $('#drag' + ventanaActual.num).find('#' + idElem).val('');
            break;
        case 'cmb':
            /* alert('primer valor del combo='+$('#drag'+ventanaActual.num).find('#'+idElem+ ' option:selected').text())

                alert('a= '+document.getElementById(idElem).options.length);
                alert('b= '+document.getElementById(idElem).options[0]);*/


            $("#" + idElem + " option[value='00']").attr('selected', 'selected');
            $("#" + idElem + " option[value='']").attr('selected', 'selected');
            break;
        case 'chk':
            $('#drag' + ventanaActual.num).find('#' + idElem).attr('checked', '');
            break;
    }

    if (activo == 0 || activo == undefined) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}

function habilitar(idElem, activo) { // si es 1 desabilitado

    if (activo == 1) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}

function desHabilitar(idElem, activo) { // si es 1 desabilitado

    if (activo == 0) $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', '');
    else $('#drag' + ventanaActual.num).find('#' + idElem).attr('disabled', 'disabled');

}

function concatenarCodigoNombre(codigo, nombre) {
    var cadena = '';
    if (codigo != '') {
        if (nombre != '') {
            cadena = codigo + '-' + nombre;
        } else cadena = codigo;
    }
    return cadena;
}

function concatenarOpcionSistema(codigo, nombre, opcion) {
    var cadena = '';
    if (codigo != '') {
        if (nombre != '') {
            cadena = codigo + '-' + nombre;
        } if (opcion != '') {
            cadena = codigo + '-' + nombre + '-' + opcion;
        } else cadena = codigo;
    }
    return cadena;
}

function splitIdentificacionNombre(nombre) {
    var cadena = '';
    if (nombre != '') {
        cadena = nombre.split("-")[1]
    }
    return cadena
}

function splitTipoIdNombre(nombre) {
    var cadena = '';
    if (nombre != '') {
        cadena = nombre.split("-")[0]
    }
    return cadena
}

/********************************************************** VENTANA MODAL*********************/
function abrirModal() {

    var ancho = 600;
    var alto = 250;
    // fondo transparente
    // creamos un div nuevo, con dos atributos
    var bgdiv = $('<div>').attr({
        className: 'bgtransparent',
        id: 'bgtransparent'
    });
    // agregamos nuevo div a la pagina
    $('body').append(bgdiv);

    // obtenemos ancho y alto de la ventana del explorer
    var wscr = $(window).width();
    var hscr = $(window).height();
    //establecemos las dimensiones del fondo
    $('#bgtransparent').css("width", wscr);
    $('#bgtransparent').css("height", hscr);

    // ventana modal
    // creamos otro div para la ventana modal y dos atributos
    var moddiv = $('<div>').attr({
        className: 'bgmodal',
        id: 'bgmodal'
    });
    // agregamos div a la pagina
    $('body').append(moddiv);

    // agregamos contenido HTML a la ventana modal
    var contenidoHTML = '<p>Tu contenido HTML aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii</p><button onclick=\"cerrarVentanaModal()\">Cerrarrr</button>';
    // var contenidoHTML = $('#drag'+ventanaActual.num).find('#divEditar2');
    // document.getElementById("ventanasActivas").appendChild(contenedor);
    // var contenidoHTML = document.getElementById('divEditar2');		

    titulo = 'dddd';

    var html = "" +
        "<table cellpadding='3' cellspacing='0' border='0' class='ventana-modal-ventana1'><tr><td class='ventana-modal-barra1' align='center' >" + titulo + "</td><td class='ventana-modal-barra1' >" +
        "<img class='ventana-modal-cerrar' src='/pcweb/utilidades/imagenes/ventana-modal/cerrar.gif' title='Cerrar ventana' onclick='cerrarVentanaModal()'>" +
        "</td></tr><tr><td align='center' colspan='2' background='../utilidades/imagenes/acciones/fondo-aplicacion.gif' ><div id='contenidoModal'></div>"
        //+ "<iframe name='" + nombre + "' src='" + pagina + "' width='100%' height='" + (parseInt(alto) - 30) + "' frameborder='0'></iframe>"

        +
        "</td></tr></table>";


    $('#bgmodal').append(html);
    //			      document.getElementById("drag"+ventanaActual.num).innerHTML = varajaxMenu.responseText;
    //  $('#bgmodal').innerHTML ="dddddddd"

    //     $('#bgmodal').setContenido(document.getElementById('divEditar2'));
    // redimensionamos para que se ajuste al centro y mas
    $(window).resize();
    //   });

    $(window).resize(function () {
        // dimensiones de la ventana del explorer 
        var wscr = $(window).width();
        var hscr = $(window).height();

        // estableciendo dimensiones de fondo
        $('#bgtransparent').css("width", wscr);
        $('#bgtransparent').css("height", hscr);

        // estableciendo tama�o de la ventana modal
        $('#bgmodal').css("width", ancho + 'px');
        $('#bgmodal').css("height", alto + 'px');

        // obtiendo tama�o de la ventana modal
        var wcnt = $('#bgmodal').width();
        var hcnt = $('#bgmodal').height();

        // obtener posicion central
        var mleft = (wscr - wcnt) / 2;
        var mtop = (hscr - hcnt) / 2;

        // estableciendo ventana modal en el centro
        $('#bgmodal').css("left", mleft + 'px');
        $('#bgmodal').css("top", mtop + 'px');
    });

};

function cerrarVentanaModal() {
    // removemos divs creados
    $('#bgmodal').remove();
    $('#bgtransparent').remove();
}



///

function validarFecha(campo, limite) {

    fecha = valorAtributo(campo).replace(/%2F/g, "/");

    fecha = new Date(desformatearFecha(fecha));
    hoy = new Date();

    if (fecha >= hoy) {
        alert('LA FECHA NO PUEDE SER MAYOR AL DIA DE HOY');
        return false;
    }

    if (calcularEdad(fecha) > limite) {
        alert('LA EDAD NO PUEDE SER MAYOR A ' + limite);
        return false;
    }

}

function calcularEdad(fecha) {
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    return edad;
}


function validaEspacio(e, campo) {
    key = e.keyCode ? e.keyCode : e.which;
    if (key == 32) { return false; }
}

function buscarUsuario(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';

    switch (arg) {
        case 'listGrillaAntecedentesFolio':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1010&parametros=";

            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#listGrillaAntecedentesFolio").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_ANTECEDENTE', 'ANTECEDENTE'],
                colModel: [
                    { name: 'Tot', index: 'Tot', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_ANTECEDENTE', index: 'ID_ANTECEDENTE', hidden: true },
                    { name: 'ANTECEDENTE', index: 'ANTECEDENTE', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 350,
                width: ancho,
                caption: "ANTECENTES TIPO FOLIO",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaAntecedentesFolio').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdAntec', datosRow.ID_ANTECEDENTE, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaGrupoUsuarios':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=598&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdentificacion'));

            $('#drag' + ventanaActual.num).find("#listGrillaGrupoUsuarios").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID ROL', 'ROL', 'DEFINICION'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_grupo', index: 'id_grupo', width: anchoP(ancho, 3) },
                    { name: 'descripcion', index: 'descripcion', width: anchoP(ancho, 10) },
                    { name: 'definicion', index: 'definicion', width: anchoP(ancho, 10) }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 350,
                width: ancho,
                caption: "ROLES DE USUARIO",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaGrupoUsuarios').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdGrupo', datosRow.id_grupo, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPerfiles':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=941&parametros=";

            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));


            $('#drag' + ventanaActual.num).find("#listGrillaPerfiles").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['contador', 'ID_GRUPO', 'ROL', 'DEFINICION', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID_GRUPO', index: 'ID_GRUPO', width: anchoP(ancho, 4) },
                    { name: 'ROL', index: 'ROL', width: anchoP(ancho, 10) },
                    { name: 'DEFINICION', index: 'DEFINICION', width: anchoP(ancho, 10) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPerfiles').getRowData(rowid);

                    asignaAtributo('txtId', datosRow.ID_GRUPO, 1);
                    asignaAtributo('txtNombre', datosRow.ROL, 0);
                    asignaAtributo('txtdefinicion', datosRow.DEFINICION, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                    buscarParametros('listGrillaGrupopciones');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;



        case 'listGrillaUsuarios':
            // limpiarDivEditarJuan(arg); 
            //ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=595&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusId'));
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(valorAtributo('cmbSE'));
            add_valores_a_mandar(valorAtributo('cmbSE'));

            $('#drag' + ventanaActual.num).find("#listGrillaUsuarios").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'TIPO ID', 'IDENTIFICACION', 'pnombre', 'snombre', 'papellido', 'sapellido', 'NOMBRE PERSONAL', 'id_prof', 'PROFESION', 'registro', 'estado', 'usuario', 'Correo', 'Telefono', 'Sede', 'Estado', 'Soporte', 'Firma'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'tipo_id', index: 'tipo_id', width: 20 },
                    { name: 'id', index: 'id', width: 38 },
                    { name: 'pnombre', index: 'pnombre', hidden: true },
                    { name: 'snombre', index: 'snombre', hidden: true },
                    { name: 'papellido', index: 'papellido', hidden: true },
                    { name: 'sapellido', index: 'sapellido', hidden: true },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 25) },
                    { name: 'id_profesion', index: 'id_profesion', hidden: true },
                    { name: 'Profesiones', index: 'Profesiones', width: anchoP(ancho, 15) },
                    { name: 'noregistro', index: 'noregistro', hidden: true },
                    { name: 'estado', index: 'estado', hidden: true },
                    { name: 'usuario', index: 'usuario', hidden: true },
                    { name: 'Correo', index: 'Correo', hidden: true },
                    { name: 'Telefono', index: 'Telefono', hidden: true },
                    { name: 'Sede', index: 'Sede', hidden: true },
                    { name: 'Estado', index: 'Estado', hidden: true },
                    { name: 'Soporte', index: 'Soporte', hidden: true },
                    { name: 'Firma', index: 'Firma', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: 1050,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaUsuarios').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('cmbTipoId', datosRow.tipo_id, 0);
                    asignaAtributo('txtIdentificacion', datosRow.id, 0);
                    asignaAtributo('txtPNombre', datosRow.pnombre, 0);
                    asignaAtributo('txtSNombre', datosRow.snombre, 0);
                    asignaAtributo('txtPApellido', datosRow.papellido, 0);
                    asignaAtributo('txtSApellido', datosRow.sapellido, 0);

                    asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                    asignaAtributo('cmbIdTipoProfesion', datosRow.id_profesion, 0);
                    asignaAtributo('txtRegistro', datosRow.noregistro, 0);
                    asignaAtributo('cmbVigente', datosRow.estado, 0);
                    asignaAtributo('txtUSuario', datosRow.usuario, 0);
                    asignaAtributo('txtCorreo', datosRow.Correo, 0);
                    asignaAtributo('txtDigitacion', datosRow.Fecha_Digitacion, 0);
                    asignaAtributo('txtTipo', datosRow.Tipo, 0);
                    asignaAtributo('txtPhone', datosRow.Telefono, 0);
                    asignaAtributo('cmbSede', datosRow.Sede, 0);
                    asignaAtributo('cmbEstado', datosRow.Estado, 0);
                    asignaAtributo('cmbResponsable', datosRow.Soporte, 0);
                    recargarFirmaProfesional(datosRow.Firma);

                    asignaAtributo('lblFirmaProfesional', datosRow.Firma, 0);

                    buscarUsuario('sede');
                    setTimeout(() => {
                        buscarUsuario('listGrillaGrupoUsuarios');
                        setTimeout(() => {
                            buscarUsuario('sedes');
                        }, 200);
                    }, 200);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'sede':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=938&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdentificacion'));

            $('#drag' + ventanaActual.num).find("#sede").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id GR', 'NOMBRE SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 3), hidden: true },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 125,
                width: ancho,
                caption: "SEDES SECUNDARIAS",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#sede').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdSede', datosRow.id_sede, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'sedes':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=982&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdentificacion'));

            $('#drag' + ventanaActual.num).find("#sedes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id GR', 'NOMBRE SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 3), hidden: true },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 125,
                width: ancho,
                caption: "SEDES",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosSedeRow = jQuery('#drag' + ventanaActual.num).find('#sedes').getRowData(rowid);
                    estad = 0;
                    //asignaAtributo('lblIdSede', datosSedeRow.id_sede, 0);
                },
                multiselect: true,
                multiboxonly: true
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'sedeB':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=996&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdentificacion'));

            $('#drag' + ventanaActual.num).find("#sedeB").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id GR', 'NOMBRE SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 3), hidden: true },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 125,
                width: ancho,
                caption: "SEDES ASOCIADAS",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#sedeB').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdSede', datosRow.id_sede, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'sedesB':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=995&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdentificacion'));

            $('#drag' + ventanaActual.num).find("#sedesB").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'id GR', 'NOMBRE SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 3), hidden: true },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 10) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 125,
                width: ancho,
                caption: "SEDES",
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosSedeRow = jQuery('#drag' + ventanaActual.num).find('#sedesB').getRowData(rowid);
                    estad = 0;
                    //asignaAtributo('lblIdSede', datosSedeRow.id_sede, 0);
                },
                multiselect: true,
                multiboxonly: true
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

    }
}


function mostrarOpcionesAgenda() {
    $("#tituloOpciones").show();
    $("#divOpciones").show();
    $("#divBotonOpciones").hide();
}

function OcultarOpcionesAgenda() {
    $("#tituloOpciones").hide();
    $("#divOpciones").hide();
    $("#divBotonOpciones").show();
}


function ponerTitulo(idElemento, mensaje) {
    msj = String(mensaje);
    document.getElementById(idElemento).title = mensaje;
}

function quitarTitulo(idElemento) {
    document.getElementById(idElemento).removeAttribute("title");
}

function mostrarGrafica() {
    var url = "hc/graficas/grafica.jsp";
    var dimension = 'width=1150,height=952,scrollbars=YES,statusbar=NO,left=150,top=90';
    window.open(url, '', dimension);
}

function graficaPlantilla(idEvolucion, idPlantillaGrafica) {


    var url = "hc/graficas/grafica.jsp?idEvolucion=" + idEvolucion + "&idPlantillaGrafica='" + idPlantillaGrafica + "'";
    var dimension = 'width=1150,height=952,scrollbars=YES,statusbar=NO,left=150,top=90';
    window.open(url, '', dimension);
}

function imprimirPdd(idEvolucion) {
    console.log(idEvolucion)

    jasper = 'VIH_TBL';
    nombre = 'TB-LATENTE-' + valorAtributo("lblIdentificacionPaciente");
    var nuevaURL = "ireports/hc/generaImpresionProMed.jsp?procedimientos=" + idEvolucion + "&jasper=" + jasper + "&nombre=" + nombre + "&evo=" + valorAtributo("lblIdDocumento");
    window.open(nuevaURL, '', 'width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230');

} function imprimirVistaParcialVih(idEvolucion) {
    console.log(idEvolucion)
    jasper = 'B24XR';
    nombre = 'HC-CC' + valorAtributo("lblIdentificacionPaciente");

    var nuevaURL = "ireports/hc/generaHC.jsp?reporte=" + jasper + "&evo=" + idEvolucion + "&nombre=" + nombre;
    window.open(nuevaURL, '', 'width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230');
}

function validarNumeros(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function calcularFechas() {
    function calcularFechas() {

        var numero = valorAtributo('txtDiasIncapacidad').toString();
        var fechaInicial = valorAtributo('txtFechaIncaInicio').toString();
        var fechamod = fechaInicial.replace("%2F", "/");
        var fechamod2 = fechamod.replace("%2F", "/");
        var numero2 = parseInt(numero);
        var fecha200 = new Date(formatoFecha(fechamod2));
        fecha200.setDate(fecha200.getDate() + numero2);
        var fecha201 = fecha200.toLocaleDateString();
        asignaAtributo('txtFechaIncaFin', fecha201, 0);
        asignaAtributo('txtFechaIncaFin2', fecha201, 0);

    }

    function formatoFecha(fecha) {

        var fechaT = fecha.split('/');
        return fechaT[1] + '/' + fechaT[0] + '/' + fechaT[2];

    }
    var numero = valorAtributo('txtDiasIncapacidad').toString();
    var fechaInicial = valorAtributo('txtFechaIncaInicio').toString();
    var fechamod = fechaInicial.replace("%2F", "/");
    var fechamod2 = fechamod.replace("%2F", "/");
    var numero2 = parseInt(numero);
    numero2 = numero2 - 1;
    console.log(numero2)
    var fecha200 = new Date(formatoFecha(fechamod2));
    fecha200.setDate(fecha200.getDate() + numero2);
    var fecha201 = fecha200.toLocaleDateString();
    asignaAtributo('txtFechaIncaFin', fecha201, 0);
    asignaAtributo('txtFechaIncaFin2', fecha201, 0);

}

function formatoFecha(fecha) {

    var fechaT = fecha.split('/');
    return fechaT[1] + '/' + fechaT[0] + '/' + fechaT[2];

}

function validarFechaAnt() {
    var hoy = new Date().toISOString().split('T')[0];
    var hora = new Date().toLocaleTimeString('en-GB')
    console.log('h', hora)
    try {
        document.getElementById("txtFechaEntrega1").setAttribute('min', hoy);
    } catch (error) { };

    try {
        document.getElementById("txtFechaAplazada1").setAttribute('min', hoy);
    } catch (error) { };

    try {
        document.getElementById("txtFechaTentativaSolucion").setAttribute('min', hoy + ' ' + hora.toString());
    } catch (error) { };



}

function imprimirEncuesta() {

    var nuevaURL = "";
    var idReporte = "TAMIZAJES";
    nuevaURL = "ireports/hc/generaHC.jsp?reporte=" + idReporte + "&evo=" + valorAtributo('lblIdEncuestaPaciente');
    var dimension = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90';
    if (valorAtributo('lblIdEncuestaPaciente') != '') window.open(nuevaURL, '', dimension);
    else alert('Debe Seleccionar una encuesta')
}

function imprimirAnexo1() {
    var idPaciente = valorAtributo("lblIdP");
    var nombrepdf = "A1-" + valorAtributo("lblNumeroId") + ".pdf"
    var Url = "/clinica/paginas/ireports/hc/generaAnexo.jsp?idpaciente=" + idPaciente + "&nombre=" + nombrepdf;
    var dimen = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90';
    window.open(Url, '', dimen);

}

function recargarFirmaProfesional(nombre) {

    var imagen = '../paginas/ireports/imagenes/firmas/' + nombre + '?' + Math.floor((Math.random() * 100) + 1);
    $("#imgFirmaProfesional").attr('src', imagen);

}

function validarImagenFirma() {
    if (valorAtributo('txtIdentificacion') === '') {
        alert('Debe seleccionar un profesional');
        return;
    }

    var nombre = document.getElementById("firma").value;
    var ext = nombre.substring(nombre.length - 3, nombre.length).toLowerCase();
    //Se valida la extension del archivo
    if (ext === "jpg" || ext === "jpeg" || ext === "png") {

        var selectedfile = document.getElementById("firma").files;

        if (selectedfile.length > 0) {
            var imageFile = selectedfile[0];

            //Se transforma la imagen a base64
            var fileReader = new FileReader();
            fileReader.onload = function (fileLoadedEvent) {
                var srcData = fileLoadedEvent.target.result;
                valores_a_mandar = 'base64=' + srcData + "&ext=" + ext + "&firma=" + valorAtributo('lblFirmaProfesional');
                ajaxSubirImagen();

            }

            fileReader.readAsDataURL(imageFile);

        }

    } else {
        alert("Archivo no valido");
        document.getElementById("firma").focus();
    }
}

function ajaxSubirImagen() {
    varajax = crearAjax();
    varajax.open("POST", '/clinica/paginas/accionesXml/crear_imagen_xml.jsp', true);
    varajax.onreadystatechange = respuestaSubirImagen;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}

function respuestaSubirImagen() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            MsgAlerta = raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data;

            if (raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data == '') {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                    alert('Imagen guardada');
                    recargarFirmaProfesional(valorAtributo('lblFirmaProfesional'));
                    document.getElementById("firma").value = '';

                } else {
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            }
            else alert('PROBLEMAS AL REGISTRAR\n' + MsgAlerta)

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
    }
}

var nom1 = "";
var nom2 = "";
var ape1 = "";
var ape2 = "";
function llenarNombre1() {
    nom1 = document.getElementById("txtPNombre").value;
    nom1 = nom1.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 + ape2;
}
/*function llenarNombre2()
{
    nom2 = document.getElementById("txtSNombre").value;
    nom2 = nom2.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 +ape2;
}*/
function llenarApe1() {
    ape1 = document.getElementById("txtPApellido").value;
    ape1 = ape1.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 + ape2;
}
/*function llenarApe2()
{
    ape2 = document.getElementById("txtSApellido").value;
    ape2 = ape2.toUpperCase() + " ";
    document.getElementById("txtNomPersonal").value = nom1 + nom2 + ape1 +ape2;
}*/

function teleconsulta() {
    var btnVideo = document.getElementById("divVideollamada");
    btnVideo.style.display = "BLOCK";
    //var frame = $('#frame');
    //var url = 'https://190.60.242.160:8082';
    //frame.attr('src',url).show();
    //var url = "https://190.60.242.160:8082";
    //window.open(url, '_blank');

}

function ventanaEditar(divPaVentanita) {
    if (valorAtributo('lblIdDocumento') == '') {
        alert('SELECCIONE UNA SOLICITUD');
        return false;
    }
    else {
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        varajaxMenu.open("POST", '/clinica/paginas/autorizaciones/editar.jsp', true);

        varajaxMenu.onreadystatechange = function () { llenarVentanaEditar(divPaVentanita) };
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
    }
}

function llenarVentanaEditar(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var f = new Date();
            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            var doc = valorAtributo('lblIdDocumento');
            asignaAtributo('txtSolicitud', valorAtributo('lblSolicitud'), 1);
            asignaAtributo('txtPaciente', valorAtributo('lblPaciente'), 1);
            asignaAtributo('txtMedico', valorAtributo('lblMedico'), 1);
            asignaAtributo('cmbTipoDiagnostico', valorAtributo('lblTipoDiagnostico'), 0);
            asignaAtributo('txtDiagnosticoP', valorAtributo('lblDiagnosticoP'), 0);
            asignaAtributo('txtJustificacion', valorAtributo('lblJustificacion'), 0);
            asignaAtributo('lblIdDocumentoV', doc, 0);
            asignaAtributo('txtFecha', f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear(), 1);
            asignaAtributo('txtHora', f.getHours() + ":" + f.getMinutes(), 1);
            setTimeout(buscarHC('listGrillaProcedimientos'), 1000);
            setTimeout(buscarHistoria('listArchivosAdjuntosAnexos'), 1000);
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function traerVentanitaFuncionesAT(idLupitaVentanita, accion, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaFuncionesSO.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaFuncionesAT(divPaVentanita, accion) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaFuncionesAT(divPaVentanita, accion) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuestaS');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            asignaAtributo("txtAccionVentintaHC", accion, 0)
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}


function cargarVentana(tipo) {
    switch (tipo) {
        case "procedimientos":
            mostrar('divCargarProcedimientos');
            setTimeout("cargarFoliosPaciente('cmbHcPacienteProcedimentos', 'lblIdPaciente','procedimiento')", 300);
            setTimeout("buscarHC('listProcedimientosPaciente')", 500);
            break;
        case "medicamentos":
            mostrar('divCargarMedicamentos');
            setTimeout("cargarFoliosPaciente('cmbHcPacienteMedicamentos', 'lblIdPaciente','medicamentos')", 300);
            setTimeout("buscarHC('listMedicamentosPaciente')", 500);
            break;
        case "paqueteProcedimientos":
            mostrar('divCargarPaqProcedimientos');
            setTimeout("buscarHC('listPaqueteProcedimientos')", 500);
            break;
    }

}

function cerrarVentana(tipo) {
    if (tipo == "procedimientos") {
        ocultar('divCargarProcedimientos');
        ocultar('gview_listGrillaCargarProcedimientos');
        ocultar('gbox_listGrillaCargarProcedimientos');
    }
    else {
        ocultar('divCargarMedicamentos');
        ocultar('gview_listGrillaCargarMedicamentos');
        ocultar('gbox_listGrillaCargarMedicamentos')
    }
}

function imprimirCitasPaciente() {
    if (valorAtributo("lblIdPacienteExiste") == '') {
        alert("SELECCIONE UN PACIENTE")
        return false
    }
    var nombrepdf = "citas.pdf"
    var Url = "/clinica/paginas/ireports/hc/generaAgendaPaciente.jsp?p1=" + valorAtributo("lblIdPacienteExiste") + "&p2=" + valorAtributo("txtFechainicial") + "&p3=" + valorAtributo("txtFechafinal") + "&p4=" + valorAtributo("cmbIdespecialidad") + "&nombre=" + nombrepdf;
    var dimen = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90';
    window.open(Url, '', dimen);
}


function ingresarLimites(input, superior, inferior, valorActual) {

    superior = parseInt(superior)
    inferior = parseInt(inferior)
    var inputV = document.getElementById(input)
    inputV.setAttribute('max', superior);
    inputV.setAttribute('min', inferior);
    if (valorActual > superior || valorActual < inferior) {
        if (valorActual < inferior)
            alert("Solo se permiten valores mayores a " + inferior);
        else
            alert("Solo se permiten valores entre " + inferior + " y " + superior);

        if (valorActual > superior) {
            inputV.value = superior
        }
        if (valorActual < inferior) {
            inputV.value = inferior
        }
        return
    }
}

function maxLongitudNumero(input, max) {
    var inputV = document.getElementById(input)
    max = parseInt(max)
    if (inputV.value.length > max) {
        if (inputV.value[0] == '-') {
            max = max + 1
        }
        inputV.value = inputV.value.slice(0, max);
    }
}

function imprimirAgendaPaciente() {
    var dimension = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90'
    var nombrepdf = "AGENDA PACIENTE - " + valorAtributo('txtIdentificacion') + ".pdf"
    nuevaURL = "ireports/hc/generaAgendaPaciente.jsp?p1=" + valorAtributoIdAutoCompletar('lblIdPacienteExiste') + "&p2=" + valorAtributo("txtFechaInicioCitasPaciente") + "&p3=" + valorAtributo("txtFechaFinCitasPaciente") + "&p4=" + valorAtributo("cmbIdEspecialidadCitasPaciente") + "&p5=" + valorAtributo("cmbIdTipoCitasPaciente") + "&nombre=" + nombrepdf;
    window.open(nuevaURL, '', dimension);
}

function imprimirAnexo3() {
    var dimension = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90'
    var nombrepdf = "ORDEN - " + valorAtributo('txtNumeroSolicitud') + ".pdf"
    nuevaURL = "ireports/hc/generaHC.jsp?reporte=anexo3&evo=" + valorAtributo('txtNumeroSolicitud') + "&nombre=" + nombrepdf;
    window.open(nuevaURL, '', dimension);
}

function habilitarMedida() {
    if (document.getElementById("txtCant").value == null || document.getElementById("txtCant").value == '') {
        limpiaAtributo("txtMedida", 1)
        desHabilitar('txtMedida', 1);
    } else {
        desHabilitar('txtMedida', 0);
    }
}

function habilitarRemitido() {
    if (document.getElementById("txtRemitidoDesde").checked == false) {
        desHabilitar('txtIpsRemisora', 1);
        desHabilitar('txtMedicoRemite', 1);
        desHabilitar('cmbEspecialidadRemite', 1);
        desHabilitar('txtFechaRemite', 1);
        desHabilitar('txtHoraRemite', 1);
        desHabilitar('txtDXRemite', 1);
    }
    else {
        desHabilitar('txtIpsRemisora', 0);
        desHabilitar('txtMedicoRemite', 0);
        desHabilitar('cmbEspecialidadRemite', 0);
        desHabilitar('txtFechaRemite', 0);
        desHabilitar('txtHoraRemite', 0);
        desHabilitar('txtDXRemite', 0);
    }
}

function habilitarRemitir() {
    if (document.getElementById("txtRemitidoa").checked == false) {
        desHabilitar('txtIpsRemitida', 1);
        desHabilitar('txtMedicoRemiteR', 1);
        desHabilitar('cmbEspecialidadRemite', 1);
        desHabilitar('txtFechaRemitida', 1);
        desHabilitar('txtHoraRemitida', 1);
        desHabilitar('cmbIdDxRemision', 1);
        desHabilitar('cmbPrioridadRemision', 1);
    }
    else {
        desHabilitar('txtIpsRemitida', 0);
        desHabilitar('cmbEspecialidadRemitida', 0);
        desHabilitar('cmbIdDxRemision', 0);
        desHabilitar('cmbPrioridadRemision', 0);
    }
}

function habilitarContra() {
    if (document.getElementById("txtContra").checked == false) {
        desHabilitar('txtPertinente', 1);
        desHabilitar('txtFechaContra', 1);
        desHabilitar('txtHoraContra', 1);
        desHabilitar('txtDxContra', 1);
    }
    else {
        desHabilitar('txtPertinente', 0);
        desHabilitar('txtFechaContra', 0);
        desHabilitar('txtHoraContra', 0);
        desHabilitar('txtDxContra', 0);
    }
}

function imprimirRemision(id) {
    var dimension = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90'
    var nombrepdf = "REMISION - " + valorAtributo('txtIdentificacion') + ".pdf"
    nuevaURL = "ireports/hc/generaHC.jsp?evo=" + id + "&reporte=REFERENCIA" + "&nombre=" + nombrepdf;
    window.open(nuevaURL, '', dimension);
}

function listaProcedimientos() {
    var lista_procedimientos = [];
    var ids = $("#listaProcedimientoConductaTratamientoS").getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listaProcedimientoConductaTratamientoS_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosProcedimientosRow = $("#listaProcedimientoConductaTratamientoS").getRowData(ids[i]);
            lista_procedimientos.push(datosProcedimientosRow.ID);
        }
    }
    console.log(lista_procedimientos);
    return lista_procedimientos;
}

function traerVentanitaDx(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaDx.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanita(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function onKeyDownHandler(event, item) {

    var codigo = event.which || event.keyCode;
    if (codigo === 111) {
        console.log(item)
        console.log(document.getElementById(item).value)
        document.getElementById(item).value = document.getElementById(item).value + "/";
    }
}

function cargarTipoCitaModelo(comboOrigen, comboDestino) { //alert(6666)
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 2501, valorAtributo(comboOrigen));
}

function cargarRutas(combo) {
    cargarComboGRALCondicion1(
        "cmbPadre",
        "1",
        combo,
        3100,
        valorAtributo('lblEdadPaciente')
    );
}

function traerVentanitaProfesionalModelo(idLupitaVentanita, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;
    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaProfesionalModelo.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaProfesionalModelo(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaProfesionalModelo(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuestaModelo');

            var d = document.getElementById('ventanitaHijaModelo');
            d.style.position = "fixed";
            //alert(d.style.top)
            //d.style.position = "absolute";
            //d.style.top = topY + 'px';

            $('#drag' + ventanaActual.num).find('#txtCodProfesional').focus();
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function bloquearCampos() {
    ocultar("diasIncapacidad")
    ocultar("finalIncapacidad")
    switch (valorAtributo('cmbIdTipo')) {
        case "1":
            mostrar("diasIncapacidad");
            mostrar("finalIncapacidad")
            break;
        case "2":
            ocultar("diasIncapacidad");
            ocultar("finalIncapacidad")
        case "3":
            mostrar("diasIncapacidad");
            mostrar("finalIncapacidad");
            break;
    }
}



function alertasGenerales(input, valorBaseAlerta, valorInput) {


    var inputV = document.getElementById(input)
    // console.log("valor para la alerta", valorBaseAlerta, "Valor input", valorInput);
    // console.log("datos Input", inputV.id);
    // console.log("datos Input", inputV);

    switch (inputV.id) {
        case 'formulario.daan.c26':
            // Validacion que arroja alerta de riesgo cardiovascular para paciente masculino
            var valorTecleado = parseInt(valorInput);
            var valorAlerta = parseInt(valorBaseAlerta);
            if (valorTecleado >= valorAlerta) {
                alert("Paciente con riesgo cardiovascular");
            }
            break;

        case 'formulario.daan.c27':
            // Validacion que arroja alerta de riesgo cardiovascular para paciente femenido
            var valorTecleado = parseInt(valorInput);
            var valorAlerta = parseInt(valorBaseAlerta);
            if (valorTecleado >= valorAlerta) {
                alert("Paciente con riesgo cardiovascular");
            }

            break;

        default:
            break;
    }


}

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        /* if present, the header is where you move the DIV from:*/
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

function ImprimirOrdenExterna() {
    evo = $("#listaProcedimientosOrdenExterna").getRowData(1).ID_EVOLUCION;
    var nombrepdf = "PROCEDIMIENTOS - " + valorAtributo('txtIdentificacionOrdenes') + ".pdf"
    nuevaURL = "ireports/hc/generaHC.jsp?reporte=formula_procedimientos&evo=" + evo + "&nombre=" + nombrepdf;
    window.open(nuevaURL, '');
}

function swAlertError(text, title = '') {
    Swal.fire({
        icon: 'error',
        title: title,
        text: text
    })
}



/**
 * Función que muestra un modal de SweetAlert con un título, mensaje, icono y botones de confirmación y cancelación. Si el usuario confirma, se ejecuta una función de callback.
 * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
 * @param {String} title - El título del modal.
 * @param {String} message - El mensaje a mostrar en el modal.
 * @param {String} icon - El tipo de icono a mostrar en el modal (success, error, warning, info, question).
 * @param {function} onConfirm - La función a ejecutar si el usuario confirma la acción en el modal.
 * @param {object} [buttons] - Opcional. Objeto que contiene las etiquetas de los botones. Si no se proporciona, se utilizarán los valores por defecto.
 * @param {string} [buttons.cancel='Cancelar'] - Opcional. Etiqueta del botón de cancelación.
 * @param {string} [buttons.confirm='Confirmar'] - Opcional. Etiqueta del botón de confirmación.
 * @returns {void}
*/
function ConfirmAlertSw(title, message, icon, onConfirm, buttons) {
    if (buttons == undefined) {
        buttons = {
            cancel: 'Cancelar',
            confirm: 'Confirmar',
        }
    }
    Swal.fire({
        title: title,
        text: message,
        icon: icon,
        showCancelButton: true,
        cancelButtonText: buttons.cancel,
        confirmButtonText: buttons.confirm,
        confirmButtonColor: '#137493', // Define el color del botón Confirmar
        cancelButtonColor: '#fda253',
    }).then(function (result) {
        if (result.isConfirmed) {
            onConfirm();
        }
    });
}

/**
 * Muestra una alerta personalizada en la pantalla utilizando SweetAlert2.
 * 
 * @param {string} icon - El ícono a mostrar en la alerta. Puede ser 'success', 'error', 'warning', 'info', o 'question'.
 * @param {string} text - El texto principal de la alerta.
 * @param {string} [title=''] - El título de la alerta. Por defecto está vacío.
 * @param {string} [html=''] - HTML adicional para mostrar en la alerta. Por defecto está vacío.
 * @param {number} [timer=0] - Tiempo en milisegundos antes de que la alerta se cierre automáticamente. 0 significa que no se cerrará automáticamente.
 * @param {boolean} [showConfirmButton=false] - Indica si se debe mostrar el botón de confirmación. Por defecto es 'false'.
 * @param {string} [position='center'] - La posición en la que se mostrará la alerta. Puede ser 'top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', o 'bottom-end'. Por defecto es 'center'.
 * 
 * Ejemplo de uso:
 * swAlert('success', 'Operación completada exitosamente', 'Éxito');
 * 
 * @author Nicolas Caicedo <nicolas.caicedo1799@gmail.com>
 * @returns {void}
 */
function swAlert(icon, text, title = '', html = '', timer = 0, showConfirmButton = false, position = 'center') {
    // success - error - warning - info - question
    Swal.fire({
        icon: icon,
        title: title,
        text: text,
        html,
        timer: timer,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Continuar',
        showConfirmButton: showConfirmButton,
        position: position
    })
}


function fecha_actual_laboratorios() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1; //hoy es 0!
    var yyyy = hoy.getFullYear();
    if (dd < 10) {
        dd = "0" + dd;
    }
    if (mm < 10) {
        mm = "0" + mm;
    }
    hoy = yyyy + "-" + mm + "-" + dd;
    return hoy;
}


/**
 * Muestra la alerta que se encuentre en el jsp. Importante tener el esquema en el jsp en el que se quiere que se muestre
 * @author Nicolas Caicedo <nicolascr@firmasdigitales.com.co>
 * @param {string} divId - id del div que contiene la notificación a mostrar
 * @param {string} title - Titulo que contiene la notificación
 * @param {string} content - El contenido que se mostrara en la notificación
 * @param {boolean} hideButtonContinue - Variable booleana la cual oculta el boton de confirmación si está en true y muestra el boton de confirmación si es false
 * @returns {void} Nothing
*/
function mostrarAlerta(divId, title, content, hideButtonContinue = true) {
    $(`#tittle${divId.slice(3)}`).text(title);
    $(`#lbl${divId.slice(3)}`).text(content);
    if (hideButtonContinue) {
        $(`#btn${divId.slice(3)}Alerta`).hide();
    } else {
        $(`#btn${divId.slice(3)}Alerta`).show();
    }
    $(`#${divId}`).show()
}

function test_() {
    var myHeaders = new Headers();
    myHeaders.append("Cookie", "JSESSIONID8380=CE7C9D24952A818676AC015DCC6E48AC");

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    for (i = 0; i < 100; i++) {
        fetch("https://190.60.243.113:8383/clinica/paginas/accionesXml/buscarGrilla_xml.jsp?idQuery=240&parametros=_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-1_-1_-1_-1_-1_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x&_search=false&nd=1665459333990&rows=-1&page=1&sidx=&sord=asc", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }
}

function limpiarYdeshabilitar(value) {
    console.log({ value });
    switch (value) {
        case 'noAplica':
            let checkNoAplica = document.getElementById('checkIdCheckNoAplicaPerodicidad').checked
            if (checkNoAplica) {
                console.log({ checkNoAplica });
                limpiaAtributo('txtIdCheckMesesPerodicidadMin')
                desHabilitar('txtIdCheckMesesPerodicidadMin', 1)
                limpiaAtributo('txtIdCheckMesesPerodicidadMax')
                desHabilitar('txtIdCheckMesesPerodicidadMax', 1)
                desHabilitar('agregarEdadMaxMin',1) 

            }

            break;
        case 'meses':
            let checkMeses = document.getElementById('checkIdCheckMesesPerodicidad').checked
            if (checkMeses) {
                console.log({ checkMeses });
                desHabilitar('txtIdCheckMesesPerodicidadMin', 0)
                desHabilitar('txtIdCheckMesesPerodicidadMax', 0)
                desHabilitar('agregarEdadMaxMin',0) 

            }

            break;
        case 'anios':
            let checkAnios = document.getElementById('checkIdCheckAniosPerodicidad').checked
            if (checkAnios) {
                console.log({ checkAnios });
                limpiaAtributo('txtIdCheckMesesPerodicidad')
                desHabilitar('txtIdCheckMesesPerodicidad', 1)
                desHabilitar('txtIdCheckAniosPerodicidad', 0)
            }

            break;

        default:
            break;
    }
}

function test(url = 'https://cristalweb.emssanar.org.co:8384/clinica/paginas/accionesXml/buscarGrilla_xml.jsp?idQuery=240&parametros=_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-1_-1_-1_-1_-1_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x_-x.X.x&_search=false&nd=1665459333990&rows=-1&page=1&sidx=&sord=asc') {
    for (let i = 0; i < 100; i++) {
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                console.log("ENVIANDO -> " + i)
            },
            success: function (object) {
                console.log("OK -> " + i)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("ERROR")
            }
        });
    }
}

function initSelect2(id) {
    $(`#${id}`).select2();
}

function adjustArrowPosition(tooltip, label) {
    const arrow = tooltip.querySelector('.tooltip-arrow');
    if (!arrow) return;

    // Obtener las dimensiones y posición del label y del tooltip
    const labelRect = label.getBoundingClientRect();
    const tooltipRect = tooltip.getBoundingClientRect();

    // Calcular la posición central del label
    const labelCenter = labelRect.left + (labelRect.width / 2);

    // Calcular la nueva posición de la flecha para que apunte al centro del label
    const newLeft = labelCenter - tooltipRect.left - (arrow.offsetWidth / 2);
    arrow.style.left = `${newLeft}px`;
}


function showFullMessage(div) {
    const label = div.childNodes[0]
    const labelMaxWidth = label.style.maxWidth
    const span = div.childNodes[1]
    if (label.scrollWidth > label.offsetWidth) {
        span.style.visibility = "visible";
        span.style.width = labelMaxWidth
        //adjustArrowPosition(span, label); // Ajusta la posición de la flecha
    } else {
        hideMessageBox(div)
    }
}

function hideMessageBox(div) {
    const span = div.childNodes[1]
    span.style.visibility = "hidden";
}

/**
 * Calcula la cantidad total de medicamento necesaria para un tratamiento.
 * 
 * La función toma la frecuencia y duración del tratamiento desde el DOM,
 * los convierte a horas si es necesario, y calcula la cantidad total de medicamento.
 * 
 * @returns {boolean} False si hay un error o True.
 */
function medicamentoCantidad() {
    try {
        // Obtener valores del DOM
        const frecuencia = parseFloat(document.getElementById('txtFrecuencia').value);
        const tipoFrecuencia = document.getElementById('cmbIdFrecuencia').value;
        const duracionTratamiento = parseFloat(document.getElementById('txtDias').value);
        const tipoDuracionTratamiento = document.getElementById('cmbIdDuracionTratamiento').value;
        const cantidadPorDosis = parseFloat(document.getElementById('txtCant').value); // Asume 2 o cambia según sea necesario

        // Validar que los campos no estén vacíos
        if (!frecuencia || !tipoFrecuencia || !duracionTratamiento || !tipoDuracionTratamiento) {
            console.error('Todos los campos deben estar llenos.');
            return false;
        }

        // Convertir la duración del tratamiento a horas
        let duracionEnHoras = convertirADuracionEnHoras(duracionTratamiento, tipoDuracionTratamiento);

        // Convertir la frecuencia a horas
        let frecuenciaEnHoras = convertirAFrecuenciaEnHoras(frecuencia, tipoFrecuencia);

        // Calcular el total de dosis y la cantidad total de medicamento
        const totalDosis = duracionEnHoras / frecuenciaEnHoras;
        const cantidad = document.getElementById('txtCantidad')
        cantidad.value = totalDosis * cantidadPorDosis
        return true

    } catch (error) {
        console.error('Error en el cálculo: ', error);
        return false;
    }
}

/**
 * Convierte la duración del tratamiento a horas, dependiendo de su tipo.
 * 
 * @param {number} duracion - Duración del tratamiento.
 * @param {string} tipo - Tipo de duración ('HORA', 'DIA', 'MES').
 * @returns {number} Duración en horas.
 */
function convertirADuracionEnHoras(duracion, tipo) {
    switch (tipo) {
        case 'HORA':
            return duracion
        case 'DIA':
            return duracion * 24;
        case 'MES':
            return duracion * 30 * 24; // Considera 30 días por mes
        default:
            throw new Error('Tipo de duración no reconocido.');
    }
}

/**
 * Convierte la frecuencia de dosificación a horas, dependiendo de su tipo.
 * 
 * @param {number} frecuencia - Frecuencia de dosificación.
 * @param {string} tipo - Tipo de frecuencia ('HORA', 'DIA', 'MES').
 * @returns {number} Frecuencia en horas.
 */
function convertirAFrecuenciaEnHoras(frecuencia, tipo) {
    switch (tipo) {
        case 'HORA':
            return frecuencia;
        case 'DIA':
            return frecuencia * 24;
        case 'MES':
            return frecuencia * 30 * 24; // Considera 30 días por mes
        default:
            throw new Error('Tipo de frecuencia no reconocido.');
    }
}


function crearFolioUnicaOpcion() {
    try {
        if (traerTituloFilaSeleccionada('listGrillaPacientes', 'Admision') === "EN ATENCION--"){
            comboCreacionDedocumentos().then(() => {
                if (valorAtributo('cmbTipoDocumento') !== '' ) {
                    console.log('crearFolio ');
                    // verificarNotificacionAntesDeGuardar('crearFolio');
                }
            });

        }
        
    } catch (error) {
        console.log('error: ', error);
        
    }
}


function calcularEdad(fechaNacimiento) {
    var fechaActual = new Date();
    var anios = fechaActual.getFullYear() - fechaNacimiento.getFullYear();
    var meses = fechaActual.getMonth() - fechaNacimiento.getMonth();
    var dias = fechaActual.getDate() - fechaNacimiento.getDate();

    if (meses < 0 || (meses === 0 && dias < 0)) {
        anios--;
        meses += 12;
    }

    if (dias < 0) {
        var ultimoDiaMesAnterior = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), 0).getDate();
        dias += ultimoDiaMesAnterior;
        meses--;
    }

    return {
        anios: anios,
        meses: meses,
        dias: dias
    };
}

function validarEdad(arg) {
    const fecha_decodificada = decodeURIComponent(arg);
    const edad = calcularEdad(new Date(fecha_decodificada)).anios;
    if (edad < 18 && valorAtributo('txtNomAcompanante') === '') {
        alert('FALTA DILIGENCIAR EL NOMBRE DEL ACOMPAÑANTE');
        return false;
    }
    return true;
}