// JavaScript Document

var archivosVarios = '';
var protocolo = window.location.protocol; //https/http
var host = window.location.host;

function eliminarAdjuntosOtros() {
    setTimeout(() => {
        if (verificarCamposGuardar("eliminarArchivoOtros")) {
            var aux = $("#listArchivosAdjuntosVarios").jqGrid('getGridParam', 'selrow')
            var archivo = $("#listArchivosAdjuntosVarios").getRowData(aux).id + ".pdf"
            eliminarArchivo("otros", archivo, function (respuesta) {
                if (respuesta) {
                    modificarCRUD("eliminarArchivoOtros")
                } else {
                    alert("EL ARCHIVO NO PUDO SER ELIMINADO. POR FAVOR INTENTE NUEVAMETE")
                }
            })
        }
    }, 200);
}

function eliminarAdjuntosOtrosHC() {
    setTimeout(() => {
        if (verificarCamposGuardar("eliminarArchivoOtros")) {
            var aux = $("#listArchivosAdjuntosVariosHC").jqGrid('getGridParam', 'selrow')
            var archivo = $("#listArchivosAdjuntosVariosHC").getRowData(aux).id + ".pdf"
            eliminarArchivo("docPDF", archivo, function (respuesta) {
                if (respuesta) {
                    modificarCRUD("eliminarArchivoOtros")
                } else {
                    alert("EL ARCHIVO NO PUDO SER ELIMINADO. POR FAVOR INTENTE NUEVAMETE")
                }
            })
        }
    }, 200);
}

function eliminarArchivo(carpeta, nombre_archivo, callback) {
    $.ajax({
        url: "/clinica/paginas/hc/hc/eliminar_archivo.jsp",
        type: "GET",
        data: { "carpeta": carpeta, "archivo": nombre_archivo },
        beforeSend: function () { },
        success: function (data) {
            callback(data.respuesta)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false)
        }
    });
}

function subirArchivo(ruta_archivo, nombre_archivo, id_elemento_archivo, id_boton_accion, callback) {
    var data = new FormData();
    var aux_boton1 = $("#" + id_boton_accion);
    //var aux_boton = '<input id="btnSubirAdjuntosE" align="center" type="button" title="BT84P." class="small button blue" value="SUBIR DOCUMENTOS" onclick="subirAdjuntosEvento()" />'
    var aux_boton = '<input id="btnSubirAdjuntos" type="button" title="BT84P." class="small button blue" value="SUBIR DOCUMENTOS" onclick="subirAdjuntos()" align="middle">'
    var file = document.getElementById(id_elemento_archivo);

    data.append("ruta_archivo", ruta_archivo);
    data.append("nombre_archivo", nombre_archivo);

    if (file.files && file.files[0]) {
        var extension = file.files[0].name.split(".")[1]
        if (extension.toLowerCase() == "pdf" || extension.toLowerCase() == "mp4" || extension.toLowerCase() == "docx" || extension.toLowerCase() == "doc" || extension.toLowerCase() == "xls" || extension.toLowerCase() == "xlsx" || extension.toLowerCase() == "png" || extension.toLowerCase() == "jpg" || extension.toLowerCase() == "jpeg" || extension.toLowerCase() == "csv") {
            data.append("arhivo", file.files[0])
        } else {
            $("#fileUpc").val(null);
            alert("Por favor seleccione un archivo valido.".toUpperCase())
            $("#loader_aux001").replaceWith(aux_boton);
            return;
        }
    } else {
        alert("No ha seleccionado ningun archivo.".toUpperCase());
        $("#loader_aux001").replaceWith(aux_boton);
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/clinica/paginas/hc/hc/subir_archivo.jsp', true);
    xhr.send(data);
    $("#" + id_boton_accion).replaceWith('<img id="loader_aux001" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">');
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            setTimeout(() => {
                $("#loader_aux001").replaceWith(aux_boton1);
            }, 500);
            if (this.status == 200) {
                var jsonResponse = JSON.parse(this.responseText);
                callback(jsonResponse.respuesta);
            } else {
                callback(false);
            }
        }
    };
}

function subirArchivoOrderProgramacion() {
    var fila_seleccionada = $("#listGrillaApoyoDiagnotico").jqGrid('getGridParam', 'selrow');
    var id_programacion = $("#listGrillaApoyoDiagnotico").getRowData(fila_seleccionada).ID

    var ruta_archivo = "/opt/tomcat8/webapps/docPDF/ordenProgramacion"
    var nombre_archivo = id_programacion + ".pdf"

    subirArchivo(ruta_archivo, nombre_archivo, "fileUpc", "btnSubirArchivoOrdenProgramacion", function (respuesta) {
        if (respuesta) {
            modificarCRUDArchivo("registrarArchivoOrderProgramacion")
        } else {
            alert("Error al subir archivo.")
        }
    });
}

function subirArchivoEvento() {
    var nomJas = document.getElementById("fileUpc").value;
    var ruta = "";
    alert(nomJas.substring(nomJas.length - 4, nomJas.length));
    if (nomJas.substring(nomJas.length - 4, nomJas.length) == 'DOCX' || nomJas.substring(nomJas.length - 4, nomJas.length) == 'docx' ||
        nomJas.substring(nomJas.length - 4, nomJas.length) == 'JEPG' || nomJas.substring(nomJas.length - 4, nomJas.length) == 'jepg' ||
        nomJas.substring(nomJas.length - 4, nomJas.length) == 'XLSX' || nomJas.substring(nomJas.length - 4, nomJas.length) == 'xlsx') {
        extension = "." + nomJas.substring(nomJas.length - 4, nomJas.length)
        nombre_archivo = nomJas.substring(12, nomJas.length - 5);
    }
    else {
        extension = "." + nomJas.substring(nomJas.length - 3, nomJas.length)
        nombre_archivo = nomJas.substring(12, nomJas.length - 4);
    }

    var fecha = new Date;
    var dia = fecha.getDate();
    var mes = fecha.getMonth();
    var anio = fecha.getFullYear();

    mes = mes + 1;

    ruta_archivo = "/opt/tomcat8/webapps/docPDF/evento/" + anio + "/" + mes + "/" + dia;

    ruta_crud = "/docPDF/evento/" + anio + "/" + mes + "/" + dia;

    subirArchivo(ruta_archivo, nombre_archivo + extension, "fileUpc", "btnEvento", function (respuesta) {
        if (respuesta) {
            modificarCRUDArchivo("registrarArchivoEvento", nombre_archivo, ruta_crud, extension)
        } else {
            alert("Error al subir archivo.")
        }
    });
}

function subirAdjuntos() {
    var tipo_archivo = valorAtributo("cmbIdTipoArchivosVarios")
    var observacion = decodeURI(document.getElementById("txtObservacionAdjuntosVarios").value)
    if (ventanaActual.opc == 'archivosAdjuntos') {
        var id_paciente = valorAtributoIdAutoCompletar('txtIdBusPaciente')
    } else {
        var id_paciente = valorAtributo("lblPacienteAdjuntos").split("-")[0]
    }
    var id_cita = valorAtributo("lblIdCitaAdjuntos") == "" ? -1 : valorAtributo("lblIdCitaAdjuntos")
    var id_admision = valorAtributo("lblIdAdmisionAdjuntos") == "" ? -1 : valorAtributo("lblIdAdmisionAdjuntos")
    var id_factura = valorAtributo("lblIdFacturaAdjuntos") == "" ? -1 : valorAtributo("lblIdFacturaAdjuntos")
    var id_evolucion = valorAtributo("lblIdEvolucionAdjuntos") == "" ? -1 : valorAtributo("lblIdEvolucionAdjuntos")
    var usuario = IdSesion()
    var bean_adjunto = Math.trunc(Math.random() * 1000000)
    var file = document.getElementById("fileUpc");


    if (id_paciente == "") {
        alert("FALTA SELECCIONAR UN PACIENTE")
        return;
    }

    if (tipo_archivo == "") {
        alert("FALTA SELECCIONAR EL TIPO DE ARCHIVO")
        return;
    }

    if (file.files && file.files[0]) {
        var aux = file.files[0].name.split(".")
        var extension = aux[aux.length - 1]
        if (extension.toLowerCase() != "pdf") {
            $("#fileUpc").val(null);
            alert("Por favor seleccione un archivo pdf.")
            return;
        }
    } else {
        alert("No ha seleccionado ningún archivo.");
        return;
    }

    if (ventanaActual.opc == "principalOrdenes" || ventanaActual.opc == "principalHC") {
        if (id_evolucion == "") {
            alert("FALTA SELECCIONAR EL FOLIO")
            return;
        }
    }

    valores_a_mandar = ""
    add_valores_a_mandar(id_paciente)
    add_valores_a_mandar(tipo_archivo)
    add_valores_a_mandar(encodeURI(observacion))
    add_valores_a_mandar(id_cita)
    add_valores_a_mandar(id_admision)
    add_valores_a_mandar(id_factura)
    add_valores_a_mandar(id_evolucion)
    add_valores_a_mandar(usuario)
    add_valores_a_mandar(bean_adjunto)

    var aux_boton = $("#btnSubirAdjuntos");
    $("#btnSubirAdjuntos").replaceWith('<img id="loader_aux001" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">');

    $.ajax({
        url: "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp",
        type: "POST",
        data: { "accion": "crearRegistroAdjuntoOtros", "idQuery": 1052, "parametros": valores_a_mandar },
        beforeSend: function () { },
        success: function (data) {
            var respuesta = data.getElementsByTagName('respuesta');
            if (respuesta[0].firstChild.data) {
                valores_a_mandar = ""
                add_valores_a_mandar(bean_adjunto)
                //CONSULTA ID NUEVO ARCHIVO
                $.ajax({
                    url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                    type: "POST",
                    data: {
                        "idQuery": 1053,
                        "parametros": valores_a_mandar,
                        "_search": false,
                        "nd": 1611873687449,
                        "rows": -1,
                        "page": 1,
                        "sidx": "NO FACT",
                        "sord": "asc"
                    },
                    beforeSend: function () { },
                    success: function (data) {
                        var respuesta = data.getElementsByTagName('cell');
                        var ruta_archivo = "/opt/tomcat8/webapps/docPDF/otros" // cambiar tomcat_daniel a tomcat8
                        var nombre_archivo = respuesta[1].firstChild.data + ".pdf"
                        id_nuevo_archivo_otros = respuesta[1].firstChild.data

                        subirArchivo(ruta_archivo, nombre_archivo, "fileUpc", "0", function (respuesta) {
                            $("#loader_aux001").replaceWith(aux_boton);
                            if (respuesta) {
                                modificarCRUDArchivo("registrarArchivoOtros")
                            } else {
                                alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
                            }
                        });

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
                        $("#loader_aux001").replaceWith(aux_boton);
                    }
                });
            } else {
                alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
                $("#loader_aux001").replaceWith(aux_boton);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
            $("#loader_aux001").replaceWith(aux_boton);
        }
    });
}

function subirAdjuntosEvento() {
    // var tipo_archivo = valorAtributo("cmbIdTipoArchivosVarios")
    var observacion = valorAtributo("txtObservacionAdjuntosVarios")
    var id_ticket = valorAtributo("lblIdEventoARelacio").split("-")[0]

    var usuario = IdSesion()
    var bean_adjunto = Math.trunc(Math.random() * 1000000)


    if (id_ticket == "") {
        alert("FALTA SELECCIONAR TICKET")
        return;
    }

    valores_a_mandar = "accion=crearRegistroAdjuntoEvento&idQuery=2278&parametros="

    add_valores_a_mandar(id_ticket)
    add_valores_a_mandar(decodeURI(observacion))
    add_valores_a_mandar(usuario)
    add_valores_a_mandar(bean_adjunto)

    var aux_boton = '<input id="btnSubirAdjuntosE" align="center" type="button" title="BT84P." class="small button blue" value="SUBIR DOCUMENTOS" onclick="subirAdjuntosEvento()" />';
    $("#btnSubirAdjuntosE").replaceWith('<img id="loader_aux001" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">');

    $.ajax({
        url: "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp",
        type: "POST",
        data: valores_a_mandar,
        beforeSend: function () { },
        success: function (data) {
            var respuesta = data.getElementsByTagName('respuesta');
            if (respuesta[0].firstChild.data) {
                valores_a_mandar = ""
                add_valores_a_mandar(bean_adjunto)
                //CONSULTA ID NUEVO ARCHIVO
                $.ajax({
                    url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                    type: "GET",
                    data: {
                        "idQuery": 2279,
                        "parametros": valores_a_mandar,
                        "rows": -1,
                        "page": 1,
                        "sord": "asc"
                    },
                    beforeSend: function () { },
                    success: function (data) {
                        var nomJas = document.getElementById("fileUpc").value;
                        extension = (/[.]/.exec(nomJas)) ? /[^.]+$/.exec(nomJas)[0] : undefined;
                        nombre_archivo = nomJas.split('.' + extension)[0].replace(/^.*[\\\/]/, '')
                        extension = '.' + extension;
                        var ruta = "";

                        /* if (nomJas.substring(nomJas.length - 4, nomJas.length) == '.DOCX' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.docx' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.DOC' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.doc' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.JPEG' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.jpeg' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.PNG' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.png' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.XLSX' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.xlsx' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.PDF' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.pdf' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.CSV' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.csv' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.JPG' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.jpg' ||
                            nomJas.substring(nomJas.length - 4, nomJas.length) == '.XLS' || nomJas.substring(nomJas.length - 4, nomJas.length) == '.xls') {
                            extension = nomJas.substring(nomJas.length - 4, nomJas.length)
                            nombre_archivo = nomJas.substring(12, nomJas.length - 4);
                        }
                        else {
                            extension = "." + nomJas.substring(nomJas.length - 3, nomJas.length)
                            nombre_archivo = nomJas.substring(12, nomJas.length - 4);
                        } */



                        var respuesta = data.getElementsByTagName('cell');
                        var ruta_archivo = "/opt/tomcat8/webapps/docPDF/evento"
                        /* var ruta_archivo = "/opt/tomcat_nicolas/webapps/docPDF/evento" */
                        var nombre_archivo = respuesta[1].firstChild.data + extension
                        id_nuevo_archivo_evento = respuesta[1].firstChild.data
                        ruta_crud = "/docPDF/evento"

                        subirArchivo(ruta_archivo, nombre_archivo, "fileUpc", "0", function (respuesta) {
                            $("#loader_aux001").replaceWith(aux_boton);
                            if (respuesta) {
                                modificarCRUDArchivo("registrarArchivoHelp", nombre_archivo, ruta_crud, extension)
                            } else {
                                alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
                            }
                        });

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
                        $("#loader_aux001").replaceWith(aux_boton);
                    }
                });
            } else {
                alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
                $("#loader_aux001").replaceWith(aux_boton);
            }
        },
        complete: function () {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
            $("#loader_aux001").replaceWith(aux_boton);
        }
    });
}


function modificarCRUDArchivo(arg, nombre_archivo, ruta_archivo, extension) {

    pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
        switch (arg) {
            case 'registrarArchivoOrderProgramacion':
                var fila_seleccionada = $("#listGrillaApoyoDiagnotico").jqGrid('getGridParam', 'selrow');
                var id_programacion = $("#listGrillaApoyoDiagnotico").getRowData(fila_seleccionada).ID
                var id_paciente = $("#listGrillaApoyoDiagnotico").getRowData(fila_seleccionada).ID_PACIENTE

                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1042&parametros=";
                add_valores_a_mandar(id_programacion);
                add_valores_a_mandar(id_paciente);
                add_valores_a_mandar(valorAtributo("txtObservacionAdjunto"));
                add_valores_a_mandar(LoginSesion());

                add_valores_a_mandar(id_programacion);
                ajaxModificar();
                break;

            case 'registrarArchivoEvento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1103&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdEventoARelacio'));
                add_valores_a_mandar(valorAtributo('txtObservacionAdjunto'));
                add_valores_a_mandar(nombre_archivo);
                add_valores_a_mandar(extension);
                add_valores_a_mandar(ruta_archivo);
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
                break;

            case 'registrarArchivoOtros':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1054&parametros=";
                add_valores_a_mandar(id_nuevo_archivo_otros);
                ajaxModificar();
                break;

            case 'registrarArchivoHelp':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2280&parametros=";

                add_valores_a_mandar(nombre_archivo);
                add_valores_a_mandar(extension);
                add_valores_a_mandar(ruta_archivo);
                add_valores_a_mandar(id_nuevo_archivo_evento);

                ajaxModificar();
                break;
        }
    }
}


function respuestaCRUDArchivo(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
        switch (arg) {
            case 'registrarArchivoOrderProgramacion':
                buscarHistoria('listLaboratoriosPdf')
                setTimeout(() => {
                    buscarHC('listGrillaApoyoDiagnotico')
                }, 500);
                $("#fileUpc").val(null);
                limpiaAtributo("txtObservacionAdjunto", 0)

                var fila_seleccionada = $("#listGrillaApoyoDiagnotico").jqGrid('getGridParam', 'selrow');
                var id_programacion = $("#listGrillaApoyoDiagnotico").getRowData(fila_seleccionada).ID
                var configuracion_ventana = "width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230";

                window.open(protocolo + "//" + host + "/docPDF/ordenProgramacion/" + id_programacion + ".pdf", "", configuracion_ventana);
                break;

            case 'registrarArchivoEvento':
                buscarHistoria('listEventoPDF')
                var nomJas = document.getElementById("fileUpc").value;
                if (nomJas.substring(nomJas.length - 4, nomJas.length) == 'DOCX' || nomJas.substring(nomJas.length - 4, nomJas.length) == 'docx' ||
                    nomJas.substring(nomJas.length - 4, nomJas.length) == 'JEPG' || nomJas.substring(nomJas.length - 4, nomJas.length) == 'jepg' ||
                    nomJas.substring(nomJas.length - 4, nomJas.length) == 'XLSX' || nomJas.substring(nomJas.length - 4, nomJas.length) == 'xlsx') {
                    extension = "." + nomJas.substring(nomJas.length - 4, nomJas.length)
                    nombre_archivo = nomJas.substring(12, nomJas.length - 5);
                }
                else {
                    extension = "." + nomJas.substring(nomJas.length - 3, nomJas.length)
                    nombre_archivo = nomJas.substring(12, nomJas.length - 4);
                }

                $("#fileUpc").val(null);
                var fecha = new Date;
                var dia = fecha.getDate();
                var mes = fecha.getMonth();
                var anio = fecha.getFullYear();

                mes = mes + 1;
                limpiaAtributo("txtObservacionAdjunto", 0)
                var configuracion_ventana = "width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230";
                window.open(protocolo + "//" + host + "/docPDF/evento/" + anio + "/" + mes + "/" + dia + "/" + nombre_archivo + extension, "", configuracion_ventana)
                break;

            case 'registrarArchivoOtros':
       
             try {
                document.getElementById('txtObservacionAdjuntosVarios').value = ''
                document.getElementById('fileUpc').value = ''
                document.getElementById('cmbIdTipoArchivosVarios').value = ''
             } catch (error) {
                
             }
                Swal.fire({
                    html: 'DOCUMENTO CARGADO EXITOSAMENTE',
                    icon: "success",
                    cancelButtonText: "CANCELAR",
                    showCancelButton: "true",
                    confirmButtonText: "VER DOCUMENTO",
                    confirmButtonColor: '#3085D6',
                    cancelButtonColor: '#999999',
                  /*   position: "top", */
                }).then(function (sweetAlertResult) {
                    buscarHistoria('listArchivosAdjuntosVarios')
                    if (sweetAlertResult.isConfirmed) {
                        
                        var configuracion_ventana = "width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230";
                        window.open(protocolo + "//" + host + "/docPDF/otros/" + id_nuevo_archivo_otros + ".pdf", "", configuracion_ventana);
                    } else {
                        
                       return
                    }
                });
              
                break;

            case 'registrarArchivoHelp':
                alert('ARCHIVO AGREGADO DE MANERA EXITOSA');
                var nomJas = document.getElementById("fileUpc").value;
                extension = (/[.]/.exec(nomJas)) ? /[^.]+$/.exec(nomJas)[0] : undefined;
                nombre_archivo = nomJas.split('.' + extension)[0].replace(/^.*[\\\/]/, '')
                extension = '.' + extension;
                $('#fileUpc').val('');
                limpiaAtributo('txtObservacionAdjuntosVarios', 0);

                var configuracion_ventana = "width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230";
                window.open(protocolo + "//" + host + "/docPDF/evento/" + id_nuevo_archivo_evento + extension, "", configuracion_ventana);

                // Correcion archivos adjuntos
                valores_a_mandar = '';
                pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
                paginaActual = "crearRegistroAdjuntoEvento";
                valores_a_mandar = "accion=" + paginaActual;
                valores_a_mandar = valores_a_mandar + "&idQuery=2669&parametros="; //POR DEFINIR 
                add_valores_a_mandar(valorAtributo("lblIdEventoARelacio"));
                ajaxModificar();
                break;
        }
    }
}

function comprobarExcelFacturacion() {
    if (verificarCamposGuardar('subirExcelFacturacion')) {
        archivosVarios = "excelFacturacion";
        var nomJas = document.getElementById("fileExcel").value;
        if (nomJas.substring(nomJas.length - 4, nomJas.length) == "xlsx"
            || nomJas.substring(nomJas.length - 3, nomJas.length) == "xls") {

            comprobarSiElCandadoEstaAbierto('txt', 554);

        } else {
            alert("El archivo a enviar no es EXCEL o el campo esta vacio");
            document.getElementById("fileExcel").focus();
        }
    }
}

function subirExcelFacturacion() {
    let rutaTarifasExcel = "/opt/tomcat8/webapps/docPDF/excel";
    let nombreArchivoExcel = "excel.xlsx";
    let idPlan = $("#txtIdPlan").val();
    let usuarioCrea = LoginSesion();

    subirArchivo(rutaTarifasExcel, nombreArchivoExcel, "fileExcel", "subirTarifas", function (res) {
        if (res) {
            //leerExcel Tarifas
            $.ajax({
                url: "/clinica/paginas/accionesXml/LeerExcelTarifas.jsp",
                type: "POST",
                data: {
                    "ruta": rutaTarifasExcel,
                    "nombreArchivo": nombreArchivoExcel,
                    "idPlan": idPlan,
                    "Usuario": usuarioCrea,
                },
                success: function (mensajeJSN) {
                    if (mensajeJSN.respuesta) alert(mensajeJSN.respuesta);
                    $('#listGrillaPlanesDetalle').trigger('reloadGrid');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE ENLAZAR LOS PROCEDIMIENTOS, SI EL PROBLEMA PERSISTE POR FAVOR COMUNICARSE CON SOPORTE");
                }
            });

        } else {
            alert("SE HA PRESENTADO UN PROBLEMA AL MOMENTO DE SUBIR EL ARCHIVO. POR FAVOR INTENTE NUEVAMENTE.")
        }
    });
}

function comprobarYSubirArchivos(opcionBoton) {
    archivosVarios = opcionBoton
    var nomJas = document.getElementById("fileUpc").value;
    if (nomJas.substring(nomJas.length - 3, nomJas.length) == "pdf") { // si el archivo es de extencion pdf
        if (!valorAtributo('cmbTipoArchivoSubir') == 'ayudaDx' || !valorAtributo('cmbTipoArchivoSubir') == 'orden_programacion')
            comprobarSiElCandadoEstaAbierto('txt', 542)
        else
            comprobarSiElCandadoEstaAbierto('txt', 554)
    } else {
        alert("El archivo a enviar no es PDF o el campo está vacio");
        document.getElementById("fileUpc").focus();
    }
}


function comprobarYSubirArchivosS(opcionBoton) {
    archivosVarios = opcionBoton
    var nomJas = document.getElementById("fileUpcs").value;
    if (nomJas.substring(nomJas.length - 3, nomJas.length) == "pdf") { // si el archivo es de extencion pdf
        if (!valorAtributo('cmbTipoArchivoSubir') == 'ayudaDx' || !valorAtributo('cmbTipoArchivoSubir') == 'orden_programacion')
            comprobarSiElCandadoEstaAbierto('txt', 542)
        else
            comprobarSiElCandadoEstaAbierto('txt', 554)
    } else {
        alert("El archivo a enviar no es PDF o el campo está vacio");
        document.getElementById("fileUpcs").focus();
    }
}

function comprobarSiElCandadoEstaAbierto(elementoDestino, idQueryCombo) {   //alert('idQueryCombo='+idQueryCombo)
    idcombo = elementoDestino;
    porDefecto = '';
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=0";
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = respuestacomprobarSiElCandadoEstaAbierto;
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacomprobarSiElCandadoEstaAbierto() {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            c = raiz.getElementsByTagName('id');
            n = raiz.getElementsByTagName('nom');
            d = raiz.getElementsByTagName('title');
            if (c.length > 0) {
                if (c[0].firstChild.data == 'CERRADO') {
                    alert('4.UN USUARIO ESTA SUBIENDO ARCHIVO, POR FAVOR ESPERE E INTENTE NUEVAMENTE')

                } else {
                    if (archivosVarios == 'no_varios') {
                        subirArchivoAdjunto()
                    }
                    else if (archivosVarios === "excelFacturacion") {
                        subirExcelFacturacion()
                    } else if (archivosVarios === "orden_programacion") {
                        subirArchivoApoyoDiagnostico();
                    }
                    else if (archivosVarios == "anexos") {
                        subirArchivoAdjuntosAnexos()
                    }
                    else {
                        subirArchivoAdjuntosVarios()
                    }
                }
            } else {
                alert('Problema al cargar respuestacargarElementoGRAL..')
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}
/*función para subir archivos varios al sistema*/

function subirArchivoAdjuntosVarios() {

    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));

    if (valorAtributo('cmbIdTipoArchivosVarios') != '') {
        var ruta = "";
        ruta = "evento=crear" +
            "&subCarpeta=otros&usuario_crea=" + LoginSesion() +
            "&id_paciente=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] +
            "&tipo_id=" + valorAtributo("lblPacienteAdjuntos").split("-")[1] +
            "&identificacion=" + valorAtributo("lblPacienteAdjuntos").split("-")[2] +
            "&id_cita=" + valorAtributo("lblIdCitaAdjuntos") +
            "&id_admision=" + valorAtributo("lblIdAdmisionAdjuntos") +
            "&id_evolucion=" + valorAtributo("lblIdEvolucionAdjuntos");
        ruta = ruta + "&nombreArchivo=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + valorAtributo('cmbIdTipoArchivosVarios') + "&var1=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + "&var2=" + valorAtributo('cmbIdTipoArchivosVarios') + "&var3=" + valorAtributo('txtObservacionAdjuntosVarios') + '&archivoTamano=' + archivoTamano;

        document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        document.forms['formUpc'].submit();

    }
}

function subirArchivoAdjuntosAnexos() {

    var fileSize = $('#fileUpcs')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));

    console.log(archivoTamano)

    if (valorAtributo('cmbIdTipoArchivosAnexo') != '') {
        var ruta = "";
        ruta = "evento=crear" +
            "&subCarpeta=anexos&usuario_crea=" + LoginSesion() +
            "&id_paciente=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] +
            "&id_evolucion=" + valorAtributo("lblIdEvolucionAdjuntos");
        ruta = ruta + "&nombreArchivo=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + valorAtributo('cmbIdTipoArchivosAnexo') + "&var1=" + valorAtributo("lblPacienteAdjuntos").split("-")[0] + "&var2=" + valorAtributo('cmbIdTipoArchivosAnexo') + "&var3=" + valorAtributo('txtObservacionAdjuntosVarios') + '&archivoTamano=' + archivoTamano;

        document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        document.forms['formUpc'].submit();
    }
    else {
        alert('SELECCIONE UN TIPO DE ANEXO');
    }
}

function subirArchivoAdjuntosFirmas() {
    alert(9999)

    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));

    alert('archivoTamano::' + archivoTamano)


    if (valorAtributo('cmbIdTipoArchivosVarios') != '') {
        var nomJas = document.getElementById("fileUpc").value;
        var ruta = "";

        ruta = "evento=crear" + "&subCarpeta=otros&usuario_crea=" + LoginSesion();
        ruta = ruta + "&nombreArchivo=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + valorAtributo('cmbIdTipoArchivosVarios') + "&var1=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + "&var2=" + valorAtributo('cmbIdTipoArchivosVarios') + "&var3=" + valorAtributo('txtObservacionAdjuntosVarios') + '&archivoTamano=' + archivoTamano;
        var comboOtros = document.getElementById("cmbIdTipoArchivosVarios");
        var opcionSelect = comboOtros.options[comboOtros.selectedIndex].text;


        document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        document.forms['formUpc'].submit();
    }

}


/*fin de la función otros archivos adjuntos*/

function subirArchivoAdjunto() {  // para  enviar archivo  UPC

    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));
    var nomJas = document.getElementById("fileUpc").value;
    var ruta = "";
    ruta = "evento=crear" + "&subCarpeta=" + valorAtributo('cmbTipoArchivoSubir') + '&usuario_crea=' + LoginSesion() + '&archivoTamano=' + archivoTamano;

    switch (valorAtributo('cmbTipoArchivoSubir')) {
        case 'fisico':
            if (valorAtributo('txtIdBusPaciente') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + "&var1=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            } else {
                ruta = "";
                alert('SELECCIONE PACIENTE')
            }
            break;
        case 'identificacion':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributo('cmbTipoId') + valorAtributo('txtIdentificacion') + "&var1=" + valorAtributoIdAutoCompletar('txtIdBusPaciente') + "&var2=" + valorAtributo('cmbTipoId') + "&var3=" + valorAtributo('txtIdentificacion');
            } else {
                ruta = "";
                alert('SELECCIONE PACIENTE')
            }
            break;
        case 'remision':
            if (valorAtributo('lblIdAgendaDetalle') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributo('lblIdAgendaDetalle') + "&var1=" + valorAtributo('lblIdAgendaDetalle');
                console.log(ruta)
            } else {
                ruta = "";
                alert('SELECCIONE CITA')
            }
            break;
        case 'carnet':
            if (valorAtributoIdAutoCompletar('txtIdBusPaciente') != '') {
                ruta = ruta + "&nombreArchivo=" + valorAtributo('lblIdAgendaDetalle') + "&var1=" + valorAtributo('lblIdAgendaDetalle') + "&var2=" + valorAtributoIdAutoCompletar('txtIdBusPaciente');
            } else {
                ruta = "";
                alert('SELECCIONE PACIENTE')
            }
            break;
        case 'ayudaDx':
            if (valorAtributo('txtIdEsdadoDocumento') == 1) {
                ruta = "";
                alert('EL ESTADO DEL DOCUMENTO NO PERMITE ADJUNTAR .1')
            } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
                ruta = "";
                alert('EL ESTADO DEL DOCUMENTO NO PERMITE ADJUNTAR .8')
            } else {
                if (valorAtributo('txtIdProfesionalElaboro') == IdSesion()) {

                    if (valorAtributo('lblIdDocumento') != '') {
                        ruta = ruta + "&nombreArchivo=" + valorAtributo('lblIdDocumento') + valorAtributo('cmbIdSitioAdjunto') + "&var1=" + valorAtributo('lblIdDocumento') + "&var2=" + valorAtributo('lblIdPaciente') + "&var3=" + valorAtributo('txtObservacionAdjunto') + "&var4=" + valorAtributo('cmbIdSitioAdjunto');
                    } else {
                        ruta = "";
                        alert('SELECCIONE FOLIO')
                    }
                } else {
                    ruta = "";
                    alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA ELIMINAR')
                }
            }
            break;
    }
    if (ruta != '') {

        if (valorAtributo('cmbTipoArchivoSubir') == 'ayudaDx') {
            document.forms['formUpc'].action = '/clinica/servlet/UPC2?' + ruta;
        } else {
            document.forms['formUpc'].action = '/clinica/servlet/UPC?' + ruta;
        }
        console.log("ruta: " + ruta);
        document.forms['formUpc'].submit();
    }
}


function subirArchivoApoyoDiagnostico() {  // para  enviar archivo  UPC
    var fileSize = $('#fileUpc')[0].files[0].size;
    var archivoTamano = Math.round(parseFloat(fileSize / 1024).toFixed(2));
    var nomJas = document.getElementById("fileUpc").value;
    var ruta = "";
    ruta = "evento=crear" + "&subCarpeta=" + 'ordenProgramacion' + '&usuario_crea=' + LoginSesion() + '&archivoTamano=' + archivoTamano;

    ruta = ruta + "&nombreArchivo=" + valorAtributo('txtIdPlanEvolucion') + "&var1=" + valorAtributo('txtIdBusPaciente') + "&var2=" + valorAtributo('txtObservacionAdjunto');

    document.forms['formUpc'].action = '/clinica/servlet/UPC2?' + ruta;

    console.log("ruta: " + ruta);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/clinica/servlet/UPC2?' + ruta);
    xhr.onload = function (event) {
        var respuesta = new String(event.target.response);
        respuesta = respuesta.replace('<SCRIPT language=javascript>', '');
        respuesta = respuesta.replace('</SCRIPT>', '');
        console.log(respuesta);
        eval(respuesta)
        if (respuesta.includes("window.open")) {
            modificarCRUD('actualizarOrdenProgramacion')
        }
    };
    var formData = new FormData(document.getElementById("formUpc"));
    xhr.send(formData);
}




function eliminarArchivoAdjunto(arg) {  // para  eliminar archivo  UPC
    if (confirm('Seguro de Eliminar archivo? ')) {
        switch (tabActivo) {
            case 'divArchivosAdjuntos':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=elimina' + '&subCarpeta=fisico';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;
            case 'divListadoIdentificacion':
                //if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=elimina' + '&subCarpeta=identificacion';
                document.forms['formUpcElimina'].submit();
                //} else alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;
            case 'divListadoRemision':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=remision';
                    document.forms['formUpcElimina'].submit();
                } else
                    alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR');
                break;
            case 'divListadoCarnet':
                if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=carnet';
                    document.forms['formUpcElimina'].submit();
                } else
                    alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;
            case 'divListadoAyudasDiagnosticas':
                if (valorAtributo('txtIdEsdadoDocumento') == 1) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.1')
                } else if (valorAtributo('txtIdEsdadoDocumento') == 8) {
                    alert('EL ESTADO DEL DOCUMENTO NO PERMITE ELIMINAR.8')
                } else {
                    //if(valorAtributo('txtIdProfesionalElaboro')==IdSesion()){ 
                    if (valorAtributo('lblUsuarioElaboro') == LoginSesion()) {
                        document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=ayudaDx';
                        document.forms['formUpcElimina'].submit();

                    } else
                        alert('USTED NO ES EL AUTOR DE ESTE FOLIO \n NO PODRA ELIMINAR')

                }
                break;
            case 'divListadoOtrosDocumentos':
                //if (valorAtributo('lblUsuarioElaboro') == IdSesion()) {
                if (true) {
                    document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=elimina' + '&subCarpeta=otros';
                    document.forms['formUpcElimina'].submit();
                } else
                    alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;
            case 'divListadoDocumentosEvento':
                //if (valorAtributo('lblUsuarioElaboro') == IdSesion()) {
                if (true) {
                    if (arg == 'eliminarArchivoAdjunto') {
                        modificarCRUD('eliminarArchivoAdjunto');
                    } else {
                        document.forms['formUpcElimina'].action = "/clinica/servlet/UPC?nombreArchivo=" + valorAtributo('lblNombreElemento') + "&var1=" + valorAtributo('lblId') + '&evento=elimina' + '&subCarpeta=otros';
                        document.forms['formUpcElimina'].submit();
                    }
                } else
                    alert('USTED NO ES EL AUTOR DE ESTE ARCHIVO\nNO PODRA ELIMINAR')
                break;

            case 'divAyudasDiagnosticas':
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=ayudaDx';
                document.forms['formUpcElimina'].submit();
                break;

            case 'divSubirArchivo':
                document.forms['formUpcElimina'].action = "/clinica/servlet/UPC2?nombreArchivo=" + valorAtributo('lblNombreElemento') + '&evento=elimina' + '&subCarpeta=ordenProgramacion';
                document.forms['formUpcElimina'].submit();
                break;
        }
    }

}

function cerrarVentanitaArchivosAdjuntos() {
    ocultar('divVentanitaArchivosAdjuntos');
    switch (tabActivo) {
        case 'divArchivosAdjuntos':
            buscarHistoria('listArchivosAdjuntos')
            break;
        case 'divListadoIdentificacion':
            buscarHistoria('listArchivosAdjuntosIdentificacion')
            break;
        case 'divListadoRemision':
            buscarHistoria('listArchivosAdjuntosRemision')
            break;
        case 'divListadoCarnet':
            buscarHistoria('listArchivosAdjuntosCarnet')
            break;
        case 'divListadoAyudasDiagnosticas':
            buscarHistoria('listAyudasDiagnost')
            break;
        case 'divListadoOtrosDocumentos':
            buscarHistoria('listArchivosAdjuntosVarios')
            break;
        case 'divListadoDocumentosEvento':
            buscarHistoria('listArchivosAdjuntosEvento')
            break;

    }
}




function dibujarCanvas_() {

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext("2d");
    var textarea = document.getElementById('code');
    var reset = document.getElementById('reset');
    var edit = document.getElementById('edit');
    var code = textarea.value;

    ctx.clearRect(0, 0, 300, 100);
    ctx.fillStyle = 'blue';

    eval(textarea.value);
}


function dibujarCanvas() {
    alert('0000')
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    // Dibujamos algo sencillo en el Canvas para exportarlo
    ctx.fillStyle = "rgb(255,0,0)";
    ctx.fillRect(5, 5, 50, 50);

    ctx.fillStyle = "rgb(0,255,0)";
    ctx.fillRect(8, 8, 50, 50);

}

function pasarImg() { //alert(7777)
    var img = document.getElementById("laimagen");
    //  var png = document.getElementById("png");
    /*	  png.addEventListener("click",function(){	
              img.src = canvas.toDataURL("image/png");	
          },false);	*/
    img.src = canvas.toDataURL("image/png");
}


function ponerImagen() {
    //constants
    var MAX_WIDHT = 200,
        MAX_HEIGHT = 200;
    var URL = window.webkitURL || window.URL;
    var inputFile = document.getElementById('fileUpc');
    alert(URL)
    alert(inputFile)
}

function cambiar() {
    var URL = window.webkitURL || window.URL;
    //      var file = event.target.files[0];

    //elements
    var canvas = document.getElementById('preview'),
        ctx = canvas.getContext('2d'),
        url = URL.createObjectURL(file);

    var img = new Image();
}

function loadin() {

    var width = img.width,
        height = img.height;

    //resize
    if (width > height) {
        if (width > MAX_WIDHT) {
            height *= MAX_WIDHT / width;
            width = MAX_WIDHT;
        }
        else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }
    }

    canvas.width = width;
    canvas.height = height;

    ctx.drawImage(img, 0, 0, width, height);

    //Form data (POST) 

    console.log(file);

    //name
    var imageName = id('imageName');
    imageName.value = file.name;

    //contentType
    var contentType = id('contentType');
    contentType.value = file.type;

    //image data
    var imageData = id('imageData'),
        dataUrl = id('preview').toDataURL('image/png').replace('data:image/png;base64,', '');

    imageData.value = dataUrl;
    img.src = url;

}




var wrapper = document.getElementById("signature-pad");
/*
var canvas = wrapper.querySelector("canvas");
var signaturePad = new SignaturePad(canvas, {
  backgroundColor: 'rgb(255, 255, 255)'
});
function probando()
{
    alert('hola')
    if (signaturePad.isEmpty()) {
    alert("Please provide a signature first.");
  } else {
    //var dataURL = signaturePad.toDataURL();
    var img = document.getElementById("imgCanvas");
    var png = document.getElementById("png");
    img.src = canvas.toDataURL("image/png");

    }
 }

function enviarImg()
{
    window.open("http://181.48.155.146:8383/clinica/paginas/hc/prueba.jsp");
}

function dibujarCanvas2() {
    alert('0000')
    var canvas = document.getElementById("miicanvas");
    var ctx = canvas.getContext("2d");
    // Dibujamos algo sencillo en el Canvas para exportarlo
    ctx.fillStyle = "rgb(255,0,0)";
    ctx.fillRect(5,5,50,50);

    ctx.fillStyle = "rgb(0,255,0)";
    ctx.fillRect(8,8,50,50);

}

var canvasC = document.getElementById("miicanvas");
var w = window.innerWidth;
var h = window.innerHeight;

canvasC.width = w;
canvasC.height = h/2.5;

var signaturePad = new SignaturePad(canvasC,{
    dotSize: 1
});
function subirCanvas()
{
    alert('subir')
       var imageURI = signaturePad.toDataURL();
    document.getElementById("preview").src = imageURI;
}

function subirCanvasAimg()
{
    alert('mover')
   signaturePad.clear();
}




var canvas3 = document.getElementById("miicanvas");
var ctx = canvas3.getContext("2d");
ctx.fillStyle = "#FF0000";
ctx.fillRect(50, 25, 100, 50);
$( '#imagen' ).hide();

function guardar() {
    alert(111)
    var imagen = canvas3.toDataURL("image/png");
    /* Envío la petición XHR al servidor con los datos de la imagen
    $.ajax({
        url: "<?= $_SERVER['PHP_SELF'] ?>",
        method: 'post',
        data: { imagen: imagen},
    }).done(function(retorno) {
        alert(retorno);
        $( '#imagen' ).show();
    });
}

*/