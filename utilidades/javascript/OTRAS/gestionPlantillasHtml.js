class GestionPlantillasHtml {
  constructor(idFolio, tipoFolio, estadoFolio, elementosPlantillaHtml) {
    this.idFolio = idFolio
    this.tipoFolio = tipoFolio
    this.estadoFolio = estadoFolio
    this.elementosPlantillaHtml = elementosPlantillaHtml

  }

  obtenerElementos(tipoFolio) {
    //console.log({tipoFolio});
    const divElement = document.querySelector(`#plantilla_${tipoFolio}`)
    const elementosDentroDiv = divElement.querySelectorAll('select, input, textarea');
    // console.log(elementosDentroDiv);
    this.addEvenlistAllElements(elementosDentroDiv)

  }

  addEvenlistAllElements(elemts) {
    elemts.forEach(element => {
      //console.log(element.name);
      if (element.type !== 'button' || element.id) {
        // console.log(element.id);
        element.addEventListener('blur', () => {
          //console.log(element.value);
          let elementsID = element.id.split('_')
          // console.log(elementsID[1], elementsID[2], element.value);
          guardarDatosPlantilla(elementsID[1].toLowerCase(), element.name, element.value)
          // guardarDatosPlantilla(elementsID[1].toLowerCase(),'observaciones','element.value')
        })
      }
    });
  }





}

let gestionPlantillasHtml = new GestionPlantillasHtml