arg = ''
var pagina = ''

function eliminarNotafactura() {
    setTimeout(() => {
        id_nota = traerDatoFilaSeleccionada("listaNotasFactura", "ID_RECIBO");
        pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
        
        valores_a_mandar = 'accion=eliminarNotaFatura';
        valores_a_mandar = valores_a_mandar + "&idQuery=9021&parametros=";
        add_valores_a_mandar(id_nota);
        ajaxModificar();
    }, 100);
}


function objeto_html_enviar_nota_fe(rowObject) {
    let id_nota = rowObject.getElementsByTagName("cell")[2].childNodes[0].nodeValue

    let enviar_fe = document.createElement("img");

    enviar_fe.style.margin = "0 auto";
    enviar_fe.style.display = "block";
    enviar_fe.width = 24;
    enviar_fe.height = 24;
    enviar_fe.src = "/clinica/utilidades/imagenes/icons/icons/cloud-arrow-up.svg";
    enviar_fe.setAttribute("onclick", "verificarNotificacionAntesDeGuardar('validarEnvioNotaCredito')")

    return enviar_fe.outerHTML
}

function objeto_html_eliminar_nota(rowObject) {
    let id_nota = rowObject.getElementsByTagName("cell")[2].childNodes[0].nodeValue

    let eliminar_nota = document.createElement("img");

    eliminar_nota.style.margin = "0 auto";
    eliminar_nota.style.display = "block";
    eliminar_nota.width = 21;
    eliminar_nota.height = 21;
    eliminar_nota.src = "/clinica/utilidades/imagenes/icons/icons/x-circle.svg";
    eliminar_nota.setAttribute("onclick", "verificarNotificacionAntesDeGuardar('validarEliminaciónNotaFactura')")

    return eliminar_nota.outerHTML
}

function limpiarCasillasCrearFacturas() {
    limpiaAtributo('txtIdArticuloFactura', 0);
    limpiaAtributo('txtIdProcedimientoCex', 0);
    limpiaAtributo('lblValorUnitarioArticuloFactura', 1);
    limpiaAtributo('lblValorUnitarioProc', 1);
    limpiaAtributo('cmbCantidadArticuloFactura', 0);
    document.getElementById('cmbCantidad').value = '';
    limpiaAtributo('txtNoAutorizacionProcedimiento', 0);
}

function tipificarFacturasBaseNom() {
    Swal.fire({
        html: "El proceso de tipificaci&oacuten solo se aplicar&aacute a facturas de base nominal finalizadas.",
        icon: "info",
        confirmButtonText: "Continuar",
        confirmButtonColor: '#3085d6',
        position: "top",
    }).then(function (sweetAlertResult) {
        if (sweetAlertResult.isConfirmed) {
            valores_a_mandar = "";
            add_valores_a_mandar(valorAtributo('cmbAnio'));
            add_valores_a_mandar(valorAtributo('cmbMesNefro'));
            /* FILTRO PARA ATENDIDO */
            add_valores_a_mandar(valorAtributo('cmbAtendido'));
            /* FILTRO PARA PACIENTE FACTURADO POR BASE NOMINAL */
            add_valores_a_mandar(valorAtributo('cmbFacturadoBaseNom'));
            /* FILTRO PARA PACIENTE */
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            /* FILTRO PARA ESTADIO */
            let lista_estadios = []
            $("[id='chkestadio']:checked").each(function () {
                lista_estadios.push(this.value);
            });
            add_valores_a_mandar(lista_estadios.length == 0 ? "" : lista_estadios);
            /* FILTRO PARA DIAGNOSTICO DIABETES */
            add_valores_a_mandar(valorAtributo('cmbdxDiabetes'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionPrim'));
            /* FILTRO PARA OTRAS ETIOLOGIAS */
            add_valores_a_mandar(valorAtributo('cmbdxEtiologias'));
            /* FILTRO PARA ESTADO DE LA FACTURA */
            add_valores_a_mandar(valorAtributo('cmbEstadoFactura'))
            //LIMIT
            // add_valores_a_mandar("100")
            /* FILTRO PARA LA SECCIONAL */
            add_valores_a_mandar(valorAtributo('cmbSeccional'));
            /* FILTRO PARA LOS PAQUETES */
            add_valores_a_mandar(valorAtributo('cmbPaquete'));
            /* FILTRO PARA REGIMEN */
            add_valores_a_mandar(valorAtributo('cmbTipoRegimen'));
            // estado de tipificacion
            add_valores_a_mandar(valorAtributo('cmbTipificacion'));
            // Tipo de Red
            add_valores_a_mandar(valorAtributo('cmbTipoRed'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionSec'));
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                data: {
                    "idQuery": 2757,
                    "parametros": valores_a_mandar,
                    "rows": -1,
                    "page": 1,
                    "sord": "asc"
                },
                beforeSend: function () {
                    mostrarLoader("tipificarFacturasBaseNominal")
                },
                success: function (data) {
                    let respuesta = data.getElementsByTagName('cell');
                    tipificarFacturas(respuesta[1].firstChild.data.split(","));
                    asignaAtributo("cmbEstadoFactura", 2, 0);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("error peticion traer ids de facturas para tipifica.");
                },
                complete: function () {
                    buscarHC('traerBaseNefro')
                    ocultarLoader("tipificarFacturasBaseNominal")
                }
            });
        }
    });
}


function numeraFacEnBloque() {
    Swal.fire({
        html: "Se tomaran unicamente el numero de pacientes <label style='color: blue;'>NO ATENDIDOS</label> con <label style='color: blue;'>FACTURAS PENDIENTES</label> creadas <label style='color: blue;'>POR BASE NOMINAL</label>. Esta seguro de continuar ?",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Continuar",
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#999999',
        position: "top",
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading(),
        preConfirm: function () {
            valores_a_mandar = "";
            add_valores_a_mandar(valorAtributo('cmbAnio'));
            add_valores_a_mandar(valorAtributo('cmbMesNefro'));
            /* FILTRO PARA ATENDIDO */
            add_valores_a_mandar(valorAtributo('cmbAtendido'));
            /* FILTRO PARA PACIENTE FACTURADO POR BASE NOMINAL */
            add_valores_a_mandar(valorAtributo('cmbFacturadoBaseNom'));
            /* FILTRO PARA PACIENTE */
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            /* FILTRO PARA ESTADIO */
            let lista_estadios = []
            $("[id='chkestadio']:checked").each(function () {
                lista_estadios.push(this.value);
            });
            add_valores_a_mandar(lista_estadios.length == 0 ? "" : lista_estadios);
            /* FILTRO PARA DIAGNOSTICO DIABETES */
            add_valores_a_mandar(valorAtributo('cmbdxDiabetes'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionPrim'));
            /* FILTRO PARA OTRAS ETIOLOGIAS */
            add_valores_a_mandar(valorAtributo('cmbdxEtiologias'));
            /* FILTRO PARA ESTADO DE LA FACTURA */
            add_valores_a_mandar(valorAtributo('cmbEstadoFactura'))
            //LIMIT
            // add_valores_a_mandar("100")
            /* FILTRO PARA LA SECCIONAL */
            add_valores_a_mandar(valorAtributo('cmbSeccional'));
            /* FILTRO PARA LOS PAQUETES */
            add_valores_a_mandar(valorAtributo('cmbPaquete'));
            /* FILTRO PARA REGIMEN */
            add_valores_a_mandar(valorAtributo('cmbTipoRegimen'));
            // estado de tipificacion
            add_valores_a_mandar(valorAtributo('cmbTipificacion'));
            // TIPO DE RED
            add_valores_a_mandar(valorAtributo('cmbTipoRed'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionSec'));
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                async: false,
                data: {
                    "idQuery": 1107,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () { },
                success: function (data) {
                    respuesta = data.getElementsByTagName("cell");
                    Swal.fire({
                        html: "Cantidad de facturas disponibles para numerar: <strong>" + respuesta[1].firstChild.data + "</strong>",
                        icon: "info",
                        showCancelButton: true,
                        confirmButtonText: "Continuar",
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#999999',
                        position: "top",
                    }).then(function (sweetAlertResult) {
                        if (sweetAlertResult.isConfirmed) {
                            if (banFacturarBaseNominal || true) {
                                if (valorAtributo('cmbSeccional') == '1') {
                                    numeraFacEnBloqueExterna('35', respuesta[1].firstChild.data);
                                } else if (valorAtributo('cmbSeccional') == '2') {
                                    numeraFacEnBloqueExterna('15', respuesta[1].firstChild.data);
                                } else {
                                    alert("LA SECCIONAL NO SE ENCUENTRA PARAMETRIZADA")
                                }
                            }
                        }
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
                complete: function (jqXHR, textStatus) { },
            });
        },
    });
}

function numeraFacEnBloqueExterna(idSede, cantidad_facturas) {
    var sede;
    switch (idSede) {
        case "35":
            sede = "MEDICRON"
            break;

        case "15":
            sede = "RENAL Y PULMONAR"
            break;

        default:
            sede = "";
            break;
    }

    var query = "mutation($sede: String!, $incrementar:Boolean, $totalFacturas:Int!){ numeracionFacturaNominal(sede:$sede, incrementar:$incrementar, totalFacturas:$totalFacturas){ numeracion{ numeroInicial numeroFinal }}}";
    var variables = { "sede": sede, "incrementar": false, "totalFacturas": cantidad_facturas }
    $.ajax({
        url: "http://190.60.243.113:8081/graphql",
        type: "POST",
        contentType: "application/json",
        crossDomain: true,
        data: JSON.stringify({ "query": query, "variables": variables }),
        beforeSend: function () { },
        success: function (data) {
            var respuesta = data.data.numeracionFacturaNominal.numeracion
            if (respuesta != null) {
                //PROCESAR NUMERACION
                pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
                valores_a_mandar = 'accion=numerarFacturasBloque';
                valores_a_mandar = valores_a_mandar + "&idQuery=2678&parametros=";

                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('cmbAnio'));
                add_valores_a_mandar(valorAtributo('cmbMesNefro'));

                /* FILTRO PARA ATENDIDO */
                add_valores_a_mandar(valorAtributo('cmbAtendido'));
                add_valores_a_mandar(valorAtributo('cmbAtendido'));

                /* FILTRO PARA PACIENTE FACTURADO POR BASE NOMINAL */
                add_valores_a_mandar(valorAtributo('cmbFacturadoBaseNom'));
                add_valores_a_mandar(valorAtributo('cmbFacturadoBaseNom'));

                /* FILTRO PARA PACIENTE */
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'))

                /* FILTRO PARA ESTADIO */
                let lista_estadios = []
                $("[id='chkestadio']:checked").each(function () {
                    lista_estadios.push(this.value);
                });
                add_valores_a_mandar(lista_estadios);
                add_valores_a_mandar(lista_estadios);

                /* FILTRO PARA DIAGNOSTICO DIABETES */
                add_valores_a_mandar(valorAtributo('cmbdxDiabetes'));
                add_valores_a_mandar(valorAtributo('cmbdxDiabetes'));

                /* FILTRO PARA DIAGNOSTICO HTA */
                add_valores_a_mandar(valorAtributo('cmbdxHipertensionPrim'));
                add_valores_a_mandar(valorAtributo('cmbdxHipertensionPrim'));

                /* FILTRO PARA OTRAS ETIOLOGIAS */
                add_valores_a_mandar(valorAtributo('cmbdxEtiologias'));
                add_valores_a_mandar(valorAtributo('cmbdxEtiologias'));

                /* FILTRO PARA ESTADO DE LA FACTURA */
                add_valores_a_mandar(valorAtributo('cmbEstadoFactura'))
                add_valores_a_mandar(valorAtributo('cmbEstadoFactura'))

                /* FILTRO PARA SECCIONAL */
                add_valores_a_mandar(valorAtributo('cmbSeccional'));

                /* FILTRO PARA LOS PAQUETES */
                add_valores_a_mandar(valorAtributo('cmbPaquete'));
                add_valores_a_mandar(valorAtributo('cmbPaquete'));

                /* FILTRO PARA REGIMEN */
                add_valores_a_mandar(valorAtributo('cmbTipoRegimen'));
                add_valores_a_mandar(valorAtributo('cmbTipoRegimen'));

                /* FILTRO PARA ESTADO TIPIFICACION */
                add_valores_a_mandar(valorAtributo('cmbTipificacion'));
                add_valores_a_mandar(valorAtributo('cmbTipificacion'));

                /* FILTRO PARA TIPO DE RED */
                add_valores_a_mandar(valorAtributo('cmbTipoRed'));
                add_valores_a_mandar(valorAtributo('cmbTipoRed'));

                /* FILTRO PARA DIAGNOSTICO HTA */
                add_valores_a_mandar(valorAtributo('cmbdxHipertensionSec'));
                add_valores_a_mandar(valorAtributo('cmbdxHipertensionSec'));

                add_valores_a_mandar(parseInt(respuesta.numeroInicial) + 1);
                add_valores_a_mandar(respuesta.numeroFinal);

                ajaxModificar();
            } else {
                alert("No se puede obtener numeracion externa. Por favor ponte en contacto con soporte tecnico.")
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (1)")
        }
    });
}


function revertirFacturasBaseNominal() {
    Swal.fire({
        html: "Se eliminar las facturas del listado en estado <label style='color: blue;'>PENDIENTE</label> creadas por <label style='color: blue;'>BASE NOMINAL</label>. Est&aacute seguro de continuar ?",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Continuar",
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#999999',
        position: "top",
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading(),
        preConfirm: function () {
            valores_a_mandar = "";
            add_valores_a_mandar(valorAtributo('cmbAnio'));
            add_valores_a_mandar(valorAtributo('cmbMesNefro'));
            /* FILTRO PARA ATENDIDO */
            add_valores_a_mandar(valorAtributo('cmbAtendido'));
            /* FILTRO PARA PACIENTE FACTURADO POR BASE NOMINAL */
            add_valores_a_mandar(valorAtributo('cmbFacturadoBaseNom'));
            /* FILTRO PARA PACIENTE */
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            /* FILTRO PARA ESTADIO */
            let lista_estadios = []
            $("[id='chkestadio']:checked").each(function () {
                lista_estadios.push(this.value);
            });
            add_valores_a_mandar(lista_estadios.length == 0 ? "" : lista_estadios);
            /* FILTRO PARA DIAGNOSTICO DIABETES */
            add_valores_a_mandar(valorAtributo('cmbdxDiabetes'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionPrim'));
            /* FILTRO PARA OTRAS ETIOLOGIAS */
            add_valores_a_mandar(valorAtributo('cmbdxEtiologias'));
            /* FILTRO PARA ESTADO DE LA FACTURA */
            add_valores_a_mandar(valorAtributo('cmbEstadoFactura'))
            //LIMIT
            // add_valores_a_mandar("100")
            /* FILTRO PARA LA SECCIONAL */
            add_valores_a_mandar(valorAtributo('cmbSeccional'));
            /* FILTRO PARA LOS PAQUETES */
            add_valores_a_mandar(valorAtributo('cmbPaquete'));
            /* FILTRO PARA REGIMEN */
            add_valores_a_mandar(valorAtributo('cmbTipoRegimen'));
            // estado de tipificacion
            add_valores_a_mandar(valorAtributo('cmbTipificacion'));
            // TIPO DE RED
            add_valores_a_mandar(valorAtributo('cmbTipoRed'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionSec'));
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                async: false,
                data: {
                    "idQuery": 2692,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () { },
                success: function (data) {
                    respuesta = data.getElementsByTagName("cell");
                    Swal.fire({
                        html: "Cantidad de facturas a eliminar: <strong>" + respuesta[1].firstChild.data + "</strong>",
                        icon: "info",
                        showCancelButton: true,
                        confirmButtonText: "Continuar",
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#999999',
                        position: "top",
                    }).then(function (sweetAlertResult) {
                        if (sweetAlertResult.isConfirmed) {
                            modificarCRUD("eliminarFacturasBaseNominal")
                        }
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
                complete: function (jqXHR, textStatus) { },
            });
        },
    });
}

function abrirVentanaFacBaseNom() {
    if (valorAtributo('lblTotalResultados') == '' || valorAtributo('lblTotalResultados') == '0') { alert('NO HAY DATOS PARA FACTURAR. POR FAVOR ELIJA OTRAS FECHAS'); return; }
    //limpiaAtributo('lblValorUnitarioProc', 1);
    //limpiaAtributo('cmbIdProcedimientoFacBN', 0);
    /* EN EL CAMBIO DE CUALQUIER CAMPO SE COLOCA UNA BANDERA EN FALSO PARA OBLIGAR A BUSCAR  */


    if (banFacturarBaseNominal || true) {
        //asignaAtributo('cmbIdProcedimientoFacBN', valorAtributo('cmbPaquete'), 0);
        /* if (valorAtributo('cmbPaquete') != '') {
            asignaAtributo('cmbIdProcedimientoFacBN', valorAtributo('cmbPaquete'), 1);
        } */
        mostrar('divVentanitaFacBaseNom');
        valores_a_mandar = ''
        jQuery("#listProcedimientosFacBloque").jqGrid("clearGridData");
        $('#drag' + ventanaActual.num).find("#listProcedimientosFacBloque").jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['', 'CODIGO', 'CUPS', 'PROCEDIMIENTO', 'CANTIDAD', 'VALOR UNITARIO', 'TOTAL'],
            colModel: [
                { name: '', index: 'contador', hidden: true },
                { name: 'CODIGO', index: 'CODIGO', width: anchoP(ancho, 10) },
                { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 20) },
                { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 15) },
                { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 60) },
                { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', width: anchoP(ancho, 60) },
                { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 60) }
            ],

            onSelectRow: function (rowid) {
                var datosRow = jQuery('#drag' + ventanaActual.num).find('#listProcedimientosFacBloque').getRowData(rowid);
                // jQuery('#listCitaCexProcedimientoTraer').delRowData(rowid);
                eliminarElementoGrilla('listProcedimientosFacBloque', rowid);
                limpiaAtributo('lblValorUnitarioProc', 1);
                //asignaAtributo('lblNomSitioQuirurgico', datosRow.nombreSitioQuirur, 0)
                //asignaAtributo('lblIdProcedimiento', datosRow.idProcedimiento, 0)
                //asignaAtributo('lblNomProcedimiento', datosRow.nombreProcedimiento, 0)
                //mostrar('divVentanitaEliminarCirugiaProcedimientos')
            },
            height: 80,
            autowidth: true,
        });
        $('#drag' + ventanaActual.num).find("#listProcedimientosFacBloque").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

    } else {
        alert('HAN OCURRIDO CAMBIOS EN LOS CAMPOS, POR FAVOR VUELVA A REALIZAR LA CONSULTA PARA PODER FACTURAR');
    }
}

function traerCantidadNoAplica() {
    valores_a_mandar = "";
    add_valores_a_mandar(valorAtributo('cmbMesNefro'));
    add_valores_a_mandar(valorAtributo('cmbAnio'));

    $.ajax({
        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        type: "POST",
        async: false,
        data: {
            "idQuery": 1119,
            "parametros": valores_a_mandar,
            "_search": false,
            "nd": 1611873687449,
            "rows": -1,
            "page": 1,
            "sidx": "NO FACT",
            "sord": "asc"
        },
        beforeSend: function () { },
        success: function (data) {
            respuesta = data.getElementsByTagName("cell");
            asignaAtributo("lblcantidadNoAplica", respuesta[1].firstChild.data)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { },
        complete: function (jqXHR, textStatus) { },
    });
}

function traerCantidadNovedades() {
    valores_a_mandar = "";
    add_valores_a_mandar(valorAtributo('cmbMesNefro'));
    add_valores_a_mandar(valorAtributo('cmbAnio'));

    $.ajax({
        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        type: "POST",
        async: false,
        data: {
            "idQuery": 1109,
            "parametros": valores_a_mandar,
            "_search": false,
            "nd": 1611873687449,
            "rows": -1,
            "page": 1,
            "sidx": "NO FACT",
            "sord": "asc"
        },
        beforeSend: function () { },
        success: function (data) {
            respuesta = data.getElementsByTagName("cell");
            asignaAtributo("lblcantidadNovedades", respuesta[1].firstChild.data)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { },
        complete: function (jqXHR, textStatus) { },
    });
}

function crearFacBloque(arg) {
    Swal.fire({
        html: "Se tomaran unicamente el numero de pacientes <label style='color: blue;'>NO ATENDIDOS</label> en estado <label style='color: blue;'>SIN FACTURA</label>. Esta seguro de continuar ?",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Continuar",
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#999999',
        position: "top",
        showLoaderOnConfirm: true,
        allowOutsideClick: () => !Swal.isLoading(),
        preConfirm: function () {
            valores_a_mandar = "";
            add_valores_a_mandar(valorAtributo('cmbAnio'));
            add_valores_a_mandar(valorAtributo('cmbMesNefro'));
            /* FILTRO PARA ATENDIDO */
            add_valores_a_mandar(valorAtributo('cmbAtendido'));
            /* FILTRO PARA PACIENTE FACTURADO POR BASE NOMINAL */
            add_valores_a_mandar(valorAtributo('cmbFacturadoBaseNom'));
            /* FILTRO PARA PACIENTE */
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            /* FILTRO PARA ESTADIO */
            let lista_estadios = []
            $("[id='chkestadio']:checked").each(function () {
                lista_estadios.push(this.value);
            });
            add_valores_a_mandar(lista_estadios.length == 0 ? "" : lista_estadios);
            /* FILTRO PARA DIAGNOSTICO DIABETES */
            add_valores_a_mandar(valorAtributo('cmbdxDiabetes'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionPrim'));
            /* FILTRO PARA OTRAS ETIOLOGIAS */
            add_valores_a_mandar(valorAtributo('cmbdxEtiologias'));
            /* FILTRO PARA ESTADO DE LA FACTURA */
            add_valores_a_mandar(valorAtributo('cmbEstadoFactura'))
            //LIMIT
            // add_valores_a_mandar("100")
            /* FILTRO PARA LA SECCIONAL */
            add_valores_a_mandar(valorAtributo('cmbSeccional'));
            /* FILTRO PARA LOS PAQUETES */
            add_valores_a_mandar(valorAtributo('cmbPaquete'));
            /* FILTRO PARA REGIMEN */
            add_valores_a_mandar(valorAtributo('cmbTipoRegimen'));
            // estado de tipificacion
            add_valores_a_mandar(valorAtributo('cmbTipificacion'));
            // TIPO DE RED
            add_valores_a_mandar(valorAtributo('cmbTipoRed'));
            /* FILTRO PARA DIAGNOSTICO HTA */
            add_valores_a_mandar(valorAtributo('cmbdxHipertensionSec'));
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                async: false,
                data: {
                    "idQuery": 1106,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () { },
                success: function (data) {
                    respuesta = data.getElementsByTagName("cell");
                    Swal.fire({
                        html: "Cantidad de facturas a crear por base nominal: <strong>" + respuesta[1].firstChild.data + "</strong>",
                        icon: "info",
                        showCancelButton: true,
                        confirmButtonText: "Continuar",
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#999999',
                        position: "top",
                    }).then(function (sweetAlertResult) {
                        if (sweetAlertResult.isConfirmed) {
                            modificarCRUD("crearFacturasBaseNominalNefro")
                        }
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { },
                complete: function (jqXHR, textStatus) { },
            });
        },
    })
}

/*function numeraFacEnBloque(accion, raiz) {
    if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
        switch (accion) {
            case 'FacCreadasBloque':
                alert('FACTURAS CREADAS CORRECTAMENTE')
                $('#divVentanitaFacBaseNom').hide()
                buscarHC('traerBaseNefro');
                break;

            case 'numerarFacturasBloque_':
                alert('FACTURAS NUMERADAS CORRECTAMENTE--');
                buscarHC('traerBaseNefro');
                break;
        }
    }
}*/

function listGrillaFacBaseNom(arg) {
    url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        valores_a_mandar = url + "?idQuery=2677&parametros=";
    add_valores_a_mandar('')
    $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
        url: valores_a_mandar,
        datatype: 'xml',
        mtype: 'GET',
        colNames: ['', 'CODIGO', 'CUPS', 'PROCEDIMIENTO', 'CANTIDAD', 'VALOR UNITARIO', 'TOTAL'],
        colModel: [
            { name: '', index: 'contador', hidden: true },
            { name: 'CODIGO', index: 'CODIGO', width: anchoP(ancho, 10) },
            { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 20) },
            { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 15) },
            { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 60) },
            { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', width: anchoP(ancho, 60) },
            { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 60) }
        ],

        onSelectRow: function (rowid) {
            /* var datosRow = jQuery('#drag' + ventanaActual.num).find('#listProcedimientosFacBloque').getRowData(rowid);
            // jQuery('#listCitaCexProcedimientoTraer').delRowData(rowid);
            eliminarElementoGrilla('listProcedimientosFacBloque', rowid); */
            //asignaAtributo('lblNomSitioQuirurgico', datosRow.nombreSitioQuirur, 0)
            //asignaAtributo('lblIdProcedimiento', datosRow.idProcedimiento, 0)
            //asignaAtributo('lblNomProcedimiento', datosRow.nombreProcedimiento, 0)
            //mostrar('divVentanitaEliminarCirugiaProcedimientos')
        },
        height: 160,
        autowidth: true,
    });
    $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
}

function pruebagragp() {
    $.ajax({
        url: "http://190.60.243.113:8081/graphql",
        type: "POST",
        contentType: "application/json",
        crossDomain: true,
        data: JSON.stringify({ "query": "mutation{numeracionFactura(incrementar:true){numero}}", "variables": null }),
        beforeSend: function () { },
        success: function (data) {
            alert(JSON.stringify(data))
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus)
        }
    });
}

function exportarLogTipificacion() {
    var dimension = 'width=1150,height=752,scrollbars=NO,statusbar=NO,left=150,top=90';
    var id_envio = valorAtributo('lblIdEnvioVentanita');
    var url_ = "facturacion/log_tipificacion.jsp?id_envio=" + id_envio;
    window.open(url_, '', dimension)
}

function traerValorRealCuenta() {
    valores_a_mandar = ""
    add_valores_a_mandar(valorAtributo("lblIdEnvioVentanita"))

    $.ajax({
        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        type: "POST",
        data: {
            "idQuery": 1083,
            "parametros": valores_a_mandar,
            "_search": false,
            "nd": 1611873687449,
            "rows": -1,
            "page": 1,
            "sidx": "NO FACT",
            "sord": "asc"
        },
        beforeSend: function () { },
        success: function (data) {
            var respuesta = data.getElementsByTagName('cell');
            var valor = respuesta[1].firstChild.data

            asignaAtributo("txtNuevoValorCuenta", valor, 0)
        },
        complete: function (jqXHR, String) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("NO HA SIDO POSIBLE COMPLETAR LA SOLICITUD. POR FAVOR INTENTE NUEVAMENTE O PONGASE EN CONTACTO CON SOPORTE.")
        }
    });
}

function editarValorCuenta() {
    var cuenta = $("#listaCuentas").jqGrid('getGridParam', 'selrow')
    var id_estado = $("#listaCuentas").getRowData(cuenta).ID_ESTADO

    if (id_estado == 0) {
        mostrar('tablaEditarValorCuenta');
        ocultar('tablaValorCuenta');
    } else {
        alert("LA CUENTA NO SE PUEDE MODIFICAR.")
    }
}

function editarRemision() {
    var cuenta = $("#listaCuentas").jqGrid('getGridParam', 'selrow')
    var id_estado = $("#listaCuentas").getRowData(cuenta).ID_ESTADO

    if (id_estado == 0) {
        mostrar('tablaEditarRemision');
        ocultar('tablaInfoRemision')
    } else {
        alert("LA CUENTA NO SE PUEDE MODIFICAR.")
    }
}

function editarNumeroCuenta() {
    var cuenta = $("#listaCuentas").jqGrid('getGridParam', 'selrow')
    var id_estado = $("#listaCuentas").getRowData(cuenta).ID_ESTADO

    if (id_estado == 0) {
        mostrar('tablaEditarInfoCuenta');
        ocultar('tablaInfoCuenta')
    } else {
        alert("LA CUENTA NO SE PUEDE MODIFICAR.")
    }
}


function imprimirPDFCuentaCobroRips() {
    var dimension = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90'
    var id_envio = valorAtributo('lblIdEnvioVentanita');
    nuevaURL = "ireports/hc/generaHC.jsp?reporte=CUENTA_COBRO_RIPS&evo=" + id_envio;
    window.open(nuevaURL, '', dimension);
}

function radicarCuenta() {
    var cuenta = $("#listaCuentas").jqGrid('getGridParam', 'selrow')
    var id_estado = $("#listaCuentas").getRowData(cuenta).ID_ESTADO

    if (id_estado != 1) {
        mostrar('divVentanitaCerrarCuenta')
    } else {
        alert("LA CUENTA YA ESTA CERRADA.")
    }
}

function ocultarDivVentanitaGeneraRips() {
    ocultar('divVentanitaGeneraRips');
    //buscarFacturacion('listaCuentas');
    mostrar('tablaValorCuenta');
    ocultar('tablaEditarValorCuenta');
    limpiaAtributo("txtNuevoValorCuenta");
}

function ocultarDetalleFacturaDetalle() {
    ocultar("divOrdenesFacturaDetalle");
    buscarFacturacion("listProcedimientosDeFactura");
}

function verFormatoOrdenPendiente() {
    setTimeout(() => {
        var folio = $("#listaHcSinFactura").jqGrid('getGridParam', 'selrow')
        if ($("#listaHcSinFactura").getRowData(folio).ID_TIPO == "1") {
            var tipo = $("#listaHcSinFactura").getRowData(folio).DESCRIPCION.split("-")[0].trim()
            var id_folio = $("#listaHcSinFactura").getRowData(folio).ID
            var nombre_pdf = "HC-" + id_folio

            verFormatoHc(tipo, id_folio, nombre_pdf)
        } else {
            alert("EL ELEMENTO SELECCIONADO NO ES HISTORIA CLINICA.")
        }
    }, 100);
}

function verFormatoOrdenFacturada() {
    setTimeout(() => {
        var folio = $("#listaFacturaDetalleEvolucion").jqGrid('getGridParam', 'selrow')
        if ($("#listaFacturaDetalleEvolucion").getRowData(folio).ID_TIPO == "1") {
            var tipo = $("#listaFacturaDetalleEvolucion").getRowData(folio).DESCRIPCION.split("-")[0].trim()
            var id_folio = $("#listaFacturaDetalleEvolucion").getRowData(folio).ID
            var nombre_pdf = "HC-" + id_folio

            verFormatoHc(tipo, id_folio, nombre_pdf)
        } else {
            alert("EL ELEMENTO SELECCIONADO NO ES HISTORIA CLINICA.")
        }
    }, 100);
}

function verFormatoHcElementosPendientes() {
    setTimeout(() => {
        var folio = $("#listaHcSinFactura").jqGrid('getGridParam', 'selrow')
        var tipo = $("#listaHcSinFactura").getRowData(folio).TIPO.split("-")[0].trim()
        var id_folio = $("#listaHcSinFactura").getRowData(folio).FOLIO
        var nombre_pdf = "HC-" + id_folio

        verFormatoHc(tipo, id_folio, nombre_pdf)
    }, 200);
}

function verFormatoHcFolioFacturado() {
    setTimeout(() => {
        var folio = $("#listaFacturaDetalleEvolucion").jqGrid('getGridParam', 'selrow')
        var tipo = $("#listaFacturaDetalleEvolucion").getRowData(folio).TIPO.split("-")[0].trim()
        var id_folio = $("#listaFacturaDetalleEvolucion").getRowData(folio).FOLIO
        var nombre_pdf = "HC-" + id_folio

        verFormatoHc(tipo, id_folio, nombre_pdf)
    }, 200);
}

function traerobservaciones(id_factura) {
    valores_a_mandar = ""
    add_valores_a_mandar(id_factura)

    $.ajax({
        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        type: "POST",
        data: {
            "idQuery": 4113,
            "parametros": valores_a_mandar,
            "_search": false,
            "nd": 1611873687449,
            "rows": -1,
            "page": 1,
            "sidx": "NO FACT",
            "sord": "asc"
        },
        beforeSend: function () { },
        success: function (data) {
            var respuesta = data.getElementsByTagName('cell');
            var valor = respuesta[1].firstChild.data
            console.log(respuesta)
            asignaAtributo("txtObservacionFactura", valor, 0)
        },
        complete: function (jqXHR, String) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function buscarFacturacion(arg, callback) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {
        case "tablaTipificacionExterna":
            setTimeout(() => {
                ancho = $("#" + arg).width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=1045&parametros=";

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['#', 'Grupo', 'Total facturas', 'Disponibles', 'Tipificadas', 'Restantes'],
                    colModel: [
                        { name: '#', index: '#', hidden: true },
                        { name: 'Grupo', index: 'Grupo', width: anchoP(ancho, 2) },
                        { name: 'Total facturas', index: 'Total facturas', width: anchoP(ancho, 2) },
                        { name: 'Disponibles', index: 'Disponibles', width: anchoP(ancho, 2) },
                        { name: 'Tipificadas', index: 'Tipificadas', width: anchoP(ancho, 2) },
                        { name: 'Restantes', index: 'Restantes', width: anchoP(ancho, 2) },
                    ],
                    height: 150,
                    width: ancho,
                    onSelectRow: function (rowid) { },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 200);
            break;

        case 'listaElementosHCFacturados':
            setTimeout(() => {
                ancho = $("#divHcFactura").width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=3008&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdFactura"));
                add_valores_a_mandar(valorAtributo("lblIdFactura"));
                add_valores_a_mandar(valorAtributo("lblIdFactura"));

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['#', 'TIPO', 'ID_TIPO', 'ID_FACTURA_DETALLE', 'ID',
                        'DESCRIPCION', 'CANT', 'FECHA', 'PROFESIONAL', 'CITA', 'ESTADO', ''],
                    colModel: [
                        { name: '#', index: '#', width: anchoP(ancho, 1) },
                        { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 3) },
                        { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                        { name: 'ID_FACTURA_DETALLE', index: 'ID_FACTURA_DETALLE', hidden: true },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 1) },
                        { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 8) },
                        { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 1) },
                        { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 3) },
                        { name: 'PROFESIONAL', index: 'PROFESIONAL', width: anchoP(ancho, 5) },
                        { name: 'CITA', index: 'CITA', width: anchoP(ancho, 2) },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 2) },
                        {
                            name: "", index: "", hidden: true, align: 'center', formatter: function () {
                                return '<img onclick="_verFormatoOrdenPendiente()" src="/clinica/utilidades/imagenes/icons/icons/search.svg" width="18px" height="18px" align="middle">'
                            }
                        },
                    ],
                    height: 280,
                    width: ancho,
                    multiselect: true,
                    multiboxonly: true,
                    grouping: true,
                    groupingView: {
                        groupField: ['TIPO'],
                        groupText: ['<b>{0} ({1})</b>']
                    },
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 200);
            break;

        case "listaHcAnuladaFacturacion":
            $("#listaHcAnuladaFacturacion").jqGrid("clearGridData");
            setTimeout(() => {
                ancho = $("#listaHcAnuladaFacturacion").width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=3006&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['#', 'TIPO', 'ID_TIPO', 'ID',
                        'DESCRIPCION', 'CANT', 'FECHA', 'PROFESIONAL', 'CITA', 'ESTADO', 'OBSERVACION'],
                    colModel: [
                        { name: '#', index: '#', width: anchoP(ancho, 1) },
                        { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 2) },
                        { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 1) },
                        { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 8) },
                        { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 1) },
                        { name: 'FECHA', index: 'FECHA', hidden: true },
                        { name: 'PROFESIONAL', index: 'PROFESIONAL', width: anchoP(ancho, 5) },
                        { name: 'CITA', index: 'CITA', hidden: true },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 2) },
                        { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 4) },
                    ],
                    height: 280,
                    width: ancho,
                    grouping: true,
                    groupingView: {
                        groupField: ['TIPO'],
                        groupText: ['<b>{0} ({1})</b>']
                    },
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 200);
            break;

        case 'listaFacturasPendientes':
            ancho = $("#divListaFacturasPendientes").width();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=205&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPlanVentanita'));
            add_valores_a_mandar(valorAtributo('lblRangoVentanita').split('-')[1]);
            add_valores_a_mandar(valorAtributo('lblRangoVentanita').split('-')[0]);
            add_valores_a_mandar(valorAtributo('lblPosVentanita'))

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID_FACTURA', 'FACTURA', 'ID_PLAN', 'PLAN', ''],
                colModel: [
                    { name: 'C', index: 'C', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                    { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 3) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 10) },
                    { name: '', index: '', width: anchoP(ancho, 1) },
                ],
                height: 320,
                autowidth: true,
                multiboxonly: true,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    var detalle = "";

                    if (valorAtributo('lblIdRipsVentanita') == 'US') {
                        detalle = datosRow.IDENTIFICACION + " " + datosRow.NOMBRE + "-" + datosRow.ADMINISTRADORA;
                    } else {
                        detalle = datosRow.FACTURA + " " + datosRow.PLAN + " " + datosRow.IDENTIFICACION + " " + datosRow.NOMBRE;
                    }
                    asignaAtributo('lblIdPlanFactura', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblPlanFactura', datosRow.PLAN, 0)
                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('lblNumeroFactura', datosRow.FACTURA, 0)
                },
                multiselect: true,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listaFacturasCuenta':
            ancho = $("#divListaFacturasCuenta").width();
            valores_a_mandar = pag;

            valores_a_mandar = valores_a_mandar + "?idQuery=1082&parametros=";
            add_valores_a_mandar(valorAtributo('txtCuentaActual'));
            add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
            add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
            add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
            add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
            add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
            add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));

            colNames = ['#', 'ID_ARCHIVO', 'ID_FACTURA', 'FACTURA', 'ID_PLAN', 'PLAN', 'ID_ADMISION', 'ID_PACIENTE', 'IDENTIFICACION', 'NOMBRE', ''];
            colModel = [
                { name: '#', index: '#', width: anchoP(ancho, 2) },
                { name: 'ID_ARCHIVO', index: 'ID_ARCHIVO', hidden: true },
                { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 5) },
                { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                { name: 'ID_ADMISION', index: 'ID_ADMISION', hidden: true },
                { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 15) },
                { name: '', index: '', width: anchoP(ancho, 2) },
            ]

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: colNames,
                colModel: colModel,
                height: 320,
                autowidth: true,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdEnvioFactura', datosRow.ID_ENVIO, 0)
                    asignaAtributo('lblIdArchivoFactura', datosRow.ID_ARCHIVO, 0)
                    asignaAtributo('lblIdPlanFactura', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblPlanFactura', datosRow.PLAN, 0)
                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('lblNumeroFactura', datosRow.FACTURA, 0)
                },
                multiselect: true,
                multiboxonly: true,
                gridComplete: function () {
                    buscarFacturacion("listaFacturasPendientes")
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listaCuentas':
            ancho = $("#divTablaListaCuentas").width();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1081&parametros=";
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPlan'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPlan'));
            add_valores_a_mandar(valorAtributo('cmbIdAnio'));
            add_valores_a_mandar(valorAtributo('cmbIdAnio'));
            add_valores_a_mandar(valorAtributo('cmbIdMes'));
            add_valores_a_mandar(valorAtributo('cmbIdMes'));
            add_valores_a_mandar(valorAtributo('cmbIdAnioPrestacion'));
            add_valores_a_mandar(valorAtributo('cmbIdAnioPrestacion'));
            add_valores_a_mandar(valorAtributo('cmbIdMesPrestacion'));
            add_valores_a_mandar(valorAtributo('cmbIdMesPrestacion'));
            add_valores_a_mandar(valorAtributo('cmbIdTipo'));
            add_valores_a_mandar(valorAtributo('cmbIdTipo'));
            add_valores_a_mandar(valorAtributo('txtCuenta'));
            add_valores_a_mandar(valorAtributo('txtCuenta'));
            add_valores_a_mandar(valorAtributo('txtRemision'));
            add_valores_a_mandar(valorAtributo('txtRemision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['#', '', 'ID_ENVIO', 'ENVIO', 'ID_PLAN', 'PLAN', 'ID_SEDE', 'SEDE',
                    'ANIO', 'MES', 'FACTURACION', 'ANIO_PRESTACION', 'MES_PRESTACION',
                    'PRESTACION', 'CUENTA', 'REMISION', 'VALOR', 'TIPO', 'USUARIO', 'CREA', 'RADICACION', 'ID_ESTADO'],
                colModel: [
                    { name: '#', index: '#', hidden: true },
                    {
                        name: "", index: "", width: anchoP(ancho, 1), align: 'center', formatter: function () {
                            return '<img src="/clinica/utilidades/imagenes/icons/icons/pencil-square.svg" width="18px" height="18px" align="middle">'
                        }
                    },
                    { name: 'ID_ENVIO', index: 'ID_ENVIO', hidden: true },
                    { name: 'ENVIO', index: 'ENVIO', width: anchoP(ancho, 2) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 12) },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: anchoP(ancho, 10) },
                    { name: 'ANIO', index: 'ANIO', hidden: true },
                    { name: 'MES', index: 'MES', hidden: true },
                    { name: 'FACTURACION', index: 'FACTURACION', width: anchoP(ancho, 4) },
                    { name: 'ANIO_PRESTACION', index: 'ANIO_PRESTACION', hidden: true },
                    { name: 'MES_PRESTACION', index: 'MES_PRESTACION', hidden: true },
                    { name: 'FACTURACION', index: 'FACTURACION', width: anchoP(ancho, 4) },
                    { name: 'CUENTA', index: 'CUENTA', width: anchoP(ancho, 3) },
                    { name: 'REMISION', index: 'REMISION', width: anchoP(ancho, 3) },
                    { name: 'VALOR', index: 'VALOR', width: anchoP(ancho, 3) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 2) },
                    { name: 'USUARIO', index: 'USUARIO', width: anchoP(ancho, 5) },
                    { name: 'CREA', index: 'CREA', width: anchoP(ancho, 4) },
                    { name: 'RADICACION', index: 'RADICACION', width: anchoP(ancho, 4) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                ],
                height: 280,
                autowidth: true,
                multiselect: true,
                multiboxonly: true,
                onCellSelect: function (rowid, iCol, cellcontent, e) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo("txtCuentaActual", datosRow.ID_ENVIO)
                    setTimeout(() => {
                        if (iCol == 2) {
                            asignaAtributo('lblIdPlanVentanita', datosRow.ID_PLAN, 0);
                            asignaAtributo('lblPlanVentanita', datosRow.PLAN, 0);
                            asignaAtributo('lblRangoVentanita', datosRow.ANIO + '-' + datosRow.MES, 0);
                            asignaAtributo('lblPosVentanita', datosRow.TIPO, 0);
                            asignaAtributo('lblIdEnvioVentanita', datosRow.ID_ENVIO, 0);
                            asignaAtributo('lblCuentaVentanita', datosRow.CUENTA, 0);
                            asignaAtributo('lblRemisionVentanita', datosRow.REMISION, 0);
                            mostrar('divVentanitaGeneraRips');

                            setTimeout(() => {
                                totalEnvioRips(valorAtributo('lblIdEnvioVentanita'))
                            }, 200);
                        }
                        setTimeout(() => {
                            buscarFacturacion("listGrillaRips");
                        }, 400);
                    }, 100);
                },
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo("txtCuentaActual", datosRow.ID_ENVIO)
                    setTimeout(() => {
                        buscarFacturacion("listGrillaRips");
                    }, 200);
                },
                gridComplete: function () {
                    var datos = $("#listaCuentas").getDataIDs();
                    $('#listGrillaRips').jqGrid('clearGridData')
                    for (i = 1; i <= datos.length; i++) {
                        if (valorAtributo("txtCuentaActual") == $("#listaCuentas").getRowData(i).ID_ENVIO) {
                            $("#listaCuentas").jqGrid("setSelection", i, true);
                            return;
                        }
                    }
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listaOrdenesSinFactura':
            setTimeout(() => {
                ancho = $("#divProgramacionPendiente").width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=1077&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['#', 'PROGRAMACION', 'PROCEDIMIENTO', 'CANTIDAD', 'CITA', 'ESTADO', ''],
                    colModel: [
                        { name: '#', index: '#', width: anchoP(ancho, 1) },
                        { name: 'PROGRAMACION', index: 'FOLIO', width: anchoP(ancho, 2) },
                        { name: 'PROCEDIMIENTO', index: 'TIPO', width: anchoP(ancho, 8) },
                        { name: 'CANTIDAD', index: 'FECHA', width: anchoP(ancho, 2) },
                        { name: 'CITA', index: 'FECCITAHA', width: anchoP(ancho, 2) },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 4) },
                        {
                            name: "", index: "", width: anchoP(ancho, 1), align: 'center', formatter: function () {
                                return '<img onclick="_verFormatoOrdenPendiente()" src="/clinica/utilidades/imagenes/icons/icons/search.svg" width="18px" height="18px" align="middle">'
                            }
                        },
                    ],
                    height: 280,
                    width: ancho,
                    multiselect: true,
                    multiboxonly: true,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 200);
            break;

        case 'listaPlanEvolucionFactura':
            setTimeout(() => {
                factura_detalle = $("#listProcedimientosDeFactura").jqGrid('getGridParam', 'selrow')

                id_factura_detalle = $("#listProcedimientosDeFactura").getRowData(factura_detalle).ID

                ancho = $("#divProgramacionFactura").width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=1080&parametros=";
                add_valores_a_mandar(id_factura_detalle);
                add_valores_a_mandar(id_factura_detalle);
                add_valores_a_mandar(id_factura_detalle);

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['#', 'TIPO_ELEMENTO', 'ID_TIPO_ELEMENTO', 'ID_FACTURA_DETALLE', 'ID',
                        'DESCRIPCION', 'CANTIDAD', 'FECHA', 'PROFESIONAL', 'CITA', 'ESTADO', ''],
                    colModel: [
                        { name: '#', index: '#', width: anchoP(ancho, 1) },
                        { name: 'TIPO_ELEMENTO', index: 'TIPO_ELEMENTO', width: anchoP(ancho, 2) },
                        { name: 'ID_TIPO_ELEMENTO', index: 'ID_TIPO_ELEMENTO', width: anchoP(ancho, 8) },
                        { name: 'ID_FACTURA_DETALLE', index: 'ID_FACTURA_DETALLE', width: anchoP(ancho, 8) },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                        { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 2) },
                        { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 8) },
                        { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 2) },
                        { name: 'PROFESIONAL', index: 'PROFESIONAL', width: anchoP(ancho, 2) },
                        { name: 'CITA', index: 'CITA', width: anchoP(ancho, 2) },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 4) },
                        {
                            name: "", index: "", width: anchoP(ancho, 1), align: 'center', formatter: function () {
                                return '<img onclick="_verFormatoOrdenPendiente()" src="/clinica/utilidades/imagenes/icons/icons/search.svg" width="18px" height="18px" align="middle">'
                            }
                        },
                    ],
                    height: 280,
                    width: ancho,
                    multiselect: true,
                    multiboxonly: true,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 200);
            break;

        case 'listaHcSinFactura':
            $("#listaHcSinFactura").jqGrid("clearGridData");
            setTimeout(() => {
                ancho = $("#divHcPendiente").width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=1065&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())
                add_valores_a_mandar(valorAtributo("lblIdAdmision"));
                add_valores_a_mandar(IdSede())

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['#', 'TIPO', 'ID_TIPO', 'ID',
                        'DESCRIPCION', 'CANT', 'FECHA', 'PROFESIONAL', 'CITA', 'FECHA CITA', 'ESTADO', ''],
                    colModel: [
                        { name: '#', index: '#', width: anchoP(ancho, 1) },
                        { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 3) },
                        { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 1) },
                        { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 8) },
                        { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 1) },
                        { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 3) },
                        { name: 'PROFESIONAL', index: 'PROFESIONAL', width: anchoP(ancho, 5) },
                        { name: 'CITA', index: 'CITA', width: anchoP(ancho, 2) },
                        { name: 'FECHA CITA', index: 'FECHA CITA', width: anchoP(ancho, 3) },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 2) },
                        {
                            name: "", index: "", width: anchoP(ancho, 1), align: 'center', formatter: function () {
                                return '<img onclick="verFormatoOrdenPendiente()" src="/clinica/utilidades/imagenes/icons/icons/search.svg" width="18px" height="18px" align="middle">'
                            }
                        },
                    ],
                    height: 280,
                    width: ancho,
                    multiselect: true,
                    multiboxonly: true,
                    grouping: true,
                    groupingView: {
                        groupField: ['TIPO'],
                        groupText: ['<b>{0} ({1})</b>']
                    },
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 200);
            break;

        case 'listaFacturaDetalleEvolucion':
            $("#listaFacturaDetalleEvolucion").jqGrid("clearGridData");
            setTimeout(() => {
                factura_detalle = $("#listProcedimientosDeFactura").jqGrid('getGridParam', 'selrow')

                id_factura_detalle = $("#listProcedimientosDeFactura").getRowData(factura_detalle).ID

                ancho = $("#divHcFactura").width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=1063&parametros=";
                add_valores_a_mandar(id_factura_detalle);
                add_valores_a_mandar(id_factura_detalle);
                add_valores_a_mandar(id_factura_detalle);

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['#', 'TIPO', 'ID_TIPO', 'ID_FACTURA_DETALLE', 'ID',
                        'DESCRIPCION', 'CANT', 'FECHA', 'PROFESIONAL', 'CITA', 'FECHA CITA', 'ESTADO', ''],
                    colModel: [
                        { name: '#', index: '#', width: anchoP(ancho, 1) },
                        { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 3) },
                        { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                        { name: 'ID_FACTURA_DETALLE', index: 'ID_FACTURA_DETALLE', hidden: true },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 1) },
                        { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 8) },
                        { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 1) },
                        { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 3) },
                        { name: 'PROFESIONAL', index: 'PROFESIONAL', width: anchoP(ancho, 5) },
                        { name: 'CITA', index: 'CITA', width: anchoP(ancho, 2) },
                        { name: 'FECHA CITA', index: 'FECHA CITA', width: anchoP(ancho, 3) },
                        { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 2) },
                        {
                            name: "", index: "", width: anchoP(ancho, 2), align: 'center', formatter: function () {
                                return '<img onclick="verFormatoOrdenFacturada()" src="/clinica/utilidades/imagenes/icons/icons/search.svg" width="18px" height="18px" align="middle">'
                            }
                        },
                    ],
                    height: 280,
                    width: ancho,
                    multiselect: true,
                    multiboxonly: true,
                    grouping: true,
                    groupingView: {
                        groupField: ['TIPO'],
                        groupText: ['<b>{0} ({1})</b>']
                    },
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 200);
            break;

        case 'listProgramacionFacturaDetalle':
            factura_detalle = $("#listProcedimientosDeFactura").jqGrid('getGridParam', 'selrow')

            id_factura_detalle = $("#listProcedimientosDeFactura").getRowData(factura_detalle).ID

            ancho = 1040;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=832&parametros=";
            add_valores_a_mandar(id_factura_detalle);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['#', 'ID', 'id_procedimiento', 'CUPS', 'PROCEDIMIENTO', 'ESTADO', 'FECHA PROGRAMACION', 'ENVIADO'],
                colModel: [
                    { name: '#', index: '#', width: anchoP(ancho, 1) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 2) },
                    { name: 'id_procedimiento', index: 'id_procedimiento', hidden: true },
                    { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 2) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 10) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 4) },
                    { name: 'FECHA_PROGRAMACION', index: 'FECHA PROGRAMACION', width: anchoP(ancho, 4) },
                    { name: 'ENVIADO', index: 'ENVIADO', width: anchoP(ancho, 4) },
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listOrdenesProgramadas':
            ancho = 1040;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=838&parametros=";
            //add_valores_a_mandar(valorAtributo('lblIdAdmision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['#', 'ID', 'id_procedimiento', 'CUPS', 'PROCEDIMIENTO', 'ESTADO', 'FECHA PROGRAMACION'],
                colModel: [
                    { name: '#', index: '#', width: anchoP(ancho, 1) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 2) },
                    { name: 'id_procedimiento', index: 'id_procedimiento', hidden: true },
                    { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 2) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 10) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 4) },
                    { name: 'FECHA_PROGRAMACION', index: 'FECHA PROGRAMACION', width: anchoP(ancho, 4) },
                ],
                height: 100,
                width: ancho,
                multiselect: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaDocumentosTipificacion':
            ancho = 1040;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=734&parametros=";
            add_valores_a_mandar(valorAtributo('txtNombreDocumento'));
            add_valores_a_mandar(valorAtributo('cmbPOS'));
            add_valores_a_mandar(valorAtributo('cmbPOS'));
            add_valores_a_mandar(valorAtributo('cmbPOS'));
            add_valores_a_mandar(valorAtributo('cmbPOS'));
            add_valores_a_mandar(valorAtributo('cmbAplicaIPS'));
            add_valores_a_mandar(valorAtributo('cmbAplicaIPS'));
            add_valores_a_mandar(valorAtributo('cmbVigente'));
            add_valores_a_mandar(valorAtributo('cmbVigente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'Id', 'Nombre', 'Prefijo', 'POS', 'NO POS', 'Aplica IPS', 'Vigente'],
                colModel: [
                    { name: 'C', index: 'C', hidden: true },
                    { name: 'Id', index: 'ID', width: anchoP(ancho, 2) },
                    { name: 'NOMRE', index: 'Nombre', width: anchoP(ancho, 10) },
                    { name: 'PREFIJO', index: 'Prefijo', width: anchoP(ancho, 2) },
                    { name: 'POS', index: 'POS', width: anchoP(ancho, 2) },
                    { name: 'NOPOS', index: 'NO POS', width: anchoP(ancho, 2) },
                    { name: 'APLICA_IPS', index: 'Aplica IPS', width: anchoP(ancho, 3) },
                    { name: 'VIGENTE', index: 'Vigente', width: anchoP(ancho, 3) },
                ],
                height: 530,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listArchivosTificacion':
            setTimeout(() => {
                ancho = $("#divTipificacion").width();
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=296&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdFacturaAdjuntos'));

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['C', 'ID FACTURA', 'PDF', 'TIPO ARCHIVO', 'FECHA CREACION'],
                    colModel: [
                        { name: 'C', index: 'C', hidden: true },
                        { name: 'ID_FACTURA', index: 'ID FACTURA', width: anchoP(ancho, 10) },
                        { name: 'PDF', index: 'PDF', width: anchoP(ancho, 1) },
                        { name: 'TIPO_ARCHIVO', index: 'TIPO ARCHIVO', width: anchoP(ancho, 10) },
                        { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 10) },
                    ],
                    height: 200,
                    width: ancho,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    },
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            }, 300);
            break;

        case 'listArticulosPlan':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=154&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodBus'));
            add_valores_a_mandar(valorAtributo('txtNomBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID_ARTICULO', 'NOMBRE_GENERICO', 'NOMBRE_COMERCIAL', 'PORC_IVA', 'PORC_IVA_VENTA', 'POS', 'ID_TIPO_SERVICIO', 'TIPO_SERVICIO', 'ID_CENTRO_COSTO', 'CENTRO_COSTO', 'ID_GRUPO_CENTRO_COSTO', 'GRUPO_CENTRO_COSTO'],
                colModel: [
                    { name: 'C', index: 'C', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 5) },
                    { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 15) },
                    { name: 'NOMBRE_COMERCIAL', index: 'NOMBRE_COMERCIAL', width: anchoP(ancho, 15) },
                    { name: 'PORC_IVA', index: 'PORC_IVA', width: anchoP(ancho, 5) },
                    { name: 'PORC_IVA_VENTA', index: 'PORC_IVA_VENTA', hidden: true },
                    { name: 'POS', index: 'POS', width: anchoP(ancho, 5) },
                    { name: 'ID_TIPO_SERVICIO', index: 'ID_TIPO_SERVICIO', hidden: true },
                    { name: 'TIPO_SERVICIO', index: 'TIPO_SERVICIO', width: anchoP(ancho, 10) },
                    { name: 'ID_CENTRO_COSTO', index: 'ID_CENTRO_COSTO', hidden: true },
                    { name: 'CENTRO_COSTO', index: 'CENTRO_COSTO', width: anchoP(ancho, 10) },
                    { name: 'ID_GRUPO_CENTRO_COSTO', index: 'ID_GRUPO_CENTRO_COSTO', hidden: true },
                    { name: 'GRUPO_CENTRO_COSTO', index: 'GRUPO_CENTRO_COSTO', width: anchoP(ancho, 10) }
                ],
                height: 300,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO, 1)
                    asignaAtributo('txtNombreArticulo', datosRow.NOMBRE_GENERICO, 0)
                    asignaAtributo('txtNombreComercial', datosRow.NOMBRE_COMERCIAL, 0)
                    asignaAtributo('txtIVA', datosRow.PORC_IVA, 0)
                    asignaAtributo('cmbIVAVenta', datosRow.PORC_IVA_VENTA, 0)
                    asignaAtributo('cmbPOS', datosRow.POS, 0)
                    asignaAtributoCombo('cmbTipoServicio', datosRow.ID_TIPO_SERVICIO, datosRow.TIPO_SERVICIO)
                    asignaAtributoCombo('cmbIdCentroCosto', datosRow.ID_CENTRO_COSTO, datosRow.CENTRO_COSTO)
                    asignaAtributoCombo('cmbIdGrupoCentroCosto', datosRow.ID_GRUPO_CENTRO_COSTO, datosRow.GRUPO_CENTRO_COSTO)



                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listSoloFacturas':
            ancho = ($('#drag' + ventanaActual.num).find("#listSoloFacturas").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=73&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(IdEmpresa());
            add_valores_a_mandar(IdSede());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', '', 'ID_FACTURA', 'ID_ADMINISTRADORA',
                    'ADMINISTRADORA', 'NO_FACTURA', 'ID_PLAN', 'PLAN_DESCRIPCION', 'ID_TIPO_AFILIADO', 'NOM_TIPO_AFILIADO', 'ID_RANGO', 'NOM_RANGO',
                    'ID_REGIMEN', 'NOMBRE_REGIMEN', 'ID_ESTADO', 'ESTADO', 'FECHA', 'NO_AUTORIZACION', 'USUARIO CREA', 'SW_DIAGNOSTICO', 'DIAGNOSTICO',
                    'ID_SEDE', 'ID_TIPO_PLAN', 'TIPO_PLAN'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    {
                        name: "", index: '', width: anchoP(ancho, 1), align: 'center', formatter: function () {
                            return '<img id="editarAuto" src="/clinica/utilidades/imagenes/icons/icons/link.svg" width="18px" height="18px" align="middle">'
                        }
                    },
                    { name: 'ID_FACTURA', index: 'ID', width: anchoP(ancho, 5) },
                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'ADMINISTRADORA', index: 'ADMINISTRADORA', width: anchoP(ancho, 7) },
                    { name: 'NO_FACTURA', index: 'NO_FACTURA', width: anchoP(ancho, 5) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN_DESCRIPCION', index: 'PLAN_DESCRIPCION', width: anchoP(ancho, 11) },
                    { name: 'ID_TIPO_AFILIADO', index: 'ID_TIPO_AFILIADO', hidden: true },
                    { name: 'NOM_TIPO_AFILIADO', index: 'NOM_TIPO_AFILIADO', hidden: true },
                    { name: 'ID_RANGO', index: 'ID_RANGO', hidden: true },
                    { name: 'NOM_RANGO', index: 'NOM_RANGO', hidden: true },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'NOMBRE_REGIMEN', index: 'NOMBRE_REGIMEN', hidden: true },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 5) },
                    { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 6) },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', hidden: true },
                    { name: 'USUARIO_CREA', index: 'USUARIO_CREA', width: anchoP(ancho, 5) },
                    { name: 'SW_DIAGNOSTICO', index: 'SW_DIAGNOSTICO', hidden: true },
                    { name: 'DIAGNOSTICO', index: 'DIAGNOSTICO', hidden: true },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'ID_TIPO_PLAN', index: 'ID_TIPO_PLAN', hidden: true },
                    { name: 'TIPO_PLAN', index: 'TIPO_PLAN', hidden: true }
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    ocultar('divInfoFacturaRecibos')

                    asignaAtributo('lblIdSedeAdmision', datosRow.ID_SEDE, 0)
                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('lblIdEpsPlan', datosRow.ID_ADMINISTRADORA, 0)
                    asignaAtributo('lblIdPlan', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblNomPlan', datosRow.PLAN_DESCRIPCION, 0)
                    asignaAtributo('lblIdRegimen', datosRow.ID_REGIMEN, 0)
                    asignaAtributo('lblNomRegimen', datosRow.NOMBRE_REGIMEN, 0)
                    asignaAtributo('lblIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, 0)
                    asignaAtributo('lblNomTipoAfiliado', datosRow.NOM_TIPO_AFILIADO, 0)
                    asignaAtributo('lblIdRango', datosRow.ID_RANGO, 0)
                    asignaAtributo('lblNomRango', datosRow.NOM_RANGO, 0)
                    asignaAtributo('lblUsuarioCrea', datosRow.USUARIO_CREA, 0)
                    asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)

                    asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.ADMINISTRADORA), 0)
                    asignaAtributo('txtIdAdminisAdmision', datosRow.ID_ADMINISTRADORA, 0)


                    asignaAtributoCombo('cmbIdTipoRegimen', datosRow.ID_REGIMEN, datosRow.NOMBRE_REGIMEN);
                    asignaAtributoCombo('cmbIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, datosRow.NOM_TIPO_AFILIADO);
                    asignaAtributoCombo('cmbIdRango', datosRow.ID_RANGO, datosRow.NOM_RANGO);
                    asignaAtributoCombo('cmbIdPlan', datosRow.ID_PLAN, datosRow.PLAN_DESCRIPCION);
                    asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblNomPlanContratacion', datosRow.PLAN_DESCRIPCION, 0)
                    asignaAtributo('txtDiagnostico', datosRow.SW_DIAGNOSTICO, 0)
                    asignaAtributo('txtIdDx', datosRow.DIAGNOSTICO, 0)
                    asignaAtributo('lblTipoPlan', datosRow.TIPO_PLAN, 0)
                    limpiarCasillasCrearFacturas()

                    traerobservaciones(datosRow.ID_FACTURA)

                    buscarFacturacion('listArticulosDeFactura')
                    buscarFacturacion('listProcedimientosDeFactura')
                    buscarFacturacion('listaOtrosDesatallesFactura')
                    setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')", 200);
                },
                onCellSelect: function (rowid, iCol, cellcontent, e) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    if (iCol == 1) {
                        console.log('col:',iCol)
                        mostrar('divNotas')
                        asignaAtributo("lblIdFacturaAsociarCita", datosRow.ID_FACTURA, 1);
                        setTimeout(() => {
                            buscarAGENDA('notas')
                            buscarAGENDA('notasFacturas')
                        }, 300);
                    }
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listFacturasVentas':

            ancho = ($('#drag' + ventanaActual.num).find("#listFacturasVentas").width()) - 40;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=115" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_FACTURA', 'ID_ADMINISTRADORA',
                    'ADMINISTRADORA', 'NO_FACTURA', 'ID_PLAN', 'PLAN', 'ID_TIPO_AFILIADO', 'NOM_TIPO_AFILIADO', 'ID_RANGO', 'RANGO',
                    'ID_REGIMEN', 'REGIMEN', 'ID_ESTADO_FACTURA', 'ESTADO_FACTURA', 'FECHA_FACTURA', 'USUARIO CREA',
                    'ID_DOC', 'FECHA_CREA', 'FECHA_CIERRE', 'ID_BODEGA', 'BODEGA', 'ID_ESTADO_DOC', 'ESTADO_DOC', 'ID_TIPO_PAGO', 'TIPO_PAGO', 'ID_RECIBO', 'OBSERVACIONES'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', width: anchoP(ancho, 4) },
                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'ADMINISTRADORA', index: 'ADMINISTRADORA', hidden: true },
                    { name: 'NO_FACTURA', index: 'NO_FACTURA', width: anchoP(ancho, 4) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 10) },
                    { name: 'ID_TIPO_AFILIADO', index: 'ID_TIPO_AFILIADO', hidden: true },
                    { name: 'NOM_TIPO_AFILIADO', index: 'NOM_TIPO_AFILIADO', hidden: true },
                    { name: 'ID_RANGO', index: 'ID_RANGO', hidden: true },
                    { name: 'RANGO', index: 'RANGO', hidden: true },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'REGIMEN', index: 'REGIMEN', hidden: true },
                    { name: 'ID_ESTADO_FACTURA', index: 'ID_ESTADO_FACTURA', hidden: true },
                    { name: 'ESTADO_FACTURA', index: 'ESTADO_FACTURA', width: anchoP(ancho, 5) },
                    { name: 'FECHA_FACTURA', index: 'FECHA_FACTURA', width: anchoP(ancho, 6) },
                    { name: 'USUARIO_CREA', index: 'USUARIO_CREA', width: anchoP(ancho, 5) },
                    { name: 'ID_DOC', index: 'ID_DOC', width: anchoP(ancho, 3) },
                    { name: 'FECHA_CREA', index: 'FECHA_CREA', width: anchoP(ancho, 5) },
                    { name: 'FECHA_CIERRE', index: 'FECHA_CIERRE', width: anchoP(ancho, 5) },
                    { name: 'ID_BODEGA', index: 'ID_BODEGA', hidden: true },
                    { name: 'BODEGA', index: 'BODEGA', width: anchoP(ancho, 5) },
                    { name: 'ID_ESTADO_DOC', index: 'ID_ESTADO_DOC', hidden: true },
                    { name: 'ESTADO_DOC', index: 'ESTADO_DOC', width: anchoP(ancho, 5) },
                    { name: 'ID_TIPO_PAGO', index: 'ID_TIPO_PAGO', hidden: true },
                    { name: 'TIPO_PAGO', index: 'TIPO_PAGO', hidden: true },
                    { name: 'ID_RECIBO', index: 'ID_RECIBO', hidden: true },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true }

                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    selectRowFacturacion(arg, rowid)
                    ocultar('divInfoFacturaRecibos')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaProcedimientosContratados':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=750&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'PLAN', 'ID_PROC', 'PROCEDIMIENTO', 'SITIO', 'LAT', 'VALOR UNITARIO', 'ORDEN', '%', 'VALOR FINAL', 'EXCEPCIONES', 'ID_SITIO', 'SW_PROGRAMADOS'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 5) },
                    { name: 'ID_PROC', index: 'ID_PROC', width: anchoP(ancho, 4) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 26) },
                    { name: 'SITIO', index: 'SITIO', width: anchoP(ancho, 6) },
                    { name: 'LAT', index: 'LAT', width: anchoP(ancho, 3) },
                    { name: 'VALOR UNITARIO', index: 'VALOR UNITARIO', width: anchoP(ancho, 8) },
                    { name: 'ORDEN', index: 'ORDEN', width: anchoP(ancho, 4) },
                    { name: 'PORC', index: 'PORC', width: anchoP(ancho, 2) },
                    { name: 'VALOR FINAL', index: 'VALOR FINAL', width: anchoP(ancho, 6) },
                    { name: 'EXCEPCIONES', index: 'EXCEPCIONES', width: anchoP(ancho, 6) },
                    { name: 'ID_SITIO', index: 'ID_SITIO', hidden: true },
                    { name: 'SW_PROGRAMADOS', index: 'SW_PROGRAMADOS', hidden: true }
                ],
                height: 100,
                width: 1100,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdSitioLiquidacionVentanita', datosRow.ID_SITIO, 0);
                    asignaAtributo('lblIdProcedimientoLiquidacionVentanita', datosRow.ID_PROC, 0);
                    asignaAtributo('lblProcedimientoLiquidacionVentanita', datosRow.PROCEDIMIENTO, 0);
                    asignaAtributo('lblIdProgramadosVentanita', datosRow.SW_PROGRAMADOS, 0);

                    mostrar('divVentanitaLiquidacionContratadasDetalle')
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;



        case 'listGrillaAsistenciaPacientes':

            ancho = ($('#drag' + ventanaActual.num).find("#idTablaAsistenciaPaciente").width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=656" + "&parametros=";

            add_valores_a_mandar(valorAtributo('cmbExito'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadCita'));
            //add_valores_a_mandar(valorAtributo('cmbIdTipoServicio'));
            add_valores_a_mandar(valorAtributo('cmbIdProfesionalesCita'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('cmbPreferencial'));
            add_valores_a_mandar(valorAtributo('txtBusFechaDesde'));
            add_valores_a_mandar(valorAtributo('txtBusFechaHasta'));
            add_valores_a_mandar(valorAtributo('cmbAsiste'));
            add_valores_a_mandar(valorAtributo('cmbAdmision'));

            //Se limpia los campos de editar cuando se hace una nueva busqueda
            asignaAtributo('lblIdentificacionEdit', '', 0);
            asignaAtributo('lblIdCitaEdit', '', 0);
            asignaAtributo('lblPacienteEdit', '', 0);
            asignaAtributo('cmbIdPreferencialEdit', 'NO', 0);
            asignaAtributo('cmbIdAsisteEdit', 'NO', 0);
            asignaAtributo('txtObservacionEdit', '', 0);
            asignaAtributo('cmbEstadoCitaEdit', '', 0);
            asignaAtributo('cmbMotivoConsultaEdit', '', 0);


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'FECHA_CITA', 'HORA', 'MIN_DURACION',
                    'ID PACIENTE', 'TIPO_ID', 'IDENTIFICACION', 'NOMBRE PACIENTE',
                    'FECHA_ELABORO', 'ID_ESTADO_AGENDA', 'TIPO', 'NOMTIPO',
                    'NOM_ADMINISTRADORA', 'NO_REMISION', 'OBSERVACION', 'ID_ESTADO', 'ESTADO',
                    'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'MEDICO', 'USUARIO_MODIFICA', 'ASISTE',
                    'ASISTE_USUARIO', 'ASISTE_FECHA', 'PREFERENCIAL', 'OBSERVACION', 'ID_MOTIVO', 'MOTIVO',
                    'ID_MOTIVO_CLASE', 'MOTIVO_CLASE'

                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'FECHA_CITA', index: 'FECHA_CITA', width: anchoP(ancho, 8) },
                    { name: 'HORA', index: 'HORA', width: anchoP(ancho, 5) },
                    { name: 'MIN_DURACION', index: 'MIN_DURACION', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO_ID', index: 'TIPO_ID', width: anchoP(ancho, 2) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 9) },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 25) },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                    { name: 'ID_ESTADO_AGENDA', index: 'ID_ESTADO_AGENDA', hidden: true },
                    { name: 'TIPO', index: 'TIPO', hidden: true },
                    { name: 'NOMTIPO', index: 'NOMTIPO', width: anchoP(ancho, 10) },
                    { name: 'NOM_ADMINISTRADORA', index: 'NOM_ADMINISTRADORA', hidden: true },
                    { name: 'NO_REMISION', index: 'NO_REMISION', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 8) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 8) },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true },
                    // { name: 'SUB_ESPECIALIDAD', index: 'SUB_ESPECIALIDAD', hidden: true },
                    { name: 'MEDICO', index: 'MEDICO', width: anchoP(ancho, 10) },
                    { name: 'USUARIO_MODIFICA', index: 'USUARIO_MODIFICA', hidden: true },
                    { name: 'ASISTE', index: 'ASISTE', width: anchoP(ancho, 9) },
                    { name: 'ASISTE_USUARIO', index: 'ASISTE_USUARIO', hidden: true },
                    { name: 'ASISTE_FECHA', index: 'ASISTE_FECHA', hidden: true },
                    { name: 'PREFERENCIAL', index: 'PREFERENCIAL', width: anchoP(ancho, 5) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', hidden: true },
                    { name: 'ID_MOTIVO', index: 'ID_MOTIVO', hidden: true },
                    { name: 'MOTIVO', index: 'MOTIVO', hidden: true },
                    { name: 'ID_MOTIVO_CLASE', index: 'ID_MOTIVO_CLASE', hidden: true },
                    { name: 'MOTIVO_CLASE', index: 'MOTIVO_CLASE', hidden: true }
                ],
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdCitaEdit', datosRow.ID, 0);
                    asignaAtributo('lblIdentificacionEdit', datosRow.ID_PACIENTE, 0);
                    asignaAtributo('lblPacienteEdit', datosRow.NOMBRE_PACIENTE, 0);

                    var asistencia = datosRow.ASISTE.split(";");

                    asignaAtributo('cmbIdAsisteEdit', asistencia[1], 0);
                    asignaAtributo('cmbIdPreferencialEdit', datosRow.PREFERENCIAL, 0);
                    asignaAtributo('txtObservacionEdit', datosRow.OBSERVACION, 0);

                    asignaAtributo('cmbEstadoCitaEdit', datosRow.ID_ESTADO, 0);
                    //asignaAtributo('cmbMotivoConsultaEdit', datosRow.MOTIVO, 0);

                    asignaAtributoCombo('cmbMotivoConsultaEdit', datosRow.ID_MOTIVO, datosRow.MOTIVO);
                    asignaAtributoCombo('cmbMotivoConsultaClaseEdit', datosRow.ID_MOTIVO_CLASE, datosRow.MOTIVO_CLASE);
                    asignaAtributo('txtIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listMedicamentosConsumidos':
            limpiarDivEditarJuan(arg);
            ancho = 1100
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=566&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPlan'));
            add_valores_a_mandar(valorAtributo('lblIdAdmision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'POST',
                colNames: ['contador', 'ID', 'ID_EVOLUCION', 'ID_ARTICULO', 'NOMBRE GENERICO', 'CANTIDAD', 'USUARIO', 'FECHA_ELABORO', 'ID_PROCEDIMIENTO', 'CONTRATADO'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_EVOLUCION', index: 'ID_EVOLUCION', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 35) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 5) },
                    { name: 'USUARIO', index: 'USUARIO', width: anchoP(ancho, 5) },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', width: anchoP(ancho, 12) },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 12) },
                    { name: 'CONTRATADO', index: 'CONTRATADO', width: anchoP(ancho, 12) }
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtIdProcedimientoCex', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.NOMBRE, 0)
                    asignaAtributo('cmbCantidad', datosRow.CANTIDAD, 0);
                    traerValorUnitarioProcedimientoCuenta();
                    ocultar('divVentanitaConsumoMedicamento')

                },
                height: 350,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaRecibosDesdeAdmisiones':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=571" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Num Recibo', 'id_paciente', 'Nombre Paciente', 'Valor', 'Cantidad', 'id_concepto', 'Concepto', 'observacion', 'id_caja', 'Caja',
                    'usuario_elaboro', 'Fecha elaboro', 'id_estado', 'Estado', 'usuario_anulo', 'fecha_anulo', 'id_motivo_anulo', 'id_factura'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'num_Recibo', index: 'num_Recibo', width: anchoP(ancho, 5) },
                    { name: 'id_paciente', index: 'id_paciente', hidden: true },
                    { name: 'nomPaciente', index: 'nomPaciente', width: anchoP(ancho, 10) },

                    { name: 'valor', index: 'valor', width: anchoP(ancho, 5) },
                    { name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 5) },
                    { name: 'id_concepto', index: 'id_concepto', hidden: true },
                    { name: 'nom_concepto', index: 'nom_concepto', width: anchoP(ancho, 10) },
                    { name: 'observacion', index: 'observacion', hidden: true },
                    { name: 'id_caja', index: 'id_caja', hidden: true },
                    { name: 'nom_caja', index: 'nom_caja', width: anchoP(ancho, 5) },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', hidden: true },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 5) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 5) },
                    { name: 'usuario_anulo', index: 'usuario_anulo', hidden: true },
                    { name: 'fecha_anulo', index: 'fecha_anulo', hidden: true },
                    { name: 'id_motivo_anulo', index: 'id_motivo_anulo', hidden: true },
                    { name: 'id_factura', index: 'id_factura', width: anchoP(ancho, 5) }
                ],
                height: 100,
                width: 1100,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdRecibo', datosRow.num_Recibo, 0)
                    asignaAtributo('txtIdEditPacienteRecibo', datosRow.id_paciente + '-' + datosRow.nomPaciente, 0)
                    asignaAtributo('txtValorRecibo', datosRow.valor, 0)
                    asignaAtributo('cmbCantidadRecibo', datosRow.cantidad, 0)

                    asignaAtributo('cmbIdConceptoRecibo', datosRow.id_concepto, 0)
                    asignaAtributo('txtObservacionRecibo', datosRow.observacion, 0)
                    asignaAtributo('lblIdUsuarioRecibo', datosRow.usuario_elaboro, 0)
                    asignaAtributo('lblFechaElaboroRecibo', datosRow.fecha_elaboro, 0)
                    asignaAtributo('lblIdEstadoRecibo', datosRow.id_estado, 0)
                    asignaAtributo('lblEstadoRecibo', datosRow.estado, 0)

                    asignaAtributo('lblIdUsuarioAnulo', datosRow.usuario_anulo, 0)
                    asignaAtributo('lblFechaAnulo', datosRow.fecha_anulo, 0)
                    asignaAtributo('cmbIdMotivoAnulacionRecibo', datosRow.id_motivo_anulo, 0)
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRecibos':
            ancho = 990;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=569&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNumRecibo'));
            add_valores_a_mandar(valorAtributo('txtBusNumRecibo'));
            add_valores_a_mandar(valorAtributo('cmbIdPLan'));
            add_valores_a_mandar(valorAtributo('cmbIdPLan'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('txtFechaRecibo'));
            add_valores_a_mandar(valorAtributo('txtFechaRecibo'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'Numero', 'identificacion', 'id_paciente', 'Nombre Paciente', 'id_plan', 'Plan', 'Valor', 'Cantidad',
                    'id_concepto', 'Concepto', 'observacion', 'id_caja', 'Caja', 'usuario_elaboro', 'Fecha elaboro', 'id_estado',
                    'Estado', 'usuario_anulo', 'fecha_anulo', 'id_motivo_anulo', 'id_factura', 'Auditoria', 'id_estado_auditoria', 'id_recibos_consecutivos',
                    'administradora', 'regimen', 'valor_unidad', 'sw_mostrar_en_disponibles', 'id_medio_pago'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_recibo', index: 'id_recibo', hidden: true },
                    { name: 'numero', index: 'numero', width: anchoP(ancho, 3) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 6) },
                    { name: 'id_paciente', index: 'id_paciente', hidden: true },
                    { name: 'nombre_paciente', index: 'nombre_paciente', width: anchoP(ancho, 20) },
                    { name: 'id_plan', index: 'id_plan', hidden: true },
                    { name: 'plan', index: 'plan', width: anchoP(ancho, 18) },
                    { name: 'valor', index: 'valor', width: anchoP(ancho, 4) },
                    { name: 'cantidad', index: 'cantidad', hidden: true },
                    { name: 'id_concepto', index: 'id_concepto', hidden: true },
                    { name: 'nom_concepto', index: 'nom_concepto', width: anchoP(ancho, 10) },
                    { name: 'observacion', index: 'observacion', hidden: true },
                    { name: 'id_caja', index: 'id_caja', hidden: true },
                    { name: 'nom_caja', index: 'nom_caja', hidden: true },
                    { name: 'usuario_elaboro', index: 'usuario_elaboro', hidden: true },
                    { name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 6) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 6) },
                    { name: 'usuario_anulo', index: 'usuario_anulo', hidden: true },
                    { name: 'fecha_anulo', index: 'fecha_anulo', hidden: true },
                    { name: 'id_motivo_anulo', index: 'id_motivo_anulo', hidden: true },
                    { name: 'id_factura', index: 'id_factura', width: anchoP(ancho, 5) },
                    { name: 'auditoria', index: 'Auditoria', width: anchoP(ancho, 5) },
                    { name: 'id_estado_auditoria', index: 'id_estado_auditoria', hidden: true },
                    { name: 'id_recibos_consecutivos', index: 'id_recibos_consecutivos', hidden: true },
                    { name: 'administradora', index: 'administradora', hidden: true },
                    { name: 'regimen', index: 'regimen', hidden: true },
                    { name: 'valor_unidad', index: 'valor_unidad', hidden: true },
                    { name: 'sw_mostrar_en_disponibles', index: 'sw_mostrar_en_disponibles', hidden: true },
                    { name: 'id_medio_pago', index: 'id_medio_pago', hidden: true }
                ],
                height: 330,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdRecibo', datosRow.id_recibo, 0)
                    asignaAtributo('txtIdEditPacienteRecibo', datosRow.id_paciente + '-' + datosRow.nombre_paciente, 0)
                    asignaAtributo('txtAdministradora1', datosRow.administradora, 0)
                    asignaAtributo('cmbIdTipoRegimen', datosRow.regimen, 0)
                    asignaAtributo('lblIdPlanContratacion', datosRow.id_plan, 0)
                    asignaAtributo('lblNomPlanContratacion', datosRow.plan, 0)
                    asignaAtributo('txtValorUnidadRecibo', datosRow.valor_unidad, 0)
                    asignaAtributo('lblValorRecibo', datosRow.valor, 0)
                    asignaAtributo('cmbCantidadRecibo', datosRow.cantidad, 0)

                    asignaAtributo('cmbIdConceptoRecibo', datosRow.id_concepto, 0)
                    asignaAtributo('txtObservacionRecibo', datosRow.observacion, 0)
                    asignaAtributo('lblIdUsuarioRecibo', datosRow.usuario_elaboro, 0)
                    asignaAtributo('lblFechaElaboroRecibo', datosRow.fecha_elaboro, 0)
                    asignaAtributo('lblIdEstadoRecibo', datosRow.id_estado, 0)
                    asignaAtributo('lblEstadoRecibo', datosRow.estado, 0)

                    asignaAtributo('lblIdUsuarioAnulo', datosRow.usuario_anulo, 0)
                    asignaAtributo('lblFechaAnulo', datosRow.fecha_anulo, 0)
                    asignaAtributo('cmbIdMotivoAnulacionRecibo', datosRow.id_motivo_anulo, 0)
                    asignaAtributo('txtIdEstadoAuditoria', datosRow.id_estado_auditoria, 0)
                    asignaAtributo('lblIdReciboConsecutivo', datosRow.id_recibos_consecutivos, 0)
                    asignaAtributo('cmbDisponible', datosRow.sw_mostrar_en_disponibles, 0)
                    asignaAtributo('cmbIdMedioPago', datosRow.id_medio_pago, 0)
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listRecibosFactura':
            if (traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == null || traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == "") {
                return
            }
            ancho = $("#divTableRecibosFactura").width();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=147&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdFactura'));
            add_valores_a_mandar(valorAtributo('lblIdFactura'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['CONTADOR', 'ID', 'NUMERO', 'IDENTIFICACION', 'ID_PACIENTE', 'NOMBRE PACIENTE', 'PLAN', 'VALOR', 'CANTIDAD',
                    'ID_CONCEPTO', 'CONCEPTO', 'OBSERVACION', 'ID_CAJA', 'CAJA', 'USUARIO_ELABORO', 'FECHA ELABORO', 'ID_ESTADO',
                    'ESTADO', 'ID_FACTURA'
                ],
                colModel: [
                    { name: 'CONTADOR', index: 'CONTADOR', hidden: true },
                    { name: 'ID_RECIBO', index: 'ID_RECIBO', hidden: true },
                    { name: 'NUMERO', index: 'NUMERO', width: anchoP(ancho, 4) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 7) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 15) },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                    { name: 'VALOR', index: 'VALOR', width: anchoP(ancho, 4) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', hidden: true },
                    { name: 'ID_CONCEPTO', index: 'ID_CONCEPTO', hidden: true },
                    { name: 'NOM_CONCEPTO', index: 'NOM_CONCEPTO', width: anchoP(ancho, 10) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', hidden: true },
                    { name: 'ID_CAJA', index: 'ID_CAJA', hidden: true },
                    { name: 'NOM_CAJA', index: 'NOM_CAJA', hidden: true },
                    { name: 'USUARIO_ELABORO', index: 'USUARIO_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', width: anchoP(ancho, 7) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 6) },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', width: anchoP(ancho, 5) }
                ],
                height: 140,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdReciboAsociar', datosRow.ID_RECIBO, 0)
                    asignaAtributo('lblNumeroReciboAsociar', datosRow.NUMERO, 0)
                    asignaAtributo('lblIdEstadoRecibo', datosRow.ID_ESTADO, 0)
                    asignaAtributo('lblEstadoRecibo', datosRow.ESTADO, 0)
                    asignaAtributo('lblIdFacturaAsociar', valorAtributo('lblIdFactura'), 0)
                    asignaAtributo('lblNumeroFacturaAsociar', valorAtributo('lblNumeroFactura'), 0)
                    asignaAtributo('lblNomPacienteAsociar', datosRow.IDENTIFICACION + ' ' + datosRow.NOMBRE_PACIENTE, 0)

                    mostrar('divVentanitaRecibosAsociar')

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRecibosCartera':
            ancho = 1050;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=126" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['CONTADOR', 'ID', 'NUMERO', 'IDENTIFICACION', 'ID_PACIENTE', 'NOMBRE PACIENTE', 'PLAN', 'VALOR', 'CANTIDAD',
                    'ID_CONCEPTO', 'CONCEPTO', 'OBSERVACION', 'ID_CAJA', 'CAJA', 'USUARIO_ELABORO', 'FECHA ELABORO', 'ID_ESTADO',
                    'ESTADO', 'USUARIO_ANULO', 'FECHA_ANULO', 'ID_MOTIVO_ANULO', 'ID_FACTURA'
                ],
                colModel: [
                    { name: 'CONTADOR', index: 'CONTADOR', hidden: true },
                    { name: 'ID_RECIBO', index: 'ID_RECIBO', hidden: true },
                    { name: 'NUMERO', index: 'NUMERO', width: anchoP(ancho, 3) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                    { name: 'VALOR', index: 'VALOR', width: anchoP(ancho, 5) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 3) },
                    { name: 'ID_CONCEPTO', index: 'ID_CONCEPTO', hidden: true },
                    { name: 'NOM_CONCEPTO', index: 'NOM_CONCEPTO', width: anchoP(ancho, 10) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', hidden: true },
                    { name: 'ID_CAJA', index: 'ID_CAJA', hidden: true },
                    { name: 'NOM_CAJA', index: 'NOM_CAJA', width: anchoP(ancho, 5) },
                    { name: 'USUARIO_ELABORO', index: 'USUARIO_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', width: anchoP(ancho, 5) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 5) },
                    { name: 'USUARIO_ANULO', index: 'USUARIO_ANULO', hidden: true },
                    { name: 'FECHA_ANULO', index: 'FECHA_ANULO', hidden: true },
                    { name: 'ID_MOTIVO_ANULO', index: 'ID_MOTIVO_ANULO', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', width: anchoP(ancho, 5) }
                ],
                height: 150,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdRecibo', datosRow.ID_RECIBO, 0)


                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaCopagos':
            ancho = 400;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=162&parametros=";

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'REGIMEN', 'ID_REGIMEN', 'RANGO', 'ID_RANGO', 'EVENTO', 'ANUAL'],
                colModel: [
                    { name: 'C', index: 'C', hidden: true },
                    { name: 'REGIMEN', index: 'REGIMEN', width: anchoP(ancho, 20) },
                    { name: 'ID_REGIMEN', index: 'ID_REGIMEN', hidden: true },
                    { name: 'RANGO', index: 'RANGO', width: anchoP(ancho, 20) },
                    { name: 'ID_RANGO', index: 'ID_RANGO', hidden: true },
                    { name: 'EVENTO', index: 'EVENTO', width: anchoP(ancho, 10) },
                    { name: 'ANUAL', index: 'ANUAL', width: anchoP(ancho, 10) }
                ],
                height: 110,
                width: 400,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtIdRegimen', datosRow.ID_REGIMEN, 0)
                    asignaAtributo('txtIdRango', datosRow.ID_RANGO, 0)
                    asignaAtributo('lblRegimen', datosRow.REGIMEN, 0)
                    asignaAtributo('lblRango', datosRow.RANGO, 0)
                    asignaAtributo('txtEvento', datosRow.EVENTO, 0)
                    asignaAtributo('txtAnual', datosRow.ANUAL, 0)
                    mostrar('divVentanitaEditarCopago');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRips':
            ancho = $("#divTableListaRips").width();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=638&parametros=";
            add_valores_a_mandar(valorAtributo('txtCuentaActual'));
            //add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'SEDE', 'ID', 'ID_RIPS', 'RIPS', 'ID_PLAN', 'PLAN', 'ANIO', 'MES', 'CUENTA', 'REMISION', 'POS', 'ID_ENVIO'],
                colModel: [
                    { name: 'C', index: 'C', hidden: true },
                    { name: 'SEDE', index: 'SEDE', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
                    { name: 'ID_RIPS', index: 'ID_RIPS', width: anchoP(ancho, 5) },
                    { name: 'RIPS', index: 'RIPS', width: anchoP(ancho, 20) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', hidden: true },
                    { name: 'ANIO', index: 'ANIO', width: anchoP(ancho, 5) },
                    { name: 'MES', index: 'MES', width: anchoP(ancho, 5) },
                    { name: 'CUENTA', index: 'CUENTA', width: anchoP(ancho, 10) },
                    { name: 'REMISION', index: 'REMISION', width: anchoP(ancho, 10) },
                    { name: 'POS', index: 'POS', width: anchoP(ancho, 5) },
                    { name: 'ID_ENVIO', index: 'ID_ENVIO', width: anchoP(ancho, 5) }
                ],
                height: 200,
                autowidth: true,
                caption: "DETALLES DE LA CUENTA",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    /*asignaAtributo('lblIdArchivoVentanita', datosRow.ID, 0)
                    asignaAtributo('lblIdRipsVentanita', datosRow.ID_RIPS, 0)
                    asignaAtributo('lblRipsVentanita', datosRow.RIPS, 0)
                    asignaAtributo('lblIdPlanVentanita', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblPlanVentanita', datosRow.PLAN, 0)
                    asignaAtributo('lblRangoVentanita', datosRow.ANIO + '-' + datosRow.MES, 0)
                    asignaAtributo('lblPosVentanita', datosRow.POS, 0)
                    asignaAtributo('lblIdEnvioVentanita', datosRow.ID_ENVIO, 0)
                    asignaAtributo('lblRemisionVentanita', datosRow.REMISION, 0)
                    mostrar('divVentanitaGeneraRips');
                    buscarFacturacion('listGrillaDetallesArchivoRips',
                        setTimeout(() => {
                            buscarFacturacion('listGrillaDetallesPendientesArchivoRips',
                                setTimeout(() => {
                                    totalEnvioRips(valorAtributo('lblIdEnvioVentanita'))
                                }, 100))
                        }, 100))*/
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaDetallesPendientesArchivoRips':
            document.getElementById('contenedorRegistrosPendientes').innerHTML = '<table id="listGrillaDetallesPendientesArchivoRips" class="scroll"></table>';
            limpiaAtributo('lblIdRegistroEliminar', 0)
            limpiaAtributo('lblDetalleRegistro', 0)

            valores_a_mandar = pag;

            valores_a_mandar = valores_a_mandar + "?idQuery=205&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPlanVentanita'));
            add_valores_a_mandar(valorAtributo('lblRangoVentanita').split('-')[1]);
            add_valores_a_mandar(valorAtributo('lblRangoVentanita').split('-')[0]);
            add_valores_a_mandar(valorAtributo('lblPosVentanita'))
            //add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID_FACTURA', 'FACTURA', 'ID_PLAN', 'PLAN', ''],
                colModel: [
                    { name: 'C', index: 'C', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                    { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 3) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 10) },
                    { name: '', index: '', width: anchoP(ancho, 1) },
                ],
                height: 320,
                autowidth: true,
                multiboxonly: true,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    var detalle = "";

                    if (valorAtributo('lblIdRipsVentanita') == 'US') {
                        detalle = datosRow.IDENTIFICACION + " " + datosRow.NOMBRE + "-" + datosRow.ADMINISTRADORA;
                    } else {
                        detalle = datosRow.FACTURA + " " + datosRow.PLAN + " " + datosRow.IDENTIFICACION + " " + datosRow.NOMBRE;
                    }
                    asignaAtributo('lblIdPlanFactura', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblPlanFactura', datosRow.PLAN, 0)
                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('lblNumeroFactura', datosRow.FACTURA, 0)
                },
                multiselect: true,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaDetallesArchivoRips':
            document.getElementById('contenedorRegistros').innerHTML = '<table id="listGrillaDetallesArchivoRips" class="scroll"></table>';
            limpiaAtributo('lblIdRegistroEliminar', 0)
            limpiaAtributo('lblDetalleRegistro', 0)

            ancho = 1000;
            valores_a_mandar = pag;

            var query = '';
            var multiselec = true;
            switch (valorAtributo('lblIdRipsVentanita')) {
                case 'AC':
                    multiselec = false;
                    query = '200';
                    colNames = ['C', 'ID_ARCHIVO', 'ID_FACTURA', 'FACTURA', 'ID_PLAN', 'PLAN', 'IDENTIFICACION', 'NOMBRE', 'PROCEDIMIENTO'];
                    colModel = [
                        { name: 'C', index: 'C', width: anchoP(ancho, 2) },
                        { name: 'ID_ARCHIVO', index: 'ID_ARCHIVO', hidden: true },
                        { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                        { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 5) },
                        { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                        { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                        { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 15) },
                        { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 15) },
                    ]
                    break;

                case 'AF':
                    query = '201';
                    colNames = ['C', 'ID_ARCHIVO', 'ID_FACTURA', 'FACTURA', 'ID_PLAN', 'PLAN', 'IDENTIFICACION', 'NOMBRE', ''];
                    colModel = [
                        { name: 'C', index: 'C', width: anchoP(ancho, 2) },
                        { name: 'ID_ARCHIVO', index: 'ID_ARCHIVO', hidden: true },
                        { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                        { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 5) },
                        { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                        { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                        { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 15) },
                        { name: '', index: '', width: anchoP(ancho, 2) },
                    ]
                    break;

                case 'AP':
                    multiselec = false;
                    query = '202';
                    colNames = ['C', 'ID_ARCHIVO', 'ID_FACTURA', 'FACTURA', 'ID_PLAN', 'PLAN', 'IDENTIFICACION', 'NOMBRE', 'PROCEDIMIENTO'];
                    colModel = [
                        { name: 'C', index: 'C', width: anchoP(ancho, 2) },
                        { name: 'ID_ARCHIVO', index: 'ID_ARCHIVO', hidden: true },
                        { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                        { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 5) },
                        { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                        { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                        { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 15) },
                        { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 15) },
                    ]
                    break;

                case 'AT':
                    multiselec = false;
                    query = '203';
                    colNames = ['C', 'ID_ARCHIVO', 'ID_FACTURA', 'FACTURA', 'ID_PLAN', 'PLAN', 'IDENTIFICACION', 'NOMBRE', 'SERVICIO'];
                    colModel = [
                        { name: 'C', index: 'C', width: anchoP(ancho, 2) },
                        { name: 'ID_ARCHIVO', index: 'ID_ARCHIVO', hidden: true },
                        { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                        { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 5) },
                        { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                        { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                        { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 15) },
                        { name: 'SERVICIO', index: 'SERVICIO', hidden: true },
                    ]
                    break;

                case 'US':
                    multiselec = false;
                    query = '204';
                    colNames = ['C', 'IDENTIFICACION', 'NOMBRE', 'ADMINISTRADORA'];
                    colModel = [
                        { name: 'C', index: 'C', width: anchoP(ancho, 2) },
                        { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 15) },
                        { name: 'ADMINISTRADORA', index: 'ADMINISTRADORA', width: anchoP(ancho, 15) },
                    ]
                    break;

                case 'CT':
                    multiselec = false;
                    query = '208';
                    colNames = ['C', 'PRESTADOR', 'FECHA REMISION', 'CODIGO ARCHIVO', 'TOTAL REGISTRO'];
                    colModel = [
                        { name: 'C', index: 'C', width: anchoP(ancho, 2) },
                        { name: 'PRESTADOR', index: 'PRESTADOR', width: anchoP(ancho, 10) },
                        { name: 'REMISION', index: 'FECHA REMISION', width: anchoP(ancho, 15) },
                        { name: 'ARCHIVO', index: 'CODIGO ARCHIVO', width: anchoP(ancho, 15) },
                        { name: 'TOTAL', index: 'TOTAL REGISTRO', width: anchoP(ancho, 15) },
                    ]
                    break;
            }

            valores_a_mandar = valores_a_mandar + "?idQuery=" + query + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdArchivoVentanita'));
            if (query == 201) {
                add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
                add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
                add_valores_a_mandar(valorAtributo('cmbFacturasTipificadas'));
            }

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: colNames,
                colModel: colModel,
                height: 320,
                autowidth: true,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdEnvioFactura', datosRow.ID_ENVIO, 0)
                    asignaAtributo('lblIdArchivoFactura', datosRow.ID_ARCHIVO, 0)
                    asignaAtributo('lblIdPlanFactura', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblPlanFactura', datosRow.PLAN, 0)
                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('lblNumeroFactura', datosRow.FACTURA, 0)
                },
                multiselect: multiselec,
                multiboxonly: true
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listSedePlan':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=911&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_PLAN', 'DESCRIPCION', 'ID_SEDE', 'SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'DESCRIPCION', index: 'NOMBRE', width: anchoP(ancho, 50) },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: anchoP(ancho, 20) },
                ],
                height: 170,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdSede', datosRow.ID_SEDE + "-" + datosRow.SEDE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listExcepcionesEnvio':
            setTimeout(() => {
                buscarFacturacion('listGrillaExcepcionesDx');
            }, 200);
            setTimeout(() => {
                buscarFacturacion('listGrillaExcepcionesMunicipios')
            }, 400);
            break;

        case 'listGrillaExcepcionesDx':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=225" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdEnvioVentanita'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['CONTADOR', 'NUMERO_FACTURA', 'CLIENTE', 'DIAGNOSTICO'
                ],
                colModel: [
                    { name: 'CONTADOR', index: 'CONTADOR', hidden: true },
                    { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 2) },
                    { name: 'CLIENTE', index: 'CLIENTE', width: anchoP(ancho, 5) },
                    { name: 'DIAGNÓSTICO', index: 'DX', width: anchoP(ancho, 8) },

                ],
                height: 320,
                width: 550,
                onSelectRow: function (rowid) {
                    selectRowFacturacion(arg, rowid);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaExcepcionesMunicipios':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=226" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdEnvioVentanita'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['CONTADOR', 'NUMERO_FACTURA', 'CLIENTE', 'MUNICIPIO'
                ],
                colModel: [
                    { name: 'CONTADOR', index: 'CONTADOR', hidden: true },
                    { name: 'FACTURA', index: 'FACTURA', width: anchoP(ancho, 2) },
                    { name: 'CLIENTE', index: 'CLIENTE', width: anchoP(ancho, 5) },
                    { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 8) },

                ],
                height: 320,
                width: 550,
                onSelectRow: function (rowid) {
                    selectRowFacturacion(arg, rowid);
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaFacturas':
            ancho = 1310;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=561&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNumFactura'));
            add_valores_a_mandar(valorAtributo('txtBusNumFactura'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoFactura'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoFactura'));
            add_valores_a_mandar(valorAtributo('cmbEstadoFE'));
            add_valores_a_mandar(valorAtributo('cmbEstadoFE'));
            //add_valores_a_mandar(valorAtributo('cmbIdPlan'));

            add_valores_a_mandar(valorAtributo('txtFechaDesdeFactura'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaFactura'));

            add_valores_a_mandar(valorAtributo('cmbIdFacturador'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));

            add_valores_a_mandar(valorAtributo('cmbSedeFactura'));
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(valorAtributo('cmbSedeFactura'));

            add_valores_a_mandar(valorAtributo('cmbIdEstadoTipificacion'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoTipificacion'));

            add_valores_a_mandar(valorAtributo('cmbIdTipoPlan'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoPlan'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['#', 'ID', '', 'NUMERO', 'TOTAL', 'FECHA', 'ID_PACIENTE',
                    'IDENTIFICACION', 'PACIENTE', 'PLAN', 'ID_ESTADO', 'ESTADO', 'ID_USUARIO', 'USUARIO', 'NO_DEVOLUCION', 'FECHA_ADMISION',
                    'ID_ADMISION', 'DIAN', 'ID_DOCUMENTO', 'ID_MEDICO', 'AUDITORIA', 'ID_ESTADO_AUDITORIA',
                    'FECHA_NACIMIENTO', 'ID_MUNICIPIO', 'TIPO_ID_CLIENTE', 'IDENTIFICACION_CLIENTE',
                    'NOMBRE_CLIENTE', 'NO_AUTORIZACION', 'MEDICO_FACTURA', 'NOMBRE_MEDICO_FACTURA', 'DX_INGRESO',
                    'ID_FACTURAS_CONSECUTIVO', 'VALOR_NOCUBIERTO', 'VALOR_CUBIERTO', 'VALOR_DESCUENTO', 'FECHA_EGRESO', 'ID_ESTADO_TIPIFICACION', 'ID_ESTADO_FE'
                ],
                colModel: [
                    { name: '#', index: '#', width: anchoP(ancho, 2) },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                    { name: '', index: '', width: anchoP(ancho, 2) },
                    { name: 'NUM_FACTURA', index: 'NUM_FACTURA', width: anchoP(ancho, 5) },
                    { name: 'TOTAL_FACTURA', index: 'TOTAL_FACTURA', hidden: true },
                    { name: 'FECHA_NUMERACION', index: 'FECHA_NUMERACION', width: anchoP(ancho, 6) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 7) },
                    { name: 'PACIENTE', index: 'PACIENTE', width: anchoP(ancho, 15) },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 16) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                    { name: 'ID_USUARIO', index: 'ID_USUARIO', hidden: true },
                    { name: 'USUARIO', index: 'Usuario', width: anchoP(ancho, 10) },
                    { name: 'NO_DEVOLUCION', index: 'NO_DEVOLUCION', hidden: true },
                    { name: 'FECHA_ADMISION', index: 'FECHA_ADMISION', hidden: true },
                    { name: 'ID_ADMISION', index: 'ID_ADMISION', width: anchoP(ancho, 5) },
                    { name: 'DIAN', index: 'DIAN', width: anchoP(ancho, 6) },
                    { name: 'ID_DOCUMENTO', index: 'ID_DOCUMENTO', hidden: true },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    { name: 'AUDITORIA', index: 'AUDITORIA' },
                    { name: 'ID_ESTADO_AUDITORIA', index: 'ID_ESTADO_AUDITORIA', hidden: true },
                    { name: 'FECHA_NACIMIENTO', index: 'FECHA_NACIMIENTO', hidden: true },
                    { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                    { name: 'TIPO_ID_CLIENTE', index: 'TIPO_ID_CLIENTE', hidden: true },
                    { name: 'IDENTIFICACION_CLIENTE', index: 'IDENTIFICACION_CLIENTE', hidden: true },
                    { name: 'NOMBRE_CLIENTE', index: 'NOMBRE_CLIENTE', hidden: true },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', hidden: true },
                    { name: 'MEDICO_FACTURA', index: 'MEDICO_FACTURA', hidden: true },
                    { name: 'NOMBRE_MEDICO_FACTURA', index: 'NOMBRE_MEDICO_FACTURA', hidden: true },
                    { name: 'DX_INGRESO', index: 'DX_INGRESO', hidden: true },
                    { name: 'ID_FACTURAS_CONSECUTIVO', index: 'ID_FACTURAS_CONSECUTIVO', hidden: true },
                    { name: 'VALOR_NOCUBIERTO', index: 'VALOR_NOCUBIERTO', hidden: true },
                    { name: 'VALOR_CUBIERTO', index: 'VALOR_CUBIERTO', hidden: true },
                    { name: 'VALOR_DESCUENTO', index: 'VALOR_DESCUENTO', hidden: true },
                    { name: 'FECHA_EGRESO', index: 'FECHA_EGRESO', hidden: true },
                    { name: 'ID_ESTADO_TIPIFICACION', index: 'ID_ESTADO_TIPIFICACION', hidden: true },
                    { name: 'ID_ESTADO_FE', index: 'ID_ESTADO_FE', hidden: true }
                ],
                height: 380,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);


                    var ids = $("#listGrillaFacturas").getDataIDs();
                    var aux = 0;

                    for (var i = 0; i < ids.length; i++) {
                        var c = "jqg_listGrillaFacturas_" + ids[i];
                        if ($("#" + c).is(":checked")) {
                            aux += 1;
                            if (aux > 1) {
                                break;
                            }
                        }
                    }

                    if (aux > 1) {
                        $("#txtNoAutorizacion").attr("readonly", "readonly");
                        $("#txtDxIngreso").attr("readonly", "readonly");
                        $("#txtFechaIngreso").attr("readonly", "readonly");
                    } else {
                        $("#txtNoAutorizacion").removeAttr("readonly");
                        $("#txtDxIngreso").removeAttr("readonly");
                        $("#txtFechaIngreso").removeAttr("readonly");
                    }

                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('lblNumeroFactura', datosRow.NUM_FACTURA, 0)
                    asignaAtributo('lblIdEstadoFactura', datosRow.ID_ESTADO, 0)
                    asignaAtributo('lblEstadoFactura', datosRow.ESTADO, 0)
                    asignaAtributo('lblValorTotalFactura', datosRow.TOTAL_FACTURA, 0)
                    asignaAtributo('lblIdPaciente', datosRow.ID_PACIENTE, 0)
                    asignaAtributo('lblIdentificacion', datosRow.IDENTIFICACION, 0)
                    asignaAtributo('lblNomPaciente', datosRow.PACIENTE, 0)
                    asignaAtributo('lblIdUsuario', datosRow.ID_USUARIO, 0)
                    asignaAtributo('txtNoDevolucion', datosRow.NO_DEVOLUCION, 0)
                    asignaAtributo('lblNomUsuario', datosRow.USUARIO, 0)
                    asignaAtributo('txtFechaAdmision', datosRow.FECHA_ADMISION, 0)
                    asignaAtributo('cmbIdProfesionales', datosRow.ID_MEDICO, 0)
                    asignaAtributo('lblIdAdmision', datosRow.ID_ADMISION, 0)
                    asignaAtributo('lblIdDoc', datosRow.ID_DOCUMENTO, 0)
                    asignaAtributo('lblIdEstadoAuditoria', datosRow.ID_ESTADO_AUDITORIA, 0)


                    asignaAtributo('cmbTipoId', datosRow.TIPO_ID_CLIENTE, 0)
                    asignaAtributo('txtDocumento', datosRow.IDENTIFICACION_CLIENTE, 0)
                    asignaAtributo('txtNacimiento', datosRow.FECHA_NACIMIENTO, 0)
                    asignaAtributo('txtMunicipio', datosRow.ID_MUNICIPIO, 0)
                    asignaAtributoCombo('cmbIdProfesionalesFactura', datosRow.MEDICO_FACTURA, datosRow.NOMBRE_MEDICO_FACTURA)
                    asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                    asignaAtributo('txtFechaIngreso', datosRow.FECHA_ADMISION, 0)
                    asignaAtributo('txtFechaEgreso', datosRow.FECHA_EGRESO, 0)
                    asignaAtributo('txtDxIngreso', datosRow.DX_INGRESO, 0)

                    asignaAtributo('cmbNumeracionFactura', datosRow.ID_FACTURAS_CONSECUTIVO, 0)
                    asignaAtributo('txtValorNoCubierto', datosRow.VALOR_NOCUBIERTO, 0)
                    asignaAtributo('txtValorDescuento', datosRow.VALOR_DESCUENTO, 0)
                    asignaAtributo('lblIdEstadoFe', datosRow.ID_ESTADO_FE, 0)

                    buscarFacturacion('listProcedimientosDeFactura')
                    buscarFacturacion('listaNotasFactura')
                },
                gridComplete: function () {
                    datos = $("#" + arg).getDataIDs()
                    for (i = 1; i <= datos.length; i++) {
                        if (valorAtributo("lblIdFactura") == $("#" + arg).getRowData(i).ID_FACTURA) {
                            //$("#listAdmisionCuenta").jqGrid("setSelection", i, true);
                            $("#listGrillaFacturas #" + i).click();
                            return;
                        }
                    }
                    jQuery("#" + arg).contextMenu('contextMenu', {
                        bindings: {
                            'exportar': function (t) {
                                //const exportarFactura = new GestionFacturacion()
                                gestionFacturacion.exportarFacturaTipificada(arg)
                            },
                            'numerar': function (t) {
                                //const exportarFactura = new GestionFacturacion()
                                numerarFacturasBloque.init(arg)
                            },
                        }
                    })
                },
                onRightClickRow: function (rowid, iRow, iCol, e) {
                   
                },
                multiselect: true,
                multiboxonly: true,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaFacturasCartera':
            ancho = 1050;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=125" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('txtNumeroFactura'));
            add_valores_a_mandar(valorAtributo('txtNumeroFactura'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeFactura'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeFactura'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaFactura'));
            add_valores_a_mandar(valorAtributo('cmbTipoFactura'));
            add_valores_a_mandar(valorAtributo('cmbTipoPago'));



            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['CONTADOR', 'ID', 'NUMERO', 'TOTAL FACTURA', 'TOTAL_PAGAR', 'PAGADO', 'SALDO', 'RECIBOS_FINALIZADOS', 'FEC_NUMERACION',
                    'ID_PACIENTE', 'PLAN', 'ID_ESTADO', 'ESTADO', 'ID_USUARIO', 'USUARIO', 'FECHA ADMISION', 'ID_MEDICO', 'ID_TIPO_PAGO', 'TIPO_PAGO', 'ID_ADMISION'
                ],
                colModel: [
                    { name: 'CONTADOR', index: 'CONTADOR', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                    { name: 'NUM_FACTURA', index: 'NUM_FACTURA', width: anchoP(ancho, 5) },
                    { name: 'TOTAL_FACTURA', index: 'TOTAL_FACTURA', width: anchoP(ancho, 7) },
                    { name: 'TOTAL_PAGAR', index: 'TOTAL_PAGAR', hidden: true },
                    { name: 'VALOR PAGADO', index: 'PAGADO', width: anchoP(ancho, 7) },
                    { name: 'SALDO', index: 'SALDO', width: anchoP(ancho, 7) },
                    { name: 'RECIBOS_FINALIZADOS', index: 'RECIBOS_FINALIZADOS', width: anchoP(ancho, 10) },
                    { name: 'FECHA_NUMERACION', index: 'FECHA_NUMERACION', width: anchoP(ancho, 6) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 16) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 7) },
                    { name: 'ID_USUARIO', index: 'ID_USUARIO', hidden: true },
                    { name: 'USUARIO', index: 'USUARIO', width: anchoP(ancho, 10) },
                    { name: 'FECHA_ADMISION', index: 'FECHA_ADMISION', hidden: true },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    { name: 'ID_TIPO_PAGO', index: 'ID_TIPO_PAGO', hidden: true },
                    { name: 'TIPO_PAGO', index: 'TIPO_PAGO', width: anchoP(ancho, 5) },
                    { name: 'ID_ADMISION', index: 'ID_ADMISION', hidden: true }

                ],
                height: 150,
                width: ancho,
                onSelectRow: function (rowid) {


                    selectRowFacturacion(arg, rowid);

                    /*var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                    asignaAtributo('lblTotalFactura', datosRow.TOTAL_FACTURA, 0)
                    asignaAtributo('lblSaldoFactura', datosRow.SALDO, 0)
                    limpiaAtributo('lblIdRecibo',0)

                    buscarFacturacion('listGrillaRecibosCartera')*/



                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;






        case 'listGrillaFacturasOptica':
            ancho = 1150;
            valores_a_mandar = pag;

            valores_a_mandar = valores_a_mandar + "?idQuery=771" + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdEstadoFactura'));
            add_valores_a_mandar(valorAtributo('txtBusNumFactura'));
            add_valores_a_mandar(valorAtributo('txtNumeroOrden'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('cmbTrabajo'));
            add_valores_a_mandar(valorAtributo('cmbProveedor'));

            add_valores_a_mandar(valorAtributo('txtFechaDesdeTra'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeTra'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaTra'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'Numero', 'Total Factura', 'Fecha Facturacion', 'id_paciente',
                    'identificacion', 'Datos Paciente', 'idTrabajo', 'Orden', 'Id Proveedor', 'Proveedor', 'id_funcionario_recibe', 'Nombre funcionario recibe', 'Fecha Funcionario', 'Fecha Cumplimiento',
                    'usuario envia', 'Fecha Envia', 'id funcionario cumplimiento', 'Fecha Cumplimiento', 'usuario cumplimiento',
                    'observacion cumplimiento', 'Fecha Entrega', 'Tipo Id', 'id cliente recibe', 'nombre cliente recibe', 'observacion recibe', 'idEstado', 'Etapa', 'Etapa2', 'Estado',
                    'Tiene Trabajo', 'FirmaE', 'FirmaC', 'FirmaT'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_factura', index: 'id_factura', hidden: true },
                    { name: 'num_Factura', index: 'num_Factura', width: anchoP(ancho, 5) },
                    { name: 'TotalFactura', index: 'TotalFactura', hidden: true },
                    { name: 'Fecha_numeracion', index: 'Fecha_numeracion', width: anchoP(ancho, 15) },
                    { name: 'id_paciente', index: 'id_paciente', hidden: true },
                    { name: 'identificacion', index: 'identificacion', hidden: true },
                    { name: 'Paciente', index: 'Paciente', width: anchoP(ancho, 20) },
                    { name: 'idTrabajo', index: 'idTrabajo', hidden: true },
                    { name: 'numero_orden', index: 'numero_orden', width: anchoP(ancho, 10) },
                    { name: 'proveedor', index: 'proveedor', hidden: true },
                    { name: 'nombre_proveedor', index: 'nombre_proveedor', width: anchoP(ancho, 10) },
                    { name: 'id_funcionario_recibe', index: 'id_funcionario_recibe', hidden: true },
                    { name: 'nombre_funcionario', index: 'nombre_funcionario', width: anchoP(ancho, 15) },
                    { name: 'fecha_funcionario', index: 'fecha_funcionario', hidden: true },
                    { name: 'fecha_cumplimiento', index: 'fecha_cumplimiento', width: anchoP(ancho, 15) },
                    { name: 'usuario_envia', index: 'usuario_envia', hidden: true },
                    { name: 'fecha_envia', index: 'fecha_envia', hidden: true },
                    { name: 'id_funcionario_cumplimiento', index: 'id_funcionario_cumplimiento', hidden: true },
                    { name: 'fecha_cumplimientoR', index: 'fecha_cumplimientoR', hidden: true },
                    { name: 'usuario_cumplimiento', index: 'usuario_cumplimiento', hidden: true },
                    { name: 'observacion_cumplimiento', index: 'observacion_cumplimiento', hidden: true },
                    { name: 'fecha_entrega', index: 'fecha_entrega', hidden: true },
                    { name: 'tipo_id', index: 'tipo_id', hidden: true },
                    { name: 'id_cliente_recibe', index: 'id_cliente_recibe', hidden: true },
                    { name: 'nombre_clienteR', index: 'nombre_clienteR', hidden: true },
                    { name: 'observacion_recibe', index: 'observacion_recibe', hidden: true },
                    { name: 'idEstado', index: 'idEstado', hidden: true },
                    { name: 'Etapa', index: 'Etapa', width: anchoP(ancho, 20) },
                    { name: 'Etapa2', index: 'Etapa2', hidden: true },
                    { name: 'Estado', index: 'Estado', width: anchoP(ancho, 20) },
                    { name: 'tieneTrabajo', index: 'tieneTrabajo', width: anchoP(ancho, 10) },
                    { name: 'firmaE', index: 'firmaE', hidden: true },
                    { name: 'FirmaC', index: 'FirmaC', hidden: true },
                    { name: 'firmaT', index: 'firmaT', hidden: true }
                ],
                height: 180,
                width: 1080,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);



                    asignaAtributo('txtIdFactura', datosRow.id_factura, 0)
                    asignaAtributo('lblNumeroFactura', datosRow.num_Factura, 0)
                    asignaAtributo('lblIdEstadoFactura', datosRow.id_estado, 0)
                    asignaAtributo('lblEstadoFactura', datosRow.estado, 0)
                    asignaAtributo('lblValorTotalFactura', datosRow.TotalFactura, 0)
                    asignaAtributo('lblIdPaciente', datosRow.id_paciente, 0)
                    asignaAtributo('lblNomPaciente', datosRow.Paciente, 0)
                    asignaAtributo('lblIdUsuario', datosRow.id_usuario, 0)
                    asignaAtributo('lblNomUsuario', datosRow.Usuario, 0)

                    asignaAtributo('lblNumFactura', datosRow.num_Factura, 0)
                    asignaAtributo('txtIdEstado', datosRow.idEstado, 0)
                    asignaAtributo('txtEstado', datosRow.Etapa2, 0)

                    asignaAtributo('lblIdTrabajo', datosRow.idTrabajo, 0)

                    asignaAtributo('txtNumeroOrdenN', datosRow.numero_orden, 0)
                    asignaAtributo('cmbNombreLabo', datosRow.proveedor, 0)
                    //asignaAtributo('cmbNombreFuncionario1', datosRow.nombre_funcionario, 0)

                    asignaAtributoCombo2('cmbNombreFuncionario1', datosRow.id_funcionario_recibe, datosRow.nombre_funcionario);



                    asignaAtributo('lblCedulaPdf', '3E' + datosRow.nombre_funcionario, 0)
                    asignaAtributo('lblCedulaPdf2', '3C' + datosRow.id_funcionario_cumplimiento, 0)
                    asignaAtributo('lblFirmaCliente', '3E' + datosRow.numero_orden + datosRow.id_cliente_recibe, 0)
                    asignaAtributo('lblFirma1', datosRow.firmaE, 0)
                    asignaAtributo('lblFirma2', datosRow.FirmaC, 0)
                    asignaAtributo('lblFirma3', datosRow.firmaT, 0)

                    asignaAtributo('txtNomBus', datosRow.nombre_funcionario, 0)
                    asignaAtributo('txtFechaRecibeF', datosRow.fecha_funcionario, 0)
                    asignaAtributo('txtFechaCumpli', datosRow.fecha_cumplimiento, 0)

                    asignaAtributo('txtFechaRecepcion', datosRow.fecha_cumplimientoR, 0)
                    asignaAtributo('cmbNombreFuncionario2', datosRow.id_funcionario_cumplimiento, 0)
                    asignaAtributo('txtObservacionCumpliento', datosRow.observacion_cumplimiento, 0)

                    asignaAtributo('txtFechaTrabajoEntregado', datosRow.fecha_entrega, 0)
                    asignaAtributo('cmbTipoId', datosRow.tipo_id, 0)
                    asignaAtributo('txtNombreQuienRep', datosRow.id_cliente_recibe, 0)
                    asignaAtributo('txtNombresCompletos', datosRow.nombre_clienteR, 0)
                    asignaAtributo('txtObservacion', datosRow.observacion_recibe, 0)

                    asignaAtributo('lblFechFactura', datosRow.Fecha_numeracion, 0)
                    asignaAtributo('lblPaciente', datosRow.identificacion, 0)
                    asignaAtributo('lblNombrePaciente', datosRow.Paciente, 0)

                    asignaAtributo('lblNombrePaciente', datosRow.Paciente, 0)
                    asignaAtributo('lblIdId', 'hola' + datosRow.nombre_funcionario, 0);

                    asignaAtributo('lblFechaDeHoyT', document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0)

                    recargarFirmaTrabajo('');


                    var ban = false;
                    for (let item of ordenes) {
                        if (item == datosRow.numero_orden) {
                            if (valorAtributo('lblIdEstadoFirmar') == 'E') {
                                asignaAtributo('chkEnvioFirma', 1, 0)
                            } else if (valorAtributo('lblIdEstadoFirmar') == 'C') {
                                asignaAtributo('chkCumplidoFirma', 1, 0)
                            }

                            ban = true;
                        }
                    }

                    if (!ban) {
                        if (valorAtributo('lblIdEstadoFirmar') == 'E') {
                            limpiaAtributo('chkEnvioFirma', 0)
                        } else if (valorAtributo('lblIdEstadoFirmar') == 'C') {
                            limpiaAtributo('chkCumplidoFirma', 0)
                        }
                    }

                    habilitar('chkEnvioFirma', 0)
                    habilitar('chkCumplidoFirma', 0)

                    if (datosRow.idEstado == 'E') {
                        habilitar('chkEnvioFirma', 1)
                    } else if (datosRow.idEstado == 'C') {
                        habilitar('chkCumplidoFirma', 1)
                    }
                    buscarFacturacion('listArticulosFacturaTrabajo')


                },

            });

            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listArticulosFacturaTrabajo':


            ancho = 670;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=142" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdTrabajo'));
            add_valores_a_mandar(valorAtributo('txtIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_DETALLE', 'ID_ARTICULO', 'NOMBRE', 'ESTADO', 'ID_TRABAJO'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_DETALLE', index: 'ID_DETALLE', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 2) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 8) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 8) },
                    { name: 'ID_TRABAJO', index: 'ID_TRABAJO', hidden: true }
                ],
                height: 80,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdTrabajoVentanita', datosRow.ID_TRABAJO, 0)
                    asignaAtributo('lblIdDetalleTrabajoVentanita', datosRow.ID_DETALLE, 0)
                    asignaAtributo('lblIdArticuloTrabajoVentanita', datosRow.ID_ARTICULO, 0)
                    asignaAtributo('lblNombreArticuloTrabajoVentanita', datosRow.NOMBRE, 0)
                    asignaAtributo('lblEstadoTrabajoVentanita', datosRow.ESTADO, 0)

                    mostrar('divVentanitaArticulosTrabajo')

                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;


        case 'listGrillaFacturasAnuladas':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=565" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNumFactura'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Num Factura', 'Total Factura', 'Fecha_registro', 'id_paciente', 'Paciente', 'id_administradora', 'id_estado', 'estado', 'id_usuario', 'Usuario', 'Fecha Admision', 'id_admision', 'id_medico',
                    'motivo_anulacion', 'usuario Anulo', 'Fecha Anula'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'num_Factura', index: 'num_Factura', width: anchoP(ancho, 10) },
                    { name: 'TotalFactura', index: 'TotalFactura', width: anchoP(ancho, 10) },
                    { name: 'Fecha_registro', index: 'Fecha_registro', width: anchoP(ancho, 10) },

                    { name: 'id_paciente', index: 'id_paciente', hidden: true },
                    { name: 'Paciente', index: 'Paciente', width: anchoP(ancho, 10) },
                    { name: 'id_administradora', index: 'id_administradora', hidden: true },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 5) },
                    { name: 'id_usuario', index: 'id_usuario', hidden: true },
                    { name: 'Usuario', index: 'Usuario', width: anchoP(ancho, 10) },
                    { name: 'FechaAdmision', index: 'FechaAdmision', hidden: true },
                    { name: 'id_admision', index: 'id_admision', hidden: true },
                    { name: 'id_medico', index: 'id_medico', hidden: true },
                    { name: 'motivo_anulacion', index: 'motivo_anulacion', width: anchoP(ancho, 20) },
                    { name: 'usuarioAnulo', index: 'usuarioAnulo', width: anchoP(ancho, 20) },
                    { name: 'FechaAnula', index: 'FechaAnula', width: anchoP(ancho, 20) }
                ],
                height: 100,
                width: 1000,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdFactura', datosRow.num_Factura, 0)
                    asignaAtributo('lblValorTotalFactura', datosRow.TotalFactura, 0)
                    asignaAtributo('lblIdPaciente', datosRow.id_paciente, 0)
                    asignaAtributo('lblNomPaciente', datosRow.Paciente, 0)

                    asignaAtributo('lblIdUsuario', datosRow.id_usuario, 0)
                    asignaAtributo('lblNomUsuario', datosRow.Usuario, 0)
                    asignaAtributo('txtFechaAdmision', datosRow.FechaAdmision, 0)
                    asignaAtributo('cmbIdProfesionales', datosRow.id_medico, 0)
                    asignaAtributo('lblIdAdmision', datosRow.id_admision, 0)
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listLiquidacionQx':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;

            valores_a_mandar = valores_a_mandar + "?idQuery=542" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'factura', 'finalidad', 'tipoAtencion', 'anestesiologo', 'id_tipo_anestesia',
                    'sw_cirujano', 'sw_anestesiologo', 'sw_ayudante', 'sw_sala', 'sw_materiales', 'IdTarifario_liq', 'Variabilidad_liq', 'ValorTotal_liq', 'Liquida_liq', 'TipoTarifario_liq'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 2) },
                    { name: 'id_factura', index: 'id_factura', width: anchoP(ancho, 2) },
                    { name: 'id_finalidad', index: 'id_finalidad', width: anchoP(ancho, 2) },
                    { name: 'id_tipoAtencion', index: 'id_tipoAtencion', width: anchoP(ancho, 2) },
                    { name: 'id_anestesiologo', index: 'id_anestesiologo', width: anchoP(ancho, 2) },
                    { name: 'id_tipo_anestesia', index: 'id_tipo_anestesia', width: anchoP(ancho, 5) },
                    { name: 'sw_cirujano', index: 'sw_cirujano', width: anchoP(ancho, 5) },
                    { name: 'sw_anestesiologo', index: 'sw_anestesiologo', width: anchoP(ancho, 5) },
                    { name: 'sw_ayudante', index: 'sw_ayudante', width: anchoP(ancho, 5) },
                    { name: 'sw_sala', index: 'sw_sala', width: anchoP(ancho, 5) },
                    { name: 'sw_materiales', index: 'sw_materiales', width: anchoP(ancho, 5) },

                    { name: 'IdTarifario_liq', index: 'IdTarifario_liq', hidden: true },
                    { name: 'Variabilidad_liq', index: 'Variabilidad_liq', hidden: true },
                    { name: 'ValorTotal_liq', index: 'ValorTotal_liq', hidden: true },
                    { name: 'Liquida_liq', index: 'Liquida_liq', hidden: true },
                    { name: 'TipoTarifario_liq', index: 'TipoTarifario_liq', hidden: true },
                ],
                height: 50,
                width: 1150,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdLiquidacion', datosRow.id, 0)
                    asignaAtributo('cmbIdAnestesiologo', datosRow.id_anestesiologo, 0)
                    asignaAtributo('cmbIdTipoAnestesia', datosRow.id_tipo_anestesia, 0)
                    asignaAtributo('cmbIdFinalidad', datosRow.id_finalidad, 0)
                    asignaAtributo('cmbIdTipoAtencion', datosRow.id_tipoAtencion, 0)

                    asignaAtributo('chk_Cirujano', datosRow.sw_cirujano, 0)
                    asignaAtributo('chk_Anestesiologo', datosRow.sw_anestesiologo, 0)
                    asignaAtributo('chk_Ayudante', datosRow.sw_ayudante, 0)
                    asignaAtributo('chk_Sala', datosRow.sw_sala, 0)
                    asignaAtributo('chk_Materiales', datosRow.sw_materiales, 0)

                    asignaAtributo('lblIdTarifario_liq', datosRow.IdTarifario_liq, 0)
                    asignaAtributo('lblVariabilidad_liq', datosRow.Variabilidad_liq, 0)
                    asignaAtributo('lblValorTotal_liq', datosRow.ValorTotal_liq, 0)
                    asignaAtributo('lblLiquida_liq', datosRow.Liquida_liq, 0)
                    asignaAtributo('lblIdTipoTarifario_liq', datosRow.TipoTarifario_liq, 0)

                    buscarFacturacion('listLiquidacionQxProcedimientos')
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listLiquidacionQxProcedimientos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=545" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'id_sitio_quirurgico', 'id_procedimiento', 'nombre_procedimi', 'id_lateralidad', 'lateralidad', 'id_via_de_acceso', 'via_de_acceso',
                    'valor_unitario', 'id_tipo_unidad', 'id_unidad_precio', 'orden', 'valor_con_descuento', 'id_tipo_tarifario'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 2) },
                    { name: 'id_sitio_quirurgico', index: 'id_sitio_quirurgico', hidden: true },
                    { name: 'id_procedimiento', index: 'id_procedimiento', width: anchoP(ancho, 10) },
                    { name: 'nombre_procedimi', index: 'nombre_procedimi', width: anchoP(ancho, 50) },
                    { name: 'id_lateralidad', index: 'lateralidad', width: anchoP(ancho, 2) },
                    { name: 'lateralidad', index: 'lateralidad', width: anchoP(ancho, 15) },
                    { name: 'id_via_de_acceso', index: 'id_via_de_acceso', hidden: true },
                    { name: 'via_de_acceso', index: 'via_de_acceso', width: anchoP(ancho, 10) },

                    { name: 'valor_unitario', index: 'valor_unitario', width: anchoP(ancho, 8) },
                    { name: 'id_tipo_unidad', index: 'id_tipo_unidad', width: anchoP(ancho, 8) },
                    { name: 'id_unidad_precio', index: 'id_unidad_precio', width: anchoP(ancho, 8) },
                    { name: 'orden', index: 'orden', width: anchoP(ancho, 8) },
                    { name: 'valor_con_descuento', index: 'valor_con_descuento', width: anchoP(ancho, 8) },
                    { name: 'id_tipo_tarifario', index: 'id_tipo_tarifario', width: anchoP(ancho, 8) }


                ],
                height: 200,
                width: 1150,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdLiquiProced', datosRow.id_procedimiento, 0)
                    asignaAtributo('lblNomLiquiProced', datosRow.nombre_procedimi, 0)
                    asignaAtributo('cmbIdLateralLiquiProced', datosRow.id_lateralidad, 0)
                    asignaAtributo('cmbIdViaLiquiProced', datosRow.id_via_de_acceso, 0)

                    mostrar('divVentanitaLiquidacionProcedimientos')
                    buscarFacturacion('listLiquidacionQxProcedimientosDerechos')
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listLiquidacionQxProcedimientosDerechos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=557" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdLiquidacion'));
            add_valores_a_mandar(valorAtributo('lblIdLiquiProced'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id_derechos', 'nom_derechos', 'valor_unitario', 'porcentaje', 'id_unidad_precio', 'id_tarifario', 'id_tipo_tarifario', 'val_unit_variabilidad'

                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_derechos', index: 'id_derechos', width: anchoP(ancho, 5) },
                    { name: 'nom_derechos', index: 'nom_derechos', width: anchoP(ancho, 35) },
                    { name: 'valor_unitario', index: 'valor_unitario', width: anchoP(ancho, 20) },
                    { name: 'porcentaje', index: 'porcentaje', width: anchoP(ancho, 10) },
                    { name: 'id_unidad_precio', index: 'id_unidad_precio', width: anchoP(ancho, 10) },
                    { name: 'id_tarifario', index: 'id_tarifario', width: anchoP(ancho, 10) },
                    { name: 'id_tipo_tarifario', index: 'id_tipo_tarifario', width: anchoP(ancho, 10) },
                    { name: 'val_unit_variabilidad', index: 'val_unit_variabilidad', width: anchoP(ancho, 10) },
                ],
                height: 200,
                width: 1150,
                onSelectRow: function (rowid) {
                    mostrar('divVentanitaLiquidacionProcedimientosDerechos')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdLiquiProcedDerecho', datosRow.id_derechos, 0)
                    asignaAtributo('lblNomLiquiProcedDerecho', datosRow.nom_derechos, 0)
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listaOtrosDesatallesFactura':
            /*if(traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == null || traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == ""){
                return
            }*/
            ancho = $("#divListOtrosDetallesFactura").width();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1180&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'CODIGO', 'PROCEDIMIENTO', 'CANTIDAD', 'VALOR UNITARIO', 'SUB TOTAL'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'CODIGO', index: 'CODIGO', width: anchoP(ancho, 3) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 20) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', align: 'right', width: anchoP(ancho, 4) },
                    { name: 'SUB_TOTAL', index: 'SUB_TOTAL', align: 'right', width: anchoP(ancho, 3) },
                ],
                onCellSelect: function (rowid, iCol, cellcontent, e) {
                    if (iCol == 1) {
                        mostrar('divOrdenesFacturaDetalle')
                        setTimeout(() => {
                            $('#tabsDetallesFactura').tabs('select', 0);
                            buscarFacturacion("listaFacturaDetalleEvolucion")
                        }, 100);
                    } else {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                        asignaAtributo('lblIdProcedimiento', datosRow.ID, 0)
                        asignaAtributo('txtCodigoProcedimientoEditar', datosRow.CODIGO, 0)
                        asignaAtributo('txtProcedimientoEditar', datosRow.PROCEDIMIENTO, 0)
                        asignaAtributo('txtCantidadProcedimientoEditar', datosRow.CANTIDAD, 0)
                        asignaAtributo('txtValorUnitarioProcedimientoEditar', datosRow.VALOR_UNITARIO, 0)
                        mostrar('divVentanitaProcedimientosOtro');
                    }
                },
                height: 100,
                autowidth: true,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listaNotasFactura':
            ancho = $("#" + arg).width();
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=6016&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['#', 'ID_FACTURA', 'ID_RECIBO', 'TIPO DOCUMENTO', 'TIPO NOTA', 'VALOR', 'ID_ESTADO', 'ESTADO', 'NUMERO',
                    'FECHA', 'ESTADO FE', 'ENVIAR', 'ELIMINAR'
                ],
                colModel: [
                    { name: '#', index: '#', hidden: true },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                    { name: 'ID_RECIBO', index: 'ID_RECIBO', width: anchoP(ancho, 2) },
                    { name: 'TIPO DOCUMENTO', index: 'TIPO DOCUMENTO', width: anchoP(ancho, 3) },
                    { name: 'TIPO NOTA', index: 'TIPO NOTA', width: anchoP(ancho, 3) },
                    { name: 'VALOR', index: 'VALOR', width: anchoP(ancho, 3) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO',  hidden: true },
                    { name: 'NUMERO', index: 'NUMERO', width: anchoP(ancho, 3) },
                    { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 3) },
                    { name: 'ESTADO FE', index: 'ESTADO FE', width: anchoP(ancho, 3) },
                    {
                        name: 'ENVIAR', index: 'ENVIAR', width: anchoP(ancho, 1), formatter: function (cellvalue, options, rowObject) {
                            return objeto_html_enviar_nota_fe(rowObject);
                        }
                    },
                    {
                        name: 'ELIMINAR', index: 'ELIMINAR', width: anchoP(ancho, 1), formatter: function (cellvalue, options, rowObject) {
                            return objeto_html_eliminar_nota(rowObject);
                        }
                    },
                ],
                onCellSelect: function (rowid, iCol, cellcontent, e) {

                },
                height: 100,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listProcedimientosDeFactura':
            /*if(traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == null || traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == ""){
                return
            }*/
            ancho = ($('#drag' + ventanaActual.num).find("#listProcedimientosDeFactura").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=424&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', '', 'ID', 'CODIGO', 'CUPS', 'PROCEDIMIENTO', 'NO_AUTORIZACION', 'EJECUTADO',
                    'ID_EJECUTADO', 'CANTIDAD', 'VALOR UNITARIO', 'SUB TOTAL', 'ID_MEDICO', 'MEDICO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    {
                        name: "", index: '', width: anchoP(ancho, 2), align: 'center', formatter: function () {
                            return '<img src="/clinica/utilidades/imagenes/icons/icons/link.svg" width="18px" height="18px" align="middle">'
                        }
                    },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'CODIGO', index: 'CODIGO', width: anchoP(ancho, 3) },
                    { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 3) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 20) },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', width: anchoP(ancho, 5) },
                    { name: 'EJECUTADO', index: 'EJECUTADO', width: anchoP(ancho, 3) },
                    { name: 'ID_EJECUTADO', index: 'ID_EJECUTADO', hidden: true },
                    { name: 'CANTIDAD', index: 'CANTIDAD', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', align: 'right', width: anchoP(ancho, 4) },
                    { name: 'SUB_TOTAL', index: 'SUB_TOTAL', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    { name: 'MEDICO', index: 'MEDICO', hidden: true },
                ],
                onCellSelect: function (rowid, iCol, cellcontent, e) {
                    if (iCol == 1) {
                        mostrar('divOrdenesFacturaDetalle')
                        setTimeout(() => {
                            $('#tabsDetallesFactura').tabs('select', 0);
                            buscarFacturacion("listaFacturaDetalleEvolucion")
                        }, 100);
                    } else {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                        asignaAtributo('lblIdTransac', datosRow.ID, 0)
                        asignaAtributo('txtProcedimientoEditar', datosRow.CODIGO + "-" + datosRow.CUPS + " " + datosRow.PROCEDIMIENTO, 0)
                        asignaAtributo('txtCantidadProcedimientoEditar', datosRow.CANTIDAD, 0)
                        asignaAtributo('txtNoAutorizacionProcedimientoEditar', datosRow.NO_AUTORIZACION, 0)
                        asignaAtributoCombo('cmbIdProfesionalesFacturaEditar', datosRow.ID_MEDICO, datosRow.MEDICO)
                        asignaAtributo('cmbEjecutadoEditar', datosRow.ID_EJECUTADO, 0)
                        mostrar('divVentanitaProcedimCuenta');
                    }
                },
                height: 170,
                autowidth: true,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listArticulosDeFactura':
            /*if(traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == null || traerDatoFilaSeleccionada("listAdmisionCuenta", "ID_FACTURA") == ""){
                return
            }*/
            ancho = 1100;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=729&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'ID_ARTICULO', 'NOMBRE_ARTICULO', 'CANTIDAD', 'VALOR UNITARIO', 'SUB TOTAL'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
                    { name: 'NOMBRE_ARTICULO', index: 'NOMBRE_ARTICULO', width: anchoP(ancho, 20) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', align: 'right', width: anchoP(ancho, 4) },
                    { name: 'SUB_TOTAL', index: 'SUB_TOTAL', align: 'right', width: anchoP(ancho, 3) }
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdTransaccionArticuloVentanita', datosRow.ID, 0)
                    asignaAtributo('lblIdArticuloVentanita', datosRow.ID_ARTICULO, 0)
                    asignaAtributo('lblNomArticuloVentanita', datosRow.NOMBRE_ARTICULO, 0)
                    mostrar('divVentanitaArticuloFactura')
                },
                height: 80,
                width: 1190,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listArticulosVentas':
            ancho = 1100;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=116" + "&parametros=";

            add_valores_a_mandar(valorAtributo('lblIdDoc'));
            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_DETALLE', 'ID_ARTICULO', 'COD_EXTERNO', 'NOMBRE_ARTICULO', 'ID_FACTURA',
                    'VALOR_UNITARIO_FACTURA', 'CANTIDAD_FACTURA', 'SUB_TOTAL', 'ID_TRANSACCION', 'ID_DOCUMENTO', 'CANTIDAD_DOCUMENTO',
                    'VALOR_UNITARIO_DOCUMENTO', 'IVA_DOCUMENTO', 'VALOR_IMPUESTO', 'ID_NATURALEZA', 'LOTE', 'FECHA_VENCIMIENTO', 'SERIAL',
                    'PRECIO_VENTA', 'EXITO', 'MEDIDA', 'VALOR_DESCUENTO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_DETALLE', index: 'ID_DETALLE', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
                    { name: 'COD_EXTERNO', index: 'COD_EXTERNO', hidden: true },
                    { name: 'NOMBRE_ARTICULO', index: 'NOMBRE_ARTICULO', width: anchoP(ancho, 20) },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', width: anchoP(ancho, 3) },
                    { name: 'VALOR_UNITARIO_FACTURA', index: 'VALOR_UNITARIO_FACTURA', width: anchoP(ancho, 4) },
                    { name: 'CANTIDAD_FACTURA', index: 'CANTIDAD_FACTURA', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'SUB_TOTAL', index: 'SUB_TOTAL', width: anchoP(ancho, 4) },
                    { name: 'ID_TRANSACCION', index: 'ID_TRANSACCION', hidden: true },
                    { name: 'ID_DOCUMENTO', index: 'ID_DOCUMENTO', width: anchoP(ancho, 3) },
                    { name: 'CANTIDAD_DOCUMENTO', index: 'CANTIDAD_DOCUMENTO', hidden: true },
                    { name: 'VALOR_UNITARIO_DOCUMENTO', index: 'VALOR_UNITARIO_DOCUMENTO', hidden: true },
                    { name: 'IVA_DOCUMENTO', index: 'IVA_DOCUMENTO', width: anchoP(ancho, 3) },
                    { name: 'VALOR_IMPUESTO', index: 'VALOR_IMPUESTO', hidden: true },
                    { name: 'ID_NATURALEZA', index: 'ID_NATURALEZA', hidden: true },
                    { name: 'LOTE', index: 'LOTE', width: anchoP(ancho, 3) },
                    { name: 'FECHA_VENCIMIENTO', index: 'FECHA_VENCIMIENTO', width: anchoP(ancho, 3) },
                    { name: 'SERIAL', index: 'SERIAL', width: anchoP(ancho, 3) },
                    { name: 'PRECIO_VENTA', index: 'PRECIO_VENTA', hidden: true },
                    { name: 'EXITO', index: 'EXITO', width: anchoP(ancho, 2) },
                    { name: 'MEDIDA', index: 'MEDIDA', hidden: true },
                    { name: 'VALOR_DESCUENTO', index: 'VALOR_DESCUENTO', hidden: true }

                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdDetalleVentasVentanita', datosRow.ID_DETALLE, 0);
                    asignaAtributo('lblIdTransaccionVentasVentanita', datosRow.ID_TRANSACCION, 0);
                    asignaAtributo('lblIdArticuloVentasVentanita', datosRow.ID_ARTICULO, 0);
                    asignaAtributo('lblNomArticuloVentasVentanita', datosRow.NOMBRE_ARTICULO, 0);
                    asignaAtributo('lblSerialVentasVentanita', datosRow.SERIAL, 0);
                    asignaAtributo('lblValorBaseVentanita', datosRow.SUB_TOTAL, 0);
                    asignaAtributo('lblValorDescuentoVentanita', datosRow.VALOR_DESCUENTO, 0);



                    if (datosRow.VALOR_DESCUENTO == '') {
                        habilitar('btnAplicarDescuentoVentanita', 1)
                        habilitar('btnRevertirDescuentoVentanita', 0)
                    } else {
                        habilitar('btnAplicarDescuentoVentanita', 0)
                        habilitar('btnRevertirDescuentoVentanita', 1)
                    }

                    limpiaAtributo('cmbPorcentajeDescuentoVentanita', 0)
                    limpiaAtributo('txtValorDescuentoVentanita', 1)

                    mostrar('divVentanitaArticuloFacturaVentas')

                },
                height: 80,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;



        case 'listOtrosArticulosVentas':
            ancho = 1100;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=122" + "&parametros=";

            add_valores_a_mandar(valorAtributo('lblIdFactura'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_DETALLE', 'ID_ARTICULO', 'COD_EXTERNO', 'NOMBRE_ARTICULO', 'OBSERVACION', 'ID_FACTURA',
                    'VALOR_UNITARIO', 'CANTIDAD', 'SUB_TOTAL', 'VALOR_DESCUENTO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_DETALLE', index: 'ID_DETALLE', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
                    { name: 'COD_EXTERNO', index: 'COD_EXTERNO', width: anchoP(ancho, 5) },
                    { name: 'NOMBRE_ARTICULO', index: 'NOMBRE_ARTICULO', width: anchoP(ancho, 10) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 20) },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', width: anchoP(ancho, 5) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', width: anchoP(ancho, 5) },
                    { name: 'CANTIDAD', index: 'CANTIDAD_FACTURA', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'SUB_TOTAL', index: 'SUB_TOTAL', width: anchoP(ancho, 4) },
                    { name: 'VALOR_DESCUENTO', index: 'VALOR_DESCUENTO', hidden: true }
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('lblIdDetalleVentasOtroVentanita', datosRow.ID_DETALLE, 0);
                    asignaAtributo('lblIdArticuloVentasOtroVentanita', datosRow.ID_ARTICULO, 0);
                    asignaAtributo('lblNomArticuloVentasOtroVentanita', datosRow.NOMBRE_ARTICULO, 0);
                    asignaAtributo('lblValorBaseOtroVentanita', datosRow.SUB_TOTAL, 0);
                    asignaAtributo('lblValorDescuentoOtroVentanita', datosRow.VALOR_DESCUENTO, 0);



                    if (datosRow.VALOR_DESCUENTO == '') {
                        habilitar('btnAplicarDescuentoVentanita', 1)
                        habilitar('btnRevertirDescuentoVentanita', 0)
                    } else {
                        habilitar('btnAplicarDescuentoVentanita', 0)
                        habilitar('btnRevertirDescuentoVentanita', 1)
                    }

                    limpiaAtributo('cmbPorcentajeDescuentoOtroVentanita', 0)
                    limpiaAtributo('txtValorDescuentoOtroVentanita', 0)
                    mostrar('divVentanitaArticuloVentasOtro');

                },
                height: 80,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;




        case 'listGrillaProcedimientosSinEjecutar':

            ancho = ($('#drag' + ventanaActual.num).find("#listAdmisionCuenta").width()) - 200;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=716&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdFacturaAsociar'));
            //add_valores_a_mandar(valorAtributo('lblIdCuenta'));
            //add_valores_a_mandar(valorAtributo('lblIdCuenta'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'CODIGO', 'PROCEDIMIENTO', 'NO_AUTORIZACION',
                    'CANTIDAD', 'VALOR UNITARIO', 'SUB TOTAL'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'CODIGO', index: 'CODIGO', width: anchoP(ancho, 3) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 20) },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', width: anchoP(ancho, 5) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', align: 'right', width: anchoP(ancho, 4) },
                    { name: 'SUB_TOTAL', index: 'SUB_TOTAL', align: 'right', width: anchoP(ancho, 3) }
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtIdProcedimientoRealizar', datosRow.ID, 0);
                    asignaAtributo('txtProcedimientoEstado', 'S', 0);

                    modificarCRUD('modificarProcedimientoRealizado');


                },
                height: 80,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');




            break;


        case 'listGrillaProcedimientosEjecutados':

            ancho = ($('#drag' + ventanaActual.num).find("#listAdmisionCuenta").width()) - 200;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=718" + "&parametros=";

            add_valores_a_mandar(valorAtributo('txtIdFacturaAsociar'));
            //add_valores_a_mandar(valorAtributo('lblIdCuenta'));
            //add_valores_a_mandar(valorAtributo('lblIdCuenta'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'CODIGO', 'PROCEDIMIENTO', 'NO_AUTORIZACION',
                    'CANTIDAD', 'VALOR UNITARIO', 'SUB TOTAL'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'CODIGO', index: 'CODIGO', width: anchoP(ancho, 3) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 20) },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', width: anchoP(ancho, 5) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', align: 'right', width: anchoP(ancho, 3) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', align: 'right', width: anchoP(ancho, 4) },
                    { name: 'SUB_TOTAL', index: 'SUB_TOTAL', align: 'right', width: anchoP(ancho, 3) }
                ],
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtIdProcedimientoRealizar', datosRow.ID, 0);
                    asignaAtributo('txtProcedimientoEstado', 'N', 0);

                    modificarCRUD('modificarProcedimientoRealizado');


                },
                height: 80,
                width: ancho,
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRangos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) + 400;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=410&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_TIPO_AFILIADO', 'TIPO AFILIADO', 'ID_RANGO', 'RANGO', 'COPAGO %', 'CUOTA MODERADORA'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_TIPO_AFILIADO', index: 'ID_TIPO_AFILIADO', hidden: true },
                    { name: 'NOMBRE_TIPO_AFILIADO', index: 'NOMBRE_TIPO_AFILIADO', width: anchoP(ancho, 100) },
                    { name: 'ID_RANGO', index: 'ID_RANGO', hidden: true },
                    { name: 'RANGO', index: 'RANGO', width: anchoP(ancho, 100) },
                    { name: 'COPAGO', index: 'COPAGO', width: anchoP(ancho, 100) },
                    { name: 'CUOTA_MODERADORA', index: 'CUOTA_MODERADORA', width: anchoP(ancho, 100) },
                ],
                height: 170,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('cmbIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, 0)
                    asignaAtributo('cmbIdRango', datosRow.ID_RANGO, 0)
                    asignaAtributo('txtCopago', datosRow.COPAGO, 0)
                    asignaAtributo('txtCuotaModeradora', datosRow.CUOTA_MODERADORA, 0)
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaContratos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) + 400;

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=736&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'ID_PLAN', 'ORDEN_PROCEDIMIENTO', 'PORCENTAJE', 'LATERALIDAD', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 30) },
                    { name: 'ID_PLAN', index: 'ID_PLAN', width: anchoP(ancho, 100) },
                    { name: 'ORDEN_PROCEDIMIENTO', index: 'ORDEN_PROCEDIMIENTO', width: anchoP(ancho, 100) },
                    { name: 'PORCENTAJE', index: 'PORCENTAJE', width: anchoP(ancho, 100) },
                    { name: 'LATERALIDAD', index: 'LATERALIDAD', width: anchoP(ancho, 100) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 100) },
                ],
                height: 170,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdRegla', datosRow.ID, 0)
                    asignaAtributo('cmbOrdenProcedimiento', datosRow.ORDEN_PROCEDIMIENTO, 0)
                    asignaAtributo('txtPorcentaje', datosRow.PORCENTAJE, 0)
                    asignaAtributo('cmbLateralidad', datosRow.LATERALIDAD, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaProcedimientos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) + 400;


            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=744&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'PLAN', 'ID PROCEDIMIENTO', 'NOMBRE', 'ORDEN', 'PORCENTAJE', 'LATERALIDAD', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 4) },
                    { name: 'PLAN', index: 'ID_PLAN', width: anchoP(ancho, 10) },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 10) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 25) },
                    { name: 'ORDEN_PROCEDIMIENTO', index: 'ORDEN_PROCEDIMIENTO', width: anchoP(ancho, 8) },
                    { name: 'PORCENTAJE', index: 'PORCENTAJE', width: anchoP(ancho, 8) },
                    { name: 'LATERALIDAD', index: 'LATERALIDAD', width: anchoP(ancho, 8) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },
                ],
                height: 170,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.ID, 0)
                    asignaAtributo('lblPlan', datosRow.PLAN, 0)
                    asignaAtributo('txtIdProcedimientoLiquida', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.NOMBRE, 0)
                    asignaAtributo('cmbOrdenProcedimientto', datosRow.ORDEN_PROCEDIMIENTO, 0)
                    asignaAtributo('txtPorcen', datosRow.PORCENTAJE, 0)
                    asignaAtributo('cmbLate', datosRow.LATERALIDAD, 0)
                    asignaAtributo('cmbVigentte', datosRow.VIGENTE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaMunicipios':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) + 400;


            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=761&parametros=";
            add_valores_a_mandar(valorAtributo('lblPlanM'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_PLAN', 'ID_MUNICIPIO', 'MUNICIPIO', 'DEPARTAMENTO'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                    { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 15) },
                    { name: 'DEPARTAMENTO', index: 'DEPARTAMENTO', width: anchoP(ancho, 15) },

                ],
                height: 170,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblPlanM', datosRow.ID_PLAN, 0)
                    asignaAtributo('lblMuni', datosRow.ID_MUNICIPIO + '-' + datosRow.MUNICIPIO, 0)
                    asignaAtributo('txtMunicipio', datosRow.ID_MUNICIPIO + '-' + datosRow.DEPARTAMENTO + ' ' + datosRow.MUNICIPIO, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPlanExcepcion':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=767&parametros=";
            add_valores_a_mandar(valorAtributo('lblPlanM'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID PROCEDIMIENTO', 'CUPS', 'CANTIDAD EXCEPCION', 'MES', 'A&Ntilde;O'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 10) },
                    { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 10) },
                    { name: 'CANTIDAD_EXCEPCION', index: 'CANTIDAD_EXCEPCION', width: anchoP(ancho, 10) },
                    { name: 'MES', index: 'MES', width: anchoP(ancho, 10) },
                    { name: 'ANIO', index: 'ANIO', width: anchoP(ancho, 10) },

                ],
                height: 170,
                autowidth: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtCantidadExcepcion', datosRow.CANTIDAD_EXCEPCION, 0)
                    asignaAtributo('cmbyear', datosRow.ANIO, 0)
                    asignaAtributo('cmbmes', datosRow.MES, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPlanesMedicamentos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=588&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID ARTICULO', 'NOMBRE ARTICULO', 'PRECIO', 'COD EXTERNO', 'SW_NOMBRE_EXTERNO', 'NOMBRE EXTERNO'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 100) },
                    { name: 'NOM_ARTICULO', index: 'NOM_ARTICULO', width: anchoP(ancho, 100) },
                    { name: 'PRECIO', index: 'PRECIO', width: anchoP(ancho, 100) },
                    { name: 'COD_EXTERNO', index: 'COD_EXTERNO', width: anchoP(ancho, 100) },
                    { name: 'SW_NOMBRE_EXTERNO', index: 'SW_NOMBRE_EXTERNO', hidden: true },
                    { name: 'NOMBRE_EXTERNO', index: 'NOMBRE_EXTERNO', width: anchoP(ancho, 100) }
                ],
                height: 170,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOM_ARTICULO, 0)
                    asignaAtributo('txtIdPrecioArticulo', datosRow.PRECIO, 0)
                    asignaAtributo('txt_codExterno', datosRow.COD_EXTERNO, 0)

                    asignaAtributo('cmbNombreExternoArticulo', datosRow.SW_NOMBRE_EXTERNO, 0)
                    var mostrar = datosRow.SW_NOMBRE_EXTERNO === 'S' ? 0 : 1;
                    asignaAtributo('txtNombreExternoArticulo', datosRow.NOMBRE_EXTERNO, mostrar)

                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPlanes':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=406&parametros=";
            add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
            add_valores_a_mandar(valorAtributo('txtNomBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'DESCRIPCION', 'ID_ADMINISTRADORA', 'ADMINISTRADORA', 'ID_TIPO_REGIMEN', 'ID_TIPO_CLIENTE',
                    'NUM_CONTRATO', 'FECHA INICIO', 'FECHA FINAL', 'MONTO_CONTRATO', 'TIPO_PLAN', 'ID_TARIFARIO', 'INCREMENTO (%)', 'ADMITE_REDONDEO', 'FECHA_REGISTRO', 'OBSERVACION',
                    'SW_SOLICITA_AUTORIZACION_ADMISION', 'ID_ELABORO', 'ID_ESTADO', 'ESTADO', 'HC', 'ID_PROGRAMA', 'PROGRAMA', 'CODIGO_ADMINISTRADORA', 'NUMERO_POLIZA', 'ID_MODALIDAD_CONTRATO',
                    'ID_COBERTURA'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PLAN', index: 'ID', width: anchoP(ancho, 5) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 20) },
                    { name: 'ID_ADMINISTRADORA', index: 'ID_ADMINISTRADORA', hidden: true },
                    { name: 'ADMINISTRADORA', index: 'ADMINISTRADORA', width: anchoP(ancho, 15) },
                    { name: 'ID_TIPO_REGIMEN', index: 'ID_TIPO_REGIMEN', hidden: true },
                    { name: 'ID_TIPO_CLIENTE', index: 'ID_TIPO_CLIENTE', hidden: true },
                    { name: 'NUM_CONTRATO', index: 'NUM_CONTRATO', hidden: true },
                    { name: 'FECHA_INICIO', index: 'FECHA INICIO', width: anchoP(ancho, 10) },
                    { name: 'FECHA_FINAL', index: 'FECHA FINAL', width: anchoP(ancho, 10) },
                    { name: 'MONTO_CONTRATO', index: 'MONTO_CONTRATO', hidden: true },
                    { name: 'TIPO_PLAN', index: 'TIPO_PLAN', hidden: true },
                    { name: 'ID_TARIFARIO', index: 'ID_TARIFARIO', hidden: true },

                    { name: 'PORCEN_INCREMENTO', index: 'INCREMENTO', width: anchoP(ancho, 10) },
                    { name: 'ADMITE_REDONDEO', index: 'ADMITE REDONDEO', hidden: true },
                    { name: 'FECHA_REGISTRO', index: 'FECHA_REGISTRO', hidden: true },

                    { name: 'OBSERVACION', index: 'OBSERVACION', hidden: true },
                    { name: 'SW_SOLICITA_AUTORIZACION_ADMISION', index: 'SW_SOLICITA_AUTORIZACION_ADMISION', hidden: true },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                    { name: 'HC', index: 'HC', hidden: true },
                    { name: 'ID_PROGRAMA', index: 'ID_PROGRAMA', hidden: true },
                    { name: 'PROGRAMA', index: 'PROGRAMA', width: anchoP(ancho, 20) },

                    { name: 'CODIGO_ADMINISTRADORA', index: 'CODIGO_ADMINISTRADORA', hidden: true },
                    { name: 'NUMERO_POLIZA', index: 'NUMERO_POLIZA', hidden: true },
                    { name: 'ID_MODALIDAD_CONTRATO', index: 'ID_PROGRAMA', hidden: true },
                    { name: 'ID_COBERTURA', index: 'ID_COBERTURA', hidden: true },
                ],
                height: 270,
                width: 1100,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtIdPlan', datosRow.ID_PLAN, 1)
                    asignaAtributo('txtDescPlan', datosRow.DESCRIPCION, 0)
                    asignaAtributo('txtIdAdministradora', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.ADMINISTRADORA), 0)
                    asignaAtributo('cmbIdTipoRegimen', datosRow.ID_TIPO_REGIMEN, 0)
                    asignaAtributo('cmbIdTipoCliente', datosRow.ID_TIPO_CLIENTE, 0)
                    asignaAtributo('txtNumeroContrato', datosRow.NUM_CONTRATO, 0)
                    asignaAtributo('txtFechaInicio', datosRow.FECHA_INICIO, 0)
                    asignaAtributo('txtFechaFinal', datosRow.FECHA_FINAL, 0)
                    asignaAtributo('txtMontoContrato', datosRow.MONTO_CONTRATO, 0)
                    asignaAtributo('cmbIdTipoPlan', datosRow.TIPO_PLAN, 0)
                    asignaAtributo('cmbIdTarifario', datosRow.ID_TARIFARIO, 0)
                    asignaAtributo('txtPorcentajeIncremento', datosRow.PORCEN_INCREMENTO, 0)
                    asignaAtributo('cmbAdmiteRedondeo', datosRow.ADMITE_REDONDEO, 0)

                    asignaAtributo('lblFechaRegistro', datosRow.FECHA_REGISTRO, 0)
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0)
                    asignaAtributo('cmbSolicitaAutorizacionAdmision', datosRow.SW_SOLICITA_AUTORIZACION_ADMISION, 0)
                    asignaAtributo('lblIdElaboro', datosRow.ID_ELABORO, 0)
                    asignaAtributo('cmbIdEstadoEdit', datosRow.ID_ESTADO, 0)
                    asignaAtributo('lblPlanM', datosRow.ID_PLAN, 1)
                    asignaAtributo('cmbVerHc', datosRow.ID_PROGRAMA, 0)
                    asignaAtributo('cmbIdCoberturaBeneficios', datosRow.ID_COBERTURA, 0)


                    asignaAtributo('txtCodigoAdministradora', datosRow.CODIGO_ADMINISTRADORA, 0)
                    /*cargarComboGRALCondicion1('cmbPadre', '1', 'cmbIdModalidadContrato',8000 , valorAtributo('cmbIdTipoPlan'));
                    setTimeout(() => {
                        if (valorAtributo('cmbIdModalidadContrato') == datosRow.ID_MODALIDAD_CONTRATO) {
                            asignaAtributo('cmbIdModalidadContrato', datosRow.ID_MODALIDAD_CONTRATO, 0)
                        } else {
                            asignaAtributo('cmbIdModalidadContrato', '', 0)
                        }
                    }, 100);*/
                    asignaAtributo('cmbIdModalidadContrato', datosRow.ID_MODALIDAD_CONTRATO, 0)

                    console.log("mod: " + datosRow.ID_MODALIDAD_CONTRATO + " || " + valorAtributo('cmbIdModalidadContrato'))

                    asignaAtributo('txtNumeroPoliza', datosRow.NUMERO_POLIZA, 0)



                    buscarFacturacion('listGrillaPlanesDetalle')
                    setTimeout(() => {
                        buscarFacturacion('listGrillaPlanExcepcion')
                        setTimeout(() => {
                            buscarFacturacion('listGrillaPlanesMedicamentos')
                            setTimeout(() => {
                                buscarFacturacion('listGrillaRangos')
                                setTimeout(() => {
                                    buscarFacturacion('listGrillaContratos')
                                    setTimeout(() => {
                                        buscarFacturacion('listGrillaProcedimientos')
                                        setTimeout(() => {
                                            buscarFacturacion('listGrillaMunicipios')
                                            setTimeout(() => {
                                                buscarFacturacion('listSedePlan')
                                            }, 300);
                                        }, 300);
                                    }, 300);
                                }, 300);
                            }, 300);
                        }, 300);
                    }, 300);
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPlanesDetalle':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=408&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdPlan'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID', 'CUPS', 'COD EXTERNO', 'PROCEDIMIENTO', 'TARIFARIO BASE',
                    'PRECIO TARIF BASE', 'PRECIO', '% DESCUENTO', 'C DEBIDO', 'C CREDITO', 'SW_NOMBRE_EXTERNO',
                    'NOMBRE_EXTERNO', 'CANTIDAD', 'AUTORIZACION'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID', width: anchoP(ancho, 5) },
                    { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 6) },
                    { name: 'COD_EXTERNO', index: 'COD_EXTERNO', width: anchoP(ancho, 6) },
                    { name: 'PROCEDIMIENTO', index: 'PROCEDIMIENTO', width: anchoP(ancho, 35) },
                    { name: 'TARIFARIO_BASE', index: 'TARIFARIO_BASE', hidden: true },
                    { name: 'PRECIO_TARIFARIO_BASE', index: 'PRECIO_TARIFARIO_BASE', hidden: true },
                    { name: 'PRECIO', index: 'PRECIO', width: anchoP(ancho, 6) },
                    { name: 'PORCEN_DESCUENTO', index: 'PORCEN_DESCUENTO', hidden: true },
                    { name: 'CUENTA_DEBIDO', index: 'CUENTA_DEBIDO', hidden: true },
                    { name: 'CUENTA_CREDITO', index: 'CUENTA_CREDITO', hidden: true },
                    { name: 'SW_NOMBRE_EXTERNO', index: 'SW_NOMBRE_EXTERNO', hidden: true },
                    { name: 'NOMBRE_EXTERNO', index: 'NOMBRE_EXTERNO', hidden: true },
                    { name: 'CANTIDAD_MAXIMA', index: 'CANTIDAD', width: anchoP(ancho, 6) },
                    { name: 'AUTORIZACION', index: 'AUTORIZACION', width: anchoP(ancho, 7) }

                ],
                height: 170,
                autowidth: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    console.log(datosRow)
                    asignaAtributo('txtIdProcedimiento', concatenarCodigoNombre(datosRow.ID_PROCEDIMIENTO, datosRow.CUPS + " " + datosRow.PROCEDIMIENTO), 0)
                    asignaAtributo('txtPrecioProcedimiento', datosRow.PRECIO, 0)
                    asignaAtributo('txtCodigoExterno', datosRow.COD_EXTERNO, 0)
                    asignaAtributo('cmbNombreExterno', datosRow.SW_NOMBRE_EXTERNO, 0)
                    asignaAtributo('txtCantidadMaximaMes', datosRow.CANTIDAD_MAXIMA, 0)
                    //asignaAtributo('cmbAutorizacion', datosRow.AUTORIZACION, 0)

                    $('#cmbAutorizacion').val(datosRow.AUTORIZACION == 'SI' ? 'S' : 'N');

                    buscarFacturacion('listGrillaPlanExcepcion')
                    var mostrar = datosRow.SW_NOMBRE_EXTERNO === 'S' ? 0 : 1;

                    asignaAtributo('txtNombreExterno', datosRow.NOMBRE_EXTERNO, mostrar)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaTarifario':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=397" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodBus'));
            add_valores_a_mandar(valorAtributo('txtNomBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'ID_TIPO', 'TIPO'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 10) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },
                    { name: 'ID_TIPO', index: 'ID_TIPO', width: anchoP(ancho, 10) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 20) },
                ],
                height: 200,
                width: 1100,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    // limpiarDatosAdmision();				   
                    asignaAtributo('lblId', datosRow.ID, 0)
                    asignaAtributo('lblIdTarifario', datosRow.ID, 0)
                    asignaAtributo('lblNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbClase', datosRow.ID_CLASE, 0)
                    asignaAtributo('cmbTipo', datosRow.TIPO, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    buscarFacturacion('listGrillaTarifarioDetalle')
                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaTarifarioDetalle':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=398" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblId'));
            add_valores_a_mandar(valorAtributo('txtIdCargoBus'));
            add_valores_a_mandar(valorAtributo('txtNomCargoBus'));
            add_valores_a_mandar(valorAtributo('cmbVigencia'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_TARIFARIO', 'ID_GRUPO_TARIFARIO', 'ID_SUBGRUPO_TARIFARIO', 'CARGO', 'DESCRIPCION', 'PRECIO', 'ID_TIPO_CARGO',
                    'ID_GRUPO_TIPO_CARGO', 'GRAVAMEN', 'SW_CANTIDAD', 'ID_NIVEL', 'SW_HONORARIOS', 'ID_CONCEPTOS_RIPS', 'SW_UVRS',
                    'ID_GRUPOS_MAPIPOS', 'ID_TIPO_UNIDAD', 'PORCENTAJE_DEFAULT', 'EQUIVALENCIA', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID_TARIFARIO', index: 'ID_TARIFARIO', width: anchoP(ancho, 10) },

                    { name: 'ID_GRUPO_TARIFARIO', index: 'ID_GRUPO_TARIFARIO', hidden: true },
                    { name: 'ID_SUBGRUPO_TARIFARIO', index: 'ID_SUBGRUPO_TARIFARIO', hidden: true },

                    { name: 'CARGO', index: 'CARGO', width: anchoP(ancho, 10) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 55) },
                    { name: 'PRECIO', index: 'PRECIO', width: anchoP(ancho, 10) },

                    { name: 'ID_TIPO_CARGO', index: 'ID_TIPO_CARGO', hidden: true },
                    { name: 'ID_GRUPO_TIPO_CARGO', index: 'ID_GRUPO_TIPO_CARGO', hidden: true },

                    { name: 'GRAVAMEN', index: 'GRAVAMEN', hidden: true },
                    { name: 'SW_CANTIDAD', index: 'SW_CANTIDAD', hidden: true },
                    { name: 'ID_NIVEL', index: 'ID_NIVEL', hidden: true },

                    { name: 'SW_HONORARIOS', index: 'SW_HONORARIOS', hidden: true },
                    { name: 'ID_CONCEPTOS_RIPS', index: 'ID_CONCEPTOS_RIPS', hidden: true },
                    { name: 'SW_UVRS', index: 'SW_UVRS', hidden: true },

                    { name: 'ID_GRUPOS_MAPIPOS', index: 'ID_GRUPOS_MAPIPOS', hidden: true },

                    { name: 'ID_TIPO_UNIDAD', index: 'ID_TIPO_UNIDAD', hidden: true },
                    { name: 'PORCENTAJE_DEFAULT', index: 'PORCENTAJE_DEFAULT', hidden: true },
                    { name: 'EQUIVALENCIA', index: 'EQUIVALENCIA', width: anchoP(ancho, 10) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 3) }

                ],
                height: 300,
                width: 1100,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    // limpiarDatosAdmision();				   
                    asignaAtributo('lblIdTarifario', datosRow.ID_TARIFARIO, 0)
                    asignaAtributo('lblCargo', datosRow.CARGO, 0)
                    asignaAtributo('txtDescCargoTarifario', datosRow.DESCRIPCION, 0)

                    asignaAtributo('cmbIdGrupoTipoCargo', datosRow.ID_GRUPO_TIPO_CARGO, 0)
                    asignaAtributo('cmbIdTipoCargo', datosRow.ID_TIPO_CARGO, 0)

                    asignaAtributo('cmbIdGrupoTarifario', datosRow.ID_GRUPO_TARIFARIO, 0)
                    asignaAtributo('cmbIdSubGrupoTarifario', datosRow.ID_SUBGRUPO_TARIFARIO, 0)

                    asignaAtributo('cmbIdConceptoRips', datosRow.ID_CONCEPTOS_RIPS, 0)
                    asignaAtributo('cmbIdNivelAtencion', datosRow.ID_NIVEL, 0)

                    asignaAtributo('txtUnidadPrecio', datosRow.PRECIO, 0)
                    asignaAtributo('cmbIdTipoUnidad', datosRow.ID_TIPO_UNIDAD, 0)

                    asignaAtributo('txtGravamen', datosRow.GRAVAMEN, 0)
                    asignaAtributo('cmbHonorarios', datosRow.SW_HONORARIOS, 0)
                    asignaAtributo('cmbExigeCantidad', datosRow.SW_CANTIDAD, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                    buscarFacturacion('listGrillaTarifarioEquivalencia')
                    $('#drag' + ventanaActual.num).find('#txtIdCargoNuevo').attr('disabled', 'disabled');

                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaTarifarioEquivalencia':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=399" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdTarifario'));
            add_valores_a_mandar(valorAtributo('lblCargo'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'CARGO', 'ID CUPS', 'DESCRIPCION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'CARGO', index: 'CARGO', width: anchoP(ancho, 20) },
                    { name: 'ID_CUPS', index: 'ID_CUPS', width: anchoP(ancho, 20) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 60) },
                ],
                height: 100,
                width: 1000,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    mostrar('divVentanitaEquivalencias')
                    asignaAtributo('lblCargo', datosRow.CARGO, 0)
                    asignaAtributo('lblIdEquivalenciaCups', datosRow.ID_CUPS, 0)
                    asignaAtributo('lblNombreEquivalencia', datosRow.DESCRIPCION, 0)

                },

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaProcedimiento':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusIdProcedimientoBus'));
            add_valores_a_mandar(valorAtributo('cmbClaseBus'));
            add_valores_a_mandar(valorAtributo('cmbClaseBus'));
            add_valores_a_mandar(valorAtributo('cmbTipoBus'));
            add_valores_a_mandar(valorAtributo('cmbTipoBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'TIPO_RIPS', 'RIPS', 'ID_CENTRO_COSTO', 'NOMBRE_COSTO', 'ID_GRUPO_CENTRO_COSTO',
                    'GRUPO_CENTRO_COSTO', 'NIVEL', 'CUPS', 'CODIGO INTERNO', 'COD_CONTABLE', 'ID_TIPO_SERVICIO', 'TIPO_SERVICIO', 'COSTO', 'ID_CLASE', 'CLASE', 'TIPO', 'VIGENTE', 'MOSTRAR_HC', 'VIGENTEMASDEUNPROCEDIMIENTO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 6) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 50) },
                    { name: 'ID_TIPO_RIPS', index: 'ID_TIPO_RIPS', hidden: true },
                    { name: 'N_RIP', index: 'N_RIP', hidden: true },
                    { name: 'ID_CENTRO_COSTO', index: 'ID_CENTRO_COSTO', hidden: true },
                    { name: 'NOMBRE_COSTO', index: 'NOMBRE_COSTO', hidden: true },
                    { name: 'ID_GRUPO_CENTRO_COSTO', index: 'ID_GRUPO_CENTRO_COSTO', hidden: true },
                    { name: 'GRUPO_CENTRO_COSTO', index: 'GRUPO_CENTRO_COSTO', hidden: true },
                    { name: 'NIVEL', index: 'NIVEL', hidden: true },
                    { name: 'CUPS', index: 'CUPS', hidden: true },
                    { name: 'CODIGO_INTERNO', index: 'CODIGO INTERNO', width: anchoP(ancho, 8) },
                    { name: 'COD_CONTABLE', index: 'COD_CONTABLE', hidden: true },
                    { name: 'ID_TIPO_SERVICIO', index: 'ID_TIPO_SERVICIO', hidden: true },
                    { name: 'TIPO_SERVICIO', index: 'TIPO_SERVICIO', hidden: true },
                    { name: 'COSTO', index: 'COSTO', hidden: true },
                    { name: 'ID_CLASE', index: 'ID_CLASE', hidden: true },
                    { name: 'CLASE', index: 'CLASE', width: anchoP(ancho, 8) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 4) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },
                    { name: 'MOSTRAR_HC', index: 'MOSTRAR_HC', hidden: true },
                    { name: 'VIGENTEMASDEUNPROCEDIMIENTO', index: 'VIGENTEMASDEUNPROCEDIMIENTO', hidden: true },

                ],
                height: 500,
                //width: 990,
                autowidth: true,
                pager: jQuery('#pager'),
                rowNum: 500,
                rownumbers: true,
                viewrecords: true,
                loadonce: true,
                toppager: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    // limpiarDatosAdmision();				   
                    asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbIdTipoRips', datosRow.ID_TIPO_RIPS, 0)
                    asignaAtributoCombo('cmbIdCentroCosto', datosRow.ID_CENTRO_COSTO, datosRow.NOMBRE_COSTO)
                    asignaAtributoCombo('cmbIdGrupoCentroCosto', datosRow.ID_GRUPO_CENTRO_COSTO, datosRow.GRUPO_CENTRO_COSTO)
                    asignaAtributo('cmbNivel', datosRow.NIVEL, 0)
                    asignaAtributo('txtCups', datosRow.CUPS, 0)
                    asignaAtributo('txtCodigoInterno', datosRow.CODIGO_INTERNO, 0)
                    asignaAtributo('txtCodContable', datosRow.COD_CONTABLE, 0)

                    asignaAtributo('txtCosto', datosRow.COSTO, 0)

                    asignaAtributo('cmbClase', datosRow.ID_CLASE, 0)
                    asignaAtributoCombo('cmbTipoServicio', datosRow.ID_TIPO_SERVICIO, datosRow.TIPO_SERVICIO)
                    asignaAtributo('cmbTipo', datosRow.TIPO, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributo('cmbMostrarHc', datosRow.MOSTRAR_HC, 0)
                    asignaAtributo('cmbMostrarV', datosRow.VIGENTEMASDEUNPROCEDIMIENTO, 0)

                }

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar, datatype: 'xml' }).trigger('reloadGrid');
            break;



    }
}

function desactivarNombreExterno() {
    if (valorAtributo('cmbNombreExterno') === 'N') {
        asignaAtributo('txtNombreExterno', '', 1)
    } else {
        asignaAtributo('txtNombreExterno', '', 0)
    }
}

function desactivarNombreExternoArticulo() {
    if (valorAtributo('cmbNombreExternoArticulo') === 'N') {
        asignaAtributo('txtNombreExternoArticulo', '', 1)
    } else {
        asignaAtributo('txtNombreExternoArticulo', '', 0)
    }
}

function modificarAdmisionFacturacion() {
    if (valorAtributo('lblIdAdmision') != "") {
        if (valorAtributo("lblIdFactura") != "") {
            modificarCRUD("modificarAdmisionFactura")
        } else {
            modificarCRUD("modificarAdmisionSinFactura")
        }
    } else {
        alert('SELECCIONE UNA ADMISION PARA MODIFICAR')
    }
}

function eliminarAdmisionFacturacion() {
    var funcion = '';
    if (valorAtributo('lblIdAdmision') != '') {
        funcion = valorAtributo('txtEstadoAdmisionFactura') === 'SIN_FACTURA' ? 'eliminarAdmisionSinFactura' : 'eliminarAdmisionFactura';
        modificarCRUD(funcion)
    } else {
        alert('SELECCIONE UNA ADMISION PARA ELIMINAR')
    }
}

/*function completarNombreExterno(){

    if(valorAtributo('txtIdProcedimiento') != ''){
        var nombre = unescape(valorAtributo('txtIdProcedimiento').split('-')[1]);
        if(nombre){
                nombre = unescape(nombre.split('::')[0]);
                asignaAtributo('txtNombreExterno',nombre,0);
        }        
    }

}*/

function selectRowFacturacion(arg, rowid) {

    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

    asignaAtributo('lblRowId', rowid, 0)

    switch (arg) {

        case 'listGrillaFacturasCartera':

            asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
            asignaAtributo('lblTotalFactura', datosRow.TOTAL_FACTURA, 0)
            asignaAtributo('lblSaldoFactura', datosRow.SALDO, 0)
            asignaAtributo('lblIdTipoPago', datosRow.ID_TIPO_PAGO, 0)
            asignaAtributo('lblTipoPago', datosRow.TIPO_PAGO, 0)
            limpiaAtributo('lblIdRecibo', 0)

            buscarFacturacion('listGrillaRecibosCartera')

            break;


        case 'listFacturasVentas':


            asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
            asignaAtributo('lblIdEpsPlan', datosRow.ID_ADMINISTRADORA, 0)
            asignaAtributo('lblIdPlan', datosRow.ID_PLAN, 0)
            asignaAtributo('lblNomPlan', datosRow.PLAN, 0)
            asignaAtributo('lblIdRegimen', datosRow.ID_REGIMEN, 0)
            asignaAtributo('lblNomRegimen', datosRow.REGIMEN, 0)
            asignaAtributo('lblIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, 0)
            asignaAtributo('lblNomTipoAfiliado', datosRow.NOM_TIPO_AFILIADO, 0)
            asignaAtributo('lblIdRango', datosRow.ID_RANGO, 0)
            asignaAtributo('lblNomRango', datosRow.RANGO, 0)
            asignaAtributo('lblUsuarioCrea', datosRow.USUARIO_CREA, 0)
            asignaAtributo('lblIdDoc', datosRow.ID_DOC, 0)
            asignaAtributo('lblIdEstadoDoc', datosRow.ID_ESTADO_DOC, 0)
            asignaAtributo('lblIdBodega', datosRow.ID_BODEGA, 0)

            asignaAtributo('txtAdministradora1', concatenarCodigoNombre(datosRow.ID_ADMINISTRADORA, datosRow.ADMINISTRADORA), 0)
            asignaAtributo('txtIdAdminisAdmision', datosRow.ID_ADMINISTRADORA, 0)

            //informacion admision
            asignaAtributoCombo('cmbIdTipoRegimen', datosRow.ID_REGIMEN, datosRow.REGIMEN);
            asignaAtributoCombo('cmbIdTipoAfiliado', datosRow.ID_TIPO_AFILIADO, datosRow.NOM_TIPO_AFILIADO);
            asignaAtributoCombo('cmbIdRango', datosRow.ID_RANGO, datosRow.RANGO);
            asignaAtributo('lblIdPlanContratacion', datosRow.ID_PLAN, 0)
            asignaAtributo('lblNomPlanContratacion', datosRow.PLAN, 0)
            asignaAtributo('lblIdReciboInicial', datosRow.ID_RECIBO, 0)
            asignaAtributo('txtObservacionVentas', datosRow.OBSERVACIONES, 0)

            buscarFacturacion('listArticulosVentas')
            setTimeout("buscarFacturacion('listOtrosArticulosVentas')", 400);
            setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')", 800);
            setTimeout("deshabilitarAbonoInicial()", 1000);


            break;
    }
}

function deshabilitarAbonoInicial() {

    if ($('input:radio[name=formaPago]:checked').val() == '1') {
        limpiaAtributo('txtValorInicial', 1)
        limpiaAtributo('txtObservacionInicial', 1)
        desHabilitar('btnCrearReciboInicial', 1)
    } else {
        limpiaAtributo('txtValorInicial', 0)
        limpiaAtributo('txtObservacionInicial', 0)
        desHabilitar('btnCrearReciboInicial', 0)
    }
}


function recargarFirmaTrabajo(id) {

    var nombre = '';

    switch (id) {

        case 'imgFirmaEnviado':
            nombre = valorAtributo('lblFirma1');
            break;

        case 'imgFirmaCumplido':
            nombre = valorAtributo('lblFirma2');
            break;

        case 'imgFirmaEntregado':
            nombre = valorAtributo('lblFirma3');
            adicionarFirma('firmarPaciente')
            break;
    }

    var imagen = '/docPDF/firmas/ventas/' + nombre + '?' + Math.floor((Math.random() * 100) + 1);
    $("#imgFirmaTrabajo").attr('src', imagen);

}


function adicionarFirma(atributo) {

    var ban = false;

    if (atributo === 'chkEnvioFirma' && valorAtributo('lblFirma1') != '') {
        alert('PACIENTE YA FIRMO ENVIADO')
        ban = true;
    } else if (atributo === 'chkCumplidoFirma' && valorAtributo('lblFirma2') != '') {
        alert('PACIENTE YA FIRMO CUMPLIDO')
        ban = true;
    } else if (valorAtributo('lblIdEstadoFirmar') != '') {

        if (valorAtributo('lblIdEstadoFirmar') != valorAtributo('txtIdEstado')) {
            alert('NO PUEDE AGREGAR UN ESTADO DIFERENTE')
            ban = true;
        } else if (atributo === 'chkEnvioFirma' && valorAtributo('lblIdFuncionarioFirmar') != valorAtributo('cmbNombreFuncionario1')) {
            alert('NO PUEDE ESCOGER OTRO FUNCIONARIO')
            ban = true;
        } else if (atributo === 'chkCumplidoFirma' && valorAtributo('lblIdFuncionarioFirmar') != valorAtributo('cmbNombreFuncionario2')) {
            alert('NO PUEDE ESCOGER OTRO FUNCIONARIO')
            ban = true;
        }


    } else {

        asignaAtributo('lblIdEstadoFirmar', valorAtributo('txtIdEstado'), 0)
        asignaAtributo('lblEstadoFirmar', valorAtributo('txtEstado'), 0)

        if (atributo === 'chkEnvioFirma') {
            asignaAtributo('lblIdFuncionarioFirmar', valorAtributo('cmbNombreFuncionario1'), 0)
            asignaAtributo('lblFuncionarioFirmar', valorAtributoCombo('cmbNombreFuncionario1'), 0)

        } else if (atributo === 'chkCumplidoFirma') {
            asignaAtributo('lblIdFuncionarioFirmar', valorAtributo('cmbNombreFuncionario1'), 0)
            asignaAtributo('lblFuncionarioFirmar', valorAtributoCombo('cmbNombreFuncionario2'), 0)
        }

    }



    if (ban) {
        limpiaAtributo('chkEnvioFirma', 0)
        limpiaAtributo('chkCumplidoFirma', 0)
    }

    if ($('#drag' + ventanaActual.num).find('#' + atributo).attr('checked') || atributo === 'firmarPaciente') {
        ordenes.add(valorAtributo('txtNumeroOrdenN'))
        trabajos.add(valorAtributo('lblIdTrabajo'))
    } else {
        ordenes.delete(valorAtributo('txtNumeroOrdenN'))
        trabajos.delete(valorAtributo('lblIdTrabajo'))
        if (ordenes.size == 0) {
            limpiaAtributo('lblIdEstadoFirmar', 0)
            limpiaAtributo('lblEstadoFirmar', 0)
            limpiaAtributo('lblFuncionarioFirmar', 0)
            limpiaAtributo('lblIdFuncionarioFirmar', 0)
        }
    }

    var o = '';
    for (let item of ordenes) {
        o += item + ' - ';
    }

    var t = '';
    for (let item of trabajos) {
        t += item + ',';
    }

    asignaAtributo('lblOrdenesFirmar', o)
    asignaAtributo('lblTrabajosFirmar', t)

    modificarCRUD('firmarPaciente')

}

function imprimirFacturasPDF() {

    var lista_facturas = [];
    var idFacturas;
    var ids = $("#listGrillaFacturas").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_listGrillaFacturas_" + ids[i];
        if ($("#" + c).is(':checked')) {
            datosRow = $("#listGrillaFacturas").getRowData(ids[i]);
            lista_facturas.push(datosRow.ID_FACTURA);
        }
    }

    idFacturas = lista_facturas.toString();
    if (idFacturas != '') {

        var nuevaURL = "ireports/facturacion/generarFacturas.jsp?numFacturas=" + idFacturas;
        window.open(nuevaURL, '', 'width=1000,height=682,scrollbars=NO,statusbar=NO,left=350,top=230');

    } else {
        alert('DEBE SELECCIONAR AL MENOS UNA FACTURA')
    }
}


function totalEnvioRips(id_envio) {
    limpiaAtributo('lblTotalEnvio')
    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = resultadoTotalEnvioRips;
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=totalEnvioRips";
    valores_a_mandar = valores_a_mandar + "&idEnvio=" + id_envio;
    varajaxInit.send(valores_a_mandar);
}

function resultadoTotalEnvioRips() {
    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            totalCuenta = raiz.getElementsByTagName('totalEnvioRips')[0].firstChild.data
            if (totalCuenta != 'null') {
                asignaAtributo('lblTotalEnvio', raiz.getElementsByTagName('totalEnvioRips')[0].firstChild.data, 0)
            }
            buscarFacturacion("listaFacturasCuenta")
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
    }
}
function listaFacturas() {
    let facturasData  =  gestionFacturacion.obtenerSeleccionGrilla('listGrillaFacturas')
    let facturas = [];

    facturasData.forEach((factura)=>{
    facturas.push(factura.ID_FACTURA)
   })

   
   return facturas
    /* var facturas = [];

    var ids = $("#listGrillaFacturas").getDataIDs();

    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + ids[i];
        console.log('c',c)
        if ($("#" + c).is(":checked")) {
            datosRow = $("#listGrillaFacturas").getRowData(ids[i]);
            console.log('datosRow',datosRow)
            facturas.push(datosRow.ID_FACTURA);
        }
    }

    return facturas */
   
}

function auditarFacturas(facturas){

    var factura = facturas.pop()
    $.ajax({
        url: "/clinica/paginas/accionesXml/guardarHc_xml.jsp",
        type: "GET",
        data: {
            "accion": "auditarFactura",
            "lblIdFactura": factura,
        },
        beforeSend: function () {
        },
        success: function (data) {
            $("#lblLogAuditoriaBloque").html(" Log -> " + factura + ":" + data.getElementsByTagName('diligenciado')[0].firstChild.data)
            console.log(factura + ":" + data.getElementsByTagName('diligenciado')[0].firstChild.data)
        },
        complete: function () {
            if(facturas.length > 0){
                auditarFacturas(facturas)
            } else {
                buscarFacturacion('listGrillaFacturas')
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (1)")
        }
    });
}
