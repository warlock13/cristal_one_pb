
function TabActivoEventos(idTab) {
	tabActivo = idTab;
	switch (idTab) {
		case 'divPaciente':
			if (valorAtributo('lblIdPaciente') != '') {
				asignaAtributo('txtIdBusPaciente', valorAtributo('lblIdPaciente') + '-')
				buscarHC('listDiagnosticos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
				setTimeout("buscarInformacionBasicaPaciente()", 1000);
				setTimeout("asignaAtributo('txtIdBusPaciente',valorAtributo('txtIdBusPaciente')+'-'+valorAtributo('txtApellido1')+'-'+valorAtributo('txtNombre1'))", 5000);
			}
			break;
		case 'divFormula':
			if (valorAtributo('lblIdPaciente') != '') {
				asignaAtributo('txtIdBusPaciente', valorAtributo('lblIdPaciente') + '-')
				buscarHC('listFolios', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

			}
			break;

		case 'divClasificarEA':
			buscarEvento('listClasificarEA')
			break;
		case 'divAnalisisEA':
			buscarEvento('listAnalisisEA')
			break;
		case 'divPlanAccion':
			buscarEvento('listPlanAccion')
			break;
		case 'divEspinaPescado':
			buscarEvento('listEspinaPescado')
			break;
		case 'divEventoRelacionado':
			buscarEvento('listEventoRelacionado')
			break;
		case 'divControl':
			buscarEvento('listControl')
			break;
		case 'divHistoricosAtencion':
			buscarHC('listDocumentosHistoricosTodos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
			break;

	}
}
function cargarTableAnalisisEvento(paginaOpcion) {

	document.getElementById("divParaAnalisisEvento").innerHTML = ''
	document.getElementById("divParaAnalisisTrabajos").innerHTML = ''
	document.getElementById("divParaAnalisisNoConforme").innerHTML = ''

	varajaxMenu = crearAjax();
	valores_a_mandar = "";
	varajaxMenu.open("POST", '/clinica/paginas/calidad/evento/' + paginaOpcion + '.jsp', true);
	varajaxMenu.onreadystatechange = function () { llenarcargarTableAnalisisEvento(paginaOpcion) };
	varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);
}
function llenarcargarTableAnalisisEvento(paginaOpcion) {
	if (varajaxMenu.readyState == 4) {
		if (varajaxMenu.status == 200) {
			switch (paginaOpcion) {

				case 'seguridadPaciente':
					document.getElementById("divParaAnalisisEvento").innerHTML = varajaxMenu.responseText;
					$("#tabsPrincipalAnalisis").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
					TabActivoEventos('divPaciente')
					calendario('txtFechaTarea', 0)
					break;
				case 'trabajos':
					document.getElementById("divParaAnalisisTrabajos").innerHTML = varajaxMenu.responseText;
					$("#tabsPrincipalAnalisisTrabajos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
					TabActivoEventos('divPaciente')
					calendario('txtFechaTarea', 0)
					calendario('txtConFechaEntregalab', 0)
					calendario('txtConFechaRecepcion', 0)
					calendario('txtConFechaEntrega', 0)


					break;
				case 'noConforme':
					document.getElementById("divParaAnalisisNoConforme").innerHTML = varajaxMenu.responseText;
					$("#tabsPrincipalAnalisisNoConforme").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
					TabActivoEventos('divPlanAccion')
					calendario('txtFechaTarea', 0)
					break;

			}

		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxMenu.readyState == 1) {
		swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	}
}



function cargarCriterioFactorEA(idElemDestino, idDependeDe) {
	cargarComboGRALCondicion1('cmbPadre', '1', idElemDestino, 525, valorAtributo(idDependeDe));
}

function cargarPacienteEnTipoEvento(idTipoEvento) {

	ocultar('tablaPacienteReporteEvento')
	ocultar('tablaPacienteIngresarEvento')

	switch (idTipoEvento) {

		case '8':
			mostrar('tablaPacienteReporteEvento')
			break;
		case '10':
			mostrar('tablaPacienteReporteEvento')
			break;
		case '11':
			//
			break;


		case '3':
			mostrar('tablaEventoArea')
			break;


	}
}
function cargarPacienteEnTipo(idTipoEvento) {

	mostrar('btn_Bus')
	mostrar('btn_Buscar')
	ocultar('tablaPacienteGestionarEvento')
	ocultar('tablaPacienteIngresarEvento')
	mostrar('eventosDiv')
	ocultar('eventoId')

	switch (idTipoEvento) {

		case '10':
			mostrar('tablaPacienteGestionarEvento')
			ocultar('btn_Buscar')
			ocultar('eventosDiv')
			mostrar('eventoId')


			break;
		case '7':
			mostrar('btn_Bus')
			break;
	}
}
function desahibilitarComb(especialidad) {

	document.getElementById("cmbSitio").disabled = true;

	switch (especialidad) {

		case '2':
			document.getElementById("cmbSitio").disabled = false;
			break;
	}
}
function cargarPacienteEnTipoIngresar(idTipoEvento) {

	mostrar('tablaPacienteIngresarEvento')
	switch (idTipoEvento) {

		case '10':
			ocultar('tablaPacienteIngresarEvento')
			break;
		case '11':
			//
			break;
	}
}


function cargarAcordeonTipoEvento(idTipoEvento) {
	ocultar('divGestionEventoAdverso');
	ocultar('divAcordionTrabajos'); ocultar('divGestionTrabajos')
	ocultar('divAcordionNoConforme'); ocultar('divGestionNoConforme')

	switch (idTipoEvento) {

		case 'S':
			mostrar('divVentanitaAdjuntos');
		
			break;
		case 'T':
			ocultar('divVentanitaAdjuntos');
		break;
		
		case 'P':
			ocultar('divVentanitaAdjuntos');
			mostrar('divAcordionNoConforme');
			break;
	}
}

function buscarEvento(arg) {
	pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
	switch (arg) {

		case 'sedes':
			ancho = ( $('#'+arg).width());
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=936&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdentificacion'));			

			$('#'+arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['Tot','CODIGO', 'NOMBRE SEDE','TIPO'],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'CODIGO', index: 'CODIGO', hidden:true},
					{ name: 'NOMBRE', index: 'NOMBRE',width:70},
					{ name: 'TIPO', index: 'TIPO',width:30},	
				],				
				width:650,
				height:300,
				onSelectRow: function (rowid) {					
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('lblIdCodigo', datosRow.CODIGO, 0);					
				},
			});
			$('#'+arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
		break;

		case 'listEvento':
			// limpiarDivEditarJuan(arg); 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 10;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=52&parametros=";
			add_valores_a_mandar(valorAtributo('txtCodEventoBus'));
			add_valores_a_mandar(valorAtributo('txtEventoBus'));
			add_valores_a_mandar(valorAtributo('cmbMiAutoriaBus'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['Tot', 'TICKET', 'DESCRIPCION', 'ID_TIPO_EVENTO', 'TIPO_EVENTO', 'ID_PROCESO', 'PROCESO', 'OBSERVACION RESPUESTA', 'ID_PERSONAL_RESPONSABLE', 'PRIORIZACION', 'ETAPA', 'APLAZADO',
					'NO_DOC_RELACIONADO', 'DESCRIP_DOC_RELACIONADO', 'ESTADO', 'FECHA_REGISTRO', 'FECHA_INICIA', 'FECHA DE CREACION', 'FECHA DE ASIGNACION', 'FECHA ENTREGA APLAZADO', 'FECHA_SOLUCION', 'FECHA TENTATIVA SOLUCION', 'ID_APLAZADO_MOTIVO', 'MOTIVO_APLAZA', 'APLAZADO_OBSERVACION',
					'ID_ESTADO', 'ID_CALIFICACION', 'CALIFICACION', 'ID_PERSONAL_RECIBE', 'OBSERVACION_RECIBE', 'ID_PERSONA_REGISTRA', 'REPORTO','ID_SEDE','SEDE','ID_MODULO','MODULO'
				],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'TICKET', index: 'TICKET', width: 50},
					{ name: 'DESCRIPCION', index: 'DESCRIPCION', width: 400},
					{ name: 'ID_TIPO_EVENTO', index: 'ID_TIPO_EVENTO', hidden: true },
					{ name: 'TIPO_EVENTO', index: 'TIPO_EVENTO', hidden: true },

					{ name: 'ID_PROCESO', index: 'ID_PROCESO', hidden: true },
					{ name: 'PROCESO', index: 'PROCESO', hidden: true },
					{ name: 'OBSERVACIONES', index: 'OBSERVACIONES' , width: 400},
					{ name: 'ID_PERSONAL_RESPONSABLE', index: 'ID_PERSONAL_RESPONSABLE', hidden: true },
					{ name: 'PRIORIDAD', index: 'PRIORIZACION', hidden: true },
					{ name: 'ETAPA', index: 'ETAPA', hidden: true },
					{ name: 'APLAZADO', index: 'APLAZADO', hidden: true},

					{ name: 'NO_DOC_RELACIONADO', index: 'NO_DOC_RELACIONADO', hidden: true },
					{ name: 'DESCRIP_DOC_RELACIONADO', index: 'DESCRIP_DOC_RELACIONADO', hidden: true },
					{ name: 'ESTADO', index: 'ESTADO'},
					{ name: 'FECHA_REGISTRO', index: 'FECHA_REGISTRO', hidden: true },
					{ name: 'FECHA_INICIA', index: 'FECHA_INICIA', hidden: true },
					{ name: 'FECHA_CREA', index: 'FECHA_CREA'},
					{ name: 'FECHA_ENTREGA', index: 'FECHA_ENTREGA'},
					{ name: 'FECHA_ENTREGA_APLAZADO', index: 'FECHA_ENTREGA_APLAZADO', hidden: true },
					{ name: 'FECHA_SOLUCION', index: 'FECHA_SOLUCION'},
					{ name: 'FECHA_TENTATIVA_SOLUCION', index: 'FECHA_TENTATIVA_SOLUCION',},

					{ name: 'ID_APLAZADO_MOTIVO', index: 'ID_APLAZADO_MOTIVO', hidden: true },
					{ name: 'MOTIVO_APLAZA', index: 'MOTIVO_APLAZA', hidden: true },
					{ name: 'APLAZADO_OBSERVACION', index: 'APLAZADO_OBSERVACION', hidden: true },
					{ name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },

					{ name: 'ID_CALIFICACION', index: 'ID_CALIFICACION', hidden: true },
					{ name: 'CALIFICACION', index: 'CALIFICACION', hidden: true },
					{ name: 'ID_PERSONAL_RECIBE', index: 'ID_PERSONAL_RECIBE', hidden: true },
					{ name: 'OBSERVACION_RECIBE', index: 'OBSERVACION_RECIBE', hidden: true },
					{ name: 'ID_PERSONA_REGISTRA', index: 'ID_PERSONA_REGISTRA', hidden: true },
					{ name: 'PERSONA_REGISTRA', index: 'PERSONA_REGISTRA', hidden: true },
					{ name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
					{ name: 'SEDE', index: 'SEDE', hidden: true },
					{ name: 'ID_MODULO', index: 'ID_MODULO', hidden: true },
					{ name: 'MODULO', index: 'MODULO'},
				],

				//  pager: jQuery('#pagerGrilla'), 
				//height: 250, 
				width: ancho,
				shrinkToFit: false,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('txtIdEvento', datosRow.TICKET, 0);
					asignaAtributo('lblIdEventoARelacio', datosRow.TICKET, 0);
					asignaAtributo('lblDesEventoARelacio', datosRow.DESCRIPCION, 0);
					asignaAtributo('lblSede', datosRow.SEDE, 0);

					asignaAtributo('txtIdSede', datosRow.ID_SEDE + '-' + datosRow.SEDE, 0);
					asignaAtributo('txtDescripcion', datosRow.DESCRIPCION, 0);
					asignaAtributo('cmbIdTipoEvento', datosRow.ID_TIPO_EVENTO, 0);
					asignaAtributo('cmbIdIdProceso', datosRow.ID_PROCESO, 0);
					asignaAtributo('cmbIdModulos', datosRow.ID_MODULO, 0);
					buscarHistoria('listArchivosAdjuntosEvento');

					if (datosRow.ID_ESTADO == 0){
						habilitar('txtIdSede', 1)
						habilitar('txtDescripcion', 1)
						habilitar('cmbIdTipoEvento', 1)
						habilitar('cmbIdIdProceso', 1)
						habilitar('cmbIdModulos', 1)
					}else{
						habilitar('txtIdSede', 0)
						habilitar('txtDescripcion', 0)
						habilitar('cmbIdTipoEvento', 0)
						habilitar('cmbIdIdProceso', 0)
						habilitar('cmbIdModulos', 0)
					}

					if (['1', '2', '3', '5', '6',].includes(datosRow.ID_ESTADO)) {
						mostrar('divVentanitaAdjuntos');
						habilitar('btnSubirAdjuntosE', 0);
						ocultar('divAcordionNoConforme');
					}else{
						ocultar('divAcordionNoConforme');
						mostrar('divVentanitaAdjuntos');
						habilitar('btnSubirAdjuntosE', 1);
					}
					
					if (datosRow.ID_ESTADO == 4) {
						alert('Recuerde realizar la calificacion del servicio');
						ocultar('divVentanitaAdjuntos');
						mostrar('divAcordionNoConforme');
						habilitar('btn_CALIFICAR', 1);

					}

					limpiaAtributo('cmbIdCalificacion');
					limpiaAtributo('txtObservacionSugerencias');


				},
				gridComplete: function (){
					mostrar('divVentanitaAdjuntos');
					acordionArchivosAdjuntosEvento();
				},

			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;
		/*
			 case 'listEventoEnHc':   /*SOLO PARA CREAR*
			// limpiarDivEditarJuan(arg); 
			 ancho= 1100;
			 valores_a_mandar=pag;
			 valores_a_mandar=valores_a_mandar+"?idQuery=613&parametros=";
			 add_valores_a_mandar( valorAtributo('lblIdPaciente')  );		   			 			 
			 $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
			 url:valores_a_mandar, 	
			 datatype: 'xml', 
			 mtype: 'GET', 
			 
			 colNames:[ 'Tot','CODIGO','DESCRIPCION','ID_TIPO_EVENTO','TIPO_EVENTO','ID_PROCESO','PROCESO','OBSERVACION RESPUESTA','ID_PERSONAL_RESPONSABLE','PRIORIZACION', 'ETAPA', 'APLAZADO',
			 'NO_DOC_RELACIONADO','DESCRIP_DOC_RELACIONADO','FECHA_REGISTRO','FECHA_INICIA','FECHA_ENTREGA','FECHA_ENTREGA_APLAZADO','ID_APLAZADO_MOTIVO','MOTIVO_APLAZA','APLAZADO_OBSERVACION',
			 'ID_ESTADO','ESTADO','ID_CALIFICACION','CALIFICACION','ID_PERSONAL_RECIBE','OBSERVACION_RECIBE','FECHA_RECIBE','ID_PERSONA_REGISTRA','REPORTO'
			  ],
			 colModel :[ 
			   {name:'contador', index:'contador', hidden:true},  
			   {name:'CODIGO', index:'CODIGO', width: anchoP(ancho,2)},			   
			   {name:'DESCRIPCION', index:'DESCRIPCION', width: anchoP(ancho,30)}, 
			   {name:'ID_TIPO_EVENTO', index:'ID_TIPO_EVENTO', hidden:true},
			   {name:'TIPO_EVENTO', index:'TIPO_EVENTO',  hidden:true}, 
	  
			   {name:'ID_PROCESO', index:'ID_PROCESO', hidden:true},		 	
			   {name:'PROCESO', index:'PROCESO', hidden:true}, 
			   {name:'OBSERVACIONES', index:'OBSERVACIONES', width: anchoP(ancho,15)}, 
			   {name:'ID_PERSONAL_RESPONSABLE', index:'ID_PERSONAL_RESPONSABLE', hidden:true},
			   {name:'PRIORIDAD', index:'PRIORIZACION',  hidden:true}, 
			   {name:'ETAPA', index:'ETAPA', width: anchoP(ancho,7)}, 		 
			   {name:'APLAZADO', index:'APLAZADO', width: anchoP(ancho,5)}, 		 		 
			   
			   {name:'NO_DOC_RELACIONADO', index:'NO_DOC_RELACIONADO', hidden:true}, 
			   {name:'DESCRIP_DOC_RELACIONADO', index:'DESCRIP_DOC_RELACIONADO', hidden:true},
			   {name:'FECHA_REGISTRO', index:'FECHA_REGISTRO', hidden:true}, 
			   {name:'FECHA_INICIA', index:'FECHA_INICIA', hidden:true}, 		 
			   {name:'FECHA_ENTREGA', index:'FECHA_ENTREGA', width: anchoP(ancho,5)}, 
			   {name:'FECHA_ENTREGA_APLAZADO', index:'FECHA_ENTREGA_APLAZADO', hidden:true},
			   
			   {name:'ID_APLAZADO_MOTIVO', index:'ID_APLAZADO_MOTIVO', hidden:true}, 
			   {name:'MOTIVO_APLAZA', index:'MOTIVO_APLAZA', hidden:true}, 
			   {name:'APLAZADO_OBSERVACION', index:'APLAZADO_OBSERVACION', hidden:true},
			   {name:'ID_ESTADO', index:'ID_ESTADO', hidden:true},
			   {name:'ESTADO', index:'ESTADO', width: anchoP(ancho,5)}, 
			   
			   {name:'ID_CALIFICACION', index:'ID_CALIFICACION', hidden:true},	
			   {name:'CALIFICACION', index:'CALIFICACION',  hidden:true}, 	
			   {name:'ID_PERSONAL_RECIBE', index:'ID_PERSONAL_RECIBE', hidden:true},	
			   {name:'OBSERVACION_RECIBE', index:'OBSERVACION_RECIBE', hidden:true},	 	 
			   {name:'FECHA_RECIBE', index:'FECHA_RECIBE', hidden:true}, 	
			   {name:'ID_PERSONA_REGISTRA', index:'ID_PERSONA_REGISTRA', hidden:true}, 	 	 
			   {name:'PERSONA_REGISTRA', index:'PERSONA_REGISTRA', width: anchoP(ancho,15)} 	 
			   ], 
			 
		   //  pager: jQuery('#pagerGrilla'), 
			 height: 250, 
			 width: ancho+40,			
	  
		 });  
		$('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
		break;   
		*/
		case 'GestionEventoTrabajo':
			// limpiarDivEditarJuan(arg); 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=760&parametros=";
			add_valores_a_mandar(valorAtributo('txtCodEventoBus'));
			add_valores_a_mandar(valorAtributo('txtEventoBus'));
			add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
			add_valores_a_mandar(valorAtributo('cmbIdTipoEventoBus'));
			add_valores_a_mandar(valorAtributo('cmbResponsabilidadBus'));
			add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['Tot', 'CODIGO', 'DESCRIPCION', 'ID_TIPO_EVENTO', 'TIPO EVENTO', 'ID_PROCESO', 'PROCESO', 'OBSERVACIONES', 'ID_PERSONAL_RESPONSABLE', 'PRIORIZACION', 'ETAPA', 'APLAZADO',
					'NO_DOC_RELACIONADO', 'DESCRIP_DOC_RELACIONADO', 'FECHA_REGISTRO', 'FECHA_INICIA', 'FECHA_ENTREGA', 'FECHA_ENTREGA_APLAZADO', 'ID_APLAZADO_MOTIVO', 'MOTIVO_APLAZA', 'APLAZADO_OBSERVACION',
					'ID_ESTADO', 'ESTADO', 'ID_CALIFICACION', 'CALIFICACION', 'ID_PERSONAL_RECIBE', 'OBSERVACION_RECIBE', 'FECHA_RECIBE', 'ID_PERSONA_REGISTRA', 'PERSONA_REGISTRA', 'ID_PACIENTE', 'ID_FOLIO', 'TIPO_FOLIO', 'TAREAS CUMPLIMIENTO'
				],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'CODIGO', index: 'CODIGO', width: anchoP(ancho, 2) },
					{ name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 40) },
					{ name: 'ID_TIPO_EVENTO', index: 'ID_TIPO_EVENTO', hidden: true },
					{ name: 'TIPO_EVENTO', index: 'TIPO_EVENTO', width: anchoP(ancho, 10) },

					{ name: 'ID_PROCESO', index: 'ID_PROCESO', hidden: true },
					{ name: 'PROCESO', index: 'PROCESO', hidden: true },
					{ name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true },
					{ name: 'ID_PERSONAL_RESPONSABLE', index: 'ID_PERSONAL_RESPONSABLE', hidden: true },
					{ name: 'PRIORIDAD', index: 'PRIORIDAD', width: anchoP(ancho, 5) },
					{ name: 'ETAPA', index: 'ETAPA', width: anchoP(ancho, 5) },
					{ name: 'APLAZADO', index: 'APLAZADO', width: anchoP(ancho, 5) },

					{ name: 'NO_DOC_RELACIONADO', index: 'NO_DOC_RELACIONADO', hidden: true },
					{ name: 'DESCRIP_DOC_RELACIONADO', index: 'DESCRIP_DOC_RELACIONADO', hidden: true },
					{ name: 'FECHA_REGISTRO', index: 'FECHA_REGISTRO', hidden: true },
					{ name: 'FECHA_INICIA', index: 'FECHA_INICIA', hidden: true },
					{ name: 'FECHA_ENTREGA', index: 'FECHA_ENTREGA', hidden: true },
					{ name: 'FECHA_ENTREGA_APLAZADO', index: 'FECHA_ENTREGA_APLAZADO', hidden: true },

					{ name: 'ID_APLAZADO_MOTIVO', index: 'ID_APLAZADO_MOTIVO', hidden: true },
					{ name: 'MOTIVO_APLAZA', index: 'MOTIVO_APLAZA', hidden: true },
					{ name: 'APLAZADO_OBSERVACION', index: 'APLAZADO_OBSERVACION', hidden: true },
					{ name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
					{ name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 5) },

					{ name: 'ID_CALIFICACION', index: 'ID_CALIFICACION', hidden: true },
					{ name: 'CALIFICACION', index: 'CALIFICACION', hidden: true },
					{ name: 'ID_PERSONAL_RECIBE', index: 'ID_PERSONAL_RECIBE', hidden: true },
					{ name: 'OBSERVACION_RECIBE', index: 'OBSERVACION_RECIBE', hidden: true },
					{ name: 'FECHA_RECIBE', index: 'FECHA_RECIBE', hidden: true },
					{ name: 'ID_PERSONA_REGISTRA', index: 'ID_PERSONA_REGISTRA', hidden: true },
					{ name: 'PERSONA_REGISTRA', index: 'PERSONA_REGISTRA', hidden: true },
					{ name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
					{ name: 'ID_FOLIO', index: 'ID_FOLIO', hidden: true },
					{ name: 'TIPO_FOLIO', index: 'TIPO_FOLIO', hidden: true },
					{ name: 'CUMPLIMIENTO', index: 'CUMPLIMIENTO', width: anchoP(ancho, 5) }
				],


				//  pager: jQuery('#pagerGrilla'), 
				height: 250,
				width: ancho + 40,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('txtIdEvento', datosRow.CODIGO, 1);
					asignaAtributo('txtIdEstado', datosRow.CODIGO, 1);
					asignaAtributo('lblDescripcion', datosRow.DESCRIPCION, 1);
					asignaAtributo('cmbIdTipoEvento', datosRow.ID_TIPO_EVENTO, 0);
					asignaAtributo('cmbIdIdProceso', datosRow.ID_PROCESO, 0);
					asignaAtributo('txtObservacion', datosRow.OBSERVACIONES, 0);
					asignaAtributo('cmbIdIdResponsable', datosRow.ID_PERSONAL_RESPONSABLE, 0);
					asignaAtributo('txtPriorizacion', datosRow.PRIORIDAD, 0);
					asignaAtributo('txtNoDocumento', datosRow.NO_DOC_RELACIONADO, 0);
					asignaAtributo('txtDescDocumento', datosRow.DESCRIP_DOC_RELACIONADO, 0);
					asignaAtributo('txtFechaInicia', datosRow.FECHA_INICIA, 0);
					asignaAtributo('txtFechaEntrega', datosRow.FECHA_ENTREGA, 0);

					asignaAtributo('cmbIdMotivoAplaza', datosRow.ID_APLAZADO_MOTIVO, 0);
					asignaAtributo('txtFechaAplazada', datosRow.FECHA_ENTREGA_APLAZADO, 0);
					asignaAtributo('txtObservacionAplaza', datosRow.APLAZADO_OBSERVACION, 0);

					asignaAtributo('cmbIdCalificacion', datosRow.ID_CALIFICACION, 0);
					asignaAtributo('cmbIdPersonalRecibe', datosRow.ID_PERSONAL_RECIBE, 0);
					asignaAtributo('txtObservacionRecibe', datosRow.OBSERVACION_RECIBE, 0);
					asignaAtributo('txtFechaRecibe', datosRow.FECHA_RECIBE, 0);
					asignaAtributo('lblPersonalRegistra', datosRow.PERSONA_REGISTRA, 0);

					asignaAtributo('lblIdPaciente', datosRow.ID_PACIENTE, 0);
					asignaAtributo('lblIdDocumento', datosRow.ID_FOLIO, 0);
					asignaAtributo('lblTipoDocumento', datosRow.TIPO_FOLIO, 0);






					if (datosRow.ID_ESTADO == 5) {
						asignaAtributo('lblAnulado', datosRow.ESTADO, 0);
					}
					else {
						asignaAtributo('lblAnulado', '');
					}

					cargarAcordeonTipoEvento(datosRow.ID_TIPO_EVENTO)



					if (datosRow.ID_ESTADO == 1) {
						habilitar('btn_priorizado', 1)
					}
					else habilitar('btn_priorizado', 0)

					if (datosRow.APLAZADO != 'SI') {
						habilitar('btn_APLAZADO', 1)
					}
					else habilitar('btn_APLAZADO', 0)


					if (datosRow.ID_ESTADO == 4) {
						habilitar('btn_ENTREGADO', 0)
						habilitar('btn_APLAZADO', 0)
						alert('EVENTO SE ENCUENTRA FINALIZADO')
					}
					else {
						if (datosRow.ID_ESTADO == 1)
							habilitar('btn_ENTREGADO', 0)
						else habilitar('btn_ENTREGADO', 1)
					}

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;
			case 'RespuestaEvento':
				// limpiarDivEditarJuan(arg); 
	
				ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
				valores_a_mandar = pag;
				valores_a_mandar = valores_a_mandar + "?idQuery=2292&parametros=";
				add_valores_a_mandar(valorAtributo('txtCodEventoBus'));
				add_valores_a_mandar(valorAtributo('txtEventoBus'));
				add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
				add_valores_a_mandar(valorAtributo('cmbIdTipoEventoBus'));
				add_valores_a_mandar(valorAtributo('cmbResponsabilidadBus'));
				// add_valores_a_mandar( valorAtributoIdAutoCompletar('txtIdBusPaciente')  );	
	
				$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
					url: valores_a_mandar,
					datatype: 'xml',
					mtype: 'GET',
	
					colNames: ['Tot', 'TICKET', 'DESCRIPCION', 'ID_TIPO_EVENTO', 'TIPO EVENTO', 'ID_PROCESO', 'PROCESO', 'ID_MODULO','OBSERVACIONES', 'ID_PERSONAL_RESPONSABLE','RESPONSABLE' ,'PRIORIZACION', 'SEMAFORO PRIORIDAD', 'ETAPA', 'APLAZADO',
							'NO_DOC_RELACIONADO', 'DESCRIP_DOC_RELACIONADO', 'FECHA_REGISTRO', 'FECHA_INICIA', 'FECHA DE CREACION', 'FECHA DE ASIGNACION', 'FECHA_ENTREGA_APLAZADO', 'FECHA_SOLUCION', 'FECHA TENTATIVA SOLUCION', 'ID_APLAZADO_MOTIVO', 'MOTIVO_APLAZA', 'APLAZADO_OBSERVACION',
							'ID_ESTADO', 'ESTADO', 'ID_CALIFICACION', 'CALIFICACION', 'ID_PERSONAL_RECIBE', 'OBSERVACION_RECIBE', 'ID_PERSONA_REGISTRA', 'PERSONA_REGISTRA', 'ID_PACIENTE', 'ID_FOLIO', 'TIPO_FOLIO', 'TAREAS CUMPLIMIENTO', 'ID_GESTION','ID_SEDE','SEDE',
							'ID_MODULO', 'MODULO'
						],
						colModel: [
							{ name: 'contador', index: 'contador', hidden: true },
							{ name: 'TICKET', index: 'TICKET', width: 30},
							{ name: 'DESCRIPCION', index: 'DESCRIPCION', width: 400},
							{ name: 'ID_TIPO_EVENTO', index: 'ID_TIPO_EVENTO', hidden: true },
							{ name: 'TIPO_EVENTO', index: 'TIPO_EVENTO',},
		
							{ name: 'ID_PROCESO', index: 'ID_PROCESO', hidden: true },
							{ name: 'PROCESO', index: 'PROCESO', hidden: true },
							{ name: 'ID_MODULO', index: 'ID_MODULO', hidden: true },
							{ name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true },
							{ name: 'ID_PERSONAL_RESPONSABLE', index: 'ID_PERSONAL_RESPONSABLE', hidden: true },
							{ name: 'RESPONSABLE', index: 'RESPONSABLE',},
							{ name: 'PRIORIDAD', index: 'PRIORIDAD', hidden:true},
							{ name: 'SEMAFORO_PRIORIDAD', index: 'SEMAFORO_PRIORIDAD'},
							{ name: 'ETAPA', index: 'ETAPA', hidden: true },
							{ name: 'APLAZADO', index: 'APLAZADO',},
							{ name: 'NO_DOC_RELACIONADO', index: 'NO_DOC_RELACIONADO', hidden: true },
							{ name: 'DESCRIP_DOC_RELACIONADO', index: 'DESCRIP_DOC_RELACIONADO', hidden: true },
							{ name: 'FECHA_REGISTRO', index: 'FECHA_REGISTRO', hidden: true },
							{ name: 'FECHA_INICIA', index: 'FECHA_INICIA', hidden: true },
							{ name: 'FECHA_CREA', index: 'FECHA_CREA'},
							{ name: 'FECHA_ENTREGA', index: 'FECHA_ENTREGA',},
							{ name: 'FECHA_ENTREGA_APLAZADO', index: 'FECHA_ENTREGA_APLAZADO', hidden: true },
							{ name: 'FECHA_SOLUCION', index: 'FECHA_SOLUCION',},
							{ name: 'FECHA_TENTATIVA_SOLUCION', index: 'FECHA_TENTATIVA_SOLUCION'},
							 { name: 'ID_APLAZADO_MOTIVO', index: 'ID_APLAZADO_MOTIVO', hidden: true },
							{ name: 'MOTIVO_APLAZA', index: 'MOTIVO_APLAZA', hidden: true },
							{ name: 'APLAZADO_OBSERVACION', index: 'APLAZADO_OBSERVACION', hidden: true },
							{ name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
							{ name: 'ESTADO', index: 'ESTADO',},
							{ name: 'ID_CALIFICACION', index: 'ID_CALIFICACION', hidden: true },
							{ name: 'CALIFICACION', index: 'CALIFICACION', hidden: true },
							{ name: 'ID_PERSONAL_RECIBE', index: 'ID_PERSONAL_RECIBE', hidden: true },
							{ name: 'OBSERVACION_RECIBE', index: 'OBSERVACION_RECIBE', hidden: true },
							{ name: 'ID_PERSONA_REGISTRA', index: 'ID_PERSONA_REGISTRA', hidden: true },
							{ name: 'PERSONA_REGISTRA', index: 'PERSONA_REGISTRA', hidden: true },
							{ name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
							{ name: 'ID_FOLIO', index: 'ID_FOLIO', hidden: true },
							{ name: 'TIPO_FOLIO', index: 'TIPO_FOLIO', hidden: true },
							{ name: 'CUMPLIMIENTO', index: 'CUMPLIMIENTO',hidden: true },
							{ name: 'ID_GESTION', index: 'ID_GESTION', hidden: true },
							{ name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
							{ name: 'SEDE', index: 'SEDE', hidden: true },
							{ name: 'ID_MODULO', index: 'ID_MODULO', hidden: true},
							{ name: 'MODULO', index: 'MODULO'},
						],
		
		
						//  pager: jQuery('#pagerGrilla'), 
						height: 250,
						width: ancho + 40,
						shrinkToFit: false,
						onSelectRow: function (rowid) {
							//			 limpiarDivEditarJuan(arg); 
							var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
							estad = 0;
							asignaAtributo('txtIdEvento', datosRow.TICKET, 1);
							asignaAtributo('txtIdIdent', datosRow.ID_PERSONA_REGISTRA, 1);
							asignaAtributo('lblDescripcion', datosRow.DESCRIPCION, 1);
							asignaAtributo('cmbIdTipoEvento', datosRow.ID_TIPO_EVENTO, 1);
							asignaAtributo('cmbIdIdProceso', datosRow.ID_PROCESO,0);
							asignaAtributo('cmbIdIModulo', datosRow.ID_MODULO, 0);
							 asignaAtributo('txtObservacion', datosRow.OBSERVACIONES, 1);
							asignaAtributo('cmbIdIdResponsable', datosRow.ID_PERSONAL_RESPONSABLE, 1);
							asignaAtributo('txtPriorizacion', datosRow.PRIORIDAD, 1);
							asignaAtributo('txtNoDocumento', datosRow.NO_DOC_RELACIONADO, 1);
							asignaAtributo('txtDescDocumento', datosRow.DESCRIP_DOC_RELACIONADO, 1);
							if (datosRow.FECHA_TENTATIVA_SOLUCION != ''){
								
								let fechaTentativa = datosRow.FECHA_TENTATIVA_SOLUCION.split('/')
								
								if(datosRow.ID_ESTADO=="2"){
									asignaAtributo('txtFechaTentativaSolucion', `${fechaTentativa[2].split(' ')[0]}-${fechaTentativa[1]}-${fechaTentativa[0]}T${fechaTentativa[2].split(' ')[1]}`, 0);
								}else{
									asignaAtributo('txtFechaTentativaSolucion', `${fechaTentativa[2].split(' ')[0]}-${fechaTentativa[1]}-${fechaTentativa[0]}T${fechaTentativa[2].split(' ')[1]}`, 1);
								}
								
							}else{
								if(datosRow.ID_ESTADO=="2"){
									asignaAtributo('txtFechaTentativaSolucion', '', 0);
								}
								else{
									asignaAtributo('txtFechaTentativaSolucion', '', 1);
								}
							}
							
							asignaAtributo('txtFechaEntrega1', datosRow.FECHA_ENTREGA, 1);
		
							asignaAtributo('cmbIdMotivoAplaza', datosRow.ID_APLAZADO_MOTIVO, 0);
							asignaAtributo('txtFechaAplazada1', datosRow.FECHA_ENTREGA_APLAZADO, 0);
							asignaAtributo('txtObservacionAplaza', datosRow.APLAZADO_OBSERVACION, 0);
		
							asignaAtributo('cmbIdCalificacion', datosRow.ID_CALIFICACION, 1);
							asignaAtributo('cmbIdPersonalRecibe', datosRow.ID_PERSONAL_RECIBE, 1);
							asignaAtributo('txtObservacionRecibe', datosRow.OBSERVACION_RECIBE, 0);
							asignaAtributo('txtFechaRecibe', datosRow.FECHA_RECIBE, 1);
							asignaAtributo('lblPersonalRegistra', datosRow.PERSONA_REGISTRA,1);
		
							asignaAtributo('lblIdPaciente', datosRow.ID_PACIENTE, 1);
							asignaAtributo('lblIdDocumento', datosRow.ID_FOLIO, 1);
							asignaAtributo('lblTipoDocumento', datosRow.TIPO_FOLIO, 1);
							asignaAtributo('lblSede', datosRow.SEDE, 1);
							asignaAtributo('lblIdEventoARelacio', datosRow.TICKET,1);
							asignaAtributo('lblDesEventoARelacio', datosRow.DESCRIPCION, 1);

							asignaAtributo('cmbIdIModulo', datosRow.ID_MODULO, 1);

							
							//cargarAcordeonTipoEvento(datosRow.ID_GESTION)
	
			
						
						if (datosRow.ID_ESTADO == 5) {
							asignaAtributo('lblAnulado', datosRow.ESTADO, 0);
							habilitar('btnSubirAdjuntosE', 0);
							habilitar('btn_APLAZADO', 0);
							habilitar('btn_ENTREGADO', 0);
						}
						else {
							asignaAtributo('lblAnulado', '');
						}
	
						//cargarAcordeonTipoEvento(datosRow.ID_GESTION)
	
						if (datosRow.ID_ESTADO == 1) {
							habilitar('btn_priorizado', 1)
							habilitar('btn_priorizado_', 1)
						}
						else habilitar('btn_priorizado', 0)
	
						if (datosRow.APLAZADO != 3) {
							habilitar('btn_APLAZADO', 1)
						}
						else habilitar('btn_APLAZADO', 0)
	
	
						if (datosRow.ID_ESTADO == 4) {
							habilitar('btn_ENTREGADO', 0)
							habilitar('btn_APLAZADO', 0)
							habilitar('btn_priorizado_', 1)
							alert('EVENTO SE ENCUENTRA FINALIZADO')
						}
						else {
							if (datosRow.ID_ESTADO == 1)
								habilitar('btn_ENTREGADO', 0)
							else habilitar('btn_ENTREGADO', 1)
						}

						if (datosRow.ID_ESTADO == 3 || datosRow.ID_ESTADO == 4 || datosRow.ID_ESTADO == 6) {
							habilitar('btnSubirAdjuntosE', 0);
							habilitar('btn_ENTREGADO', 0)
							habilitar('btn_APLAZADO', 0)
						}

						if (datosRow.ID_ESTADO == 2) {
							habilitar('btnSubirAdjuntosE', 1);
							habilitar('btn_ENTREGADO', 1)
							habilitar('btn_APLAZADO', 1)
						}
	
					},
					gridComplete: function(){
						mostrar('divVentanitaAdjuntos');
						acordionArchivosAdjuntosEvento();
					}
				});
				$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');			
			break;

			case 'GestionEvento':
				// limpiarDivEditarJuan(arg); 
	
				ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
				valores_a_mandar = pag;
				valores_a_mandar = valores_a_mandar + "?idQuery=53&parametros=";
				add_valores_a_mandar(valorAtributo('txtCodEventoBus'));
				add_valores_a_mandar(valorAtributo('txtEventoBus'));
				add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
				add_valores_a_mandar(valorAtributo('cmbIdTipoEventoBus'));
				add_valores_a_mandar(valorAtributo('cmbResponsabilidadBus'));
				// add_valores_a_mandar( valorAtributoIdAutoCompletar('txtIdBusPaciente')  );	
	
				$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
					url: valores_a_mandar,
					datatype: 'xml',
					mtype: 'GET',
	
					colNames: ['Tot', 'TICKET', 'DESCRIPCION', 'ID_TIPO_EVENTO', 'TIPO EVENTO', 'ID_PROCESO', 'PROCESO', 'ID_MODULO','OBSERVACIONES', 'ID_PERSONAL_RESPONSABLE', 'PRIORIZACION', 'SEMAFORO PRIORIDAD', 'ETAPA', 'APLAZADO',
						'NO_DOC_RELACIONADO', 'DESCRIP_DOC_RELACIONADO', 'FECHA_REGISTRO', 'FECHA_INICIA', 'FECHA DE CREACION', 'FECHA DE ASIGNACION', 'FECHA_ENTREGA_APLAZADO', 'FECHA_SOLUCION', 'FECHA TENTATIVA SOLUCION', 'ID_APLAZADO_MOTIVO', 'MOTIVO_APLAZA', 'APLAZADO_OBSERVACION',
						'ID_ESTADO', 'ESTADO', 'ID_CALIFICACION', 'CALIFICACION', 'ID_PERSONAL_RECIBE', 'OBSERVACION_RECIBE', 'ID_PERSONA_REGISTRA', 'PERSONA_REGISTRA', 'ID_PACIENTE', 'ID_FOLIO', 'TIPO_FOLIO', 'TAREAS CUMPLIMIENTO', 'ID_GESTION','ID_SEDE','SEDE',
						'ID_MODULO', 'MODULO'
					],
					colModel: [
						{ name: 'contador', index: 'contador', hidden: true },
						{ name: 'TICKET', index: 'TICKET', width: 30},
						{ name: 'DESCRIPCION', index: 'DESCRIPCION', width: 400},
						{ name: 'ID_TIPO_EVENTO', index: 'ID_TIPO_EVENTO', hidden: true },
						{ name: 'TIPO_EVENTO', index: 'TIPO_EVENTO',},
	
						{ name: 'ID_PROCESO', index: 'ID_PROCESO', hidden: true },
						{ name: 'PROCESO', index: 'PROCESO', hidden: true },
						{ name: 'ID_MODULO', index: 'ID_MODULO', hidden: true },
						{ name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true },
						{ name: 'ID_PERSONAL_RESPONSABLE', index: 'ID_PERSONAL_RESPONSABLE', hidden: true },
						{ name: 'PRIORIDAD', index: 'PRIORIDAD', hidden: true},
						{ name: 'SEMAFORO_PRIORIDAD', index: 'SEMAFORO_PRIORIDAD'},
						{ name: 'ETAPA', index: 'ETAPA', hidden: true },
						{ name: 'APLAZADO', index: 'APLAZADO'},
						{ name: 'NO_DOC_RELACIONADO', index: 'NO_DOC_RELACIONADO', hidden: true },
						{ name: 'DESCRIP_DOC_RELACIONADO', index: 'DESCRIP_DOC_RELACIONADO', hidden: true },
						{ name: 'FECHA_REGISTRO', index: 'FECHA_REGISTRO', hidden: true },
						{ name: 'FECHA_INICIA', index: 'FECHA_INICIA', hidden: true },
						{ name: 'FECHA_CREA', index: 'FECHA_CREA'},
						{ name: 'FECHA_ENTREGA', index: 'FECHA_ENTREGA' },
						{ name: 'FECHA_ENTREGA_APLAZADO', index: 'FECHA_ENTREGA_APLAZADO', hidden: true },
						{ name: 'FECHA_SOLUCION', index: 'FECHA_SOLUCION'},
						{ name: 'FECHA_TENTATIVA_SOLUCION', index: 'FECHA_TENTATIVA_SOLUCION',},
	     				{ name: 'ID_APLAZADO_MOTIVO', index: 'ID_APLAZADO_MOTIVO', hidden: true },
						{ name: 'MOTIVO_APLAZA', index: 'MOTIVO_APLAZA', hidden: true },
						{ name: 'APLAZADO_OBSERVACION', index: 'APLAZADO_OBSERVACION', hidden: true },
						{ name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
						{ name: 'ESTADO', index: 'ESTADO'},
	    				{ name: 'ID_CALIFICACION', index: 'ID_CALIFICACION', hidden: true },
						{ name: 'CALIFICACION', index: 'CALIFICACION', hidden: true },
						{ name: 'ID_PERSONAL_RECIBE', index: 'ID_PERSONAL_RECIBE', hidden: true },
						{ name: 'OBSERVACION_RECIBE', index: 'OBSERVACION_RECIBE', hidden: true },
						{ name: 'ID_PERSONA_REGISTRA', index: 'ID_PERSONA_REGISTRA', hidden: true },
						{ name: 'PERSONA_REGISTRA', index: 'PERSONA_REGISTRA', hidden: true },
						{ name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
						{ name: 'ID_FOLIO', index: 'ID_FOLIO', hidden: true },
						{ name: 'TIPO_FOLIO', index: 'TIPO_FOLIO', hidden: true },
						{ name: 'CUMPLIMIENTO', index: 'CUMPLIMIENTO',hidden: true },
						{ name: 'ID_GESTION', index: 'ID_GESTION', hidden: true },
						{ name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
						{ name: 'SEDE', index: 'SEDE', hidden: true },
						{ name: 'ID_MODULO', index: 'ID_MODULO', hidden: true},
						{ name: 'MODULO', index: 'MODULO'},
					],
	
	
					//  pager: jQuery('#pagerGrilla'), 
					height: 250,
					width: ancho + 40,
					shrinkToFit: false,
					onSelectRow: function (rowid) {
						//			 limpiarDivEditarJuan(arg); 
						var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
						estad = 0;
						asignaAtributo('txtIdEvento', datosRow.TICKET, 1);
						asignaAtributo('txtIdEstado', datosRow.TICKET, 1);
						asignaAtributo('lblDescripcion', datosRow.DESCRIPCION, 1);
						asignaAtributo('cmbIdTipoEvento', datosRow.ID_TIPO_EVENTO, 0);
						asignaAtributo('cmbIdIdProceso', datosRow.ID_PROCESO, 0);
						if (datosRow.ID_PROCESO == '3') {
							asignaAtributo('cmbIdIModulo', datosRow.ID_MODULO, 0);
						}
						else{
							asignaAtributo('cmbIdIModulo', datosRow.ID_MODULO, 1);
						}
	     				asignaAtributo('txtObservacion', datosRow.OBSERVACIONES, 0);
						asignaAtributo('cmbIdIdResponsable', datosRow.ID_PERSONAL_RESPONSABLE, 0);
						asignaAtributo('txtPriorizacion', datosRow.PRIORIDAD, 0);
						asignaAtributo('txtNoDocumento', datosRow.NO_DOC_RELACIONADO, 0);
						asignaAtributo('txtDescDocumento', datosRow.DESCRIP_DOC_RELACIONADO, 0);
	
						asignaAtributo('txtFechaInicia1', datosRow.FECHA_INICIA, 1);
						if (datosRow.FECHA_ENTREGA != ''){
							let fechaEntrega = datosRow.FECHA_ENTREGA.split('/')
							asignaAtributo('txtFechaEntrega1', `${fechaEntrega[2].split(' ')[0]}-${fechaEntrega[1]}-${fechaEntrega[0]}`, 1);
						}else{
							asignaAtributo('txtFechaEntrega1', '', 1);
						}
	
						asignaAtributo('cmbIdMotivoAplaza', datosRow.ID_APLAZADO_MOTIVO, 0);
						asignaAtributo('txtFechaAplazada1', datosRow.FECHA_ENTREGA_APLAZADO, 0);
						asignaAtributo('txtObservacionAplaza', datosRow.APLAZADO_OBSERVACION, 0);
	
						asignaAtributo('cmbIdCalificacion', datosRow.ID_CALIFICACION, 0);
						asignaAtributo('cmbIdPersonalRecibe', datosRow.ID_PERSONAL_RECIBE, 0);
						asignaAtributo('txtObservacionRecibe', datosRow.OBSERVACION_RECIBE, 0);
						asignaAtributo('txtFechaRecibe', datosRow.FECHA_RECIBE, 0);
						asignaAtributo('lblPersonalRegistra', datosRow.PERSONA_REGISTRA, 0);
	
						asignaAtributo('lblIdPaciente', datosRow.ID_PACIENTE, 0);
						asignaAtributo('lblIdDocumento', datosRow.ID_FOLIO, 0);
						asignaAtributo('lblTipoDocumento', datosRow.TIPO_FOLIO, 0);
						asignaAtributo('lblSede', datosRow.SEDE, 0);
						asignaAtributo('lblIdEventoARelacio', datosRow.TICKET, 0);
					    asignaAtributo('lblDesEventoARelacio', datosRow.DESCRIPCION, 0);
						
						//cargarAcordeonTipoEvento(datosRow.ID_GESTION)
	
	
						if (datosRow.ID_ESTADO == 1) {
							habilitar('btn_priorizado', 1)
							habilitar('btn_limpiar', 1)
							habilitar('btn_anulado', 1)
							habilitar('btnSubirAdjuntosE', 1)
						}
						else if (datosRow.ID_ESTADO == 2){
							habilitar('btn_priorizado', 1)
							habilitar('btn_limpiar', 1)
							habilitar('btn_anulado', 1)
							habilitar('btnSubirAdjuntosE', 1)
						}
	
	
						if (datosRow.ID_ESTADO == 4) {
							habilitar('btn_ENTREGADO', 0)
							habilitar('btn_priorizado', 0)
							habilitar('btn_anulado', 0)
							habilitar('btnSubirAdjuntosE', 0)
							alert('EVENTO SE ENCUENTRA FINALIZADO')
						}

						if (datosRow.ID_ESTADO == 5) {
							asignaAtributo('lblAnulado', datosRow.ESTADO, 0);
							habilitar('btn_limpiar', 0)
							habilitar('btn_priorizado', 0)
							habilitar('btn_anulado', 0)
							habilitar('btnSubirAdjuntosE', 0)
						}
						else {
							asignaAtributo('lblAnulado', '', 0);
						}

						if (datosRow.ID_ESTADO == 6 || datosRow.ID_ESTADO == 3) {
							habilitar('btn_limpiar', 0)
							habilitar('btn_priorizado', 0)
							habilitar('btn_anulado', 0)
							habilitar('btnSubirAdjuntosE', 0)
						}
						
	
					},
					gridComplete: function (){
						mostrar('divVentanitaAdjuntos');
						acordionArchivosAdjuntosEvento();
					}
				});
				$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');			
				break;


		case 'listClasificarEA':
			// limpiarDivEditarJuan(arg); 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=553&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdEvento'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['contador', 'ID', 'ID_CLASE', 'CLASE', 'ID_TIPO', 'SERVICIO DONDE OCURRIO', 'CRONOLOGIA', 'RECOMENDACIONES'
				],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
					{ name: 'ID_CLASE', index: 'ID_CLASE', hidden: true },
					{ name: 'CLASE', index: 'CLASE', width: anchoP(ancho, 25) },
					{ name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
					{ name: 'ID_SERVICIO_DONDE_OCURRIO', index: 'ID_SERVICIO_DONDE_OCURRIO', hidden: true },
					{ name: 'CRONOLOGIA', index: 'CRONOLOGIA', width: anchoP(ancho, 35) },
					{ name: 'RECOMENDACIONES', index: 'RECOMENDACIONES', width: anchoP(ancho, 35) },
				],
				//  pager: jQuery('#pagerGrilla'), 
				height: 100,
				width: ancho - 50,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('lblClasifiEA', datosRow.ID, 0);
					asignaAtributo('cmbBusClaseEA', datosRow.ID_CLASE, 0);
					asignaAtributo('cmbTipoEA', datosRow.ID_TIPO, 0);
					asignaAtributo('cmbIdTipoServicio', datosRow.ID_SERVICIO_DONDE_OCURRIO, 0);
					asignaAtributo('txtCronologia', datosRow.CRONOLOGIA, 0);
					asignaAtributo('txtRecomendacion', datosRow.RECOMENDACIONES, 0);

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;
		case 'listAnalisisEA':
			// limpiarDivEditarJuan(arg); 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=549&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdEvento'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['contador', 'ID', 'ACCION INSEGURA', 'ID_FACTOR_CONTRIBUTIVO', 'FACTOR CONTRIBUTIVO'
				],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'ID', index: 'ID', width: anchoP(ancho, 10) },
					{ name: 'ACCION_INSEGURA', index: 'ACCION_INSEGURA', width: anchoP(ancho, 45) },
					{ name: 'ID_FACTOR_CONTRIBUTIVO', index: 'ID_FACTOR_CONTRIBUTIVO', hidden: true },
					{ name: 'FACTOR_CONTRIBUTIVO', index: 'FACTOR_CONTRIBUTIVO', width: anchoP(ancho, 45) },
				],
				//  pager: jQuery('#pagerGrilla'), 
				height: 100,
				width: ancho - 50,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('lblIdAnalisisEA', datosRow.ID, 0);
					asignaAtributo('txtAccionInsegura', datosRow.ACCION_INSEGURA, 0);
					asignaAtributo('cmbFactContributivo', datosRow.ID_FACTOR_CONTRIBUTIVO, 0);

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;

		case 'listPlanAccion':
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=529&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdEvento'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['contador', 'ID', 'TAREA', 'FECHA', 'RESPONSABLE', 'COSTO', 'HORAS', 'id_estado', 'ESTADO', 'CUMPLE', 'SEGUIMIENTO', 'OBSERVACION'
				],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
					{ name: 'tarea', index: 'tarea', width: anchoP(ancho, 40) },
					{ name: 'fecha', index: 'fecha', width: anchoP(ancho, 10) },
					{ name: 'responsable', index: 'responsable', width: anchoP(ancho, 20) },
					{ name: 'costo', index: 'costo', width: anchoP(ancho, 5) },
					{ name: 'horas', index: 'horas', width: anchoP(ancho, 5) },
					{ name: 'id_estado', index: 'id_estado', hidden: true },
					{ name: 'estado', index: 'estado', width: anchoP(ancho, 5) },
					{ name: 'cumple', index: 'cumple', width: anchoP(ancho, 5) },
					{ name: 'seguimiento', index: 'seguimiento', width: anchoP(ancho, 15) },
					{ name: 'observacion', index: 'observacion', width: anchoP(ancho, 15) },
				],
				height: 100,
				autowidth: true,
				onSelectRow: function (rowid) {
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('lblIdTareaPlanAccion', datosRow.ID, 1);
					asignaAtributo('lblDescripcionTareaPlanAccion', datosRow.tarea, 1);
					asignaAtributo('cmbIdEstadoTareaEdit', datosRow.id_estado, 0);
					asignaAtributo('lblIdEstadoTareaEdita', datosRow.id_estado, 0);
					asignaAtributo('cmbCumple', datosRow.cumple, 0);
					asignaAtributo('txtSeguimiento', datosRow.seguimiento, 0);
					asignaAtributo('txtObservcion', datosRow.observacion, 0);

					mostrar('divVentanitaPlanAccion')


				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;

		case 'listControl': 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=755&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdPacientee'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['contador', 'ID', 'NUMERO FACTURA', 'IDENTIFICACION', 'ENTIDAD', 'MEDICO', 'TIPO LENTE',
					'NUMERO ORDEN', 'NOMBRE LABORATORIO', 'PERSONA RECIBE', 'FECHA ENTREGA', 'FECHA RECEPCION',
					'FECHA ENTREGA CLIENTE', 'CLIENTE RECIBE', 'ESTADO'],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'ID', index: 'ID', hidden: true },
					{ name: 'NUMEROFACTURA', index: 'NUMEROFACTURA', hidden: true },
					{ name: 'IDENTIFICACION', index: 'IDENTIFICACION', hidden: true },
					{ name: 'ENTIDAD', index: 'ENTIDAD', hidden: true },
					{ name: 'MEDICO', index: 'MEDICO', width: anchoP(ancho, 20) },
					{ name: 'TIPOLENTE', index: 'TIPOLENTE', width: anchoP(ancho, 20) },
					{ name: 'NUMEROORDEN', index: 'NUMEROORDEN', width: anchoP(ancho, 20) },
					{ name: 'NOMBRELABORATORIO', index: 'NOMBRELABORATORIO', width: anchoP(ancho, 20) },
					{ name: 'PERSONARECIBE', index: 'PERSONARECIBE', width: anchoP(ancho, 12) },
					{ name: 'FECHAENTREGA', index: 'FECHAENTREGA', width: anchoP(ancho, 12) },
					{ name: 'FECHARECEPCION', index: 'FECHARECEPCION', width: anchoP(ancho, 12) },
					{ name: 'FECHAENTREGACLIENTE', index: 'FECHAENTREGACLIENTE', width: anchoP(ancho, 12) },
					{ name: 'CLIENTERECIBE', index: 'CLIENTERECIBE', width: anchoP(ancho, 12) },
					{ name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 12) },

				],
				//  pager: jQuery('#pagerGrilla'), 
				height: 100,
				width: ancho - 50,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('txtNumeroFactura', datosRow.NUMEROFACTURA, 0);
					asignaAtributo('txtIdPacientee', datosRow.IDENTIFICACION, 0);
					asignaAtributo('txtEntidad', datosRow.ENTIDAD, 0);
					asignaAtributo('cmbTipoLente', datosRow.TIPOLENTE, 0);
					asignaAtributo('txtNumeroOrden', datosRow.NUMEROORDEN, 0);
					asignaAtributo('cmbNombreLabo', datosRow.NOMBRELABORATORIO, 0);
					asignaAtributo('txtIdMedico', datosRow.MEDICO, 0);
					asignaAtributo('txtPersonaRecibe', datosRow.PERSONARECIBE, 0);
					asignaAtributo('txtClienteRecibe', datosRow.CLIENTERECIBE, 0);


				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;

			break;
		case 'listEspinaPescado':
			// limpiarDivEditarJuan(arg); 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=527&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdEvento'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['contador', 'ID', 'CALIFICA', 'FACTOR', 'CRITERIO DE CAUSA', 'ANALISIS JUSTIFICACION'
				],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
					{ name: 'CALIFICA', index: 'CALIFICA', width: anchoP(ancho, 10) },
					{ name: 'FACTOR', index: 'FACTOR', width: anchoP(ancho, 10) },
					{ name: 'CRITERIO', index: 'CRITERIO', width: anchoP(ancho, 15) },
					{ name: 'ANALISIS', index: 'ANALISIS', width: anchoP(ancho, 55) },

				],

				//  pager: jQuery('#pagerGrilla'), 
				height: 100,
				width: ancho - 100,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('lblIdEspinaPezEA', datosRow.ID, 0);
					asignaAtributo('txtAnalisisFactor', datosRow.ANALISIS, 0);

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;
		case 'listEventoRelacionado':
			// limpiarDivEditarJuan(arg); 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=654&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdEvento'));

			$('#drag' + ventanaActual.num).find("#listEventoRelacionado").jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['contador', 'ID', 'DESCRIPCION', 'FECHA_REGISTRO', 'ID_ESTADO', 'ESTADO'
				],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
					{ name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 80) },
					{ name: 'FECHA_REGISTRO', index: 'FECHA_REGISTRO', width: anchoP(ancho, 10) },
					{ name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
					{ name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 5) },
				],

				//  pager: jQuery('#pagerGrilla'), 
				height: 100,
				width: ancho - 100,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find("#listEventoRelacionado").getRowData(rowid);
					estad = 0;
					//asignaAtributo('lblIdEspinaPezEA',datosRow.ID,0); 

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;


	}
}		