// JavaScript para la programacion de los botones

function limpiarBarraNuevoAng(arg){
	switch(arg){
		  case 'tiposIdentificaciones':		
 		    $('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
		  break;
		  case 'tiposEspecialidades':	
		    $('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');			
		  break;
		  case 'tiposAfiliados':	
		    $('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
		  break;
		  case 'viasIngreso':				
		    $('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
		  break;			  		 
		  case 'centrosCostos':				
		    $('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
		  break;
		  case 'tiposServicios':				
		    $('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
		  break;   
		   case 'consultorios':				
		    $('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
		  break;
		  case 'prestadorasSalud':				
			$('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNit').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
			$('#drag'+ventanaActual.num).find('#txtDir').val('');
			$('#drag'+ventanaActual.num).find('#txtTelefono').val('');
			$('#drag'+ventanaActual.num).find('#txtFax').val('');
			$('#drag'+ventanaActual.num).find('#txtEmail').val('');
			$('#drag'+ventanaActual.num).find('#txtResponsable').val('');
			$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val('00');
			
		  break; 
		  case 'entidadesAdministradoras':				
			$('#drag'+ventanaActual.num).find('#txtCodigo').val('');
			$('#drag'+ventanaActual.num).find('#txtNit').val('');
			$('#drag'+ventanaActual.num).find('#txtNombre').val('');
			$('#drag'+ventanaActual.num).find('#txtDir').val('');
			$('#drag'+ventanaActual.num).find('#txtTelefono').val('');
			$('#drag'+ventanaActual.num).find('#txtFax').val('');
			$('#drag'+ventanaActual.num).find('#txtEmail').val('');
			$('#drag'+ventanaActual.num).find('#txtResponsable').val('');
			$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val('00');
		  break;
	}	
}
function guardarAng(arg, pag){ 
	pagina=pag;
	paginaActual=arg;	
	switch (arg){
		
		
		
      	case 'registrarComunitario':
		        //alert("comunitario");
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");				
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];

				valores_a_mandar=valores_a_mandar+"&tipo_visita="+$('#drag'+ventanaActual.num).find('#id_visita').val();
		        valores_a_mandar=valores_a_mandar+"&no_pue_bebe="+$('#drag'+ventanaActual.num).find('#no_puede_beber').attr("checked");
		        valores_a_mandar=valores_a_mandar+"&convulsi="+$('#drag'+ventanaActual.num).find('#convulsiona').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&somnoli="+$('#drag'+ventanaActual.num).find('#somnoliento').attr("checked");
				valores_a_mandar=valores_a_mandar+"&no_presenta_signos="+$('#drag'+ventanaActual.num).find('#no_signos_muerte').attr("checked");
				
				valores_a_mandar=valores_a_mandar+"&tos_difi_respirar="+$('#drag'+ventanaActual.num).find('#id_respiracion').val();
 			    valores_a_mandar=valores_a_mandar+"&signos_desnutricion="+$('#drag'+ventanaActual.num).find('#id_desnutricion').val();
			    valores_a_mandar=valores_a_mandar+"&esquema_vacuna="+$('#drag'+ventanaActual.num).find('#id_vacunacion').val();
			    valores_a_mandar=valores_a_mandar+"&diarrea="+$('#drag'+ventanaActual.num).find('#id_diarrea').val();
			    valores_a_mandar=valores_a_mandar+"&peso="+$('#drag'+ventanaActual.num).find('#peso').val();				
				valores_a_mandar=valores_a_mandar+"&problemas_desarrollo="+$('#drag'+ventanaActual.num).find('#id_desarrollo').val();
				valores_a_mandar=valores_a_mandar+"&fiebe="+$('#drag'+ventanaActual.num).find('#id_fiebre').val();
		        valores_a_mandar=valores_a_mandar+"&se_enferma_frecuencia="+$('#drag'+ventanaActual.num).find('#id_frecuencia').val();				
		        valores_a_mandar=valores_a_mandar+"&buen_trato="+$('#drag'+ventanaActual.num).find('#id_trato').val();
				
 			    valores_a_mandar=valores_a_mandar+"&riesgo_ahogamiento="+$('#drag'+ventanaActual.num).find('#riesgo_ahogamiento').attr("checked");
				valores_a_mandar=valores_a_mandar+"&riesgo_intoxica="+$('#drag'+ventanaActual.num).find('#riesgo_intoxicacion').attr("checked");
				valores_a_mandar=valores_a_mandar+"&riesgo_quema="+$('#drag'+ventanaActual.num).find('#riesgo_quemadura').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&riesgo_trauma_herida="+$('#drag'+ventanaActual.num).find('#riesgo_trauma').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&no_presenta_riesgo="+$('#drag'+ventanaActual.num).find('#no_riesgo').attr("checked");
				
			    valores_a_mandar=valores_a_mandar+"&problem_aseo_perso="+$('#drag'+ventanaActual.num).find('#aseo_personal').val();	
			    valores_a_mandar=valores_a_mandar+"&manejo_inadecua_alimen="+$('#drag'+ventanaActual.num).find('#manejo_alimentos').attr("checked");	
				valores_a_mandar=valores_a_mandar+"&proble_aseo_vivienda="+$('#drag'+ventanaActual.num).find('#aseo_vivienda').attr("checked");	
			    valores_a_mandar=valores_a_mandar+"&manejo_inade_basuras="+$('#drag'+ventanaActual.num).find('#manejo_basuras').attr("checked");				
				valores_a_mandar=valores_a_mandar+"&prsencia_insec_ratas="+$('#drag'+ventanaActual.num).find('#presencia_insectos').attr("checked");	
			    valores_a_mandar=valores_a_mandar+"&manejo_inade_excretas="+$('#drag'+ventanaActual.num).find('#manejo_excretas').attr("checked");	
				
				valores_a_mandar=valores_a_mandar+"&inade_convi_animal="+$('#drag'+ventanaActual.num).find('#convivencia_animales').attr("checked");	
			    valores_a_mandar=valores_a_mandar+"&riesgo_dengue_palu="+$('#drag'+ventanaActual.num).find('#riesgo_dengue').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&riesgo_gripa_tos="+$('#drag'+ventanaActual.num).find('#riesgo_gripa').attr("checked");	
			    valores_a_mandar=valores_a_mandar+"&riesgo_parasitos="+$('#drag'+ventanaActual.num).find('#riesgo_parasitismo').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&consumo_agua_pota="+$('#drag'+ventanaActual.num).find('#agua_potable').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&cond_ambienta_higie_adecua="+$('#drag'+ventanaActual.num).find('#condiciones_higiene').attr("checked");					
				
				valores_a_mandar=valores_a_mandar+"&problemas_salud_madre="+$('#drag'+ventanaActual.num).find('#id_madre').val();	
				if($('#drag'+ventanaActual.num).find('#remision1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&remision="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&remision="+"false";
				}
		 		alert(valores_a_mandar);
				//ajaxGuardar();
		 break;
		 
		case 'registrarClinico1':
		        alert("Clinico 1");
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");				
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];

				valores_a_mandar=valores_a_mandar+"&tipovisita="+$('#drag'+ventanaActual.num).find('#id_visita').val();
		        valores_a_mandar=valores_a_mandar+"&spg_enfer_grave="+$('#drag'+ventanaActual.num).find('#enfermedad_grave').attr("checked");
				
		        valores_a_mandar=valores_a_mandar+"&no_pres_tos="+$('#drag'+ventanaActual.num).find('#no_respirar').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&obstru_via_aerea="+$('#drag'+ventanaActual.num).find('#obstruccion').attr("checked");
				valores_a_mandar=valores_a_mandar+"&neumo_grave="+$('#drag'+ventanaActual.num).find('#neumonia_grave').attr("checked");				
				valores_a_mandar=valores_a_mandar+"&neumonia="+$('#drag'+ventanaActual.num).find('#neumonia').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&tos="+$('#drag'+ventanaActual.num).find('#tos').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&sibilancia="+$('#drag'+ventanaActual.num).find('#sibilancia').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&sibilancia_recu="+$('#drag'+ventanaActual.num).find('#sibilancia_recurrente').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&no_prese_diarrea="+$('#drag'+ventanaActual.num).find('#no_diarrea').attr("checked");								
				valores_a_mandar=valores_a_mandar+"&deshi_grave="+$('#drag'+ventanaActual.num).find('#deshidratacion_grave').attr("checked");
				valores_a_mandar=valores_a_mandar+"&alg_gra_deshi="+$('#drag'+ventanaActual.num).find('#deshidratacion_alta').attr("checked");				
		        valores_a_mandar=valores_a_mandar+"&no_tiene_deshidra="+$('#drag'+ventanaActual.num).find('#no_deshidratacion').attr("checked");			
		        valores_a_mandar=valores_a_mandar+"&diarrea_persis_grave="+$('#drag'+ventanaActual.num).find('#diarrea_grave').attr("checked");				
 			    valores_a_mandar=valores_a_mandar+"&diarrea_persistente="+$('#drag'+ventanaActual.num).find('#diarrea_persistente').attr("checked");				
				valores_a_mandar=valores_a_mandar+"&disenteria="+$('#drag'+ventanaActual.num).find('#disenteria').attr("checked");				
				valores_a_mandar=valores_a_mandar+"&no_presen_fiebre="+$('#drag'+ventanaActual.num).find('#no_fiebre').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&enfer_febril_grave="+$('#drag'+ventanaActual.num).find('#febril_grave').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&sospe_bacteremia="+$('#drag'+ventanaActual.num).find('#bacteremia').attr("checked");								
			    valores_a_mandar=valores_a_mandar+"&enfer_febril="+$('#drag'+ventanaActual.num).find('#febril').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&sospe_mala_comp="+$('#drag'+ventanaActual.num).find('#malaria_comp').attr("checked");	
				valores_a_mandar=valores_a_mandar+"&sospe_mala_no_comp="+$('#drag'+ventanaActual.num).find('#malaria_no_comp').attr("checked");	
			    valores_a_mandar=valores_a_mandar+"&sospe_deng_hemo="+$('#drag'+ventanaActual.num).find('#dengue_hemorragico').attr("checked");								
				valores_a_mandar=valores_a_mandar+"&sospe_deng_clas="+$('#drag'+ventanaActual.num).find('#dengue_clasico').attr("checked");	
			    valores_a_mandar=valores_a_mandar+"&sospe_saramp="+$('#drag'+ventanaActual.num).find('#sarampion').attr("checked");				
				valores_a_mandar=valores_a_mandar+"&no_otitis_media="+$('#drag'+ventanaActual.num).find('#no_otitis').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&mastoidi="+$('#drag'+ventanaActual.num).find('#mastoiditis').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&oti_medi_aguda="+$('#drag'+ventanaActual.num).find('#otitis_aguda').attr("checked");	
			    valores_a_mandar=valores_a_mandar+"&oti_medi_croni="+$('#drag'+ventanaActual.num).find('#otitis_cronica').attr("checked");									
				valores_a_mandar=valores_a_mandar+"&no_tien_faringue="+$('#drag'+ventanaActual.num).find('#no_faringe').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&faringoamig="+$('#drag'+ventanaActual.num).find('#faringe_viral').attr("checked");									
				valores_a_mandar=valores_a_mandar+"&vitamina_a="+$('#drag'+ventanaActual.num).find('#vitamina_a').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&sulfato_ferroso="+$('#drag'+ventanaActual.num).find('#sulfato_ferroso').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&otro="+$('#drag'+ventanaActual.num).find('#otra_formulacion').attr("checked");													
				valores_a_mandar=valores_a_mandar+"&tipomaltrato="+$('#drag'+ventanaActual.num).find('#id_maltrato').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&dtn_ane="+$('#drag'+ventanaActual.num).find('#id_desnutricion').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&desarrol="+$('#drag'+ventanaActual.num).find('#id_desarrollo').attr("checked");									
				valores_a_mandar=valores_a_mandar+"&vacuna="+$('#drag'+ventanaActual.num).find('#id_vacunacion').attr("checked");							
				
				valores_a_mandar=valores_a_mandar+"&no_recibe_lacta_mater="+$('#drag'+ventanaActual.num).find('#no_lm').attr("checked");									
				valores_a_mandar=valores_a_mandar+"&reci_orien_lacta="+$('#drag'+ventanaActual.num).find('#lm_orientacion').attr("checked");	
				valores_a_mandar=valores_a_mandar+"&ini_lac_mat_prim_me_ho="+$('#drag'+ventanaActual.num).find('#lm_primera').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&lacta_mater_exclu_6_meses="+$('#drag'+ventanaActual.num).find('#lm_exclusiva').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&lacta_mater_mixta="+$('#drag'+ventanaActual.num).find('#lm_mixta').attr("checked");									
				valores_a_mandar=valores_a_mandar+"&alimen_complemen="+$('#drag'+ventanaActual.num).find('#alimentacion_complementaria').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&alojam_conjunt_has_6_meses="+$('#drag'+ventanaActual.num).find('#alojamiento_conjunto').attr("checked");					
				valores_a_mandar=valores_a_mandar+"&posici_correc_lactan="+$('#drag'+ventanaActual.num).find('#posicion_lactante').attr("checked");					
			    valores_a_mandar=valores_a_mandar+"&posici_correc_madre="+$('#drag'+ventanaActual.num).find('#posicion_madre').attr("checked");																	
				if($('#drag'+ventanaActual.num).find('#remision1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&remision="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&remision="+"false";
				}
		 		alert(valores_a_mandar);
				//ajaxGuardar();
		 break; 
		 
		 
		 case 'registrarClinico2':
		        alert("Clinico 2");
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");				
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];

				valores_a_mandar=valores_a_mandar+"&tipovisita="+$('#drag'+ventanaActual.num).find('#id_visita').val();
		        valores_a_mandar=valores_a_mandar+"&enfermedad="+$('#drag'+ventanaActual.num).find('#id_enfermedad').val();				
		        valores_a_mandar=valores_a_mandar+"&tiene_diarrea="+$('#drag'+ventanaActual.num).find('#id_diarrea').val();
 			    valores_a_mandar=valores_a_mandar+"&evalue_alimentacion="+$('#drag'+ventanaActual.num).find('#id_alimentacion').val();
				valores_a_mandar=valores_a_mandar+"&problemas_desarrollo="+$('#drag'+ventanaActual.num).find('#id_desarrollo').val();			
				valores_a_mandar=valores_a_mandar+"&esquema_vacunacion="+$('#drag'+ventanaActual.num).find('#id_vacunacion').val();
				
 			    valores_a_mandar=valores_a_mandar+"&no_recibe_lact_materna="+$('#drag'+ventanaActual.num).find('#no_lm').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&recibio_orienta_lact_mater="+$('#drag'+ventanaActual.num).find('#lm_orientacion').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&inicia_lacta_mater_em_primera_media_hora="+$('#drag'+ventanaActual.num).find('#lm_primera').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&lacta_mater_exclusiv_hasta_seis_meses="+$('#drag'+ventanaActual.num).find('#lm_exclusiva').attr("checked");								
				valores_a_mandar=valores_a_mandar+"&lacta_mater_mixta="+$('#drag'+ventanaActual.num).find('#lm_mixta').attr("checked");
				valores_a_mandar=valores_a_mandar+"&alimenta_complemen="+$('#drag'+ventanaActual.num).find('#alimentacion_complementaria').attr("checked");				
		        valores_a_mandar=valores_a_mandar+"&alojamien_conjunt_has_seis_meses="+$('#drag'+ventanaActual.num).find('#alojamiento_conjunto').attr("checked");			
		        valores_a_mandar=valores_a_mandar+"&posicion_correct_madre="+$('#drag'+ventanaActual.num).find('#posicion_madre').attr("checked");				
 			    valores_a_mandar=valores_a_mandar+"&posicion_correct_lactante="+$('#drag'+ventanaActual.num).find('#posicion_lactante').attr("checked");				
				if($('#drag'+ventanaActual.num).find('#remision1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&remision="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&remision="+"false";
				}
		 		alert(valores_a_mandar);
				//ajaxGuardar();
		 break; 		 
		 case 'registroAgudezaVisual':
		        alert("Agudeza Visual");
				
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&idCita="+$('#drag'+ventanaActual.num).find('#idcita').val();
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+$('#drag'+ventanaActual.num).find('#tipoid').val();
		        valores_a_mandar=valores_a_mandar+"&txtId="+$('#drag'+ventanaActual.num).find('#id').val();
				
				valores_a_mandar=valores_a_mandar+"&conlentes_ojoderecho="+$('#drag'+ventanaActual.num).find('#ojoderconlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoderconlentedos').val();
		        valores_a_mandar=valores_a_mandar+"&conlentes_ojoizquierdo="+$('#drag'+ventanaActual.num).find('#ojoizqconlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoizqconlentesdos').val();				
		        valores_a_mandar=valores_a_mandar+"&conlentes_ambosojos="+$('#drag'+ventanaActual.num).find('#ojoambosconlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoambosconlentesdos').val();
				
 			    valores_a_mandar=valores_a_mandar+"&sinlentes_ojoderecho="+$('#drag'+ventanaActual.num).find('#ojodersinlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojodersinlentesdos').val();
				valores_a_mandar=valores_a_mandar+"&sinlentes_ojoizquierdo="+$('#drag'+ventanaActual.num).find('#ojoizqsinlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoizqsinlentesdos').val();			
				valores_a_mandar=valores_a_mandar+"&sinlentes_ambosojos="+$('#drag'+ventanaActual.num).find('#ojosambsinlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojosambsinlentesdos').val();				
				
 			    valores_a_mandar=valores_a_mandar+"&visioncercana_ojoderecho="+$('#drag'+ventanaActual.num).find('#ojodercercanauno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojodercercanados').val();
			    valores_a_mandar=valores_a_mandar+"&visioncercana_ojoizquierdo="+$('#drag'+ventanaActual.num).find('#ojoizqcercanauno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoizqcercanados').val();
			    valores_a_mandar=valores_a_mandar+"&visioncercana_ambosojos="+$('#drag'+ventanaActual.num).find('#ojoambcercauno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoambcercados').val();
				
			    valores_a_mandar=valores_a_mandar+"&diagnostico="+$('#drag'+ventanaActual.num).find('#diagnostico').val();								
				valores_a_mandar=valores_a_mandar+"&conducta="+$('#drag'+ventanaActual.num).find('#conducta').val();
				if($('#drag'+ventanaActual.num).find('#remision1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&remision="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&remision="+"0";
				}
		 		alert(valores_a_mandar);
				ajaxGuardar();
		 break; 
		 
		 case 'registroAdultoMayor':
		        //alert("Adulto Mayor");
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&idCita="+$('#drag'+ventanaActual.num).find('#idcita').val();
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+$('#drag'+ventanaActual.num).find('#tipoid').val();
		        valores_a_mandar=valores_a_mandar+"&txtId="+$('#drag'+ventanaActual.num).find('#id').val();

				valores_a_mandar=valores_a_mandar+"&valor_g="+$('#drag'+ventanaActual.num).find('#txtg').val();
		        valores_a_mandar=valores_a_mandar+"&valor_p="+$('#drag'+ventanaActual.num).find('#txtp').val();				
		        valores_a_mandar=valores_a_mandar+"&valor_a="+$('#drag'+ventanaActual.num).find('#txta').val();
 			    valores_a_mandar=valores_a_mandar+"&valor_c="+$('#drag'+ventanaActual.num).find('#txtc').val();
				valores_a_mandar=valores_a_mandar+"&ciclo="+$('#drag'+ventanaActual.num).find('#txtciclo').val();			
				valores_a_mandar=valores_a_mandar+"&edad_meno="+$('#drag'+ventanaActual.num).find('#txtedamenu').val();												
				if($('#drag'+ventanaActual.num).find('#rad11').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&citologia="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&citologia="+"0";
				}
			    valores_a_mandar=valores_a_mandar+"&fechacitologia="+$('#drag'+ventanaActual.num).find('#txtfecha1').val();
			    valores_a_mandar=valores_a_mandar+"&resultadocitologia="+$('#drag'+ventanaActual.num).find('#txtresul1').val();								
				if($('#drag'+ventanaActual.num).find('#rad21').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&examenmama="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&examenmama="+"0";
				}
				valores_a_mandar=valores_a_mandar+"&fechaexamenmama="+$('#drag'+ventanaActual.num).find('#txtfecha2').val();
				valores_a_mandar=valores_a_mandar+"&resultadoexamenmama="+$('#drag'+ventanaActual.num).find('#txtresul2').val();
				if($('#drag'+ventanaActual.num).find('#rad31').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&consumo_cigarrillo="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&consumo_cigarrillo="+"0";
				}
		        valores_a_mandar=valores_a_mandar+"&cantidadcigarillos="+$('#drag'+ventanaActual.num).find('#txtfecha3').val();
 			    valores_a_mandar=valores_a_mandar+"&tiempocigarillos="+$('#drag'+ventanaActual.num).find('#txtresul3').val();
				if($('#drag'+ventanaActual.num).find('#rad41').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&consumo_alcohol="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&consumo_alcohol="+"0";
				}
				valores_a_mandar=valores_a_mandar+"&cantidadalcohol="+$('#drag'+ventanaActual.num).find('#txtfecha4').val();				
 			    valores_a_mandar=valores_a_mandar+"&tiempoalcohol="+$('#drag'+ventanaActual.num).find('#txtresul4').val();
				if($('#drag'+ventanaActual.num).find('#rad51').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&consumo_drogas="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&consumo_drogas="+"0";
				}
			    valores_a_mandar=valores_a_mandar+"&cantidaddrogas="+$('#drag'+ventanaActual.num).find('#txtfecha5').val();				
			    valores_a_mandar=valores_a_mandar+"&tiempodrogas="+$('#drag'+ventanaActual.num).find('#txtresul5').val();								
				if($('#drag'+ventanaActual.num).find('#rad71').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&actividadfisica="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&actividadfisica="+"0";
				}
			    valores_a_mandar=valores_a_mandar+"&cantidadactividadfisica="+$('#drag'+ventanaActual.num).find('#txtfecha7').val();
				if($('#drag'+ventanaActual.num).find('#rad61').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&exposicionhumo="+"1";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&exposicionhumo="+"0";
				}
			    valores_a_mandar=valores_a_mandar+"&tiempohumo="+$('#drag'+ventanaActual.num).find('#txtfecha6').val();
				
				if($('#drag'+ventanaActual.num).find('#rad81').attr("checked") == true){
				 valores_a_mandar=valores_a_mandar+"&consumosalmenos="+"1";
				}else
				{
 				valores_a_mandar=valores_a_mandar+"&consumosalmenos="+"0";	
				}
				if($('#drag'+ventanaActual.num).find('#rad82').attr("checked") == true){
				valores_a_mandar=valores_a_mandar+"&consumosaligual="+"1";
				}else
				{
				valores_a_mandar=valores_a_mandar+"&consumosaligual="+"0";	
				}
				if($('#drag'+ventanaActual.num).find('#rad83').attr("checked") == true){
				valores_a_mandar=valores_a_mandar+"&consumosalmas="+"1";
				}else
				{
				valores_a_mandar=valores_a_mandar+"&consumosalmas="+"0";
				}
				if($('#drag'+ventanaActual.num).find('#rad91').attr("checked") == true){
				valores_a_mandar=valores_a_mandar+"&grasasaturada="+"1";
				}else
				{
				valores_a_mandar=valores_a_mandar+"&grasasaturada="+"0";
				}
				if($('#drag'+ventanaActual.num).find('#rad92').attr("checked") == true){
				valores_a_mandar=valores_a_mandar+"&grasanosaturada="+"1";
				}else
				{
				valores_a_mandar=valores_a_mandar+"&grasanosaturada="+"0";
				}
			    valores_a_mandar=valores_a_mandar+"&resul_glice="+$('#drag'+ventanaActual.num).find('#txtglicemia').val();
				valores_a_mandar=valores_a_mandar+"&resul_perfillipi="+$('#drag'+ventanaActual.num).find('#txtperepa').val();
			    valores_a_mandar=valores_a_mandar+"&resul_hdl="+$('#drag'+ventanaActual.num).find('#txthdl').val();
				valores_a_mandar=valores_a_mandar+"&resul_ldl="+$('#drag'+ventanaActual.num).find('#txtldl').val();
			    valores_a_mandar=valores_a_mandar+"&resul_colestotal="+$('#drag'+ventanaActual.num).find('#txtcolesto').val();
				valores_a_mandar=valores_a_mandar+"&resul_trigli="+$('#drag'+ventanaActual.num).find('#txttrigli').val();
			    valores_a_mandar=valores_a_mandar+"&resul_creati="+$('#drag'+ventanaActual.num).find('#txtcreati').val();
				valores_a_mandar=valores_a_mandar+"&resul_uroana="+$('#drag'+ventanaActual.num).find('#txturoana').val();
			    valores_a_mandar=valores_a_mandar+"&resul_diagnos="+$('#drag'+ventanaActual.num).find('#txtdiag').val();
				valores_a_mandar=valores_a_mandar+"&resul_conduc="+$('#drag'+ventanaActual.num).find('#txtcondu').val();
			    valores_a_mandar=valores_a_mandar+"&resul_educa="+$('#drag'+ventanaActual.num).find('#txteduca').val();
				valores_a_mandar=valores_a_mandar+"&remision_programa="+$('#drag'+ventanaActual.num).find('#txtremisprog').val();
			    valores_a_mandar=valores_a_mandar+"&tratamiento="+$('#drag'+ventanaActual.num).find('#txttrata').val();
		 		//alert(valores_a_mandar);
				ajaxGuardar();
		 break; 
		 
		 case 'registroAlteracionesJoven':
		        alert("Alteraciones Joven");
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");				
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];
				if($('#drag'+ventanaActual.num).find('#depeneco1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&depen_economi="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&depen_economi="+"false";
				}
				valores_a_mandar=valores_a_mandar+"&depen_economi_quien="+$('#drag'+ventanaActual.num).find('#depenecoquien').val();
				if($('#drag'+ventanaActual.num).find('#respeco1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&respo_economi="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&respo_economi="+"false";
				}				
		        valores_a_mandar=valores_a_mandar+"&respo_economi_quien="+$('#drag'+ventanaActual.num).find('#resecoquien').val();								
		        
				if($('#drag'+ventanaActual.num).find('#figauto1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&fig_aut_hogar="+"p";									
				}else if($('#drag'+ventanaActual.num).find('#figauto2').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&fig_aut_hogar="+"m";									
				}else if($('#drag'+ventanaActual.num).find('#figauto3').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&fig_aut_hogar="+"o";									
				}
 			    valores_a_mandar=valores_a_mandar+"&fig_aut_cual="+$('#drag'+ventanaActual.num).find('#txtc').val();
				
				valores_a_mandar=valores_a_mandar+"&rela_madre="+$('#drag'+ventanaActual.num).find('#cmbmadre').val();			
				valores_a_mandar=valores_a_mandar+"&porque_madre="+$('#drag'+ventanaActual.num).find('#madreporque').val();	
				
				valores_a_mandar=valores_a_mandar+"&rela_padre="+$('#drag'+ventanaActual.num).find('#cmbpadre').val();				
		        valores_a_mandar=valores_a_mandar+"&porque_padre="+$('#drag'+ventanaActual.num).find('#padreporque').val();
				
 			    valores_a_mandar=valores_a_mandar+"&rela_hermanos="+$('#drag'+ventanaActual.num).find('#cmbhermanos').val();
				valores_a_mandar=valores_a_mandar+"&porque_hermanos="+$('#drag'+ventanaActual.num).find('#hermanosporque').val();			
				
				valores_a_mandar=valores_a_mandar+"&ocupa_madre="+$('#drag'+ventanaActual.num).find('#ocupamadre').val();		
				valores_a_mandar=valores_a_mandar+"&escola_madre="+$('#drag'+ventanaActual.num).find('#escolamadre').val();		
				valores_a_mandar=valores_a_mandar+"&ocupa_padre="+$('#drag'+ventanaActual.num).find('#ocupapadre').val();		
				valores_a_mandar=valores_a_mandar+"&escola_padre="+$('#drag'+ventanaActual.num).find('#escolapadre').val();		
				
				
				if($('#drag'+ventanaActual.num).find('#provida1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&proyec_vida="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&proyec_vida="+"false";
				}
			    valores_a_mandar=valores_a_mandar+"&proyec_vida_cual="+$('#drag'+ventanaActual.num).find('#providacual').val();
				
				if($('#drag'+ventanaActual.num).find('#conalcoho1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&consum_alcohol="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&consum_alcohol="+"false";
				}
			    valores_a_mandar=valores_a_mandar+"&consum_alcohol_frecu="+$('#drag'+ventanaActual.num).find('#frecualcohol').val();
			    valores_a_mandar=valores_a_mandar+"&estad_animo="+$('#drag'+ventanaActual.num).find('#cmbestadoanimo').val();				
				
				if($('#drag'+ventanaActual.num).find('#conspsico1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&consum_psicofar="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&consum_psicofar="+"false";
				}
			    valores_a_mandar=valores_a_mandar+"&consum_psicofar_frecu="+$('#drag'+ventanaActual.num).find('#frecupsicofar').val();
			    valores_a_mandar=valores_a_mandar+"&consum_psicofar_cual="+$('#drag'+ventanaActual.num).find('#cualpsicofarma').val();
				
				if($('#drag'+ventanaActual.num).find('#fuma1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&fuma="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&fuma="+"false";
				}
			    valores_a_mandar=valores_a_mandar+"&fuma_frecu="+$('#drag'+ventanaActual.num).find('#frefuma').val();
			    valores_a_mandar=valores_a_mandar+"&fuma_cual="+$('#drag'+ventanaActual.num).find('#cualfuma').val();
				
				if($('#drag'+ventanaActual.num).find('#usarmas1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&usa_armas="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&usa_armas="+"false";
				}
			    valores_a_mandar=valores_a_mandar+"&usa_armas_frecu="+$('#drag'+ventanaActual.num).find('#frecuarmas').val();
			    valores_a_mandar=valores_a_mandar+"&usa_armas_cual="+$('#drag'+ventanaActual.num).find('#cualarmas').val();
											    
				if($('#drag'+ventanaActual.num).find('#antecejudic1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&antece_judico="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&antece_judico="+"false";
				}
				valores_a_mandar=valores_a_mandar+"&antece_judico_cual="+$('#drag'+ventanaActual.num).find('#cualantecejudic').val();
				
		        valores_a_mandar=valores_a_mandar+"&activi_tiempo_libre="+$('#drag'+ventanaActual.num).find('#actitielibre').val();
 			    valores_a_mandar=valores_a_mandar+"&ocupacion="+$('#drag'+ventanaActual.num).find('#ocupacionj').val();				
				valores_a_mandar=valores_a_mandar+"&escolaridad="+$('#drag'+ventanaActual.num).find('#escolarij').val();
				

				if($('#drag'+ventanaActual.num).find('#deserescola1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&ser_escolar="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&ser_escolar="+"false";
				}
			    valores_a_mandar=valores_a_mandar+"&ser_escolar_porque="+$('#drag'+ventanaActual.num).find('#porquedesescolar').val();				
				
			    valores_a_mandar=valores_a_mandar+"&rendi_academi="+$('#drag'+ventanaActual.num).find('#cmbrendiacade').val();								
				valores_a_mandar=valores_a_mandar+"&rendi_academi_porque="+$('#drag'+ventanaActual.num).find('#porquerendiaca').val();				
			    valores_a_mandar=valores_a_mandar+"&numer_estable_estudi="+$('#drag'+ventanaActual.num).find('#cantestaestudi').val();
				
				valores_a_mandar=valores_a_mandar+"&discipli_esco="+$('#drag'+ventanaActual.num).find('#cmbdisciescola').val();	;
			    valores_a_mandar=valores_a_mandar+"&discipli_esco_porque="+$('#drag'+ventanaActual.num).find('#porquedisciescola').val();
				valores_a_mandar=valores_a_mandar+"&rela_compa="+$('#drag'+ventanaActual.num).find('#cmbrelacompa').val();					
			    valores_a_mandar=valores_a_mandar+"&rela_compa_porque="+$('#drag'+ventanaActual.num).find('#porquerelacompa').val();	
				valores_a_mandar=valores_a_mandar+"&rela_profe="+$('#drag'+ventanaActual.num).find('#cmbrelaprofe').val();	
				valores_a_mandar=valores_a_mandar+"&rela_profe_porque="+$('#drag'+ventanaActual.num).find('#relaprofeporque').val();					
			    valores_a_mandar=valores_a_mandar+"&rela_amigos="+$('#drag'+ventanaActual.num).find('#cmbrelamigos').val();	
				valores_a_mandar=valores_a_mandar+"&rela_amigos_porque="+$('#drag'+ventanaActual.num).find('#relaamigosporque').val();	
				
				if($('#drag'+ventanaActual.num).find('#pergruorga1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&perte_grup_organi="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&perte_grup_organi="+"false";
				}
				valores_a_mandar=valores_a_mandar+"&perte_grup_organi_cual="+$('#drag'+ventanaActual.num).find('#cualpertgrupo').val();
				
				if($('#drag'+ventanaActual.num).find('#seintersocial1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&se_inter_social="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&se_inter_social="+"false";
				}
				
				if($('#drag'+ventanaActual.num).find('#realtrabacomu1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&real_acion_comuni="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&real_acion_comuni="+"false";
				}				
				valores_a_mandar=valores_a_mandar+"&caracte_grup_amigos="+$('#drag'+ventanaActual.num).find('#caracgrupamigos').val();
				
				if($('#drag'+ventanaActual.num).find('#intrespiri1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&inter_espiri="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&inter_espiri="+"false";
				}								
			    valores_a_mandar=valores_a_mandar+"&religion="+$('#drag'+ventanaActual.num).find('#religionj').val();
				
				if($('#drag'+ventanaActual.num).find('#practireli1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&practicante="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&practicante="+"false";
				}
				
				if($('#drag'+ventanaActual.num).find('#interactiecolo1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&inter_activi_ecolo="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&inter_activi_ecolo="+"false";
				}
				
				if($('#drag'+ventanaActual.num).find('#interimagecorpo1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&inter_imagen_corpo="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&inter_imagen_corpo="+"false";
				}
				
				if($('#drag'+ventanaActual.num).find('#realizaejer1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&realiza_ejerci="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&realiza_ejerci="+"false";
				}								
				valores_a_mandar=valores_a_mandar+"&actividad_fis="+$('#drag'+ventanaActual.num).find('#activifisj').val();				
				valores_a_mandar=valores_a_mandar+"&edad_menar_esper="+$('#drag'+ventanaActual.num).find('#edadmenarca').val();
				valores_a_mandar=valores_a_mandar+"&fum="+$('#drag'+ventanaActual.num).find('#fumj').val();
				
				if($('#drag'+ventanaActual.num).find('#ciclosj1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&ciclos="+"R";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&ciclos="+"I";
				}												
				valores_a_mandar=valores_a_mandar+"&duracion="+$('#drag'+ventanaActual.num).find('#duracionj').val();

				if($('#drag'+ventanaActual.num).find('#dismorea1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&dismorrea="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&dismorrea="+"false";
				}												

				if($('#drag'+ventanaActual.num).find('#sangranormal1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&sangr_anor="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&sangr_anor="+"false";
				}												

				if($('#drag'+ventanaActual.num).find('#iniactivsex1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&ini_activi_sexu="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&ini_activi_sexu="+"false";
				}												
				valores_a_mandar=valores_a_mandar+"&ini_activi_sexu_edad="+$('#drag'+ventanaActual.num).find('#edadjo').val();
				valores_a_mandar=valores_a_mandar+"&ini_activi_sexu_frecu="+$('#drag'+ventanaActual.num).find('#fecuenactisex').val();

				if($('#drag'+ventanaActual.num).find('#vidsexacti1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&vida_sex_acti="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&vida_sex_acti="+"false";
				}												
				valores_a_mandar=valores_a_mandar+"&num_compa_ulti_ano="+$('#drag'+ventanaActual.num).find('#numcompaano').val();

				if($('#drag'+ventanaActual.num).find('#mastrubaj1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&masturba="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&masturba="+"false";
				}												
				valores_a_mandar=valores_a_mandar+"&masturba_fre="+$('#drag'+ventanaActual.num).find('#frecuemasturbj').val();

				if($('#drag'+ventanaActual.num).find('#conometoantico1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&conoci_meto_anti="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&conoci_meto_anti="+"false";
				}												
				valores_a_mandar=valores_a_mandar+"&conoci_meto_anti_cual="+$('#drag'+ventanaActual.num).find('#cualmetoanticon').val();
				
				if($('#drag'+ventanaActual.num).find('#usometoanticoncep1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&uso_meto_anti="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&uso_meto_anti="+"false";
				}												
				valores_a_mandar=valores_a_mandar+"&uso_meto_anti_cual="+$('#drag'+ventanaActual.num).find('#cualmetoanticon').val();
				
				if($('#drag'+ventanaActual.num).find('#conoits1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&conocimi_its="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&conocimi_its="+"false";
				}												

				if($('#drag'+ventanaActual.num).find('#anteits1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&antece_its="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&antece_its="+"false";
				}												
				valores_a_mandar=valores_a_mandar+"&antece_its_cual="+$('#drag'+ventanaActual.num).find('#anteceits').val();
				
				valores_a_mandar=valores_a_mandar+"&ago_g="+$('#drag'+ventanaActual.num).find('#txtg').val();
			    valores_a_mandar=valores_a_mandar+"&ago_p="+$('#drag'+ventanaActual.num).find('#txtp').val();
				valores_a_mandar=valores_a_mandar+"&ago_c="+$('#drag'+ventanaActual.num).find('#txtc').val();
			    valores_a_mandar=valores_a_mandar+"&ago_a="+$('#drag'+ventanaActual.num).find('#txta').val();
				valores_a_mandar=valores_a_mandar+"&ago_e="+$('#drag'+ventanaActual.num).find('#txte').val();
			    valores_a_mandar=valores_a_mandar+"&ago_m="+$('#drag'+ventanaActual.num).find('#txtm').val();
			    valores_a_mandar=valores_a_mandar+"&ago_v="+$('#drag'+ventanaActual.num).find('#txtv').val();				
				valores_a_mandar=valores_a_mandar+"&edad_pre_gesta="+$('#drag'+ventanaActual.num).find('#edadprigest').val();	
				
				if($('#drag'+ventanaActual.num).find('#deseagest1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&desea="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&desea="+"false";
				}
				if($('#drag'+ventanaActual.num).find('#citocerviute1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&citolo_cer_uterina="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&citolo_cer_uterina="+"false";
				}
				valores_a_mandar=valores_a_mandar+"&ul_citolo="+$('#drag'+ventanaActual.num).find('#ultimacitoloj').val();				
				if($('#drag'+ventanaActual.num).find('#resulcitoulti1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&resultado_ul_citolo="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&resultado_ul_citolo="+"anormal";
				}
				
				if($('#drag'+ventanaActual.num).find('#alterfamipatoca1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&alter_fami_pato="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&alter_fami_pato="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#adostrabaj1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&adoles_traba="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&adoles_traba="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#malfisemoc1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&maltra_fis_emo="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&maltra_fis_emo="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#crisitua1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&crisis_situ="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&crisis_situ="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#violaabussex1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&viola_abuso="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&viola_abuso="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#separj1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&separa="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&separa="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#desplavictimaviol').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&despla_violencia="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&despla_violencia="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#dueloj').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&duelo="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&duelo="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#intesuici').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&inten_suici="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&inten_suici="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#perdiemple').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&perdi_empleo="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&perdi_empleo="+"anormal";
				}
 		        valores_a_mandar=valores_a_mandar+"&otros="+$('#drag'+ventanaActual.num).find('#txtotrosj').val();
				
				
				if($('#drag'+ventanaActual.num).find('#diagnostij1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&diagnostico="+"sin factores";									
				}else if($('#drag'+ventanaActual.num).find('#diagnostij2').attr("checked") == true){
 			     valores_a_mandar=valores_a_mandar+"&diagnostico="+"con factores";
				}else if($('#drag'+ventanaActual.num).find('#diagnostij3').attr("checked") == true){
 			     valores_a_mandar=valores_a_mandar+"&diagnostico="+"con enfermedad";
				}				
 		        valores_a_mandar=valores_a_mandar+"&diagnostico_cual="+$('#drag'+ventanaActual.num).find('#diagcual').val();
				
				if($('#drag'+ventanaActual.num).find('#enseautotest1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&ense_auto_tes_mama="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&ense_auto_tes_mama="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#anticonj1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&anticon="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&anticon="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#inforpreriespro1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&infor_orie_preve="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&infor_orie_preve="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#factriesgo1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&factor_riesgo="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&factor_riesgo="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#hbht1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&hb_hto="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&hb_hto="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#vdrl1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&vdrl_sex="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&vdrl_sex="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#colester1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&colestetol_hdl="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&colestetol_hdl="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#vihsid1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&vih_sida="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&vih_sida="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#citocuj1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&cito_cu="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&cito_cu="+"anormal";
				}
				if($('#drag'+ventanaActual.num).find('#tratamj1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&tratamiento="+"normal";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&tratamiento="+"anormal";
				}
			    valores_a_mandar=valores_a_mandar+"&tratamiento_cual="+$('#drag'+ventanaActual.num).find('#resultrata').val();
			    valores_a_mandar=valores_a_mandar+"&resulta_paracli="+$('#drag'+ventanaActual.num).find('#resulpracli').val();
				
		 		alert(valores_a_mandar);
				//ajaxGuardar();
		 break; 
		 case 'registroPlanificacionFamiliar':
		        alert("Planificacion Familiar");
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");				
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];

				valores_a_mandar=valores_a_mandar+"&conlentes_ojoderecho="+$('#drag'+ventanaActual.num).find('#ojoderconlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoderconlentedos').val();
		        valores_a_mandar=valores_a_mandar+"&conlentes_ojoizquierdo="+$('#drag'+ventanaActual.num).find('#ojoizqconlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoizqconlentesdos').val();				
		        valores_a_mandar=valores_a_mandar+"&conlentes_ambosojos="+$('#drag'+ventanaActual.num).find('#ojoambosconlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoambosconlentesdos').val();
				
 			    valores_a_mandar=valores_a_mandar+"&sinlentes_ojoderecho="+$('#drag'+ventanaActual.num).find('#ojodersinlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojodersinlentesdos').val();
				valores_a_mandar=valores_a_mandar+"&sinlentes_ojoizquierdo="+$('#drag'+ventanaActual.num).find('#ojoizqsinlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoizqsinlentesdos').val();			
				valores_a_mandar=valores_a_mandar+"&sinlentes_ambosojos="+$('#drag'+ventanaActual.num).find('#ojosambsinlentesuno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojosambsinlentesdos').val();				
				
 			    valores_a_mandar=valores_a_mandar+"&visioncercana_ojoderecho="+$('#drag'+ventanaActual.num).find('#ojodercercanauno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojodercercanados').val();
			    valores_a_mandar=valores_a_mandar+"&visioncercana_ojoizquierdo="+$('#drag'+ventanaActual.num).find('#ojoizqcercanauno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoizqcercanados').val();
			    valores_a_mandar=valores_a_mandar+"&visioncercana_ambosojos="+$('#drag'+ventanaActual.num).find('#ojoambcercauno').val()+"/"+$('#drag'+ventanaActual.num).find('#ojoambcercados').val();
				
			    valores_a_mandar=valores_a_mandar+"&diagnostico="+$('#drag'+ventanaActual.num).find('#diagnostico').val();								
				valores_a_mandar=valores_a_mandar+"&conducta="+$('#drag'+ventanaActual.num).find('#conducta').val();
				if($('#drag'+ventanaActual.num).find('#remision1').attr("checked") == true){
			     valores_a_mandar=valores_a_mandar+"&remision="+"true";									
				}else{
 			     valores_a_mandar=valores_a_mandar+"&remision="+"false";
				}
		 		alert(valores_a_mandar);
				//ajaxGuardar();
		 break; 
		 //inicio materno
	    case 'antecedentesmat':
		       // alert("1");
				var vect = $('#drag'+ventanaActual.num).find('#lblHistoria').html().split(" ");
		        valores_a_mandar="accion="+arg;
		        //valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        //valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];
 			    valores_a_mandar=valores_a_mandar+"&checirgpel="+$('#drag'+ventanaActual.num).find('#checirgpel').attr("checked");
				valores_a_mandar=valores_a_mandar+"&cheinfert="+$('#drag'+ventanaActual.num).find('#cheinfert').attr("checked");
				valores_a_mandar=valores_a_mandar+"&chevihpos="+$('#drag'+ventanaActual.num).find('#chevihpos').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&chemolas="+$('#drag'+ventanaActual.num).find('#chemolas').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&checarnepar="+$('#drag'+ventanaActual.num).find('#checarnepar').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&checonmedgr="+$('#drag'+ventanaActual.num).find('#checonmedgr').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&cheectopi="+$('#drag'+ventanaActual.num).find('#cheectopi').attr("checked");				
				valores_a_mandar=valores_a_mandar+"&txtgesprev="+$('#drag'+ventanaActual.num).find('#txtgesprev').val();				
				valores_a_mandar=valores_a_mandar+"&txtabortos="+$('#drag'+ventanaActual.num).find('#txtabortos').val();				
				valores_a_mandar=valores_a_mandar+"&txtvaginales="+$('#drag'+ventanaActual.num).find('#txtvaginales').val();				
 			    valores_a_mandar=valores_a_mandar+"&txtnacvivos="+$('#drag'+ventanaActual.num).find('#txtnacvivos').val();				
			    valores_a_mandar=valores_a_mandar+"&txtviven="+$('#drag'+ventanaActual.num).find('#txtviven').val();
			    valores_a_mandar=valores_a_mandar+"&txttresespcon="+$('#drag'+ventanaActual.num).find('#txttresespcon').val();
			    valores_a_mandar=valores_a_mandar+"&txtmuertsem="+$('#drag'+ventanaActual.num).find('#txtmuertsem').val();				
				valores_a_mandar=valores_a_mandar+"&cmbultimoprevio="+$('#drag'+ventanaActual.num).find('#cmbultimoprevio').val();
				valores_a_mandar=valores_a_mandar+"&txtpartos="+$('#drag'+ventanaActual.num).find('#txtpartos').val();
				valores_a_mandar=valores_a_mandar+"&txtcesareas="+$('#drag'+ventanaActual.num).find('#txtcesareas').val();
 			    valores_a_mandar=valores_a_mandar+"&txtnacimuer="+$('#drag'+ventanaActual.num).find('#txtnacimuer').val();
			    valores_a_mandar=valores_a_mandar+"&txtdesunasem="+$('#drag'+ventanaActual.num).find('#txtdesunasem').val();				
			    valores_a_mandar=valores_a_mandar+"&cheemnpaldes="+$('#drag'+ventanaActual.num).find('#cheemnpaldes').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&txtfechaanteemb="+$('#drag'+ventanaActual.num).find('#txtfechaanteemb').val();				
				valores_a_mandar=valores_a_mandar+"&chemenorunmascin="+$('#drag'+ventanaActual.num).find('#chemenorunmascin').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&cmbmetodoanti="+$('#drag'+ventanaActual.num).find('#cmbmetodoanti').val();			
				//alert(valores_a_mandar);
			    //ajaxGuardar();
		 break;	
	    case 'gestacionactualmat':
		        //alert("2");
		 break;	
	    case 'citasmaternomat':
		        //alert("3");
		 break;	
	    case 'partoabortomat':
		        //alert("4");
		 break;	
	    case 'reciennacidomat':
		        //alert("5");
		 break;	
	    case 'puerperiomat':
		        //alert("6");
		 break;	
	    case 'egresomat':
		        //alert("7");
		 break;	

    //fin materno
	//citologia//
	    case 'datospersonalescitologia':
		       // alert("datospersonalescitologia");
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");
		        valores_a_mandar="accion="+arg;
		        valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];
 			    valores_a_mandar=valores_a_mandar+"&txtOcupacion="+$('#drag'+ventanaActual.num).find('#cmbOcupacion').val();
				valores_a_mandar=valores_a_mandar+"&txtEstadocivil="+$('#drag'+ventanaActual.num).find('#cmbEstadoCivil').val();
				valores_a_mandar=valores_a_mandar+"&txtPais="+$('#drag'+ventanaActual.num).find('#cmbPaisRes').val();
 			    valores_a_mandar=valores_a_mandar+"&txtDepto="+$('#drag'+ventanaActual.num).find('#cmbDeptoRes').val();
			    valores_a_mandar=valores_a_mandar+"&txtMcipio="+$('#drag'+ventanaActual.num).find('#cmbMpioRes').val();
			    valores_a_mandar=valores_a_mandar+"&txtBarrio="+$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val();
			    valores_a_mandar=valores_a_mandar+"&txtResiden="+$('#drag'+ventanaActual.num).find('#cmbZonaResidencia').val();
				//alert(valores_a_mandar);
			    //ajaxGuardar();
		 break;
	    case 'historiaclinicacitologia':
		       //alert("historiaclinicacitologia");
			    var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");
				 if($('#drag'+ventanaActual.num).find('#txtplaca').val()!=''){
			     valores_a_mandar="accion="+arg;
				 valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		         valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];
 			     valores_a_mandar=valores_a_mandar+"&fechaatencion="+$('#drag'+ventanaActual.num).find('#lblFechaAt').html();
				 valores_a_mandar=valores_a_mandar+"&nplaca="+$('#drag'+ventanaActual.num).find('#txtplaca').val();
		         valores_a_mandar=valores_a_mandar+"&txtg="+$('#drag'+ventanaActual.num).find('#txtg').val();
		         valores_a_mandar=valores_a_mandar+"&txtp="+$('#drag'+ventanaActual.num).find('#txtp').val();
 			     valores_a_mandar=valores_a_mandar+"&txta="+$('#drag'+ventanaActual.num).find('#txta').val();
				 valores_a_mandar=valores_a_mandar+"&txtc="+$('#drag'+ventanaActual.num).find('#txtc').val();
				 valores_a_mandar=valores_a_mandar+"&chediu="+$('#drag'+ventanaActual.num).find('#chediu').attr("checked");
 			     valores_a_mandar=valores_a_mandar+"&cheano="+$('#drag'+ventanaActual.num).find('#cheano').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&cheotro="+$('#drag'+ventanaActual.num).find('#cheotro').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&txtCualp="+$('#drag'+ventanaActual.num).find('#txtCualp').val();
			     valores_a_mandar=valores_a_mandar+"&txtFum="+$('#drag'+ventanaActual.num).find('#txtFum').val();				
				 valores_a_mandar=valores_a_mandar+"&chep="+$('#drag'+ventanaActual.num).find('#chep').attr("checked");				
 			     valores_a_mandar=valores_a_mandar+"&cher="+$('#drag'+ventanaActual.num).find('#cher').attr("checked");				
				 valores_a_mandar=valores_a_mandar+"&txtfechaC="+$('#drag'+ventanaActual.num).find('#txtfechaC').val();				
				 valores_a_mandar=valores_a_mandar+"&txtResultadoC="+$('#drag'+ventanaActual.num).find('#txtResultadoC').val();				
 			     valores_a_mandar=valores_a_mandar+"&cheAusente="+$('#drag'+ventanaActual.num).find('#cheAusente').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&cheSano="+$('#drag'+ventanaActual.num).find('#cheSano').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&cheAnormal="+$('#drag'+ventanaActual.num).find('#cheAnormal').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&txtEspecifiqueA="+$('#drag'+ventanaActual.num).find('#txtEspecifiqueA').val();				
				 valores_a_mandar=valores_a_mandar+"&checkHiste="+$('#drag'+ventanaActual.num).find('#checkHiste').attr("checked");		
				 valores_a_mandar=valores_a_mandar+"&checkRadioT="+$('#drag'+ventanaActual.num).find('#checkRadioT').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&checkTto="+$('#drag'+ventanaActual.num).find('#checkTto').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&checkotro="+$('#drag'+ventanaActual.num).find('#checkotro').attr("checked");
			     valores_a_mandar=valores_a_mandar+"&txtCualPro="+$('#drag'+ventanaActual.num).find('#txtCualPro').val();
				 //alert(valores_a_mandar);
				 ajaxGuardar();
				 }else{
					//alert("Falta escribir numero de placa");
					//$('#drag'+ventanaActual.num).find('#txtplaca').focus();
				 }
		break;
		case 'resultadocitologia':
		 //alert("resultadocitologia");
		   if($('#drag'+ventanaActual.num).find('#txtplaca').val()!=''){
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");				
		 		valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];
 			    valores_a_mandar=valores_a_mandar+"&fechaatencion="+$('#drag'+ventanaActual.num).find('#lblFechaAt').html();
				valores_a_mandar=valores_a_mandar+"&nplaca="+$('#drag'+ventanaActual.num).find('#txtplaca').val();
		        valores_a_mandar=valores_a_mandar+"&cheSatisfactorio="+$('#drag'+ventanaActual.num).find('#cheSatisfactorio').attr("checked");
		        valores_a_mandar=valores_a_mandar+"&chePresentes="+$('#drag'+ventanaActual.num).find('#chePresentes').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&cheAusentes="+$('#drag'+ventanaActual.num).find('#cheAusentes').attr("checked");
				valores_a_mandar=valores_a_mandar+"&checkNegativa="+$('#drag'+ventanaActual.num).find('#checkNegativa').attr("checked");
				valores_a_mandar=valores_a_mandar+"&checkAnormalidad="+$('#drag'+ventanaActual.num).find('#checkAnormalidad').attr("checked");
 			    valores_a_mandar=valores_a_mandar+"&checkOtros="+$('#drag'+ventanaActual.num).find('#checkOtros').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&cheInsatisfactorio="+$('#drag'+ventanaActual.num).find('#cheInsatisfactorio').attr("checked");
			    valores_a_mandar=valores_a_mandar+"&txtMuestraPro="+$('#drag'+ventanaActual.num).find('#txtMuestraPro').val();
			    valores_a_mandar=valores_a_mandar+"&txtMuestraRech="+$('#drag'+ventanaActual.num).find('#txtMuestraRech').val();				
		 		//alert(valores_a_mandar);
				ajaxGuardar();
			}
		break;
		case 'interpretacioncitologia':
		          //alert("interpretacioncitologia");
				  if($('#drag'+ventanaActual.num).find('#txtplaca').val()!=''){
					 var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");
					  valores_a_mandar="accion="+arg;
					  valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
					  valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];
					  valores_a_mandar=valores_a_mandar+"&fechaatencion="+$('#drag'+ventanaActual.num).find('#lblFechaAt').html();
					  valores_a_mandar=valores_a_mandar+"&nplaca="+$('#drag'+ventanaActual.num).find('#txtplaca').val();
					  valores_a_mandar=valores_a_mandar+"&checkTrichomas="+$('#drag'+ventanaActual.num).find('#checkTrichomas').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkConsistente="+$('#drag'+ventanaActual.num).find('#checkConsistente').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkFlora="+$('#drag'+ventanaActual.num).find('#checkFlora').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkBacterias="+$('#drag'+ventanaActual.num).find('#checkBacterias').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkCambios="+$('#drag'+ventanaActual.num).find('#checkCambios').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkBacilo="+$('#drag'+ventanaActual.num).find('#checkBacilo').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&checkCambiosCelulares="+$('#drag'+ventanaActual.num).find('#checkCambiosCelulares').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkInflamacion="+$('#drag'+ventanaActual.num).find('#checkInflamacion').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheL="+$('#drag'+ventanaActual.num).find('#cheL').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheM="+$('#drag'+ventanaActual.num).find('#cheM').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheS="+$('#drag'+ventanaActual.num).find('#cheS').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkRadiacion="+$('#drag'+ventanaActual.num).find('#checkRadiacion').attr("checked");								
					  valores_a_mandar=valores_a_mandar+"&checkContracepcion="+$('#drag'+ventanaActual.num).find('#checkContracepcion').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkCelulasGlandulares="+$('#drag'+ventanaActual.num).find('#checkCelulasGlandulares').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkAtrofia="+$('#drag'+ventanaActual.num).find('#checkAtrofia').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&checkPresencia="+$('#drag'+ventanaActual.num).find('#checkPresencia').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheCelEscaAnor="+$('#drag'+ventanaActual.num).find('#cheCelEscaAnor').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheSignificacnciaInter="+$('#drag'+ventanaActual.num).find('#cheSignificacnciaInter').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheNoPuedeLIE="+$('#drag'+ventanaActual.num).find('#cheNoPuedeLIE').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheLesionIntra="+$('#drag'+ventanaActual.num).find('#cheLesionIntra').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&cheSugestivo="+$('#drag'+ventanaActual.num).find('#cheSugestivo').attr("checked");								
					  valores_a_mandar=valores_a_mandar+"&cheDisplasia="+$('#drag'+ventanaActual.num).find('#cheDisplasia').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheIntraEscam="+$('#drag'+ventanaActual.num).find('#cheIntraEscam').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheDisplaMod="+$('#drag'+ventanaActual.num).find('#cheDisplaMod').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheDisplaSev="+$('#drag'+ventanaActual.num).find('#cheDisplaSev').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&checarsosinva="+$('#drag'+ventanaActual.num).find('#checarsosinva').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&cheCarciCel="+$('#drag'+ventanaActual.num).find('#cheCarciCel').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&cheCelGlanAti="+$('#drag'+ventanaActual.num).find('#cheCelGlanAti').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheEndocer="+$('#drag'+ventanaActual.num).find('#cheEndocer').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheEndom="+$('#drag'+ventanaActual.num).find('#cheEndom').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheSinEspec="+$('#drag'+ventanaActual.num).find('#cheSinEspec').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&cheCelGlanPos="+$('#drag'+ventanaActual.num).find('#cheCelGlanPos').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&cheEndocervical="+$('#drag'+ventanaActual.num).find('#cheEndocervical').attr("checked");				
					  valores_a_mandar=valores_a_mandar+"&cheSinEspec2="+$('#drag'+ventanaActual.num).find('#cheSinEspec2').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheAdenoEndo="+$('#drag'+ventanaActual.num).find('#cheAdenoEndo').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&cheAdenocer="+$('#drag'+ventanaActual.num).find('#cheAdenocer').attr("checked");
					  valores_a_mandar=valores_a_mandar+"&txtCualOtras="+$('#drag'+ventanaActual.num).find('#txtCualOtras').val();				
					  valores_a_mandar=valores_a_mandar+"&txtsugerencias="+$('#drag'+ventanaActual.num).find('#txtsugerencias').val();								
					 // alert(valores_a_mandar);	
					  ajaxGuardar();
				  }
		break;
		///fincitologia
		//iniciocrecimientoydesarrollo
		case 'crecimientodesarrollocontroles':
				valores_a_mandar="accion="+arg;
				valores_a_mandar=valores_a_mandar+"&lblIdCita="+$('#drag'+ventanaActual.num).find('#lblIdCita').text();				
				var ids = jQuery("#listControles").getDataIDs();
				for(var i=0;i<ids.length;i++){ 
					var c = ids[i];
					var datosDelRegistro = jQuery("#listControles").getRowData(c);  
					valorestado=$.trim(datosDelRegistro.Estado);  
					if(valorestado=='1'){
						valores_a_mandar=valores_a_mandar+"&fecha="+$.trim(datosDelRegistro.Fecha);
						valores_a_mandar=valores_a_mandar+"&edad="+$.trim(datosDelRegistro.Edad);
						valores_a_mandar=valores_a_mandar+"&peso="+$.trim(datosDelRegistro.Peso);			   
						valores_a_mandar=valores_a_mandar+"&talla="+$.trim(datosDelRegistro.Talla);			   
						valores_a_mandar=valores_a_mandar+"&temperatura="+$.trim(datosDelRegistro.Temperatura);			   
						valores_a_mandar=valores_a_mandar+"&perimecefalico="+$.trim(datosDelRegistro.Perimetrocefalico);			   
						valores_a_mandar=valores_a_mandar+"&perimetoraxico="+$.trim(datosDelRegistro.Perimetrotoraxico);			   
						valores_a_mandar=valores_a_mandar+"&cabeza="+$.trim(datosDelRegistro.Cabeza);			   
						valores_a_mandar=valores_a_mandar+"&ojos="+$.trim(datosDelRegistro.Ojos);			   
						valores_a_mandar=valores_a_mandar+"&nariz="+$.trim(datosDelRegistro.Nariz);			   
						valores_a_mandar=valores_a_mandar+"&oidos="+$.trim(datosDelRegistro.Oidos);			   
						valores_a_mandar=valores_a_mandar+"&boca="+$.trim(datosDelRegistro.Boca);			   
						valores_a_mandar=valores_a_mandar+"&cuello="+$.trim(datosDelRegistro.Cuello);			   
						valores_a_mandar=valores_a_mandar+"&toraxcardiores="+$.trim(datosDelRegistro.Toraxcardiores);			   
						valores_a_mandar=valores_a_mandar+"&abdomen="+$.trim(datosDelRegistro.Abdomen);			   
						valores_a_mandar=valores_a_mandar+"&genitouri="+$.trim(datosDelRegistro.Genitouri);			   
						valores_a_mandar=valores_a_mandar+"&ano="+$.trim(datosDelRegistro.Ano);			   
						valores_a_mandar=valores_a_mandar+"&extremidades="+$.trim(datosDelRegistro.Extremidades);			   
						valores_a_mandar=valores_a_mandar+"&piel="+$.trim(datosDelRegistro.Piel);			   
						valores_a_mandar=valores_a_mandar+"&sistemanervioso="+$.trim(datosDelRegistro.Sistemanervioso);			   
						valores_a_mandar=valores_a_mandar+"&motora="+$.trim(datosDelRegistro.Motora);			   
						valores_a_mandar=valores_a_mandar+"&adaptativa="+$.trim(datosDelRegistro.Adaptativa);			   
						valores_a_mandar=valores_a_mandar+"&lenguaje="+$.trim(datosDelRegistro.Lenguaje);			   
						valores_a_mandar=valores_a_mandar+"&personalsocial="+$.trim(datosDelRegistro.Personalsocial);			   
						valores_a_mandar=valores_a_mandar+"&curvapeso="+$.trim(datosDelRegistro.Curvapeso1);			   
						valores_a_mandar=valores_a_mandar+"&curvatalla="+$.trim(datosDelRegistro.Curvatalla1);			   
						valores_a_mandar=valores_a_mandar+"&estadonutricional="+$.trim(datosDelRegistro.Estado_nutricional1);			   
						valores_a_mandar=valores_a_mandar+"&remitido="+$.trim(datosDelRegistro.Remitido);			   
						valores_a_mandar=valores_a_mandar+"&proximacita="+$.trim(datosDelRegistro.Proximacita);			   
				   }
				}
			   alert(valores_a_mandar);
				//ajaxGuardar();		
		break;
		
		case 'crecimientodesarrolloescalaabreviada':
				valores_a_mandar="accion="+arg;
				var vect = $('#drag'+ventanaActual.num).find('#lblId').html().split(" ");
				valores_a_mandar=valores_a_mandar+"&txtTipoId="+vect[0];
		        valores_a_mandar=valores_a_mandar+"&txtId="+vect[2];				
 			    valores_a_mandar=valores_a_mandar+"&A=";			   
			    for(var j=0;j<37;j++){
					cad1='txt_1_'+j;
					if(document.getElementById(cad1)!=null){
						cad3=$('#drag'+ventanaActual.num).find('#'+cad1).val();
						valores_a_mandar=valores_a_mandar+cad3+",";
					}
				}	
				valores_a_mandar=valores_a_mandar+"&B=";			   
			    for(var j=0;j<38;j++){
					cad1='txt_2_'+j;
					if(document.getElementById(cad1)!=null){
						cad3=$('#drag'+ventanaActual.num).find('#'+cad1).val();
						valores_a_mandar=valores_a_mandar+cad3+",";
					}
				}	
				valores_a_mandar=valores_a_mandar+"&C=";			   
			    for(var j=0;j<40;j++){
					cad1='txt_3_'+j;
					if(document.getElementById(cad1)!=null){
						cad3=$('#drag'+ventanaActual.num).find('#'+cad1).val();
						valores_a_mandar=valores_a_mandar+cad3+",";
					}
				}	
				valores_a_mandar=valores_a_mandar+"&D=";			   
			    for(var j=0;j<37;j++){
					cad1='txt_4_'+j;
					if(document.getElementById(cad1)!=null){
						cad3=$('#drag'+ventanaActual.num).find('#'+cad1).val();
						valores_a_mandar=valores_a_mandar+cad3+",";
					}
				}	
			   // alert(valores_a_mandar);
				//ajaxGuardar();		
		break;
		
		
		//fincrecimientoydesarrollo				
		case 'tiposIdentificaciones':     
		   if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxGuardar();
		   }
		break;	
		case 'tiposEspecialidades':     
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxGuardar();
		   }
		break;
		case 'tiposAfiliados':     
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxGuardar();
		   }
		break;	
		case 'viasIngreso':     
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxGuardar();
		   }
		break;
		case 'centrosCostos':     
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxGuardar();
		   }
		break;
		case 'tiposServicios':     
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxGuardar();
		   }
		break;
		case 'consultorios':     
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxGuardar();
		   }
		break;
		case 'prestadorasSalud':       
		 if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNit').val()=="")
			{
				alert("Favor ingresar un Nit  Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNit="+$('#drag'+ventanaActual.num).find('#txtNit').val();
   		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
               cadenabarrio = $('#drag'+ventanaActual.num).find('#cmbMpioRes').val();
			   cadenabarrio = cadenabarrio +"_"+$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val(); 
               valores_a_mandar=valores_a_mandar+"&codBarrio="+cadenabarrio;
		       valores_a_mandar=valores_a_mandar+"&txtDir="+$('#drag'+ventanaActual.num).find('#txtDir').val();
		       valores_a_mandar=valores_a_mandar+"&txtTelefono="+$('#drag'+ventanaActual.num).find('#txtTelefono').val();
			   valores_a_mandar=valores_a_mandar+"&txtFax="+$('#drag'+ventanaActual.num).find('#txtFax').val();
		       valores_a_mandar=valores_a_mandar+"&txtEmail="+$('#drag'+ventanaActual.num).find('#txtEmail').val();
			   valores_a_mandar=valores_a_mandar+"&txtResponsable="+$('#drag'+ventanaActual.num).find('#txtResponsable').val();
		       valores_a_mandar=valores_a_mandar+"&rol="+"P";
			   valores_a_mandar=valores_a_mandar+"&codigoent="+"0";
			   ajaxGuardar();	
			}
		break;		
		case 'entidadesAdministradoras':                 
		   if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNit').val()=="")
			{
				alert("Favor ingresar un Nit  Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNit="+$('#drag'+ventanaActual.num).find('#txtNit').val();
   		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
               cadenabarrio = $('#drag'+ventanaActual.num).find('#cmbMpioRes').val();
			   cadenabarrio = cadenabarrio +"_"+$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val(); 
               valores_a_mandar=valores_a_mandar+"&codBarrio="+cadenabarrio;
		       valores_a_mandar=valores_a_mandar+"&txtDir="+$('#drag'+ventanaActual.num).find('#txtDir').val();
		       valores_a_mandar=valores_a_mandar+"&txtTelefono="+$('#drag'+ventanaActual.num).find('#txtTelefono').val();
			   valores_a_mandar=valores_a_mandar+"&txtFax="+$('#drag'+ventanaActual.num).find('#txtFax').val();
		       valores_a_mandar=valores_a_mandar+"&txtEmail="+$('#drag'+ventanaActual.num).find('#txtEmail').val();
			   valores_a_mandar=valores_a_mandar+"&txtResponsable="+$('#drag'+ventanaActual.num).find('#txtResponsable').val();			   
		       valores_a_mandar=valores_a_mandar+"&rol="+"A";
			   valores_a_mandar=valores_a_mandar+"&codigoent="+$('#drag'+ventanaActual.num).find('#cmbTipoEntidad').val();
			   ajaxGuardar();
			}
		break;
		case 'horariosMedicos': 
		       turno_1=0; turno_2=0; turno_3=0; turno_4=0; turno_5=0; 
			   hora11=0; hora21=0; hora31=0; hora41=0; hora51=0; 
   			   hora12=0; hora22=0; hora32=0; hora42=0; hora52=0; 
			   pacientes1=0; pacientes2=0; pacientes3=0; pacientes4=0; pacientes5=0;
			   duracion1=0; duracion2=0; duracion3=0; duracion4=0; duracion5=0;
			   servicio1=0; servicio2=0; servicio3=0; servicio4=0; servicio5=0;
			   consultorio1=0; consultorio2=0; consultorio3=0; consultorio4=0; consultorio5=0;
				
			   valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&tipoId="+$('#drag'+ventanaActual.num).find('#lblTipoId').text();
		       valores_a_mandar=valores_a_mandar+"&nId="+$('#drag'+ventanaActual.num).find('#lblId').text();     
   		       valores_a_mandar=valores_a_mandar+"&diaT="+$('#drag'+ventanaActual.num).find('#txtFechaTurno').val();
			   
			   if ($('#drag'+ventanaActual.num).find('#cmbHoraInicio1').val() != "-"){
				  if ($('#drag'+ventanaActual.num).find('#cmbMinutoInicio1').val() != "-"){
				     if ($('#drag'+ventanaActual.num).find('#cmbAmPmInicio1').val() != "-"){				    
						if ($('#drag'+ventanaActual.num).find('#cmbHoraFin1').val() != "-"){
				          if ($('#drag'+ventanaActual.num).find('#cmbMinutoFin1').val() != "-"){
				            if ($('#drag'+ventanaActual.num).find('#cmbAmPmFin1').val() != "-"){				    
							    if ($('#drag'+ventanaActual.num).find('#txtCantidadP1').val() !=""){
	 							    if ($('#drag'+ventanaActual.num).find('#txtDuracionC1').val() !=""){
										 if ($('#drag'+ventanaActual.num).find('#cmbTipoServ1').val() != "0"){
											 if ($('#drag'+ventanaActual.num).find('#cmbConsult1').val() != "0"){
												turno_1 = "1";
												hora11 =  $('#drag'+ventanaActual.num).find('#cmbHoraInicio1').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoInicio1').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmInicio1').val();
												hora12 = $('#drag'+ventanaActual.num).find('#cmbHoraFin1').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoFin1').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmFin1').val();
												pacientes1 = $('#drag'+ventanaActual.num).find('#txtCantidadP1').val();
												duracion1 = $('#drag'+ventanaActual.num).find('#txtDuracionC1').val();
												servicio1 = $('#drag'+ventanaActual.num).find('#cmbTipoServ1').val();
												consultorio1 = $('#drag'+ventanaActual.num).find('#cmbConsult1').val();
											 }
										 }
									 }
							    }
							 }
						  }
						}
					 }
			      }
			   }
			  if ($('#drag'+ventanaActual.num).find('#cmbHoraInicio2').val() != "-"){
				  if ($('#drag'+ventanaActual.num).find('#cmbMinutoInicio2').val() != "-"){
				     if ($('#drag'+ventanaActual.num).find('#cmbAmPmInicio2').val() != "-"){				    
						if ($('#drag'+ventanaActual.num).find('#cmbHoraFin2').val() != "-"){
				          if ($('#drag'+ventanaActual.num).find('#cmbMinutoFin2').val() != "-"){
				            if ($('#drag'+ventanaActual.num).find('#cmbAmPmFin2').val() != "-"){				    
							    if ($('#drag'+ventanaActual.num).find('#txtCantidadP2').val() !=""){
	 							    if ($('#drag'+ventanaActual.num).find('#txtDuracionC2').val() !=""){
										 if ($('#drag'+ventanaActual.num).find('#cmbTipoServ2').val() != "0"){
											 if ($('#drag'+ventanaActual.num).find('#cmbConsult2').val() != "0"){
												turno_2 = "2";
												hora21 =  $('#drag'+ventanaActual.num).find('#cmbHoraInicio2').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoInicio2').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmInicio2').val();
												hora22 = $('#drag'+ventanaActual.num).find('#cmbHoraFin2').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoFin2').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmFin2').val();
												pacientes2 = $('#drag'+ventanaActual.num).find('#txtCantidadP2').val();
												duracion2 = $('#drag'+ventanaActual.num).find('#txtDuracionC2').val();
												servicio2 = $('#drag'+ventanaActual.num).find('#cmbTipoServ2').val(); 
												consultorio2 = $('#drag'+ventanaActual.num).find('#cmbConsult2').val();
											 }
										 }
									 }
							    }
							 }
						  }
						}
					 }
			      }
			   }

			 if ($('#drag'+ventanaActual.num).find('#cmbHoraInicio3').val() != "-"){
				  if ($('#drag'+ventanaActual.num).find('#cmbMinutoInicio3').val() != "-"){
				     if ($('#drag'+ventanaActual.num).find('#cmbAmPmInicio3').val() != "-"){				    
						if ($('#drag'+ventanaActual.num).find('#cmbHoraFin3').val() != "-"){
				          if ($('#drag'+ventanaActual.num).find('#cmbMinutoFin3').val() != "-"){
				            if ($('#drag'+ventanaActual.num).find('#cmbAmPmFin3').val() != "-"){				    
							    if ($('#drag'+ventanaActual.num).find('#txtCantidadP3').val() !=""){
	 							    if ($('#drag'+ventanaActual.num).find('#txtDuracionC3').val() !=""){
										 if ($('#drag'+ventanaActual.num).find('#cmbTipoServ3').val() != "0"){	
										    if ($('#drag'+ventanaActual.num).find('#cmbConsult3').val() != "0"){
												turno_3 = "3";
												hora31 =  $('#drag'+ventanaActual.num).find('#cmbHoraInicio3').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoInicio3').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmInicio3').val();
												hora32 = $('#drag'+ventanaActual.num).find('#cmbHoraFin3').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoFin3').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmFin3').val();
												pacientes3 = $('#drag'+ventanaActual.num).find('#txtCantidadP3').val();
												duracion3 = $('#drag'+ventanaActual.num).find('#txtDuracionC3').val();
												servicio3 = $('#drag'+ventanaActual.num).find('#cmbTipoServ3').val(); 
												consultorio3 = $('#drag'+ventanaActual.num).find('#cmbConsult3').val();
											 }
										 }
									 }
							    }
							 }
						  }
						}
					 }
			      }
			   }
			   
			   if ($('#drag'+ventanaActual.num).find('#cmbHoraInicio4').val() != "-"){
				  if ($('#drag'+ventanaActual.num).find('#cmbMinutoInicio4').val() != "-"){
				     if ($('#drag'+ventanaActual.num).find('#cmbAmPmInicio4').val() != "-"){				    
						if ($('#drag'+ventanaActual.num).find('#cmbHoraFin4').val() != "-"){
				          if ($('#drag'+ventanaActual.num).find('#cmbMinutoFin4').val() != "-"){
				            if ($('#drag'+ventanaActual.num).find('#cmbAmPmFin4').val() != "-"){				    
							    if ($('#drag'+ventanaActual.num).find('#txtCantidadP4').val() !=""){
	 							    if ($('#drag'+ventanaActual.num).find('#txtDuracionC4').val() !=""){
										 if ($('#drag'+ventanaActual.num).find('#cmbTipoServ4').val() != "0"){	
										  if ($('#drag'+ventanaActual.num).find('#cmbConsult4').val() != "0"){
												turno_4 = "4";
												hora41 =  $('#drag'+ventanaActual.num).find('#cmbHoraInicio4').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoInicio4').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmInicio4').val();
												hora42 = $('#drag'+ventanaActual.num).find('#cmbHoraFin4').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoFin4').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmFin4').val();
												pacientes4 = $('#drag'+ventanaActual.num).find('#txtCantidadP4').val();
												duracion4 = $('#drag'+ventanaActual.num).find('#txtDuracionC4').val();
												servicio4 = $('#drag'+ventanaActual.num).find('#cmbTipoServ4').val(); 
												consultorio4 = $('#drag'+ventanaActual.num).find('#cmbConsult4').val();
											 }
										 }
									 }
							    }
							 }
						  }
						}
					 }
			      }
			   }
			
			if ($('#drag'+ventanaActual.num).find('#cmbHoraInicio5').val() != "-"){
				  if ($('#drag'+ventanaActual.num).find('#cmbMinutoInicio5').val() != "-"){
				     if ($('#drag'+ventanaActual.num).find('#cmbAmPmInicio5').val() != "-"){				    
						if ($('#drag'+ventanaActual.num).find('#cmbHoraFin5').val() != "-"){
				          if ($('#drag'+ventanaActual.num).find('#cmbMinutoFin5').val() != "-"){
				            if ($('#drag'+ventanaActual.num).find('#cmbAmPmFin5').val() != "-"){				    
							    if ($('#drag'+ventanaActual.num).find('#txtCantidadP5').val() !=""){
	 							    if ($('#drag'+ventanaActual.num).find('#txtDuracionC5').val() !=""){
										 if ($('#drag'+ventanaActual.num).find('#cmbTipoServ5').val() != "0"){
											 if ($('#drag'+ventanaActual.num).find('#cmbConsult5').val() != "0"){
												turno_5 = "5";
												hora51 =  $('#drag'+ventanaActual.num).find('#cmbHoraInicio5').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoInicio5').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmInicio5').val();
												hora52 = $('#drag'+ventanaActual.num).find('#cmbHoraFin5').val()+":"+$('#drag'+ventanaActual.num).find('#cmbMinutoFin5').val()+":"+$('#drag'+ventanaActual.num).find('#cmbAmPmFin5').val();
												pacientes5 = $('#drag'+ventanaActual.num).find('#txtCantidadP5').val();
												duracion5 = $('#drag'+ventanaActual.num).find('#txtDuracionC5').val();
												servicio5 = $('#drag'+ventanaActual.num).find('#cmbTipoServ5').val(); 
												consultorio5 = $('#drag'+ventanaActual.num).find('#cmbConsult5').val();
											 }
										 }
									 }
							    }
							 }
						  }
						}
					 }
			      }
			   }
			      valores_a_mandar=valores_a_mandar+"&nturno=";
					if (turno_1!=0){
					   valores_a_mandar=valores_a_mandar+turno_1+",";
					}
					if (turno_2!=0){
					   valores_a_mandar=valores_a_mandar+turno_2+",";						
					}
					if (turno_3!=0){
					   valores_a_mandar=valores_a_mandar+turno_3+",";
					}
					if (turno_4!=0){
					   valores_a_mandar=valores_a_mandar+turno_4+",";
					}
					if (turno_5!=0){
					   valores_a_mandar=valores_a_mandar+turno_5+",";
					}

				   valores_a_mandar=valores_a_mandar+"&iniciot=";
					if (hora11!=0){
					   valores_a_mandar=valores_a_mandar+hora11+",";
					}
					if (hora21!=0){
					   valores_a_mandar=valores_a_mandar+hora21+",";						
					}
					if (hora31!=0){
					   valores_a_mandar=valores_a_mandar+hora31+",";
					}
					if (hora41!=0){
					   valores_a_mandar=valores_a_mandar+hora41+",";
					}
					if (hora51!=0){
					   valores_a_mandar=valores_a_mandar+hora51+",";
					}
					
					valores_a_mandar=valores_a_mandar+"&fint=";
					
					if (hora12!=0){
					   valores_a_mandar=valores_a_mandar+hora12+",";
					}
					if (hora22!=0){
					   valores_a_mandar=valores_a_mandar+hora22+",";						
					}
					if (hora32!=0){
					   valores_a_mandar=valores_a_mandar+hora32+",";
					}
					if (hora42!=0){
					   valores_a_mandar=valores_a_mandar+hora42+",";
					}
					if (hora52!=0){
					   valores_a_mandar=valores_a_mandar+hora52+",";
					}
										
				   valores_a_mandar=valores_a_mandar+"&pacientest=";
					
					if (pacientes1!=0){
					   valores_a_mandar=valores_a_mandar+pacientes1+",";
					}
					if (pacientes2!=0){
					   valores_a_mandar=valores_a_mandar+pacientes2+",";						
					}
					if (pacientes3!=0){
					   valores_a_mandar=valores_a_mandar+pacientes3+",";
					}
					if (pacientes4!=0){
					   valores_a_mandar=valores_a_mandar+pacientes4+",";
					}
					if (pacientes5!=0){
					   valores_a_mandar=valores_a_mandar+pacientes5+",";
					}
               
					valores_a_mandar=valores_a_mandar+"&duracionc=";
					
					if (duracion1!=0){
					   valores_a_mandar=valores_a_mandar+duracion1+",";
					}
					if (duracion2!=0){
					   valores_a_mandar=valores_a_mandar+duracion2+",";						
					}
					if (duracion3!=0){
					   valores_a_mandar=valores_a_mandar+duracion3+",";
					}
					if (duracion4!=0){
					   valores_a_mandar=valores_a_mandar+duracion4+",";
					}
					if (duracion5!=0){
					   valores_a_mandar=valores_a_mandar+duracion5+",";
					}
					
					valores_a_mandar=valores_a_mandar+"&servicioc=";
					
					if (servicio1!=0){
					   valores_a_mandar=valores_a_mandar+servicio1+",";
					}
					if (servicio2!=0){
					   valores_a_mandar=valores_a_mandar+servicio2+",";						
					}
					if (servicio3!=0){
					   valores_a_mandar=valores_a_mandar+servicio3+",";
					}
					if (servicio4!=0){
					   valores_a_mandar=valores_a_mandar+servicio4+",";
					}
					if (servicio5!=0){
					   valores_a_mandar=valores_a_mandar+servicio5+",";
					}
					valores_a_mandar=valores_a_mandar+"&consultorio=";
					
					if (consultorio1!=0){
					   valores_a_mandar=valores_a_mandar+consultorio1+",";
					}
					if (consultorio2!=0){
					   valores_a_mandar=valores_a_mandar+consultorio2+",";						
					}
					if (consultorio3!=0){
					   valores_a_mandar=valores_a_mandar+consultorio3+",";
					}
					if (consultorio4!=0){
					   valores_a_mandar=valores_a_mandar+consultorio4+",";
					}
					if (consultorio5!=0){
					   valores_a_mandar=valores_a_mandar+consultorio5+",";
					}
					//alert(valores_a_mandar);						   						   
			        ajaxGuardar();	
		break;
		
	  }

	}


/* Mensaje personalizado segun ventana. Cuando es positivo, o verdadero el guardar */
function respuestaGuardarAng(arg, xmlraiz){ 

		switch(arg){
		  case 'tiposIdentificaciones':
	        alert("Tipo Identificaci�n Registrado Exitosamente !");
		  break;
		  case 'tiposEspecialidades':
	        alert("Tipo Especialidad Registrado Exitosamente !");
		  break; 
		  case 'tiposAfiliados':
	        alert("Tipo Afiliado Registrado Exitosamente !");
		  break; 
		  case 'viasIngreso':
	        alert("Via de Ingreso Registrado Exitosamente !");
		  break;
		  case 'centrosCostos':
	        alert("Centro de Costo Registrado Exitosamente !");
		  break;
		  case 'tiposServicios':
	        alert("Tipo de Servicio Registrado Exitosamente !");
		  break;
		  case 'consultorios':
	        alert("Consultorio Registrado Exitosamente !");
		  break;
		  case 'prestadorasSalud':
	        alert("Prestadora Salud Registrado Exitosamente !");
		  break;
		  case 'entidadesAdministradoras':
	        alert("Prestadora Salud Registrado Exitosamente !");
		  break;
		  case 'horariosMedicos':
	        alert("Horario Medico Registrado Exitosamente !");
		  break;
		}		
}


function modificarAng(arg, pag){ // PARA UPDATE
	pagina=pag;
	paginaActual=arg;		 
	switch (arg){
		
		case 'tiposIdentificaciones':     
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxModificar();
		   }						
		break;
		case 'tiposEspecialidades':     
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxModificar();
		   }						
		break;	
		case 'tiposAfiliados':     
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxModificar();
		   }							
		break;	
		case 'viasIngreso':     
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxModificar();
		   }							
		break;
		case 'centrosCostos':     
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxModificar();
		   }							
		break;
		case 'tiposServicios':     
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxModificar();
		   }							
		break;
		case 'consultorios':     
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
			   ajaxModificar();
		   }							
		break;
		case 'prestadorasSalud': 
		 if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNit').val()=="")
			{
				alert("Favor ingresar un Nit  Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNit="+$('#drag'+ventanaActual.num).find('#txtNit').val();
   		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
               cadenabarrio = $('#drag'+ventanaActual.num).find('#cmbMpioRes').val();
			   cadenabarrio = cadenabarrio +"_"+$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val(); 
               valores_a_mandar=valores_a_mandar+"&codBarrio="+cadenabarrio;
		       valores_a_mandar=valores_a_mandar+"&txtDir="+$('#drag'+ventanaActual.num).find('#txtDir').val();
		       valores_a_mandar=valores_a_mandar+"&txtTelefono="+$('#drag'+ventanaActual.num).find('#txtTelefono').val();
			   valores_a_mandar=valores_a_mandar+"&txtFax="+$('#drag'+ventanaActual.num).find('#txtFax').val();
		       valores_a_mandar=valores_a_mandar+"&txtEmail="+$('#drag'+ventanaActual.num).find('#txtEmail').val();
			   valores_a_mandar=valores_a_mandar+"&txtResponsable="+$('#drag'+ventanaActual.num).find('#txtResponsable').val();
		       valores_a_mandar=valores_a_mandar+"&rol="+"P";
			   valores_a_mandar=valores_a_mandar+"&codigoent="+"0";
			   ajaxModificar();	
			}
		break;	
		case 'entidadesAdministradoras':                 
		  if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNit').val()=="")
			{
				alert("Favor ingresar un Nit  Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
		       valores_a_mandar=valores_a_mandar+"&txtNit="+$('#drag'+ventanaActual.num).find('#txtNit').val();
   		       valores_a_mandar=valores_a_mandar+"&txtNombre="+$('#drag'+ventanaActual.num).find('#txtNombre').val();
               cadenabarrio = $('#drag'+ventanaActual.num).find('#cmbMpioRes').val();
			   cadenabarrio = cadenabarrio +"_"+$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val(); 
               valores_a_mandar=valores_a_mandar+"&codBarrio="+cadenabarrio;
		       valores_a_mandar=valores_a_mandar+"&txtDir="+$('#drag'+ventanaActual.num).find('#txtDir').val();
		       valores_a_mandar=valores_a_mandar+"&txtTelefono="+$('#drag'+ventanaActual.num).find('#txtTelefono').val();
			   valores_a_mandar=valores_a_mandar+"&txtFax="+$('#drag'+ventanaActual.num).find('#txtFax').val();
		       valores_a_mandar=valores_a_mandar+"&txtEmail="+$('#drag'+ventanaActual.num).find('#txtEmail').val();
			   valores_a_mandar=valores_a_mandar+"&txtResponsable="+$('#drag'+ventanaActual.num).find('#txtResponsable').val();			   
		       valores_a_mandar=valores_a_mandar+"&rol="+"A";
			   valores_a_mandar=valores_a_mandar+"&codigoent="+$('#drag'+ventanaActual.num).find('#cmbTipoEntidad').val();
			   ajaxModificar();
			}
		break;
		
	}
}

function respuestaModificarAng(arg, xmlraiz){ 

		switch(arg){
		  case 'tiposIdentificaciones':
	        alert("Tipo Id modificado Exitosamente !");
		  break;  
		  case 'tiposEspecialidades':
	        alert("Especialidad modificada Exitosamente !");
		  break; 
		  case 'tiposAfiliados':
	        alert("Tipo Afiliado modificada Exitosamente !");
		  break; 
		  case 'viasIngreso':
	        alert("Via de Ingreso modificada Exitosamente !");
		  break;
		  case 'centrosCostos':
	        alert("Centro de Costo modificado Exitosamente !");
		  break;
		  case 'tiposServicios':
	        alert("Tipo de Servicio modificado Exitosamente !");
		  break;
		  case 'consultorios':
	        alert("Consultorio modificado Exitosamente !");
		  break;
		  case 'prestadorasSalud':
	        alert("Entidad Prestadora de Salud modificada Exitosamente !");
		  break;
		  case 'entidadesAdministradoras':
	        alert("Entidades Administradorasde Salud modificada Exitosamente !");
		  break;
		}		
}



/** funciones para la accion de eliminacion*/
function eliminarAng(arg, pag){ 
	pagina=pag;
	paginaActual=arg;	
	switch (arg){
		
		case 'tiposIdentificaciones':    
			if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{          
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			   ajaxEliminar();
		    }								
		break;	
		case 'tiposEspecialidades':    
			
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			   ajaxEliminar();
		    }								
		break;
		case 'tiposAfiliados':    
			
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			   ajaxEliminar();
		    }								
		break;	
		case 'viasIngreso':    
			
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			   ajaxEliminar();
		    }								
		break;
		case 'centrosCostos':    
			
            if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			   ajaxEliminar();
		    }								
		break;
		case 'tiposServicios':    
			
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			   ajaxEliminar();
		    }								
		break;
		case 'consultorios':    			
           if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val()=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		       valores_a_mandar="accion="+arg;
		       valores_a_mandar=valores_a_mandar+"&txtCodigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			   ajaxEliminar();
		    }								
		break;
		case 'prestadorasSalud':    			          
		 if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNit').val()=="")
			{
				alert("Favor ingresar un Nit  Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val().value=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		         valores_a_mandar="accion="+arg;
				 valores_a_mandar=valores_a_mandar+"&codigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			     ajaxEliminar();		    
			}
		break;
		case 'entidadesAdministradoras':    			          
		    if($('#drag'+ventanaActual.num).find('#txtCodigo').val()=="")
		    {
				alert("Favor ingresar un C�digo Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNit').val()=="")
			{
				alert("Favor ingresar un Nit  Correcto. ");
			}else if($('#drag'+ventanaActual.num).find('#txtNombre').val().value=="")
			{
				alert("Favor ingresar un Nombre  Correcto. ");
			}else{
		         valores_a_mandar="accion="+arg;
				 valores_a_mandar=valores_a_mandar+"&codigo="+$('#drag'+ventanaActual.num).find('#txtCodigo').val();
			     ajaxEliminar();		    
			}
		break;
		case 'horariosMedicos':    			          
		         valores_a_mandar="accion="+arg;
				 valores_a_mandar=valores_a_mandar+"&tipoId="+$('#drag'+ventanaActual.num).find('#lblTipoId').text();
		         valores_a_mandar=valores_a_mandar+"&nId="+$('#drag'+ventanaActual.num).find('#lblId').text();
   		         valores_a_mandar=valores_a_mandar+"&diaT="+$('#drag'+ventanaActual.num).find('#txtFechaTurno').val();
			     ajaxEliminar();		    
		break;
	}	
}
function respuestaEliminarAng(arg, xmlraiz){ 

		switch(arg){
		  case 'tiposIdentificaciones':
	        alert("!Tipo Id Eliminado Exitosamente");
		  break; 
		  case 'tiposEspecialidades':
	        alert("!Especialidad Eliminada Exitosamente");
		  break; 
		  case 'tiposAfiliados':
	        alert("!Tipo Afiliado Eliminada Exitosamente");
		  break; 
		  case 'viasIngreso':
	        alert("!Tipo Afiliado Eliminada Exitosamente");
		  break;
		  case 'centrosCostos':
	        alert("!Centro de Costo Eliminado Exitosamente");
		  break;
		  case 'tiposServicios':
	        alert("!Tipo de Servicio Eliminado Exitosamente");
		  break;
		   case 'consultorios':
	        alert("!Consultorio Eliminado Exitosamente");
		  break;
		  case 'prestadorasSalud':
	        alert("!Entidad Prestadora de Salud Eliminada Exitosamente");
		  break;
		  case 'entidadesAdministradoras':
	        alert("!Entidad Administradora de Salud Eliminada Exitosamente");
		  break;
		  case 'horariosMedicos':
	        alert("!Horario Medico Eliminado Exitosamente");
		  break;
		}		
}


/** funciones para la accion de busqueda*/
function buscarAng(arg, pag){     
	pagina=pag;
       ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width())*92/100;
	 switch (arg){
		 case 'tiposIdentificaciones':  
		 
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();

		
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Descripci�n'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:((ancho*30)/100), sorttype:"int", align:'left'},  
               {name:'descripcion', index:'descripcion',  width:((ancho*70)/100),sorttype:"int", align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:15, 
             rowList:[20,50,100], 
             sortname: '1', 
             sortorder: "asc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: ancho,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));		
              
               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 

         });  alert(pag+' valores_a_mandar= '+ valores_a_mandar);
		  $('#drag'+ventanaActual.num).find("#list").setGridParam({url:valores_a_mandar}).trigger('reloadGrid');		
        break;	
		case 'tiposEspecialidades':  
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
		 		 
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Descripci�n'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'descripcion', index:'descripcion', width:450, align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');		   
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));

               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;	
		case 'tiposAfiliados':  
		
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();		
		 		 
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Descripci�n'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'descripcion', index:'descripcion', width:450, align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');				   
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));		
              
			  //document.getElementById('txtCodigoCentroSalud').disabled=true;// preguntar
               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;		
		case 'viasIngreso':  
		
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
		 		 
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Descripci�n'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'descripcion', index:'descripcion', width:450, align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');			   
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));		
              
			  //document.getElementById('txtCodigoCentroSalud').disabled=true;// preguntar
               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;	
		case 'centrosCostos':  
		
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
		 		 
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Descripci�n'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'descripcion', index:'descripcion', width:450, align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');			   
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));		
              
			  //document.getElementById('txtCodigoCentroSalud').disabled=true;// preguntar
               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;
		case 'tiposServicios':  
		
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
		 		 
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Descripci�n'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'descripcion', index:'descripcion', width:450, align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		       $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');			   
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));		
              
			  //document.getElementById('txtCodigoCentroSalud').disabled=true;// preguntar
               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;	
		case 'consultorios':  		
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
		 		 
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Descripci�n'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'descripcion', index:'descripcion', width:450, align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');			   
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));		              
               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;
		case 'prestadorasSalud':  	
		    var datosubicacion = new Array();
		     valores_a_mandar=pag+"?accion="+ventanaActual.opc;
			 valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
  		     valores_a_mandar=valores_a_mandar+"&txtBusNit="+$('#drag'+ventanaActual.num).find('#txtBusNit').val();
		     valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
			 valores_a_mandar=valores_a_mandar+"&txtRol="+"P";
			 //alert(valores_a_mandar);
			 $('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Nit','Nombre','Barrio','Direccion','Telefono','Fax','Email','Responsable'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'nit', index:'nit', width:150, align:'left'},  
					{name:'nombre', index:'nombre', width:200, align:'left'}, 
					{name:'barrio', index:'barrio', hidden:true}, 
					{name:'direccion', index:'direccion', hidden:true}, 
					{name:'telefono', index:'telefono', hidden:true}, 
					{name:'fax', index:'fax', hidden:true}, 
					{name:'email', index:'email', hidden:true}, 
					{name:'responsable', index:'responsable', hidden:true}, 
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','250px');			   
				   
			   $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo)); 
			   $('#drag'+ventanaActual.num).find('#txtNit').val($.trim(datosDelRegistro.nit));			  
			   $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.nombre));
				datosubicacion = datosDelRegistro.barrio.split('_');
				$('#drag'+ventanaActual.num).find('#cmbPaisRes').val(datosubicacion[0]);
				$('#drag'+ventanaActual.num).find('#cmbDeptoRes').val(datosubicacion[0]+"_"+datosubicacion[1]); 
				$('#drag'+ventanaActual.num).find('#cmbMpioRes').val(datosubicacion[0]+"_"+datosubicacion[1]+"_"+datosubicacion[2]); 
				$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val(datosubicacion[3]); 				
				$('#drag'+ventanaActual.num).find('#txtDir').val($.trim(datosDelRegistro.direccion)); 
			   $('#drag'+ventanaActual.num).find('#txtTelefono').val($.trim(datosDelRegistro.telefono));			  
			   $('#drag'+ventanaActual.num).find('#txtFax').val($.trim(datosDelRegistro.fax));		
			   $('#drag'+ventanaActual.num).find('#txtEmail').val($.trim(datosDelRegistro.email));			  
			   $('#drag'+ventanaActual.num).find('#txtResponsable').val($.trim(datosDelRegistro.responsable));		
            },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;
		  case 'entidadesAdministradoras':  	
		    var datosubicacion = new Array();
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
  		     valores_a_mandar=valores_a_mandar+"&txtBusNit="+$('#drag'+ventanaActual.num).find('#txtBusNit').val();
		     valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
			 valores_a_mandar=valores_a_mandar+"&txtRol="+"A";
			 //alert(valores_a_mandar);
			 $('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Nit','Nombre','Barrio','Direccion','Telefono','Fax','Email','Responsable','TipoEnt'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'nit', index:'nit', width:150, align:'left'},  
					{name:'nombre', index:'nombre', width:200, align:'left'}, 
					{name:'barrio', index:'barrio', hidden:true}, 
					{name:'direccion', index:'direccion', hidden:true}, 
					{name:'telefono', index:'telefono', hidden:true}, 
					{name:'fax', index:'fax', hidden:true}, 
					{name:'email', index:'email', hidden:true}, 
					{name:'responsable', index:'responsable', hidden:true},
					{name:'tipoent', index:'tipoent', hidden:true},
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','250px');				   
				   
			   $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo)); 
			   $('#drag'+ventanaActual.num).find('#txtNit').val($.trim(datosDelRegistro.nit));			  
			   $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.nombre));
				datosubicacion = datosDelRegistro.barrio.split('_');
				$('#drag'+ventanaActual.num).find('#cmbPaisRes').val(datosubicacion[0]);
				$('#drag'+ventanaActual.num).find('#cmbDeptoRes').val(datosubicacion[0]+"_"+datosubicacion[1]); 
				$('#drag'+ventanaActual.num).find('#cmbMpioRes').val(datosubicacion[0]+"_"+datosubicacion[1]+"_"+datosubicacion[2]); 
				$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val(datosubicacion[3]); 				
				$('#drag'+ventanaActual.num).find('#txtDir').val($.trim(datosDelRegistro.direccion)); 
			   $('#drag'+ventanaActual.num).find('#txtTelefono').val($.trim(datosDelRegistro.telefono));			  
			   $('#drag'+ventanaActual.num).find('#txtFax').val($.trim(datosDelRegistro.fax));		
			   $('#drag'+ventanaActual.num).find('#txtEmail').val($.trim(datosDelRegistro.email));			  
			   $('#drag'+ventanaActual.num).find('#txtResponsable').val($.trim(datosDelRegistro.responsable));
				$('#drag'+ventanaActual.num).find('#cmbTipoEntidad').val($.trim(datosDelRegistro.tipoent));
            },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;
		case 'horariosMedicos':  		    
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
			 if ($('#drag'+ventanaActual.num).find('#cmbBusTipoId').val() =="00"){
			   valores_a_mandar=valores_a_mandar+"&BusTipoId="+"";
			 }
			 if ($('#drag'+ventanaActual.num).find('#cmbBusTipoId').val()!="00"){
			   valores_a_mandar=valores_a_mandar+"&BusTipoId="+$('#drag'+ventanaActual.num).find('#cmbBusTipoId').val();
			 }			 			
		    valores_a_mandar=valores_a_mandar+"&BusNumId="+$('#drag'+ventanaActual.num).find('#txtBusIdentificacion').val();
		    valores_a_mandar=valores_a_mandar+"&BusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();
		    valores_a_mandar=valores_a_mandar+"&BusApellido="+$('#drag'+ventanaActual.num).find('#txtBusApellido').val();
			 if ($('#drag'+ventanaActual.num).find('#cmbBusEspecialidad').val()=="00"){
			   valores_a_mandar=valores_a_mandar+"&BusEspecialidad="+"";
			 }
			 if ($('#drag'+ventanaActual.num).find('#cmbBusEspecialidad').val()!="00"){
			 valores_a_mandar=valores_a_mandar+"&BusEspecialidad="+$('#drag'+ventanaActual.num).find('#cmbBusEspecialidad').val();		 		 		
			 }				 
		    $('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['TipoId','Identificacion','Nombre1','Nombre2','Apellido1','Apellido2'], 
             colModel :[  
               {name:'TipoId', index:'TipoId', width:100, sorttype:"int", align:'left'},  
               {name:'Identificacion', index:'Identificacion', width:100, align:'left'},
			   {name:'Nombre1', index:'Nombre1', width:100, sorttype:"int", align:'left'},  
   			   {name:'Nombre2', index:'Nombre2', width:100, sorttype:"int", align:'left'},  
               {name:'Apellido1', index:'Apellido1', width:100, align:'left'},
               {name:'Apellido2', index:'Apellido2', width:100, align:'left'},
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'Nombre1', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','172px');			   
				   
			   $('#drag'+ventanaActual.num).find('#lblTipoId').html($.trim(datosDelRegistro.TipoId));  // coloca los datos LIMPIOS
			   $('#drag'+ventanaActual.num).find('#lblId').html($.trim(datosDelRegistro.Identificacion));  // coloca los datos LIMPIOS
			   $('#drag'+ventanaActual.num).find('#lblNombre').html($.trim(datosDelRegistro.Nombre1));  // coloca los datos LIMPIOS
			   $('#drag'+ventanaActual.num).find('#lblApellido').html($.trim(datosDelRegistro.Apellido1));  // coloca los datos LIMPIOS			   
				
				LlenarServicios($.trim(datosDelRegistro.TipoId),$.trim(datosDelRegistro.Identificacion),'/clinica/paginas/accionesXml/cargarServiciosRol_xml.jsp');				
			   },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;
		  case 'registroCitologia': 		  
		     valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		     valores_a_mandar=valores_a_mandar+"&BusTipoId="+$('#drag'+ventanaActual.num).find('#cmbBusTipoId').val();
		     valores_a_mandar=valores_a_mandar+"&BusIdPersona="+$('#drag'+ventanaActual.num).find('#txtBusIdPersona').val();
		     valores_a_mandar=valores_a_mandar+"&BusPrimNombre="+$('#drag'+ventanaActual.num).find('#txtBusPrimNombre').val();
		     valores_a_mandar=valores_a_mandar+"&BusSegunNombre="+$('#drag'+ventanaActual.num).find('#txtBusSegunNombre').val();
		     valores_a_mandar=valores_a_mandar+"&BusPrimApell="+$('#drag'+ventanaActual.num).find('#txtBusPrimApell').val();
		     valores_a_mandar=valores_a_mandar+"&BusSegunApell="+$('#drag'+ventanaActual.num).find('#txtBusSegunApell').val();	
		     valores_a_mandar=valores_a_mandar+"&BusEstadoCita="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoCita').val();
		     valores_a_mandar=valores_a_mandar+"&BusFechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();	
		     valores_a_mandar=valores_a_mandar+"&BusFechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();		
		     valores_a_mandar=valores_a_mandar+"&tipoServicio=6";		  		  		  			
            $('#drag'+ventanaActual.num).find('#list').jqGrid({         
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['Tipo Id','Identificaci�n','Nombre', 'Estado de Cita','Fecha Cita','Hora Cita' ,'horaAtencion' ,'Sexo','FechaNac','Edad','GrupoSan','TipoUsu',
					    'EntidadAdminst','PaisRes', 'DeptoRes' ,'MpioRes','BarrioRes','ZonaResidencia','MpioNac','Procedimiento',
                        'Ocupacion','EstadoCivil', 'GrupoEsp','AMotivo',
					 ], 
             colModel :[  
               {name:'tipId', index:'1', width:30, sorttype:"int", align:'left'}, 
               {name:'idPersona', index:'2', width:50, sorttype:"int", align:'left'},  
               {name:'nombre', index:'3', width:155, align:'left'},  
               {name:'estadoCita', index:'4', width:100, align:'left'},  	
               {name:'fechaCita', index:'5', width:50, align:'left'},  	
               {name:'horaCita', index:'6', width:75, align:'left'},  				   
			   {name:'horaAtencion', index:'horaAtencion', hidden:true},   
               {name:'Sexo', index:'Sexo', hidden:true},   
			   {name:'FechaNac', index:'FechaNac', hidden:true },  
			   {name:'Edad', index:'Edad', hidden:true },  
			   {name:'GrupoSan', index:'GrupoSan', hidden:true }, 			   
			   {name:'TipoUsu', index:'TipoUsu', hidden:true},
			   {name:'EntidadAdminst', index:'EntidadAdminst', hidden:true},
			   {name:'PaisRes', index:'PaisRes', hidden:true},
			   {name:'DeptoRes', index:'DeptoRes', hidden:true},			   
               {name:'MpioRes', index:'MpioRes', hidden:true }, 
   			   {name:'BarrioRes', index:'BarrioRes', hidden:true},
			   {name:'ZonaResidencia', index:'ZonaResidencia', hidden:true},
			   {name:'MpioNac', index:'MpioNac', hidden:true },			   
			   {name:'Procedimiento', index:'Procedimiento', hidden:true},		
			   {name:'Ocupacion', index:'Ocupacion', hidden:true},  
			   {name:'EstadoCivil', index:'EstadoCivil', hidden:true},
			   {name:'GrupoEsp', index:'GrupoEsp', hidden:true},			   
			   {name:'AMotivo', index:'AMotivo', hidden:true}, 			   

             ], 
             pager: $('#drag'+ventanaActual.num).find('#pager'), 
             rowNum:20, 
             rowList:[20,50,100], 
             sortname: '4', 
             sortorder: "asc", 
             viewrecords: true, 
			 hidegrid: false, 
			 width: 500,			 
			 height: 300, 			 
			 
			 ondblClickRow: function( rowid) {  limpiarDivEditarJuan(arg);
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
     	      $('#drag'+ventanaActual.num).find('#divContenido').css('height','550px');		
			   $('#drag'+ventanaActual.num).find('#tipoIdPacienteCita').val($.trim(datosDelRegistro.tipId));	
			   $('#drag'+ventanaActual.num).find('#idPacienteCita').val($.trim(datosDelRegistro.idPersona));				   
              
			  cargarDepartamentosIns(datosDelRegistro.DeptoRes, 'cmbDeptoRes', datosDelRegistro.PaisRes,'/clinica/paginas/accionesXml/cargarDeptos_xml.jsp');
			  cargarBarriosIns( datosDelRegistro.BarrioRes, 'cmbBarrioRes', datosDelRegistro.MpioRes, '/clinica/paginas/accionesXml/cargarBarrios_xml.jsp' );
			  cargarCiudadesIns(datosDelRegistro.MpioRes, 'cmbMpioRes', datosDelRegistro.DeptoRes,'/clinica/paginas/accionesXml/cargarMunicipios_xml.jsp'  );
              $("#cmbZonaResidencia option[value='"+$.trim(datosDelRegistro.ZonaResidencia)+"']").attr('selected', 'selected');	              

			  $('#drag'+ventanaActual.num).find('#lblHistoria').append($.trim(datosDelRegistro.tipId)+$.trim(datosDelRegistro.idPersona)); 
			  $('#drag'+ventanaActual.num).find('#lblNombre').append($.trim(datosDelRegistro.nombre));			  
  			  
			  $('#drag'+ventanaActual.num).find('#lblId').append($.trim(datosDelRegistro.tipId)+" No " +$.trim(datosDelRegistro.idPersona));
			  $('#drag'+ventanaActual.num).find('#lblHoraAt').append($.trim(datosDelRegistro.horaAtencion)); 
			  
  			  $('#drag'+ventanaActual.num).find('#lblSexo').append($.trim(datosDelRegistro.Sexo)); 	
              $('#drag'+ventanaActual.num).find('#lblFechaNac').append($.trim(datosDelRegistro.FechaNac)); 
  			  $('#drag'+ventanaActual.num).find('#lblEdad').append($.trim(datosDelRegistro.Edad)); 
  			  
			  
			  $('#drag'+ventanaActual.num).find('#lblGrupoSan').append(tipoSangre( $.trim(datosDelRegistro.GrupoSan) ));
			  
              $('#drag'+ventanaActual.num).find('#lblTipoUsu').append($.trim(datosDelRegistro.TipoUsu)); 
			  $('#drag'+ventanaActual.num).find('#lblAdministradora').append($.trim(datosDelRegistro.EntidadAdminst)); 
 		      $('#drag'+ventanaActual.num).find('#lblMunicipioProced').append($.trim(datosDelRegistro.MunicipioProced)); 
			  $('#drag'+ventanaActual.num).find('#lblLugNac').append($.trim(datosDelRegistro.MpioNac)); 			  
		      $('#drag'+ventanaActual.num).find('#lblProcedimiento').append($.trim(datosDelRegistro.Procedimiento)); 	
			  
			  $('#drag'+ventanaActual.num).find('#lblGrupoEsp').append($.trim(datosDelRegistro.GrupoEsp));			  
			  
			  $("#cmbOcupacion option[value='"+$.trim(datosDelRegistro.Ocupacion)+"']").attr('selected', 'selected');
			  $("#cmbEstadoCivil option[value='"+$.trim(datosDelRegistro.EstadoCivil)+"']").attr('selected', 'selected');	

			  $('#drag'+ventanaActual.num).find('#txtAMotivo').val($.trim(datosDelRegistro.AMotivo));		
              $('#drag'+ventanaActual.num).find('#txtAEnfermedadAct').val($.trim(datosDelRegistro.AEnfermedadAct));			  

			  $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divBuscar').hide();
              },
			 //height: 210, 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         }); 

        break;		 
		  
		   case 'registroMaterno':  	
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&cmbBusTipoId="+$('#drag'+ventanaActual.num).find('#cmbBusTipoId').val(); 
			 valores_a_mandar=valores_a_mandar+"&txtBusIdPersona="+$('#drag'+ventanaActual.num).find('#txtBusIdPersona').val(); 
		    valores_a_mandar=valores_a_mandar+"&txtBusPrimNombre="+$('#drag'+ventanaActual.num).find('#txtBusPrimNombre').val();			 
 		    valores_a_mandar=valores_a_mandar+"&txtBusSegunNombre="+$('#drag'+ventanaActual.num).find('#txtBusSegunNombre').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusPrimApell="+$('#drag'+ventanaActual.num).find('#txtBusPrimApell').val();
 		    valores_a_mandar=valores_a_mandar+"&txtBusSegunApell="+$('#drag'+ventanaActual.num).find('#txtBusSegunApell').val();			 
			 valores_a_mandar=valores_a_mandar+"&txtBusFechaDesde="+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusFechaHasta="+$('#drag'+ventanaActual.num).find('#txtBusFechaHasta').val();
 		    valores_a_mandar=valores_a_mandar+"&cmbBusEstadoCita="+$('#drag'+ventanaActual.num).find('#cmbBusEstadoCita').val();			 
			 //alert(valores_a_mandar);
			 $('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['Tipo Id','Identificacion','Nombre y Apellidos ','Estado Cita','Fecha Cita','Hora Cita'], 
             colModel :[  
               {name:'tipoid', index:'tipoid', width:100, sorttype:"int", align:'left'},  
               {name:'identificacion', index:'identificacion', width:150, align:'left'},  
					{name:'nombres', index:'nombres', width:200, align:'left'}, 
					{name:'estadocita', index:'estadocita', width:100, align:'left'}, 
					{name:'fechacita', index:'fechacita', width:100, align:'left'}, 
					{name:'horacita', index:'horacita', width:100, align:'left'}, 
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'tipoid', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  
		      
				$('#drag'+ventanaActual.num).find('#divBuscar').hide();
	         $('#drag'+ventanaActual.num).find('#divEditar').show();
			   $('#drag'+ventanaActual.num).find('#divContenido').css('height','250px');				   
				
			   $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo)); 
			   $('#drag'+ventanaActual.num).find('#txtNit').val($.trim(datosDelRegistro.nit));			  
			   $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.nombre));
				datosubicacion = datosDelRegistro.barrio.split('_');
				$('#drag'+ventanaActual.num).find('#cmbPaisRes').val(datosubicacion[0]);
				$('#drag'+ventanaActual.num).find('#cmbDeptoRes').val(datosubicacion[0]+"_"+datosubicacion[1]); 
				$('#drag'+ventanaActual.num).find('#cmbMpioRes').val(datosubicacion[0]+"_"+datosubicacion[1]+"_"+datosubicacion[2]); 
				$('#drag'+ventanaActual.num).find('#cmbBarrioRes').val(datosubicacion[3]); 				
				$('#drag'+ventanaActual.num).find('#txtDir').val($.trim(datosDelRegistro.direccion)); 
			   $('#drag'+ventanaActual.num).find('#txtTelefono').val($.trim(datosDelRegistro.telefono));			  
			   $('#drag'+ventanaActual.num).find('#txtFax').val($.trim(datosDelRegistro.fax));		
			   $('#drag'+ventanaActual.num).find('#txtEmail').val($.trim(datosDelRegistro.email));			  
			   $('#drag'+ventanaActual.num).find('#txtResponsable').val($.trim(datosDelRegistro.responsable));
				$('#drag'+ventanaActual.num).find('#cmbTipoEntidad').val($.trim(datosDelRegistro.tipoent));
            },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;	
		  
		case 'medicamentos':  		
		    valores_a_mandar=pag+"?accion="+ventanaActual.opc;
		    valores_a_mandar=valores_a_mandar+"&txtBusCodigo="+$('#drag'+ventanaActual.num).find('#txtBusCodigo').val();
		    valores_a_mandar=valores_a_mandar+"&txtBusNombre="+$('#drag'+ventanaActual.num).find('#txtBusNombre').val();		 		 
		$('#drag'+ventanaActual.num).find('#list').jqGrid({ 
             url:valores_a_mandar, 
             datatype: 'xml', 
             mtype: 'GET', 
             colNames:['C�digo','Medicamento','Unidad Medida','Cantidad','Posologia'], 
             colModel :[  
               {name:'codigo', index:'codigo', width:150, sorttype:"int", align:'left'},  
               {name:'medicamento', index:'medicamento', width:450, align:'left'},
			   {name:'unidad_medida', index:'unidad_medida', width:150, sorttype:"int", align:'left'},  
               {name:'cantidad', index:'cantidad', width:150, align:'left'}, 
			   {name:'posologia', index:'posologia', width:150, sorttype:"int", align:'left'},  
                ], 
             pager: jQuery('#pager'), 
             rowNum:10, 
             rowList:[20,50,100], 
             sortname: 'codigo', 
             sortorder: "desc", 
             viewrecords: true, 
			 hidegrid: false, 
			 height: 250, 
			 width: 600,
			 
			 ondblClickRow: function( rowid) {
				 
  		     var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #list").getRowData(rowid);   //trae los datos de esta fila con este idrow  

		      $('#drag'+ventanaActual.num).find('#divBuscar').hide();
	          $('#drag'+ventanaActual.num).find('#divEditar').show();
			  $('#drag'+ventanaActual.num).find('#divContenido').css('height','150px');			   
				   
			  $('#drag'+ventanaActual.num).find('#txtCodigo').val($.trim(datosDelRegistro.codigo));  // coloca los datos LIMPIOS
			  $('#drag'+ventanaActual.num).find('#txtNombre').val($.trim(datosDelRegistro.descripcion));		
               },
			 
             imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
             caption: 'Resultados' 
         });  
        break;	
	 }
}


function respuestaBuscarAng(){  
	if(varajax.readyState == 4 ){
		VentanaModal.cerrar();
			if(varajax.status == 200){
			  raiz=varajax.responseXML.documentElement;
			  posicionActual=0;///posicion de elementos de la busqueda
			  switch(paginaActual){
				  
				   /* case 'paginaNotificaciones': 
					 document.getElementById('mensajesNuevos').innerHTML = "Tiene 3 Mensajes Nuevos";				  
				  
				    break; */
					
					case 'paginaCargarManifiestos': 
					break;		

			  }
		 }
	}
}

function cargarFormulariosBusquedaAng(){
		 var filase=0;
		 switch(paginaActual){
			      case 'opcion1':				      
				   break;  
		}
}



var contcitasmaterno=0;
function agregarDatosCitasMaterno(contenido,idtabla){
		 tablabody=document.getElementById(idtabla).lastChild;
		  
		  Nregistro=document.createElement("TR");
		  var posi=contcitasmaterno+1;
		  Nregistro.id='fila_'+posi;
		  Nregistro.className='label_left'+'1';
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_1';
		  Ncampo.innerHTML=contenido[0].value;
		  Ncampo.headers=contenido[0].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_2';
		  Ncampo.innerHTML=contenido[1].value;
		  Ncampo.headers=contenido[1].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_3';
		  Ncampo.innerHTML=contenido[2].value;
		  Ncampo.headers=contenido[2].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_4';
		  Ncampo.innerHTML=contenido[3].value;
		  Ncampo.headers=contenido[3].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_5';
		  Ncampo.innerHTML=contenido[4].value;
		  Ncampo.headers=contenido[4].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_6';
		  Ncampo.innerHTML=contenido[5].value;
		  Ncampo.headers=contenido[5].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_7';
		  Ncampo.innerHTML=contenido[6].value;
		  Ncampo.headers=contenido[6].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_8';
		  Ncampo.innerHTML=contenido[7].value;
		  Ncampo.headers=contenido[7].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_9';
		  Ncampo.innerHTML=contenido[8].value;
		  Ncampo.headers=contenido[8].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_10';
		  Ncampo.innerHTML=contenido[9].value;
		  Ncampo.headers=contenido[9].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_11';
		  Ncampo.innerHTML=contenido[10].value;
		  Ncampo.headers=contenido[10].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campo_'+posi+'_12';
		  Ncampo.innerHTML=contenido[11].value;
		  Ncampo.headers=contenido[11].value;
		  Nregistro.appendChild(Ncampo);

		  
		  Ncampo=document.createElement("TD");
		  Ncampo.align="center";
		  Ncampo.width="5%";
	      Ncampo1=document.createElement("IMG");
		  Ncampo1.setAttribute('src','/academia/utilidades/imagenes/acciones/edit_remove.png');
		  Ncampo1.setAttribute('width','20');
		  Ncampo1.setAttribute('height','20');
		  Ncampo1.setAttribute('align','middle');
		  Ncampo1.setAttribute('onclick',"eliminarc("+posi+",1);");
		  Ncampo.appendChild(Ncampo1);
		  Nregistro.appendChild(Ncampo);
	      tablabody.appendChild(Nregistro);
	      contcitasmaterno++;	

	}


var contpuerperiomaterno=0;
function agregarDatosPuerperioMaterno(contenido,idtabla){
		 tablabody=document.getElementById(idtabla).lastChild;
		  
		  Nregistro=document.createElement("TR");
		  var posi=contpuerperiomaterno+1;
		  Nregistro.id='filap_'+posi;
		  Nregistro.className='label_left'+'1';
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campop_'+posi+'_1';
		  Ncampo.innerHTML=contenido[0].value;
		  Ncampo.headers=contenido[0].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campop_'+posi+'_2';
		  Ncampo.innerHTML=contenido[1].value;
		  Ncampo.headers=contenido[1].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campop_'+posi+'_3';
		  Ncampo.innerHTML=contenido[2].value;
		  Ncampo.headers=contenido[2].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campop_'+posi+'_4';
		  Ncampo.innerHTML=contenido[3].value;
		  Ncampo.headers=contenido[3].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campop_'+posi+'_5';
		  Ncampo.innerHTML=contenido[4].value;
		  Ncampo.headers=contenido[4].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campop_'+posi+'_6';
		  Ncampo.innerHTML=contenido[5].value;
		  Ncampo.headers=contenido[5].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campop_'+posi+'_7';
		  Ncampo.innerHTML=contenido[6].value;
		  Ncampo.headers=contenido[6].value;
		  Nregistro.appendChild(Ncampo);
		  
		  Ncampo=document.createElement("TD");
		  Ncampo.align="center";
		  Ncampo.width="5%";
	      Ncampo1=document.createElement("IMG");
		  Ncampo1.setAttribute('src','/academia/utilidades/imagenes/acciones/edit_remove.png');
		  Ncampo1.setAttribute('width','20');
		  Ncampo1.setAttribute('height','20');
		  Ncampo1.setAttribute('align','middle');
		  Ncampo1.setAttribute('onclick',"eliminarc("+posi+",2);");
		  Ncampo.appendChild(Ncampo1);
		  Nregistro.appendChild(Ncampo);
	      tablabody.appendChild(Nregistro);
	      contpuerperiomaterno++;	

	}



/*function agregarDatosControlesCreDes(contenido,idtabla){
		 tablabody=document.getElementById(idtabla).lastChild;
		  
		  Nregistro=document.createElement("TR");
		  var posi=contcontrolcrecidesarrollo+1;
		  Nregistro.id='filaccd_'+posi;
		  Nregistro.className='label_left'+'1';
		  

		  for (j=1; j<37; j++){
		  Ncampo=document.createElement("TD");
		  Ncampo.id='campoccd_'+posi+'_'+j;
		  Ncampo.innerHTML=contenido[j-1].value;
		  Ncampo.headers=contenido[j-1].value;
		  Nregistro.appendChild(Ncampo);
		  }
		  		  		  
		  Ncampo=document.createElement("TD");
		  Ncampo.align="center";
		  Ncampo.width="5%";
	      Ncampo1=document.createElement("IMG");
		  Ncampo1.setAttribute('src','/academia/utilidades/imagenes/acciones/edit_remove.png');
		  Ncampo1.setAttribute('width','20');
		  Ncampo1.setAttribute('height','20');
		  Ncampo1.setAttribute('align','middle');
		  Ncampo1.setAttribute('onclick',"eliminarc("+posi+",3);");
		  Ncampo.appendChild(Ncampo1);
		  Nregistro.appendChild(Ncampo);
	      tablabody.appendChild(Nregistro);
	      contcontrolcrecidesarrollo++;	
	}*/
function eliminarc(indc,cual){  
  if(cual==1){
	var cadb="fila_"+indc;  
  	document.getElementById('tablacitasmaterno').lastChild.removeChild(document.getElementById(cadb));
  }
  if(cual==2){
	var cadb="filap_"+indc;  
  	document.getElementById('tablapuerperio').lastChild.removeChild(document.getElementById(cadb));
  }
  if(cual==3){
	var cadb="filaccd_"+indc;  
  	document.getElementById('tabladatosbasicoscredesa').lastChild.removeChild(document.getElementById(cadb));
  }
}

function marcardatosentabla()
{
  valores_1 = new Array();	
  valores_2 = new Array();	
  valores_3 = new Array();	
  valores_4 = new Array();	
  var valor=0;
//for(i=1; i<5; i++){
//   for(var j=1;j<4;j++){
//		 cad1='txt_'+i+'_'+j;
//		 if(document.getElementById(cad1)!=null){
//			 valor=valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad1).val());
//		 }
//	}	
//	//alert("valor"+valor);
//	for(t=1; t<5; t++){
//	  var ban1=0;
//	  var ban2=0;
//	  idpos='lbl_1_3_'+i+'_'+t;
//	  valortabla=$('#drag'+ventanaActual.num).find('#'+idpos).text().split('-');
//	  if(valor>=valortabla[0]){ban1=1}	  
//	  if(valor<=valortabla[1]){ban2=1}
//	  if(ban1==1 && ban2==1){
//		  //alert("entra"+idpos);
//		  $('#drag'+ventanaActual.num).find('#'+idpos).css('background-color','#CCF');
//		  t=5;
//	  }
//	}
//	valor=0;
//  }
  i=1;
  y=1;
  while(i<40){
    for(j=1; j<5; j++){
	 var valor = 0;	
	   if(j==1){		   
		  cad1='txt_'+j+'_'+i;
		  p=i+1;
		  cad2='txt_'+j+'_'+p;
		  p=i+2;
		  cad3='txt_'+j+'_'+p;
		  if(document.getElementById(cad1)!=null){
			  valor=valor+parseInt($('#drag'+ventanaActual.num).find('#'+cad1).val());
		  }
		  if(document.getElementById(cad2)!=null){ 
		    valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad2).val());
		  }
		  if(document.getElementById(cad3)!=null){
			valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad3).val());		  
		  }
		  valores_1[y]=valor;
	   }
	   if(j==2){
		  cad1='txt_'+j+'_'+i;
		  p=i+1;
		  cad2='txt_'+j+'_'+p;
		  p=i+2;
		  cad3='txt_'+j+'_'+p;
		 if(document.getElementById(cad1)!=null){
			  valor=valor+parseInt($('#drag'+ventanaActual.num).find('#'+cad1).val());
		  }
		  if(document.getElementById(cad2)!=null){ 
		    valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad2).val());
		  }
		  if(document.getElementById(cad3)!=null){
			valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad3).val());		  
		  }
		  valores_2[y]=valor;
	   }

	   if(j==3){
		  cad1='txt_'+j+'_'+i;
		  p=i+1;
		  cad2='txt_'+j+'_'+p;
		  p=i+2;
		  cad3='txt_'+j+'_'+p;
		 if(document.getElementById(cad1)!=null){
			  valor=valor+parseInt($('#drag'+ventanaActual.num).find('#'+cad1).val());
		  }
		  if(document.getElementById(cad2)!=null){ 
		    valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad2).val());
		  }
		  if(document.getElementById(cad3)!=null){
			valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad3).val());		  
		  }
		  valores_3[y]=valor;
	   }

	   if(j==4){
		  cad1='txt_'+j+'_'+i;
		  p=i+1;
		  cad2='txt_'+j+'_'+p;
		  p=i+2;
		  cad3='txt_'+j+'_'+p;
		  if(document.getElementById(cad1)!=null){
			  valor=valor+parseInt($('#drag'+ventanaActual.num).find('#'+cad1).val());
		  }
		  if(document.getElementById(cad2)!=null){ 
		    valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad2).val());
		  }
		  if(document.getElementById(cad3)!=null){
			valor = valor + parseInt($('#drag'+ventanaActual.num).find('#'+cad3).val());		  
		  }
		  valores_4[y]=valor;
	   }
	}	  
    i=i+3;	  
	y=y+1;
  }
  var ban1=0;
  var ban2=0;
  var ant=0;
 // alert("1:  "+valores_1.length+valores_1+"   2:  "+valores_2+"   3:  "+valores_3+"    4:  "+valores_4);
  for(s=1; s<13; s++){
	  for(f=1; f<5; f++){
		 ban1=0;
		 ban2=0;
	     idpos='lbl_A_'+s+'_'+f;	  
		 dato=$('#drag'+ventanaActual.num).find('#'+idpos).text().split('-');
		 valor =  valores_1[s] + ant;
		  if(valor>=dato[0]){ban1=1}	  
		  if(valor<=dato[1]){ban2=1}
		  if(ban1==1 && ban2==1){
			  $('#drag'+ventanaActual.num).find('#'+idpos).css('background-color','#CCF');
			  ant=valor;
			  f=5;
		  }		  
	  }	  
  }
  ant=0;
  for(s=1; s<14; s++){
	  for(f=1; f<5; f++){
		 ban1=0;
		 ban2=0;
	     idpos='lbl_B_'+s+'_'+f;	  
		 dato=$('#drag'+ventanaActual.num).find('#'+idpos).text().split('-');
		 valor =  valores_1[s] + ant;
		  if(valor>=dato[0]){ban1=1}	  
		  if(valor<=dato[1]){ban2=1}
		  if(ban1==1 && ban2==1){
			  $('#drag'+ventanaActual.num).find('#'+idpos).css('background-color','#CCF');
			  ant=valor;
			  f=5;
		  }		  
	  }	  
  }
  ant=0;
  for(s=1; s<14; s++){
	  for(f=1; f<5; f++){
		 ban1=0;
		 ban2=0;
	     idpos='lbl_C_'+s+'_'+f;	  
		 dato=$('#drag'+ventanaActual.num).find('#'+idpos).text().split('-');
		 valor =  valores_1[s] + ant;
		  if(valor>=dato[0]){ban1=1}	  
		  if(valor<=dato[1]){ban2=1}
		  if(ban1==1 && ban2==1){
			  $('#drag'+ventanaActual.num).find('#'+idpos).css('background-color','#CCF');
			  ant=valor;
			  f=5;
		  }		  
	  }	  
  }
  ant=0;
  for(s=1; s<14; s++){
	  for(f=1; f<5; f++){
		 ban1=0;
		 ban2=0;
	     idpos='lbl_D_'+s+'_'+f;	  
		 dato=$('#drag'+ventanaActual.num).find('#'+idpos).text().split('-');
		 valor =  valores_1[s] + ant;
		  if(valor>=dato[0]){ban1=1}	  
		  if(valor<=dato[1]){ban2=1}
		  if(ban1==1 && ban2==1){
			  $('#drag'+ventanaActual.num).find('#'+idpos).css('background-color','#CCF');
			  ant=valor;
			  f=5;
		  }		  
	  }	  
  }
 
 ant=0;
  for(s=1; s<14; s++){
	  for(f=1; f<5; f++){
		 ban1=0;
		 ban2=0;
	     idpos='lbl_T_'+s+'_'+f;	  
		 dato=$('#drag'+ventanaActual.num).find('#'+idpos).text().split('-');
		 valor =  valores_1[s] +  valores_2[s] +  valores_3[s]+ + valores_4[s]+ant;
		  if(valor>=dato[0]){ban1=1}	  
		  if(valor<=dato[1]){ban2=1}
		  if(ban1==1 && ban2==1){
			  $('#drag'+ventanaActual.num).find('#'+idpos).css('background-color','#CCF');
			  ant=valor;
			  f=5;
		  }		  
	  }	  
  }
 
}


function marcarPuntosGraficos(){
	
	var edadn = new Array();
	var peson = new Array();
	var tallan = new Array();
	cont_edadn=0;
	cont_peson=0;
	cont_tallan=0;
	cuerpoMap=document.getElementById('graficaninas'); 
	top=cuerpoMap.style.top.substr(0,cuerpoMap.style.top.length-2);   //al top se le quita el px : 111px queda 111  !!!!!!!!!!!!!aqui angelica borrar
    left=cuerpoMap.style.left.substr(0,cuerpoMap.style.top.length-2);
    topY=parseInt(top); 		
	leftX=  parseInt(left); 
	//alert("y"+topY+"x"+leftX);
	/*xinicial=94;
	yinicial=814;
	leftX=94;
	topY=814;
	cuerpoDiv=document.getElementById('contenedor_de_puntos');
	Nregistro=document.createElement("DIV");
	Nregistro.className='bolita  posibolita';
	Nregistro.style.position='absolute';
	Nregistro.style.top=topY+'px';
	Nregistro.style.left=leftX+'px';
	cuerpoDiv.appendChild(Nregistro);	
	
	xfinal=643;
	yfinal=58;
	leftX=643;	
	topY=58;	
	cuerpoDiv=document.getElementById('contenedor_de_puntos');
	Nregistro=document.createElement("DIV");
	Nregistro.className='bolita  posibolita';
	Nregistro.style.position='absolute';
	Nregistro.style.top=topY+'px';
	Nregistro.style.left=leftX+'px';
	cuerpoDiv.appendChild(Nregistro);*/	
	var cantidad =jQuery('#listControles').getGridParam('records');  
	//alert("cantidad "+cantidad);
	var ids = jQuery("#listControles").getDataIDs();
	cant = ids.length;
	for(var i=0;i<ids.length;i++){ 
	var c = ids[i];
	var datosDelRegistro = jQuery("#listControles").getRowData(c);  
    edadn[cont_edadn]=$.trim(datosDelRegistro.Edad);  
	cont_edadn++;
	peson[cont_peson]=$.trim(datosDelRegistro.Peso);
	cont_peson++;
	tallan[cont_tallan]=$.trim(datosDelRegistro.Talla);
	cont_tallan++;
	}
	
	//alert("edadn"+edadn+"peson"+peson+"tallan"+tallan);				
	for(var s=0; s<cont_edadn; s++){
		 if(edadn[s]>=0 && edadn[s]<=1){
		   pos_x=94 + (edadn[s] * 16);
		 }
		if(edadn[s]>1 && edadn[s]<=2){
		   pos_x=94 + (edadn[s] * 16);
		 }
		if(edadn[s]>2 && edadn[s]<=3){
		   pos_x=94 + (edadn[s] * 16);
		 }
		if(edadn[s]>3 && edadn[s]<=4){
		   pos_x=94 + (edadn[s] * 15);
		 }
		if(edadn[s]>4&& edadn[s]<=5){
		   pos_x=94 + (edadn[s] * 15);
		 }
		if(edadn[s]>5 && edadn[s]<=6){
		   pos_x=94 + (edadn[s] * 15);
		 }
		if(edadn[s]>6 && edadn[s]<=7){
		   pos_x=94 + (edadn[s] * 15);
		 }
		if(edadn[s]>7 && edadn[s]<=8){
		   pos_x=94 + (edadn[s] * 15);
		 }
		if(edadn[s]>8 && edadn[s]<=9){
		   pos_x=94 + (edadn[s] * 15);
		 }
		if(edadn[s]>9 && edadn[s]<=10){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>10 && edadn[s]<=11){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		 if(edadn[s]>11 && edadn[s]<=12){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>12 && edadn[s]<=13){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>13 && edadn[s]<=14){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>14 && edadn[s]<=15){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>15 && edadn[s]<=16){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>16 && edadn[s]<=17){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>17 && edadn[s]<=18){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>18 && edadn[s]<=19){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>19 && edadn[s]<=20){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>20 && edadn[s]<=21){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>21 && edadn[s]<=22){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>22 && edadn[s]<=23){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>23 && edadn[s]<=24){
		   pos_x=94 + (edadn[s] * 15.2);
		 }
		if(edadn[s]>24 && edadn[s]<=25){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>25 && edadn[s]<=26){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>26 && edadn[s]<=27){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>27 && edadn[s]<=28){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>28 && edadn[s]<=29){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>29 && edadn[s]<=30){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>30 && edadn[s]<=31){
		   pos_x=94 + (edadn[s] * 15.3);
		 }
		if(edadn[s]>31 && edadn[s]<=32){
		   pos_x=94 + (edadn[s] * 15.2);
		 }
		if(edadn[s]>32 && edadn[s]<=33){
		   pos_x=94 + (edadn[s] * 15.2);
		 }
		if(edadn[s]>33 && edadn[s]<=34){
		   pos_x=94 + (edadn[s] * 15.2);
		 }
		if(edadn[s]>34 && edadn[s]<=35){
		   pos_x=94 + (edadn[s] * 15.2);
		 }
		if(edadn[s]>35 && edadn[s]<=36){
		   pos_x=94 + (edadn[s] * 15.2);
		 }
		
		
		//valpeso = peson[s]/1000; 
		valpeso = peson[s]; 
		if(valpeso>0 && valpeso<=1.4){
		  pos_y=813;
		 }
		if(valpeso>1.4 && valpeso<=1.6){
		  pos_y=806;
		 }
		if(valpeso>1.6 && valpeso<=1.8){
		  pos_y=799;
		 }
		if(valpeso>1.8 && valpeso<=2){
		  pos_y=790;
		 }
		if(valpeso>2 && valpeso<=2.2){
		  pos_y=784;
		 }
		if(valpeso>2.2 && valpeso<=2.4){
		  pos_y=776;
		 }
		if(valpeso>2.4 && valpeso<=2.6){
		  pos_y=769;
		 }
		if(valpeso>2.6 && valpeso<=2.8){
		  pos_y=762;
		 }
		if(valpeso>2.8 && valpeso<=3){
		  pos_y=756;
		 }
		 
		 if(valpeso>3 && valpeso<=3.2){
		  pos_y=746;
		 }
		if(valpeso>3.2 && valpeso<=3.4){
		  pos_y=740;
		 }
		if(valpeso>3.4 && valpeso<=3.6){
		  pos_y=732;
		 }
		if(valpeso>3.6 && valpeso<=3.8){
		  pos_y=727;
		 }
		if(valpeso>3.8 && valpeso<=4){
		  pos_y=721;
		 }
		 
		 if(valpeso>4 && valpeso<=4.2){
		  pos_y=714;
		 }
		if(valpeso>4.2 && valpeso<=4.4){
		  pos_y=706;
		 }
		if(valpeso>4.4 && valpeso<=4.6){
		  pos_y=699;
		 }
		if(valpeso>4.6 && valpeso<=4.8){
		  pos_y=693;
		 }
		if(valpeso>4.8 && valpeso<=5){
		  pos_y=685;
		 }
		 
		 if(valpeso>5 && valpeso<=5.2){
		  pos_y=676;
		 }
		if(valpeso>5.2 && valpeso<=5.4){
		  pos_y=669;
		 }
		if(valpeso>5.4 && valpeso<=5.6){
		  pos_y=663;
		 }
		if(valpeso>5.6 && valpeso<=5.8){
		  pos_y=657;
		 }
		if(valpeso>5.8 && valpeso<=6){
		  pos_y=648;
		 }
		 
		 if(valpeso>6 && valpeso<=6.2){
		  pos_y=640;
		 }
		if(valpeso>6.2 && valpeso<=6.4){
		  pos_y=630;
		 }
		if(valpeso>6.4 && valpeso<=6.6){
		  pos_y=625;
		 }
		if(valpeso>6.6 && valpeso<=6.8){
		  pos_y=618;
		 }
		if(valpeso>6.8 && valpeso<=7){
		  pos_y=612;
		 }
		 
		 if(valpeso>7 && valpeso<=7.2){
		  pos_y=604;
		 }
		if(valpeso>7.2 && valpeso<=7.4){
		  pos_y=598;
		 }
		if(valpeso>7.4 && valpeso<=7.6){
		  pos_y=590;
		 }
		if(valpeso>7.6 && valpeso<=7.8){
		  pos_y=584;
		 }
		if(valpeso>7.8 && valpeso<=8){
		  pos_y=578;
		 }
		 
		 if(valpeso>8 && valpeso<=8.2){
		  pos_y=568;
		 }
		if(valpeso>8.2 && valpeso<=8.4){
		  pos_y=562;
		 }
		if(valpeso>8.4 && valpeso<=8.6){
		  pos_y=556;
		 }
		if(valpeso>8.6 && valpeso<=8.8){
		  pos_y=545;
		 }
		if(valpeso>8.8 && valpeso<=9){
		  pos_y=540;
		 }
		 
		 if(valpeso>9 && valpeso<=9.2){
		  pos_y=533;
		 }
		if(valpeso>9.2 && valpeso<=9.4){
		  pos_y=523;
		 }
		if(valpeso>9.4 && valpeso<=9.6){
		  pos_y=518;
		 }
		if(valpeso>9.6 && valpeso<=9.8){
		  pos_y=512;
		 }
		if(valpeso>9.8 && valpeso<=10){
		  pos_y=505;
		 }
		 
		 if(valpeso>10 && valpeso<=10.2){
		  pos_y=498;
		 }
		if(valpeso>10.2 && valpeso<=10.4){
		  pos_y=488;
		 }
		if(valpeso>10.4 && valpeso<=10.6){
		  pos_y=483;
		 }
		if(valpeso>10.6 && valpeso<=10.8){
		  pos_y=477;
		 }
		if(valpeso>10.8 && valpeso<=11){
		  pos_y=468;
		 }
		 
		 if(valpeso>11 && valpeso<=11.2){
		  pos_y=462;
		 }
		if(valpeso>11.2 && valpeso<=11.4){
		  pos_y=452;
		 }
		if(valpeso>11.4 && valpeso<=11.6){
		  pos_y=447;
		 }
		if(valpeso>11.6 && valpeso<=11.8){
		  pos_y=440;
		 }
		if(valpeso>11.8 && valpeso<=12){
		  pos_y=432;
		 }
		 
		 if(valpeso>12 && valpeso<=12.2){
		  pos_y=426;
		 }
		if(valpeso>12.2 && valpeso<=12.4){
		  pos_y=419;
		 }
		if(valpeso>12.4 && valpeso<=12.6){
		  pos_y=412;
		 }
		if(valpeso>12.6 && valpeso<=12.8){
		  pos_y=406;
		 }
		if(valpeso>12.8 && valpeso<=13){
		  pos_y=398;
		 }
		 
		 
		 if(valpeso>13 && valpeso<=13.2){
		  pos_y=390;
		 }
		if(valpeso>13.2 && valpeso<=13.4){
		  pos_y=383;
		 }
		if(valpeso>13.4 && valpeso<=13.6){
		  pos_y=374;
		 }
		if(valpeso>13.6 && valpeso<=13.8){
		  pos_y=367;
		 }
		if(valpeso>13.8 && valpeso<=14){
		  pos_y=361;
		 }
		 
		 if(valpeso>14 && valpeso<=14.2){
		  pos_y=353;
		 }
		if(valpeso>14.2 && valpeso<=14.4){
		  pos_y=345;
		 }
		if(valpeso>14.4 && valpeso<=14.6){
		  pos_y=339;
		 }
		if(valpeso>14.6 && valpeso<=14.8){
		  pos_y=333;
		 }
		if(valpeso>14.8 && valpeso<=15){
		  pos_y=327;
		 }
		 
		 if(valpeso>15 && valpeso<=15.2){
		  pos_y=317;
		 }
		if(valpeso>15.2 && valpeso<=15.4){
		  pos_y=310;
		 }
		if(valpeso>15.4 && valpeso<=15.6){
		  pos_y=303;
		 }
		if(valpeso>15.6 && valpeso<=15.8){
		  pos_y=298;
		 }
		if(valpeso>15.8 && valpeso<=16){
		  pos_y=290;
		 }
		 
		 if(valpeso>16 && valpeso<=16.2){
		  pos_y=283;
		 }
		if(valpeso>16.2 && valpeso<=16.4){
		  pos_y=276;
		 }
		if(valpeso>16.4 && valpeso<=16.6){
		  pos_y=269;
		 }
		if(valpeso>16.6 && valpeso<=16.8){
		  pos_y=262;
		 }
		if(valpeso>16.8 && valpeso<=17){
		  pos_y=254;
		 }
		 
		 if(valpeso>17 && valpeso<=17.2){
		  pos_y=248;
		 }
		if(valpeso>17.2 && valpeso<=17.4){
		  pos_y=240;
		 }
		if(valpeso>17.4 && valpeso<=17.6){
		  pos_y=232;
		 }
		if(valpeso>17.6 && valpeso<=17.8){
		  pos_y=225;
		 }
		if(valpeso>17.8 && valpeso<=18){
		  pos_y=218;
		 }
		 
		 
		 //medidad de talla
		 
		 if(tallan[s]<=45 ){
		   pos_y1=505;
		 }
		 if(tallan[s]>45 && tallan[s]<=46){
		   pos_y1=498;
		 }
		 if(tallan[s]>46 && tallan[s]<=47){
		   pos_y1=488;
		 }
		 if(tallan[s]>47 && tallan[s]<=48){
		   pos_y1=483;
		 }
		 if(tallan[s]>48 && tallan[s]<=49){
		   pos_y1=477;
		 }
		 if(tallan[s]>49 && tallan[s]<=50){
		   pos_y1=468;
		 }

		if(tallan[s]>50 && tallan[s]<=51){
		   pos_y1=462;
		 }
		 if(tallan[s]>51 && tallan[s]<=52){
		   pos_y1=452;
		 }
		 if(tallan[s]>52 && tallan[s]<=53){
		   pos_y1=447;
		 }
		 if(tallan[s]>53 && tallan[s]<=54){
		   pos_y1=440;
		 }
		 if(tallan[s]>54 && tallan[s]<=55){
		   pos_y1=432;
		 }
		 
		 		 		 		 
		 if(tallan[s]>55 && tallan[s]<=56){
		   pos_y1=426;
		 }
		 if(tallan[s]>56 && tallan[s]<=57){
		   pos_y1=419;
		 }
		 if(tallan[s]>57 && tallan[s]<=58){
		   pos_y1=412;
		 }
		 if(tallan[s]>58&& tallan[s]<=59){
		   pos_y1=406;
		 }
		 if(tallan[s]>59 && tallan[s]<=60){
		   pos_y1=398;
		 }


		if(tallan[s]>60 && tallan[s]<=61){
		   pos_y1=390;
		 }
		 if(tallan[s]>61 && tallan[s]<=62){
		   pos_y1=383;
		 }
		 if(tallan[s]>62 && tallan[s]<=63){
		   pos_y1=374;
		 }
		 if(tallan[s]>63 && tallan[s]<=64){
		   pos_y1=367;
		 }
		 if(tallan[s]>64 && tallan[s]<=65){
		   pos_y1=361;
		 }
		 
		 if(tallan[s]>65 && tallan[s]<=66){
		   pos_y1=353;
		 }
		 if(tallan[s]>66 && tallan[s]<=67){
		   pos_y1=345;
		 }
		 if(tallan[s]>67 && tallan[s]<=68){
		   pos_y1=339;
		 }
		 if(tallan[s]>68 && tallan[s]<=69){
		   pos_y1=333;
		 }
		 if(tallan[s]>69 && tallan[s]<=70){
		   pos_y1=327;
		 }
		 
		 if(tallan[s]>70 && tallan[s]<=71){
		   pos_y1=317;
		 }
		 if(tallan[s]>71 && tallan[s]<=72){
		   pos_y1=310;
		 }
		 if(tallan[s]>72 && tallan[s]<=73){
		   pos_y1=303;
		 }
		 if(tallan[s]>73 && tallan[s]<=74){
		   pos_y1=298;
		 }
		 if(tallan[s]>74 && tallan[s]<=75){
		   pos_y1=290;
		 }
		 
		 if(tallan[s]>75 && tallan[s]<=76){
		   pos_y1=283;
		 }
		 if(tallan[s]>76 && tallan[s]<=77){
		   pos_y1=276;
		 }
		 if(tallan[s]>77 && tallan[s]<=78){
		   pos_y1=269;
		 }
		 if(tallan[s]>78 && tallan[s]<=79){
		   pos_y1=262;
		 }
		 if(tallan[s]>79 && tallan[s]<=80){
		   pos_y1=254;
		 }
		 
		 if(tallan[s]>80 && tallan[s]<=81){
		   pos_y1=248;
		 }
		 if(tallan[s]>81 && tallan[s]<=82){
		   pos_y1=240;
		 }
		 if(tallan[s]>82 && tallan[s]<=83){
		   pos_y1=232;
		 }
		 if(tallan[s]>83 && tallan[s]<=84){
		   pos_y1=225;
		 }
		 if(tallan[s]>84 && tallan[s]<=85){
		   pos_y1=218;
		 }
		 
		 if(tallan[s]>85 && tallan[s]<=86){
		   pos_y1=211;
		 }
		 if(tallan[s]>86 && tallan[s]<=87){
		   pos_y1=204;
		 }
		 if(tallan[s]>87 && tallan[s]<=88){
		   pos_y1=198;
		 }
		 if(tallan[s]>88 && tallan[s]<=89){
		   pos_y1=190;
		 }
		 if(tallan[s]>89 && tallan[s]<=90){
		   pos_y1=183;
		 }
		 
		 if(tallan[s]>90 && tallan[s]<=91){
		   pos_y1=175;
		 }
		 if(tallan[s]>91 && tallan[s]<=92){
		   pos_y1=168;
		 }
		 if(tallan[s]>92 && tallan[s]<=93){
		   pos_y1=160;
		 }
		 if(tallan[s]>93 && tallan[s]<=94){
		   pos_y1=153;
		 }
		 if(tallan[s]>94 && tallan[s]<=95){
		   pos_y1=145;
		 }
		 
		 if(tallan[s]>95 && tallan[s]<=96){
		   pos_y1=139;
		 }
		 if(tallan[s]>96 && tallan[s]<=97){
		   pos_y1=132;
		 }
		 if(tallan[s]>97 && tallan[s]<=98){
		   pos_y1=125;
		 }
		 if(tallan[s]>98 && tallan[s]<=99){
		   pos_y1=118;
		 }
		 if(tallan[s]>99 && tallan[s]<=100){
		   pos_y1=110;
		 }
		 
		 if(tallan[s]>100 && tallan[s]<=101){
		   pos_y1=103;
		 }
		 if(tallan[s]>101 && tallan[s]<=102){
		   pos_y1=95;
		 }
		 if(tallan[s]>102 && tallan[s]<=103){
		   pos_y1=87;
		 }
		 if(tallan[s]>103 && tallan[s]<=104){
		   pos_y1=80;
		 }
		 if(tallan[s]>104 && tallan[s]<=105){
		   pos_y1=72;
		 }
		 //peso vs edad
		cuerpoDiv=document.getElementById('contenedor_de_puntos');
		 Nregistro=document.createElement("DIV");
		 Nregistro.className='bolita  posibolita';
		 Nregistro.style.position='absolute';
		 Nregistro.style.top=pos_y+'px';
		 Nregistro.style.left=pos_x+'px';
		 cuerpoDiv.appendChild(Nregistro);
		 
		 //talla vs edad
		 cuerpoDiv=document.getElementById('contenedor_de_puntos');
		 Nregistro=document.createElement("DIV");
		 Nregistro.className='bolita  posibolita';
		 Nregistro.style.position='absolute';
		 Nregistro.style.top=pos_y1+'px';
		 Nregistro.style.left=pos_x+'px';
		 cuerpoDiv.appendChild(Nregistro);
		 
	}	
}
banderadobleclick=0;
function presentarTablaControlesCreDesa(caso){
	var pag='/clinica/paginas/accionesXml/buscar_xml.jsp';
	switch (caso){
		case 1:  //crecimiento y desarrollo
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=controlescredesa";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listControles').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Fecha','Edad','Peso','Talla','Temperatura','Perimetrocefalico','Perimetrotoraxico','Cabeza','Ojos','Nariz','Oidos','Boca','Cuello','Toraxcardiores','Abdomen','Genitouri','Ano','Extremidades','Piel','Sistemanervioso','Motora','Adaptativa','Lenguaje','Personalsocial','Curvapeso','Curvatalla','Estado_nutricional','Remitido','Proximacita','IdMedico','Medico','Curvapeso1','Curvatalla1','Estado_nutricional1','Estado'], 
						 colModel :[ 
						   {name:'Fecha', index:'fechacita', width:100, align:'left'},
						   {name:'Edad', index:'edad', width:100, align:'left'},  
						   {name:'Peso', index:'peso', width:100, align:'left'},  
						   {name:'Talla', index:'talla', width:100,  align:'left'},  
						   {name:'Temperatura', index:'temperatura', width:100, align:'left'},  
						   {name:'Perimetrocefalico', index:'perimetrocefalico', width:100,  align:'left'},  
						   {name:'Perimetrotoraxico', index:'perimetrotoraxico', width:100, align:'left'},  
						   {name:'Cabeza', index:'cabeza', width:100,  align:'left'},  
						   {name:'Ojos', index:'ojos', width:100, align:'left'},  
						   {name:'Nariz', index:'nariz', width:100, align:'left'},  
						   {name:'Oidos', index:'oidos', width:100, align:'left'},  
						   {name:'Boca', index:'boca', width:100,  align:'left'},  
						   {name:'Cuello', index:'cuello', width:100, align:'left'},  
						   {name:'Toraxcardiores', index:'toraxcardiores', width:100, align:'left'},  
						   {name:'Abdomen', index:'abdomen', width:100, align:'left'},  
						   {name:'Genitouri', index:'genitouri', width:100, align:'left'},  
						   {name:'Ano', index:'ano', width:100, align:'left'},  
						   {name:'Extremidades', index:'extremidades', width:100, align:'left'},  
						   {name:'Piel', index:'piel', width:100, align:'left'},  						   
						   {name:'Sistemanervioso', index:'sistemanervioso', width:100, align:'left'},  
						   {name:'Motora', index:'motora', width:100, align:'left'},  
						   {name:'Adaptativa', index:'adaptativa', width:100, align:'left'},  
						   {name:'Lenguaje', index:'lenguaje', width:100,  align:'left'},  
						   {name:'Personalsocial', index:'personalsocial', width:100, align:'left'},  
						   {name:'Curvapeso', index:'curvapeso', width:100,  align:'left'},						   
						   {name:'Curvatalla', index:'curvatalla', width:100, align:'left'}, 
						   {name:'Estado_nutricional', index:'estado_nutricional', width:100, align:'left'},  
						   {name:'Remitido', index:'remitido', width:100,  align:'left'},  
						   {name:'Proximacita', index:'proximacita', width:100, align:'left'},  
						   {name:'IdMedico', index:'idmedico', width:100,  align:'left'},
						   {name:'Medico', index:'nombreapellido', width:200,  align:'left'},
						   {name:'Curvapeso1', index:'curvapeso1', hidden:true},
						   {name:'Curvatalla1', index:'curvatalla1', hidden:true},
						   {name:'Estado_nutricional1', index:'estado_nutricional1', hidden:true},
						   {name:'Estado', index:'estado', hidden:true},
							],  
						 pager: jQuery('#pagerAntControles'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'edad', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 2:  //citas materno perinatal
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=citasmaternoperinatal";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listControles').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Fecha','Edad_Ges','Peso','Presion_Arterial','Altura_Uterina','Presentacion','FCF','Mov_Fetales','Fe','Folatos','Calcio','Estado_Nutricional','Signo_Alarma','Proxima_Cita'], 
						 colModel :[ 
						   {name:'Fecha', index:'fechacita', width:100, align:'left'},
						   {name:'Edad_Ges', index:'edad', width:100, align:'left'},  
						   {name:'Peso', index:'peso', width:100, align:'left'},  
						   {name:'Presion_Arterial', index:'talla', width:100,  align:'left'},  
						   {name:'Altura_Uterina', index:'temperatura', width:100, align:'left'},  
						   {name:'Presentacion', index:'perimetrocefalico', width:100,  align:'left'},  
						   {name:'FCF', index:'perimetrotoraxico', width:100, align:'left'},  
						   {name:'Mov_Fetales', index:'cabeza', width:100,  align:'left'},  
						   {name:'Fe', index:'ojos', width:100, align:'left'},  
						   {name:'Folatos', index:'nariz', width:100, align:'left'},  
						   {name:'Calcio', index:'oidos', width:100, align:'left'},  
						   {name:'Estado_Nutricional', index:'boca', width:100,  align:'left'},  
						   {name:'Signo_Alarma', index:'cuello', width:100, align:'left'},  
						   {name:'Proxima_Cita', index:'toraxcardiores', width:100, align:'left'},  
							],  
						 pager: jQuery('#pagerAntControles'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'edad', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 3: // control puerperio - materno perinatal
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=controlpuerperio";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listControlesPuerperio').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['No','Hora','T.C','Pulso','PA','Invol.Uter','Loquios'], 
						 colModel :[ 
						   {name:'No', index:'fechacita', width:100, align:'left'},
						   {name:'Hora', index:'edad', width:100, align:'left'},  
						   {name:'T.C', index:'peso', width:100, align:'left'},  
						   {name:'Pulso', index:'talla', width:100,  align:'left'},  
						   {name:'PA', index:'temperatura', width:100, align:'left'},  
						   {name:'Invol.Uter', index:'perimetrocefalico', width:100,  align:'left'},  
						   {name:'Loquios', index:'perimetrotoraxico', width:100, align:'left'},  
							],  
						 pager: jQuery('#pagerAntControles2'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'edad', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 4: // clinico 1 // problemas
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=problemasclinico1";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listProblemasclinico1').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Codigo','Descripcion'], 
						 colModel :[ 
						   {name:'Codigo', index:'codigo', width:300, align:'left'},
						   {name:'Descripcion', index:'descripcion', width:500, align:'left'},  
							],  
						 pager: jQuery('#pagerAntControles'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 5: // clinico 2// problemas
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=problemasclinico2";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listProblemasclinico2').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Codigo','Descripcion'], 
						 colModel :[ 
						   {name:'Codigo', index:'codigo', width:300, align:'left'},
						   {name:'Descripcion', index:'descripcion', width:500, align:'left'},  
							],  
						 pager: jQuery('#pagerAntControles'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		
		case 6: // diagnosticos parto/aborto// materno-perinatal
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=diagnosticosindicaciones";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listIndicaciones').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Codigo','Descripcion'], 
						 colModel :[ 
						   {name:'Codigo', index:'codigo', width:200, align:'left'},
						   {name:'Descripcion', index:'descripcion', width:510, align:'left'},  
							],  
						 pager: jQuery('#pagerAntControles1'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;		
		
		case 7: // planificacion familiar, embarazos anteriores
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=planiembarazosanteriores";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listEmbarazosAnt').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Numero','A�o Terminacion','Mes de Gestacion','Parto Vaginal','Cesarea','Aborto','Nacidos Vivos','Nacidos Muertos'], 
						 colModel :[ 
						   {name:'numero', index:'numero', width:60, align:'left'},									
						   {name:'terminacion', index:'terminacion', width:100, align:'left'},
						   {name:'gestacion', index:'gestacion', width:100, align:'left'},
						   {name:'vaginal', index:'vaginal', width:100, align:'left'},
						   {name:'cesarea', index:'cesarea', width:100, align:'left'},
						   {name:'aborto', index:'aborto', width:100, align:'left'},
						   {name:'vivos', index:'vivos', width:80, align:'left'},
						   {name:'muertos', index:'muertos', width:80, align:'left'},
							],  
						 pager: jQuery('#pagerEmbarazosAnt'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 8: // planificacion familiar, historia menstrual anticoncepcional
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=historiaanticoncep";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listHistoriaAntico').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Numero','A�o Terminacion','Mes de Gestacion','Parto Vaginal','Cesarea','Aborto','Nacidos Vivos','Nacidos Muertos'], 
						 colModel :[ 
						   {name:'numero', index:'numero', width:60, align:'left'},									
						   {name:'terminacion', index:'terminacion', width:100, align:'left'},
						   {name:'gestacion', index:'gestacion', width:100, align:'left'},
						   {name:'vaginal', index:'vaginal', width:100, align:'left'},
						   {name:'cesarea', index:'cesarea', width:100, align:'left'},
						   {name:'aborto', index:'aborto', width:100, align:'left'},
						   {name:'vivos', index:'vivos', width:80, align:'left'},
						   {name:'muertos', index:'muertos', width:80, align:'left'},
							],  
						 pager: jQuery('#pagerHistoriaAntico'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 9: // planificacion familiar, consultas, exploracion del estado
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=consultashistoanti";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listConsultasPlani').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Numero','fecha','peso','tension arterial','metodo utilizado','satisfactorio con el metodo','transtornos menstruales','cambios de comportamiento','cefaleas','mareos','manchas en la piel','molestias en mamas','edema','varices','expulsion dispositivo','dolor bajo viente','flujo vaginal','sintomas urinarios','citologia','resultado','fecha','riesgo reproductivo','cambio de metodo','cual','fecha','remision a','proxima cita'], 
						 colModel :[ 
						   {name:'numero', index:'numero', width:60, align:'left'},									
						   {name:'fecha', index:'terminacion', width:100, align:'left'},
						   {name:'peso', index:'peso', width:100, align:'left'},
						   {name:'tensionart', index:'tensionart', width:100, align:'left'},
						   {name:'metodouti', index:'metodouti', width:100, align:'left'},
						   {name:'satismeto', index:'satismeto', width:100, align:'left'},
						   {name:'transmens', index:'transmens', width:80, align:'left'},
						   {name:'cambioscomp', index:'cambioscomp', width:80, align:'left'},
						   {name:'cefaleas', index:'cefaleas', width:60, align:'left'},									
						   {name:'mareos', index:'mareos', width:100, align:'left'},
						   {name:'manchaspiel', index:'manchaspiel', width:100, align:'left'},
						   {name:'molestiasmama', index:'molestiasmama', width:100, align:'left'},
						   {name:'edema', index:'edema', width:100, align:'left'},
						   {name:'varices', index:'varices', width:100, align:'left'},
						   {name:'expuldispo', index:'expuldispo', width:80, align:'left'},
						   {name:'dolorbajovien', index:'dolorbajovien', width:80, align:'left'},
						   {name:'flujovag', index:'transmens', width:80, align:'left'},
						   {name:'sintomasuri', index:'cambioscomp', width:80, align:'left'},
						   {name:'citologia', index:'cefaleas', width:60, align:'left'},									
						   {name:'resultado', index:'mareos', width:100, align:'left'},
						   {name:'fechacito', index:'manchaspiel', width:100, align:'left'},
						   {name:'riesgorepro', index:'molestiasmama', width:100, align:'left'},
						   {name:'cambiometo', index:'edema', width:100, align:'left'},
						   {name:'cual', index:'varices', width:100, align:'left'},
						   {name:'fechacambiometo', index:'expuldispo', width:80, align:'left'},
						   {name:'remisiona', index:'dolorbajovien', width:80, align:'left'},
   						   {name:'proximacita', index:'proximacita', width:80, align:'left'},
							],  
						 pager: jQuery('#pagerConsultasPlani'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 10: //enfermedades parto/aborto// materno-perinatal
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=enfermeparto";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listEnfermeparto').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Codigo','Descripcion'], 
						 colModel :[ 
						   {name:'Codigo', index:'codigo', width:200, align:'left'},
						   {name:'Descripcion', index:'descripcion', width:510, align:'left'},  
							],  
						 pager: jQuery('#pagerAntEnfermeparto'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 11: //enfermedades congenitas/recien-nacido/ materno-perinatal
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=enfermecongenitas";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listEnfermecongenitas').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Codigo','Descripcion'], 
						 colModel :[ 
						   {name:'Codigo', index:'codigo', width:200, align:'left'},
						   {name:'Descripcion', index:'descripcion', width:510, align:'left'},  
							],  
						 pager: jQuery('#pagerAntEnfermecongenitas'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
		case 12: //enfermedades recien-nacido// materno-perinatal
		cadenadatosid=$('#drag'+ventanaActual.num).find('#lblId').text().split('-');		
		valores_a_mandar=pag+"?accion=enfermereciennacido";
		valores_a_mandar=valores_a_mandar+"&tipoid="+cadenadatosid[0];
		valores_a_mandar=valores_a_mandar+"&identificacion="+cadenadatosid[1];		 		 
		$('#drag'+ventanaActual.num).find('#listEnfermereciennacido').jqGrid({  
		                url:valores_a_mandar, 
						 datatype: 'xml', 
						 mtype: 'GET', 
						 colNames:['Codigo','Descripcion'], 
						 colModel :[ 
						   {name:'Codigo', index:'codigo', width:200, align:'left'},
						   {name:'Descripcion', index:'descripcion', width:510, align:'left'},  
							],  
						 pager: jQuery('#pagerAntEnfermereciennacido'), 
						 rowNum:10, 
						 rowList:[20,50,100], 
						 sortname: 'codigo', 
						 sortorder: "desc", 
						 viewrecords: true, 
						 hidegrid: false, 
						 
						 
						 ondblClickRow: function(rowid) {				 
						 alert("rowid:  "+rowid);
						 banderadobleclick=1;
						 limpiarDatosControlesCreDes();
						 var datosDelRegistro = jQuery('#drag'+ventanaActual.num+" #listControles").getRowData(rowid);   //trae los datos de esta fila con este idrow  						 						  
						   $('#drag'+ventanaActual.num).find('#lblFechaActual').text($.trim(datosDelRegistro.Fecha));	 
						   $('#drag'+ventanaActual.num).find('#txtedad').val($.trim(datosDelRegistro.Edad));
						   $('#drag'+ventanaActual.num).find('#txtpeso').val($.trim(datosDelRegistro.Peso));
						   $('#drag'+ventanaActual.num).find('#txttalla').val($.trim(datosDelRegistro.Talla));
						   $('#drag'+ventanaActual.num).find('#txttemperatura').val($.trim(datosDelRegistro.Temperatura));
						   $('#drag'+ventanaActual.num).find('#txtpercefa').val($.trim(datosDelRegistro.Perimetrocefalico));
						   $('#drag'+ventanaActual.num).find('#txtpertor').val($.trim(datosDelRegistro.Perimetrotoraxico));
						   $('#drag'+ventanaActual.num).find('#txtcabeza').val($.trim(datosDelRegistro.Cabeza));
						   $('#drag'+ventanaActual.num).find('#txtojos').val($.trim(datosDelRegistro.Ojos));
						   $('#drag'+ventanaActual.num).find('#txtnariz').val($.trim(datosDelRegistro.Nariz));
						   $('#drag'+ventanaActual.num).find('#txtoidos').val($.trim(datosDelRegistro.Oidos));
						   $('#drag'+ventanaActual.num).find('#txtboca').val($.trim(datosDelRegistro.Boca));
						   $('#drag'+ventanaActual.num).find('#txtcuello').val($.trim(datosDelRegistro.Cuello));
						   $('#drag'+ventanaActual.num).find('#txttorax').val($.trim(datosDelRegistro.Toraxcardiores));
						   $('#drag'+ventanaActual.num).find('#txtabdomen').val($.trim(datosDelRegistro.Abdomen));
						   $('#drag'+ventanaActual.num).find('#txtgeniuri').val($.trim(datosDelRegistro.Genitouri));
						   $('#drag'+ventanaActual.num).find('#txtano').val($.trim(datosDelRegistro.Ano));
						   $('#drag'+ventanaActual.num).find('#txtextremi').val($.trim(datosDelRegistro.Extremidades));
						   $('#drag'+ventanaActual.num).find('#txtpiel').val($.trim(datosDelRegistro.Piel));
						   $('#drag'+ventanaActual.num).find('#txtsisnerv').val($.trim(datosDelRegistro.Sistemanervioso));
						   $('#drag'+ventanaActual.num).find('#txtmotora').val($.trim(datosDelRegistro.Motora));
						   $('#drag'+ventanaActual.num).find('#txtadaptati').val($.trim(datosDelRegistro.Adaptativa));
						   $('#drag'+ventanaActual.num).find('#txtlenguaje').val($.trim(datosDelRegistro.Lenguaje));
						   $('#drag'+ventanaActual.num).find('#txtpersoc').val($.trim(datosDelRegistro.Personalsocial));
						   $('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text($.trim(datosDelRegistro.Curvapeso));
						   $('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text($.trim(datosDelRegistro.Curvatalla));
						   $('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text($.trim(datosDelRegistro.Estado_nutricional));
						   $('#drag'+ventanaActual.num).find('#txtremitido').val($.trim(datosDelRegistro.Remitido));
						   $('#drag'+ventanaActual.num).find('#txtproxcita').val($.trim(datosDelRegistro.Proximacita));
						   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text($.trim(datosDelRegistro.IdMedico));
						   $('#drag'+ventanaActual.num).find('#nombreMedico').text($.trim(datosDelRegistro.Medico));
						   },
						 height: 100, 
						 imgpath: '/clinica/utilidades/javascript/themes/basic/images', 
						 caption: 'Resultados' 					 
			});
		break;
	}
}


var contcontrolcrecidesarrollo=0;

function agregarDatosControlesCreDes(){
		 var ids = jQuery("#listControles").getDataIDs();
		 cant = ids.length;		 
		 prox=parseInt(ids[ids.length-1])-1;
		 /*for(var i=0;i<ids.length;i++){ 
		 var c = ids[i]; 
		   alert(c);
		 }*/
		 var datarow = {
		 Fecha:$('#drag'+ventanaActual.num).find('#lblFechaActual').text(),	 
		 Edad:$('#drag'+ventanaActual.num).find('#txtedad').val(),
		 Peso:$('#drag'+ventanaActual.num).find('#txtpeso').val(),
		 Talla:$('#drag'+ventanaActual.num).find('#txttalla').val(),
		 Temperatura:$('#drag'+ventanaActual.num).find('#txttemperatura').val(),
		 Perimetrocefalico:$('#drag'+ventanaActual.num).find('#txtpercefa').val(),
		 Perimetrotoraxico:$('#drag'+ventanaActual.num).find('#txtpertor').val(),
		 Cabeza:$('#drag'+ventanaActual.num).find('#txtcabeza').val(),
		 Ojos:$('#drag'+ventanaActual.num).find('#txtojos').val(),
		 Nariz:$('#drag'+ventanaActual.num).find('#txtnariz').val(),
		 Oidos:$('#drag'+ventanaActual.num).find('#txtoidos').val(),
		 Boca:$('#drag'+ventanaActual.num).find('#txtboca').val(),
		 Cuello:$('#drag'+ventanaActual.num).find('#txtcuello').val(),
		 Toraxcardiores:$('#drag'+ventanaActual.num).find('#txttorax').val(),
		 Abdomen:$('#drag'+ventanaActual.num).find('#txtabdomen').val(),
		 Genitouri:$('#drag'+ventanaActual.num).find('#txtgeniuri').val(),
		 Ano:$('#drag'+ventanaActual.num).find('#txtano').val(),
		 Extremidades:$('#drag'+ventanaActual.num).find('#txtextremi').val(),
		 Piel:$('#drag'+ventanaActual.num).find('#txtpiel').val(),
		 Sistemanervioso:$('#drag'+ventanaActual.num).find('#txtsisnerv').val(),
		 Motora:$('#drag'+ventanaActual.num).find('#txtmotora').val(),
		 Adaptativa:$('#drag'+ventanaActual.num).find('#txtadaptati').val(),
		 Lenguaje:$('#drag'+ventanaActual.num).find('#txtlenguaje').val(),
		 Personalsocial:$('#drag'+ventanaActual.num).find('#txtpersoc').val(),
		 Curvapeso:$('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text(),
		 Curvatalla:$('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text(),
		 Estado_nutricional:$('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text(),
		 Remitido:$('#drag'+ventanaActual.num).find('#txtremitido').val(),
		 Proximacita:$('#drag'+ventanaActual.num).find('#txtproxcita').val(),
		 IdMedico:$('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text(),
		 Medico:$('#drag'+ventanaActual.num).find('#nombreMedico').text(),
		 Curvapeso1:$('#drag'+ventanaActual.num).find('#cmbcurvapeso').val(),
		 Curvatalla1:$('#drag'+ventanaActual.num).find('#cmbcurvatalla').val(),
		 Estado_nutricional1:$('#drag'+ventanaActual.num).find('#cmbestadonutricional').val(),
		 Estado:'1'
		 };
         var su=jQuery('#listControles').addRowData(prox,datarow);
		 //prox=prox-1;
		 // var su1=jQuery('#listControles').removeRowData(prox);
         //if(su) alert('Si se pudo'); else alert('');
	}
	
function modificarDatosControlesCreDes(){
	if(banderadobleclick == 1){
		var id = jQuery('#drag'+ventanaActual.num).find('#listControles').getGridParam('selrow');
		var ret = jQuery('#drag'+ventanaActual.num).find('#listControles').getRowData(id);		
				var su=jQuery('#drag'+ventanaActual.num).find('#listControles').setRowData(id,
				{ 				 
				   Fecha:$('#drag'+ventanaActual.num).find('#lblFechaActual').text(),	 
				   Edad:$('#drag'+ventanaActual.num).find('#txtedad').val(),
				   Peso:$('#drag'+ventanaActual.num).find('#txtpeso').val(),
				   Talla:$('#drag'+ventanaActual.num).find('#txttalla').val(),
				   Temperatura:$('#drag'+ventanaActual.num).find('#txttemperatura').val(),
				   Perimetrocefalico:$('#drag'+ventanaActual.num).find('#txtpercefa').val(),
				   Perimetrotoraxico:$('#drag'+ventanaActual.num).find('#txtpertor').val(),
				   Cabeza:$('#drag'+ventanaActual.num).find('#txtcabeza').val(),
				   Ojos:$('#drag'+ventanaActual.num).find('#txtojos').val(),
				   Nariz:$('#drag'+ventanaActual.num).find('#txtnariz').val(),
				   Oidos:$('#drag'+ventanaActual.num).find('#txtoidos').val(),
				   Boca:$('#drag'+ventanaActual.num).find('#txtboca').val(),
				   Cuello:$('#drag'+ventanaActual.num).find('#txtcuello').val(),
				   Toraxcardiores:$('#drag'+ventanaActual.num).find('#txttorax').val(),
				   Abdomen:$('#drag'+ventanaActual.num).find('#txtabdomen').val(),
				   Genitouri:$('#drag'+ventanaActual.num).find('#txtgeniuri').val(),
				   Ano:$('#drag'+ventanaActual.num).find('#txtano').val(),
				   Extremidades:$('#drag'+ventanaActual.num).find('#txtextremi').val(),
				   Piel:$('#drag'+ventanaActual.num).find('#txtpiel').val(),
				   Sistemanervioso:$('#drag'+ventanaActual.num).find('#txtsisnerv').val(),
				   Motora:$('#drag'+ventanaActual.num).find('#txtmotora').val(),
				   Adaptativa:$('#drag'+ventanaActual.num).find('#txtadaptati').val(),
				   Lenguaje:$('#drag'+ventanaActual.num).find('#txtlenguaje').val(),
				   Personalsocial:$('#drag'+ventanaActual.num).find('#txtpersoc').val(),
				   Curvapeso:$('#drag'+ventanaActual.num).find('#cmbcurvapeso :selected').text(),
				   Curvatalla:$('#drag'+ventanaActual.num).find('#cmbcurvatalla :selected').text(),
				   Estado_nutricional:$('#drag'+ventanaActual.num).find('#cmbestadonutricional :selected').text(),
				   Remitido:$('#drag'+ventanaActual.num).find('#txtremitido').val(),
				   Proximacita:$('#drag'+ventanaActual.num).find('#txtproxcita').val(),
				   IdMedico:$('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text(),
				   Medico:$('#drag'+ventanaActual.num).find('#nombreMedico').text(),
				   Curvapeso1:$('#drag'+ventanaActual.num).find('#cmbcurvapeso').val(),
				   Curvatalla1:$('#drag'+ventanaActual.num).find('#cmbcurvatalla').val(),
				   Estado_nutricional1:$('#drag'+ventanaActual.num).find('#cmbestadonutricional').val(),
				   Estado:'1'				 
				 });
				if(su){ alert("Se modific� el registro satisfactoriamente"); 
					banderadobleclick=0;
				}else alert("No se puede modificar el registro");					
	}else alert('Por favor d� doble click sobre el registro a modificar y realice las modificaciones correspondientes');
}
	
function quitarDatosControlesCreDes(lista){ //quitar esta funcion y remplazar por la de abajo en los formularios
	alert("lista"+lista);
	var id = jQuery('#drag'+ventanaActual.num).find('#'+lista).getGridParam('selrow');
	alert("id"+id);			
	if( id != null ) jQuery("#"+lista).delRowData(id); else alert("Error!"); 	
	}	
	
function quitarDatosJQGRID(lista){
	//alert("lista"+lista);
	var id = jQuery('#drag'+ventanaActual.num).find('#'+lista).getGridParam('selrow');
	//alert("id"+id);			
	if( id != null ) jQuery("#"+lista).delRowData(id); else alert("Error!"); 	
	}	
function limpiarDatosControlesCreDes(){
   $('#drag'+ventanaActual.num).find('#lblFechaActual').text("");	 
   $('#drag'+ventanaActual.num).find('#txtedad').val("");
   $('#drag'+ventanaActual.num).find('#txtpeso').val("");
   $('#drag'+ventanaActual.num).find('#txttalla').val("");
   $('#drag'+ventanaActual.num).find('#txttemperatura').val("");
   $('#drag'+ventanaActual.num).find('#txtpercefa').val("");
   $('#drag'+ventanaActual.num).find('#txtpertor').val("");
   $('#drag'+ventanaActual.num).find('#txtcabeza').val("");
   $('#drag'+ventanaActual.num).find('#txtojos').val("");
   $('#drag'+ventanaActual.num).find('#txtnariz').val("");
   $('#drag'+ventanaActual.num).find('#txtoidos').val("");
   $('#drag'+ventanaActual.num).find('#txtboca').val("");
   $('#drag'+ventanaActual.num).find('#txtcuello').val("");
   $('#drag'+ventanaActual.num).find('#txttorax').val("");
   $('#drag'+ventanaActual.num).find('#txtabdomen').val("");
   $('#drag'+ventanaActual.num).find('#txtgeniuri').val("");
   $('#drag'+ventanaActual.num).find('#txtano').val("");
   $('#drag'+ventanaActual.num).find('#txtextremi').val("");
   $('#drag'+ventanaActual.num).find('#txtpiel').val("");
   $('#drag'+ventanaActual.num).find('#txtsisnerv').val("");
   $('#drag'+ventanaActual.num).find('#txtmotora').val("");
   $('#drag'+ventanaActual.num).find('#txtadaptati').val("");
   $('#drag'+ventanaActual.num).find('#txtlenguaje').val("");
   $('#drag'+ventanaActual.num).find('#txtpersoc').val("");
   $('#drag'+ventanaActual.num).find('#txtremitido').val("");
   $('#drag'+ventanaActual.num).find('#txtproxcita').val("");
   $('#drag'+ventanaActual.num).find('#lblTipoIdMedico').text("");
   $('#drag'+ventanaActual.num).find('#nombreMedico').text("");	
}

function Autocompletar_Ang(elem_qescoge,paramAutoc){ 
 
	    datos_a_mandar="accion="+paramAutoc;
		datos_a_mandar=datos_a_mandar+"&tipo="+$('#drag'+ventanaActual.num).find('#'+elem_qescoge).val();				
		varajaxInit=crearAjax();
		varajaxInit.open("POST",'/clinica/paginas/accionesXml/datosAutocomplete_xml.jsp',true);
		varajaxInit.onreadystatechange=function(){respuestaAutocompletar_Ang(paramAutoc)};
		varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		varajaxInit.send(datos_a_mandar);		
}
function respuestaAutocompletar_Ang(paramAutoc){  
	if(varajaxInit.readyState == 4 ){
			if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('item')!=null){
				cod=raiz.getElementsByTagName('cod');		  
			    text=raiz.getElementsByTagName('text');	
				data = new Array();
			    for(a=0;a<text.length;a++ ){						     
				     data[a]=cod[a].firstChild.data+' - '+ text[a].firstChild.data;
				}
				
				$('#'+paramAutoc).flushCache(); //sirve para borrar el cache
				$('#'+paramAutoc).autocomplete(data,{
				   multiple:false,
				   matchContains: true, //sirve para buscar en cualquier parte de la frase
				   autofill:false,
				   selectedFirst:false, // ordena segun los caracteres coinidentes
				   cacheLength:1,
				   width:400,
				   //formatItem:formatItem,
				   //formatResult:formatResult,											  
				  // selSeparator: '|', //separador de campos '|' por defecto
				  // lineSeparator:'\n' //separador de lineas (registros) '\n' por defecto
				   minChars:1 //minimo de caracteres para empezar a desplegar la lista
			    });
				
			  }else{
				  alert("No se puede Autocompletar");
			     }
			 }else{
					swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			 }
		 }
		if(varajaxInit.readyState == 1){
		}

}


//funciones para indicacion principal de induccion


function agregarIndicacionesPartoAborto(){
	     var datoscadena=$('#drag'+ventanaActual.num).find('#txtCodigoDiagnosticos').val().split('-');	 
		 var ids = jQuery("#listIndicaciones").getDataIDs();
		 cant = ids.length;		 
		 prox=parseInt(ids[ids.length-1])-1;
		 /*for(var i=0;i<ids.length;i++){ 
		 var c = ids[i]; 
		   alert(c);
		 }*/
		 var datarow = {
		 	 
		 Codigo:datoscadena[0],	 
		 Descripcion:datoscadena[1],		 
		 };
         var su=jQuery('#listIndicaciones').addRowData(prox,datarow);
		 $('#drag'+ventanaActual.num).find('#txtCodigoDiagnosticos').val("");
		 //prox=prox-1;
		 // var su1=jQuery('#listControles').removeRowData(prox);
         //if(su) alert('Si se pudo'); else alert('');
	}
	
	

function cargarMenuAng(pagina,num,opc,msg,guardar,modificar,eliminar,imprimir,limpiar,buscar,valores){ alert('s');
       // alert(objetosMenu.length);
		///crear el objeto ventanaActual 
			 ventanaActual=new objetoMenu();
	         ventanaActual.num=num;
	         ventanaActual.opc=opc;
	         ventanaActual.mensaje=msg;
	         ventanaActual.guardar=guardar;
	         ventanaActual.modificar=modificar;
	         ventanaActual.eliminar=eliminar;
	         ventanaActual.imprimir=imprimir;
	         ventanaActual.limpiar=limpiar;	 
	         ventanaActual.buscar=buscar;
		   ////fin crear el objeto ventanaActual
		if(buscaObjetoMenu(num)){
			//$("#drag"+num).show(); 
			sobreponer(document.getElementById("drag"+num));
			//insertarNuevaVentana();
		}else{	
		   varajaxMenu=crearAjax();
           valores_a_mandar="";
			  valores_a_mandar=valores;
		   varajaxMenu.open("POST",pagina,true);
		   varajaxMenu.onreadystatechange=llenarinfoMenu;
		   varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		   varajaxMenu.send(valores_a_mandar); 
		   //alert(valores_a_mandar);  
		}
 }	
 