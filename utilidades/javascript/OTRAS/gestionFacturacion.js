/**
 * clase para la gestion del umbral o rangos permitidos .
 * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
*/

class GestionUmbral {
    /**
     * metodo para traer datos 
     * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
     * @param {String} valores_a_mandar - parametros para la peticion.
     * @param {Int} query - query de la bd.
     * 
    */
    traerDatosUmbral(valores_a_mandar,query){
        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                data: {
                    "idQuery": query,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () {
                    //mostrarLoader(urlParams.get("accion"))
                },
                success: function (data) {
                    
                    let rows = data.getElementsByTagName('row');
                    resolve(rows)

                },
                complete: function (jqXHR, textStatus) {
                    //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    reject();
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            });

        });

    }


    async procesarDatosUmbralPlanes(plan,procedimiento){
        valores_a_mandar = ''
        add_valores_a_mandar(plan)
        add_valores_a_mandar(procedimiento)
        let rows = await this.traerDatosUmbral(valores_a_mandar,9046)
        let arreglo = [];
        for (i = 0; i < rows.length; i++) {
            let idPlan = rows[i].getElementsByTagName('cell')[1].textContent
            let totalFacturas = rows[i].getElementsByTagName('cell')[2].textContent;
            let porcentajeUmbral = rows[i].getElementsByTagName('cell')[3].textContent;
            let tipo = rows[i].getElementsByTagName('cell')[4].textContent;
            let cantidadMaxima = rows[i].getElementsByTagName('cell')[5].textContent;
            let objeto = {
                'idPlan': idPlan,
                'numeroFacturaMax': totalFacturas,
                'porcentajeUmbral': porcentajeUmbral,
                'tipo': tipo,
                'final': cantidadMaxima 
            };
            arreglo.push(objeto);
        }
        return arreglo;

    }


    async umbralPlanes(plan,procedimiento) {
        //let dataGrilla = gestionFacturacion.obtenerSeleccionGrilla('listGrillaFacturas');
        let bloquearNumeracion = false
        let consecutivosABloquear = []    
       // let { consecutivos, totalFacturasSeleccionadas } = this.obtenerConsecutivosYTotales(dataGrilla);    
        // console.log({ consecutivos, totalFacturasSeleccionadas });
        let mensaje = '';
    
        try {
            let data = await this.procesarDatosUmbralPlanes(plan,procedimiento);
            
            mensaje = this.procesarResultadosUmbralPlanes(data, '', mensaje, bloquearNumeracion, consecutivosABloquear);
            
            console.log(data, '-----------******');
            // console.log(mensaje);
            // console.log(consecutivosABloquear);

            //swAlert('warning','','',mensaje)    
           
            return {
                bloquearNumeracion: bloquearNumeracion,
                consecutivosABloquear: consecutivosABloquear,
                mensaje:mensaje
            };
        } catch (error) {
            console.error('Error en umbral:', error);
            throw error;
        }
    }
    
    async procesarDatosUmbral(consecutivos){
        valores_a_mandar = ''
        add_valores_a_mandar(consecutivos)
        let rows = await this.traerDatosUmbral(valores_a_mandar,9025)
        let arreglo = [];
        for (i = 0; i < rows.length; i++) {
            let numeroFacturaMax = rows[i].getElementsByTagName('cell')[1].textContent
            let idFacturasConsecutivo = rows[i].getElementsByTagName('cell')[2].textContent;
            let final = rows[i].getElementsByTagName('cell')[3].textContent;
            let porcentajeUmbral = rows[i].getElementsByTagName('cell')[4].textContent;
            let numeroResolucion = rows[i].getElementsByTagName('cell')[5].textContent;
            let objeto = {
                'numeroFacturaMax': numeroFacturaMax,
                'idFacturasConsecutivo': idFacturasConsecutivo,
                'final': final,
                'porcentajeUmbral': porcentajeUmbral,
                'numeroResolucion':numeroResolucion
            };
            arreglo.push(objeto);
        }
        return arreglo;

    }

    obtenerConsecutivosYTotales(dataGrilla) {
        let consecutivos = [...new Set(dataGrilla.map(objeto => objeto.ID_FACTURAS_CONSECUTIVO).filter(consecutivo => consecutivo != "null"))];
        console.log(consecutivos);
        let totalFacturasSeleccionadas = dataGrilla.reduce((acumulador, objeto) => {
            const idConsecutivo = objeto.ID_FACTURAS_CONSECUTIVO;
            acumulador[idConsecutivo] = (acumulador[idConsecutivo] || 0) + 1;
            return acumulador;
        }, {});
    
        // console.log({ consecutivos, totalFacturasSeleccionadas });
    
        return { consecutivos, totalFacturasSeleccionadas };
    }

    procesarResultadosUmbralPlanes(data, totalFacturasSeleccionadas, mensaje, bloquearNumeracion, consecutivosABloquear) {
        // console.log(totalFacturasSeleccionadas, mensaje, bloquearNumeracion, consecutivosABloquear);
        data.forEach(obj => {
            let resultadoPorcentajes = this.calcularPorcentajes(obj);
            resultadoPorcentajes.sort((a, b) => a - b);
    
            console.log('resultadoPorcentajes',resultadoPorcentajes)
            let indice = resultadoPorcentajes.indexOf(parseFloat(obj.numeroFacturaMax));
            let superaMaximo = this.verificarMaximo(totalFacturasSeleccionadas, obj);
    
            if (indice > 0) {
                mensaje = this.procesarIndiceAlertaPlanes(indice, resultadoPorcentajes, obj, mensaje);
            }
    
            if (superaMaximo) {
                let totalDisponibleANumerar = obj.final - obj.numeroFacturaMax;
                mensaje = this.procesarSuperaMaximoPlanes(obj, totalFacturasSeleccionadas, totalDisponibleANumerar, mensaje, bloquearNumeracion, consecutivosABloquear);
            }
        });
        return mensaje
    }
    procesarResultadosUmbral(data, totalFacturasSeleccionadas, mensaje, bloquearNumeracion, consecutivosABloquear) {
        // console.log(totalFacturasSeleccionadas, mensaje, bloquearNumeracion, consecutivosABloquear);
        data.forEach(obj => {
            let resultadoPorcentajes = this.calcularPorcentajes(obj);
            resultadoPorcentajes.sort((a, b) => a - b);
    
            let indice = resultadoPorcentajes.indexOf(parseFloat(obj.numeroFacturaMax));
            let superaMaximo = this.verificarMaximo(totalFacturasSeleccionadas, obj);
    
            if (indice > 0) {
                mensaje = this.procesarIndiceAlerta(indice, resultadoPorcentajes, obj, mensaje);
            }
    
            if (superaMaximo) {
                let totalDisponibleANumerar = obj.final - obj.numeroFacturaMax;
                mensaje = this.procesarSuperaMaximo(obj, totalFacturasSeleccionadas, totalDisponibleANumerar, mensaje, bloquearNumeracion, consecutivosABloquear);
            }
        });
        return mensaje
    }
    calcularPorcentajes(obj) {
        let porcentajes = obj.porcentajeUmbral.split('-');
        return porcentajes.map(elemento => parseFloat(elemento) * obj.final).concat(parseFloat(obj.numeroFacturaMax));
    }
    
    verificarMaximo(totalFacturasSeleccionadas, obj) {
        let totalDisponibleANumerar = obj.final - obj.numeroFacturaMax;
        return totalDisponibleANumerar < totalFacturasSeleccionadas[obj.idFacturasConsecutivo];
    }
    

    procesarIndiceAlertaPlanes(indice, resultadoPorcentajes, obj, mensaje) {
        let indiceAlerta = indice - 1;
        let porcentajeAlerta = resultadoPorcentajes[indice - 1] / obj.final;
        mensaje += '<h3>Alerta</h3> Para el plan: ' + obj.idPlan +
             
            ' se excedió el ' + '<strong>'+ porcentajeAlerta * 100 + '% </strong> ';
        
        return mensaje
    }

    procesarSuperaMaximoPlanes(obj, totalFacturasSeleccionadas, totalDisponibleANumerar, mensaje, bloquearNumeracion, consecutivosABloquear) {
        
        mensaje += '<h3>Accion bloqueada </h3> El máximo asignado al plan supero al disponible. <ul><li>Disponibles plan: ' +
            totalDisponibleANumerar + '</li><li> numero plan ' + numeroFacturaMax+ '</li></ul>';
        bloquearNumeracion = true;
        consecutivosABloquear.push(obj.idPlan);

        return mensaje
    }

    procesarIndiceAlerta(indice, resultadoPorcentajes, obj, mensaje) {
        let indiceAlerta = indice - 1;
        let porcentajeAlerta = resultadoPorcentajes[indice - 1] / obj.final;
        mensaje += '<h3>Alerta</h3> Para el consecutivo: ' + obj.idFacturasConsecutivo +
            ' asociado a resolucion: ' + '<strong>' +obj.numeroResolucion + '</strong>' +
            ' se excedió el ' + '<strong>'+ porcentajeAlerta * 100 + '% </strong>' + ' de numeración. ';
        
        return mensaje
    }
    
    procesarSuperaMaximo(obj, totalFacturasSeleccionadas, totalDisponibleANumerar, mensaje, bloquearNumeracion, consecutivosABloquear) {
        
        mensaje += '<h3>Numeracion bloqueada </h3> El máximo de facturas a numerar es mayor al disponible. <ul><li>Disponibles a numerar: ' +
            totalDisponibleANumerar + '</li><li> número de facturas que ha seleccionado ' + totalFacturasSeleccionadas[obj.idFacturasConsecutivo] + '</li></ul>';
        bloquearNumeracion = true;
        consecutivosABloquear.push(obj.idFacturasConsecutivo);

        return mensaje
    }

    async umbral() {
        let dataGrilla = gestionFacturacion.obtenerSeleccionGrilla('listGrillaFacturas');
        let bloquearNumeracion = false
        let consecutivosABloquear = []    
        let { consecutivos, totalFacturasSeleccionadas } = this.obtenerConsecutivosYTotales(dataGrilla);    
        // console.log({ consecutivos, totalFacturasSeleccionadas });
        let mensaje = '';
    
        try {
            let data = await this.procesarDatosUmbral(consecutivos);
            
            mensaje = this.procesarResultadosUmbral(data, totalFacturasSeleccionadas, mensaje, bloquearNumeracion, consecutivosABloquear);
            
            // console.log(data, '-----------******');
            // console.log(mensaje);
            // console.log(consecutivosABloquear);

            //swAlert('warning','','',mensaje)    
           
            return {
                bloquearNumeracion: bloquearNumeracion,
                consecutivosABloquear: consecutivosABloquear,
                mensaje:mensaje
            };
        } catch (error) {
            console.error('Error en umbral:', error);
            throw error;
        }
    }
    

}

class GestionFacturacion {
    constructor(numeroFactura, idFactura, estado) {
        this.numeroFactura = numeroFactura
        this.idFactura = idFactura
        this.estado = estado
    }
  
    obtenerSeleccionGrilla(arg) {
        let grid = $("#" + arg);
        let selectedRowIds = grid.jqGrid("getGridParam", "selarrrow");
        let dataGrilla = [];
        for (let i = 0; i < selectedRowIds.length; i++) {
            let rowData = grid.jqGrid("getRowData", selectedRowIds[i]);
            dataGrilla.push(rowData)
        }

        return dataGrilla

    }

    validacionTificacion(arg) {
        let facturasTipificadas = [];
        let dataGrilla = this.obtenerSeleccionGrilla(arg)
        let control = false
        dataGrilla.forEach((data) => {
            if (data.ID_ESTADO_TIPIFICACION != '3' || control) {
                console.log('data:', data)
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    showConfirmButton: false,
                    showCloseButton: true,
                    html: '<p style="color:black">Selecciono facturas sin tipificar</style>',

                });
                control = true
                return facturasTipificadas = []
            } else {
                console.log('data1:', data)
                data.NUM_FACTURA = data.NUM_FACTURA.split(';')[1] == undefined ? data.NUM_FACTURA : data.NUM_FACTURA.split(';')[1]
                console.log(' data.NUM_FACTURA', data.NUM_FACTURA)
                facturasTipificadas.push(data.NUM_FACTURA);
            }


        })


        return facturasTipificadas

    }

    facturasPrefinalizadas(arg) {
        let facturasPrefinalizadas = [];
        let dataGrilla = this.obtenerSeleccionGrilla(arg)
        let control = false
        dataGrilla.forEach((data) => {
            console.log('ID_ESTADO', typeof data.ID_ESTADO)
            if (data.ID_ESTADO != 'P' || control) {
                console.log('data', data)
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    showConfirmButton: false,
                    showCloseButton: true,
                    html: '<p style="color:black">Las factura deben estar en estado Prefinalizado</style>',

                });
                control = true
                return facturasPrefinalizadas = []
            } else {
                facturasPrefinalizadas.push(data.ID_FACTURA);
            }


        })


        return facturasPrefinalizadas

    }

    exportarFacturaTipificada(arg) {
        let numeroFacturas = this.validacionTificacion(arg)
        console.log('numeroFacturas', numeroFacturas)
        this.descargaComprimidoSolicitud(numeroFacturas)


    }


    descargaComprimidoSolicitud(numeroFacturas) {
        const zip = new JSZip();
        const promises = [];
        let countFacturas = 0;
    
        numeroFacturas.forEach(carpetaNombre => {
            countFacturas++;
    
            const promise = this.cargarListaArchivos(carpetaNombre)
                .then((data) => {
                    return Promise.all(data.map(row => {
                        let url = '/tipificacion/' + carpetaNombre + '/' + row;
                        return fetch(url)
                            .then(response => response.blob())
                            .then(blob => {
                                zip.file(carpetaNombre + '/' + row, blob);
                            });
                    }));
                });
    
            promises.push(promise);
        });
    
        
        Promise.all(promises)
            .then(() => {
               
                zip.generateAsync({ type: 'blob' })
                .then(function (blob) {
                       
                    const blobURL = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = blobURL;
                    a.download = 'archivos.zip';
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                    window.URL.revokeObjectURL(blobURL);
                    countFacturas = 0;
                    });
            })
            .catch(() => {
             
            });
    }
    
    cargarListaArchivos(carpetaNombre) {
        valores_a_mandar = '';
        add_valores_a_mandar(carpetaNombre);
        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                data: {
                    "idQuery": 6007,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () {
                    //mostrarLoader(urlParams.get("accion"))
                },
                success: function (data) {
                    let respuesta = data.getElementsByTagName('cell')[1].firstChild.data;
                    console.log('r', respuesta.split(','))
                    resolve(respuesta.split(','));
                },
                complete: function (jqXHR, textStatus) {
                    //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    reject();
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            });

        });
    }


    /**
     * metodo para consultar los id de las facturas asociado a un consecutivo
     * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
     * @param {Array} consecutivos - array de numeros - numero consecutivo que pertenece a una factura.     
    */
    facturasDeConsecutivo(consecutivos){
        console.log(consecutivos);
        var id_facturas = []

        consecutivos.forEach(consecutivo => {
            let dataGrilla = this.obtenerSeleccionGrilla('listGrillaFacturas');
            id_facturas = [...new Set(dataGrilla
                .filter(objeto => objeto.ID_FACTURAS_CONSECUTIVO == consecutivo)
                .map(objeto => objeto.ID_FACTURA)
              )];            
        });

        return id_facturas
        
    }


}

class NumerarFacturasBloque extends GestionFacturacion {
    constructor(numeroFactura, idFactura, estado) {
        super(numeroFactura, idFactura, estado)
        this.listaFacturasNumeracion = []
        this.listaAdmisionesCerrar = []
        this.year = ''
        this.mes = ''
        this.listaFacturasError = []
    }
    async init(arg) {
        let {bloquearNumeracion,consecutivosABloquear,mensaje} =  await gestionUmbral.umbral()
        let idFacturasABloqear = gestionFacturacion.facturasDeConsecutivo(consecutivosABloquear)
        let idFacturas = this.facturasPrefinalizadas(arg)

        //se eliminan las facturas bloqueadas por el umbral
        idFacturas = idFacturas.filter(item => !idFacturasABloqear.includes(item))
        let continuar = false
        if (mensaje !== '') {
            try {
                await Swal.fire({
                    title: '',
                    text: '',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Continuar la numeración',
                    cancelButtonText: 'Cancelar',
                    showLoaderOnConfirm: true,
                    html:mensaje,
                }).then((result) => {
                    if (result.value) {
                        continuar = true
                        
                    }else{
                        continuar = false
                    }
                });
        
            } catch (error) {
                console.error('Hubo un error:', error);
            }            
            if (!continuar) {
                return
            }
            
        }

        
        // console.log({continuar});

        if (idFacturas.length <= 0) {
            return
        }
        //return
        //validacion folios en estado borradorquery 6008
        this.validacionIdfactura(idFacturas, 9008)
            .then((datos) => {

                this.respuestaValidacion(datos, 'FOLIOS EN ESTADO BORRADOR')
                    .then((response) => {

                        let idFacturasBorrador = datos.map(factura => factura.idFactura)
                        let facturasFiltradasBorrador = idFacturas.filter(item => !idFacturasBorrador.includes(item))

                        this.validacionIdfactura(facturasFiltradasBorrador, 9009)
                            .then((datos) => {
                                let idFacturaMontos = datos.map(factura => factura.idFactura)
                                let facturasFiltradasMontos = facturasFiltradasBorrador.filter(item => !idFacturaMontos.includes(item))


                                this.respuestaValidacion(datos, 'MONTOS')
                                    .then((response) => {

                                        if (facturasFiltradasMontos.length > 0) {
                                            this.modalPeriodoFacturado(facturasFiltradasMontos)

                                        } else {

                                            Swal.fire({
                                                title: `<span style="font-size: 15px;">Numeracion en bloque</span>`,
                                                html:
                                                    `
                                                <div style="max-height: 300px; overflow-y: auto;">
                                                    No hay facturas validas para numerar.
                                                </div>
                                            `,
                                                inputAttributes: {
                                                    autocapitalize: 'off'
                                                },
                                                showCancelButton: true,
                                                confirmButtonText: 'Continuar',
                                                cancelButtonText: 'Cancelar',
                                                showLoaderOnConfirm: true
                                            })
                                        }

                                    })
                                    .catch(() => { })

                            })
                            .catch(() => { })


                    })

                    .catch((err) => { })

            })

            .catch((err) => { })

    }


    modalPeriodoFacturado(idFacturas) {

        let timerInterval
        let today = new Date();
        let currentYear = today.getFullYear();
        let lastYear = currentYear - 1;


        Swal.fire({
            title: '<span style="font-size: 15px;">PERIODO DE PRESTACION DE LOS SERVICIOS FACTURADOS</span>',
            html:
                `
               <table style="border: none; width: 100%;">
                   <tr>
                       <td style="border: none;"><label for="cmbAnioPrestacionBloque">Año:</label></td>
                       <td style="border: none;">
                           <select id="cmbAnioPrestacionBloque"  style="width: 60%;" class="swal2-input" onchange="numerarFacturasBloque.limpiarMes()">
                               <option value="${lastYear}">${lastYear}</option>
                               <option value="${currentYear}" selected>${currentYear}</option>
                           </select>
                       </td>
                   </tr>
                   <tr>
                       <td style="border: none;"><label for="cmbMesPrestacionBloque">Mes:</label></td>
                       <td style="border: none;">
                           <select id="cmbMesPrestacionBloque" class="swal2-input" onfocus="numerarFacturasBloque.mostrarMeses()" style="width: 60%;">
                               <!-- Opciones de mes aquí -->
                           </select>
                       </td>
                   </tr>
               </table>
            `,
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            //allowOutsideClick: () => !Swal.isLoading()
        })

            .then((result) => {

                if (result.isConfirmed) {

                    try {

                        this.year = document.getElementById('cmbAnioPrestacionBloque').value
                        this.mes = document.getElementById('cmbMesPrestacionBloque').value

                        this.validacionNumeracionBloque(idFacturas, 9010)
                            .then((datosPaquete) => {

                                let idFacturasPaquete = datosPaquete.map(factura => factura.idFactura)
                                console.log('idFacturas3: ', idFacturas)
                                let facturasFiltradasPaquete = idFacturas.filter(item => !idFacturasPaquete.includes(item))
                                this.listaFacturasError.push(idFacturasPaquete)

                                this.validacionNumeracionBloque(facturasFiltradasPaquete, 9011)
                                    .then((datosPlan) => {
                                        let idFacturasPlan = datosPlan.map(factura => factura.idFactura)
                                        this.listaFacturasError.push(idFacturasPlan)
                                        let facturasFiltradasPlan = idFacturas.filter(item => !idFacturasPlan.includes(item))
                                        this.listaFacturasNumeracion = facturasFiltradasPlan
                                        console.log('err', this.listaFacturasError)
                                        modificarCRUD('actualizarFechaPreriodoServiciosFacturadosBloque')

                                    })
                                    .catch((err) => { console.log('err  ', err) })

                            })
                            .catch((error) => { console.log('err', error, this.year) })


                    } catch (error) { }

                }

            })
    }


    cerrarAdmisiones(idFacturas) {
        console.log('idFacturas: ', idFacturas)
        this.validacionFacturasAdmisionUnica(idFacturas, 9015)
            .then((datos) => {
                let admisionesMultiple = datos.filter(item => item.total > 1)
                let LAdmisionesMultiplet = admisionesMultiple.map(admisiones => admisiones.idAdmision)
                let LAdmisionesMultiple = LAdmisionesMultiplet.filter((valor, indice) => {
                    return LAdmisionesMultiplet.indexOf(valor) === indice;
                });
                let admisionesUnaE = datos.filter(item => item.total == 1)
                let LAdmisionesUnaET = admisionesUnaE.map(admisiones => admisiones.idAdmision)
                let LAdmisionesUnaE = LAdmisionesUnaET.filter((valor, indice) => {
                    return LAdmisionesUnaET.indexOf(valor) === indice;
                });
                this.validacionAdmisionMensajes(LAdmisionesUnaE, 9016)
                    .then((admisionesSinMensaje1) => {
                        let admisionesMensaje1 = LAdmisionesUnaE.filter(item => !admisionesSinMensaje1.includes(item))
                        this.validacionAdmisionMensajes(admisionesSinMensaje1, 9017)
                            .then((admisionesCerrar) => {
                                let admisionesMensaje2 = admisionesSinMensaje1.filter(item => !admisionesCerrar.includes(item))
                                this.respuestaValidacionAdmisionesFinal(admisionesCerrar, 'CERRAR LAS SIGUIENTES ADMISIONES: ')
                                    .then((response) => {
                                        if (admisionesCerrar.length > 0) {
                                            this.respuestaValidacionAdmisiones(LAdmisionesMultiple, admisionesMensaje1, admisionesMensaje2)
                                                .then((response) => {
                                                    this.listaAdmisionesCerrar = admisionesCerrar
                                                    modificarCRUD('cerrarAdmisionesBloque')
                                                })
                                        } else {
                                            Swal.fire({
                                                title: '<span style="font-size: 15px;">NO QUEDAN AMISIONES POR CERRAR</span>',
                                                html:
                                                    `
                                                `,
                                                inputAttributes: {
                                                    autocapitalize: 'off'
                                                },
                                                showCancelButton: false,
                                                confirmButtonText: 'Aceptar',
                                                showLoaderOnConfirm: true,
                                                //allowOutsideClick: () => !Swal.isLoading()
                                            })
                                        }
                                    })
                            })
                    })
            })
    }


    validacionIdfactura(idFacturas, query) {
        valores_a_mandar = '';
        add_valores_a_mandar(idFacturas);
        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                data: {
                    "idQuery": query,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () {
                    //mostrarLoader(urlParams.get("accion"))
                },
                success: function (data) {

                    let rows = data.getElementsByTagName('row'); // Get all 'row' elements
                    let datosRespuesta = [];

                    for (let i = 0; i < rows.length; i++) {
                        let cellElements = rows[i].getElementsByTagName('cell'); // Get all 'cell' elements within the 'row'
                        let idFactura = cellElements[1].textContent; // Get the 'id' attribute of the 'row' element
                        let mensaje = cellElements[2].textContent; // Get the text content of the third 'cell' element

                        let respuesta = {
                            idFactura,
                            mensaje
                        };

                        datosRespuesta.push(respuesta);
                    }


                    // Now datosRespuesta contains an array of objects with idFactura and mensaje
                    resolve(datosRespuesta);
                },
                complete: function (jqXHR, textStatus) {
                    //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    reject();
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            });

        });
    }


    validacionNumeracionBloque(idFacturas, query) {


        valores_a_mandar = '';
        add_valores_a_mandar(idFacturas);
        add_valores_a_mandar(this.year)
        add_valores_a_mandar(this.mes)
        console.log(' valores_a_mandar', valores_a_mandar)
        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                data: {
                    "idQuery": query,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () {
                    //mostrarLoader(urlParams.get("accion"))
                },
                success: function (data) {

                    let rows = data.getElementsByTagName('row'); // Get all 'row' elements
                    let datosRespuesta = [];

                    for (let i = 0; i < rows.length; i++) {
                        let cellElements = rows[i].getElementsByTagName('cell'); // Get all 'cell' elements within the 'row'
                        let idFactura = cellElements[1].textContent; // Get the 'id' attribute of the 'row' element
                        let mensaje = cellElements[2].textContent; // Get the text content of the third 'cell' element

                        let respuesta = {
                            idFactura,
                            mensaje
                        };

                        datosRespuesta.push(respuesta);
                    }


                    // Now datosRespuesta contains an array of objects with idFactura and mensaje

                    resolve(datosRespuesta);
                },
                complete: function (jqXHR, textStatus) {
                    //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    reject();
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            });

        });

    }

    validacionFacturasAdmisionUnica(idFacturas, query) {

        valores_a_mandar = '';
        add_valores_a_mandar(idFacturas)
        console.log(' valores_a_mandar', valores_a_mandar)
        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                data: {
                    "idQuery": query,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () {
                    //mostrarLoader(urlParams.get("accion"))
                },
                success: function (data) {

                    let rows = data.getElementsByTagName('row'); // Get all 'row' elements
                    let datosRespuesta = [];

                    for (let i = 0; i < rows.length; i++) {
                        let cellElements = rows[i].getElementsByTagName('cell'); // Get all 'cell' elements within the 'row'
                        let idFactura = cellElements[1].textContent; // Get the text content of the third 'cell' element
                        let idAdmision = cellElements[2].textContent; // Get the text content of the third 'cell' element
                        let total = cellElements[3].textContent; // Get the text content of the third 'cell' element

                        let respuesta = {
                            idFactura,
                            idAdmision,
                            total
                        };

                        datosRespuesta.push(respuesta);
                    }


                    // Now datosRespuesta contains an array of objects with idFactura and mensaje
                    console.log('res: ', datosRespuesta)
                    resolve(datosRespuesta);
                },
                complete: function (jqXHR, textStatus) {
                    //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    reject();
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            });

        });

    }

    validacionAdmisionMensajes(idAdmision, query) {

        valores_a_mandar = '';
        add_valores_a_mandar(idAdmision)
        add_valores_a_mandar(idAdmision)
        console.log(' valores_a_mandar', valores_a_mandar)
        return new Promise((resolve = () => { }, reject = () => { }) => {
            $.ajax({
                url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                type: "POST",
                data: {
                    "idQuery": query,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () {
                    //mostrarLoader(urlParams.get("accion"))
                },
                success: function (data) {

                    let rows = data.getElementsByTagName('row'); // Get all 'row' elements
                    let datosRespuesta = [];

                    for (let i = 0; i < rows.length; i++) {
                        let cellElements = rows[i].getElementsByTagName('cell'); // Get all 'cell' elements within the 'row'
                        let admision = cellElements[1].textContent; // Get the text content of the third 'cell' element

                        datosRespuesta.push(admision);
                    }


                    // Now datosRespuesta contains an array of objects with idFactura and mensaje
                    console.log('res: ', datosRespuesta)
                    resolve(datosRespuesta);
                },
                complete: function (jqXHR, textStatus) {
                    //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    reject();
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            });

        });

    }

    respuestaValidacion(datos, titulo) {
        return new Promise((resolve, reject) => {
            let mensaje = ''
            console.log('datos', datos)
            if (datos.length > 0) {
                datos.forEach((data) => {

                    mensaje += `Id factura: ${data.idFactura}</br>${data.mensaje} </br></br>`


                })

                Swal.fire({
                    title: `<span style="font-size: 15px;">${titulo}</span>`,
                    html:
                        `
                    <div style="max-height: 300px; overflow-y: auto;">
                     ${mensaje}
                    </div>
                `,
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Continuar',
                    cancelButtonText: 'Cancelar',
                    showLoaderOnConfirm: true
                })
                    .then((result) => {

                        if (result.isConfirmed) {

                            resolve(result.isConfirmed);
                        } else {
                            reject();
                        }
                    })
            } else {

                resolve(true)
            }


        })

    }

    respuestaValidacionAdmisionesFinal(datos, titulo) {
        return new Promise((resolve, reject) => {
            let mensaje = ''
            console.log('datos', datos)
            if (datos.length > 0) {
                datos.forEach((data) => {

                    mensaje += `Id Admision: ${data} </br></br>`


                })

                Swal.fire({
                    title: `<span style="font-size: 15px;">${titulo}</span>`,
                    html:
                        `
                    <div style="max-height: 300px; overflow-y: auto;">
                     ${mensaje}
                    </div>
                `,
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Continuar',
                    cancelButtonText: 'Cancelar',
                    showLoaderOnConfirm: true
                })
                    .then((result) => {

                        if (result.isConfirmed) {

                            resolve(result.isConfirmed);
                        } else {
                            reject();
                        }
                    })
            } else {

                resolve(true)
            }


        })

    }

    respuestaValidacionAdmisiones(datos1, datos2, datos3) {
        return new Promise((resolve, reject) => {
            let mensaje1 = ''
            let mensaje2 = ''
            let mensaje3 = ''
            console.log('datos', datos)

            if (datos1.length > 0) {
                mensaje1 = `<b><span style="font-size: 15px;">ADMISIONES CON MULTIPLES CITAS:</span></b></br></br>
                            <div style="max-height: 300px; overflow-y: auto;">`
                datos1.forEach((data) => {

                    mensaje1 += `Id Admision: ${data} </br>`


                })
                mensaje1 += `</div>`
            } if (datos2.length > 0) {
                mensaje2 = `<b><span style="font-size: 15px;">LAS ADMISIONES TIENEN FOLIOS EN ESTADO BORRADOR O EL ESTADO DE LA ADMISION "PENDIENTE FACTURA":</span></b></br></br>
                            <div style="max-height: 300px; overflow-y: auto;">`
                datos2.forEach((data) => {

                    mensaje2 += `Id Admision: ${data} </br>`


                })
                mensaje2 += `</div>`
            } if (datos3.length > 0) {
                mensaje3 = `<b><span style="font-size: 15px;">LAS ADMISIONES TIENE ELEMENTOS PENDIENTES POR FACTURAR:</span></b></br></br>
                            <div style="max-height: 300px; overflow-y: auto;">`
                datos3.forEach((data) => {

                    mensaje3 += `Id Admision: ${data} </br>`


                })
                mensaje3 += `</div>`
            } if (datos1.length > 0 || datos2.length > 0 || datos3.length > 0) {
                Swal.fire({
                    title: ` <span style="font-size: 15px;">LAS SIGUIENTES ADMISIONES NO SE PUEDEN CERRAR: </span>`,
                    html:
                        `                   
                     ${mensaje1}
                     </br>
                     ${mensaje2}
                     </br>
                     ${mensaje3}
                     </br>
                `,
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Continuar',
                    cancelButtonText: 'Cancelar',
                    showLoaderOnConfirm: true
                })
                    .then((result) => {

                        if (result.isConfirmed) {

                            resolve(result.isConfirmed);
                        } else {
                            reject();
                        }
                    })
            } else {

                resolve(true)
            }


        })

    }


    limpiarMes() {
        $("#cmbMesPrestacionBloque").empty()
        $("#cmbMesPrestacionBloque").append($("<option>", {
            value: "",
            text: ""
        }));
    }


    mostrarMeses() {
        $("#cmbMesPrestacionBloque").empty()
        let meses = [
            { id: 1, nombre: "Enero" },
            { id: 2, nombre: "Febrero" },
            { id: 3, nombre: "Marzo" },
            { id: 4, nombre: "Abril" },
            { id: 5, nombre: "Mayo" },
            { id: 6, nombre: "Junio" },
            { id: 7, nombre: "Julio" },
            { id: 8, nombre: "Agosto" },
            { id: 9, nombre: "Septiembre" },
            { id: 10, nombre: "Octubre" },
            { id: 11, nombre: "Noviembre" },
            { id: 12, nombre: "Diciembre" },
        ]

        var aux = new Date();
        var anio_actual = aux.getFullYear()
        var mes_actual = aux.getMonth() + 1;

        for (i = 0; i < meses.length; i++) {

            if (anio_actual == $("#cmbAnioPrestacionBloque").val() && i + 1 > mes_actual) {
                return
            }

            $("#cmbMesPrestacionBloque").append($("<option>", {
                value: meses[i].id,
                text: meses[i].nombre
            }));
        }

    }





}

let gestionFacturacion = new GestionFacturacion();
let numerarFacturasBloque = new NumerarFacturasBloque()
let gestionUmbral = new GestionUmbral()

