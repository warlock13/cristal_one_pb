var intentog = 0;
function buscarPaciente(arg, pag) {
    //pag = "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp";
    pagina = pag;
    ancho = 500;
    switch (arg) {
        case 'paciente':
            //limpiarDivEditarJuan(arg);
            //$('#drag' + ventanaActual.num).find('#divContenido').css('height', '400px');
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=327&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdentificacion'));
            add_valores_a_mandar(valorAtributo('cmbTipoId'));
            add_valores_a_mandar(valorAtributo('txtNombrePaciente'));

            //var sexo = 'accionesXml/obtenerCombos.jsp?idQueryCombo=110';
            //var sexo2 = ":cargando";
            //var regimen = 'accionesXml/obtenerCombos.jsp?idQueryCombo=518';
            //var regimenAfiliacion = 'accionesXml/obtenerCombos.jsp?idQueryCombo=305';
            //var etnia = 'accionesXml/obtenerCombos.jsp?idQueryCombo=2';
            //var discapacidad = 'accionesXml/obtenerCombos.jsp?idQueryCombo=953';
            //var estadoCivil = 'accionesXml/obtenerCombos.jsp?idQueryCombo=589';
            //var programaSocial = 'accionesXml/obtenerCombos.jsp?idQueryCombo=956';

            /*var tipoID = 'http://10.0.0.3:9010/combo/105';
            var escolaridad = 'http://10.0.0.3:9010/combo/4';
            var grupopoblacional = 'http://10.0.0.3:9010/combo/955';
            var parentesco = 'http://10.0.0.3:9010/combo/957';
            var condicionVulnerabilidad = 'http://10.0.0.3:9010/combo/959';
            var hechosVictimizantes = 'http://10.0.0.3:9010/combo/954';


            //var administradora = 'accionesXml/obtenerAutocomplete.jsp?idQuery=308';
            var administradora = 'http://10.0.0.3:9010/autocomplete/308';
            var jsonAdministradora = new Array();

            var ipsPrimaria = 'http://10.0.0.3:9010/autocomplete/313';
            var jsonIpsPrimaria = new Array();

            var municipio = 'http://10.0.0.3:9010/autocomplete/212';
            var jsonMunicipio = new Array();

            var barrio = 'http://10.0.0.3:9010/autocomplete/229';
            var jsonBarrio = new Array();

            var ocupacion = 'http://10.0.0.3:9010/autocomplete/8';
            var jsonOcupacion = new Array();

            var localidad = 'http://10.0.0.3:9010/autocomplete/934';
            var jsonLocalidad = new Array();


            $.get(administradora, function (data) {
                jsonAdministradora = data;
            });

            $.get(municipio, function (data) {
                jsonMunicipio = data;
            });

            $.get(barrio, function (data) {
                jsonBarrio = data;
            });

            $.get(ipsPrimaria, function (data) {
                jsonIpsPrimaria = data;
            });

            $.get(ocupacion, function (data) {
                jsonOcupacion = data;
            });

            $.get(localidad,function(data)
            {
                jsonLocalidad = data;
            });

            var viewOptions =
            {
                labelswidth:'10%',
                width: 1200,

            };*/
            $('#drag' + ventanaActual.num).find("#listGrilla").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId', 'Identificacion', 'Codigo Afiliacion', 'Nombre 1', 'Nombre 2', 'Apellido 1', 'Apellido 2',
                    'Nombre Completo', 'Fecha Nacimiento', 'Sexo', 'Nacionalidad', 'Municipio', 'Nivel Sisben', 'Direccion', 'Barrio', 'Localidad', 'Estrato',
                    'Telefono', 'Celular 1', 'Celular 2', 'e-mail', 'Administradora', 'IPS Primaria', 'Regimen', 'Regimen', 'Tipo Afiliado', 'Tipo Afiliado',
                    'Ocupacion', 'Estado Civil', 'Estado Civil', 'Lugar Nacimiento', 'Etnia', 'Etnia', 'Discapacidad', 'Discapacidad', 'Nivel Escolar', 'Nivel Escolar', 'Grupo Poblacional', 'Grupo Poblacional',
                    'Acudiente', 'Parentesco', 'Parentesco', 'TipoId Acudiente', 'Identificacion Acudiente', 'Direccion Acudiente', 'Telefono Acudiente', 'e-mail Acudiente',
                    'CondicionVulneravilidad', 'CondicionVulnerabilidad', 'hv', 'Hechos Victimizantes', 'Programa Social', 'Programa Social', 'Intersexual', 'Prueba', 'Gestante', 'Fecha_afiliacion_eps',
                    'orientacion_sexual', 'grupSanguineo'

                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 10), editrules: { required: true }, formoptions: { rowpos: 3, colpos: 1 }, editable: true, edittype: "select", },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 30), editrules: { required: true, number: true }, editable: true, formoptions: { rowpos: 3, colpos: 2 }, viewable: true, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'afiliacion', index: 'afiliacion', width: anchoP(ancho, 30), editrules: { required: false }, editable: true, formoptions: { rowpos: 4, colpos: 1 }, viewable: true, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'Nombre1', index: 'Nombre1', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 5, colpos: 1 }, viewable: false },
                    { name: 'Nombre2', index: 'Nombre2', hidden: true, editrules: { required: false, edithidden: true }, editable: true, formoptions: { rowpos: 5, colpos: 2 }, viewable: false },
                    { name: 'Apellido1', index: 'Apellido1', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 6, colpos: 1 }, viewable: false },
                    { name: 'Apellido2', index: 'Apellido2', hidden: true, editrules: { required: false, edithidden: true }, editable: true, formoptions: { rowpos: 6, colpos: 2 }, viewable: false },
                    { name: 'NombreCompleto', index: 'NombreCompleto', width: anchoP(ancho, 110), align: 'left', viewable: true, formoptions: { rowpos: 4, colpos: 1 } },
                    { name: 'FechaNacimiento', index: 'FechaNacimiento', width: anchoP(ancho, 35), align: 'left', },
                    { name: 'Sexo', index: 'Sexo', width: anchoP(ancho, 10), align: 'left', editrules: { required: true }, editable: true, edittype: "select", },
                    { name: 'Nacionalidad', index: 'Nacionalidad', width: anchoP(ancho), align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'MunicipioResi', index: 'MunicipioResi', width: anchoP(ancho), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'Sisben', index: 'Sisben', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 8, colpos: 2 }, },
                    { name: 'Direccion', index: 'Direccion', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, formoptions: { rowpos: 9, colpos: 1 } },
                    { name: 'Barrio', index: 'Barrio', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, },
                    { name: 'Localidad', index: 'Localidad', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, },
                    { name: 'idestrato', index: 'idestrato', width: anchoP(ancho, 20), align: 'left', viewable: false, hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 10, colpos: 2 }, },
                    //{ name: 'Estrato', index: 'Estrato', width: anchoP(ancho, 20), align: 'left', hidden: true, viewable:true, formoptions: { rowpos: 26, colpos: 1 }},                                                            
                    { name: 'Telefono', index: 'Telefono', width: anchoP(ancho, 20), align: 'left', editrules: { required: false }, editable: true, formoptions: { rowpos: 11, colpos: 1 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'celular1', index: 'celular1', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, formoptions: { rowpos: 11, colpos: 2 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'celular2', index: 'celular2', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, formoptions: { rowpos: 12, colpos: 1 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'email', index: 'email', align: 'center', hidden: true, editrules: { edithidden: true, required: false }, editable: true, edittype: "text", formoptions: { rowpos: 12, colpos: 2 }, formatter: 'email', editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return email(e) }) } } },
                    { name: 'administradora', index: 'administradora', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, edittype: "text", },
                    { name: 'ipsprimaria', index: 'ipsprimaria', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { required: true, edithidden: true }, editable: true, edittype: "text", },
                    { name: 'idregimen', index: 'idregimen', width: anchoP(ancho, 30), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'regimen', index: 'regimen', width: anchoP(ancho, 30), align: 'left', formoptions: { rowpos: 8, colpos: 3 } },
                    { name: 'idregia', index: 'idregia', width: anchoP(ancho, 30), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "select", },
                    { name: 'regimenafiliacion', index: 'regimenafiliacion', width: anchoP(ancho, 30), align: 'left', formoptions: { rowpos: 8, colpos: 4 } },
                    { name: 'Ocupacion', index: 'Ocupacion', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "text", },
                    { name: 'idec', index: 'idec', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'estadocivil', index: 'estadocivil', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 9, colpos: 2 } },
                    { name: 'lugarNacimiento', index: 'lugarNacimiento', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'idet', index: 'idet', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'Etnia', index: 'Etnia', width: anchoP(ancho, 60), align: 'left', formoptions: { rowpos: 10, colpos: 1 } },
                    //{ name: 'Pueblo', index: 'Pueblo', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 17, colpos: 1 } },
                    { name: 'iddis', index: 'iddis', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "select", },
                    { name: 'discapacidad', index: 'discapacidad', width: anchoP(ancho, 60), align: 'left', hidden: true, formoptions: { rowpos: 11, colpos: 1 } },
                    { name: 'idne', index: 'idne', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'NivelEsc', index: 'NivelEsc', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 11, colpos: 2 } },
                    { name: 'idgp', index: 'idgp', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'grupopoblacional', index: 'grupopoblacional', width: anchoP(ancho, 10), align: 'left', formoptions: { rowpos: 11, colpos: 3 } },
                    { name: 'acudiente', index: 'acudiente', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, formoptions: { rowpos: 19, colpos: 1 }, editrules: { required: false, edithidden: true } },
                    { name: 'idps', index: 'idps', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, editrules: { required: false, edithidden: true }, edittype: "select", },
                    { name: 'parentesco', index: 'parentesco', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 26, colpos: 1 } },
                    { name: 'tipo_id_acudiente', index: 'tipo_id_acudiente', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, edittype: "select", },
                    { name: 'identificacionA', index: 'identificacionA', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, editrules: { edithidden: false, required: false }, formoptions: { rowpos: 20, colpos: 2 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'acudienteDir', index: 'acudienteDir', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 21, colpos: 1 } },
                    { name: 'acudienteTel', index: 'acudienteTel', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 21, colpos: 2 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'acudienteEmail', index: 'acudienteEmail', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 22, colpos: 1 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return email(e) }) } } },
                    { name: 'idcv', index: 'idcv', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'condicionVulnerabilidad', index: 'condicionVulnerabilidad', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 14, colpos: 1 } },
                    { name: 'hv', index: 'hv', width: anchoP(ancho, 20), align: 'left', hidden: true },
                    { name: 'hechos_victimizantes', index: 'hechos_victimizantes', width: anchoP(ancho, 20), hidden: true, align: 'left', formoptions: { rowpos: 14, colpos: 3 } },
                    { name: 'idpso', index: 'idpso', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'programaSocial', index: 'programaSocial', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 14, colpos: 2 } },
                    { name: 'intersexual', index: 'intersexual', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'prueba', index: 'prueba', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'gestante', index: 'gestante', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'fecha_afiliacion_eps', index: 'fecha_afiliacion_eps', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'orientacion_sexual', index: 'orientacion_sexual', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'grupSanguineo', index: 'grupSanguineo', hidden: true, }
                ],
                height: 500,
                width: 700,
                //shrinkToFit : false,
                //autowidth: true,
                caption: "Paciente",
                shrinkToFit: false,
                onSelectRow: function (rowid) {
                    const rh = {
                        '0': 'A +',
                        '1': 'A -',
                        '2': 'B +',
                        '3': 'B -',
                        '4': 'AB +',
                        '5': 'O +',
                        '6': 'O -',
                        '7': 'AB -',
                        '8': 'No Sabe'
                    }
                    mostrar('divEditar')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrilla').getRowData(rowid);

                    console.log('datosRow.Sexo,', datosRow.Sexo)

                    estad = 0;
                    asignaAtributo('txtIdPac', datosRow.id, 0);
                    asignaAtributo('cmbTipoIdPac', datosRow.TipoId, 1);
                    asignaAtributo('cmbTipoId', datosRow.TipoId, 0);
                    asignaAtributo('txtIdentificacion', datosRow.identificacion, 0);
                    asignaAtributo('txtIdentificacionPac', datosRow.identificacion, 1);
                    asignaAtributo('txtAfiliacion', datosRow.afiliacion, 0);
                    asignaAtributo('cmbSexo', datosRow.Sexo, estad);
                    asignaAtributo('txtNombre1', datosRow.Nombre1, estad);
                    asignaAtributo('txtNombre2', datosRow.Nombre2, estad);
                    asignaAtributo('txtApellido1', datosRow.Apellido1, estad);
                    asignaAtributo('txtApellido2', datosRow.Apellido2, estad);
                    var fecha = datosRow.FechaNacimiento.substring(6, 10) + '-' + datosRow.FechaNacimiento.substring(3, 5) + '-' + datosRow.FechaNacimiento.substring(0, 2);
                    asignaAtributo('txtFechaNac', fecha, estad);
                    asignaAtributo('txtNacionalidad', datosRow.Nacionalidad.split('-')[0], estad)
                    asignaAtributo('txtMunicipio', datosRow.MunicipioResi.split('-')[0], estad)
                    $('#txtBarrio').select2('data', {id:datosRow.Barrio.split('-')[0], text:datosRow.Barrio.split('-')[1]})
                    asignaAtributo('cmbSisben', datosRow.Sisben, estad);
                    asignaAtributo('txtDireccion', datosRow.Direccion, estad);
                    
                    //asignaAtributo('txtBarrio', datosRow.Barrio, estad);
                    asignaAtributo('txtLocalidad', datosRow.Localidad, estad);
                    asignaAtributo('cmbEstrato', datosRow.idestrato, estad);
                    asignaAtributo('cmbEstadoCivil', datosRow.idec, estad);
                    asignaAtributo('txtTelefono', datosRow.Telefono, estad);
                    asignaAtributo('txtCelular1', datosRow.celular1, estad);
                    asignaAtributo('txtCelular2', datosRow.celular2, estad);
                    asignaAtributo('txtEmail', datosRow.email, estad);
                    asignaAtributo('txtAdministradora', datosRow.administradora.split('-')[0], estad);
                    asignaAtributo('txtIpsPrimaria', datosRow.ipsprimaria, estad);

                    asignaAtributo('cmbRegimen', datosRow.idregimen, estad);
                    asignaAtributo('cmbTipoAfiliado', datosRow.idregia, estad);
                    asignaAtributo('txtOcupacion', datosRow.Ocupacion.split('-')[0], estad);
                    asignaAtributo('txtNacimiento', datosRow.lugarNacimiento.split('-')[0], estad);
                    asignaAtributo('cmbEtnia', datosRow.idet, estad);
                    asignaAtributo('cmbDiscapacidad', datosRow.iddis, estad);
                    asignaAtributo('cmbEscolaridad', datosRow.idne, estad);
                    asignaAtributo('cmbGrupoPoblacional', datosRow.idgp, estad);
                    asignaAtributo('cmbVulnerabilidad', datosRow.idcv, estad);
                    asignaAtributo('cmbHechosVicti', datosRow.hv, estad);
                    asignaAtributo('txtAcudiente', datosRow.acudiente, estad);
                    asignaAtributo('cmbParentesco', datosRow.idps, estad);
                    asignaAtributo('cmbTipoIdAcudiente', datosRow.tipo_id_acudiente, estad);
                    asignaAtributo('txtIdentificacionAcudiente', datosRow.identificacionA, estad);
                    asignaAtributo('txtDireccionAcudiente', datosRow.acudienteDir, estad);
                    asignaAtributo('txtTelefonoAcudiente', datosRow.acudienteTel, estad);
                    asignaAtributo('txtEmailAcudiente', datosRow.acudienteEmail, estad);
                    asignaAtributo('cmbProgramaSocial', datosRow.idpso, estad);
                    asignaAtributo('cmbPrueba', datosRow.prueba, estad);
                    asignaAtributo('cmbGestante', datosRow.gestante, estad);
                    asignaAtributo('cmbIntersexual', datosRow.intersexual, estad);
                    asignaAtributo('txtFechaAfi', datosRow.fecha_afiliacion_eps, estad);

                    asignaAtributo('cmbOrientacion', datosRow.orientacion_sexual, estad);
                    asignaAtributo('txtObservaciones', datosRow.Observaciones, estad);
                    asignaAtributo('lblIdAnteriores', datosRow.idAnteriores, estad);

                    asignaAtributo('cmbGruposSanguineo', datosRow.grupSanguineo, 0);
                    asignaAtributo('lblRH', rh[datosRow.grupSanguineo], 0);




                    console.log('dat', datosRow)
                    //buscarHC('listGrillaHomologacionIdentifica', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')


                    var fechaNacimiento = new Date(fecha);
                    var edad = calcularEdad(fechaNacimiento);
                    var nombreVentanita = datosRow.Nombre1 + ' ' + datosRow.Nombre2 + ' ' + datosRow.Apellido1 + ' ' + datosRow.Apellido2;
                    var edadVentanita = edad.anios + ' Años ' + edad.meses + ' Meses ' + edad.dias + ' Días'


                    asignaAtributo("lblNombrePacienteVentanita", nombreVentanita, 1);
                    asignaAtributo("lblEdadPacienteVentanita", edadVentanita, 1);


                    if (datosRow.gestante == 'SI') {
                        document.getElementById("chkgestante").checked = true;
                    } else {
                        document.getElementById("chkgestante").checked = false;
                    }

                    if (document.getElementById("cmbSexo").value == 'F' && edad.anios > 10 && edad.anios < 50) {
                        document.getElementById("divDatosGestantes").style.display = "flex";
                    } else {
                        document.getElementById("divDatosGestantes").style.display = "none";
                        document.getElementById("cmbGestante").value = 'NO';
                        document.getElementById("cmbGestante").disabled = true;
                    }

                    document.getElementById("divInfoPacienteVentana").style.display = "flex";
                    moverTarjeta()
                    $('.select2-custom').trigger('change');
                    

                },
                loadBeforeSend: function (xhr, settings) {
                    modificarCRUD('pacienteTempG');
                },
                gridComplete: function () {
                    $("#listGrilla").jqGrid("setSelection", 1, true);
                    $('#gbox_listGrilla').hide()
                },
            });
            $('#drag' + ventanaActual.num).find("#listGrilla").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        ///////////////////
        /////////////////

        case 'paciente2':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=327&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdentificacionPac'));
            add_valores_a_mandar(valorAtributo('cmbTipoIdPac'));
            add_valores_a_mandar(valorAtributo('txtNombre1'));

            $('#drag' + ventanaActual.num).find("#listGrilla").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId', 'Identificacion', 'Codigo Afiliacion', 'Nombre 1', 'Nombre 2', 'Apellido 1', 'Apellido 2',
                    'Nombre Completo', 'Fecha Nacimiento', 'Sexo', 'Nacionalidad', 'Municipio', 'Nivel Sisben', 'Direccion', 'Barrio', 'Localidad', 'Estrato',
                    'Telefono', 'Celular 1', 'Celular 2', 'e-mail', 'Administradora', 'IPS Primaria', 'Regimen', 'Regimen', 'Tipo Afiliado', 'Tipo Afiliado',
                    'Ocupacion', 'Estado Civil', 'Estado Civil', 'Lugar Nacimiento', 'Etnia', 'Etnia', 'Discapacidad', 'Discapacidad', 'Nivel Escolar', 'Nivel Escolar', 'Grupo Poblacional', 'Grupo Poblacional',
                    'Acudiente', 'Parentesco', 'Parentesco', 'TipoId Acudiente', 'Identificacion Acudiente', 'Direccion Acudiente', 'Telefono Acudiente', 'e-mail Acudiente',
                    'CondicionVulneravilidad', 'CondicionVulnerabilidad', 'hv', 'Hechos Victimizantes', 'Programa Social', 'Programa Social', 'Intersexual', 'Prueba', 'Gestante', 'Fecha_afiliacion_eps',
                    'orientacion_sexual', 'grupSanguineo'

                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2), hidden: true },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 10), editrules: { required: true }, formoptions: { rowpos: 3, colpos: 1 }, editable: true, edittype: "select", },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 30), editrules: { required: true, number: true }, editable: true, formoptions: { rowpos: 3, colpos: 2 }, viewable: true, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'afiliacion', index: 'afiliacion', width: anchoP(ancho, 30), editrules: { required: false }, editable: true, formoptions: { rowpos: 4, colpos: 1 }, viewable: true, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'Nombre1', index: 'Nombre1', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 5, colpos: 1 }, viewable: false },
                    { name: 'Nombre2', index: 'Nombre2', hidden: true, editrules: { required: false, edithidden: true }, editable: true, formoptions: { rowpos: 5, colpos: 2 }, viewable: false },
                    { name: 'Apellido1', index: 'Apellido1', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 6, colpos: 1 }, viewable: false },
                    { name: 'Apellido2', index: 'Apellido2', hidden: true, editrules: { required: false, edithidden: true }, editable: true, formoptions: { rowpos: 6, colpos: 2 }, viewable: false },
                    { name: 'NombreCompleto', index: 'NombreCompleto', width: anchoP(ancho, 110), align: 'left', viewable: true, formoptions: { rowpos: 4, colpos: 1 } },
                    { name: 'FechaNacimiento', index: 'FechaNacimiento', width: anchoP(ancho, 35), align: 'left', },
                    { name: 'Sexo', index: 'Sexo', width: anchoP(ancho, 10), align: 'left', editrules: { required: true }, editable: true, edittype: "select", },
                    { name: 'Nacionalidad', index: 'Nacionalidad', width: anchoP(ancho), align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'MunicipioResi', index: 'MunicipioResi', width: anchoP(ancho), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'Sisben', index: 'Sisben', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 8, colpos: 2 }, },
                    { name: 'Direccion', index: 'Direccion', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, formoptions: { rowpos: 9, colpos: 1 } },
                    { name: 'Barrio', index: 'Barrio', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, },
                    { name: 'Localidad', index: 'Localidad', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, },
                    { name: 'idestrato', index: 'idestrato', width: anchoP(ancho, 20), align: 'left', viewable: false, hidden: true, editrules: { required: true, edithidden: true }, editable: true, formoptions: { rowpos: 10, colpos: 2 }, },
                    { name: 'Telefono', index: 'Telefono', width: anchoP(ancho, 20), align: 'left', editrules: { required: false }, editable: true, formoptions: { rowpos: 11, colpos: 1 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'celular1', index: 'celular1', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, formoptions: { rowpos: 11, colpos: 2 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'celular2', index: 'celular2', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, formoptions: { rowpos: 12, colpos: 1 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'email', index: 'email', align: 'center', hidden: true, editrules: { edithidden: true, required: false }, editable: true, edittype: "text", formoptions: { rowpos: 12, colpos: 2 }, formatter: 'email', editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return email(e) }) } } },
                    { name: 'administradora', index: 'administradora', width: anchoP(ancho, 50), align: 'left', editrules: { required: true }, editable: true, edittype: "text", },
                    { name: 'ipsprimaria', index: 'ipsprimaria', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { required: true, edithidden: true }, editable: true, edittype: "text", },
                    { name: 'idregimen', index: 'idregimen', width: anchoP(ancho, 30), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'regimen', index: 'regimen', width: anchoP(ancho, 30), align: 'left', formoptions: { rowpos: 8, colpos: 3 } },
                    { name: 'idregia', index: 'idregia', width: anchoP(ancho, 30), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "select", },
                    { name: 'regimenafiliacion', index: 'regimenafiliacion', width: anchoP(ancho, 30), align: 'left', formoptions: { rowpos: 8, colpos: 4 } },
                    { name: 'Ocupacion', index: 'Ocupacion', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "text", },
                    { name: 'idec', index: 'idec', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'estadocivil', index: 'estadocivil', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 9, colpos: 2 } },
                    { name: 'lugarNacimiento', index: 'lugarNacimiento', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'idet', index: 'idet', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'Etnia', index: 'Etnia', width: anchoP(ancho, 60), align: 'left', formoptions: { rowpos: 10, colpos: 1 } },
                    { name: 'iddis', index: 'iddis', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { required: true, edithidden: true }, editable: true, edittype: "select", },
                    { name: 'discapacidad', index: 'discapacidad', width: anchoP(ancho, 60), align: 'left', hidden: true, formoptions: { rowpos: 11, colpos: 1 } },
                    { name: 'idne', index: 'idne', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'NivelEsc', index: 'NivelEsc', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 11, colpos: 2 } },
                    { name: 'idgp', index: 'idgp', width: anchoP(ancho, 60), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'grupopoblacional', index: 'grupopoblacional', width: anchoP(ancho, 10), align: 'left', formoptions: { rowpos: 11, colpos: 3 } },
                    { name: 'acudiente', index: 'acudiente', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, formoptions: { rowpos: 19, colpos: 1 }, editrules: { required: false, edithidden: true } },
                    { name: 'idps', index: 'idps', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, editrules: { required: false, edithidden: true }, edittype: "select", },
                    { name: 'parentesco', index: 'parentesco', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 26, colpos: 1 } },
                    { name: 'tipo_id_acudiente', index: 'tipo_id_acudiente', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: false }, editable: true, edittype: "select", },
                    { name: 'identificacionA', index: 'identificacionA', width: anchoP(ancho, 20), align: 'left', hidden: true, editable: true, editrules: { edithidden: false, required: false }, formoptions: { rowpos: 20, colpos: 2 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'acudienteDir', index: 'acudienteDir', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 21, colpos: 1 } },
                    { name: 'acudienteTel', index: 'acudienteTel', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 21, colpos: 2 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return numeros(e) }) } } },
                    { name: 'acudienteEmail', index: 'acudienteEmail', width: anchoP(ancho, 20), align: 'left', editable: true, hidden: true, editrules: { required: false, edithidden: true }, formoptions: { rowpos: 22, colpos: 1 }, editoptions: { dataInit: function (elem) { $(elem).bind("keypress", function (e) { return email(e) }) } } },
                    { name: 'idcv', index: 'idcv', width: anchoP(ancho, 20), align: 'left', hidden: true, editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'condicionVulnerabilidad', index: 'condicionVulnerabilidad', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 14, colpos: 1 } },
                    { name: 'hv', index: 'hv', width: anchoP(ancho, 20), align: 'left', hidden: true },
                    { name: 'hechos_victimizantes', index: 'hechos_victimizantes', width: anchoP(ancho, 20), hidden: true, align: 'left', formoptions: { rowpos: 14, colpos: 3 } },
                    { name: 'idpso', index: 'idpso', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'programaSocial', index: 'programaSocial', width: anchoP(ancho, 20), align: 'left', hidden: true, formoptions: { rowpos: 14, colpos: 2 } },
                    { name: 'intersexual', index: 'intersexual', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'prueba', index: 'prueba', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'gestante', index: 'gestante', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'fecha_afiliacion_eps', index: 'fecha_afiliacion_eps', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "text", },
                    { name: 'orientacion_sexual', index: 'orientacion_sexual', width: anchoP(ancho, 20), hidden: true, align: 'left', editrules: { edithidden: true, required: true }, editable: true, edittype: "select", },
                    { name: 'grupSanguineo', index: 'grupSanguineo', hidden: true, }
                ],
                height: 500,
                width: 800,
                caption: "Paciente",
                shrinkToFit: false,
                onSelectRow: function (rowid) {
                    const rh = {
                        '0': 'A +',
                        '1': 'A -',
                        '2': 'B +',
                        '3': 'B -',
                        '4': 'AB +',
                        '5': 'O +',
                        '6': 'O -',
                        '7': 'AB -',
                        '8': 'No Sabe'
                    }
                    mostrar('divEditar')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrilla').getRowData(rowid);

                    console.log('datosRow.Sexo,', datosRow.Sexo)

                    estad = 0;
                    asignaAtributo('txtIdPac', datosRow.id, 0);
                    asignaAtributo('cmbTipoIdPac', datosRow.TipoId, 1);
                    asignaAtributo('cmbTipoId', datosRow.TipoId, 0);
                    asignaAtributo('txtIdentificacion', datosRow.identificacion, 0);
                    asignaAtributo('txtIdentificacionPac', datosRow.identificacion, 1);
                    asignaAtributo('txtAfiliacion', datosRow.afiliacion, 0);
                    asignaAtributo('cmbSexo', datosRow.Sexo, estad);
                    asignaAtributo('txtNombre1', datosRow.Nombre1, estad);
                    asignaAtributo('txtNombre2', datosRow.Nombre2, estad);
                    asignaAtributo('txtApellido1', datosRow.Apellido1, estad);
                    asignaAtributo('txtApellido2', datosRow.Apellido2, estad);
                    var fecha = datosRow.FechaNacimiento.substring(6, 10) + '-' + datosRow.FechaNacimiento.substring(3, 5) + '-' + datosRow.FechaNacimiento.substring(0, 2);
                    asignaAtributo('txtFechaNac', fecha, estad);
                    asignaAtributo('txtNacionalidad', datosRow.Nacionalidad.split('-')[0], estad);
                    asignaAtributo('txtMunicipio', datosRow.MunicipioResi.split('-')[0], estad);
                    asignaAtributo('cmbSisben', datosRow.Sisben, estad);
                    asignaAtributo('txtDireccion', datosRow.Direccion, estad);
                    //asignaAtributo('txtBarrio', datosRow.Barrio, estad);
                    asignaAtributo('txtLocalidad', datosRow.Localidad, estad);
                    asignaAtributo('cmbEstrato', datosRow.idestrato, estad);
                    asignaAtributo('cmbEstadoCivil', datosRow.idec, estad);
                    asignaAtributo('txtTelefono', datosRow.Telefono, estad);
                    asignaAtributo('txtCelular1', datosRow.celular1, estad);
                    asignaAtributo('txtCelular2', datosRow.celular2, estad);
                    asignaAtributo('txtEmail', datosRow.email, estad);
                    asignaAtributo('txtAdministradora', datosRow.administradora.split('-')[0], estad);
                    asignaAtributo('txtIpsPrimaria', datosRow.ipsprimaria, estad);

                    asignaAtributo('cmbRegimen', datosRow.idregimen, estad);
                    asignaAtributo('cmbTipoAfiliado', datosRow.idregia, estad);
                    asignaAtributo('txtOcupacion', datosRow.Ocupacion.split('-')[0], estad);
                    asignaAtributo('txtNacimiento', datosRow.lugarNacimiento.split('-')[0], estad);
                    asignaAtributo('cmbEtnia', datosRow.idet, estad);
                    asignaAtributo('cmbDiscapacidad', datosRow.iddis, estad);
                    asignaAtributo('cmbEscolaridad', datosRow.idne, estad);
                    asignaAtributo('cmbGrupoPoblacional', datosRow.idgp, estad);
                    asignaAtributo('cmbVulnerabilidad', datosRow.idcv, estad);
                    asignaAtributo('cmbHechosVicti', datosRow.hv, estad);
                    asignaAtributo('txtAcudiente', datosRow.acudiente, estad);
                    asignaAtributo('cmbParentesco', datosRow.idps, estad);
                    asignaAtributo('cmbTipoIdAcudiente', datosRow.tipo_id_acudiente, estad);
                    asignaAtributo('txtIdentificacionAcudiente', datosRow.identificacionA, estad);
                    asignaAtributo('txtDireccionAcudiente', datosRow.acudienteDir, estad);
                    asignaAtributo('txtTelefonoAcudiente', datosRow.acudienteTel, estad);
                    asignaAtributo('txtEmailAcudiente', datosRow.acudienteEmail, estad);
                    asignaAtributo('cmbProgramaSocial', datosRow.idpso, estad);
                    asignaAtributo('cmbPrueba', datosRow.prueba, estad);
                    asignaAtributo('cmbGestante', datosRow.gestante, estad);
                    asignaAtributo('cmbIntersexual', datosRow.intersexual, estad);
                    asignaAtributo('txtFechaAfi', datosRow.fecha_afiliacion_eps, estad);

                    asignaAtributo('cmbOrientacion', datosRow.orientacion_sexual, estad);
                    asignaAtributo('txtObservaciones', datosRow.Observaciones, estad);
                    asignaAtributo('lblIdAnteriores', datosRow.idAnteriores, estad);

                    asignaAtributo('cmbGruposSanguineo', datosRow.grupSanguineo, 0);
                    asignaAtributo('lblRH', rh[datosRow.grupSanguineo], 0);




                    console.log('dat', datosRow)
                    buscarHC('listGrillaHomologacionIdentifica', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')


                    var fechaNacimiento = new Date(fecha);
                    var edad = calcularEdad(fechaNacimiento);
                    var nombreVentanita = datosRow.Nombre1 + ' ' + datosRow.Nombre2 + ' ' + datosRow.Apellido1 + ' ' + datosRow.Apellido2;
                    var edadVentanita = edad.anios + ' Años ' + edad.meses + ' Meses ' + edad.dias + ' Días'


                    asignaAtributo("lblNombrePacienteVentanita", nombreVentanita, 1);
                    asignaAtributo("lblEdadPacienteVentanita", edadVentanita, 1);


                    if (datosRow.gestante == 'SI') {
                        document.getElementById("chkgestante").checked = true;
                    } else {
                        document.getElementById("chkgestante").checked = false;
                    }

                    if (document.getElementById("cmbSexo").value == 'F' && edad.anios > 10 && edad.anios < 50) {
                        document.getElementById("divDatosGestantes").style.display = "flex";
                    } else {
                        document.getElementById("divDatosGestantes").style.display = "none";
                        document.getElementById("cmbGestante").value = 'NO';
                        document.getElementById("cmbGestante").disabled = true;
                    }

                    document.getElementById("divInfoPacienteVentana").style.display = "flex";
                    moverTarjeta()
                    $('.select2-custom').trigger('change');

                },
                loadBeforeSend: function (xhr, settings) {
                    modificarCRUD('pacienteTempG');
                },
                gridComplete: function () {
                    $("#listGrilla").jqGrid("setSelection", 1, true);
                },
            });
            $('#drag' + ventanaActual.num).find("#listGrilla").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        ///////////////

    }

}


function validacionGestante() {
    console.log('on change')
    if (document.getElementById("cmbSexo").value == 'F') {
        document.getElementById("cmbGestante").disabled = false;
    } else {
        document.getElementById("cmbGestante").value = 'NO';
        document.getElementById("cmbGestante").disabled = true;
    }
}

function calcularEdad(fechaNacimiento) {
    var fechaActual = new Date();

    var anios = fechaActual.getFullYear() - fechaNacimiento.getFullYear();
    var meses = fechaActual.getMonth() - fechaNacimiento.getMonth();
    var dias = fechaActual.getDate() - fechaNacimiento.getDate();

    if (meses < 0 || (meses === 0 && dias < 0)) {
        anios--;
        meses += 12;
    }

    if (dias < 0) {
        var ultimoDiaMesAnterior = new Date(fechaActual.getFullYear(), fechaActual.getMonth(), 0).getDate();
        dias += ultimoDiaMesAnterior;
        meses--;
    }

    return {
        anios: anios,
        meses: meses,
        dias: dias
    };
}

function numeros(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true;
    patron = /\d/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function validarFecha(id) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById(id).setAttribute("max", today);
}

function limpiarVentanaPaciente() {
    estad = 0
    asignaAtributo('txtIdPac', '', 0);
    asignaAtributo('cmbTipoIdPac', '', 0);
    asignaAtributo('txtIdentificacionPac', '', 0);
    asignaAtributo('txtAfiliacion', '', 0);
    asignaAtributo('cmbSexo', '', estad);
    asignaAtributo('txtNombre1', '', estad);
    asignaAtributo('txtNombre2', '', estad);
    asignaAtributo('txtApellido1', '', estad);
    asignaAtributo('txtApellido2', '', estad);

    asignaAtributo('txtFechaNac', '', estad);
    asignaAtributo('txtNacionalidad', '', estad);
    asignaAtributo('txtMunicipio', '', estad);
    asignaAtributo('cmbSisben', '', estad);
    asignaAtributo('txtDireccion', '', estad);
    asignaAtributo('txtBarrio', '', estad);
    asignaAtributo('txtLocalidad', '', estad);
    asignaAtributo('cmbEstrato', '', estad);
    asignaAtributo('cmbEstadoCivil', '', estad);
    asignaAtributo('txtTelefono', '', estad);
    asignaAtributo('txtCelular1', '', estad);
    asignaAtributo('txtCelular2', '', estad);
    asignaAtributo('txtEmail', '', estad);
    asignaAtributo('txtAdministradora', '', estad);
    //asignaAtributo('txtIpsPrimaria','' , estad);

    asignaAtributo('cmbRegimen', '', estad);
    asignaAtributo('cmbTipoAfiliado', '', estad);
    asignaAtributo('txtOcupacion', '', estad);
    asignaAtributo('txtNacimiento', '', estad);
    asignaAtributo('cmbEtnia', '', estad);
    asignaAtributo('cmbDiscapacidad', '', estad);
    asignaAtributo('cmbEscolaridad', '', estad);
    asignaAtributo('cmbGrupoPoblacional', '', estad);
    asignaAtributo('cmbVulnerabilidad', '', estad);
    asignaAtributo('cmbHechosVicti', '', estad);
    asignaAtributo('txtAcudiente', '', estad);
    asignaAtributo('cmbParentesco', '', estad);
    asignaAtributo('cmbTipoIdAcudiente', '', estad);
    asignaAtributo('txtIdentificacionAcudiente', '', estad);
    asignaAtributo('txtDireccionAcudiente', '', estad);
    asignaAtributo('txtTelefonoAcudiente', '', estad);
    asignaAtributo('txtEmailAcudiente', '', estad);
    asignaAtributo('cmbProgramaSocial', '', estad);
    asignaAtributo('cmbPrueba', 'N', estad);
    asignaAtributo('cmbIntersexual', '2', estad);
    asignaAtributo('cmbOrientacion', '0', estad);



    asignaAtributo('txtObservaciones', '', estad);
    asignaAtributo('lblIdAnteriores', '', estad);
    $('#txtNacionalidad').val('CO')
    $('.select2-custom').trigger('change');
}

/**
 * Valida los datos del formulario de paciente antes de guardar.
 * 
 * @param {string} tipo - Tipo de operación: 'crearPac' o 'editarPac'.
 * @returns {boolean} - Retorna false si hay errores de validación.
 */
function validarDatosAntesDeGuardarPaciente(tipo) {
    const camposAValidar = [
        { campo: 'cmbTipoIdPac', mensaje: 'Seleccione Tipo de Identificacion' },
        { campo: 'txtIdentificacionPac', mensaje: 'Digite Identificacion' },
        { campo: 'cmbSexo', mensaje: 'Seleccione Sexo' },
        { campo: 'txtNombre1', mensaje: 'Digite primer nombre' },
        { campo: 'txtApellido1', mensaje: 'Digite primer apellido' },
        { campo: 'txtFechaNac', mensaje: 'Seleccione fecha de nacimiento' },
        { campo: 'txtNacionalidad', mensaje: 'Seleccione nacionalidad' },
        { campo: 'txtMunicipio', mensaje: 'Seleccione municipio actual' },
        { campo: 'cmbSisben', mensaje: 'Seleccione nivel de SISBEN' },
        { campo: 'txtDireccion', mensaje: 'Digite dirección actual' },
        { campo: 'cmbEstrato', mensaje: 'Seleccione estrato' },
        { campo: 'cmbEstadoCivil', mensaje: 'Seleccione estado civil' },
        { campo: 'txtCelular1', mensaje: 'Digite Número Celular' },
        { campo: 'txtAdministradora', mensaje: 'Seleccione administradora' },
        { campo: 'txtIpsPrimaria', mensaje: 'Digite IPS primaria' },
        { campo: 'cmbRegimen', mensaje: 'Seleccione régimen' },
        { campo: 'cmbTipoAfiliado', mensaje: 'Seleccione tipo afiliado' },
        { campo: 'txtOcupacion', mensaje: 'Seleccione ocupación' },
        { campo: 'txtNacimiento', mensaje: 'Seleccione lugar de nacimiento' },
        { campo: 'cmbEtnia', mensaje: 'Seleccione etnia' },
        { campo: 'cmbDiscapacidad', mensaje: 'Seleccione discapacidad' },
        { campo: 'cmbEscolaridad', mensaje: 'Seleccione nivel de escolaridad' },
        { campo: 'cmbGrupoPoblacional', mensaje: 'Seleccione grupo poblacional' },
        { campo: 'cmbVulnerabilidad', mensaje: 'Seleccione vulnerabilidad' },
        { campo: 'cmbHechosVicti', mensaje: 'Seleccione hechos victimizantes' },
        { campo: 'cmbProgramaSocial', mensaje: 'Digite programa social' },
        { campo: 'cmbGruposSanguineo', mensaje: 'Seleccione grupo sanguineo' }
    ];

    if (!validarEdadYTipoIdentificacion()) {
        return false;
    }

    if (!validarDatosAcudiente()) {
        return false;
    }

    let mensajesError = [];

    camposAValidar.forEach(({ campo, mensaje }) => {
        if (valorAtributo(campo) === '') {
            mensajesError.push(mensaje);
        }
    });

    // Mostrar errores si los hay
    if (mensajesError.length > 0) {
        swAlert('warning', '', 'Campos incompletos', `${mensajesError.join('<br>')}`, 0, true)
        return false;
    }

    // Ejecutar acción según el tipo
    if (tipo == 'crearPac') {
        modificarCRUD('crearPacienteAtencion')
    }
    if (tipo == 'editarPac') {
        // Verifica si se ha seleccionado un paciente
        if (traerDatoFilaSeleccionada('listGrilla', 'id') === '') {
            // Muestra una alerta si no hay un paciente seleccionado
            swAlert('warning', '', 'Falta seleccionar un paciente', '', 0, true);
            return false; // Detiene la ejecución si no se ha seleccionado un paciente
        }
        // Procede a editar el paciente
        modificarCRUD('editarPacienteAtencion')
    }
    return true // Retornar true si todo es válido
}


function validarEdadYTipoIdentificacion() {
    const tipoIdentificacion = valorAtributo('cmbTipoIdPac');
    const fechaNacimiento = new Date(valorAtributo('txtFechaNac'));
    const edad = calcularEdad(fechaNacimiento).anios;

    const tiposIdentificacionMenores = ['TI', 'RC', 'MS', 'CN'];

    // Verificar si el tipo de identificación es para menores
    if (tiposIdentificacionMenores.includes(tipoIdentificacion)) {
        if (edad >= 18) {
            swAlert('warning', '', 'Edad no coincide', `La edad: ${edad} no corresponde para el tipo de identificación ${tipoIdentificacion} (menor de 18 años)`, 0, true);
            return false;
        }
    } else {
        // Verificar si el tipo de identificación es para mayores y la edad es menor que 18
        if (edad < 18) {
            swAlert('warning', '', 'Edad no coincide', `La edad: ${edad} no corresponde para el tipo de identificación ${tipoIdentificacion} (mayor de 18 años)`, 0, true);
            return false;
        }
    }

    return true;
}


function validarDatosAcudiente() {
    const edad = calcularEdad(new Date(valorAtributo('txtFechaNac'))).anios;
    
    let mensajesError = [];

    if (edad < 18) {
        const camposAcudiente = [
            { campo: 'txtAcudiente', mensaje: 'Digite el nombre del acudiente' },
            { campo: 'cmbParentesco', mensaje: 'Digite el parentesco del acudiente' },
            { campo: 'cmbTipoIdAcudiente', mensaje: 'Digite el tipo de identificacion del acudiente' },
            { campo: 'txtIdentificacionAcudiente', mensaje: 'Digite la identificacion del acudiente' },
            { campo: 'txtTelefonoAcudiente', mensaje: 'Digite el telefono del acudiente' },
        ];

        camposAcudiente.forEach(({ campo, mensaje }) => {
            if (valorAtributo(campo) === '') {
                mensajesError.push(mensaje);
            }
        });

        if (mensajesError.length > 0) {
            swAlert('warning', '', 'Datos de acudiente requeridos', `${mensajesError.join('<br>')}`, 0, true)
            return false;
        }
    }
    return true;
}