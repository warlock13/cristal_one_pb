var varajax;
var bandera = 0;
var idBoton = "";
const url = 'http://190.60.242.160:8001/hora';
var timestamp_descargado = "";
var banderadrag = true;

if (!sessionStorage.sessionID) {
    sessionStorage.sessionID = Math.trunc(Math.random() * 1000000)
}

function nobackbutton() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button" //chrome
    window.onhashchange = function () { window.location.hash = "no-back-button"; }
}

function lauchTimer() {
    $.ajax({
        url: 'http://190.60.242.160:8001/hora',
        dataType: 'json',
        success: function (object) {
            timestamp_descargado = +object.horas; // se puede trabajar como objeto.										
        }
    });
}

function mueveReloj() {
    momentoActual = new Date()
    ++timestamp_descargado
    hora = momentoActual.getHours()
    minuto = momentoActual.getMinutes()
    segundo = momentoActual.getSeconds()

    if (hora < 10) {
        hora = "0" + hora
    }
    if (minuto < 10) {
        minuto = "0" + minuto
    }
    if (segundo < 10) {
        segundo = "0" + segundo
    }

    horaImprimible = hora + ":" + minuto + ":" + segundo

    $("#lblHoraServidor").text(horaImprimible)
    setTimeout("mueveReloj()", 1000)
}

function llenarinfo() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            document.getElementById('inicial').innerHTML = varajax.responseText;
            window.resizeTo(1200, 900);
            window.moveTo(50, 50)
            //iniciarObjetosMenu();
            iniciarDiccionarioConNotificaciones(1);
            $('#dock').Fisheye(
                {
                    maxWidth: 10,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container',
                    itemWidth: 20,
                    proximity: 20,
                    halign: 'left'
                }
            );
            $("#msgBienvenido").dropShadow({ left: 2, top: 2, blur: 1, opacity: 1, color: "WHITE", swap: false });

            $('#divMenuTreeTitle').corner("tear 10px").parent().css('padding', '4px').corner("fray 5px"); 
            $("#browser").treeview({
                toggle: function () {
                    console.log("%s was toggled.", $(this).find(">span").text());
                }
            });
            //funciones para ocultar y mostrar el menu										
            var puedeDesaparecer = false
            $('#disparaMenu').mousemove(function (event) {
                setTimeout("", 50);
                if (puedeDesaparecer == false) {
                    $("#menuDesplegable").animate({ width: "180px" }, { queue: false, duration: 30 });
                    $('#divMenuTreeTitle').show();
                }
                puedeDesaparecer = false;
            });
            $('#main').mousemove(function (event) {
                if (puedeDesaparecer == true) {
                    $("#menuDesplegable").animate({ width: "0px" }, { queue: false, duration: 30 });
                    $('#divMenuTreeTitle').hide();
                }
                puedeDesaparecer = false;
            });
            $('#menuDesplegable').mousemove(function (event) {
                //$("#menuDesplegable").animate( { width:"180px" }, { queue:false, duration:30 } );
                puedeDesaparecer = false
            });
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        document.getElementById('inicial').innerHTML = crearMensaje();
    }
    document.getElementById('idTableLogin').setAttribute("hidden", "true"); 
}

function principal() {
    //--- INGRESAR
    $.ajax({
        url: "/clinica/paginas/principal.jsp",
        type: "POST",
        beforeSend: function () {
            document.getElementById('inicial').innerHTML = crearMensaje();
        },
        success: function (data) {
            document.getElementById('inicial').innerHTML = data;
            window.resizeTo(1200, 900);
            window.moveTo(50, 50)

            sedes()

            iniciarDiccionarioConNotificaciones(1);
            $('#dock').Fisheye(
                {
                    maxWidth: 10,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container',
                    itemWidth: 20,
                    proximity: 20,
                    halign: 'left'
                }
            );
            $('#divMenuTreeTitle').corner("tear 10px").parent().css('padding', '4px').corner("fray 5px");
            $("#browser").treeview({
                toggle: function () {
                    console.log("%s was toggled.", $(this).find(">span").text());
                }
            });

            lauchTimer();
            mueveReloj();
            var puedeDesaparecer = false
            $('#disparaMenu').mousemove(function (event) {
                setTimeout("", 50);
                if (puedeDesaparecer == false) {
                    $("#menuDesplegable").animate({ width: "180px" }, { queue: false, duration: 30 });
                    $('#divMenuTreeTitle').show();
                }
                puedeDesaparecer = false;
            });
            $('#main').mousemove(function (event) {
                if (puedeDesaparecer == true) {
                    $("#menuDesplegable").animate({ width: "0px" }, { queue: false, duration: 30 });
                    $('#divMenuTreeTitle').hide();
                }
                puedeDesaparecer = false;
            });
            $('#menuDesplegable').mousemove(function (event) {
                //$("#menuDesplegable").animate( { width:"180px" }, { queue:false, duration:30 } );
                puedeDesaparecer = false
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Error al cargar la pagina. Por favor verifique su conexión a internet.")
        }
    });
}


function cerrar_sesion() {
    $.ajax({
        url: "/clinica/paginas/logout.jsp",
        type: "POST",
        beforeSend: function () {},
        success: function (data) {
            if (data.getElementsByTagName('respuesta')[0].firstChild.data) {
                localStorage.removeItem("sessionID")
                sessionStorage.removeItem("checkSede")
                location.reload()
            } else {
                alert("No ha sido posible cerrar sesi&oacute;n de manera segura. Por favor cierre el navegador y pongase en contacto con el equipo de soporte.")
            }
        },

        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("No ha sido posible cerrar sesi&oacute;n de manera segura. Por favor cierre el navegador y pongase en contacto con el equipo de soporte.")
        }
    });
}

function login0() {
    $.ajax({
        url: "/clinica/paginas/login0.jsp",
        type: "POST",
        beforeSend: function () {
            document.getElementById('inicial').innerHTML = crearMensaje();
            //document.getElementById('idTableLogin').setAttribute("hidden", "true");
        },
        success: function (data) {
            document.getElementById('inicial').innerHTML = data;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + "-")
        }
    });
}

function ingresar() {
    $.ajax({
        url: "/clinica/paginas/sesion_nav.jsp",
        type: "POST",
        data: { "tabID": tabID },
        beforeSend: function () { },
        success: function (data) {
            if (data.sesion == tabID) {
                principal();
            } else {
                login0();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + "---------")
        }
    });
}

function sedes() {
    if (!sessionStorage.checkSede) {
        $.ajax({
            url: "/clinica/paginas/sedes_xml.jsp",
            type: "POST",
            beforeSend: function () { },
            success: function (data) {
                if (data.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                    cargarMenu('infosedes.jsp', '4-0-0', 'infosedes', 'infosedes', 's', 's', 's', 's', 's', 's');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { }
        });
    }
}

function respuestasedes() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) { }
}

function iniciar_sesion() {
    $.ajax({
        url: "/clinica/paginas/iniciar_sesion.jsp",
        type: "POST",
        contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
        data: {
            "login": document.getElementById('txtLogin').value,
            "password": document.getElementById('txtContrasena').value,
            "idSesion": sessionStorage.sessionID
        },
        beforeSend: function () {
            $("#btn_ENTRAR").hide()
            $("#loader_login").show()
        },
        success: function (data) {
            if (data.estado) {
                localStorage.removeItem("sessionID")
                sessionStorage.removeItem("checkSede")
                window.location.reload()
            } else {
                $("#btn_ENTRAR").show()
                $("#loader_login").hide()
                Swal.fire({
                    icon: 'info',
                    text: data.mensaje,
                    confirmButtonColor: '#3085d6'
                })
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus)
            $("#btn_ENTRAR").show()
            $("#loader_login").hide()
        }, complete: function (jqXHR, textStatus) {}
    });
}

function verificar_disponibilidad_sesion() {
    $.ajax({
        url: "/clinica/paginas/sesion_nav.jsp",
        type: "POST",
        beforeSend: function () {
        },
        success: function (data) {
            if (data.sesion == -1) {
                validar()
            } else {
                login0()
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + "-")
        }
    });
}

function validar() {
    $.ajax({
        url: "/clinica/paginas/ingresar_xml.jsp",
        type: "POST",
        data: {
            "login": document.getElementById('txtLogin').value,
            "password": document.getElementById('txtContrasena').value,
            "idSesion": tabID
        },
        beforeSend: function () {
        },
        success: function (data) {
            if (data.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                ingresar()
            } else {
                alert("Usuario, contrasena incorrecta o se encuentra inactivo. \n Favor informar al administrador del sistema.");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + "-")
        }
    });
}

function validarIngreso() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                if (bandera == 0) {
                    sedes();
                    bandera = 1;
                }
                else {
                    ingresar();
                }
            } else {
                alert("Usuario, contrasena incorrecta o se encuentra inactivo. \n Favor informar al administrador del sistema.");
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
    }
}

function actualizarValidacionSede() {
    $.ajax({
        url: "/clinica/paginas/verificacion_sede.jsp",
        type: "POST",
        beforeSend: function () { },
        success: function (data) { },
        error: function (XMLHttpRequest, textStatus, errorThrown) { }
    });
}

function validar2() {
    valores_a_mandar = "login=" + document.getElementById('txtLogin').value + "&password=" + document.getElementById('txtContrasena').value;
    varajax = crearAjax();
    varajax.open("POST", "<%= response.encodeURL(ingresar_xml.jsp)%>", true);
    varajax.onreadystatechange = validarIngreso2;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);
}

function validarIngreso2() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
                ingresar();
            } else {
                //  alert("Usuario, contrase�a incorrecta o se encuentra inactivo. \n Favor informar al administrador del sistema.");
            }
        } else {
            swAlertError('LA ACCION NO SsedesE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        // document.getElementById('inicial').innerHTML =	crearMensaje();
    }
}

/*Script del Reloj */
function actualizaReloj() {
    /* Capturamos la Hora, los minutos y los segundos */
    marcacion = new Date()
    /* Capturamos la Hora */
    Hora = marcacion.getHours()
    /* Capturamos los Minutos */
    Minutos = marcacion.getMinutes()
    /* Capturamos los Segundos */
    Segundos = marcacion.getSeconds()
    /*variable para el ap�strofe de am o pm*/
    dn = ""
    if (Hora > 12) {
        dn = ""
        Hora = Hora
    }
    if (Hora == 0)
        Hora = 12
    /* Si la Hora, los Minutos o los Segundos son Menores o igual a 9, le a�adimos un 0 */
    if (Hora <= 9) Hora = "0" + Hora
    if (Minutos <= 9) Minutos = "0" + Minutos
    if (Segundos <= 9) Segundos = "0" + Segundos
    /* Termina el Script del Reloj */

    /*Script de la Fecha */

    var Dia = new Array("Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo");
    var Mes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
        "Octubre", "Noviembre", "Diciembre");
    var Mesn = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09",
        "10", "11", "12");
    var Hoy = new Date();
    var Anio = Hoy.getFullYear();
    var Fecha = Dia[Hoy.getDay()] + ", " + Hoy.getDate() + " de " + Mes[Hoy.getMonth()] + " de " + Anio + ". ";
    var Fecha2 = Hoy.getDate() + "/" + Mesn[Hoy.getMonth()] + "/" + Anio + "  ";


    /* Termina el script de la Fecha */

    /* Creamos 2 variables para darle formato a nuestro Script */
    var Script, Script2, Total, Total2

    /* En Reloj le indicamos la Hora, los Minutos y los Segundos */
    Script = Fecha + Hora + ":" + Minutos + ":" + Segundos + " " + dn

    /* En total Finalizamos el Reloj uniendo las variables */
    Total = Script
    Total2 = Fecha2 + Hora + ":" + Minutos + ":" + Segundos + " " + dn

    /* Capturamos una celda para mostrar el Reloj */
    document.getElementById('Fecha_Reloj').innerHTML = Total
    document.getElementById('format').value = Total2

    /* Indicamos que nos refresque el Reloj cada 1 segundo */
    setTimeout("actualizaReloj()", 1000)
}

function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
}

function MM_preloadImages() { //v3.0
    var d = document; if (d.images) {
        if (!d.MM_p) d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
    }
}

function MM_findObj(n, d) { //v4.01
    var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById) x = d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
}

function mover(idReg) {//funcion que permite dar movimineto a los div que tengan como id=drag y como div interno con id=tituloForma
    $('#drag' + idReg).draggable(
        {
            zIndex: 1000,
            ghosting: true,
            opacity: 0.7,
            containment: 'parent',
            handle: '#tituloForma'

        }
    );
}
//validar();
function checkKey(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; }
    else { unicode = key.keyCode; }
    //alert(unicode); // Para saber que codigo de tecla presiono , descomentar

    if (unicode == 13) {
        iniciar_sesion();
    }
}