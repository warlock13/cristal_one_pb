//FUNCIONES PARA CRONOMETRAR EL TIEMPO DE SESION DE UN USUARIO EN UNA PAGINA DETERMINADA
var cont = 1;
var accionLoader;
var estadoPaciente;

/**
 * Esta función verifica las notificaciones de un evento y ejecuta la acción dada si cumple con las validaciones.
 * 
 * @author John Mario Getial <johnmariogetial@firmasdigitales.com.co>
 * 
 * @param {String} evento Nombre del evento o accion que se quiere validar. 
 */

function ValidarEventoModificarCRUD(evento) {
    /** @type {function(String)} */
    let callback = modificarCRUD
    /** @type {json} */
    let parametros = {}

    if (verificarCamposGuardar(evento)) {
        switch (evento) {
            case "crearFolio":
                parametros["tipo_folio"] = valorAtributo('cmbTipoDocumento')
                parametros["id_admision"] = valorAtributo("lblIdAdmisionAgen")
                parametros["id_cita"] = valorAtributo("lblIdCita") == "" ? -1 : valorAtributo("lblIdCita")
                parametros["id_usuario"] = IdSesion()
                callback = alert
                break;

            default:
                break;
        }
        VerificarNotificacionesV2(evento, parametros, callback)
    }
}

/**
 * Esta función ejecuta las notificaciones asociadas a un evento antes de procesarlo.
 * 
 * @author John Mario Getial <johnmariogetial@firmasdigitales.com.co>
 * 
 * 
 * @param {String} evento Nombre del evento o accion que se quiere validar. 
 * @param {json} parametros Conjunto de parametros que se utilizaran para ejecutar las notificaciones o validaciones del evento
 * @param {function(String)} callback Función que será llamada si el evento no es bloqueado por alguna notificación (envía como parámetro el evento que se ha validado)
 * @returns {void}
 */

function VerificarNotificacionesV2(evento, parametros, callback) {
    parametros["evento"] = evento
    $.ajax({
        url: '/clinica/paginas/accionesXml/notificaciones.jsp',
        type: "POST",
        data: parametros,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        beforeSend: function () {
            mostrarLoader(evento)
        },
        success: function (data) {
            if (!data.error) {
                if (data.notificacion) {
                    Swal.fire({
                        html: data.mensaje,
                        icon: data.bloqueante ? "warning" : "info",
                        showCancelButton: !data.bloqueante,
                        confirmButtonText: data.bloqueante ? "Cerrar" : "Continuar",
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#999999',
                        position: "top",
                    }).then(function (sweetAlertResult) {
                        if (!data.bloqueante) {
                            if (sweetAlertResult.isConfirmed) {
                                if (callback !== undefined) {
                                    callback(evento)
                                }
                            }
                        }
                    });
                } else {
                    if (callback !== undefined) {
                        callback(evento)
                    }
                }
            } else {
                Swal.fire({
                    html: data.mensaje,
                    icon: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonColor: '#999999',
                    position: "top",
                })
            }
        },
        complete: function (jqXHR, textStatus) {
            ocultarLoader(evento)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            Swal.fire({
                html: "E" + XMLHttpRequest.status + " - Error an la solicitud. Por favor ponte en contacto con soporte.",
                icon: "error",
                confirmButtonText: "Cerrar",
                confirmButtonColor: '#999999',
                position: "top",
            })
        }
    });
}


//MANTINE LA SESION ACTIVA
function autoCheck() {
    $.ajax({
        url: "/clinica/paginas/refrescar_sesion.jsp",
        type: "POST",
        beforeSend: function () { },
        success: function (data) {
            console.log(data.acceso)
            if (data.acceso) {
                setTimeout(() => {
                    autoCheck();
                }, 60 * 1000);
            } else {
                alert("LA SESION ESTA VENCIDAD. POR FAVOR INICIE NUEVAMENTE")
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { }
    });
}

function buscarNotificacion(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';

    switch (arg) {
        case 'listGrillaNotificaciones':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=619&parametros=";
            add_valores_a_mandar(IdSesion());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID', 'ASUNTO', 'CONTENIDO', 'ID_EMISOR', 'NOMBRE EMISOR', 'FECHA_CREA', 'FECHA_INI', 'FECHA_FIN'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ASUNTO', index: 'ASUNTO', width: anchoP(ancho, 65) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', hidden: true },
                    { name: 'id_emisor', index: 'id_emisor', hidden: true },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 20) },
                    { name: 'fecha_crea', index: 'fecha_crea', width: anchoP(ancho, 10) },
                    { name: 'fecha_ini', index: 'fecha_ini', hidden: true },
                    { name: 'fecha_fin', index: 'fecha_fin', hidden: true },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //           limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;

                    asignaAtributo('lblIdNotificacion', datosRow.ID, 1);
                    buscarNotificacion('listGrillaNotificacionDestino');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaNotificacionDestino':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=621&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdNotificacion'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'id_destino', 'id_notificacion', 'id_persona', 'PERSONA DESTINO', 'ID_ESTADO', 'ESTADO', 'FECHA_RECIBIDA'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'id_notifi_destino', index: 'id_notifi_destino', hidden: true },
                    { name: 'id_notificacion', index: 'id_notificacion', hidden: true },
                    { name: 'id_personal_destino', index: 'id_personal_destino', width: anchoP(ancho, 12) },
                    { name: 'persona', index: 'persona', width: anchoP(ancho, 25) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 8) },
                    { name: 'fecha_recibida', index: 'fecha_recibida', width: anchoP(ancho, 10) },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //           limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;

                    asignaAtributo('lblIdNotificacionEnviada', datosRow.id_notifi_destino, 1);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaLeerNotificacion':
            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=616&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdProfesionalesBus'));
            add_valores_a_mandar(valorAtributo('txtAsuntoBus'));
            add_valores_a_mandar(valorAtributo('cmbEstadoBus'));
            add_valores_a_mandar(IdSesion());
            $('#drag' + ventanaActual.num).find("#listGrillaLeerNotificacion").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'ID', 'ID_NOT_DESTINO', 'ASUNTO', 'CONTENIDO', 'NOMBRE EMISOR', 'ID_ESTADO', 'ESTADO'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_NOT_DESTINO', index: 'ID_NOT_DESTINO', hidden: true },
                    { name: 'ASUNTO', index: 'ASUNTO', width: anchoP(ancho, 75) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', hidden: true },
                    { name: 'NOMBRE_EMISOR', index: 'NOMBRE_EMISOR EMISOR', width: anchoP(ancho, 20) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 10) },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //           limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaLeerNotificacion').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdNotDestino', datosRow.ID_NOT_DESTINO, 1);
                    asignaAtributo('lblNomEmisor', datosRow.NOMBRE_EMISOR, 1);
                    asignaAtributo('lblAsunto', datosRow.ASUNTO, 1);
                    asignaAtributo('lblContenido', datosRow.CONTENIDO, 1);
                    asignaAtributo('lblNomEstado', datosRow.ESTADO, 0);

                    if (datosRow.ID_ESTADO == 'S')
                        habilitar('btn_leer_not', 1)
                    else
                        habilitar('btn_leer_not', 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaLeerNotificacion").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
    }
}

function abrirVentanasNotificadas() {
    cargarMenu('calidad/planMejora/administrarPlanMejora.jsp', '2-1-3', 'administrarPlanMejora', 'administrarPlanMejora', 's', 's', 's', 's', 's', 's')
    alert("Esta noticaci�n, queda registrada como RECIBIDO");
    $('#drag' + ventanaActual.num).find('#txtBusIdEvento').val('00000272');
    buscarJuan('administrarPlanMejora', '/clinica/paginas/accionesXml/buscar_xml.jsp')
    mostrarBuscar()
    //   setTimeout("$('#drag'+ventanaActual.num).find('#txtBusIdEvento').val('E00000099')", 1000);   
    //   setTimeout("buscarJuan('administrarPlanMejora','/clinica/paginas/accionesXml/buscar_xml.jsp')", 5000);  

}








var CronoID = null
var CronoEjecutandose = false
var decimas, segundos, minutos, limite

//.....LLAMADO A SERVIDOR MEDIANTE AJAX
var varajax;


var timerHora = '';

function actualizaTiempo(lahora, elminuto, elsegundo) { /*actualiza homa minuto segundo del servidor y refresca en el lbl del menu*/

    hora = parseInt(lahora);
    minuto = parseInt(elminuto);
    segundo = parseInt(elsegundo);
    segundo++;
    if (segundo == 60) {
        segundo = 0;
        minuto++;
    } else if (segundo == 30) { }
    if (minuto == 60) {
        minuto = 0;
        hora++;
    }
    if (hora == 24) {
        hora = 0;
    }
    lahora = (hora < 10) ? ("0" + hora) : hora;
    elminuto = (minuto < 10) ? ("0" + minuto) : minuto;
    elsegundo = (segundo < 10) ? ("0" + segundo) : segundo;
    document.getElementById("lblHoraServidor").innerHTML = lahora + ":" + elminuto + ":" + elsegundo;
    timerHora = setTimeout("actualizaTiempo(hora,minuto,segundo)", 1000);
}

function iniciarDiccionarioConNotificaciones(limite) { //alert(8888)
    ejecutarAjaxDiccionario()
    setTimeout("consultarNotificacionAutomatica(" + limite + ")", 800);
}

function consultarNotificacionAutomatica(limite) { //alert(9999)
    ejecutarAjaxDiccionario()
    // traerNotificacion('escrita');
    //traerNotificacion(arg, 'escrita2', 9, 'P1');
    // alert('sssss')
    // setTimeout("consultarNotificacionAutomatica(" + limite + ")", 50000); // cada 5 mtos,  aqui el tiempo en dispararce      60000=1 minuto  
    return true;
}

function prueba_notificacion() {

    if (Notification) {

        if (Notification.permission !== "granted") {

            Notification.requestPermission()

        }
        var title = "Fundonar"
        var extra = {

            icon: "",
            body: "pruebaaa"


        }
        var nuevo = "hola"
        var noti = new Notification(title, extra, nuevo)
        noti.onclick = {



        }
        noti.onclose = {

        }
        setTimeout(function () { noti.close() }, 10000)

    }
}

function validarFolio() {
    folioTipo = traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO");
    if (folioTipo == 'HCPI') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCPI');
    } else if (folioTipo == 'HCRN') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCRN');
    } else if (folioTipo == 'HCAZ') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCAZ');
    } else if (folioTipo == 'HCPF') {
        verificarNotificacionAntesDeGuardar('verficarDiagnosticoPrincipalHCPF');
    }
    else if (folioTipo == 'HCAD') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCAD');
    }
    else if (folioTipo == 'HCC') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCC');
    }
    else if (folioTipo == 'HCJU') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCJU');
    }
    else if (folioTipo == 'CRNH') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCJU');
    }
    else if (folioTipo == 'HCIN') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCIN');
    }

    else if (folioTipo == 'HCVE') {
        verificarNotificacionAntesDeGuardar('verificarDatosHCVE');
    }
    else {
        verificarNotificacionAntesDeGuardar('verificarDatosEPOC');
    }
};


function verificarNotificacionAntesDeGuardar(arg) {
    switch (arg) {
        case 'validarEliminaciónNotaFactura':
            if(confirm("Esta seguro que desea eliminar ?")){
                setTimeout(() => {
                    id_nota = traerDatoFilaSeleccionada("listaNotasFactura", "ID_RECIBO");
                    traerNotificacion(arg, 'notificacionAlert', 138, id_nota);
                }, 100);
            }
            break;

        case 'validarEnvioNotaCredito':
            setTimeout(() => {
                id_nota = traerDatoFilaSeleccionada("listaNotasFactura", "ID_RECIBO");
                traerNotificacion(arg, 'notificacionAlert', 136, valorAtributo("lblIdFactura"), id_nota);
            }, 100);
            break;

        case 'validarValorNotasCredito':
            traerNotificacion(arg, 'notificacionAlert', 137, valorAtributo("lblIdFactura"));
            break;

        case 'enviarOrdenMedicaURG':
            traerNotificacion(arg, 'notificacionAlert', 134, valorAtributo("lblIdAdmisionAgen"), valorAtributo("lblIdAdmisionAgen"));
            break;
        case 'verificarDatosAppfih':
            traerNotificacion(arg, 'notificacionAlert', 132, valorAtributo("lblIdDocumento"));
            break;

        case 'verificarAntecedentesHCIN':
            accionLoader = "firmarFolio"
            traerNotificacion(arg, 'notificacionAlert', 114, valorAtributo("lblIdPaciente"));
            break;

        case 'verificarTamizajesHCIN':
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 131, idFolioSel, idFolioSel);
            break;

        case 'verificarDatosHCJU':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 124, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'), traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break;

        case 'verificarAntecedentesHCJU':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 114, valorAtributo("lblIdPaciente"));
            break;

        case 'verificarTamizajesHCJU':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 130,
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));
            break;

        case 'crearPacienteAdmision':
            if (verificarCamposGuardar('modificarPaciente')) {
                traerNotificacion(arg, 'notificacionAlert', 116, valorAtributo("cmbTipoId"), valorAtributo("txtIdentificacion"));
            }
            break;

        case 'ValidarFrecuenciaCantidadEditar':
            frecuencia = document.getElementById('cmbFrecuenciaEditar').value;
            cantidad = document.getElementById('txtCantidadEditar').value;
            traerNotificacion(arg, 'notificacionAlert', 110, frecuencia, cantidad, frecuencia, cantidad);
            break;

        case 'ValidarCantidadEditarInsumos':
            cantidad = document.getElementById('txtCantidadEditar').value;
            traerNotificacion(arg, 'notificacionAlert', 111, cantidad, cantidad);
            break;

        case 'ValidarFrecuenciaCantidad':
            frecuencia = document.getElementById('txtFrecuencia').value;
            cantidad = document.getElementById('txtCantidad').value;
            traerNotificacion(arg, 'notificacionAlert', 110, frecuencia, cantidad, frecuencia, cantidad);
            break;

        case 'ValidarCantidadInsumos':
            cantidad = document.getElementById('txtCantidadInsumos').value;
            traerNotificacion(arg, 'notificacionAlert', 111, cantidad, cantidad);
            break;

        case 'enviaSolicitud':
            procedimientos = listaProcedimientos();
            traerNotificacion(arg, 'notificacionAlert', 106, procedimientos);
            break;

        case 'verificarTensionSistolicaIngreso':
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 104, idFolioSel);
            break;
        case 'verificarTensionDiasIngreso':
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 103, idFolioSel);
            break;


        case 'verificarEstadioNoti':
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 83, idFolioSel);
            break;

        case 'verificarPesoTallaFol':
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            if ($("#daan").css('display') == 'block') {
                traerNotificacion(arg, 'notificacionAlert', 90, idFolioSel, document.getElementById('formulario.daan.c3').value);
            } else {
                traerNotificacion(arg, 'notificacionAlert', 90, idFolioSel, $('#txtc3').val());
            }
            break;

        case 'verificarPresiones':
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 88, idFolioSel);
            break;

        case 'foliosHcBorrador':
            traerNotificacion(arg, 'notificacionAlert', 79, valorAtributo("lblIdFactura"),);
            break;

        case "verificarCuentasRepetidas":
            traerNotificacion(arg, 'notificacionAlert', 78, valorAtributo("txtCuenta"));
            break;

        case "eliminarProcedimientosHC":
            if (verificarCamposGuardar(arg)) {
                var lista_elementos = [];

                var ids = $("#listaHcAnuladaFacturacion").getDataIDs();

                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_listaHcAnuladaFacturacion_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listaHcAnuladaFacturacion").getRowData(ids[i]);
                        lista_elementos.push({ "id": datosRow.ID, "tipo": datosRow.ID_TIPO });
                    }
                }
                traerNotificacion(arg, 'notificacionAlert', 84, JSON.stringify(lista_elementos));
            }
            break;

        case 'verificarPerimetroc20':
            setTimeout(() => {
                idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
                traerNotificacion(arg, 'notificacionAlert', 89, idFolioSel);
            }, 100);
            break;


        case 'verificarDiagnosticoHipertension':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 77, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'))
            break;

        case 'verificarDatosEPOC':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 112,
                traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'),
                traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'),
                traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'),
                traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'))
            break;
        case 'verificarDatosHCVE':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 124, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'), traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break
        case 'verificarAntecedentesHCVE':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 114, valorAtributo("lblIdPaciente"));
            break
        case 'verificarTamizajesHCVE1':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 128,
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));

            break
        case 'verificarTamizajesHCVE2':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 129,
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));
            break

        case 'verificarDatosHCRN':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 124, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'), traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break

        case 'verficarDiagnosticoPrincipalHCRN':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 56, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break

        case 'verificarDatosHCPI':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 113, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'), traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break;

        case 'verificarDatosHCIN':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 124, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'), traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break;


        case 'verificarDatosHCC':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 124, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'), traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break

        case 'verificarAntecedentesHCPI':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 114, valorAtributo("lblIdPaciente"));
            break

        case 'verificarTamizajesHCPI':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 115,
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));
            break
        case 'verificarDatosHCAD':
            accionLoader = "firmarFolio";

        case 'verificarDatosHCAZ':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 122, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            break

        case 'verificarAntecedentesHCAZ':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 114, valorAtributo("lblIdPaciente"));

            /*   traerNotificacion(arg, 'notificacionAlert', 124, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO')); */
            break

        case 'verificarAntecedentesHCAD':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 114, valorAtributo("lblIdPaciente"));
            break

        case 'verificarTamizajesHCAD':
            accionLoader = "firmarFolio";

            traerNotificacion(arg, 'notificacionAlert', 123,
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));
            break

        case 'verificarDesviacionEstandarHCDA':
            accionLoader = "firmarFolio";

            if (valorAtributo('txtSexoPaciente') == 'M') {
                traerNotificacion(arg, 'notificacionAlert', 125, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            } else if (valorAtributo('txtSexoPaciente') == 'F') {
                traerNotificacion(arg, 'notificacionAlert', 126, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            }
            break

        case 'verificarTamizajesHCAZ_1':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 121,
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));
            break

        case 'verificarTamizajesHCAZ_2':
            accionLoader = "firmarFolio";
            traerNotificacion(arg, 'notificacionAlert', 127,
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"),
                traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));
            break

        case 'verificarDesviacionEstandar':
            accionLoader = "firmarFolio";

            if (valorAtributo('txtSexoPaciente') == 'M' && valorAtributo('lblMesesEdadPaciente') <= 24) {
                traerNotificacion(arg, 'notificacionAlert', 117, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            } else if (valorAtributo('txtSexoPaciente') == 'M' && valorAtributo('lblMesesEdadPaciente') > 24) {
                traerNotificacion(arg, 'notificacionAlert', 119, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            } else if (valorAtributo('txtSexoPaciente') == 'F' && valorAtributo('lblMesesEdadPaciente') <= 24) {
                traerNotificacion(arg, 'notificacionAlert', 118, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            } else if (valorAtributo('txtSexoPaciente') == 'F' && valorAtributo('lblMesesEdadPaciente') > 24) {
                traerNotificacion(arg, 'notificacionAlert', 120, traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            }
            break

        case 'verificarMaxMinRExterno':
            if (valorAtributo("txtTipoResultado") == 'texto') {
                traerNotificacion(arg, 'notificacionAlert', 82,
                    valorAtributo("txtResultadoAnalito"),
                    valorAtributo("txtResultadoAnalito"),
                    valorAtributo("txtIdOrden")
                );
            }
            else {
                traerNotificacion(arg, 'notificacionAlert', 82,
                    valorAtributo("txtResultadoNumericoAnalito"),
                    valorAtributo("txtResultadoNumericoAnalito"),
                    valorAtributo("txtIdOrden")
                );
            }
            break;

        case 'verificarPosibleDuplicadoRExterno':
            if (valorAtributo("txtModificar") == 'True') {
                verificarNotificacionAntesDeGuardar("verificarMaxMinRExterno")
            }
            else {
                if (valorAtributo("txtTipoResultado") == 'texto') {
                    traerNotificacion(arg, 'notificacionAlert', 81,
                        valorAtributo("txtResultadoAnalito"),
                        valorAtributo("txtFechaResultadoAnalito"),
                        valorAtributo("txtFechaResultadoAnalito"),
                        valorAtributo("txtIdOrden"),
                        valorAtributo("lblIdPaciente")
                    );
                }
                else {
                    traerNotificacion(arg, 'notificacionAlert', 81,
                        valorAtributo("txtResultadoNumericoAnalito"),
                        valorAtributo("txtFechaResultadoAnalito"),
                        valorAtributo("txtFechaResultadoAnalito"),
                        valorAtributo("txtIdOrden"),
                        valorAtributo("lblIdPaciente")
                    );
                }
            }
            break;

        case 'verificarDuplicadosRExternos':
            if (valorAtributo("txtTipoResultado") == 'texto') {
                traerNotificacion(arg, 'notificacionAlert', 80,
                    valorAtributo("txtResultadoAnalito"),
                    valorAtributo("txtFechaResultadoAnalito"),
                    valorAtributo("txtIdOrden"),
                    valorAtributo("lblIdPaciente")
                );
            }
            else {
                traerNotificacion(arg, 'notificacionAlert', 80,
                    valorAtributo("txtResultadoNumericoAnalito"),
                    valorAtributo("txtFechaResultadoAnalito"),
                    valorAtributo("txtIdOrden"),
                    valorAtributo("lblIdPaciente")
                );
            }
            break;

        case 'eliminarFacturasRips':
            accionLoader = "eliminarFacturasRips"

            traerNotificacion(arg, 'notificacionAlert',
                79,
                $("#lblIdEnvioVentanita").html()
            );
            break;

        case "validarElementosPendientesAdmision":
            traerNotificacion(arg, 'notificacionAlert',
                91,
                valorAtributo("lblIdAdmisionModificarURG"));
            break;

        case 'validarPendienteFactura':
            traerNotificacion(arg, 'notificacionAlert',
                87,
                valorAtributo("lblIdAdmisionModificarURG"));
            break;

        case 'verificarPesoTalla':
            accionLoader = "firmarFolio"
            traerNotificacion(arg, 'notificacionAlert', 72, valorAtributo("lblIdDocumento"));
            break;

        case 'veririficarCambioAExitoNo':
            traerNotificacion(arg, 'notificacionAlert', 74, valorAtributo("lblIdAgendaDetalle"));
            break;

        case 'validarCitasaUnificar':
            let lista_citasUn = [];
            let lista_tiempoUn = [];
            ids = $("#listAgenda").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                c = "jqg_listAgenda_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listAgenda").getRowData(ids[i]);
                    lista_citasUn.push(datosRow.ID);
                    lista_tiempoUn.push({ "hora": datosRow.HORA_CITA, "duracion": datosRow.DURACION });
                }
                //else if(!$("#" + c).is(":checked") && lista_citasUn.length > 0){break;}
            }

            /*
            if(lista_citasUn.length <= 1){
                alert("SELECCIONAR MÁS DE UNA CITA CONSECUTIVA");
                return;
            }
            */

            for (let j = 0; j < lista_tiempoUn.length; j++) {
                if (lista_tiempoUn[j + 1]) {
                    if (lista_tiempoUn[j + 1].hora != sumarDuracionaHora(lista_tiempoUn[j].hora, lista_tiempoUn[j].duracion)) {
                        alert("LAS CITAS DEBEN SER EN HORARIO CONTINUO");
                        return;
                    }
                }
            }

            traerNotificacion(arg, 'notificacionAlert', 73, lista_citasUn);
            break

        case 'validarCausaFinalidad':
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 75, idFolioSel);
            break;

        case 'validarFrami':
            traerNotificacion(arg, 'notificacionAlert', 71, valorAtributo("lblIdPaciente"), valorAtributo("cmbEncuestaParaCrear"), valorAtributo("cmbEncuestaParaCrear"));
            break;

        case 'validarRCV':
            traerNotificacion(arg, 'notificacionAlert', 70, valorAtributo("lblIdPaciente"), valorAtributo("cmbEncuestaParaCrear"), valorAtributo("cmbEncuestaParaCrear"));
            break;

        case 'validarEstadoPaciente':
            traerNotificacion(arg, 'notificacionAlert', 69, idPacienteNoti);
            break;

        case 'validarPaqueteEventoNumerarFactura':
            traerNotificacion(arg,
                'notificacionAlert',
                68,
                popupFactura.$('#lblIdFactura').text(),
                popupFactura.$('#cmbAnioPrestacion').attr("value"),
                popupFactura.$('#cmbMesPrestacion').attr("value"))
            break;

        case 'validarCuentaRips':
            traerNotificacion(arg,
                'notificacionAlert',
                102,
                popupFactura.$('#cmbAnioPrestacion').attr("value"),
                popupFactura.$('#cmbMesPrestacion').attr("value"),
                popupFactura.$('#lblIdFactura').text()
            )
            break;

        case 'validarPaqueteEventoModificarAdmision':
            traerNotificacion(arg, 'notificacionAlert', 68, valorAtributo('lblIdFactura'))
            break;

        case "tipificarUnicaFactura":
            traerNotificacion(arg, 'notificacionAlert', 62, valorAtributo("lblIdFacturaAdjuntos"));
            break;

        case "validarValoracionInicial":
            accionLoader = "crearFolio"
            traerNotificacion(arg,
                'notificacionAlert',
                61,
                valorAtributo("cmbTipoDocumento"),
                valorAtributo("lblIdAdmisionAgen"));
            break;

        case 'verificarResultadosExternos':
            traerNotificacion(arg, 'notificacionAlert', 60, valorAtributo('lblIdDocumento'))
            break;

        case "validarNumeroAutorizacion":
            let valAtributo = valorAtributo('txtNoAutorizacion');
            if (valAtributo.length > 0) {
                traerNotificacion(arg, 'notificacionAlert', 55, valAtributo, valAtributo.length);
            }
            break;

        case 'eliminarAdmision':
            traerNotificacion(arg, 'notificacionAlert', 52, valorAtributo('lblIdAdmision'));
            break;

        case "subdividirAgenda":
            traerNotificacion(arg, 'notificacionAlert', 22, valorAtributo('lblIdAgendaDetalle'));
            break;

        case "modificarAgenda":
            traerNotificacion(arg, 'notificacionAlert', 22, valorAtributo('lblIdAgendaDetalle'));
            break;

        case "verificarFechasAgendaIndividualNoDisponible":
            traerNotificacion(arg, 'notificacionAlert', 22, valorAtributo('lblIdAgendaDetalle'));
            break;

        case "verificarFechasMoverAgenda":
            var hora_cita = [];
            var ids = $("#listAgenda").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                c = "jqg_listAgenda_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listAgenda").getRowData(ids[i]);
                    hora_cita.push(datosRow.HORA_CITA24);
                    break;
                }
            }

            traerNotificacion(arg,
                'notificacionAlert',
                48,
                valorAtributo("txtFechaDestino"),
                hora_cita);
            break;

        case "verificarFechasCopiarAgenda":
            var hora_cita = [];
            var ids = $("#listAgenda").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                c = "jqg_listAgenda_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listAgenda").getRowData(ids[i]);
                    hora_cita.push(datosRow.HORA_CITA24);
                    break;
                }
            }

            traerNotificacion(arg,
                'notificacionAlert',
                48,
                valorAtributo("txtFechaDestino"),
                hora_cita);
            break;

        case "verificarFechasNoDisponibleAgenda":
            lista_dias = [];
            ids = $("#listDiasC").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                c = "jqg_listDiasC_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listDiasC").getRowData(ids[i]);
                    lista_dias.push(datosRow.Fecha2);
                    break;
                }
            }

            traerNotificacion(arg,
                'notificacionAlert',
                48,
                lista_dias,
                valorAtributo("txtHoraInicioNoDisponibles"));
            break;

        case "verificarFechasEliminarDisponiblesAgenda":
            $("#loaderEliminarDis").show();
            lista_dias = [];
            ids = $("#listDiasC").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                c = "jqg_listDiasC_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listDiasC").getRowData(ids[i]);
                    lista_dias.push(datosRow.Fecha2);
                    break;
                }
            }

            traerNotificacion(arg,
                'notificacionAlert',
                48,
                lista_dias,
                valorAtributo("txtHoraInicioEliminar"));
            break;

        case "verificarFechasCrearAgenda":
            if (verificarCamposGuardar("listAgenda")) {
                lista_dias = [];
                ids = $("#listDiasC").getDataIDs();
                primerDiaChk = [];

                for (i = 0; i < ids.length; i++) {
                    c = "jqg_listDiasC_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listDiasC").getRowData(ids[i]);
                        lista_dias.push(datosRow.Fecha2);
                    }
                }

                traerNotificacion(arg,
                    'notificacionAlert',
                    48,
                    lista_dias,
                    valorAtributo("txtHoraInicio"));
            }
            break;

        case "verificarAgendasOtrasSE":

            traerNotificacion(arg,
                "notificacionAlert",
                50,
                lista_dias,
                valorAtributo("cmbIdProfesionales"),
                valorAtributo("txtHoraInicio"),
                valorAtributo("txtHoraFin"),
                valorAtributo("txtHoraInicio"),
                valorAtributo("txtHoraFin"),
            );
            break;

        case "verificarCitasAFuturoPaciente":
            traerNotificacion(arg,
                'notificacionAlert',
                47,
                valorAtributoIdAutoCompletar('txtIdBusPaciente') ,
                traerDatoFilaSeleccionada("listAgenda", "ID_ESPECIALIDAD"));
                // Agregar un escuchador de eventos para la tecla Escape
                document.addEventListener("keydown", manejarTeclaEscape);
            break;

        case "modificarEstadoProgramacionCitasBloque":
            if (verificarCamposGuardar(arg)) {
                var citas = [];
                var ids = $("#listaProgramacionTerapiasAgenda").getDataIDs();

                for (i = 0; i < ids.length; i++) {
                    var c = "jqg_listaProgramacionTerapiasAgenda_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listaProgramacionTerapiasAgenda").getRowData(ids[i]);
                        citas.push(datosRow.ID_CITA);
                    }
                }
                traerNotificacion(arg, 'notificacionAlert', 51, citas);

            }
            break;

        case "modificarProgramacionCitasBloque":
            if (verificarCamposGuardar(arg)) {
                var citas = [];
                var ids = $("#listaProgramacionTerapiasAgenda").getDataIDs();

                for (i = 0; i < ids.length; i++) {
                    var c = "jqg_listaProgramacionTerapiasAgenda_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listaProgramacionTerapiasAgenda").getRowData(ids[i]);
                        citas.push(datosRow.ID_CITA);
                    }
                }
                traerNotificacion(arg, 'notificacionAlert', 51, citas);
            }
            break;

        case "modificarEstadoCitaProgramacion":
            if (verificarCamposGuardar(arg)) {
                var citas = [];
                var ids = $("#listaProgramacionTerapiasAgenda").getDataIDs();

                for (i = 0; i < ids.length; i++) {
                    var c = "jqg_listaProgramacionTerapiasAgenda_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listaProgramacionTerapiasAgenda").getRowData(ids[i]);
                        citas.push(datosRow.ID_CITA);
                    }
                }
                traerNotificacion(arg, 'notificacionAlert', 51, citas);

            }
            break;

        case "reprogramarProgramacionLE":
            var le = [];
            var ids = $("#listaProgramacionLE").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionLE_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listaProgramacionLE").getRowData(ids[i]);
                    le.push(datosRow.ID_LE);
                }
            }
            traerNotificacion(arg, 'notificacionAlert', 49, le);
            break;

        case "reprogramarProgramacionLEBloque":
            var le = [];
            var ids = $("#listaProgramacionLE").getDataIDs();

            for (i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionLE_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    datosRow = $("#listaProgramacionLE").getRowData(ids[i]);
                    le.push(datosRow.ID_LE);
                }
            }
            traerNotificacion(arg, 'notificacionAlert', 49, le);
            break;

        case "modificarProgramacionLEBloque":
            if (verificarCamposGuardar(arg)) {
                var le = [];
                var ids = $("#listaProgramacionLE").getDataIDs();

                for (i = 0; i < ids.length; i++) {
                    var c = "jqg_listaProgramacionLE_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listaProgramacionLE").getRowData(ids[i]);
                        le.push(datosRow.ID_LE);
                    }
                }
                traerNotificacion(arg, 'notificacionAlert', 49, le);
            }
            break;

        case "copiarCitaAgenda":
            if (verificarCamposGuardar(arg)) {
                var dias = [];

                var ids = $("#listDias").getDataIDs();

                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_listDias_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listDias").getRowData(ids[i]);
                        dias.push(datosRow.Fecha2);
                    }
                }

                traerNotificacion(arg,
                    'notificacionAlert',
                    45,
                    valorAtributo("lblIdAgendaDetalle"),
                    valorAtributo("lblIdAgendaDetalle"),
                    valorAtributo("lblIdAgendaDetalle"),
                    valorAtributo("lblIdAgendaDetalle"),
                    valorAtributo("lblIdAgendaDetalle"),
                    dias);
            }
            break;

        case "verificarProcedimientoContratadoSede":
            traerNotificacion(arg,
                'notificacionAlert',
                44,
                valorAtributoIdAutoCompletar("txtIdProcedimientoOrdenExt"),
                valorAtributo("lblIdSedeAgendaOrdenes"));
            break;

        case "verificarCamposObligatorios":
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            tipoFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO");
            let queryNot;
            if (['B24X', 'ENFV', 'NTSV'].includes(tipoFolioSel)) {
                queryNot = 108
            } else {
                queryNot = 43
            }
            traerNotificacion(arg,
                'notificacionAlert',
                queryNot,
                tipoFolioSel,
                idFolioSel);
            break;

        case "verificarCamposObligatoriosB24X":
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            tipoFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO");
            if (['B24X'].includes(tipoFolioSel)) {
                traerNotificacion(arg,
                    'notificacionAlert',
                    108,
                    tipoFolioSel,
                    idFolioSel);
            }
            break;

        case "verificarSiPuedeFirmarFolio":
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 42, idFolioSel, idFolioSel);
            break;

        case "verificarSiPuedeCrearFolio":
            accionLoader = "crearFolio"
            traerNotificacion(arg,
                'notificacionAlert',
                41,
                valorAtributo("cmbTipoDocumento"),
                valorAtributo("lblIdCita") == "" ? -1 : valorAtributo("lblIdCita"));
            break;

        case "dobleBorradorCita":
            accionLoader = "crearFolio"
            traerNotificacion(arg,
                'notificacionAlert',
                53,
                valorAtributo("lblIdAdmisionAgen"),
                valorAtributo("lblIdCita"),
                valorAtributo("cmbTipoDocumento"),
                IdSesion());
            break;

        case "AdmisionEgresadaNotificacion":
            traerNotificacion(arg,
                'notificacionAlert',
                54,///43
                valorAtributo("lblIdAdmisionModificarURG"));
            break;

        case 'crearProcedimientoEjecutado':
            if (verificarCamposGuardar(arg)) {
                traerNotificacion(arg, 'notificacionAlert', 40, valorAtributo("lblIdDocumento"),
                    valorAtributoIdAutoCompletar("txtIdProcedimientoEjecutado"));
            }
            break;

        case 'validarDiasProgramacion':
            var ids_citas = [];

            var ids = $("#listaProgramacionTerapiasAgenda").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionTerapiasAgenda_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    var datosRow = $("#listaProgramacionTerapiasAgenda").getRowData(ids[i]);
                    ids_citas.push(datosRow.ID_CITA);
                }
            }
            traerNotificacion(arg, 'notificacionAlert', 37, ids_citas);
            break;

        case 'eliminarLEBorrador':
            var ids_le = [];

            var ids = $("#listaProgramacionLE").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionLE_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    var datosRow = $("#listaProgramacionLE").getRowData(ids[i]);
                    ids_le.push(datosRow.ID_LE);
                }
            }

            if (ids_le.length > 0) {
                if (confirm("Esta seguro de eliminar la programacion seleccionada ?")) {
                    traerNotificacion(arg, 'notificacionAlert', 39, ids_le);
                }
            } else {
                alert("Debe seleccionar al menos una lista de espera")
            }
            break;

        case 'eliminarCitasBorrador':
            var ids_citas = [];

            var ids = $("#listaProgramacionTerapiasAgenda").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionTerapiasAgenda_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    var datosRow = $("#listaProgramacionTerapiasAgenda").getRowData(ids[i]);
                    ids_citas.push(datosRow.ID_CITA);
                }
            }

            if (ids_citas.length > 0) {
                if (confirm("Esta seguro de eliminar la programacion seleccionada ?")) {
                    traerNotificacion(arg, 'notificacionAlert', 36, ids_citas);
                }
            } else {
                alert("Debe seleccionar al menos una cita")
            }
            break;

        case 'eliminarOrdenInt':
            if ($("#tabsCoordinacionTerapia").tabs("option", "selected") == 0) {
                id_tabla = "listCoordinacionTerapiaOrdenadosAgenda"
            } else if ($("#tabsCoordinacionTerapia").tabs("option", "selected") == 1) {
                id_tabla = "listaOrdenesHistoricas"
            }
            var fila = $("#" + id_tabla).jqGrid('getGridParam', 'selrow')

            traerNotificacion(arg, 'notificacionAlert', 35,
                $("#" + id_tabla).getRowData(fila).ID);
            break;

        case 'modificarProgramacionLE':
            var fila = $("#listaProgramacionLE").jqGrid('getGridParam', 'selrow')

            traerNotificacion(arg, 'notificacionAlert', 34,
                $("#listaProgramacionLE").getRowData(fila).ID_LE,
                valorAtributo("txtFechaProgramacionLEEditar"));
            break;

        case 'asociarCitaAdmision':
            traerNotificacion(arg, 'notificacionAlert', 31, valorAtributo('lblIdCita'));
            break;

        case 'modificarProgramacionTerapiaAgenda':
            var fila = $("#listaProgramacionTerapiasAgenda").jqGrid('getGridParam', 'selrow')

            traerNotificacion(arg, 'notificacionAlert', 30,
                $("#listaProgramacionTerapiasAgenda").getRowData(fila).ID_CITA,
                valorAtributo("txtFechaTerapiaEditar") + " " + valorAtributo("txtHoraTerapiaEditar"));
            break;

        case 'validarDiasProgramacionLE':
            var ids_le = [];

            var ids = $("#listaProgramacionLE").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionLE_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    var datosRow = $("#listaProgramacionLE").getRowData(ids[i]);
                    ids_le.push(datosRow.ID_LE);
                }
            }
            traerNotificacion(arg, 'notificacionAlert', 38, ids_le);
            break;

        case 'guardarProgramacionLE':
            var ids_le = [];

            var ids = $("#listaProgramacionLE").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionLE_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    var datosRow = $("#listaProgramacionLE").getRowData(ids[i]);
                    ids_le.push(datosRow.ID_LE);
                }
            }
            traerNotificacion(arg, 'notificacionAlert', 33, ids_le);
            break;

        case 'guardarCitasProgramadas':
            var ids_citas = [];

            var ids = $("#listaProgramacionTerapiasAgenda").getDataIDs();

            for (var i = 0; i < ids.length; i++) {
                var c = "jqg_listaProgramacionTerapiasAgenda_" + ids[i];
                if ($("#" + c).is(":checked")) {
                    var datosRow = $("#listaProgramacionTerapiasAgenda").getRowData(ids[i]);
                    ids_citas.push(datosRow.ID_CITA);
                }
            }
            traerNotificacion(arg, 'notificacionAlert', 29, ids_citas);
            break;

        case 'crearProgramacionLE':
            if (verificarCamposGuardar(arg)) {
                orden_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');
                id_orden = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(orden_seleccionada).ID

                var fechas = [];

                var ids = $("#listDias").getDataIDs();

                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_listDias_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listDias").getRowData(ids[i]);
                        fechas.push(datosRow.Fecha2 + " " + valorAtributo("txtHoraCitas"));
                    }
                }

                if ($('#chkFrecuencia').attr('checked')) {
                    traerNotificacion(arg,
                        'notificacionAlert',
                        32,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia") * valorAtributo("cmbRepeticiones"),
                        fechas,
                        id_orden,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia") * valorAtributo("cmbRepeticiones"),
                        fechas);
                } else {
                    traerNotificacion(arg,
                        'notificacionAlert',
                        32,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia"),
                        fechas,
                        $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).ID,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia"),
                        fechas);
                }
            }
            break;

        case 'crearProgramacionOrdenes':
            if (verificarCamposGuardar(arg)) {
                var orden_seleccionada = $("#listCoordinacionTerapiaOrdenadosAgenda").jqGrid('getGridParam', 'selrow');
                var id_orden = $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(orden_seleccionada).ID

                var fechas = [];

                var ids = $("#listDias").getDataIDs();

                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_listDias_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#listDias").getRowData(ids[i]);
                        fechas.push(datosRow.Fecha2 + " " + valorAtributo("txtHoraCitas"));
                    }
                }

                if ($('#chkFrecuencia').attr('checked')) {
                    traerNotificacion(arg,
                        'notificacionAlert',
                        28,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia") * valorAtributo("cmbRepeticiones"),
                        fechas,
                        $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).ID,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia") * valorAtributo("cmbRepeticiones"),
                        fechas);
                } else {
                    traerNotificacion(arg,
                        'notificacionAlert',
                        28,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia"),
                        fechas,
                        $("#listCoordinacionTerapiaOrdenadosAgenda").getRowData(terapia_seleccionada).ID,
                        fechas.length * valorAtributo("cmbCantidadCitasTerapia"),
                        fechas);
                }
            }
            break;

        case 'crearFolio':
            if (valorAtributo("cmbTipoDocumento") != '') {
                // VERIFICAR ESTADO DE ADMISION Y FACTURA
                accionLoader = "crearFolio"
                traerNotificacion(arg, 'notificacionAlert', 27, valorAtributo('cmbTipoDocumento'), valorAtributo('lblIdAdmisionAgen'));
            } else {
                alert("DEBE SELECCIONAR UN TIPO DE FOLIO")
            }
            break;

        case 'verificaEstadoAdmision':
            accionLoader = "firmarFolio"
            if (["FOEN", "NOAC", "IFTP", "IFTS", "NENF"].indexOf(valorAtributo("lblTipoDocumento")) != -1) {
                modificarCRUD("firmarFolio")
            } else if (document.getElementById('idTituloForma').innerHTML.indexOf('MODIFICA') != -1) {
                verificarNotificacionAntesDeGuardar('verificarSiPuedeFirmarFolio');
            } else {
                idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
                traerNotificacion(arg, 'notificacionAlert', 76, idFolioSel);
            }
            break;

        case 'accionesAlEstadoFolio':
            if (["2", "1", "3", "8", "9", "11", "14", "16", '17', '18'].indexOf(valorAtributo("cmbAccionFolio")) != -1) {
                modificarCRUD('accionesAlEstadoFolio');
            } else if (valorAtributo("cmbAccionFolio") == "5") {
                verificarNotificacionAntesDeGuardar('verificarProfesionalCreoEliminar')
            } else if (valorAtributo("cmbAccionFolio") == "4") {
                traerNotificacion(arg, 'notificacionAlert', 27, 'EVNE', valorAtributo('lblIdAdmision')); //CREAR NOTA DE ENFERMERIA
            } else if (valorAtributo("cmbAccionFolio") == "10") {
                traerNotificacion(arg, 'notificacionAlert', 105, traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"));
            }

            break;

        case 'asociarCitasAdmision':
            if (verificarCamposGuardar(arg)) {
                var lista_citas = [];

                var ids = $("#citasDisponiblesAsociar").getDataIDs();

                for (var i = 0; i < ids.length; i++) {
                    var c = "jqg_citasDisponiblesAsociar_" + ids[i];
                    if ($("#" + c).is(":checked")) {
                        datosRow = $("#citasDisponiblesAsociar").getRowData(ids[i]);
                        lista_citas.push(datosRow.ID_CITA);
                    }
                }
                traerNotificacion(arg, 'notificacionAlert', 25, lista_citas);
            }
            break;

        case 'dejarDisponibleAgendaDetalle':
            traerNotificacion(arg, 'notificacionAlert', 23, valorAtributo('lblIdAgendaDetalle'));
            break;

        case 'eliminarAgendaDetalle':
            $("#loaderEliminarDis").show();
            traerNotificacion(arg, 'notificacionAlert', 22, valorAtributo('lblIdAgendaDetalle'));
            break;

        case 'crearAdmisionSinEncuesta':
            if (verificarCamposGuardar(arg)) {
                traerNotificacion(arg, 'notificacionAlert', 21, valorAtributo('lblIdPaciente'));
            }
            break;

        case 'crearAdmisionEncuesta':
            if (verificarCamposGuardar(arg)) {
                traerNotificacion(arg, 'notificacionAlert', 21, valorAtributo('lblIdPaciente'));
            }
            break;

        case 'ingresarPacienteURG':
            if (verificarCamposGuardar(arg)) {
                traerNotificacion(arg, 'notificacionAlert', 21, valorAtributo('lblIdPacienteUrgencias'));
            }
            break;

        case 'verificarSoloAnularFactura':
            traerNotificacion(arg, 'notificacionAlert', 19, valorAtributo('lblIdFactura'));
            break;

        case 'verificarAnularFactura':
            traerNotificacion(arg, 'notificacionAlert', 19, valorAtributo('lblIdFactura'));
            break;

        case 'verificarAnularFacturaVentas':
            traerNotificacion(arg, 'notificacionAlert', 19, valorAtributo('lblIdFactura'));
            break;

        case 'verificarFechaPaciente':
            if ($("#lblIdListaEspera").html() == '') { return; }
            traerNotificacion(arg, 'notificacionAlert', 18, valorAtributo('txtFechaPacienteCita'), valorAtributo('lblIdListaEspera'))
            break;

        case 'verificarMontoContrato':
            traerNotificacion(arg, 'notificacionAlert', 17, valorAtributo('lblIdFactura'));
            break;

        case 'crearSoloFactura':
            traerNotificacion(arg, 'notificacionAlert', 16, valorAtributo('cmbIdTipoRegimen').split('-')[0], valorAtributo('lblIdPlanContratacion'), valorAtributo('lblIdPlanContratacion'));
            break;

        case 'validarPlanAdmision':
            traerNotificacion(arg, 'notificacionAlert', 16, valorAtributo('cmbIdTipoRegimen').split('-')[0], valorAtributo('lblIdPlanContratacion'), valorAtributo('lblIdPlanContratacion'));
            break;

        case 'validarCantidadMaximaAgendaDetalle':
            traerNotificacion(arg, 'escrita2', 9, valorAtributoIdAutoCompletar('txtMunicipio'));
            break;

        case 'validarTipoCitaFactura':
            traerNotificacion(arg, 'notificacionAlert', 5, valorAtributoIdAutoCompletar('txtIdBusPaciente'), valorAtributo('cmbTipoAdmision'))
            break;

        case 'validarIdentificacionPaciente':
            traerNotificacion(arg, 'notificacionAlert', 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            break;

        case 'crearAdmision':
            traerNotificacion(arg, 'notificacionAlert', 1, valorAtributo('lblIdCita'));
            break;

        case 'crearAdmisionCirugia':
            traerNotificacion(arg, 'notificacionAlert', 1, valorAtributo('lblIdCita'));
            break;

        case 'verificarDxRelacionados':
            traerNotificacion(arg, 'notificacionAlert', 3, valorAtributo('lblId'), valorAtributo('lblIdElemento'));
            break;

        case 'verificarEmbarazoProcedimiento':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            break;

        case 'verificarEmbarazoAyudaDiagnostica':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica'));
            break;

        case 'verificarEmbarazoTerapia':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdTerapia'));
            break;

        case 'verificarEmbarazoLaboratorio':
            traerNotificacion(arg, 'notificacionAlert', 4, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdLaboratorio'));
            break;

        case 'existenciaBodegaOptica':
            traerNotificacion(arg, 'notificacionAlert', 6, valorAtributo('cmbIdBodega'), valorAtributo('txtIdBarCode'));
            break;

        case 'existenciaBodegaVentas':
            traerNotificacion(arg, 'notificacionAlert', 6, valorAtributo('lblIdBodega'), valorAtributo('txtIdCodigoBarrasVentas'));
            break;

        case 'crearAdmisionFactura':
            traerNotificacion(arg, 'notificacionAlert', 7, valorAtributo('cmbTipoAdmision'), valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            break;

        case 'crearAdmisionSinFactura':
            traerNotificacion(arg, 'notificacionAlert', 7, valorAtributo('cmbTipoAdmision'), valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            break;

        case 'crearCita':
            sacarValoresColumnasDeGrilla('listCitaCexProcedimientoTraer');
            traerNotificacion(arg, 'notificacionAlert', 11,
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('txtColumnaIdProced'),
                valorAtributo('txtColumnaIdSitioQuirur'),
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('lblIdPlanContratacion'));
            break;

        case 'validarFechasCrearCita':
            traerNotificacion(arg, 'notificacionAlert', 24,
                valorAtributo('lblIdAgendaDetalle'));
            break;

        case 'crearCitaCirugia':
            sacarValoresColumnasDeGrilla('listCitaCirugiaProcedimiento');
            traerNotificacion(arg, 'notificacionAlert', 11,
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('txtColumnaIdProced'),
                valorAtributo('txtColumnaIdSitioQuirur'),
                valorAtributo('lblIdAgendaDetalle'),
                valorAtributo('lblIdPlanContratacion'));
            break;
        case 'verificarCantidadCups':
            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            break;
        case 'verificarCantidadCups2':
            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdAyudaDiagnostica'));
            break;
        case 'verificarCantidadCups3':

            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdTerapia'));

            break;
        case 'verificarCantidadCups4':

            traerNotificacion(arg, 'notificacionAlert', 12, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdLaboratorio'));

            break;

        case 'verificarTipoFolio':
            traerNotificacion(arg, 'notificacionAlert', 13,
                valorAtributo('cmbTipoDocumento'),
                valorAtributo('lblIdPaciente'),
                valorAtributo('lblIdPaciente'),
                valorAtributo('lblIdTipoAdmision')
            );
            break;

        case 'verificarOrdenAlta':
            traerNotificacion(arg, 'notificacionAlert', 58, valorAtributo('lblIdAdmisionAgen'));
            break;

        case 'verificarCronicosCreatinina':
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 57, idFolioSel);
            break;

        case 'verificarCronicosDiagnosticos':
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");

            traerNotificacion(arg, 'notificacionAlert', 59, idFolioSel);
            break;

        case 'verificarLabNarinoClinizad':
            traerNotificacion(arg, 'notificacionAlert', 67, valorAtributo('lblIdDocumento'), valorAtributoIdAutoCompletar('txtIdProcedimiento'), valorAtributoIdAutoCompletar('txtIdProcedimiento'));
            break;

        case 'verificarLaboratorios':
            accionLoader = "firmarFolio"
            // VALIDA SI EL PACIENTE NECESITA LABORATORIOS OBLIGATORIOS DEPENDIENDO DEL DIAGNOSTICO
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");

            traerNotificacion(arg, 'notificacionAlert', 14, idFolioSel);
            break;

        case 'verificarProfesionalCreoFirmar':
            accionLoader = "firmarFolio"
            // VERIFICA QUE EL USUARIO QUE CREA EL FOLIO SEA EL UNICO QUE PUEDE FIRMARLO
            traerNotificacion(arg, 'notificacionAlert', 63, valorAtributo('lblIdDocumento'), IdSesion());
            break;

        case 'verificarProfesionalCreoEliminar':
            accionLoader = "accionesAlEstadoFolio";
            traerNotificacion(arg, 'notificacionAlert', 64, valorAtributo('lblIdDocumento'), IdSesion());
            break;

        case 'verificarProfesionalCreoTamizajeEliminar':
            traerNotificacion(arg, 'notificacionAlert', 65, valorAtributo('lblIdEncuestaPaciente'), IdSesion());
            break;

        case 'verficarDiagnosticoPrincipal':
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 56, idFolioSel);
            break;

        case 'verficarDiagnosticoPrincipalHCPF':
            accionLoader = "firmarFolio"
            idFolioSel = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO");
            traerNotificacion(arg, 'notificacionAlert', 56, idFolioSel);
            break;

        case 'verificarProcedimientosRepetidos':
            traerNotificacion(arg, 'notificacionAlert', 15,
                valorAtributo('lblIdPaciente'),
                valorAtributo('cmbIdSitioQuirurgico'),
                valorAtributoIdAutoCompletar('txtIdProcedimiento')
            );

            break;

        case 'verificarPersonalCirugia':
            sacarValoresColumnasDeGrillaPersonal('listPersonalCirugia');

            var idProfesion = valorAtributo('txtColumnaIdProfesion');
            if (idProfesion === '') idProfesion = 1;

            traerNotificacion(arg, 'notificacionAlert', 20,
                valorAtributo('cmbTipoAnestesia'),
                idProfesion,
                valorAtributo('lblIdAgendaDetalle')
            );
            break;
    }
}

function traerNotificacion(arg, tipoNotificacion, idQuery, parametro1, parametro2, parametro3, parametro4, parametro5, parametro6) {
    if (parametro2 == undefined) parametro2 = '';
    if (parametro3 == undefined) parametro3 = '';
    if (parametro4 == undefined) parametro4 = '';
    if (parametro5 == undefined) parametro5 = '';
    if (parametro6 == undefined) parametro6 = '';

    valores_a_mandar = "tipoNotificacion=" + tipoNotificacion;
    valores_a_mandar = valores_a_mandar + "&accion=" + accionLoader;
    valores_a_mandar = valores_a_mandar + "&parametro1=" + parametro1;
    valores_a_mandar = valores_a_mandar + "&parametro2=" + parametro2;
    valores_a_mandar = valores_a_mandar + "&parametro3=" + parametro3;
    valores_a_mandar = valores_a_mandar + "&parametro4=" + parametro4;
    valores_a_mandar = valores_a_mandar + "&parametro5=" + parametro5;
    valores_a_mandar = valores_a_mandar + "&parametro6=" + parametro6;
    valores_a_mandar = valores_a_mandar + "&idQuery=" + idQuery;

    urlParams = new URLSearchParams(valores_a_mandar);
    $.ajax({
        url: '/clinica/paginas/accionesXml/notificaciones_xml.jsp',
        type: "POST",
        data: valores_a_mandar,
        contentType: "application/x-www-form-urlencoded",
        beforeSend: function () {
            mostrarLoader(urlParams.get("accion"))
        },
        success: function (data) { },
        complete: function (jqXHR, textStatus) {
            raiz = jqXHR.responseXML
            ocultarLoader(new URLSearchParams(this.data).get("accion"))
            if (raiz.getElementsByTagName('totalNot') != null) {
                totalNotSinLeer = raiz.getElementsByTagName('totalNot')[0].firstChild.data;
                descripcionMensaje = raiz.getElementsByTagName('descripcionNot')[0].firstChild.data;
                estadoPaciente = raiz.getElementsByTagName('descripcionNot')[0].firstChild.data;

                if (totalNotSinLeer > 0) {
                    switch (tipoNotificacion) {
                        case 'escrita':
                            document.getElementById('lblTotMensajes').lastChild.nodeValue = totalNotSinLeer;
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = 'TIENE ' + totalNotSinLeer + 'SIN LEER';
                            document.getElementById('lblVentanillaPie').lastChild.nodeValue = 'Clic aqui para leer';
                            break;

                        case 'escrita2':
                            mostrar('divNotificacionSuperior')
                            document.getElementById('lblTextoNoti').innerHTML = '' + descripcionMensaje;
                            break;

                        case 'alert':
                            //document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = raiz.getElementsByTagName('descripcion')[0].firstChild.data;
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = descripcionMensaje;
                            break;

                        case 'soloMensaje':
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = descripcionMensaje;
                            break;

                        case 'notificacionAlert':
                            // document.getElementById('lblTotNotifVentanillaAlert').lastChild.nodeValue = descripcionMensaje;
                            document.getElementById('lblTotNotifVentanillaAlert').innerHTML = descripcionMensaje;
                            break;
                    }
                }
                enviarAProcesar(arg, totalNotSinLeer, descripcionMensaje, tipoNotificacion);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            ocultarLoader(urlParams.get("accion"))
        }
    });
}

function traerNotificacion_(arg, tipoNotificacion, idQuery, parametro1, parametro2, parametro3, parametro4, parametro5, parametro6) {

    if (parametro2 == undefined) parametro2 = '';
    if (parametro3 == undefined) parametro3 = '';
    if (parametro4 == undefined) parametro4 = '';
    if (parametro5 == undefined) parametro5 = '';
    if (parametro6 == undefined) parametro6 = '';

    /*if (idBoton != '') {
        $("#" + idBoton.id).replaceWith('<img id="loader_aux001" src="/clinica/utilidades/imagenes/ajax-loader.gif" width="20px" height="20px">');
        console.log('EXITOENTRADA');
    } else {
        console.log('NOBOTONENTRADA');
    };*/

    mostrarLoader(accionLoader)

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/notificaciones_xml.jsp', true);
    varajaxInit.onreadystatechange = function () {
        respuestatraerNotificacion(arg, tipoNotificacion)
    };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "tipoNotificacion=" + tipoNotificacion;
    valores_a_mandar = valores_a_mandar + "&accion=" + accionLoader;
    valores_a_mandar = valores_a_mandar + "&parametro1=" + parametro1;
    valores_a_mandar = valores_a_mandar + "&parametro2=" + parametro2;
    valores_a_mandar = valores_a_mandar + "&parametro3=" + parametro3;
    valores_a_mandar = valores_a_mandar + "&parametro4=" + parametro4;
    valores_a_mandar = valores_a_mandar + "&parametro5=" + parametro5;
    valores_a_mandar = valores_a_mandar + "&parametro6=" + parametro6;
    valores_a_mandar = valores_a_mandar + "&idQuery=" + idQuery;
    varajaxInit.send(valores_a_mandar);
}

function respuestatraerNotificacion(arg, tipoNotificacion) {
    if (varajaxInit.readyState == 4) {

        /*if (idBoton != '') {
            $("#loader_aux001").replaceWith(idBoton);
            idBoton = '';
            console.log('EXITOSALIDA');
        } else {
            console.log('NOBOTONSALIDA');
            idBoton = '';
        };*/

        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;

            //alert(raiz.getElementsByTagName('accion')[0].firstChild.data)
            ocultarLoader(raiz.getElementsByTagName('accion')[0].firstChild.data)

            if (raiz.getElementsByTagName('totalNot') != null) {

                totalNotSinLeer = raiz.getElementsByTagName('totalNot')[0].firstChild.data;
                descripcionMensaje = raiz.getElementsByTagName('descripcionNot')[0].firstChild.data;
                estadoPaciente = raiz.getElementsByTagName('descripcionNot')[0].firstChild.data;
                //                 alert('*totalNotSinLeer='+ totalNotSinLeer  +'\n*arg='+arg   +'\n*length='+descripcionMensaje.length   )     

                if (totalNotSinLeer > 0) {
                    switch (tipoNotificacion) {
                        case 'escrita':
                            document.getElementById('lblTotMensajes').lastChild.nodeValue = totalNotSinLeer;
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = 'TIENE ' + totalNotSinLeer + 'SIN LEER';
                            document.getElementById('lblVentanillaPie').lastChild.nodeValue = 'Clic aqui para leer';
                            break;
                        case 'escrita2':
                            mostrar('divNotificacionSuperior')
                            document.getElementById('lblTextoNoti').innerHTML = '' + descripcionMensaje;
                            break;
                        case 'alert':
                            //document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = raiz.getElementsByTagName('descripcion')[0].firstChild.data;
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = descripcionMensaje;
                            break;
                        case 'soloMensaje':
                            document.getElementById('lblTotNotifVentanilla').lastChild.nodeValue = descripcionMensaje;
                            break;

                        case 'notificacionAlert':
                            // document.getElementById('lblTotNotifVentanillaAlert').lastChild.nodeValue = descripcionMensaje;
                            document.getElementById('lblTotNotifVentanillaAlert').innerHTML = descripcionMensaje;
                            break;
                    }
                }
                enviarAProcesar(arg, totalNotSinLeer, descripcionMensaje, tipoNotificacion)

            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
}


function verificarFacturaAnular(functionAnulacion) {
    switch (functionAnulacion) {
        case 'soloAnularFactura':
            verificarNotificacionAntesDeGuardar('verificarSoloAnularFactura')
            break;

        case 'anularFactura':
            verificarNotificacionAntesDeGuardar('verificarAnularFactura')
            break;

        case 'anularFacturaVentas':
            verificarNotificacionAntesDeGuardar('verificarAnularFacturaVentas')
            break;
    }
}

function enviarAProcesar(arg, totalNotSinLeer, descripcionMensaje, tipoNotificacion, boton) {
    //ocultarLoader(boton);

    ban = 0
    switch (arg) {
        case "validarEliminaciónNotaFactura":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                eliminarNotafactura()
            }
            break;

        case "validarValorNotasCredito":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //alert("Valida para enviar")
                modificarCRUD('numerarNotaFactura')
                //guardarYtraerDatoAlListado('auditarNotaCreditoFE')
            }
            break;

        case "validarEnvioNotaCredito":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarValorNotasCredito');
            }
            break;

        case 'enviarOrdenMedicaURG':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
                if (ban == 0) {
                    modificarCRUD('enviarOrdenMedicaURG')
                }
            }
            break;

        case 'verificarDatosAppfih':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarDiagnosticoHipertension');
            }
            break;

        case 'crearPacienteAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD(arg);
            }
            break;
        case 'verificarDatosHCJU':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarAntecedentesHCJU');
            }
            break;

        case 'verificarAntecedentesHCJU':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                if (traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO') == 'CRNH') {
                    verificarNotificacionAntesDeGuardar('verificarDatosEPOC');

                } else {
                    verificarNotificacionAntesDeGuardar("verificarTamizajesHCJU");
                }
            }
            break;

        case 'verificarTamizajesHCJU':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarDatosEPOC');
            }
            break;

        case 'ValidarFrecuenciaCantidadEditar':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('listMedicamentosEditar', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
            }
            break;

        case 'ValidarCantidadEditarInsumos':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('listInsumosEditar', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
            }
            break;

        case 'ValidarFrecuenciaCantidad':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPos')
            }
            break;

        case 'ValidarCantidadInsumos':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosInsumos')
            }
            break;


        case 'verificarDiagnosticoHipertension':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarProfesionalCreoFirmar");
            }
            break;

        case 'verificarDatosEPOC':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                if (['MEDG', 'B24X', 'MGPT', 'MGTL', 'MGPD', 'HCCE', 'CRNC', 'ENFN', 'HCCP', 'HCVE', 'HCAD', 'HCAZ', 'HCJU', 'HCPF', 'CRNH'].includes(folioTipo)) {
                    verificarNotificacionAntesDeGuardar('verificarDatosAppfih');
                } else {
                    verificarNotificacionAntesDeGuardar("verificarDiagnosticoHipertension");

                }
            }
            break;
        case 'verificarDatosHCAD':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarAntecedentesHCAD');
            }
            break;

        case 'verificarAntecedentesHCAD':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarTamizajesHCAD");
            }
            break;

        case 'verificarTamizajesHCAD':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarDesviacionEstandarHCDA");
            }
            break;
        case 'verificarDesviacionEstandarHCDA':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarDatosEPOC');
            }
            break;

        case 'verificarDatosHCRN':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verficarDiagnosticoPrincipalHCRN');
            }
            break;
        case 'verficarDiagnosticoPrincipalHCRN':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //modificarCRUD("firmarFolio")
                //verificarNotificacionAntesDeGuardar("verificarCamposObligatorios")
                verificarNotificacionAntesDeGuardar('verificarDatosEPOC');
            }
            break;

        case 'verificarDatosHCVE':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarAntecedentesHCVE');
            }
            break;

        case 'verificarAntecedentesHCVE':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarTamizajesHCVE1');
            }
            break;
        case 'verificarTamizajesHCVE1':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarTamizajesHCVE2');
            }
            break;
        case 'verificarTamizajesHCVE2':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarDatosEPOC');
            }
            break;

        case 'verificarDatosHCPI':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarAntecedentesHCPI');
            }
            break;
        case 'verificarDatosHCIN':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //verificarNotificacionAntesDeGuardar('verificarTamizajesHCIN');
                //verificarNotificacionAntesDeGuardar('verificarDiagnosticoHipertension');
                verificarNotificacionAntesDeGuardar('verificarAntecedentesHCIN');

            }
            break;

        case 'verificarTamizajesHCIN':
            /* if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
              
                verificarNotificacionAntesDeGuardar('verificarAntecedentesHCIN');
            } */
            break;

        case 'verificarAntecedentesHCIN':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {

                verificarNotificacionAntesDeGuardar('verificarDiagnosticoHipertension');
            }
            break;

        case 'verificarDatosHCC':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarDiagnosticoHipertension');
            }
            break;


        case 'verificarAntecedentesHCPI':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //verificarNotificacionAntesDeGuardar("verificarTamizajesHCPI");
                verificarNotificacionAntesDeGuardar("verificarDesviacionEstandar");
            }
            break;

        case 'verificarTamizajesHCPI':
        /* if (totalNotSinLeer > 0) {
            if (descripcionMensaje.length > 0) {
                dibujarVentanitaAlerta(arg, tipoNotificacion);
                ban = 1;
            }
        }
        if (ban == 0) {
            verificarNotificacionAntesDeGuardar("verificarDesviacionEstandar");
        }
        break; */

        case 'verificarDatosHCAZ':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarAntecedentesHCAZ');
            }
            break;

        case 'verificarAntecedentesHCAZ':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarTamizajesHCAZ_1");
            }
            break;

        case 'verificarTamizajesHCAZ_1':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarTamizajesHCAZ_2");
            }
            break;

        case 'verificarTamizajesHCAZ_2':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarDiagnosticoHipertension");
            }
            break;

        case 'verificarDesviacionEstandar':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarDiagnosticoHipertension");
            }
            break;

        case 'enviaSolicitud':

            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }

            if (ban == 0) {
                modificarCRUD(arg);
            }
            break;


        case 'verificarTensionSistolicaIngreso':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }

            if (ban == 0) {
                modificarCRUD(arg);
            }
            break;

        case 'verificarPerimetroc20':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }

            if (ban == 0) {
                modificarCRUD(arg)
            }
            break;

        case 'verificarTensionDiasIngreso':
            if (totalNotSinLeer > 0) { //Prueba para push
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            break;

        case 'verificarPesoTallaFol':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }

            if (ban == 0) {
                modificarCRUD(arg)
            }
            break;

        case 'verificarPresiones':

            if (totalNotSinLeer > 0) { //Prueba para push
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                }
            }
            break;



        /**
         *  se establece la validacion de duplicidad de procedimientos, en caso de ser correcto va a dibujar ventana de alerta
         *  en caso contrario se procede a agregar los procedimientos
         *  @author JUAN CARLOS JIMENEZ <juankjibe@gmail.com>
         *  @returns {void}
         */
        case 'agregarProcedimiento':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                /**
                 *  agregado normal de valores sin restriccion
                 */
                agregarProcedimientoLimpio();
                break;
            }
            break;
        case 'agregarPaqPro':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                /**
                 *  agregado normal de valores sin restriccion
                 */
                agregarPaqProcedimientoLimpio();
                break;
            }
            break;
        /**
         * 
         */
        case 'foliosHcBorrador':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlertaPopup(descripcionMensaje);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarMontoContrato');
            }
            break;

        case "verificarCuentasRepetidas":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD("crearArchivosRips")
            }
            break;

        case "eliminarProcedimientosHC":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD(arg)
            }
            break;

        case 'verificarMaxMinRExterno':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                if (valorAtributo("txtModificar") == 'True') {
                    modificarCRUD('modificarResultadoExterno')
                }
                else {
                    modificarCRUD('agregarResultadoExterno')
                }
            }
            break;

        case 'verificarPosibleDuplicadoRExterno':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarMaxMinRExterno')
            }
            break;

        case 'verificarEstadioNoti':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD("firmarFolio")
            }
            break;

        case 'verificarDuplicadosRExternos':

            /* MP validacion de campos */
            let folio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO')
            if (folio === 'EPOC') {
                //console.log('EPOC')
                let max = parseInt(document.getElementById('lblMaxAnalito').textContent)
                let min = parseInt(document.getElementById('lblMinAnalito').textContent)
                let inptValue = parseFloat(document.getElementById('txtResultadoNumericoAnalito').value)
                let inptVal = document.getElementById('txtResultadoNumericoAnalito').value

                /* MP para validar rangos y que el campo no este vacio */
                if (inptValue < min || inptValue > max || inptVal === '') {
                    Swal.fire({
                        title: 'Error!',
                        icon: 'error',
                        showConfirmButton: false,
                        showCloseButton: true,
                        html: '<p style="color:black">No se puede registrar el dato. Dato fuera de rango </style>',

                    });
                    return
                }
            }


            /* end MP */



            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarPosibleDuplicadoRExterno')
            }
            break;

        case 'eliminarFacturasRips':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            else if (ban == 0) {
                modificarCRUD('eliminarFacturasRips');
            }
            break;

        case 'validarElementosPendientesAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            else if (ban == 0) {
                modificarCRUD('modificarServicioPacienteURG');
            }
            break;

        case 'validarPendienteFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            else if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarElementosPendientesAdmision');
            }
            break;

        case 'veririficarCambioAExitoNo':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=315&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdAgendaDetalle"));
                add_valores_a_mandar(valorAtributo("cmbEstadoCita"));
                add_valores_a_mandar(valorAtributo("cmbMotivoConsulta"));
                add_valores_a_mandar(valorAtributo("cmbMotivoConsultaClase"));
                add_valores_a_mandar(
                    "Estado:" +
                    valorAtributo("cmbEstadoCita") +
                    "-" +
                    valorAtributo("txtObservacion") +
                    " " +
                    LoginSesion()
                );
                add_valores_a_mandar(valorAtributo("lblIdAgendaDetalle"));
                ajaxModificar();
            }
            break;

        case 'verificarPesoTalla':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                console.log(traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO"));
                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == 'CRNC' || traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == 'CRNH') {
                    verificarNotificacionAntesDeGuardar('verificarEstadioNoti')
                } else {
                    modificarCRUD("firmarFolio")
                }

            }
            break;

        case 'validarCausaFinalidad':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarPesoTalla')
            }
            break;

        case 'validarCitasaUnificar':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD("unificarCitas");
            }
            break;
        case 'validarFrami':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarMapCRUD('crearEncuestaPaciente')
            }
            break;

        case 'validarRCV':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarFrami')
            }
            break;

        case 'validarEstadoPaciente':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlertaSinCerrar(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                habilitar('btnGuardarCita', 1);
                habilitar('btnProcedimiento', 1);
                habilitar('btnCrearAdmision', 1);
                cargarInformacionPaciente()
            }
            break;

        case 'validarPaqueteEventoNumerarFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlertaPopup(descripcionMensaje);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("validarCuentaRips")
            }
            break;

        case 'validarCuentaRips':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlertaPopup(descripcionMensaje);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('actualizarFechaPreriodoServiciosFacturados')
            }
            break;

        case 'validarPaqueteEventoModificarAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarAdmisionFacturacion()
            }
            break;

        case "tipificarUnicaFactura":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                tipificarFacturas([valorAtributo("lblIdFacturaAdjuntos")])
            }
            break;

        case 'verificarResultadosExternos':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('insertarProcedimientoResultadosExternos');
            }
            break;

        case "validarNumeroAutorizacion":
            if (descripcionMensaje) {
                $("#txtNoAutorizacion").val('');
                dibujarVentanitaAlerta(arg, tipoNotificacion);
            }
            break;

        case "eliminarAdmision":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('eliminarAdmision');
            }
            break;

        case "subdividirAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('subDividirAgenda');
            }
            break;

        case "modificarAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('MoverCitaPaciente');
            }
            break;

        case "verificarFechasAgendaIndividualNoDisponible":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                mostrar('divVentanitaObservacionNoDisponible');
            }
            break;

        case "verificarFechasMoverAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                    $("#loaderEliminarDis").hide();
                }
            } if (ban == 0) {
                modificarCRUD('MoverAgenda');
            }
            break;

        case "verificarFechasCopiarAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                    $("#loaderEliminarDis").hide();
                }
            } if (ban == 0) {
                modificarCRUD('listAgendaCopiarDia');
            }

            break;

        case "verificarFechasNoDisponibleAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    $("#loaderEliminarDis").hide();
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                    $("#loaderEliminarDis").hide();
                }
            } if (ban == 0) {
                modificarCRUD('noDisponible');
            }
            break;

        case "verificarFechasEliminarDisponiblesAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                    $("#loaderEliminarDis").hide();
                }
            } if (ban == 0) {
                modificarCRUD('eliminarDisponibles');
            }
            break;

        case "verificarFechasCrearAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } else if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarAgendasOtrasSE");
            }
            break;

        case "verificarAgendasOtrasSE":
            if (totalNotSinLeer > 0 && descripcionMensaje.length > 0) {
                if (!confirm(descripcionMensaje + "\nESTA SEGURO QUE DESA CONTINUAR?")) {
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD("listAgenda")
            }
            break;

        case "verificarCitasAFuturoPaciente":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            break;

        case "reprogramarProgramacionLE":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('reprogramarProgramacionLE');
            }
            break;

        case "modificarEstadoProgramacionCitasBloque":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            modificarCRUD(arg);
            break;

        case "modificarProgramacionCitasBloque":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            modificarCRUD(arg);
            break;

        case "modificarEstadoCitaProgramacion":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            modificarCRUD(arg);
            break;

        case "reprogramarProgramacionLEBloque":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('reprogramarProgramacionLEBloque');
            }
            break;

        case "modificarProgramacionLEBloque":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('modificarProgramacionLEBloque');
            }
            break;

        case "copiarCitaAgenda":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD("copiarCitaAgenda")
            }
            break;

        case "verificarProcedimientoContratadoSede":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD('crearOrdenExterna')
            }
            break;

        case "verificarCamposObligatorios":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarLaboratorios');
                //popupFirmar.inicio(popupFirmar.document.forms['frmFinalizar']);
            }
            break;

        case "verificarSiPuedeFirmarFolio":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificarCamposObligatorios")
            }
            break;

        case "verificarSiPuedeCrearFolio":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                verificarNotificacionAntesDeGuardar("validarValoracionInicial")
            }
            break;

        case "validarValoracionInicial":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                modificarCRUD("crearFolio")
                //guardarYtraerDatoAlListado('nuevoDocumentoHC')
            }
            break;

        case "dobleBorradorCita":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarSiPuedeCrearFolio')
            }
            break;

        case "AdmisionEgresadaNotificacion":
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            } if (ban == 0) {
                verificarNotificacionAntesDeGuardar("validarPendienteFactura");
                /*if( $("#cmbModificarEstadoAdmisionURG").val() == '2'){
                    verificarNotificacionAntesDeGuardar("validarPendienteFactura");
                }else{
                    modificarCRUD('modificarServicioPacienteURG');
                }*/
            }
            break;

        case 'crearProcedimientoEjecutado':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                }
            } if (ban == 0) {
                modificarCRUD('crearProcedimientoEjecutado');
            }
            break;

        case 'eliminarLEBorrador':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                }
            }
            modificarCRUD('eliminarLEBorrador');
            break;

        case 'eliminarCitasBorrador':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                }
            }
            modificarCRUD('eliminarCitasBorrador');
            break;

        case 'eliminarOrdenInt':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('eliminarOrdenInt');
            }
            break;

        case 'modificarProgramacionLE':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('modificarProgramacionLE');
            }
            break;

        case 'asociarCitaAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            break;

        case 'modificarProgramacionTerapiaAgenda':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('modificarProgramacionTerapiaAgenda');
            }
            break;

        case 'validarDiasProgramacionLE':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('guardarProgramacionLE');
            }
            break;

        case 'guardarProgramacionLE':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("validarDiasProgramacionLE");
            }
            break;

        case 'validarDiasProgramacion':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('guardarCitasProgramadas');
            }
            break;

        case 'guardarCitasProgramadas':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarDiasProgramacion');
            }
            break;

        case 'crearProgramacionLE':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearProgramacionLE');
            }
            break;

        case 'crearProgramacionOrdenes':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearCitasTerapiasAgenda');
            }
            break;

        case 'verificaEstadoAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarSiPuedeFirmarFolio');
            }
            break;

        case 'verificarCamposObligatoriosB24X':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('accionesAlEstadoFolio');
            }
            break;

        case 'accionesAlEstadoFolio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarCamposObligatoriosB24X');
            }
            break;

        case 'asociarCitasAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    modificarCRUD('asociarCitasAdmision');
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('asociarCitasAdmision');
            }
            break;

        case 'dejarDisponibleAgendaDetalle':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('dejarDisponibleAgendaDetalle', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
            }
            break;

        case 'eliminarAgendaDetalle':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    $("#loaderEliminarDis").hide();
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('eliminarAgendaDetalle', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
            }
            break;

        case 'crearAdmisionSinEncuesta':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionSinEncuesta');
            }
            break;

        case 'crearAdmisionEncuesta':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionEncuesta');
            }
            break;

        case 'ingresarPacienteURG':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('ingresarPacienteURG');
            }
            break;

        case 'verificarSoloAnularFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('soloAnularFactura')
            }
            break;

        case 'verificarAnularFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('anularFactura')
            }
            break;

        case 'verificarAnularFacturaVentas':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('anularFacturaVentas')
            }
            break;

        case 'verificarFechaPaciente':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                    limpiaAtributo('txtFechaPacienteCita', 0)
                }
            }
            break;

        case 'validarTipoCitaFactura':
            // if (totalNotSinLeer > 0) {
            //     if (descripcionMensaje.length > 0) {
            //         dibujarVentanitaAlerta(arg, tipoNotificacion);
            //         ban = 1;
            //     }
            // }
            // if (ban == 0) {
            // }
            verificarNotificacionAntesDeGuardar('validarPlanAdmision');
            break;

        case 'validarCantidadMaximaAgendaDetalle':

            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            break;

        case 'validarIdentificacionPaciente':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            break;

        case 'crearAdmisionCirugia':
            if (totalNotSinLeer > 0) {
                alert('PACIENTE SE ENCUENTRA EN ESTADO NO ASISTIO O REPROGRAMADO CIRUGIA!!!!')
            } else {

                traerNotificacion('validarTipoCitaCirugia', tipoNotificacion, 5, valorAtributoIdAutoCompletar('txtIdBusPaciente'), valorAtributo('cmbTipoAdmision'))

                //traerNotificacion('tienePdfIdentificacionCirugia', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            }
            break;

        case 'crearAdmision':
            if (totalNotSinLeer > 0) {
                alert('PACIENTE SE ENCUENTRA EN ESTADO NO ASISTIO O REPROGRAMADO!!!!')
            } else {
                traerNotificacion('validarTipoCita', tipoNotificacion, 5, valorAtributoIdAutoCompletar('txtIdBusPaciente'), valorAtributo('cmbTipoAdmision'))

                //traerNotificacion('tienePdfIdentificacion', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            }
            break;

        case 'validarTipoCitaCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                traerNotificacion('tienePdfIdentificacionCirugia', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            break;

        case 'validarTipoCita':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                traerNotificacion('tienePdfIdentificacion', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            //traerNotificacion('tienePdfIdentificacion', tipoNotificacion, 2, valorAtributoIdAutoCompletar('txtIdBusPaciente'))
            break;


        case 'tienePdfIdentificacionCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('crearAdmisionCirugia', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                modificarCRUD('crearAdmisionCirugia');
            break;

        case 'tienePdfIdentificacion':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('crearAdmision', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0)
                modificarCRUD('crearAdmision');
            break;

        case 'verificarDxRelacionados':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta('verificarDxRelacionados', tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //alert('se lo puede eliminar');
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=350&parametros=";
                add_valores_a_mandar(valorAtributo('lblId'));
                add_valores_a_mandar(valorAtributo('txtIdTrans'));
                ajaxModificar();
            }
            break;

        case 'verificarEmbarazoProcedimiento':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');
            }
            break;

        case 'verificarCantidadCups':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                //verificarNotificacionAntesDeGuardar('verificarProcedimientosRepetidos');
                guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');
                // verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');

            }
            break;

        case 'verificarCantidadCups2':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosAyudaDiagnosticaNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoAyudaDiagnostica');
            }
            break;

        case 'verificarEmbarazoAyudaDiagnostica':

            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosAyudaDiagnosticaNotificacion');
            }
            break;

        case 'verificarCantidadCups3':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosTerapiaNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoTerapia');

            }
            break;

        case 'verificarEmbarazoTerapia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosTerapiaNotificacion');
            }
            break;

        case 'verificarCantidadCups4':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosLaboratorioNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoLaboratorio');
            }
            break;

        case 'verificarEmbarazoLaboratorio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosLaboratorioNotificacion');
            }
            break;

        case 'existenciaBodegaOptica':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                codigoBarras();
            }
            break;

        case 'existenciaBodegaVentas':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                codigoBarrasVentas();
            }
            break;

        case 'crearSoloFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearSoloFactura')
            }
            break;

        case 'verificarMontoContrato':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlertaPopup(descripcionMensaje);
                    ban = 1;
                }
            }
            if (ban == 0) {
                /*
                SE LLAMA A LA FUNCION 'numeraFactura(frm)' CONTENIDA EN LA VENTANA EMERGENTE 
                :popupFactura: elemento instanciado de window.open
                */
                popupFactura.$("#divActualizarPeriodoPrestacion").show()

                popupFactura.mostrarMeses()
                //popupFactura.numeraFactura(popupFactura.document.forms['frmFinalizar'])
            }
            break;

        case 'validarPlanAdmision':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                mostrar('divVentanitaVerificacion')
            }
            break;

        case 'crearAdmisionFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionFactura');
            }
            break;

        case 'crearAdmisionSinFactura':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearAdmisionSinFactura');
            }
            break;

        case 'validarFechasCrearCita':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearCita')
            }
            break;

        case 'crearCita':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarFechasCrearCita')
            }
            break;

        case 'crearCitaCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarPersonalCirugia');
                //modificarCRUD('crearCitaCirugia')
            }
            break;

        case 'crearFolio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('dobleBorradorCita');
            }
            break;

        case 'verificarTipoFolio':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('nuevoDocumentoHC')
            }
            break;

        case 'verificarOrdenAlta':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('enviarOrdenMedicaURG');
            }
            break;

        case 'verificarLaboratorios':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                /*  || traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == 'CRNH' */
                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == 'CRNC') {
                    verificarNotificacionAntesDeGuardar('verificarCronicosCreatinina')
                }
                else {
                    verificarNotificacionAntesDeGuardar('verficarDiagnosticoPrincipal')
                }
            }
            break;

        case 'verificarProfesionalCreoFirmar':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar("verificaEstadoAdmision");
            }
            break;

        case 'verificarProfesionalCreoEliminar':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    $('#loaderHcClonar').hide();
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('eliminarFolio');
            }
            break;

        case 'verificarProfesionalCreoTamizajeEliminar':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarMapCRUD('eliminarEncuestaPaciente');
            }
            break;

        case 'verficarDiagnosticoPrincipal':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('validarCausaFinalidad')
                //guardarYtraerDatoAlListado('elementosDelFolio');
            }
            break;
        case 'verficarDiagnosticoPrincipalHCPF':
            console.log('aquii en noti', totalNotSinLeer)
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarDatosEPOC');
                //guardarYtraerDatoAlListado('elementosDelFolio');
            }
            break;

        case 'verificarCronicosCreatinina':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verificarCronicosDiagnosticos')
            }
            break;

        case 'verificarCronicosDiagnosticos':
            if (totalNotSinLeer > 0) {
                console.log("AQUII")
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                verificarNotificacionAntesDeGuardar('verficarDiagnosticoPrincipal')
            }
            break;

        case 'verificarLabNarinoClinizad':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosProcedim')
            }
            break;

        case 'verificarProcedimientosRepetidos':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');
                verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');
            }
            break;

        case 'verificarPersonalCirugia':
            if (totalNotSinLeer > 0) {
                if (descripcionMensaje.length > 0) {
                    dibujarVentanitaAlerta(arg, tipoNotificacion);
                    ban = 1;
                }
            }
            if (ban == 0) {
                modificarCRUD('crearCitaCirugia')
            }
            break;
    }
}

function dibujarVentanitaAlertaPopup(descripcionMensaje) {
    popupFactura.document.getElementById('divNotificacionAlert').style.display = 'block';
    popupFactura.document.getElementById('lblTotNotifVentanillaAlert').innerHTML = descripcionMensaje;
}

function dibujarVentanitaAlertaPopupHC(descripcionMensaje) {
    popupFirmar.document.getElementById('divNotificacionAlert').style.display = 'block';
    popupFirmar.document.getElementById('lblTotNotifVentanillaAlert').innerHTML = descripcionMensaje;
}

function dibujarVentanitaAlerta(funcionCRUD, tipoNotificacion) {

    switch (tipoNotificacion) {

        case ('notificacionAlert'):
            $('#divNotificacionAlert').addClass('cssjas');
            $('#imgCerrarAlerta').show();
            $('#divNotificacionAlert').show("explode", { number: 9 }, 400);
            break;

        case ('escrita2'):
            $('#divNotificacionSuperior').show("", { number: 9 }, 400);
            break;
        default:

            $('#divNotificacion').addClass('cssjas');
            $('#imgCerrarAlerta').show();
            $('#divNotificacion').show("explode", { number: 9 }, 400);
            break;

    }

    // velocidad en que aparece         
    //   setTimeout("cierraNotifica()",5000); // esperar para cerrar la notificacion    
    //$('#divNotificacion').dialog( 'open' );   


    switch (funcionCRUD) {
        /**
         *  se dibula la ventana de alerta con la ubicacion de los botones continuar y cancelar
         *  @author JUAN CARLOS JIMENEZ <juankjibe@gmail.com>
         *  @returns {void}
         */
        case "agregarProcedimiento":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "modificarCRUD('agregarProcedimiento');$('#divNotificacionAlert').hide()");

            TD2 = document.createElement("td");
            TD2.setAttribute('align', 'center');
            TD2.setAttribute('width', '33%');

            boton2 = document.createElement("input");
            boton2.setAttribute('type', 'button');
            boton2.setAttribute('class', 'small button blue');
            boton2.setAttribute('value', 'Cancelar');
            boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");


            TD1.appendChild(boton1);
            TD2.appendChild(boton2);
            TR.appendChild(TD1);
            TR.appendChild(TD2);
            tabla.appendChild(TR);
            break;

        case "agregarPaqPro":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "modificarCRUD('agregarPaqPro');$('#divNotificacionAlert').hide()");

            TD2 = document.createElement("td");
            TD2.setAttribute('align', 'center');
            TD2.setAttribute('width', '33%');

            boton2 = document.createElement("input");
            boton2.setAttribute('type', 'button');
            boton2.setAttribute('class', 'small button blue');
            boton2.setAttribute('value', 'Cancelar');
            boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");


            TD1.appendChild(boton1);
            TD2.appendChild(boton2);
            TR.appendChild(TD1);
            TR.appendChild(TD2);
            tabla.appendChild(TR);
            break;
        /**
         * 
         */

        case "verificarPerimetroc20": case "verificarPesoTallaFol": case "verificarPresiones":
        case "verificarTensionDiasIngreso": case "verificarTensionSistolicaIngreso":

            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');

            boton1.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);
            break;

        case "verificarEstadioNoti":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('style', 'margin-left:10px');
            boton1.setAttribute('onclick', "modificarCRUD('firmarFolio');$('#divNotificacionAlert').hide()");

            boton2 = document.createElement("input");
            boton2.setAttribute('type', 'button');
            boton2.setAttribute('class', 'small button blue');
            boton2.setAttribute('value', 'No firmar');
            boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton2);
            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);
            break;

        /**
         *  se dibula la ventana de alerta con la ubicacion de los botones continuar y cancelar
         *  @author JUAN CARLOS JIMENEZ <juankjibe@gmail.com>
         *  @returns {void}
         */
        case "agregarProcedimiento":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "modificarCRUD('agregarProcedimiento');$('#divNotificacionAlert').hide()");

            TD2 = document.createElement("td");
            TD2.setAttribute('align', 'center');
            TD2.setAttribute('width', '33%');

            boton2 = document.createElement("input");
            boton2.setAttribute('type', 'button');
            boton2.setAttribute('class', 'small button blue');
            boton2.setAttribute('value', 'Cancelar');
            boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");


            TD1.appendChild(boton1);
            TD2.appendChild(boton2);
            TR.appendChild(TD1);
            TR.appendChild(TD2);
            tabla.appendChild(TR);
            break;

        case "agregarPaqPro":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "modificarCRUD('agregarPaqPro');$('#divNotificacionAlert').hide()");

            TD2 = document.createElement("td");
            TD2.setAttribute('align', 'center');
            TD2.setAttribute('width', '33%');

            boton2 = document.createElement("input");
            boton2.setAttribute('type', 'button');
            boton2.setAttribute('class', 'small button blue');
            boton2.setAttribute('value', 'Cancelar');
            boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");


            TD1.appendChild(boton1);
            TD2.appendChild(boton2);
            TR.appendChild(TD1);
            TR.appendChild(TD2);
            tabla.appendChild(TR);
            break;
        /**
         * 
         */

        case "verificarCuentasRepetidas":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "modificarCRUD('crearArchivosRips');$('#divNotificacionAlert').hide()");

            TD2 = document.createElement("td");
            TD2.setAttribute('align', 'center');
            TD2.setAttribute('width', '33%');

            boton2 = document.createElement("input");
            boton2.setAttribute('type', 'button');
            boton2.setAttribute('class', 'small button blue');
            boton2.setAttribute('value', 'Cancelar');
            boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");


            TD1.appendChild(boton1);
            TD2.appendChild(boton2);
            TR.appendChild(TD1);
            TR.appendChild(TD2);
            tabla.appendChild(TR);
            break;

        case 'verificarDiagnosticoHipertension':
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "verificarNotificacionAntesDeGuardar('verificarProfesionalCreoFirmar');$('#divNotificacionAlert').hide()");
            boton1.setAttribute('style', 'margin-left:10px');

            boton2 = document.createElement("input");
            boton2.setAttribute('type', 'button');
            boton2.setAttribute('class', 'small button blue');
            boton2.setAttribute('value', 'No firmar');
            boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton2);
            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);

            break;

        case "verificarMaxMinRExterno":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            if (valorAtributo("txtModificar") == 'True') {
                boton1.setAttribute('onclick', "modificarCRUD('modificarResultadoExterno');$('#divNotificacionAlert').hide()");
            }
            else {
                boton1.setAttribute('onclick', "modificarCRUD('agregarResultadoExterno');$('#divNotificacionAlert').hide()");
            }

            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);
            break;

        case "verificarPosibleDuplicadoRExterno":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "verificarNotificacionAntesDeGuardar('verificarMaxMinRExterno');$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);
            break;

        case "reprogramarProgramacionLEBloque":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "modificarCRUD('reprogramarProgramacionLEBloque');$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);
            break;

        case "modificarProgramacionLEBloque":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "modificarCRUD('modificarProgramacionLEBloque');$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);
            break;

        case "verificarProcedimientoContratadoSede":
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("TD");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'SI');
            boton1.setAttribute('onclick', "modificarCRUD('crearOrdenExterna');$('#divNotificacionAlert').hide()");

            TD3 = document.createElement("TD");
            TD3.setAttribute('align', 'center');
            TD3.setAttribute('width', '33%');

            boton3 = document.createElement("input");
            boton3.setAttribute('type', 'button');
            boton3.setAttribute('class', 'small button blue');
            boton3.setAttribute('value', 'NO');
            boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TD3.appendChild(boton3);
            TR.appendChild(TD1);
            TR.appendChild(TD3);
            tabla.appendChild(TR);
            break;

        case 'validarDiasProgramacionLE':
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'SI');
            boton1.setAttribute('onclick', "modificarCRUD('guardarProgramacionLE');$('#divNotificacionAlert').hide()");

            TD3 = document.createElement("TD");
            TD3.setAttribute('align', 'center');
            TD3.setAttribute('width', '33%');

            boton3 = document.createElement("input");
            boton3.setAttribute('type', 'button');
            boton3.setAttribute('class', 'small button blue');
            boton3.setAttribute('value', 'NO');
            boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TD3.appendChild(boton3);
            TR.appendChild(TD1);
            TR.appendChild(TD3);
            tabla.appendChild(TR);
            break;

        case 'validarDiasProgramacion':
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'SI');
            boton1.setAttribute('onclick', "modificarCRUD('guardarCitasProgramadas');$('#divNotificacionAlert').hide()");

            TD3 = document.createElement("TD");
            TD3.setAttribute('align', 'center');
            TD3.setAttribute('width', '33%');

            boton3 = document.createElement("input");
            boton3.setAttribute('type', 'button');
            boton3.setAttribute('class', 'small button blue');
            boton3.setAttribute('value', 'NO');
            boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TD3.appendChild(boton3);
            TR.appendChild(TD1);
            TR.appendChild(TD3);
            tabla.appendChild(TR);
            break;

        case 'verificarDxRelacionados':
            limpTablas('idTableBotonNotificaAlert')
            ocultar('divVentanitaDx');
            break;

        case 'verificarEmbarazoProcedimiento':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'verificarEmbarazoAyudaDiagnostica':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'verificarEmbarazoTerapia':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'verificarEmbarazoLaboratorio':
            fabricaBotonesEmbarazo(funcionCRUD);
            break;

        case 'crearAdmisionCirugia':
            fabricaBotonNotificacion(funcionCRUD);
            break;

        case 'crearAdmision':
            fabricaBotonNotificacion(funcionCRUD);
            break;

        case 'crearAdmisionFactura':
            var id_factura = descripcionMensaje;
            id_factura = id_factura.split('FACTURA:')[1].trim();
            estado_factura = descripcionMensaje.split('<b>')[1].trim().split('</b>')[0].trim();

            asignaAtributo('txtIdFacturaAsociar', id_factura, 0);
            asignaAtributo('txtEstadoFacturaAsociar', estado_factura, 0);
            fabricaBotonesAdmisiones(funcionCRUD);
            break;

        case 'verificarTipoFolio':
            fabricaBotonesFolio(funcionCRUD);
            break;

        case 'verificarLaboratorios':
            fabricaBotonesLaboratorios(funcionCRUD);
            break;

        case 'verificarCronicosDiagnosticos':
            fabricaBotonesLaboratorios(funcionCRUD);
            break;

        case 'verificarLabNarinoClinizad':
            fabricaBotonesLaboratoriosNarinoClinizad(funcionCRUD);
            break;

        case 'verificarProcedimientosRepetidos':
            fabricaBotonesProcedimientosRepetidos(funcionCRUD);
            break;

        case 'asociarCitaAdmision':
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("TD");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'SI');
            boton1.setAttribute('onclick', "modificarCRUD('asociarCitasAdmisioAbierta');$('#divNotificacionAlert').hide()");

            TD3 = document.createElement("TD");
            TD3.setAttribute('align', 'center');
            TD3.setAttribute('width', '33%');

            boton3 = document.createElement("input");
            boton3.setAttribute('type', 'button');
            boton3.setAttribute('class', 'small button blue');
            boton3.setAttribute('value', 'NO');
            boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TD3.appendChild(boton3);
            TR.appendChild(TD1);
            TR.appendChild(TD3);
            tabla.appendChild(TR);
            break;

        default:
            limpTablas('idTableBotonNotificaAlert');
            break;
    }


}

function cierraNotifica() {
    $('#divNotificacion').hide("explode", { number: 9 }, 1000); // velocidad en que desaparece      
}


function fabricaBotonNotificacion(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");
    TD = document.createElement("TD");
    TD.setAttribute('align', 'center');

    /*Ncampo1 = document.createElement("IMG");
    Ncampo1.setAttribute('src', '/clinica/utilidades/imagenes/acciones/aceptar.png');
    Ncampo1.setAttribute('width', '80');
    Ncampo1.setAttribute('height', '35');
    Ncampo1.setAttribute('align', 'center');
    Ncampo1.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "');$('#divNotificacionAlert').hide()");*/


    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'ACEPTAR');
    boton1.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "');$('#divNotificacionAlert').hide()");

    TD.appendChild(boton1);
    TR.appendChild(TD);
    tabla.appendChild(TR);
}


function fabricaBotonesEmbarazo(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'SI');
    boton1.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "Si');$('#divNotificacionAlert').hide()");

    TD2 = document.createElement("TD");
    TD2.setAttribute('align', 'center');
    TD2.setAttribute('width', '33%');

    boton2 = document.createElement("input");
    boton2.setAttribute('type', 'button');
    boton2.setAttribute('class', 'small button blue');
    boton2.setAttribute('value', 'NO');


    switch (funcionCRUD) {

        case 'verificarEmbarazoProcedimiento':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;

        case 'verificarEmbarazoAyudaDiagnostica':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosAyudaDiagnosticaNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;

        case 'verificarEmbarazoTerapia':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosTerapiaNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;

        case 'verificarEmbarazoLaboratorio':
            boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "No');" +
                "guardarYtraerDatoAlListado('buscarSiEsNoPosLaboratorioNotificacion');" +
                "$('#divNotificacionAlert').hide()");
            break;


    }


    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'NO SABE');
    boton3.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "NoSabe');$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD2.appendChild(boton2);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD2);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}


function fabricaBotonesAdmisiones(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    if (valorAtributo('txtEstadoFacturaAsociar') != 'ANULADA') {
        boton1 = document.createElement("input");
        boton1.setAttribute('type', 'button');
        boton1.setAttribute('class', 'small button blue');
        boton1.setAttribute('value', 'ASOCIAR A FACTURA');
        boton1.setAttribute('onclick', "modificarCRUD('crearAdmisionAsociada');$('#divNotificacionAlert').hide()");
    }

    TD2 = document.createElement("TD");
    TD2.setAttribute('align', 'center');
    TD2.setAttribute('width', '33%');

    boton2 = document.createElement("input");
    boton2.setAttribute('type', 'button');
    boton2.setAttribute('class', 'small button blue');
    boton2.setAttribute('value', 'CREAR ADMISION CON NUEVA FACTURA');
    boton2.setAttribute('onclick', "modificarCRUD('" + funcionCRUD + "');$('#divNotificacionAlert').hide()");

    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'CANCELAR');
    boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD2.appendChild(boton2);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD2);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}

function fabricaBotonesProcedimientosRepetidos() {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'CONTINUAR');
    boton1.setAttribute('onclick', "guardarYtraerDatoAlListado('buscarSiEsNoPosProcedimientoNotificacion');verificarNotificacionAntesDeGuardar('verificarEmbarazoProcedimiento');;$('#divNotificacionAlert').hide()");


    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'CANCELAR');
    boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}

function fabricaBotonesFolio() {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'CREAR FOLIO');
    boton1.setAttribute('onclick', " guardarYtraerDatoAlListado('nuevoDocumentoHC');$('#divNotificacionAlert').hide()");



    TD3 = document.createElement("TD");
    TD3.setAttribute('align', 'center');
    TD3.setAttribute('width', '33%');

    boton3 = document.createElement("input");
    boton3.setAttribute('type', 'button');
    boton3.setAttribute('class', 'small button blue');
    boton3.setAttribute('value', 'CANCELAR');
    boton3.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD3.appendChild(boton3);
    TR.appendChild(TD1);
    TR.appendChild(TD3);
    tabla.appendChild(TR);
}



function fabricaBotonesLaboratorios(funcionCRUD) {
    console.log("INGRESO AQUI")
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'FINALIZAR FOLIO');
    boton1.setAttribute('onclick', "verificarNotificacionAntesDeGuardar('verficarDiagnosticoPrincipal');$('#divNotificacionAlert').hide()");



    TD2 = document.createElement("TD");
    TD2.setAttribute('align', 'center');
    TD2.setAttribute('width', '33%');

    boton2 = document.createElement("input");
    boton2.setAttribute('type', 'button');
    boton2.setAttribute('class', 'small button blue');
    boton2.setAttribute('value', 'CANCELAR');
    boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD2.appendChild(boton2);
    TR.appendChild(TD2);
    TR.appendChild(TD1);
    tabla.appendChild(TR);
}

function fabricaBotonesLaboratoriosNarinoClinizad(funcionCRUD) {
    limpTablas('idTableBotonNotificaAlert')
    tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
    tabla.className = 'inputBlanco';
    tabla.setAttribute('width', '50%');
    TR = document.createElement("TR");

    TD1 = document.createElement("TD");
    TD1.setAttribute('align', 'center');
    TD1.setAttribute('width', '33%');

    boton1 = document.createElement("input");
    boton1.setAttribute('type', 'button');
    boton1.setAttribute('class', 'small button blue');
    boton1.setAttribute('value', 'CONTINUAR');
    boton1.setAttribute('onclick', "guardarYtraerDatoAlListado('buscarSiEsNoPosProcedim');$('#divNotificacionAlert').hide()");



    TD2 = document.createElement("TD");
    TD2.setAttribute('align', 'center');
    TD2.setAttribute('width', '33%');

    boton2 = document.createElement("input");
    boton2.setAttribute('type', 'button');
    boton2.setAttribute('class', 'small button blue');
    boton2.setAttribute('value', 'CANCELAR');
    boton2.setAttribute('onclick', "$('#divNotificacionAlert').hide()");

    TD1.appendChild(boton1);
    TD2.appendChild(boton2);
    TR.appendChild(TD2);
    TR.appendChild(TD1);
    tabla.appendChild(TR);
}


//var diccionario = new Array;
var diccionario = '';

function ejecutarAjaxDiccionario() {
    valores_a_mandar = "idQueryCombo=550&cantCondiciones=0";
    varajax2 = crearAjax();
    varajax2.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax2.onreadystatechange = respuestaejecutarAjaxDiccionario;
    varajax2.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax2.send(valores_a_mandar);
}

function respuestaejecutarAjaxDiccionario() {
    if (varajax2.readyState == 4) {
        if (varajax2.status == 200) {
            raiz = varajax2.responseXML.documentElement;
            co = raiz.getElementsByTagName('id');
            no = raiz.getElementsByTagName('nom');
            de = raiz.getElementsByTagName('title');
            if (co.length > 0) {
                for (i = 0; i < co.length; i++) {
                    //diccionario.push(co[i].firstChild.data+'_-_'+no[i].firstChild.data);
                    diccionario = diccionario + co[i].firstChild.data + '_-_' + no[i].firstChild.data + '_-_';
                    //alert(diccionario[i]);
                }
                //alert(diccionario.length)
            } else {
                console.log('No hay datos en el diccionario');
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax2.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}



function DetenerCrono() {
    if (CronoEjecutandose)
        clearTimeout(CronoID)
    CronoEjecutandose = false
}

function InicializarCrono() { //inicializa contadores globales
    decimas = 0;
    segundos = 0;
    minutos = 0;
}

function MostrarCrono2(limite) { //incrementa el crono   
    decimas++;

    if (decimas > 9) {
        decimas = 0;
        segundos++;
    }

    if (segundos > 59) {
        segundos = 0;
        minutos++;
    }

    if (minutos > 99) {
        DetenerCrono();
        return true;
    }
    if (segundos == limite) {
        ejecutarAjaxCrono();
        iniciarCronometroNotificaciones(limite);
    }
    CronoID = setTimeout("MostrarCrono(" + limite + ")", 10000); // aqui el tiempo en dispararce 100
    CronoEjecutandose = true;
    return true;
}





// FIN FUNCIONES DE CRONOMETRO DE SESION




/*
 function DetenerCronoCitas(){   
 if(CronoEjecutandose)
 clearTimeout(CronoID)
 CronoEjecutandose = false
 }
 
 function InicializarCronoCitas (){ //inicializa contadores globales
 decimas = 0;   segundos = 0;   minutos = 0;     
 }*/





function dispararCronometro(limite, opcion) {
    switch (opcion) {
        case 'citas':
            if (ventanaActual.opc == 'citas') { // si la ventana actual es la de citas (principal.jsp) entonces dispararce
                if ($('#drag' + ventanaActual.num + ' #divListaEspera').css('display') == 'block' || $('#drag' + ventanaActual.num + ' #divTraerListaEspera').css('display') == 'block') {
                    setTimeout("dispararCronometro(" + limite + ",'citas')", 4000); // pa mantener vivo el asunto si no esta en la ventana de citas  
                } else {
                    llenarCitas();
                    setTimeout("dispararCronometro(" + limite + ",'citas')", 40000); // (40 segundos) aqui el tiempo en dispararce   5000=5 segundos     50000=5 minutos    
                }
            } else {
                setTimeout("dispararCronometro(" + limite + ",'citas')", 20000); // pa mantener vivo el asunto si no esta en la ventana de citas     
            }
            break;
        case 'misCitasHoy':
            if (ventanaActual.opc == 'misCitasHoy') { // si la ventana actual es la de citas (principal.jsp) entonces dispararce
                buscarMisPacienteHoy();
                setTimeout("dispararCronometro(" + limite + ",'misCitasHoy')", 50000);
            }
            break;
    }
}


function dibujarVentanitaAlertaSinCerrar(funcionCRUD, tipoNotificacion) {
    switch (tipoNotificacion) {

        case ('notificacionAlert'):
            $('#divNotificacionAlert').addClass('cssjas');
            $('#imgCerrarAlerta').hide();
            $('#divNotificacionAlert').show("explode", { number: 9 }, 400);
            break;

        case ('escrita2'):
            $('#divNotificacionSuperior').show("", { number: 9 }, 400);
            break;
        default:

            $('#divNotificacion').addClass('cssjas');
            $('#imgCerrarAlerta').hide();
            $('#divNotificacion').show("explode", { number: 9 }, 400);
            break;

    }

    switch (funcionCRUD) {

        case 'validarEstadoPaciente':
            var id_causa = estadoPaciente.split('-')
            id_causa = id_causa['1']
            if (id_causa == "64" || id_causa == "70" || id_causa == "122") {
                habilitar('btnGuardarCita', 0);
                habilitar('btnProcedimiento', 0);
                habilitar('btnCrearAdmision', 0);
            }
            limpTablas('idTableBotonNotificaAlert')
            tabla = document.getElementById('idTableBotonNotificaAlert').lastChild;
            tabla.className = 'inputBlanco';
            tabla.setAttribute('width', '100%');
            TR = document.createElement("TR");

            TD1 = document.createElement("td");
            TD1.setAttribute('align', 'center');
            TD1.setAttribute('width', '33%');

            boton1 = document.createElement("input");
            boton1.setAttribute('type', 'button');
            boton1.setAttribute('class', 'small button blue');
            boton1.setAttribute('value', 'Continuar');
            boton1.setAttribute('onclick', "cargarInformacionPaciente();$('#divNotificacionAlert').hide()");

            TD1.appendChild(boton1);
            TR.appendChild(TD1);
            tabla.appendChild(TR);
            break;
    }

}