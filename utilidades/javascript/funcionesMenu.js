////*****metodos para el menu
var varajaxMenu; //ajax para traer informacion de paginas desde menu
var objetosMenu = new Array(); // arreglo que contiene los objetos de las opciones de menu activas
var ventanaActual; //variable para guardar la ventana actual en ejecuci�n
var objetosMenuaux = new Array();
//var sisobreponer=true;
var data = new Array();
var data1 = new Array();

//// funcion que se llama cuando se da click en una opcion del menu 
function cargarMenu(pagina, num, opc, msg, guardar, modificar, eliminar, imprimir, limpiar, buscar) {
    //console.log(pagina,num,opc,msg,guardar);
    try {
        cerrarForma(document.getElementById('imgCerrar')) 
    } catch (error) {
        
    }
    ///crear el objeto ventanaActual 
    $("#loaderPrincipal").show();
    ventanaActual = new objetoMenu();
    ventanaActual.num = num; /*numero de ventana a activar*/
    ventanaActual.opc = opc;
    ventanaActual.mensaje = msg;
    ventanaActual.guardar = guardar;
    ventanaActual.modificar = modificar;
    ventanaActual.eliminar = eliminar;
    ventanaActual.imprimir = imprimir;
    ventanaActual.limpiar = limpiar;
    ventanaActual.buscar = buscar;
    ////fin crear el objeto ventanaActual

    if (buscaObjetoMenu(num)) { // si el objeto 3-3-1  ya esta creado no abre ventana ...  comentariado por juan porque no estaba validando bien
    
    }else{
        $.ajax({
            url: pagina,
            data: '',
            contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
            beforeSend: function () {
                //mostrarLoader(urlParams.get("accion"))
            },
            success: function (data) {
                if (document.getElementById("drag" + ventanaActual.num) == null) {
                    insertarNuevaVentana();
                    contenedor = document.createElement("DIV");
                    contenedor.id = "drag" + ventanaActual.num;
    
                    contenedor.style.position = "relative";
                    //				  contenedor.style.left="150px"; // distancia left de ventanas del menu principal al desplegarce  izquierda
    
                    contenedor.style.top = topSu + "px";
                    contenedor.style.left = 0 + topSu + "px"; /*ESTABA EN 90*/
                    topSu = topSu + 21;
    
    
                    //contenedor.setAttribute('onclick',"sobreponer(this);");
                    contenedor.setAttribute('onmouseup', "sobreponer(this);");
                    document.getElementById("main").appendChild(contenedor);
                    mensaje = ventanaActual.mensaje;
    
                    document.getElementById("drag" + ventanaActual.num).style.zIndex = 51;
                    /////crear opcion en menu de ventanas activas
                    contenedor = document.createElement("li");
                    contenedor.id = "li" + ventanaActual.num;
                    vinculo = document.createElement("a");
                    vinculo.href = "#";
                    vinculo.appendChild(document.createTextNode(ventanaActual.opc));
                    vinculo.setAttribute('onclick', "sobreponer(document.getElementById('drag" + ventanaActual.num + "'))");
                    contenedor.appendChild(vinculo);
                    document.getElementById("ventanasActivas").appendChild(contenedor);
    
                    if (objetosMenu.length > 1) {
                        deshabilitarVentana();
                    }
                    habilitarVentana();
                } else {
                    $("#drag" + ventanaActual.num).show();
                }
    
                document.getElementById("drag" + ventanaActual.num).innerHTML = data;
                actualizarMensaje();
                mover(ventanaActual.num);
                $("#drag" + ventanaActual.num).find("#tituloTexto").dropShadow({ left: 2, top: 2, blur: 1, opacity: 1, color: "black", swap: false });
                switch (ventanaActual.opc) {
                    case "tipificacion_externa":
                        setTimeout(() => {
                            buscarFacturacion('tablaTipificacionExterna')
                        }, 100);
                        break;

                    case 'notasNefro':
                        $("#tabsnotasSegumiento").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    break;

                    case 'notasVih':
                        $("#tabsnotasSegumiento").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    break;
                    
                    case 'apoyoDx02':
                        $("#tabsSubirDocumentos").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
                        break;
    
                    case 'comprobanteEntrada':                    
                        calendario('txtFechaDesdeDoc',1);
                        calendario('txtFechaHastaDoc',1);
                        calendario('txtFechaVencimiento',1); 
                        break;
                    
                    case 'ordenesCompra':
                        calendario('txtFechaDesdeDoc',1);
                        calendario('txtFechaHastaDoc',1);
                        calendario('txtFechaDocumento',1);
                    break; 
                    
                    case 'clonarFolios':
                        $('#cmbIdFolio').select2({dropdownCssClass : "set_ddl_size"})
                        break
    
                    case 'folios':
                        $("#tabsParametrosFolio").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        setTimeout(() => {
                            buscarHC('listGrillaTiposFormulario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                            setTimeout(() => {
                                tabActivoFoliosParametros('divFoliosTipoAdmision')
                            }, 300);
                        }, 200);
                        break;
    
                    case 'autorizaciones':
                        $("#tabsAutorizaciones").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    break;
    
                    case 'planT':
                        $("#tabsPlanT").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        setTimeout(() => {
                            buscarHC('listGrillaAutorizaciones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')},200);
                        break;
    
                    case 'contactosPaciente':
                        calendario('txtFechaNacimientoUrgencias', 0)
                        break;
    
                    case 'planes':
                        $("#tabsPrincipalPlanes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        calendario('txtFechaInicio', 1);
                        calendario('txtFechaFinal', 1);
                        break;
    
                        case 'panelMedicamentos':
                        calendario('txtFechaDesdeP', 1);
                        calendario('txtFechaHastaP', 1);
                        buscarSuministros('listaPanelMedicamentos');
                        break;
                        
                    case 'apoyoDx':
                        calendario('txtFechaDesdeP', 1);
                        calendario('txtFechaHastaP', 1);
                        buscarHC('listGrillaApoyoDiagnotico', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                        break;
    
                    case 'pruebaLaboratorio':
                        console.log(valorAtributo('txtIdLaboratorioEmpresa'))
                        buscarRutas('listaHistoriaClinicaLaboratorio');
                    break;
    
                    case 'plantillaLaboratorio':
                        console.log(valorAtributo('txtIdPlantillaLaboratorio'))
                        buscarHistoria('listaPlantillaLaboratorio');
                    break;
    
                    case 'listaEsperaAgendamiento':
                        calendario('txtFechaCita', 0)
                        break;
    
                    case 'principalOrdenes':
                        calendario('txtFechaAgenda', 1)
                        break;
    
                    case 'cartera':
                        calendario('txtFechaDesdeFactura', 0)
                        calendario('txtFechaHastaFactura', 0)
                        break;
    
                    case 'leerNotificacion':
                        buscarNotificacion('listGrillaLeerNotificacion')
                        break;
                    case 'asistenciaPacientes':
                        calendario('txtBusFechaDesde', 1)
                        calendario('txtBusFechaHasta', 1)
                        break;
                    case 'principalDocumentos':
                        calendario('txtFechaModifica', 0)
                        calendario('txtFechaDesdeDoc', 1)
                        calendario('txtFechaHastaDoc', 1)
                        calendario('txtFechaInicioTerapia', 1)
                        break;
                        case 'coordinacionHomecare':
                            calendario('txtDocFechaDesde', 1)
                            calendario('txtDocFechaHasta', 1)
                            calendario('txtFechaInicioTerapia', 1)
                            $("#tabsCoordinacionTerapia").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
                            tabActivo = "divProcedimientosOrdenados";
                            break;
    
                    case 'codigoBarras':
                        document.getElementById('txtCodBarrasBus').focus()
                        break;
                    case 'Facturas':
                        calendario('txtFechaAdmision', 1)
                        calendario('txtFechaDesdeFactura', 1)
                        calendario('txtFechaHastaFactura', 1)
                        calendario('txtFechaIngreso', 0)
                        calendario('txtFechaEgreso', 0)
                        calendario('txtNacimiento', 0)
                        break;
    
                    case 'recibos':
                        calendario('txtFechaRecibo', 0)
                        break;
    
                    case 'solicitudes':
                        idArticulo = '';
                        calendario('txtFechaDesdeDoc', 1)
                        calendario('txtFechaHastaDoc', 1)
                        break;
                    case 'CrearBodega':
                        asignaAtributo('cmbIdBodega', '', 1)
                        break;
                    case 'principalArticulo':
                        asignaAtributo('txtIdArticulo', '', 1)
                        calendario('txtVigInvima', 0)
                        break;
                    case 'principalDocumentos':
                        calendario('txtFechaDesdeDoc', 1)
                        calendario('txtFechaHastaDoc', 1)
    
                        break;
                    case 'devoluciones':
                        calendario('txtFechaDesdeDoc', 1)
                        calendario('txtFechaHastaDoc', 1)
                        break;
    
                    case 'devolucionCompras':
                        calendario('txtFechaDocumento', 0);
                        calendario('txtFechaDesdeDoc', 1);
                        calendario('txtFechaHastaDoc', 1);
                        calendario('txtFechaDesdeDev', 1);
                        calendario('txtFechaHastaDev', 1);
                        idArticulo = '';
                        break;
                    case 'salidasDeBodega':
                        calendario('txtFechaDocumento', 0);
                        calendario('txtFechaDesdeDoc', 1);
                        calendario('txtFechaHastaDoc', 1);
                        idArticulo = '';
                        break;
                    case 'facturasTrabajo':
                        calendario('txtFechaDesdeTra', 0);
                        calendario('txtFechaHastaTra', 0);
                        calendario('txtFechaEntrega', 1);
                        calendario('txtFechaRecepcion', 1);
                        calendario('txtFechaTrabajoEntregado', 1);
                        calendario('txtFechaRecibeF', 1);
                        calendario('txtFechaCumpli', 1);
                        calendario('txtEntrega', 0);
                        break;
    
                    case 'entradasDeBodega':
                        calendario('txtFechaDocumento', 0);
                        calendario('txtFechaDesdeDoc', 1);
                        calendario('txtFechaHastaDoc', 1);
                        calendario('txtFechaVencimiento', 0);
                        break;
    
                    case 'gestionarEvento':
                        calendario('txtFechaInicia', 1)
                        calendario('txtFechaEntrega', 1)
                        calendario('txtFechaAplazada', 1);
                        calendario('txtFechaRecibe', 0);
                        break;

                    case 'sedeEspecialidades':
                        $('#cmbTipoCitaRelacionado').select2({dropdownCssClass : "set_ddl_size"})                        
                        break   
                    case 'agenda':
                        calendario("txtFechaNac", 1);
                        //calendario('txtFechaPacienteCita', 1);
                        //calendario('txtFechaVigencia', 0);
                        //calendario('txtFechaTerapiaEditar', 0);
    
                        //calendario('txtFechaNacOrdenes', 0);
                        //calendario('txtFechaVigenciaOrdenes', 0);
    
                        var f = new Date();
                        var mes = (f.getMonth() + 1).toString();
                        var anio = f.getFullYear().toString();
                        asignaAtributo('lblMes', mes, 0)
                        asignaAtributo('lblAnio', anio, 0)
    
                        //calendario('txtFechaNac', 1)
                        buscarAGENDA('listDias')
                        $("#tabsCoordinacionTerapia").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });

                        pagerSelect2(`idQueryCombo=${557}&cantCondiciones=2&condicion1=${IdEmpresa()}&condicion2=${IdSede()}`, 'cmbSede').then((idCombo) => {
                            desHabilitar(idCombo, 0)                            
                        })
                        $('#cmbIdEspecialidad').select2({dropdownCssClass : "set_ddl_size"})                        
                        $('#cmbIdProfesionales').select2({dropdownCssClass : "set_ddl_size"})

                        //$('#cmbTipoCitaLE').select2({dropdownCssClass : "set_ddl_size"})

                        /*
                        */

                        //$('#cmbSede').select2();

                        break;
    
                    case 'despachoProgramacionProcedimientos':
                        var f = new Date();
                        var mes = (f.getMonth() + 1).toString();
                        var anio = f.getFullYear().toString();
                        asignaAtributo('lblMes', mes, 0)
                        asignaAtributo('lblAnio', anio, 0) // para primera pantalla del mes actual						 					 
                        asignaAtributo('lblFechaSeleccionada', document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0)
                        break;
    
                    case 'listaEspera':
                        pagerSelect2(`idQueryCombo=${557}&cantCondiciones=2&condicion1=${IdEmpresa()}&condicion2=${IdSede()}`, 'cmbSede').then((idCombo) => {
                            desHabilitar(idCombo, 0)                            
                        })
                        $('#cmbIdEspecialidad').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbIdSubEspecialidad').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbTipoCita').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbSitio').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbEstado').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbMotivo').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbNecesidad').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbDiscapaciFisica').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbEmbarazo').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbEsperar').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbIdProfesionales').select2({dropdownCssClass : "set_ddl_size"})


                        buscarAGENDA('listProcedimientoLE');
                        calendario('txtFechaNac', 0)
                        calendario('txtFechaVigencia', 0);
                        calendario('txtFechaVigenciaLE', 0);
                        pagerSelect2(`idQueryCombo=${1417}&cantCondiciones=0`, 'cmbEspecialidadAtusuarios')
                        .then((idCombo) => {
                            desHabilitar(idCombo, 0)       
                                                 
                        })
                        pagerSelect2(`idQueryCombo=${2649}&cantCondiciones=0`, 'cmbTipoCitaAtusuarios')
                        .then((idCombo) => {
                            desHabilitar(idCombo, 0)                            
                        })

                        pagerSelect2(`idQueryCombo=${6000}&cantCondiciones=0`, 'cmbPlanContratacion')
                        .then((idCombo) => {
                            desHabilitar(idCombo, 0)                            
                        })
                        /* pagerSelect2(`idQueryCombo=${2650}&cantCondiciones=0`, 'cmbSubEspecialidadAtusuarios')
                        .then((idCombo) => {
                            desHabilitar(idCombo, 0)                            
                        }) */

                        $('#cmbMotivo').val('2')
                        $('#cmbMotivo').trigger('change')
                        // llamarAutocomIdDescripcionConDato('txtAdministradora1',308)						 		
                        buscarAGENDA('listHistoricoListaEspera')				 
                        break;
                    case 'listaEsperaCirugia':
                        buscarAGENDA('listCitaCirugiaProcedimientoLE');
                        calendario('txtFechaNac', 0)
                        calendario('txtFechaVigencia', 0);
                        calendario('txtFechaVigenciaLE', 0);
                        //	 llamarAutocomIdDescripcionConDato('txtAdministradora1',308)							 							 						 							 						 
                        break;
    
                    case 'horarios':
                        var f = new Date();
                        var mes = (f.getMonth() + 1).toString();
                        var anio = f.getFullYear().toString();
                        asignaAtributo('lblMes', mes, 0)
                        asignaAtributo('lblAnio', anio, 0)
                        calendario('txtFechaDestino', 1)
                        calendario('txtFechaDestinoCita', 1)
                        buscarAGENDA('listDiasC')
                        //$('#cmbSede').select2({dropdownCssClass : "set_ddl_size"})
                        //$('#cmbIdEspecialidad').select2({dropdownCssClass : "set_ddl_size"})
                        //$('#cmbIdProfesionales').select2({dropdownCssClass : "set_ddl_size"})
                        //pagerSelect2(`idQueryCombo=${173}&cantCondiciones=0`, 'cmbIdEspecialidad').then((idCombo) => {
                          //  desHabilitar(idCombo, 0)
                        //})
                        //pagerSelect2(`idQueryCombo=${173}&cantCondiciones=0`, 'cmbIdProfesionales').then((idCombo) => {
                          //  desHabilitar(idCombo, 0)
                        //})
                        pagerSelect2(`idQueryCombo=${557}&cantCondiciones=2&condicion1=${IdEmpresa()}&condicion2=${IdSede()}`, 'cmbSede').then((idCombo) => {
                            desHabilitar(idCombo, 0)
                        })
                        $('#cmbIdEspecialidad').select2({dropdownCssClass : "set_ddl_size"})
                        $('#cmbIdProfesionales').select2({dropdownCssClass : "set_ddl_size"})

                        break;
    
                    case 'agendaHomecare':
                        calendario('txtFechaInicio', 1);
                        calendario('txtFechaFin', 1)
    
                    break;
    
                    case 'reportarNoConformidad':
                        $("#drag" + ventanaActual.num).find('#txtFechaEvento').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
    
                        $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#tabsEventos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        break;
    
                    case 'administrarNoConformidad':
                        $("#drag" + ventanaActual.num).find('#txtFechaEvento').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#tabsEventos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        $("#tabsEventosIndicadores").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfHasta2').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        break;
    
                    case 'reportarEventoAdverso':
                        // swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
                        break;
    
    
                    case 'administrarEventoAdverso':
                        $("#drag" + ventanaActual.num).find('#txtFechaEvento').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
    
                        $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaCuandoOportMejora').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#tabsEventosEA").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        $("#tabsEventos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
    
    
                        $("#tabsEventosIndicadores").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfHasta2').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#tabsEventosPlanDeAccion").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
    
                        setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1);
                        break;
    
                    case 'administrarPlanMejora':
    
                        $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#tabsEventosPlanDeAccion").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
    
                        $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        break;
    
                    case 'administrarPlatin':
                        tabActivo = '1';
    
                        $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaIniPlatin').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#tabsPrincipal").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
    
                        $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        buscarPlatin('administrarPlatin', '/clinica/paginas/accionesXml/buscarHc_xml.jsp')
                        break;
    
                    case 'fichaPlatin':
                        tabActivo = '1';
    
                        $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaIniPlatin').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaFinPlatin').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
    
                        $("#tabsPrincipal").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
    
                        $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        // alert('ddd');
                        //buscarPlatin('lista','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp') 
                        buscarPlatin('fichaPlatin', '/clinica/paginas/accionesXml/buscarHc_xml.jsp')
    
                        setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1500);
    
                        setTimeout("llamarAutocomIdDescripcion('txtIdPaciente',15)", 3500);
    
                        break;
    
                    case 'Calendario':
                        llenarCalendario();
                        break;
    
                    case 'citasE':
                        // llenarCitas();
                        vertanasMoviblesConPunteroMouse('divTraerListaEspera', 3);
                        // vertanasMoviblesConPunteroMouse('divListaEspera',23);		
    
                        $('#drag' + ventanaActual.num).find('#divListaEspera').draggable({
                            zIndex: 20000,
                            ghosting: true,
                            opacity: 0.7,
                            //containment : 'parent',
                            handle: '#tituloForma'
                        });
    
                        vertanasMoviblesConPunteroMouse('divSubVentanaAgendaNoPlaneada', 1);
    
    
                        $("#drag" + ventanaActual.num).find('#txtBusFechaDesdeListaEspera').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
    
                        $("#drag" + ventanaActual.num).find('#txtBusFechaHastaListaEspera').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        $("#drag" + ventanaActual.num).find('#txtFechaNacimientoListaEspera').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
    
                        dispararCronometro('100', 'citas');
    
    
                        break;
    
                    case 'misCitasHoy':
                        //	buscarMisPacienteHoy();
                        calendario('txtBusFechaDesde', 1)
                        calendario('txtBusFechaHasta', 1)
                        break;
    
                    case 'reportes':
                        calendario('txtBusFechaDesde', 1)
                        calendario('txtBusFechaHasta', 1)
                        break;
    
                    case 'fichaTecnica':
                        $("#drag" + ventanaActual.num).find('#txtFechaVigenciaIni').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });
                        break;
                    case 'indicador':
                        $("#tabsIndicadores").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
    
                        break;
    
                    case 'documentos':
                        calendario('txtDocFechaDesde', 1)
                        calendario('txtDocFechaHasta', 1)
                        break;
    
                    case 'paciente':
                        tabActivo = '1';
                        calendario('txtFechaNacimiento', 1)
                        try {
                            buscarPlatin('paciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                        } catch (error) {
                            
                        }
    
                        setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1);
                        $("#drag" + ventanaActual.num).find('#txtFechaNacimiento').datepicker({
                            changeMonth: true,
                            changeYear: true
                        });

                        cargarCombosPaciente();
    
                        break;
    
    
                    case 'principalHC':
                        calendario('txtFechaDesdeFolio', 1)
                        calendario('txtFechaHastaFolio', 1)
                        calendario('txtFechaAgenda', 1)
                        calendario('txtFechaAgendaFin', 1)
                        document.getElementById('txtContrasenaAdm').focus();
                        contenedorBuscarPaciente();

                        break;
    
                    case 'admision':
                        calendario('txtFechaNac', 1)
                        //calendario('txtFechaVigenciaAutorizacion', 1)
                        calendario('txtFechaTraerCita', 1)
                        calendario('txtFechaNacimientoUrgencias', 0)
                        break;
                    case 'soloFactura':
                        asignaAtributoCombo2('cmbIdProfesionalesFactura', IdSesion(), $('#lblIdNombreProfesionalA').text()) 
                        break;
    
                    case 'admisiones':
                        calendario('txtFechaNac', 0)
                        //calendario('txtFechaVigenciaAutorizacion', 1)
                        calendario('txtFechaTraerCita', 1)
                        calendario('txtFechaNacimientoUrgencias', 0)
                        comboDependienteSede('cmbIdEspecialidad', '849')
                        $("#tabsDetallesFactura").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        $("#tabsElementosPendientes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        setTimeout(() => {
                            comboDependienteEmpresa('cmbIdProfesionales', '77')
                            setTimeout(() => {
                                // comboDependienteSede('cmbIdProfesionalesFactura','77')
                                cargarComboGRALCondicion1_('', '', 'cmbIdProfesionalesFactura', 10360, IdSede());
                                setTimeout(() => {
                                    comboDependienteEmpresa('cmbIdTipoServicio','74')
                                    $('#cmbIdTipoServicio').select2({dropdownCssClass : "set_ddl_size"})
                                    $('#cmbIdEspecialidad').select2({dropdownCssClass : "set_ddl_size"})
                                    $('#cmbCausaExterna').select2({dropdownCssClass : "set_ddl_size"})
                                    $('#cmbFinalidad').select2({dropdownCssClass : "set_ddl_size"})
                                    $('#cmbIdProfesionalesFactura').select2({dropdownCssClass : "set_ddl_size"})
                                    agregarNuevaOpcion('cmbIdProfesionalesFactura', IdSesion(), $('#lblIdNombreProfesionalA').text() )
                                    
                                }, 200);
                            }, 200);
                        }, 200);
                        break;
    
                    case 'procedimientos':
                        calendario('txtFechaDesdeP', 1)
                        calendario('txtFechaHastaP', 1)
                        break;
                    case 'planes':
                        calendario('txtFechaInicio', 1)
                        calendario('txtFechaFinal', 1)
                        calendario('txtFech', 1)
                        break;
                    case 'recepcionTecnica':
                        calendario('txtFechaVtoRecepTecnica', 1);
                        break;
                    case 'trasladoBodegas':
                        calendario('txtFechaDocumentoDesde', 1);
                        calendario('txtFechaDocumentoHasta', 1);
                        break;
    
                    case 'salidasVentas':
                        calendario('txtFechaNac', 1);
                        comboCargarBodegaVentas('cmbBodegaVentas');
                        break;
    
                    case 'articulosPlan':
    
                        break;
    
                    case 'parlante':
                        buscarAGENDA('listPacientesParlante');
                        break;
                    
                    case 'tipificacion_facturas':
                        calendario('txtFechaDesdeFactura', 1)
                        calendario('txtFechaHastaFactura', 1)
                        break;
    
                    case 'gestionRiesgos':
                        calendario('txtFechaDesdeRiesgo', 1)
                        calendario('txtFechaHastaRiesgo', 1)
                        break;
    
                        case 'infosedes':
                            buscarEvento('sedes')
                        break;
    
                    default:
    
                        calendario('txtFechaOrdenMed', 1)
                        calendario('txtFechaDocumento', 1)
                        calendario('txtBusFechaConsumo', 1)
                        calendario('txtFechaConsumo', 1)
                        calendario('txtFechaConsumo2', 1)
                        calendario('txtFechaReferencia', 0)
                        calendario('txtBusFechaAdminis', 1)
    
                        calendario('txtFechaConsumo2', 0)
                        calendario('txtFechaConsumo', 0)
                        calendario('txtFechaHastaOrdenMed', 1)
                        calendario('txtFechaDesdeOrdenMed', 1)
                        calendario('lblFecha', 1) //DEVOLUION FARMACIA	
                        calendario('txtFechaDocDesde', 1)
                        calendario('txtFechaDocHasta', 1)
    
                        calendario('txtFechaDocumentoDesde', 1)
                        calendario('txtFechaDocumentoHasta', 1)
                        calendario('txtFechaCierreDesde', 1)
                        calendario('txtFechaCierreHasta', 1)
                        calendario('txtFechaNac', 1);
    
                        break;
    
    
                }
                //CC: uso una clase en el txt para no poner aca todos los txt q sean fecha
                $("#drag" + ventanaActual.num).find('.fechasTxt').datepicker({
                    changeMonth: true,
                    changeYear: true,
                });
            },
            complete: function (jqXHR, textStatus) {
                $("#loaderPrincipal").hide();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            }
        });
    }
}

//// funcion que se llama cuando se da click en una opcion del menu 
function cargarMenu_(pagina, num, opc, msg, guardar, modificar, eliminar, imprimir, limpiar, buscar) {
    // alert(objetosMenu.length);
    ///crear el objeto ventanaActual 
    $("#loaderPrincipal").show();
    ventanaActual = new objetoMenu();
    ventanaActual.num = num; /*numero de ventana a activar*/
    ventanaActual.opc = opc;
    ventanaActual.mensaje = msg;
    ventanaActual.guardar = guardar;
    ventanaActual.modificar = modificar;
    ventanaActual.eliminar = eliminar;
    ventanaActual.imprimir = imprimir;
    ventanaActual.limpiar = limpiar;
    ventanaActual.buscar = buscar;
    ////fin crear el objeto ventanaActual

    if (buscaObjetoMenu(num)) { // si el objeto 3-3-1  ya esta creado no abre ventana ...  comentariado por juan porque no estaba validando bien
        //$("#drag"+num).show();  
        //alert('Ventana activa');
        //	sobreponer(document.getElementById("drag"+num));
        //insertarNuevaVentana();
    } else {
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        varajaxMenu.open("POST", pagina, true);
        varajaxMenu.onreadystatechange = llenarinfoMenu;
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
    }
}

function ocultarMenu(bandera)
{   
    varajaxMenu = crearAjax();     
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/principalMenu.jsp', true);
    varajaxMenu.onreadystatechange = llenarmenu;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarmenu()
{
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {  
            
            contenedorDiv = document.getElementById('menumenu1');
            tabla = document.createElement('table');  // se crean los div hijos
            tabla.id = 'idTableMen'            
            contenedorDiv.appendChild(tabla);            
           
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

///// funcion para realizar la busqueda de un objeto en el array de ventanas activas
function buscaObjetoMenu(num) {

    if (objetosMenu.length == 0) topSu = 1;

    for (i = 0; i < objetosMenu.length; i++) {
        if (objetosMenu[i].num == num) {
            $("#loaderPrincipal").hide();
            alert('La ventana se encuentra abierta');
            return true;
        }
    }
    return false;
}


function insertarNuevaVentana() {
    for (i = objetosMenu.length; i > 0; i--) {
        objetosMenu[i] = objetosMenu[i - 1];
    }
    objetosMenu[0] = ventanaActual;
    $("#loaderPrincipal").hide();
    //if(objetosMenu.length>1){
    // $('#drag'+objetosMenu[1].num).fadeTo("slow",0.8);	
    //}
}


function crearObjetoMenu(num, opc, msg, guardar, modificar, eliminar, imprimir, limpiar, buscar) {
    objetosMenu[num] = new objetoMenu();
    objetosMenu[num].num = num;
    objetosMenu[num].opc = opc;
    objetosMenu[num].mensaje = msg;
    objetosMenu[num].guardar = guardar;
    objetosMenu[num].modificar = modificar;
    objetosMenu[num].eliminar = eliminar;
    objetosMenu[num].imprimir = imprimir;
    objetosMenu[num].limpiar = limpiar;
    objetosMenu[num].buscar = buscar;
}

//clase para crear cada una de las opciones del menu
function objetoMenu() {
    num = "";
    opcion = "";
    mensaje = "";
    guardar = "no";
    modificar = "no";
    eliminar = "no";
    imprimir = "no";
    limpiar = "no";
    buscar = "no";
    activa = false;
}

/// sobreponer las ventanas
function sobreponer(obj) {
    //try{
    //	alert("sobrponer");
    //	alert('sobreponer='+objetosMenu.length);
    for (i = 1; i < objetosMenu.length; i++) {
        if (objetosMenu[i].num == obj.id.substr(4, obj.id.length)) {
            ventanaActual = objetosMenu[i];
            break;
        }
    }
    mensaje = ventanaActual.mensaje;
    //cambiar el orden en el arreglo de las ventanas
    for (i = 0; i < objetosMenu.length; i++) {
        //si se encuentra la posicion de la ventana anterior
        if (objetosMenu[i].num == ventanaActual.num)
            break;
    }
    //intercambiar posiciones	
    for (j = i; j > 0; j--) {
        objetosMenu[j] = objetosMenu[j - 1];
    }
    objetosMenu[0] = ventanaActual;
    ///ubicar posicion con zIndex segun el orden del array
    j = 50;
    for (i = 0; i < objetosMenu.length; i++) {
        document.getElementById('drag' + objetosMenu[i].num).style.zIndex = j;
        j--;
    }
    ///ubicar transparencia de las ventanas inactivas
    //$('#drag'+objetosMenu[0].num).fadeTo("fast", 1);
    //if(objetosMenu.length>1)
    // $('#drag'+objetosMenu[1].num).fadeTo("fast", 0.8);

    if (objetosMenu.length > 1) {
        deshabilitarVentana();

        //$('#drag'+objetosMenu[1].num+" DIV#nifty").addClass('niftyDeshabilitado');
        //$('#drag'+objetosMenu[1].num+" DIV#nifty").attr("id","niftyDeshabilitado");
    }

    //$('#drag'+objetosMenu[0].num+" DIV#nifty").css("background","red");
    habilitarVentana();

    actualizarMensaje();
    //}catch (e){;}
}


function deshabilitarVentana() {
    $('#drag' + objetosMenu[1].num).find("#idTituloForma").removeClass('tituloForma').addClass('tituloFormaInhabilitado');
    $('#drag' + objetosMenu[1].num).find("#rtop").removeClass('rtop').addClass('rtopDeshabilitado');
    $('#drag' + objetosMenu[1].num).find("#rbottom").removeClass('rbottom').addClass('rbottomDeshabilitado');
    $('#drag' + objetosMenu[1].num + ".fondoTabla").removeClass('fondoTabla').addClass('fondoTablaDeshabilitado');
    $('#drag' + objetosMenu[1].num).find(".filoInferiorTabla").removeClass('filoInferiorTabla').addClass('filoInferiorTablaDeshabilitado');
    $('#drag' + objetosMenu[1].num + " .titulos").removeClass('titulos').addClass('titulosDeshabilitado');
    $('#drag' + objetosMenu[1].num + " input").addClass('inputDeshabilitado');
    $('#drag' + objetosMenu[1].num + " select").addClass('selectDeshabilitado');
    $('#drag' + objetosMenu[1].num).find("#imgCerrar").attr("src", "/clinica/utilidades/imagenes/acciones/Cerrar1Deshabilitado.gif");
    $('#drag' + objetosMenu[1].num + " .label_left2").removeClass('label_left2').addClass('label_left2Deshabilitado');
    // LISTAS JQG
    //$('#drag'+objetosMenu[1].num+" .HeaderLeft").removeClass('HeaderLeft').addClass('HeaderLeft_off');
    $('#drag' + objetosMenu[1].num + " .HeaderLeft").addClass('HeaderLeft_off');
    $('#drag' + objetosMenu[1].num + " .HeaderRight").addClass('HeaderRight_off');
    //$('#drag'+objetosMenu[1].num+" .HeaderButton").addClass('HeaderButton_off');
    $('#drag' + objetosMenu[1].num + " .Header").addClass('Header_off');
    //$('#drag'+objetosMenu[1].num+" .modalhead").addClass('modalhead_off');//degrad head
    $('#drag' + objetosMenu[1].num + " table.navtable").addClass('navtable_off'); //degrad foot
    $('#drag' + objetosMenu[1].num + " table.scroll").addClass('scroll_off'); // degrad head
    $('#drag' + objetosMenu[1].num + " .selected-row").addClass('selected-row_off'); //fila selecc
    $('#drag' + objetosMenu[1].num + " img.imgLupa").attr("src", '/clinica/utilidades/imagenes/acciones/lupita.gif');
}

function habilitarVentana() {
    $('#drag' + objetosMenu[0].num).find("#idTituloForma").removeClass('tituloFormaInhabilitado').addClass('tituloForma');
    $('#drag' + objetosMenu[0].num).find("#rtop").removeClass('rtopDeshabilitado').addClass('rtop');
    $('#drag' + objetosMenu[0].num).find("#rbottom").removeClass('rbottomDeshabilitado').addClass('rbottom');
    $('#drag' + objetosMenu[0].num + " .fondoTablaDeshabilitado").removeClass('fondoTablaDeshabilitado').addClass('fondoTabla');
    $('#drag' + objetosMenu[0].num).find(".filoInferiorTablaDeshabilitado").removeClass('filoInferiorTablaDeshabilitado').addClass('filoInferiorTabla');
    $('#drag' + objetosMenu[0].num + " .titulosDeshabilitado").removeClass('titulosDeshabilitado').addClass('titulos');
    $('#drag' + objetosMenu[0].num + " input").removeClass('inputDeshabilitado');
    $('#drag' + objetosMenu[0].num + " select").removeClass('selectDeshabilitado');
    $('#drag' + objetosMenu[0].num).find("#imgCerrar").attr("src", "/clinica/utilidades/imagenes/acciones/cerrarVentana.png");
    $('#drag' + objetosMenu[0].num + " .label_left2Deshabilitado").removeClass('label_left2Deshabilitado').addClass('label_left2');
    // LISTAS JQG
    //$('#drag'+objetosMenu[0].num+" .HeaderLeft_off").removeClass('HeaderLeft_off').addClass('HeaderLeft');
    $('#drag' + objetosMenu[0].num + " .HeaderLeft_off").removeClass('HeaderLeft_off');
    $('#drag' + objetosMenu[0].num + " .HeaderRight_off").removeClass('HeaderRight_off');
    //$('#drag'+objetosMenu[0].num+" .HeaderButton_off").removeClass('HeaderButton_off');
    $('#drag' + objetosMenu[0].num + " .Header_off").removeClass('Header_off');
    //$('#drag'+objetosMenu[0].num+" .modalhead_off").removeClass('modalhead_off');
    $('#drag' + objetosMenu[0].num + " table.navtable_off").removeClass('navtable_off');
    $('#drag' + objetosMenu[0].num + " table.scroll_off").removeClass('scroll_off');
    $('#drag' + objetosMenu[0].num + " .selected-row_off").removeClass('selected-row_off');
    $('#drag' + objetosMenu[0].num + " img.imgLupa").attr("src", '/clinica/utilidades/imagenes/acciones/lupa.gif');



}

function cerrarForma(obj) {
    activarLI('basio');
    if (obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id == 'divIndicadores')
        // $('#divIndicadores').html('');
        ocultar('divIndicadores');
    if (obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id == 'divSubVentanaAgendaNoPlaneada')
        ocultar('divSubVentanaAgendaNoPlaneada');
    if (obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id == 'divListaEspera')
        ocultar('divListaEspera');
    if (obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id == 'divTraerListaEspera')
        ocultar('divTraerListaEspera');

    //alert(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id);
    if (obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id == 'drag' + objetosMenu[0].num) {

        $('#' + obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id).slideUp();
        //document.getElementById(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id).innerHTML = "";
        $('#' + obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id).remove();
        $('#li' + objetosMenu[0].num).remove(); //quitar opcion del menu de ventanas activas
        //document.getElementById(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id).parentNode.removeChild(document.getElementById(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id));
        //alert(objetosMenu[0].num);
        objetosMenuaux = null;
        objetosMenuaux = new Array();
        //alert(objetosMenu.length);
        for (i = 0; i < objetosMenu.length - 1; i++) {
            //pasar todos los elementos del array de una posicion a la inferior del otro array borrando el primero
            objetosMenuaux[i] = objetosMenu[i + 1];
        }
        objetosMenu = new Array();
        objetosMenu = null;
        objetosMenu = objetosMenuaux;
        //alert(objetosMenu.length);

        if (objetosMenu.length > 0) {
            ventanaActual = objetosMenu[0];
            mensaje = ventanaActual.mensaje;
        } else {
            ventanaActual = null;
            mensaje = "";
        }

        if (objetosMenu.length > 1) {
            deshabilitarVentana();
        }
        if (objetosMenu.length > 0) {
            habilitarVentana();
        }
        actualizarMensaje();
    }
    // alert(objetosMenu[0].num);
    //objetosMenu.;
    //document.getElementById("drag"+objetosMenu[0].num).innerHTML = "";
}


/*function buscaVentanaId(id){
	//alert("id: "+id);
	for(i=1;i<=idReg;i++){
	 //alert("ventanas[i].id: "+ventanas[i].id); 
		 if(ventanas[i].id==id && ventanas[i].activo==true){
			return i;
	 }
	}
	return 0;
 }*/



function actualizarMensaje() {
    document.getElementById('mensaje').innerHTML = "<label>" + mensaje + "</label>";
}



/* var varajaxMenu;
 function llenarinfoMenu(){
		if(varajaxMenu.readyState == 4 ){
			if(varajaxMenu.status == 200){
			   document.getElementById('main').innerHTML = varajaxMenu.responseText;
			   cargarFocos();
			   actualizarMensaje();
			   //alert(focos);
			   //if(focos.length>0)
			   if(focos[1])
			      document.getElementById(focos[1]).focus();
			  }else{
					swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			  }
		 } 
		if(varajaxMenu.readyState == 1){
		     document.getElementById('main').innerHTML =	crearMensaje();
		 }
    }

 function cargarMenu(pagina){
	    posicionActual=-1;
		//setResultados=null;
        varajaxMenu=crearAjax();
        valores_a_mandar="";
		varajaxMenu.open("POST",pagina,true);
		varajaxMenu.onreadystatechange=llenarinfoMenu;
		varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		varajaxMenu.send(valores_a_mandar);   
 }	
 
 function actualizarMensaje(){
	 document.getElementById('mensaje').innerHTML = "<label>"+mensaje+"</label>";
  }*/

/*function buscaVentana(num){
//alert("id: "+id);
for(i=1;i<=idReg;i++){
 //alert("ventanas[i].num: "+ventanas[i].num+" num: "+num); 
	 if(ventanas[i].num==num && ventanas[i].activo==true){
		return i;
 }
}
return 0;
}*/

///// funcion que se llama cuando se debe cargar una pagina o ventana nueva 
var topSu = 1;

function llenarinfoMenu() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            if (document.getElementById("drag" + ventanaActual.num) == null) {
                insertarNuevaVentana();
                contenedor = document.createElement("DIV");
                contenedor.id = "drag" + ventanaActual.num;

                contenedor.style.position = "absolute";
                //				  contenedor.style.left="150px"; // distancia left de ventanas del menu principal al desplegarce  izquierda

                contenedor.style.top = topSu + "px";
                contenedor.style.left = 10 + topSu + "px"; /*ESTABA EN 90*/
                topSu = topSu + 21;


                //contenedor.setAttribute('onclick',"sobreponer(this);");
                contenedor.setAttribute('onmouseup', "sobreponer(this);");
                document.getElementById("main").appendChild(contenedor);
                mensaje = ventanaActual.mensaje;

                document.getElementById("drag" + ventanaActual.num).style.zIndex = 51;
                /////crear opcion en menu de ventanas activas
                contenedor = document.createElement("li");
                contenedor.id = "li" + ventanaActual.num;
                vinculo = document.createElement("a");
                vinculo.href = "#";
                vinculo.appendChild(document.createTextNode(ventanaActual.opc));
                vinculo.setAttribute('onclick', "sobreponer(document.getElementById('drag" + ventanaActual.num + "'))");
                contenedor.appendChild(vinculo);
                document.getElementById("ventanasActivas").appendChild(contenedor);

                if (objetosMenu.length > 1) {
                    deshabilitarVentana();
                }
                habilitarVentana();
            } else {
                $("#drag" + ventanaActual.num).show();
            }

            document.getElementById("drag" + ventanaActual.num).innerHTML = varajaxMenu.responseText;
            actualizarMensaje();
            mover(ventanaActual.num);
            $("#drag" + ventanaActual.num).find("#tituloTexto").dropShadow({ left: 2, top: 2, blur: 1, opacity: 1, color: "black", swap: false });
            switch (ventanaActual.opc) {

                case 'notasNefro':
                    $("#tabsnotasSegumiento").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                break;

                case 'notasVih':
                    $("#tabsnotasSegumiento").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                break;
                
                case 'apoyoDx02':
                    $("#tabsSubirDocumentos").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
                    break;

                case 'comprobanteEntrada':                    
                    calendario('txtFechaDesdeDoc',1);
                    calendario('txtFechaHastaDoc',1);
                    calendario('txtFechaVencimiento',1); 
                    break;
                
                case 'ordenesCompra':
                    calendario('txtFechaDesdeDoc',1);
                    calendario('txtFechaHastaDoc',1);
                    calendario('txtFechaDocumento',1);
                break; 
                

                case 'folios':
                    $("#tabsParametrosFolio").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    setTimeout(() => {
                        buscarHC('listGrillaTiposFormulario', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                        setTimeout(() => {
                            tabActivoFoliosParametros('divFoliosTipoAdmision')
                        }, 300);
                    }, 200);
                    break;

                case 'autorizaciones':
                    $("#tabsAutorizaciones").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                break;

                case 'planT':
                    $("#tabsPlanT").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    setTimeout(() => {
                        buscarHC('listGrillaAutorizaciones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')},200);
                    break;

                case 'contactosPaciente':
                    calendario('txtFechaNacimientoUrgencias', 0)
                    break;

                case 'planes':
                    $("#tabsPrincipalPlanes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    calendario('txtFechaInicio', 1);
                    calendario('txtFechaFinal', 1);
                    break;

                    case 'panelMedicamentos':
                    calendario('txtFechaDesdeP', 1);
                    calendario('txtFechaHastaP', 1);
                    buscarSuministros('listaPanelMedicamentos');
                    break;
                    
                case 'apoyoDx':
                    calendario('txtFechaDesdeP', 1);
                    calendario('txtFechaHastaP', 1);
                    buscarHC('listGrillaApoyoDiagnotico', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');
                    break;

                case 'pruebaLaboratorio':
                    console.log(valorAtributo('txtIdLaboratorioEmpresa'))
                    buscarRutas('listaHistoriaClinicaLaboratorio');
                break;

                case 'plantillaLaboratorio':
                    console.log(valorAtributo('txtIdPlantillaLaboratorio'))
                    buscarHistoria('listaPlantillaLaboratorio');
                break;

                case 'listaEsperaAgendamiento':
                    calendario('txtFechaCita', 0)
                    break;

                case 'principalOrdenes':
                    calendario('txtFechaAgenda', 1)
                    break;

                case 'cartera':
                    calendario('txtFechaDesdeFactura', 0)
                    calendario('txtFechaHastaFactura', 0)
                    break;

                case 'leerNotificacion':
                    buscarNotificacion('listGrillaLeerNotificacion')
                    break;
                case 'asistenciaPacientes':
                    calendario('txtBusFechaDesde', 1)
                    calendario('txtBusFechaHasta', 1)
                    break;
                case 'principalDocumentos':
                    calendario('txtFechaModifica', 0)
                    calendario('txtFechaDesdeDoc', 1)
                    calendario('txtFechaHastaDoc', 1)
                    calendario('txtFechaInicioTerapia', 1)
                    break;
                    case 'coordinacionHomecare':
                        calendario('txtDocFechaDesde', 1)
                        calendario('txtDocFechaHasta', 1)
                        calendario('txtFechaInicioTerapia', 1)
                        $("#tabsCoordinacionTerapia").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
                        tabActivo = "divProcedimientosOrdenados";
                        break;


                case 'codigoBarras':
                    document.getElementById('txtCodBarrasBus').focus()
                    break;
                case 'Facturas':
                    calendario('txtFechaAdmision', 1)
                    calendario('txtFechaDesdeFactura', 1)
                    calendario('txtFechaHastaFactura', 1)
                    calendario('txtFechaIngreso', 0)
                    calendario('txtFechaEgreso', 0)
                    calendario('txtNacimiento', 0)
                    break;

                case 'recibos':
                    calendario('txtFechaRecibo', 0)
                    break;

                case 'solicitudes':
                    idArticulo = '';
                    calendario('txtFechaDesdeDoc', 1)
                    calendario('txtFechaHastaDoc', 1)
                    break;
                case 'CrearBodega':
                    asignaAtributo('cmbIdBodega', '', 1)
                    break;
                case 'principalArticulo':
                    asignaAtributo('txtIdArticulo', '', 1)
                    calendario('txtVigInvima', 0)
                    break;
                case 'principalDocumentos':
                    calendario('txtFechaDesdeDoc', 1)
                    calendario('txtFechaHastaDoc', 1)

                    break;
                case 'devoluciones':
                    calendario('txtFechaDesdeDoc', 1)
                    calendario('txtFechaHastaDoc', 1)
                    break;

                case 'devolucionCompras':
                    calendario('txtFechaDocumento', 0);
                    calendario('txtFechaDesdeDoc', 1);
                    calendario('txtFechaHastaDoc', 1);
                    calendario('txtFechaDesdeDev', 1);
                    calendario('txtFechaHastaDev', 1);
                    idArticulo = '';
                    break;
                case 'salidasDeBodega':
                    calendario('txtFechaDocumento', 0);
                    calendario('txtFechaDesdeDoc', 1);
                    calendario('txtFechaHastaDoc', 1);
                    idArticulo = '';
                    break;
                case 'facturasTrabajo':
                    calendario('txtFechaDesdeTra', 0);
                    calendario('txtFechaHastaTra', 0);
                    calendario('txtFechaEntrega', 1);
                    calendario('txtFechaRecepcion', 1);
                    calendario('txtFechaTrabajoEntregado', 1);
                    calendario('txtFechaRecibeF', 1);
                    calendario('txtFechaCumpli', 1);
                    calendario('txtEntrega', 0);
                    break;

                case 'entradasDeBodega':
                    calendario('txtFechaDocumento', 0);
                    calendario('txtFechaDesdeDoc', 1);
                    calendario('txtFechaHastaDoc', 1);
                    calendario('txtFechaVencimiento', 0);
                    break;

                case 'gestionarEvento':
                    calendario('txtFechaInicia', 1)
                    calendario('txtFechaEntrega', 1)
                    calendario('txtFechaAplazada', 1);
                    calendario('txtFechaRecibe', 0);
                    break;

                case 'agenda':
                    calendario("txtFechaNac", 1);
                    //calendario('txtFechaPacienteCita', 1);
                    //calendario('txtFechaVigencia', 0);
                    //calendario('txtFechaTerapiaEditar', 0);

                    //calendario('txtFechaNacOrdenes', 0);
                    //calendario('txtFechaVigenciaOrdenes', 0);

                    var f = new Date();
                    var mes = (f.getMonth() + 1).toString();
                    var anio = f.getFullYear().toString();
                    asignaAtributo('lblMes', mes, 0)
                    asignaAtributo('lblAnio', anio, 0)

                    //calendario('txtFechaNac', 1)
                    buscarAGENDA('listDias')
                    $("#tabsCoordinacionTerapia").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    break;

                case 'despachoProgramacionProcedimientos':
                    var f = new Date();
                    var mes = (f.getMonth() + 1).toString();
                    var anio = f.getFullYear().toString();
                    asignaAtributo('lblMes', mes, 0)
                    asignaAtributo('lblAnio', anio, 0) // para primera pantalla del mes actual						 					 
                    asignaAtributo('lblFechaSeleccionada', document.getElementById('lblFechaDeHoy').lastChild.nodeValue, 0)
                    break;

                case 'listaEspera':
                    buscarAGENDA('listProcedimientoLE');
                    calendario('txtFechaNac', 0)
                    calendario('txtFechaVigencia', 0);
                    calendario('txtFechaVigenciaLE', 0);
                    // llamarAutocomIdDescripcionConDato('txtAdministradora1',308)						 						 
                    break;
                case 'listaEsperaCirugia':
                    buscarAGENDA('listCitaCirugiaProcedimientoLE');
                    calendario('txtFechaNac', 0)
                    calendario('txtFechaVigencia', 0);
                    calendario('txtFechaVigenciaLE', 0);
                    //	 llamarAutocomIdDescripcionConDato('txtAdministradora1',308)							 							 						 							 						 
                    break;

                case 'horarios':
                    var f = new Date();
                    var mes = (f.getMonth() + 1).toString();
                    var anio = f.getFullYear().toString();
                    asignaAtributo('lblMes', mes, 0)
                    asignaAtributo('lblAnio', anio, 0)
                    calendario('txtFechaDestino', 1)
                    calendario('txtFechaDestinoCita', 1)
                    buscarAGENDA('listDiasC')
                    break;

                case 'agendaHomecare':
                    calendario('txtFechaInicio', 1);
                    calendario('txtFechaFin', 1)

                break;

                case 'reportarNoConformidad':
                    $("#drag" + ventanaActual.num).find('#txtFechaEvento').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });

                    $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#tabsEventos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    break;

                case 'administrarNoConformidad':
                    $("#drag" + ventanaActual.num).find('#txtFechaEvento').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#tabsEventos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    $("#tabsEventosIndicadores").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfHasta2').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    break;

                case 'reportarEventoAdverso':
                    // swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
                    break;


                case 'administrarEventoAdverso':
                    $("#drag" + ventanaActual.num).find('#txtFechaEvento').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });

                    $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaCuandoOportMejora').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#tabsEventosEA").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    $("#tabsEventos").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });


                    $("#tabsEventosIndicadores").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfDesde2').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaEventoNoConfHasta2').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#tabsEventosPlanDeAccion").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });

                    setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1);
                    break;

                case 'administrarPlanMejora':

                    $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#tabsEventosPlanDeAccion").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });

                    $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    break;

                case 'administrarPlatin':
                    tabActivo = '1';

                    $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaIniPlatin').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#tabsPrincipal").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });

                    $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    buscarPlatin('administrarPlatin', '/clinica/paginas/accionesXml/buscarHc_xml.jsp')
                    break;

                case 'fichaPlatin':
                    tabActivo = '1';

                    $("#drag" + ventanaActual.num).find('#txtBusFechaDesde').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtBusFechaHasta').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaIniPlatin').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaFinPlatin').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });

                    $("#tabsPrincipal").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });

                    $("#drag" + ventanaActual.num).find('#txtFechaIniPlanAccion').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaCuando').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    // alert('ddd');
                    //buscarPlatin('lista','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp') 
                    buscarPlatin('fichaPlatin', '/clinica/paginas/accionesXml/buscarHc_xml.jsp')

                    setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1500);

                    setTimeout("llamarAutocomIdDescripcion('txtIdPaciente',15)", 3500);

                    break;

                case 'Calendario':
                    llenarCalendario();
                    break;

                case 'citasE':
                    // llenarCitas();
                    vertanasMoviblesConPunteroMouse('divTraerListaEspera', 3);
                    // vertanasMoviblesConPunteroMouse('divListaEspera',23);		

                    $('#drag' + ventanaActual.num).find('#divListaEspera').draggable({
                        zIndex: 20000,
                        ghosting: true,
                        opacity: 0.7,
                        //containment : 'parent',
                        handle: '#tituloForma'
                    });

                    vertanasMoviblesConPunteroMouse('divSubVentanaAgendaNoPlaneada', 1);


                    $("#drag" + ventanaActual.num).find('#txtBusFechaDesdeListaEspera').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });

                    $("#drag" + ventanaActual.num).find('#txtBusFechaHastaListaEspera').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    $("#drag" + ventanaActual.num).find('#txtFechaNacimientoListaEspera').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });

                    dispararCronometro('100', 'citas');


                    break;

                case 'misCitasHoy':
                    //	buscarMisPacienteHoy();
                    calendario('txtBusFechaDesde', 1)
                    calendario('txtBusFechaHasta', 1)
                    break;

                case 'reportes':
                    calendario('txtBusFechaDesde', 1)
                    calendario('txtBusFechaHasta', 1)
                    break;

                case 'fichaTecnica':
                    $("#drag" + ventanaActual.num).find('#txtFechaVigenciaIni').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                    break;
                case 'indicador':
                    $("#tabsIndicadores").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });

                    break;

                case 'documentos':
                    calendario('txtDocFechaDesde', 1)
                    calendario('txtDocFechaHasta', 1)
                    break;

                case 'paciente':
                    tabActivo = '1';
                    calendario('txtFechaNacimiento', 1)
                    buscarPlatin('paciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')

                    setTimeout("llamarAutocomIdDescripcion('txtBusIdPaciente',15)", 1);
                    $("#drag" + ventanaActual.num).find('#txtFechaNacimiento').datepicker({
                        changeMonth: true,
                        changeYear: true
                    });

                    break;


                case 'principalHC':
                    calendario('txtFechaDesdeFolio', 1)
                    calendario('txtFechaHastaFolio', 1)
                    calendario('txtFechaAgenda', 1)
                    document.getElementById('txtContrasenaAdm').focus();
                    break;

                case 'admision':
                    calendario('txtFechaNac', 1)
                    //calendario('txtFechaVigenciaAutorizacion', 1)
                    calendario('txtFechaTraerCita', 1)
                    calendario('txtFechaNacimientoUrgencias', 0)
                    break;

                case 'admisiones':
                    calendario('txtFechaNac', 0)
                    //calendario('txtFechaVigenciaAutorizacion', 1)
                    calendario('txtFechaTraerCita', 1)
                    calendario('txtFechaNacimientoUrgencias', 0)
                    comboDependienteSede('cmbIdEspecialidad', '849')
                    $("#tabsDetallesFactura").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    $("#tabsElementosPendientes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                    setTimeout(() => {
                        comboDependienteEmpresa('cmbIdProfesionales', '77')
                        setTimeout(() => {
                            comboDependienteSede('cmbIdProfesionalesFactura','77')
                            setTimeout(() => {
                                comboDependienteEmpresa('cmbIdTipoServicio','74')
                            }, 200);
                        }, 200);
                    }, 200);
                    break;

                case 'procedimientos':
                    calendario('txtFechaDesdeP', 1)
                    calendario('txtFechaHastaP', 1)
                    break;
                case 'planes':
                    calendario('txtFechaInicio', 1)
                    calendario('txtFechaFinal', 1)
                    calendario('txtFech', 1)
                    break;
                case 'recepcionTecnica':
                    calendario('txtFechaVtoRecepTecnica', 1);
                    break;
                case 'trasladoBodegas':
                    calendario('txtFechaDocumentoDesde', 1);
                    calendario('txtFechaDocumentoHasta', 1);
                    break;

                case 'salidasVentas':
                    calendario('txtFechaNac', 1);
                    comboCargarBodegaVentas('cmbBodegaVentas');
                    break;

                case 'articulosPlan':

                    break;

                case 'parlante':
                    buscarAGENDA('listPacientesParlante');
                    break;
                
                case 'tipificacion_facturas':
                    calendario('txtFechaDesdeFactura', 1)
                    calendario('txtFechaHastaFactura', 1)
                    break;

                case 'gestionRiesgos':
                    calendario('txtFechaDesdeRiesgo', 1)
                    calendario('txtFechaHastaRiesgo', 1)
                    break;

                    case 'infosedes':
                        buscarEvento('sedes')
                    break;

                default:

                    calendario('txtFechaOrdenMed', 1)
                    calendario('txtFechaDocumento', 1)
                    calendario('txtBusFechaConsumo', 1)
                    calendario('txtFechaConsumo', 1)
                    calendario('txtFechaConsumo2', 1)
                    calendario('txtFechaReferencia', 0)
                    calendario('txtBusFechaAdminis', 1)

                    calendario('txtFechaConsumo2', 0)
                    calendario('txtFechaConsumo', 0)
                    calendario('txtFechaHastaOrdenMed', 1)
                    calendario('txtFechaDesdeOrdenMed', 1)
                    calendario('lblFecha', 1) //DEVOLUION FARMACIA	
                    calendario('txtFechaDocDesde', 1)
                    calendario('txtFechaDocHasta', 1)

                    calendario('txtFechaDocumentoDesde', 1)
                    calendario('txtFechaDocumentoHasta', 1)
                    calendario('txtFechaCierreDesde', 1)
                    calendario('txtFechaCierreHasta', 1)
                    calendario('txtFechaNac', 1);

                    break;


            }
            //CC: uso una clase en el txt para no poner aca todos los txt q sean fecha
            $("#drag" + ventanaActual.num).find('.fechasTxt').datepicker({
                changeMonth: true,
                changeYear: true,
            });
            //CC

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        //document.getElementById('main').innerHTML =	crearMensaje();
    }
}


function cargarCombosPaciente(){
    cargarComboBarrio()
    $('.select2-custom').select2({ dropdownCssClass: "set_ddl_size" });
    $('#txtNacionalidad').val('CO').trigger('change');
}

function cargarComboBarrio() {
    pagerSelect2(`idQueryCombo=${10391}&cantCondiciones=1&condicion1=${valorAtributo('txtMunicipio')}`, 'txtBarrio').then((idCombo) => {
        if (traerDatoFilaSeleccionada('listGrilla', 'Barrio') != '') {
            $('#txtBarrio').select2('data', { id: traerDatoFilaSeleccionada('listGrilla', 'Barrio').split('-')[0], text: traerDatoFilaSeleccionada('listGrilla', 'Barrio').split('-')[1] })
        }
        desHabilitar(idCombo, 0)

    })
}