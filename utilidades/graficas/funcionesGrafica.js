var RANGO_EDAD, GENERO, TIPO;

//variables de graficas 
var tituloEjeX, tituloEjeY;
var categoria;
var datosgrafica;
var toJsongrafica;
var tituloGrafica;
var subtituloGrafica;
var maximo;
var minimo;
var grafica;
var jsonGrafica;
var punteada;

RANGO_EDAD = "0-2";
GENERO = "M";
TIPO = "P";
var valores_a_mandar = "";

function verImagen() {

    RANGO_EDAD = "0-2";
    GENERO = "M";
    TIPO = "P";

    grafica = 'http://190.60.242.160:8383/clinica/utilidades/graficas/datos.json';
    jsonGrafica = new Array();
    $.getJSON(grafica, function (data) {
        jsonGrafica = data;
        console.log(jsonGrafica.datosGrafica);
        consultar();
        graficar();
    });

}

function verImagen2() {

    RANGO_EDAD = "2-5";
    GENERO = "M";
    TIPO = "P";
    //grafica = 'http://190.60.242.160:8383/clinica/utilidades/graficas/datos2.json';
    grafica = 'http://190.60.242.160:8001/servicio/grafica?idEvolucion=519&tabla=agnut';

    jsonGrafica = new Array();

    $.get(grafica, function (data) {
        jsonGrafica = JSON.parse(data);
        console.log(jsonGrafica);
        consultar();
        graficar();
    });



    /* $.getJSON(grafica, function (data) {
                   jsonGrafica = data;
                   console.log(jsonGrafica.datosGrafica);             
                   consultar();
                   graficar();
               });*/
}


/*function graficaPlantilla(idEvolucion,idPlantillaGrafica){


    var url = 'hc/graficas/grafica.jsp?idEvolucion='+idEvolucion+'&idPlantillaGrafica='+idPlantillaGrafica;
    var dimension = 'width=1150,height=952,scrollbars=YES,statusbar=NO,left=150,top=90';
    window.open(url, '', dimension);


    /*grafica = 'http://190.60.242.160:8001/servicio/grafica?idEvolucion='+idEvolucion+'&tabla='+tabla;

    jsonGrafica = new Array();
    
    $.get(grafica, function (data) {
        jsonGrafica =  JSON.parse(data);
                    console.log(jsonGrafica);             
                    consultar();
                    graficar();
    });


}*/


function consultar() {
    tituloGrafica = jsonGrafica.titulografica;
    subtituloGrafica = jsonGrafica.subtitulografica;
    tituloEjeX = jsonGrafica.tituloejex;
    tituloEjeY = jsonGrafica.tituloejey;
    maximo = jsonGrafica.maximo;
    minimo = jsonGrafica.minimo;
    categoria = jsonGrafica.categoria;
    toJsonGrafica = jsonGrafica.datosGrafica;
    toJsonDatos = jsonGrafica.medida;
    if (minimo == 1 || minimo == 4) {
        punteada = JSON.parse('{"-2":[{"start":0}],"+2":[{"start":0}]}');
    } else {
        punteada = JSON.parse('{"-2":[{"start":0}]}');
    }



    $("#tituloGrafica").html("<h3><b>" + tituloGrafica + "</b></h3><h5>" + subtituloGrafica + "</h5>");


}
//-----------------grafica------------
function graficar() {
    var chart = c3.generate({
        bindto: "#grafica_salud",
        size: {
            height: 630,
            width: 1250
        },
        padding: {
            bottom: 50
        },
        data: {

            json: toJsonGrafica
            ,

            types: {
                '+3': 'line',
                '+2': 'line',
                '+1': 'line',
                '0': 'line',
                '-1': 'line',
                '-2': 'line',
                '-3': 'line',
                'D :Obesidad para la edad gestacional': 'area-spline',
                'C :Sobrepeso para le edad gestacional': 'area-spline',
                'B :IMC adecuado para la edad gestacional': 'area-spline',
                'A :Bajo peso para la edad gestacional': 'area-spline',

            },
            colors: {
                '+3': 'red',
                '+2': 'red',
                '+1': 'orange',
                'A :Bajo peso para la edad gestacional': 'gray',
                 '0': 'green',
                '-1': 'orange',
                '-2': 'red',
                '-3': 'red'
            },
            regions:

                punteada
            //{"-2": [{"start":0}]}


        },

        point: {
            show: true,
            r: 2.5,
            select: {//aumenta el taao del punto cuando se selecciona
                r: 8
            }
        },
        line: {
            connectNull: true,
            width: 30
        },
        axis: {
            x: {
                type: 'category',

                tick: {
                    multiline: true
                    //     culling: {
                    //         max: 10 //numero de categoris visibles en el eje x 
                    //     }
                },

                label: {
                    text: tituloEjeX,
                    position: 'inner-middle'
                },
                categories: categoria
            },
            y: {

                padding: { top: 10, bottom: 20 },
                label: {
                    text: tituloEjeY,
                    position: 'outer-middle'
                },
                max: maximo,
                min: minimo
            },
            y2: {
                //padding: {top: 30, bottom: 30},
                //outer: false,
                show: true,
                default: [minimo, maximo]
                //max: 100,
                //min: 40
            }
        }
        ,
        grid: {
            x: {
                show: true
            },
            y: {
                show: true
            }
        }
        ,
        zoom: {
            enabled: true,
            rescale: true
        },
        legend: {
            position: 'right'
        }
    });


    setTimeout(function () {
        chart.load({
            json: toJsonDatos
            ,
            colors: {
                medida: 'black'
            }
            , types: {
                medida: 'spline'
            }
        });
    }, 1000);

}


function exportImageAsPNG(idChart,idEvolucion,idPlantilla) {
    var nodeList1 = document.getElementById(idChart).querySelector('svg').querySelectorAll('.c3-axis path');
    var nodeList2 = document.getElementById(idChart).querySelector('svg').querySelectorAll('.c3 line');
    var fillLine = Array.from(nodeList1).concat(Array.from(nodeList2));
    fillLine.map(element => {
        element.style.fill = "none";
        if (!element.style.stroke || element.style.stroke === '') {
            element.style.stroke = "rgba(0, 0, 0, 0.52)";
        }

    })

    var nodeList = document.getElementById(idChart).querySelector('svg').querySelectorAll('svg .c3-chart path.c3-shape.c3-shape.c3-line');
    var fillArea = Array.from(nodeList);
    fillArea.map(element => {
        element.style.fill = "none";
    });

    var nodeCircle = document.getElementById(idChart).querySelector('svg').querySelectorAll('.c3-circle');
    var fillCircle = Array.from(nodeCircle);
    fillCircle.map(element => {
        let color = element.style.color;
        element.style.fill = color;
        element.style.stroke = color;

    });

    //Exportamos imagen
    var svg = $(`#${idChart}`).find('svg')[0];
    var img = document.getElementById("fromcanvas");
    svg.toDataURL("image/png", {
        callback: (data) => {
           // console.log(data)
           // img.setAttribute("src", data)
            valores_a_mandar = 'base64=' + data + "&ext=" + "png" + "&nombre=" + idEvolucion+"_"+idPlantilla;
            ajaxGuardarImagen();
        }
    })
}


function ajaxGuardarImagen() {
    varajax = crearAjax();
    varajax.open("POST", '/clinica/paginas/hc/graficas/guardar_grafica_xml.jsp', true);
    varajax.onreadystatechange = respuestaGuardarImagen;
    varajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    varajax.send(valores_a_mandar);

}

function respuestaGuardarImagen() {
    if (varajax.readyState == 4) {
        if (varajax.status == 200) {
            raiz = varajax.responseXML.documentElement;
            MsgAlerta = raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data;

            if (raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data == '') {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {                    
                    
                } else {
                    swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
                }
            }
            else alert('PROBLEMAS AL REGISTRAR\n' + MsgAlerta)

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax.readyState == 1) {
    }
}

function crearAjax() {
    var ajax = false;
    if (!ajax && typeof XMLHttpRequest != 'undefined') {
      ajax = new XMLHttpRequest();
    }
    return ajax;
  }