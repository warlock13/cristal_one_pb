  <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
  <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
  <%@ page import = "Sgh.Utilidades.*" %>
  <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
  <%@ page import = "Clinica.Presentacion.*" %>
  
  <%@ page import="net.sf.jasperreports.engine.*" %> 
  <%@ page import="net.sf.jasperreports.engine.util.*" %>
  <%@ page import="net.sf.jasperreports.engine.export.*" %>
  <%@ page import="net.sf.jasperreports.view.JasperViewer" %>
  <%@ page import="javax.naming.*" %>  
  <%@ page import="java.io.*" %> 
  <%@ page import="java.awt.Font" %>   
  <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->  
<% 	 

	System.out.println("Conectado a: " + beanSession.cn.getServidor());  
	boolean estado;
	String ruta="";
%> 
 <!DOCTYPE HTML >  
 <% 
    Connection conexion = beanSession.cn.getConexion(); 
	ruta = "paginas/ireports/citas/";
%>


<link rel="stylesheet" type="text/css" href="estiloAsistencia_.css" />
<SCRIPT language=javascript src="asistencia.js"></SCRIPT>

<%		
    System.out.println("banderaAsiste= "+request.getParameter("banderaAsiste"));
    System.out.println("identificacion= "+request.getParameter("identificacion"));	
   
    String siNo = "", identificacion = "%";
    if(request.getParameter("banderaAsiste") != null ){
		 siNo = request.getParameter("banderaAsiste") ;
	}
	else siNo = "NO";
	
    if(request.getParameter("identificacion") == null || request.getParameter("identificacion") == "" ){
		 identificacion = "%";	
	}
	else  identificacion = request.getParameter("identificacion");
%>	

<div id="pagewrap"  style=" width:57%;  display:block; z-index:1;">	

    <h4 class="widgettitulo"  onclick="javascript:refrescarPagina()" >ASISTE A CITAs</h4>    
	<div id="content">	
       <form method="post" enctype="multipart/form-data" target="iframeBusca" id="formBuscar" >   
            <table width="100%"  border="1" >
              <tr align="center">
                <td width="20%">ASISTIO:</td>
                <td width="20%">
                       <select id="cmbAsistio"  style="width:90%" >
						   <%  if( siNo.equals("NO") ){  %>
                             <option value="NO" selected>NO</option>
                             <option value="SI">SI</option>                         
                           <%}else{ %>  
                             <option value="NO">NO</option>
                             <option value="SI" selected>SI</option>                                                
                           <%}%>  
                       </select>  
                 </td>                                
                <td width="60%">ID:<input type="text" id="identificacion" style="width:60%"> </td>
                <td width="60%">
                          <input type="button" value="BUSCAR" onClick="botonBuscar()">                      
                </td>
              </tr>
            </table>
       </form>                 
                  
                      <table width="100%">
                        <tr>
                          <td width="40%"><h4>Paciente</h4></td>
                          <td width="50%"><h4>Tipo cita</h4></td> 
                          <td width="10%"><h4>Hora</h4></td>                                
                        </tr>
                      </table>  
          <%	
          try{
              Statement stmt = conexion.createStatement();	
              String SQL = "  select" +
                           "	ad.id, "+
                           "	CAST( to_char(ad.fecha_cita + interval '0 hour','HH24:MI')  AS varchar(5)) as hora, "+
                           "	p.identificacion, "+
                           "	lower(p.nombre1 ||' ' ||  p.apellido1 ) nombre_completo, "+
                           "	lower(t.nombre) nom_tipo "+
                           "	from "+
                           "	citas.agenda a "+
                           "	inner join citas.agenda_detalle ad on ad.id_agenda = a.id "+
                           "	inner join citas.tipo t on ad.id_tipo = t.id "+
                           "	inner join hc.vi_paciente p on ad.id_paciente = p.id "+
                           "	where a.id_sede::text  like '1' "+
                           "	and a.fecha_agenda = to_char(now(), 'dd/mm/yyyy')::date "+
                           "  and ad.asiste = '"+siNo+"' " +
                           "  and p.identificacion like '%'|| '" +identificacion+ "' ||'%' " +
                           "	 order by ad.fecha_cita "; 	
              System.out.println( SQL );
              
                  ResultSet rs = stmt.executeQuery(SQL);
              
              while (rs.next()){
          %>		
                      <table width="100%" id="idTablaListado">
                        <tr id="<%= rs.getString("id")+"-"+rs.getString("identificacion")+"-"+rs.getString("nombre_completo")+"-"+rs.getString("nom_tipo")+"-"+rs.getString("hora")  %>" onClick="asignarAVentanilla(this.id)">
                          <td width="40%" ><h7><%= rs.getString("nombre_completo") %></h7></td>
                          <td width="50%" ><h7><%= rs.getString("nom_tipo") %></h7></td> 
                          <td width="10%" ><h7><%= rs.getString("hora") %></h7></td>                                
                        </tr>
                      </table> 		
          <%		
              }	
              stmt.close();
              conexion.close();
          }
              catch ( Exception e ){
              System.out.println(e.getMessage());
          }
          %>
          </div>	

</div>


<div id="idVentanillaPaciente" style=" width:57%; display:none; z-index:2; ">
    <div id="idDivTransparencia"  style=" position:absolute; width:57%; height:2000px; top:1px; left:11px;  background-color:#B6B6B6; z-index:3; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div>  
    <div id="idContenido"   align="center"  style="z-index:4;  position:absolute; top:20px; left:15px; width:57%; ">     
            <table width="100%" align="center" border="0" bgcolor="#B0E6FF">
              <tr>
                <td width="30%"><h4>Id cita: </h4></td>
                <td width="70%"><h7><label id="id_cita"></label></h7></td> 
              </tr>
              <tr>
                <td width="30%"><h4>Identificacion:</h4></td>
                <td width="70%"><h7><label id="identificacion"></label></h7></td> 
              </tr>
              <tr>
                <td width="30%"><h4>Paciente:</h4></td>
                <td width="70%"><h7><label id="nomPaciente"></label></h7></td> 
              </tr>              
              <tr>
                <td width="30%"><h4>Tipo Cita:</h4></td>
                <td width="70%"><h7><label id="idTipoCita"></label></h7></td> 
              </tr>
              <tr>
                <td width="30%"><h4>Hora:</h4></td>
                <td width="70%"><h7><label id="hora"></label></h7></td> 
              </tr>  
              <tr >
                <td width="70%" colspan="2" align="center">
                  <table width="100%" align="center">
                    <tr>
                      <td align="center">
                        <input type="button" value="CERRAR" onClick="cerrarVentanita()">
                      </td>
                      <td align="center">
                          <form method="post" enctype="multipart/form-data" target="iframeAsiste" id="formPacienteAsiste" >   
                              <input type="button" value="ASISTIO" onClick="pacienteAsiste()">                      
                          </form>                        
                      </td>                      
                    </tr>
                  </table>
                </td> 
              </tr>                           
            </table> 
    </div>
</div>    



</body></html>

