<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<%  beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
   <tr>
       <td>
           <!-- AQUI COMIENZA EL TITULO -->
           <div align="center" id="tituloForma">
               <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
               <jsp:include page="../titulo.jsp" flush="true">
                   <jsp:param name="titulo" value="CONFIGURACION PLAN DE TRATAMIENTO" />
               </jsp:include>
           </div>
           <!-- AQUI TERMINA EL TITULO -->
       </td>
   </tr>
   <tr>
       <td>
           <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
               <tr>
                   <td>
                    <div id="divParaVentanita"></div>
                    <div id="tabsPlanT" style="width:98%; height:auto">
                        <ul>
                            <li>
                                <a href="#divPlanTratamiento" onclick="tabActivoFoliosParametros('divPlanTratamiento')">
                                    <center>PLAN TRATAMIENTO</center>
                                </a>
                            </li>
                            <li>
                                <a href="#divFolios" onclick="tabActivoFoliosParametros('divFolios')">
                                    <center>ASOCIACION</center>
                                </a>
                            </li>
                        </ul>
                        <div id="divPlanTratamiento" class="titulos">
                            <div style="overflow:auto;height:300px; width:100%" id="divContenido">
                                <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                                 <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                     <tr class="titulosCentrados">
                                         <td colspan="3">OBJETIVOS GENERALES</td>
                                     </tr>
                                     <tr class="titulos" align="center">
                                         <td width="20%">ID</td>
                                         <td width="40%">DESCRIPCION</td>
                                         <td width="20%">&nbsp;</td>
                                     </tr>
                                     <tr class="estiloImput">
                                         <td><input type="text" id="txtIdObj" style="width:90%" onkeyup="javascript: this.value= this.value.toUpperCase();"  maxlength="4"  />
                                         </td>
                                         <td><input type="text" id="txtDesObj" style="width:90%"  />
                                         </td>                                  
                                         <td>
                                             <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="BUSCAR" onclick="buscarParametros('listGrillaObjG')" />
                                         </td>
                                     </tr>
                                 </table>
                                <table id="listGrillaObjG" class="scroll" width="100%"></table>
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                         <tr><td>&nbsp;</td></tr>
                                         <tr class="titulos">
                                             <td width="20%">ID</td>
                                             <td align="left"><input type="text" id="txtIdOG" style="width:20%"  /></td>
                                         </tr>
                                         <tr class="titulos">
                                             <td width="20%">DESCRIPCION</td>
                                             <td align="left"><textarea id="txtDesOG" style="width: 90%; height: 50px;"></textarea></td>
                                         </tr>
                                         <tr class="estiloImput">
                                             <td><input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="CREAR" onclick="modificarCRUDParametros('crearOG')" />
                                                 <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUDParametros('modificarOG')" />
                                                 <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUDParametros('eliminarOG')" /></td>
                                         </tr>
                                 </table>                           
                             </div>
                             <div style="overflow:auto;height:300px; width:100%" id="divContenido">
                                 <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                     <tr class="titulosCentrados">
                                         <td colspan="3">OBJETIVOS ESPECIFICOS</td>
                                     </tr>                                
                                 </table>
                                <table id="listGrillaObjE" class="scroll" width="100%"></table>
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                         <tr><td>&nbsp;</td></tr>
                                         <tr class="titulos">
                                             <td width="20%">ID</td>
                                             <td align="left"><input type="text" id="txtIdOE" style="width:20%"  /></td>
                                         </tr>
                                         <tr class="titulos">
                                             <td width="20%">DESCRIPCION</td>
                                             <td align="left"><textarea id="txtDesOE" style="width: 90%; height: 50px;"></textarea></td>
                                         </tr>
                                         <tr class="estiloImput">
                                             <td><input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="CREAR" onclick="modificarCRUDParametros('crearOE')" />
                                                 <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUDParametros('modificarOE')" />
                                                 <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUDParametros('eliminarOE')" /></td>
                                         </tr>
                                 </table>                
                             </div>
                             <div style="overflow:auto;height:300px; width:100%" id="divContenido">
                                 <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                     <tr class="titulosCentrados">
                                         <td colspan="3">PLAN DE TRATAMIENTO</td>
                                     </tr>                                                                
                                 </table>
                                <table id="listGrillaPT" class="scroll" width="100%"></table>
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                         <tr><td>&nbsp;</td></tr>
                                         <tr class="titulos">
                                             <td width="20%">ID</td>
                                             <td align="left"><input type="text" id="txtIdPT" style="width:20%"  /></td>
                                         </tr>
                                         <tr class="titulos">
                                             <td width="20%">DESCRIPCION</td>
                                             <td align="left"><textarea id="txtDesPT" style="width: 90%; height: 50px;"></textarea></td>
                                         </tr>
                                         <tr class="estiloImput">
                                             <td><input id = "btnCrearPT" title="PTTT1" type="button" class="small button blue" value="CREAR" onclick="modificarCRUDParametros('crearPT')" />
                                                 <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUDParametros('modificarPT')" />
                                                 <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUDParametros('eliminarPT')" /></td>
                                         </tr>
                                 </table>                
                             </div>
                        </div>                    
                       <div id="divFolios" class="titulos">
                        <div style="overflow:auto;height:300px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                             <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                 <tr class="titulosCentrados">
                                     <td colspan="4">TIPOS DE FOLIO</td>
                                 </tr>
                                 <tr class="titulos" align="center">
                                     <td width="30%">ID FOLIO</td>
                                     <td width="30%">ID DIAGNOSTICO</td>
                                     <td width="20%">ID OBJETIVO GENERAL</td>
                                     <td width="20%">&nbsp;</td>
                                 </tr>
                                 <tr class="estiloImput">
                                     <td><input type="text" id="txtIdFolio" style="width:90%" disabled  maxlength="4"  />
                                        <img width="18px" height="18px" align="middle" title="VEN3" id="idLupitaVentanitaFolio" onclick="traerVentanita(this.id,'','divParaVentanita','txtIdFolio','40')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                     </td>
                                     <td><input type="text" id="txtIdDx" style="width:90%" disabled  />
                                        <img width="18px" height="18px" align="middle" title="VEN3" id="idLupitaVentanitaFolio" onclick="traerVentanita(this.id,'','divParaVentanita','txtIdDx','5')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                     </td>                                  
                                     <td><input type="text" id="txtIdOGF" style="width:90%"  />
                                     </td>                                  
                                     <td>
                                         <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="BUSCAR" onclick="buscarParametros('listGrillaFolios')" />
                                     </td>
                                 </tr>
                             </table>
                            <table id="listGrillaFolios" class="scroll" width="100%"></table>
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                     <tr><td>&nbsp;</td></tr>
                                     <tr class="titulos">
                                         <td width="20%">ID FOLIO</td>
                                         <td align="left"><input type="text" id="txtIdFolioA" disabled style="width: 80%;"  />
                                            <img width="18px" height="18px" align="middle" title="VEN3" id="idLupitaVentanitaFolio" onclick="traerVentanita(this.id,'','divParaVentanita','txtIdFolioA','40')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                         </td>
                                     </tr>
                                     <tr class="titulos">
                                        <td width="20%">CIE10</td>
                                        <td align="left"><input type="text" id="txtIdDxA" disabled style="width: 80%;"  />
                                           <img width="18px" height="18px" align="middle" title="VEN3" id="idLupitaVentanitaFolio" onclick="traerVentanita(this.id,'','divParaVentanita','txtIdDxA','5')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                        </td>
                                    </tr>
                                    <tr class="titulos">
                                        <td width="20%">ID OBJETIVO GENERAL</td>
                                        <td align="left"><input type="text" id="txtIdOGA" style="width:20%"  />                                           
                                        </td>
                                    </tr>                                     
                                     <tr class="estiloImput">
                                         <td><input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="ASOCIAR" onclick="modificarCRUDParametros('crearFPT')" />                                             
                                             <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUDParametros('eliminarFPT')" /></td>
                                     </tr>
                             </table>                           
                         </div>                         
                       </div>
                    </div>               
       </td>
   </tr>
</table>
