 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="SEDE ESPECIALIDADES" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>
           <div style="overflow:auto;height:400px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
                   <tr class="titulos" align="center">
                      <td width="60%">NOMBRE</td>
                      <td width="20%">VIGENTE</td>
                      <td width="20%">&nbsp;</td>                            
                   </tr>
                   <tr class="estiloImput"> 
                      <td><input type="text" id="txtBusNombre"  style="width:90%"   /> 
                      </td> 
                      <td><select size="1" id="cmbVigenteBus" style="width:80%"  >	                                        
                          <option value="S">SI</option>
                          <option value="N">NO</option>                  
                         </select>	                   
                      </td>     
                      <td>
                          <input name="btn_BUSCAR" title="SE54" type="button" class="small button blue" value="BUSCAR"  onclick="buscarParametros('listEspecialidad')"/>                    
                      </td>         
                  </tr>	                          
              </table>
              <table id="listEspecialidad" class="scroll" width="100%"></table>  
          </div>


<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3">ESPECIALIDADES POR SEDE
      </td>   
    </tr>  
    <tr>
      <td>
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr>
      <td>
         <table width="100%">
            <tr class="estiloImputIzq2">
              <td width="30%">ID :</td><td width="70%"> <input type="text" id="txtId"  style="width:95%"/></td>
            </tr>
            <tr class="estiloImputIzq2">
              <td width="30%">NOMBRE :</td><td width="70%"><input type="text" id="txtNombre"  style="width:95%"/></td> 
            </tr>     
            
            <tr class="estiloImputIzq2">
              <td width="30%">VIGENTE:</td><td width="70%"><select size="1" id="cmbVigente" style="width:7%"  >	                                        
                  <option value=""></option>
                  <option value="S">SI</option>
                  <option value="N">NO</option>                  
                 </select></td> 
            </tr>
            <tr class="estiloImputIzq2">
                <td width="30%">SERVICIO:</td>
               <td>
                    <select style="width:50%" id="cmbServicio">
                       <option value=""></option>
                           <% resultaux.clear();
                              resultaux=(ArrayList)beanAdmin.combo.cargar(852);   
                              ComboVO cmb32;
                             for(int k=0;k<resultaux.size();k++){
                             cmb32=(ComboVO)resultaux.get(k);
                             %>
                       <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>"><%= cmb32.getDescripcion()%></option>
                          <%}%>  
                    </select>
                </td>
            </tr>
                                                                                      		           
        </table> 
      </td>   
    </tr>   
  </table>

  <table width="100%"  style="cursor:pointer" >
      <tr><td width="99%" class="titulos">
      <input name="btn_limpiar_new" type="button" title="F76" class="small button blue" value="LIMPIAR" onclick="limpiaAtributo('txtId',0);limpiaAtributo('txtNombre',0);  limpiaAtributo('cmbVigente',0);limpiaAtributo('cmbServicio')" />                      
      <input name="btn_CREAR" title="F77" type="button" class="small button blue" value="CREAR" onclick="modificarCRUDParametros('crearEspecialidadSede','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  /> 
      <input name="btn_MODIFICAR" title="F79" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUDParametros('modificarEspecialidad','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
      </td></tr>
  </table>  
  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr>
      <td>
        <table width="100%">        
          <tr class="titulosCentrados">
            <td colspan="3">ASIGNAR SEDE
            </td>   
          </tr>            
          <tr class="estiloImputIzq2">
            <td width="30%">SEDE:</td><td width="70%"><input type="text" id="txtIdSede"  onkeypress="llamarAutocomIdDescripcionConDato('txtIdSede',898)" style="width:90%"/></td> 
          </tr>	
          <tr>
             <td colspan="2" align="center">
               <input id="btn_limpia" title="E55" class="small button blue" type="button" value="LIMPIAR"  onclick="limpiaAtributo('txtIdSede',0);">              
               <input id="btn_crea" title="AS23" class="small button blue" type="button"  value="ASIGNAR SEDE"  onclick="modificarCRUDParametros('asignarSedeEspecialidad')">                               
               <input name="btn_elimina" title="E57" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUDParametros('eliminarEspecialidadSede')">                               
             </td>
          </tr>   
         </table> 
        </td>   
    </tr>   
    <tr>
      <td>
            <table id="listSedeEspecialidad" class="scroll"></table>  
      </td>
    </tr>
  </table>    

  </td></tr>
  </table> 
        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>

