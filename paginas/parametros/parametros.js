

function apareceCursoDeVida() {
    //console.log('entro')
    let elemntDiv = $("#trCursosDeVida");
    let trGenero = $("#trGenero");
    let trAIEPI = $("#trAIEPI");
    //let trPrimeraVez = $("#trPrimeraVez")
    let trTipoCitaRelacionado = $("#trTipoCitaRelacionado")
    if (valorAtributo('cmbProgramaTipoCita') == "4") {
        elemntDiv.show();
        trGenero.show();
        //trPrimeraVez.show();
        if (valorAtributo('cmbPrimeraVezTipoCita') == "N") {
            trTipoCitaRelacionado.show();            
        }
    } else {
        elemntDiv.hide();
        trGenero.hide();
        //trPrimeraVez.hide();
        trTipoCitaRelacionado.hide();
        limpiarSelect2('cmbTipoCitaRelacionado') 
    }
    if (valorAtributo('cmbProgramaTipoCita') == "1") {
        trAIEPI.show();
    } else {
        trAIEPI.hide();
    }
}
function mostrarCmbTipoCitaRelacionado() {
    let trTipoCitaRelacionado = $("#trTipoCitaRelacionado")

    if (valorAtributo('cmbPrimeraVezTipoCita') == "N" && valorAtributo('cmbProgramaTipoCita') === '4') {
        trTipoCitaRelacionado.show();        
    } else {
        trTipoCitaRelacionado.hide();
        limpiarSelect2('cmbTipoCitaRelacionado') 
    }

    
}


function notificacionFIre(texto, valor, query, datosRow, estado) {
    Swal.fire({
        html: texto,
        icon: "warning",
        showCancelButton: "Cancelar",
        confirmButtonText: "Continuar",
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#999999',
        position: "top",
    }).then(function (sweetAlertResult) {
        if (sweetAlertResult.isConfirmed) {

            var sw = valor.replace("'", "").replace("'", "").trim()
            if (estado == "true") {
                sw = true
            } else {
                sw = false
            }

            pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
            valores_a_mandar = "accion=listGrillaTiposFormularioCita";
            valores_a_mandar = valores_a_mandar + `&idQuery=${query}&parametros=`
            add_valores_a_mandar(sw); // pendiente factura
            add_valores_a_mandar(datosRow.ID); // id_tipo_formulario
            add_valores_a_mandar(datosRow.ID_TIPO_CITA); // id_tipo_cita
            ajaxModificar();
        } else {
            buscarParametros('listGrillaTiposFormularioCita');
        }

    });

}
function validarCheckBox(cellvalue, options, rowObject) {
    console.log('cc', cellvalue)
    var name_model = options.colModel.name;
    var checkedStr = "";
    if (cellvalue == 't') {
        checkedStr = " checked=checked ";
    }
    if (cellvalue == 'f') {
        checkedStr = "";
    }

    return `<input type="checkbox" onchange="actualizarCheckbox('${name_model}',this,${rowObject.id})" value="'${cellvalue}  '" ' ${checkedStr}  '/>`;
}
function actualizarCheckbox(name, chkBox, rowObject) {
    var datosRow = jQuery("#drag" + ventanaActual.num)
        .find("#listGrillaTiposFormularioCita")
        .getRowData(rowObject);
    if ($(chkBox).is(":checked")) {
        if (name == 'sw_pendiente_factura') {
            valor = $(chkBox).val()
            notificacionFIre("Actualizar pendiente factura", valor, "4019", datosRow, "true")

        } else if (name == 'sw_alta_consulta') {
            valor = $(chkBox).val()
            notificacionFIre("Actualizar alta consulta", valor, "4020", datosRow, "true")

        }


    }
    else if (!($(chkBox).is(":checked"))) {
        if (name == 'sw_pendiente_factura') {
            valor = $(chkBox).val()
            notificacionFIre("Actualizar pendiente factura", valor, "4019", datosRow, "false")


        }
        else if (name == 'sw_alta_consulta') {
            valor = $(chkBox).val()
            notificacionFIre("Actualizar alta consulta", valor, "4020", datosRow, "false")
        }



    }
    return false;
}
function buscarParametros(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {

        case 'listFinalidad':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2670" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_FINALIDAD', 'FINALIDAD'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID_FINALIDAD', index: 'ID_FINALIDAD', hidden: true },
                    { name: 'FINALIDAD', index: 'FINALIDAD' }
                ],
                height: 100,
                autowidth: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    const comparacion = ["11", "12", "13", "14", "15"];
                    if (comparacion.includes(datosRow.ID_FINALIDAD)) {
                        datosRow.ID_FINALIDAD = datosRow.ID_FINALIDAD.slice(1);
                    }
                    asignaAtributo('cmbFinalidadTipoCita', datosRow.ID_FINALIDAD);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listCausaExterna':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2671" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_CAUSA', 'CAUSA EXTERNA'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID_CAUSA', index: 'ID_CAUSA', hidden: true },
                    { name: 'CAUSA', index: 'CAUSA' },
                ],
                height: 100,
                autowidth: true,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('cmbCausaExternaTipoCita', datosRow.ID_CAUSA);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaFolios':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1164" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFolio'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDx'));
            add_valores_a_mandar(valorAtributo('txtIdOGF'));
            add_valores_a_mandar(valorAtributo('txtIdOGF'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID FOLIO', 'FOLIO', 'CIE10', 'DIAGNOSTICO', 'ID OBJETIVO GENERAL'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID_FOLIO', index: 'ID_FOLIO', width: 5 },
                    { name: 'FOLIO', index: 'FOLIO', width: 40 },
                    { name: 'CIE10', index: 'CIE10', width: 10 },
                    { name: 'NCIE10', index: 'NCIE10', width: 40 },
                    { name: 'ID_OBG', index: 'ID_OBG', width: 5 },
                ],
                height: 100,
                width: 1000,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdFolioA', concatenarCodigoNombre(datosRow.ID_FOLIO, datosRow.FOLIO));
                    asignaAtributo('txtIdDxA', concatenarCodigoNombre(datosRow.CIE10, datosRow.NCIE10));
                    asignaAtributo('txtIdOGA', datosRow.ID_OBG, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaObjG':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1150" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdObj'));
            add_valores_a_mandar(valorAtributo('txtDesObj'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'DESCRIPCION'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID', index: 'ID', width: 20 },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: 80 },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdOG', datosRow.ID, 1);
                    asignaAtributo('txtIdOGA', datosRow.ID, 0);
                    asignaAtributo('txtDesOG', datosRow.DESCRIPCION, 0);
                    buscarParametros('listGrillaObjE')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaObjE':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1154" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdOG'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'DESCRIPCION'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID', index: 'ID', width: 20 },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: 80 },
                ],
                height: 100,
                width: 1000,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdOE', datosRow.ID, 1);
                    asignaAtributo('txtDesOE', datosRow.DESCRIPCION, 0);
                    buscarParametros('listGrillaPT')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPT':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1158" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdOE'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'ID OBJETIVO ESPECIFICO', 'DESCRIPCION'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID', index: 'ID', width: 20 },
                    { name: 'ID_OE', index: 'ID_OE', hidden: true },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: 80 },
                ],
                height: 100,
                width: 1000,
                loadComplete: function (data) {
                    var tota = jQuery("#drag" + ventanaActual.num).find("#" + arg).getDataIDs().length;
                    if (tota > 0) {
                        ocultar("btnCrearPT")
                    }
                    else {
                        mostrar("btnCrearPT")
                    }
                },
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdPT', datosRow.ID, 1);
                    asignaAtributo('txtDesPT', datosRow.DESCRIPCION, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listEspecialidadProfesional':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=947" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_ESPECIALIDAD', 'ID_PERSONAL', 'NOMBRE', 'ID_PROFESION', 'PROFESION'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ID_PERSONAL', index: 'ID_PERSONAL', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'ID_PROFESION', index: 'ID_PROFESION', hidden: true },
                    { name: 'PROFESION', index: 'PROFESION', width: anchoP(ancho, 20) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdGrupo', datosRow.ID_PERSONAL, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listIncapacidad':

            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=781" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Tot', 'Id', 'Id_evolucion', 'id_paciente', 'Numero dias', 'Fecha Inicio', 'Motivo', 'Diagnostico', 'Tipo'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'Id', index: 'ID', hidden: true },
                    { name: 'Id_evolucion', index: 'ID_EVOLUCION', hidden: true },
                    { name: 'id_paciente', index: 'ID_PACIENTE', hidden: true },
                    { name: 'Numero_dias', index: 'NO_DIAS', width: anchoP(ancho, 10) },
                    { name: 'Fecha_Desde', index: 'FECHA_DESDE', width: anchoP(ancho, 10) },
                    // { name: 'Fecha_Hasta', index: 'FECHA_HASTA', width: anchoP(ancho, 10) },
                    { name: 'Motivo', index: 'MOTIVO', width: anchoP(ancho, 10) },
                    { name: 'Diagnostico', index: 'DIAGNOSTICO', width: anchoP(ancho, 10) },
                    // { name: 'Observacion', index: 'OBSERVACION', width: anchoP(ancho, 10) },
                    { name: 'Tipo', index: 'Tipo', width: anchoP(ancho, 10) }
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdIncap', datosRow.Id, 0);
                    asignaAtributo('lblNumerodias', datosRow.Numero_dias, 0);
                    asignaAtributo('lblMotivo', datosRow.Motivo, 0);
                    asignaAtributo('lblFechaI', datosRow.Fecha_Desde, 0);
                    // asignaAtributo('lblFechaF', datosRow.Fecha_Hasta, 0);

                    mostrar('divVentanitaIncapacidad');

                    asignaAtributo('txtDiasIncapacidad', datosRow.Numero_dias, 0);
                    asignaAtributo('txtFechaIncaInicio', datosRow.Fecha_Desde, 0);
                    // asignaAtributo('txtFechaIncaFin', datosRow.Fecha_Hasta, 0);
                    asignaAtributo('txtMotivo', datosRow.Motivo, 0);
                    asignaAtributo('lblIdEvolucionIncapacidad', datosRow.Id_evolucion, 0);
                    // asignaAtributo('txtObservaciones', datosRow.Observacion, 0);
                    cargarDiagnosticoRelacionadoValorReal('cmbIdDxRelacionadoIncapacidad', datosRow.Id_evolucion);
                    actualizarDiagnostico('')



                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaEducacionPac':

            ancho = ($('#drag' + ventanaActual.num).find("#tabsPrincipalConductaYTratamiento").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=933" + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdDocumento'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'EVOLUCION', 'PACIENTE', 'OBSERVACION'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'EVOLUCION', index: 'ID_EVOLUCION', width: anchoP(ancho, 10) },
                    { name: 'PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 70) },
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtObservacionesEducacion', datosRow.OBSERVACION, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        // eliminar educacion
        // editar educacion
        case 'listEspecialidad':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=896" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'VIGENTE', 'ID_SERVICIO', 'SERVICIO'
                ],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: anchoP(ancho, 20) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'SERVICIO', index: 'SERVICIO', width: anchoP(ancho, 8) },
                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID_ESPECIALIDAD, 1)
                    asignaAtributo('txtNombre', datosRow.ESPECIALIDAD, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributoCombo('cmbServicio', datosRow.ID_SERVICIO, datosRow.SERVICIO)
                    buscarParametros('listSedeEspecialidad')
                    setTimeout(() => {
                        buscarParametros('listEspecialidadProfesional')
                    }, 300);


                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listSedeEspecialidad':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=902" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'COD_HABILITACION', 'ID_SEDE', 'SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', hidden: true },
                    { name: 'COD_HABILITACION', index: 'COD_HABILITACION', width: anchoP(ancho, 15) },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: anchoP(ancho, 20) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdSede', datosRow.ID_SEDE + "-" + datosRow.SEDE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaGrupopciones':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=944" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_MENU', 'DESCRIPCION', 'OPCION', 'ID_GRUPO'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 5) },
                    { name: 'ID_MENU', index: 'ID_MENU', width: anchoP(ancho, 10) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 30) },
                    { name: 'OPCION', index: 'OPCION', width: anchoP(ancho, 30) },
                    { name: 'ID_GRUPO', index: 'ID_GRUPO', hidden: true },


                ],
                height: 350,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdGrupo', datosRow.ID_MENU, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listSede':

            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=951" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'NOMBRE', 'VIGENTE', 'DIRECCION', 'TELEFONO', 'NOMBRE_ALT', 'DIRECCION2', 'TELEFONO2', 'CELULAR', 'COD_HABILITACION', 'ID_SECCIONAL', 'SECCIONAL', 'SW_HOME'
                ],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 4) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 65) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },
                    { name: 'DIRECCION', index: 'DIRECCION', width: anchoP(ancho, 25) },
                    { name: 'TELEFONO', index: 'TELEFONO', hidden: true },
                    { name: 'NOMBRE_ALT', index: 'NOMBRE_ALT', hidden: true },
                    { name: 'DIRECCION2', index: 'DIRECCION2', hidden: true },
                    { name: 'TELEFONO2', index: 'TELEFONO2', hidden: true },
                    { name: 'CELULAR', index: 'CELULAR', hidden: true },
                    { name: 'COD_HABILITACION', index: 'COD_HABILITACION', hidden: true },
                    { name: 'ID_SECCIONAL', index: 'ID_SECCIONAL', hidden: true },
                    { name: 'NOMBRE SECCIONAL', index: 'SECCIONAL', width: anchoP(ancho, 6) },
                    { name: 'SW_HOME', index: 'SW_HOME', hidden: true },

                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('txtNombre2', datosRow.NOMBRE_ALT, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributo('txtDireccion', datosRow.DIRECCION, 0)
                    asignaAtributo('txtTelefono', datosRow.TELEFONO, 0)
                    asignaAtributo('txtNobre2', datosRow.NOMBRE_ALT, 0)
                    asignaAtributo('txtDireccion2', datosRow.DIRECCION2, 0)
                    asignaAtributo('txtTelefono2', datosRow.TELEFONO2, 0)
                    asignaAtributo('txtCelular', datosRow.CELULAR, 0)
                    asignaAtributo('txtCodHabilitacion', datosRow.COD_HABILITACION, 0)
                    asignaAtributo('cmbServicio', datosRow.SW_HOME, 0)
                    asignaAtributo('cmbSeccional', datosRow.ID_SECCIONAL, 0)


                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listArea':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=916" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbSede1'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'NOMBRE', 'VIGENTE', 'ID_SERVICIO', 'SERVICIO', 'ID_SEDE', 'SEDE'
                ],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },
                    { name: 'ID_SERVICIO', index: 'ID_SERVICIO', hidden: true },
                    { name: 'SERVICIO', index: 'SERVICIO', width: anchoP(ancho, 20) },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', hidden: true },

                ],
                height: 320,
                width: ancho,

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributo('cmbServicio', datosRow.ID_SERVICIO, 0)
                    asignaAtributo('cmbSede', datosRow.ID_SEDE, 0)

                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listEdadMinMax':

            ancho = $("#divContenedorListEdadMinMax").width();
            height = $("#divContenedorListEdadMinMax").height() - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=6023" + "&parametros=";
            add_valores_a_mandar(traerDatoFilaSeleccionada('listGrillaTipoCita','ID'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: [
                    'contador',
                    'ID',
                    'EDAD_MIN',
                    'EDAD_MAX',
                    'ID_TIPO_CITA'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true  },
                    { name: 'EDAD_MIN', index: 'EDAD_MIN', width: anchoP(ancho, 50) },
                    { name: 'EDAD_MAX', index: 'EDAD_MAX', width: anchoP(ancho, 50) },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', hidden: true },
                ],
                height: height,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    /* asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('txtRecomendaciones', datosRow.RECOMENDACIONES, 0) */
                    
                   
                },
                gridComplete: function () {
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            


            break            
        case 'listGrillaTipoCita':
            ancho = $("#divContenedorTiposCita").width();
            height = $("#divContenedorTiposCita").height() - 20;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=4112" + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadBus'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(IdEmpresa());

            limpiarListadosTotales('listGrillaTipoCitaProcedimientos');

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'RECOMENDACIONES', 'VIGENTE', 'ID_ESPECIALIDAD',
                    'ESPECIALIDAD', 'TIENE COSTO', 'NUMERO PROCEDIMIENTOS', 'ID_PROGRAMA', 'ID_CURSO_VIDA', 'MODALIDADES',
                    'GENERO', 'AIEPI', 'MESES_PERIODICIDAD','PRIM_VEZ','RELAC_CONTROL'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 3) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'RECOMENDACIONES', index: 'RECOMENDACIONES', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: anchoP(ancho, 15) },
                    { name: 'TIENE_COSTO', index: 'TIENE_COSTO', hidden: true },
                    { name: 'NUMERO_PROCEDIMIENTOS', index: 'NUMERO_PROCEDIMIENTOS', hidden: true },
                    { name: 'ID_PROGRAMA', index: 'ID_PROGRAMA', hidden: true },
                    { name: 'ID_CURSO_VIDA', index: 'ID_CURSO_VIDA', hidden: true },
                    { name: 'IDS_MODALIDADES', index: 'IDS_MODALIDADES', hidden: true },
                    { name: 'GENERO', index: 'GENERO', hidden: true },
                    { name: 'AIEPI', index: 'AIEPI', hidden: true },
                    { name: 'MESES_PERIODICIDAD', index: 'MESES_PERIODICIDAD', hidden: true },
                    { name: 'PRIM_VEZ', index: 'PRIM_VEZ', hidden: true },
                    { name: 'RELAC_CONTROL', index: 'RELAC_CONTROL', hidden: true },
                ],
                height: height,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    limpiarSelect2('cmbTipoCitaRelacionado')     
                    asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('txtRecomendaciones', datosRow.RECOMENDACIONES, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    asignaAtributo('cmbIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0)
                    asignaAtributo('cmbCosto', datosRow.TIENE_COSTO, 0)
                    asignaAtributo('cmbProgramaTipoCita', datosRow.ID_PROGRAMA, 0)

                    let periodicidad = parseInt(datosRow.MESES_PERIODICIDAD)

                   /*  document.getElementById('txtIdCheckAniosPerodicidad').value = ''
                    document.getElementById('checkIdCheckAniosPerodicidad').checked = false
                    document.getElementById('txtIdCheckMesesPerodicidad').value = ''
                    document.getElementById('checkIdCheckMesesPerodicidad').checked = false
                    document.getElementById('checkIdCheckNoAplicaPerodicidad').checked = false */
                    asignaAtributo('cmbPrimeraVezTipoCita',datosRow.PRIM_VEZ,0)
                    asignaAtributo('cmbTipoCitaRelacionado',datosRow.RELAC_CONTROL,0)
                    $('#cmbTipoCitaRelacionado').val(datosRow.RELAC_CONTROL).trigger('change'); 

                    
                    let trTipoCitaRelacionado = $("#trTipoCitaRelacionado")
                    
                    if (periodicidad >= 12 && periodicidad % 12 === 0) {
                        document.getElementById('txtIdCheckAniosPerodicidad').value = periodicidad / 12
                        document.getElementById('checkIdCheckAniosPerodicidad').checked = true
                        desHabilitar('txtIdCheckMesesPerodicidad',1)
                        desHabilitar('txtIdCheckAniosPerodicidad',0)
                    }else if (periodicidad === 0 || !periodicidad ) {
                        document.getElementById('checkIdCheckNoAplicaPerodicidad').checked = true                        
                    }
                     else {
                        document.getElementById('txtIdCheckMesesPerodicidad').value = periodicidad
                        document.getElementById('checkIdCheckMesesPerodicidad').checked = true
                        desHabilitar('txtIdCheckAniosPerodicidad',1)
                        desHabilitar('txtIdCheckMesesPerodicidad',0)
                    }

                    if (datosRow.ID_PROGRAMA == '4') {
                        document.getElementById("trCursosDeVida").style.display = "";
                        document.getElementById("trGenero").style.display = "";

                        asignaAtributo('cmbCursosDeVidaTipoCita', datosRow.ID_CURSO_VIDA, 0)
                        asignaAtributo('cmbGeneroTipoCita', datosRow.GENERO, 0)

                        if (datosRow.PRIM_VEZ === 'N') {
                            trTipoCitaRelacionado.show();
                        }
                    } else {
                        document.getElementById("trCursosDeVida").style.display = "none";
                        document.getElementById("trGenero").style.display = "none";
                        trTipoCitaRelacionado.hide();       

                    }
                    if (datosRow.ID_PROGRAMA == '1') {
                        document.getElementById("trAIEPI").style.display = "";
                        asignaAtributo('cmbAIEPITipoCita', datosRow.AIEPI, 0)
                    } else {
                        document.getElementById("trAIEPI").style.display = "none";
                    }
                    let idsModalidad = datosRow.IDS_MODALIDADES.split(',');
                    let checkList = document.querySelectorAll(".radioModalidad");
                    for (let i = 0; i < checkList.length; i++) {
                        if (idsModalidad.includes(checkList[i].value)) {
                            checkList[i].checked = true;
                        } else {
                            checkList[i].checked = false;
                        }
                    }
                    limpiaAtributo('lblIdFormulario');
                    limpiaAtributo('lblNombreFormulario');
                    limpiaAtributo('cmbDocumentoTipificacion');
                    $('#drag' + ventanaActual.num).find('#' + 'listGrillaFolioProcedimientos').jqGrid("clearGridData");
                    $('#drag' + ventanaActual.num).find('#' + 'listGrillaProfesiones').jqGrid("clearGridData");

                    buscarParametros('listGrillaTiposFormularioCita');

                    //setTimeout(() => {
                    //    setTimeout(cargarComboGRALCondicion1('cmbPadre', '', 'cmbIdModalidad', 3572, datosRow.ID), 400);
                    //}, 100);

                    cargarComboGRALCondicion1('cmbPadre', '', 'cmbIdModalidad', 3572, datosRow.ID)
                    setTimeout(() => cargarComboGRALCondicion1('cmbPadre', '', 'cmbIdModalidadCita', 3572, datosRow.ID), 900);
         

                    buscarParametros('listFinalidad')
                    setTimeout(() => {
                        buscarParametros('listCausaExterna')
                    }, 300);

                    buscarParametros('listEdadMinMax')
                    buscarParametros("listGrillaCitaProcedimientos");

     
                },
                gridComplete: function () {
                    buscarFacturacion("listaFacturasPendientes")
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaTiposFormularioCita':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2666&parametros=";

            let idRowSelected = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
            let datosRowSelected = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowSelected);

            add_valores_a_mandar(datosRowSelected['ID'].trim());

            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .jqGrid({
                    url: valores_a_mandar,
                    datatype: "xml",
                    mtype: "GET",
                    colNames: ["#", "ID", "NOMBRE", "ID_TIPIFICACION", "ARCHIVO TIPIFICACION", "PENDIENTE FACTURA", "ALTA CONSULTA", "ID TIPO CITA"],
                    colModel: [
                        { name: "#", index: "#", hidden: true },
                        { name: "ID", index: "ID", width: 1 },
                        { name: "NOMBRE", index: "NOMBRE", width: 10 },
                        { name: "ID_TIPIFICACION", index: "ID_TIPIFICACION", hidden: true },
                        { name: "ARCHIVO TIPIFICAICON", index: "ARCHIVO TIPIFICACION", width: 10, },
                        { name: "sw_pendiente_factura", index: "sw_pendiente_factura", width: 3, editable: true, editOptions: { value: "true:false" }, formatter: validarCheckBox, formatoptions: { disabled: false }, loadonce: true },
                        { name: "sw_alta_consulta", index: "sw_alta_consulta", width: 3, editable: true, edittype: 'checkbox', editOptions: { value: "true:false" }, formatter: validarCheckBox, formatoptions: { disabled: false }, loadonce: true, hidden: true },
                        { name: "ID_TIPO_CITA", index: "ID_TIPO_CITA", hidden: true },
                    ],
                    height: 200,
                    autowidth: true,
                    onSelectRow: function (rowid) {




                        var datosRow = jQuery("#drag" + ventanaActual.num)
                            .find("#" + arg)
                            .getRowData(rowid);


                        console.log('dfdfdfd00: ', ventanaActual.num, 'arg:', arg)
                        limpiaAtributo('cmbIdEspecialidad', 0)
                        limpiaAtributo('cmbTipoCita', 0)
                        limpiaAtributo('txtIdProcedimiento', 0)

                        asignaAtributo("lblIdFormulario", datosRow.ID, 0);
                        asignaAtributo("cmbFormulario", datosRow.ID, 0);
                        asignaAtributo("cmbDocumentoTipificacion", datosRow.ID_TIPIFICACION, 0)
                        buscarParametros("listGrillaFolioProcedimientos");
                        setTimeout(() => {
                            setTimeout(buscarParametros("listGrillaProfesiones"), 400);
                        }, 100);

                        
                    },
                });
            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .setGridParam({ url: valores_a_mandar })
                .trigger("reloadGrid");

            break;

        case 'listGrillaFolioProcedimientos':
            let idRowCita = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
            let idTipoCita = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCita)['ID'];

            let idRowFormulario = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').jqGrid('getGridParam', 'selrow');
            let idTipoFormulario = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').getRowData(idRowFormulario)['ID'];

            valores_a_mandar = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
            valores_a_mandar = valores_a_mandar + "?idQuery=815&parametros=";

            add_valores_a_mandar(idTipoFormulario.trim());
            add_valores_a_mandar(idTipoCita.trim());

            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .jqGrid({
                    url: valores_a_mandar,
                    datatype: "xml",
                    mtype: "GET",
                    colNames: ["#", "ID_TIPO_FORMULARIO", "ID_TIPO_ADMISION",
                        "ID", "CUPS", "NOMBRE", "VIGENTE", "ID_VIGENTE", "ID_MODALIDAD", "MODALIDAD"],
                    colModel: [
                        { name: "#", index: "#", hidden: true },
                        { name: "ID_TIPO_FORMULARIO", index: "ID_TIPO_FORMULARIO", hidden: true },
                        { name: "ID_TIPO_ADMISION", index: "ID_TIPO_ADMISION", hidden: true },
                        { name: "ID_PROCEDIMIENTO", index: "ID", width: 2 },
                        { name: "CUPS", index: "CUPS", width: 2 },
                        { name: "NOMBRE", index: "NOMBRE", width: 10 },
                        { name: "VIGENTE", index: "VIGENTE", hidden: true },
                        { name: "ID_VIGENTE", index: "ID_VIGENTE", hidden: true },
                        { name: "ID_MODALIDAD", index: "ID_MODALIDAD", hidden: true },
                        { name: "NOM_MODALIDAD", index: "NOM_MODALIDAD", width: 2 },
                    ],
                    height: 150,
                    autowidth: true,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery("#drag" + ventanaActual.num)
                            .find("#" + arg)
                            .getRowData(rowid);

                        asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.CUPS + ' ' + datosRow.NOMBRE, 0)
                        asignaAtributo('cmbVigenteFolioProcedimiento', datosRow.ID_VIGENTE, 0);
                        asignaAtributo('cmbIdModalidad', datosRow.ID_MODALIDAD, 0);
                    },

                });

            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .setGridParam({ url: valores_a_mandar })
                .trigger("reloadGrid");
            break;

        case 'listGrillaCitaProcedimientos':
                let idRowCita1 = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
                let idTipoCita1 = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCita1)['ID'];
    
                valores_a_mandar = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
                valores_a_mandar = valores_a_mandar + "?idQuery=9037&parametros=";
    
                add_valores_a_mandar(idTipoCita1.trim());
    
                $("#drag" + ventanaActual.num)
                    .find("#" + arg)
                    .jqGrid({
                        url: valores_a_mandar,
                        datatype: "xml",
                        mtype: "GET",
                        colNames: ["#",  "ID_TIPO_CITA",
                            "ID", "CUPS", "NOMBRE","ID_MODALIDAD", "MODALIDAD"],
                        colModel: [
                            { name: "#", index: "#", hidden: true },
                          
                            { name: "ID_TIPO_CITA", index: "ID_TIPO_CITA", hidden: true },
                            { name: "ID_PROCEDIMIENTO", index: "ID", width: 2 },
                            { name: "CUPS", index: "CUPS", width: 2 },
                            { name: "NOMBRE", index: "NOMBRE", width: 10 },
                            { name: "ID_MODALIDAD", index: "ID_MODALIDAD", hidden: true },
                            { name: "NOM_MODALIDAD", index: "NOM_MODALIDAD", width: 2 },
                        ],
                        height: 150,
                        autowidth: true,
                        onSelectRow: function (rowid) {
                            var datosRow = jQuery("#drag" + ventanaActual.num)
                                .find("#" + arg)
                                .getRowData(rowid);
    
                            asignaAtributo('txtIdProcedimientoCita', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.CUPS + ' ' + datosRow.NOMBRE, 0)
                            asignaAtributo('cmbIdModalidadCita', datosRow.ID_MODALIDAD, 0);
                        },
    
                    });
    
                $("#drag" + ventanaActual.num)
                    .find("#" + arg)
                    .setGridParam({ url: valores_a_mandar })
                    .trigger("reloadGrid");
                break;


        case 'listGrillaProfesiones':
            valores_a_mandar = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
            valores_a_mandar = valores_a_mandar + "?idQuery=1035&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdFormulario"));

            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .jqGrid({
                    url: valores_a_mandar,
                    datatype: "xml",
                    mtype: "GET",
                    colNames: ["#", "ID", "NOMBRE"],
                    colModel: [
                        { name: "#", index: "#", hidden: true },
                        { name: "ID", index: "ID", width: 1, hidden: true },
                        { name: "NOMBRE", index: "NOMBRE", width: 10 },
                    ],
                    height: 150,
                    autowidth: true,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery("#drag" + ventanaActual.num)
                            .find("#" + arg)
                            .getRowData(rowid);
                        asignaAtributo("txtProfesion", datosRow.ID, 0);
                    },
                });
            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .setGridParam({ url: valores_a_mandar })
                .trigger("reloadGrid");
            break;
        case 'listGrillaExcepcion':

            ancho = 1000;

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=715" + "&parametros=";

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'PROCEDIMIENTO ID', 'NOMBRE', 'ESTADO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'ESTADO', index: 'ESTADO', hidden: true },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdProcedimientoe', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.NOMBRE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaTipoCitaProcedimientos':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=720" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID TIPO CITA', 'ID PROCEDIMIENTO', 'NOMBRE PROCEDIMIENTO', 'VIGENTE', 'CUPS',
                    'CUM', 'POS', 'NIVEL'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', width: anchoP(ancho, 10) },
                    { name: 'ID_PROCEDIMIENTO', index: 'ID_PROCEDIMIENTO', width: anchoP(ancho, 15) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 70) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 10) },
                    { name: 'CUPS', index: 'CUPS', width: anchoP(ancho, 10) },
                    { name: 'CUM', index: 'CUM', hidden: true },
                    { name: 'POS', index: 'POS', width: anchoP(ancho, 10) },
                    { name: 'NIVEL', index: 'NIVEL', width: anchoP(ancho, 10) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID_TIPO_CITA, 1)
                    asignaAtributo('txtIdProcedimiento', datosRow.ID_PROCEDIMIENTO + '-' + datosRow.NOMBRE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        /* Grilla para el listado de habitaciones*/
        case 'listGrillaHabitacion':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=926&parametros=";
            add_valores_a_mandar(valorAtributo('cmbSede1'));
            add_valores_a_mandar(valorAtributo('cmbSede1'));
            add_valores_a_mandar(valorAtributo('cmbIdArea1'));
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'ID_SEDE', 'SEDE', 'HABITACION', 'ID_TIPO', 'TIPO DE HABITACION', 'ID_AREA', 'AREA', 'ID_CENTRO_COSTO', 'CENTRO COSTO', 'VIGENTE'
                ],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: anchoP(ancho, 30) },
                    { name: 'HABITACION', index: 'HABITACION', width: anchoP(ancho, 30) },
                    { name: 'ID_TIPO', index: 'TIPO', hidden: true },
                    { name: 'TIPO DE HABITACION', index: 'TIPO DE HABITACION', width: anchoP(ancho, 20) },
                    { name: 'ID_AREA', index: 'ID_AREA', hidden: true },
                    { name: 'AREA', index: 'AREA', width: anchoP(ancho, 20) },
                    { name: 'ID_CENTRO_COSTO', index: 'ID_CENTRO_COSTO', hidden: true },
                    { name: 'CENTRO COSTO', index: 'CENTRO COSTO', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },
                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.HABITACION, 0)
                    asignaAtributoCombo('cmbIdArea', datosRow.ID_AREA, datosRow.AREA)
                    asignaAtributoCombo('cmbSedeCrear', datosRow.ID_SEDE, datosRow.SEDE)
                    asignaAtributo('cmbIdTipo', datosRow.ID_TIPO, 0)
                    asignaAtributo('cmbCosto', datosRow.ID_CENTRO_COSTO, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    // asignaAtributo('cmbIdAdmision', datosRow.ID_ADMISION, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaCama':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=927" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'CAMA', 'ID_ESTADO', 'ESTADO', 'ID_TIPO', 'TIPO DE CAMA', 'ID_HABITACION', 'HABITACION', 'VIGENTE'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'CAMA', index: 'CAMA', width: anchoP(ancho, 30) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTAD0', hidden: true },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    { name: 'TIPO DE CAMA', index: 'TIPO DE CAMA', width: anchoP(ancho, 20) },
                    { name: 'ID_HABITACION', index: 'ID_HABITACION', hidden: true },
                    { name: 'HABITACION', index: 'HABITACION', width: anchoP(ancho, 30) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },

                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 1)
                    asignaAtributo('txtNombre', datosRow.CAMA, 0)
                    asignaAtributo('cmbIdEstado', datosRow.ID_ESTADO, 1)
                    asignaAtributo('cmbIdTipo', datosRow.ID_TIPO, 0)
                    asignaAtributo('cmbHabitacion', datosRow.ID_HABITACION, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaProfesion':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=954" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID', 'PROFESION', 'DESCRIPCION', 'ID_TIPO', 'TIPO', 'PREFIJO', 'SW_FOLIO', 'ID_LAZOS', 'VIGENTE'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', width: anchoP(ancho, 5) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'PROFESION', index: 'PROFESION', width: anchoP(ancho, 30) },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', hidden: true },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 10) },
                    { name: 'PREFIJO', index: 'PREFIJO', hidden: true },
                    { name: 'SW_FOLIO', index: 'SW_FOLIO', hidden: true },
                    { name: 'ID_LAZOS', index: 'ID_LAZOS', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },

                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.PROFESION, 0)
                    asignaAtributo('txtNombre2', datosRow.DESCRIPCION, 0)
                    asignaAtributo('cmbIdEstado', datosRow.ID_TIPO, 0)
                    asignaAtributo('txtPrefijo', datosRow.PREFIJO, 0)
                    asignaAtributo('cmbFolio', datosRow.SW_FOLIO, 0)
                    asignaAtributo('txtCod', datosRow.ID_LAZOS, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaTipoCitaProcedimientosPrestador':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=856" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID EMPRESA', 'RAZON SOCIAL', 'CODIGO HABILITACION', 'DIRECCION', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_EMPRESA', index: 'ID_EMPRESA', width: anchoP(ancho, 10) },
                    { name: 'RAZON_SOCIAL', index: 'RAZON_SOCIAL', width: anchoP(ancho, 50) },
                    { name: 'CODIGO_HABILITACION', index: 'CODIGO_HABILITACION', width: anchoP(ancho, 20) },
                    { name: 'DIRECCION', index: 'DIRECCION', width: anchoP(ancho, 20) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 20) }

                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtPrestadorTecnologia', datosRow.ID_EMPRESA + '-' + datosRow.RAZON_SOCIAL, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillaPiePagina':

            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=731" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'TIPO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 8) },
                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbTipo', datosRow.TIPO, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listGrillaCentroCostos':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=740" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(IdEmpresa());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'PADRE', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },
                    { name: 'PADRE', index: 'PADRE', hidden: true },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },


                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbPadre', datosRow.PADRE, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaProcesoHD':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2283" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 8) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },

                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },


                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaEncuestaHD':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=3058" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 8) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },

                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },


                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaModuloHD':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2286" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusNombre'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 8) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },

                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },


                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listGrillafolios':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=991" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusTipo'));
            add_valores_a_mandar(valorAtributo('txtBusNombre'));



            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 60) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true },


                ],
                height: 320,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 1);
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 1);
                    buscarUsuario('listGrillaAntecedentesFolio')




                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaPlanAtencionRuta':

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=174&parametros=";
            add_valores_a_mandar(valorAtributo('txtNombrePlanAtencion'));
            add_valores_a_mandar(valorAtributo('cmbVigente'));
            add_valores_a_mandar(IdEmpresa());

            var editOptions = {
                editData: {
                    accion: "editarPlanAtencion",
                    idQuery: "194",
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                closeAfterEdit: true,
                recreateForm: true,
                onclickSubmit: function (params) {
                    var id = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getRowData', id);
                    return { id: ret.id }
                }
            };
            var addOptions = {
                editData: {
                    accion: "crearPlanAtencion",
                    idQuery: "188"
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                recreateForm: true,
                closeAfterAdd: true
            };
            var delOptions = {
                delData: {
                    accion: "eliminarPlanAtencion",
                    idQuery: "195"
                }, afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#listGrillaPlanAtencionRuta").jqGrid('getRowData', id);
                    return { idDel: ret.id }
                }
            };

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 10), hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 40), editrules: { required: true }, editable: true, edittype: "text" },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 10), editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: { 'S': 'SI', 'N': 'NO' } } }
                ],
                height: 374,
                autowidth: true,
                caption: "Rutas",
                pager: "#pagerG",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                /*onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdPlanAtencion', datosRow.ID, 0)
                    asignaAtributo('txtNombrePlanAtencion', datosRow.NOMBRE, 0)
                    asignaAtributo('cmbVigentePlanAtencion', datosRow.VIGENTE, 0)
                },*/
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid').navGrid('#pagerG', {
                del: true, add: true, edit: true, view: true, refresh: true, search: false,
                edittext: "Editar", addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar", deltext: "Eliminar"
            }, editOptions, addOptions, delOptions);
            break;
    }
}

function obtenerPeriodicidad() {


    checkNoAplicaPeriodicidad = document.getElementById('checkIdCheckNoAplicaPerodicidad').checked
    checkMesesPeriodicidad = document.getElementById('checkIdCheckMesesPerodicidad').checked
    checkAniosPeriodicidad = document.getElementById('checkIdCheckAniosPerodicidad').checked
    mesesPeriodicidad = document.getElementById('txtIdCheckMesesPerodicidad').value
    aniosPeriodicidad = document.getElementById('txtIdCheckAniosPerodicidad').value
    mensaje = ''
    valor = ''
    respuesta = { mensaje }

    if (checkNoAplicaPeriodicidad) {
        return '0'
    }
    if (checkMesesPeriodicidad && mesesPeriodicidad !== '') {
        return mesesPeriodicidad
    } else if (checkMesesPeriodicidad && mesesPeriodicidad === '') {
        respuesta.mensaje = 'falta el campo meses'
        return respuesta
    }
    if (checkAniosPeriodicidad && aniosPeriodicidad !== '') {
        return aniosPeriodicidad * 12
    } else if (checkAniosPeriodicidad && aniosPeriodicidad === '') {
        respuesta.mensaje = 'falta el campo años'
        return respuesta
    }

    respuesta.mensaje = 'falta seleccionar periodicidad'
    return respuesta
}



function modificarCRUDParametros(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg))
        switch (arg) {

            case 'eliminarCausaExterna':
                if (valorAtributo('txtId') == '') { alert('Seleccione un tipo de cita'); return false; }
                if (valorAtributo('cmbCausaExternaTipoCita') == '') { alert('Seleccione Causa Externa'); return false; }
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2675&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('cmbCausaExternaTipoCita'));
                ajaxModificar();
                break;

            case 'eliminarFinalidad':
                if (valorAtributo('txtId') == '') { alert('Seleccione un tipo de cita'); return false; }
                if (valorAtributo('cmbFinalidadTipoCita') == '') { alert('Seleccione Finalidad'); return false; }
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2674&parametros=";
                add_valores_a_mandar(valorAtributo('cmbFinalidadTipoCita'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'agregarCausaExterna':
                if (valorAtributo('txtId') == '') { alert('Seleccione un tipo de cita'); return false; }
                if (valorAtributo('cmbCausaExternaTipoCita') == '') { alert('Seleccione Causa Externa'); return false; }
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2673&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('cmbCausaExternaTipoCita'));
                ajaxModificar();
                break;

            case 'agregarFinalidad':
                if (valorAtributo('txtId') == '') { alert('Seleccione un tipo de cita'); return false; }
                if (valorAtributo('cmbFinalidadTipoCita') == '') { alert('Seleccione Finalidad'); return false; }
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2672&parametros=";
                add_valores_a_mandar(valorAtributo('cmbFinalidadTipoCita'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'crearOG':
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1151&parametros=";
                add_valores_a_mandar(valorAtributo('txtDesOG'));
                ajaxModificar();
                break;

            case 'modificarOG':
                if (!confirm("ESTA SEGURO DE MODIFICAR ESTE OBJETIVO GENERAL?")) return
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1152&parametros=";
                add_valores_a_mandar(valorAtributo('txtDesOG'));
                add_valores_a_mandar(valorAtributo('txtIdOG'));
                ajaxModificar();
                break;

            case 'eliminarOG':
                if (!confirm("ESTA SEGURO DE ELIMINAR ESTE OBJETIVO GENERAL?")) return
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1153&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdOG'));
                add_valores_a_mandar(valorAtributo('txtIdOG'));
                add_valores_a_mandar(valorAtributo('txtIdOG'));
                ajaxModificar();
                break;

            case 'crearOE':
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1155&parametros=";
                add_valores_a_mandar(valorAtributo('txtDesOE'));
                add_valores_a_mandar(valorAtributo('txtIdOG'));
                ajaxModificar();
                break;

            case 'modificarOE':
                if (!confirm("ESTA SEGURO DE MODIFICAR ESTE OBJETIVO ESPECIFICO?")) return
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1156&parametros=";
                add_valores_a_mandar(valorAtributo('txtDesOE'));
                add_valores_a_mandar(valorAtributo('txtIdOE'));
                ajaxModificar();
                break;

            case 'eliminarOE':
                if (!confirm("ESTA SEGURO DE ELEMINAR ESTE OBJETIVO ESPECIFICO?")) return
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1157&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdOE'));
                add_valores_a_mandar(valorAtributo('txtIdOE'));
                ajaxModificar();
                break;

            case 'crearPT':
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1159&parametros=";
                add_valores_a_mandar(valorAtributo('txtDesPT'));
                add_valores_a_mandar(valorAtributo('txtIdOE'));
                ajaxModificar();
                break;

            case 'modificarPT':
                if (!confirm("ESTA SEGURO DE MODIFICAR ESTE PLAN DE TRATAMIENTO?")) return
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1160&parametros=";
                add_valores_a_mandar(valorAtributo('txtDesPT'));
                add_valores_a_mandar(valorAtributo('txtIdPT'));
                ajaxModificar();
                break;

            case 'eliminarPT':
                if (!confirm("ESTA SEGURO DE ELIMINAR ESTE PLAN DE TRATAMIENTO?")) return
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1161&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdPT'));
                ajaxModificar();
                break;

            case 'crearFPT':
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1162&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFolioA'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDxA'));
                add_valores_a_mandar(valorAtributo('txtIdOGA'));
                ajaxModificar();
                break;

            case 'eliminarFPT':
                if (!confirm("ESTA SEGURO DE ELIMINAR DE REALIZAR ESTA ACCION?")) return
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1163&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdFolioA'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdDxA'));
                add_valores_a_mandar(valorAtributo('txtIdOGA'));
                ajaxModificar();
                break;

            case 'eliminarProfesionFormulario':
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');
                profesion = $("#listGrillaProfesiones").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1033&parametros=";
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);
                add_valores_a_mandar($("#listGrillaProfesiones").getRowData(profesion).ID);
                ajaxModificar();
                break;

            case 'eliminarProfesionFormularioTipoCitas':
                let idRowFormularioEp = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').jqGrid('getGridParam', 'selrow');
                let idTipoFormularioEp = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').getRowData(idRowFormularioEp)['ID'];

                let idRowProfesionEp = $('#drag' + ventanaActual.num).find('#' + 'listGrillaProfesiones').jqGrid('getGridParam', 'selrow');
                let idProfesionEp = $('#drag' + ventanaActual.num).find('#' + 'listGrillaProfesiones').getRowData(idRowProfesionEp)['ID'];

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1033&parametros=";
                add_valores_a_mandar(idTipoFormularioEp);
                add_valores_a_mandar(idProfesionEp);
                ajaxModificar();
                break;

            case 'adicionarProfesionFormulario':
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1034&parametros=";
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);
                add_valores_a_mandar(valorAtributo("cmbProfesion"));
                ajaxModificar();
                break;

            case 'adicionarProfesionFormularioTipoCitas':
                let idRowFormularioPa = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').jqGrid('getGridParam', 'selrow');
                let idTipoFormularioPa = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').getRowData(idRowFormularioPa)['ID'];

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1034&parametros=";
                add_valores_a_mandar(idTipoFormularioPa);
                add_valores_a_mandar(valorAtributo("cmbProfesion"));
                add_valores_a_mandar(LoginSesion());
                ajaxModificar();
                break;

            case 'eliminarFolioProcedimientoTipoCitas':
                let idRowCitaEl = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
                let idTipoCitaEl = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCitaEl)['ID'];

                let idRowProcedimientoEl = $('#drag' + ventanaActual.num).find('#' + 'listGrillaFolioProcedimientos').jqGrid('getGridParam', 'selrow');
                let currProcedimientoEl = $('#drag' + ventanaActual.num).find('#' + 'listGrillaFolioProcedimientos').getRowData(idRowProcedimientoEl);

                let idRowFormularioEl = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').jqGrid('getGridParam', 'selrow');
                let idTipoFormularioEl = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').getRowData(idRowFormularioEl)['ID'];

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=817&parametros=";
                add_valores_a_mandar(idTipoFormularioEl.trim());
                add_valores_a_mandar(idTipoCitaEl.trim());
                add_valores_a_mandar(currProcedimientoEl['ID_PROCEDIMIENTO'].trim());
                add_valores_a_mandar(currProcedimientoEl['ID_MODALIDAD'].trim());
                ajaxModificar();
                break;


            case 'eliminarProcedimientoTipoCitas':
                let idRowCitaDelete = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
                let idTipoCitaDelete = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCitaDelete)['ID'];
    
                let idRowProcedimientoDelete = $('#drag' + ventanaActual.num).find('#' + 'listGrillaCitaProcedimientos').jqGrid('getGridParam', 'selrow');
                let currProcedimientoDelete = $('#drag' + ventanaActual.num).find('#' + 'listGrillaCitaProcedimientos').getRowData(idRowProcedimientoDelete);

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=9036&parametros=";
         
                add_valores_a_mandar(idTipoCitaDelete.trim());
                add_valores_a_mandar(currProcedimientoDelete['ID_PROCEDIMIENTO'].trim());
                add_valores_a_mandar(currProcedimientoDelete['ID_MODALIDAD'].trim());
                ajaxModificar();
                break;

            case 'eliminarFolioProcedimiento':
                tipo_cita = $("#listGrillaTiposCita").jqGrid('getGridParam', 'selrow');
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');
                procedimiento = $("#listGrillaFolioProcedimientos").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=817&parametros=";
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);
                add_valores_a_mandar($("#listGrillaFolioProcedimientos").getRowData(procedimiento).ID_PROCEDIMIENTO);
                add_valores_a_mandar($("#listGrillaFolioProcedimientos").getRowData(procedimiento).ID_MODALIDAD);
                ajaxModificar();
                break;

            case 'adicionarFolioProcedimiento':
                tipo_cita = $("#listGrillaTiposCita").jqGrid('getGridParam', 'selrow');
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=816&parametros=";
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
                add_valores_a_mandar(valorAtributo("cmbIdModalidad"));
                //add_valores_a_mandar(valorAtributo('cmbVigenteFolioProcedimiento'))
                ajaxModificar();
                break;

            case 'adicionarFolioProcedimientoTipoCitas':
                let idRowCitaAd = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
                let idTipoCitaAd = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCitaAd)['ID'];

                let idRowFormularioAd = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').jqGrid('getGridParam', 'selrow');
                let idTipoFormularioAd = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').getRowData(idRowFormularioAd)['ID'];

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=816&parametros=";
                add_valores_a_mandar(idTipoFormularioAd.trim());
                add_valores_a_mandar(idTipoCitaAd.trim());
                add_valores_a_mandar(idTipoCitaAd.trim());
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'));
                add_valores_a_mandar(valorAtributo("cmbIdModalidad"));
                add_valores_a_mandar(LoginSesion());
                ajaxModificar();
                break;

            case 'adicionarProcedimientoTipoCitas':
                let idRowCitaAdd = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
                let idTipoCitaAdd = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCitaAdd)['ID'];

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=9035&parametros=";
                
                add_valores_a_mandar(idTipoCitaAdd.trim());
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoCita'));
                add_valores_a_mandar(valorAtributo("cmbIdModalidadCita"));
                add_valores_a_mandar(LoginSesion());
                ajaxModificar();
                break;

            case 'adicionarTipoCitaFormularioTipoCitas':
                let idRowCitaAf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
                let idTipoCitaAf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCitaAf)['ID'];

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=961&parametros=";
                add_valores_a_mandar(valorAtributo("cmbFormulario"));
                add_valores_a_mandar(idTipoCitaAf.trim());
                //add_valores_a_mandar(LoginSesion());      
                ajaxModificar();
                break;

            case 'adicionarTipoCitaFormulario':
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=961&parametros=";
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                ajaxModificar();
                break;

            case 'eliminarTipoCitaFormulario':
                tipo_cita = $("#listGrillaTiposCita").jqGrid('getGridParam', 'selrow');
                tipo_formulario = $("#listGrillaTiposFormulario").jqGrid('getGridParam', 'selrow');

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=962&parametros=";
                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);

                add_valores_a_mandar($("#listGrillaTiposFormulario").getRowData(tipo_formulario).ID);
                add_valores_a_mandar($("#listGrillaTiposCita").getRowData(tipo_cita).ID_TIPO);
                ajaxModificar();
                break;

            case 'eliminarTipoCitaFormularioTipoCitas':
                let idRowCitaEf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').jqGrid('getGridParam', 'selrow');
                let idTipoCitaEf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTipoCita').getRowData(idRowCitaEf)['ID'];

                let idRowFormularioEf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').jqGrid('getGridParam', 'selrow');
                let idTipoFormularioEf = $('#drag' + ventanaActual.num).find('#' + 'listGrillaTiposFormularioCita').getRowData(idRowFormularioEf)['ID'];

                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=962&parametros=";
                add_valores_a_mandar(idTipoFormularioEf);
                add_valores_a_mandar(idTipoCitaEf.trim());

                add_valores_a_mandar(idTipoFormularioEf);
                add_valores_a_mandar(idTipoCitaEf.trim());
                ajaxModificar();
                break;

            case "eliminarProfesionalEspecialidad":
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=949&parametros=";

                add_valores_a_mandar(valorAtributo("txtId"));
                add_valores_a_mandar(valorAtributo("lblIdGrupo"));
                ajaxModificar();
                break;

            case "eliminarProfesionalBodega":
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1005&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdGrupo"));
                add_valores_a_mandar(valorAtributo("txtId"));

                ajaxModificar();
                break;

            case 'adicionarParametro':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=846&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbEsperar'));
                add_valores_a_mandar(valorAtributo('txtOrden'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                ajaxModificar();
                break;

            case 'agregarEspProfesional':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1803&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdGrupo'));

                ajaxModificar();
                break;
            case 'agregaUsuarioBodega':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1004&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdGrupo'));
                add_valores_a_mandar(valorAtributo('txtId'));

                ajaxModificar();
                break;
            case 'crearUsuario':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=596&parametros=";
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                    add_valores_a_mandar(valorAtributo('txtPNombre'));
                    add_valores_a_mandar(valorAtributo('txtSNombre'));
                    add_valores_a_mandar(valorAtributo('txtPApellido'));
                    add_valores_a_mandar(valorAtributo('txtSApellido'));
                    add_valores_a_mandar(valorAtributo('txtNomPersonal'));
                    add_valores_a_mandar(valorAtributo('cmbIdTipoProfesion'));
                    add_valores_a_mandar(valorAtributo('txtRegistro'));
                    add_valores_a_mandar(valorAtributo('txtPhone'));
                    add_valores_a_mandar(valorAtributo('txtCorreo'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));


                    /* usuario */
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('txtUSuario'));
                    add_valores_a_mandar(valorAtributo('cmbEstado'));
                    add_valores_a_mandar(valorAtributo('cmbResponsable'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));

                    /* sede */
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    ajaxModificar();
                }
                break;
            case 'modificarUsuario':
                if (verificarCamposGuardar(arg)) {

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=597&parametros=";

                    add_valores_a_mandar(valorAtributo('txtPNombre'));
                    add_valores_a_mandar(valorAtributo('txtSNombre'));
                    add_valores_a_mandar(valorAtributo('txtPApellido'));
                    add_valores_a_mandar(valorAtributo('txtSApellido'));
                    add_valores_a_mandar(valorAtributo('txtNomPersonal'));
                    add_valores_a_mandar(valorAtributo('cmbIdTipoProfesion'));
                    add_valores_a_mandar(valorAtributo('txtRegistro'));
                    add_valores_a_mandar(valorAtributo('txtPhone'));
                    add_valores_a_mandar(valorAtributo('txtCorreo'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));

                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));

                    /* usuario */
                    add_valores_a_mandar(valorAtributo('txtUSuario'));
                    add_valores_a_mandar(valorAtributo('cmbEstado'));
                    add_valores_a_mandar(valorAtributo('cmbResponsable'));
                    add_valores_a_mandar(valorAtributo('cmbTipoId'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    /* sede */
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    add_valores_a_mandar(valorAtributo('cmbSede'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacion'));

                    ajaxModificar();
                }
                break;
            case 'crearTipoCita':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=725&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('txtRecomendaciones'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(IdEmpresa());
                add_valores_a_mandar(valorAtributo('cmbProgramaTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbCursosDeVidaTipoCita'));
                add_valores_a_mandar(LoginSesion());

                if (valorAtributo('cmbProgramaTipoCita') == 4) {
                    add_valores_a_mandar(valorAtributo('cmbGeneroTipoCita'));
                } else {
                    add_valores_a_mandar('A');
                }
                if (valorAtributo('cmbProgramaTipoCita') == 1) {
                    add_valores_a_mandar(valorAtributo('cmbAIEPITipoCita'));
                } else {
                    add_valores_a_mandar('');
                }


               /*  res = obtenerPeriodicidad()
                if (typeof (res) === "object") {
                    alert(res.mensaje)
                    return
                } else {
                    add_valores_a_mandar(res)
                } */
                add_valores_a_mandar(valorAtributo('cmbTipoCitaRelacionado'))
                add_valores_a_mandar(valorAtributo('cmbPrimeraVezTipoCita'))
                add_valores_a_mandar(obtenerValoresCheckList('radioModalidad'));

                ajaxModificar();
                break;
            case 'modificarTipoProcedim':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=724&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('txtRecomendaciones'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(valorAtributo('cmbProgramaTipoCita'));
                add_valores_a_mandar(valorAtributo('cmbCursosDeVidaTipoCita'));
                add_valores_a_mandar(LoginSesion());

                if (valorAtributo('cmbProgramaTipoCita') == 4) {
                    add_valores_a_mandar(valorAtributo('cmbGeneroTipoCita'));
                } else {
                    add_valores_a_mandar('A');
                }
                if (valorAtributo('cmbProgramaTipoCita') == 1) {
                    add_valores_a_mandar(valorAtributo('cmbAIEPITipoCita'));
                } else {
                    add_valores_a_mandar('');
                }

                /* res = obtenerPeriodicidad()
                if (typeof (res) === "object") {
                    alert(res.mensaje)
                    return
                } else {
                    add_valores_a_mandar(res)
                } */
                add_valores_a_mandar(valorAtributo('cmbTipoCitaRelacionado'))
                add_valores_a_mandar(valorAtributo('cmbPrimeraVezTipoCita'))

                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(obtenerValoresCheckList('radioModalidad'));
                ajaxModificar();
                limpiarListadosTotales('listGrillaTipoCitaProcedimientos');
                break;
            case 'eliminarTipoCita':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=957&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));
                    add_valores_a_mandar(valorAtributo('txtId'));
                    ajaxModificar();
                    limpiarListadosTotales('listGrillaTipoCitaProcedimientos');
                }
                break;
            case 'adicionarPrestadorTecnologia':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=857&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorTecnologia'))
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'eliminarPrestadorTecnologia':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=858&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorTecnologia'))
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;

            case 'crearTipoCitaProcedimiento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=726&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'))

                ajaxModificar();
                break;

            case 'eliminarTipoCitaProcedimiento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=727&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimiento'))
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'modificarPiePagina':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=732&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbTipo'));
                add_valores_a_mandar(valorAtributo('lblId'))
                ajaxModificar();
                break;

            case 'adicionarPiePagina':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=733&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbTipo'));
                add_valores_a_mandar(IdEmpresa());
                ajaxModificar();
                break;
            case 'adicionarCentroCosto':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=741&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                //add_valores_a_mandar(valorAtributo('cmbPadre'));
                ajaxModificar();
                break;
            case 'modificarCentroCosto':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=742&parametros=";

                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbPadre'));
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;
            case 'eliminarCentroCosto':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=743&parametros=";

                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;

            case 'adicionarProcesosHD':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2284&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));

                ajaxModificar();
                break;

            case 'modificarProcesosHD':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2285&parametros=";

                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;

            case 'adicionarEncuestaHD':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=3059&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));

                ajaxModificar();
                break;

            case 'modificarEncuestaHD':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=3060&parametros=";

                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(traerDatoFilaSeleccionada('listGrillaEncuestaHD', 'ID'))
                ajaxModificar();
                break;

            case 'adicionarModuloHD':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2288&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));

                ajaxModificar();
                break;
            case 'modificarModuloHD':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2287&parametros=";

                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'))
                ajaxModificar();
                break;

            case 'adicionarProcedimientoE':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=784&parametros=";

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoe'))
                ajaxModificar();
                break;
            case 'eliminarProcedimientoE':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=785&parametros=";

                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoe'))
                ajaxModificar();
                break;

            case 'crearSede':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=953&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('txtNombre2'));
                    add_valores_a_mandar(valorAtributo('txtDireccion'));
                    add_valores_a_mandar(valorAtributo('txtDireccion2'));
                    add_valores_a_mandar(valorAtributo('txtTelefono'))
                    add_valores_a_mandar(valorAtributo('txtTelefono2'))
                    add_valores_a_mandar(valorAtributo('txtCelular'));
                    add_valores_a_mandar(valorAtributo('txtCodHabilitacion'));
                    add_valores_a_mandar(valorAtributo('cmbSeccional'));
                    add_valores_a_mandar(valorAtributo('cmbServicio'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));


                    ajaxModificar();

                }
                break;

            case 'crearProfesion':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=955&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'));
                    add_valores_a_mandar(valorAtributo('txtNombre2'));
                    add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                    add_valores_a_mandar(valorAtributo('txtPrefijo'));
                    add_valores_a_mandar(valorAtributo('cmbFolio'));
                    add_valores_a_mandar(valorAtributo('txtCod'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(LoginSesion());


                    ajaxModificar();

                }
                break;
            case 'modificarProfesion':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=956&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                add_valores_a_mandar(valorAtributo('txtPrefijo'));
                add_valores_a_mandar(valorAtributo('cmbFolio'));
                add_valores_a_mandar(valorAtributo('txtCod'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('txtId'));

                ajaxModificar();
                break;

            case 'crearEspecialidadSede':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=899&parametros=";
                // add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('txtCodigo'));

                ajaxModificar();
                break;


            case 'crearArea':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=919&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('cmbSede'));

                ajaxModificar();
                break;

            case 'modificarSede':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=952&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'));
                add_valores_a_mandar(valorAtributo('txtNombre2'));
                add_valores_a_mandar(valorAtributo('txtDireccion'));
                add_valores_a_mandar(valorAtributo('txtDireccion2'));
                add_valores_a_mandar(valorAtributo('txtTelefono'));
                add_valores_a_mandar(valorAtributo('txtTelefono2'));
                add_valores_a_mandar(valorAtributo('txtCelular'));
                add_valores_a_mandar(valorAtributo('txtCodHabilitacion'));
                add_valores_a_mandar(valorAtributo('cmbSeccional'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'modificarArea':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=920&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('cmbSede'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'crearHabitacion':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=922&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdArea'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                ajaxModificar();
                break;

            case 'modificarHabitacion':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=923&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdArea'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbCosto'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'crearElemento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=928&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbHabitacion'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));

                ajaxModificar();
                break;

            case 'crearPerfil':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=942&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'))
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('txtdefinicion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));

                    ajaxModificar();
                }
                break;

            case 'modificarPerfil':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=943&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('txtdefinicion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('txtId'));

                    ajaxModificar();
                }
                break;

            case 'modificarElemento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=929&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbIdEstado'));
                add_valores_a_mandar(valorAtributo('cmbIdTipo'));
                add_valores_a_mandar(valorAtributo('cmbHabitacion'));
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('txtId'));


                ajaxModificar();
                break;

            case 'inhabilitarElemento':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=921&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));


                    ajaxModificar();
                }
                break;

            case 'asignarSedeEspecialidad':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=900&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdSede'))

                ajaxModificar();
                break;

            case 'asignarSedeBodega':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=994&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdSede'))

                ajaxModificar();
                break;

            case 'eliminarSedeBodega':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=993&parametros=";

                    add_valores_a_mandar(valorAtributo('txtId'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdSede'))

                    ajaxModificar();
                }
                break;

            case 'ubicarHabitacionArea':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=900&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'))
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtidhabitacion'))
                ajaxModificar();
                break;
            case 'modificarEspecialidad':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=901&parametros=";
                add_valores_a_mandar(valorAtributo('txtNombre'))
                add_valores_a_mandar(valorAtributo('cmbVigente'));
                add_valores_a_mandar(valorAtributo('cmbServicio'));
                add_valores_a_mandar(valorAtributo('txtCodigo'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
                break;

            case 'eliminarEspecialidadSede':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=903&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdSede'))
                add_valores_a_mandar(valorAtributo('txtId'));

                ajaxModificar();
                break;
        }
}
function respuestaCRUDParametros(arg, xmlraiz) {

    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {

        switch (arg) {

            case 'eliminarCausaExterna':
                alert('Causa Externa Eliminada');
                buscarParametros('listCausaExterna');
                limpiaAtributo('cmbCausaExternaTipoCita');
                break;

            case 'eliminarFinalidad':
                alert('Finalidad Eliminada');
                buscarParametros('listFinalidad');
                limpiaAtributo('cmbFinalidadTipoCita')
                break;

            case 'agregarCausaExterna':
                alert('Causa Externa Agregada');
                buscarParametros('listCausaExterna');
                limpiaAtributo('cmbCausaExternaTipoCita')
                break;

            case 'agregarFinalidad':
                alert('Finalidad Agregada');
                buscarParametros('listFinalidad');
                limpiaAtributo('cmbFinalidadTipoCita')
                break;

            case 'crearOG':
                alert('Objetivo General Creado')
                limpiaAtributo('txtIdOG')
                limpiaAtributo('txtDesOG')
                limpiaAtributo('txtIdOE')
                limpiaAtributo('txtDesOE')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                buscarParametros('listGrillaObjG')
                buscarParametros('listGrillaObjE')
                buscarParametros('listGrillaPT')
                break;
            case 'modificarOG':
                alert('Objetivo General Modificado')
                limpiaAtributo('txtIdOG')
                limpiaAtributo('txtDesOG')
                limpiaAtributo('txtIdOE')
                limpiaAtributo('txtDesOE')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                buscarParametros('listGrillaObjG')
                buscarParametros('listGrillaObjE')
                buscarParametros('listGrillaPT')
                break;
            case 'eliminarOG':
                alert('Objetivo General Eliminado')
                limpiaAtributo('txtIdOG')
                limpiaAtributo('txtDesOG')
                limpiaAtributo('txtIdOE')
                limpiaAtributo('txtDesOE')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                buscarParametros('listGrillaObjG')
                buscarParametros('listGrillaObjE')
                buscarParametros('listGrillaPT')
                break;

            case 'crearOE':
                alert('Objetivo Especifico Creado')
                buscarParametros('listGrillaObjE')
                buscarParametros('listGrillaPT')
                limpiaAtributo('txtIdOE')
                limpiaAtributo('txtDesOE')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                break;
            case 'modificarOE':
                alert('Objetivo Especifico Modificado')
                buscarParametros('listGrillaObjE')
                buscarParametros('listGrillaPT')
                limpiaAtributo('txtIdOE')
                limpiaAtributo('txtDesOE')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                break;
            case 'eliminarOE':
                alert('Objetivo Especifico Eliminado')
                buscarParametros('listGrillaObjE')
                buscarParametros('listGrillaPT')
                limpiaAtributo('txtIdOE')
                limpiaAtributo('txtDesOE')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                break;

            case 'crearPT':
                alert('Plan de Tratamiento Creado')
                buscarParametros('listGrillaPT')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                break;
            case 'modificarPT':
                alert('Plan de Tratamiento Modificado')
                buscarParametros('listGrillaPT')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                break;
            case 'eliminarPT':
                alert('Plan de Tratamiento Eliminado')
                buscarParametros('listGrillaPT')
                limpiaAtributo('txtIdPT')
                limpiaAtributo('txtDesPT')
                break;

            case 'crearFPT':
                alert('Plan de Tratamiento Asociado')
                buscarParametros('listGrillaFolios')
                limpiaAtributo('txtIdDxA')
                limpiaAtributo('txtIdFolioA')
                limpiaAtributo('txtIdOGA')
                break;

            case 'eliminarFPT':
                alert('Plan de Tratamiento Desasociado')
                buscarParametros('listGrillaFolios')
                limpiaAtributo('txtIdDxA')
                limpiaAtributo('txtIdFolioA')
                limpiaAtributo('txtIdOGA')
                break;

            case 'eliminarProfesionFormulario':
                alert('Elemento eliminado')
                buscarHC('listGrillaProfesiones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'eliminarProfesionFormularioTipoCitas':
                alert('Elemento eliminado')
                $('#drag' + ventanaActual.num).find("#" + "listGrillaProfesiones").trigger("reloadGrid");
                break;

            case 'adicionarProfesionFormulario':
                alert("Elemento adicionado")
                buscarHC('listGrillaProfesiones', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                break;

            case 'adicionarProfesionFormularioTipoCitas':
                alert("Elemento adicionado")
                $('#drag' + ventanaActual.num).find("#" + "listGrillaProfesiones").trigger("reloadGrid");
                break;

            case 'eliminarFolioProcedimiento':
                alert("Elemento eliminado")
                buscarHC('listGrillaFolioProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                limpiaAtributo('txtIdProcedimiento', 0)
                limpiaAtributo('cmbIdModalidad');
                asignaAtributo('cmbVigenteFolioProcedimiento', 'S', 0)
                break;

            case 'eliminarFolioProcedimientoTipoCitas':
                alert("Elemento eliminado")
                $('#drag' + ventanaActual.num).find("#" + "listGrillaFolioProcedimientos").trigger("reloadGrid");
                limpiaAtributo('txtIdProcedimiento', 0)
                limpiaAtributo('cmbIdModalidad');
                break;

            case 'eliminarProcedimientoTipoCitas':
                alert("Elemento eliminado")
                $('#drag' + ventanaActual.num).find("#" + "listGrillaCitaProcedimientos").trigger("reloadGrid");
                limpiaAtributo('txtIdProcedimientoCita', 0)
                limpiaAtributo('cmbIdModalidadCita');
                break;
                

            case 'adicionarFolioProcedimiento':
                alert("Elemento adicionado")
                buscarHC('listGrillaFolioProcedimientos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
                limpiaAtributo('txtIdProcedimiento', 0)
                //limpiaAtributo('adicionarFolioProcedimiento', 0);
                asignaAtributo('cmbVigenteFolioProcedimiento', 'S', 0)
                break;

            case 'adicionarFolioProcedimientoTipoCitas':
                alert("Elemento adicionado");
                $('#drag' + ventanaActual.num).find("#" + "listGrillaFolioProcedimientos").trigger("reloadGrid");
                limpiaAtributo('txtIdProcedimiento', 0);
                break;

            case 'adicionarProcedimientoTipoCitas':
                alert("Elemento adicionado");
                $('#drag' + ventanaActual.num).find("#" + "listGrillaCitaProcedimientos").trigger("reloadGrid");
                limpiaAtributo('txtIdProcedimientoCita', 0);
                break;

                
            case 'adicionarTipoCitaFormulario':
                alert('Elemento adicionado');
                buscarHC('listGrillaTiposCita');
                limpiaAtributo('cmbIdEspecialidad', 0);
                limpiaAtributo('cmbTipoCita', 0);
                break;

            case 'adicionarTipoCitaFormularioTipoCitas':
                alert('Elemento adicionado');
                $('#drag' + ventanaActual.num).find("#" + "listGrillaTiposFormularioCita").trigger("reloadGrid");
                limpiaAtributo('cmbFormulario', 0);
                limpiaAtributo('cmbDocumentoTipificacion', 0);
                break;

            case 'eliminarTipoCitaFormularioTipoCitas':
                alert('Elemento eliminado');
                $('#drag' + ventanaActual.num).find("#" + "listGrillaTiposFormularioCita").trigger("reloadGrid");
                limpiaAtributo('cmbFormulario', 0);
                limpiaAtributo('cmbDocumentoTipificacion', 0);
                break;

            case 'eliminarTipoCitaFormulario':
                alert('Elemento eliminado');
                buscarHC('listGrillaTiposCita');
                setTimeout(() => {
                    buscarHC('listGrillaFolioProcedimientos');
                }, 300);
                limpiaAtributo('cmbIdEspecialidad', 0);
                limpiaAtributo('cmbTipoCita', 0);
                break;

            case 'agregarEspProfesional':
                buscarParametros('listEspecialidadProfesional')
                alert('Profesional Agregado Exitosamente')
                break;


            case "eliminarProfesionalEspecialidad":
                buscarParametros('listEspecialidadProfesional');
                alert("Eliminado exitosamente");
                limpiaAtributo('cmbIdGrupo', 0);
                break;

            case 'agregaUsuarioBodega':
                buscarSuministrosBodegas('listUsuarioBodega')
                alert('Profesional Agregado Exitosamente')
                limpiaAtributo('txtIdGrupo', 0);
                break;

            case "eliminarProfesionalBodega":
                buscarSuministrosBodegas('listUsuarioBodega');
                alert("Eliminado exitosamente");
                limpiaAtributo('txtIdGrupo', 0);
                break;


            case 'crearPacientePrograma':
                buscarProgramas('listGrillaParametrosPaciente');
                buscarProgramas('listGrillaProgramaPaciente');
                break;

            case 'adicionarParametro':
                buscarProgramas('listGrillaParametros');
                break;

            case 'crearUsuario':
                buscarUsuario('listGrillaUsuarios')
                buscarUsuario('sede')
                alert('Creado exitosamente');
                limpiarDivEditarJuan('limpiarCrearUsuario');
                break;

            case 'modificarUsuario':
                buscarUsuario('listGrillaUsuarios')
                buscarUsuario('sede')
                alert('Modificado exitosamente');
                limpiarDivEditarJuan('limpiarCrearUsuario');
                break;

            case 'adicionarPrestadorTecnologia':
                limpiaAtributo('txtPrestadorTecnologia', 0)
                buscarParametros('listGrillaTipoCitaProcedimientosPrestador')
                break;
            case 'eliminarPrestadorTecnologia':
                limpiaAtributo('txtPrestadorTecnologia', 0)
                buscarParametros('listGrillaTipoCitaProcedimientosPrestador')
                break;


            case 'crearTipoCita':
                buscarParametros('listGrillaTipoCita')
                
                swAlert('success','Tipo cita procedimiento registrado','','')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('txtRecomendaciones', 0)
                limpiaAtributo('cmbProgramaTipoCita', 0)
                limpiaAtributo('cmbCursosDeVidaTipoCita', 0)
                limpiaAtributo('cmbGeneroTipoCita', 0)
                limpiaAtributo('cmbCursosDeVidaTipoCita', 0)
                $('#checkIdCheckNoAplicaPerodicidad').attr('checked', false);
                $('#checkIdCheckMesesPerodicidad').attr('checked', false);
                $('#checkIdCheckAniosPerodicidad').attr('checked', false);
                limpiaAtributo('txtIdCheckAniosPerodicidad', 0)
                limpiaAtributo('txtIdCheckMesesPerodicidad', 0)
                limpiarSelect2('cmbTipoCitaRelacionado')


                break;
            case 'crearEspecialidadSede':
                buscarParametros('listEspecialidad')
                alert('Especialidad  registrada')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                break;
            case 'crearProfesion':
                buscarParametros('listGrillaProfesion')
                alert('Profesion  registrada')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('txtNombre2', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('txtPrefijo', 0)
                limpiaAtributo('cmbFolio', 0)
                limpiaAtributo('txtPCod', 0)
                limpiaAtributo('cmbVigente', 0)

                break;


            case 'crearArea':
                buscarParametros('listArea')
                alert('Area  registrada')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                limpiaAtributo('cmbSede', 0)
                break;

            case 'crearSede':
                buscarParametros('listSede')
                alert('Sede  registrada')
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('txtNombre2', 0)
                limpiaAtributo('txtDireccion', 0)
                limpiaAtributo('txtTelefono', 0)
                limpiaAtributo('txtTelefono2', 0)
                limpiaAtributo('txtCelular', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('txtCodHabilitacion', 0)
                limpiaAtributo('cmbSeccional', 0)
                limpiaAtributo('cmbServicio', 0)
                break;

            case 'crearHabitacion':
                buscarParametros('listGrillaHabitacion')
                alert('Habitacion  Creada ')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbIdArea', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('cmbIdAdmision', 0)
                break;

            case 'modificarHabitacion':
                buscarParametros('listGrillaHabitacion')
                alert('Habitacion  Modificada ')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbIdArea', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('cmbIdAdmision', 0)
                break;

            case 'crearElemento':
                buscarParametros('listGrillaCama')
                alert('Elemento  Creado con exito ')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbHabitacion', 0)
                limpiaAtributo('cmbVigente', 0)

                break;

            case 'crearPerfil':
                buscarUsuario('listGrillaPerfiles');
                alert('Registro exitiso');
                limpiaAtributo('txtId', 0);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('txtdefinicion', 0);
                limpiaAtributo('cmbVigente', 0);

                break;

            case 'modificarPerfil':
                buscarUsuario('listGrillaPerfiles');

                alert('Registro modificado con exito');
                limpiaAtributo('txtId', 1);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('txtdefinicion', 0);
                limpiaAtributo('cmbVigente', 0);

                break;


            case 'modificarElemento':
                alert('Elemento  Modificado')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbHabitacion', 0)
                limpiaAtributo('cmbVigente', 0)
                setTimeout(() => {
                    buscarParametros('listGrillaCama')
                }, 100);
                break;


            case 'inhabilitarElemento':
                buscarParametros('listGrillaCama')
                alert(' Elemento  No Habilitado')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('cmbIdTipo', 0)
                limpiaAtributo('cmbHabitacion', 0)
                limpiaAtributo('cmbVigente', 0)
                break;


            case 'asignarSedeEspecialidad':
                buscarParametros('listSedeEspecialidad')
                alert('Especialidad  Asignada a Sede')
                limpiaAtributo('txtIdSede', 0)
                break;
            case 'asignarSedeBodega':
                buscarSuministrosBodegas('listBodegaSede');
                alert('La Sede se asigno con Exito');
                limpiaAtributo('txtIdSede', 0);
                break;


            case 'ubicarHabitacionArea':
                buscarParametros('listSedeEspecialidad')
                alert('Se realizo la asignacion de manera exitosa')
                limpiaAtributo('txtidhabitacion', 0)
                break;



            case 'asignarPlanesaSedes':
                buscarParametros('listSedePlan')
                alert('Plan  Asignado a Sede')
                limpiaAtributo('txtIdSede', 0)
                break;
            case 'modificarTipoProcedim':
                buscarParametros('listGrillaTipoCita')

                swAlert('success','TIPO DE CITA MODIFICADO CON EXITO!!','','')


                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('txtRecomendaciones', 0)
                limpiaAtributo('cmbProgramaTipoCita', 0)
                limpiaAtributo('cmbCursosDeVidaTipoCita', 0)
                limpiaAtributo('cmbGeneroTipoCita', 0)
                limpiaAtributo('cmbCursosDeVidaTipoCita', 0)
                limpiaAtributo('txtIdCheckMesesPerodicidad', 0)
                limpiaAtributo('txtIdCheckAniosPerodicidad', 0)
                limpiarSelect2('cmbTipoCitaRelacionado')
                
                

                break;
            case 'eliminarTipoCita':
                buscarParametros('listGrillaTipoCita')
                //alert('')
                swAlert('success','TIPO DE CITA ELIMINADO CON EXITO!!','','')

                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbCosto', 0)
                limpiaAtributo('txtRecomendaciones', 0)
                break;

            case 'modificarEspecialidad':
                buscarParametros('listEspecialidad')
                alert('Especialidad modificada')
                limpiaAtributo('txtId', 1)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                break;

            case 'modificarArea':
                buscarParametros('listArea')
                alert('Area modificada')
                limpiaAtributo('txtId', 1)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('cmbServicio', 0)
                limpiaAtributo('cmbSede', 0)
                break;
            case 'modificarSede':
                buscarParametros('listSede')
                alert('Sede modificada')
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('txtNombre2', 0)
                limpiaAtributo('txtDireccion', 0)
                limpiaAtributo('txtTelefono', 0)
                limpiaAtributo('txtTelefono2', 0)
                limpiaAtributo('txtCelular', 0)
                limpiaAtributo('cmbVigente', 0)
                limpiaAtributo('txtCodHabilitacion', 0)
                limpiaAtributo('cmbSeccional', 0)
                limpiaAtributo('cmbServicio', 0)
                break;

            case 'modificarProfesion':
                buscarParametros('listGrillaProfesion')
                alert('Profesion  Modificada')
                limpiaAtributo('txtId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('txtNombre2', 0)
                limpiaAtributo('cmbIdEstado', 0)
                limpiaAtributo('txtPrefijo', 0)
                limpiaAtributo('cmbFolio', 0)
                limpiaAtributo('txtCod', 0)
                limpiaAtributo('cmbVigente', 0)
                break;

            case 'eliminarEspecialidadSede':
                alert('Se ha Eliminado correctamente')
                buscarParametros('listSedeEspecialidad');
                break;
            case 'eliminarSedeBodega':
                alert('Se ha Eliminado correctamente')
                buscarSuministrosBodegas('listBodegaSede')
                break;

            case 'eliminarhabitacionArea':
                alert('Se ha Eliminado correctamente')
                buscarParametros('listAreaHabitacion');
                break;
            case 'crearTipoCitaProcedimiento':
                buscarParametros('listGrillaTipoCitaProcedimientos')
                alert('Tipo cita procedimiento agregado')
                limpiaAtributo('txtIdProcedimiento', 0)

                break;
            case 'eliminarTipoCitaProcedimiento':
                buscarParametros('listGrillaTipoCitaProcedimientos')
                alert('Tipo cita procedimiento eliminado')
                limpiaAtributo('txtIdProcedimiento', 0)

                break;
            case 'modificarPiePagina':
                buscarParametros('listGrillaPiePagina');
                alert('Pie de pagina modificado')
                limpiaAtributo('lblId', 0)
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbTipo', 0)

                break;

            case 'adicionarPiePagina':
                buscarParametros('listGrillaPiePagina');
                limpiaAtributo('txtNombre', 0)
                limpiaAtributo('cmbTipo', 0)
                alert('Pie de pagina creado')

                break;

            case 'adicionarCentroCosto':
                buscarParametros('listGrillaCentroCostos');
                break;

            case 'modificarCentroCosto':
                alert('Se ha modificado correctamente')
                buscarParametros('listGrillaCentroCostos');
                break;

            case 'eliminarCentroCosto':
                alert('Se ha eliminado correctamente')
                buscarParametros('listGrillaCentroCostos');
                break;
            case 'adicionarProcesosHD':
                alert('Creado con exito')
                buscarParametros('listGrillaProcesoHD');
                break;

            case 'modificarProcesosHD':
                alert('Se ha modificado correctamente')
                buscarParametros('listGrillaProcesoHD');
                break;

            case 'adicionarEncuestaHD':
                alert('Creado con exito')
                buscarParametros('listGrillaEncuestaHD');
                break;

            case 'modificarEncuestaHD':
                alert('Creado con exito')
                buscarParametros('listGrillaEncuestaHD');
                break;

            case 'adicionarModuloHD':
                alert('Creado con exito')
                buscarParametros('listGrillaModuloHD');
                break;

            case 'modificarModuloHD':
                alert('Se ha modificado correctamente')
                buscarParametros('listGrillaModuloHD');
                break;


            case 'adicionarProcedimientoE':
                alert('Se ha adicionado correctamente')
                buscarParametros('listGrillaExcepcion');
                break;
            case 'eliminarProcedimientoE':
                alert('Se ha Eliminado correctamente')
                buscarParametros('listGrillaExcepcion');
                break;
        }
    }
}
