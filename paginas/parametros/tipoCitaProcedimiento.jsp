<%@ page session="true" contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.*,java.text.*,java.sql.*" %>
<%@ page import="Sgh.Utilidades.*" %>
<%@ page import="Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import="Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% beanAdmin.setCn(beanSession.getCn()); ArrayList resultaux=new ArrayList(); %>
<style>
    .container-periodicidad {
        display: grid;
        grid-template-columns: repeat(1, 1fr);
        font-size: 10px;
        grid-row-gap: 10px;
        align-items: center;
        margin-left: 10px;
    }

    .sub-container-periodicidad {
        /* display: flex;
        align-items: center;
        gap: 10px; */
        text-align: left;
    }

    .sub-container-periodicidad label {
        width: 50px;
        text-align: left;
    }

    .text-input-periodicidad {
        width: 80px;
    }
</style>
<table width="1250px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="TIPO DE CITA" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <div style="overflow:auto; width:100%" id="divContenido">
              <table width="100%">
                <tr class="estiloImputListaEspera">
                  <td width="50%" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" align="left" style="margin: 5px 0 5px 0">
                      <tr class="titulosListaEspera" align="center">
                        <td width="60%">ESPECIALIDAD</td>
                        <td width="20%">VIGENTE</td>
                        <td width="10%"></td>
                      </tr>
                      <tr class="estiloImputListaEspera">
                        <td>
                          <select size="1" id="cmbIdEspecialidadBus" style="width:90%"  style="width:50%"
                            >
                            <option value=""></option>
                            <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(125); ComboVO cmb01; for
                              (int k=0; k < resultaux.size(); k++) { cmb01=(ComboVO) resultaux.get(k); %>
                              <option value="<%= cmb01.getId()%>" title="<%= cmb01.getTitle()%>">
                                <%= cmb01.getDescripcion()%>
                              </option>
                              <%}%>
                          </select>
                        </td>
                        <td>
                          <select size="1" id="cmbVigenteBus" style="width:85%" >
                            <option value="S">SI</option>
                            <option value="N">NO</option>
                          </select>
                        </td>
                        <td>
                          <input type="button" class="small button blue" value="BUSCAR"
                            onclick="buscarParametros('listGrillaTipoCita')" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3">
                          <div id="divContenedorTiposCita" style="width: 100%; height: 300px;">
                            <table id="listGrillaTipoCita"></table>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <table style="width: 100%;">
                      <tr class="titulosCentrados">
                        <td colspan="2">
                          Periodicidad de consultas
                        </td>
                      </tr>
                      <tr>
                        <td class="titulosListaEspera" width="50%">
                          Periodicidad
                        </td>
                        <td  class="titulosListaEspera">
                          -
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="container-periodicidad">
                            <div class="sub-container-periodicidad">
                              <input type="radio" onclick="limpiarYdeshabilitar('noAplica');" name="periodicidad"
                                id="checkIdCheckNoAplicaPerodicidad" value="second_checkbox" />
                              <label for="checkIdCheckNoAplicaPerodicidad">No
                                Aplica</label>
                            </div>
                            <div class="sub-container-periodicidad">
                              <input onclick="limpiarYdeshabilitar('meses');" type="radio" name="periodicidad"
                                value="second_checkbox" id="checkIdCheckMesesPerodicidad" />
                              <label for="checkIdCheckMesesPerodicidad">Meses RANGO (Min - Max)</label>                              
                            </div>
                            <div class="sub-container-periodicidad">
                              <input onkeypress="javascript:return NumCheck(event, this.id, '0.0', 'null');"
                                onblur="validarNumero1(this.id,this.value,3,0)" class="text-input-periodicidad"
                                id="txtIdCheckMesesPerodicidadMin">
                              <input onkeypress="javascript:return NumCheck(event, this.id, '0.0', 'null');"
                                onblur="validarNumero1(this.id,this.value,3,0)" class="text-input-periodicidad"
                                id="txtIdCheckMesesPerodicidadMax">
                            </div>
                            <div class="sub-container-periodicidad">
                              <button id="agregarEdadMaxMin" disabled onclick="modificarCRUD('agregarRangoEdad')" class="small button blue">Agregar</button>
                            </div>
                            <!-- <div class="sub-container-periodicidad">
                              <input onclick="limpiarYdeshabilitar('anios');" type="radio" name="periodicidad"
                                value="second_checkbox" id="checkIdCheckAniosPerodicidad" />
                              <label for="checkIdCheckAniosPerodicidad">Años</label>
                              <input onkeypress="javascript:return NumCheck(event, this.id, '0.0', 'null');"
                                onblur="validarNumero1(this.id,this.value,3,0)" type="number"
                                class="text-input-periodicidad" id="txtIdCheckAniosPerodicidad">
                            </div> -->
                          </div>
                        </td>
                        <td style="vertical-align: baseline;">
                          <div id="divContenedorListEdadMinMax">
                            <table id="listEdadMinMax"></table>
                          </div>
                          <button id="eliminarEdadMaxMin" onclick="modificarCRUD('eliminarRangoEdad')" class="small button blue">Eliminar</button>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td valign="top">
                    <table width="100%" height="100%">
                      <tr class="titulosCentrados">
                        <td>PARAMETROS DEL TIPO DE CITA</td>
                      </tr>
                      <tr class="titulosListaEspera">
                        <td>
                          <table width="100%" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                              <td>
                                <table width="100%" height="100%">
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">ID TIPO
                                      DE CITA</td>
                                    <td width="70%"> <input readonly type="text" id="txtId" style="width:8%"
                                        maxlength="2" onkeyup="javascript: this.value = this.value.toUpperCase();" />
                                    </td>
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">NOMBRE
                                      TIPO DE CITA</td>
                                    <td width="70%"><input type="text" id="txtNombre"
                                        onkeyup="javascript: this.value = this.value.toUpperCase();"
                                        style="width:95%" />
                                    </td>
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">
                                      RECOMENDACIONES</td>
                                    <td width="70%">
                                      <textarea id="txtRecomendaciones"  style="width:95%"
                                        maxlength="4000" rows="5" type="text">
                                                                            </textarea>
                                    </td>
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">
                                      ESPECIALIDAD</td>
                                    <td>
                                      <select size="1" id="cmbIdEspecialidad" style="width:50%" 
                                        >
                                        <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(125); ComboVO
                                          cmb32; for (int k=0; k < resultaux.size(); k++) { cmb32=(ComboVO)
                                          resultaux.get(k); %>
                                          <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>">
                                            <%= cmb32.getDescripcion()%>
                                          </option>
                                          <%}%>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">TIENE
                                      COSTO</td>
                                    <td width="70%"><select size="1" id="cmbCosto" style="width:15%" >
                                        <option value="S">SI
                                        </option>
                                        <option value="N">NO
                                        </option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">PROGRAMA
                                    </td>
                                    <td width="70%">
                                      <select size="1" id="cmbProgramaTipoCita" style="width:35%" 
                                        onchange=" apareceCursoDeVida();">
                                        <option value="">
                                        </option>
                                        <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(3818);
                                          ComboVO cmbProgramaTipoCita; for (int p=0; p < resultaux.size(); p++) {
                                          cmbProgramaTipoCita=(ComboVO) resultaux.get(p); %>
                                          <option value="<%= cmbProgramaTipoCita.getId()%>"
                                            title="<%= cmbProgramaTipoCita.getTitle()%>">
                                            <%= cmbProgramaTipoCita.getDescripcion()%>
                                          </option>
                                          <%}%>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr style="display: none;" id="trCursosDeVida" class="estiloImputIzq2">
                                    <td width="30%">CURSO DE
                                      VIDA</td>
                                    <td width="70%"><select size="1" id="cmbCursosDeVidaTipoCita" style="width:35%"
                                        >
                                        <option value="0">
                                          TODOS
                                        </option>
                                        <option value="1">
                                          PRIMERA
                                          INFANCIA
                                        </option>
                                        <option value="2">
                                          INFANCIA
                                        </option>
                                        <option value="3">
                                          ADOLESCENCIA
                                        </option>
                                        <option value="4">
                                          JUVENTUD
                                        </option>
                                        <option value="5">
                                          ADULTEZ
                                        </option>
                                        <option value="6">
                                          VEJEZ
                                        </option>
                                        <option value="7">
                                          CERVIX MENOR
                                          A 30
                                        </option>
                                        <option value="8">
                                          CERVIX MAYOR
                                          A 30
                                        </option>
                                        <option value="9">
                                          PROSTATA
                                        </option>
                                        <option value="10">
                                          MAMA
                                        </option>


                                      </select></td>
                                  </tr>
                                  <tr style="display: none;" id="trGenero" class="estiloImputIzq2">
                                    <td width="30%">GENERO
                                    </td>
                                    <td width="70%">
                                      <select size="1" id="cmbGeneroTipoCita" style="width:35%"
                                        >
                                        <option value="A">
                                          AMBOS
                                        </option>
                                        <option value="F">
                                          FEMENINO
                                        </option>
                                        <option value="M">
                                          MASCULINO
                                        </option>
                                      </select>
                                    </td>
                                  </tr>

                                  <tr id="trPrimeraVez" class="estiloImputIzq2">
                                    <td width="30%">Primera Vez
                                    </td>
                                    <td td width="70%">
                                      <select size="1" onchange="mostrarCmbTipoCitaRelacionado()" id="cmbPrimeraVezTipoCita" style="width:15%"
                                        >
                                        <!-- <option value="">
                                          ----
                                        </option> -->
                                        <option value="S">
                                          SI
                                        </option>
                                        <option value="N">
                                          NO
                                        </option>
                                      </select>
                                    </td>
                                  </tr>


                                  <tr style="display: none;" id="trTipoCitaRelacionado" class="estiloImputIzq2">
                                    <td width="30%">Tipo Cita Relacionado
                                    </td>
                                    <td td width="70%" style="max-width: 100px;">
                                      <select size="1" id="cmbTipoCitaRelacionado" style="width:95%; max-width: 100px;"
                                        >
                                        <option value="">
                                          [SELECCIONE]
                                        </option>       
                                        <% resultaux.clear();
                                        resultaux=(ArrayList) beanAdmin.combo.cargar(5002);
                                        ComboVO cmb1;
                                        for(int k=0; k < resultaux.size(); k++) {
                                          cmb1=(ComboVO) resultaux.get(k); %>
                                        <option value="<%= cmb1.getId()%>" title="<%= cmb1.getTitle()%>">
                                          <%= cmb1.getDescripcion()%>
                                        </option>
                                        <%}%>                                 
                                      </select>
                                    </td>
                                  </tr>

                                  <tr style="display: none;" id="trAIEPI" class="estiloImputIzq2">
                                    <td width="30%">AIEPI
                                    </td>
                                    <td width="70%"><select size="1" id="cmbAIEPITipoCita" style="width:35%"
                                        >
                                        <option value="">
                                        </option>
                                        <option value="1">
                                          AIEPI 0-2
                                          meses
                                        </option>
                                        <option value="2">
                                          AIEPI de 2-6
                                          meses
                                        </option>
                                        <option value="3">
                                          AIEPI 3
                                          meses-5 años
                                        </option>
                                      </select></td>
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">VIGENTE
                                    </td>
                                    <td width="70%">
                                      <select size="1" id="cmbVigente" style="width:15%" >
                                        <option value="S">SI
                                        </option>
                                        <option value="N">NO
                                        </option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%">
                                      MODALIDADES</td>
                                    <td>
                                      <table style="width:60%;">
                                        <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(3529);
                                          ComboVO rdMod; for (int k=0; k < resultaux.size(); k++) { rdMod=(ComboVO)
                                          resultaux.get(k); %>
                                          <td><input class="radioModalidad" type="checkbox"
                                              id="radioMod<%=rdMod.getId()%>" id value="<%=rdMod.getId()%>" />
                                          </td>
                                          <td><label for="radioMod<%=rdMod.getId()%>">
                                              <%=rdMod.getDescripcion()%>
                                            </label>
                                          </td>
                                          <%}%>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr class="estiloImputListaEspera">
                        <td>
                          <table width="100%" style="/*cursor:pointer;*/ padding: 5px 0 5px" align="center">
                            <tr>
                              <td width="99%">
                                <input name="btn_limpiar_new" type="button" title="F76" class="small button blue"
                                  value="LIMPIAR"
                                  onclick="limpiarListadosTotales('listGrillaTipoCitaProcedimientos')" />
                                <input name="btn_CREAR" title="F77" type="button" class="small button blue"
                                  value="CREAR"
                                  onclick="modificarCRUDParametros('crearTipoCita', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                <input name="btn_MODIFICAR" title="F79" type="button" class="small button blue"
                                  value="MODIFICAR"
                                  onclick="modificarCRUDParametros('modificarTipoProcedim', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                <input name="btn_MODIFICAR" title="F79" type="button" class="small button blue"
                                  value="ELIMINAR"
                                  onclick="modificarCRUDParametros('eliminarTipoCita', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr class="titulosCentrados">
                        <td>
                          Finalidad Consulta y Causa Externa
                        </td>
                      </tr>
                      <tr class="estiloImputListaEspera">
                        <td>
                          <table width="100%" border="0">
                            <tr>
                              <td class="titulosListaEspera">
                                Finalidad
                              </td>
                              <td class="titulosListaEspera">
                                Causa Externa
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <select style="width:50%" id="cmbFinalidadTipoCita">
                                  <option value=""></option>
                                  <% resultaux.clear(); resultaux=(ArrayList)beanAdmin.combo.cargar(609); ComboVO cmbLa;
                                    for(int k=0;k<resultaux.size();k++){ cmbLa=(ComboVO)resultaux.get(k); %>
                                    <option value="<%= cmbLa.getId()%>" title="<%= cmbLa.getTitle()%>">
                                      <%= cmbLa.getDescripcion()%>
                                    </option>
                                    <%}%>
                                </select>
                              </td>
                              <td>
                                <select style="width:50%" id="cmbCausaExternaTipoCita">
                                  <option value=""></option>
                                  <% resultaux.clear(); resultaux=(ArrayList)beanAdmin.combo.cargar(162); for(int
                                    k=0;k<resultaux.size();k++){ cmbLa=(ComboVO)resultaux.get(k); %>
                                    <option value="<%= cmbLa.getId()%>" title="<%= cmbLa.getTitle()%>">
                                      <%= cmbLa.getDescripcion()%>
                                    </option>
                                    <%}%>
                                </select>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <input name="btn_CREAR" title="F77" type="button" class="small button blue"
                                  value="AGREGAR"
                                  onclick="modificarCRUDParametros('agregarFinalidad', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                <input name="btn_MODIFICAR" title="F79" type="button" class="small button blue"
                                  value="ELIMINAR"
                                  onclick="modificarCRUDParametros('eliminarFinalidad', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                              </td>
                              <td>
                                <input name="btn_CREAR" title="F77" type="button" class="small button blue"
                                  value="AGREGAR"
                                  onclick="modificarCRUDParametros('agregarCausaExterna', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                <input name="btn_MODIFICAR" title="F79" type="button" class="small button blue"
                                  value="ELIMINAR"
                                  onclick="modificarCRUDParametros('eliminarCausaExterna', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                              </td>
                            </tr>
                            <tr class="titulos">
                              <td style="width:50%">
                                <table id="listFinalidad">
                                </table>
                              </td>
                              <td style="width:50%">
                                <table id="listCausaExterna">
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="100%" style="margin-top: 5px" align="center">
                <tr class="titulosCentrados">
                  <td>TIPOS FOLIOS DE HISTORIA CLINICA</td>
                </tr>
                <tr class="titulosListaEspera">
                  <td>
                    <table width="50%">
                      <tr class="estiloImputIzq2">
                        <td>ID FORMULARIO: </td>
                        <td align="LEFT">
                          <label id=lblIdFormulario></label>
                        </td>
                      </tr>
                      <tr class="estiloImputIzq2">
                        <td width="25%">NOMBRE FORMULARIO: </td>
                        <td align="LEFT">
                          <select id="cmbFormulario" style="width:60%;">
                            <option value="">[ Ninguno ]</option>
                            <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(3573); ComboVO cmbForm;
                              for (int k=0; k < resultaux.size(); k++) { cmbForm=(ComboVO) resultaux.get(k); %>
                              <option value="<%= cmbForm.getId()%>">
                                <%= cmbForm.getId()%> - <%= cmbForm.getDescripcion()%>
                              </option>
                              <%}%>
                          </select>
                        </td>
                      </tr>
                      <tr class="estiloImputIzq2">
                        <td>ARCHIVO DE TIPIFICACION: </td>
                        <td align="LEFT">
                          <select id="cmbDocumentoTipificacion" style="width: 60%;">
                            <option value="">[ Ninguno ]</option>
                            <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(193); ComboVO cmbTB; for
                              (int k=0; k < resultaux.size(); k++) { cmbTB=(ComboVO) resultaux.get(k); %>
                              <option value="<%= cmbTB.getId()%>">
                                <%= cmbTB.getDescripcion()%>
                              </option>
                              <%}%>
                          </select>
                        </td>
                      </tr>
                      <tr class="estiloImputListaEspera">
                        <td colspan="2" align="CENTER">
                          <input type="button" class="small button blue" value="ADICIONAR" style="margin: 5px 0 5px;"
                            onclick="modificarCRUDParametros('adicionarTipoCitaFormularioTipoCitas');" />
                          <input type="button" class="small button blue" value="MODIFICAR" style="margin: 5px 0 5px;"
                            onclick="modificarCRUD('modificarDocumentoTipificacionFormulario');" />
                          <input type="button" class="small button blue" value="ELIMINAR" style="margin: 5px 0 5px;"
                            onclick="modificarCRUDParametros('eliminarTipoCitaFormularioTipoCitas');" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table id="listGrillaTiposFormularioCita" class="scroll" width="100%"></table>
                  </td>
                </tr>
              </table>
              <table width="100%">
                <tr class="titulosListaEspera">
                  <td valign="top" width="50%">
                    <table width="100%">
                      <tr class="titulosCentrados">
                        <td>
                          PROCEDIMIENTO ASOCIADOS AL FOLIO
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td width="10%"><label for="txtIdProcedimiento">PROCEDIMIENTO:
                                </label></td>
                              <td width="40%">
                                <input type="text" id="txtIdProcedimiento"
                                  onkeyup="javascript: this.value = this.value.toUpperCase();"
                                  oninput="llenarElementosAutoCompletarKey(this.id, 204, this.value)"
                                  style="width:90%" />
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="10%">
                                <label for="cmbIdModalidad">MODALIDAD:
                                </label>
                              </td>
                              <td width="40%">
                                <select id="cmbIdModalidad">
                                  <option value="">[ NINGUNO ]
                                  </option>
                                </select>
                              </td>
                            </tr>

                            <tr class="estiloImputDer">
                              <td colspan="4" align="CENTER">
                                <input type="button" class="small button blue" value="ADICIONAR"
                                  style="margin: 5px 0 5px;"
                                  onclick="modificarCRUDParametros('adicionarFolioProcedimientoTipoCitas')" />
                                <input type="button" class="small button blue" value="ELIMINAR"
                                  style="margin: 5px 0 5px;"
                                  onclick="modificarCRUDParametros('eliminarFolioProcedimientoTipoCitas')" />
                              </td>
                            </tr>
                            <tr class="titulos">
                              <td colspan="4">
                                <table id="listGrillaFolioProcedimientos">
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="50%">
                    <table width="100%">
                      <tr class="titulosCentrados">
                        <td>PROFESIONES ASOCIADAS AL FOLIO</td>
                      </tr>
                      <tr>
                        <td>
                          <table width="100%">
                            <tr>
                              <td>
                                <table width="100%">
                                  <tr class="estiloImputIzq2">
                                    <td width="10%"><label for="cmbProfesion">PROFESION:
                                      </label></td>
                                    <td width="60%">
                                      <select id="cmbProfesion" style="width: 30%;">
                                        <option value="">[
                                          NINGUNO ]
                                        </option>
                                        <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(40); ComboVO
                                          cmbP; for (int k=0; k < resultaux.size(); k++) { cmbP=(ComboVO)
                                          resultaux.get(k); %>
                                          <option value="<%= cmbP.getId()%>">
                                            <%= cmbP.getDescripcion()%>
                                          </option>
                                          <%}%>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr class="estiloImputDer">
                                    <td><label></label></td>
                                    <td><select style="visibility: hidden;"></select>
                                    </td>
                                  </tr>
                                  <tr class="estiloImputDer">
                                    <td colspan="4" align="CENTER">
                                      <input type="button" class="small button blue" value="ADICIONAR"
                                        style="margin: 5px 0 5px;"
                                        onclick="modificarCRUDParametros('adicionarProfesionFormularioTipoCitas')" />
                                      <input type="button" class="small button blue" value="ELIMINAR"
                                        style="margin: 5px 0 5px;"
                                        onclick="modificarCRUDParametros('eliminarProfesionFormularioTipoCitas')" />
                                    </td>
                                  </tr>
                                  <tr class="titulos">
                                    <td colspan="4">
                                      <table id="listGrillaProfesiones" class="scroll">
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>






              <table width="100%" style="margin-top: 5px" align="center">
                <tr class="titulosCentrados">
                  <td> PROCEDIMIENTO ASOCIADO AL TIPO DE CITA </td>
                </tr>
                <tr class="titulosListaEspera">
                  <td>
                    <table width="50%">
                      

                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td width="10%"><label for="txtIdProcedimientoCita">PROCEDIMIENTO:
                                </label></td>
                              <td width="40%">
                                <input type="text" id="txtIdProcedimientoCita"
                                  onkeyup="javascript: this.value = this.value.toUpperCase();"
                                  oninput="llenarElementosAutoCompletarKey(this.id, 204, this.value)"
                                  style="width:90%" />
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="10%">
                                <label for="cmbIdModalidadCita">MODALIDAD:
                                </label>
                              </td>
                              <td width="40%">
                                <select id="cmbIdModalidadCita">
                                  <option value="">[ NINGUNO ]
                                  </option>
                                </select>
                              </td>
                            </tr>

                            <tr class="estiloImputDer">
                              <td colspan="4" align="CENTER">
                                <input type="button" class="small button blue" value="ADICIONAR"
                                  style="margin: 5px 0 5px;"
                                  onclick="modificarCRUDParametros('adicionarProcedimientoTipoCitas')" />
                                <input type="button" class="small button blue" value="ELIMINAR"
                                  style="margin: 5px 0 5px;"
                                  onclick="modificarCRUDParametros('eliminarProcedimientoTipoCitas')" />
                              </td>
                            </tr>
                            <tr class="titulos">
                              <td colspan="4">
                                <table id="listGrillaCitaProcedimientos">
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>




                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table id="listGrillaTiposFormularioCita" class="scroll" width="100%"></table>
                  </td>
                </tr>
              </table>











            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>