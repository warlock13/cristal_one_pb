function crearTablasSolicitudesFarmacia(arg) {
	idArticulo = '';
	pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
	pagina = pag;
	ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
	switch (arg) {

		case 'listSolicitudesPaciente':
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=2288&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdentificacion'));
			add_valores_a_mandar(valorAtributo('txtIdAdmision'));
			add_valores_a_mandar(valorAtributo('cmbServicio'));
			add_valores_a_mandar(document.getElementById('lblIdSede').textContent);
			add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc') + ' 00:00:00');
			add_valores_a_mandar(valorAtributo('txtFechaHastaDoc') + ' 23:59:59');
			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: [
					'#', 'ID', 'ID ADMISION', 'ID ARTICULO', 'NOMBRE COMERCIAL', 'CANTIDAD', 'OBSERVACION', 'ID ESTADO', 'ID ELABORO',
					'NOMBRE PERSONAL', 'ID TIPO SOLICITUD', 'ID SERVICIO ORDENA', 'NOMBRE SERVICIO',
					'ID SEDE', 'NOMBRE SEDE', 'ESTADO INVENTARIO', 'ID USUARIO', 'NOMBRE USUARIO', 'FECHA ELABORO'
				],
				colModel: [
					{ name: 'CANT', index: 'CANT', width: anchoP(ancho, 2) },
					{ name: 'id', index: 'id', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_admision', index: 'id_admision', width: anchoP(ancho, 2) - 10 },
					{ name: 'id_articulo', index: 'id_articulo', width: anchoP(ancho, 2) - 10, hidden: true },
					{ name: 'nombre_comercial', index: 'nombre_comercial', width: anchoP(ancho, 2) + 10, hidden: true },
					{ name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 2) - 10, hidden: true },
					{ name: 'observacion', index: 'observacion', width: anchoP(ancho, 2) + 10, hidden: true },
					{ name: 'id_estado', index: 'id_estado', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_elaboro', index: 'id_elaboro', width: anchoP(ancho, 2), hidden: true },
					{ name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_tipo_solicitud', index: 'id_tipo_solicitud', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_servicio_ordena', index: 'id_servicio_ordena', width: anchoP(ancho, 2) },
					{ name: 'nombre_servicio', index: 'nombre_servicio', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 2), hidden: true },
					{ name: 'nombre_sede', index: 'nombre_sede', width: anchoP(ancho, 2), hidden: true },
					{ name: 'sw_inventario', index: 'sw_inventario', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_usuario', index: 'id_usuario', width: anchoP(ancho, 2) },
					{ name: 'nombre_usuario', index: 'nombre_usuario', width: anchoP(ancho, 2) },
					{ name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 2) },
				],

				height: 150,
				width: ancho + 40,
				onSelectRow: function (rowid) {
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#listSolicitudesPaciente').getRowData(rowid);
					asignaAtributo('lblAdmisionGrilla', datosRow.id_admision, 0);
					asignaAtributo('id_usuario', datosRow.id_admision, 0);
					setTimeout(() => {
						crearTablasSolicitudesFarmacia("listaSolicitudesFarmacia");
					}, 100);

					setTimeout(() => {
						crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
					}, 100);
				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;

		case 'listaSolicitudesFarmacia':
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=2286&parametros=";
			add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
			add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc') + ' 00:00:00');
			add_valores_a_mandar(valorAtributo('txtFechaHastaDoc') + ' 23:59:59');
			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: [
					'CANT', 'ID', 'ID ADMISION', 'ID ARTICULO', 'NOMBRE COMERCIAL', 'CANTIDAD', 'OBSERVACION', 'ID ESTADO', 'ID ELABORO',
					'NOMBRE PERSONAL', 'FECHA ELABORO', 'ID TIPO SOLICITUD', 'ID SERVICIO ORDENA', 'NOMBRE SERVICIO',
					'ID SEDE', 'NOMBRE SEDE', 'ESTADO INVENTARIO', 'ULT. CANT. ENTRE.', 'TOTAL ENTREGADO','TIPO SOLICITUD','LOTE DEV','ID TRANSACCION DEV'
				],
				colModel: [
					{ name: 'CANT', index: 'CANT', width: anchoP(ancho, 2) },
					{ name: 'id', index: 'id', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_admision', index: 'id_admision', width: anchoP(ancho, 2) - 10, hidden: true },
					{ name: 'id_articulo', index: 'id_articulo', width: anchoP(ancho, 2) - 10 },
					{ name: 'nombre_comercial', index: 'nombre_comercial', width: anchoP(ancho, 2) + 10 },
					{ name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 2) - 10 },
					{ name: 'observacion', index: 'observacion', width: anchoP(ancho, 2) + 10 },
					{ name: 'id_estado', index: 'id_estado', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_elaboro', index: 'id_elaboro', width: anchoP(ancho, 2), hidden: true },
					{ name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 2) },
					{ name: 'fecha_elaboro', index: 'fecha_elaboro', width: anchoP(ancho, 2) },
					{ name: 'id_tipo_solicitud', index: 'id_tipo_solicitud', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_servicio_ordena', index: 'id_servicio_ordena', width: anchoP(ancho, 2), hidden: true },
					{ name: 'nombre_servicio', index: 'nombre_servicio', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_sede', index: 'id_sede', width: anchoP(ancho, 2), hidden: true },
					{ name: 'nombre_sede', index: 'nombre_sede', width: anchoP(ancho, 2), hidden: true },
					{ name: 'sw_inventario', index: 'sw_inventario', width: anchoP(ancho, 2), hidden: true },
					{ name: 'ultima_cantidad_entregada', index: 'ultima_cantidad_entregada', width: anchoP(ancho, 2) },
					{ name: 'total_entregado', index: 'total_entregado', width: anchoP(ancho, 2) },

					{ name: 'id_solicitud_tipo', index: 'id_solicitud_tipo', width: anchoP(ancho, 2)},
					{ name: 'lote_devolucion', index: 'lote_devolucion', width: anchoP(ancho, 2), hidden:true },
					{ name: 'id_transaccion_dev', index: 'id_transaccion_dev', width: anchoP(ancho, 2), hidden:true },
				],

				height: 150,
				width: ancho + 40,
				ondblClickRow: function(rowId) {
					if(valorAtributo('cmbIdBodega') === ''){
						alert('Seleccione la bodega para devolver el producto');
						return
					}
					if(confirm('Esta seguro que realizar esta accion')){
						modificarCRUDDespachoPedidos('recibirBodega');
					}else{
						return
					}
				},
				onSelectRow: function (rowid) {
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#listaSolicitudesFarmacia').getRowData(rowid);
					asignaAtributo("lblArticuloLabel", datosRow.id_articulo, 0);
					document.getElementById("txtIdArticulo").value = datosRow.id_articulo + "-";
					asignaAtributo("lblNombreArticulo", datosRow.nombre_comercial, 0);
					asignaAtributo('lblCantidadTotal', datosRow.cantidad, 0);
					asignaAtributo('lblUltimaCantidadEntregada', datosRow.ultima_cantidad_entregada, 0)
					asignaAtributo('lblCantidadEntregada', datosRow.total_entregado, 0)
					let cantidadTotal = valorAtributo('lblCantidadTotal');
					let cantidadEntregada = valorAtributo('lblCantidadEntregada');
					let totalDisponible = cantidadTotal - cantidadEntregada;
					asignaAtributo('txtCantidad', totalDisponible, 0);
					asignaAtributo('lblIdTransaccion', datosRow.id);
					asignaAtributo('lblTipoSolicitud',datosRow.id_solicitud_tipo)
					asignaAtributo('lblLoteDevolucion',datosRow.lote_devolucion)
					asignaAtributo('lblIdTransDev',datosRow.id_transaccion_dev)
					asignaAtributo('lblAdmisionDevolucion', datosRow.id_admision)

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;

		case 'listTransaccionesAdmision':
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=2294&parametros=";
			add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: [
					'CANT', 'ID', 'ID ADMISION', 'ID ARTICULO', 'NOMBRE ARTICULO', 'CANTIDAD', 'ID LOTE', 'ESTADO INV','ID SOLICITUD FARMACIA'
				],
				colModel: [
					{ name: 'CANT', index: 'CANT', width: anchoP(ancho, 2) },
					{ name: 'id', index: 'id', width: anchoP(ancho, 2), hidden: true },
					{ name: 'id_admision', index: 'id_admision', width: anchoP(ancho, 2) - 10, hidden: true },
					{ name: 'id_articulo', index: 'id_articulo', width: anchoP(ancho, 2) - 10 },
					{ name: 'nombre_articulo', index: 'nombre_articulo', width: anchoP(ancho, 2) + 10 },
					{ name: 'cantidad', index: 'cantidad', width: anchoP(ancho, 2) - 10 },
					{ name: 'id_lote', index: 'id_lote', width: anchoP(ancho, 2) - 10 },
					{ name: 'sw_inventario', index: 'sw_inventario', width: anchoP(ancho, 2) - 10 },
					{ name: 'id_solicitud_farmacia', index: 'id_solicitud_farmacia', width: anchoP(ancho, 2) - 10, hidden:true},
				],

				height: 150,
				width: ancho + 40,
				onSelectRow: function (rowid) {
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#listTransaccionesAdmision').getRowData(rowid);
					lblIdRevertirTransaccion
					asignaAtributo('lblIdRevertirTransaccion',datosRow.id,0);
					asignaAtributo('lblReveritrTransaccion', datosRow.sw_inventario, 0);
					asignaAtributo('lblSolicitudFarmaciaTransaccion',datosRow.id_solicitud_farmacia,0);
					asignaAtributo('lblCantidadTransaccion',datosRow.cantidad);

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;
	}
}