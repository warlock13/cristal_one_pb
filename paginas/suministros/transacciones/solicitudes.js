function validarFechaVencimiento() {
    let infoLotes = document.getElementById('cmbIdLote')
    let infoLote = infoLotes.options[infoLotes.selectedIndex].text;
    let fechaVencimiento = infoLote.split("FECHA_VENCIMIENTO:")[1].split('-');
    let diaVencimiento = parseInt(fechaVencimiento[2]);
    let mesVencimiento = parseInt(fechaVencimiento[1]);
    let anioVencimiento = parseInt(fechaVencimiento[0]);
    let fecha = new Date();
    let dia = parseInt(fecha.getDate());
    let mes = parseInt(fecha.getMonth()) + 1;
    let anio = parseInt(fecha.getFullYear());
    if(infoLote.includes('SIN_LOTE')){
        return 'true';
    }else{
        if (anio > anioVencimiento) {
            return 'false'
        }
        if (anio === anioVencimiento && mes > mesVencimiento) {
            return 'false'
        }
        if (anio === anioVencimiento && mes === mesVencimiento && dia > diaVencimiento) {
            return 'false'
        }
        if (anio === anioVencimiento && mes === mesVencimiento && dia === diaVencimiento) {
            return 'warning'
        }
    
    }
}

function validarCantidad() {
    let infoCantidades = document.getElementById('cmbIdLote');
    let infoCantidad = infoCantidades.options[infoCantidades.selectedIndex].text;
    let cantidad = infoCantidad.split("EXISTENCIAS:")[1];
    let cantidadFinal = parseInt(cantidad.split("::"));
    if (cantidadFinal < parseInt(valorAtributo('txtCantidad'))) {
        return 'false';
    }
}

function verificarYEnviarSalidaSolicitud() {
    return  new Promise((resolve,reject) => {
            count = 0;
            var rows = jQuery("#listTransaccionesAdmision").jqGrid('getDataIDs');
            if(rows.length === 0){
                alert('No puedes cerrar el documento, no tiene transacciones');
                return 
            }
            for (var i = 0; i < rows.length; i++) {
                var row = jQuery("#listTransaccionesAdmision").getRowData(rows[i]);
                if(row.sw_inventario === 'F'){
                    count+=1;
                }
                if(count === rows.length){
                    alert('No puedes realizar esta accion, todas las solicitudes han sido gestionadas (F)');
                    return;
                }
            }
            var message = '';
            for (var i = 0; i < rows.length; i++) {
                var row = jQuery("#listTransaccionesAdmision").getRowData(rows[i]);
                if(row.sw_inventario === 'N'){
                    grafica = 'http://190.60.242.160:8001/servicio/buscarDisponibilidadProductos/' + row.id_articulo + '/' + valorAtributo('cmbIdBodega') + '/' + row.cantidad + '/' + row.id_lote;
                    console.log(grafica);
                    $.get(grafica, function (data) {
                        if (data === null || data === undefined) {
                            message += '\n' + 'Debes retirar el siguiente producto:' + '\nID PRODUCTO: ' + row.id_articulo + '\nID BODEGA: ' + valorAtributo('cmbIdBodega') + '\nCANTIDAD: ' + row.cantidad + '\nLOTE:' + row.id_lote + '\nYa no hay existencia suficientes para realizar la salida';
                            document.getElementById('lblMessage').textContent = message;
                        }else{
                            document.getElementById('lblMessage').textContent = '';
                        }
                    }).done(function() {
                        resolve('exito');
                    }).fail(function(error) {
                        alert('Error de codigo ' + error.status + ' Por favor contacte con soporte');
                    });
                }
            } 
    });
}

async function asynCallSalidaSolicitud(arg){
    if(document.getElementById('cmbIdBodega').value === ''){
        alert('Escoja una bodega');
        return;
    }
    const result = await verificarYEnviarSalidaSolicitud();
    console.log(result);
    if(result === 'exito'){
        if(valorAtributo('lblMessage') !== ''){
            alert(valorAtributo('lblMessage'));
            return
        }
        valores_a_mandar = "accion=" + arg;
        valores_a_mandar = valores_a_mandar + "&idQuery=2295&parametros=";
        add_valores_a_mandar("35");
        add_valores_a_mandar(IdSesion());
        add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
        add_valores_a_mandar(document.getElementById('cmbIdBodega').value);
        add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
        add_valores_a_mandar(IdSesion());
        add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
        ajaxModificar();
    }
}



function modificarCRUDDespachoPedidos(arg) {
    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {

        case 'cerrarTransacciones':
            asynCallSalidaSolicitud(arg);
        break;
        

        case 'revertirTransaccionSolicitudEntregada':
            if (valorAtributo("lblIdRevertirTransaccion") === '') {
                alert('Seleccione el producto que desea reverir');
                return;
            }
            if (valorAtributo('lblReveritrTransaccion') === 'F') {
                alert('No puede revertir transacciones con estado F(FINALIZADO), puesto que ya se encuentran registradas en el inventario del sistema');
                return;
            }
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2314&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdRevertirTransaccion"));
            add_valores_a_mandar(valorAtributo("lblCantidadTransaccion"));
            add_valores_a_mandar(valorAtributo("lblSolicitudFarmaciaTransaccion"));
            add_valores_a_mandar(valorAtributo("lblSolicitudFarmaciaTransaccion"));
            ajaxModificar();
            break;

        case 'enviarProductosASalida':
            if (valorAtributo('lblTipoSolicitud') === 'D') {
                alert('Haga doble clic en la fila para realizar la devolucion de este producto');
                return
            } else {
                var mensaje_error = '';
                if (validarFechaVencimiento() === 'false') {
                    mensaje_error = 'ADVERTENCIA#1. El lote del producto esta vencido, selecciona otro lote\n';
                }
                if (validarFechaVencimiento() === 'warning') {
                    mensaje_error = mensaje_error + 'ADVERTENCIA#2. El producto vence el dia de hoy, selecciona otro lote \n';
                }
                if (validarCantidad() === 'false') {
                    mensaje_error = mensaje_error + 'ADVERTENCIA#3. La cantidad que desea entregar supera las existencia del lote'
                }
                if(parseInt(valorAtributo('lblCantidadTotal')) < parseInt(valorAtributo('txtCantidad'))){
                    alert('No puedes entregar mas de lo que te piden en la solicitud, por favor entrega la cantidad total o menos');
                    return;
                }
                if (mensaje_error !== '') {
                    alert(mensaje_error);
                    return
                }
                var rows = jQuery("#listTransaccionesAdmision").jqGrid('getDataIDs');
                for (var i = 0; i < rows.length; i++) {
                    var row = jQuery("#listTransaccionesAdmision").getRowData(rows[i]);
                    if (row.id_articulo === valorAtributoIdAutoCompletar("txtIdArticulo") && row.id_lote === valorAtributo("cmbIdLote") && row.sw_inventario === 'N') {
                        alert('Ya tiene un articulo con igual lote pendiente por entregar, no puedes adicionar del mismo lote hasta entregar el anterior');
                        return
                    }

                }
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2293&parametros=";
                add_valores_a_mandar(valorAtributo("lblAdmisionGrilla"));
                add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdArticulo"));
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("cmbIdLote"));
                let infoLotes = document.getElementById('cmbIdLote')
                let infoLote = infoLotes.options[infoLotes.selectedIndex].text;
                let fechaVencimiento = infoLote.split("FECHA_VENCIMIENTO:")[1].split('-');
                let diaVencimiento = parseInt(fechaVencimiento[2]);
                let mesVencimiento = parseInt(fechaVencimiento[1]);
                let anioVencimiento = parseInt(fechaVencimiento[0]);
                let fecha_final = diaVencimiento + '/' + mesVencimiento + '/' + anioVencimiento;
                add_valores_a_mandar(fecha_final);
                add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                //actualizar cantidades de la tabla
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                ajaxModificar();
            }
            break;


        case 'enviarProductosHojaConsumo':
            if (valorAtributo('txtCantidadRestante') === '0') {
                alert('Ya has suministrado todas las cantidades del producto');
                return
            }
            if (valorAtributo("lblIdArticuloTransaccion") === '') {
                alert('Seleccione un producto');
                return
            }
            if(document.getElementById('lblIdAdmisionAgen') === ''){
                alert('Debe seleccionar un paciente con admision');
                return
            }
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2312&parametros=";
            add_valores_a_mandar(document.getElementById("lblIdAdmisionAgen").textContent);
            add_valores_a_mandar(valorAtributo("lblIdArticuloTransaccion"));
            add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(valorAtributo("cmbIdTipoServicio"));
            ajaxModificar();
            // add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
            // add_valores_a_mandar(valorAtributo("lblCantidadDosis"));
            // add_valores_a_mandar(valorAtributo("lblTotal"));



            break;


        case 'enviarProductosHojaConsumo':
            if (valorAtributo('txtCantidadRestante') === '0') {
                alert('Ya has suministrado todas las cantidades del producto');
                return
            }
            if (valorAtributo("lblIdArticuloTransaccion") === '') {
                alert('Seleccione un producto');
                return
            }
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2297&parametros=";
            add_valores_a_mandar(document.getElementById("lblIdAdmisionAgen").textContent);
            add_valores_a_mandar(valorAtributo("lblIdArticuloTransaccion"));
            add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(valorAtributo("cmbIdTipoServicio"));


            add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
            add_valores_a_mandar(valorAtributo("lblCantidadDosis"));
            add_valores_a_mandar(valorAtributo("lblTotal"));


            ajaxModificar();
            break;

        case 'revertirHojaConsumo':
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2299&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdTransHojaDeGastos"));
            add_valores_a_mandar(valorAtributo("lblTransaccionResta"));
            ajaxModificar();
            break;


        case 'anadirUltimoConsumo':
            var rows = jQuery("#listHojaGastos").jqGrid('getDataIDs');
            var count = 0;
            for (var i = 0; i < rows.length; i++) {
                var row = jQuery("#listHojaGastos").getRowData(rows[i]);
                if (row.id_transaccion.trim() === valorAtributo("lblIdTransaccion")) {
                    count = count + 1;
                }
            }
            if (count > 0) {
                if (confirm("Estas seguro es la ultima cantidad ?")) {
                    anadirUltimoConsumoConfirmada();
                } else {
                    console.log('tarabajando.......');
                }
            }
            if (count === 0) {
                alert('No puedes poner este articulo en ultimo consumo porque no lo has usado ni una solo vez !!!');
                return
            }

            function anadirUltimoConsumoConfirmada() {
                if (valorAtributo('lblSwPresentacion') === 'NO') {

                    valores_a_mandar = "accion=" + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2313&parametros=";
                    add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                    add_valores_a_mandar(valorAtributo("lblIdTransaccion"));

                    add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                    add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                    add_valores_a_mandar(valorAtributo("lblTotal"));
                    ajaxModificar();
                } else {
                    valores_a_mandar = "accion=" + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2300&parametros=";
                    add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                    ajaxModificar();
                }

            }
            break;


        case 'devolverABodega':
            var rows = jQuery("#listHojaGastos").jqGrid('getDataIDs');
            if (valorAtributo('lblSwPresentacion') === 'NO') {
                for (var i = 0; i < rows.length; i++) {
                    var row = jQuery("#listHojaGastos").getRowData(rows[i]);
                    if (row.id_transaccion.trim() === valorAtributo("lblIdTransaccion") && row.cantidad_ultimo_consumo.trim() === '0') {
                        alert('Por favor pon en ultimo consumo este producto para poder devolverlo');
                        return;
                    }
                }
            } 

            if (confirm("Esta seguro de querer devolver esta cantidad a Bodega ?")) {
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2304&parametros=";
                add_valores_a_mandar(document.getElementById('lblIdAdmisionAgen').textContent.trim());
                add_valores_a_mandar(valorAtributo('lblIdArticuloTransaccion'));
                add_valores_a_mandar(valorAtributo('txtCantidadRestante'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(document.getElementById('lblIdSede').textContent.trim());
                add_valores_a_mandar(valorAtributo('lblIdArticuloTransaccion'));
                add_valores_a_mandar(document.getElementById('cmbIdTipoServicio').value);
                add_valores_a_mandar(valorAtributo('lblLoteDespacho'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                ajaxModificar();
            } else {
                return
            }
            break;


        case 'recibirBodega':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2307&parametros=";
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(document.getElementById('lblAdmisionDevolucion').textContent);
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));

            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('lblLoteDevolucion'));
            add_valores_a_mandar(document.getElementById('lblAdmisionDevolucion').textContent);
            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));


            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));

            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            add_valores_a_mandar(valorAtributo('lblLoteDevolucion'));

            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));

            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));

            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

            ajaxModificar();
            break
    }
}

function respuestaModificarCRUDDespachoPedidos(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case "enviarProductosASalida":
                alert('Producto adiconado correctamente');
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listaSolicitudesFarmacia');
                }, 100);
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
                }, 150);
                break;


            case 'recibirBodega':
                alert('Productos devueltos con exito');
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listaSolicitudesFarmacia');
                }, 100);
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
                }, 150);
                break;

            case "cerrarTransacciones":
                alert("Documento creado con exito");
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
                }, 100);
                break;

            case "anadirUltimoConsumo":
                setTimeout(() => {
                    crearTablasGastos('listHojaGastosDespachos');
                }, 100);
                setTimeout(() => {
                    crearTablasGastos('listHojaGastos');
                }, 150);
                break;

            case "enviarProductosHojaConsumo":
                document.getElementById('lblIdArticuloTransaccion').textContent = '';
                document.getElementById('lblCantidadDosis').textContent = '';
                document.getElementById('lblTotal').textContent = '';
                document.getElementById('txtCantidadRestante').value = '';
                setTimeout(() => {
                    crearTablasGastos('listHojaGastosDespachos');
                }, 100);
                setTimeout(() => {
                    crearTablasGastos('listHojaGastos');
                }, 150);
                break;

            case 'devolverABodega':
                alert('Exito !!! Entrega los productos en almacen o bedaga de la sede')
                setTimeout(() => {
                    crearTablasGastos('listHojaGastosDespachos');
                }, 100);
                setTimeout(() => {
                    crearTablasGastos('listHojaGastos');
                }, 150);
                setTimeout(() => {
                    crearTablasGastos('listGrillaSolicitudFarmacia');
                }, 200);
                break;

            case "revertirHojaConsumo":
                setTimeout(() => {
                    crearTablasGastos('listHojaGastos');
                }, 100);
                break

            case "revertirTransaccionSolicitudEntregada":
                asignaAtributo("lblIdRevertirTransaccion",'',0);
                asignaAtributo("lblCantidadTransaccion",'',0);
                asignaAtributo("lblSolicitudFarmaciaTransaccion",'',0);
                asignaAtributo("lblSolicitudFarmaciaTransaccion",'',0);
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listaSolicitudesFarmacia');
                }, 100);
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
                }, 200);
                break;
        }
    }
}