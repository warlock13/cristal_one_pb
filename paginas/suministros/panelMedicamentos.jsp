<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1150px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <div align="center" id="tituloForma">
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="PANEL MEDICAMENTOS" />
                </jsp:include>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:750px; width:1150px" id="divContenido">
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">

                                    <tr class="titulosListaEspera">
                                        <td width="25%">SEDE</td>
                                        <td width="30%">PACIENTE</td>
                                        <td width="10%">FECHA DESDE</td>    
                                        <td width="10%">FECHA HASTA</td>  
                                        <td width="15%">ESTADO</td> 
                                        <td width="10%"></td>
                                    </tr>

                                    <tr class="estiloImput">


                                        <td>
                                            <select size="1" id="cmbSede" style="width:95%" title="QC65"
                                                    onfocus="comboDependienteSede('cmbSede', '557')">	 
                                                <option value=""></option>
                                            </select>                        
                                        </td>

                                        <td>
                                            <input type="text" id="txtIdPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdPaciente', 307)" style="width:80%"/> 
                                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPaciente"
                                                 onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdPaciente', '26')"
                                                 src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                            <div id="divParaVentanita"></div>
                                        </td> 
                                        <td><input type="text" style="width:85%"  id="txtFechaDesdeP"/></td>    
                                        <td><input type="text" style="width:90%"  id="txtFechaHastaP"/></td>  

                                        <td><select size="1" id="cmbEstado" style="width:80%" >
                                                <option value=""></option>
                                                <% resultaux.clear();
                                                    resultaux = (ArrayList) beanAdmin.combo.cargar(92);
                                                    ComboVO cmbEtn;
                                                    for (int k = 0; k < resultaux.size(); k++) {
                                                        cmbEtn = (ComboVO) resultaux.get(k);
                                                %>
                                                <option value="<%= cmbEtn.getId()%>" title="<%= cmbEtn.getTitle()%>">
                                                    <%= cmbEtn.getDescripcion()%></option>
                                                    <%}%> 
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" class="small button blue" title="BTL4" value="BUSCAR"  onclick="buscarSuministros('listaPanelMedicamentos')"   />                                                                      
                                        </td>                            
                                    </tr>   

                                </table>
                                <table id="listaPanelMedicamentos" class="scroll"></table>  

                                <table align="center" width="100%">
                                    <tr class="estiloImput">
                                        <td>
                                            <input type="button" class="small button blue" title="BTL4" value="ENVIAR SISTEMA FARMACEUTICO"  onclick="guardarYtraerDatoAlListado('enviarSF')"   /> 
                                            <input type="button" class="small button blue" title="BTL4" value="CONSULTAR SF"  onclick="guardarYtraerDatoAlListado('consultarSF_')"   />   
                                        </td>
                                    </tr>
                                </table>

                                <div style="height: 150px;">
                                    <table id="listaDetallePanelMedicamentos" class="scroll"></table>   
                                </div>



                                <table width="100%">

                                    <tr class="titulosListaEspera">
                                        <td width="60%">MEDICAMENTO</td>
                                        <td width="20%">CANTIDAD</td>
                                        <td></td>
                                    </tr>

                                    <tr class="estiloImput">
                                        <td>
                                            <input type="text" id="txtArticulo"
                                                   onkeypress="llamarAutocompletarParametro1('txtArticulo', 830, 'lblIdArticulo')" style="width: 85%;"  />
                                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem"
                                                 onclick="traerVentanitaCondicion1(this.id, 'lblIdArticulo', 'divParaVentanita', 'txtArticulo', '33');"
                                                 src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                        </td>
                                        <td><input type="number"   id="txtCantidad"/></td>   
                                        <td>
                                            <input type="button" class="small button blue" title="BTL4" value="ENTREGAR"  onclick="modificarSuministros('agregarEntregaMedicamento')"   /> 

                                        </td>

                                    </tr>

                                </table>



                                <table id="listaEntregaPanelMedicamentos" class="scroll"></table>  
                            </div>              
                        </div>

                    </td>
                </tr>
            </table>

        </td>
    </tr> 

</table>




<input type="hidden" id="lblIdEvolucion"  />  
<input type="hidden" id="lblIdMedicacion"  />  
<input type="hidden" id="lblIdArticulo"  />  





