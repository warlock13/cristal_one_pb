﻿ <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="ARTICULO" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

   <div style="overflow:auto;height:350px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
      <div id="divBuscar"  style="display:block"   >
      <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
           <tr class="titulos" align="center">
              <td width="10%">CODIGO</td>                            
              <td width="40%">NOMBRE</td>
              <td width="20%">CLASE</td>              
              <td width="20%">VIGENTE</td>
              <td width="10%">&nbsp;</td>                            
           </tr>
           <tr class="estiloImput"> 
              <td colspan="1"><input type="text" id="txtCodBus"  style="width:90%"   /> 
              </td> 
              <td colspan="1"><input type="text" id="txtNomBus"  style="width:90%"   /> 
              </td> 
              <td width="20%">
					<select id="cmbIdClaseBus" style="width:70%"   >
                       <option value=""></option>
                       	<%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(308);	
                               ComboVO cmbc; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbc=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbc.getId()%>" title="<%= cmbc.getTitle()%>"><%= cmbc.getId()+"  "+cmbc.getDescripcion()%></option>
                             <%} %>					
                   </select>              
              </td>                            
              
              <td><select size="1" id="cmbVigenteBus" style="width:80%"  >	                                        
                  <option value="SI">SI</option>
                  <option value="NO">NO</option>                  
                 </select>	                   
              </td>     
              <td>
                  <input title="bw79U" type="button" class="small button blue" value="BUSCAR"  onclick="buscarSuministros('listGrillaElemento3')"   />                    
              </td>         
          </tr>	                          
      </table>
     <table id="listGrillaElemento3" class="scroll"></table>  
    </div>              
  </div><!-- div contenido-->

<div id="divEditar" style="display:block; width:100%" >  

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3">DATOS DEL ARTICULO
      </td>   
    </tr>  
    <tr>
      <td>
         <table width="100%">
            <tr   class="estiloImputIzq2">
              <td width="30%">ID ARTICULO:</td><td width="70%"><input type="text" id="txtIdArticulo" style="width:5%" readonly="readonly" /></td> 
            </tr>	
            <tr   class="estiloImputIzq2">                         
              <td>NOMBRE ARTICULO (Generico):</td><td>              
					<input type="text" id="txtNombreArticulo" style="width:70%" />
              </td>                                             
            </tr>
			<tr   class="estiloImputIzq2">                         
              <td>NOMBRE COMERCIAL:</td><td>              
					<input type="text" id="txtNombreComercial" style="width:70%" />
              </td>                                             
            </tr>
            <tr class="estiloImputIzq2">                          
              <td>CLASE:</td>
			  <td>					
				   <select id="cmbIdClase" style="width:30%"   >
						<option value=""></option>
						<option value="DM">DM Dispositivo Médico</option>
						<option value="MD">MD Medicamento</option>
						<option value="PO">PO Producto de óptica</option>
                        <option value="KI">KI Kit's</option>
				   </select >
				</td>                                             
            </tr>	      
            <tr class="estiloImputIzq2">                          
              <td>GRUPO</td><td><select id="cmbIdGrupo" style="width:40%"   >
                       <option value=""></option>
                       	<%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(23);	
                               ComboVO cmb2A; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb2A=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmb2A.getId()%>" title="<%= cmb2A.getTitle()%>"><%= cmb2A.getId()+"  "+cmb2A.getDescripcion()%></option>
                             <%} %>					
                   </select></td>                                             
            </tr>	            	
			<tr   class="estiloImputIzq2">
              <td width="30%">PRESENTACION:</td><td width="70%">
			  <select id="cmbPresentacion" style="width:40%"  >
                      <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(38);	
                               ComboVO cmbPre; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbPre=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbPre.getId()%>" > <%=cmbPre.getDescripcion()%></option>
                             <%}%>			
                   </select></td> 
            </tr>
			<tr   class="estiloImputIzq2">
              <td width="30%">CONCENTRACION:</td><td width="70%"><input type="text" id="txtConcentracion"  style="width:20%"/></td> 
            </tr>	
			
             <tr   class="estiloImputIzq2">                         
              <td>CODIGO ARTICULO:</td><td>              
					<input type="text" id="txtCodArticulo" style="width:20%" />
              </td>                                             
            </tr>
             <tr   class="estiloImputIzq2">                         
              <td>DESCRIPCION COMPLETA:</td><td>              
					<textarea type="text" id="txtDescripcionCompleta" rows="2" size="1000"  maxlength="1000" style="width:95%; border:solid #06C 1px"   > </textarea>                   
              </td>                                             
            </tr>
            <tr   class="estiloImputIzq2">                         
              <td>DESCRIPCION ABREVIADA: (Referencia)</td><td>               
					<input type="text" id="txtDescripcionAbreviada" style="width:80%" />
              </td>                                             
            </tr>            
            <tr   class="estiloImputIzq2">                         
              <td>FABRICANTE:</td><td>              
					<input type="text" id="txtIdFabricante" onkeypress="llamarAutocomIdDescripcionConDato('txtIdFabricante',297)" style="width:70%" />
              </td>                                             
            </tr> 
			<tr   class="estiloImputIzq2">                         
              <td>MARCA:</td><td>              
					<input type="text" id="txtMarca"  style="width:70%" />
              </td>                                             
            </tr>                              
            <tr class="estiloImputIzq2">                         
              <td>UNIDAD DE MEDIDA</td><td> 
					<select id="cmbIdUnidad" style="width:40%"  >
                      <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(32);	
                               ComboVO cmb9; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb9=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmb9.getId()%>" title="<%= cmb9.getTitle()%>"><%=cmb9.getDescripcion()%></option>
                             <%}%>			
                   </select> POR 
                   <input type="text" id="txtValorUnidad" style="width:10%"  />
				   &nbsp;
				   <select id="cmbML" style="width:7%"  >
					<option value=""></option>
					<option value="centimetros">Centimetros</option>
					<option value="dosis">Dosis</option>
					<option value="gramos">Gramos</option>
					<option value="metros">Metros</option>
					<option value="metros cubicos">Metros cubicos</option>
					<option value="mcg">mcg</option>
					<option value="ml">ml</option>
					<option value="mg">mg</option>
					<option value="hebras">Hebras</option>
					<option value="unidades">Unidades</option>
					<option value="yardas">Yardas</option>
                   </select>
               </td>                                             
             </tr>
			 <tr   class="estiloImputIzq2">
              <td width="30%">CADA ARTICULO CONTIENE:</td>
			  <td width="70%"><input type="text" id="txtFactorConv"  style="width:20%"/>
				<select id="cmbIdUnidadAdministracion" style="width:15%"   >
                      <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(39);	
                               ComboVO cmbUA; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbUA=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbUA.getId()%>" title="<%= cmbUA.getTitle()%>"><%=cmbUA.getDescripcion()%></option>
                             <%}%>			
                   </select>
			  </td> 
            </tr> 
            <tr class="estiloImputIzq2">                         
              <td>PORCENTAJE IVA</td><td> 
	              <input type="text" id="txtIVA" style="width:20%"  /> %					
               </td>                                             
             </tr>             
             <tr class="estiloImputIzq2">                           
              <td>PORCENTAJE IVA VENTA</td>
              <td> 
                  <select id="cmbIVAVenta" style="width:8%"   >
                     <option value="N">NO</option>
                      <option value="S">SI</option>
                  </select> 
                 %	    
                
                    </td>                                             
             </tr> 
             <tr class="estiloImputIzq2">                           
              <td>REGISTRO INVIMA</td><td>
                  <input type="text" id="txtInvima" style="width:20%"  />	                  
                &nbsp;&nbsp; VIGENCIA REGISTRO INVIMA &nbsp;
                <input type="text" id="txtVigInvima" style="width:15%" />
              </td> 		
             </tr>    
             <tr class="estiloImputIzq2">                         
                <td>CLASE DE RIESGO</td><td> 
					<select id="cmbIdClaseRiesgo" style="width:20%"   >
                      <option value=""></option>                        
                      <option value="I">I</option>                        
                      <option value="II">II</option>                        
                      <option value="IIA">IIA</option>                        
                      <option value="IIB">IIB</option>                        
                      <option value="III">III</option>                        
                      <option value="NA">NA</option>                        
                   </select>
				&nbsp;&nbsp;&nbsp; VIDA UTIL: &nbsp;
					<select id="cmbIdVidaUtil" style="width:10%"   >
                      <option value=""></option>                        
                      <option value="1">1</option>                                                                    
                      <option value="2">2</option>                                                                    
                      <option value="3">3</option>                                                                    
                      <option value="4">4</option>                                                                    
                      <option value="5">5</option>                                                                    
                      <option value="6">6</option>                                                                    
                      <option value="NA">NA</option>                        
                   </select> AÑO(S)
              </td>                                           
             </tr>    
               
             <tr class="estiloImputIzq2">                                   
              <td>TIPO DE DESPACHO</td><td> 
                <select size="1" id="cmbIdFechaVencimiento" style="width:20%"  >
                  <option value=""></option>
                  <option value="DE">DESPACHO PROGRAMACION</option>
                  <option value="CO">CONSUMO</option>                                                    
                  <option value="SA">SALIDA</option>                                                    
                 </select>	 
              </td>                                             
             </tr> 
              <tr class="estiloImputIzq2">                           
              <td>PROVEEDOR UNICO</td><td>  
              	 <select size="1" id="cmbIdProveedorUnico" style="width:20%"  >
                  <option value=""></option>               
                  <option value="SI">SI</option>
                  <option value="NO">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>  
             <tr   class="estiloImputIzq2">
              <td width="30%">ARTICULO POS:</td><td width="70%"><select size="1" id="cmbPOS"  style="width:20%"  >
                  <option value=""></option>               
                  <option value="POS">POS</option>
                  <option value="NOPOS">NOPOS</option>                                                    
                  <option value="NA">NO APLICA</option>                                                    
                 </select>	</td> 
            </tr>
             <tr class="estiloImputIzq2">                           
              <td>VIGENTE</td><td>  
              	 <select size="1" id="cmbVigente" style="width:20%"  >
                  <option value=""></option>               
                  <option value="SI">SI</option>
                  <option value="NO">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>
             <tr>
               <td colspan="2" align="center">
				 <input id="btn_limpia" title="btn_lp78e"class="small button blue" type="button" value="Limpiar"  onclick="limpiarDivEditarJuan('articulo')">              
                 <input id="btn_crea" title="btn_c75r" class="small button blue" type="button"  value="Crear"  onclick="modificarCRUD('crearArticulo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                               
                 <input name="btn_modifica" title="btn_mo56t" type="button" class="small button blue" value="Modificar" onclick="modificarCRUD('modificaArticulo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
<!--                 <input name="btn_elimina" title="btn_el98y" type="button" class="small button blue" value="Eliminar" onclick="modificarCRUD('eliminaArticulo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />                   -->
               </td>
             </tr>                                                     
        </table>
      </td>  
    </tr>  
  </table> 
  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3">DATOS DEL MEDICAMENTO
      </td>   
    </tr>  
    <tr>
      <td>
  		<table width="100%">
			<tr class="estiloImputIzq2">                           
              <td>CUM</td><td> <input id="txtATC" style="width:20%" />
                </td>                                             
             </tr> 
			 <tr class="estiloImputIzq2">                           
              <td>ATC</td><td> <input id="txtCodAtc" style="width:20%" />
                </td>                                             
             </tr> 
            <tr   class="estiloImputIzq2">
              <td width="30%">ANATOMOFARMACOLOGICO:</td><td width="70%"><select id="cmbAnatomofarmacologico" style="width:40%"  >
                      <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(35);	
                               ComboVO cmbAf; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbAf=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbAf.getId()%>" title="<%= cmbAf.getTitle()%>"><%=cmbAf.getDescripcion()%></option>
                             <%}%>			
                   </select></td> 
            </tr>
            <tr   class="estiloImputIzq2">
              <td width="30%">PRINCIPIO ACTIVO:</td><td width="70%"><select id="cmbPrincipioActivo" style="width:40%"  >
                      <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(36);	
                               ComboVO cmbPa; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbPa=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbPa.getId()%>" title="<%= cmbPa.getTitle()%>"><%=cmbPa.getDescripcion()%></option>
                             <%}%>			
                   </select></td> 
            </tr>
            <tr   class="estiloImputIzq2">
              <td width="30%">FORMA FARMACOLOGICA:</td><td width="70%"><select id="cmbFormaFarmacologica" style="width:40%"  >
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(37);	
                               ComboVO cmbFf; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbFf=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbFf.getId()%>" title="<%= cmbFf.getTitle()%>"><%=cmbFf.getDescripcion()%></option>
                             <%}%>			
                   </select></td> 
            </tr>                  
            
         </table>
      </td>   
    </tr>   
  </table>
   
   <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3">VIAS DE ADMINISTRACION
      </td>   
    </tr>  
    <tr>
      <td>
  		<table width="100%">
			<tr class="estiloImputIzq2">                           
              <td>ARTICULO</td><td> <label id="lblNomArticulo" style="width:20%" ></label>
				<input type="hidden" id="txtIdViaArticulo" />
			   </td>                                             
             </tr> 
            <tr   class="estiloImputIzq2">
              <td width="30%">VIA ADMINISTRACION:</td><td width="70%"><select id="cmbViaArticulo" style="width:40%"  >
                      <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(524);	
                               ComboVO cmbVA; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbVA=(ComboVO)resultaux.get(k);
                        %>
                       <option value="<%= cmbVA.getId()%>" title="<%= cmbVA.getTitle()%>"><%=cmbVA.getDescripcion()%></option>
                             <%}%>	 			
                   </select></td> 
            </tr>
			<tr>
               <td colspan="2" align="center">
				 <input id="btn_limpia" title="btn_lp78"class="small button blue" type="button" value="Limpiar"  onclick="limpiarDivEditarJuan('articulo')">              
                 <input id="btn_crea" title="btn_c75v" class="small button blue" type="button"  value="Crear"  onclick="modificarCRUD('crearViaArticulo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                               
                 <input name="btn_modifica" title="btn_el58" type="button" class="small button blue" value="Eliminar" onclick="modificarCRUD('eliminarViaArticulo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  
               </td>
             </tr>
		</table>
		<table id="listGrillaViaArticulo" class="scroll"></table>   
	  </td>	
	</tr>  
  </table>	
   
</div><!-- divEditar--> 
      

        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>

