<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 

<%@ page import="java.util.zip.ZipEntry" %> 
<%@ page import="java.util.zip.ZipOutputStream" %> 
<%@ page import="java.util.List" %> 
<%@ page import="java.util.ArrayList" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session"/> 

<!DOCTYPE HTML>  
<%
    File file = null;
    try {
        Connection conexion = beanSession.cn.getConexion();

        String ruta_tipificacion = "/opt/extras_cw/tipificacion_externa/";
        PreparedStatement pr = null;
        String tabla = request.getParameter("tabla");
        int bean = (int) (Math.random() * 10000);
        String ruta_archivo_tipificacion_comprimido = ruta_tipificacion + tabla + bean + "_.zip";
        String sql = null;

        sql = String.format("SELECT id_factura FROM facturacion.pacientes_tipificacion_externa WHERE estado IS NOT NULL AND plan='%s'", tabla);

        pr = conexion.prepareStatement(sql);
        ResultSet facturas = pr.executeQuery();

        FileOutputStream fosT = new FileOutputStream(ruta_archivo_tipificacion_comprimido);
        ZipOutputStream zipOutT = new ZipOutputStream(fosT);

        while (facturas.next()) {

            String sqlT = String.format("SELECT "
                    + "	    'HCE' nombre_archivo, "
                    + "     btrim(factura) numero_factura, "
                    + "     identificacion "
                    + " FROM "
                    + "	    facturacion.pacientes_tipificacion_externa "
                    + " WHERE "
                    + "     id_factura = %s", facturas.getInt("id_factura"));

            PreparedStatement prT = conexion.prepareStatement(sqlT);
            ResultSet archivosFactura = prT.executeQuery();

            while (archivosFactura.next()) {
                try {
                    System.err.println(ruta_tipificacion + archivosFactura.getString("numero_factura") + "/" + archivosFactura.getString("nombre_archivo") + ".pdf");
                    File fileToZip = new File(ruta_tipificacion + archivosFactura.getString("numero_factura") + "/" + archivosFactura.getString("nombre_archivo") + ".pdf");

                    System.err.println("NUEVO ARCHIVO ZIP: " + fileToZip.getAbsolutePath());
                    FileInputStream fis = new FileInputStream(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(archivosFactura.getString("numero_factura") + "/" + fileToZip.getName());
                    zipOutT.putNextEntry(zipEntry);

                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = fis.read(bytes)) >= 0) {
                        zipOutT.write(bytes, 0, length);
                    }
                    fis.close();
                } catch (Exception e) {
                    System.err.println("ERROR::");
                    e.printStackTrace();
                }
            }
        }
        zipOutT.close();
        fosT.close();

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=" + tabla + "_.zip");

        file = new File(ruta_archivo_tipificacion_comprimido);
        FileInputStream fileIn = new FileInputStream(file);
        ServletOutputStream out2 = response.getOutputStream();

        byte[] outputByte = new byte[4096];
        while (fileIn.read(outputByte, 0, 4096) != -1) {
            out2.write(outputByte, 0, 4096);
        }
        fileIn.close();
        out2.flush();
        out2.close();
    } catch (Exception e) {
        System.err.println("ERROR AL GENERAR LA TIPIFICACION:" + e.getMessage());
        e.printStackTrace();
    } finally {
        if (file != null) {
            file.delete();
        }
    }
%>
