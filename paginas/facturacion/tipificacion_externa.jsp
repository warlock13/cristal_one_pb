<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />

<%
    Integer procesos = (Integer) application.getAttribute("procesosTipificacionExterna");
%>

<table width="500" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="TIPIFICACION EXTERNA" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <div style="overflow:auto;height:350px; width:500px" id="divContenido">
                <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                    <tr>
                        <td>
                            <input type="button" value="Actualizar" class="small button blue" onclick="buscarFacturacion('tablaTipificacionExterna')">
                            <table width="100%" id="tablaTipificacionExterna"></table>
                        </td>
                    </tr>
                    <!--<%
                        Enumeration params = application.getAttributeNames();
                        while (params.hasMoreElements()) {
                            String atributo = (String) params.nextElement();
                            System.err.println(atributo);
                            if (atributo.contains("procesoTipificacionExterna_grupo")) {
                                String grupo = (String) application.getAttribute(atributo);
                    %>
                    <tr><td>

                            <%="Proceso grupo " + grupo%><%
                                    }
                                }
                            %>
                        </td>
                    </tr>-->
                    <tr>
                        <td>
                            <hr>
                            <input type="button" class="small button blue" value="Iniciar tipificacion" onclick="iniciarProcesoTipificacionExterna(10)">
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="txtPlanTipificacionExterna" type="text" placeholder="Ingrese el plan">
                            <input type="button" class="small button blue" value="Descargar tipificacion" onclick="crearArchivosTipificacionPacientesNefro()">
                            <hr>
                        </td>
                    </tr>
                </table>  
            </div>            
        </td>
    </tr>
</table>
