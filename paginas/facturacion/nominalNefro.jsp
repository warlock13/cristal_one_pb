<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<!--  -->
<table width="1300px" style="position: relative;" cellpadding="0" cellspacing="0" class="fondoTabla" >
    <tr> 
        <td class="w100">
            <!-- AQUI COMIENZA EL TITULO -->	
            <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="BASE NOMINAL NEFRO" />
                </jsp:include>
            </div>	
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%">
                <tr class="titulosListaEspera">
                    <td width="50%">FILTROS PARA BUSQUEDA</td>
                    <td width="50%">RESUMEN</td>
                </tr>
                <tr>
                    <td align="LEFT" valign="top">
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td width="15%">Seccional</td>
                                <td width="10%">Año</td>
                                <td width="15%">Mes</td>
                                <td width="15%">Regimen</td>
                                <td width="15%">Tipo de red</td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td align="center">
                                    <select style="width: 90%;" id="cmbSeccional">
                                        <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(3691);
                                            ComboVO cmbSeccional;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmbSeccional = (ComboVO) resultaux.get(k);
                                        %>
                                        <option value="<%= cmbSeccional.getId()%>" title="<%= cmbSeccional.getTitle()%>">
                                            <%= cmbSeccional.getDescripcion()%></option>
                                            <%}%> 
                                    </select>
                                </td> 
                                <td align="center">
                                    <select style="width: 90%;" id="cmbAnio">
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <select style="width: 90%;" id="cmbMesNefro">
                                        <option value="1">ENERO</option>
                                        <option value="2">FEBRERO</option>
                                        <option value="3">MARZO</option>
                                        <option value="4">ABRIL</option>
                                        <option value="5">MAYO</option>
                                        <option value="6">JUNIO</option>
                                        <option value="7">JULIO</option>
                                        <option value="8">AGOSTO</option>
                                        <option value="9">SEPTIEMBRE</option>
                                        <option value="10">OCTUBRE</option>
                                        <option value="11">NOVIEMBRE</option>
                                        <option value="12">DICIEMBRE</option>
                                    </select>
                                </td>
                                <td>  
                                    <select id="cmbTipoRegimen" style="width:90%;">
                                        <option value="C">CONTRIBUTIVO</option>
                                        <option value="S">SUBSIDIADO</option>
                                        <option value="OT">OTROS</option>           
                                    </select> 
                                </td>  
                                <td>  
                                    <select id="cmbTipoRed" style="width:90%;">
                                        <option value="0">RED MEDICRON</option>
                                        <option value="1">RED PÚBLICA</option>    
                                    </select> 
                                </td> 
                            </tr>
                        </table>  
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td>
                                    Paciente
                                </td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td align="center">
                                    <input style="width: 90%;" type="text" id="txtIdBusPaciente" title="Permite realizar la búsqueda por Identificación"/>
                                    <img  width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                                    <img onclick="limpiaAtributo('txtIdBusPaciente');" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">
                                </td>       
                                <div id="divParaVentanita"></div>  
                            </tr>
                        </table>
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td width="10%">Atendido</td>
                                <td width="10%">Estado Factura</td>
                                <td width="10%">Factura por base nominal</td>
                                <td width="10%">Estado de Tipificación</td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td align="center">
                                    <select style="width: 90%;" id="cmbAtendido">
                                        <option value="">[ TODO ]</option>
                                        <option value="SI">SI</option>
                                        <option value="NO">NO</option>
                                    </select>
                                </td>
                                <td align="center">
                                    <select style="width: 90%;" id="cmbEstadoFactura">
                                        <option value="">[ TODO ]</option>
                                        <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(3621);
                                            ComboVO cmbEstadoFactura;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmbEstadoFactura = (ComboVO) resultaux.get(k);
                                        %>
                                        <option value="<%= cmbEstadoFactura.getId()%>" title="<%= cmbEstadoFactura.getTitle()%>">
                                            <%= cmbEstadoFactura.getDescripcion()%></option>
                                            <%}%> 
                                    </select>
                                </td>
                                <td align="center">
                                    <select style="width: 90%;" id="cmbFacturadoBaseNom">
                                        <option value="">[ TODO ]</option>
                                        <option value="SI">SI</option>
                                        <option value="NO">NO</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="cmbTipificacion" style="width: 90%;">
                                        <option value="">[ TODO ]</option>
                                        <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(3631);
                                            ComboVO cmbTipi;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmbTipi = (ComboVO) resultaux.get(k);
                                        %>
                                        <option value="<%= cmbTipi.getId()%>" title="<%= cmbTipi.getTitle()%>">
                                            <%= cmbTipi.getDescripcion()%></option>
                                            <%}%> 
                                    </select>    
                                </td>               
                            </tr>
                        </table>
                        <table width="100%">
                            <tr>
                                <td width="10%">
                                    <table width="100%">
                                        <tr class="titulosListaEspera">
                                            <td>Estadio</td>
                                        </tr>
                                        <tr class="estiloImputListaEspera">
                                            <td align="LEFT">
                                                <table>
                                                    <tr class="estiloImputListaEspera">
                                                        <td align="LEFT">
                                                            <dl>
                                                                <dt><input type="checkbox" id="chkestadio" name="chkEstadoLE" value="1">1</dt>
                                                                <dt><input type="checkbox" id="chkestadio" name="chkEstadoLE" value="2">2</dt>
                                                                <dt><input type="checkbox" id="chkestadio" name="chkEstadoLE" value="3a">3a</dt>
                                                                <dt><input type="checkbox" id="chkestadio" name="chkEstadoLE" value="3b">3b</dt>
                                                                <dt><input type="checkbox" id="chkestadio" name="chkEstadoLE" value="4">4</dt>
                                                                <dt><input type="checkbox" id="chkestadio" name="chkEstadoLE" value="5">5</dt>
                                                            </dl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="90%" valign="top">
                                    <table width="100%">
                                        <tr class="titulosListaEspera">
                                            <td width="10%">Dx Diabetes</td>
                                            <td width="10%">Dx Hipertension (P)</td>
                                            <td width="10%">Dx Hipertension (S)</td>
                                            <td width="10%">Dx Otras Etiologias</td>
                                        </tr>
                                        <tr class="estiloImputListaEspera">             
                                            <td align="center">
                                                <select style="width: 90%;" id="cmbdxDiabetes">
                                                    <option value="">[ TODO ]</option>
                                                    <option value="1">SI</option>
                                                    <option value="0">NO</option>
                                                </select>
                                            </td>             
                                            <td align="center">
                                                <select style="width: 90%;" id="cmbdxHipertensionPrim">
                                                    <option value="">[ TODO ]</option>
                                                    <option value="1">SI</option>
                                                    <option value="0">NO</option>
                                                </select>
                                            </td>   
                                            <td align="center">
                                                <select style="width: 90%;" id="cmbdxHipertensionSec">
                                                    <option value="">[ TODO ]</option>
                                                    <option value="1">SI</option>
                                                    <option value="0">NO</option>
                                                </select>
                                            </td>    
                                            <td align="center">
                                                <select style="width: 90%;" id="cmbdxEtiologias">
                                                    <option value="">[ TODO ]</option>
                                                    <option value="1">SI</option>
                                                    <option value="0">NO</option>
                                                </select>
                                            </td>                                           
                                        </tr>
                                    </table>
                                    <table width="100%">
                                        <tr class="titulosListaEspera">
                                            <td>Paquete</td>
                                        </tr>
                                        <tr class="estiloImputListaEspera">
                                            <td>
                                                <select id="cmbPaquete" style="width: 95%;">
                                                    <option value="">[ TODO ]</option>
                                                    <% resultaux.clear();
                                                        resultaux = (ArrayList) beanAdmin.combo.cargar(224);
                                                        ComboVO cmbPaquete;
                                                        for (int k = 0; k < resultaux.size(); k++) {
                                                            cmbPaquete = (ComboVO) resultaux.get(k);
                                                    %>
                                                    <option value="<%= cmbPaquete.getId()%>" title="<%= cmbPaquete.getTitle()%>">
                                                        <%= cmbPaquete.getDescripcion()%></option>
                                                        <%}%> 
                                                </select>                                    
                                            </td>
                                        </tr>
                                    </table> 
                                    <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla" style="border:0px !important;">
                                        <tr align="center">                    
                                            <td>
                                                <table style="width: 100%;">
                                                    <tr class="estiloImputListaEspera">
                                                        <td>
                                                            <input type="button" class="small button blue" value="GENERAR BASE NOMINAL" onclick="buscarHC('traerBaseNefro')">
                                                            <input type="button" class="small button blue" value="LIMPIAR" onclick="">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>                        
                                        </tr> 
                                    </table> 
                                </td>
                            </tr>
                        </table>  
                                               
                    </td>
                    <td valign="top">
                        <table width="100%">
                <tr>
                    <td width="60%" valign="top">
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td width="50%">Atendidos</td>
                                <td width="50%">Estado Facturacion</td>
                            </tr>
                            <tr>
                                <td class="estiloImputListaEspera"><label id="lblCantidadPacientesAtendidos"></label></td>
                                <td>
                                    <table width="100%">
                                        <tr class="estiloImputIzq2">
                                            <td>Sin factura</td>
                                            <td><label id="lblCantidadSinFacturaAtendidos">0</label></td>
                                        </tr>
                                        <tr class="estiloImputIzq2">
                                            <td>Pendiente factura</td>
                                            <td><label id="lblCantidadPendienteFacturaAtendidos">0</label></td>
                                        </tr>
                                        <tr class="estiloImputIzq2">
                                            <td>Facturado</td>
                                            <td><label id="lblCantidadFacturadosAtendidos">0</label></td>
                                        </tr>
                                        <tr class="estiloImputIzq2">
                                            <td>Error</td>
                                            <td><label id="lblCantidadErrorAtendidos">0</label></td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr>
                        </table>
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td width="50%">No Atendidos</td>
                                <td width="50%">Estado Facturacion</td>
                            </tr>
                            <tr>
                                <td class="estiloImputListaEspera"><label id="lblCantidadPacientesNoAtendidos"></label></td>
                                <td>
                                    <table width="100%">
                                        <tr class="estiloImputIzq2">
                                            <td>Sin factura</td>
                                            <td><label id="lblCantidadSinFacturaNoAtendidos">0</label></td>
                                        </tr>
                                        <tr class="estiloImputIzq2">
                                            <td>Pendiente factura</td>
                                            <td><label id="lblCantidadPendienteFacturaNoAtendidos">0</label></td>
                                        </tr>
                                        <tr class="estiloImputIzq2">
                                            <td>Facturado</td>
                                            <td><label id="lblCantidadFacturadosNoAtendidos">0</label></td>
                                        </tr>
                                        <tr class="estiloImputIzq2">
                                            <td>Error</td>
                                            <td><label id="lblCantidadErrorNoAtendidos">0</label></td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr>
                        </table>
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td width="50%">Novedades</td>
                                <td>No aplica a programa</td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td align="center">
                                    <h2><u><label id="lblcantidadNovedades">0</label></u></h2>
                                </td>
                                <td align="center">
                                    <h2><u><label id="lblcantidadNoAplica">0</label></u></h2>
                                </td>
                            </tr>
                        </table>
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td colspan="2">Paquetes</td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td width="80%">Q0000395-PAQUETE HIPERTENSION ARTERIAL Y O  DIABETES MELLITUS CON ENFERMEDAD RENAL CRONICA ESTADIOS 3A - 3B - 4 Y 5 SIN DIALISIS</td>
                                <td width="20%" align="center"><label id="lblPaquete1">0</label></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td width="80%">Q0000773-PAQUETE HIPERTENSION ARTERIAL SIN O CON ENFERMEDAD RENAL CRONICA ESTADIOS 1 - 2 - IPS ESPECIALIZADA</td>
                                <td width="20%" align="center"><label id="lblPaquete2">0</label></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td width="80%">Q0000774-PAQUETE DIABETES MELLITUS SIN O CON ENFERMEDAD RENAL CRONICA ESTADIOS 1 - 2 IPS ESPECIALIZADA</td>
                                <td width="20%" align="center"><label id="lblPaquete3">0</label></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td width="80%">Q0000775-PAQUETE ENFERMEDAD RENAL CRONICA ESTADIOS 1 - 2 ( HIPERTENSION ARTERIAL - DIABETES MELLITUS )</td>
                                <td width="20%" align="center"><label id="lblPaquete4">0</label></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                                <td width="80%">Q0001231-PAQUETE ENFERMEDAD RENAL CRONICA ESTADIOS 1 A 5 - OTRAS ETIOLOGIAS DIFERENTES A HIPERTENSION Y DIABETES</td>
                                <td width="20%" align="center"><label id="lblPaquete5">0</label></td>
                            </tr>
                        </table>
                    </td>
                    <td width="40%" valign="top">
                        <table width="100%">
                            <tr class="titulosListaEspera">
                                <td>Total resultados</td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td>
                                    <label id="lblTotalResultados" style="width: 90%;" class="bloque">0</label>
                                </td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td align="left">
                                    <input style="margin: 2px 2px 5px 0px;" type="button" class="small button blue" value="CREAR FACTURAS BASE NOMINAL" onclick="abrirVentanaFacBaseNom()">
                                </td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td align="left">
                                    <input style="margin: 2px 2px 5px 0px;" type="button" class="small button blue" value="NUMERAR FACTURAS CREADAS POR BASE NOMINAL" onclick="numeraFacEnBloque()"> 
                                </td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td align="left">
                                    <input style="margin: 2px 2px 5px 0px;" type="button" class="small button blue" value="REVERTIR FACTURACIÓN" onclick="revertirFacturasBaseNominal()"> 
                                </td>
                            </tr>
                            <tr class="estiloImputListaEspera">
                                <td align="left">
                                    <input style="margin: 2px 2px 5px 0px;" id="tipificarFacturasBaseNominal" type="button" class="small button blue" value="TIPIFICAR FACTURAS" onclick="tipificarFacturasBaseNom()" />
                                </td>
                            </tr>   
                            <tr class="estiloImputListaEspera">
                                <td align="left">
                                    <input style="margin: 2px 2px 5px 0px;" type="button" class="small button blue" value="EXPORTAR CSV" onclick="exportarBaseNominalNefroproteccion()" />
                                </td>
                            </tr>                               
                        </table>
                    </td>
                </tr>
            </table>
                    </td>
                </tr>
            </table>
            <table id="listGrillaNominalNefro" class="scroll" width="100%"></table>
        </td>
    </tr>
</table>

<div id="divVentanitaFacBaseNom"  style="display:none; z-index:2000; top:1px; width:800px; left:150px;" style="border:0px !important;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);" style="border:0px !important;">
        <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">  
            <table width="100%" class="fondoTabla" style="border:0px !important;">
                <tr class="estiloImput">
                    <td align="left" width="50%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaFacBaseNom')"/></td>
                </tr>
                <table style="width:100%; border:0px !important;" class="fondoTabla">
                    <tr class="titulosListaEspera">
                        <td width="10%">Administradora Factura <label style="color: red;">(*)</label></td> 
                        <td width="10%">Sede <label style="color: red;">(*)</label></td>            
                        <td width="10%">Tipo Régimen - Plan <label style="color: red;">(*)</label></td>  
                        <td width="10%">Plan Contratación <label style="color: red;">(*)</label></td>  
                        <td width="10%">Tipo Afiliado <label style="color: red;">(*)</label></td>
                        <td width="10%">Rango <label style="color: red;">(*)</label></td>
                    </tr>
                    <tr class="estiloImputListaEspera">
                        <td align="center">
                            <input type="text" value="1-1 EMSSANAR SAS" id="txtAdministradora1" style="width: 80%;" maxlength="50" onfocus="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')" onkeypress="llamarAutocompletarEmpresa('txtAdministradora1', 184)"/>
                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem" onclick="traerVentanitaEmpresa(this.id, 'divParaVentanita', 'txtAdministradora1', '24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        </td>
                        <td align="center">
                            <select style="width: 90%;" id="cmbSede" onfocus="cargarComboGRALCondicion1('', '', this.id, 240, valorAtributo('cmbSeccional'))"></select>
                            <!--<select style="width: 90%;" id="cmbSede">
                            <% resultaux.clear();
                                resultaux = (ArrayList) beanAdmin.combo.cargar(963);
                                ComboVO cmbSede;
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmbSede = (ComboVO) resultaux.get(k);
                            %>
                        <option value="<%= cmbSede.getId()%>" title="<%= cmbSede.getTitle()%>">
                            <%= cmbSede.getDescripcion()%></option>
                            <%}%> 
                        </select>-->
                        </td>
                        <td align="center">
                            <select id="cmbIdTipoRegimen" style="width: 90%;" 
                                    onfocus="cargarComboGRALCondicion2('', '', this.id, 241, valorAtributoIdAutoCompletar('txtAdministradora1'), valorAtributo('cmbSede'))" 
                                    onchange="limpiaAtributo('cmbIdPlan', 0); limpiaAtributo('cmbIdTipoAfiliado', 0); limpiaAtributo('cmbIdRango', 0);">
                                <option value=""></option>
                            </select>
                        </td>
                        <td align="center">
                            <select id="cmbIdPlan" style="width: 90%;" 
                                    onfocus="cargarComboGRALCondicion4('', '', this.id, 242, valorAtributoIdAutoCompletar('txtAdministradora1'), valorAtributo('cmbSede'), valorAtributo('cmbIdTipoRegimen'), valorAtributo('cmbIdTipoRegimen'))"
                                    onchange="limpiaAtributo('cmbIdTipoAfiliado', 0); limpiaAtributo('cmbIdRango', 0);">
                                <option value=""></option>
                            </select>
                        </td>
                        <td align="center">
                            <select id="cmbIdTipoAfiliado" style="width: 90%;" 
                                    onfocus="comboDependiente('cmbIdTipoAfiliado', 'cmbIdPlan', '30')"
                                    onchange="limpiaAtributo('cmbIdRango', 0);">
                                <option value=""></option>
                            </select>
                        </td>
                        <td align="center">
                            <select id="cmbIdRango"  style="width: 90%;" onfocus="comboDependienteDosCondiciones('cmbIdRango', 'cmbIdPlan', 'cmbIdTipoAfiliado', '31')">
                                <option value=""></option>
                            </select>
                        </td>
                    </tr>
                </table>

                <table style="width:100%; border:0px !important;" class="fondoTabla">
                    <tr class="titulosListaEspera">
                        <td width="10%">Diagnóstico <label style="color: red;">(*)</label></td>  
                        <td width="10%">Profesional Factura</td>  
                        <td width="10%">Número de Autorización</td>
                    </tr>
                    <tr class="estiloImputListaEspera">
                        <td>
                            <input type="text" id="txtIdDx" oninput="llenarElementosAutoCompletarKey(this.id, 206, this.value)"
                                    style="width: 90%;">
                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
                                 onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdDx', '5')"
                                 src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        </td>
                        <td>
                            <select id="cmbIdProfesionalesFactura" style="width: 90%;" align='right' onfocus="comboDependienteEmpresa('cmbIdProfesionalesFactura', '86')" >
                                <option value=""></option>
                            </select>
                        </td>
                        <td>
                            <input type="number" id="txtNoAutorizacion" style="width: 90%;" onKeyPress="javascript:return teclearExcluirCaracter(event, '%');" onblur="$('#txtNoAutorizacionProcedimiento').val($('#' + this.id).val())"  />
                        </td>
                    </tr>
                </table>
                <table width="100%"  cellpadding="0" class="fondoTabla" cellspacing="0" style="border:0px !important;">
                    <tr>
                        <td style="border:#e8e8e8;" class="tdTitulo">
                            <p class="pTitulo">PROCEDIMIENTOS</p>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla" style="border:0px !important;">
                    <tr class="titulosListaEspera">
                        <td width="25%">Procedimientos</td>  
                        <td width="5%">Valor Unitario</td>  
                        <td width="5%">Cantidad</td>
                        <td width="5%">Número De<br>Autorización</td>
                        <td width="10%">Ejecutado</td>
                        <td width="10%"></td>
                    </tr>
                    <tr class="estiloImputListaEspera">
                        <td>
                            <select id="cmbIdProcedimientoFacBN" style="width: 95%;">
                                <option value=""></option>
                                <% resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(224);
                                    ComboVO cmbProcedimientoCex;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbProcedimientoCex = (ComboVO) resultaux.get(k);
                                %>
                                <option value="<%= cmbProcedimientoCex.getId()%>" title="<%= cmbProcedimientoCex.getTitle()%>">
                                    <%= cmbProcedimientoCex.getDescripcion()%></option>
                                    <%}%> 
                            </select>
                        </td>
                        <td>
                            <label id="lblClasifProced"></label>
                            <label id="lblValorUnitarioProc"></label>
                        </td>           
                        <td>
                            <input type="text" id="cmbCantidad" style="width: 90%;" onKeyPress="javascript:return teclearsoloDigitos(event);"  style="width:80%"  value="1" disabled/>    
                        </td>
                        <td>
                            <input type="text" id="txtNoAutorizacionProcedimiento" style="width: 90%;" onKeyPress="javascript:return teclearsoloDigitos(event);"  style="width:80%"  disabled/>
                        </td>
                        <td>
                            <select id="cmbEjecutado" style="width:50%"  >
                                <option value="1">SI</option>
                                <option value="2">NO</option>
                            </select>                                  
                        </td>               
                        <td>                     
                            <input id="btnAdicionarFactura" type="button" class="small button blue" value="ADICIONAR" title="BTJJ52" onclick="modificarCRUD('listProcedimientosFacBloque');"   />
                        </td>
                    </tr>
                </table>
                <table id="listProcedimientosFacBloque" style="width:100%" class="scroll"></table>
                <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla" style="border:0px !important;">
                    <tr align="center">                    
                        <td>
                            <input id="btnCrearFacturasBaseNominal" type="button" class="small button blue" value="CREAR FACTURAS" onclick="crearFacBloque('crearFacturasBaseNominalNefro')">
                        </td>                        
                    </tr> 
                </table>
            </table>
            <label id="lblIdPlanContratacion" hidden></label>
        </div> 
    </div>    
</div>

<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:164px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divAcordionAdjuntos" style="display:BLOCK">      
                        <div id="divParaArchivosAdjuntos"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>

<div  id="loaderBaseNom" align="center" title="MENSAJE" style="z-index:3000; top:0px; left:0px; position:fixed; display:none;" >
    <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
    </div> 
    <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%;">     
        <table bgcolor="#FFFFFF" align="center"  width="300px" height="100px" id="tablaNotificacionesAlert" style="border: 1px solid #000;" >
            <tr>
                <td colspan="2" bgcolor="#7CD9FC" align="center" background="/clinica/utilidades/imagenes/menu/six_0_.gif" >
                    <b>ALERTA</b>
                </td>      
            </tr>	   
            <tr>
                <td>
                    <i><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="100px" height="100px"></i>
                </td>
                <td>
                    <i><label style="font-family: Helvetica; text-align: justify" id="lblTotNotifVentanillaAlert"></label></i>
                </td>      
            </tr>
            <tr>
                <td align="center">  
                    <TABLE id="idTableBotonNotificaAlert" width="50%" border="1">
                    </TABLE>
                </td>        	               
            </tr>            
        </table>
        <br/>
    </div>   
</div>