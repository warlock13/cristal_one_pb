<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn());
ArrayList resultaux = new ArrayList();
%>

<table width="1320px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="FACTURAS" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:1000; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <table width="100%" align="center">
                                <tr class="titulosListaEspera" align="center">
                                    <td width="15%">ESTADO FACTURA</td>
                                    <td width="15%">ESTADO TIPIFICACION</td>
                                    <td width="10%">FECHA CREA DESDE</td>
                                    <td width="10%">FECHA HASTA</td>
                                    <td width="10%">NUMERO FACTURA</td>
                                    <td width="15%">TIPO PLAN</td>
                                    <td width="25%">ESTADO FE</td>
                                </tr>
                                <tr class="estiloImputListaEspera">
                                    <td>
                                        <select id="cmbIdEstadoFactura" style="width:80%" >
                                            <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(558);
                                            ComboVO cmbF;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                            cmbF = (ComboVO) resultaux.get(k);
                                            %>
                                            <option value="<%= cmbF.getId()%>" title="<%= cmbF.getTitle()%>">
                                                <%= cmbF.getDescripcion()%></option>
                                            <%}%>                                 
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbIdEstadoTipificacion" style="width:80%" >
                                            <option value="">[ TODO ]</option>
                                            <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(3631);
                                            for (int k = 0; k < resultaux.size(); k++) {
                                            cmbF = (ComboVO) resultaux.get(k);
                                            %>
                                            <option value="<%= cmbF.getId()%>" title="<%= cmbF.getTitle()%>">
                                                <%= cmbF.getDescripcion()%></option>
                                            <%}%>                                 
                                        </select>
                                    </td>
                                    <td><input type="text" id="txtFechaDesdeFactura"  style="width:80%"/></td>
                                    <td><input type="text" id="txtFechaHastaFactura"  style="width:80%"/></td>
                                    <td><input type="text" id="txtBusNumFactura" style="width:80%"/></td>
                                    <td>
                                        <select id="cmbIdTipoPlan" style="width:80%">
                                            <option value="">[ TODO ]</option>
                                            <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(21);
                                            for (int k = 0; k < resultaux.size(); k++) {
                                            cmbF = (ComboVO) resultaux.get(k);
                                            %>
                                            <option value="<%= cmbF.getId()%>" title="<%= cmbF.getTitle()%>">
                                                <%= cmbF.getDescripcion()%></option>
                                            <%}%>                                 
                                        </select>
                                    </td> 
                                    <td>
                                        <select id="cmbEstadoFE" style="width:90%">
                                            <option value="">TODO</option>
                                            <% resultaux.clear();
                                                resultaux = (ArrayList) beanAdmin.combo.cargar(629);
                                                ComboVO cmbFac;
                                                for (int k = 0; k < resultaux.size(); k++) {
                                                    cmbFac = (ComboVO) resultaux.get(k);
                                            %>
                                            <option value="<%= cmbFac.getId()%>" title="<%= cmbFac.getTitle()%>">
                                                <%= cmbFac.getDescripcion()%></option>
                                                <%}%>  
                                        </select>
                                    </td> 
                                    <!--<td>
                                        <select size="1" id="cmbIdPlan" style="width:80%"  onfocus="comboDependienteEmpresa('cmbIdPlan', '78')">
                                            <option value=""></option>

                                        </select>  
                                    </td>-->                           
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <table width="100%">
                                            <tr class="titulosListaEspera" align="center">
                                                <td width="25%">SEDE</td>
                                                <td width="20%">FACTURADORES</td>
                                                <td width="20%">PROCEDIMIENTOS</td>
                                                <td width="25%">PACIENTE</td>
                                                <td width="10%"></td>
                                            </tr> 
                                            <tr class="estiloImputListaEspera">
                                                <td>
                                                    <select id="cmbSedeFactura" style="width:90%" onfocus="cargarComboGRALCondicion1('', '', this.id, 327, IdSesion())">
                                                        <option value="">[ SELECCIONE ]</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbIdFacturador" style="width:90%" onfocus="cargarComboGRALCondicion2('', '', this.id, 328, valorAtributo('cmbSedeFactura'), valorAtributo('cmbSedeFactura'))">
                                                        <option value="">[ SELECCIONE ]</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIdProcedimiento" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento', 300)" style="width:90%"/>
                                                </td>
                                                <td><input type="text" size="70" maxlength="70" style="width:90%" id="txtIdBusPaciente"  />
                                                    <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '7')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                    <div id="divParaVentanita"></div>
                                                </td>
                                                <td>
                                                    <input title="BF932" type="button" class="small button blue" value="BUSCAR" onclick="buscarFacturacion('listGrillaFacturas')" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <div style="height: 405px;">
                                            <table id="listGrillaFacturas" class="scroll" width="100%"></table>
                                        </div>
                                        <div class="contextMenu" id="contextMenu" style="display:none; width:400px;">
                                            <ul style="width: 400px; font-size: 65%;">
                                                <li id="exportar">
                                                    <span class="ui-icon ui-icon-plus" style="float:left"></span>
                                                    <span style="font-size:130%; font-family:Verdana">Exportar</span>
                                                </li>
                                                <li id="numerar">
                                                    <span class="ui-icon ui-icon-plus" style="float:left"></span>
                                                    <span style="font-size:130%; font-family:Verdana">Numerar</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr class="titulosCentrados">
                                    <td colspan="2">INFORMACION FACTURA
                                    </td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td width="30%">Id Factura:</td>
                                    <td width="70%"><label id="lblIdFactura"></label></td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td width="30%">Id Admision:</td>
                                    <td width="70%"><label id="lblIdAdmision"></label></td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td width="30%">Numero Factura:</td>
                                    <td width="70%"><label id="lblNumeroFactura"></label></td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td width="30%">Estado Factura:</td>
                                    <td width="70%"><label id="lblIdEstadoFactura"></label> - <label id="lblEstadoFactura"></label> </td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td width="30%">Total factura:</td>
                                    <td width="70%"><label id="lblValorTotalFactura"></label></td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td>Paciente:</td>
                                    <td width="70%"><label id="lblIdPaciente"></label>-<label id="lblIdentificacion"></label>-<label id="lblNomPaciente"></label></td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td>Usuario Elaboro:</td>
                                    <td>
                                        <label id="lblIdUsuario"></label>-<label id="lblNomUsuario"></label>
                                    </td>
                                </tr>
                                <label hidden id="lblIdEstadoFe"></label>
                                <% if (beanSession.usuario.preguntarMenu("FAC08")){%>
                                <tr class="estiloImputIzq2">
                                    <td>Motivos para anular:</td>
                                    <td>
                                        <select size="1" id="cmbIdMotivoAnulacion" style="width:40%" >
                                            <option value=""></option>
                                            <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(141);
                                            ComboVO cmb3;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                            cmb3 = (ComboVO) resultaux.get(k);
                                            %>
                                            <option value="<%= cmb3.getId()%>" title="<%= cmb3.getTitle()%>">
                                                <%= cmb3.getDescripcion()%></option>
                                            <%}%>                                 
                                        </select> 
                                        <input title="BF77" type="button" value="ANULAR FACTURA" onclick="verificarFacturaAnular('soloAnularFactura')"/>
                                    </td>
                                </tr>
                                <tr class="estiloImputIzq2">
                                    <td>Motivos para abrir:</td>
                                    <td>
                                        <select size="1" id="cmbIdMotivoAbrir" style="width:40%" >	                                        
                                            <option value=""></option>
                                            <% resultaux.clear();
                                                resultaux = (ArrayList) beanAdmin.combo.cargar(559);
                                                ComboVO cmb4;
                                                for (int k = 0; k < resultaux.size(); k++) {
                                                    cmb4 = (ComboVO) resultaux.get(k);
                                            %>
                                            <option value="<%= cmb4.getId()%>" title="<%= cmb4.getTitle()%>">
                                                <%= cmb4.getDescripcion()%></option>
                                                <%}%>                                 
                                        </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input title="BF79" type="button" value="ABRIR FACTURA" onclick="modificarCRUD('abrirFactura')" />

                                        <input title="BF80" type="button" value="CERRAR FACTURA" onclick="modificarCRUD('cerrarFactura')" />
                                    </td>
                                </tr>
                                <%}%>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" style="cursor:pointer">
                                            <tr>
                                                <td width="99%" class="titulos">
                                                    <input title="BF73" type="button" class="small button blue"
                                                           value="IMPRIMIR"
                                                           onclick="formatoPDFFactura(valorAtributo('lblIdFactura'));" />
                                                    <input title="BF73" type="button" class="small button blue"
                                                           value="IMPRIMIR VARIAS FACTURAS"
                                                           onclick="imprimirFacturasPDF()"/>
                                                    <input title="BF73" id="btnTipificacion" type="button"
                                                           class="small button blue" value="TIPIFICAR FACTURAS"
                                                           onclick="tipificarFacturas(obtenerListaFacturasTabla('listGrillaFacturas'))" />
                                                    <div id="loaderTipificacion" hidden>
                                                        <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt=""
                                                             width="20px" height="20px">
                                                        Tipificando facturas <label id="progreso">0</label>%
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr width="99%" class="titulos">
                                                <td>
                                                    <input type="button" class="small button blue" value="AUDITAR CONTABILIDAD" onclick="guardarYtraerDatoAlListado('auditarFactura')"/>
                                                    <input type="button" class="small button blue" value="AUDITAR VARIAS FACTURAS CONTABILIDAD" onclick="auditarFacturas(listaFacturas())"/>

                                                    <input type="button" class="small button blue" value="AUDITAR FACTURACION ELECTRONICA"
                                                        onclick="guardarYtraerDatoAlListado('auditarFE')"/>
                                                    <input type="button" class="small button blue" value="AUDITAR NOTA CREDITO FE"
                                                        onclick="guardarYtraerDatoAlListado('auditarNotaCreditoFE')"/> 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="5">NOTAS CRÉDITO/DÉBITO
                                    </td>
                                </tr>
                                <tr class="titulosListaEspera">
                                    <td style="width: 25%;">Tipo de documento</td>       
                                    <td style="width: 25%;">Tipo de nota</td> 
                                    <td style="width: 25%;">Observaciones</td> 
                                    <td style="width: 10%;">Valor</td> 
                                    <td></td>                    
                                </tr>
                                <tr class="estiloImputListaEspera">
                                    <td>
                                        <select id="cmbTipoDocumento" style="width: 95%;" onchange="limpiaAtributo('cmbTipoNota', 0)">
                                            <option value="">[ SELECCIONE ]</option>
                                            <% resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(10392);
                                            ComboVO cmb4;
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmb4 = (ComboVO) resultaux.get(k);
                                                %>
                                                <option value="<%= cmb4.getId()%>" title="<%= cmb4.getTitle()%>"><%= cmb4.getDescripcion()%></option>
                                                <%}%> 
                                        </select>
                                    </td>       
                                    <td>
                                        <select id="cmbTipoNota" style="width: 95%;" onfocus="cargarComboGRALCondicion1('', '', this.id, 10393, valorAtributo('cmbTipoDocumento'))">
                                            <option value="">[ SELECCIONE ]</option>
                                        </select>
                                    </td> 
                                    <td>
                                        <textarea id="txtObservacionesNota" rows="1" placeholder="Observaciones" style="width: 95%;"></textarea>
                                    </td>
                                    <td>
                                        <input id="txtValorNota" type="number" style="width: 95%;">
                                    </td>   
                                    <td>
                                        <input type="button" class="small button blue" value="CREAR NOTA" onclick="modificarCRUD('crearNotaFactura')">
                                    </td>  
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <table id="listaNotasFactura" style="width:100%" class="scroll">
                                        </table>
                                    </td>                                   
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3">EDITAR FACTURA
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr class="titulosListaEspera">
                                                            <td>Tipo documento</td>
                                                            <td>Documento</td>
                                                            <td>N&uacutemero de autorizaci&oacuten</td>
                                                            <td>Dx ingreso</td>
                                                            <td>Fecha de ingreso</td>
                                                            <td>Fecha de egreso</td>
                                                            <!--<td>Profesional factura</td>
                                                            <td>Fecha de Nacimiento</td>
                                                            <td>Municipio</td>-->
                                                        </tr>
                                                        <tr class="estiloImputListaEspera">
                                                            <td>
                                                                <select id="cmbTipoId" class="w90" >
                                                                    <option value=""></option>
                                                                    <option value="CC">Cedula de Ciudadania</option>
                                                                    <option value="CE">Cedula de Extranjeria</option>
                                                                    <option value="MS">Menor sin Identificar</option>
                                                                    <option value="PA">Pasaporte</option>
                                                                    <option value="RC">Registro Civil</option>
                                                                    <option value="TI">Tarjeta de identidad</option>
                                                                    <option value="CD">Carnet Diplomatico</option>
                                                                    <option value="NV">Certificado nacido vivo</option>
                                                                    <option value="AS">Adulto sin Identificar</option>
                                                                </select>
                                                            </td>
                                                            <td><input id="txtDocumento" type="text" 
                                                                       readonly />
                                                            <td><input id="txtNoAutorizacion" type="text"
                                                                        />
                                                            </td>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <input type="text" id="txtDxIngreso" style="width: 100%;" readonly/>
                                                                        </td>
                                                                        <td>
                                                                            <img width="18px" height="18px"
                                                                                 align="middle" title="VEN52"
                                                                                 id="idLupitaVentanitaDx"
                                                                                 onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtDxIngreso', '5')"
                                                                                 src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                                            <div id="divParaVentanita"></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtFechaIngreso"/>
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtFechaEgreso"/>
                                                            </td>
                                                            <td hidden>
                                                                <select size="1" id="cmbIdProfesionalesFactura"
                                                                        onfocus="comboDependienteEmpresa('cmbIdProfesionalesFactura', '86')"
                                                                        style="width:100%">
                                                                    <option value=""></option>

                                                                </select>
                                                            </td>
                                                        <input type="hidden" id="txtIdPaciente"/>
                                                        <td><input id="txtNacimiento" type="text" hidden/>
                                                        </td>
                                                        <td align="CENTER" hidden>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <input type="text" id="txtMunicipio"
                                                                                read />
                                                                    </td>
                                                                    <td>
                                                                        <img width="18px" height="18px"
                                                                             align="middle" title="VEN52"
                                                                             id="idLupitaVentanitaMunicT"
                                                                             onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')"
                                                                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                                        <div id="divParaVentanita"></div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                            <table width="100%" style="cursor:pointer">
                                <tr>
                                    <td width="99%" class="titulos">
                                        <input title="BF77" type="button" class="small button blue"
                                               value="MODIFICAR" onclick="modificarCRUD('modificarFactura')" />
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulosCentrados">
                    <td colspan="3">PROCEDIMIENTOS DE FACTURA
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="listProcedimientosDeFactura" style="width:100%" class="scroll">
                        </table>
                    </td>                                   
                </tr>
            </table>
            <div id="divOrdenesFacturaDetalle"  style="display:none; z-index:2000; top:500px; left:50px;" hidden>
                <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                </div>  
                <div  style="z-index:2004; position:fixed; top:400px; left:200px; width:100%">  
                    <table width="50%" height="80"  cellpadding="0" cellspacing="0" class="fondoTabla" >
                        <tr class="estiloImput">
                            <td width="20%" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divOrdenesFacturaDetalle')" /></td>  
                            <td width="60%">&nbsp;</td>                  
                            <td width="20%" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divOrdenesFacturaDetalle')" /></td>  
                        </tr> 
                        <tr class="titulosCentrados">
                            <td colspan="3">ORDENES FACTURA</td>   
                        </tr>
                        <tr>            
                            <td td colspan="3">
                                <table id="listProgramacionFacturaDetalle" class="scroll"></table> 
                            </td>
                        </tr>
                        <!--<tr class="titulosListaEspera">
                            <td colspan="3">
                                <input type="button" class="small button blue" value="ELIMINAR PROGRAMACION" onclick="modificarCRUD('eliminaOrdenProgramacionFactura');"/>  
                            </td>
                        </tr> -->  
                    </table>  
                </div>    
            </div> 
            <!--<table width="100%" cellpadding="0" cellspacing="0" align="center">
                    <tr class="titulosListaEspera">
                        <td>Numeración Factura:</td>
                        <td>Valor No Cubierto:</td>
                        <td>Valor Descuento:</td>
                        <td></td>
        
                    </tr>
                    <tr class="estiloImputListaEspera">
                        <td>
                            <select id="cmbNumeracionFactura" style="width:60%">
                                <option value=""></option>
                                <% resultaux.clear();
                                        resultaux = (ArrayList) beanAdmin.combo.cargar(89);
                                        ComboVO cmbNF;
                                        for (int k = 0; k < resultaux.size(); k++) {
                                            cmbNF = (ComboVO) resultaux.get(k);
                                    %>
                                <option value="<%= cmbNF.getId()%>" title="<%= cmbNF.getTitle()%>">
                                    <%= cmbNF.getDescripcion()%></option><%}%>                                 
                                </select>
                            </td>
                            <td><input id="txtValorNoCubierto" type="text" width="80%" /></td>
                            <td><input id="txtValorDescuento" type="text" width="80%" /></td>
                            <td>
                                <input type="button" class="small button blue" value="MODIFICAR PGP" onclick="modificarCRUD('modificarFacturaPgp')" />
                            </td>
                    </tr>
                </table>-->
            </div>
        </td>
    </tr>
</table>
</td>
</tr>
</table>

<input type="hidden" id="lblIdDoc" />
<input type="hidden" id="lblIdEstadoAuditoria" />
<input type="hidden" id="txtNoDevolucion" />

<div id="divVentanitaProcedimCuenta" style="display:none; z-index:2000; top:400px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2004; position:fixed; top:300px;left:400px; width:70%">
        <table width="80%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                        onclick="ocultar('divVentanitaProcedimCuenta')" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="5">&nbsp;</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                         onclick="ocultar('divVentanitaProcedimCuenta')" /></td>
            </tr>
            <tr class="titulos">
                <td>TRAN.</td>
                <td>PROCEDIMIENTO</td>
                <td> NOMBRE PROCEDIMIENTO</td>
                <td>N&Uacute;MERO DE AUTORIZACION&Oacute;N</td>
                <td>PROFESIONAL</td>
                <td>EJECUTADO</td>
            </tr>
            <tr class="estiloImput">
                <td><label id="lblIdTransac"></label></td>
                <td><label id="lblIdProced"></label></td>
                <td><label id="lblNomProced"></label></td>
                <td>
                    <input type="text" id="txtNoAutorizacionProcedimientoEditar" style="width:80%"  />
                </td>
                <td>
                    <select size="1" id="cmbIdProfesionalesFacturaEditar"
                            onfocus="comboDependienteEmpresa('cmbIdProfesionalesFacturaEditar', '86')" style="width:100%">
                        <option value=""></option>

                    </select>
                </td>

                <td>
                    <select id="cmbEjecutadoEditar" style="width:80%" >
                        <option value="2">NO</option>
                        <option value="1">SI</option>
                        <option value="3">PARCIAL</option>
                    </select>
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td colspan="9">
                    <input id="BTN_MO" title="BI101" type="button" class="small button blue"
                           value="MODIFICAR PROCEDIMIENTO"
                           onclick="modificarCRUD('modificarProcedimientoCuenta', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                            />
                    <input id="BTN_MO" title="BI102" type="button" class="small button blue"
                           value="ELIMINAR PROCEDIMIENTO"
                           onclick="modificarCRUD('eliminarProcedimientoCuenta', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                            />
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:50%; width:50%;margin-left: -25%;">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR"
                           onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR"
                           onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divAcordionAdjuntos" style="display:BLOCK">
                        <div id="divParaArchivosAdjuntos"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>