<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />


<%  beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="RECIBOS" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>

                        <div style="overflow:auto;height:400px; width:1050px" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                    <tr class="titulos" align="center">
                                        <td width="15%">NUMERO RECIBO</td>
                                        <td width="15%">FECHA</td>
                                        <td width="30%">PLAN</td>
                                        <td width="30%">PACIENTE</td>
                                        <td width="10%">&nbsp;</td>
                                    </tr>
                                    <tr class="estiloImput">
                                        <td><input type="text" id="txtBusNumRecibo" style="width:50%"  />
                                        </td>

                                        <td>
                                            <input type="text" id="txtFechaRecibo" style="width:50%"  />
                                        </td>

                                        <td>

                                            <select size="1" id="cmbIdPLan" style="width:80%"  onfocus="comboDependienteEmpresa('cmbIdPLan','78')">
                                                <option value=""></option>
                                                                              
                                            </select>

                                        </td>

                                        <td><input type="text" size="70" maxlength="70" style="width:90%" id="txtIdBusPaciente"  />
                                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '7')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                            <div id="divParaVentanita"></div>
                                        </td>
                                        <td>
                                            <input title="BF934" type="button" class="small button blue" value="BUSCAR" onclick="buscarFacturacion('listGrillaRecibos')" />
                                        </td>
                                    </tr>
                                </table>
                                <table id="listGrillaRecibos" class="scroll"></table>
                            </div>
                        </div>
                        <!-- div contenido-->

                        <div id="divEditar" style="display:block; width:100%">

                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3">INFORMACION RECIBO CREAR
                                    </td>
                                </tr>
                                <tr>
                                    <td>



                                        <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Id Recibo:</td>
                                                            <td width="70%"><label id="lblIdRecibo"></label> &nbsp;&nbsp;
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Id Consecutivo:</td>
                                                            <td width="70%"><label id="lblIdReciboConsecutivo"></label> &nbsp;&nbsp;
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Paciente:</td>
                                                            <td><input type="text" size="70" maxlength="70" class="w60" id="txtIdEditPacienteRecibo"  />
                                                                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenEditT" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdEditPacienteRecibo', '7')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                                <div id="divParaVentanita"></div>
                                                            </td>
                                                        </tr>

                                                        <tr class="estiloImputIzq2">

            <td>Administradora</td>
            <td>
                <input type="text" id="txtAdministradora1" onfocus="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')" onkeypress="llamarAutocompletarEmpresa('txtAdministradora1', 184)" class="w60"  />

                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem" onclick="traerVentanitaEmpresa(this.id,'divParaVentanita', 'txtAdministradora1', '24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
            </td>
        </tr>

        <tr class="estiloImputIzq2">
            <td>Tipo Régimen - Plan</td>
            <td>
                <select id="cmbIdTipoRegimen" class="w30" onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura');comboDependienteAutocompleteEmpresa2('cmbIdTipoRegimen', 'txtAdministradora1', '79')" onchange="cargarUnLabelPlanDeContratacionEmpresa('lblIdPlanContratacion', 'lblNomPlanContratacion', 'txtAdministradora1', 'cmbIdTipoRegimen', '81')"
                        >
                    <option value=""></option>
                </select>

                <label id="lblIdPlanContratacion"></label>-<label id="lblNomPlanContratacion"></label>

            </td>

        </tr>

                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Valor unidad:</td>
                                                            <td width="70%"><input type="text" id="txtValorUnidadRecibo" onkeyup="calcularValorTotalRecibo('lblValorRecibo')" size="10" maxlength="10" class="w30"  /></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Cantidad:</td>
                                                            <td>
                                                                <select id="cmbCantidadRecibo" class="w30" onchange="calcularValorTotalRecibo('lblValorRecibo')" >                                            
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>                                                                                        
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Valor recibo:</td>
                                                            <td width="70%"> <label id="lblValorRecibo"></label></td>
                                                        </tr>

                                                        <tr class="estiloImputIzq2">
                                                            <td>Concepto:</td>
                                                            <td>
                                                                <select size="1" id="cmbIdConceptoRecibo" class="w30" >                                           
                                                                    <option value=""></option>
                                                                    <% resultaux.clear();
                                                                        resultaux = (ArrayList) beanAdmin.combo.cargar(530);
                                                                        ComboVO cmb33;
                                                                        for (int k = 0; k < resultaux.size(); k++) {
                                                                            cmb33 = (ComboVO) resultaux.get(k);
                                                                    %>
                                                                    <option value="<%= cmb33.getId()%>" title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%>
                                                                    </option>
                                                                    <%}%>                                 
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Medio de Pago</td>
                                                            <td>
                                                                <select size="1" id="cmbIdMedioPago" class="w30" >                                            
                                                                    <option value=""></option>
                                                                    <% resultaux.clear();
                                                                        resultaux = (ArrayList) beanAdmin.combo.cargar(153);

                                                                        for (int k = 0; k < resultaux.size(); k++) {
                                                                            cmb33 = (ComboVO) resultaux.get(k);
                                                                    %>
                                                                    <option value="<%= cmb33.getId()%>" title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%>
                                                                    </option>
                                                                    <%}%>                                 
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td width="30%">Observacion:</td>
                                                            <td><input type="text" class="w60" id="txtObservacionRecibo"  />
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Usuario Elaboro:</td>
                                                            <td>
                                                                <label id="lblIdUsuarioRecibo"></label>-<label id="lblNomUsuarioRecibo"></label>
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Fecha Elabora:</td>
                                                            <td>
                                                                <label id="lblFechaElaboroRecibo">
                                                            </td>                                             
                                                        </tr>
                                                        <tr   class="estiloImputIzq2">                         
                                                            <td>Estado:</td><td>              
                                                                <label id="lblIdEstadoRecibo"></label>-<label id="lblEstadoRecibo"></label>
                                                            </td>
                                                        </tr>

                                                        <% if (beanSession.usuario.preguntarMenu("10_1")) {%>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Disponible para asociar</td>
                                                            <td>
                                                                <select id="cmbDisponible">
                                                                    <option value=""></option>
                                                                    <option value="S">SI</option>
                                                                    <option value="N">NO</option>                                                                               
                                                                </select>
                                                                <input title="BF77" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('cambiarDisponibilidadRecibo')" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Auditar Recibo</td>
                                                            <td><input title="BF77" type="button" class="small button blue" value="AUDITAR" onclick="guardarYtraerDatoAlListado('auditarRecibo')" /></td>
                                                        </tr>
                                                        <%}%>
                                                        <tr>
                                                            <td colspan="2" width="99%" class="titulos">
                                                                <input title="BF38" type="button" class="small button blue" value="LIMPIAR CAMPOS" onclick="limpiarDivEditarJuan('limpiarRecibos')" />
                                                                <input title="BP77" type="button" class="small button blue" value="VER RECIBO" onclick="formatoPDFRecibo(valorAtributo('lblIdRecibo'))" />
                                                                <input title="BF38" type="button" class="small button blue" value="CREAR RECIBO" onclick="modificarCRUD('crearRecibo')" />
                                                                <!--input  title="BF38" type="button" class="small button blue" value="NUMERAR RECIBO" onclick="modificarCRUD('numerarRecibo')"  /-->

                                                            </td>
                                                        </tr>
                                                        <% if (beanSession.usuario.preguntarMenu("FAC08")) {%>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Motivo de anulacion:</td>
                                                            <td>
                                                                <select size="1" id="cmbIdMotivoAnulacionRecibo" style="width:40%" >                                          
                                                                    <option value=""></option>
                                                                    <% resultaux.clear();
                                                                        resultaux = (ArrayList) beanAdmin.combo.cargar(141);
                                                                        ComboVO cmb3;
                                                                        for (int k = 0; k < resultaux.size(); k++) {
                                                                            cmb3 = (ComboVO) resultaux.get(k);
                                                                    %>
                                                                    <option value="<%= cmb3.getId()%>" title="<%= cmb3.getTitle()%>"><%= cmb3.getDescripcion()%>
                                                                    </option>
                                                                    <%}%>                                 
                                                                </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <input title="BF77" type="button" value="ANULAR RECIBO" onclick="modificarCRUD('anularRecibo')" />
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Usuario que anul&oacute;:</td>
                                                            <td>
                                                                <label id="lblIdUsuarioAnulo"></label>
                                                            </td>
                                                        </tr>
                                                        <tr class="estiloImputIzq2">
                                                            <td>Fecha anul&oacute;:</td>
                                                            <td>
                                                                <label id="lblFechaAnulo"></label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%}%>
                                        </table>



                                    </td>
                                </tr>
                            </table>

                        </div>
                        <!-- divEditar-->


                    </td>
                </tr>
            </table>

        </td>
    </tr>

</table>


<input type="hidden" id="txtIdEstadoAuditoria" />