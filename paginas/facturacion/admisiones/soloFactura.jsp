<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>


<table width="1200px" align="center" border="0" cellspacing="0" cellpadding="1" class="fondoTabla">
  <tr>
    <td>
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="CREAR FACTURA" />
        </jsp:include>
      </div>
    </td>
  </tr>


  <tr>
    <td>

      <div>
        <div class="btitulos">
          <label class="bloque w20">Tipo Id</label>
          <label class="bloque w20">Identificación</label>
          <label class="bloque w60">PACIENTE</label>
        </div>


        <div class="bestiloImput">
          <div class="bloque w20">
            <select id="cmbTipoId" class="w90" >
              
              <%     resultaux.clear();
                  resultaux=(ArrayList)beanAdmin.combo.cargar(105);    
                  ComboVO cmb105; 
                  for(int k=0;k<resultaux.size();k++){ 
                          cmb105=(ComboVO)resultaux.get(k);
              %>
              <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                <%=cmb105.getId() + " " + cmb105.getDescripcion()%></option>
              <%}%>  
            </select>
          </div>


          <div class="bloque w20">
            <input type="text" id="txtIdentificacion" size="20" maxlength="20"
              onfocus="javascript:return document.getElementById('txtApellido1').focus()"
              onKeyPress="javascript:checkKey2(event); return teclearsoloTelefono(event);" oncontextmenu="return false"
               />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPacienT"
              onclick="traerVentanitaPaciente(this.id,'','divParaVentanita','txtIdBusPaciente','27')"
              src="/clinica/utilidades/imagenes/acciones/buscar.png">
          </div>

          <div class="bloque w60">
            <input type="text" value=" " size="110" maxlength="210" id="txtIdBusPaciente" class="w80" oncontextmenu="return false"
               />
            <input name="btn_busAgenda" type="button" class="small button blue" title="ADMI87" value="BUSCAR."
              onclick="buscarInformacionBasicaPaciente()" />
          </div>




        </div>

        <div class="btitulos">
          <label class="bloque w30">Apellidos</label>
          <label class="bloque w30">Nombres</label>
          <label class="bloque w10">Fecha Nacimiento</label>
          <label class="bloque w10">Sexo</label>
          <label class="bloque w20">Email</label>
        </div>


        <div class="bestiloImput">
          <div class="bloque w30">
            <input type="text" id="txtApellido1" class="w40" onkeypress="javascript:return teclearsoloan(event);"
              onblur="v28(this.value,this.id);"  />
            <input type="text" id="txtApellido2" class="w40" onkeypress="javascript:return teclearsoloan(event);"
              onblur="v28(this.value,this.id);"  />
          </div>

          <div class="bloque w30">

            <input type="text" id="txtNombre1" class="w40" onkeypress="javascript:return teclearsoloan(event);"
              onblur="v28(this.value,this.id);"  />
            <input type="text" id="txtNombre2" class="w40" onkeypress="javascript:return teclearsoloan(event);"
              onblur="v28(this.value,this.id);"  />


          </div>

          <input id="txtFechaNac" type="text" class="bloque w10"  />

          <div class="bloque w10">
            <select size="1" id="cmbSexo" class="w90" >
              <option value=""></option>
              <option value="F">FEMENINO</option>
              <option value="M">MASCULINO</option>
              <option value="O">OTRO</option>
            </select>

          </div>

          <input type="text" id="txtEmail" class="bloque w20"  />

        </div>

        <div class="btitulos">
          <label class="bloque w20">Municipio</label>
          <label class="bloque w30">Dirección</label>
          <label class="bloque w20">Acompañante</label>
          <label class="bloque w10">Celular1</label>
          <label class="bloque w10">Celular2</label>
          <label class="bloque w10">Teléfono</label>
        </div>

        <div class="bestiloImput">
          <div class="bloque w20">
            <input type="text" id="txtMunicipio" class="w80"  />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaMunicT"
              onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')"
              src="/clinica/utilidades/imagenes/acciones/buscar.png">
            <div id="divParaVentanita"></div>

          </div>

          <input type="text" id="txtDireccionRes" maxlength="100" class="bloque w30"
            onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value,this.id);"  />

          <div class="bloque w20">
            <input type="text" id="txtNomAcompanante" maxlength="100" class="w90" style="width:95%"
              onkeypress="javascript:return teclearExcluirCaracter(event);"  />
          </div>

          <div class="bloque w10">
            <input type="text" id="txtCelular1" class="w90" maxlength="30"
              onkeypress="javascript:return teclearsoloan(event);"  />
          </div>

          <div class="bloque w10">
            <input type="text" id="txtCelular2" class="w90" maxlength="30"
              onkeypress="javascript:return teclearsoloan(event);"  />
          </div>

          <div class="bloque w10">
            <input type="text" id="txtTelefonos" class="w90" maxlength="30"
              onkeypress="javascript:return teclearsoloan(event);"  />
          </div>

        </div>


        <div class="btitulos">
          <label class="bloque w25">Administradora Paciente</label>
          <label class="bloque w15">Etnia</label>
          <label class="bloque w15">Nivel Escolaridad</label>
          <label class="bloque w10">Estrato</label>
          <label class="bloque w15">Zona Residencia</label>
          <label class="bloque w20">Ocupación</label>

        </div>


        <div class="bestiloImput">
          <input type="text" id="txtAdministradoraPaciente" class="bloque w25" />

          <div class="bloque w15">

            <select id="cmbIdEtnia" class="w80" style="margin 5%;" >
              <option value=""></option>
              <% resultaux.clear();
                     resultaux=(ArrayList)beanAdmin.combo.cargar(2);  
                     ComboVO cmbEt; 
                     for(int k=0;k<resultaux.size();k++){ 
                         cmbEt=(ComboVO)resultaux.get(k);%>
              <option value="<%= cmbEt.getId()%>" title="<%= cmbEt.getTitle()%>">
                <%= cmbEt.getDescripcion()%>
              </option>
              <%}%>
          </select>
        
      </div>

    <div class="bloque w15">


      <select id="cmbIdNivelEscolaridad" class="w80" >
              <option value=""></option>
              <% resultaux.clear();
                     resultaux=(ArrayList)beanAdmin.combo.cargar(4);  
                     ComboVO cmbES; 
  for(int k=0;k<resultaux.size();k++){ 
  cmbES=(ComboVO)resultaux.get(k);
%>
              <option value="<%= cmbES.getId()%>" title="<%= cmbES.getTitle()%>">
                <%= cmbES.getDescripcion()%>
              </option>
              <%}%>
    </select>

    </div>

<div class="bloque w10">

  <select id="cmbIdEstrato" class="w80" >
            <option value=""></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">Sin Estrato</option>
        </select>


      
 </div>


<div class="bloque w15">

 <select id="cmbIdZonaResidencia" class="w80" >
                                                            <option value="R">Rural</option>
                                                            <option value="U">Urbano</option>
                                                        </select>


    </div>  



    <div class="bloque w20">

<input id="txtIdOcupacion" type="text"   class="w90" onkeypress="llamarAutocomIdDescripcionConDato('txtIdOcupacion',8)" >
        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaOcupT" onclick="traerVentanita(this.id,'','divParaVentanita','txtIdOcupacion','6')" src="/clinica/utilidades/imagenes/acciones/buscar.png">

    </div>



  </div>
  <div>
    <table width="100%">
      <tr>
        <td align="CENTER">
            <input name="btn_crear_admsion" type="button" class="small button blue" value="MODIFICAR PACIENTE" onclick="modificarCRUD('modificarPaciente')"  />  
        </td>
      </tr>
    </table>
  </div>



   <div class="btitulos">
      <label class="bloque w20">Administradora Factura</label> 
      <label class="bloque w20">Tipo Régimen - Plan</label>
      <label class="bloque w20">Plan Contratación</label>
      <label class="bloque w20">Tipo Afiliado</label>
      <label class="bloque w20">Rango</label>
      
</div>


<div class="bestiloImput">

  <div class="bloque w20">

    <input type="text" id="txtAdministradora1" class="w80" maxlength="50" onfocus="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')" onkeypress="llamarAutocompletarEmpresa('txtAdministradora1', 184)"  />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem" onclick="traerVentanitaEmpresa(this.id,'divParaVentanita','txtAdministradora1','24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')" src="/clinica/utilidades/imagenes/acciones/buscar.png">

  </div>


  <select id="cmbIdTipoRegimen" class="bloque w20" onclick="limpiarDivEditarJuan('limpiarDatosPlan')" onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura');comboDependienteAutocompleteEmpresa2('cmbIdTipoRegimen','txtAdministradora1','79')" onchange="cargarUnLabelPlanDeContratacionEmpresa('lblIdPlanContratacion','lblNomPlanContratacion','txtAdministradora1','cmbIdTipoRegimen','81')" >
             <option value=""></option>
          </select>
    <div class="bloque w20">
      <select id="cmbIdPlan" style="width: 95%;"
      onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura'); limpiarDivEditarJuan('limpiarDatosPlan')
               traerPlanesDeContratacion(this.id, '165', 'txtAdministradora1', 'cmbIdTipoRegimen')"
      onchange="asignaAtributo('lblIdPlanContratacion', valorAtributo(this.id), 0)"
      >
      <option value=""></option>
  </select>
    
    </div>
    

    <div class="bloque w20">

          <select id="cmbIdTipoAfiliado" class="w80" onfocus="limpiarDivEditarJuan('limpiarDatosAfiliadoFactura');comboDependiente('cmbIdTipoAfiliado','lblIdPlanContratacion','30')"  >
                <option value=""></option>
            </select>

          </div>

          <select id="cmbIdRango"  class="bloque w20" onfocus="comboDependienteDosCondiciones('cmbIdRango','lblIdPlanContratacion','cmbIdTipoAfiliado','31')"  >
                <option value=""></option>
            </select>

        <!--   <div class="bloque w20">
                <label id="lblIdPlanContratacion"></label>-<label id="lblNomPlanContratacion"></label>
            </div>
          -->
          <div hidden>
            <label id="lblIdPlanContratacion"></label>-<label id="lblNomPlanContratacion"></label>
        </div>

</div>



<div class="btitulos">
      <label class="bloque w15">Número de Autorización</label>
      <label class="bloque w40">Diagnóstico</label>
      <label class="bloque w25">Profesional Factura</label>

    </div>

<div class="bestiloImput">

       <div class="bloque w15">
            <input type="number" id="txtNoAutorizacion" class="w90" onKeyPress="javascript:return teclearExcluirCaracter(event,'%');" onblur="guardarYtraerDatoAlListado('nuevaAdmisionCuenta')"  />
       </div>
 
        <div class="bloque w40">
          <input type="text" id="txtIdDx" oninput="llenarElementosAutoCompletarKey(this.id, 206, this.value)"
              class="w90" >
          <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
              onclick="traerVentanita(this.id,'','divParaVentanita','txtIdDx','5')"
              src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </div>



<div class="bloque w25">
      <select id="cmbIdProfesionalesFactura" onfocus="comboDependienteEmpresa('cmbIdProfesionalesFactura','86')"  class="w90">
                            <option value=""></option>
                                
                        </select>  

  


    </div>


  </div>



</td>

</tr>



<tr>
      <td align="center">
          <input name="btn_crear_admsion" type="button" title="BLI33" class="small button blue" value="LIMPIAR" onclick="limpiarDatosAdmisiones()" />
          
           <input name="btn_crear_admsion" type="button" title="BTC21" class="small button blue" value="VER FACTURAS" onclick="buscarFacturacion('listSoloFacturas')" />

           <input name="btn_crear_admsion" type="button" title="BTC21" class="small button blue" value="MODIFICAR FACTURA" onclick="modificarCRUD('modificarSoloFactura')" />

           <input name="btn_crear_admsion" type="button" title="BTC21" class="small button blue" value="ELIMINAR FACTURA" onclick="modificarCRUD('eliminarSoloFactura')" />

           <input name="btn_crear_admsion" type="button" title="BTC21" class="small button blue" value="CREAR FACTURA" onclick="verificarNotificacionAntesDeGuardar('crearSoloFactura')" />

           <input name="btn_crear_admsion" type="button" class="small button blue" value="CARGAR ADJUNTOS" onclick="mostrar('divVentanitaAdjuntos');acordionArchivosAdjuntos();"  />
          </td>
  </tr>

<tr>   
        <td width="100%" >   
          <div style="overflow:auto; height:120px; width:100%" >        
                <table id="listSoloFacturas" style="width:100%" class="scroll"></table> 
                </div> 
        </td>
    </tr> 
  <tr>
      <td>
                <div id="divAcordionInfoFacturaInventario" style="display:BLOCK"> 
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="DETALLES FACTURA DE INVENTARIO" />
                        <jsp:param name="idDiv" value="divInfoFacturaInventario" />
                        <jsp:param name="pagina" value="infoFacturaInventario.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="" />
                    </jsp:include>
                </div>

                <div id="divAcordionInfoFactura" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="OTROS" />
                        <jsp:param name="idDiv" value="divInfoFacturaOtros" />
                        <jsp:param name="pagina" value="otrosSoloFactura.jsp"/>
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="buscarFacturacion('listaOtrosDesatallesFactura')" />
                    </jsp:include>
                </div>

                <div id="divAcordionInfoFactura" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="DETALLES FACTURA DEL SERVICIO" />
                        <jsp:param name="idDiv" value="divInfoFactura" />
                        <jsp:param name="pagina" value="infoFacturaServicioSoloFactura.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="" />
                    </jsp:include>
                </div>


                 <div id="divAcordionInfoFacturaDetalle" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="DETALLES FACTURA" />
                        <jsp:param name="idDiv" value="divInfoFacturaDetalle" />
                        <jsp:param name="pagina" value="infoFacturaDetalle.jsp" />
                        <jsp:param name="display" value="BLOCK" />
                        <jsp:param name="funciones" value="" />
                    </jsp:include>
                </div>

                <div id="divAcordionInfoFacturaRecibos" style="display:BLOCK">
                  <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="RECIBOS FACTURA" />
                      <jsp:param name="idDiv" value="divInfoFacturaRecibos" />
                      <jsp:param name="pagina" value="infoFacturaRecibos.jsp" />
                      <jsp:param name="display" value="none" />
                      <jsp:param name="funciones" value="buscarFacturacion('listRecibosFactura')" />
                  </jsp:include>
              </div>


            </td>
        </tr>
       
</table>
<label id="lblUsuarioCrea"></label>
<label> Cita futura:</label>
<label id="lblCitaFutura" align="left"></label>
<!--
 <div id="divVentanitaEditarAdmision"  style="display:none; z-index:2000; top:1px; left:-211px;">
  
      <div class="transParencia" style="z-index:2001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
      </div>  
      <div  style="z-index:2002; position:absolute; top:200px; left:20px; width:95%">  
      
      
      
         
      </div>   
  </div>
  -->
<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:260px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">

          <tr class="estiloImput">
                <td align="left" >
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
          <tr>
              <td colspan="2">
               <div id="divAcordionAdjuntos" style="display:BLOCK">      
                  <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                      <jsp:param name="titulo" value="Archivos adjuntos" />
                      <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                      <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />                
                      <jsp:param name="display" value="BLOCK" />  
                      <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
                  </jsp:include> 
              </div>
            </td>
          </tr>
        </table>
    </div>
</div>

<div hidden><label id="lblIdSedeAdmision"></label></div>
<input type="text" id="txtDiagnostico" value="S" /> 
<input type="hidden" id="txtSoloAdmision" value="SI" /> 
<input type="hidden" id="txtIdEspecialidad" /> 
<input type="hidden" id="txtEsCirugia" /> 
<input type="hidden" id="txtAdmisiones" value="SI" /> 

  <!-- HISTORICOS ATENCION --> 
 <input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />       
<input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
<input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />   
<input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />    
<input type="hidden" id="txtPrincipalHC" value="NO"/> 


<div id="divNotas" style="display:none; z-index:2000; top:100px; left:50px;">
  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>  
  <div style="z-index:2004; position:fixed; top:200px;left:50%; width:70%;margin-left: -34%;">  
      <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
          <tr class="estiloImput">
              <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divNotas')" /></td>              
              <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divNotas')" /></td>  
          </tr>
          <tr class="titulosCentrados" >
              <td width="100%" colspan="2">Asociar Notas a la Factura actual</td>
              <input type="button" value="buscar" onclick="buscarAGENDA('notas'); buscarAGENDA('citasAdmision')">
          <tr>
          <tr>
              <td colspan="2"><hr></td>
          </tr>
          <tr class="titulos">
              <td colspan="2"> ID FACTURA:<label id="lblIdFacturaAsociarCita"></label>
          </tr>
          <tr>
              <td colspan="2">
                  <table width="100%">
                      <tr class="titulosCentrados">
                          <td>NOTAS</td>
                          <td>NOTAS ASOCIADAS</td>
                      </tr>
                      <tr>
                          <td>
                              <table width="100%" id="notas"></table>
                          </td>
                          <td>
                              <table width="100%" id="notasFacturas"></table>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
          <tr>
              <td align="CENTER">
                  <input type="button" class="small button blue" value="ASOCIAR NOTA SELECCIONADA" onclick="modificarCRUD('asociarNotaFactura')" style="width: 40%;"/>              
              </td>
          </tr>
      </table>  
  </div>  
</div>

