<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>


<table width="1200px" align="center" border="0" cellspacing="0" cellpadding="1" class="fondoTabla">
    <tr>
        <td class="w100">
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="ADMISIONES" />
                </jsp:include>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div>
                <table width="100%">
                    <tr class="titulos">
                        <td width="5%"><strong>Id Cita</strong></td>
                        <td width="5%"></td>
                        <td width="5%"></td>
                        <td width="60%"><strong>Paciente</strong></td>
                        <td width="10%"></td>
                    </tr>
                    <tr class="estiloImput">
                        <td>
                            <input type="hidden" id="lblIdAgendaDetalle" />
                            <label id="lblIdCita" style="width: 90%;"></label>
                        </td>
                        <td>
                            <input type="button" class="small button blue" title="ADM57" value="AGENDA"
                                   onclick="abrirVentanaTraerCitaAlaAdmisionCuenta()" style="width: 90%;" />
                        </td>
                        <td>
                            <input type="button" class="small button blue" value="INGRESOS"
                                   onclick="abrirVentanaAdmisionUrgencia();" style="width: 90%;" />
                        </td>
                        <td>
                            <input type="text" id="txtIdBusPaciente" 
                                   style="width: 95%;" readonly/>
                        </td>
                        <td>
                            <input name="btn_busAgenda" type="button" class="small button blue" title="ADMI87"
                                   style="width: 90%;" value="BUSCAR." onclick="buscarInformacionBasicaPaciente()" />
                        </td>
                    </tr>
                </table>
                <div class="btitulos">
                    <label class="bloque w10 campoRequerido ">Tipo Id</label> 
                    <label class="bloque w20 campoRequerido">Identificación</label>
                    <label class="bloque w25 campoRequerido">Apellidos</label>
                    <label class="bloque w25 campoRequerido">Nombres</label>
                    <label class="bloque w10 campoRequerido">Fecha Nacimiento</label>
                    <label class="bloque w5">Edad</label>
                    <label class="bloque w10 campoRequerido">Sexo</label>
                </div>
                <div class="bestiloImput">
                    <div class="bloque w10">
                        <select id="cmbTipoId" class="w90" >
                            <%     resultaux.clear();
                                resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                                ComboVO cmb105;
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmb105 = (ComboVO) resultaux.get(k);
                            %>
                            <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                <%=cmb105.getId() + " " + cmb105.getDescripcion()%></option>
                                <%}%>  
                        </select>
                    </div>
                    <div class="bloque w20">
                        <input type="text" id="txtIdentificacion" class="90w"
                               onKeyPress="javascript:checkKey2(event); return teclearsoloTelefono(event);"
                                />
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPacienT"
                             onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')"
                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                    </div>
                    <div class="bloque w25">
                        <input  type="text" id="txtApellido1"
                               onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value, this.id);"
                                style="width:45%" />
                        <input  type="text" id="txtApellido2"
                               onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value, this.id);"
                                style="width:45%" />
                    </div>
                    <div class="bloque w25">
                        <input  type="text" id="txtNombre1" onkeypress="javascript:return teclearsoloan(event);"
                               onblur="v28(this.value, this.id);"  style="width:45%" />
                        <input  type="text" id="txtNombre2" onkeypress="javascript:return teclearsoloan(event);"
                               onblur="v28(this.value, this.id);"  style="width:45%" />
                    </div>
                    <div class="bloque w10">
                        <input id="txtFechaNac" type="text" class="w80"  /> 
                    </div>
                    <div class="bloque w5">
                        <label id="txtEdad" style="display:none;"></label>
                        <label id="txtEdadCompleta"><label> 
                    </div>
                    <div class="bloque w10">
                        <select id="cmbSexo"  >
                            <option value=""></option>
                            <option value="F">FEMENINO</option>
                            <option value="M">MASCULINO</option>
                            <option value="O">OtrO</option>
                        </select>
                    </div>
                        
                </div>
                <div class="btitulos">
                    <label class="bloque w25">Nacionalidad</label>
                    <label class="bloque w25 campoRequerido">Municipio</label>
                    <label class="bloque w30 campoRequerido">Dirección</label>
                    <label class="bloque w15">Acompañante</label>
                    <label class="bloque w10 campoRequerido">Celular1</label>
                    <label class="bloque w10">Celular2</label>
                    <label class="bloque w10 ">Telefono</label>
                </div>

                <div class="bestiloImput">

                    <div class="bloque w25">
                        <input type="text" id="txtNacionalidad" class="w80"  onkeypress="llamarAutocomIdDescripcionConDato('txtNacionalidad', 6014)"  value="COLOMBIA"/>
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaMunicT"
                             onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtNacionalidad', '60')"
                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        <div id="divParaVentanita"></div>
                    </div>

                    <div class="bloque w25">
                        <input type="text" id="txtMunicipio" class="w80"  />
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaMunicT"
                             onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')"
                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        <div id="divParaVentanita"></div>
                    </div>

                    <input type="text" id="txtDireccionRes" class="bloque w30"
                           onkeypress="javascript:return teclearsoloan(event);" onblur="v28(this.value, this.id);"
                            />
                    <input type="text" id="txtNomAcompanante" class="bloque w15"
                           onkeypress="javascript:return teclearExcluirCaracter(event);"  />
                    <input type="text" id="txtCelular1" class="bloque w10"
                           onkeypress="javascript:return teclearsoloan(event);"  />
                    <input type="text" id="txtCelular2" class="bloque w10"
                           onkeypress="javascript:return teclearsoloan(event);"  />
                    <input type="text" id="txtTelefonos" class="bloque w10"
                           onkeypress="javascript:return teclearsoloan(event);"  />

                </div>

                <div class="btitulos">
                    <label class="bloque w20 ">Email</label>
                    <label class="bloque w20 campoRequerido">Etnia</label>
                    <label class="bloque w20 campoRequerido">Nivel Escolaridad</label>
                    <label class="bloque w10 campoRequerido">Estrato</label>
                    <label class="bloque w30 campoRequerido">Ocupación</label>
                </div>

                <div class="bestiloImput">
                    <input type="text" id="txtEmail" class="bloque w20"  />
                    <select id="cmbIdEtnia" class="bloque w20 m20" >
                        <option value=""></option>
                        <% resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(2);
                            ComboVO cmbEtn;
                            for (int k = 0; k < resultaux.size(); k++) {
                                cmbEtn = (ComboVO) resultaux.get(k);
                        %>
                        <option value="<%= cmbEtn.getId()%>" title="<%= cmbEtn.getTitle()%>">
                            <%= cmbEtn.getDescripcion()%></option>
                            <%}%> 
                    </select>
                    <select id="cmbIdNivelEscolaridad" class="bloque w20 m20"  >
                        <option value=""></option>
                        <% resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(4);
                            ComboVO cmbNi;
                            for (int k = 0; k < resultaux.size(); k++) {
                                cmbNi = (ComboVO) resultaux.get(k);
                        %>
                        <option value="<%= cmbNi.getId()%>" title="<%= cmbNi.getTitle()%>">
                            <%= cmbNi.getDescripcion()%></option>
                            <%}%> 
                    </select>                         

                    <select  id="cmbIdEstrato" class="bloque w10 m20"  >
                        <option value=""></option>
                        <option value="1">1</option>
                        <option value="2">2</option>   
                        <option value="3">3</option>   
                        <option value="4">4</option>   
                        <option value="5">5</option>   
                        <option value="6">6</option>   
                        <option value="7">7</option>
                        <option value="8">Sin Estrato</option>                            
                    </select>                         

                    <div class="bloque w30">
                        <input id="txtIdOcupacion" type="text"  class="w80" onkeypress="llamarAutocomIdDescripcionConDato('txtIdOcupacion', 8)" >    
                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaOcupT" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdOcupacion', '6')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                    </div>               
                </div>
                <table width="100%">
                    <tr class="titulosListaEspera">
                        <td width="10%">Zona</td>
                        <td width="20%" class="campoRequerido">Administradora Paciente</td>
                        <td width="25%" class="campoRequerido">Ips Primaria</td>
                        <td width="10%" class="campoRequerido">Regimen</td>
                        <td width="15%" class="campoRequerido">Nivel Sisben</td>
                        <td width="20%">Estadio</td>
                    </tr>
                    <tr class="estiloImputListaEspera">
                        <td>
                            <select id="cmbIdZonaResidencia" style="width: 95%;">
                                <option value="R">Rural</option>
                                <option value="U">Urbano</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" id="txtAdministradoraPaciente" style="width: 75%;" onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraPaciente', 6015)" />
                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdmPac"
                             onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtAdministradoraPaciente', '59')"
                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        <div id="divParaVentanita"></div>
                        </td>
                        <td>
                            <input type="text"  id="txtIpsPaciente" style="width: 75%;" value="190010918001-BIOS MEDICAL CENTER SAS SEDE 1::POPAYAN"/>
                            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIpsPac"
                             onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIpsPaciente', '55')"
                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                        <div id="divParaVentanita"></div>
                        </td>
                        <td>
                            <select id="cmbTipoRegimen" style="width: 95%;">
                                <option value=""></option>
                                <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(518);
                                    ComboVO cmbPro1;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbPro1 = (ComboVO) resultaux.get(k);%>
                                <option value="<%= cmbPro1.getId()%>" title="<%= cmbPro1.getTitle()%>"><%=cmbPro1.getDescripcion()%></option><%}%>           
                            </select> 
                        </td>
                        <td>
                            <select id="cmbNivelSisben" style="width: 95%;">
                                <option value=""></option>
                                <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(3505);
                                    ComboVO cmbNS;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbNS = (ComboVO) resultaux.get(k);%>
                                <option value="<%= cmbNS.getId()%>" title="<%= cmbNS.getTitle()%>"><%=cmbNS.getDescripcion()%></option><%}%>           
                            </select> 
                        </td>
                        <td><label class="parpadea" style="color: #ff0000; font-size: medium;" id="lblEstadio" style="width: 95%;"></label></td>
                    </tr>
                </table>
            </div>

        </td>
    </tr>
    <tr>
        <td align="CENTER">
            <input name="btn_crear_admsion" type="button" class="small button blue" value="MODIFICAR PACIENTE" onclick="modificarCRUD('modificarPaciente')"/> 
            <input name="btn_crear_admsion" type="button" class="small button blue" value="INCONSISTENCIA EN DATOS" onclick="abrirVentanaAnexo1()"/> 
            <input name="btn_crear_paciente_admsion" type="button" class="small button blue" value="CREAR PACIENTE" onclick="verificarNotificacionAntesDeGuardar('crearPacienteAdmision')"/> 
        </td>
    </tr>
    <tr>
        <td> 
            <div id="divAcordionInfoIngresoAdicional" style="display:BLOCK">      
                <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                    <jsp:param name="titulo" value="INFORMACION DE LA ADMISION ADICIONAL" />
                    <jsp:param name="idDiv" value="divInfoIngresoAdicional" />
                    <jsp:param name="pagina" value="infoIngresoAdicional.jsp" />                
                    <jsp:param name="display" value="NONE" />  
                    <jsp:param name="funciones" value="" />
                </jsp:include> 
            </div> 


            <div id="divAcordionInfoIngreso" style="display:BLOCK">      
                <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                    <jsp:param name="titulo" value="INFORMACION DE LA ADMISION" />
                    <jsp:param name="idDiv" value="divInfoIngreso" />
                    <jsp:param name="pagina" value="infoIngreso.jsp" />                
                    <jsp:param name="display" value="BLOCK" />  
                    <jsp:param name="funciones" value="" />
                </jsp:include> 
            </div> 


        </td>  
    </tr> 
    <tr>
        <td>   
            <div class="btitulos" style="padding: 20px;">
                <div class="bloque w10">
                    <input type="button" title="BLI33" class="small button blue" value="LIMPIAR CAMPOS" onclick="limpiarDatosAdmisiones()"/>
                </div>

                <div class="bloque w15">
                    <input title="BT17A" type="button" class="small button blue" value="BUSCAR ADMISIONES" onclick="buscarAGENDA('listAdmisionCuenta');"/></div>

                <div class="bloque w15">
                    <input type="button" id="btnCrearAdmision" class="small button blue" title="BTLL7" value="CREAR ADMISION" onclick="ventanaVerificacionAdmisionFactura()" />
                </div>

                <div class="bloque w15">
                    <input type="button" class="small button blue" title="B3TK" value="ELIMINAR ADMISION" onclick="verificarNotificacionAntesDeGuardar('eliminarAdmision')"/>
                </div>

                <div class="bloque w15">
                    <input type="button" class="small button blue" title="B5TL" value="MODIFICAR ADMISION" onclick="modificarAdmisionFacturacion()"/>
                </div>

                <div  class="bloque w15">
                    <input type="button" title="BLI33" class="small button blue" value="MULTIPLE FACTURA" onclick="modificarCRUD('crearFacturaDesdeAdmision')" />
                </div>

                <div  class="bloque w15">
                    <input type="button" title="BLI33" class="small button blue" value="ELIMINAR FACTURA" onclick="modificarCRUD('eliminarFacturaAdmision')" /> 
                </div>

                <div class="bloque w15">
                    <input type="button" class="small button blue" value="CARGAR ADJUNTOS" onclick="mostrar('divVentanitaAdjuntos');acordionArchivosAdjuntos();"/>   
                </div>

                <div class="bloque w15">
                    <input name="btn_crear_cita" id="btnaa" type="button" class="small button blue" value="CREAR ORDEN EXTERNA" onclick="mostrarVentanaOrdenExterna()"  />
                </div>

                <div class="bloque w15">
                    <input name="btn_crear_cita" id="btnaa" type="button" class="small button blue" value="VER HISTORIA CLINICA" onclick="mostrarVentanaHcAdmision()"  />
                </div>
            </div>
        </td>
    </tr>

    <tr class="titulos" >   
        <td colspan="8">  
            <div id="divContenedorListaAdmisiones" style="height:200px">
                <table id="listAdmisionCuenta"  class="scroll" width="100%"></table>  
            </div>   
        </td>
    </tr>   
    <tr>    
        <td>
            <div class="btitulos" style="background: white; ">
                <label  id="lblInternado" hidden></label>
                <label class="bloque w10">SEDE</label>
                <label class="bloque w10">ADMISIÓN</label>
                <label class="bloque w15">TIPO ADMISIÓN</label>
                <label class="bloque w20">PLAN</label>
                <label class="bloque w10">TIPO REGIMEN</label>
                <label class="bloque w15">TIPO AFILIADO</label>
                <label class="bloque w15">RANGO</label>
                <label class="bloque w15">USUARIO</label>
                <label class="bloque w15"></label>
            </div>
            <div class="bestiloImput">
                <div class="bloque w10"><label id="lblIdSedeAdmision"></label>-<label id="lblNombreSedeAdmision"></label></div>
                <div class="bloque w10"> <label id="lblIdAdmision" > </label> </div>
                <div class="bloque w15"><label id="lblIdTipoAdmision"></label>-<label id="lblNomTipoAdmision"> </label></div>
                <div class="bloque w20"><label id="lblIdPlan"></label>-<label id="lblNomPlan"> </label></div>
                <div class="bloque w10"><label id="lblIdRegimen"></label>-<label id="lblNomRegimen"> </label></div>
                <div class="bloque w15"><label id="lblIdTipoAfiliado"></label>-<label id="lblNomTipoAfiliado"> </label></div>
                <div class="bloque w15"><label id="lblIdRango"></label>-<label id="lblNomRango"> </label></div>   
                <label id="lblUsuarioCrea" class="bloque w15"> </label>
                <div class="bloque w15"> 
                <input type="button" title="BLI33" class="small button blue" value="IMPRIMIR CERTIFICADO" onclick="imprimirCertificadoAtencion()"/>
                </div>
            </div>


            <div id="divAcordionInfoCirugia" style="display:BLOCK">      
                <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                    <jsp:param name="titulo" value="INFORMACION DE PROCEDIMIENTOS CONtrATADOS A REALIZAR" />
                    <jsp:param name="idDiv" value="divInfoCirugia" />
                    <jsp:param name="pagina" value="infoCirugia.jsp" />                
                    <jsp:param name="display" value="BLOCK" />  
                    <jsp:param name="funciones" value="" />
                </jsp:include> 
            </div> 

            <div id="divAcordionInfoFacturaInventario" style="display:BLOCK">      
                <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                    <jsp:param name="titulo" value="DETALLES FACTURA DE INVENTARIO"/>
                    <jsp:param name="idDiv" value="divInfoFacturaInventario"/>
                    <jsp:param name="pagina" value="infoFacturaInventario.jsp"/>                
                    <jsp:param name="display" value="none"/>
                    <jsp:param name="funciones" value="buscarFacturacion('listArticulosDeFactura')"/>
                </jsp:include> 
            </div>

            <div id="divAcordionInfoFactura" style="display:BLOCK">       
                <jsp:include page="divsAcordeon.jsp" flush="FALSE"> 
                    <jsp:param name="titulo" value="DETALLES FACTURA DEL SERVICIO"/> 
                    <jsp:param name="idDiv" value="divInfoFactura"/> 
                    <jsp:param name="pagina" value="infoFacturaServicio.jsp"/>                
                    <jsp:param name="display" value="BLOCK"/> 
                    <jsp:param name="funciones" value=""/> 
                </jsp:include> 
            </div> 

            <div id="divAcordionInfoFacturaDetalle" style="display:BLOCK">
                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                    <jsp:param name="titulo" value="DETALLES FACTURA" />
                    <jsp:param name="idDiv" value="divInfoFacturaDetalle" /> 
                    <jsp:param name="pagina" value="infoFacturaDetalle.jsp" />
                    <jsp:param name="display" value="BLOCK" />
                    <jsp:param name="funciones" value="" />
                </jsp:include>
            </div> 

            <div id="divAcordionInfoFacturaRecibos" style="display:BLOCK">
                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                    <jsp:param name="titulo" value="RECIBOS FACTURA" />
                    <jsp:param name="idDiv" value="divInfoFacturaRecibos" />
                    <jsp:param name="pagina" value="infoFacturaRecibos.jsp" />
                    <jsp:param name="display" value="BLOCK" />
                    <jsp:param name="funciones" value="buscarFacturacion('listRecibosFactura')" />
                </jsp:include>
            </div>

            <div id="divAcordionCartera" style="display:BLOCK">
                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                    <jsp:param name="titulo" value="PAGOS PACIENTE" />
                    <jsp:param name="idDiv" value="divInfoCartera" />
                    <jsp:param name="pagina" value="crearRecibosFactura.jsp" />
                    <jsp:param name="display" value="BLOCK" />
                    <jsp:param name="funciones" value="" />
                </jsp:include>
            </div>
        </td>
    </tr>
</table>





<label id="lblUsuarioCrea"></label>
<label> Cita futura:</label> 
<label id="lblCitaFutura" align="left"></label>


<div id="divVentanitaVerificacion"  style="display:none; z-index:2000; top:1px; left:211px;">

    <div class="transParencia" style="z-index:2001; background:#999; filter:alpha(opacity=5);float:left;">
    </div>  
    <div  style="z-index:2002; position:fixed; top:200px; left:100px; width:70%">  
        <table width="80%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput">
                <td align="left" width="50%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaVerificacion')"/></td>  
                <td align="right" width="50%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaVerificacion')"/></td>  
            <tr>    
            <tr class="titulos" >
                <td>Tipo Identificación</td>  
                <td>Identificación</td>
            <tr>                 
            <tr class="estiloImput" >
                <td >
                    <select size="1" id="cmbTipoIdVerifica" style="width:55%"  >
                        <%     resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                            ComboVO cmb1015;
                            for (int k = 0; k < resultaux.size(); k++) {
                                cmb1015 = (ComboVO) resultaux.get(k);
                        %>
                        <option value="<%= cmb1015.getId()%>" title="<%= cmb1015.getTitle()%>">
                            <%=cmb1015.getId() + " " + cmb1015.getDescripcion()%></option>
                            <%}%>       
                    </select>                   
                </td>  
                <td >
                    <input  type="text" id="txtIdentificacionVerifica"  size="20" maxlength="20" onKeyPress="javascript:return teclearsoloTelefono(event);" />
                </td>
            <tr>                                      
            <tr class="estiloImputListaEspera"> 
                <td >
                    <input name="btn_crear_admsion" type="button" class="small button blue" title="BTN33" value="CREAR ADMISION SIN FACTURA" onclick="crearAdmisionSinFactura()"  />                     
                </td> 
                <td >
                    <input  type="button" class="small button blue" title="BTN3W" value="CREAR ADMISION CON FACTURA" onclick="crearAdmisionConFactura()"  />   
                </td> 

            </tr>
        </table>  


    </div>     
</div>

<div id="divVentanitaAgendaAdmision"  style="display:none; z-index:2000; top:1px; width:600px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div> 
    <div  style="z-index:2002; position:fixed; top:100px; left:50%;margin-left: -33%;">  
        <table width="100%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput" >
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgendaAdmision')"/></td>  
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgendaAdmision')"/></td>  
            <tr> 
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr class="titulos">                                                          
                            <td width="30%">Paciente</td>               
                            <td width="15%">Especialidad</td>  
                            <td width="15%">Tipo cita</td>
                            <td width="25%">Profesional</td>  
                            <td width="5%">Exito</td>             
                            <td width="10%">Admision</td>                          
                        </tr>  
                        <tr class="estiloImputListaEspera">
                            <td>
                                <input type="text" size="110"  maxlength="210"  id="txtIdBusPacienteCita" oninput="llenarElementosAutoCompletarKey(this.id, 205, this.value)" style="width:95%"/>
                            </td>               
                            <td>
                                <select id="cmbIdEspecialidadCita" style="width:95%" 
                                        onfocus="cargarComboGRALCondicion1('', '', this.id, 849, document.getElementById('lblIdSede').innerText)"
                                        onchange="limpiaAtributo('cmbIdTipoCitaAgenda', 0); limpiaAtributo('cmbIdProfesionalesCita', 0)">	                                        
                                    <option value="">[ SELECCIONE ]</option>  				
                                </select> 
                            </td>
                            <td>
                                <select id="cmbIdTipoCitaAgenda" style="width:95%" 
                                        onfocus="cargarComboGRALCondicion1('', '', this.id, 551, valorAtributo('cmbIdEspecialidadCita'));"
                                        onchange="limpiaAtributo('cmbIdProfesionalesCita', 0)">	                                        
                                    <option value="">[ SELECCIONE ]</option>  				
                                </select> 
                            </td>
                            <td>
                                <select id="cmbIdProfesionalesCita" style="width:95%" onfocus="cargarComboGRALCondicion2('', '', this.id, 197, valorAtributo('cmbIdEspecialidadCita'), document.getElementById('lblIdSede').innerText);">	                                        
                                    <option value="">[ SELECCIONE ]</option>                     
                                </select>                         
                            </td> 
                            <td>
                                <select size="1" id="cmbExito" style="width:90%"    >	                                        
                                    <option value="S">Si</option>
                                    <option value="N">No</option>                                
                                </select>
                            </td>              
                            <td>
                                <select size="1" id="cmbAdmision" style="width:90%"    >	   
                                    <!--<option value="P">Sin Admision y Pre Factura</option>-->                                    
                                    <option value="N">Sin Admision</option>
                                    <option value="S">Con Admision</option>  

                                </select>                         
                            </td>                          
                        </tr> 
                    </table>  
                </td>
            </tr>                                      
            <tr> 
                <td colspan="2">
                    <table width="80%">
                        <tr class="estiloImputListaEspera">
                            <td width="40%">
                                <label>Administradora: </label> 
                                <input type="text"  id="txtAdministraPaciente" onkeypress="llamarAutocompletarEmpresa('txtAdministraPaciente', 184)" style="width:90%"/> 
                                <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaAdPaciente" onclick="traerVentanitaEmpresa(this.id, 'divParaVentanita', 'txtAdministraPaciente', '24')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                            </td>
                            <td width="12%">
                                <label>Asistio: </label> 
                                <select size="1" id="cmbAsiste" style="width:90%" >	      
                                    <option value="">[ SELECCIONE ]</option>
                                    <option value="SI">SI</option>
                                    <option value="NO">NO</option>                                
                                </select>
                            </td>
                            <td width="12%">
                                <label>Modalidad: </label>
                                <select id="cmbModalidad" style="width:90%" onfocus="cargarComboGRAL('', '', this.id, '3529');">
                                    <option value="">[ SELECCIONE ]</option>
                                </select>
                            </td>
                            <td width="12%">
                                <label>Fecha cita: </label>
                                <input size="10" maxlength="10" id="txtFechaTraerCita"  type="text">
                            </td>                
                            <td width="12%">
                                <input name="btn_crear_cita" type="button" class="small button blue" value="BUSCAR CITAS" title="B887" onclick="buscarAGENDA('listAgendaAdmisionCuentas');"/>  
                            </td> 
                            <td width="12%">
                                <input type="button" class="small button blue" value="LIMPIAR"/>  
                            </td> 
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="titulos" >   
                <td colspan="2">  
                    <table id="listAgendaAdmision" class="scroll" width="100%"></table>  
                </td>
            </tr>                
        </table>  
        <input type="text" id="txtEstadoFacturaVentanitaAgenda" hidden> 
    </div>     
</div>

<!--ventana cambiada -->
<div id="divVentanitaEditarAgenda"  style="display:none; z-index:2000; top:1px; left:-211px;">
    <div class="transParencia" style="z-index:2003; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div>  
    <div  style="z-index:2004; position:fixed; top:200px; left:50%; width:70%;margin-left: -35%;">
        <table width="90%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput" >
                <td align="left" colspan="4" width="15%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarAgenda')" /></td>                
                <td align="right"colspan="3" width="15%"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditarAgenda')" /></td>  
            </tr> 
            <tr>
                <td colspan="6"><table style="width:100%">
                    <tr class="titulosListaEspera" >
                        <td width="7%">Preferencial</td> 
                        <td width="7%">Id Cita</td>  
                        <td width="25%">Paciente</td>
                        <td width="12%">Fecha cita</td> 
                        <td width="12%">Modalidad</td>                                
                        <td width="12%">Estado</td>  
                        <td width="12%">Motivo</td>   
                        <td width="12%">Clasificacion</td>  
                    </tr>  
                    <tr class="estiloImput">
                        <td>
                            <select id="cmbPreferencial" onchange="modificarCRUD('modificarEstadoPreferencial', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                <option value="NO">NO</option>
                                <option value="SI">SI</option>
                            </select>
                        </td>
                        <td><label id="lblIdCitaEdit"></label></td>  
                        <td><label id="lblPacienteCitaEdit"></label></td>
                        <td><label id="lblFechaCitaEdit"></label></td>

                        <td>
                            <select id="cmbModalidadCitaEdit" style="width:90%" >
                                <option value=""></option>
                                <%  resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(3529);
                                    ComboVO cmbMod;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbMod = (ComboVO) resultaux.get(k);
                                %>
                                <option value="<%= cmbMod.getId()%>" title="<%= cmbMod.getTitle()%>">
                                    <%=cmbMod.getDescripcion()%></option>
                                    <%}%>						
                            </select>
                        
                        </td>

                        <td>
                            <select id="cmbEstadoCitaEdit" style="width:90%"  onfocus="limpiarDivEditarJuan('limpiarEstadoMotivoCitaEdit')">
                                <option value=""></option>
                                <%     resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(99);
                                    ComboVO cmbEst;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbEst = (ComboVO) resultaux.get(k);
                                %>
                                <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                                    <%= cmbEst.getId() + "  " + cmbEst.getDescripcion()%></option>
                                    <%}%>						
                            </select>                   
                        </td> 
                        <td >
                            <select id="cmbMotivoConsultaEdit" style="width:90%"   onfocus="comboDependiente('cmbMotivoConsultaEdit', 'cmbEstadoCitaEdit', '96');limpiarDivEditarJuan('limpiarEstadoCitaEdit');">
                                <option value=""></option>
                                <%     resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(106);
                                    ComboVO cmbM;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbM = (ComboVO) resultaux.get(k);
                                %>
                                <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                    <%= cmbM.getId() + "  " + cmbM.getDescripcion()%></option>
                                    <%}%>						
                            </select> 
                        </td>  
                        <td>
                            <select id="cmbMotivoConsultaClaseEdit" style="width:90%"  onfocus="comboDependienteDosCondiciones('cmbMotivoConsultaClaseEdit', 'cmbMotivoConsultaEdit', 'txtIdEspecialidad', '563')">
                                <option value=""></option>                      
                            </select> 
                        </td>
                    </tr>                        
                </td></table>            
            </tr>                                               
            <tr class="estiloImputListaEspera"> 
                <td colspan="8">

                    <input id="BTN_MODIFICAR_ESTADO_CITA"  type="button" class="small button blue" value="MODIFICAR ESTADO CITA" onclick="modificarCRUD('modificarEstadoCitaCuenta', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />
                    <input id="BTN_MODIFICAR_PREFACTURA" type="button" class="small button blue" value="MODIFICAR ESTADO CITA PREFACTURA" onclick="modificarCRUD('modificarEstadoCitaPrefactura');"   />
                </td> 
            </tr>
        </table>  
    </div>     
</div>
<!--fin cambiado -->

<div id="divVentanitaAdmisionUrgencias"  style="display:none; z-index:2000; top:1px; width:800px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        <div style="z-index:2002; position:fixed; top:100px; width:60%">  
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdmisionUrgencias')" /></td>  
                    <td align="right" colspan="5"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdmisionUrgencias')" /></td>  
                </tr> 
                <tr>
                    <td>
                        <table width="100%">
                            <tr class="titulos">
                                <td width="20%">TIPO ID</td>
                                <td width="20%">IDENTIFICACION</td>
                                <td width="20%">ID PACIENTE:<label id="lblIdPacienteUrgencias" style="color: red;"></label></td>
                                <td width="20%">ESTADO</td>                     
                                <td width="20%">PREFERENCIAL</td>                  
                            </tr>    
                            <tr>
                                <td>
                                    <select id="cmbTipoIdUrgencias" style="width: 95%;"> 
                                        <option value=""></option>
                                        <%
                                            resultaux.clear();
                                            resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                                            for (int k = 0; k < resultaux.size(); k++) {
                                                cmbM = (ComboVO) resultaux.get(k);
                                        %>
                                        <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                            <%=cmbM.getId() + " " + cmbM.getDescripcion()%></option>
                                            <%}%>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" id="txtIdentificacionUrgencias" style="width: 82%;"
                                        onKeyPress="javascript:checkKey3(event);javascript:return soloTelefono(event);" onInput="limpiarCamposURG('limpiarDatosPaciente')"/>
                                    <img width="18px" height="18px" id="imgLupaPacienteUrgencias"
                                        onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPacienteUrgencias', '27')"
                                        src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                </td>
                                <td>
                                    <input type="text" id="txtIdBusPacienteUrgencias" style="width: 95%;" readonly/>
                                </td>      
                                <td>
                                    <select style="width: 95%;" id="cmbTieneOrden">
                                        <option value="1"><img width='12' height='12' style='cursor:pointer' src='/clinica/utilidades/imagenes/acciones/alerta_cambiante.gif'>&nbsp;PENDIENTE FACTURA</option>
                                        <option value="2"><img width='12' height='12' style='cursor:pointer' src='/clinica/utilidades/imagenes/acciones/alertta.gif'>&nbsp;CON ALTA MEDICA</option>
                                        <option value="3"><img width='12' height='12' style='cursor:pointer' src='/clinica/utilidades/imagenes/acciones/punto_amarillo.gif'>&nbsp;EN ATENCION</option>
                                    </select>
                                </td>                            
                                <td>
                                    <select style="width: 95%;" id="cmbPreferencialURG">
                                        <option value=""></option>
                                        <option value="S">SI</option>
                                        <option value="N">NO</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="titulos">
                                <td width="20%">SERVICIO</td>
                                <td width="20%">ESPECIALIDAD</td>
                                <td width="20%">FECHA CREACION INICIO,</td>   
                                <td width="20%">FECHA CREACION FINAL</td> 
                                <td width="20%"></td> 
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbIdTipoServicioUrgencias"
                                            onfocus="comboDependienteEmpresa('cmbIdTipoServicioUrgencias', '163')" onchange="traerInfoServicio(); limpiaAtributo('cmbIdTipoEspecialidadUrgencias')"
                                            style="width: 95%;">
                                        <option value=""></option>
                                    </select>
                                </td>
                                <td>
                                    <select id="cmbIdTipoEspecialidadUrgencias"
                                            onfocus="cargarComboGRALCondicion2('cmbPadre', '1', 'cmbIdTipoEspecialidadUrgencias', 3226, valorAtributo('cmbIdTipoServicioUrgencias'), IdSede());" 
                                            style="width: 95%;">
                                        <option value=""></option>
                                    </select>
                                </td>
                                <td>
                                    <input type="date" style="width: 95%;" id="txtFechaCreacionInicio"  onkeydown="return false">
                                </td>
                                <td>
                                    <input type="date" style="width: 95%;" id="txtFechaCreacionFinal"  onkeydown="return false">
                                </td>
                                <td class="titulos">
                                   <label for="chkAdmitidosPorUsuario">Solo Admitidos Por Mi:<label><input type='checkbox' name='chkAdmitidosPorUsuario' id='chkAdmitidosPorUsuario' checked>
                                </td>
                            </tr>
                                    
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table width="100%">
                            <tr>
                                <td style="width: 33%;"><hr></td>
                                <td colspan="4" align="CENTER">
                                    <!-- <input type="button" class="small button blue" value="INGRESAR PACIENTE" onclick="verificarNotificacionAntesDeGuardar('ingresarPacienteURG');"/> -->                         
                                    <input type="button" class="small button blue" value="BUSCAR" onclick="buscarAGENDA('listaPacientesUrgencias', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"/>  
                                    <input type="button" class="small button blue" value="LIMPIAR" onclick="limpiarCamposURG('limpiarDatosBusquedaURG');"/>  
                                </td>   
                                <td style="width: 33%;"><hr></td>                     
                            </tr>

                        </table> 
                    </td>
                </tr>
                <tr>
                    <td colspan="6">  
                        <div id="divContenedorListaIngresos" style="width: 100%;">
                            <table id="listaPacientesUrgencias" class="scroll" width="100%"></table>  
                        </div>   
                    </td>
                </tr>
            </table>
        </div> 
    </div>    
</div>

<div id="divVentanitaHistoricosAtencion" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:40%;margin-left: -25%; width:70%; overflow: auto;height: 80vh;">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaHistoricosAtencion')" />
                </td>
                <td width="80%"></td>
                <td align="right" width="10%">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaHistoricosAtencion')" />
                </td>
            </tr>
            <tr class="titulos" hidden>
                <td></td>
                <td>HISTORICOS ATENCION</td>
                <td>  
                    IdAuxiliar=<label id="lblIdAuxiliar"></label>
                    IdPaciente=<label id="lblIdPaciente" style="size:1PX"></label>
                    Administradora=<label id="lblIdAdministradora" style="size:1PX"></label>

                    Especialidad<label style="font-size:10px" id="lblIdEspecialidad">1</label>
                </td>
            </tr>
            <!--<tr class="titulos">
                <td colspan="3">
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                        <tr class="titulos overs">
                            <td>Folio a crear</td>		
                        </tr>
                        <tr class="estiloImput overs">
                            <td >
                                <label id="lblIdDocumento"></label>
                                <select size="1" id="cmbTipoDocumento" style="width:40%" onFocus="comboCreacionDedocumentos()"
                                        title="80">
                                </select>
                                <input type="text" id="txtIdEsdadoDocumento" style="width:1%" disabled="disabled" />
                                <label id="lblNomEstadoDocumento" style="size:1PX"></label>
                                <label class="parpadea text"  style="color:#F00;font-weight:bold"
                                       id="lblSemeforoRiesgo"></label>
                            </td>
                        </tr>                        
                        <tr class="estiloImput overs">
                            <td width="100%" >
                                <input id="btnProcedimiento_a" type="button" class="small button blue" value="CREAR FOLIO" title="TT65" onClick="verificarNotificacionAntesDeGuardar('crearFolio')" />
                                <input type="button" onclick="cerrarDocumentClinicoSinImp()" value="FINALIZAR SIN IMPRIMIR" title="btn587" class="small button blue" id="btnFinalizarDocumento_">        
                                <input type="hidden" id="txtIdProfesionalElaboro" />
                            </td>
                        </tr>             
                    </table>
                </td>
            </tr> -->
            <tr class="titulosListaEspera">
                <td colspan="3">HISTORIA CLINICA DE ADMISION</td>
            </tr>
            <tr class="titulos">
                <td colspan="3">
                    <div id="divdatosbasicoscredesa" style="height:180px; width:100%; overflow-y: scroll;">
                        <table id="listDocumentosHistoricosAdm">
                            <tr><th></th></tr>                     
                        </table>
                    </div> 
                </td>
            </tr>

            <tr class="titulos"></tr>
            <td colspan="3"> 
                <div id="divAcordionDiagnosticos_" style="display:BLOCK">
                    <jsp:include page="../../hc/hc/divsAcordeon.jsp" flush="FALSE"> 
                        <jsp:param name="titulo" value="Diagn&oacute;stico"/>
                        <jsp:param name="idDiv" value="divDiagnosticos_"/>
                        <jsp:param name="pagina" value="../../hc/hc/diagnosticos.jsp"/>
                        <jsp:param name="display" value="NONE"/>
                        <jsp:param name="funciones" value="buscarHC('listDiagnosticos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                    </jsp:include>
                </div>
            </td>
            </tr>                   

            <tr class="titulos">
                <td colspan="3"> 
                    <div id="divAcordionProcedimientos" style="display:BLOCK">
                        <jsp:include page="../../hc/hc/divsAcordeon.jsp" flush="FALSE"> 
                            <jsp:param name="titulo" value="Conducta y tratamiento"/>
                            <jsp:param name="idDiv" value="divProcedimientos" />
                            <jsp:param name="pagina"  value="../../hc/hc/contenidoPlanTratamiento.jsp"/>
                            <jsp:param name="display" value="NONE" />
                            <jsp:param name="funciones" value="cargarTablePlanTratamiento()" />
                        </jsp:include>
                    </div>
                </td>
            </tr>              




        </table>
    </div>
</div>


<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:50%;margin-left: -33%; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divAcordionAdjuntos" style="display:BLOCK">      
                        <div id="divParaArchivosAdjuntos"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>


<div id="divCrearPacienteUrgencias"  style="display:none; z-index:2000; top:100px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:200px; width:70%">  
        <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divCrearPacienteUrgencias')" /></td>              
                <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divCrearPacienteUrgencias')" /></td>  
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Identificación</td>	                                                                                               
                            <td width="10%">Apellido 1</td>                                 
                            <td width="10%">Apellido 2</td>   
                            <td width="10%">Nombre 1</td> 
                            <td width="10%">Nombre 2</td>
                            <td width="40%">Acompañante</td>                                                               
                        </tr>		
                        <tr class="estiloImput"> 
                            <td>
                                <strong><label id="lblTipoIdCrearPacienteURG"></label></strong>
                                <strong><label id="lblIdCrearPacienteURG"></label></strong>
                            </td>    
                            <td><input type="text" onkeyup="javascript: this.value = this.value.toUpperCase();" id="txtApellido1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>                            
                            <td><input type="text" onkeyup="javascript: this.value = this.value.toUpperCase();" id="txtApellido2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>                                                                                                                                 
                            <td><input type="text" onkeyup="javascript: this.value = this.value.toUpperCase();" id="txtNombre1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>      
                            <td><input type="text" onkeyup="javascript: this.value = this.value.toUpperCase();" id="txtNombre2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>   
                            <td><input type="text" onkeyup="javascript: this.value = this.value.toUpperCase();" id="txtAcompananteUrgencias" style="width:95%" onblur="v28(this.value, this.id);"></td>                            
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table>
                                    <tr class="titulos"> 
                                        <td width="10%">Sexo</td>	
                                        <td width="15%">Fecha Nacimiento</td>
                                        <td width="35%">Telefonos</td>  
                                        <td width="30%">Administradora EPS</td>                                                                                                                                                                     
                                    </tr>		
                                    <tr class="estiloImput"> 
                                        <td>
                                            <select id="cmbSexoUrgencias" style="width:95%">	                                        
                                                <option value=""> </option>  
                                                <option value="F">FEMENINO</option> 
                                                <option value="M">MASCULINO</option>                              
                                                <option value="O">OTRO</option>                                                          
                                            </select>	                   
                                        </td>
                                        <td><input style="width:95%" type="text" id="txtFechaNacimientoUrgencias"/></td>      
                                        <td>
                                            <input type="text"  id="txtTelefonoUrgencias" style="width:30%"  size="50"  maxlength="50"  >
                                            <input type="text"  id="txtCelular1Urgencias" style="width:30%"  size="50"  maxlength="50"  >
                                            <input type="text"  id="txtCelular2Urgencias" style="width:30%"  size="50"  maxlength="50"  >
                                        </td>                                                             
                                        <td><input type="text"  id="txtAdministradoraUrgencias" onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraUrgencias', 308)" style="width:95%"/>                                  	 </td>                                                                                                                                                                
                                    </tr>  
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table width=681.9>
                                    <tr class="titulos"> 
                                        <td width="30%">Municipio</td> 
                                        <td width="30%">Direccion</td>   
                                        <td width="20%">Regimen Afiliacion</td>
                                        <td width="20%">Tipo Regimen</td>
                                    </tr>		
                                    <tr class="estiloImput"> 
                                        <td><input type="text" id="txtMunicipioResidenciaUrgencias"  style="width:85%">
                                            <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipioResidenciaUrgencias', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                                            <div id="divParaVentanita"></div>                                                    
                                        </td>    
                                        <td><input type="text" id="txtDireccionUrgencias" style="width:95%" onblur="v28(this.value, this.id);" ></td>                            
                                        <td>
                                            <select id="cmbRegimenUrgencias" style="width:95%">
                                                <option value="A">PARTICULAR</option>
                                                <option value="C">CONTRIBUTIVO</option>
                                                <option value="S">SUBSIDIADO</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="cmbTipoRegimenUrgencias" style="width:95%">
                                                <option value="0">OTRO</option>
                                                <option value="1">BENEFICIARIO</option>
                                                <option value="2">COTIZANTE</option>
                                            </select> 
                                        </td>
                                    </tr> 
                                </table> 
                            </td>
                        </tr>
                        <tr class="titulos">
                            <td align="CENTER" colspan="6"> 
                                <input type="button" class="small button blue" value="CREAR PACIENTE" onclick="modificarCRUD('crearPacienteUrgencias');"/>                         
                            </td>
                        </tr>
                    </table> 
                </td>
            </tr>
        </table>  
    </div>     
</div>


<div id="divAsociarCita" style="display:none; z-index:2000; top:100px; left:50px;">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:50%; width:70%;margin-left: -34%;">  
        <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divAsociarCita')" /></td>              
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divAsociarCita')" /></td>  
            </tr>
            <tr class="titulosCentrados" >
                <td width="100%" colspan="2">Asociar citas a la admision actual</td>
                <input type="button" value="buscar" onclick="buscarAGENDA('citasDisponiblesAsociar'); buscarAGENDA('citasAdmision')">
            <tr>
            <tr>
                <td colspan="2"><hr></td>
            </tr>
            <tr class="titulos">
                <td colspan="2">ID ADMSION ACTUAL:<label id="lblIdAdmisionAsociarCita" style="color: red;"></label> / ID FACTURA:<label id="lblIdFacturaAsociarCita"></label>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr class="titulosCentrados">
                            <td>CITAS PENDIENTES</td>
                            <td>CITAS CON ADMISION</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" id="citasDisponiblesAsociar"></table>
                            </td>
                            <td>
                                <table width="100%" id="citasAdmision"></table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="CENTER">
                    <input type="button" class="small button blue" value="ASOCIAR CITAS SELECCIONADAS" onclick="verificarNotificacionAntesDeGuardar('asociarCitasAdmision')" style="width: 40%;"/>              
                </td>
                <td>
                    <input type="button" class="small button blue" value="VER FORMATO DE FIRMAS" onclick="imprimirPDFFormatoFirmas()" style="width: 40%;"/>                         
                </td>
            </tr>
        </table>  
    </div>  
</div>

<div id="divOrdenesExternas" style="display:none; z-index:2000; top:100px; left:50px;">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:absolute; top:10%;left:50%;margin-left: -25%;">  
        <table width="90%" class="fondoTabla"> 
            <tr class="estiloImput">
                <td align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divOrdenesExternas')"/></td>              
                <td align="right" colspan="5"><input type="button" align="right" value="CERRAR" onclick="ocultar('divOrdenesExternas')"/></td>  
            </tr>
            <tr class="titulosListaEspera">
                <td width="5%">LABORATORIO</td>
                <td width="30%">PROCEDIMIENTO</td>
                <td width="5%">CANTIDAD</td>
                <td width="10%">NECESIDAD</td>
                <td width="20%">INDICACI&Oacute;N</td>
                <td width="30%">DIAGNOSTICO ASOCIADO</td>
                <td width="5%"></td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td>
                    <input type="checkbox" id="cmbTipoProcedimientoExterno" onchange="busquedaExternos()">
                </td>
                <td>
                    <input id="txtIdProcedimientoOrdenExt"
                           oninput="llenarElementosAutoCompletarKey(this.id, 204, this.value)"
                           style="width:95%" >

                    <input id="txtIdProcedimientolabOrdenExt"
                        oninput="llenarElementosAutoCompletarexternos(this.id, 10366, this.value)"
                        style="width:95%" hidden = true >
                    
                </td>
                <td>
                    <input type="number" id="txtCantidadOrdenExt" style="width:70%">
                </td>
                <td>
                    <select id="cmbNecesidadOrdenExt" style="width:90%">
                        <option value=""></option>  
                        <%  resultaux.clear();
                            ComboVO cmbPro;
                            resultaux = (ArrayList) beanAdmin.combo.cargar(113);
                            for (int k = 0; k < resultaux.size(); k++) {
                                cmbPro = (ComboVO) resultaux.get(k);%>
                        <option value="<%= cmbPro.getId()%>" title="<%= cmbPro.getTitle()%>"><%=cmbPro.getDescripcion()%></option><%}%>             
                    </select> 
                </td>
                <td class="titulos">
                    <textarea id="txtIndicacionOrdenExt" rows="2" style="width:95%" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea>
                </td>
                <td>
                    <input id="txtDxRelacionadoOrdenExt"
                           oninput="llenarElementosAutoCompletarKey(this.id, 206, this.value)"
                           style="width:95%">
                </td>
                <td>
                    <input id="btnProcedimiento" type="button" class="small button blue" value="ADICIONAR" onclick="modificarCRUD('crearOrdenExternaAdmision')"/>
                </td>
            </tr>                                
            <tr class="titulos">
                <td colspan="6">
                    <div id="divContenedorVentanaOrdenExterna" style="width: 800px;">
                        <table id="listaProcedimientosOrdenExternaAdmision"></table>
                    </div> 
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td colspan="6" align="CENTER">
                    <input type="button" id="btnFinalizarOrdenExterna" class="small button blue" value="FINALIZAR" onclick="modificarCRUD('finalizarOrdenExternaAdmision')" />
                </td>
            </tr>
        </table>
    </div>  
</div>

<div id="divmodificarAdmisionPacienteURG" style="display:none; z-index:2000; top:100px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:200px; width:60%">  
        <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divmodificarAdmisionPacienteURG')" /></td>              
                <td align="right" colspan="6"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divmodificarAdmisionPacienteURG')" /></td>  
            </tr>
            <tr class="titulos">
                <td colspan="7">
                    <table width=100%>
                        <tr>
                            <td>ID PACIENTE: <label id="lblIdPacienteModificarURG" style="color: red;"></label></td>
                            <td><strong><label id="lblIdentificacionPacienteModificarURG" style="width: 95%;"></label></strong></td>
                            <td><strong><label id="lblNombrePacienteModificarURG" style="width: 95%;"></label></strong></td>
                            <td>ID ADMSION:<label id="lblIdAdmisionModificarURG"></label> / ID FACTURA:<label id="lblIdFacturaModificarURG"></label>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7"><hr></td>
            </tr>
            <tr class="titulos">
                <td width="10%">SERVICIO</td>
                <td width="30%">ADMISION ACTUAL</td>
                <td width="20%">PROFESIONAL</td>
                <td width="10%">PREFERENCIAL</td>
                <!--<td width="5%">FECHA EGRESO</td>-->
                <td width="10%">ESTADO</td>
                <td></td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <strong><label id="lblIdServicio"></label>:<label id="lblServicio"></label></strong>
                </td>
                <td>
                    <select id="cmbIdTipoAdmisionModificarURG" style="width: 95%;"> 
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <select id="cmbProfesionalAdmision" style="width: 95%;">
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <select id="cmbModificarPreferencialURG" style="width: 90%;"> 
                        <option value="S">SI</option>
                        <option value="N">NO</option>
                    </select>
                </td>
                <!--<td>
                    <input type="date" value="<%=LocalDate.now()%>" id="txtFechaCierre">
                </td>-->
                <td>
                    <select style="width: 95%;" id="cmbModificarEstadoAdmisionURG">
                        <option value=""></option>
                        <%
                            resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(158);
                            for (int k = 0; k < resultaux.size(); k++) {
                                cmbM = (ComboVO) resultaux.get(k);
                        %>
                        <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                            <%=cmbM.getDescripcion()%></option>
                            <%}%>
                    </select>
                </td>
                <td>
                    <input type="button" class="small button blue" value="MODIFICAR" onclick="verificarNotificacionAntesDeGuardar('validarPendienteFactura');" style="width: 90%;"/>                         
                </td>
            </tr>
            <tr class="titulosCentrados">
                <td colspan="7">Ordenes pendientes por gestionar</td>
            </tr>
            <tr>
                <td colspan="7">
                    <table style="width: 100%;" id="ordenesNoGestionadasURG"></table>
                </td>
            </tr>
            <tr>
                <td><hr></td>
                <td align="CENTER" colspan="5">
                    <input type="button" class="small button blue" value="VER ADMISION" onclick="traerAdmisionPacienteURG()" style="width: 90%;"/>                         
                </td>
                <td><hr></td>
            </tr>
        </table>  
    </div>  
</div>

<div id="divVentanitaEliminarOrdenExt" style="display:none; z-index:2000;">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2004; position:absolute; top:500px;left:70px; width:90%">
        <table width="50%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                        onclick="ocultar('divVentanitaEliminarOrdenExt')" /></td>
                <td align="right" colspan="2"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                        onclick="ocultar('divVentanitaEliminarOrdenExt')"/></td>
            </tr>
            <tr class="titulosListaEspera">
                <td colspan="2" align="CENTER">DESEA ELIMINAR ESTA ORDEN ?</td>
            </tr>
            <tr class="estiloImput">
                <td align="CENTER">
                    <input type="button" class="small button blue"
                           value="ELIMINAR"
                           onclick="modificarCRUD('eliminarOrdenExternaAdmision')"/> 
                </td>
                <td align="CENTER">
                    <input type="button" class="small button blue"
                           value="CANCELAR"
                           onclick="ocultar('divVentanitaEliminarOrdenExt')"/>
                </td> 
            </tr>
        </table>
    </div>
</div>

<label id="lblgestante" hidden="true"></label>

<div class="bubble" id="bubble" onclick="maximizarVentanita('divInfoPacienteVentana')">
    <i class="fa-solid fa-address-card"></i>
</div>

<div id="divInfoPacienteVentana" style="display: none; top: 75px; right: 3%; width: 170px; position: fixed; white-space: normal;" onclick="moverTarjeta()">
    <div class="card-container">
  
        <div class="interno-card" id="miTarjeta">
          <div class="class-card barbarian">
            <div class="class-card__image class-card__image--barbarian">
                <!-- <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divInfoPacienteVentana')" /> -->
                <label onclick="ocultar('divInfoPacienteVentana')" class="cerrarInfoPaciente">
                    <i class="fas fa-times-circle"></i>
                </label>
                <label onclick="minimizarVentanita('divInfoPacienteVentana')" class="minimizarInfoPaciente">
                    <i aria-hidden="true" class="fas fa-chevron-circle-down" ></i>
                </label>
              <i class="fas fa-hospital-user fasCard"></i>
            </div>
            <div class="class-card__level class-card__level--barbarian">NOMBRE PACIENTE:</div>
            <div class="class-card__unit-name"><label id="lblNombrePacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label></div>
            <div class="grRh">
                <label for="">RH:</label>
                <label for="" id="lblRH"></label>
            </div>
            <div class="class-card__unit-description">
                <label id="lblEdadPacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label><label class="" style="color:white;"></label>
            </div>
            <div class="parentCard">

                <div id="divDatosGestantes" style="display: flex; padding-top: 5%; margin-left: 13%; gap:5px;">
                    <label style="color:rgb(231, 58, 58);"> <b>Gestante</b> </label>

                    <div class="onoffswitch2" >
                        <input type="checkbox" name="chkgestante" class="onoffswitch-checkbox2" id="chkgestante"  onclick="valorSwitchsn(this.id, this.checked);guardarGestanteVentanita(document.getElementById('lblIdPaciente').innerHTML , this.value,'ventana principal'); ">  
                        <label class="onoffswitch-label2" for="chkgestante">
                            <span  class="onoffswitch-inner2" id="chkgestante-span1"></span>
                            <span  class="onoffswitch-switch2" id="chkgestante-span2"></span>
                        </label>
                    </div>

                </div>

                <div>
                    <!-- <h5>Creatinina</h5> -->
                    <p><label id="lblCreatininaVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
                </div>
                <div>
                    <!-- <h5>TFG Actual</h5> -->
                    <p><label id="lblTFGVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
                </div>
                <div>
                    <!-- <h5>GRADO</h5> -->
                    <p><label id="lblEstadioVentanita" class="" style="color:rgb(1, 1, 1);"></label></p>
                </div>
            </div>      
          </div> <!-- end class-card barbarian-->
        </div> <!-- end wrapper -->
      </div> <!-- end container -->
</div>



<input type="hidden" id="txtSoloAdmision" value="SI" /> 
<input type="hidden" id="txtIdEspecialidad" /> 
<input type="hidden" id="txtEsCirugia" /> 
<input type="hidden" id="txtAdmisiones" value="SI" /> 
<input type="hidden" id="txtEstadoAdmisionFactura" /> 

<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />       
<input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
<input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />   
<input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />    
<input type="hidden" id="txtPrincipalHC" value="NO"/> 