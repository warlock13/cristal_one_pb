<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%  beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<div>
    <div class="btitulos">
        <label class="bloque w20">Tipo</label>
        <label class="bloque w10">Codigo</label>
        <label class="bloque w40">Observaciones</label>
        <label class="bloque w10">Cantidad</label>
        <label class="bloque w15">Valor Total</label>
        <label class="bloque w10"> </label>
    </div>

    <div class="bestiloImput">

        <div class="bloque w20">
            <select id="cmbIdDetalleOtro" class="w90"   title="58">
                <option value="">[ SELECCIONE ]</option>
                <% resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(243);
                    ComboVO cmbAo;
                    for (int k = 0; k < resultaux.size(); k++) {
                  cmbAo = (ComboVO) resultaux.get(k);%>
                <option value="<%= cmbAo.getId()%>" title="<%= cmbAo.getTitle()%>">
                    <%= cmbAo.getDescripcion()%>
                </option>
                <%}%>
            </select>
        </div>
        <input type="text" id="txtCodigoExternoOtro" class="bloque w10"  />
        <input type="text" id="txtObservacionOtro" class="bloque w40"  />
        <input type="number" id="txtCantidadDetalleOtro" class="bloque w10"  />
        <input type="number" id="txtValorTotalOtro" class="bloque w15" />
        <div class="bloque w10">
            <input type="button" class="small button blue" value="ADICIONAR" title="BTO12" onclick="modificarCRUD('insertarDetalleFacturaOtro');" />
        </div>
    </div>
    <div style="overflow:auto; height:120px; width:100%" id="divListOtrosDetallesFactura">             
        <table id="listaOtrosDesatallesFactura" style="width:100%" class="scroll"></table>  
    </div>  
</div>


<div id="divVentanitaArticuloVentasOtro" class="ventanita">
    <div class="transParencia fondoVentanita">
    </div>
    <div class="fondoVentanitaContenido w40 t200 l250 fondoTabla">
        <div>
            <div class="estiloImput">
                <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaArticuloVentasOtro')" style="float: left; " />
                <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaArticuloVentasOtro')" style=" float: right;" />
            </div>

            <div class="btitulos">
                <label class="bloque w20">ID DETALLE</label>
                <label class="bloque w20">ID ARTICULO</label>
                <label class="bloque w60">ARTICULO</label>
            </div>

            <div class="bestiloImput">
                <label id="lblIdDetalleVentasOtroVentanita" class="bloque w20"></label>
                <label id="lblIdArticuloVentasOtroVentanita" class="bloque w20"></label>
                <label id="lblNomArticuloVentasOtroVentanita" class="bloque w60"></label>
            </div>

            <div class="estiloImputListaEspera">
                <input title="BTVA1" type="button" class="small button blue" value="ELIMINAR ARTICULO" onclick="modificarCRUD('eliminarArticuloVentasOtro')"  />
            </div>

            <div class="btitulos">
                <label class="bloque w15">VALOR</label>
                <label class="bloque w15">PORCENTAJE</label>
                <label class="bloque w20">VALOR DESCUENTO</label>
                <label class="bloque w50"></label>
            </div>

            <div class="bestiloImput">
                <label id="lblValorBaseOtroVentanita" class="bloque w15"></label>
                <select id="cmbPorcentajeDescuentoOtroVentanita" class="bloque w15" onchange="calculoDescuentoPorcentajeVentasOtro()"  >
                    <option value=""></option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                    <option value="40">40</option> 
                    <option value="45">45</option>
                    <option value="50">50</option>
                </select> 

                <div class="bloque w20">
                    <input type="text" id="txtValorDescuentoOtroVentanita"  maxlength="10"  class="w80"   readonly>
                </div>

                <div class="bloque w50">
                    <input title="BTVA1" id="btnAplicarDescuentoVentanita" type="button" class="small button blue" value="APLICAR DESCUENTO" onclick="modificarCRUD('modificarTransaccionVentasOtro')"  />
                    <input title="BTVA2" id="btnRevertirDescuentoVentanita" type="button" class="small button blue" value="REVERTIR DESCUENTO" onclick="modificarCRUD('revertirTransaccionVentasOtro')"  />
                    <label id="lblValorDescuentoOtroVentanita" ></label>
                </div>
            </div>
        </div>
    </div>
</div>





<div id="divVentanitaProcedimientosOtro" style="display:none; z-index:2000; top:400px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2004; position:fixed; top:300px;left:400px; width:70%">
        <table width="80%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                        onclick="ocultar('divVentanitaProcedimientosOtro')" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="5">&nbsp;</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                         onclick="ocultar('divVentanitaProcedimientosOtro')" /></td>
            </tr>
            <tr class="titulos">
                <td>ID</td>
                <td>CODIGO</td>
                <td>PROCEDIMIENTO</td>
                <td>CANTIDAD</td>
                <td>VALOR UNITARIO</td>
            </tr>

            <tr class="estiloImput">
                <td><label id="lblIdProcedimiento"></label></td>
                <td>
                    <input type="text" id="txtCodigoProcedimientoEditar" style="width:80%"  />
                </td>
                <td>
                    <input type="text" id="txtProcedimientoEditar" style="width:100%"  />
                </td>
                <td>
                    <input type="text" id="txtCantidadProcedimientoEditar" style="width:50%"  />
                </td>
                <td>
                    <input type="text" id="txtValorUnitarioProcedimientoEditar" style="width:50%"  />
                </td>
            </tr>


            <tr class="estiloImputListaEspera">
                <td colspan="9">
                    <input id="BTN_MO" title="BI101" type="button" class="small button blue"
                           value="MODIFICAR PROCEDIMIENTO"
                           onclick="modificarCRUD('modificarProcedimientoOtros', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                            />
                    <input id="BTN_MO" title="BI102" type="button" class="small button blue"
                           value="ELIMINAR PROCEDIMIENTO"
                           onclick="modificarCRUD('eliminarProcedimientoOtros', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                            />
                </td>
            </tr>
        </table>
    </div>
</div>