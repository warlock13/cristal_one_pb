<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn());

    ArrayList resultaux = new ArrayList();
%>

<table>
    <tr class="titulosListaEspera">
        <td width="40%">Procedimiento</td>
        <td width="10%">Valor Unitario</td>
        <td width="10%">Cantidad</td>
        <td width="10%">N&uacute;mero de autorizaci&oacute;n</td>
        <td width="10%">Ejecutado</td>
        <td width="10%"></td>
        <td width="10%">Ordenes</td>
        <td width="10%">Cirugia</td>
    </tr>
    <tr class="titulosListaEspera">
        <td>
            <input type="text" id="txtIdProcedimientoCex" size="180" onblur="traerValorUnitarioProcedimientoCuenta()"
                   onkeypress="llamarAutocomIdDescripcionParametro('txtIdProcedimientoCex', 420, 'lblIdPlanContratacion')"
                   style="width:99%" />
        </td>
        <td>
            <label id="lblClasifProced"></label>
            <strong><label id="lblValorUnitarioProc"></label></strong>
        </td>
        <td>
            <select id="cmbCantidad" style="width:50%" >
                <option value="1" title="1">1</option>
                <% for (int k = 2; k < 16; k++) {%>
                <option value="<%= k%>" title="<%= k%>"><%= k%></option>
                <%}%>                                                           
            </select>                                  
        </td>  
        <td>
            <input type="text" id="txtNoAutorizacionProcedimiento"  style="width:80%" />
        </td>

        <td>
            <select id="cmbEjecutado" style="width:50%"  >
                <option value="1">SI</option>
                <option value="2">NO</option> 
            </select>                                  
        </td>               
        <td>                     
            <input id="btnAdicionarFactura" type="button" class="small button blue" value="ADICIONAR" title="BTXX2" onclick="modificarCRUD('adicionarProcedimientosDeFactura');"   />
        </td>
        <td>
            <input id="btnProgramcacionPendiente" type="button" class="small button blue" value="HISTORIA CLINICA" onclick="mostrarOrdenesProgramadas()"/>
        </td>
        <td>
            <table width="100%">
                <tr class="titulosListaEspera">
                    <td>
                        <input id="btnContratados" type="button" class="small button blue" value="CONTRATADOS" title="BUU" onclick="mostrar('divVentanitaLiquidacionContratadas');buscarFacturacion('listGrillaProcedimientosContratados')"   />                               
                    </td>
                </tr>
                <tr class="titulosListaEspera">
                    <td>
                        <input id="btnTarifarios" type="button" class="small button blue" value="TARIFARIOS" title="BUU" onclick="irLiquidarCirugia()"   />
                    </td>
                </tr>
            </table>
        </td>
    </tr>  
    <tr>   
        <td width="100%" colspan="8">
            <div style="overflow:auto; height:200px; width:100%" id="divCitaProcedimientoConsultaExterna">             
                <table id="listProcedimientosDeFactura" style="width:100%" class="scroll"></table>  
            </div>    
        </td>
    </tr>    
</table>

<div id="divElementosPendientes"  style="display:none; z-index:2000; top:400px; left:20px;">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div  style="z-index:2004; position:fixed; top:200px; left:200px; width:100%">  
        <table width="70%" height="120"  cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput" >
                <td width="20%" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divElementosPendientes')" /></td>            
                <td width="20%" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divElementosPendientes')" /></td>  
            </tr> 
            <tr>            
                <td colspan="2">
                    <div id="tabsElementosPendientes" style="width: 99%; height:450px">
                        <ul>
                            <li><a href="#divHcPendiente" onclick="buscarFacturacion('listaHcSinFactura')">
                                    <center>PENDIENTE</center>
                                </a></li>
                            <!--<li><a href="#divHcFacturada" onclick="buscarFacturacion('listaHcSinFactura')">
                                    <center>FACTURADO</center>
                                </a></li>-->
                            <li><a href="#divHcAnulada" onclick="buscarFacturacion('listaHcAnuladaFacturacion')">
                                    <center>ANULADO</center>
                                </a></li> 
                        </ul> 

                        <div id="divHcPendiente"> 
                            <table width="100%">
                                <tr class="titulos">            
                                    <td colspan="3">
                                        <table id="listaHcSinFactura" class="scroll"></table> 
                                    </td>
                                </tr>
                                <tr class="titulosListaEspera">
                                    <td>
                                        <input type="button" class="small button blue" value="ASOCIAR A LA FACTURA ACTUAL" onclick="modificarCRUD('insertarFacturaDetalleEvolucion');"/>  
                                    </td>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td width="70%">
                                                    <textarea id="txtObservacionesAnulacionHc" rows="1" style="width: 95%;" placeholder="Observaciones"></textarea>
                                                </td>
                                                <td width="30%" align="left">
                                                    <input type="button" class="small button blue" value="ANULAR" onclick="modificarCRUD('anularProcedimientosHc');"/> 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>  
                            </table> 	 
                        </div>

                        <!--<div id="divHcFacturada"> 
                            <table width="100%">
                                <tr class="titulos">            
                                    <td colspan="3">
                                        <table id="listaHcSinFactura" class="scroll"></table> 
                                    </td>
                                </tr>
                                <tr class="titulosListaEspera">
                                    <td>
                                        <input type="button" class="small button blue" value="ASOCIAR A LA FACTURA ACTUAL" onclick="modificarCRUD('insertarFacturaDetalleEvolucion');"/>  
                                    </td>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td width="70%">
                                                    <textarea id="txtObservacionesAnulacionHc" rows="1" style="width: 95%;" placeholder="Observaciones"></textarea>
                                                </td>
                                                <td width="30%" align="left">
                                                    <input type="button" class="small button blue" value="ANULAR" onclick="modificarCRUD('anularProcedimientosHc');"/> 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>  
                            </table> 	 
                        </div>-->

                        <div id="divHcAnulada">
                            <table width="100%">
                                <tr class="titulos">            
                                    <td>
                                        <table width="100%" id="listaHcAnuladaFacturacion"></table> 
                                    </td>
                                </tr> 
                            </table> 
                        </div> 
                    </div>
                </td>
            </tr> 
        </table> 
    </div>    
</div>

<div id="divOrdenesFacturaDetalle"  style="display:none; z-index:2000; top:500px; left:50px;">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div  style="z-index:2004; position:fixed; top:200px; left:50%; width:100%;margin-left: -30%;">  
        <table width="70%" height="80"  cellpadding="0" cellspacing="0" class="fondoTabla" >
            <tr class="estiloImput" >
                <td width="20%" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultarDetalleFacturaDetalle()" /></td>            
                <td width="20%" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultarDetalleFacturaDetalle()" /></td>  
            </tr> 
            <tr>            
                <td colspan="2">
                    <div id="divHcFactura"> 
                        <table width="100%">
                            <tr class="titulos">            
                                <td>
                                    <table id="listaFacturaDetalleEvolucion" class="scroll"></table> 
                                </td>
                            </tr>
                            <tr class="titulosListaEspera">
                                <td>
                                    <input type="button" class="small button blue" value="ASOCIAR EN OTRA FACTURA" onclick="modificarCRUD('eliminarFacturaDetalleEvolucion');"/>  
                                </td>
                            </tr>  
                        </table> 	 
                    </div>
                </td>
            </tr> 
        </table> 
    </div>  
</div>




