<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session --> 


<%  beanAdmin.setCn(beanSession.getCn()); 

ArrayList resultaux = new ArrayList();
%>

<table width="100%" align="center">
    <tr>
        <td>
            <table with="100%">
                <tr class="estiloImputListaEspera">
                    <td >
                        Observación  
                    </td>
                    <td>
                        <textarea id="txtObservacionFactura" cols="100" rows="3" style="width: 100%;"></textarea>
                    </td>
                    <td>
                        <input type="button" class="small button blue"
                            value="GUARDAR" onclick="modificarCRUD('guardarObservacionFactura')"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="titulosListaEspera">
        <td width="45%">
            <table class="tablaFacturacion">
                <!--<tr>
                    <td width="40%" align="right">MEDIO DE PAGO</td>
                    <td width="30%">
                        <label>EFECTIVO</label><input type="radio" name="medioPago" id="efectivo" value="1" onchange="modificarCRUD('cambiarMedioPago')" checked>
                    </td>
                    <td width="30%">
                        <label>TARJETA</label><input type="radio" name="medioPago" id="tarjeta" value="2" onchange="modificarCRUD('cambiarMedioPago')">
                    </td>
                </tr>-->
                <tr>
                    <td align="right">ID FACTURA</td>
                    <td >
                        <label id="lblIdFactura"></label>
                    </td>
                    <td >
                    </td>
                </tr>
                <tr>
                    <td align="right">ESTADO FACTURA</td>
                    <td>
                        <label id="lblIdEstadoFactura"></label> -
                        <label id="lblEstadoFactura"></label>
                    </td>
                    <td>
                        <input type="button" class="small button blue"
                            value="&nbsp;&nbsp;&nbsp;&nbsp;VER FACTURA&nbsp;&nbsp;&nbsp;&nbsp;" title="VF24"
                            onclick="numerarFactura()"  />
                    </td>
                </tr>
                <tr>
                    <td align="right">NUMERO FACTURA</td>
                    <td>
                        <label id="lblNumeroFactura"></label>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="40%" align="right">FORMA DE PAGO</td>
                    <td width="30%">
                        <label>CONTADO</label><input type="radio" name="formaPago" id="contado" value="1" onchange="modificarCRUD('modificarFormaPago')" checked>
                    </td>
                    <td width="30%">
                        <label>CREDITO</label><input type="radio" name="formaPago" id="credito" value="2" onchange="modificarCRUD('modificarFormaPago')">
                    </td>
                </tr>
            </table>
        </td>
        <td width="45%" valign="top">
            <table class="tablaFacturacion">
                <tr style="font-weight: bold;">
                    <td width="40%" align="right">TOTAL FACTURA</td>
                    <td width="40%">
                        <label id="lblTotalFactura"></label>
                    </td>
                    <td width="20%"></td>
                </tr>
                <tr>
                    <td align="right">VALOR NO CUBIERTO</td>
                    <td>
                        <label id="lblValorNoCubierto"></label>
                    </td>
                    <td>
                        <input type="button" class="small button blue" value="DESCUENTO" title="VF25"
                            onclick="mostrarDivVentanaDescuento()"  />
                    </td>
                </tr>
                <tr>
                    <td align="right">VALOR CUBIERTO</td>
                    <td>
                        <label id="lblValorCubierto"></label>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right">DESCUENTO</td>
                    <td>
                        <label id="lblDescuento"></label>
                    </td>
                    <td></td>
                </tr>
            </table>
        </td>
    </TR>
</table>


<div id="divVentanitaLiquidacionCirugia" style="display:none; z-index:2000; top:1px; left:211px;">

    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2004; position:fixed; top:130px; left:50%; width:70%;margin-left: -30%;">
        <table width="99%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaLiquidacionCirugia')" /></td>
                <td colspan="3">LIQUIDAR CIRUGIA</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaLiquidacionCirugia')" /></td>
            </tr>
            <tr class="titulos">
                <td width="20%">ID LIQUIDACION</td>
                <td width="20%">ANESTESIOLOGO</td>
                <td width="20%">TIPO ANESTESIA</td>
                <td width="20%">FINALIDAD</td>
                <td width="20%">TIPO DE ATENCION</td>
            </tr>
            <tr class="estiloImput">
                <td> <label id="lblIdLiquidacion"></label></td>
                <td>
                    <select id="cmbIdAnestesiologo" style="width:80%" 
                        onfocus="comboDependiente('cmbIdAnestesiologo', 'lblIdAdmision', '56')">
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <select id="cmbIdTipoAnestesia" style="width:90%"
                        onfocus="comboDependiente('cmbIdTipoAnestesia', 'lblIdCita', '539')" >
                        <option value=""></option>
                        <option value="1">LOCAL</option>
                        <option value="3">GENERAL</option>
                        <option value="4">BLOQUEO</option>
                    </select>
                </td>
                <td>
                    <select id="cmbIdFinalidad" style="width:90%" >
                        <option value=""></option>
                        <option value="1">Diagnostico</option>
                        <option value="2" selected="selected">Terapeutico</option>
                        <option value="3">Proteccion especifica</option>
                        <option value="4">Deteccion Temprana de Enfermedad General</option>
                        <option value="5">Deteccion Temprana de Enfermedad Profesional</option>
                    </select>
                </td>
                <td>
                    <select id="cmbIdTipoAtencion" style="width:90%" >
                        <option value="1">Ambulatoria</option>
                        <option value="4">Cirugia ambulatoria</option>
                    </select>
                </td>
            </tr>

            <tr class="titulos">
                <td>Honorarios Cirujano</td>
                <td>Anestesiologo</td>
                <td>Ayudante</td>
                <td>Derechos de Sala</td>
                <td>Materiales</td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <input id="chk_Cirujano" type="checkbox" />
                </td>
                <td>
                    <input id="chk_Anestesiologo" type="checkbox" />
                </td>
                <td>
                    <input id="chk_Ayudante" type="checkbox" />
                </td>
                <td>
                    <input id="chk_Sala" type="checkbox" />
                </td>
                <td>
                    <input id="chk_Materiales" type="checkbox" />
                </td>
            </tr>
            <tr class="titulos">
                <td>id_tarifario</td>
                <td>%Variabilidad</td>
                <td>Valor total</td>
                <td>liquida</td>
                <td>id_tipo_tarifario_base</td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <label id="lblIdTarifario_liq"></label>
                </td>
                <td>
                    <label id="lblVariabilidad_liq"></label>
                </td>
                <td>
                    <label id="lblValorTotal_liq"></label>
                </td>
                <td>
                    <label id="lblLiquida_liq"></label>
                </td>
                <td>
                    <label id="lblIdTipoTarifario_liq"></label>
                </td>
            </tr>

            <tr class="estiloImputListaEspera">
                <td colspan="1">
                    <input id="BTN_MO" title="BQ165" type="button" class="small button blue" value="CREAR LIQUIDACION"
                        onclick="modificarCRUD('crearLiquidacionCirugia');"  />
                </td>
                <td colspan="1">
                    <input id="BTN_MO" title="BQ166" type="button" class="small button blue"
                        value="ELIMINAR LIQUIDACION" onclick="modificarCRUD('eliminarLiquidacionCirugia');"
                         />
                </td>
                <td colspan="1">
                    <input id="BTN_MO" title="BQ185" type="button" class="small button blue" value="RE LIQUIDACION"
                        onclick="modificarCRUD('modificarLiquidacionCirugia');"  />
                </td>
                <td colspan="1">
                    <input id="BTN_MO" title="BQ175" type="button" class="small button blue" value="FACTURAR"
                        onclick="modificarCRUD('facturarLiquidacionCirugia');"  />
                </td>

                <td colspan="1">
                    .
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="5">
                    <table id="listLiquidacionQx" class="scroll" width="100%"></table>
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="5">
                    Procedimientos a liquidar
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="5">
                    <table id="listLiquidacionQxProcedimientos" class="scroll" width="100%"></table>
                </td>
            </tr>

        </table>
    </div>
</div>


<div id="divVentanitaLiquidacionProcedimientos" style="display:none; z-index:2010; top:500px; left:-211px;">

    <div class="transParencia" style="z-index:2013; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2014; position:fixed; top:200px; left:200px; width:60%">
        <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaLiquidacionProcedimientos')" />V.79</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaLiquidacionProcedimientos')" /></td>
            </tr>
            <tr class="titulos">
                <td>Id Procedimiento</td>
                <td>Procedimiento</td>
                <td>Lateralidad</td>
                <td>Via Acceso</td>
            </tr>
            <tr class="estiloImput">
                <td><label id="lblIdLiquiProced"></label></td>
                <td><label id="lblNomLiquiProced"></label></td>
                <td>
                    <select id="cmbIdLateralLiquiProced" style="width:90%" >
                        <option value="U">UNILATERAL</option>
                        <option value="B">BILATERAL</option>
                    </select>
                </td>
                <td>
                    <select id="cmbIdViaLiquiProced" style="width:90%" >
                        <option value="11">IGUAL VIA</option>
                        <option value="12">DIFERENTE VIA</option>
                    </select>
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td colspan="4">
                    <input id="BTN_MO" title="BT99E" type="button" class="small button blue" value="ELIMINAR"
                        onclick="modificarCRUD('eliminarProcLiquidacion')"  />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input id="BTN_MO" title="BT79L" type="button" class="small button blue" value="MODIFICAR"
                        onclick="modificarCRUD('modificarProcLiquidacion')"  />
                </td>

            </tr>
            <tr class="titulos">
                <td colspan="4">
                    Procedimientos a liquidar
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="4">
                    <table id="listLiquidacionQxProcedimientosDerechos" class="scroll" width="100%"></table>
                </td>
            </tr>
        </table>
    </div>
</div>


<div id="divVentanitaLiquidacionProcedimientosDerechos" style="display:none; z-index:2020; top:400px; left:-211px;">

    <div class="transParencia" style="z-index:2023; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2024; position:fixed; top:220px; left:500px; width:40%">
        <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaLiquidacionProcedimientosDerechos')" /></td>
                <td>Eliminar derechos para este procedimiento</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaLiquidacionProcedimientosDerechos')" /></td>
            </tr>
            <tr class="titulos">
                <td>Id Derecho</td>
                <td>Nombre Derecho</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="estiloImput">
                <td><label id="lblIdLiquiProcedDerecho"></label></td>
                <td><label id="lblNomLiquiProcedDerecho"></label></td>
                <td>&nbsp;</td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td colspan="3">
                    <input id="BTN_MO" title="BM99E" type="button" class="small button blue" value="ELIMINAR DERECHO"
                        onclick="modificarCRUD('eliminarProcLiquidacionDerecho')"  />
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="divVentanitaProcedimCuenta2" class="ventanita">
    <div class="transParencia fondoVentanita">
    </div>

    <div class="fondoVentanitaContenido w50 fondoTabla">

        <div class="estiloImput">
            <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaProcedimCuenta')"
                style="float: left; " />
            <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaProcedimCuenta')"
                style=" float: right;" />
        </div>

        <div class="btitulos">
            <label class="bloque w15">TRANSACCION</label>
            <label class="bloque w15">ID PROCEDIMIENTO</label>
            <label class="bloque w70">PROCEDMIENTO</label>
        </div>

    </div>

</div>

<div id="divVentanitaProcedimCuenta" style="display:none; z-index:2000; top:400px; left:50px; ">

    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2004; position:fixed; top:300px;left:170px; width:60%;left: 50%;margin-left: -25%;">
        <table width="85%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaProcedimCuenta')" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="5">&nbsp;</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaProcedimCuenta')" /></td>
            </tr>
            <tr class="titulos">
                <td width="5%">TRAN.</td>
                <td width="45%">PROCEDIMIENTO</td>
                <td width="5%">CANTIDAD</td>
                <td width="10%">NÚMERO DE AUTORIZACIÓN</td>
                <td width="30%">PROFESIONAL</td>
                <td width="5%">EJECUTADO</td>
            </tr>
            <tr class="estiloImput">
                <td><label id="lblIdTransac"></label></td>
                <td>
                    <input type="text" id="txtProcedimientoEditar" style="width: 90%;" onclick="llamarAutocomIdDescripcionParametro(this.id, 420, 'lblIdPlanContratacion')">
                    <img onclick="limpiaAtributo('txtProcedimientoEditar')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">
                </td>
                <td><input type="number" id="txtCantidadProcedimientoEditar" style="width:90%"></td>
                <td>
                    <input type="text" id="txtNoAutorizacionProcedimientoEditar" style="width:95%"  />
                </td>
                <td>
                    <select id="cmbIdProfesionalesFacturaEditar" onfocus="comboDependienteEmpresa('cmbIdProfesionalesFacturaEditar','86')" style="width:95%">
                        <option value=""></option>
                    </select> 

                </td>

                <td>
                    <select id="cmbEjecutadoEditar" style="width:95%">
                        <option value="1">SI</option>
                        <option value="2">NO</option>
                        <option value="3">PARCIAL</option>
                    </select>
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td colspan="9">
                    <input id="BTN_MO" title="BI101" type="button" class="small button blue"
                        value="MODIFICAR PROCEDIMIENTO"
                        onclick="modificarCRUD('modificarProcedimientoCuenta', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                         />
                    <input id="BTN_MO" title="BI102" type="button" class="small button blue"
                        value="ELIMINAR PROCEDIMIENTO"
                        onclick="modificarCRUD('eliminarProcedimientoCuenta', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                         />
                </td>
            </tr>
        </table>
    </div>
</div>


<div id="divVentanitaDescuentoFactura" style="display:none; z-index:2000; top:400px; left:50px;">

    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2004; position:fixed; top:300px; left:50%; width:80%;margin-left: -25%;">

        <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td width="10%" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaDescuentoFactura')" /></td>
                <td width="10%">&nbsp;</td>
                <td width="15%">&nbsp;</td>
                <td width="20%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="20%" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                        onclick="ocultar('divVentanitaDescuentoFactura')" /></td>
            </tr>
            <tr class="titulos">
                <td>Total</td>
                <td>Porcentaje</td>
                <td>Valor Descuento</td>
                <td>Quien autoriza</td>
                <td>Motivo</td>
                <td>Observación</td>
            </tr>
            <tr class="estiloImput">
                <td><label id="lblValorBaseDescuento"></label></td>
                <td>
                    <select id="cmbPorcentajeDescuentoCuenta" style="width:70%"
                        onchange="calculoDescuentoPorcentajeCuenta()" >
                        <option value=""></option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="25">25</option>
                        <option value="30">30</option>
                        <option value="35">35</option>
                        <option value="40">40</option>
                        <option value="45">45</option>
                        <option value="50">50</option>
                    </select>
                </td>
                <td><input type="text" id="txtValorDescuentoCuenta" size="10" maxlength="10" style="width:50%"
                         />
                </td>
                <td>


                    <select size="1" id="cmbIdQuienAutoriza" style="width:95%" title="76"  onfocus="comboDependienteEmpresa('cmbIdQuienAutoriza','87')">
                        <option value=""></option>
                                       
                    </select>    
                        </td>
                        <td>
                            <select size="1" id="cmbIdMotivoAutoriza" style="width:80%"  >
                            <option value=""></option>                    
                            <% resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(556);	
                               ComboVO cmbt2; 
                               for(int k=0;k < resultaux.size();k++){ 
                                   cmbt2=(ComboVO)resultaux.get(k);
                                   
                            %>
                        <option value="<%= cmbt2.getId()%>" title="<%= cmbt2.getTitle()%>" > <%= cmbt2.getDescripcion()%>
                        </option>
                        <%}%>                    
                    </select>       




                        </td> 
                        <td>
                        	 <textArea id="txtObservacionDescuento" style="width:90%"  maxlength="200"  ></textArea>
                        </td>                     
                    </tr>   

                    <tr class="titulos">
                    	<td colspan="6">
                    		NOTA: CUANDO SE AGREGUE O SE ELIMINE ALGUN PROCEDIMIENTO DE LA FACTURA, EL DESCUENTO SERA REVERTIDO.
                    	</td>
                        <tr class="titulos">
                        <td colspan="6">
                            SI EL TIPO DE REGIMEN ES 'PARTICULAR', EL DESCUENTO SE AÑADIRA AL VALOR CUBIERTO, CASO CONTRARIO AL VALOR NO CUBIERTO.
                        </td>
                    </tr>                                 
                    <tr class="estiloImputListaEspera"> 
                        <td colspan="6">
                            <input id="BTN_MO" title="BP994" type="button" class="small button blue" value="ADICIONAR DESCUENTO" onclick="modificarCRUD('adicionaDescuentoFactura', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   />  

                            <input id="BTN_REVERTIR" title="BR995" type="button" class="small button blue" value="REVERTIR DESCUENTO" onclick="modificarCRUD('revertirDescuentoFactura', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   /> 
                        </td> 
                    </tr>
                </table>  
            </div>    
    </div>


    <div id="divVentanitaAsociarFactura"  style="display:none; z-index:2000; top:400px; left:50px;">

        <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>  
        <div  style="z-index:2004; position:fixed; top:200px; left:300px; width:50%">  
               
                <table width="50%" height="80"  cellpadding="0" cellspacing="0" class="fondoTabla" >
                        <tr class="estiloImput" >
                        <td width="20%" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAsociarFactura')" /></td>  
                        <td width="60%">&nbsp;</td>                  
                        <td width="20%" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAsociarFactura')" /></td>  
                    </tr> 
                    <tr class="titulosCentrados">
                                    <td colspan="3"> PROCEDIMIENTOS NO EJECUTADOS
                                    </td>   
                                </tr>
<tr>
    <td td colspan="3">
         <table id="listGrillaProcedimientosSinEjecutar" class="scroll"></table> 
    </td>
</tr>

<tr class="titulosCentrados">
                                    <td colspan="3"> PROCEDIMIENTOS EJECUTADOS
                                    </td>   
                                </tr>
<tr>
    <td td colspan="3">
         <table id="listGrillaProcedimientosEjecutados" class="scroll"></table> 
    </td>
</tr>
<tr class="titulos">
    <td colspan="3">
        <input title="BF90" type="button" class="small button blue" value="ACEPTAR" onclick="ocultar('divVentanitaAsociarFactura');buscarFacturacion('listProcedimientosDeFactura')"   />  
    </td>
</tr>
   
                </table>  
            </div>    
    </div>





<div id="divVentanitaLiquidacionContratadas"  style="display:none; z-index:2000; top:400px; left:50px;">

        <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>  
        <div  style="z-index:2004; position:fixed; top:200px; left:50%; width:100%;margin-left: -25%;">  
               
                <table width="50%" height="80"  cellpadding="0" cellspacing="0" class="fondoTabla" >
                        <tr class="estiloImput" >
                        <td width="20%" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaLiquidacionContratadas')" /></td>  
                        <td width="60%">&nbsp;</td>                  
                        <td width="20%" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaLiquidacionContratadas')" /></td>  
                    </tr> 
                    <tr class="titulosCentrados">
                                    <td colspan="3"> PROCEDIMIENTOS LIQUIDACION CONTRATADOS
                                    </td>   
                                </tr>
                    <tr>            
                        <td td colspan="3">
                             <table id="listGrillaProcedimientosContratados" class="scroll"></table> 
                        </td>
                </tr>
<tr class="titulos">
    <td colspan="3">
        <input title="BTN745" type="button" class="small button blue" value="CREAR LIQUIDACION" onclick="modificarCRUD('crearLiquidacionCirugiaContratadas');"   />  
        <input title="BF35" type="button" class="small button blue" value="ELIMINAR LIQUIDACION" onclick="modificarCRUD('eliminarLiquidacionCirugiaContratadas');"   />
        <input title="BF36" type="button" class="small button blue" value="FACTURAR" onclick="modificarCRUD('facturarLiquidacionCirugiaContratadas');"   />
        <input title="BQ153" type="button" class="small button blue" value="TRAER PROCEDIMIENTOS DE HC" onclick="modificarCRUD('crearLiquidacionCirugiaContratadasPos');"   />
    </td>
</tr>   
                </table>  
            </div>    
    </div>


    <div id="divVentanitaLiquidacionContratadasDetalle" class="ventanita">
    <div class="transParencia fondoVentanita2">
    </div>
    <div class="fondoVentanitaContenido2 w40 t300 l300 fondoTabla">
        <div>
            <div class="estiloImput">
                <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaLiquidacionContratadasDetalle')" style="float: left; " />
                <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaLiquidacionContratadasDetalle')" style=" float: right;" />
            </div>
            <div class="btitulos">
                <label class="bloque w10">PROGRAMADOS</label>
                <label class="bloque w15">ID SITIO</label>
                <label class="bloque w15">ID PROCEDIMIENTO</label>
                <label class="bloque w60">PROCEDIMIENTO</label>
            </div>
            <div class="bestiloImput">
                <label id="lblIdProgramadosVentanita" class="bloque w10"></label>
                <label id="lblIdSitioLiquidacionVentanita" class="bloque w15"></label>
                <label id="lblIdProcedimientoLiquidacionVentanita" class="bloque w15"></label>
                <label id="lblProcedimientoLiquidacionVentanita" class="bloque w60"></label>
            </div>
            <div class="estiloImputListaEspera">
                <input title="BTVA1" type="button" class="small button blue" value="IGNORAR DE LIQUIDACION" onclick="modificarCRUD('editarProcedimientoLiquidacionCirugiaContratadas')"  />
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="txtColumnaIdSitioQuirur" /> 
<input type="hidden" id="txtColumnaIdProced" />
<input type="hidden" id="txtColumnaObservacion" /> 
<input type="hidden" id="txtIdFacturaAsociar" /> 
<input type="hidden" id="txtEstadoFacturaAsociar" /> 
<input type="hidden" id="txtIdProcedimientoRealizar" />
<input type="hidden" id="txtProcedimientoEstado" /> 
<label id="lblTipoPlan" style="display: none;"></label>