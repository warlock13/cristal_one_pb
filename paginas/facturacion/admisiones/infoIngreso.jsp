<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="100%">
    <tr class="titulos">
        <td style="display: none;"><strong></strong></td>
        <td width="50%"><strong>Institución que remite</strong> <label style="color: red;">(*)</label></td>
        <td><strong>Profesional Admisión</strong> <label style="color: red;">(*)</label></td>
    </tr>
    <tr class="estiloImputListaEspera">
        <td style="display: none;"> 
                <input type="text" id="txtIdDx" class="w90"  value="0-" >
                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
                    onclick="traerVentanita(this.id,'','divParaVentanita','txtIdDx','5')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
            <input type="text" id="txtIPSRemite" onkeypress="llamarAutocomIdDescripcionConDato('txtIPSRemite',313)"
                class="w85"  />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIpsRemT"
                onclick="traerVentanita(this.id,'','divParaVentanita','txtIPSRemite','4')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
            <select id="cmbIdProfesionales" style="width: 95%;" 
                onfocus="cargarComboGRALCondicion2('', '', this.id, 197, valorAtributo('cmbIdEspecialidad'), IdSede());">
                <option value=""></option>
            </select>
        </td>
    </tr>
</table>

<table width="100%">
    <tr class="titulos">
        <td width="30%"><strong>Administradora</strong> <label style="color: red;">(*)</label></td>
        <td width="15%"><strong>Tipo Régimen - Tipo Plan</strong> <label style="color: red;">(*)</label></td>
        <td width="15%"><strong>Número de Autorización</strong></td>
        <td width="10%"><strong>Fecha</strong></td>
        <td width="30%"><strong>Tipo Admisión</strong> <label style="color: red;">(*)</label></td>
    </tr>
    <tr class="estiloImputListaEspera">
        <td>
            <input type="text" id="txtAdministradora1"
                onkeypress="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                onkeypress="llamarAutocompletarEmpresa('txtAdministradora1',184)" style="width: 85%;"  />
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem"
                onclick="traerVentanitaEmpresa(this.id,'divParaVentanita','txtAdministradora1','24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
            <select id="cmbIdTipoRegimen" style="width: 95%;" 
                onfocus="comboDependienteAutocompletarSede('cmbIdTipoRegimen','txtAdministradora1','79')""
                onchange="
                    asignaAtributoCombo2('cmbIdPlan', '', '[ SELECCIONE ]');
                    asignaAtributoCombo2('cmbIdTipoAfiliado', '', '[ SELECCIONE ]');
			        asignaAtributoCombo2('cmbIdRango', '', '[ SELECCIONE ]');">
                <option value=""></option>
            </select>
        </td>
        <td>
            <input type="text" id="txtNoAutorizacion" style="width: 95%;"
                onKeyPress="javascript:return validarMax(this,'20'); teclearExcluirCaracter(event,'%');"
                onblur="verificarNotificacionAntesDeGuardar('validarNumeroAutorizacion')"                
                maxlength="20"
                />                
        </td>
        <td>
            <input type="date" style="width: 95%;" id="txtFechaVigenciaAutorizacion"
                 onkeydown="return false"
                >
        </td>
        <td>
            <select id="cmbTipoAdmision" style="width: 95%;"
                onchange="buscarAGENDA('listAdmisionCEXProcedimientoTraer')"
                onfocus="cargarTipoCitaSegunEspecialidad('cmbTipoAdmision','cmbIdEspecialidad')" >
                <option value=""></option>
            </select>
        </td>
    </tr>
</table>

<table width="100%">
    <tr class="titulos">
        <td width="50%"><strong>Plan de Contratación</strong> <label style="color: red;">(*)</label></td>
        <td width="25%"><strong>Tipo Afiliado</strong> <label style="color: red;">(*)</label></td>
        <td width="25%"><strong>Rango</strong> <label style="color: red;">(*)</label></td>
    </tr>
    <tr class="estiloImputListaEspera">
        <td>
            <div hidden>
                <label id="lblIdPlanContratacion"></label>-<label id="lblNomPlanContratacion"></label>
            </div>
            <select id="cmbIdPlan" style="width: 95%;"
                onfocus="traerPlanesDeContratacion(this.id, '165', 'txtAdministradora1', 'cmbIdTipoRegimen')"
                onchange="asignaAtributo('lblIdPlanContratacion', valorAtributo(this.id), 0); 
                    asignaAtributoCombo2('cmbIdTipoAfiliado', '', '[ SELECCIONE ]');
			        asignaAtributoCombo2('cmbIdRango', '', '[ SELECCIONE ]');">
                <option value=""></option>
            </select>
        </td>
        <td>
            <select id="cmbIdTipoAfiliado" style="width: 95%;"
                onfocus="comboDependiente('cmbIdTipoAfiliado','cmbIdPlan','30')"
                onchange="asignaAtributoCombo2('cmbIdRango', '', '[ SELECCIONE ]');">
                <option value=""></option>
            </select>
        </td>
        <td>
            <select id="cmbIdRango" style="width: 95%;"
                onfocus="comboDependienteDosCondiciones('cmbIdRango','cmbIdPlan','cmbIdTipoAfiliado','31')">
                <option value=""></option>
            </select>
        </td>
    </tr>
</table>

