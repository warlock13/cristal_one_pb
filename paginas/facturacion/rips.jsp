<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1200px" align="center">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="RIPS"/>
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:720px; width:1300px" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="60%">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr class="titulosListaEspera">
                                                    <td width="60%" colspan="2">PLAN CONTRATACIÓN</td>
                                                    <td width="40%">SEDE</td>
                                                </tr>
                                                <tr class="estiloImputListaEspera">
                                                    <td colspan="2">
                                                        <input type="text" id="txtPlan" oninput="llenarElementosAutoCompletarKey(this.id, 215, this.value); limpiaAtributo('cmbSede')" style="width:90%">
                                                        <img id="idLupitaVentanitaPlan" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtPlan', '46');" 
                                                             src="/clinica/utilidades/imagenes/acciones/buscar.png" 
                                                             width="18px" height="18px" align="middle">
                                                        <img onclick="limpiaAtributo('txtPlan')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">
                                                    </td>
                                                    <td>
                                                        <select style="width:95%" id="cmbSede" onfocus="cargarComboGRALCondicion2('',
                                                                        '',
                                                                        this.id,
                                                                        216,
                                                                        valorAtributoIdAutoCompletar('txtPlan'),
                                                                        valorAtributoIdAutoCompletar('txtPlan'))"> 
                                                            <option value="">[ SELECCIONE ]</option> 

                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr class="titulosListaEspera" align="center">
                                                    <td Width="50%">PERIODO FACTURACION</td>
                                                    <td Width="50%">PERIODO PRESTACION</td>
                                                </tr>
                                                <tr class="estiloImputListaEspera">
                                                    <td>
                                                        <select id="cmbIdAnio" style="width:45%">
                                                            <option value="">[ TODO ]</option>
                                                            <%resultaux.clear();
                                                                resultaux = (ArrayList) beanAdmin.combo.cargar(71);
                                                                ComboVO cmbAn;
                                                                for (int k = 0; k < resultaux.size(); k++) {
                                                                    cmbAn = (ComboVO) resultaux.get(k);
                                                            %>
                                                            <option value=" <%= cmbAn.getId()%>" title="<%= cmbAn.getTitle()%>"><%=cmbAn.getDescripcion()%>
                                                            </option>
                                                            <%} %>
                                                        </select>
                                                        <select id="cmbIdMes" style="width:45%">
                                                            <option value="">[ TODO ]</option>
                                                            <option value="1">Enero</option>
                                                            <option value="2">Febrero</option>
                                                            <option value="3">Marzo</option>
                                                            <option value="4">Abril</option>
                                                            <option value="5">Mayo</option>
                                                            <option value="6">Junio</option>
                                                            <option value="7">Julio</option>
                                                            <option value="8">Agosto</option>
                                                            <option value="9">Septienbre</option>
                                                            <option value="10">Octubre</option>
                                                            <option value="11">Noviembre</option>
                                                            <option value="12">Diciembre</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="cmbIdAnioPrestacion" style="width:45%">
                                                            <option value="">[ TODO ]</option>
                                                            <%resultaux.clear();
                                                                resultaux = (ArrayList) beanAdmin.combo.cargar(71);
                                                                for (int k = 0; k < resultaux.size(); k++) {
                                                                    cmbAn = (ComboVO) resultaux.get(k);
                                                            %>
                                                            <option value=" <%= cmbAn.getId()%>" title="<%= cmbAn.getTitle()%>"><%=cmbAn.getDescripcion()%>
                                                            </option>
                                                            <%} %>
                                                        </select>
                                                        <select id="cmbIdMesPrestacion" style="width:45%">
                                                            <option value="">[ TODO ]</option>
                                                            <option value="1">Enero</option>
                                                            <option value="2">Febrero</option>
                                                            <option value="3">Marzo</option>
                                                            <option value="4">Abril</option>
                                                            <option value="5">Mayo</option>
                                                            <option value="6">Junio</option>
                                                            <option value="7">Julio</option>
                                                            <option value="8">Agosto</option>
                                                            <option value="9">Septienbre</option>
                                                            <option value="10">Octubre</option>
                                                            <option value="11">Noviembre</option>
                                                            <option value="12">Diciembre</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr class="titulosListaEspera" align="center">
                                                    <td Width="15%">TIPO</td>
                                                    <td width="10%">CUENTA</td>
                                                    <td Width="10%">REMISION</td>
                                                    <td Width="15%"></td>
                                                    <td Width="15%"></td>
                                                    <td Width="25%"></td>
                                                    <td Width="10%"></td>
                                                </tr>
                                                <tr class="estiloImputListaEspera">
                                                    <td>
                                                        <select id="cmbIdTipo" style="width:95%">
                                                            <%  resultaux.clear();
                                                                resultaux = (ArrayList) beanAdmin.combo.cargar(55);
                                                                ComboVO cmbT;
                                                                for (int k = 0; k < resultaux.size(); k++) {
                                                                    cmbT = (ComboVO) resultaux.get(k);
                                                            %>
                                                            <option value="<%= cmbT.getId()%>" title="<%= cmbT.getTitle()%>"><%=cmbT.getDescripcion()%>
                                                            </option>
                                                            <%}%>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <!--<input type="text" id="txtPrefijoCuenta" style="width:30%" placeholder="Prefijo"/> -->
                                                        <input type="text" id="txtCuenta" style="width:95%"/>
                                                    </td> 
                                                    <td>
                                                        <input type="text" id="txtRemision" style="width:95%" />
                                                    </td>
                                                    <td>
                                                        <input type="button" class="small button blue" value="Buscar RIPS"
                                                               onclick="buscarFacturacion('listaCuentas')"/>
                                                    </td>
                                                    <td>
                                                        <input type="button" class="small button blue" value="Limpiar"
                                                               onclick="limpiarDivEditarJuan('parametrosRips')"/>
                                                    </td>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" id="chkTodasSedes"> GENERAR PARA TODAS LAS SEDES
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type="radio" name="periodoGeneraRips" value="" checked> Por periodo de facturacion
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type="radio" name="periodoGeneraRips" value="1"> Por periodo de prestacion
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <input id="btnCrearRips" type="button" class="small button blue" value="Crear RIPS"
                                                               onclick="verificarNotificacionAntesDeGuardar('verificarCuentasRepetidas')"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div id="divParaVentanita"></div>
                                <div id="divTablaListaCuentas">
                                    <table id="listaCuentas"></table>
                                </div>
                                <table width="50%">
                                    <tr class="titulosListaEspera">
                                        <td>ESTADO DE FACTURAS</td>
                                        <td></td>
                                    </tr>
                                    <tr class="estiloImputListaEspera">
                                        <td>
                                            <select id="cmbEstadoTipificacionFacturas" style="width: 95%;">
                                                <option value="">[ SELECCIONE ]</option>
                                                <option value="0">TODO</option>
                                                <option value="1">SIN TIPIFICAR</option>
                                                <option value="2">TIPIFICADAS</option>
                                            </select>
                                        </td>
                                        <td>
                                            <div id="loaderTipificacion" hidden>
                                                <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="20px" height="20px" >
                                                Tipificando ...
                                            </div>
                                            <input type="button" id="btnTipificacion" class="small button blue" value="INICIAR TIPIFICACION DE LAS CUENTAS SELECCIONADAS"
                                                   onclick="tipificarFacturasPorBloqueEnEnvios()"/>
                                        </td>
                                    </tr>
                                </table>
                                <div id="divTableListaRips">
                                    <table id="listGrillaRips"></table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <div id="divVentanitaGeneraRips" style="display:none; z-index:2000; top:0px; width:1100px; left:150px;">
                <div class="transParencia"
                     style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                </div>
                <div style="z-index:2002; position:relative; top:-720px; left:50px; width:1100px">
                    <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
                        <tr class="estiloImput">
                            <td align="left" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                                onclick="ocultarDivVentanitaGeneraRips()" /></td>
                            <td align="right" colspan="5"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                                 onclick="ocultarDivVentanitaGeneraRips()" /></td>
                        <tr>
                        <tr class="titulosListaEspera">
                            <td width="10%">ID ENVIO</td>
                            <td width="10%">ID PLAN</td>
                            <td width="20%">PLAN</td>
                            <td width="10%">RANGO</td>
                            <td width="10%">POS</td>
                            <td width="10%">CUENTA</td>
                            <td width="10%">REMISION</td>
                        </tr>
                        <tr class="estiloImputListaEspera">
                            <td id="lblIdEnvioVentanita"></td>
                            <td id="lblIdPlanVentanita"></td>
                            <td id="lblPlanVentanita"></td>
                            <td id="lblRangoVentanita"></td>
                            <td id="lblPosVentanita"></td>
                            <td>
                                <table width="100%" id="tablaInfoCuenta">
                                    <tr>
                                        <td align="CENTER">
                                            <label id="lblCuentaVentanita"></label>
                                        </td>
                                        <td width="20%">
                                            <img src="/clinica/utilidades/imagenes/icons/icons/pencil-square.svg" 
                                                 width="15px" height="15px" align="middle" 
                                                 onclick="editarNumeroCuenta()">
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tablaEditarInfoCuenta" hidden>
                                    <tr>
                                        <td align="CENTER">
                                            <input type="text" id="txtNuevaCuenta" style="width: 70%;">
                                        </td>
                                        <td width="10%">
                                            <img src="/clinica/utilidades/imagenes/icons/icons/check2-circle.svg" align="middle"  title="Guardar"
                                                 onclick="modificarCRUD('modificarNumeroCuenta')">
                                        </td>
                                        <td width="10%">
                                            <img src="/clinica/utilidades/imagenes/icons/icons/x.svg" 
                                                 title="Cancelar" align="middle" 
                                                 onclick="mostrar('tablaInfoCuenta');
                                                         ocultar('tablaEditarInfoCuenta'); limpiaAtributo('txtNuevaCuenta')">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%" id="tablaInfoRemision">
                                    <tr>
                                        <td align="CENTER"><label id="lblRemisionVentanita"></label></td>
                                        <td width="20%">
                                            <img src="/clinica/utilidades/imagenes/icons/icons/pencil-square.svg"
                                                 align="middle" title="Editar"
                                                 onclick="editarRemision()">
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tablaEditarRemision" hidden>
                                    <tr>
                                        <td align="CENTER">
                                            <input type="text" id="txtNuevaRemision" style="width: 70%;">
                                        </td>
                                        <td width="10%">
                                            <img src="/clinica/utilidades/imagenes/icons/icons/check2-circle.svg" align="middle" title="Guardar"
                                                 onclick="modificarCRUD('modificarRemision')">
                                        </td>
                                        <td width="10%">
                                            <img src="/clinica/utilidades/imagenes/icons/icons/x.svg" title="Cancelar" align="middle" 
                                                 onclick="mostrar('tablaInfoRemision');
                                                         ocultar('tablaEditarRemision');
                                                         limpiaAtributo('txtNuevaRemision')">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <table width="100%">
                                    <tr class="estiloImputListaEspera">
                                        <td width="14%" align="center">
                                            <input type="button" class="small button blue" value="EXPORTAR RIPS" onclick="crearRips();"/>
                                        </td>
                                        <td width="14%" align="center">
                                            <input type="button" class="small button blue" value="EXPORTAR TIPIFICACION" onclick="crearArchivosTipificacion();"/>
                                        </td>
                                        <td width="14%" align="center">
                                            <input type="button" class="small button blue" value="RADICAR CUENTA" onclick="radicarCuenta()"/>
                                        </td>
                                        <td width="14%" align="center">
                                            <input type="button" class="small button blue" value="VER CUENTA DE COBRO" onclick="imprimirPDFCuentaCobroRips()"/>
                                        </td>
                                        <td width="14%" align="center">
                                            <input type="button" class="small button blue" value="EXCEPCIONES DE ENVIO"
                                                   onclick="mostrar('divVentanitaExcepcionesRips');
                                                           ocultar('divVentanitaGeneraRips');
                                                           buscarFacturacion('listExcepcionesEnvio')"/>
                                        </td>
                                        <td width="14%" align="center">
                                            <input type="button" class="small button blue" value="ELIMINAR ENVIO"
                                                   onclick="modificarCRUD('eliminarEnvioRips')" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <table width="100%">
                                    <tr class="titulosCentrados">
                                        <td  width="60%">FACTURAS RIPS</td>                            
                                        <td>FACTURAS PENDIENTES</td>
                                    </tr>
                                    <tr>
                                        <td align="LEFT">
                                            <table width="100%" id="tablaOpcionesRipsAF">
                                                <tr class="titulosListaEspera">
                                                    <td width="20%">TIPIFICACION</td>
                                                    <td width="30%">VALOR CUENTA</td>
                                                </tr>
                                                <tr class="estiloImputListaEspera">
                                                    <td>
                                                        <select id="cmbFacturasTipificadas" style="width: 90%;" onchange="buscarFacturacion('listaFacturasCuenta')">
                                                            <option value="">[ TODO ]</option>
                                                            <%resultaux.clear();
                                                                            resultaux = (ArrayList) beanAdmin.combo.cargar(3631);
                                                                            ComboVO cmbTipi;
                                                                            for (int k = 0; k < resultaux.size(); k++) {
                                                                                cmbTipi = (ComboVO) resultaux.get(k);
                                                                        %>
                                                                        <option value=" <%= cmbTipi.getId()%>" title="<%= cmbTipi.getTitle()%>"><%=cmbTipi.getDescripcion()%>
                                                                        </option>
                                                                        <%} %>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <table width="90%" id="tablaValorCuenta" align="center">
                                                            <tr>
                                                                <td class="titulosListaEspera"><label id="lblTotalEnvio" style="width: 100%;"></label></td>
                                                                <td>
                                                                    <img src="/clinica/utilidades/imagenes/icons/icons/pencil-square.svg" width="15px" height="15px" align="middle" onclick="editarValorCuenta()">
                                                                </td>
                                                            </tr>
                                                        </table> 
                                                        <table width="100%" id="tablaEditarValorCuenta" hidden>
                                                            <tr>
                                                                <td class="titulosListaEspera">
                                                                    <input type="number" id="txtNuevoValorCuenta">
                                                                </td>
                                                                <td><img src="/clinica/utilidades/imagenes/icons/icons/calculator.svg" title="Calcular valor de cuenta" align="middle" onclick="traerValorRealCuenta()"></td>
                                                                <td><img src="/clinica/utilidades/imagenes/icons/icons/check2-circle.svg" title="Guardar valor" align="middle" onclick="modificarCRUD('guardarValorCuenta')"></td>
                                                                <td><img src="/clinica/utilidades/imagenes/icons/icons/x-circle.svg" title="Cancelar" align="middle" onclick="mostrar('tablaValorCuenta');
                                                                        ocultar('tablaEditarValorCuenta');
                                                                        limpiaAtributo('txtNuevoValorCuenta')"></td>
                                                            </tr>
                                                        </table>                         
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="titulos">
                                        <td id="contenedorRegistros">
                                            <div id="divListaFacturasCuenta">
                                                <table id="listaFacturasCuenta"></table>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="divListaFacturasPendientes">
                                                <table id="listaFacturasPendientes"></table>
                                            </div>
                                        </td>
            
                                    </tr>
                                    <tr class="estiloImputListaEspera">
                                        <td align="CENTER">
                                            <input id="eliminar_facturas_rips" type="button" class="small button blue" value="ELIMINAR FACTURAS"
                                                        onclick="verificarNotificacionAntesDeGuardar('eliminarFacturasRips');"/>
                                            <input type="button" id="btnTipificacion" class="small button blue" value="TIPIFICAR FACTURAS SELECCIONADAS"
                                                        onclick="tipificarFacturas(obtenerListaFacturasTipificacionRips())"/>
                                            <div id="loaderTipificacion" hidden>
                                                <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="20px" height="20px" >
                                                Tipificando facturas <label id="progreso">0</label>%
                                            </div>
                                        </td>
                                        <td align="CENTER">
                                            <input type="hidden" class="small button blue" value="ADICIONAR FACTURAS A ENVIO ACTUAL"
                                                   onclick="modificarCRUD('agregarRegistrosAEnvioRips');" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <table width="100%" border="2" class="fondoTabla">
                                    <tr class="titulosCentrados">
                                        <td colspan="4">DETALLES DE FACTURA
                                        </td>
                                    </tr>
                                    <tr class=" estiloImputIzq2">
                                        <td width="15%">Archivo:</td>
                                        <td width="85%" colspan="3"><label id="lblIdArchivoFactura"></label></td>
                                    </tr>
                                    <tr class=" estiloImputIzq2">
                                        <td width="15%">Id Envio:</td>
                                        <td colspan="3"><label id="lblIdEnvioFactura"></label></td>
                                    </tr>
                                    <tr class=" estiloImputIzq2">
                                        <td>Plan</td>
                                        <td colspan="3"><label id="lblIdPlanFactura"></label>-<label id="lblPlanFactura"></label></td>
                                    </tr>
                                    <tr class=" estiloImputIzq2">
                                        <td>Id Factura:</td>
                                        <td colspan="3"><label id="lblIdFactura"></label></td>
                                    </tr>
                                    <tr class="estiloImputIzq2">
                                        <td>Numero Factura:</td>
                                        <td colspan="3"><label id="lblNumeroFactura"></label></td>
                                    </tr>
                                    <tr class="titulos">
                                        <td>
                                            <input title="BF73" type="button" class="small button blue" value="IMPRIMIR"
                                                   onclick="formatoPDFFactura(valorAtributo('lblIdFactura'));" style="background: #3085D6;color: white;padding-top: 9px;font-weight: bold;font-size: 12px;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>


            <div id="divVentanitaExcepcionesRips" style="display:none; z-index:2000; top:1px; width:1100px; left:150px;">
                <div class="transParencia"
                     style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                </div>

                <div style="z-index:2002; position:fixed; top:100px; left:180px; width:1100px">


                    <table width="100%" border="2" class="fondoTabla">
                        <tr class="estiloImput">
                            <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                    onclick="ocultar('divVentanitaExcepcionesRips');
                                                            mostrar('divVentanitaGeneraRips')"/></td>
                            <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                     onclick="ocultar('divVentanitaExcepcionesRips');mostrar('divVentanitaGeneraRips')"/></td>
                        <tr>
                        <tr class="titulos">
                            <td colspan="2" align="CENTER">
                                <label>EXCEPCIONES DE ENVIO</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><hr></td>
                        </tr>
                        <tr class="titulosCentrados">
                            <td width='50%'>DIAGNÓSTICOS DE INGRESO NO VALIDOS
                            </td>
                            <td>MUNICIPOS NO VIGENTES - PLANES PGP 
                            </td>
                        </tr>
                        <tr class="titulos">
                            <td>
                                <table id="listGrillaExcepcionesDx" class="scroll"></table>
                            </td>
                            <td>
                                <table id="listGrillaExcepcionesMunicipios" class="scroll"></table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>


            <!--<div id="divVentanitaArchivosTipificacion" style="display:none; z-index:2000; top:1px; width:700px; left:150px;">
                <div class="transParencia"
                     style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                </div>
                <div style="z-index:2002; position:fixed; top:150px; left:150; width:700">
                    <table width="100%" border="2" class="fondoTabla">
                        <tr class="estiloImput">
                            <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                    onclick="ocultar('divVentanitaArchivosTipificacion')"/></td>
                            <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                     onclick="ocultar('divVentanitaArchivosTipificacion')"/></td>
                        <tr>
                        <tr class="titulos">
                            <td align="center" colspan="2"><input align="center" type="button" class="boton" value="REFRESCAR LISTADO" onclick="buscarHistoria('listArchivosAdjuntosVarios')" /></td>
                        </tr>                          
                        <tr class="titulos">
                            <td colspan="2">                                    
                                <table id="listArchivosTificacion"></table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>-->


            <div id="divVentanitaTipificacion"  style="position:absolute; display:none; background-color:#E2E1A5; top:10px; left:100px; width:600px; height:90px; z-index:2004">
                <div class="transParencia" style="z-index:2005; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
                </div> 
                <div style="z-index:2006; position:absolute; top:200px; left:100px; width:95%">          
                    <table id="idSubVentanita" width="100%" class="fondoTabla">
                        <tr class="estiloImput">
                            <td colspan="3" align="right">
                                <input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaTipificacion')"/>
                            </td>  
                        <tr>     
                        <tr class="titulos">
                            <td width="5%">ID FACTURA</td>  
                            <td width="65%">NOMBRE ARCHIVO</td>
                            <td width="30%"></td>                      
                        <tr>           
                        <tr class="estiloImput" >  
                            <td><label id="lblIdFactura"></label></td>            
                            <td align="CENTER" onClick="abrirPopupArchivoTipificacion()">
                                <label id="lblTipoArchvio"></label><label id="lblImagen"></label><a><u>Abrir</u></a>
                            </td>            
                        <tr> 
                    </table>  
                </div>            
            </div> 

            <div id="divVentanitaCerrarCuenta" style="display:none; z-index:2001; top:0px; width:500px; left:150px;">
                <div class="transParencia"
                     style="z-index:2002; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                </div>
                <div style="z-index:2002; position:relative; top:-1300px; left:300px; width:600px">
                    <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
                        <tr class="estiloImput">
                            <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                    onclick="ocultar('divVentanitaCerrarCuenta')" /></td>
                            <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                     onclick="ocultar('divVentanitaCerrarCuenta')" /></td>
                        <tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr class="titulosListaEspera">
                                        <td>Fecha radicación</td>
                                        <td>N° Radicacion</td>
                                        <td></td>
                                    </tr>
                                    <tr class="estiloImputListaEspera">
                                        <td>
                                            <input type="date" id="txtFechaRadicacion" style="width: 95%;">
                                        </td>
                                        <td> 
                                            <input type="text" id="txtNumeroRadicacion" style="width: 95%;">
                                        </td>
                                        <td>
                                            <input class="small button blue" type="button" value="Continuar" onclick="modificarCRUD('cerrarCuenta')"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
                                                        
<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:70%">
        <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR"
                           onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" align="right" value="CERRAR"
                           onclick="ocultar('divVentanitaAdjuntos')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="divAcordionAdjuntos" style="display:BLOCK">
                        <div id="divParaArchivosAdjuntos"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>                                                        

<!--PARAMETROS-->
<input type="text" id="txtCuentaActual" hidden>
<!--<input type="button" value="cn" onclick="tipificacionPersonalizadaNefro_con_na()">-->
<input id="txtPlanTipificacionExterna" type="text" placeholder="Ingrese el plan">
<input type="button" onclick="crearArchivosTipificacionPacientesNefro()" value="Descargar">
