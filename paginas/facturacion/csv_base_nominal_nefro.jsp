<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 

<%@ page import="java.util.zip.ZipEntry" %> 
<%@ page import="java.util.zip.ZipOutputStream" %> 
<%@ page import="java.util.List" %> 
<%@ page import="java.util.ArrayList" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 

<%

    boolean hay_datos = false;
    String ruta = "/opt/tomcat8/webapps/clinica/paginas/ireports/excel_informes/";
    String archivo = "";
    Conexion conexion = beanSession.getCn();

    FileWriter fichero = null;

    String sql = "";
    String cad_datos = "";

    try {

        System.out.println("GENRANDO ARCHIVO CSV - FACTURACION NEFROPROTECCION");
        sql = conexion.traerElQuery(3026).toString();

        String anio = request.getParameter("anio");
        String mes = request.getParameter("mes");
        String atendido = request.getParameter("atendido") == "" ? "%" : request.getParameter("atendido");
        String paciente = request.getParameter("paciente") == "" ? "%" : request.getParameter("paciente");
        String estadio = request.getParameter("estadio") == "" ? "%" : request.getParameter("estadio");
        String diabetes = request.getParameter("diabetes") == "" ? "%" : request.getParameter("diabetes");
        String hta = request.getParameter("hta") == "" ? "%" : request.getParameter("hta");
        String otras_etiologias = request.getParameter("otras_etiologias") == "" ? "%" : request.getParameter("otras_etiologias");
        String estado_factura = request.getParameter("estado_factura") == "" ? "%" : request.getParameter("estado_factura");
        String seccional = request.getParameter("seccional") == "" ? "%" : request.getParameter("seccional");
        String paquete = request.getParameter("paquete") == "" ? "%" : request.getParameter("paquete");
        String regimen = request.getParameter("regimen") == "" ? "%" : request.getParameter("regimen");
        String estado_tipificacion = request.getParameter("estado_tipificacion") == "" ? "%" : request.getParameter("estado_tipificacion");
        String tipo_red = request.getParameter("tipo_red") == "" ? "%" : request.getParameter("tipo_red");

        System.out.println(request.getParameterMap() + "**/*");

        archivo = ruta + "base_nominal.csv";

        PreparedStatement ps = new LoggableStatement(conexion.getConexion(), sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ps.setString(1, anio);
        ps.setString(2, mes);
        ps.setString(3, atendido);
        ps.setString(4, paciente);
        ps.setString(5, estadio);
        ps.setString(6, diabetes);
        ps.setString(7, hta);
        ps.setString(8, otras_etiologias);
        ps.setString(9, estado_factura);
        ps.setString(10, seccional);
        ps.setString(11, paquete);
        ps.setString(12, regimen);
        ps.setString(13, estado_tipificacion);
        ps.setString(14, tipo_red);

        ResultSet resultado = ps.executeQuery();

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("c1");
            if (!resultado.isLast()) {
                cad_datos += "\r\n";
            }
        }

        ps.close();
        
        fichero = new FileWriter(archivo);
        if (hay_datos) {
            fichero.write(cad_datos);
            fichero.flush();
            fichero.close();
        }

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=facturas.csv");

        OutputStream out2 = response.getOutputStream();
        FileInputStream in2 = new FileInputStream(archivo);
        byte[] buffer2 = new byte[4096];
        int length2;
        while ((length2 = in2.read(buffer2)) > 0) {
            out2.write(buffer2, 0, length2);
        }
        in2.close();
        out2.flush();
        out2.close();
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }
%>