function activarCRCo(el){
    if(el.value === 'CLI' || el.value === 'PAR' || el.value === 'EMP'){
        document.getElementById('txtCodigoPostal').style.display =  ""
        document.getElementById('txtResponsabilidadFiscal').style.display =  ""
        document.getElementById('txtCodigoObligacion').style.display =  "" 
    }else{
        setTimeout(() => {
            document.getElementById('txtCP').value = "";
        }, 100);
        setTimeout(() => {
            document.getElementById('txtRF').value = ""; 
        }, 100);
        setTimeout(() => {
            document.getElementById('txtCO').value = ""; 
        }, 100);
        document.getElementById('txtCodigoPostal').style.display =  "none"
        document.getElementById('txtResponsabilidadFiscal').style.display =  "none"
        document.getElementById('txtCodigoObligacion').style.display =  "none"       
    }

}

function tipoEntidad(el){
    if(el.value === '1'){
        document.getElementById('trTipoEntidad').style.display = ""
    }else{
        document.getElementById('trTipoEntidad').style.display = "none"
        setTimeout(() => {
            document.getElementById('cmbTipoEntidad').value = ''; 
        }, 200);
        
    }
}



function autoretenedor(el){
    if(el.value === 'RAUT'){
        document.getElementById('trAutorretenedorICA').style.display="";
        document.getElementById('trAutorretenedorIVA').style.display="";
        
    }else{
        document.getElementById('trAutorretenedorICA').style.display="none";
        document.getElementById('trAutorretenedorIVA').style.display="none";
        document.getElementById('cmbAutorretenedorICA').value = '';
        document.getElementById('cmbAutorretenedorIVA').value = '';
        document.getElementById('txtManejaICA').value = ''; 
        document.getElementById('txtManejaTOPE').value = ''; 
    }
}


function ocultarDiv(arg,el){
    document.getElementById(arg).style.display = 'none';
    el.src='/clinica/utilidades/imagenes/acciones/buildup.png';
}

function mostrarDiv(arg,el){
    document.getElementById(arg).style.display = '';
    el.src='/clinica/utilidades/imagenes/acciones/build.png'
}


function modificarCRUDTerceros(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {
        case 'adicionarTercero':
            if(verificarCamposGuardarTercero(arg)) {
                console.log("Estamos ingresando...");
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2259&parametros=";
                add_valores_a_mandar(valorAtributo('txtTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdTercero'));
                add_valores_a_mandar(valorAtributo('txtNombreTercero'));
                add_valores_a_mandar(valorAtributo('txtCodigoEnteControl'));
                add_valores_a_mandar(valorAtributo('txtDireccion'));
                add_valores_a_mandar(valorAtributo('txtTelefono'));
                add_valores_a_mandar(valorAtributo('cmbTipoPersona'));
                add_valores_a_mandar('');
                add_valores_a_mandar(valorAtributo('cmbEstadoTercero'));
                add_valores_a_mandar(valorAtributo('cmbTipoTercero'));
                add_valores_a_mandar(valorAtributo('cmbTipoRetencion'));
                add_valores_a_mandar(valorAtributo('txtDigitoVerificacion'));
                add_valores_a_mandar(valorAtributo('txtLugarExpedicionDocumento'));
                add_valores_a_mandar(valorAtributo('txtPrimerNombre'));
                add_valores_a_mandar(valorAtributo('txtSegundoNombre'));
                add_valores_a_mandar(valorAtributo('txtPrimerApellido'));
                add_valores_a_mandar(valorAtributo('txtSegundoApellido'));
                add_valores_a_mandar(valorAtributo('cmbAgenteDeclarante'));
                add_valores_a_mandar(valorAtributo('cmbTipoPersona'));
                add_valores_a_mandar(valorAtributo('txtIdMunicipio'));
                add_valores_a_mandar(valorAtributo('txtCodPais'));
                add_valores_a_mandar(valorAtributo('cmbTipoEntidad'));
                add_valores_a_mandar(valorAtributo('txtCIU'));
                add_valores_a_mandar(valorAtributo('cmbTipoContribuyente'));
                add_valores_a_mandar(valorAtributo('cmbTipoRetencion'));
                add_valores_a_mandar(valorAtributo('cmbAutorretenedorICA'));
                add_valores_a_mandar(valorAtributo('cmbAutorretenedorIVA'));
                add_valores_a_mandar(valorAtributo('txtManejaICA'));
                add_valores_a_mandar(valorAtributo('txtManejaTOPE'));
                add_valores_a_mandar(valorAtributo('cmbTipoEmpresa'));
                add_valores_a_mandar(valorAtributo('cmbTipoTercero'));
                add_valores_a_mandar(valorAtributo('txtCP'));
                add_valores_a_mandar(valorAtributo('txtRF'));
                add_valores_a_mandar(valorAtributo('txtCO'));
                add_valores_a_mandar(valorAtributo('cmbNacionalidadTercero'));
                add_valores_a_mandar(valorAtributo('cmbTipoDireccion'));
                add_valores_a_mandar(valorAtributo('cmbDireccionPrincipal'));
                add_valores_a_mandar(valorAtributo('cmbTipoTelefono'));
                add_valores_a_mandar(valorAtributo('cmbTelefonoPrincipal'));
                ajaxModificar();
            }
            break;
        case 'eliminarTercero':
            if (verificarCamposGuardarTercero(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=792&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
            }
            break;
        case 'modificarTercero':
            if (verificarCamposGuardarTercero(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2261&parametros=";
                add_valores_a_mandar(valorAtributo('txtTipoId'));
                add_valores_a_mandar(valorAtributo('txtIdTercero'));
                add_valores_a_mandar(valorAtributo('txtNombreTercero'));
                add_valores_a_mandar(valorAtributo('txtCodigoEnteControl'));
                add_valores_a_mandar(valorAtributo('txtDireccion'));
                add_valores_a_mandar(valorAtributo('txtTelefono'));
                add_valores_a_mandar(valorAtributo('cmbTipoPersona'));
                add_valores_a_mandar('');
                add_valores_a_mandar(valorAtributo('cmbEstadoTercero'));
                add_valores_a_mandar(valorAtributo('cmbTipoTercero'));
                add_valores_a_mandar(valorAtributo('cmbTipoRetencion'));
                add_valores_a_mandar(valorAtributo('txtDigitoVerificacion'));
                add_valores_a_mandar(valorAtributo('txtLugarExpedicionDocumento'));
                add_valores_a_mandar(valorAtributo('txtPrimerNombre'));
                add_valores_a_mandar(valorAtributo('txtSegundoNombre'));
                add_valores_a_mandar(valorAtributo('txtPrimerApellido'));
                add_valores_a_mandar(valorAtributo('txtSegundoApellido'));
                add_valores_a_mandar(valorAtributo('cmbAgenteDeclarante'));
                add_valores_a_mandar(valorAtributo('cmbTipoPersona'));
                add_valores_a_mandar(valorAtributo('txtIdMunicipio'));
                add_valores_a_mandar(valorAtributo('txtCodPais'));
                add_valores_a_mandar(valorAtributo('cmbTipoEntidad'));
                add_valores_a_mandar(valorAtributo('txtCIU'));
                add_valores_a_mandar(valorAtributo('cmbTipoContribuyente'));
                add_valores_a_mandar(valorAtributo('cmbTipoRetencion'));
                add_valores_a_mandar(valorAtributo('cmbAutorretenedorICA'));
                add_valores_a_mandar(valorAtributo('cmbAutorretenedorIVA'));
                add_valores_a_mandar(valorAtributo('txtManejaICA'));
                add_valores_a_mandar(valorAtributo('txtManejaTOPE'));
                add_valores_a_mandar(valorAtributo('cmbTipoEmpresa'));
                add_valores_a_mandar(valorAtributo('cmbTipoTercero'));
                add_valores_a_mandar(valorAtributo('txtCP'));
                add_valores_a_mandar(valorAtributo('txtRF'));
                add_valores_a_mandar(valorAtributo('txtCO'));
                add_valores_a_mandar(valorAtributo('cmbNacionalidadTercero'));
                add_valores_a_mandar(valorAtributo('cmbTipoDireccion'));
                add_valores_a_mandar(valorAtributo('cmbDireccionPrincipal'));
                add_valores_a_mandar(valorAtributo('cmbTipoTelefono'));
                add_valores_a_mandar(valorAtributo('cmbTelefonoPrincipal'));
                add_valores_a_mandar(valorAtributo('txtId'));
                ajaxModificar();
            }
            break;
    }
}


function limpiarFormTerceros(arg){

    switch (arg) {
        case 'limpiarTerceros':
            limpiaAtributo('txtTipoId', 0);
                limpiaAtributo('txtIdTercero', 0);
                limpiaAtributo('txtDigitoVerificacion', 0);
                limpiaAtributo('txtDigitoVerificacion', 0);
                limpiaAtributo('txtPrimerNombre', 0);
                limpiaAtributo('txtSegundoNombre', 0);
                limpiaAtributo('txtPrimerApellido', 0);
                limpiaAtributo('txtSegundoApellido', 0);
                limpiaAtributo('txtNombreTercero', 0);
                limpiaAtributo('cmbEstadoTercero', 0);
                limpiaAtributo('txtIdMunicipio', 0);
                limpiaAtributo('txtCodPais', 0);
                limpiaAtributo('cmbTipoEntidad', 0);
                limpiaAtributo('txtCodigoEnteControl', 0);
                limpiaAtributo('txtCIU', 0);
                limpiaAtributo('cmbTipoContribuyente', 0);
                limpiaAtributo('cmbTipoRetencion', 0);
                limpiaAtributo('txtManejaICA', 0);
                limpiaAtributo('txtManejaTOPE', 0);
                limpiaAtributo('cmbTipoEmpresa', 0);
                limpiaAtributo('cmbTipoTercero', 0);
                limpiaAtributo('txtCP', 0);
                limpiaAtributo('txtRF', 0);
                limpiaAtributo('txtCO', 0);
                limpiaAtributo('cmbTipoDireccion', 0);
                limpiaAtributo('txtDireccion', 0);
                limpiaAtributo('cmbDireccionPrincipal', 0);
                limpiaAtributo('cmbTipoTelefono', 0);
                limpiaAtributo('txtTelefono', 0);
                limpiaAtributo('cmbTelefonoPrincipal',0);
                limpiaAtributo('txtLugarExpedicionDocumento',0); 
                limpiaAtributo('cmbAgenteDeclarante',0); 
                limpiaAtributo('cmbNacionalidadTercero',0);
                limpiaAtributo('cmbTipoPersona',0);
                limpiaAtributo('cmbAutorretenedorICA',0);
                limpiaAtributo('cmbAutorretenedorIVA',0)
            break;
    
        default:
            break;
    }
    
}


function respuestaModificarCRUDTerceros(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
        switch (arg) {
            case 'adicionarTercero':
                limpiarFormTerceros('limpiarTerceros')
                setTimeout(() => {
                    tablasTerceros('listTercerosVentas'); 
                }, 150);
                
                alert('ADICIONADO CORRECTAMENTE!!!');
                break;
            case 'eliminarTercero':
                limpiarFormTerceros('limpiarTerceros')
                setTimeout(() => {
                    tablasTerceros('listTercerosVentas')
                }, 150);
                alert('ELIMINADO EXITOSAMENTE!!!');
                break;
            case 'modificarTercero':
                limpiarFormTerceros('limpiarTerceros')
                setTimeout(() => {
                    tablasTerceros('listTercerosVentas')
                }, 150);
                alert('MODIFICADO EXITOSAMENTE!!!');
                break;

        }
    }
}

function verificarCamposGuardarTercero(arg){

    switch(arg){
        case 'adicionarTercero': 
        console.log('Estamos Verificando campos');
            if(valorAtributo('txtTipoId') === ''){
                alert('EL TIPO DE IDENTIFICACION DEL TERCERO NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtTipoId') === 'CC'){
                if(valorAtributo('txtDigitoVerificacion') !== ''){
                    alert('EL TIPO DE TERCERO NO PUEDE LLEVAR DIGITO DE VERIFICACION ESTE CAMPO ESTA RESERVADO PARA TERCEROS CON TIPO DE ID NIT'); 
                    return false;
                } 
            }
            if(valorAtributo('txtTipoId') === 'NIT'){
                if(valorAtributo('txtDigitoVerificacion') === ''){
                    alert('PARA EL TIPO DE IDENTIFICACION NIT EL DIGITO VERIFICACION NO PUEDE SER VACIO'); 
                    return false; 
                }
            }
            if(valorAtributo('txtIdTercero') === ''){
                alert('EL NUMERO DE DOCUMENTO DE IDENTIFICACION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtPrimerNombre') === ''){
                alert('EL PRIMER NOMBRE NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtTipoId') === 'CC'){
                if(valorAtributo('txtPrimerApellido') === ''){
                    alert("PARA EL TIPO DE IDENTIFICACION CC EL PRIMER APELLIDO NO PUEDE SER VACIO");
                    return false;
                }
            }
            if(valorAtributo('txtNombreTercero') === ''){
                alert('EL NOMBRE COMPLETO DEL TERCERO NO PUEDE SER VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbEstadoTercero') === '' ){
                alert('EL ESTADO DEL TERCERO NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoPersona') === ''){
                alert('El TIPO DE PERSONA NO PUEDE ESTAR VACIO'); 
                return false; 
                
            }
            if(valorAtributo('cmbTipoPersona') === '1'){
                if(valorAtributo('cmbTipoEntidad') === ''){
                    alert('EL VALOR DE TIPO DE ENTIDAD NO PUEDE SER VACIO PARA TIPO DE PERSONA JURIDICA'); 
                    return false;
                }
            }
            if(valorAtributo('cmbTipoContribuyente') === ''){
                alert('EL TIPO DE CONTRIBUYENTE NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoRetencion') === ''){
                alert('LA RETENCION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoRetencion') === ''){
                alert('LA RETENCION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoEmpresa') === 'RAUT'){
                if(valorAtributo('cmbAutorretenedorICA') === ''){
                    alert('AUTORETENEDOR ICA NO PUEDE SER VACIO PARA AUTORETENEDOR');
                    return false;
                }
                if(valorAtributo('cmbAutorretenedorIVA') === ''){
                    alert('AUTORETENEDOR IVA NO PUEDE SER VACIO PARA AUTORETENEDOR');
                    return false;
                }
                 
            }
            if(valorAtributo('cmbTipoEmpresa') === ''){
                alert('TIPO DE EMPRESA NO PUEDE ESTAR VACIO');
                return false; 
            }
            if(valorAtributo('cmbTipoTercero') === ''){
                alert('EL TIPO DE TERCERO NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbAgenteDeclarante') === ''){
                alert('EL AGENTE DECLARANTE NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtIdMunicipio') === ''){
                alert('EL MUNICIPIO DE RESIDENCIA DEL TERCERO NO PUEDE ESTAR VACIO');
                return false;
            }
            if(valorAtributo('txtCodPais') === ''){
                alert('EL CODIGO DEL PAIS NO PUEDE SER VACIO');
                return false;
            }
            if(valorAtributo('cmbTipoTercero') === 'CLI' || valorAtributo('cmbTipoTercero') === 'PAR' || valorAtributo('cmbTipoTercero') === 'EMP'){
                if(valorAtributo('txtCP') === ''){
                    alert('PARA EL TIPO DE TERCERO EL CAMPO CODIGO POSTAL NO PUEDE QUEDAR VACIO');
                    return false; 
                }
                if(valorAtributo('txtRF') === ''){
                    alert('PARA EL TIPO DE TERCERO EL CAMPO RESPONSABILIDAD FISCAL NO PUEDE QUEDAR VACIO'); 
                    return false; 
                }
                if(valorAtributo('txtCO') === ''){
                    alert('PARA EL TIPO DE TERCERO EL CODIGO DE OBLIGACION NO PUEDE SER VACIO');
                    return false; 
                }
            }
            if(valorAtributo('cmbTipoDireccion') === ''){
                alert('EL TIPO DE DIRECCION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtDireccion') === ''){
                alert('LA DIRECCION NO PUEDE ESTAR VACIA'); 
                return false; 
            }
            if(valorAtributo('cmbDireccionPrincipal') === ''){
                alert('DIRECCION PRINCIPAL NO PUEDE SER VACIO'); 
                return false; 
            }

            if(valorAtributo('cmbTipoTelefono') === ''){
                alert('EL TIPO TELEFONO NO PUEDE SER VACIO'); 
                return false; 
            }
            if(valorAtributo('txtTelefono') === ''){
                alert('EL TELEFONO NO PUEDE SER VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTelefonoPrincipal') === ''){
                alert('TELEFONO PRINCIPAL NO PUEDE SER VACIO'); 
                return false; 
            }
        break;

        case 'modificarTercero': 
        console.log('Estamos Verificando campos');
            if(valorAtributo('txtTipoId') === ''){
                alert('EL TIPO DE IDENTIFICACION DEL TERCERO NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtTipoId') === 'CC'){
                if(valorAtributo('txtDigitoVerificacion') !== ''){
                    alert('EL TIPO DE TERCERO NO PUEDE LLEVAR DIGITO DE VERIFICACION ESTE CAMPO ESTA RESERVADO PARA TERCEROS CON TIPO DE ID NIT'); 
                    return false;
                } 
            }
            if(valorAtributo('txtTipoId') === 'NIT'){
                if(valorAtributo('txtDigitoVerificacion') === ''){
                    alert('PARA EL TIPO DE IDENTIFICACION NIT EL DIGITO VERIFICACION NO PUEDE SER VACIO'); 
                    return false; 
                }
            }
            if(valorAtributo('txtIdTercero') === ''){
                alert('EL NUMERO DE DOCUMENTO DE IDENTIFICACION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtPrimerNombre') === ''){
                alert('EL PRIMER NOMBRE NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtTipoId') === 'CC'){
                if(valorAtributo('txtPrimerApellido') === ''){
                    alert("PARA EL TIPO DE IDENTIFICACION CC EL PRIMER APELLIDO NO PUEDE SER VACIO");
                    return false;
                }
            }
            if(valorAtributo('txtNombreTercero') === ''){
                alert('EL NOMBRE COMPLETO DEL TERCERO NO PUEDE SER VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbEstadoTercero') === '' ){
                alert('EL ESTADO DEL TERCERO NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoPersona') === ''){
                alert('El TIPO DE PERSONA NO PUEDE ESTAR VACIO'); 
                return false; 
                
            }
            if(valorAtributo('cmbTipoPersona') === '1'){
                if(valorAtributo('cmbTipoEntidad') === ''){
                    alert('EL VALOR DE TIPO DE ENTIDAD NO PUEDE SER VACIO PARA TIPO DE PERSONA JURIDICA'); 
                    return false;
                }
            }
            if(valorAtributo('cmbTipoContribuyente') === ''){
                alert('EL TIPO DE CONTRIBUYENTE NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoRetencion') === ''){
                alert('LA RETENCION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoRetencion') === ''){
                alert('LA RETENCION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTipoEmpresa') === 'RAUT'){
                if(valorAtributo('cmbAutorretenedorICA') === ''){
                    alert('AUTORETENEDOR ICA NO PUEDE SER VACIO PARA AUTORETENEDOR');
                    return false;
                }
                if(valorAtributo('cmbAutorretenedorIVA') === ''){
                    alert('AUTORETENEDOR IVA NO PUEDE SER VACIO PARA AUTORETENEDOR');
                    return false;
                }
                 
            }
            if(valorAtributo('cmbTipoEmpresa') === ''){
                alert('TIPO DE EMPRESA NO PUEDE ESTAR VACIO');
                return false; 
            }
            if(valorAtributo('cmbTipoTercero') === ''){
                alert('EL TIPO DE TERCERO NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbAgenteDeclarante') === ''){
                alert('EL AGENTE DECLARANTE NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtIdMunicipio') === ''){
                alert('EL MUNICIPIO DE RESIDENCIA DEL TERCERO NO PUEDE ESTAR VACIO');
                return false;
            }
            if(valorAtributo('txtCodPais') === ''){
                alert('EL CODIGO DEL PAIS NO PUEDE SER VACIO');
                return false;
            }
            if(valorAtributo('cmbTipoTercero') === 'CLI' || valorAtributo('cmbTipoTercero') === 'PAR' || valorAtributo('cmbTipoTercero') === 'EMP'){
                if(valorAtributo('txtCP') === ''){
                    alert('PARA EL TIPO DE TERCERO EL CAMPO CODIGO POSTAL NO PUEDE QUEDAR VACIO');
                    return false; 
                }
                if(valorAtributo('txtRF') === ''){
                    alert('PARA EL TIPO DE TERCERO EL CAMPO RESPONSABILIDAD FISCAL NO PUEDE QUEDAR VACIO'); 
                    return false; 
                }
                if(valorAtributo('txtCO') === ''){
                    alert('PARA EL TIPO DE TERCERO EL CODIGO DE OBLIGACION NO PUEDE SER VACIO');
                    return false; 
                }
            }
            if(valorAtributo('cmbTipoDireccion') === ''){
                alert('EL TIPO DE DIRECCION NO PUEDE ESTAR VACIO'); 
                return false; 
            }
            if(valorAtributo('txtDireccion') === ''){
                alert('LA DIRECCION NO PUEDE ESTAR VACIA'); 
                return false; 
            }
            if(valorAtributo('cmbDireccionPrincipal') === ''){
                alert('DIRECCION PRINCIPAL NO PUEDE SER VACIO'); 
                return false; 
            }

            if(valorAtributo('cmbTipoTelefono') === ''){
                alert('EL TIPO TELEFONO NO PUEDE SER VACIO'); 
                return false; 
            }
            if(valorAtributo('txtTelefono') === ''){
                alert('EL TELEFONO NO PUEDE SER VACIO'); 
                return false; 
            }
            if(valorAtributo('cmbTelefonoPrincipal') === ''){
                alert('TELEFONO PRINCIPAL NO PUEDE SER VACIO'); 
                return false; 
            }
        break;

    }  
    
    return true;

}