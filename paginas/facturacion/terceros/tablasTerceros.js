function tablasTerceros(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
    switch (arg) {
        case "listTercerosVentas":
            ancho =
                $("#drag" + ventanaActual.num)
                .find("#divContenido")
                .width() - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2260&parametros=";
            add_valores_a_mandar(valorAtributo("txtNitTercero"));
            add_valores_a_mandar(valorAtributo("txtNombTercero"));
            add_valores_a_mandar(valorAtributo("cmbVigenteTercero"));
            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .jqGrid({
                    url: valores_a_mandar,
                    datatype: "xml",
                    mtype: "GET",

                    colNames: [
                        "TOT", "ID", "TIPOID", "IDTERCERO", "NOMBRE TERCERO", "CODIGO", "DIRECCION", "TELEFONO", "PERSONAJURIDICA", "DV", "ESTADO", "PROVEEDOR", "RETENCION",
                        "DIGITO DE VERIFICACION", "LUGAR EXPEDICION DOCUMENTO", "PRIMER NOMBRE", "SEGUNDO NOMBRE", "PRIMER APELLIDO", "SEGUNDO APELLIDO", "AGENTE DECLARANTE", "TIPO PERSONA",
                        "MUNICIPIO", "CODIGO PAIS", "TIPO ENTIDAD", "CIU", "TIPO CONTRIBUYENTE", "TIPO RETENCION", "AUTORRETENEDOR ICA", "AUTORETENEDOR IVA", "MANEJA ICA", "MANEJA TOPE",
                        "TIPO EMPRESA", "TIPO TERCERO", "CODIGO POSTAL", "RESPONSABILIDAD FISCAL", "CODIGO OBLIGACION", "NACIONALIDAD", "TIPO DIRECCION", "DIRECCION PRINCIPAL", "TIPO TELEFONO",
                        "TELEFONO PRINCIPAL"
                    ],
                    colModel: [{
                            name: "contador",
                            index: "contador",
                            width: anchoP(ancho, 2)
                        },
                        {
                            name: "ID",
                            index: "ID",
                            width: anchoP(ancho, 2)
                        },
                        {
                            name: "TIPOID",
                            index: "TIPOID",
                            width: anchoP(ancho, 3)
                        },
                        {
                            name: "IDTERCERO",
                            index: "IDTERCERO",
                            width: anchoP(ancho, 4)
                        },
                        {
                            name: "NOMBRE_TERCERO",
                            index: "NOMBRE_TERCERO",
                            width: anchoP(ancho, 15),
                        },
                        {
                            name: "CODIGO",
                            index: "CODIGO",
                            hidden: true
                        },
                        {
                            name: "DIRECCION",
                            index: "DIRECCION",
                            width: anchoP(ancho, 10)
                        },
                        {
                            name: "TELEFONO",
                            index: "TELEFONO",
                            width: anchoP(ancho, 5)
                        },
                        {
                            name: "PERSONAJURIDICA",
                            index: "PERSONAJURIDICA",
                            hidden: true
                        },
                        {
                            name: "DV",
                            index: "DV",
                            hidden: true
                        },
                        {
                            name: "ESTADO",
                            index: "ESTADO",
                            hidden: true
                        },
                        {
                            name: "PROVEEDOR",
                            index: "PROVEEDOR",
                            hidden: true
                        },
                        {
                            name: "TERCERO",
                            index: "TERCERO",
                            hidden: true
                        },
                        ///////////////////////////////
                        {
                            name: "digito_verificacion",
                            index: "digito_verificacion",
                            hidden: true
                        }, {
                            name: "lugar_expedicion_documento",
                            index: "lugar_expedicion_documento",
                            hidden: true
                        }, {
                            name: "primer_nombre",
                            index: "primer_nombre",
                            hidden: true
                        }, {
                            name: "segundo_nombre",
                            index: "segundo_nombre",
                            hidden: true
                        }, {
                            name: "primer_apellido",
                            index: "primer_apellido",
                            hidden: true
                        }, {
                            name: "segundo_apellido",
                            index: "segundo_apellido",
                            hidden: true
                        }, {
                            name: "agente_declarante",
                            index: "agente_declarante",
                            hidden: true
                        }, {
                            name: "tipo_persona",
                            index: "tipo_persona",
                            hidden: true
                        }, {
                            name: "id_municipio",
                            index: "id_municipio",
                            hidden: true
                        }, {
                            name: "cod_pais",
                            index: "cod_pais",
                            hidden: true
                        }, {
                            name: "tipo_entidad",
                            index: "tipo_entidad",
                            hidden: true
                        }, {
                            name: "ciu",
                            index: "ciu",
                            hidden: true
                        }, {
                            name: "tipo_contribuyente",
                            index: "tipo_contribuyente",
                            hidden: true
                        }, {
                            name: "tipo_retencion",
                            index: "tipo_retencion",
                            hidden: true
                        },{
                            name: "autoretenedor_ica",
                            index: "autoretenedor_ica",
                            hidden: true
                        },
                        {
                            name: "autoretenedor_iva",
                            index: "autoretenedor_iva",
                            hidden: true
                        },{
                            name: "maneja_ica",
                            index: "maneja_ica",
                            hidden: true
                        },{
                            name: "maneja_tope",
                            index: "maneja_tope",
                            hidden: true
                        },{
                            name: "tipo_empresa",
                            index: "tipo_empresa",
                            hidden: true
                        },{
                            name: "tipo_tercero",
                            index: "tipo_tercero",
                            hidden: true
                        },{
                            name: "codigo_postal",
                            index: "codigo_postal",
                            hidden: true
                        },{
                            name: "responsabilidad_fiscal",
                            index: "responsabilidad_fiscal",
                            hidden: true
                        },{
                            name: "codigo_obligacion",
                            index: "codigo_obligacion",
                            hidden: true
                        },{
                            name: "nacionalidad",
                            index: "nacionalidad",
                            hidden: true
                        },{
                            name: "tipo_direccion",
                            index: "tipo_direccion",
                            hidden: true
                        },{
                            name: "direccion_principal",
                            index: "direccion_principal",
                            hidden: true
                        },{
                            name: "tipo_telefono",
                            index: "tipo_telefono",
                            hidden: true
                        },{
                            name: "telefono_principal",
                            index: "telefono_principal",
                            hidden: true
                        }
                    ],

                    //  pager: jQuery('#pagerGrilla'),
                    height: 250,
                    width: ancho + 40,
                    onSelectRow: function (rowid) {
                        //       limpiarDivEditarJuan(arg);
                        var datosRow = jQuery("#drag" + ventanaActual.num)
                            .find("#" + arg)
                            .getRowData(rowid);
                        estad = 0;
                        asignaAtributo("lblIdTercero", datosRow.ID, 0);

                        asignaAtributo("txtId", datosRow.ID, 1);
                        asignaAtributo("txtTipoId", datosRow.TIPOID, 0);
                        asignaAtributo("txtIdTercero", datosRow.IDTERCERO, 0);
                        asignaAtributo("txtNombreTercero", datosRow.NOMBRE_TERCERO, 0);
                        asignaAtributo("txtCodigo", datosRow.CODIGO, 0);
                        asignaAtributo("txtDireccion", datosRow.DIRECCION, 0);
                        asignaAtributo("txtTelefono", datosRow.TELEFONO, 0);
                        asignaAtributo("cmbEstadoTercero", datosRow.ESTADO, 0);
                        asignaAtributo("cmbTipoTercero", datosRow.PROVEEDOR, 0);
                        asignaAtributo("txtDigitoVerificacion", datosRow.digito_verificacion, 0);
                        asignaAtributo("txtLugarExpedicionDocumento", datosRow.lugar_expedicion_documento, 0);
                        asignaAtributo("txtPrimerNombre", datosRow.primer_nombre, 0);
                        asignaAtributo("txtSegundoNombre", datosRow.segundo_nombre, 0);
                        asignaAtributo("txtPrimerApellido", datosRow.primer_apellido, 0);
                        asignaAtributo("txtSegundoApellido", datosRow.segundo_apellido, 0);
                        asignaAtributo("cmbAgenteDeclarante",datosRow.agente_declarante,0)
                        asignaAtributo("cmbTipoPersona", datosRow.tipo_persona, 0);
                        asignaAtributo("txtIdMunicipio", datosRow.id_municipio, 0);
                        asignaAtributo("txtCodPais", datosRow.cod_pais, 0);
                        asignaAtributo("cmbTipoEntidad", datosRow.tipo_entidad, 0);
                        asignaAtributo("txtCIU", datosRow.ciu, 0);
                        asignaAtributo("cmbTipoContribuyente", datosRow.tipo_contribuyente, 0);
                        asignaAtributo("cmbTipoRetencion", datosRow.tipo_retencion, 0);
                        asignaAtributo("cmbAutorretenedorICA", datosRow.autoretenedor_ica, 0);
                        asignaAtributo("cmbAutorretenedorIVA", datosRow.autoretenedor_iva,0)
                        asignaAtributo("txtManejaICA", datosRow.maneja_ica, 0);
                        asignaAtributo("txtManejaTOPE", datosRow.maneja_tope, 0);
                        asignaAtributo("cmbTipoEmpresa", datosRow.tipo_empresa, 0);
                        asignaAtributo("txtCP", datosRow.codigo_postal, 0);
                        asignaAtributo("txtRF", datosRow.responsabilidad_fiscal, 0);
                        asignaAtributo("txtCO", datosRow.codigo_obligacion, 0);
                        asignaAtributo("cmbNacionalidadTercero", datosRow.nacionalidad, 0);
                        asignaAtributo("cmbTipoDireccion",datosRow.tipo_direccion,0);
                        asignaAtributo("cmbDireccionPrincipal", datosRow.direccion_principal, 0);
                        asignaAtributo("cmbTipoTelefono", datosRow.tipo_telefono, 0);
                        asignaAtributo("cmbTelefonoPrincipal", datosRow.telefono_principal, 0);
                        buscarHistoria("listFuncionariosTerceros");
                        setTimeout(() => {
                            document.getElementById('divInformacionGeneral').style.display = ""; 
                        }, 150);
                    },
                });
            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .setGridParam({
                    url: valores_a_mandar
                })
                .trigger("reloadGrid");
            break;
    }
}