 <%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>

 <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
 <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
 <!-- instanciar bean de session -->


 <% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

 <table width="1100px" align="center" border="0" cellspacing="0" cellpadding="1">
   <tr>
     <td>
       <!-- AQUI COMIENZA EL TITULO -->
       <div align="center" id="tituloForma">
         <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
         <jsp:include page="../../titulo.jsp" flush="true">
           <jsp:param name="titulo" value="TERCEROS" />
         </jsp:include>
       </div>
       <!-- AQUI TERMINA EL TITULO -->
     </td>
   </tr>
   <tr>
     <td>
       <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
         <tr>
           <td>

             <div style="overflow:auto;height:350px; width:100%" id="divContenido">
               <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
               <div id="divBuscar" style="display:block">
                 <table width="100%" cellpadding="0" cellspacing="0" align="center">
                   <tr class="titulos" align="center">
                     <td width="10%">NIT</td>
                     <td width="60%">NOMBRE</td>
                     <td width="20%">PROVEEDOR</td>
                     <td width="10%">&nbsp;</td>
                   </tr>
                   <tr class="estiloImput">
                     <td colspan="1"><input class="estImputAdminLargo" type="text" id="txtNitTercero" style="width:90%"
                          /></td>
                     <td colspan="1"><input class="estImputAdminLargo" type="text" id="txtNombTercero" style="width:90%"
                          /></td>
                     <td><select class="selectH" size="1" id="cmbVigenteTercero" style="width:80%" >
                         <option value=""></option>
                         <option value="S">SI</option>
                         <option value="N">NO</option>
                       </select>
                     </td>
                     <td>
                       <input title="btn_bt754" type="button" class="small button blue" value="BUSCAR"
                         onclick="tablasTerceros('listTercerosVentas')" />
                     </td>
                   </tr>
                 </table>
                 <table id="listTercerosVentas" class="scroll"></table>
               </div>
             </div><!-- div contenido-->

             <div id="divEditar" style="display:block; width:100%">

               <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <td class="tdTitulo">
                    <img src="/clinica/utilidades/imagenes/acciones/buildup.png" class="imgNuevoAcordeon"
                      onclick="mostrarDiv('divInformacionGeneral',this)"
                      ondblclick="ocultarDiv('divInformacionGeneral',this);">
                    <p class="pTitulo">INFORMACION GENERAL</p>
                  </td>
                </tr>

                <div>
                  <tr id="divInformacionGeneral" style="display:none;">
                    <td>
                      <table width="100%">
                        <tr class="estiloImputIzq2" hidden>
                          <td width="30%">ID:</td>
                          <td width="70%"><input class="estImputAdminLargo" type="text" id="txtId" style="width:70%"
                              disabled="disabled" /></td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> TIPO DE DOCUMENTO </td>
                          <td width="70%">
                            <select class="selectH" name="" id="txtTipoId">
                              <option value=""></option>
                              <option value="CC">CEDULA DE CIUDADANIA</option>
                              <option value="NIT">NIT</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> IDENTIFICACIÓN</td>
                          <td width="70%"><input class="estImputAdminLargo" type="text" id="txtIdTercero"
                              style="width:70%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="dep">*</strong> DIGITO VERIFICACION</td>
                          <td width="10%"><input class="estImputAdminLargo" type="text" id="txtDigitoVerificacion"
                              style="width:10%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%">LUGAR EXPEDICION DE DOCUMENTO:</td>
                          <td width="70%"><input class="estImputAdminLargo" type="text"
                              id="txtLugarExpedicionDocumento" style="width:70%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td><strong class="req">*</strong> PRIMER NOMBRE</td>
                          <td>
                            <input class="estImputAdminLargo" type="text" id="txtPrimerNombre" style="width:30%" />
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td>SEGUNDO NOMBRE</td>
                          <td>
                            <input class="estImputAdminLargo" type="text" id="txtSegundoNombre" style="width:30%" />
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td>PRIMER APELLIDO</td>
                          <td>
                            <input class="estImputAdminLargo" type="text" id="txtPrimerApellido" style="width:30%" />
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td>SEGUNDO APELLIDO</td>
                          <td>
                            <input class="estImputAdminLargo" type="text" id="txtSegundoApellido" style="width:30%" />
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td><strong class="req">*</strong> NOMBRE COMPLETO</td>
                          <td>
                            <input class="estImputAdminLargo" type="text" id="txtNombreTercero" style="width:70%" />
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td><strong class="req">*</strong> TERCERO ACTIVO</td>
                          <td>
                            <select class="selectH" size="1" id="cmbEstadoTercero" style="width:20%" >
                              <option value=""></option>
                              <option value="1">SI</option>
                              <option value="0">NO</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td><strong class="req">*</strong> AGENTE DECLARANTE</td>
                          <td>
                            <select class="selectH" size="1" id="cmbAgenteDeclarante" style="width:20%" >
                              <option value=""></option>
                              <option value="1">SI</option>
                              <option value="0">NO</option>
                            </select>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </div>



                 <tr>
                   <td class="tdTitulo">
                     <img src="/clinica/utilidades/imagenes/acciones/buildup.png" class="imgNuevoAcordeon"
                       onclick="mostrarDiv('divInformacionEspecifica',this)"
                       ondblclick="ocultarDiv('divInformacionEspecifica',this);" alt="">
                     <p class="pTitulo">INFORMACION ESPECIFICA</p>
                   </td>
                 </tr>

                 <div>
                   <tr id="divInformacionEspecifica" style="display:none;">
                     <td>
                       <table width="100%">
                         <tr class="estiloImputIzq2">
                           <td width="30%"><strong class="req">*</strong> TIPO PERSONA</td>
                           <td>
                             <select class="selectH" size="1" id="cmbTipoPersona" style="width:20%" >
                              <option value=""></option>
                               <option value="1" onclick="tipoEntidad(this)">JURIDICA</option>
                               <option value="0" onclick="tipoEntidad(this)">NATURAL</option>
                             </select>
                           </td>
                         </tr>
                         <tr class="estiloImputIzq2">
                           <td width="30%"><strong class="req">*</strong> CIUDAD / MUNICIPIO</td>
                           <td width="70%"><input class="estImputAdminLargo" type="text" id="txtIdMunicipio"
                               style="width:70%" /></td>
                         </tr>
                         <tr class="estiloImputIzq2">
                           <td width="30%"><strong class="req">*</strong> CODIGO DEL PAIS</td>
                           <td width="70%"><input class="estImputAdminLargo" type="text" id="txtCodPais"
                               style="width:70%" /></td>
                         </tr>
                         <tr class="estiloImputIzq2" id="trTipoEntidad">
                           <td width="30%">TIPO ENTIDAD</td>
                           <td>
                             <select class="selectH" size="1" id="cmbTipoEntidad" style="width:20%" >
                              <option value=""></option> 
                              <option value="CEN">CENTRALIZADA</option>
                               <option value="DES">DESCENTRALIZADA</option>
                             </select>
                           </td>
                         </tr>
                         <tr class="estiloImputIzq2">
                           <td width="30%">CODIGO ENTIDAD</td>
                           <td width="70%"><input class="estImputAdminLargo" type="text" id="txtCodigoEnteControl"
                               style="width:70%" /></td>
                         </tr>
                         <tr class="estiloImputIzq2">
                          <td width="30%">CIU(CODIGO ACTIVIDAD ECONOMICA)</td>
                          <td width="70%"><input class="estImputAdminLargo" type="text" id="txtCIU"
                              style="width:70%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> TIPO CONTRIBUYENTE</td>
                          <td>
                            <select class="selectH" size="1" id="cmbTipoContribuyente" style="width:20%" >
                              <option value=""></option>
                              <option value="RC">REGIMEN COMUN</option>
                              <option value="RS">REGIMEN SIMPLIFICADO</option>
                              <option value="GC">GRAN CONTRIBUYENTE</option>
                              <option value="EE">EMPRESA ESTATAL</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> RETENCION</td>
                          <td>
                            <select class="selectH" size="1" id="cmbTipoRetencion" style="width:20%" >
                              <option value=""></option>
                              <option value="REXC" onclick="autoretenedor(this)">EXCENTO</option>
                              <option value="RAUT" onclick="autoretenedor(this)">AUTORETENEDOR</option>
                              <option value="RSDR" onclick="autoretenedor(this)">SE DEBE HACER RETENCION</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2" id="trAutorretenedorICA">
                          <td width="30%">AUTORETENEDOR ICA</td>
                          <td>
                            <select class="selectH" size="1" id="cmbAutorretenedorICA" style="width:20%" >
                              <option value=""></option>
                              <option value="N">NO</option>
                              <option value="S">SI</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2" id="trAutorretenedorIVA">
                          <td width="30%">AUTORETENEDOR IVA</td>
                          <td>
                            <select class="selectH" size="1" id="cmbAutorretenedorIVA" style="width:20%" >
                              <option value=""></option>
                              <option value="N">NO</option>
                              <option value="S">SI</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%">MANEJA ICA</td>
                          <td width="70%"><input class="estImputAdminLargo" type="text" id="txtManejaICA"
                              style="width:70%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%">MANEJA TOPE</td>
                          <td width="70%"><input class="estImputAdminLargo" type="text" id="txtManejaTOPE"
                              style="width:70%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong>TIPO DE EMPRESA</td>
                          <td>
                            <select class="selectH" size="1" id="cmbTipoEmpresa" style="width:20%" >
                              <option value=""></option>
                              <option value="MUN">MUNICIPAL</option>
                              <option value="DEP">DEPARTAMENTAL</option>
                              <option value="NAL">NACIONAL</option>
                              <option value="PRI">PRIVADA</option>
                              <option value="OTR">OTRA</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong>TIPO DE TERCERO</td>
                          <td>
                            <select class="selectH" size="1" id="cmbTipoTercero" style="width:20%" >
                              <option value=""></option>
                              <option value="CLI" onclick="activarCRCo(this)">CLIENTE</option>
                              <option value="PRO" onclick="activarCRCo(this)">PROOVEDOR</option>
                              <option value="EML" onclick="activarCRCo(this)">EMPLEADO</option>
                              <option value="CON" onclick="activarCRCo(this)">CONTRIBUYENTE</option>
                              <option value="PEN" onclick="activarCRCo(this)">PENSIONADO</option>
                              <option value="PAR" onclick="activarCRCo(this)">PARTICULAR</option>
                              <option value="EMP" onclick="activarCRCo(this)">EMPRESA</option>
                              <option value="OTR" onclick="activarCRCo(this)">OTRO</option>
                            </select>
                          </td>
                        </tr>



                        <tr class="estiloImputIzq2" id="txtCodigoPostal">
                          <td width="30%"><strong class="dep">*</strong> CODIGO POSTAL</td>
                          <td width="15%"><input class="estImputAdminLargo" type="text" onkeyup="this.value=this.value.replace(/[^\d]/,'')"  minlength="6"  maxlength="6" id="txtCP"
                              style="width:15%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2" id="txtResponsabilidadFiscal">
                          <td width="30%"><strong class="dep">*</strong> RESPONSABILIDAD FISCAL</td>
                          <td width="15%"><input class="estImputAdminLargo" type="text" onkeyup="this.value=this.value.replace(/[^\d]/,'')" minlength="1"  maxlength="7" id="txtRF"
                              style="width:15%" /></td>
                        </tr>
                        <tr class="estiloImputIzq2" id="txtCodigoObligacion">
                          <td width="30%"><strong class="dep">*</strong> CODIGO OBLIGACION</td>
                          <td width="15%"><input class="estImputAdminLargo" type="text" onkeyup="this.value=this.value.replace(/[^\d]/,'')" minlength="1"  maxlength="2" id="txtCO"
                              style="width:15%" /></td>
                        </tr>





                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> NACIONALIDAD</td>
                          <td>
                            <select class="selectH" size="1" id="cmbNacionalidadTercero" style="width:20%" >
                              <option value=""></option>
                              <option value="NAL">NACIONAL</option>
                              <option value="EXT">EXTRANJERO</option>
                            </select>
                          </td>
                        </tr>
                       </table>
                     </td>
                   </tr>
                 </div>

                 <tr>
                  <td class="tdTitulo">
                    <img src="/clinica/utilidades/imagenes/acciones/buildup.png" class="imgNuevoAcordeon"
                      onclick="mostrarDiv('trDireccionTercero',this)"
                      ondblclick="ocultarDiv('trDireccionTercero',this);" alt="">
                    <p class="pTitulo">INFORMACION DIRECCION</p>
                  </td>
                </tr>

                <div>
                  <tr id="trDireccionTercero" style="display:none;">
                    <td>
                      <table width="100%">
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> TIPO DE DIRECCION</td>
                          <td> 
                            <select class="selectH" size="1" id="cmbTipoDireccion" style="width:30%; height: 20px;" >
                              <option value=""></option>
                              <option value="RES">RESIDENCIA</option>
                              <option value="OFI">OFICINA</option>
                              <option value="TRA">TRABAJO</option>
                              <option value="CON">CONTACTO</option>
                              <option value="DIR">DIRECCION DE NOTIFICACION</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> DIRECCIÓN:</td>
                          <td>
                            <input class="estImputAdminLargo" type="text" id="txtDireccion" style="width:70%" />
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> DIRECCION PRINCIPAL</td>
                          <td>
                            <select class="selectH" size="1" id="cmbDireccionPrincipal" style="width:30%; height: 20px;" >
                              <option value=""></option>
                              <option value="S">SI</option>
                              <option value="N">NO</option>
                            </select>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </div>


                <tr>
                  <td class="tdTitulo">
                    <img src="/clinica/utilidades/imagenes/acciones/buildup.png" class="imgNuevoAcordeon"
                      onclick="mostrarDiv('trContactoTercero',this)"
                      ondblclick="ocultarDiv('trContactoTercero',this);" alt="">
                    <p class="pTitulo">INFORMACION DE CONTACTO TELEFONICO</p>
                  </td>
                </tr>

                <div>
                  <tr id="trContactoTercero" style="display:none;">
                    <td>
                      <table width="100%">
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> TIPO DE TELEFONO</td>
                          <td> 
                            <select class="selectH" size="1" id="cmbTipoTelefono" style="width:30%; height: 20px;" >
                              <option value=""></option>
                              <option value="RES">RESIDENCIA</option>
                              <option value="FAX">FAX</option>
                              <option value="CPI">CELULAR MOVIL PRIVADO</option>
                              <option value="CMP">CELULAR MOVIL PUBLICO</option>
                            </select>
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> TELEFONO</td>
                          <td>
                            <input class="estImputAdminLargo" type="text" id="txtTelefono" style="width:70%" />
                          </td>
                        </tr>
                        <tr class="estiloImputIzq2">
                          <td width="30%"><strong class="req">*</strong> TELEFONO PRINCIPAL</td>
                          <td>
                            <select class="selectH" size="1" id="cmbTelefonoPrincipal" style="width:30%; height: 20px;" >
                              <option value=""></option>
                              <option value="S">SI</option>
                              <option value="N">NO</option>
                            </select>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </div>
               </table>
           </td>
         </tr>
         </div>

         <tr hidden>
          <td class="tdTitulo">
            <img src="/clinica/utilidades/imagenes/acciones/anadir.png" class="imgNuevoAcordeon"
              onclick="mostrarDiv('trContactoTercero',this)" alt="">
            <p class="pTitulo">ADICIONAR DATOS A UN PROOVEDOR</p>
          </td>
        </tr>


         <div>
           <tr>
             <td>
               <table width="100%">
                 <tr>
                   <td colspan="2" align="center">
                     <input id="btn_limpia" title="btn_lp78e" class="small button blue" type="button" value="LIMPIAR"
                       onclick="limpiarFormTerceros('limpiarTerceros')">
                     <input id="btn_crea" title="E56" class="small button blue" type="button" value="ADICIONAR"
                       onclick="modificarCRUDTerceros('adicionarTercero')">
                     <input name="btn_elimina" title="E57" type="button" class="small button blue" value="ELIMINAR"
                       onclick="modificarCRUDTerceros('eliminarTercero')">
                     <input name="btn_modifica" title="E57" type="button" class="small button blue" value="MODIFICAR"
                       onclick="modificarCRUDTerceros('modificarTercero')">
                   </td>
                 </tr>
               </table>
             </td>
           </tr>
         </div>
       </table>


       </div><!-- divEditar-->


     </td>
   </tr>
 </table>

 </td>
 </tr>

 </table>