 <%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="TERCEROS" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

   <div style="overflow:auto;height:350px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
      <div id="divBuscar"  style="display:block"   >
      <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
           <tr class="titulos" align="center">
              <td width="10%">NIT</td>                            
              <td width="60%">NOMBRE</td>
              <td width="20%">PROVEEDOR</td>
              <td width="10%">&nbsp;</td>                             
           </tr>
           <tr class="estiloImput"> 
                      <td colspan="1"><input type="text" id="txtNitTercero"  style="width:90%"   /></td> 
                      <td colspan="1"><input type="text" id="txtNombTercero"  style="width:90%"   /></td> 
                      <td><select size="1" id="cmbVigenteTercero" style="width:80%"  >
                          <option value=""></option>
                          <option value="S">SI</option>
                          <option value="N">NO</option>                  
                         </select>                     
                      </td>    
              <td>
                  <input title="btn_bt754" type="button" class="small button blue" value="BUSCAR"  onclick="buscarHistoria('listTercerosVentas')"   />                    
              </td>         
          </tr>	                          
      </table>
     <table id="listTercerosVentas" class="scroll"></table>  
    </div>              
  </div><!-- div contenido-->

<div id="divEditar" style="display:block; width:100%" >  

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr class="titulosCentrados">
      <td colspan="3">DATOS DEL TERCERO
      </td>   
    </tr>  
    <tr>
      <td>
         <table width="100%">
            <tr   class="estiloImputIzq2">
              <td width="30%">ID:</td><td width="70%"><input type="text" id="txtId" style="width:70%" disabled="disabled" /></td> 
            </tr>
            <tr   class="estiloImputIzq2">
              <td width="30%">TIPO IDENTIFICACIÓN:</td><td width="70%"><input type="text" id="txtTipoId" style="width:70%"/></td> 
            </tr>
            <tr   class="estiloImputIzq2">
              <td width="30%">IDENTIFICACIÓN:</td><td width="70%"><input type="text" id="txtIdTercero" style="width:70%"/></td> 
            </tr>	
            <tr   class="estiloImputIzq2">                         
              <td>NOMBRE TERCERO:</td><td>              
					<input type="text" id="txtNombreTercero" style="width:70%"/>
              </td>                                             
            </tr>	              
            <tr class="estiloImputIzq2">                          
              <td>CODIGO</td><td>              
					<input type="text" id="txtCodigo" style="width:70%"/>
              </td>                                             
            </tr>	            
             <tr   class="estiloImputIzq2">                         
              <td>DIRECCIÓN:</td><td>              
					<input type="text" id="txtDireccion" style="width:70%"/>
              </td>                                             
            </tr>
            <tr   class="estiloImputIzq2">                         
              <td>TELEFONO:</td><td>               
					<input type="text" id="txtTelefono" style="width:70%"/>
              </td>                                             
            </tr>           
             <tr class="estiloImputIzq2">                           
              <td>PERSONA JURIDICA</td><td>
               <select size="1" id="cmbPersonaJuridica" style="width:20%"  >
                  <option value="1">SI</option>
                  <option value="0">NO</option>                                                    
                 </select>	    
                </td>                                             
             </tr>   
              <tr class="estiloImputIzq2">                           
              <td>DV</td><td>  
              	<input type="text" id="txtDV" style="width:20%"/>
                </td>                                             
             </tr>  
             <tr class="estiloImputIzq2">                           
              <td>ESTADO</td><td>  
              	 <select size="1" id="cmbEstadoTercero" style="width:20%"  >           
                  <option value="1">SI</option>
                  <option value="0">NO</option>                                                    
                 </select>	
                </td>                                             
             </tr>
             <tr class="estiloImputIzq2">                           
              <td>PROVEEDOR</td><td> <select size="1" id="cmbProveedor" style="width:20%"  >
                  <option value="S">SI</option>
                  <option value="N">NO</option>                                                    
                 </select>      
                </td>                                             
             </tr>
             <tr>
               <td colspan="2" align="center">
				  <input id="btn_limpia" title="btn_lp78e" class="small button blue" type="button" value="LIMPIAR"  onclick="limpiarDivEditarJuan('limpiarTerceros')">        
          <input id="btn_crea" title="E56" class="small button blue" type="button"  value="ADICIONAR"  onclick="modificarCRUD('adicionarTercero')"> 
          <input name="btn_elimina" title="E57" type="button" class="small button blue" value="ELIMINA" onclick="modificarCRUD('eliminarTercero')">                   
          <input name="btn_modifica" title="E57" type="button" class="small button blue" value="MODIFICA" onclick="modificarCRUD('modificarTercero')">           
               </td>
             </tr>                                                     
        </table>
      </td>  
    </tr>  
  </table> 
  
   
</div><!-- divEditar--> 
      

        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>

