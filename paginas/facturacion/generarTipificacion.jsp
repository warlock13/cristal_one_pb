<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 

<%@ page import="java.util.zip.ZipEntry" %> 
<%@ page import="java.util.zip.ZipOutputStream" %> 
<%@ page import="java.util.List" %> 
<%@ page import="java.util.ArrayList" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 


<!DOCTYPE HTML >  
<%
    FileOutputStream fosT = null;
    ZipOutputStream zipOutT = null;
    File file = null;
    try {
        Connection conexion = beanSession.cn.getConexion();

        String id_envio = request.getParameter("id_envio");
        String remision = request.getParameter("remision");
        int bean = (int) (Math.random() * 10000);

        String ruta_tipificacion = "/opt/extras_cw/tipificacion/";
        String ruta_archivo_tipificacion_comprimido = ruta_tipificacion + bean + "_" + id_envio + ".zip";
        PreparedStatement pr = null;

        //GENERAR ZIP TIPIFICACION
        String sql = "SELECT "
                + "	af.id_factura "
                + " FROM "
                + "	facturacion.rips_af af "
                + "    INNER JOIN facturacion.tipo_rips_archivos tra ON tra.id = af.id_archivo "
                + "WHERE "
                + "	tra.id_envio = " + id_envio;

        pr = conexion.prepareStatement(sql);
        ResultSet facturas = pr.executeQuery();

        fosT = new FileOutputStream(ruta_archivo_tipificacion_comprimido);
        zipOutT = new ZipOutputStream(fosT);

        while (facturas.next()) {

            String sqlT = "SELECT "
                    + "	BTRIM(nombre_archivo) nombre_archivo, "
                    + "   BTRIM(numero_factura) numero_factura "
                    + "FROM "
                    + "	archivos.tipificacion "
                    + "WHERE "
                    + "	id_factura = '" + facturas.getString("id_factura") + "'";

            PreparedStatement prT = conexion.prepareStatement(sqlT);
            ResultSet archivosFactura = prT.executeQuery();

            while (archivosFactura.next()) {
                try {
                    System.err.println(ruta_tipificacion + archivosFactura.getString("numero_factura") + "/" + archivosFactura.getString("nombre_archivo") + ".pdf");
                    File fileToZip = new File(ruta_tipificacion + archivosFactura.getString("numero_factura") + "/" + archivosFactura.getString("nombre_archivo") + ".pdf");

                    System.err.println("NUEVO ARCHIVO ZIP: " + fileToZip.getAbsolutePath());
                    FileInputStream fis = new FileInputStream(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(archivosFactura.getString("numero_factura") + "/" + fileToZip.getName());
                    zipOutT.putNextEntry(zipEntry);

                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = fis.read(bytes)) >= 0) {
                        zipOutT.write(bytes, 0, length);
                    }
                    fis.close();
                } catch (Exception e) {
                    System.err.println("ERROR::");
                    e.printStackTrace();
                }
            }
        }
        zipOutT.close();
        fosT.close();
        
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=" + remision + "_" + id_envio + ".zip");

        file = new File(ruta_archivo_tipificacion_comprimido);
        FileInputStream fileIn = new FileInputStream(file);
        ServletOutputStream out2 = response.getOutputStream();

        byte[] outputByte = new byte[4096];
        while (fileIn.read(outputByte, 0, 4096) != -1) {
            out2.write(outputByte, 0, 4096);
        }
        fileIn.close();
        out2.flush();
        out2.close();
    } catch (Exception e) {
        System.err.println("ERROR AL GENERAR LA TIPIFICACION:" + e.getMessage());
        e.printStackTrace();
    } finally {
        if(file != null){
            file.delete();
        }
    }
%>