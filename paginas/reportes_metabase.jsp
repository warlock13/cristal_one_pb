<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Base64" %>
<%@ page import="com.auth0.jwt.JWT" %>
<%@ page import="com.auth0.jwt.algorithms.Algorithm" %>
<%@ page import="java.util.Base64" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>

<%
String metabaseSiteUrl = request.getParameter("metabase_site_url"); 
String metabaseSecretKey = request.getParameter("metabase_secret_key");
Integer metabaseId = Integer.parseInt(request.getParameter("id_metabase"));

long tenMinutesInMillis = 10 * 60 * 1000; // 10 minutos en milisegundos
Date expiration = new Date(System.currentTimeMillis() + tenMinutesInMillis);

String token = JWT.create().withClaim("resource", new HashMap<String, Object>() {{put("question", metabaseId);}}).withClaim("params", new HashMap<>()).withExpiresAt(expiration).sign(Algorithm.HMAC256(metabaseSecretKey));
String iframeUrl = metabaseSiteUrl + "/embed/question/" + token + "#bordered=true&titled=true";
System.out.print(iframeUrl);   
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>        
    <meta http-equiv="Cache-Control" content="no-cache"/>        
    <title>CRISTAL WEB REPORTES METABASE</title>        
</head>
<body>
    <div class="card-body">
        <iframe src="<%= iframeUrl %>" style="width:100%; height:800px; border: white" allowtransparency></iframe>

    </div>
</body>


</html>