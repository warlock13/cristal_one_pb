<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<!DOCTYPE HTML > 
<style type="text/css">
#div1 {
overflow:scroll;
height:300px;
width:100%;
}
#div1 table {
width:100%;
}

.misbordes {
border-right: solid #a6c9e2 1px;
border-bottom: solid #a6c9e2 1px;
height:22px;
font-size: 11px;
font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
}
.paratrplantilla{
background-color: white;
font-size: 11px;
font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
}

</style> 

<!DOCTYPE html>
<html lang="en">
    <head>
        <script type="script" src="main.js"></script>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport">
        <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    
  
<div style="background-color: white; height: 300px; width: 100%; overflow: scroll;">

<table>
    <tr>
        <td>
            LABORATORIO
        </td>
        <td>
            ANALITO
        </td>
        <td>
            RESULTADO ANTERIOR
        </td>
        <td>
            FECHA RESULTADO
        </td>
        <td>
            RESULTADO ACTUAL
        </td>
        <td>
            FECHA RESULTADO
        </td>
    </tr>
    <tr>
        <td>
            CREATININA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            CREATININA
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c1">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c2">
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c3">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c4">
        </td>
    </tr>
    <tr>
        <td>
            COLESTEROL DE ALTA DENSIDAD
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            COLESTEROL DE ALTA DENSIDAD
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c5">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c6">
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c7">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c8">
        </td>
    </tr>
    <tr>
        <td>
            COLESTEROL DE BAJA DENSIDAD [LDL] AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            COLESTEROL DE BAJA DENSIDAD
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c9">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c10">
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c11">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c12">
        </td>
    </tr>
    <tr>
        <td>
            COLESTEROL TOTAL
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            COLESTEROL TOTAL
        </td>
        <td>
            <input type="text" id="c13">
        </td>
        <td>
            <input type="date" id="c14">
        </td>
        <td>
            <input type="text" id="c15">
        </td>
        <td>
            <input type="date" id="c16">
        </td>
    </tr>
    <tr>
        <td>
            UROANALISIS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            PROTEINAS
        </td>
        <td>
            <input type="text" id="c17">
        </td>
        <td>
            <input type="date" id="c18">
        </td>
        <td>
            <input type="text" id="c19">
        </td>
        <td>
            <input type="date" id="c20">
        </td>
    </tr>
    <tr>
        <td>
            COCIENTE DE ALBUMINURIA CREATINURIA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            COCIENTE DE ALBUMINURIA / CREATINURIA
        </td>
        <td>
            <input type="text" id="c21">
        </td>
        <td>
            <input type="date" id="c22">
        </td>
        <td>
            <input type="text" id="c23">
        </td>
        <td>
            <input type="date" id="c24">
        </td>
    </tr>
    <tr>
        <td>
            HEMOGRAMA IV
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            LEUCOCITOS
        </td>
        <td>
            <input type="text" id="c25">
        </td>
        <td>
            <input type="date" id="c26">
        </td>
        <td>
            <input type="text" id="c27">
        </td>
        <td>
            <input type="date" id="c28">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HEMOGLOBINA
        </td>
        <td>
            <input type="text" id="c29">
        </td>
        <td>
            <input type="date" id="c30">
        </td>
        <td>
            <input type="text" id="c31">
        </td>
        <td>
            <input type="date" id="c32">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HEMATOCRITO
        </td>
        <td>
            <input type="text" id="c33">
        </td>
        <td>
            <input type="date" id="c34">
        </td>
        <td>
            <input type="text" id="c35">
        </td>
        <td>
            <input type="date" id="c36">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            PLAQUETAS
        </td>
        <td>
            <input type="text" id="c37">
        </td>
        <td>
            <input type="date" id="c38">
        </td>
        <td>
            <input type="text" id="c39">
        </td>
        <td>
            <input type="date" id="c40">
        </td>
    </tr>
    <tr>
        <td>
            HEMOGLOBINA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HEMOGLOBINA
        </td>
        <td>
            <input type="text" id="c41">
        </td>
        <td>
            <input type="date" id="c42">
        </td>
        <td>
            <input type="text" id="c43">
        </td>
        <td>
            <input type="date" id="c44">
        </td>
    </tr>
    <tr>
        <td>
            GLUCOSA EN SUERO U OTRO FLUIDO DIFERENTE A ORINA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            GLUCOSA
        </td>
        <td>
            <input type="text" id="c45">
        </td>
        <td>
            <input type="date" id="c46">
        </td>
        <td>
            <input type="text" id="c47">
        </td>
        <td>
            <input type="date" id="c48">
        </td>
    </tr>
    <tr>
        <td>
            GLUCOSA PRE Y POST CARGA DE GLUCOSA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            GLICEMIA
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c49">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c50">
        </td>
        <td>
            <input type="text" id="formulario.laboratorios_nefro.c51">
        </td>
        <td>
            <input type="date" id="formulario.laboratorios_nefro.c52">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            GLUCOSA POST CARGA
        </td>
        <td>
            <input type="text" id="c53">
        </td>
        <td>
            <input type="date" id="c54">
        </td>
        <td>
            <input type="text" id="c55">
        </td>
        <td>
            <input type="date" id="c56">
        </td>
    </tr>
    <tr>
        <td>
            GLUCOSA PRE Y POST PRANDIAL
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            GLICEMIA
        </td>
        <td>
            <input type="text" id="c57">
        </td>
        <td>
            <input type="date" id="c58">
        </td>
        <td>
            <input type="text" id="c59">
        </td>
        <td>
            <input type="date" id="c60">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            GLUCOSA POST PRANDIAL
        </td>
        <td>
            <input type="text" id="c61">
        </td>
        <td>
            <input type="date" id="c62">
        </td>
        <td>
            <input type="text" id="c63">
        </td>
        <td>
            <input type="date" id="c64">
        </td>
    </tr>
    <tr>
        <td>
            HEMOGLOBINA GLICOSILADA MANUAL O SEMIAUTOMATIZADA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HEMOGLOBINA A1c
        </td>
        <td>
            <input type="text" id="c65">
        </td>
        <td>
            <input type="date" id="c66">
        </td>
        <td>
            <input type="text" id="c67">
        </td>
        <td>
            <input type="date" id="c68">
        </td>
    </tr>
    <tr>
        <td>
            TRIGLICERIDOS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            TRIGLICERIDOS
        </td>
        <td>
            <input type="text" id="c69">
        </td>
        <td>
            <input type="date" id="c70">
        </td>
        <td>
            <input type="text" id="c71">
        </td>
        <td>
            <input type="date" id="c72">
        </td>
    </tr>
    <tr>
        <td>
            DEPURACION DE CREATININA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            DEPURACION DE CREATININA
        </td>
        <td>
            <input type="text" id="c73">
        </td>
        <td>
            <input type="date" id="c74">
        </td>
        <td>
            <input type="text" id="c75">
        </td>
        <td>
            <input type="date" id="c76">
        </td>
    </tr>
    <tr>
        <td>
            PROTEINAS EN ORINA 24H
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            PROTEINAS EN ORINA 24H
        </td>
        <td>
            <input type="text" id="c77">
        </td>
        <td>
            <input type="date" id="c78">
        </td>
        <td>
            <input type="text" id="c79">
        </td>
        <td>
            <input type="date" id="c80">
        </td>
    </tr>
    <tr>
        <td>
            HORMONA PARATIROIDEA MOLECULA INTACTA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HORMONA PARATIROIDEA MOLECULA INTACTA
        </td>
        <td>
            <input type="text" id="c81">
        </td>
        <td>
            <input type="date" id="c82">
        </td>
        <td>
            <input type="text" id="c83">
        </td>
        <td>
            <input type="date" id="c84">
        </td>
    </tr>
    <tr>
        <td>
            NITROGENO UREICO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            NITROGENO UREICO
        </td>
        <td>
            <input type="text" id="c85">
        </td>
        <td>
            <input type="date" id="c86">
        </td>
        <td>
            <input type="text" id="c87">
        </td>
        <td>
            <input type="date" id="c88">
        </td>
    </tr>
    <tr>
        <td>
            ACIDO URICO EN SUERO U OTROS FLUIDOS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            ACIDO URICO
        </td>
        <td>
            <input type="text" id="c89">
        </td>
        <td>
            <input type="date" id="c90">
        </td>
        <td>
            <input type="text" id="c91">
        </td>
        <td>
            <input type="date" id="c92">
        </td>
    </tr>
    <tr>
        <td>
            ALBUMINA EN ORINA DE 24 HORAS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            ALBUMINA EN ORINA DE 24 HORAS
        </td>
        <td>
            <input type="text" id="c93">
        </td>
        <td>
            <input type="date" id="c94">
        </td>
        <td>
            <input type="text" id="c95">
        </td>
        <td>
            <input type="date" id="c96">
        </td>
    </tr>
    <tr>
        <td>
            ALBUMINA EN SUERO U OTROS FLUIDOS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            ALBUMINA
        </td>
        <td>
            <input type="text" id="c97">
        </td>
        <td>
            <input type="date" id="c98">
        </td>
        <td>
            <input type="text" id="c99">
        </td>
        <td>
            <input type="date" id="c100">
        </td>
    </tr>
    <tr>
        <td>
            POTASIO EN SUERO U OTROS FLUIDOS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            POTASIO
        </td>
        <td>
            <input type="text" id="c101">
        </td>
        <td>
            <input type="date" id="c102">
        </td>
        <td>
            <input type="text" id="c103">
        </td>
        <td>
            <input type="date" id="c104">
        </td>
    </tr>
    <tr>
        <td>
            CALCIO IONICO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            CALCIO IONICO
        </td>
        <td>
            <input type="text" id="c105">
        </td>
        <td>
            <input type="date" id="c106">
        </td>
        <td>
            <input type="text" id="c107">
        </td>
        <td>
            <input type="date" id="c108">
        </td>
    </tr>
    <tr>
        <td>
            FOSFORO EN SUERO U OTROS FLUIDOS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            FOSFORO
        </td>
        <td>
            <input type="text" id="c109">
        </td>
        <td>
            <input type="date" id="c110">
        </td>
        <td>
            <input type="text" id="c111">
        </td>
        <td>
            <input type="date" id="c112">
        </td>
    </tr>
    <tr>
        <td>
            SODIO EN SUERO U OTROS FLUIDOS
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            SODIO
        </td>
        <td>
            <input type="text" id="c113">
        </td>
        <td>
            <input type="date" id="c114">
        </td>
        <td>
            <input type="text" id="c115">
        </td>
        <td>
            <input type="date" id="c116">
        </td>
    </tr>
    <tr>
        <td>
            FERRITINA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            FERRITINA
        </td>
        <td>
            <input type="text" id="c117">
        </td>
        <td>
            <input type="date" id="c118">
        </td>
        <td>
            <input type="text" id="c119">
        </td>
        <td>
            <input type="date" id="c120">
        </td>
    </tr>
    <tr>
        <td>
            SATURACION DE TRANSFERRINA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            TRANSFERRINA
        </td>
        <td>
            <input type="text" id="c121">
        </td>
        <td>
            <input type="date" id="c122">
        </td>
        <td>
            <input type="text" id="c123">
        </td>
        <td>
            <input type="date" id="c124">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            PORCENTAJE DE SATURACION
        </td>
        <td>
            <input type="text" id="c125">
        </td>
        <td>
            <input type="date" id="c126">
        </td>
        <td>
            <input type="text" id="c127">
        </td>
        <td>
            <input type="date" id="c128">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HIERRO TOTAL
        </td>
        <td>
            <input type="text" id="c129">
        </td>
        <td>
            <input type="date" id="c130">
        </td>
        <td>
            <input type="text" id="c133">
        </td>
        <td>
            <input type="date" id="c134">
        </td>
    </tr>
    <tr>
        <td>
            HIERRO TOTAL
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HIERRO TOTAL
        </td>
        <td>
            <input type="text" id="c135">
        </td>
        <td>
            <input type="date" id="c136">
        </td>
        <td>
            <input type="text" id="c137">
        </td>
        <td>
            <input type="date" id="c138">
        </td>
    </tr>
    <tr>
        <td>
            HORMONA ESTIMULANTE DEL TIROIDES
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            HORMONA ESTIMULANTE DEL TIROIDES
        </td>
        <td>
            <input type="text" id="c139">
        </td>
        <td>
            <input type="date" id="c140">
        </td>
        <td>
            <input type="text" id="c141">
        </td>
        <td>
            <input type="date" id="c142">
        </td>
    </tr>
    <tr>
        <td>
            ANTIGENO ESPECIFICO DE PROSTATA SEMIAUTOMATIZADO O AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            ANTIGENO ESPECIFICO DE PROSTATA (PSA)
        </td>
        <td>
            <input type="text" id="c143">
        </td>
        <td>
            <input type="date" id="c144">
        </td>
        <td>
            <input type="text" id="c145">
        </td>
        <td>
            <input type="date" id="c146">
        </td>
    </tr>
    <tr>
        <td>
            SANGRE OCULTA EN MATERIA FECAL [GUAYACO O EQUIVALENTE]
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            SANGRE OCULTA
        </td>
        <td>
            <input type="text" id="c147">
        </td>
        <td>
            <input type="date" id="c148">
        </td>
        <td>
            <input type="text" id="c149">
        </td>
        <td>
            <input type="date" id="c150">
        </td>
    </tr>
    <tr>
        <td>
            PROTEINA C REACTIVA MANUAL O SEMIAUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            PROTEINA C
        </td>
        <td>
            <input type="text" id="c151">
        </td>
        <td>
            <input type="date" id="c152">
        </td>
        <td>
            <input type="text" id="c153">
        </td>
        <td>
            <input type="date" id="c154">
        </td>
    </tr>
    <tr>
        <td>
            TIEMPO DE PROTROMBINA [TP]
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            TIEMPO DE PROTROMBINA
        </td>
        <td>
            <input type="text" id="c155">
        </td>
        <td>
            <input type="date" id="c156">
        </td>
        <td>
            <input type="text" id="c157">
        </td>
        <td>
            <input type="date" id="c158">
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            INR
        </td>
        <td>
            <input type="text" id="c159">
        </td>
        <td>
            <input type="date" id="c160">
        </td>
        <td>
            <input type="text" id="c161">
        </td>
        <td>
            <input type="date" id="c162">
        </td>
    </tr>
    <tr>
        <td>
            TIEMPO DE TROMBOPLASTINA PARCIAL [TTP]
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            TIEMPO DE TROMBOPLASTINA PARCIAL
        </td>
        <td>
            <input type="text" id="c163">
        </td>
        <td>
            <input type="date" id="c164">
        </td>
        <td>
            <input type="text" id="c165">
        </td>
        <td>
            <input type="date" id="c166">
        </td>
    </tr>
    <tr>
        <td>
            BICARBONATO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            BICARBONATO
        </td>
        <td>
            <input type="text" id="c167">
        </td>
        <td>
            <input type="date" id="c168">
        </td>
        <td>
            <input type="text" id="c169">
        </td>
        <td>
            <input type="date" id="c170">
        </td>
    </tr>
    <tr>
        <td>
            FOSFATASA ALCALINA
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            FOSFATASA ALCALINA
        </td>
        <td>
            <input type="text" id="c171">
        </td>
        <td>
            <input type="date" id="c172">
        </td>
        <td>
            <input type="text" id="c173">
        </td>
        <td>
            <input type="date" id="c174">
        </td>
    </tr>
    <tr>
        <td>
            COMPLEMENTO SERICO C3 AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            COMPLEMENTO SERICO C3
        </td>
        <td>
            <input type="text" id="c175">
        </td>
        <td>
            <input type="date" id="c176">
        </td>
        <td>
            <input type="text" id="c177">
        </td>
        <td>
            <input type="date" id="c178">
        </td>
    </tr>
    <tr>
        <td>
            COMPLEMENTO SERICO C4 AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            COMPLEMENTO SERICO C4
        </td>
        <td>
            <input type="text" id="c179">
        </td>
        <td>
            <input type="date" id="c180">
        </td>
        <td>
            <input type="text" id="c181">
        </td>
        <td>
            <input type="date" id="c182">
        </td>
    </tr>
    <tr>
        <td>
            ANTICUERPOS ANTINUCLEARES AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            ANTICUERPOS ANTINUCLEARES
        </td>
        <td>
            <input type="text" id="c183">
        </td>
        <td>
            <input type="date" id="c184">
        </td>
        <td>
            <input type="text" id="c185">
        </td>
        <td>
            <input type="date" id="c186">
        </td>
    </tr>
    <tr>
        <td>
            CITOPLASMA DE NEUTROFILOS ANTICUERPOS TOTALES [C ANCA O P ANCA] AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            CITOPLASMA DE NEUTROFILOS ANTICUERPOS TOTALES
        </td>
        <td>
            <input type="text" id="c187">
        </td>
        <td>
            <input type="date" id="c188">
        </td>
        <td>
            <input type="text" id="c189">
        </td>
        <td>
            <input type="date" id="c190">
        </td>
    </tr>
    <tr>
        <td>
            FACTOR REUMATOIDEO SEMIAUTOMATIZADO O AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            FACTOR REUMATOIDEO
        </td>
        <td>
            <input type="text" id="c191">
        </td>
        <td>
            <input type="date" id="c192">
        </td>
        <td>
            <input type="text" id="c193">
        </td>
        <td>
            <input type="date" id="c194">
        </td>
    </tr>
    <tr>
        <td>
            Sm ANTICUERPOS SEMIAUTOMATIZADO O AUTOMATIZADO
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
            Sm ANTICUERPOS
        </td>
        <td>
            <input type="text" id="c195">
        </td>
        <td>
            <input type="date" id="c196">
        </td>
        <td>
            <input type="text" id="c197">
        </td>
        <td>
            <input type="date" id="c198">
        </td>
    </tr>    
</table>
</div>
</body>
</html>