<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import =  "java.util.ArrayList"%>
<%@ page import =  "java.util.List"%>


<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />

<!DOCTYPE HTML >
<head>    
    <link href="/clinica/paginas/encuesta/templateStyles.css " rel="stylesheet" type="text/css">    
</head>
<body>
<%
    ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
    Connection conexion = iConnection.getConnection();
    Conexion cn = new Conexion(conexion);

    ControlAdmin beanAdmin = new ControlAdmin();
    beanAdmin.setCn(cn);

    ArrayList resulCombo = new ArrayList();

    String var = request.getParameter("tipo");
    String tipo_folio = request.getParameter("tipo_folio");
    String id_evolucion = request.getParameter("id_evolucion");
    String id_sexo = request.getParameter("id_sexo");
    String rango = request.getParameter("rango");
    String rango2 = request.getParameter("rango2");
    String bloquear = "";    
    String edad = request.getParameter("edade");
    String profesional = request.getParameter("profesional");
    String servicio_auditoria = request.getParameter("servicio_auditoria");
    String modo = request.getParameter("modo");
    String idEstadoFolio = request.getParameter("id_estado_folio");

    int totalSecciones = 1;
    String cardClass = "card-total";
    String heightTextArea = "120px";
    String widthTextArea = "100%";

    Map<Integer,List<String>> mapCampos = new HashMap<>();
    Map<Integer,List<String>> mapTitulos = new HashMap<>();    
    Map<Integer,Integer> mapColumnas = new HashMap<>();
    
    List<String> campos = new ArrayList<>();
    List<String> titulos = new ArrayList<>();    
    
    Integer seccionActual;
    String campo = "";
    String titulo = "";    
    String maxWidth = "";
    String widthCard = "";
    String flexCard = "";

    try{
        String queryCol = beanSession.cn.traerElQuery(1130).toString();
        PreparedStatement stmt = (PreparedStatement) new LoggableStatement(conexion, queryCol, 1005, 1007);
        stmt.setString(1, id_evolucion);
        stmt.setString(2, var);
        ResultSet rs = stmt.executeQuery();        
        while(rs.next()){                        
                mapColumnas.put(rs.getInt("seccion"),rs.getInt("numero_columnas"));
                widthCard = rs.getString("ancho_secciones").trim();
        }                
        if(mapColumnas.size()>1){
            totalSecciones = mapColumnas.size();
            cardClass = "card";
        }
        if(widthCard.equals("auto")){
            flexCard = "flex: 1";
        }
    }            
    catch (Exception e){
        System.out.println("Consulta 1:" + e.getMessage());
    }

    seccionActual = mapColumnas.keySet().iterator().next();    
        
    try{
        String SQL = beanSession.cn.traerElQuery(1117).toString();
        PreparedStatement stmt = (PreparedStatement) new LoggableStatement(conexion, SQL, 1005, 1007);
        stmt.setString(1, servicio_auditoria);
        stmt.setString(2, id_evolucion);
        stmt.setString(3, var);
        stmt.setString(4, id_evolucion);
        stmt.setString(5, var);

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {

            String validacion = "";
            String campo_destino = "";
            String texto = "";
            String color = "";
            String fontWeight = "normal";

            if (rs.getString("campo_destino") != null) {
                campo_destino = rs.getString("campo_destino");
            }

            if (rs.getString("etiqueta") != null || rs.getString("etiqueta") != "") {
                fontWeight = rs.getString("etiqueta");
            }

            if (rs.getString("texto") != null) {
                texto = rs.getString("texto");
            }

            if (rs.getString("color") != null) {
                color = rs.getString("color");
            }

            if (rs.getString("validaciones") != null) {
                validacion = "validarCampo(this.id,'" + rs.getString("validaciones") + "','v2');";
            }

            String actualizar = "";

            if (rs.getInt("formulas") > 0) {
                actualizar = "actualizarCampoFormula(this.id, this.value);";
            }

            if (rs.getInt("estado_folio") > 0) {
                bloquear = "disabled";
            } else {
                bloquear = " ";
            }
            
            Integer column = mapColumnas.get(rs.getInt("id_padre_orden"));
            if(totalSecciones == 1){
                if(column == 1){
                    maxWidth = "990px";
                }else if(column == 2){
                    maxWidth = "495px";
                }else if (column == 3){
                    maxWidth = "330px";
                }else{
                    maxWidth = "248px";
                }
            }else if(totalSecciones == 2){
                if(column == 1){
                    maxWidth = "500px";
                }else if(column == 2){
                    maxWidth = "250px";
                }else if (column == 3){
                    maxWidth = "125px";
                }else{
                    maxWidth = "100px";
                }
            }else if(totalSecciones == 3){
                if(column == 1){
                    maxWidth = "330px";
                }else if(column == 2){
                    maxWidth = "170px";
                }else if (column == 3){
                    maxWidth = "125px";
                }else{
                    maxWidth = "100px";
                }
            }else if(totalSecciones == 4){
                if(column == 1){
                    maxWidth = "100px";
                }else if(column == 2){
                    maxWidth = "100px";
                }else if (column == 3){
                    maxWidth = "80px";
                }else{
                    maxWidth = "100px";
                }
            }
            else{
                if(column == 1){
                    maxWidth = "150px";
                }else if(column == 2){
                    maxWidth = "200px";
                }else if (column == 3){
                    maxWidth = "100px";
                }else{
                    maxWidth = "75px";
                }
            }
            
            if(rs.getInt("id_padre_orden")>seccionActual){
                seccionActual = seccionActual+1;                
                while(mapColumnas.get(seccionActual) == null ){
                    seccionActual = seccionActual+1;                    
                }
                campos.clear();
                titulos.clear();
            }            

            if (rs.getString("id_jerarquia").equals("TI")) {                
                String etiqueta = rs.getString("maxlenght");                
                if(rs.wasNull()){
                    etiqueta = "2";
                }
                if(etiqueta.trim().equals("")){
                    etiqueta = "2";
                }
                etiqueta = etiqueta.trim();
                String descripcion = rs.getString("descripcion");
                String descripcionCapitalizada = descripcion.substring(0, 1).toUpperCase() + descripcion.substring(1).toLowerCase();
                titulo = "<h" + etiqueta + ">" + descripcionCapitalizada + "</h" + etiqueta +">";
                titulos.add(titulo);
                mapTitulos.put(seccionActual,new ArrayList<>(titulos));
            }
            else if (rs.getString("id_jerarquia").equals("PA")) {
                String descripcion = rs.getString("descripcion");
                String descripcionCapitalizada = descripcion.substring(0, 1).toUpperCase() + descripcion.substring(1).toLowerCase();
                campo = "<strong style='padding-bottom: 10px;'>"+ descripcionCapitalizada +"</strong>";
                campos.add(campo);
                mapCampos.put(seccionActual,new ArrayList<>(campos));
            }
            else{                
                campo = "<div onmouseover='showFullMessage(this)' onmouseout='hideMessageBox(this)' class='tooltip-container'><label class='label-class tooltip' style='max-width:" + maxWidth + " ; font-weight:" + fontWeight + "' id='label.formulario." + var + "." + rs.getString("referencia") +"' for='formulario." + var + "." + rs.getString("referencia")  +"'>" + rs.getString("descripcion") + "</label><span class='tooltiptext'>" + rs.getString("descripcion") + "</span></div>";

                if (rs.getString("tipo_input").trim().equals("input")) {
                    campo = campo + "<input class='input'  type='text' maxlength=' " + rs.getString("maxlenght") + "' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";                        
                    }
                }else if (rs.getString("tipo_input").trim().equals("inputl")){
                    if(rs.getString("campo_bloquea")==null){
                        campo = campo + "<input class='inputl' type='text' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                    }else if(rs.getString("campo_bloquea").trim().equals("1")){
                        campo = campo + "<input class='inputl'  type='text' title='" + rs.getString("referencia") + "' readonly id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                    }else{
                        campo = campo + "<input class='inputl'  type='text' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                    }
                    campo = campo + "&nbsp; &nbsp; <img width='18px' height='18px' align='middle' title='VEN52' id='idLupitaVentanitaDxIngrT' onclick=\"traerVentanitaFuncionesHcPlantilla(this.id,'actualizarDxIngresoHC','divParaVentanita','formulario." + var + "." + rs.getString("referencia") +"','"+ rs.getString("id_query_ventanita") +"')\" src='/clinica/utilidades/imagenes/acciones/buscar.png'" + bloquear + ">";                                     
                }else if (rs.getString("tipo_input").trim().equals("hora")) {
                    campo = campo + "<input class='input'  type='time' step='600' title='" + rs.getString("referencia") +"' id='formulario." + var + "." + rs.getString("referencia") + "' name='" + var + "' value='" +  rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value); " + validacion + "\"" + bloquear + "/>";                    
                }else if (rs.getString("tipo_input").trim().equals("numeros")) {
                    campo = campo + "<input class='input' type='number' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeypress='javascript:return soloTelefono(event);' oninput=\"maxLongitudNumero(this.id,'" + rs.getString("maxlenght") + "');\" onkeyup=\"ingresarLimites(this.id,'" + rs.getString("maximo_input") + "','" + rs.getString("minimo_input") + "',this.value); alertasGenerales(this.id,'" + rs.getString("valor_alerta") + "', this.value);" + actualizar + "\" onblur=\"ValidacionesAIEPI('" + campo_destino + "','v2'); guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if (rs.getString("tipo_input").trim().equals("decimal")) {
                    campo = campo + "<input class='input' type='number' step='" + rs.getString("cantdecimales") + "' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeypress=\"javascript:return NumCheck(event, this.id, '"+ rs.getString("cantdecimales") +"', '" + rs.getString("maxlenght") + "');\" oninput=\"maxLongitudNumero(this.id,'" + rs.getString("maxlenght") + "');\" onkeyup=\"ingresarLimites(this.id,'" + rs.getString("maximo_input") + "','" + rs.getString("minimo_input") + "',this.value); alertasGenerales(this.id,'" + rs.getString("valor_alerta") + "', this.value);" + actualizar + "\" onblur=\"ValidacionesAIEPI('" + campo_destino + "','v2'); guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }                                     
                }else if (rs.getString("tipo_input").trim().equals("check")) {
                    String estado = " ";
                    if (rs.getString("dato").equals("SI")) {
                        estado = "checked";
                    }
                    campo = campo + "<input class='input checkbox' type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitchsn(this.id, this.checked);ValidacionesAIEPI('" + campo_destino + "','v2'); guardarDatosPlantilla(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + "\"" + bloquear + estado +  "/>";

                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if (rs.getString("tipo_input").trim().equals("switchsn")) {
                    String estado = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("SI") || rs.getString("dato").equals("NO")) {
                        class_span1 = "onoffswitch-inner2";
                        class_span2 = "onoffswitch-switch2";
                        activar = "";

                        if (rs.getString("dato").equals("SI")) {
                            estado = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchsn('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }

                    if (rs.getString("campo_bloquea") == null || rs.getString("campo_bloquea").equals("")) {
                        cam_bloquear = " ";
                    } else {
                        parametros = rs.getString("campo_bloquea");
                        cam_bloquear = "bloquearMisCampos('" + parametros + "','" + var + "', this.checked);";
                    }
                    campo = campo + "<div class='centerSwitch'>";
                    campo = campo + "<div class='onoffswitch2' onclick=\"" + activar + "\">";
                    campo = campo + "<input class='onoffswitch-checkbox2' type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitchsn(this.id, this.checked); guardarDatosPlantilla(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + cam_bloquear + "\"" + bloquear + " " + estado +  "/>";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                    campo = campo + "<label class='onoffswitch-label2' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                    campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                    campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                    campo = campo + "</label></div></div>";
                    bloquear = bloquear_aux;
                }
                else if (rs.getString("tipo_input").trim().equals("switchsnCronicos")) {
                    String estado = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("SI") || rs.getString("dato").equals("NO")) {
                        class_span1 = "onoffswitch-inner2";
                        class_span2 = "onoffswitch-switch2";
                        activar = "";

                        if (rs.getString("dato").equals("SI")) {
                            estado = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchsn('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }

                    if (rs.getString("campo_bloquea") == null || rs.getString("campo_bloquea").equals("")) {
                        cam_bloquear = " ";
                    } else {
                        parametros = rs.getString("campo_bloquea");
                        cam_bloquear = "bloquearMisCampos('" + parametros + "','" + var + "', this.checked);";
                    }
                    campo = campo + "<div class='centerSwitch'>";
                    campo = campo + "<div class='onoffswitch2' onclick=\"" + activar + "\">";
                    campo = campo + "<input class='onoffswitch-checkbox2' type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitchsn(this.id, this.checked); ocultarTabs(); guardarDatosPlantilla(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + cam_bloquear + "\"" + bloquear + " " + estado +  "/>";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                    campo = campo + "<label class='onoffswitch-label2' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                    campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                    campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                    campo = campo + "</label></div></div>";
                    bloquear = bloquear_aux;
                    
                }else if (rs.getString("tipo_input").trim().equals("switchS10")){
                    String estado = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("1") || rs.getString("dato").equals("0")) {
                        class_span1 = "onoffswitch-inner2";
                        class_span2 = "onoffswitch-switch2";
                        activar = "";

                        if (rs.getString("dato").equals("1")) {
                            estado = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchS10('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }
                    campo = campo + "<div class='centerSwitch'>";
                    campo = campo + "<div class='onoffswitch2' onclick=\"" + activar + "\">";
                    campo = campo + "<input class='onoffswitch-checkbox2' type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitch10(this.id, this.checked); guardarDatosPlantilla(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + "\"" + bloquear + " " + estado +  "/>";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                    campo = campo + "<label class='onoffswitch-label2' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                    campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                    campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                    campo = campo + "</label></div></div>";
                    bloquear = bloquear_aux;
                } else if (rs.getString("tipo_input").trim().equals("switch10")) {
                    String estado1 = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("1") || rs.getString("dato").equals("0")) {
                        class_span1 = "onoffswitch-inner3";
                        class_span2 = "onoffswitch-switch3";
                        activar = "";

                        if (rs.getString("dato").equals("1")) {
                            estado1 = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitch10('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }
                    campo = campo + "<div class='centerSwitch'>";
                    campo = campo + "<div class='onoffswitch3' onclick=\"" + activar + "\">";
                    campo = campo + "<input class='onoffswitch-checkbox3' type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitch10(this.id, this.checked); guardarDatosPlantilla(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + "\"" + bloquear + " "  + estado1 +  "/>";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                    campo = campo + "<label class='onoffswitch-label3' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                    campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                    campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                    campo = campo + "</label></div></div>";
                    bloquear = bloquear_aux;                    
                } else if (rs.getString("tipo_input").trim().equals("switchobs")) {
                    String estado2 = "";
                    String ocultar = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";                    

                    if (rs.getString("dato").trim().equals("Normal") || !rs.getString("dato").trim().equals("")) {
                        class_span1 = "onoffswitch-inner4";
                        class_span2 = "onoffswitch-switch4";

                        if (rs.getString("dato").trim().equals("Normal")) {
                            estado2 = " ";
                            ocultar = "hidden";
                        } else {
                            estado2 = "checked";
                            ocultar = " ";
                        }
                    } else {
                        estado2 = " ";
                        ocultar = "hidden";

                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchobs('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }                    
                    campo = campo + "<div class='onoffswitch4' style='float: left;' onclick=\"" + activar + "\">";
                    campo = campo + "<input class='onoffswitch-checkbox4' type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitchObservacion(this.id, this.checked); guardarDatosPlantilla(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + "\"" + bloquear + " " + estado2 +  "/>";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                    campo = campo + "<label class='onoffswitch-label4' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                    campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                    campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                    campo = campo + "</label></div>";
                    bloquear = bloquear_aux;
                    campo = campo + "<label  id='switchobservacionformulario." + var + "." + rs.getString("referencia") + "'" + ocultar +">";
                    campo = campo + "<div class='areaObservacion'>";                    
                    campo = campo + "<textarea class='textArea' maxlength='" + rs.getString("maxlenght") +"' title='" + rs.getString("referencia") +"' name='" + var + "' id='formulario." + var + "." + rs.getString("referencia") + "' onkeypress='return validarKey(event, this.id);' onblur=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + ">" + rs.getString("dato").trim() +"</textarea>";
                    if (rs.getInt("id_formula") > 0) {                        
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }                    
                    campo = campo + "</div></label>";
                }else if (rs.getString("tipo_input").trim().equals("textadc")) {
                    if(!rs.getString("ancho").trim().equals("%") || !rs.getString("ancho").trim().equals("") || rs.getString("ancho").trim() != null){
                        widthTextArea = rs.getString("ancho").trim();
                    }
                    if(!rs.getString("altura").trim().equals("px") || !rs.getString("altura").trim().equals("") || rs.getString("altura").trim() != null){
                        heightTextArea = rs.getString("altura").trim();
                    }
                    campo = campo + "<textarea class='textArea' style='width:" + widthTextArea + " ; height: " + heightTextArea + ";' title='" + rs.getString("referencia") + "' id='txtTexArea." + rs.getString("referencia") + "' name='" + rs.getString("referencia") + "' onkeypress='return validarKey(event, this.id)' placeholder='Ingresar texto...' onblur=\"guardarDatosPlantilla('" + var + "', this.name, this.value);\"" + bloquear  + ">" + rs.getString("dato") +"</textarea>";
                }
                else if(rs.getString("tipo_input").trim().equals("texta")){
                    if(!rs.getString("ancho").trim().equals("%") || !rs.getString("ancho").trim().equals("") || rs.getString("ancho").trim() != null){
                        widthTextArea = rs.getString("ancho").trim();
                    }
                    if(!rs.getString("altura").trim().equals("px") || !rs.getString("altura").trim().equals("") || rs.getString("altura").trim() != null){
                        heightTextArea = rs.getString("altura").trim();
                    }
                    campo = campo + "<textarea class='textArea' style='width: " + widthTextArea + "; height: " + heightTextArea + ";' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia") + "' name='" + rs.getString("referencia") + "' placeholder='Ingresar texto...'onkeypress='return validarKey(event, this.id);' onblur=\"guardarDatosPlantilla('" + var + "', this.name, this.value);\"" + bloquear  + ">" + rs.getString("dato") +"</textarea>";
                }else if (rs.getString("tipo_input").trim().equals("combo")){
                    campo = campo + "<select class='selectInput' data-choices size='1' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia") + "' name='" + rs.getString("referencia") + "' onchange=\"ValidacionesAIEPI('" + campo_destino + "','v2'); guardarDatosPlantilla('" + var + "',this.name,this.value);" + actualizar + validacion + "\"" + bloquear + ">";
                    campo = campo + "<option value=''></option>";
                    resulCombo.clear();
                    resulCombo = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(rs.getString("id_query_combo")));
                    ComboVO cmb;
                    for (int i = 0; i < resulCombo.size(); i++) {
                        cmb = (ComboVO) resulCombo.get(i);
                        if (rs.getString("dato").trim().equals(cmb.getId())) {
                            campo = campo + "<option value='" + cmb.getId() + "' selected>" + cmb.getDescripcion() +"</option>";
                        }else{
                            campo = campo + "<option value='" + cmb.getId() + "'>" + cmb.getDescripcion() +"</option>";
                        }
                    }
                    campo = campo + "</select>";
                    if (rs.getInt("id_formula") > 0) {
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if (rs.getString("tipo_input").trim().equals("comboMed")) {
                    campo = campo + "<input type='hidden' class='input' value='" + rs.getString("dato").trim() + "' id='idMedEvaluacion'>";
                    campo = campo + "<select size='1' class='selectInput' title='" + rs.getString("referencia") + "' id='miComboMedEvaluacion' name='" + var + "' onclick='comboMed()' onchange=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value)\"" + bloquear + "></select>";
                }else if (rs.getString("tipo_input").trim().equals("comboInter")) {
                    campo = campo + "<input type='hidden' class='input' value='" + rs.getString("dato").trim() + "' id='idInterconsulta'>";
                    campo = campo + "<select size='1' class='selectInput' title='" + rs.getString("referencia") + "' id='miComboInterconsulta' name='" + var + "' onclick='comboInter()' onchange=\"cargarInter();guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value)\"" + bloquear + "></select>";
                }else if (rs.getString("tipo_input").trim().equals("inputFecha")) {
                    campo = campo + "<input class='input'  type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur='validarFechaPatron(this.value, this.id)' onchange=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if (rs.getString("tipo_input").trim().indexOf(",") > 0){                    
                    String tipo_input = rs.getString("tipo_input");
                    String[] tipos_input = tipo_input.split(",");                    
                    if (tipos_input[0].equals("2input")) {
                        String referencia = rs.getString("referencia");
                        String[] referencias = referencia.split(",");                        
                        String dato = rs.getString("dato");                        
                        String[] datos = dato.split(",");                                                
                        campo = campo + "<input class='input'  type='" + tipos_input[1].trim() + "' title='" + referencias[0] +"' id='formulario." + var + "." + referencias[0] +"' name='" + var +"' value='" + datos[0] +"' onchange=\"guardarDatosPlantilla(this.name, '" + referencias[0] +"',this.value);" + actualizar + validacion + "\"" + bloquear + "/>";
                        campo = campo + "<input class='input'  type='" + tipos_input[2].trim() + "' title='" + referencias[1] +"' id='formulario." + var + "." + referencias[1] +"' name='" + var +"' value='" + datos[1] +"' onchange=\"guardarDatosPlantilla(this.name, '" + referencias[1] +"',this.value);" + actualizar + validacion + "\"" + bloquear + "/>";
                    }
                }else if (rs.getString("tipo_input").trim().equals("inputFechaPosterior")) {
                    campo = campo + "<input class='input'   type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 2);' onblur='validarFechaEscrita(this.value, this.id,2)' onchange=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if (rs.getString("tipo_input").trim().equals("inputFechaAnterior")) {
                    campo = campo + "<input class='input'  type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 1);' onblur='validarFechaEscrita(this.value, this.id,1)' onchange=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if (rs.getString("tipo_input").trim().equals("inputFechaNacimientoHoy")) {
                    campo = campo + "<input class='input' type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 4);' onblur='validarFechaEscrita(this.value, this.id,4)' onchange=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if (rs.getString("tipo_input").trim().equals("inputFechaNacimiento")) {
                    campo = campo + "<input class='input'  type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 3);' onblur='validarFechaEscrita(this.value, this.id,3)' onchange=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                    if (rs.getInt("id_formula") > 0) {
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }else if(rs.getString("tipo_input").trim().equals("buttonC")){
                    campo = campo + "<input type='button' value='CARGAR' onclick='cargarExamenFisico()' class='small button blue'" + bloquear + "/>" ;
                }else if(rs.getString("tipo_input").trim().equals("buttonI")){
                    campo = campo + "<input type='button' value='Imprimir' onclick='imprimirFurips()' class='small button blue'" +  "/>" ;
                }else if(rs.getString("tipo_input").trim().equals("buttonIP")){
                    campo = campo + "<input type='button' value='Imprimir' onclick='imprimirIP()' class='small button blue'" +  "/>" ;
                }else if (rs.getString("tipo_input").trim().equals("input2")) {
                    campo = campo + "<input class='input'  type='text' maxlength=' " + rs.getString("maxlenght") + "' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onblur=\"guardarDatosPlantilla(this.name,'" + rs.getString("referencia") + "',this.value);\"" + bloquear + " />";                    
                }else if(rs.getString("tipo_input").trim().equals("button")) {
                    campo = campo + "<input type='button' value='GRAFICA' onclick=\"graficaPlantilla("+ id_evolucion +",'" + rs.getString("referencia") +"')\" class='small button blue'" + bloquear + "/>" ;
                }else if (rs.getString("tipo_input").trim().equals("buttonVih")) {
                    campo = campo + "<input type='button' value='IMPRIMIR' onclick=\"imprimirPdd("+ id_evolucion +",'" + var +"')\" class='small button blue'" + bloquear + "/>" ;
                }else if (rs.getString("tipo_input").trim().equals("select2")){
                    campo = campo + "<select onclick='initSelect2(this.id)' class='selecTo' size='1' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia") + "' name='" + rs.getString("referencia") + "' onchange=\"ValidacionesAIEPI('" + campo_destino + "','v2'); guardarDatosPlantilla('" + var + "',this.name,this.value);" + actualizar + validacion + "\"" + bloquear + ">";
                    campo = campo + "<option value=''></option>";                    
                    resulCombo.clear();
                    resulCombo = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(rs.getString("id_query_combo")));
                    ComboVO cmb;
                    for (int i = 0; i < resulCombo.size(); i++) {
                        cmb = (ComboVO) resulCombo.get(i);
                        if (rs.getString("dato").trim().equals(cmb.getId())) {
                            campo = campo + "<option value='" + cmb.getId() + "' selected>" + cmb.getDescripcion() +"</option>";
                        }else{
                            campo = campo + "<option value='" + cmb.getId() + "'>" + cmb.getDescripcion() +"</option>";
                        }
                    }
                    campo = campo + "</select>";
                    if (rs.getInt("id_formula") > 0) {
                        campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                        campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                    }
                }
                campos.add(campo);                
                mapCampos.put(seccionActual,new ArrayList<>(campos));                
            }            
        }        
%>
<div id="version">
    <input value="v1" type="button" class="small button blue" 
        onclick="changeVersionPage('v1','<%=var%>','<%=tipo_folio%>','<%=id_evolucion%>','<%=id_sexo%>',
        '<%=rango%>','<%=rango2%>','<%=edad%>','<%=idEstadoFolio%>','<%=modo%>')" style="width: 5%;">
</div>
<div id="principal">    
    <%    
    for(Map.Entry<Integer, Integer> entry : mapColumnas.entrySet()){%>
        <div class='<%=cardClass%>' style='width: <%=widthCard%>; <%=flexCard%>'>
    <%
        List<String> renderTitulos = mapTitulos.get(entry.getKey());
        List<String> renderCampos = mapCampos.get(entry.getKey());        
        if(renderTitulos != null){%>
            <div class="titles">
                <%
                for(int t=0;t<renderTitulos.size();t++){%>                    
                    <%=renderTitulos.get(t)%>
                <%}%>
            </div>
        <%}%>
        <br>        
        <%
        if(renderCampos != null){
            Integer columnas = entry.getValue();
            String col = "grid-template-columns: ";
            for(int c=0;c<columnas;c++){
                col = col + "1fr ";
            }
            col = col + ";";
            %>
            <div class='card-content' style='<%=col%>'>
                <%
                    for(int c=0;c<renderCampos.size();c++){%>
                        <div class="form-group">
                            <%=renderCampos.get(c)%>
                        </div>
                    <%}
                    %>
            </div>
        <%}
        %>
        </div>        
    <%}%>       
</div>
</body>

<%    }
    catch (Exception e){
        System.out.println("Renderizado: " + e);
        System.out.println("Renderizado: " + e.getMessage());
    }

       
%>