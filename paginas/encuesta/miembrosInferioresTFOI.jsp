<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<%  

Connection conexion = iConnection.getConnection();
Conexion cn = new Conexion(conexion);

ControlAdmin beanAdmin = new ControlAdmin();
beanAdmin.setCn(cn);  

ArrayList resulCombo = new ArrayList();
ComboVO cmb;
String id_evolucion = request.getParameter("id_evolucion");
String cbHmFlexorDer = "";
String cbHmFlexorIz = "";
String cbHmExtensorDer = "";
String cbHmExtensorIz = "";
String cbHmAductoresDer = "";
String cbHmAductoresIz = "";
String cbHmAbeductorDer = "";
String cbHmAbeductorIz = "";
String cbHmRotadorInternoDer = "";
String cbHmRotadorInternoIz = "";
String cbHmRotadorExternoDer = "";
String cbHmRotadorExternoIz = "";
String cbCdFlexorDer = "";
String cbCdFlexorIz = "";
String cbCdExtensorDer = "";
String cbCdExtensorIz = "";
String cbMnPronadoresDer = "";
String cbMnPronadoresIz = "";
String cbMnSupinadorDer = "";
String cbMnSupinadorIz = "";
String cbMnFlexorDer = "";
String cbMnFlexorIz = "";
String cbMnExtensorDer = "";
String cbMnExtensorIz = "";
String cbDedosManoFlexoresDer = "";
String cbDedosManoFlexoresIz = "";
String cbDedosManoExtensoresDer = "";
String cbDedosManoExtensoresIz = "";
String cbDedosManoAductoresDer = "";
String cbDedosManoAductoresIz = "";
String cbDedosManoAbeductoresDer = "";
String cbDedosManoAbeductoresIz = "";
String cbPlgAbductorDer = "";
String cbPlgAbductorIz = "";
String cbPlgAductorDer = "";
String cbPlgAductorIz = "";
String cbPlgOponenteDer = "";
String cbPlgOponenteIz = "";
String cbPlgExtensorLargoDer = "";
String cbPlgExtensorLargoIz = "";
String cbPlgExtensorCortoDer = "";
String cbPlgExtensorCortoIz = "";
String cbPlgFlexorLargoDer = "";
String cbPlgFlexorLargoIz = "";
String cbPlgFlexorCortoDer = "";
String cbPlgFlexorCortoIz = "";
String cbCadFlexoresDer = "";
String cbCadFlexoresIz = "";
String cbCadExtensoresDer = "";
String cbCadExtensoresIz = "";
String cbCadAductoresDer = "";
String cbCadAductoresIZ = "";
String cbCadAbeductoresDer = "";
String cbCadAbeductoresIz = "";
String cbCadRotadoresInternosDer = "";
String cbCadRotadoresInternosIz = "";
String cbCadRotadoresExternosDer = "";
String cbCadRotadoresExternosDerIz = "";
String cbRodillaFlexorDerecho = "";
String cbRodillaFlexorIz = "";
String cbRodillaExtensorDer = "";
String cbRodillaExtensorIz = "";
String cbTobilloPlantiflexorDer = "";
String cbTobilloPlantiflexorIz = "";
String cbTobilloDorsiFleDer = "";
String cbTobilloDorsiFleIz = "";
String cbPieInvertoresDer = "";
String cbPieInvertoresIz = "";
String cbPieEvertoresDer = "";
String cbPieEvertoresIz = "";
String cbDedosPieFlexoresDer = "";
String cbDedosPieFlexoresIz = "";
String cbDedosPieExtensoresDer = "";
String cbDedosPieExtensoresIz = "";
String cbDedosPieAductoresDer = "";
String cbDedosPieAductoresIz = "";
String cbDedosPieAbeductoresDer = "";
String cbDedosPieAbeductoresIz = "";

String flexionCaderaIz = "";
String flexionCaderaDer = "";
String extensionCaderaIz = "";
String extensionCaderaDer = "";
String abduccionCaderaIz = "";
String abduccionCaderaDer = "";
String aduccionCaderaIz = "";
String aduccionCaderaDer = "";
String rotacionInternaCaderaIz = "";
String rotacionInternaCaderaDer = "";
String rotacionExternaCaderaIz = "";
String rotacionExternaCaderaDer = "";
String flexionRodillaIz = "";
String flexionRodillaDer = "";
String extensionRodillaIz = "";
String extensionRodillaDer = "";
String plantiflexionCuellopieIz = "";
String plantiflexionCuellopieDer = "";
String dorsiflexionCuelloPieIz = "";
String dorsiflexionCuelloPieDer = "";
String inversionCuellopieIz = "";
String inversionCuellopieDer = "";
String eversionCuellopieIz = "";
String eversionCuellopieDer = "";
String dedosPieFlexionMTFIz = "";
String dedosPieFlexionMTFDer = "";
String dedosPieFlexionITFIz = "";
String dedosPieFlexionITFDer = "";
String dedosPieExtensionMTFIz = "";
String dedosPieExtensionMTFDer = "";
String dedosPieExtensionITFIz = "";
String dedosPieExtensionITFDer = "";

try{

Statement stmt = conexion.createStatement();
String SQL = " ";


    SQL = "select " +

    "coalesce(vrl5.c1, '')  as cbHmFlexorDer, "+  
    "coalesce(vrl5.c2, '')  as cbHmFlexorIz, "+ 
    "coalesce(vrl5.c3, '')  as cbHmExtensorDer, "+  
    "coalesce(vrl5.c4, '')  as cbHmExtensorIz, "+ 
    "coalesce(vrl5.c5, '')  as cbHmAductoresDer, "+ 
    "coalesce(vrl5.c6, '')  as cbHmAductoresIz, "+
    "coalesce(vrl5.c7, '')  as cbHmAbeductorDer, "+ 
    "coalesce(vrl5.c8, '')  as cbHmAbeductorIz, "+
    "coalesce(vrl5.c9, '')  as cbHmRotadorInternoDer, "+ 
    "coalesce(vrl5.c10, '') as cbHmRotadorInternoIz, "+ 
    "coalesce(vrl5.c11, '') as cbHmRotadorExternoDer, "+
    "coalesce(vrl5.c12, '') as cbHmRotadorExternoIz, "+ 
    "coalesce(vrl5.c13, '') as cbCdFlexorDer, "+
    "coalesce(vrl5.c14, '') as cbCdFlexorIz, "+
    "coalesce(vrl5.c15, '') as cbCdExtensorDer, "+
    "coalesce(vrl5.c16, '') as cbCdExtensorIz, "+
    "coalesce(vrl5.c17, '') as cbMnPronadoresDer, "+
    "coalesce(vrl5.c18, '') as cbMnPronadoresIz, "+
    "coalesce(vrl5.c19, '') as cbMnSupinadorDer, "+
    "coalesce(vrl5.c20, '') as cbMnSupinadorIz, "+
    "coalesce(vrl5.c21, '') as cbMnFlexorDer, "+
    "coalesce(vrl5.c22, '') as cbMnFlexorIz, "+
    "coalesce(vrl5.c23, '') as cbMnExtensorDer, "+
    "coalesce(vrl5.c24, '') as cbMnExtensorIz, "+
    "coalesce(vrl5.c25, '') as cbDedosManoFlexoresDer, "+
    "coalesce(vrl5.c26, '') as cbDedosManoFlexoresIz, "+
    "coalesce(vrl5.c27, '') as cbDedosManoExtensoresDer, "+
    "coalesce(vrl5.c28, '') as cbDedosManoExtensoresIz, "+
    "coalesce(vrl5.c29, '') as cbDedosManoAductoresDer, "+
    "coalesce(vrl5.c30, '') as cbDedosManoAductoresIz, "+
    "coalesce(vrl5.c31, '') as cbDedosManoAbeductoresDer, "+
    "coalesce(vrl5.c32, '') as cbDedosManoAbeductoresIz, "+
    "coalesce(vrl5.c33, '') as cbPlgAbductorDer, "+
    "coalesce(vrl5.c34, '') as cbPlgAbductorIz, "+
    "coalesce(vrl5.c35, '') as cbPlgAductorDer, "+
    "coalesce(vrl5.c36, '') as cbPlgAductorIz, "+
    "coalesce(vrl5.c37, '') as cbPlgOponenteDer, "+
    "coalesce(vrl5.c38, '') as cbPlgOponenteIz, "+
    "coalesce(vrl5.c39, '') as cbPlgExtensorLargoDer, "+
    "coalesce(vrl5.c40, '') as cbPlgExtensorLargoIz, "+
    "coalesce(vrl5.c41, '') as cbPlgExtensorCortoDer, "+
    "coalesce(vrl5.c42, '') as cbPlgExtensorCortoIz, "+
    "coalesce(vrl5.c43, '') as cbPlgFlexorLargoDer, "+
    "coalesce(vrl5.c44, '') as cbPlgFlexorLargoIz, "+
    "coalesce(vrl5.c45, '') as cbPlgFlexorCortoDer, "+
    "coalesce(vrl5.c46, '') as cbPlgFlexorCortoIz, "+
    "coalesce(vrl5.c47, '') as cbCadFlexoresDer, "+
    "coalesce(vrl5.c48, '') as cbCadFlexoresIz, "+
    "coalesce(vrl5.c49, '') as cbCadExtensoresDer, "+
    "coalesce(vrl5.c50, '') as cbCadExtensoresIz, "+
    "coalesce(vrl5.c51, '') as cbCadAductoresDer, "+
    "coalesce(vrl5.c52, '') as cbCadAductoresIZ, "+
    "coalesce(vrl5.c53, '') as cbCadAbeductoresDer, "+
    "coalesce(vrl5.c54, '') as cbCadAbeductoresIz, "+
    "coalesce(vrl5.c55, '') as cbCadRotadoresInternosDer, "+
    "coalesce(vrl5.c56, '') as cbCadRotadoresInternosIz, "+
    "coalesce(vrl5.c57, '') as cbCadRotadoresExternosDer, "+
    "coalesce(vrl5.c58, '') as cbCadRotadoresExternosDerIz, "+
    "coalesce(vrl5.c59, '') as cbRodillaFlexorDerecho, "+
    "coalesce(vrl5.c60, '') as cbRodillaFlexorIz, "+
    "coalesce(vrl5.c61, '') as cbRodillaExtensorDer, "+
    "coalesce(vrl5.c62, '') as cbRodillaExtensorIz, "+
    "coalesce(vrl5.c63, '') as cbTobilloPlantiflexorDer, "+
    "coalesce(vrl5.c64, '') as cbTobilloPlantiflexorIz, "+
    "coalesce(vrl5.c65, '') as cbTobilloDorsiFleDer, "+
    "coalesce(vrl5.c66, '') as cbTobilloDorsiFleIz, "+
    "coalesce(vrl5.c67, '') as cbPieInvertoresDer, "+
    "coalesce(vrl5.c68, '') as cbPieInvertoresIz, "+
    "coalesce(vrl5.c69, '') as cbPieEvertoresDer, "+
    "coalesce(vrl5.c70, '') as cbPieEvertoresIz, "+
    "coalesce(vrl5.c71, '') as cbDedosPieFlexoresDer, "+
    "coalesce(vrl5.c72, '') as cbDedosPieFlexoresIz, "+
    "coalesce(vrl5.c73, '') as cbDedosPieExtensoresDer, "+
    "coalesce(vrl5.c74, '') as cbDedosPieExtensoresIz, "+
    "coalesce(vrl5.c75, '') as cbDedosPieAductoresDer, "+
    "coalesce(vrl5.c76, '') as cbDedosPieAductoresIz, "+
    "coalesce(vrl5.c77, '') as cbDedosPieAbeductoresDer, "+
    "coalesce(vrl5.c78, '') as cbDedosPieAbeductoresIz, "+
    "coalesce(vrl3.c1, '')  as flexionCaderaIz, "+ 
    "coalesce(vrl3.c2, '')  as flexionCaderaDer, "+
    "coalesce(vrl3.c3, '')  as extensionCaderaIz, "+
    "coalesce(vrl3.c4, '')  as extensionCaderaDer, "+
    "coalesce(vrl3.c5, '')  as abduccionCaderaIz, "+
    "coalesce(vrl3.c6, '')  as abduccionCaderaDer, "+
    "coalesce(vrl3.c7, '')  as aduccionCaderaIz, "+
    "coalesce(vrl3.c8, '')  as aduccionCaderaDer, "+
    "coalesce(vrl3.c9, '')  as rotacionInternaCaderaIz, "+
    "coalesce(vrl3.c10, '') as rotacionInternaCaderaDer, "+
    "coalesce(vrl3.c11, '') as rotacionExternaCaderaIz, "+
    "coalesce(vrl3.c12, '') as rotacionExternaCaderaDer, "+
    "coalesce(vrl3.c13, '') as flexionRodillaIz, "+
    "coalesce(vrl3.c14, '') as flexionRodillaDer, "+
    "coalesce(vrl3.c15, '') as extensionRodillaIz, "+
    "coalesce(vrl3.c16, '') as extensionRodillaDer, "+
    "coalesce(vrl3.c17, '') as plantiflexionCuellopieIz, "+ 
    "coalesce(vrl3.c18, '') as plantiflexionCuellopieDer, "+
    "coalesce(vrl3.c19, '') as dorsiflexionCuelloPieIz, "+ 
    "coalesce(vrl3.c20, '') as dorsiflexionCuelloPieDer, "+
    "coalesce(vrl3.c21, '') as inversionCuellopieIz, "+
    "coalesce(vrl3.c22, '') as inversionCuellopieDer, "+
    "coalesce(vrl3.c23, '') as eversionCuellopieIz, "+
    "coalesce(vrl3.c24, '') as eversionCuellopieDer, "+
    "coalesce(vrl3.c25, '') as dedosPieFlexionMTFIz, "+
    "coalesce(vrl3.c26, '') as dedosPieFlexionMTFDer, "+
    "coalesce(vrl3.c27, '') as dedosPieFlexionITFIz , "+
    "coalesce(vrl3.c28, '') as dedosPieFlexionITFDer , "+
    "coalesce(vrl3.c29, '') as dedosPieExtensionMTFIz , "+
    "coalesce(vrl3.c30, '') as dedosPieExtensionMTFDer , "+
    "coalesce(vrl3.c31, '') as dedosPieExtensionITFIz , "+
    "coalesce(vrl3.c32, '') as dedosPieExtensionITFDer "+
    "from hc.evolucion as hc "+ 
    "left join formulario.vrl5 as vrl5 "+    
    "on hc.id = vrl5.id_evolucion "+
    "left join formulario.vrl3 as vrl3 "+
    "on hc.id = vrl3.id_evolucion "+
    "where hc.id = "+ id_evolucion;

    System.out.println(SQL);

  ResultSet rs = stmt.executeQuery(SQL);
  while(rs.next()){
    cbHmFlexorDer = rs.getString("cbHmFlexorDer");
    cbHmFlexorIz = rs.getString("cbHmFlexorIz");
    cbHmExtensorDer = rs.getString("cbHmExtensorDer");
    cbHmExtensorIz = rs.getString("cbHmExtensorIz");
    cbHmAductoresDer = rs.getString("cbHmAductoresDer");
    cbHmAductoresIz = rs.getString("cbHmAductoresIz");
    cbHmAbeductorDer = rs.getString("cbHmAbeductorDer");
    cbHmAbeductorIz = rs.getString("cbHmAbeductorIz");
    cbHmRotadorInternoDer = rs.getString("cbHmRotadorInternoDer");
    cbHmRotadorInternoIz = rs.getString("cbHmRotadorInternoIz");
    cbHmRotadorExternoDer = rs.getString("cbHmRotadorExternoDer");
    cbHmRotadorExternoIz = rs.getString("cbHmRotadorExternoIz");
    cbCdFlexorDer = rs.getString("cbCdFlexorDer");
    cbCdFlexorIz = rs.getString("cbCdFlexorIz");
    cbCdExtensorDer = rs.getString("cbCdExtensorDer");
    cbCdExtensorIz = rs.getString("cbCdExtensorIz");
    cbMnPronadoresDer = rs.getString("cbMnPronadoresDer");
    cbMnPronadoresIz = rs.getString("cbMnPronadoresIz");
    cbMnSupinadorDer = rs.getString("cbMnSupinadorDer");
    cbMnSupinadorIz = rs.getString("cbMnSupinadorIz");
    cbMnFlexorDer = rs.getString("cbMnFlexorDer");
    cbMnFlexorIz = rs.getString("cbMnFlexorIz");
    cbMnExtensorDer = rs.getString("cbMnExtensorDer");
    cbMnExtensorIz = rs.getString("cbMnExtensorIz");
    cbDedosManoFlexoresDer = rs.getString("cbDedosManoFlexoresDer");
    cbDedosManoFlexoresIz = rs.getString("cbDedosManoFlexoresIz");
    cbDedosManoExtensoresDer = rs.getString("cbDedosManoExtensoresDer");
    cbDedosManoExtensoresIz = rs.getString("cbDedosManoExtensoresIz");
    cbDedosManoAductoresDer = rs.getString("cbDedosManoAductoresDer");
    cbDedosManoAductoresIz = rs.getString("cbDedosManoAductoresIz");
    cbDedosManoAbeductoresDer = rs.getString("cbDedosManoAbeductoresDer");
    cbDedosManoAbeductoresIz = rs.getString("cbDedosManoAbeductoresIz");
    cbPlgAbductorDer = rs.getString("cbPlgAbductorDer");
    cbPlgAbductorIz = rs.getString("cbPlgAbductorIz");
    cbPlgAductorDer = rs.getString("cbPlgAductorDer");
    cbPlgAductorIz = rs.getString("cbPlgAductorIz");
    cbPlgOponenteDer = rs.getString("cbPlgOponenteDer");
    cbPlgOponenteIz = rs.getString("cbPlgOponenteIz");
    cbPlgExtensorLargoDer = rs.getString("cbPlgExtensorLargoDer");
    cbPlgExtensorLargoIz = rs.getString("cbPlgExtensorLargoIz");
    cbPlgExtensorCortoDer = rs.getString("cbPlgExtensorCortoDer");
    cbPlgExtensorCortoIz = rs.getString("cbPlgExtensorCortoIz");
    cbPlgFlexorLargoDer = rs.getString("cbPlgFlexorLargoDer");
    cbPlgFlexorLargoIz = rs.getString("cbPlgFlexorLargoIz");
    cbPlgFlexorCortoDer = rs.getString("cbPlgFlexorCortoDer");
    cbPlgFlexorCortoIz = rs.getString("cbPlgFlexorCortoIz");
    cbCadFlexoresDer = rs.getString("cbCadFlexoresDer");
    cbCadFlexoresIz = rs.getString("cbCadFlexoresIz");
    cbCadExtensoresDer = rs.getString("cbCadExtensoresDer");
    cbCadExtensoresIz = rs.getString("cbCadExtensoresIz");
    cbCadAductoresDer = rs.getString("cbCadAductoresDer");
    cbCadAductoresIZ = rs.getString("cbCadAductoresIZ");
    cbCadAbeductoresDer = rs.getString("cbCadAbeductoresDer");
    cbCadAbeductoresIz = rs.getString("cbCadAbeductoresIz");
    cbCadRotadoresInternosDer = rs.getString("cbCadRotadoresInternosDer");
    cbCadRotadoresInternosIz = rs.getString("cbCadRotadoresInternosIz");
    cbCadRotadoresExternosDer = rs.getString("cbCadRotadoresExternosDer");
    cbCadRotadoresExternosDerIz = rs.getString("cbCadRotadoresExternosDerIz");
    cbRodillaFlexorDerecho = rs.getString("cbRodillaFlexorDerecho");
    cbRodillaFlexorIz = rs.getString("cbRodillaFlexorIz");
    cbRodillaExtensorDer = rs.getString("cbRodillaExtensorDer");
    cbRodillaExtensorIz = rs.getString("cbRodillaExtensorIz");
    cbTobilloPlantiflexorDer = rs.getString("cbTobilloPlantiflexorDer");
    cbTobilloPlantiflexorIz = rs.getString("cbTobilloPlantiflexorIz");
    cbTobilloDorsiFleDer = rs.getString("cbTobilloDorsiFleDer");
    cbTobilloDorsiFleIz = rs.getString("cbTobilloDorsiFleIz");
    cbPieInvertoresDer = rs.getString("cbPieInvertoresDer");
    cbPieInvertoresIz = rs.getString("cbPieInvertoresIz");
    cbPieEvertoresDer = rs.getString("cbPieEvertoresDer");
    cbPieEvertoresIz = rs.getString("cbPieEvertoresIz");
    cbDedosPieFlexoresDer = rs.getString("cbDedosPieFlexoresDer");
    cbDedosPieFlexoresIz = rs.getString("cbDedosPieFlexoresIz");
    cbDedosPieExtensoresDer = rs.getString("cbDedosPieExtensoresDer");
    cbDedosPieExtensoresIz = rs.getString("cbDedosPieExtensoresIz");
    cbDedosPieAductoresDer = rs.getString("cbDedosPieAductoresDer");
    cbDedosPieAductoresIz = rs.getString("cbDedosPieAductoresIz");
    cbDedosPieAbeductoresDer = rs.getString("cbDedosPieAbeductoresDer");
    cbDedosPieAbeductoresIz = rs.getString("cbDedosPieAbeductoresIz");

    flexionCaderaIz = rs.getString("flexionCaderaIz");
    flexionCaderaDer = rs.getString("flexionCaderaDer");
    extensionCaderaIz = rs.getString("extensionCaderaIz");
    extensionCaderaDer = rs.getString("extensionCaderaDer");
    abduccionCaderaIz = rs.getString("abduccionCaderaIz");
    abduccionCaderaDer = rs.getString("abduccionCaderaDer");
    aduccionCaderaIz = rs.getString("aduccionCaderaIz");
    aduccionCaderaDer = rs.getString("aduccionCaderaDer");
    rotacionInternaCaderaIz = rs.getString("rotacionInternaCaderaIz");
    rotacionInternaCaderaDer = rs.getString("rotacionInternaCaderaDer");
    rotacionExternaCaderaIz = rs.getString("rotacionExternaCaderaIz");
    rotacionExternaCaderaDer = rs.getString("rotacionExternaCaderaDer");
    flexionRodillaIz = rs.getString("flexionRodillaIz");
    flexionRodillaDer = rs.getString("flexionRodillaDer");
    extensionRodillaIz = rs.getString("extensionRodillaIz");
    extensionRodillaDer = rs.getString("extensionRodillaDer");
    plantiflexionCuellopieIz = rs.getString("plantiflexionCuellopieIz");
    plantiflexionCuellopieDer = rs.getString("plantiflexionCuellopieDer");
    dorsiflexionCuelloPieIz = rs.getString("dorsiflexionCuelloPieIz");
    dorsiflexionCuelloPieDer = rs.getString("dorsiflexionCuelloPieDer");
    inversionCuellopieIz = rs.getString("inversionCuellopieIz");
    inversionCuellopieDer = rs.getString("inversionCuellopieDer");
    eversionCuellopieIz = rs.getString("eversionCuellopieIz");
    eversionCuellopieDer = rs.getString("eversionCuellopieDer");
    dedosPieFlexionMTFIz = rs.getString("dedosPieFlexionMTFIz");
    dedosPieFlexionMTFDer = rs.getString("dedosPieFlexionMTFDer");
    dedosPieFlexionITFIz = rs.getString("dedosPieFlexionITFIz");
    dedosPieFlexionITFDer = rs.getString("dedosPieFlexionITFDer");
    dedosPieExtensionMTFIz = rs.getString("dedosPieExtensionMTFIz");
    dedosPieExtensionMTFDer = rs.getString("dedosPieExtensionMTFDer");
    dedosPieExtensionITFIz = rs.getString("dedosPieExtensionITFIz");
    dedosPieExtensionITFDer = rs.getString("dedosPieExtensionITFDer");
  }
  
}
catch ( Exception e ){
  System.out.println(e.getMessage());
} %>   

<!DOCTYPE html>
<html lang="en">
    <head>
        <script type="script" src="main.js"></script>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport">
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->
    <title>Document</title>
</head>
<body>
    
  
    <div style="background-color: white; height: 300px; width: 100%; overflow: scroll;">
        <div>
            <h1 class="card-title">Valoracion De La Funcion Articular Y Muscular Miembros Inferiores </h1>
            <hr>
            <h2 class="text-center center">Cadera  (Izquierdo / Derecho)</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
                    
                    <th class="text-center center" scope="col"> Cadera Izquierdo </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                    <th class="text-center center" scope="col"> Rango normal </th>
                    <th class="text-center center" scope="col"> Cadera Derecho </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl3c1" value="<%=flexionCaderaIz%>" type="number" max="125" min="-125" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c48" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadFlexoresIz.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                   
                    <td class="text-center">0 -125</td>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl3c2" value="<%=flexionCaderaDer%>" type="number" max="125" min="-125" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c47" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadFlexoresDer.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl3c3" value="<%=extensionCaderaIz%>" type="number" max="15" min="-15" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c50" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadExtensoresIz.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                    <td class="text-center">0 - 15</td>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl3c4" value="<%=extensionCaderaDer%>" type="number" max="15" min="-15" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c49" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadExtensoresDer.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ADUCTORES</th>
                    <td class="text-center center"><input  id="vrl3c7" value="<%=aduccionCaderaIz%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c52" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadAductoresIZ.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                    <td class="text-center">0 - 45</td>
                    <th class="text-center center" scope="row">ADUCTORES</th>
                    <td class="text-center center"><input  id="vrl3c8" value="<%=aduccionCaderaDer%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c51" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadAductoresDer.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ABEDUCTORES</th>
                    <td class="text-center center"><input  id="vrl3c5" value="<%=abduccionCaderaIz%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c54" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadAbeductoresIz.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                    <td class="text-center">45 - 0</td>
                    <th class="text-center center" scope="row">ABEDUCTORES</th>
                    <td class="text-center center"><input  id="vrl3c6" value="<%=abduccionCaderaDer%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c53" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadAbeductoresDer.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ROTADORES INTERNOS</th>
                    <td class="text-center center"><input  id="vrl3c9" value="<%=rotacionInternaCaderaIz%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c56" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)"> 
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadRotadoresInternosIz.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                    <td class="text-center">0 -45</td>
                    <th class="text-center center" scope="row">ROTADORES INTERNOS</th>
                    <td class="text-center center"><input   id="vrl3c10" value="<%=rotacionInternaCaderaDer%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c55" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadRotadoresInternosDer.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ROTADORES EXTERNOS</th>
                    <td class="text-center center"><input  id="vrl3c11" value="<%=rotacionExternaCaderaIz%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c58" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadRotadoresExternosDerIz.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                    <td class="text-center">0 -45</td>
                    <th class="text-center center" scope="row">ROTADORES EXTERNOS</th>
                    <td class="text-center center"><input  id="vrl3c12" value="<%=rotacionExternaCaderaDer%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c57" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbCadRotadoresExternosDer.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                  </tr>
                 
                
                </tbody>
            </table>
        </div>
        <div>
              <h2 class="card-title text-center center">Rodilla (Izquierdo / Derecho)</h2>
              <hr>
              <table class="table table-bordered mt-1" id="tfoi">
                  <thead>
                    <tr>
                      
                      <th class="text-center center" scope="col"> Rodilla </th>
                      <th class="text-center center" scope="col"> Rango encontrado </th>
                      <th class="text-center center" scope="col"> Fuerza muscular </th>
                      <th class="text-center center" scope="col"> Rango normal </th>
                      <th class="text-center center" scope="col"> Rodilla  </th>
                      <th class="text-center center" scope="col"> Rango encontrado </th>
                      <th class="text-center center" scope="col"> Fuerza muscular </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th class="text-center center" scope="row">FLEXORES</th>
                      <td class="text-center center"><input  id="vrl3c13" value="<%=flexionRodillaIz%>" type="number" max="130" min="-130" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c60" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbRodillaFlexorIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 -130</td>
                      <th class="text-center center" scope="row">FLEXORES</th>
                      <td class="text-center center"><input  id="vrl3c14" value="<%=flexionRodillaDer%>" type="number" max="130" min="-130" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c59" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbRodillaFlexorDerecho.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                    <tr>
                      <th class="text-center center" scope="row">EXTENSORES</th>
                      <td class="text-center center"><input  id="vrl3c15" value="<%=extensionRodillaIz%>" type="number" max="130" min="-130" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c62" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbRodillaExtensorIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center"> 0- 130 </td>
                      <th class="text-center center" scope="row">EXTENSORES</th>
                      <td class="text-center center"><input  id="vrl3c16" value="<%=extensionRodillaDer%>" type="number" max="130" min="-130" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c61" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbRodillaExtensorDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                  
                   
                  
                  </tbody>
              </table>
        </div>
        <div>
              <h2 class="card-title text-center center">Cuello de pie (Izquierdo / Derecho) </h2>
              <hr><table class="table table-bordered mt-1" id="tfoi">
                  <thead>
                    <tr>
                      
                      <th class="text-center center" scope="col"> Cuello de pie Izquierdo </th>
                      <th class="text-center center" scope="col"> Rango encontrado </th>
                      <th class="text-center center" scope="col"> Fuerza muscular </th>
                      <th class="text-center center" scope="col"> Rango normal </th>
                      <th class="text-center center" scope="col"> Cuello de pie Derecho </th>
                      <th class="text-center center" scope="col"> Rango encontrado </th>
                      <th class="text-center center" scope="col"> Fuerza muscular </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th class="text-center center" scope="row">PLANTIFLEXORES</th>
                      <td class="text-center center"><input  id="vrl3c17" value="<%=plantiflexionCuellopieIz%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c64" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbTobilloPlantiflexorIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 -45</td>
                      <th class="text-center center" scope="row">PLANTIFLEXORES</th>
                      <td class="text-center center"><input  id="vrl3c18" value="<%=plantiflexionCuellopieDer%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c63" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbTobilloPlantiflexorDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                    <tr>
                      <th class="text-center center" scope="row">DORSIFLEXORES</th>
                      <td class="text-center center"><input  id="vrl3c19" value="<%=dorsiflexionCuelloPieIz%>" type="number" max="25" min="-25" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c66" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbTobilloDorsiFleIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">25 -0 </td>
                      <th class="text-center center" scope="row">DORSIFLEXORES</th>
                      <td class="text-center center"><input  id="vrl3c20" value="<%=dorsiflexionCuelloPieDer%>" type="number" max="25" min="-25" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c65" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbTobilloDorsiFleDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                    <tr>
                      <th class="text-center center" scope="row">EVERSORES</th>
                      <td class="text-center center"><input  id="vrl3c23" value="<%=eversionCuellopieIz%>" type="number" max="30" min="-30" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c70" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbPieEvertoresIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 -30</td>
                      <th class="text-center center" scope="row">EVERSORES</th>
                      <td class="text-center center"><input  id="vrl3c24" value="<%=eversionCuellopieDer%>" type="number" max="30" min="-30" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c69" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbPieEvertoresDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                    <tr>
                      <th class="text-center center" scope="row">INVERSORES</th>
                      <td class="text-center center"><input  id="vrl3c21" value="<%=inversionCuellopieIz%>" type="number" max="15" min="-15" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c68" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbPieInvertoresIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 -15</td>
                      <th class="text-center center" scope="row">INVERSORES</th>
                      <td class="text-center center"><input  id="vrl3c22" value="<%=inversionCuellopieDer%>" type="number" max="15" min="-15" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c67" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbPieInvertoresDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                   
                  
                  </tbody>
              </table>
        </div>
        <div>
              <h2 class="card-title text-center center">Dedos pie (Izquierdo / Derecho) </h2>
              <hr>
              <table class="table table-bordered mt-1" id="tfoi">
                  <thead>
                    <tr>
                      
                      <th class="text-center center" scope="col"> Dedos pie Izquierdo </th>
                      <th class="text-center center" scope="col"> Rango encontrado </th>
                      <th class="text-center center" scope="col"> Fuerza muscular </th>
                      <th class="text-center center" scope="col"> Rango normal </th>
                      <th class="text-center center" scope="col"> Dedos pie Derecho </th>
                      <th class="text-center center" scope="col"> Rango encontrado </th>
                      <th class="text-center center" scope="col"> Fuerza muscular </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th class="text-center center" scope="row">FLEXION MTF</th>
                      <td class="text-center center"><input  id="vrl3c25" value="<%=dedosPieFlexionMTFIz%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select   id="vrl5c72" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieFlexoresIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 -45</td>
                      <th class="text-center center" scope="row">FLEXION MTF</th>
                      <td class="text-center center"><input  id="vrl3c26" value="<%=dedosPieFlexionMTFDer%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c71" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieFlexoresDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                    <tr>
                      <th class="text-center center" scope="row">FLEXION ITF</th>
                      <td class="text-center center"><input  id="vrl3c27" value="<%=dedosPieFlexionITFIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c74" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieExtensoresIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 - 80 </td>
                      <th class="text-center center" scope="row">FLEXION ITF</th>
                      <td class="text-center center"><input  id="vrl3c28" value="<%=dedosPieFlexionITFDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c73" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieExtensoresDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                    <tr>
                      <th class="text-center center" scope="row">EXTENSION MTF</th>
                      <td class="text-center center"><input  id="vrl3c29" value="<%=dedosPieExtensionMTFIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c76" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieAductoresIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 -80</td>
                      <th class="text-center center" scope="row">EXTENSION MTF</th>
                      <td class="text-center center"><input  id="vrl3c30" value="<%=dedosPieExtensionMTFDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c75" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieAductoresDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                    </tr>
                    <tr>
                      <th class="text-center center" scope="row">EXTENSION ITF</th>
                      <td class="text-center center"><input  id="vrl3c31" value="<%=dedosPieExtensionITFIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c78" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieAbeductoresIz.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      <td class="text-center">0 -80</td>
                      <th class="text-center center" scope="row">EXTENSION ITF</th>
                      <td class="text-center center"><input  id="vrl3c32" value="<%=dedosPieExtensionITFDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl3',this.id,this.value)"> </td>
                      <td class="text-center" style="width: 20%;">
                          <select  id="vrl5c77" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                            <option value=""></option>
                            <%  resulCombo.clear();
                            resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                             for(int i=0;i<resulCombo.size();i++){
                                cmb=(ComboVO)resulCombo.get(i);
                                if(cbDedosPieAbeductoresDer.trim().equals(cmb.getId())){
                                 %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                                 <%}
                                 else{
                                 %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                                 <%
                               }
                           }%>
                          </select> 
                      </td>
                      </tr>
                   
                  
                  </tbody>
              </table>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</html>

<!-- 66904571 -->