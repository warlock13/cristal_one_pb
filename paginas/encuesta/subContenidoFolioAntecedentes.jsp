<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import =  "java.util.ArrayList"%>
<%@ page import =  "java.util.List"%>


<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />

<!DOCTYPE HTML >  
<style type="text/css">
    #div2 {
        overflow:scroll;
        height:300px;
        width:100%;
    }
    #div2 table {
        width:100%;
    }

    .misbordes {
        border-right: solid #a6c9e2 1px;
        border-bottom: solid #a6c9e2 1px ;
        height: auto;
        font-size: 11px;
        font-family: Arial, Helvetica, sans-serif;
    }

    .paratrplantilla {
        background-color: #f5f5f5;
        font-size: 11px;
        font-family: Arial, Helvetica, sans-serif;
    }

</style>
<%
    ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
    Connection conexion = iConnection.getConnection();
    Conexion cn = new Conexion(conexion);

    ControlAdmin beanAdmin = new ControlAdmin();
    beanAdmin.setCn(cn);

    ArrayList resulCombo = new ArrayList();

    String var = request.getParameter("tipo");
    String tipo_folio = request.getParameter("tipo_folio");
    String id_evolucion = request.getParameter("id_evolucion");
    String id_sexo = request.getParameter("id_sexo");
    String rango = request.getParameter("rango");
    String rango2 = request.getParameter("rango2");
    String bloquear = "";
    int contador = 0;
    String edad = request.getParameter("edade");
    String profesional = request.getParameter("profesional");
    String servicio_auditoria = request.getParameter("servicio_auditoria");
    try {
        String SQL = beanSession.cn.traerElQuery(1117).toString();
        PreparedStatement stmt = (PreparedStatement) new LoggableStatement(conexion, SQL, 1005, 1007);
        stmt.setString(1, servicio_auditoria);
        stmt.setString(2, id_evolucion);
        stmt.setString(3, var);
        stmt.setString(4, id_evolucion);
        stmt.setString(5, var);

        ResultSet rs = stmt.executeQuery();
%>   

<div id ="div2" style="background-color: white; height: 100%;">
    <table width="100%"  cellpadding="0" cellspacing="0"  > 
        <!-- <tbody width="100%" class="inputBlanco"> --> 

        <%       while (rs.next()) {

                String validacion = "";
                String campo_destino = "";
                String texto = "";
                String color = "";

                String tdid = "formulario." + var + "." + rs.getString("referencia") + "td";

                if (rs.getString("campo_destino") != null) {
                    campo_destino = rs.getString("campo_destino");
                }

                if (rs.getString("texto") != null) {
                    texto = rs.getString("texto");
                }

                if (rs.getString("color") != null) {
                    color = rs.getString("color");
                }

                if (rs.getString("validaciones") != null) {
                    validacion = "validarCampo(this.id,'" + rs.getString("validaciones") + "','v1');";
                }

                String actualizar = "";

                if (rs.getInt("formulas") > 0) {
                    actualizar = "actualizarCampoFormula(this.id, this.value);";
                }

                if (rs.getInt("estado_folio") > 0) {
                    bloquear = "disabled";
                } else {
                    bloquear = " ";
                }

                if (rs.getString("id_jerarquia").equals("TI")) {%>
        <tr class="paratrplantilla overs" align="left"> 
            <td class="misbordes" style="border-left: solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' > <h3><%=rs.getString("descripcion")%></h3></td> 
            <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > </td>                     
        </tr>                    
        <%} else if (rs.getString("id_jerarquia").equals("PA")) {%>
        <tr class="paratrplantilla overs" align="left"> 
            <td class="misbordes" style="border-left: solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' > <strong><%=rs.getString("descripcion")%></strong></td> 
            <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > </td>                                           
        </tr> 
        <%} else {%>
        <tr class="paratrplantilla overs" align="left">  

            <td  class="misbordes" id="<%=tdid%>" style="border-left: solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' title='<%= rs.getString("etiqueta")%>'> <%=rs.getString("descripcion")%>  </td>                    
            <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > 
                <% if (rs.getString("tipo_input").trim().equals("input")) {
                        if (rs.getInt("id_formula") > 0) {%> 
                <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width: <%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>                               
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input   type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>"  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> />                             
                <% }

                } else if (rs.getString("tipo_input").trim().equals("inputl")) {;
                    if(rs.getString("campo_bloquea")==null){
                        %>
                        <input  type="text" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' <%= bloquear%> onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>"/>
                        
                        <%
                    } else if(rs.getString("campo_bloquea").trim().equals("1")) {
                        %> 
                        <input  type="text" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' <%= bloquear%> readonly onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>"/>
                        <%
                    } else {
                        %>
                        <input  type="text" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' <%= bloquear%> onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>"/>
                        <%
                    }
                        %>
                
                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
                     onclick="traerVentanitaFuncionesHcPlantilla(this.id, 'actualizarDxIngresoHC', 'divParaVentanita', '<%= "formulario." + var + "." + rs.getString("referencia")%>', '<%= rs.getString("id_query_ventanita")%>')"
                     src="/clinica/utilidades/imagenes/acciones/buscar.png">         
                     
                     <% } else if (rs.getString("tipo_input").trim().equals("decimal")) {;
                        if(rs.getString("campo_bloquea")==null){
                            %>
                            <input  type="number" step='<%=rs.getString("cantdecimales")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheck(event, this.id, '<%=rs.getString("cantdecimales")%>', '<%=rs.getString("maxlenght")%>');" name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>"  onblur="ValidacionesAIEPI('<%= campo_destino%>');guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> />
                            <%
                        } else if(rs.getString("campo_bloquea").trim().equals("1")) {
                            %> 
                            <input  type="text" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' <%= bloquear%> readonly onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>"/>
                            <%
                        } else {
                            %>
                            <input  type="number" step='<%=rs.getString("cantdecimales")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheck(event, this.id, '<%=rs.getString("cantdecimales")%>', '<%=rs.getString("maxlenght")%>');" name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>"  onblur="ValidacionesAIEPI('<%= campo_destino%>');guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> />                            <%
                        }
                            %> 
                
                <%} else if (rs.getString("tipo_input").trim().equals("hora")) {%>
                <input  type="time" step="600" style='width: <%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> /> 
                <%} else if (rs.getString("tipo_input").trim().equals("numeros")) {
                    if (rs.getInt("id_formula") > 0) {%> 
                <input  type="number" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onkeypress="javascript:return soloTelefono(event);" oninput="maxLongitudNumero(this.id,'<%=rs.getString("maxlenght")%>');" onkeyup="ingresarLimites(this.id, '<%= rs.getString("maximo_input")%>', '<%= rs.getString("minimo_input")%>', this.value); alertasGenerales(this.id, '<%= rs.getString("valor_alerta")%>', this.value); <%= actualizar%>"  onblur="ValidacionesAIEPI('<%= campo_destino%>');guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>  
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="number" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onkeypress="javascript:return soloTelefono(event);" oninput="maxLongitudNumero(this.id,'<%=rs.getString("maxlenght")%>');" onkeyup="ingresarLimites(this.id, '<%= rs.getString("maximo_input")%>', '<%= rs.getString("minimo_input")%>', this.value); alertasGenerales(this.id, '<%= rs.getString("valor_alerta")%>', this.value); <%= actualizar%>"  onblur="ValidacionesAIEPI('<%= campo_destino%>');guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> />
                <%}
                } else if (rs.getString("tipo_input").trim().equals("decimal")) {
                    if (rs.getInt("id_formula") > 0) {%> 
                <input  type="number" step='<%=rs.getString("cantdecimales")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheck(event, this.id, '<%=rs.getString("cantdecimales")%>', '<%=rs.getString("maxlenght")%>');" name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>"  onblur="ValidacionesAIEPI('<%= campo_destino%>');guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="number" step='<%=rs.getString("cantdecimales")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheck(event, this.id, '<%=rs.getString("cantdecimales")%>', '<%=rs.getString("maxlenght")%>');" name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>"  onblur="ValidacionesAIEPI('<%= campo_destino%>');guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value); <%= validacion%>" <%= bloquear%> />
                <%}
                } else if (rs.getString("tipo_input").trim().equals("check")) {
                    String estado = " ";
                    if (rs.getString("dato").equals("SI")) {
                        estado = "checked";
                    }

                    if (rs.getInt("id_formula") > 0) {%> 
                <input  type="checkbox" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%>  <%= estado%>/>
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="checkbox" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);ValidacionesAIEPI('<%= campo_destino%>');guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%> <%= estado%> />
                <%}
                } else if (rs.getString("tipo_input").trim().equals("switchsn")) {
                    String estado = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("SI") || rs.getString("dato").equals("NO")) {
                        class_span1 = "onoffswitch-inner2";
                        class_span2 = "onoffswitch-switch2";
                        activar = "";

                        if (rs.getString("dato").equals("SI")) {
                            estado = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchsn('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }

                    if (rs.getString("campo_bloquea") == null || rs.getString("campo_bloquea").equals("")) {
                        cam_bloquear = " ";
                    } else {
                        parametros = rs.getString("campo_bloquea");
                        cam_bloquear = "bloquearMisCampos('" + parametros + "','" + var + "', this.checked);";
                    }
                %>  

                <div class="onoffswitch2" onclick="<%= activar%>">
                    <% if (rs.getInt("id_formula") > 0) {%>
                    <input  type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);<%= actualizar%><%= validacion%><%=cam_bloquear%>" <%= estado%>  <%= bloquear%>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                    <% } else if (rs.getInt("id_formula") < 1) {%>
                    <input  type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);guardarDatosPlantilla('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%><%=cam_bloquear%>" <%= estado%>  <%= bloquear%>>  
                    <% }%>
                    <label class="onoffswitch-label2" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span  class="<%= class_span1%>" id='<%= var%>-<%= rs.getString("referencia")%>-span1'></span>
                        <span  class="<%= class_span2%>" id='<%= var%>-<%= rs.getString("referencia")%>-span2'></span>
                    </label>
                </div>
                <%
                    bloquear = bloquear_aux;
                } else if (rs.getString("tipo_input").trim().equals("switchsnCronicos")) {
                    String estado = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("SI") || rs.getString("dato").equals("NO")) {
                        class_span1 = "onoffswitch-inner2";
                        class_span2 = "onoffswitch-switch2";
                        activar = "";

                        if (rs.getString("dato").equals("SI")) {
                            estado = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchsn('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }

                    if (rs.getString("campo_bloquea") == null || rs.getString("campo_bloquea").equals("")) {
                        cam_bloquear = " ";
                    } else {
                        parametros = rs.getString("campo_bloquea");
                        cam_bloquear = "bloquearMisCampos('" + parametros + "','" + var + "', this.checked);";
                    }
                %>  

                <div class="onoffswitch2" onclick="<%= activar%>">
                    <% if (rs.getInt("id_formula") > 0) {%>
                    <input  type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);<%= actualizar%><%= validacion%><%=cam_bloquear%>" <%= estado%>  <%= bloquear%>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                    <% } else if (rs.getInt("id_formula") < 1) {%>
                    <input  type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);ocultarTabs();guardarDatosPlantilla('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%><%=cam_bloquear%>" <%= estado%>  <%= bloquear%>>  
                    <% }%>
                    <label class="onoffswitch-label2" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span  class="<%= class_span1%>" id='<%= var%>-<%= rs.getString("referencia")%>-span1'></span>
                        <span  class="<%= class_span2%>" id='<%= var%>-<%= rs.getString("referencia")%>-span2'></span>
                    </label>
                </div>
                <%
                    bloquear = bloquear_aux;
                } else if (rs.getString("tipo_input").trim().equals("switchS10")) {
                    String estado = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("1") || rs.getString("dato").equals("0")) {
                        class_span1 = "onoffswitch-inner2";
                        class_span2 = "onoffswitch-switch2";
                        activar = "";

                        if (rs.getString("dato").equals("1")) {
                            estado = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchS10('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }
                %>  


                <div class="onoffswitch2" onclick="<%= activar%>">
                    <% if (rs.getInt("id_formula") > 0) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id, this.checked);<%= actualizar%><%= validacion%>" <%= estado%>  <%= bloquear%>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                    <% } else if (rs.getInt("id_formula") < 1) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id, this.checked);guardarDatosPlantilla('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= estado%>  <%= bloquear%>  >
                    <% }%>
                    <label class="onoffswitch-label2" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span  class="<%= class_span1%>" id='<%= var%>-<%= rs.getString("referencia")%>-span1'></span>
                        <span  class="<%= class_span2%>" id='<%= var%>-<%= rs.getString("referencia")%>-span2'></span>
                    </label>
                </div>
                <%
                    bloquear = bloquear_aux;
                } else if (rs.getString("tipo_input").trim().equals("switch10")) {
                    System.out.println("####" + rs.getString("referencia"));

                    String estado1 = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("1") || rs.getString("dato").equals("0")) {
                        class_span1 = "onoffswitch-inner3";
                        class_span2 = "onoffswitch-switch3";
                        activar = "";

                        if (rs.getString("dato").equals("1")) {
                            estado1 = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitch10('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }
                %>  
                <div class="onoffswitch3" onclick="<%= activar%>">
                    <% if (rs.getInt("id_formula") > 0) {%>
                    <input  type="checkbox" name='<%= var%>' class="onoffswitch-checkbox3" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id, this.checked);<%= actualizar%><%= validacion%>" <%= estado1%>  <%= bloquear%>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                    <% } else if (rs.getInt("id_formula") < 1) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox3" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id, this.checked);guardarDatosPlantilla('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= estado1%>  <%= bloquear%>  >
                    <% }%>
                    <label class="onoffswitch-label3" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span  class="<%= class_span1%>" id='<%= var%>-<%= rs.getString("referencia")%>-span1'></span>
                        <span  class="<%= class_span2%>" id='<%= var%>-<%= rs.getString("referencia")%>-span2'></span>
                    </label>
                </div>

                <%
                    bloquear = bloquear_aux;
                } else if (rs.getString("tipo_input").trim().equals("switchobs")) {
                    String estado2 = "";
                    String ocultar = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String bloquear_aux = "";
                    System.out.println("#" + rs.getString("dato").trim().length() + "#");

                    if (rs.getString("dato").trim().equals("Normal") || !rs.getString("dato").trim().equals("")) {
                        class_span1 = "onoffswitch-inner4";
                        class_span2 = "onoffswitch-switch4";

                        if (rs.getString("dato").trim().equals("Normal")) {
                            estado2 = " ";
                            ocultar = "hidden";
                        } else {
                            estado2 = "checked";
                            ocultar = " ";
                        }
                    } else {
                        estado2 = " ";
                        ocultar = "hidden";

                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getInt("estado_folio") == 0) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchobs('" + var + "', '" + rs.getString("referencia") + "')";
                        }
                    }
                %>  

                <div class="onoffswitch4" style="float: left;" onclick="<%= activar%>">
                    <% if (rs.getInt("id_formula") > 0) {%>
                    <input  type="checkbox" name='<%= var%>' class="onoffswitch-checkbox4" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>'  onclick="valorSwitchObservacion(this.id, this.checked);<%= actualizar%><%= validacion%>" <%=estado2%>  <%= bloquear%>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                    <% } else if (rs.getInt("id_formula") < 1) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox4" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>'  onclick="valorSwitchObservacion(this.id, this.checked);guardarDatosPlantilla('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%=estado2%>  <%= bloquear%>  >
                    <% }%>                      
                    <label class="onoffswitch-label4" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span  class="<%= class_span1%>" id='<%= var%>-<%= rs.getString("referencia")%>-span1'></span>
                        <span  class="<%= class_span2%>" id='<%= var%>-<%= rs.getString("referencia")%>-span2'></span>
                    </label>
                </div>
                <% bloquear = bloquear_aux;%>
                <label  id='switchobservacion<%= "formulario." + var + "." + rs.getString("referencia")%>' <%= ocultar%> >
                    <div style="float: left; align-items: center;display: flex; width: 60%;font-size: 10px;" > 
                        <label style="padding-left: 10px; padding-right: 10px; ">Hallazgo</label>
                        <% if (rs.getInt("id_formula") > 0) {%>
                        <textarea maxlength='<%=rs.getString("maxlenght")%>' style='width: 100%; text-align: left; border: 1px solid #4297d7;' title='<%= rs.getString("referencia")%>' name='<%= var%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress='return validarKey(event, this.id);' onblur="<%= validacion%>" <%= bloquear%> ><%= rs.getString("dato").trim()%></textarea> 
                        <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                        <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                        <% } else if (rs.getInt("formulas") > 0) {%>
                        <textarea maxlength='<%=rs.getString("maxlenght")%>' style='width: 100%; text-align: left; border: 1px solid #4297d7;' title='<%= rs.getString("referencia")%>' name='<%= var%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>'  onkeypress='return validarKey(event, this.id);' onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);actualizarCampoFormula(this.id, this.value);<%= validacion%>" <%= bloquear%> ><%= rs.getString("dato").trim()%></textarea> 
                        <% } else if (rs.getInt("id_formula") < 1) {%>
                        <textarea  maxlength='<%=rs.getString("maxlenght")%>' style='width: 100%; text-align: left; border: 1px solid #4297d7;' title='<%= rs.getString("referencia")%>' name='<%= var%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>'   onkeypress='return validarKey(event, this.id);' onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= validacion%>" <%= bloquear%>><%= rs.getString("dato").trim()%></textarea>                  
                        <% }
                        %>
                    </div>
                </label>
                <% } else if (rs.getString("tipo_input").trim().equals("textadc")) {%>
                <textarea   style='width: <%=rs.getString("ancho")%>;height:<%=rs.getString("altura")%> ; text-align: left; border: 1px solid #4297d7;' 
                          type="text" title='<%= rs.getString("referencia")%>' id='<%= "txtTexArea" + rs.getString("referencia")%>' 
                          name='<%= rs.getString("referencia")%>' 
                          onkeypress="return validarKey(event, this.id)" placeholder="Ingresar texto..." 
                          onblur="guardarDatosPlantilla('<%= var%>', this.name, this.value);" <%= bloquear%> ><%= rs.getString("dato")%></textarea>                                               
                <%
                } else if (rs.getString("tipo_input").trim().equals("texta")) {%>
                <textarea  maxlength='<%=rs.getString("maxlenght")%>' style='width: <%=rs.getString("ancho")%>;height:<%=rs.getString("altura")%>; text-align: left; border: 1px solid #4297d7;' 
                          type="text" title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' 
                          name='<%= rs.getString("referencia")%>' 
                          onkeyup="return validarKey(event, this.id)" placeholder="Ingresar texto..." 
                          onblur="guardarDatosPlantilla('<%= var%>', this.name, this.value);" <%= bloquear%> ><%= rs.getString("dato")%></textarea>                                               
                <%
                } else if (rs.getString("tipo_input").trim().equals("combo")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <select  size="1"  style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= rs.getString("referencia")%>' value="" onchange="guardarDatosPlantilla('<%= var%>', this.name, this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> > 
                    <option value=""></option>
                    <%  resulCombo.clear();
                        resulCombo = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(rs.getString("id_query_combo")));
                        ComboVO cmb;
                        for (int i = 0; i < resulCombo.size(); i++) {
                            cmb = (ComboVO) resulCombo.get(i);
                            if (rs.getString("dato").trim().equals(cmb.getId())) {
                    %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                    <%} else {
                    %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                    <%
                            }
                        }%>   
                </select> 
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <select  size="1"  style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= rs.getString("referencia")%>' value="" onchange="ValidacionesAIEPI('<%= campo_destino%>');
                        guardarDatosPlantilla('<%= var%>', this.name, this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> >
                    <option value=""></option>
                    <%  resulCombo.clear();
                        resulCombo = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(rs.getString("id_query_combo")));
                        ComboVO cmb;
                        for (int i = 0; i < resulCombo.size(); i++) {
                            cmb = (ComboVO) resulCombo.get(i);
                            if (rs.getString("dato").trim().equals(cmb.getId())) {
                    %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                    <%} else {
                    %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                    <%
                            }
                        }%>   
                </select> 
                <% }
                } else if (rs.getString("tipo_input").trim().equals("comboMed")) {
                %>
                <input  type="text" value='<%= rs.getString("dato").trim()%>' id="idMedEvaluacion" hidden>
                <select size="1"  
                        style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' 
                        title='<%= rs.getString("referencia")%>' 
                        id="miComboMedEvaluacion"
                        name='<%= rs.getString("referencia")%>' 
                        value='<%= rs.getString("dato").trim()%>'  
                        onclick="comboMed()" onchange ="guardarDatosPlantilla('<%= var%>', this.name, this.value)" <%= bloquear%> >                                                                   
                </select>  
                <%
                } else if (rs.getString("tipo_input").trim().equals("comboInter")) {
                %>
                <input  type="text" value='<%= rs.getString("dato").trim()%>' id="idInterconsulta" hidden>
                <select size="1"  
                        style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' 
                        title='<%= rs.getString("referencia")%>' 
                        id="miComboInterconsulta"
                        name='<%= rs.getString("referencia")%>' 
                        value='<%= rs.getString("dato").trim()%>'  
                        onclick="comboInter()" onchange ="cargarInter();guardarDatosPlantilla('<%= var%>', this.name, this.value)" <%= bloquear%> >                                                                   
                </select>  
                <%
                } else if (rs.getString("tipo_input").trim().equals("inputFecha")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onchange="<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onblur="validarFechaPatron(this.value, this.id)" onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />   
                <% }
                } else if (rs.getString("tipo_input").trim().indexOf(",") > 0) {
                    String tipo_input = rs.getString("tipo_input");
                    String[] tipos_input = tipo_input.split(",");
                    System.out.println("####" + tipo_input);
                    if (tipos_input[0].equals("2input")) {

                        String referencia = rs.getString("referencia");
                        String[] referencias = referencia.split(",");
                        String dato = rs.getString("dato");
                        String[] datos = dato.split(",");
                        System.out.println("####" + dato);
                        //System.out.println("#" + rs.getString("dato").trim().length() + "#");
                %>
                <input  type='<%= tipos_input[1].trim()%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= referencias[0]%>' id='<%= "formulario." + var + "." + referencias[0]%>' name='<%= var%>' value='<%=datos[0]%>'  onchange="guardarDatosPlantilla(this.name, '<%= referencias[0]%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />   
                <img class='<%= var + "-" + referencias[0] + "-loader"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
                <img onclick="obtenerValorPlantilla(this, '<%= var%>', '<%= referencias[0]%>');" class='<%= var + "-" + referencias[0] + "-error"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
                <input  type='<%= tipos_input[2].trim()%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= referencias[1]%>' id='<%= "formulario." + var + "." + referencias[1]%>' name='<%= var%>' value='<%= datos[1]%>'  onchange="guardarDatosPlantilla(this.name, '<%= referencias[1]%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />   
                <img class='<%= var + "-" + referencias[1] + "-loader"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
                <img onclick="obtenerValorPlantilla(this, '<%= var%>', '<%= referencias[1]%>');" class='<%= var + "-" + referencias[1] + "-error"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">

                <%  }
                } else if (rs.getString("tipo_input").trim().equals("inputFechaPosterior")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 2);" onchange="<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 2);" onblur="validarFechaEscrita(this.value, this.id, 2)" onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />   
                <% }
                } else if (rs.getString("tipo_input").trim().equals("inputFechaAnterior")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 1);" onchange="<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 1);" onblur="validarFechaEscrita(this.value, this.id, 1)" onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />   
                <% }
                } else if (rs.getString("tipo_input").trim().equals("inputFechaNacimientoHoy")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297D7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 4);" onchange="<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297D7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 4);" onblur="validarFechaEscrita(this.value, this.id, 3)" onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <% }
                } else if (rs.getString("tipo_input").trim().equals("inputFechaNacimiento")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297D7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 3);" onchange="<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297D7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 3);" onblur="validarFechaEscrita(this.value, this.id, 3)" onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <% }
                } else if (rs.getString("tipo_input").trim().equals("inputFechaNacimientoHoy")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 4);" onchange="<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 4);" onblur="validarFechaEscrita(this.value, this.id, 1)" onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />   
                <% }
                } else if (rs.getString("tipo_input").trim().equals("inputFechaNacimiento")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 3);" onchange="<%= actualizar%><%= validacion%>" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="date" style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onclick="validarFechasPlantillas(this.id, 3);" onblur="validarFechaEscrita(this.value, this.id, 1)" onchange="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> />   
                <% }

                } else if (rs.getString("tipo_input").trim().equals("buttonC")) {%>
                <input type="button" value="CARGAR" onclick="cargarExamenFisico()" class="small button blue" <%= bloquear%>/>
                <%} else if (rs.getString("tipo_input").trim().equals("input2")) {%>

                <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>;height:17px; margin-bottom:20px ;border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>'  onblur="guardarDatosPlantilla(this.name, '<%= rs.getString("referencia")%>', this.value)" <%= bloquear%> />  

                <% }
                    if (rs.getString("tipo_input").trim().equals("button")) {
                        List<String> folios = Arrays.asList("HCIN", "HCAZ", "HCAD", "HCJU", "HCVE", "HCPI", "HCC", "HCPF");
                        if (folios.contains(tipo_folio)) {%>
                            <div style="text-align: center; ">  <input type="button" value="GRAFICA" id='<%= "formulario." + var + "." + rs.getString("referencia")%>'  onclick="graficaPlantilla(<%=id_evolucion%>, '<%=rs.getString("referencia")%>')" class="small button blue" >  </div>
                        <%}else{%>
                            <div style="text-align: center; ">  <input type="button" value="GRAFICAS" id='<%= "formulario." + var + "." + rs.getString("referencia")%>'  onclick="graficaPlantilla(<%=id_evolucion%>, '<%=rs.getString("referencia")%>')" class="small button blue" <%= bloquear%> >  </div>
                        <%}
                    }
                        if (rs.getString("tipo_input").trim().equals("buttonVih")) {%>
                <div style="text-align: left; ">  <input style="margin-bottom: -9px;" type="button" value="IMPRIMIR" id='<%= "formulario." + var + "." + rs.getString("referencia")%>'  onclick="imprimirPdd(<%=id_evolucion%>, '<%= var%>')" class="small button blue" <%= bloquear%> >  </div>
                    <%}
                    %>
                    <% if (rs.getString("tipo_input").trim().indexOf(",") > 0) {
                        } else {%>
                <img class='<%= var + "-" + rs.getString("referencia") + "-loader"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
                <img onclick="obtenerValorPlantilla(this, '<%= var%>', '<%= rs.getString("referencia")%>');" class='<%= var + "-" + rs.getString("referencia") + "-error"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
                <%}%>
            </td>               
        </tr> 

        <% }
                //stmt.close(); 
            }%>
        <!-- </tbody> --> 
        <%} catch (Exception e) {
                System.out.println(e.getMessage());
            }%>

    </table> 
</div>