<%@ page session="true" contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.*,java.text.*,java.sql.*" %>
<%@ page import="Sgh.Utilidades.*" %>
<%@ page import="Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import="Clinica.Presentacion.*" %>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request" />


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Historia Clínica Perinatal</title>
    <!--librerias para jqgrid  -->

    <!-- <link href="../../utilidades/css/custom-theme/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="/clinica/utilidades/css/custom-theme/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css" /> -->
    <link href="../../utilidades/jqGrid/jquery-ui.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="../../utilidades/jqGrid-3.8.2/css/ui.jqgrid.css">
    <script src="https://code.jquery.com/jquery-1.8.3.min.js" integrity="sha256-YcbK69I5IXQftf/mYD8WY0/KmEDCv1asggHpJk1trM8=" crossorigin="anonymous"></script>
    <script language=javascript src="../../utilidades/jqGrid-4.4.3/grid.locale-es.js"></script>
    <script language=javascript src="../../utilidades/jqGrid-4.4.3/jquery.jqGrid.min.js"></script>


    <!--  -->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <style>
        * {
            font-size: 12px;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        /* .form-select {
          border: 1px solid #ccc;
          border-radius: 4px;
          height: 20px;
          font-size: 12px;
          color: #333;
          border-color: #b2b2b2;
          outline: 0;
          outline: none;
          font-size: 14px;
          color: #333;
          background: linear-gradient(#fff, #f0f0f0);
          padding-left: 10px;        

        } */
        .bodyClap {
            margin: 100px;
        }

        .sectionClass {
            /* border: rgb(255, 0, 0) solid 1px; */


        }

        section div {
            border: rgb(196, 196, 196) solid 1px;
        }

        section input[type="date"] {
            font-size: 12px;
            border-radius: 10px;
        }


        #idAntecedentes {
            display: flex;
            flex-wrap: wrap;
        }

        .contenedorAnt {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            border: 0;
            /*grid-gap: 10px;*/
        }

        .contenedorAnt input,
        .contenedorAnt label,
        .contenedorAnt select {
            margin-bottom: 5px;
        }

        .contenedorAnt label {
            text-align: center;
        }

        .contenedorAnt select {
            width: 50px;
            margin-left: 10px;
        }

        input[type="checkbox"] {
            width: 15px;
        }

        #divFamPers {
            min-width: 225px;
            max-width: 230px;
            border-left: 0;
        }

        .contenedorAnt input[type="checkbox"] {
            width: 15px;
            margin-bottom: 5px;
            margin-left: 20px;
        }

        .contenedorAntTitles {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            border-top: 0;
            border-right: 0;
            border-left: 0;

        }

        .contenedorAntTitles label:first-child {
            grid-column: span 2;


        }

        .contenedorAntTitles label:nth-child(2) {
            float: right;
            margin-right: 20px;
            border: 0;
        }

        .divAntecedentes2 {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            align-items: baseline;
            padding: 10px;
            border-left: 0px;

        }

        .divAntecedentes2 input[type="checkbox"] {
            float: right;
        }

        .divAntecedentes2 label {
            width: 100px;
        }

        /**/
        #obstetricos {
            display: flex;
            flex-wrap: wrap;
            border: 0;
        }

        .titleObstetricos {
            width: 100%;
            border: 0px;
            padding: 5px;
        }

        .ultimoYGemelares {
            display: flex;
            flex-wrap: nowrap;
            flex-direction: column;
            border: 0px;
            justify-content: center;
            width: 160px;
            padding-left: 10px;
        }

        .ultimoYGemelares input[type="radio"] {
            width: 20px;
        }

        .contOstetricos {
            display: grid;
            grid-template-columns: repeat(10, 1fr);
            width: 600px;
            align-items: center;
            padding-left: 10px;
        }

        .contOstetricos input {
            width: 80%;
            height: 20px;
            border-radius: 5px;
            border: 1px solid gray;

        }

        .subContObstetrico1 {
            grid-column: span 2;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            row-gap: 20px;
            border: 0;

        }

        .subContObstetrico1 input[type="checkbox"] {
            width: 15px;
            margin-left: 12px;
        }

        .subContObstetrico2 {
            grid-column: span 2;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            row-gap: 10px;
            border: 0px;
            align-items: center;

        }

        .ultimoPrevio {
            display: grid;
            grid-template-columns: repeat(4, 1fr);
            align-items: center;
            border: 0;
            margin-bottom: 10px;
        }

        .antGemelares {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 15%;
            align-items: center;
            border: 0;
        }


        .divEmbAnterior {
            display: flex;
            flex-direction: column;
            flex-wrap: nowrap;
            align-items: center;
            width: 250px;
            gap: 10px;
            border-left: 0;
            padding: 2px 10px;
        }

        .divEmbAnteriorCont {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            border: 0;
            gap: 5px;
            align-items: center;
        }

        .divEmbAnteriorCont div {
            border: 0;
        }

        /*gestacion actual*/
        #sectionGestacionActual {
            display: flex;
            flex-direction: column;
        }

        .contenedor1 {
            display: flex;
            flex-wrap: wrap;
            border: 0;
        }

        .divCont1 {
            flex: 1;
            padding: 5px;
        }

        #divPeso {
            display: grid;
            align-content: center;
            justify-items: center;

        }

        #divTalla {
            display: grid;
            align-content: center;
            justify-items: center;
        }

        #divPeso input,
        #divTalla input {
            width: 80px;
            margin: 10px;
            height: 20px;
        }

        #divFUM-FPP {
            display: grid;

        }

        #divFUM-FPP div {
            display: flex;
            justify-content: center;
            align-items: center;
            border: 0px;
            margin: 5px;
        }

        #divEg {
            display: grid;
            justify-items: center;
        }

        #divEg div {
            display: flex;
            justify-content: space-between;
            width: 80px;
            border: 0px;
            align-items: baseline;
        }

        #divAntirubeola div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            row-gap: 10px;
            border: 0;
        }

        #divAntitetanica div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 10px;
            border: 0px;
            padding-bottom: 10px;
        }


        #divExNorma div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 10px;
            padding-top: 20px;
            border: 0px;
        }

        /*  */
        .contenedor2 {
            display: grid;
            /* grid-template-columns: repeat(8, 1fr); */
            grid-template-columns: 1fr 1fr 1fr 1fr 0.5fr 0.5fr 0.5fr 1.5fr;
            border-top: 0;
            border-left: 0;
        }

        #diVCervix div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 10px;
            border: 0;
        }

        #divGSanguineo div{
            border: 0;
        }
        
        #divVih {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            padding: 5px;
            grid-row-end: span 2;
            align-items: center;
        }

        #divVih h6,
        p {
            grid-column: span 2;
        }

        #divVih input {
            margin-left: 15px;
        }

        #divHb,
        #divHb2 {
            display: flex;
            flex-direction: column;
        }

        #divHb div,
        #divHb2 div {
            display: flex;
            justify-content: space-evenly;
            border: 0;
        }

        .inpHb {
            margin: 2px 10px;
            width: 85px;
        }

        #divFeFolfatos div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 10px;
            border: 0px;
            padding-bottom: 10px;

        }

        /*  #divSifilis {
            display: flex;
        } */

        /*  */
        #divSifilis {
            display: grid;
            /* grid-template-columns: repeat(2, 1fr); */
            justify-items: center;
            /* width: min-content; */
            grid-row-end: span 2;

        }

        #divSifilis h6 {
            grid-column: span 3;
            height: 0;
            margin: 0;
        }

        #divSifilisPrueba {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            /* width: min-content; */
            justify-items: center;
            /* border: 0; */


        }

        #divSifilisPrueba h6 {
            grid-column: span 2;
            height: 0;
            margin: 0;
        }

        .divsSifilis {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 5px;
            gap: 10px;
            border: 0;
        }

        .divsSifilis div input {
            width: 55px;
            margin-left: 5px;
        }

        #divPrueba2 input,
        #divTratamiendo1 input {
            width: 55px;
        }

        /*  */
        #divTratamiendo {
            display: grid;
            /* width: min-content; */
            justify-items: center;
        }
        .divTto{
            grid-column: span 3;
        }
        .divTto h6{
            white-space: nowrap;
        }

        /*  */
        #divPareja {
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 80px;
            border: 0;
        }

        /*
        .contPareja{
            width: max-content;
            display: flex;
            height: 130px;
            flex-direction: column;
            align-content: center;
            justify-content: space-evenly;
            align-items: center;
            padding-top: 40px;
        } */

        /* chagas */
        #divChagasPalud {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 5px;
            border-left: 0;

        }

        #divChagasPalud div {
            border-right: 0;
            padding: 5px;
            border-bottom: 0;
            border-top: 0;

        }

        #divBacteriuria div {
            display: flex;
            align-items: center;
            gap: 10px;
        }

        .contBacteriuria {
            display: grid;
            justify-items: center;
            row-gap: 10px;

        }

        .contBacteriuria div {
            width: min-content;
            white-space: nowrap;
            border: 0;

        }

        .contBacteriuria div select {
            width: 100px;
        }

        .contBacteriuria div input[type="text"] {
            width: 100px;
        }

        #divGlucemia div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            align-items: center;
            gap: 5px;
        }

        #divEstreptococo div {
            display: flex;
            align-items: center;
            flex-direction: column;
            gap: 10px;
            border: 0;
        }

        #divEstreptococo div select {
            width: 100px;
        }

        .classParto {
            display: flex;
            flex-direction: column;
            align-items: center;
            text-align: center;
        }

        h6 {
            font-size: 11px;
            font-weight: bold;
            color: rgb(0, 14, 92) !important;
            margin: 0;
            padding: 2px;

        }

        .divEmbAnteriorTitle {
            border: 0;
        }

        .titleAntecedentes {
            border-bottom: 0;
            background: rgb(226, 226, 226);
        }

        .divCont2 {
            padding: 5px;
        }

        /* -----media Query */
       

        @media (max-width : 1400px) {
            .contenedor2 {
                display: grid;
                grid-template-columns: repeat(6, 1fr);

            }

            #divSifilis {
                row-gap: 20px;
                grid-row-start: 3;
                grid-row-end: 3;
                grid-column-start: 3;
                grid-column-end: 3;
            }

        }

        @media (max-width : 1200px) {
            .bodyClap {
                margin: 20px;
            }

            .contenedor2 {
                display: grid;
                grid-template-columns: repeat(3, 1fr);

            }

            #divSifilis {
                row-gap: 20px;
                grid-row-start: 6;
                grid-row-end: 4;
                grid-column-start: 3;
                grid-column-end: 3;
            }
            .divResponsables div:first-child{
                display: grid;
                grid-template-columns: repeat(2,1fr);
            }
            .divResponsables div:first-child input:nth-child(2){
                grid-row-start: 2;

            }

        }
        @media (max-width : 1000px) {
            .divContEgrRN4 div:nth-child(3){
                display: flex;
                flex-direction: column;
                padding: 5px;
                justify-content: center;
                align-items: center;
                text-align: center;

            }
        }

        @media (max-width : 850px) {
            .ultimoYGemelares {
                border: 1px solid rgb(196, 196, 196);
                padding: 0 15px;
                border-left: 0;
            }            

            #divFamPers {
                min-width: 0;
                max-width: inherit;
                grid-column: span 2;

            }

            #idAntecedentes {
                display: grid;
                grid-template-columns: repeat(3, 1fr);

            }

            #obstetricos {
                grid-column: span 3;

            }

            .contOstetricos {
                width: auto;
            }

            .divResponsables div:first-child{
                display: grid;
                grid-template-columns: repeat(1,1fr);
            }

            .divContEgrRN2{
                border-bottom: rgb(196, 196, 196) solid 1px !important;
            }
            .divContEgrRN3{
                border-bottom: rgb(196, 196, 196) solid 1px !important;
            }
            
        }
    
        .titleEnfermedades {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            justify-items: center;
            background: rgb(226, 226, 226);;

        }

        .titleEnfermedades h6 {
            grid-column: span 4;

        }

        .titleEnfermedades div {
            border: 0;
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            gap: 10px;
        }

        .divEnfermedades {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            align-items: baseline;
            padding: 10px;
            border-left: 0px;

        }

        .divEnfermedades div {
            border: 0;
        }

        .divContEnfermedades {
            display: grid;
            grid-template-columns: repeat(3, 1fr);

        }

        .divContEnfermedades .titleEnfermedades {
            grid-column: span 3;

        }

        .divSubEnferm1 {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            row-gap: 5px;
            padding: 5px;
            width: 150px;
            align-content: stretch;
            justify-items: start;
            align-items: center;
            column-gap: 20px;
        }

        .divSubEnferm1 h6 {
            grid-column: span 2;
        }

        .divSubEnferm2 {
            display: grid;
            justify-items: center;
            gap: 10px;
        }

        .divSubEnferm2 input {
            width: 80%;
        }

        .divSubEnferm3 {
            grid-column: span 2;
            border: 0;
        }

        .divSubEnferm3 textarea {
            width: 100%;
            margin-top: 5px;
        }

        /*  */
        .divAbortoParto {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            padding: 0;

        }

        .divSubAbortoParto1 {
            grid-column: span 2;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
            align-items: center;
            background: rgb(194, 194, 194);
            color: white;
            font-weight: bold;
        }

        .divSubAbortoParto2 div {
            display: grid;
            gap: 5px;
            padding: 5px;
            width: 150px;
            border: 0;
        }

        .divSubAbortoParto2 div:nth-child(2) {
            display: grid;
            grid-template-columns: repeat(2, 1fr);

        }

        .divSubAbortoParto3 {
            display: grid;
            justify-content: center;
            align-items: center;
            justify-items: center;
        }

        .divSubAbortoParto3 input {
            width: 80%;
        }

        .divgrid {
            display: grid;
            justify-items: center;
            align-items: center;
            gap: 5px;
        }

        .divHospitEmbaraazo input {
            width: 80%;
        }

        .divAnteparto {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            gap: 5px;

        }

        .divAnteparto div {
            border: 0;
        }

        .divAnteparto h6 {
            grid-column: span 3;
        }

        .divSubAnteparto {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            align-content: center;
            justify-content: center;
            justify-items: center;
            column-gap: 5px;
        }

        .divEdadGest {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            align-items: center;
            justify-items: center;
            column-gap: 10px;
        }

        .divEdadGest h6 {
            grid-column: span 2;
        }

        .divTamFetal {
            display: flex;
            flex-direction: column;
            align-items: center;
            row-gap: 10px;
        }

        .divTamFetal div {
            border: 0;
        }

        /*  */

        .divNacimiento {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            padding: 0;
            align-items: center;
            justify-items: start;
            max-width: 280px;

        }

        .divNacimiento h6 {
            grid-column: span 3;
        }

        .divNacimiento div {
            grid-column: span 3;
            display: flex;
            align-items: center;
            gap: 10px;
            border: 0;
            flex-wrap: wrap;
        }

        .divDateTime {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 10px;
            max-width: 300px;
            align-content: center;
        }

        .divMult {
            max-width: 150px;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 10px;
            align-items: center;

        }

        .divTermin td {
            white-space: nowrap;
            text-align: center;
            vertical-align: middle;
        }

        .divTermin td label {
            display: inline-block;
            text-align: left;
            margin-right: 5px;
        }

        .divTermin td input[type="radio"] {
            vertical-align: middle;
        }

        .divIndicPrincipal {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            max-width: 400px;
            gap: 10px;
            justify-items: center;
        }

        .divIndicPrincipal1 {
            display: grid;
            border: 0;
            align-items: center;
        }

        .divIndicPrincipal2 {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            border: 0;
            align-items: center;
            gap: 5px;
            align-content: center;

        }

        .divIndicPrincipal2 label:first-child {
            grid-column: span 2;
        }

        .divIndicPrincipal2 input {
            width: 50px;
        }

        /**/
        .divClassParto {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            align-items: center;

        }

        .divClassParto h6 {
            grid-column: span 3;
        }

        .divClassParto div {
            text-align: center;
            border: 0;
        }

        .divEpisiotomia {
            display: flex;
            align-items: center;
            flex-direction: column;
            gap: 20px;
        }

        .divDesgarros {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            justify-items: center;
            align-items: start;
        }

        .divDesgarros h6 {
            grid-column: span 2;
        }

        .divDesgarros div {
            border: 0;
            display: grid;
            align-content: center;
            justify-items: center;
        }

        .divOcitocicos {
            display: grid;
            text-align: center;
        }

        .divOcitocicos div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            align-items: center;
            justify-items: center;
            border: 0;
            gap: 10px;
        }

        .divOtrosE {
            display: flex;
            gap: 10px;
        }

        .divOtrosE div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            border: 0;
            align-items: center;
        }

        .divOtrosE div h6,
        .inpEspecificar {
            grid-column: span 2;
        }

        /* recien nacido */
        .divRecNacido {
            display: flex;
            flex-direction: column;
            padding: 0;
        }

        .divRecNacido h6 {
            grid-column: span 2;
        }

        .divRecNacido .divMainRecienN {
            display: flex;
            border: 0;
            padding: 0;
            margin: 0;
        }

        .divsubRN1 {
            border-right: 0;
            padding: 5px;
        }

        .divsubRN1 div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            justify-items: center;
            border: 0;
        }

        .divsubRN1 div:nth-child(3) {
            display: flex;
            flex-direction: column;
            text-align: center;
            border: 0;

        }

        .divsubRN1 .divPesoNacer input[type="text"] {
            width: 100px;
        }

        .divsubRN1 .divPesoNacer div {
            display: flex;
            gap: 10px;
        }

        /*  */
        .divCefaLong {
            display: grid;
        }

        .divCefaLong div {
            border: 0;
        }

        .divFalleceReferido {
            padding: 0;
        }

        .divFalleceReferido div {
            border-top: 0;
            border-right: 0;
            border-left: 0;
        }

        .divAtendido div {
            border: 0;
        }

        /*  */
        .divDefectos {
            max-width: 200px;
        }

        .divDefectos table td div {
            display: flex;
            border: 0;
            align-items: baseline;
            gap: 15px;
        }

        .divDefectos table tr td {
            border: 0;
        }

        .divClsEnfermedades {
            display: flex;
            flex-direction: column;
            max-width: 200px;
        }

        .divClsEnfermedades table input {
            width: 80%;
            height: 35px;
            margin: 5px;
        }

        .divClsEnfermedades table .tdDeg {
            position: relative;
        }

        .divClsEnfermedades table .tdDeg label {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-60%, 0%) rotate(-90deg);
            transform-origin: center;
            white-space: nowrap;
            width: 20px;
            font-size: 10px;
        }

        .divClsEnfermedades div:nth-child(2) {
            display: flex;
            align-items: center;
            gap: 10px;
            justify-content: center;
            border: 0;

        }

        .divClsEnfermedades div:nth-child(3) {
            border: 0;
        }

        .divTamizajeNeonatal {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
        }

        .divTamizajeNeonatal h6 {
            grid-column: span 2;
        }

        .divTamizajeNeonatal .tableNeo td input[type="radio"] {
            vertical-align: middle;
        }

        .divTamizajeNeonatal .tableNeo td label {
            vertical-align: middle;
        }

        .divTamizajeNeonatal div:nth-child(2) {
            border: 1px dashed rgb(141, 141, 141);
        }

        .divTamizajeNeonatal div:nth-child(3) {
            border: 0;
        }

        .divMenciono {
            max-width: 100px;
        }

        /*  */
        .divContEgrRN {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            padding: 0;
        }

        .titleEgresoRN {
            flex-basis: 100%;
            background: rgb(197, 197, 197);
        }

        .divContEgrRN1 {
            flex: 1;
            border: 0;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            align-items: center;
            max-width: 200px;
            padding: 0 10px;
        }

        .divContEgrRN1 input[type="text"],
        .divContEgrRN1 input[type="datetime-local"] {
            grid-column: span 2;
        }

        .divContEgrRN2 {
            flex: 1;
            border-top: 0;
            border-bottom: 0;
        }

        .divContEgrRN3 {
            flex: 1;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            max-width: 80px;
            align-items: center;
            padding: 0px 5px;
            border: 0;
        }

        .divContEgrRN3 h6 {
            grid-column: span 2;
        }

        .divContEgrRN4 {
            flex: 1;
            display: grid;
            max-width: 220px;
            border: 0;
        }

        .divContEgrRN4 div {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            justify-items: center;
            align-items: center;
            padding-right: 15px;
            border-bottom: 0;
        }

        .divEgreYAnticoncep {
            display: flex;
            flex-wrap: wrap;
        }

        .divEgreYAnticoncep div {
            flex: 1;
        }

        .divContAntiConcepcion {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 15px;
            padding: 5px;
            border-top: 0;

        }

        .divAntiConcepcion {
            max-width: 300px;
        }

        .divResponsables {
            display: flex;
        }

        .divResponsables div {
            flex: 1;
            display: flex;
            align-items: center;
            gap: 15px;
            padding: 15px;
        }

        .divResponsables div input {
            margin-right: 20px;
        }

        .divResponsables div label {
            white-space: nowrap;
        }
        .divDateTime{
            border: 0;
            grid-column: span 2;
            display: flex;
            flex-direction: column;
        }
        #grillaListaConsultasAntenatales div{
            border: 0;
        }
        .divConsultasAntenatales{
            display: grid;
            grid-template-columns: repeat(12,1fr);            
            align-items: center;
            padding-top: 20px;
            padding-right: 30px;
        }
        .divConsultasAntenatales input{
            width: 150px ;
        }
        .divConsultasAntenatales label{
            text-align: end;
            padding-right: 20px;
        }
        .divConsultasAntenatales label::after{
            content: ":";
        }
        .divConsultasAntenatales input,.divConsultasAntenatales label{
            flex:1;
        }
        .divConsultasAntenatales div{
            grid-column: span 12;
            border: 0;
        }
        #contGrilla{
            overflow: auto;
            padding-left: 20px;
        }

        @media (max-width : 1600px) {

            .divConsultasAntenatales{
                display: grid ;
                grid-template-columns: repeat(8,1fr);

            }
            .divConsultasAntenatales div{
                grid-column: span 8 ;
            }
        }

        @media (max-width : 1000px) {

            .divConsultasAntenatales{
                display: grid ;
                grid-template-columns: repeat(6,1fr);
                gap: 10px;

            }
            .divConsultasAntenatales div{
                grid-column: span 6 ;
            }
        }

        @media (max-width : 850px) {

            .divConsultasAntenatales{
                display: grid ;
                grid-template-columns: repeat(4,1fr);
                gap: 10px;

            }
            .divConsultasAntenatales div{
                grid-column: span 4 ;
            }
        }
        select{
            font-size: 10px !important;
            font-weight: bold !important;
            letter-spacing: 1px;
        }

        /* grillaParto */

        .TrPartoEnfermedades{
            display: flex;
            flex-wrap: wrap;
        }
        .TrPartoEnfermedades div{
            flex: 1;
        }
        .grillaTrabajoParto div{
            border: 0;

        }
        .titleTrabajoParto{
            display: grid;
            background: rgb(226, 226, 226);
            justify-content: center;
            justify-items: center;
        }
        .titleTrabajoParto div{
            border: 0;
            
        }

        /*  */
        #contGrillaTP{
/*             display: flex;
            flex-direction: column;
            align-items: center; */
            overflow: auto;
        }

        .divtrabajoPartCont input{
            min-width: 50px;
        }

        @media (max-width : 850px) {
            .TrPartoEnfermedades{
                display: grid;
            }
            #contGrillaTP{
                display: flex;
                flex-direction: column;                               
            }

            
        }
        @media (min-width: 1000px) and  (max-width : 1600px) {
            .contRN{
                display: grid;
                grid-template-columns: repeat(4,1fr);
            }

        }

        /*  */
        
            /* input[type="checkbox"] {
                display: inline-flex;
                align-items: center;
                cursor: pointer;
                background-color: #fff;
                color: #fff;
                background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
                width: 14px;
                height: 14px;
                appearance: none;
                border: 0.5px solid #000000;
                background-position: 0 -2rem;
                background-size: 100%;
                background-repeat: no-repeat;
                transition: all 0.3s ease-in-out;
            }
            input[type="checkbox"]:checked {
                background-color: rgb(55, 198, 255);
                color: rgb(53, 174, 255);
                background-position: 0 0;
                border: 1px solid #00b938;
            } 

            input[type="radio"] {
                display: inline-flex;
                align-items: center;
                cursor: pointer;
                background-color: #fff;
                color: #fff;
                background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='black' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
                width: 18px !important;
                height: 18px !important;
                appearance: none;
                border: 0.5px solid #000000;
                background-position: 0 -2rem;
                background-size: 100%;
                background-repeat: no-repeat;
                transition: all 0.3s ease-in-out;
                border-radius: 50%;
            }
            input[type="radio"]:checked {
                background-color: rgb(53, 255, 120);
                color: rgb(53, 255, 120);
                background-position: 0 0;
                border: 1px solid #018d2b;
            }  */

    </style>
</head>

<body class="bodyClap">

    <table id="myGrid"></table>
    <div id="pager"></div>


    <div>
        <input type="button" class="small button blue" title="BT7QR" value="IMPRIMIR CLAP 1- Gestacón actual" onclick="window.opener.imprecionClap({nombre:'CLAP1'})">
    </div>
    <section id="sectionAntecedentes" class="sectionClass">
        <div class="divContenedor1" style="display: flex;" id="divTalla">
            <div class="divCont1" id="divViveSola">
                <label for="">Vive sola</label>
                <input type="checkbox" id="c134" name="c134">
            </div>
            <div class="divCont1" style="display: flex; align-items: center;" id="divLugarControl">
                <label for="">Lugar de control prenatal</label>
                <input class="form-control" type="text" id="c135" name="c135">
            </div>
            <div class="divCont1" style="display: flex; align-items: center;" id="divLugarParto">
                <label for="">lugar del parto/aborto</label>
                <input class="form-control" type="text" id="c136" name="c136">
            </div>
        </div>
        <div class="titleAntecedentes ">
            <h6>ANTECEDENTES</h6>
        </div>
        <div id="idAntecedentes" class="contenidoAntecedentes">

            <div id="divFamPers" class="divFamPers">
                <div class="contenedorAntTitles">
                    <label for="">Familiares</label>
                    <label for="">Personales</label>
                </div>
                <div class="contenedorAnt">
                    <input type="checkbox" id="c1" name="c1"> <label for="">TBC</label> <input type="checkbox" id="c2"
                        name="c2">
                    <input type="checkbox" id="c3" name="c3"> <label for="">Diabetes</label>
                    <select class="form-select" name="c4" id="c4">
                        <option value=""></option>
                        <option value="1">I</option>
                        <option value="2">II</option>
                        <option value="3">G</option>
                    </select>
                    <input type="checkbox" id="c5" name="c5"> <label for="">Hipertensión</label>
                    <input type="checkbox" id="c9" name="c9">
                    <input type="checkbox" id="c6" name="c6"> <label for="">Preclampsia </label>
                    <input type="checkbox" id="c10" name="c10">
                    <input type="checkbox" id="c7" name="c7"> <label for="">Eclampsia</label> <input type="checkbox"
                        id="c11" name="c11">
                    <input type="checkbox" id="c8" name="c8"> <label for="">Otra cond. medica
                        grave</label> <input type="checkbox" id="c12" name="c12">
                </div>
            </div>
            <div class="divAntecedentes2">
                <label for="">cirugia genito-urinaria</label> <input type="checkbox" id="c13" name="c13">
                <label for="">Infertilidad</label> <input type="checkbox" id="c14" name="c14">
                <label for="">Cardiopat</label> <input type="checkbox" id="c15" name="c15">
                <label for="">Nefropatia</label> <input type="checkbox" id="c16" name="c16">
                <label for="">Violencia</label> <input type="checkbox" id="c17" name="c17">
            </div>

            <div id="obstetricos">
                <div class="titleObstetricos">
                    <h6 for="">OBSTETRICOS</h6>
                </div>
                <div class="ultimoYGemelares" id="">
                    <div id="ultimoPrevio" class="ultimoPrevio">
                        <label for=""> n/c </label><input type="radio" id="c18.n/c" name="c18" value="n/c">
                        <label for=""> normal </label><input type="radio" id="c18.normal" name="c18" value="normal">
                        <label for=""> &lt;2500 </label><input type="radio" id="c18.menorA2500" name="c18" value="menorA2500">
                        <label for="" style="white-space: nowrap;"><sup>3</sup> 4000g </label><input type="radio"
                            id="c18.4000g" name="c18" value="4000g">
                    </div>
                    <div id="antGemelares" class="antGemelares">
                        <label for="" style="width: 85px;">Antecedente de gemelares</label><input type="checkbox"
                            id="c19" name="c19" value="1">
                    </div>
                </div>
                <div id="" class="contOstetricos">
                    <label for="">Gestas previas</label><input class="form-control" type="text" id="c20" name="c20">
                    <label for="">Abortos</label><input class="form-control" type="text" id="c21" name="c21">
                    <label for="">Vaginales</label><input class="form-control" type="text" id="c22" name="c22">
                    <label for="">Nacidos vivos</label><input class="form-control" type="text" id="c23" name="c23">
                    <label for="">Viven</label><input class="form-control" type="text" id="c24" name="c24">
                    <label for="">Emb. ectópico</label><input class="form-control" type="text" id="c25" name="c25">
                    <div class="subContObstetrico1">
                        <label for=""> 3 Espont consecutivos</label><input type="checkbox" id="c26" name="c26"
                            value="1">
                        <label for="">Partos</label><input class="form-control" type="text" id="c27" name="c27">
                    </div>
                    <label for="">Cesáreas</label><input class="form-control" type="text" id="c28" name="c28">
                    <label for="">Nacidos Muertos</label><input class="form-control" type="text" id="c29" name="c29">
                    <div class="subContObstetrico2">
                        <label for="">Muertos <span style="white-space: nowrap;"> 1 <sup>ra</sup>
                                sem.</span></label><input class="form-control" type="text" id="c30" name="c30">
                        <label for="">Después <span style="white-space: nowrap;"> 1 <sup>ra</sup>
                                sem.</span></label><input class="form-control" type="text" id="c31" name="c31">
                    </div>

                </div>
                <div id="" class="divEmbAnterior">
                    <div class="divEmbAnteriorTitle">
                        <label for="">Fin embarazo anterior</label>
                    </div>
                    <div class="divEmbAnteriorCont">
                        <input type="date" class="form-control" id="c32" name="c32">
                        <div>
                            <label for=""> Menos de 1 año </label><input type="checkbox" id="c33" name="c33" value="1">
                        </div>
                        <label for="">Embarazo planeado </label><input type="checkbox" id="c34" name="c34" value="1">
                        <label for="">Fracaso metodo anticonceptivo</label>
                        <select class="form-select" name="c35" id="c35">
                            <option value="">Seleccione</option>
                            <option value="1">No usaba</option>
                            <option value="2">Barrera</option>
                            <option value="3">DIU</option>
                            <option value="4">Hormonal</option>
                            <option value="5">Emergencia</option>
                            <option value="6">Natural</option>

                        </select>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- fin de la seccion ANTECEDENTES -->
    <section id="sectionGestacionActual" class="sectionClass">
        <div class="titleAntecedentes ">
            <h6>GESTACIÓN ACTUAL</h6>
        </div>
        <div class="contenedor1" id="">
            <div class="divCont1" id="divPeso">
                <label for="">PESO ANTERIOR Kg</label> <input type="text" id="c36" name="c36" onblur="validarNumero1(this.id, this.value,2,1);">
            </div>
            <div class="divCont1" id="divTalla">
                <label for="">TALLA (cm)</label><input type="text" id="c37" name="c37" onblur="validarNumero1(this.id, this.value,2,1);">
            </div>
            <div class="divCont1" id="divFUM-FPP">
                <div>
                    <label for="">F.U.M</label><input type="date" class="form-control" name="c38" id="c38">
                </div>
                <div>
                    <label for="">F.P.P</label><input type="date" class="form-control" name="c39" id="c39">
                </div>
            </div>
            <div class="divCont1" id="divEg">
                <h6>EG CONFIABLE por</h6>
                <div>
                    <label for="">FUM </label> <input type="checkbox" id="c40" name="c40">
                </div>
                <div>
                    <label for="">Eco &lt;20 s. </label> <input type="checkbox" id="c41" name="c41">
                </div>
            </div>
            <div class="divCont1" id="">
                <table class="table table-striped-columns">
                    <tr>
                        <th></th>
                        <th>FUMA ACT.</th>
                        <th>FUMA PAS.</th>
                        <th>DROGAS</th>
                        <th>ALCOHOL</th>
                        <th>VIOLENCIA</th>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap;">1 <sup>er</sup> trim</SUp></td>
                        <td><input type="checkbox" id="c42" name="c42"></td>
                        <td><input type="checkbox" id="c43" name="c43"></td>
                        <td><input type="checkbox" id="c44" name="c44"></td>
                        <td><input type="checkbox" id="c45" name="c45"></td>
                        <td><input type="checkbox" id="c46" name="c46"></td>
                    </tr>
                    <tr>
                        <td>2 <sup>er</sup> trim</SUp></td>
                        <td><input type="checkbox" id="c47" name="c47"></td>
                        <td><input type="checkbox" id="c48" name="c48"></td>
                        <td><input type="checkbox" id="c49" name="c49"></td>
                        <td><input type="checkbox" id="c50" name="c50"></td>
                        <td><input type="checkbox" id="c51" name="c51"></td>
                    </tr>
                    <tr>
                        <td>3 <sup>er</sup> trim</SUp></td>
                        <td><input type="checkbox" id="c52" name="c52"></td>
                        <td><input type="checkbox" id="c53" name="c53"></td>
                        <td><input type="checkbox" id="c54" name="c54"></td>
                        <td><input type="checkbox" id="c55" name="c55"></td>
                        <td><input type="checkbox" id="c56" name="c56"></td>
                    </tr>
                </table>
            </div>
            <div class="divCont1" id="divAntirubeola">
                <h6>ANTIRUBEOLA</h6>
                <div>
                    <label>Previa</label><input type="radio" name="c57" id="c57.Previa" value="Previa">
                    <label>No sabe</label><input type="radio" name="c57" id="c57.NoSabe" value="NoSabe">
                    <label>Embarazo</label><input type="radio" name="c57" id="c57.Embarazo" value="Embarazo">
                    <label>No</label><input type="radio" name="c57" id="c57.No" value="No">
                </div>
            </div>
            <div class="divCont1" id="divAntitetanica">
                <h6>ANTITETANICA</h6>
                <div>
                    <label>Vigente</label><input type="checkbox" id="c58" name="c58">
                    <label>DOSIS 1</label><input type="text" id="c59" name="c59">
                    <label>DOSIS 2</label><input type="text" id="c60" name="c60">
                </div>
                <p>mes gestación</p>
            </div>
            <div class="divCont1" id="divExNorma">
                <h6>EX.NORMAL</h6>
                <div>
                    <label>ODONT.</label><input type="checkbox" id="c61" name="c61">
                    <label>MAMAS</label><input type="checkbox" id="c62" name="c62">
                </div>
            </div>
        </div>
        <div class="contenedor2" id="">
            <div class="divCont2" id="diVCervix">
                <h6>CERVIX</h6>
                <div>
                    <label for="">INSP VISUAL</label>
                    <select class="form-select" name="c63" id="c63">
                        <option value="">Seleccione</option>
                        <option value="1">Normal</option>
                        <option value="2">Anormal</option>
                        <option value="3">No se hizo</option>
                    </select>
                    <label for="">PAP</label>
                    <select class="form-select" name="c64" id="c64">
                        <option value="">Seleccione</option>
                        <option value="1">Normal</option>
                        <option value="2">Anormal</option>
                        <option value="3">No se hizo</option>
                    </select>
                    <label for="">COLP</label>
                    <select class="form-select" name="c65" id="c65">
                        <option value="">Seleccione</option>
                        <option value="1">Normal</option>
                        <option value="2">Anormal</option>
                        <option value="3">No se hizo</option>
                    </select>
                </div>

            </div>
            <div class="divCont2" id="divGSanguineo">
                <div>
                    <label for="">GRUPO</label><input type="text" id="c66" name="c66">
                </div>

                <table class="table table-striped-columns">
                    <tr>
                        <td>
                            <span></span>
                        </td>
                        <td>
                            <label for="">RH</label>
                        </td>
                        <td>
                            <label for="">inmuniz</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for=""> - </label>
                        </td>
                        <td>
                            <input type="radio" id="c67.rhNegativo" name="c67" value="rhNegativo">
                        </td>
                        <td>
                            no <input type="radio" id="c109.NoInmuniz" name="c109" value="NoInmuniz">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for=""> + </label>
                        </td>
                        <td>
                            <input type="radio" id="c67.rhPositivo" name="c67" value="rhPositivo">
                        </td>
                        <td>
                            si <input type="radio" id="c109.SiInmuniz" name="c109" value="SiInmuniz">
                        </td>
                    </tr>
                </table>
                <div>
                    <label for="">Yglobulina anti D </label>
                    <select class="form-select" name="c69" id="c69">
                        <option value="">Seleccione</option>
                        <option value="si">SI</option>
                        <option value="no">No</option>
                        <option value="n/c">n/c</option>
                    </select>
                </div>

                <!-- 
                    <label for=""> + </label>                            
                <input type="checkbox">
                <label for="">golbulina anti D </label>
                <select name="" id="">
                    <option value="">SI</option>
                    <option value="">No</option>
                    <option value="">n/c</option>
                </select> 
            -->
            </div>
            <div class="divCont2" id="divToxoPlamosis">
                <h6>TOXOPLASMOSIS</h6>
                <table class="table table-striped-columns">
                    <tr>
                        <th></th>
                        <th>-</th>
                        <th>+</th>
                        <th>No se hizo</th>
                    </tr>
                    <tr>
                        <td>&lt;20sem lgG</td>
                        <td><input type="checkbox" id="c68" name="c68"></td>
                        <td><input type="checkbox" id="c69" name="c69"></td>
                        <td><input type="checkbox" id="c70" name="c70"></td>
                    </tr>
                    <tr>
                        <td>&ge;20sem lgG</td>
                        <td><input type="checkbox" id="c71" name="c71"></td>
                        <td><input type="checkbox" id="c72" name="c72"></td>
                        <td><input type="checkbox" id="c73" name="c73"></td>
                    </tr>
                    <tr>
                        <td>1<sup>a</sup> consulta lgM</td>
                        <td><input type="checkbox" id="c74" name="c74"></td>
                        <td><input type="checkbox" id="c75" name="c75"></td>
                        <td><input type="checkbox" id="c76" name="c76"></td>
                    </tr>
                </table>

            </div>
            <div class="divCont2" id="divVih">
                <h6>VIH</h6>
                <p>&lt; 20 sem</p>
                <label for="">Solicitada</label><input type="checkbox" name="c77" id="c77">
                <label for="">realizada</label><input type="checkbox" name="c78" id="c78">
                <p>&ge; 20 sem</p>
                <label for="">Solicitada</label><input type="checkbox" name="c79" id="c79">
                <label for="">realizada</label><input type="checkbox" name="c80" id="c80">

            </div>
            <div class="divCont2" id="divHb">
                <h6 for="">Hb &lt;20sem</h6>
                <input class="inpHb" type="text" name="c81" id="c81"
                    onblur="validarNumero1(this.id, this.value,2,1);"><!-- validar numero 2 decimal 1 -->
                <div>
                    <label>&lt;11.0 g/dl</label><input type="checkbox" id="c82" name="c82">
                </div>
            </div>
            <div class="divCont2" id="divFeFolfatos">
                <h6>Fe/FOLATOS indicados</h6>
                <div class="">
                    <label for="">Fe</label><input type="checkbox" name="c83" id="c83">
                    <label for="">Folatos</label><input type="checkbox" name="c84" id="c84">
                </div>
            </div>
            <div class="divCont2" id="divHb2">
                <h6>Hb &ge; 20 sem</h6>
                <input class="inpHb" type="text" id="c85" name="c85" onblur="validarNumero1(this.id, this.value,2,1);"><!-- validar numero 2 decimal 1 -->
                <div>
                    <label>&lt;11.0 g/dl</label><input type="checkbox" id="c86" name="c86">
                </div>

            </div>
            <div class="" id="divSifilis">
                <h6 style="margin: auto;">SIFILIS - Diagnóstico y Tratamiento</h6>
                <div class="" id="divSifilisPrueba">
                    <h6>Prueba</h6>
                    <div id="divPrueba1" class="divsSifilis">
                        <label for="">No treponémica</label>
                        <select class="form-select" name="c87" id="c87">
                            <option value=""></option>
                            <option value="1">+</option>
                            <option value="2">-</option>
                            <option value="3">s/d</option>
                        </select>
                        <div style="white-space: nowrap; border: 0;">
                            <label for="">&lt;20sem</label><input type="text" id="c88" name="c88">

                        </div>
                        <div style="white-space: nowrap; border: 0;">
                            <label for="">&ge;20sem</label><input type="text" id="c89" name="c89">
                        </div>
                        <select class="form-select" name="c90" id="c90">
                            <option value=""></option>
                            <option value="1">+</option>
                            <option value="2">-</option>
                            <option value="3">s/d</option>
                        </select>
                    </div>
                    <div id="divPrueba2" class="divsSifilis">
                        <label for="">Treponémica</label>
                        <select class="form-select" name="c91" id="c91">
                            <option value=""></option>
                            <option value="1">+</option>
                            <option value="2">-</option>
                            <option value="3">s/d</option>
                            <option value="4">n/c</option>
                        </select>
                        <input type="text" id="c92" name="c92"><!-- <label for="">semanas</label> -->
                        <input type="text" id="c93" name="c93"><!-- <label for="">semanas</label> -->
                        <select class="form-select" name="c94" id="c94">
                            <option value=""></option>
                            <option value="1">+</option>
                            <option value="2">-</option>
                            <option value="3">s/d</option>
                            <option value="4">n/c</option>
                        </select>
                    </div>
                </div>

                <div class="" id="divTratamiendo">
                    <h6>Tratamiento</h6>
                    <div id="divTratamiendo1" class="divsSifilis">
                        <label for="">&nbsp;</label>
                        <select class="form-select" name="c95" id="c95">
                            <option value=""></option>
                            <option value="1">no</option>
                            <option value="2">si</option>
                            <option value="3">s/d</option>
                            <option value="4">n/c</option>
                        </select>
                        <input type="text" name="c111" id="c111"><!-- <label for="">semanas</label> -->
                        <input type="text" name="c139" id="c139"><!-- <label for="">semanas</label> -->
                        <select class="form-select" name="c96" id="c96">
                            <option value=""></option>
                            <option value="1">no</option>
                            <option value="2">si</option>
                            <option value="3">s/d</option>
                            <option value="4">n/c</option>
                        </select>
                    </div>
                </div>
                <div class="" id="divTratamiendo divTto">
                    <h6>Tto. de la pareja</h6>
                    <div id="divTratamiendo1" class="divsSifilis">
                        <label for="">&nbsp;</label>
                        <select class="form-select" name="c97" id="c97">
                            <option value=""></option>
                            <option value="1">no</option>
                            <option value="2">si</option>
                            <option value="3">s/d</option>
                            <option value="4">n/c</option>
                        </select>
                        <label for="">&nbsp;</label>
                        <label for="">&nbsp;</label>
                        <select class="form-select" name="c98" id="c98">
                            <option value=""></option>
                            <option value="1">no</option>
                            <option value="2">si</option>
                            <option value="3">s/d</option>
                            <option value="4">n/c</option>
                        </select>
                    </div>
                </div>

                <!-- <div class="" id="divPareja">
                    <h6 ></h6>
                    <div class="divsSifilis">
                        <label for="">&nbsp;</label>
                        <select name="" id="">
                            <option value="">no</option>
                            <option value="">si</option>
                            <option value="">s/d</option>
                            <option value="">n/c</option>
                        </select>
                        <label for="">&nbsp;</label>
                        <label for="">&nbsp;</label>
                        <select name="" id="">
                            <option value="">no</option>
                            <option value="">si</option>
                            <option value="">s/d</option>
                            <option value="">n/c</option>
                        </select>
                    </div>
                   
                </div> -->

            </div>

            <div class="" id="divChagasPalud">
                <div class="" id="">
                    <h6>CHAGAS</h6>
                    <label for="">&nbsp;</label>
                    <select class="form-select" name="c99" id="c99">
                        <option value=""></option>
                        <option value="1">+</option>
                        <option value="2">-</option>
                        <option value="3">no se hizo</option>
                    </select>

                </div>
                <div class="" id="">
                    <h6>PALUDISMO/ MALARIA</h6>
                    <select class="form-select" name="c100" id="c100">
                        <option value=""></option>
                        <option value="1">+</option>
                        <option value="2">-</option>
                        <option value="3">no se hizo</option>
                    </select>

                </div>
            </div>

            <div class="contBacteriuria divCont2" id="divBacteriuria">
                <H6>UROCULTIVO</H6>
                <div>
                    <label for="">&lt;20sem</label>
                    <select class="form-select" name="c101" id="c101">
                        <option value=""></option>
                        <option value="1">Normal</option>
                        <option value="2">Anormal</option>
                        <option value="3">no se hizo</option>
                    </select>
                </div>
                <div>
                    <label for="">&ge;20sem</label>
                    <select class="form-select" name="c102" id="c102">
                        <option value=""></option>
                        <option value="1">Normal</option>
                        <option value="2">Anormal</option>
                        <option value="3">no se hizo</option>
                    </select>
                </div>


            </div>
            <div class="contBacteriuria divCont2" id="divGlucemia">
                <h6>GLUCEMIA EN AYUNAS</h6>
                <div>
                    <label for="">&lt;20sem</label><input type="text" id="c103" name="c103">
                </div>
                <div>
                    <label for="">&ge;30sem</label><input type="text" id="c104" name="c104">
                </div>
                <div>
                    <label for="">&ge;105 mg/dl</label><input type="checkbox" id="c105" name="c105">
                </div>
            </div>
            <div class="contEstreptococo divCont2" id="divEstreptococo">
                <h6>ESTREPTOCOCO B</h6>
                <div>
                    <label for="">35-37 semanas</label>
                    <select class="form-select" name="c106" id="c106">
                        <option value=""></option>
                        <option value="1">+</option>
                        <option value="2">-</option>
                        <option value="3">no se hizo</option>
                    </select>
                </div>

            </div>
            <div class="classParto divCont2" id="divParto">
                <h6>PREPARACIÓN PARA EL PARTO</h6>
                <input type="checkbox" name="c107" id="c107">

            </div>
            <div class="classParto divCont2" id="">
                <h6>CONSEJERIA LACTANCIA MATERNA</h6>
                <input type="checkbox" name="c108" id="c108">

            </div>
        </div>

    </section>

    <section id="sectionConsultasAntenatales" class="sectionClass">
        <div class="divConsultasAntenatales">
            <!-- ******************  -->

                <label>fecha</label>
                <input type="date" class="form-control" name="c297" id="c297">
                <label>edad gest.</label>
                <input type="text" class="form-control" name="c298" id="c298">
                <label>peso</label>
                <input type="text" class="form-control" name="c299" id="c299">
                <label>PA</label>
                <input type="text" class="form-control" name="c300" id="c300">
                <label>altura uterina</label>
                <input type="text" class="form-control" name="c301" id="c301">
                <label>presentacion</label>
                <input type="text" class="form-control" name="c302" id="c302">
                <label>FCF(ipm)</label>
                <input type="text" class="form-control" name="c303" id="c303">
                <label>movimien. fetales</label>
                <input type="text" class="form-control" name="c304" id="c304">
                <label>proteinuria</label>
                <input type="text" class="form-control" name="c305" id="c305">
                <label>signos de alarma, examenes, tratamientos</label>
                <input type="text" class="form-control" name="c306" id="c306">
                <label>iniciales tecnico</label>
                <input type="text" class="form-control" name="c307" id="c307">
                <label>proxima cita</label>
                <input type="date" class="form-control" name="c308" id="c308">


                <div id="contGrilla">
                    <div id="grillaListaConsultasAntenatales">
                        <table id="listaConsultasAntenatales"></table>
                    </div>
                </div>
            
            <!-- ********************* -->
        </div>

    </section>

    <section class="sectionClass">
        <div class="contenedor1">

            <div class="divCont1 divAbortoParto">
                <div class="divSubAbortoParto1">
                    <label for="">PARTO</label> <input type="radio" name="c110" id="c110.parto" value="parto">
                    <label for="">ABORTO</label> <input type="radio" name="c110" id="c110.aborto" value="aborto">
                </div>
                <div class="divSubAbortoParto2">
                    <div>
                        <label for="">FECHA DE INGRESO</label><input class="form-control" type="date" name="c112" id="c112">
                    </div>
                    <div>
                        <label for="">CARNÉ</label><input type="checkbox" name="c113" id="c113">
                    </div>
                </div>
                <div class="divSubAbortoParto3">
                    <h6 for="">CONSULTAS PRENATALES </h6>
                    <label>Total</label><input class="form-control" type="text" name="c114" id="c114">
                </div>
            </div>

            <div class="divCont1 divHospitEmbaraazo divgrid">
                <h6 for="">HOSPITALIZA. en EMBARAZO </h6> <input type="checkbox" name="c115" id="c115">
                <label>días</label><input class="form-control" type="text" name="c116" id="c116">
            </div>
            <div class="divCont1 divgrid">
                <label for="">CORTICOIDES ANTENATALES</label>
                <select class="form-select" name="c117" id="c117">
                    <option value=""></option>
                    <option value="1">completo</option>
                    <option value="2">incompleto</option>
                    <option value="3">ninguna</option>
                    <option value="4">n/c</option>
                </select>
                <input type="text" name="c118" id="c118"><label for="">semana inicio</label>
            </div>
            <div class="divCont1 divgrid">
                <h6> INICIO</h6>
                <select class="form-select" name="c119" id="c119">
                    <option value=""></option>
                    <option value="1">Espontáneo</option>
                    <option value="2">Inducido</option>
                    <option value="3">cesar. elect.</option>
                </select>

            </div>
            <div class="divCont1 divAnteparto">
                <h6>ROTURA DE MEMBRANAS ANTEPARTO</h6>
                <div>
                    <label for="">No</label><input type="radio" value="no" name="c122" id="c122.no">
                    <label for="">Si</label><input type="radio" value="si" name="c122" id="c122.si">
                </div>
                <div>
                    <input class="form-control" type="date" name="c123" id="c123">
                    <input class="form-control" type="time" name="c124" id="c124">
                </div>
                <div class="divSubAnteparto">
                    <label for="">&lt;sem</label><input type="radio" value="sem" name="c125" id="c125.sem">
                    <label for="">&ge;18hs.</label><input type="radio" value="18hs" name="c125" id="c125.18hs">
                    <label for="" style="white-space: nowrap;">tem. &ge;38&deg;C</label><input
                    type="checkbox" name="c126" id="c126">                    
                    <input class="form-control" style="grid-column: span 2;" type="text" name="c127" id="c127">
                </div>
            </div>
            <div class="divCont1 divEdadGest">
                <h6>
                    EDAD GEST. al parto
                </h6>
                <label for="">semanas</label>
                <label for="">días</label>
                <input type="text" class="form-control" name="c128" id="c128">
                <input type="text" class="form-control" name="c129" id="c129">
                <label for="">por FUM</label><input type="radio" value="fum" name="c130" id="c130.fum">
                <label for="">por Eco.</label><input type="radio" value="eco" name="c130" id="c130.eco">
            </div>
            <div class="divCont1 divEdadGest">
                <h6>
                    PRESENTACIÓN SITUACION
                </h6>
                <label for="">cefálica</label><input type="radio" value="cefalica" name="c131" id="c131.cefalica">
                <label for="">pelviana</label><input type="radio" value="pelviana" name="c131" id="c131.pelviana">
                <label for="">transversa</label><input type="radio" value="transversa" name="c131" id="c131.transversa">
            </div>
            <div class="divCont1 divTamFetal">
                <h6>
                    TAMAÑO FETAL ACORDE
                </h6>
                <div class="divTamFetal">
                    <label for="">No</label><input type="radio" value="no" name="c132" id="c132.no">
                    <label for="">Si</label><input type="radio" value="si" name="c132" id="c132.si">
                </div>
            </div>
            <div class="divCont1 ">
                <h6>
                    ACOMPAÑANTE
                </h6>

                <table class="table table-striped-columns">
                    <tr>
                        <th>&nbsp;</th>
                        <th><label for="">TDP</label></th>
                    </tr>
                    <tr>
                        <td><label for="">pareja</label></td>
                        <td><input type="radio" value="parejaTdp" name="c133" id="c133.parejaTdp">
                        </td>
                    </tr>
                    <tr>
                        <td><label for="">familiar</label></td>
                        <td><input type="radio" value="familiarTdp" name="c133"
                                id="c133.familiarTdp"></td>
                    </tr>
                    <tr>
                        <td><label for="">otro</label></td>
                        <td><input type="radio" value="otroTdp" name="c133" id="c133.otroTdp"></td>
                    </tr>
                    <tr>
                        <td><label for="">ninguno</label></td>
                        <td><input type="radio" value="ningunoTdp" name="c133" id="c133.ningunoTdp">
                        </td>
                    </tr>

                </table>

            </div>
        </div>

        <div class="contenedor1">
            <div class="divCont1 TrPartoEnfermedades">

                <div class="divTrabajoParto" style="overflow: auto;">
                    <div class="titleTrabajoParto">
                        <h6>
                            TRABAJO DE PARTO
                        </h6>
                        <div>
                            <label for="">detalles en parto grama</label>
                            <input type="checkbox" name="c137" id="c137">
                        </div>
                    </div>

                    <div class="divtrabajoPartCont" style="overflow: auto;">
                        <table class="table">
                            <tr>
                                <td>hora</td>
                                <td>Posicion de la madre</td>
                                <td>PA</td>
                                <td>pulso</td>
                                <td>control /10 </td>
                                <td>dilatacion</td>
                                <td>altura presente</td>
                                <td>variedad posic.</td>
                                <td>meconio</td>
                                <td>FCF/dips</td>
                            </tr>
                            <tr>
                                <td><input type="time" class="form-control" name="c309" id="c309"></td>
                                <td><input type="text" class="form-control" name="c310" id="c310"></td>
                                <td><input type="text" class="form-control" name="c311" id="c311"></td>
                                <td><input type="text" class="form-control" name="c312" id="c312"></td>
                                <td><input type="text" class="form-control" name="c313" id="c313"></td>
                                <td><input type="text" class="form-control" name="c314" id="c314"></td>
                                <td><input type="text" class="form-control" name="c315" id="c315"></td>
                                <td><input type="text" class="form-control" name="c316" id="c316"></td>
                                <td><input type="text" class="form-control" name="c317" id="c317"></td>
                                <td><input type="text" class="form-control" name="c318" id="c318"></td>
                            </tr>                           
                        </table>
                    </div>
                    <div id="contGrillaTP">
                        <div class="grillaTrabajoParto" style="border: 0;">
                            <table id="gridTrabajoParto"></table>
                        </div>
                    </div>

                </div>

                <div class="divContEnfermedades">
                    <div class="titleEnfermedades">
                        <h6>ENFERMEDADES</h6>
                        <div>
                            <input type="radio" name="c138" id="c138.ninguna" value="ninguna"><label for="">ninguna</label>
                        </div>
                        <div>
                            <input type="radio" name="c138" id="c138.unaOmas" value="unaOmas"><label for="">1 o más</label>
                        </div>
                    </div>
                    <div class="divEnfermedades ">
                        <label for="">HTA previa</label><input type="checkbox" name="c140" id="c140">
                        <label for="">HTA inducida embarazo</label><input type="checkbox" name="c141" id="c141">
                        <label for="">preeclampsia</label><input type="checkbox" name="c142" id="c142">
                        <label for="">eclampsia</label><input type="checkbox" name="c143" id="c143">
                        <label for="">cardiopatía</label><input type="checkbox" name="c144" id="c144">
                        <label for="">nefropatía</label><input type="checkbox" name="c145" id="c145">
                        <label for="">diabetes</label>
                        <select class="form-select" name="c146"  id="c146">
                            <option value=""></option>
                            <option value="I">I</option>
                            <option value="II">II</option>
                            <option value="G">G</option>
                        </select>
                    </div>
                    <div class="divEnfermedades">
                        <label for="">infec. ovular </label><input type="checkbox" name="c147" id="c147">
                        <label for="">infec. urinaria </label><input type="checkbox" name="c148" id="c148">
                        <label for="">amenaza parto preter. </label><input type="checkbox" name="c149" id="c149">
                        <label for="">R.C.I.U </label><input type="checkbox" name="c150" id="c150">
                        <label for="">rotura prem. de membranas </label><input type="checkbox" name="c151" id="c151">
                        <label for="">anemia </label><input type="checkbox" name="c152" id="c152">
                        <label for="">otra cond. grave </label><input type="checkbox" name="c153" id="c153">
                    </div>
                    <div class="divEnfermedades">
                        <div class="divSubEnferm1">
                            <h6>
                                HEMORRAGIA
                            </h6>
                            <label for="">1<sup>er</sup> trim</label> <input type="checkbox" name="c154" id="c154">
                            <label for="">2<sup>do</sup> trim</label> <input type="checkbox" name="c155" id="c155">
                            <label for="">3<sup>er</sup> trim</label> <input type="checkbox" name="c156" id="c156">
                            <label for="">postparto</label> <input type="checkbox" name="c157" id="c157">
                            <label for="">infec. puerperal</label> <input type="checkbox" name="c158" id="c158">
                        </div>
                        <div class="divSubEnferm2">
                            <label for="">código</label>
                            <input type="text" name="c159" id="c159">
                            <input type="text" name="c160" id="c160">
                            <input type="text" name="c161" id="c161">
                        </div>
                        <div class="divSubEnferm3">
                            <textarea class="form-control" name="c162" id="c162" cols="15" rows="4"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="contenedor1">
            <div class="divCont1 divNacimiento">
                <h6>
                    NACIMIENTO
                </h6>
                <label for="">VIVO</label><input type="radio" value="vivo" name="c163" id="c163.vivo">
                <h6 for="">MUERTO</h6>
                <div>
                    <div>
                        <label for="">anteparto</label><input type="radio" value="muertoAnteparto" name="c163" id="c163.muertoAnteparto">
                    </div>
                    <div>
                        <label for="">parto</label><input type="radio" value="muertoParto" name="c163" id="c163.muertoParto">
                    </div>
                    <div>
                        <label for="">ignora momento</label><input type="radio" value="muertoIgnoraMomento" name="c163" id="c163.muertoIgnoraMomento">
                    </div>
                </div>
            </div>
            <div class="divCont1 divDateTime">
                <input class="form-control" type="time" name="c164" id="c164">
                <input class="form-control" type="date" name="c165" id="c165">

            </div>
            <div class="divCont1 divMult">
                <h6>
                    MULTIPLE
                </h6>
                <label for=""> órden</label>
                <input type="checkbox" name="c166" id="c166">
                <input class="form-control" type="text" name="c167" id="c167">
            </div>

            <div class="divCont1 divTermin">
                <h6>
                    TERMINACIÓN
                </h6>

                <table class="table">
                    <tr>
                        <td>
                            <label for="">espont. </label><input type="radio" value="espontanea" name="c168" id="c168.espontanea">
                        </td>
                        <td>
                            <label for="">cesárea </label><input type="radio" value="cesarea" name="c168" id="c168.cesarea">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="">forceps </label><input type="radio" value="forceps" name="c169" id="c169.forceps">
                        </td>
                        <td>
                            <label for="">vacuum </label><input type="radio" value="vacuum" name="c169" id="c169.vacuum">
                        </td>
                        <td>
                            <label for="">otra </label><input type="radio" value="otra" name="c169" id="c169.otra">
                        </td>
                    </tr>

                </table>
            </div>
            <div class="divCont1 divIndicPrincipal">
                <div class="divIndicPrincipal1">
                    <h6>
                        INDICACIÓN PRINCIPAL DE INDUCCIÓN O PARTO OPERATORIO
                    </h6>
                    <input class="form-control" type="text" name="c170" id="c170">
                </div>
                <div class="divIndicPrincipal2">
                    <label for="">código</label>

                    <label for="">INDUC.</label>
                    <label for="">OPER.</label>
                    <input class="form-control" type="text" name="c171" id="c171">
                    <input class="form-control" type="text" name="c172" id="c172">
                </div>
            </div>
        </div>

        <div class="contenedor1">
            <div class="divCont1 divClassParto">
                <h6>
                    POSICIÓN PARTO
                </h6>
                <label for="">sentada</label><input type="radio" value="sentada" name="c173" id="c173.sentada">
                <div>
                    <label for="">acostada</label><input type="radio" value="acostada" name="c173" id="c173.acostada">
                </div>
                <label for="">cuclillas</label><input type="radio" value="cuclillas" name="c173" id="c173.cuclillas">
            </div>
            <div class="divCont1 divEpisiotomia">
                <h6>
                    EPISIOTOMIA
                </h6>
                <input type="checkbox" name="c174" id="c174">
            </div>
            <div class="divCont1 divDesgarros">
                <h6>
                    DESGARROS
                </h6>
                <div>
                    <span>no</span><input type="checkbox" name="c175" id="c175">
                </div>
                <div>
                    <label for=""> Grado (1 a 4) </label>
                    <input type="text" class="form-control" name="c176" id="c176">
                </div>
            </div>
            <div class="divCont1 divOcitocicos">
                <h6>
                    OCITOCICOS
                </h6>
                <div>
                    <label for="">prealumbr.</label><input type="checkbox" name="c177" id="c177">
                </div>
                <div>
                    <label for="">postalumbr.</label><input type="checkbox" name="c178" id="c178">
                </div>
            </div>
            <div class="divCont1 divOcitocicos">
                <h6>
                    PLACENTA
                </h6>
                <div>
                    <label for="">completa</label><input type="checkbox" name="c179" id="c179">
                </div>
                <div>
                    <label for="">retenida</label><input type="checkbox" name="c180" id="c180">
                </div>
            </div>
            <div class="divCont1 divOcitocicos">
                <h6>LIGADURA CORDÓN </h6>
                <label for="">precoz</label>
                <div>
                    <label for="">Si</label><input type="radio" value="si" name="c181" id="c181.si">
                    <label for="">No</label><input type="radio" value="no" name="c181" id="c181.no">
                </div>
            </div>
            <div class="divCont1">
                <h6>MEDICACIÓN RECIBIDA</h6>
                <table class="table table-striped-columns">
                    <tr>
                        <th>ocitócicos en TDP</th>
                        <th>antibiot.</th>
                        <th>analgesia</th>
                        <th>anest. local</th>
                        <th>anest. region</th>
                        <th> anest. gral/th>
                        <th>transfusión</th>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="c182" id="c182"></td>
                        <td><input type="checkbox" name="c183" id="c183"></td>
                        <td><input type="checkbox" name="c184" id="c184"></td>
                        <td><input type="checkbox" name="c185" id="c185"></td>
                        <td><input type="checkbox" name="c186" id="c186"></td>
                        <td><input type="checkbox" name="c187" id="c187"></td>
                        <td><input type="checkbox" name="c188" id="c188"></td>

                    </tr>
                </table>
            </div>
            <div class="divCont1 divOtrosE">
                <div>
                    <label for="">otros</label>
                    <input type="checkbox" name="c189" id="c189">
                    <label for="">especificar</label>
                    <input type="text" class="form-control inpEspecificar" name="c190" id="c190">
                </div>
                <div>
                    <h6 for="">código</h6>
                    <label for="">medic 1</label>
                    <label for="">medic 2</label>
                    <input type="text" class="form-control" name="c191" id="c191">
                    <input type="text" class="form-control" name="c192" id="c192">
                </div>



            </div>
        </div>

    </section>

    <!-- dn -->
    <section>
        <div class="contenedor1 contRN">
            <div class="divCont1 divRecNacido">
                <h6 for="">RECIEN NACIDO</h6>
                <div class="divCont1 divMainRecienN">
                    <div class="divsubRN1">
                        <h6 for="">SEXO</h6>
                        <div>
                            <label for="">f</label>
                            <label for="">m</label>
                            <input type="radio" id="c193.F" name="c193" value="F">
                            <input type="radio" id="c193.M" name="c193" value="M">
                        </div>
                        <div>
                            <label for="" style="white-space: nowrap;padding-top: 15px;">no definido</label>
                            <input type="radio" value="noDefinido" id="c193.noDefinido" name="c193" value="noDefinido">
                        </div>
                    </div>
                    <div class="divsubRN1">
                        <h6 for="">PESO AL NACER</h6>
                        <div class="divPesoNacer">
                            <input type="text" class="form-control" name="c319" id="c319">g
                            <div>
                                <label for=""> &lt;2500g </label> <input type="radio" value="2500g" id="c195.2500g" name="c195">
                                <label for="">≥4000g</label> <input type="radio" id="c195.MayorIgual4000g" name="c195" value="MayorIgual4000g">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divCont1 divCefaLong">
                <div>
                    <label for="" style="white-space: nowrap;">P. CEFALICO cm</label>

                    <input type="text" class="form-control" name="c196" id="c196">
                </div>
                <div>
                    <label for="">LONGITUD cm</label>

                    <input type="text" class="form-control" name="c197" id="c197">
                </div>
            </div>
            <div class="divCont1 divTermin">
                <h6 for="">EDAD GESTACIONAL</h6>
                <table class="table" style="border-color: transparent;">
                    <tr>
                        <td>
                            <label for="">sem.</label>
                            <input type="text" class="form-control" name="c198" id="c198">
                            <label for="">dias</label>
                            <input type="text" class="form-control" name="c199" id="c199">
                        </td>
                        <td>
                            <label for="">FUM</label> <input type="radio" value="fum" id="c200.fum" name="c200">

                            <label for="">ECO</label> <input type="radio" value="eco" id="c200.eco" name="c200">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="">ESTIMADA</label><input style="vertical-align: middle;margin-left:4px ;"
                                type="checkbox" id="c201" name="c201">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="divCont1 divDesgarros">
                <h6 for="" style="white-space: nowrap;">PESO E.G.</h6>
                <label for="">adec</label> <input type="radio" value="adec" id="c202.adec" name="c202" >
                <label for="">peq</label> <input type="radio" value="peq" id="c202.peq" name="c202" >
                <label for="">gde</label> <input type="radio" value="gde" id="c202.gde" name="c202" >
            </div>
            <div class="divCont1 divDesgarros">
                <h6 for="" style="white-space: nowrap;">APGAR (min)</h6>
                <label for="">1<sup>er </sup></label><input type="text" class="form-control" name="c203" id="c203">
                <label for="">5<sup>to </sup></label><input type="text" class="form-control" name="c204" id="c204">
            </div>
            <div class="divCont1 divSubEnferm1">
                <h6 for="">REANIMACION</h6>
                <label for="">estimulacion</label><input type="checkbox" id="c205" name="c205">
                <label for="">aspiracion</label><input type="checkbox" id="c206" name="c206">
                <label for="">mascara</label><input type="checkbox" id="c207" name="c207">
                <label for="">oxigeno</label><input type="checkbox" id="c208" name="c208">
                <label for="">masaje</label><input type="checkbox" id="c209" name="c209">
                <label for="">tubo</label><input type="checkbox" id="c210" name="c210">
            </div>
            <div class="divCont1 divFalleceReferido">
                <div class="divSubEnferm1">
                    <h6 for=""> FALLECE EN LUGAR DE PARTO</h6>
                    <label for="">no</label> <input type="radio" value="no" id="c211.no" name="c211" >
                    <label for="">si</label> <input type="radio" value="si" id="c211.si" name="c211" >
                </div>
                <div class="divSubEnferm1">
                    <h6 for="">REFERIDO</h6>
                    <label for="">aloj. conj.</label><input type="radio" value="aloj" id="c212.aloj" name="c212">
                    <label for="">neona. tolog.</label><input type="radio" value="neona" id="c212.neona" name="c212">
                    <label for="">otro hosp</label><input type="radio" value="otro" id="c212.otro" name="c212">
                </div>
            </div>
            <div class="divCont1 divAtendido">
                <div class="divTermin">
                    <table class="table table-striped-columns">
                        <tr>
                            <td>ATENDIDO</td>
                            <td>medico</td>
                            <td>obst</td>
                            <td>enf</td>
                            <td>auxil</td>
                            <td>estud</td>
                            <td>empir</td>
                            <td>otro</td>
                            <td>Nombre</td>
                        </tr>
                        <tr>
                            <td>PARTO</td>
                            <td><input type="checkbox" id="c213" name="c213"> </td>
                            <td><input type="checkbox" id="c214" name="c214"> </td>
                            <td><input type="checkbox" id="c215" name="c215"> </td>
                            <td><input type="checkbox" id="c216" name="c216"> </td>
                            <td><input type="checkbox" id="c217" name="c217"> </td>
                            <td><input type="checkbox" id="c218" name="c218"> </td>
                            <td><input type="checkbox" id="c219" name="c219"> </td>
                            <td><input type="text" style="width: 120px;" class="form-control" name="c220" id="c220"></td>
                        </tr>
                        <tr>
                            <td>NEONATO</td>
                            <td><input type="checkbox" id="c221" name="c221"> </td>
                            <td><input type="checkbox" id="c222" name="c222"> </td>
                            <td><input type="checkbox" id="c223" name="c223"> </td>
                            <td><input type="checkbox" id="c224" name="c224"> </td>
                            <td><input type="checkbox" id="c225" name="c225"> </td>
                            <td><input type="checkbox" id="c226" name="c226"> </td>
                            <td><input type="checkbox" id="c227" name="c227"> </td>
                            <td><input type="text" style="width: 120px;" class="form-control" name="c228" id="c228"></td>
                        </tr>
                    </table>
                </div>


                <div class="divTermin">
                    <div class="titleEnfermedades">
                        <h6 for="">PUERPERIO</h6>
                    </div>
                    <table class="table">
                        <tr>
                            <td>dia</td>
                            <td>hora</td>
                            <td>TC</td>
                            <td>PA</td>
                            <td>pulso</td>
                            <td>invol. uter</td>
                            <td>loquios</td>
                        </tr>
                        <tr>
                            <td><input type="date" class="form-control" name="c229" id="c229"></td>
                            <td><input type="time" class="form-control" name="c230" id="c230"></td>
                            <td><input type="text" class="form-control" name="c231" id="c231"></td>
                            <td><input type="text" class="form-control" name="c232" id="c232"></td>
                            <td><input type="text" class="form-control" name="c233" id="c233"></td>
                            <td><input type="text" class="form-control" name="c234" id="c234"></td>
                            <td><input type="text" class="form-control" name="c235" id="c235"></td>
                        </tr>                        
                    </table>
                    <div class="grillaPuerperio" style="display: flex; justify-content: center;">
                        <table id="gridPuerperio"></table>
                    </div>
                </div>
            </div>
        </div>

        <div class="contenedor1">
            <div class="divCont1 divDefectos">
                <h6 for="">DEFECTOS CONGENITOS</h6>
                <table class="table">
                    <tr>
                        <td>
                            <input type="radio" id="c244.no" name="c244" value="no"> <label
                                for="">no</label>
                            <input type="radio" id="c244.menor" name="c244" value="menor"> <label
                                for="">menor</label>
                            <input type="radio" id="c244.mayor" name="c244" value="mayor"> <label
                                for="">mayor</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div>
                                <input type="text" class="form-control" name="c245" id="c245"> <label for="">codigo</label>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="divCont1 divClsEnfermedades">
                <h6 for="">ENFERMEDADES</h6>
                <div>
                    <label for="">ninguna</label><input type="radio" id="c246.ninguna" name="c246" value="ninguna">
                    <label for="">1 o mas</label><input type="radio" id="c246.1omas" name="c246" value="1omas">
                </div>
                <div>
                    <table class="table">

                        <tr>
                            <td><input type="text" class="form-control" name="c247" id="c247"></td>
                            <td></td>
                            <td><input type="text" class="form-control" name="c248" id="c248"></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="form-control" name="c249" id="c249"></td>
                            <td></td>
                            <td><input type="text" class="form-control" name="c250" id="c250"></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="form-control" name="c251" id="c251"></td>
                            <td class="tdDeg">
                                <label for="">código</label>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="c252" id="c252">
                            </td>
                        </tr>

                    </table>

                </div>
            </div>
            <div class="divCont1">
                <div class="divTamizajeNeonatal ">
                    <h6 for="">TAMIZAJE NEONATAL</h6>
                    <div class="tableNeo">
                        <table class="table">
                            <tr>
                                <th>VDRL</th>
                                <th>Tto</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="radio" id="c253.no" name="c253" value="no"><label for="">no</label></td>
                            </tr>
                            <tr>
                                <td><label style="font-size: 20px;" for="">-</label><input type="radio" id="c254.negativoVdrl" name="c254" value="negativoVdrl"></td>
                                <td><input type="radio" id="c253.si" name="c253" value="si">si<label for=""></label></td>
                            </tr>
                            <tr>
                                <td><label style="font-size: 20px;" for="">+</label><input type="radio" id="c254.positivoVdrl" name="c254" value="positivoVdrl"></td>
                                <td><input type="radio" id="c253.n/c" name="c253" value="n/c"><label for="">n/c</label></td>
                            </tr>
                            <tr>
                                <td><label for="">no se hizo</label><input type="radio" id="c254.no_se_hizo" name="c254" value="no_se_hizo"></td>
                                <td><input type="radio" id="c253.s/d" name="c253" value="s/d"><label for="">s/d</label></td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <table class="table table-striped-columns">
                            <tr>
                                <th></th>
                                <th>TSH</th>
                                <th>Hbpatia</th>
                                <th>Bilirub</th>
                                <th>Toxo IgM</th>
                            </tr>
                            <tr>
                                <td><label for="">-</label></td>
                                <td><input type="radio" id="c259.neonatalMenos" name="c259" value="neonatalMenos"></td>
                                <td><input type="radio" id="c260.neonatalMenos" name="c260" value="neonatalMenos"></td>
                                <td><input type="radio" id="c261.neonatalMenos" name="c261" value="neonatalMenos"></td>
                                <td><input type="radio" id="c262.neonatalMenos" name="c262" value="neonatalMenos"></td>
                            </tr>
                            <tr>
                                <td><label for="">+</label></td>
                                <td><input type="radio" id="c259.neonatalMas" name="c259" value="neonatalMas"></td>
                                <td><input type="radio" id="c260.neonatalMas" name="c260" value="neonatalMas"></td>
                                <td><input type="radio" id="c261.neonatalMas" name="c261" value="neonatalMas"></td>
                                <td><input type="radio" id="c262.neonatalMas" name="c262" value="neonatalMas"></td>
                            </tr>
                            <tr>
                                <td><label for="">-no se hizo</label></td>
                                <td><input type="radio" id="c259.neonatalNoSeHizo" name="c259" value="neonatalNoSeHizo"></td>
                                <td><input type="radio" id="c260.neonatalNoSeHizo" name="c260" value="neonatalNoSeHizo"></td>
                                <td><input type="radio" id="c261.neonatalNoSeHizo" name="c261" value="neonatalNoSeHizo"></td>
                                <td><input type="radio" id="c262.neonatalNoSeHizo" name="c262" value="neonatalNoSeHizo"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="divCont1 divSubEnferm1 divMenciono">
                <h6 for="">Meconio 1<sup>er</sup> dia</h6>
                <label for="">no</label><input type="radio" id="c263.no" name="c263" value="no">
                <label for="">si</label><input type="radio" id="c263.si" name="c263" value="si">
            </div>

            <div class="divCont1 divAntirubeolaGlobulina">
                <div class="divSubEnferm1">
                    <h6 for="">Antirubeola post parto</h6>
                    <label for="">no</label><input type="radio" id="c264.no" name="c264" value="no">
                    <label for="">si</label><input type="radio" id="c264.si" name="c264" value="si">
                    <label for="">n/c</label><input type="radio" id="c264.n/c" name="c264" value="n/c">
                </div>

                <div class="divSubEnferm1">
                    <h6 for="">yglobulina anti D</h6>
                    <label for="">no</label><input type="radio" id="c265.no" name="c265" value="no">
                    <label for="">si</label><input type="radio" id="c265.si" name="c265" value="si">
                    <label for="">n/c</label><input type="radio" id="c265.n/c" name="c265" value="n/c">
                </div>
            </div>

        </div>


    </section>




    <section>
        <div class="contenedor1">

            <div class="divCont1 divContEgrRN">
                <div class="titleEgresoRN ">
                    <h6 for="">EGRESO RN</h6>
                </div>
                <div class="divContEgrRN1">
                    <label for="">vivo</label><input type="radio" id="c266.vivo" name="c266" value="vivo">
                    <label for="">fallece</label><input type="radio" id="c266.muerto" name="c266" value="muerto">
                    <!-- <input type="datetime-local" class="form-control" name="" id=""> -->
                    <div class="divDateTime">
                        <input type="date" class="form-control" name="c268" id="c268">
                        <input type="time" class="form-control" name="c320" id="c320">
                    </div>
                    
                </div>

                <div class="divContEgrRN2">
                    <table class="table">
                        <tr>
                            <th>traslado</th>
                            <th>fallece durante o en lugar de traslado</th>
                            <th>EDAD AL EGRESO o dias completos</th>
                        </tr>
                        <tr>
                            <td> <input type="checkbox" id="c270" name="c270"> </td>
                            <td><input type="checkbox" id="c271" name="c271"> </td>
                            <td>
                                <input type="text" class="form-control" name="c272" id="c272">
                                <label for="">
                                    &lt; 1 dia</label><input type="checkbox" id="c273" name="c273">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <label for="">lugar</label><input type="text" class="form-control" name="c274" id="c274">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="divContEgrRN3">
                    <h6 for="">Alimento al Alta</h6>
                    <label for="">lact. exci.</label><input type="radio" id="c275.lact" name="c275" value="lact">
                    <label for="">parcial</label><input type="radio" id="c275.parcial" name="c275" value="parcial">
                    <label for="">artificial</label><input type="radio" id="c275.artificial" name="c275" value="artificial">
                </div>
                <div class="divContEgrRN4">
                    <div>
                        <label for="">Boca arriba</label>
                        <input type="checkbox" id="c276" name="c276">
                    </div>
                    <div>
                        <label for="">BCG</label>
                        <input type="checkbox" id="c277" name="c277">
                    </div>
                    <div>
                        <label for="">Peso al Egreso</label>
                        <input type="text" class="form-control" name="c278" id="c278">
                    </div>
                </div>


            </div>




            <div class="divCont1 divEgreYAnticoncep">
                <div class="divEgresoMaterno">
                    <div class="titleEgresoRN">
                        <h6 for="">EGRESO MATERNO</h6>
                    </div>

                    <table class="table ">
                        <tr>
                            <td><input type="date" class="form-control" name="c279" id="c279"></td>
                            <td>
                                <label for="">traslado</label><input type="checkbox" id="c280" name="c280">
                            </td>
                            <td>
                                <label for="">Lugar</label>
                                <input type="text" class="form-control" name="c281" id="c281">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="">viva</label><input type="radio" id="c282.viva" name="c282" value="viva">
                                <label for="">fallece</label><input type="radio" id="c282.fallece" name="c282" value="fallece">
                            </td>
                            <td>
                                <label for="">fallece durante o en lugar de traslado</label>
                                <input type="checkbox" id="c283" name="c283">
                            </td>
                            <td>
                                <label for="">dias completos desde el parto</label>
                                <input type="text" class="form-control" name="c284" id="c284">
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="divAntiConcepcion">
                    <div class="titleEgresoRN">
                        <h6 for="">ANTICONCEPCION</h6>
                    </div>
                    <div class="divContAntiConcepcion">
                        <label for="">CONSEJERIA</label> <input type="checkbox" id="c285" name="c285">
                    </div>
                    <table class="table table-striped-columns">
                        <tr>
                            <th colspan="2">METODO ELEGIDO</th>
                        </tr>
                        <tr>
                            <td>
                                <label for="">DIU post-evento</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.diuPost" name="c286" value="diuPost">
                            </td>
                            <td>
                                <label for="">ligadura tubaria</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.ligadura" name="c286" value="ligadura">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="">DIU</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.diu" name="c286" value="diu">
                            </td>
                            <td>
                                <label for="">natural</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.natural" name="c286" value="natural">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="">barrera</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.barrera" name="c286" value="barrera">
                            </td>
                            <td>
                                <label for="">otro</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.otro" name="c286" value="otro">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="">hormaonal</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.hormaonal" name="c286" value="hormaonal">
                            </td>
                            <td>
                                <label for="">ninguno</label>
                            </td>
                            <td>
                                <input type="radio" id="c286.ninguna" name="c286" value="ninguna">
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>

        <div class="divResponsables">
            <div>
                <label for="">id. RN</label>
                <input class="form-control" type="text" name="c269" id="c269">
                <label for="">Nombre recien nacido</label>
                <input type="text" class="form-control" name="c294" id="c294">
                <label for="">Responsable</label>
                <input type="text" class="form-control" name="c295" id="c295">
            </div>
            <div>
                <label for="">Responsable</label>
                <input type="text" class="form-control" name="c296" id="c296">
            </div>
        </div>
    </section>



    <script>

        var id_folio = '';
        
        var selectList = document.querySelectorAll('select')
        selectList.forEach(function (select) {
            var selectId = select.id;
            select.addEventListener("change", () => {
                selectOnChange(selectId);
            });
        });

        function selectOnChange(selectId) {
            var selectedValue = document.getElementById(selectId).value;
            var selectName = document.getElementById(selectId).name;
            var selectedText = document.getElementById(selectId).options[document.getElementById(selectId).selectedIndex].text;

            console.log("Valor seleccionado: " + selectedValue);
            console.log("Texto seleccionado: " + selectedText);
            console.log("id seleccionado: " + selectId);
            console.log("id seleccionado: " + selectName);

            //window.opener.modificarCRUD('xxx')
            window.opener.guardarDatosPlantilla('c1ga', selectName, selectedValue)

        }
        /*  */
        var checkboxList = document.querySelectorAll("input[type='checkbox']");
        checkboxList.forEach(function (checkbox) {
            var checkboxId = checkbox.id;

            checkbox.addEventListener("change", function () {
                checkboxOnChange(checkboxId);
                console.log('ss')
            });
        });
        function checkboxOnChange(checkboxId) {
            var checkbox = document.getElementById(checkboxId);
            var checkboxName = document.getElementById(checkboxId).name;
            var isChecked = checkbox.checked;

            console.log("ID del checkbox: " + checkboxId);
            console.log("Estado x: " + checkboxName);
            console.log("Estado seleccionado: " + isChecked);
            //alert(checkboxId,"-+-",isChecked)
            window.opener.guardarDatosPlantilla('c1ga', checkboxName, isChecked == true ? 'SI' : 'NO')

        }
        /**/

        var radioList = document.querySelectorAll("input[type='radio']");
        radioList.forEach(function (radio) {
            var radioId = radio.id;

            radio.addEventListener("change", function () {
                radioOnChange(radioId);
            });
        });
        function radioOnChange(radioId) {
            var radio = document.getElementById(radioId);
            var radioName = document.getElementById(radioId).name;
            var isChecked = radio.checked;
            var radioVal = radio.value;

            console.log("ID" + radioId);
            console.log("name " + radioName);
            console.log("Estado seleccionado: " + isChecked);
            //alert(checkboxId,"-+-",isChecked)
            window.opener.guardarDatosPlantilla('c1ga', radioName, radioVal)

        }

        /*text  */

        var textList = document.querySelectorAll("input[type='text']");
        textList.forEach(function (text) {
            var textId = text.id;

            text.addEventListener("blur", function () {
                textOnblur(textId);
                console.log('ccc')
            });
        });
         /*textArea  */

        var textAreaList = document.querySelectorAll("textarea");
        textAreaList.forEach(function (text) {
            var textAreaId = text.id;

            text.addEventListener("blur", function () {
                textOnblur(textAreaId);
                console.log('textAreaId',textAreaId)
            });
        });

        /* TIME */
        var time = document.querySelectorAll("input[type='time']");
        time.forEach(function (text) {
            var textTimeId = text.id;

            text.addEventListener("blur", function () {
                textOnblur(textTimeId);
            });
        });

        /* date */
        var textList = document.querySelectorAll("input[type='date']");
        textList.forEach(function (text) {
            var textId = text.id;

            text.addEventListener("blur", function () {
                textOnblur(textId);
                console.log('ccc')
            });
        });
        function textOnblur(textId) {
            var inputText = document.getElementById(textId);
            var inputName = document.getElementById(textId).name;
            var inputVal = inputText.value;

            console.log("ID" + textId);
            console.log("name " + inputName);
            console.log("val: " + inputVal);
            //alert(checkboxId,"-+-",isChecked)
            window.opener.guardarDatosPlantilla('c1ga', inputName, inputVal)

        }

        /* ------------------------------------------ */

        function validarNumero1(id, numero, maxEnteros, maxDecimales) {
            // Expresión regular que valida un número con 2 enteros y 1 decimal
            //const regex = /^[0-9]{1,2}(\.[0-9]{1,1})?$/;
            console.log(maxDecimales, 'maxDecimales1')
            //const regex = new RegExp()
            if (maxDecimales == 0) {
                // console.log(maxDecimales,'maxDecimales1')
                var regex = new RegExp(`^[0-9]{1,${maxEnteros}}?$`);
            } else {
                // console.log(maxDecimales,'maxDecimales2')
                var regex = new RegExp(`^[0-9]{1,${maxEnteros}}(\\.[0-9]{1,${maxDecimales}})?$`);
            }


            console.log(maxEnteros, maxDecimales, '----')
            // Comprobar si el número cumple con la expresión regular
            if (regex.test(numero)) {
                // console.log(numero,'valido')
                return true;

            } else {
                mensaje = `este campo recibe ${maxEnteros} numeros enteros y ${maxDecimales} numero decimales`
                //window.opener.Swal.fire(mensaje)
                // console.log(numero,'invalido')
                alert(mensaje)
                document.getElementById(id).value = ''
                return false;
            }
        }


        /*  */
        
        $(document).ready(function () {
            var dataRecibida
            var data
            var myGrid = $('#listaConsultasAntenatales').find("#" + "listaConsultasAntenatales")
            window.addEventListener('message', function(event) {
                console.log(event.data);
                console.log(event);
                dataRecibida = event.data;
                //console.log('Datos recibidos:', data.getElementsByTagName('cell'));
                //alert(data)
                data = dataRecibida[0];
                data.push({})

                console.log(dataRecibida[1]);
                console.log(dataRecibida[2],'dataAÑÑ');
                console.log(dataRecibida[2][0],'mp' , JSON.stringify(dataRecibida[2][0]));

                id_folio = dataRecibida[1].id_evolucion

                $("#listaConsultasAntenatales").jqGrid({
                    datatype: "local", // Indicar que los datos son locales
                    data: data, // Asignar los datos al grid
                    colModel: [
                        { name: "C1", label: "C", hidden:true },
                        { name: "C2", label: "ID FOLIO"},
                        { name: "C3", label: "TIPO", hidden: true },
                        { name: "C4", label: "IDPACIENTE", hidden: true },
                        { name: "C5", label: "FECHA", autoResizable: true },
                        { name: "C6", label: "EDAD GESTACIONAL", autoResizable: true },
                        { name: "C7", label: "PESO", autoResizable: true },
                        { name: "C8", label: "PA", autoResizable: true },
                        { name: "C9", label: "ALTURA UTERINA", autoResizable: true },
                        { name: "C10", label: "PRESENTACION", autoResizable: true },
                        { name: "C11", label: "FCF(ipm)", autoResizable: true },
                        { name: "C12", label: "MOV. FETALES", autoResizable: true },
                        { name: "C13", label: "PROTEINURIA", autoResizable: true },
                        { name: "C14", label: "SIG.ALARMA", autoResizable: true },
                        { name: "C15", label: "INICIALES_TECNICO", autoResizable: true },
                        { name: "C16", label: "PROXIMA_CITA", autoResizable: true },                       
                    ],
                    onSelectRow: function (rowid) {
                        var datosRow = $('#listaConsultasAntenatales').getRowData(rowid);
                        //var idFolio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO');
                        if (dataRecibida[1].id_evolucion == datosRow.ID_FOLIO) {
                            alert('ok')
                            /* document.getElementById("antenateales-date").value = datosRow.FECHA
                            document.getElementById("antenateales-edadg").value = datosRow.EDAD_GESTACIONAL
                            document.getElementById("antenateales-peso").value = datosRow.PESO
                            document.getElementById("antenateales-presion-sist").value = datosRow.PRESION_SISTOLICA
                            document.getElementById("antenateales-presion-diast").value = datosRow.PRESION_DIASTOLICA
                            document.getElementById("antenateales-altura-uterina").value = datosRow.ALTURA_UTERINA
                            document.getElementById("antenateales-presetacion").value = datosRow.PRESENTACION
                            document.getElementById("antenateales-frecuencia-card").value = datosRow.FRECUENCIA_CARDIACA
                            document.getElementById('antenateales-hierro').checked = datosRow.HIERRO
                            document.getElementById('antenateales-calcio').checked = datosRow.CALCIO
                            document.getElementById('antenateales-formo').checked = datosRow.FORMO
                            document.getElementById("antenateales-estado-nutr").value = datosRow.ESTADO_NUTRICIONAL
                            document.getElementById("antenateales-signos-ET").value = datosRow.SIGNOS_ALARMA_EXAMENES_Y_TRATAMIENTO
                            document.getElementById("antenateales-prodecional").value = datosRow.NOMBRE_PROFECIONAL
                            document.getElementById("antenateales-date-prox-control").value = datosRow.FECHA_PROXIMO_CONTROL */
                        } else {
                            alert('pertenece a otro folio')
                        }
                    },
                    caption: "Historico Consultas Antenatales",
                    width: "1600",
                    
                    // Resto de la configuración del jqGrid...
                });

                /* grilla gridTrabajoParto */
                
                var datosGrillaAnteParto = dataRecibida[3]
                datosGrillaAnteParto.push({})
                console.log(datosGrillaAnteParto,'datosGrillaAnteParto');



                /* datosGrillaAnteParto = [
                    { Celda1: 1, Celda2: "John Doe", edad: 25, fechaNacimiento: "" },
                    { Celda1: 2, Celda2: "Jane Smith", edad: 30, fechaNacimiento: "" },
                    { Celda1: 3, Celda2: "Jane Smith", edad: 30, fechaNacimiento: "" },
                    { },
                 ]; */

                $("#gridTrabajoParto").jqGrid({
                    datatype: "local", // Indicar que los datos son locales
                    data: datosGrillaAnteParto, // Asignar los datos al grid
                    colModel: [
                        { name: "C1", label: "C", autoResizable: true,hidden:true },
                        { name: "C2", label: "ID_FOLIO",autoResizable: true,hidden:true },
                        { name: "C3", label: "TIPO",autoResizable: true,hidden:true },
                        { name: "C4", label: "IDPACIENTE",autoResizable: true,hidden:true  },
                        { name: "C5", label: "HORA", autoResizable: true },
                        { name: "C6", label: "POSICIÓN MADRE", autoResizable: true },
                        { name: "C7", label: "PA", autoResizable: true },
                        { name: "C8", label: "PULSO", autoResizable: true },
                        { name: "C9", label: "CONTROL/10", autoResizable: true },                                 
                        { name: "C10", label: "DILATACIÓN", autoResizable: true },
                        { name: "C11", label: "ALTURA PRESETNE", autoResizable: true },
                        { name: "C12", label: "VARIEDAD POSIC.", autoResizable: true },
                        { name: "C13", label: "MECONIO", autoResizable: true },
                        { name: "C14", label: "FCF/dips", autoResizable: true },
                    ],
                    onSelectRow: function (rowid) {
                        var datosRow = $('#listaConsultasAntenatales').getRowData(rowid);


                        //var idFolio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO');
                        if (dataRecibida[1].id_evolucion == datosRow.ID_FOLIO) {
                            alert('ok')
                        } else {
                            alert('pertenece a otro folio')
                        }




                    },
                    caption: "RESULTADOS EXTERNOS",
                    width: "900",
                    
                    // Resto de la configuración del jqGrid...
                });


                /*  */

                /* grilla gridPuerperio */
                
                var datosGrillaPuerperio = dataRecibida[4]
                datosGrillaPuerperio.push({})


                /* datosGrillaPuerperio = [
                    { Celda1: 1, Celda2: "John Doe", edad: 25, fechaNacimiento: "" },
                    { Celda1: 2, Celda2: "Jane Smith", edad: 30, fechaNacimiento: "" },
                    { Celda1: 3, Celda2: "Jane Smith", edad: 30, fechaNacimiento: "" },
                    { },
                ]; */

                $("#gridPuerperio").jqGrid({
                    datatype: "local", // Indicar que los datos son locales
                    data: datosGrillaPuerperio, // Asignar los datos al grid
                    colModel: [
                        { name: "C1", label: "C", autoResizable: true, hidden:true },
                        { name: "C2", label: "ID_FOLIO", hidden:true},
                        { name: "C3", label: "TIPO", autoResizable: true, hidden:true  },
                        { name: "C4", label: "IDPACIENTE", autoResizable: true, hidden:true },
                        { name: "C5", label: "DÍA", autoResizable: true },
                        { name: "C6", label: "HORA", autoResizable: true },
                        { name: "C7", label: "TC", autoResizable: true },                                               
                        { name: "C8", label: "PA", autoResizable: true },                                               
                        { name: "C9", label: "PULSO", autoResizable: true },                                               
                        { name: "C10", label: "INVOL. UTER", autoResizable: true },                                               
                        { name: "C11", label: "LOQUIOS", autoResizable: true },                                               

                    ],
                    onSelectRow: function (rowid) {
                        var datosRow = $('#listaConsultasAntenatales').getRowData(rowid);


                        //var idFolio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO');
                        if (dataRecibida[1].id_evolucion == datosRow.ID_FOLIO) {
                            alert('ok')
                        } else {
                            alert('pertenece a otro folio')
                        }




                    },
                    caption: "RESULTADOS EXTERNOS",
                    width: "500",
                    
                    // Resto de la configuración del jqGrid...
                });


                /*  */


                console.log();
                // URL del servicio PHP
                var url = "http://localhost/clap.php";
                const parametro = id_folio;
        
                // Configurar la solicitud Fetch
                fetch('http://10.0.0.3:9010/clap?idEvolucion=' + encodeURIComponent(parametro))
                    .then(response => response.json())
                    .then(data => {
                        // Aquí puedes procesar los datos recibidos del servicio PHP
                        console.log(data);
                        const objeto = data[0];
                        let lista = []
        
                        for (const clave in objeto) {
                            if(objeto.hasOwnProperty(clave)){
                                const valor = objeto[clave];
                                const input = document.getElementById(clave)
                                const inputRadio = document.getElementById(clave+"."+valor)
                                const textArea = document.getElementById(clave)
                                lista.push(clave+"."+valor)
        
                                if (input) {                            
                                    if (input.type === 'checkbox') {                            
                                        if (valor === 'SI') {
                                            input.checked = true                                    
                                        }
                                    }else{
                                        input.value = valor
                                    }
                                    
                                }else if (inputRadio) {
                                    if (inputRadio.type === 'radio') {
                                        console.log(inputRadio,'inputRadioF');
                                        inputRadio.checked = true                                 
                                    }
                                }else if (textArea) {
                                    textArea.value= valor                            
                                }
                            }                                                          
                        }
                        console.log(lista,'inputRadio');
        
        
                    })
                    .catch(error => {
                        // Manejo de errores
                        console.error('Error:', error);
                    });
               
            });


            /*  */

           /*  var data = [
                { Celda1: 1, Celda2: "John Doe", edad: 25, fechaNacimiento: "" },
                { Celda1: 2, Celda2: "Jane Smith", edad: 30, fechaNacimiento: "" },
                { Celda1: 3, Celda2: "Jane Smith", edad: 30, fechaNacimiento: "" },
                { },
                // ... otros datos
            ]; */

           
        });



    </script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
        integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"
        integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS"
        crossorigin="anonymous"></script>
</body>

</html>
