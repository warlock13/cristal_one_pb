<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.ArrayList"%>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<!DOCTYPE HTML >  
<%
    Connection conexion = iConnection.getConnection();
    ResultSet rs = null;

    ArrayList cars = new ArrayList();
    ArrayList cars2 = new ArrayList();

    String modo = request.getParameter("modo");
    String id_parametro = request.getParameter("id_parametro");
    String id_sexo = request.getParameter("id_sexo");
    String rango = request.getParameter("rango");
    String rango2 = request.getParameter("rango2");
    String edad = request.getParameter("edade");

    try {

        Statement stmt = conexion.createStatement();
        String SQL = "";

        SQL = "SELECT "
                + "  pa.nombre nom_plantilla, "
                + "  pa.tabla "
                + "  FROM  "
                + "  formulario.plantilla pa "
                + "  INNER JOIN "
                + "  formulario.encuesta_plantilla ep "
                + "  ON pa.id = ep.id_plantilla "
                + "  WHERE ep.id_encuesta = " + id_parametro
                + "  AND (pa.sexo = 'A' or pa.sexo = '" + id_sexo + "') AND (rango_edad = 0 or rango_edad = " + rango + ") AND (" + edad + " BETWEEN pa.edad_inicial AND pa.edad_final) ORDER BY ep.orden";

        rs = stmt.executeQuery(SQL);

        while (rs.next()) {
            System.out.println("tabla=" + rs.getString("tabla"));
            cars.add(rs.getString("tabla"));
            cars2.add(rs.getString("nom_plantilla"));
        }
        stmt.close();
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }

    String vari = "";
%> 

<div id="tabsContenidoEncuestaPaciente" style="width:98%; height:100%">
    <input type="text" id="auxFocusEncuesta" style="width: 0px; height: 0px; position: absolute;z-index: -100;"> <!-- IMPORTANTE NO BORRAR! Para que el ultimo elemento seleccionado de la plantilla detecte onblur al cambiar de tab -->
    <ul>
        <%
            for (int i = 0; i < cars.size(); i++) {
                if (i == 0) {
        %>            
        <label id="lblIdPrimerTab" hidden > <%=cars.get(i)%></label>
        <label id="lblTabActual" hidden></label>
        <label id="lblTabActualModo" hidden></label>
        <%}%>             

        <li><a style="font-size: 12px;" href='#<%=cars.get(i)%>' title='<%=cars.get(i)%>' name='<%= modo%>' onclick="tabsSubContenidoEncuesta(this.title, this.name);" ><center> <%=cars2.get(i)%> </center></a></li>              
                    <%
                        }
                    %>        
    </ul> 


    <%
        for (int i = 0; i < cars.size(); i++) {
            vari = "";

            vari = "div_" + cars.get(i);
            System.out.print(vari + "-- ");
    %>  
    <div id = '<%=cars.get(i)%>' style="width:99%"> 
        <div id="loadertabsContenidoEncuesta" style="display: flex; justify-content: center; align-items: center; height: 200px; background-color: white;" hidden>
            <table>
              <tr>
                <td align="center"><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="60px" height="60px"></td>
              </tr>
              <tr>
                <td><label style="font-size: small; font-style: italic;">Cargando plantilla. Por favor espere...</label></td>
              </tr>
            </table>
        </div>
        <table width="100%" border="3px">
            <tr class="titulos">
                <td  width="80%"> 
                    <div  id = "<%= vari%>" ></div>     
                </td>
            </tr>
        </table>                
    </div> 

    <%

        }
    %>   
</div>  

<%
    try { 
        rs.close();
    } catch (Exception e) {
    }
%>