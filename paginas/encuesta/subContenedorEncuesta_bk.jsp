Subcontenedero encuesta

<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<!DOCTYPE HTML > 
<style type="text/css">
    #div1 {
        overflow:scroll;
        height:300px;
        width:100%;
    }
    #div1 table {
        width:100%;
    }

    .misbordes {
        border-right: solid #a6c9e2 1px;
        border-bottom: solid #a6c9e2 1px;
        height:22px;
        font-size: 11px;
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    }
    .paratrplantilla{
        background-color: white;
        font-size: 11px;
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    }

</style> 
<%
    Connection conexion = iConnection.getConnection();
    Conexion cn = new Conexion(conexion);

    ControlAdmin beanAdmin = new ControlAdmin();
    beanAdmin.setCn(cn);

    ArrayList resultaux = new ArrayList();
    ArrayList resulCombo = new ArrayList();

    String var = request.getParameter("tipo");
    String vari = request.getParameter("id_parametro");
    String idencuestaPaciente = request.getParameter("id_encuestaPaciente");
    String id_sexo = request.getParameter("id_sexo");
    String rango = request.getParameter("rango");
    String rango2 = request.getParameter("rango2");
    String edad = request.getParameter("edade");
    String bloquear = "";
    int contador = 0;

    try {

        Statement stmt = conexion.createStatement();
        String SQL = " ";

        SQL = " SELECT "
                + " pd.orden, "
                + " pd.id_plantilla, "
                + " pd.descripcion, "
                + " pd.referencia, "
                + " pd.id_jerarquia, "
                + " pd.id_padre_orden, "
                + " pd.resultado_default, "
                + " pd.tipo_input, "
                + " pd.id_query_combo, "
                + " pd.id_query_ventanita, "
                + " pd.cantdecimales, "
                + " pd.ancho, "
                + " pd.maxlenght, " 
                + " pd.id_formula,"
                + " pd.campo_bloquea,"
                + " p.nombre, "
                + " p.id_tipo, "
                + " p.tabla, "
                + " epa.id_estado_encuesta, "
                + " pfo.formula, "
                + " pfo.referencia_resultado, "
                + " COALESCE(formulario.fn_datos_encuesta(pd.referencia, p.tabla, epa.id::VARCHAR), '') dato, "
                + " p.td1_ancho , "
                + " p.td2_ancho, "
                + " (formulario.fn_numero_formulas_referencia_encuesta(e.id::varchar,p.tabla,pd.referencia)) formulas, "
                + " (formulario.fn_validaciones_referencia(p.tabla,pd.referencia)) validaciones"
                + " FROM "
                + " formulario.plantilla_detalle pd "
                + " INNER JOIN formulario.plantilla p ON pd.id_plantilla = p.id "
                + " INNER JOIN formulario.encuesta_plantilla ep ON p.id = ep.id_plantilla "
                + " INNER JOIN formulario.encuesta e ON ep.id_encuesta = e.id "
                + " INNER JOIN formulario.encuesta_paciente epa ON epa.id_encuesta = e.id "
                + " LEFT JOIN formulario.plantilla_formula pfo ON pd.id_formula = pfo.id"
                + " WHERE ep.id_encuesta = " + vari + " AND p.tabla = '" + var + "' AND pd.vigente='S' AND epa.id= " + idencuestaPaciente
                + " AND (pd.sexo = 'A' or pd.sexo ='" + id_sexo + "') AND (pd.rango_edad = 0 or pd.rango_edad = " + rango + ") AND (pd.rango_edad_2 = 0 or pd.rango_edad_2 = " + rango2
                + " ) AND (" + edad + " BETWEEN pd.edad_inicial AND pd.edad_final) ORDER BY pd.id_plantilla, pd.orden ";

        System.out.println("se ejecutoSQL: " + SQL);

        ResultSet rs = stmt.executeQuery(SQL);

%>
<div id ="div1" style="background-color: white;" >
    <table width="100%" cellpadding="0" cellspacing="0">
        <!-- <tbody width="100%" class="inputBlanco"> --> 
        <%    while (rs.next()) {
                if (rs.getString("id_estado_encuesta").trim().equals("A")) {
                    bloquear = " ";
                } else {
                    bloquear = "disabled";
                }

                String validacion = "";

                if (rs.getString("validaciones") != null) {
                    validacion = "validarCampoEncuesta(this.id,'" + rs.getString("validaciones") + "');";
                }

                String actualizar = "";

                if (rs.getInt("formulas") > 0) {
                    actualizar = "actualizarCampoFormulaEncuesta(this.id, this.value);";
                }

                if (rs.getString("id_jerarquia").equals("TI")) {%>
        <tr id="cargando" style="padding: 30px; margin: 50px 450px; display: none;"></tr>
        <tr class="paratrplantilla overs" align="left"> 
            <td class="misbordes" style="border-left:solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' > <h3><%=rs.getString("descripcion")%></h3></td> 
            <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > </td> 
        </tr> 
        <%} else if (rs.getString("id_jerarquia").equals("PA")) {%>
        <tr class="paratrplantilla overs" align="left"> 
            <td class="misbordes" style="border-left:solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' > <strong><%=rs.getString("descripcion")%></strong></td> 
            <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > </td> 
        </tr> 
        <%} else {%>
        <tr class="paratrplantilla overs" align="left"> 

            <td class="misbordes" style="border-left:solid #a6c9e2 1px ;" width='<%=rs.getString("td1_ancho")%>' title='<%= rs.getString("referencia")%>'> <%=rs.getString("descripcion")%> </td> 
            <td class="misbordes" width='<%=rs.getString("td2_ancho")%>' > 
                <% if (rs.getString("tipo_input").trim().equals("input")) {

                        if (rs.getInt("id_formula") > 0) {%> 
                <input type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width: <%=rs.getString("ancho")%>; ' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%> ' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur='guardarDatosEncuesta(this.name, '<%= rs.getString("referencia")%>', this.value );' <%= bloquear%> 
                       />

                <input type="hidden" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>

                <input type="hidden" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <%
                } else if (rs.getInt("id_formula") < 1) {%>
                <input type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width: <%=rs.getString("ancho")%>; ' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur="guardarDatosEncuesta(this.name, '<%= rs.getString("referencia")%>', this.value)" <%= bloquear%> /> 

                <% }
                } else if (rs.getString("tipo_input").trim().equals("numeros")) {
                    if (rs.getInt("id_formula") > 0) {%> 
                <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' onkeypress="javascript:return soloTelefono(event);" value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur="guardarDatosEncuesta(this.name, '<%= rs.getString("referencia")%>', this.value);" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>  
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="text" maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= var%>' onkeypress="javascript:return soloTelefono(event);" value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur="guardarDatosEncuesta(this.name, '<%= rs.getString("referencia")%>', this.value);" <%= bloquear%> />
                <%}
                } else if (rs.getString("tipo_input").trim().equals("decimal")) {
                    System.out.println("decimal");
                    if (rs.getInt("id_formula") > 0) {%> 
                <input  type="number" step='<%=rs.getString("cantdecimales")%>' maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheckEn(event, this.id, '<%=rs.getString("cantdecimales")%>');" name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur="guardarDatosEncuesta(this.name, '<%= rs.getString("referencia")%>', this.value);" <%= bloquear%> />
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input  type="number" step='<%=rs.getString("cantdecimales")%>' maxlength='<%=rs.getString("maxlenght")%>' style='width:<%=rs.getString("ancho")%>; height:17px; border:1px solid #4297d7;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' onkeypress="javascript:return NumCheckEn(event, this.id, '<%=rs.getString("cantdecimales")%>');" name='<%= var%>' value='<%= rs.getString("dato")%>' onkeyup="<%= actualizar%>" onblur="guardarDatosEncuesta(this.name, '<%= rs.getString("referencia")%>', this.value);" <%= bloquear%> />
                <%}
                } else if (rs.getString("tipo_input").trim().equals("check")) {
                    String estado = "";
                    if (rs.getString("dato").equals("SI")) {
                        estado = "checked";
                    }
                    if (rs.getInt("id_formula") > 0) {%>
                <input type="checkbox" name='<%= var%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);<%= actualizar%><%= validacion%>" <%= estado%>  <%= bloquear%>  >
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <input type="checkbox" name='<%= var%>'id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);guardarDatosEncuesta('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= estado%>  <%= bloquear%>>  
                <% } %>
                <%
                } else if (rs.getString("tipo_input").trim().equals("switchsn")) {
                    String estado = "";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String activarFormula = "";
                    String bloquear_aux = "";
                    if (rs.getString("dato").equals("SI") || rs.getString("dato").equals("NO")) {
                        class_span1 = "onoffswitch-inner2";
                        class_span2 = "onoffswitch-switch2";
                        activar = "";
                        activarFormula = "";
                        if (rs.getString("dato").equals("SI")) {
                            estado = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getString("id_estado_encuesta").trim().equals("A")) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitchsnE('" + var + "', '" + rs.getString("referencia") + "')";
                            activarFormula = "setTimeout(()=> {activarFormulaSwitch('" + var + "', '" + rs.getString("referencia") + "','SN')},300)";
                        }
                    }

                    if (rs.getString("campo_bloquea") == null || rs.getString("campo_bloquea").equals("")) {
                        cam_bloquear = " ";
                    } else {
                        parametros = rs.getString("campo_bloquea");
                        cam_bloquear = "bloquearMisCampos('" + parametros + "','" + var + "', this.checked);";
                    }
                %> 

                <div class="onoffswitch2" onclick="<%= activar%>;<%=activarFormula%>">
                    <% if (rs.getInt("id_formula") > 0) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);<%= actualizar%><%= validacion%><%=cam_bloquear%>" <%= estado%>  <%= bloquear%>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'> 
                    <% } else if (rs.getInt("id_formula") < 1) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitchsn(this.id, this.checked);guardarDatosEncuesta('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%><%=cam_bloquear%>" <%= estado%>  <%= bloquear%>>  
                    <% }%>
                    <label class="onoffswitch-label2" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span class="<%= class_span1%>" id='<%= var%>-<%= rs.getString("referencia")%>-span1'></span>
                        <span class="<%= class_span2%>" id='<%= var%>-<%= rs.getString("referencia")%>-span2'></span>
                    </label>
                </div>
                <%
                    bloquear = bloquear_aux;
                } else if (rs.getString("tipo_input").trim().equals("switchS10")) {
                    String estado1 = "";
                    if (rs.getString("dato").equals("1")) {
                        estado1 = "checked";
                    } else {
                        estado1 = " ";
                    }
                %> 

                <div class="onoffswitch2">
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox2" id='<%= var%>,<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id, this.checked);guardarDatosEncuesta('<%= var%>', '<%= rs.getString("referencia")%>', this.value)" <%= estado1%> <%= bloquear%> >
                    <label class="onoffswitch-label2" for='<%= var%>,<%= rs.getString("referencia")%>'>
                        <span class="onoffswitch-inner2"></span>
                        <span class="onoffswitch-switch2"></span>
                    </label>
                </div>

                <%
                } else if (rs.getString("tipo_input").trim().equals("switch10")) {
                    System.out.println("####" + rs.getString("referencia"));

                    String estado1 = " ";
                    String cam_bloquear = "";
                    String parametros = "";
                    String class_span1 = "";
                    String class_span2 = "";
                    String activar = "";
                    String activarFormula = "";
                    String bloquear_aux = "";

                    if (rs.getString("dato").equals("1") || rs.getString("dato").equals("0")) {
                        class_span1 = "onoffswitch-inner3";
                        class_span2 = "onoffswitch-switch3";
                        activar = "";
                        activarFormula = "";

                        if (rs.getString("dato").equals("1")) {
                            estado1 = "checked";
                        }
                    } else {
                        class_span1 = "onoffswitch-inner2-disabled";
                        class_span2 = "onoffswitch-switch2-disabled";
                        if (rs.getString("id_estado_encuesta").trim().equals("A")) {
                            bloquear_aux = bloquear;
                            bloquear = "disabled";
                            activar = "activarSwitch10E('" + var + "', '" + rs.getString("referencia") + "')";
                            activarFormula = "setTimeout(()=> {activarFormulaSwitch('" + var + "', '" + rs.getString("referencia") + "','SN')},300)";
                        }
                    }
                %>  


                <div class="onoffswitch3" onclick="<%= activar%>;<%=activarFormula%>">
                    <% if (rs.getInt("id_formula") > 0) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox3" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id, this.checked);<%= actualizar%><%= validacion%>" <%= estado1%>  <%= bloquear%>  >
                    <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                    <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                    <% } else if (rs.getInt("id_formula") < 1) {%>
                    <input type="checkbox" name='<%= var%>' class="onoffswitch-checkbox3" id='<%= "formulario." + var + "." + rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onclick="valorSwitch10(this.id, this.checked);guardarDatosEncuesta('<%= var%>', '<%= rs.getString("referencia")%>', this.value);<%= actualizar%><%= validacion%>" <%= estado1%>  <%= bloquear%>  >
                    <% }%>
                    <label class="onoffswitch-label3" for='<%= "formulario." + var + "." + rs.getString("referencia")%>'>
                        <span class="<%= class_span1%>" id='<%= var%>-<%= rs.getString("referencia")%>-span1'></span>
                        <span class="<%= class_span2%>" id='<%= var%>-<%= rs.getString("referencia")%>-span2'></span>
                    </label>
                </div>

                <%
                    bloquear = bloquear_aux;
                } else if (rs.getString("tipo_input").trim().equals("texta")) {%>
                <textarea maxlength='<%=rs.getString("maxlenght")%>' style='width: <%=rs.getString("ancho")%>; text-align: left;' type="text" title='<%= rs.getString("referencia")%>' id='<%= var%>' name='<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onblur='guardarDatosEncuesta(this.id, this.name, this.value)' <%=bloquear%> ><%= rs.getString("dato")%></textarea> 

                <%
                } else if (rs.getString("tipo_input").trim().equals("inputFecha")) {
                %>
                <input maxlength='<%=rs.getString("maxlenght")%>' style='width: <%=rs.getString("ancho")%>; text-align: left;' type="date" title='<%= rs.getString("referencia")%>' id='<%= var%>' name='<%= rs.getString("referencia")%>' value='<%= rs.getString("dato")%>' onblur='guardarDatosEncuesta(this.id, this.name, this.value)' <%=bloquear%> > 
                <%
                } else if (rs.getString("tipo_input").trim().equals("combo")) {
                    if (rs.getInt("id_formula") > 0) {%>
                <select size="1"  style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= rs.getString("referencia")%>' value="" onchange="guardarDatosEncuesta('<%= var%>', this.name, this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> > 
                    <option value=""></option>
                    <%  resulCombo.clear();
                        resulCombo = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(rs.getString("id_query_combo")));
                        ComboVO cmb;
                        for (int i = 0; i < resulCombo.size(); i++) {
                            cmb = (ComboVO) resulCombo.get(i);
                            if (rs.getString("dato").trim().equals(cmb.getId())) {
                    %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                    <%} else {
                    %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                    <%
                            }
                        }%>   
                </select> 
                <input type="hidden" class="campoFormula" id="campoFormula<%=rs.getInt("id_formula")%>" value='<%=rs.getString("formula")%>'>
                <input type="hidden" name="<%= var%>" id="campoResultado<%=rs.getInt("id_formula")%>" value='<%=rs.getString("referencia_resultado")%>'>
                <% } else if (rs.getInt("id_formula") < 1) {%>
                <select size="1"  style='width: <%=rs.getString("ancho")%>; height: 20px; border: 1px solid #4297d7; border-radius: 5px; background-color: white;' title='<%= rs.getString("referencia")%>' id='<%= "formulario." + var + "." + rs.getString("referencia")%>' name='<%= rs.getString("referencia")%>' value="" onchange="guardarDatosEncuesta('<%= var%>', this.name, this.value);<%= actualizar%><%= validacion%>" <%= bloquear%> >
                    <option value=""></option>
                    <%  resulCombo.clear();
                        resulCombo = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(rs.getString("id_query_combo")));
                        ComboVO cmb;
                        for (int i = 0; i < resulCombo.size(); i++) {
                            cmb = (ComboVO) resulCombo.get(i);
                            if (rs.getString("dato").trim().equals(cmb.getId())) {
                    %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                    <%} else {
                    %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                    <%
                            }
                        }%>   
                </select> 
                <% }
                    }%>

                <%
                    if (rs.getString("tipo_input").trim().equals("button")) {
                %>
                <div style="text-align: center;"> <input type="button" value="GRAFICA" onclick="verImagen()" class="small button blue" <%= bloquear%> > </div> 
                    <%
                        }%>

                        <img class='<%= var + "-" + rs.getString("referencia") + "-loader"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
                        <img onclick="obtenerValorPlantilla(this, '<%= var%>', '<%= rs.getString("referencia")%>');" class='<%= var + "-" + rs.getString("referencia") + "-error"%>' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
            </td> 
        </tr>
        <% }
                //stmt.close();
                //rs.close(); 
            }%>
        <!-- </tbody> --> 
        <%} catch (Exception e) {
                System.out.println(e.getMessage());
            }%>

    </table> 

</div>