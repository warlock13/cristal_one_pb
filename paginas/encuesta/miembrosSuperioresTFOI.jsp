<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/> 

<style type="text/css">
  #div2 {
     overflow:scroll;
     height:300px;
     width:100%;
  }
  #div2 table {
    width:100%;
  }
  
  .misbordes {
      border-right: solid #a6c9e2 1px;
      border-bottom: solid #a6c9e2 5px ;
      height:22px;
      font-size: 11px;
      font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
  }
    
    .paratrplantilla{
      background-color: white;
      font-size: 11px;
      font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    }
  
  </style>

<%
    Connection conexion = iConnection.getConnection();
    Conexion cn = new Conexion(conexion);

    ControlAdmin beanAdmin = new ControlAdmin();
    beanAdmin.setCn(cn); 

ArrayList resulCombo = new ArrayList();

ComboVO cmb;

String id_evolucion = request.getParameter("id_evolucion");

String cbHmFlexorDer = "";
String cbHmFlexorIz = "";
String cbHmExtensorDer = "";
String cbHmExtensorIz = "";
String cbHmAductoresDer = "";
String cbHmAductoresIz = "";
String cbHmAbeductorDer = "";
String cbHmAbeductorIz = "";
String cbHmRotadorInternoDer = "";
String cbHmRotadorInternoIz = "";
String cbHmRotadorExternoDer = "";
String cbHmRotadorExternoIz = "";
String cbCdFlexorDer = "";
String cbCdFlexorIz = "";
String cbCdExtensorDer = "";
String cbCdExtensorIz = "";
String cbCodoPronadoresIz = "";
String cbCodoPronadoresDer = "";
String cbCodoSupinadoresIz = "";
String cbCodoSupinadoresDer = "";
String cbMnPronadoresDer = "";
String cbMnPronadoresIz = "";
String cbMnSupinadorDer = "";
String cbMnSupinadorIz = "";
String cbMnFlexorDer = "";
String cbMnFlexorIz = "";
String cbMnExtensorDer = "";
String cbMnExtensorIz = "";
String cbDedosManoFlexoresDer = "";
String cbDedosManoFlexoresIz = "";
String cbDedosManoExtensoresDer = "";
String cbDedosManoExtensoresIz = "";
String cbDedosManoAductoresDer = "";
String cbDedosManoAductoresIz = "";
String cbDedosManoAbeductoresDer = "";
String cbDedosManoAbeductoresIz = "";
String cbPlgAbductorDer = "";
String cbPlgAbductorIz = "";
String cbPlgAductorDer = "";
String cbPlgAductorIz = "";
String cbPlgOponenteDer = "";
String cbPlgOponenteIz = "";
String cbPlgExtensorLargoDer = "";
String cbPlgExtensorLargoIz = "";
String cbPlgExtensorCortoDer = "";
String cbPlgExtensorCortoIz = "";
String cbPlgFlexorLargoDer = "";
String cbPlgFlexorLargoIz = "";
String cbPlgFlexorCortoDer = "";
String cbPlgFlexorCortoIz = "";
String cbCadFlexoresDer = "";
String cbCadFlexoresIz = "";
String cbCadExtensoresDer = "";
String cbCadExtensoresIz = "";
String cbCadAductoresDer = "";
String cbCadAductoresIZ = "";
String cbCadAbeductoresDer = "";
String cbCadAbeductoresIz = "";
String cbCadRotadoresInternosDer = "";
String cbCadRotadoresInternosIz = "";
String cbCadRotadoresExternosDer = "";
String cbCadRotadoresExternosDerIz = "";
String cbRodillaFlexorDerecho = "";
String cbRodillaFlexorIz = "";
String cbRodillaExtensorDer = "";
String cbRodillaExtensorIz = "";
String cbTobilloPlantiflexorDer = "";
String cbTobilloPlantiflexorIz = "";
String cbTobilloDorsiFleDer = "";
String cbTobilloDorsiFleIz = "";
String cbPieInvertoresDer = "";
String cbPieInvertoresIz = "";
String cbPieEvertoresDer = "";
String cbPieEvertoresIz = "";
String cbDedosPieFlexoresDer = "";
String cbDedosPieFlexoresIz = "";
String cbDedosPieExtensoresDer = "";
String cbDedosPieExtensoresIz = "";
String cbDedosPieAductoresDer = "";
String cbDedosPieAductoresIz = "";
String cbDedosPieAbeductoresDer = "";
String cbDedosPieAbeductoresIz = "";

String flexionHombroIz = "";
String flexionHombroDer = "";
String extensionHombroIz = "";
String extensionHombroDer = "";
String abducionHombroIz = "";
String abducionHombroDer = "";
String aduccionHombroIz = "";
String aduccionHombroDer = "";
String rotacionInternaHombroIz = "";
String rotacionInternaHombroDer = "";
String rotacionExternaHombroIz = "";
String rotacionExternaHombroDer = "";
String flexionCodoIz = "";
String flexionCodoDer = "";
String extensionCodoIz = "";
String extensionCodoDer = "";
String pronacionCodoIz = "";
String pronacionCodoDer = "";
String supinacionCodoIz = "";
String supinacionCodoDer = "";
String flexionMunecaIz = "";
String flexionMunecaDer = "";
String extensionMunecaIz = "";
String extensionMunecaDer = "";
String desviacionRadialIz = "";
String desviacionRadialDer = "";
String desviacionCubitalDer = "";
String desviacionCubitalIz = "";
String abducionPulgarIz = "";
String abducionPulgarDer = "";
String aduccionPulgarIz = "";
String aduccionPulgarDer = "";
String oposicionPulgarIz = "";
String oposicionPulgarDer = "";
String pulgarFlexionMCFIz = "";
String pulgarFlexionMCFDer = "";
String pulgarExtensionMCFIz = "";
String pulgarExtensionMCFDer = "";
String pulgarFlexionIFIz = "";
String pulgarFlexionIFDer = "";
String pulgarExtensionIFIz = "";
String pulgarExtensionIFDer = "";
String metacaporfalangiaFlexionIz = "";
String metacaporfalangiaFlexionDer = "";
String metacaporfalangiaExtensionIz = "";
String metacaporfalangiaExtensionDer = "";
String interfalangiaFlexiProxIz = "";
String interfalangiaFlexiProxDer = "";
String interfalangiaFlexiDistalIz = "";
String interfalangiaFlexiDistalDer = "";
String interfalangiaAbduccionIz = "";
String interfalangiaAbduccionDer = "";
String interfalangiaAduccionIz = "";
String interfalangiaAduccionDer = "";
String interfalangiaExtensionProxIz = "";
String interfalangiaExtensionProxDer = "";
String interfalangiaExtensionDisIz = "";
String interfalangiaExtensionDisDer = "";
String cbDedosManoMetacarpoFlexionIz = "";
String cbDedosManoMetacarpoFlexionDer = "";
String cbDedosManoMetacarpoExtensionIz = "";
String cbDedosManoMetacarpoExtensionDer = "";
String cbDedosManoFlexionDistalIz = "";
String cbDedosManoFlexionDistalDer = "";
String cbDedosManoExtensionDistalIz = "";
String cbDedosManoExtensionDistalDer = "";

try{

Statement stmt = conexion.createStatement();
String SQL = " ";

    SQL = "select " +
    "coalesce(vrl5.c1, '')  as cbHmFlexorDer, "+  
    "coalesce(vrl5.c2, '')  as cbHmFlexorIz, "+ 
    "coalesce(vrl5.c3, '')  as cbHmExtensorDer, "+  
    "coalesce(vrl5.c4, '')  as cbHmExtensorIz, "+ 
    "coalesce(vrl5.c5, '')  as cbHmAductoresDer, "+ 
    "coalesce(vrl5.c6, '')  as cbHmAductoresIz, "+
    "coalesce(vrl5.c7, '')  as cbHmAbeductorDer, "+ 
    "coalesce(vrl5.c8, '')  as cbHmAbeductorIz, "+
    "coalesce(vrl5.c9, '')  as cbHmRotadorInternoDer, "+ 
    "coalesce(vrl5.c10, '') as cbHmRotadorInternoIz, "+ 
    "coalesce(vrl5.c11, '') as cbHmRotadorExternoDer, "+
    "coalesce(vrl5.c12, '') as cbHmRotadorExternoIz, "+ 
    "coalesce(vrl5.c13, '') as cbCdFlexorDer, "+
    "coalesce(vrl5.c14, '') as cbCdFlexorIz, "+
    "coalesce(vrl5.c15, '') as cbCdExtensorDer, "+
    "coalesce(vrl5.c16, '') as cbCdExtensorIz, "+
    "coalesce(vrl5.c79, '') as cbCodoPronadoresIz, "+
    "coalesce(vrl5.c80, '') as cbCodoPronadoresDer, "+    
    "coalesce(vrl5.c81, '') as cbCodoSupinadoresIz, "+
    "coalesce(vrl5.c82, '') as cbCodoSupinadoresDer, "+
    "coalesce(vrl5.c17, '') as cbMnPronadoresDer, "+
    "coalesce(vrl5.c18, '') as cbMnPronadoresIz, "+
    "coalesce(vrl5.c19, '') as cbMnSupinadorDer, "+
    "coalesce(vrl5.c20, '') as cbMnSupinadorIz, "+
    "coalesce(vrl5.c21, '') as cbMnFlexorDer, "+
    "coalesce(vrl5.c22, '') as cbMnFlexorIz, "+
    "coalesce(vrl5.c23, '') as cbMnExtensorDer, "+
    "coalesce(vrl5.c24, '') as cbMnExtensorIz, "+
    "coalesce(vrl5.c25, '') as cbDedosManoFlexoresDer, "+
    "coalesce(vrl5.c26, '') as cbDedosManoFlexoresIz, "+
    "coalesce(vrl5.c27, '') as cbDedosManoExtensoresDer, "+
    "coalesce(vrl5.c28, '') as cbDedosManoExtensoresIz, "+
    "coalesce(vrl5.c29, '') as cbDedosManoAductoresDer, "+
    "coalesce(vrl5.c30, '') as cbDedosManoAductoresIz, "+
    "coalesce(vrl5.c31, '') as cbDedosManoAbeductoresDer, "+
    "coalesce(vrl5.c32, '') as cbDedosManoAbeductoresIz, "+
    "coalesce(vrl5.c83, '') as cbDedosManoMetacarpoFlexionIz, "+
    "coalesce(vrl5.c84, '') as cbDedosManoMetacarpoFlexionDer, "+
    "coalesce(vrl5.c85, '') as cbDedosManoMetacarpoExtensionIz, "+
    "coalesce(vrl5.c86, '') as cbDedosManoMetacarpoExtensionDer, "+
    "coalesce(vrl5.c87, '') as cbDedosManoFlexionDistalIz, "+
    "coalesce(vrl5.c88, '') as cbDedosManoFlexionDistalDer, "+
    "coalesce(vrl5.c89, '') as cbDedosManoExtensionDistalIz, "+
    "coalesce(vrl5.c90, '') as cbDedosManoExtensionDistalDer, "+
    "coalesce(vrl5.c33, '') as cbPlgAbductorDer, "+
    "coalesce(vrl5.c34, '') as cbPlgAbductorIz, "+
    "coalesce(vrl5.c35, '') as cbPlgAductorDer, "+
    "coalesce(vrl5.c36, '') as cbPlgAductorIz, "+
    "coalesce(vrl5.c37, '') as cbPlgOponenteDer, "+
    "coalesce(vrl5.c38, '') as cbPlgOponenteIz, "+
    "coalesce(vrl5.c39, '') as cbPlgExtensorLargoDer, "+
    "coalesce(vrl5.c40, '') as cbPlgExtensorLargoIz, "+
    "coalesce(vrl5.c41, '') as cbPlgExtensorCortoDer, "+
    "coalesce(vrl5.c42, '') as cbPlgExtensorCortoIz, "+
    "coalesce(vrl5.c43, '') as cbPlgFlexorLargoDer, "+
    "coalesce(vrl5.c44, '') as cbPlgFlexorLargoIz, "+
    "coalesce(vrl5.c45, '') as cbPlgFlexorCortoDer, "+
    "coalesce(vrl5.c46, '') as cbPlgFlexorCortoIz, "+
    "coalesce(vrl5.c47, '') as cbCadFlexoresDer, "+
    "coalesce(vrl5.c48, '') as cbCadFlexoresIz, "+
    "coalesce(vrl5.c49, '') as cbCadExtensoresDer, "+
    "coalesce(vrl5.c50, '') as cbCadExtensoresIz, "+
    "coalesce(vrl5.c51, '') as cbCadAductoresDer, "+
    "coalesce(vrl5.c52, '') as cbCadAductoresIZ, "+
    "coalesce(vrl5.c53, '') as cbCadAbeductoresDer, "+
    "coalesce(vrl5.c54, '') as cbCadAbeductoresIz, "+
    "coalesce(vrl5.c55, '') as cbCadRotadoresInternosDer, "+
    "coalesce(vrl5.c56, '') as cbCadRotadoresInternosIz, "+
    "coalesce(vrl5.c57, '') as cbCadRotadoresExternosDer, "+
    "coalesce(vrl5.c58, '') as cbCadRotadoresExternosDerIz, "+
    "coalesce(vrl5.c59, '') as cbRodillaFlexorDerecho, "+
    "coalesce(vrl5.c60, '') as cbRodillaFlexorIz, "+
    "coalesce(vrl5.c61, '') as cbRodillaExtensorDer, "+
    "coalesce(vrl5.c62, '') as cbRodillaExtensorIz, "+
    "coalesce(vrl5.c63, '') as cbTobilloPlantiflexorDer, "+
    "coalesce(vrl5.c64, '') as cbTobilloPlantiflexorIz, "+
    "coalesce(vrl5.c65, '') as cbTobilloDorsiFleDer, "+
    "coalesce(vrl5.c66, '') as cbTobilloDorsiFleIz, "+
    "coalesce(vrl5.c67, '') as cbPieInvertoresDer, "+
    "coalesce(vrl5.c68, '') as cbPieInvertoresIz, "+
    "coalesce(vrl5.c69, '') as cbPieEvertoresDer, "+
    "coalesce(vrl5.c70, '') as cbPieEvertoresIz, "+
    "coalesce(vrl5.c71, '') as cbDedosPieFlexoresDer, "+
    "coalesce(vrl5.c72, '') as cbDedosPieFlexoresIz, "+
    "coalesce(vrl5.c73, '') as cbDedosPieExtensoresDer, "+
    "coalesce(vrl5.c74, '') as cbDedosPieExtensoresIz, "+
    "coalesce(vrl5.c75, '') as cbDedosPieAductoresDer, "+
    "coalesce(vrl5.c76, '') as cbDedosPieAductoresIz, "+
    "coalesce(vrl5.c77, '') as cbDedosPieAbeductoresDer, "+
    "coalesce(vrl5.c78, '') as cbDedosPieAbeductoresIz, "+
    "coalesce(vrl2.c1, '')  as flexionHombroIz, "+ 
    "coalesce(vrl2.c2, '')  as flexionHombroDer, "+
    "coalesce(vrl2.c3, '')  as extensionHombroIz, "+
    "coalesce(vrl2.c4, '')  as extensionHombroDer, "+
    "coalesce(vrl2.c5, '')  as abducionHombroIz, "+
    "coalesce(vrl2.c6, '')  as abducionHombroDer, "+
    "coalesce(vrl2.c7, '')  as aduccionHombroIz, "+
    "coalesce(vrl2.c8, '')  as aduccionHombroDer, "+
    "coalesce(vrl2.c9, '')  as rotacionInternaHombroIz, "+
    "coalesce(vrl2.c10, '') as rotacionInternaHombroDer, "+
    "coalesce(vrl2.c11, '') as rotacionExternaHombroIz, "+
    "coalesce(vrl2.c12, '') as rotacionExternaHombroDer, "+
    "coalesce(vrl2.c13, '') as flexionCodoIz, "+
    "coalesce(vrl2.c14, '') as flexionCodoDer, "+
    "coalesce(vrl2.c15, '') as extensionCodoIz, "+
    "coalesce(vrl2.c16, '') as extensionCodoDer, "+
    "coalesce(vrl2.c17, '') as pronacionCodoIz, "+ 
    "coalesce(vrl2.c18, '') as pronacionCodoDer, "+
    "coalesce(vrl2.c19, '') as supinacionCodoIz, "+ 
    "coalesce(vrl2.c20, '') as supinacionCodoDer, "+
    "coalesce(vrl2.c21, '') as flexionMunecaIz, "+
    "coalesce(vrl2.c22, '') as flexionMunecaDer, "+
    "coalesce(vrl2.c23, '') as extensionMunecaIz, "+
    "coalesce(vrl2.c24, '') as extensionMunecaDer, "+
    "coalesce(vrl2.c25, '') as desviacionRadialIz, "+
    "coalesce(vrl2.c26, '') as desviacionRadialDer, "+
    "coalesce(vrl2.c27, '') as desviacionCubitalDer, "+ 
    "coalesce(vrl2.c28, '') as desviacionCubitalIz, "+
    "coalesce(vrl2.c29, '') as abducionPulgarIz, "+
    "coalesce(vrl2.c30, '') as abducionPulgarDer, "+
    "coalesce(vrl2.c31, '') as aduccionPulgarIz, "+
    "coalesce(vrl2.c32, '') as aduccionPulgarDer, "+
    "coalesce(vrl2.c33, '') as oposicionPulgarIz, "+
    "coalesce(vrl2.c34, '') as oposicionPulgarDer, "+
    "coalesce(vrl2.c51, '') as pulgarFlexionMCFIz, "+
    "coalesce(vrl2.c52, '') as pulgarFlexionMCFDer, "+
    "coalesce(vrl2.c53, '') as pulgarExtensionMCFIz, "+
    "coalesce(vrl2.c54, '') as pulgarExtensionMCFDer, "+
    "coalesce(vrl2.c55, '') as pulgarFlexionIFIz, "+
    "coalesce(vrl2.c56, '') as pulgarFlexionIFDer, "+
    "coalesce(vrl2.c57, '') as pulgarExtensionIFIz, "+
    "coalesce(vrl2.c58, '') as pulgarExtensionIFDer, "+
    "coalesce(vrl2.c35, '') as metacaporfalangiaFlexionIz, "+
    "coalesce(vrl2.c36, '') as metacaporfalangiaFlexionDer, "+
    "coalesce(vrl2.c37, '') as metacaporfalangiaExtensionIz, "+
    "coalesce(vrl2.c38, '') as metacaporfalangiaExtensionDer, "+
    "coalesce(vrl2.c39, '') as interfalangiaFlexiProxIz, "+
    "coalesce(vrl2.c40, '') as interfalangiaFlexiProxDer, "+
    "coalesce(vrl2.c41, '') as interfalangiaFlexiDistalIz, "+
    "coalesce(vrl2.c42, '') as interfalangiaFlexiDistalDer, "+
    "coalesce(vrl2.c43, '') as interfalangiaAbduccionIz, "+
    "coalesce(vrl2.c44, '') as interfalangiaAbduccionDer, "+
    "coalesce(vrl2.c45, '') as interfalangiaAduccionIz, "+
    "coalesce(vrl2.c46, '') as interfalangiaAduccionDer, "+
    "coalesce(vrl2.c47, '') as interfalangiaExtensionProxIz, "+
    "coalesce(vrl2.c48, '') as interfalangiaExtensionProxDer, "+
    "coalesce(vrl2.c49, '') as interfalangiaExtensionDisIz, "+
    "coalesce(vrl2.c50, '') as interfalangiaExtensionDisDer "+
    "from hc.evolucion as hc "+ 
    "left join formulario.vrl5 as vrl5 "+    
    "on hc.id = vrl5.id_evolucion "+
    "left join formulario.vrl2 as vrl2 "+
    "on hc.id = vrl2.id_evolucion "+
    "where hc.id = "+ id_evolucion;

    System.out.println(SQL);

  ResultSet rs = stmt.executeQuery(SQL);
  while(rs.next()){
    
    cbHmFlexorDer = rs.getString("cbHmFlexorDer");
    cbHmFlexorIz = rs.getString("cbHmFlexorIz");
    cbHmExtensorDer = rs.getString("cbHmExtensorDer");
    cbHmExtensorIz = rs.getString("cbHmExtensorIz");
    cbHmAductoresDer = rs.getString("cbHmAductoresDer");
    cbHmAductoresIz = rs.getString("cbHmAductoresIz");
    cbHmAbeductorDer = rs.getString("cbHmAbeductorDer");
    cbHmAbeductorIz = rs.getString("cbHmAbeductorIz");
    cbHmRotadorInternoDer = rs.getString("cbHmRotadorInternoDer");
    cbHmRotadorInternoIz = rs.getString("cbHmRotadorInternoIz");
    cbHmRotadorExternoDer = rs.getString("cbHmRotadorExternoDer");
    cbHmRotadorExternoIz = rs.getString("cbHmRotadorExternoIz");
    cbCdFlexorDer = rs.getString("cbCdFlexorDer");
    cbCdFlexorIz = rs.getString("cbCdFlexorIz");
    cbCdExtensorDer = rs.getString("cbCdExtensorDer");
    cbCdExtensorIz = rs.getString("cbCdExtensorIz");
    cbCodoPronadoresIz = rs.getString("cbCodoPronadoresIz");
    cbCodoPronadoresDer = rs.getString("cbCodoPronadoresDer");
    cbCodoSupinadoresIz = rs.getString("cbCodoSupinadoresIz");
    cbCodoSupinadoresDer = rs.getString("cbCodoSupinadoresDer");
    cbMnPronadoresDer = rs.getString("cbMnPronadoresDer");
    cbMnPronadoresIz = rs.getString("cbMnPronadoresIz");
    cbMnSupinadorDer = rs.getString("cbMnSupinadorDer");
    cbMnSupinadorIz = rs.getString("cbMnSupinadorIz");
    cbMnFlexorDer = rs.getString("cbMnFlexorDer");
    cbMnFlexorIz = rs.getString("cbMnFlexorIz");
    cbMnExtensorDer = rs.getString("cbMnExtensorDer");
    cbMnExtensorIz = rs.getString("cbMnExtensorIz");
    cbDedosManoFlexoresDer = rs.getString("cbDedosManoFlexoresDer");
    cbDedosManoFlexoresIz = rs.getString("cbDedosManoFlexoresIz");
    cbDedosManoExtensoresDer = rs.getString("cbDedosManoExtensoresDer");
    cbDedosManoExtensoresIz = rs.getString("cbDedosManoExtensoresIz");
    cbDedosManoAductoresDer = rs.getString("cbDedosManoAductoresDer");
    cbDedosManoAductoresIz = rs.getString("cbDedosManoAductoresIz");
    cbDedosManoAbeductoresDer = rs.getString("cbDedosManoAbeductoresDer");
    cbDedosManoAbeductoresIz = rs.getString("cbDedosManoAbeductoresIz");
    cbDedosManoMetacarpoFlexionIz = rs.getString("cbDedosManoMetacarpoFlexionIz");
    cbDedosManoMetacarpoFlexionDer = rs.getString("cbDedosManoMetacarpoFlexionDer");
    cbDedosManoMetacarpoExtensionIz = rs.getString("cbDedosManoMetacarpoExtensionIz");
    cbDedosManoMetacarpoExtensionDer = rs.getString("cbDedosManoMetacarpoExtensionDer");
    cbDedosManoFlexionDistalIz = rs.getString("cbDedosManoFlexionDistalIz");
    cbDedosManoFlexionDistalDer = rs.getString("cbDedosManoFlexionDistalDer");
    cbDedosManoExtensionDistalIz = rs.getString("cbDedosManoExtensionDistalIz");
    cbDedosManoExtensionDistalDer = rs.getString("cbDedosManoExtensionDistalDer");
    cbPlgAbductorDer = rs.getString("cbPlgAbductorDer");
    cbPlgAbductorIz = rs.getString("cbPlgAbductorIz");
    cbPlgAductorDer = rs.getString("cbPlgAductorDer");
    cbPlgAductorIz = rs.getString("cbPlgAductorIz");
    cbPlgOponenteDer = rs.getString("cbPlgOponenteDer");
    cbPlgOponenteIz = rs.getString("cbPlgOponenteIz");
    cbPlgExtensorLargoDer = rs.getString("cbPlgExtensorLargoDer");
    cbPlgExtensorLargoIz = rs.getString("cbPlgExtensorLargoIz");
    cbPlgExtensorCortoDer = rs.getString("cbPlgExtensorCortoDer");
    cbPlgExtensorCortoIz = rs.getString("cbPlgExtensorCortoIz");
    cbPlgFlexorLargoDer = rs.getString("cbPlgFlexorLargoDer");
    cbPlgFlexorLargoIz = rs.getString("cbPlgFlexorLargoIz");
    cbPlgFlexorCortoDer = rs.getString("cbPlgFlexorCortoDer");
    cbPlgFlexorCortoIz = rs.getString("cbPlgFlexorCortoIz");
    cbCadFlexoresDer = rs.getString("cbCadFlexoresDer");
    cbCadFlexoresIz = rs.getString("cbCadFlexoresIz");
    cbCadExtensoresDer = rs.getString("cbCadExtensoresDer");
    cbCadExtensoresIz = rs.getString("cbCadExtensoresIz");
    cbCadAductoresDer = rs.getString("cbCadAductoresDer");
    cbCadAductoresIZ = rs.getString("cbCadAductoresIZ");
    cbCadAbeductoresDer = rs.getString("cbCadAbeductoresDer");
    cbCadAbeductoresIz = rs.getString("cbCadAbeductoresIz");
    cbCadRotadoresInternosDer = rs.getString("cbCadRotadoresInternosDer");
    cbCadRotadoresInternosIz = rs.getString("cbCadRotadoresInternosIz");
    cbCadRotadoresExternosDer = rs.getString("cbCadRotadoresExternosDer");
    cbCadRotadoresExternosDerIz = rs.getString("cbCadRotadoresExternosDerIz");
    cbRodillaFlexorDerecho = rs.getString("cbRodillaFlexorDerecho");
    cbRodillaFlexorIz = rs.getString("cbRodillaFlexorIz");
    cbRodillaExtensorDer = rs.getString("cbRodillaExtensorDer");
    cbRodillaExtensorIz = rs.getString("cbRodillaExtensorIz");
    cbTobilloPlantiflexorDer = rs.getString("cbTobilloPlantiflexorDer");
    cbTobilloPlantiflexorIz = rs.getString("cbTobilloPlantiflexorIz");
    cbTobilloDorsiFleDer = rs.getString("cbTobilloDorsiFleDer");
    cbTobilloDorsiFleIz = rs.getString("cbTobilloDorsiFleIz");
    cbPieInvertoresDer = rs.getString("cbPieInvertoresDer");
    cbPieInvertoresIz = rs.getString("cbPieInvertoresIz");
    cbPieEvertoresDer = rs.getString("cbPieEvertoresDer");
    cbPieEvertoresIz = rs.getString("cbPieEvertoresIz");
    cbDedosPieFlexoresDer = rs.getString("cbDedosPieFlexoresDer");
    cbDedosPieFlexoresIz = rs.getString("cbDedosPieFlexoresIz");
    cbDedosPieExtensoresDer = rs.getString("cbDedosPieExtensoresDer");
    cbDedosPieExtensoresIz = rs.getString("cbDedosPieExtensoresIz");
    cbDedosPieAductoresDer = rs.getString("cbDedosPieAductoresDer");
    cbDedosPieAductoresIz = rs.getString("cbDedosPieAductoresIz");
    cbDedosPieAbeductoresDer = rs.getString("cbDedosPieAbeductoresDer");
    cbDedosPieAbeductoresIz = rs.getString("cbDedosPieAbeductoresIz");

    flexionHombroIz = rs.getString("flexionHombroIz");
    flexionHombroDer = rs.getString("flexionHombroDer");
    extensionHombroIz = rs.getString("extensionHombroIz");
    extensionHombroDer = rs.getString("extensionHombroDer");
    abducionHombroIz = rs.getString("abducionHombroIz");
    abducionHombroDer = rs.getString("abducionHombroDer");
    aduccionHombroIz = rs.getString("aduccionHombroIz");
    aduccionHombroDer = rs.getString("aduccionHombroDer");
    rotacionInternaHombroIz = rs.getString("rotacionInternaHombroIz");
    rotacionInternaHombroDer = rs.getString("rotacionInternaHombroDer");
    rotacionExternaHombroIz = rs.getString("rotacionExternaHombroIz");
    rotacionExternaHombroDer = rs.getString("rotacionExternaHombroDer");
    flexionCodoIz = rs.getString("flexionCodoIz");
    flexionCodoDer = rs.getString("flexionCodoDer");
    extensionCodoIz = rs.getString("extensionCodoIz");
    extensionCodoDer = rs.getString("extensionCodoDer");
    pronacionCodoIz = rs.getString("pronacionCodoIz");
    pronacionCodoDer = rs.getString("pronacionCodoDer");
    supinacionCodoIz = rs.getString("supinacionCodoIz");
    supinacionCodoDer = rs.getString("supinacionCodoDer");
    flexionMunecaIz = rs.getString("flexionMunecaIz");
    flexionMunecaDer = rs.getString("flexionMunecaDer");
    extensionMunecaIz = rs.getString("extensionMunecaIz");
    extensionMunecaDer = rs.getString("extensionMunecaDer");
    desviacionRadialIz = rs.getString("desviacionRadialIz");
    desviacionRadialDer = rs.getString("desviacionRadialDer");
    desviacionCubitalDer = rs.getString("desviacionCubitalDer");
    desviacionCubitalIz = rs.getString("desviacionCubitalIz");
    abducionPulgarIz = rs.getString("abducionPulgarIz");
    abducionPulgarDer = rs.getString("abducionPulgarDer");
    aduccionPulgarIz = rs.getString("aduccionPulgarIz");
    aduccionPulgarDer = rs.getString("aduccionPulgarDer");
    oposicionPulgarIz = rs.getString("oposicionPulgarIz");
    oposicionPulgarDer = rs.getString("oposicionPulgarDer");
    pulgarFlexionMCFIz = rs.getString("pulgarFlexionMCFIz");
    pulgarFlexionMCFDer = rs.getString("pulgarFlexionMCFDer");
    pulgarExtensionMCFIz = rs.getString("pulgarExtensionMCFIz");
    pulgarExtensionMCFDer = rs.getString("pulgarExtensionMCFDer");
    pulgarFlexionIFIz = rs.getString("pulgarFlexionIFIz");
    pulgarFlexionIFDer = rs.getString("pulgarFlexionIFDer");
    pulgarExtensionIFIz = rs.getString("pulgarExtensionIFIz");
    pulgarExtensionIFDer = rs.getString("pulgarExtensionIFDer");
    metacaporfalangiaFlexionIz = rs.getString("metacaporfalangiaFlexionIz");
    metacaporfalangiaFlexionDer = rs.getString("metacaporfalangiaFlexionDer");
    metacaporfalangiaExtensionIz = rs.getString("metacaporfalangiaExtensionIz");
    metacaporfalangiaExtensionDer = rs.getString("metacaporfalangiaExtensionDer");
    interfalangiaFlexiProxIz = rs.getString("interfalangiaFlexiProxIz");
    interfalangiaFlexiProxDer = rs.getString("interfalangiaFlexiProxDer");
    interfalangiaFlexiDistalIz = rs.getString("interfalangiaFlexiDistalIz");
    interfalangiaFlexiDistalDer = rs.getString("interfalangiaFlexiDistalDer");
    interfalangiaAbduccionIz = rs.getString("interfalangiaAbduccionIz");
    interfalangiaAbduccionDer = rs.getString("interfalangiaAbduccionDer");
    interfalangiaAduccionIz = rs.getString("interfalangiaAduccionIz");
    interfalangiaAduccionDer = rs.getString("interfalangiaAduccionDer");
    interfalangiaExtensionProxIz = rs.getString("interfalangiaExtensionProxIz");
    interfalangiaExtensionProxDer = rs.getString("interfalangiaExtensionProxDer");
    interfalangiaExtensionDisIz = rs.getString("interfalangiaExtensionDisIz");
    interfalangiaExtensionDisDer = rs.getString("interfalangiaExtensionDisDer");
          
  } 
  System.out.println("HOLAA:: " + metacaporfalangiaFlexionIz); 
}
catch ( Exception e ){
  System.out.println(e.getMessage());
} %>


<!DOCTYPE html>
<html lang="en">
    <head>
        <script type="script" src="main.js"></script>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport">
        <link rel="stylesheet" href="">
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->
    <title>Miembros Superiores</title>
</head>
<body>
    <div style="background-color: white; height: 300px; width: 100%; overflow: scroll;">
        <div>
            <h1 class="card-title">Valoracion De La Funcion Articular Y Muscular Miembros Superiores </h1>
            <hr>
            <h2 class="text-center center">Hombro  (Izquierdo / Derecho)</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
                    
                    <th class="text-center center" scope="col"> Hombro Izquierdo </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                    <th class="text-center center" scope="col"> Rango normal </th>
                    <th class="text-center center" scope="col"> Hombro Derecho </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl2c1" value="<%=flexionHombroIz%>" type="number" max="180" min="-180" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"> </td>
                    <td class="text-center" style="width: 20%;">
                        <select  id="vrl5c2" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                          <option value=""></option>
                          <%  resulCombo.clear();
                          resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                              
                           for(int i=0;i<resulCombo.size();i++){
                              cmb=(ComboVO)resulCombo.get(i);
                              if(cbHmFlexorIz.trim().equals(cmb.getId())){
                               %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                               <%}
                               else{
                               %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                               <%
                             }
                         }%>
                        </select> 
                    </td>
                    <td class="text-center">0 -180</td>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl2c2" value="<%=flexionHombroDer%>" type="number" max="180" min="-180" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c1" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmFlexorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                  </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl2c3" value="<%=extensionHombroIz%>" type="number" max="60" min="-60" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c4" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmExtensorIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                  </td>
                    <td class="text-center">0 - 60</td>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl2c4" value="<%=extensionHombroDer%>" type="number" max="60" min="-60" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c3" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmExtensorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                  </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ADUCTORES</th>
                    <td class="text-center center"><input  id="vrl2c7" value="<%=aduccionHombroIz%>" type="number" max="110" min="-110" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c6" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmAductoresIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 - 110</td>
                    <th class="text-center center" scope="row">ADUCTORES</th>
                    <td class="text-center center"><input   id="vrl2c8" value="<%=aduccionHombroDer%>" type="number" max="110" min="-110" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c5" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmAductoresDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ABEDUCTORES</th>
                    <td class="text-center center"><input  id="vrl2c5" value="<%=abducionHombroIz%>" type="number" max="180" min="-180" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c8" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmAbeductorIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 - 180</td>
                    <th class="text-center center" scope="row">ABEDUCTORES</th>
                    <td class="text-center center"><input  id="vrl2c6" value="<%=abducionHombroDer%>" type="number" max="180" min="-180" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c7" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmAbeductorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ROTADORES INTERNOS</th>
                    <td class="text-center center"><input  id="vrl2c9" value="<%=rotacionInternaHombroIz%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c10" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmRotadorInternoIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 -90</td>
                    <th class="text-center center" scope="row">ROTADORES INTERNOS</th>
                    <td class="text-center center"><input  id="vrl2c10" value="<%=rotacionInternaHombroDer%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c9" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmRotadorInternoDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">ROTADORES EXTERNOS</th>
                    <td class="text-center center"><input  id="vrl2c11" value="<%=rotacionExternaHombroIz%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c12" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmRotadorExternoIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center" >0 -90</td>
                    <th class="text-center center" scope="row">ROTADORES EXTERNOS</th>
                    <td class="text-center center"><input   id="vrl2c12" value="<%=rotacionExternaHombroDer%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c11" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbHmRotadorExternoDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                 
                
                </tbody>
            </table>
        </div>
        <div>
            <h2 class="card-title text-center center">CODO (Izquierdo / Derecho) </h2>
            <hr>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>                    
                    <th class="text-center center" scope="col"> Codo Izquierdo </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                    <th class="text-center center" scope="col"> Rango normal </th>
                    <th class="text-center center" scope="col"> Codo Derecho </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl2c13" value="<%=flexionCodoIz%>" type="number" max="145" min="-145" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c14" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCdFlexorIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center" >0 -145</td>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl2c14" value="<%=flexionCodoDer%>" type="number" max="145" min="-145" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c13" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCdFlexorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl2c15" value="<%=extensionCodoIz%>" type="number" max="145" min="-145" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c16" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCdExtensorIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center" >145 -0 </td>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl2c16" value="<%=extensionCodoDer%>" type="number" max="145" min="-145" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c15" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCdExtensorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">PRONADORES</th>
                    <td class="text-center center"><input  id="vrl2c17" value="<%=pronacionCodoIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c79" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCodoPronadoresIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 -80</td>
                    <th class="text-center center" scope="row">PRONADORES</th>
                    <td class="text-center center"><input  id="vrl2c18" value="<%=pronacionCodoDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c80" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCodoPronadoresDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">SUPINADORES</th>
                    <td class="text-center center"><input  id="vrl2c19" value="<%=supinacionCodoIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c81" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCodoSupinadoresIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 -80</td>
                    <th class="text-center center" scope="row">SUPINADORES</th>
                    <td class="text-center center"><input  id="vrl2c20" value="<%=supinacionCodoDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c82" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbCodoSupinadoresDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  
                 
                
                </tbody>
            </table>
        </div>
        <div>
            <h2 class="card-title text-center center">Mu&ntilde;eca (Izquierdo / Derecho) </h2>
            <hr>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
                    
                    <th class="text-center center" scope="col"> Mu&ntilde;eca Izquierdo </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                    <th class="text-center center" scope="col"> Rango normal </th>
                    <th class="text-center center" scope="col"> Mu&ntilde;eca Derecho </th>
                    <th class="text-center center" scope="col"> Rango encontrado </th>
                    <th class="text-center center" scope="col"> Fuerza muscular </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl2c21" value="<%=flexionMunecaIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c22" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnFlexorIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 -80</td>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl2c22" value="<%=flexionMunecaDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c21" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnFlexorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl2c23" value="<%=extensionMunecaIz%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c24" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnExtensorIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center"> 0- 70 </td>
                    <th class="text-center center" scope="row">EXTENSORES</th>
                    <td class="text-center center"><input  id="vrl2c24" value="<%=extensionMunecaDer%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c23" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnExtensorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">Desviacion Radial</th>
                    <td class="text-center center"><input  id="vrl2c25" value="<%=desviacionRadialIz%>" type="number" max="25" min="-25" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c18" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnPronadoresIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 -25</td>
                    <th class="text-center center" scope="row">Desviacion Radial</th>
                    <td class="text-center center"><input  id="vrl2c26" value="<%=desviacionRadialDer%>" type="number" max="25" min="-25" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c17" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnPronadoresDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  </tr>
                  <tr>
                    <th class="text-center center" scope="row">Desviacion Cubital</th>
                    <td class="text-center center"><input  id="vrl2c28" value="<%=desviacionCubitalIz%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c20" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnSupinadorIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <td class="text-center">0 -45</td>
                    <th class="text-center center" scope="row">Desviacion Cubital</th>
                    <td class="text-center center"><input  id="vrl2c27" value="<%=desviacionCubitalDer%>" type="number" max="45" min="-45" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c19" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbMnSupinadorDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  
                 
                
                </tbody>
            </table>
        </div>
        <div>
          <h2 class="card-title text-center center">MCF II, III, IV, V METACARPOFALINGICA- DEDO (Izquierdo / Derecho)</h2>
          <hr>
          <table class="table table-bordered mt-1" id="tfoi">
              <thead>
                <tr>
                  
                  <th class="text-center center" scope="col"> MCF II, III, IV, V </th>
                  <th class="text-center center" scope="col"> Rango encontrado </th>
                  <th class="text-center center" scope="col"> Fuerza muscular </th>
                  <th class="text-center center" scope="col"> Rango normal </th>
                  <th class="text-center center" scope="col"> MCF II, III, IV, V  </th>
                  <th class="text-center center" scope="col"> Rango encontrado </th>
                  <th class="text-center center" scope="col"> Fuerza muscular </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="text-center center" scope="row">FLEXION</th>
                  <td class="text-center center"><input  id="vrl2c35" value="<%=metacaporfalangiaFlexionIz%>" type="number" max="85" min="-85" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c83" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDedosManoMetacarpoFlexionIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                  <td class="text-center">0 -85</td>
                  <th class="text-center center" scope="row">FLEXION</th>
                  <td class="text-center center"><input  id="vrl2c36" value="<%=metacaporfalangiaFlexionDer%>" type="number" max="85" min="-85" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c84" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDedosManoMetacarpoFlexionDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                </tr>
                <tr>
                  <th class="text-center center" scope="row">EXTENSION</th>
                  <td class="text-center center"><input  id="vrl2c37" value="<%=metacaporfalangiaExtensionIz%>" type="number" max="20" min="-20" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c85" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbDedosManoMetacarpoExtensionIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center"> 0- 20 </td>
                  <th class="text-center center" scope="row">EXTENSION</th>
                  <td class="text-center center"><input  id="vrl2c38" value="<%=metacaporfalangiaExtensionDer%>" type="number" max="20" min="-20" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c86" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbDedosManoMetacarpoExtensionDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
              
               
              
              </tbody>
          </table>
        </div>
        <div>
        <hr>
        <h2 class="text-center center">IF II, III, IV, V INTERFALANGICA-DEDO  (Izquierdo / Derecho)</h2>
        <table class="table table-bordered mt-1" id="tfoi">
            <thead>
              <tr>
                
                <th class="text-center center" scope="col"> Hombro Izquierdo </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza muscular </th>
                <th class="text-center center" scope="col"> Rango normal </th>
                <th class="text-center center" scope="col"> Hombro Derecho </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza muscular </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th class="text-center center" scope="row">Flexion Proximal</th>
                <td class="text-center center"><input  id="vrl2c39" value="<%=interfalangiaFlexiProxIz%>" type="number" max="110" min="-110" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c26" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDedosManoFlexoresIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                <td class="text-center">0 -110</td>
                <th class="text-center center" scope="row">Flexion Proximal</th>
                <td class="text-center center"><input  id="vrl2c40" value="<%=interfalangiaFlexiProxDer%>" type="number" max="110" min="-110" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c25" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDedosManoFlexoresDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
              </tr>
              <tr>
                <th class="text-center center" scope="row">Flexion Distal</th>
                <td class="text-center center"><input  id="vrl2c41" value="<%=interfalangiaFlexiDistalIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c87" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDedosManoFlexionDistalIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                <td class="text-center">0 - 80</td>
                <th class="text-center center" scope="row">Flexion Distal</th>
                <td class="text-center center"><input  id="vrl2c42" value="<%=interfalangiaFlexiDistalDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c88" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDedosManoFlexionDistalDer.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
              </tr>
              <tr>
                <th class="text-center center" scope="row">Abduccion</th>
                <td class="text-center center"><input  id="vrl2c43" value="<%=interfalangiaAbduccionIz%>" type="number" max="20" min="-20" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c32" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoAbeductoresIz.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                <td class="text-center">0 - 20</td>
                <th class="text-center center" scope="row">Abduccion</th>
                <td class="text-center center"><input  id="vrl2c44" value="<%=interfalangiaAbduccionDer%>" type="number" max="20" min="-20" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c31" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoAbeductoresDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
              <tr>
                <th class="text-center center" scope="row">Aduccion</th>
                <td class="text-center center"><input  id="vrl2c45" value="<%=interfalangiaAduccionIz%>" type="number" max="20" min="-20" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c30" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoAductoresIz.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                <td class="text-center">20 - 0</td>
                <th class="text-center center" scope="row">Aduccion</th>
                <td class="text-center center"><input  id="vrl2c46" value="<%=interfalangiaAduccionDer%>" type="number" max="20" min="-20" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c29" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoAductoresDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
              <tr>
                <th class="text-center center" scope="row">Extenccion Proximal</th>
                <td class="text-center center"><input  id="vrl2c47" value="<%=interfalangiaExtensionProxIz%>" type="number" max="110" min="-110" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c28" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoExtensoresIz.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                <td class="text-center">-110 - 0</td>
                <th class="text-center center" scope="row">Extenccion Proximal</th>
                <td class="text-center center"><input  id="vrl2c48" value="<%=interfalangiaExtensionProxDer%>" type="number" max="110" min="-110" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c27" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoExtensoresDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
              <tr>
                <th class="text-center center" scope="row">Extension Distal</th>
                <td class="text-center center"><input  id="vrl2c49" value="<%=interfalangiaExtensionDisIz%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c89" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoExtensionDistalIz.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                <td class="text-center">80 - 0</td>
                <th class="text-center center" scope="row">Extension Distal</th>
                <td class="text-center center"><input  id="vrl2c50" value="<%=interfalangiaExtensionDisDer%>" type="number" max="80" min="-80" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c90" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDedosManoExtensionDistalDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
             
            
            </tbody>
        </table>
        </div>
        <div>
          <hr>
          <h2 class="text-center center">Pulgar (Izquierdo / Derecho)</h2>
          <table class="table table-bordered mt-1" id="tfoi">
              <thead>
                <tr>
                  
                  <th class="text-center center" scope="col"> Pulgar Izquierdo </th>
                  <th class="text-center center" scope="col"> Rango encontrado </th>
                  <th class="text-center center" scope="col"> Fuerza muscular </th>
                  <th class="text-center center" scope="col"> Rango normal </th>
                  <th class="text-center center" scope="col"> Pulgar Derecho </th>
                  <th class="text-center center" scope="col"> Rango encontrado </th>
                  <th class="text-center center" scope="col"> Fuerza muscular </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="text-center center" scope="row">Abductor</th>
                  <td class="text-center center"><input  id="vrl2c29" value="<%=abducionPulgarIz%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c34" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgAbductorIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center">0 -70</td>
                  <th class="text-center center" scope="row">Abductor</th>
                  <td class="text-center center"><input  id="vrl2c30" value="<%=abducionPulgarDer%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c33" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgAbductorDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
                <tr>
                  <th class="text-center center" scope="row">Aductor</th>
                  <td class="text-center center"><input  id="vrl2c31" value="<%=aduccionPulgarIz%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c36" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgAductorIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center">70 -0 </td>
                  <th class="text-center center" scope="row">Aductor</th>
                  <td class="text-center center"><input  id="vrl2c32" value="<%=aduccionPulgarDer%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c35" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgAductorDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
                <tr>
                  <th class="text-center center" scope="row">Oponente</th>
                  <td class="text-center center"><input  id="vrl2c33" value="<%=oposicionPulgarIz%>" type="number" max="8" min="-8" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c38" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgOponenteIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center">0 - 8</td>
                  <th class="text-center center" scope="row">Oponente</th>
                  <td class="text-center center"><input  id="vrl2c34" value="<%=oposicionPulgarIz%>" type="number" max="8" min="-8" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c37" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgOponenteDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
                <tr>
                  <th class="text-center center" scope="row">Flexion MCF</th>
                  <td class="text-center center"><input  id="vrl2c51" value="<%=pulgarFlexionMCFIz%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c46" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgFlexorCortoIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center">0 -70</td>
                  <th class="text-center center" scope="row">Flexion MCF</th>
                  <td class="text-center center"><input  id="vrl2c52" value="<%=pulgarFlexionMCFDer%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c45" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgFlexorCortoDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
                <tr>
                  <th class="text-center center" scope="row">Extenccion MCF</th>
                  <td class="text-center center"><input  id="vrl2c53" value="<%=pulgarExtensionMCFIz%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c42" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgExtensorCortoIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center">-70 - 0</td>
                  <th class="text-center center" scope="row">Extenccion MCF</th>
                  <td class="text-center center"><input  id="vrl2c54" value="<%=pulgarExtensionMCFDer%>" type="number" max="70" min="-70" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c41" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgExtensorCortoDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
                <tr>
                  <th class="text-center center" scope="row">Flexion IF</th>
                  <td class="text-center center"><input  id="vrl2c55" value="<%=pulgarFlexionIFIz%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c44" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgFlexorLargoIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center">0- 90</td>
                  <th class="text-center center" scope="row">Flexion IF</th>
                  <td class="text-center center"><input  id="vrl2c56" value="<%=pulgarFlexionIFDer%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c43" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgFlexorLargoDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
                <tr>
                  <th class="text-center center" scope="row">Extension IF</th>
                  <td class="text-center center"><input  id="vrl2c57" value="<%=pulgarExtensionIFIz%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c42" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgExtensorCortoIz.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                  <td class="text-center">90 - 0</td>
                  <th class="text-center center" scope="row">Extension IF</th>
                  <td class="text-center center"><input  id="vrl2c58" value="<%=pulgarExtensionIFDer%>" type="number" max="90" min="-90" maxlength="3" style="width: 45%;" oninput="maxLongitudNumero(this.id,'3');" onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl2',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c41" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbPlgExtensorCortoDer.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                </tr>
              
              </tbody>
          </table>
        </div> 
    </div>
    

</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</html>

<!-- 66904571 -->