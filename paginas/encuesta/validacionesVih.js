

function validacionesHcVih(tabla, campo) {
    return new Promise((resolve = () => { }, reject = () => { }) => {
        let cambioValor = new Map();
        let disabledForm;
        let enabledForm;
        let valorDependiente = new Map();
        switch (tabla) {

            case 'tdla':
                try { document.getElementById('formulario.tdla.c4').min = document.getElementById('formulario.tdla.c3').value } catch (error) { }
                break;

            case 'pfenf':
            
                document.getElementById('formulario.pfenf.c4').disabled = true;

                cambioValor.clear();
                switch (campo) {
                    case 'c0':
                        cambioValor.set('c2', '');
                        cambioValor.set('c5', '');
                        cambioValor.set('c8', '');
                        cambioValor.set('c9', '');
                        cambioValor.set('c21', '');
                        cambioValor.set('c24', '');
                        cambioValor.set('c10', '');
                        cambioValor.set('c22', '');
                        cambioValor.set('c12', '');
                        cambioValor.set('c13', '');
                        cambioValor.set('c14', '');
                        cambioValor.set('c15', '');
                        if (document.getElementById('formulario.pfenf.c0').value == 'NO') {
                            cambioValor.set('c1', '6');
                            asignaValorCamposHc(tabla, cambioValor, true);
                        } else {
                            cambioValor.set('c1', '');
                            asignaValorCamposHc(tabla, cambioValor, false);
                        }
                        break;

                    case 'c1':
                            
                            if (document.getElementById('formulario.pfenf.c1').value == '5') {
                                document.getElementById('formulario.pfenf.c37').disabled = false
                            } else {
                                document.getElementById('formulario.pfenf.c37').disabled = true
                                cambioValor.set('c37', '');
                                asignaValorCamposHc(tabla, cambioValor, true);
                            } 
                        break;
                    default:
                        break;
                }
                break;

            //******* */

            case 'nvvih':
                let vc1 = document.getElementById('formulario.nvvih.c1');
                switch (campo) {
                    case 'c1':
                        valorDependiente.clear();
                        valorDependiente.set('1', '1845-01-01');
                        valorDependiente.set('2', '1845-01-01');
                        valorDependiente.set('3', '1845-01-01');
                        valorDependiente.set('4', '1845-01-01');
                        valorDependiente.set('6', '1845-01-01');
                        valorDependiente.set('7', '1845-01-01');
                        valorDependiente.set('8', '1845-01-01');
                        valorDependiente.set('9', '1845-01-01');
                        valorDependiente.set('10', '1845-01-01');
                        valorDependiente.set('12', '1845-01-01');
                        valorDependiente.set('13', '1845-01-01');
                        valorDependiente.set('14', '1845-01-01');
                        valorDependiente.set('15', '1845-01-01');
                        valorDependiente.set('16', '1845-01-01');
                        valorDependiente.set('17', '1845-01-01');
                        valorDependiente.set('18', '1845-01-01');
                        valorDependiente.set('19', '1845-01-01');
                        valorDependiente.set('20', '1845-01-01');
                        valorDependiente.set('21', '1845-01-01');
                        valorDependiente.set('22', '1845-01-01');
                        valorDependiente.set('23', '1845-01-01');
                        valorDependiente.set('24', '1845-01-01');
                        valorDependiente.set('25', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c2'], vc1, valorDependiente, tabla);

                        valorDependiente.clear();
                        valorDependiente.set('1', '9');
                        valorDependiente.set('2', '9');
                        valorDependiente.set('3', '9');
                        valorDependiente.set('4', '9');
                        valorDependiente.set('6', '9');
                        valorDependiente.set('7', '9');
                        valorDependiente.set('8', '9');
                        valorDependiente.set('9', '9');
                        valorDependiente.set('10', '9');
                        valorDependiente.set('12', '9');
                        valorDependiente.set('13', '9');
                        valorDependiente.set('14', '9');
                        valorDependiente.set('15', '9');
                        valorDependiente.set('16', '9');
                        valorDependiente.set('17', '9');
                        valorDependiente.set('18', '9');
                        valorDependiente.set('19', '9');
                        valorDependiente.set('20', '9');
                        valorDependiente.set('21', '9');
                        valorDependiente.set('22', '9');
                        valorDependiente.set('23', '9');
                        valorDependiente.set('24', '9');
                        valorDependiente.set('25', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c3'], vc1, valorDependiente, tabla);

                        valorDependiente.clear();
                        valorDependiente.set('1', '1845-01-01');
                        valorDependiente.set('2', '1845-01-01');
                        valorDependiente.set('3', '1845-01-01');
                        valorDependiente.set('5', '1845-01-01');
                        valorDependiente.set('6', '1845-01-01');
                        valorDependiente.set('7', '1845-01-01');
                        valorDependiente.set('8', '1845-01-01');
                        valorDependiente.set('9', '1845-01-01');
                        valorDependiente.set('11', '1845-01-01');
                        valorDependiente.set('12', '1845-01-01');
                        valorDependiente.set('13', '1845-01-01');
                        valorDependiente.set('14', '1845-01-01');
                        valorDependiente.set('15', '1845-01-01');
                        valorDependiente.set('16', '1845-01-01');
                        valorDependiente.set('17', '1845-01-01');
                        valorDependiente.set('18', '1845-01-01');
                        valorDependiente.set('19', '1845-01-01');
                        valorDependiente.set('20', '1845-01-01');
                        valorDependiente.set('21', '1845-01-01');
                        valorDependiente.set('22', '1845-01-01');
                        valorDependiente.set('23', '1845-01-01');
                        valorDependiente.set('24', '1845-01-01');
                        valorDependiente.set('25', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c4'], vc1, valorDependiente, tabla);

                        valorDependiente.clear();
                        valorDependiente.set('1', '4');
                        valorDependiente.set('2', '4');
                        valorDependiente.set('3', '4');
                        valorDependiente.set('5', '4');
                        valorDependiente.set('6', '4');
                        valorDependiente.set('7', '4');
                        valorDependiente.set('8', '4');
                        valorDependiente.set('9', '4');
                        valorDependiente.set('11', '4');
                        valorDependiente.set('12', '4');
                        valorDependiente.set('13', '4');
                        valorDependiente.set('14', '4');
                        valorDependiente.set('15', '4');
                        valorDependiente.set('16', '4');
                        valorDependiente.set('17', '4');
                        valorDependiente.set('18', '4');
                        valorDependiente.set('19', '4');
                        valorDependiente.set('20', '4');
                        valorDependiente.set('21', '4');
                        valorDependiente.set('22', '4');
                        valorDependiente.set('23', '4');
                        valorDependiente.set('24', '4');
                        valorDependiente.set('25', '4');
                        asignaValorCamposHcDependiente(cambioValor, ['c5'], vc1, valorDependiente, tabla);
                        break;
                    default:
                        break;
                }
                break;

            //******* */
            case 'pcvi2':
                let c8 = document.getElementById('formulario.pcvi2.c8')
                let variables = $('[class ^=onoffswitch-checkbox2]')
                let contador = 0;
                let contador2 = 0;

                variables.each(((value, key) => {
                    if (key.id != 'formulario.pcvi2.c8' && key.value == 'SI') {
                        ++contador
                    }
                    else if (key.id != 'formulario.pcvi2.c8' && key.value == 'NO') {
                        ++contador2
                    }
                }))

                if (contador != 0) {
                    if (c8.value == 'SI') {
                        c8.click();
                    }
                }

                if (contador2 == variables.length - 1) {
                    if (c8.value == 'NO') {
                        c8.click();
                    }
                }
                break;

            case 'vanenf':
                let c47 = document.getElementById('formulario.vanenf.c47');

                valores_a_mandar = '';
                add_valores_a_mandar('c1');
                add_valores_a_mandar('vih0');
                add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
                traerDatoPlantilla(valores_a_mandar).then((result) => {
                    if (result != '1' && result != '') {
                        try {
                            document.getElementById('formulario.vanenf.c19td').parentNode.style.display = 'none';
                            document.getElementById('formulario.vanenf.c20td').parentNode.style.display = 'none';
                            document.getElementById('formulario.vanenf.c21td').parentNode.style.display = 'none';
                            document.getElementById('formulario.vanenf.c22td').parentNode.style.display = 'none';
                            document.getElementById('formulario.vanenf.c23td').parentNode.style.display = 'none';
                            let titulos = document.querySelectorAll("tr.paratrplantilla td h3");
                            titulos.forEach(function (titulo) {
                                if (titulo.innerHTML == 'TDAP') titulo.parentNode.parentNode.style.display = 'none'
                            })
                        } catch (error) {
                            console.error(error);
                        }
                    }
                }).catch((err) => {
                    console.error(err)
                });

                try {
                    if (document.getElementById('formulario.vanenf.c47').value == 'NO') {
                        var c72 = document.getElementById('formulario.vanenf.c72').value
                        var c73 = document.getElementById('formulario.vanenf.c73').value
                        try { document.getElementById('formulario.vanenf.c52').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c53').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c54').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c30').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c31').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c55').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c49').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c72').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c73').disabled = false } catch (error) { }


                        cambioValor.clear();
                        cambioValor.set('c52', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c53', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c54', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c30', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c31', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c55', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c49', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);


                        if (c72 != '' && c73 != '') {

                            cambioValor.clear();
                            cambioValor.set('c2', '3');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        } else {
                            cambioValor.clear();
                            cambioValor.set('c2', '6');
                            asignaValorCamposHc('vihD2', cambioValor, true);

                        }

                    } else {
                        cambioValor.set('c72', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c73', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        try { document.getElementById('formulario.vanenf.c52').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c53').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c54').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c30').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c31').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c55').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c49').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c72').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c73').disabled = true } catch (error) { }
                        if (document.getElementById('formulario.vanenf.c52').value != '' && document.getElementById('formulario.vanenf.c53').value != '' && document.getElementById('formulario.vanenf.c54').value != '') {
                            console.log('aqui:', document.getElementById('formulario.vanenf.c13').value);
                            console.log('aqui:', document.getElementById('formulario.vanenf.c32').value);
                            cambioValor.clear();
                            cambioValor.set('c2', '1');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                        else if (document.getElementById('formulario.vanenf.c52').value != '' && (document.getElementById('formulario.vanenf.c53').value == '' || document.getElementById('formulario.vanenf.c54').value == '')) {
                            console.log('aqui:', document.getElementById('formulario.vanenf.c13').value);
                            console.log('aqui:', document.getElementById('formulario.vanenf.c32').value);
                            cambioValor.clear();
                            cambioValor.set('c2', '2');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                        else if (document.getElementById('formulario.vanenf.c52').value == '' && document.getElementById('formulario.vanenf.c53').value == '' && document.getElementById('formulario.vanenf.c54').value == '') {
                            console.log('aqui:', document.getElementById('formulario.vanenf.c13').value);
                            console.log('aqui:', document.getElementById('formulario.vanenf.c32').value);
                            cambioValor.clear();
                            cambioValor.set('c2', '6');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                        else {
                            cambioValor.clear();
                            cambioValor.set('c2', '6');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                    }
                } catch (error) {

                }

                try {
                    if (document.getElementById('formulario.vanenf.c65').value == 'NO') {

                        var c67 = document.getElementById('formulario.vanenf.c67').value
                        var c68 = document.getElementById('formulario.vanenf.c68').value
                        try { document.getElementById('formulario.vanenf.c13').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c32').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c67').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c68').disabled = false } catch (error) { }

                        cambioValor.clear();
                        cambioValor.set('c13', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c32', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);


                        if (c67 != '' && c68 != '') {
                            cambioValor.clear();
                            cambioValor.set('c1', '3');
                            asignaValorCamposHc('vihD2', cambioValor, true);

                        } else {
                            cambioValor.clear();
                            cambioValor.set('c1', '5');
                            asignaValorCamposHc('vihD2', cambioValor, true);

                        }



                    } else {
                        cambioValor.set('c67', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c68', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        try { document.getElementById('formulario.vanenf.c13').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c32').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c67').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c68').disabled = true } catch (error) { }

                        if (document.getElementById('formulario.vanenf.c13').value != '' && document.getElementById('formulario.vanenf.c32').value != '') {
                            console.log('aqui:', document.getElementById('formulario.vanenf.c13').value);
                            console.log('aqui:', document.getElementById('formulario.vanenf.c32').value);
                            cambioValor.clear();
                            cambioValor.set('c1', '1');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                        else if (document.getElementById('formulario.vanenf.c13').value != '' && document.getElementById('formulario.vanenf.c32').value == '') {
                            console.log('aqui:', document.getElementById('formulario.vanenf.c13').value);
                            console.log('aqui:', document.getElementById('formulario.vanenf.c32').value);
                            cambioValor.clear();
                            cambioValor.set('c1', '2');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                        else if (document.getElementById('formulario.vanenf.c13').value == '' && document.getElementById('formulario.vanenf.c32').value == '') {
                            console.log('aqui:', document.getElementById('formulario.vanenf.c13').value);
                            console.log('aqui:', document.getElementById('formulario.vanenf.c32').value);
                            cambioValor.clear();
                            cambioValor.set('c1', '5');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                        else {
                            cambioValor.clear();
                            cambioValor.set('c1', '5');
                            asignaValorCamposHc('vihD2', cambioValor, true);
                        }
                    }
                } catch (error) { }


                try {
                    if (document.getElementById('formulario.vanenf.c69').value == 'NO') {


                        try { document.getElementById('formulario.vanenf.c28').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c29').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c70').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c71').disabled = false } catch (error) { }
                        cambioValor.clear();
                        cambioValor.set('c28', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c29', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);


                    } else {

                        try { document.getElementById('formulario.vanenf.c28').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c29').disabled = false } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c70').disabled = true } catch (error) { }
                        try { document.getElementById('formulario.vanenf.c71').disabled = true } catch (error) { }
                        cambioValor.clear();
                        cambioValor.set('c70', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                        cambioValor.set('c71', '');
                        asignaValorCamposHc('vanenf', cambioValor, true);
                    }
                } catch (error) { }

                // POLIO IM
                try { document.getElementById('formulario.vanenf.c4').min = document.getElementById('formulario.vanenf.c3').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c5').min = document.getElementById('formulario.vanenf.c4').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c6').min = document.getElementById('formulario.vanenf.c5').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c7').min = document.getElementById('formulario.vanenf.c6').value } catch (error) { }

                // HEPATITIS A
                try { document.getElementById('formulario.vanenf.c32').min = document.getElementById('formulario.vanenf.c13').value } catch (error) { }

                // PENTAVALENTE
                try { document.getElementById('formulario.vanenf.c15').min = document.getElementById('formulario.vanenf.c14').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c16').min = document.getElementById('formulario.vanenf.c15').value } catch (error) { }

                // ROTAVIRUS
                try { document.getElementById('formulario.vanenf.c18').min = document.getElementById('formulario.vanenf.c17').value } catch (error) { }

                // TDAP
                try { document.getElementById('formulario.vanenf.c20').min = document.getElementById('formulario.vanenf.c19').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c21').min = document.getElementById('formulario.vanenf.c20').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c22').min = document.getElementById('formulario.vanenf.c21').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c23').min = document.getElementById('formulario.vanenf.c22').value } catch (error) { }

                // VARICELA
                try { document.getElementById('formulario.vanenf.c51').min = document.getElementById('formulario.vanenf.c28').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c29').min = document.getElementById('formulario.vanenf.c51').value }
                catch (error) {
                    document.getElementById('formulario.vanenf.c29').min = document.getElementById('formulario.vanenf.c28').value
                }

                // HEPATITIS B
                try { document.getElementById('formulario.vanenf.c53').min = document.getElementById('formulario.vanenf.c52').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c54').min = document.getElementById('formulario.vanenf.c53').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c30').min = document.getElementById('formulario.vanenf.c54').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c31').min = document.getElementById('formulario.vanenf.c30').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c55').min = document.getElementById('formulario.vanenf.c31').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c49').min = document.getElementById('formulario.vanenf.c55').value } catch (error) { }

                // NEUMOCOCO
                try { document.getElementById('formulario.vanenf.c39').min = document.getElementById('formulario.vanenf.c38').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c40').min = document.getElementById('formulario.vanenf.c39').value } catch (error) { }

                // TT-TD
                try { document.getElementById('formulario.vanenf.c42').min = document.getElementById('formulario.vanenf.c41').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c43').min = document.getElementById('formulario.vanenf.c42').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c35').min = document.getElementById('formulario.vanenf.c43').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c36').min = document.getElementById('formulario.vanenf.c35').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c56').min = document.getElementById('formulario.vanenf.c36').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c57').min = document.getElementById('formulario.vanenf.c56').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c58').min = document.getElementById('formulario.vanenf.c57').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c59').min = document.getElementById('formulario.vanenf.c58').value } catch (error) { }

                // VPH
                try { document.getElementById('formulario.vanenf.c45').min = document.getElementById('formulario.vanenf.c44').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c46').min = document.getElementById('formulario.vanenf.c44').value } catch (error) { }

                // COVID
                try { document.getElementById('formulario.vanenf.c61').min = document.getElementById('formulario.vanenf.c60').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c62').min = document.getElementById('formulario.vanenf.c61').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c63').min = document.getElementById('formulario.vanenf.c62').value } catch (error) { }
                try { document.getElementById('formulario.vanenf.c64').min = document.getElementById('formulario.vanenf.c63').value } catch (error) { }

                break;

            case 'vih2':
                let v15 = document.getElementById('formulario.vih2.c1');
                let v24 = document.getElementById('formulario.vih2.c7');
                let v24_2 = document.getElementById('formulario.vih2.c9');
                let v24_5 = document.getElementById('formulario.vih2.c12');
                let v24_8 = document.getElementById('formulario.vih2.c15');
                let c17 = document.getElementById('formulario.vih2.c17');
                let c18 = document.getElementById('formulario.vih2.c18');
                let c19 = document.getElementById('formulario.vih2.c19');
                let c20 = document.getElementById('formulario.vih2.c20');
                let c2 = document.getElementById('formulario.vih2.c2');


                switch (campo) {
                    case 'c1':
                        cambioValor.clear()
                        // variable 19
                        cambioValor.set('c2', '1845-01-01')

                        // variable 20
                        cambioValor.set('c17', '7')
                        cambioValor.set('c3', '1845-01-01')

                        // variable 21
                        cambioValor.set('c18', '9')
                        cambioValor.set('c4', '1845-01-01')


                        // variable 22
                        cambioValor.set('c19', '9')
                        cambioValor.set('c5', '1845-01-01')


                        // variable 23
                        cambioValor.set('c20', '9')
                        cambioValor.set('c6', '1845-01-01')

                        // variable 24
                        cambioValor.set('c7', '9')

                        // variable 24.1
                        cambioValor.set('c8', '99')

                        // variable 24.2
                        cambioValor.set('c9', '9')

                        // variable 24.3
                        cambioValor.set('c10', '99')

                        // variable 24.4
                        cambioValor.set('c11', '9')

                        // variable 24.5
                        cambioValor.set('c12', '9')

                        // variable 24.6
                        cambioValor.set('c13', '1845-01-01')

                        // variable 24.7
                        cambioValor.set('c14', '9')

                        // variable 24.8
                        cambioValor.set('c15', 'NA')

                        // variable 24.9
                        cambioValor.set('c16', '9')
                        if (v15.value >= 3) {
                            asignaValorCamposHc(tabla, cambioValor, true)
                        }
                        else {
                            asignaValorCamposHc(tabla, cambioValor, false)
                        }
                        break;

                    case 'c2':
                        valores_a_mandar = '';
                        add_valores_a_mandar('c4');
                        add_valores_a_mandar('vihf');
                        add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
                        traerDatoPlantilla(valores_a_mandar).then((result) => {
                            if (result < c2.value) {
                                cambioValor.clear()
                                cambioValor.set('c17', '6')
                                asignaValorCamposHc(tabla, cambioValor, true)
                                validacionesHcVih('vih2', 'c17');
                            } else {
                                cambioValor.clear();
                                cambioValor.set('c17', '');
                                cambioValor.set('c3', '');
                                cambioValor.set('c18', '');
                                cambioValor.set('c4', '');
                                cambioValor.set('c19', '');
                                cambioValor.set('c5', '');
                                cambioValor.set('c20', '');
                                cambioValor.set('c6', '');
                                cambioValor.set('c7', '');
                                asignaValorCamposHc(tabla, cambioValor, false);
                            }
                        }).catch((err) => {
                            console.error(err)
                        });
                        break;

                    case 'c17':
                        // V20
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('4', '1799-01-01');
                        valorDependiente.set('5', '1822-02-01');
                        valorDependiente.set('6', '1833-03-03');
                        valorDependiente.set('7', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c3'], c17, valorDependiente, tabla);

                        cambioValor.clear();
                        valorDependiente.clear();
                        valorDependiente.set('1', '8');
                        valorDependiente.set('2', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c18', 'c19', 'c20'], c17, valorDependiente, tabla);

                        cambioValor.clear();
                        valorDependiente.clear();
                        valorDependiente.set('1', '1833-03-03');
                        valorDependiente.set('2', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c4', 'c5', 'c6'], c17, valorDependiente, tabla);

                        cambioValor.clear();
                        cambioValor.set('c18', '8')
                        cambioValor.set('c4', '1833-03-03')
                        cambioValor.set('c19', '8')
                        cambioValor.set('c5', '1833-03-03')
                        cambioValor.set('c20', '8')
                        cambioValor.set('c6', '1833-03-03')
                        cambioValor.set('c7', '7')
                        cambioValor.set('c8', '1')
                        if (c17.value == '6') {
                            asignaValorCamposHc(tabla, cambioValor, true)
                        }
                        break;

                    case 'c18':
                        // V21
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('4', '1799-01-01');
                        valorDependiente.set('5', '1800-01-01');
                        valorDependiente.set('6', '1811-01-01');
                        valorDependiente.set('7', '1822-02-01');
                        valorDependiente.set('8', '1833-03-03');
                        valorDependiente.set('9', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c4'], c18, valorDependiente, tabla);

                        cambioValor.clear();
                        valorDependiente.clear();
                        valorDependiente.set('1', '8');
                        valorDependiente.set('2', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c19', 'c20'], c18, valorDependiente, tabla);

                        cambioValor.clear();
                        valorDependiente.clear();
                        valorDependiente.set('1', '1833-03-03');
                        valorDependiente.set('2', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c5', 'c6'], c18, valorDependiente, tabla);
                        break;

                    case 'c19':
                        // V22
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('4', '1799-01-01');
                        valorDependiente.set('5', '1800-01-01');
                        valorDependiente.set('6', '1811-01-01');
                        valorDependiente.set('7', '1822-02-01');
                        valorDependiente.set('8', '1833-03-03');
                        valorDependiente.set('9', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c5'], c19, valorDependiente, tabla);

                        cambioValor.clear();
                        valorDependiente.clear();
                        valorDependiente.set('1', '8');
                        valorDependiente.set('2', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c20'], c19, valorDependiente, tabla);

                        cambioValor.clear();
                        valorDependiente.clear();
                        valorDependiente.set('1', '1833-03-03');
                        valorDependiente.set('2', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c6'], c19, valorDependiente, tabla);
                        break;

                    case 'c20':
                        // V23
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('4', '1799-01-01');
                        valorDependiente.set('5', '1800-01-01');
                        valorDependiente.set('6', '1811-01-01');
                        valorDependiente.set('7', '1822-02-01');
                        valorDependiente.set('8', '1833-03-03');
                        valorDependiente.set('9', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c6'], c20, valorDependiente, tabla);
                        break;

                    case 'c7':
                        // V24
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '1');
                        asignaValorCamposHcDependiente(cambioValor, ['c8'], v24, valorDependiente, tabla);
                        break;

                    case 'c9':
                        // V24.2
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('1', '1');
                        valorDependiente.set('3', '98');
                        valorDependiente.set('4', '98');
                        asignaValorCamposHcDependiente(cambioValor, ['c10'], v24_2, valorDependiente, tabla);
                        break;

                    case 'c12':
                        // V24.5
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('4', '1811-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c13'], v24_5, valorDependiente, tabla);
                        break;

                    case 'c15':
                        // V24.8
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NA', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c16'], v24_8, valorDependiente, tabla);
                        break;

                    default:
                        break;
                }
                break;

            case 'vih3':
                let v16 = document.getElementById('formulario.vih3.c1')
                let v25 = document.getElementById('formulario.vih3.c2')
                let c14 = document.getElementById('formulario.vih3.c14')
                let c15 = document.getElementById('formulario.vih3.c15')
                let c16 = document.getElementById('formulario.vih3.c16')
                valores_a_mandar = '';
                add_valores_a_mandar('c1');
                add_valores_a_mandar('vih2');
                add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
                traerDatoPlantilla(valores_a_mandar).then((result) => {
                    if (result < 3 && result != '') {
                        if (v16.value != 'NO') {
                            v16.click();
                        }
                        v16.parentNode.style.pointerEvents = "none";
                    }
                });

                switch (campo) {
                    case 'c1':

                        cambioValor.clear()
                        // V25
                        cambioValor.set('c2', 'NA')

                        //V25.1
                        cambioValor.set('c3', '9')

                        //V25.2
                        cambioValor.set('c4', '9')

                        //V26
                        cambioValor.set('c5', '9')

                        //V27
                        cambioValor.set('c6', '9')

                        //V28
                        cambioValor.set('c7', '1845-01-01')

                        //V28.1
                        cambioValor.set('c8', '99999999')

                        //V29
                        cambioValor.set('c15', '')
                        cambioValor.set('c9', '1845-01-01')

                        //V29.1
                        cambioValor.set('c10', '99999999')

                        //V30
                        cambioValor.set('c16', '0')
                        cambioValor.set('c11', '1845-01-01')

                        //V30.1
                        cambioValor.set('c12', '99999999')

                        //V31
                        cambioValor.set('c13', '9')
                        if (v16.value == 'NO') {
                            asignaValorCamposHc(tabla, cambioValor, true)
                        }
                        else {
                            asignaValorCamposHc(tabla, cambioValor, false)
                        }
                        break;

                    case 'c2':
                        // V25
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NA', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c3'], v25, valorDependiente, tabla);
                        break;

                    case 'c14':
                        // V28
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1800-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c7'], c14, valorDependiente, tabla);

                        // V28.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '99999999');
                        asignaValorCamposHcDependiente(cambioValor, ['c8'], c14, valorDependiente, tabla);
                        break;

                    case 'c15':
                        // V29
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1800-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c9'], c15, valorDependiente, tabla);

                        // V29.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '99999999');
                        asignaValorCamposHcDependiente(cambioValor, ['c10'], c15, valorDependiente, tabla);
                        break;

                    case 'c16':
                        // V30
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('2', '1800-01-01');
                        valorDependiente.set('3', '1833-03-01');
                        valorDependiente.set('0', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c11'], c16, valorDependiente, tabla);

                        // V30.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('2', '99999999');
                        valorDependiente.set('3', '99999999');
                        valorDependiente.set('0', '99999999');
                        asignaValorCamposHcDependiente(cambioValor, ['c12'], c16, valorDependiente, tabla);
                        break;

                    default:
                        break;
                }
                break;

            case 'vih4':
                let v17 = document.getElementById('formulario.vih4.c1')
                let v68 = document.getElementById('formulario.vih4.c5')
                let c4 = document.getElementById('formulario.vih4.c4')

                valores_a_mandar = '';
                add_valores_a_mandar('c1');
                add_valores_a_mandar('vih0');
                add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
                traerDatoPlantilla(valores_a_mandar).then((result) => {
                    if (result == '2') {
                        cambioValor.clear();
                        cambioValor.set('c1', '3');
                        asignaValorCamposHc(tabla, cambioValor, true);
                        //validacionesHcVih(tabla, 'c1')
                        cambioValor.clear()

                        // V32
                        cambioValor.set('c2', '1845-01-01');

                        // V33
                        cambioValor.set('c4', '0');
                        cambioValor.set('c3', '1845-01-01');
                        asignaValorCamposHc(tabla, cambioValor, true)
                    }
                });

                switch (campo) {
                    case 'c1':
                        cambioValor.clear()

                        // V32
                        cambioValor.set('c2', '1845-01-01');

                        // V33
                        cambioValor.set('c4', '0');
                        cambioValor.set('c3', '1845-01-01');
                        if (v17.value >= 3) {
                            asignaValorCamposHc(tabla, cambioValor, true)
                        }
                        else {
                            asignaValorCamposHc(tabla, cambioValor, false)
                        }
                        break;

                    case 'c5':
                        cambioValor.clear();

                        //V68.1
                        cambioValor.set('c6', '6');

                        //V68.2
                        cambioValor.set('c7', '8');

                        //V68.3
                        cambioValor.set('c8', '1799-01-01');

                        //V68.4
                        cambioValor.set('c9', '9');

                        //V68.5
                        cambioValor.set('c10', '9');

                        //V68.6
                        cambioValor.set('c11', '9');

                        //V68.7
                        cambioValor.set('c12', '9');

                        //V68.8
                        cambioValor.set('c13', '9');

                        //V68.9
                        cambioValor.set('c14', '9');

                        //V68.10
                        cambioValor.set('c15', '9');

                        //V68.11
                        cambioValor.set('c16', '9');

                        //V68.12
                        cambioValor.set('c17', '9');

                        //V68.13
                        cambioValor.set('c18', '1799-01-01');

                        //V68.14
                        cambioValor.set('c19', '10');
                        if (v68.value == 'NO') {
                            asignaValorCamposHc(tabla, cambioValor, true)
                        }
                        else {
                            asignaValorCamposHc(tabla, cambioValor, false)
                        }
                        break;

                    case 'c4':
                        // V33
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('2', '1799-01-01');
                        valorDependiente.set('3', '1822-02-01');
                        valorDependiente.set('4', '1833-03-03');
                        valorDependiente.set('0', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c3'], c4, valorDependiente, tabla);
                        break;

                    default:
                        break;
                }
                break;

            case 'vihf':
                let v18 = document.getElementById('formulario.vihf.c15')
                let v39 = document.getElementById('formulario.vihf.c10')
                let v40 = document.getElementById('formulario.vihf.c11')
                let v40_1 = document.getElementById('formulario.vihf.c12')
                let c1 = document.getElementById('formulario.vihf.c1')
                let vc6 = document.getElementById('formulario.vihf.c6')

                try { document.getElementById('formulario.vihf.c4').min = document.getElementById('formulario.vihf.c2').value } catch (error) { }
                try { document.getElementById('formulario.vihf.c8').min = document.getElementById('formulario.vihf.c4').value } catch (error) { }

                switch (campo) {
                    
                    case 'c6':
                        cambioValor.clear()
                        cambioValor.set('c16', '');

                        if (vc6.value == '100') {
                            //alert('NO ES NECESARIO QUE DILIGENCIE LA HISTORIA CLINICA, PUEDE FIRMAR EL FOLIO');
                            asignaValorCamposHc(tabla, cambioValor, false);
                        }
                        else {
                            asignaValorCamposHc(tabla, cambioValor, true);
                        }

                        break;
                    case 'c15':
                        cambioValor.clear()

                        cambioValor.set('c1', '5');
                        // V34
                        cambioValor.set('c2', '1845-01-01');

                        // V35
                        cambioValor.set('c3', '9');

                        // V36
                        cambioValor.set('c4', '1845-01-01');

                        // V36.1
                        cambioValor.set('c5', '9');

                        // V36.2
                        cambioValor.set('c6', '9');

                        // V37
                        cambioValor.set('c8', '1845-01-01');

                        // V38
                        cambioValor.set('c9', '9');

                        // V39
                        cambioValor.set('c10', '9');

                        // V40
                        cambioValor.set('c11', '9');

                        // V40.1
                        cambioValor.set('c12', '9999');

                        // V41
                        cambioValor.set('c13', '9');

                        // V41.1
                        cambioValor.set('c14', '99999999');
                        if (v18.value == '2') {
                            alert('NO ES NECESARIO QUE DILIGENCIE LA HISTORIA CLINICA, PUEDE FIRMAR EL FOLIO');
                            asignaValorCamposHc(tabla, cambioValor, true);
                        }
                        else {
                            asignaValorCamposHc(tabla, cambioValor, false);
                        }
                        break;

                    case 'c1':
                        // V34
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('2', '1799-01-01');
                        valorDependiente.set('3', '1811-11-01');
                        valorDependiente.set('4', '1822-02-01');
                        valorDependiente.set('5', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c2'], c1, valorDependiente, tabla);
                        break;

                    case 'c11':
                        valorDependiente.clear();
                        cambioValor.clear();
                        valorDependiente.set('2', '9998');
                        valorDependiente.set('3', '9997');
                        valorDependiente.set('4', '9997');
                        valorDependiente.set('9', '9999');
                        asignaValorCamposHcDependiente(cambioValor, ['c12'], v40, valorDependiente, tabla);
                        break;

                    case 'c12':
                        if (v40.value == '1') {
                            if (v40_1.value < 200 && v40_1.value != '' && v39.value != '3') {
                                if (confirm('ESTADIO EN VARIABLE 39 NO ES ACORDE AL VALOR DE CD4 VARIABLE 40.1. \u00BFDESEA MODIFICAR EL ESTADIO?')) {
                                    cambioValor.clear();
                                    cambioValor.set('c10', '3')
                                    asignaValorCamposHc(tabla, cambioValor, true)
                                    validacionesHcVih('vihf', 'c10');
                                };
                            }
                            else if (v40_1.value >= 200 && v40_1.value <= 499 && v39.value != '2') {
                                if (confirm('ESTADIO EN VARIABLE 39 NO ES ACORDE AL VALOR DE CD4 VARIABLE 40.1. \u00BFDESEA MODIFICAR EL ESTADIO?')) {
                                    cambioValor.clear();
                                    cambioValor.set('c10', '2')
                                    asignaValorCamposHc(tabla, cambioValor, true)
                                };
                            }
                            else if (v40_1.value >= 500 && v39.value != '1') {
                                if (confirm('ESTADIO EN VARIABLE 39 NO ES ACORDE AL VALOR DE CD4 VARIABLE 40.1. \u00BFDESEA MODIFICAR EL ESTADIO?')) {
                                    cambioValor.clear();
                                    cambioValor.set('c10', '1')
                                    asignaValorCamposHc(tabla, cambioValor, true)
                                };
                            }
                        }
                        break;
                    default:
                        break;
                }
                break;

            case 'vih6':
                let c26 = document.getElementById('formulario.vih6.c26')
                let var24 = document.getElementById('formulario.vih6.c24')
                switch (campo) {

                    case 'c24':
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('2', '0');
                        valorDependiente.set('3', '0');
                        valorDependiente.set('4', '0');
                        valorDependiente.set('5', '0');
                        valorDependiente.set('9', '99');
                        asignaValorCamposHcDependiente(cambioValor, ['c25'], var24, valorDependiente, tabla);
                        break;


                    case 'c26':
                        // V42
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('2', '1788-01-01');
                        valorDependiente.set('3', '1799-01-01');
                        valorDependiente.set('4', '1845-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c1'], c26, valorDependiente, tabla);

                        // V42.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '9- NO APLICA');
                        valorDependiente.set('4', '9- NO APLICA');
                        asignaValorCamposHcDependiente(cambioValor, ['c2', 'c3', 'c4', 'c5', 'c6'], c26, valorDependiente, tabla);

                        // V43
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '5');
                        valorDependiente.set('4', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c7', 'c9', 'c14'], c26, valorDependiente, tabla);

                        // V43.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '9996');
                        valorDependiente.set('4', '9999');
                        asignaValorCamposHcDependiente(cambioValor, ['c8'], c26, valorDependiente, tabla);


                        // V44.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '99999996');
                        valorDependiente.set('4', '99999999');
                        asignaValorCamposHcDependiente(cambioValor, ['c10'], c26, valorDependiente, tabla);

                        // V45
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '16');
                        valorDependiente.set('4', '99');
                        asignaValorCamposHcDependiente(cambioValor, ['c11'], c26, valorDependiente, tabla);

                        // V46
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '6');
                        valorDependiente.set('4', '9');
                        asignaValorCamposHcDependiente(cambioValor, ['c12', 'c13'], c26, valorDependiente, tabla);

                        // V49
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '0');
                        valorDependiente.set('4', '0');
                        asignaValorCamposHcDependiente(cambioValor, ['c15'], c26, valorDependiente, tabla);

                        // V51
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '5');
                        asignaValorCamposHcDependiente(cambioValor, ['c17', 'c24'], c26, valorDependiente, tabla);

                        // FALTA AGREGAR COMBO VARIABLE 51.1

                        //V51.2
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('3', '10');
                        asignaValorCamposHcDependiente(cambioValor, ['c19'], c26, valorDependiente, tabla);

                        break;

                    default:
                        break;
                }

                break;

            case 'vih7':
                let v52_2 = document.getElementById('formulario.vih7.c2');
                let v52_5 = document.getElementById('formulario.vih7.c5');
                let v52_7 = document.getElementById('formulario.vih7.c7');
                let v52_9 = document.getElementById('formulario.vih7.c9');
                let v52_10 = document.getElementById('formulario.vih7.c10');
                let v52_11 = document.getElementById('formulario.vih7.c11');
                let v52_12 = document.getElementById('formulario.vih7.c12');
                let v52_14 = document.getElementById('formulario.vih7.c14');
                let v52_15 = document.getElementById('formulario.vih7.c15');
                let v52_16 = document.getElementById('formulario.vih7.c16');
                let v52_17 = document.getElementById('formulario.vih7.c17');
                let v52_18 = document.getElementById('formulario.vih7.c18');
                let v52_19 = document.getElementById('formulario.vih7.c19');
                let v52_21 = document.getElementById('formulario.vih7.c21');
                let v52_22 = document.getElementById('formulario.vih7.c22');

                valores_a_mandar = '';
                add_valores_a_mandar('c4');
                add_valores_a_mandar('vihf');
                add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
                traerDatoPlantilla(valores_a_mandar).then((result) => {
                    if (result != '1845-01-01' && result != '') {
                        switch (campo) {
                            case 'c1':
                                // V52.1
                                try {
                                    let v52_1 = document.getElementById('formulario.vih7.c1');
                                    cambioValor.clear()
                                    valorDependiente.clear();
                                    valorDependiente.set('SI', '3');
                                    asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_1, valorDependiente, 'vih9');
                                } catch (error) { console.error(error); }
                                break;

                            case 'c2':
                                // V52.2
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_2, valorDependiente, 'vih9');
                                break;

                            case 'c3':
                                //V52.3
                                let v52_3 = document.getElementById('formulario.vih7.c3');
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_3, valorDependiente, 'vih9');
                                break;

                            case 'c4':
                                // V52.4
                                let v52_4 = document.getElementById('formulario.vih7.c4');
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_4, valorDependiente, 'vih9');
                                break;

                            case 'c5':
                                // V52.5
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_5, valorDependiente, 'vih9');
                                break;

                            case 'c6':
                                // V52.6
                                let v52_6 = document.getElementById('formulario.vih7.c6');
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_6, valorDependiente, 'vih9');
                                break;

                            case 'c7':
                                // V52.7
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_7, valorDependiente, 'vih9');
                                break;

                            case 'c8':
                                // V52.8
                                let v52_8 = document.getElementById('formulario.vih7.c8');
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_8, valorDependiente, 'vih9');
                                break;

                            case 'c9':
                                // V52.9
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_9, valorDependiente, 'vih9');
                                break;

                            case 'c10':
                                // V52.10
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_10, valorDependiente, 'vih9');
                                break;

                            case 'c11':
                                // V52.11
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_11, valorDependiente, 'vih9');
                                break;

                            case 'c12':
                                // V52.12
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_12, valorDependiente, 'vih9');
                                break;

                            case 'c14':
                                // V52.14
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_14, valorDependiente, 'vih9');
                                break;

                            case '15':
                                // V52.15
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_15, valorDependiente, 'vih9');
                                break;

                            case 'c16':
                                // V52.16
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_16, valorDependiente, 'vih9');
                                break;

                            case 'c17':
                                // V52.17
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_17, valorDependiente, 'vih9');
                                break;

                            case 'c18':
                                // V52.18
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_18, valorDependiente, 'vih9');
                                break;

                            case 'c19':
                                // V52.19
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_19, valorDependiente, 'vih9');
                                break;


                            case 'c20':
                                // V52.20
                                let v52_20 = document.getElementById('formulario.vih7.c20');
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_20, valorDependiente, 'vih9');
                                break;

                            case 'c21':
                                // V52.21
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_21, valorDependiente, 'vih9');
                                break;

                            case 'c22':
                                // V52.22
                                cambioValor.clear()
                                valorDependiente.clear();
                                valorDependiente.set('SI', '3');
                                asignaValorCamposHcDependiente(cambioValor, ['c3'], v52_22, valorDependiente, 'vih9');
                                break;

                            default:
                                break;
                        }

                        if (parseInt(result.split('-')[0]) >= 2017) {
                            cambioValor.clear()
                            disabledForm = ['c21', 'c22'];
                            disabledForm.forEach((campo => {
                                cambioValor.set(campo, '9')
                            }));
                            asignaValorCamposHc(tabla, cambioValor, true);
                        }

                    }
                    else {
                        cambioValor.clear()
                        disabledForm = ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10', 'c11', 'c12', 'c13', 'c14', 'c15', 'c16', 'c17', 'c18', 'c19', 'c20', 'c21', 'c22']

                        disabledForm.forEach((campo => {
                            cambioValor.set(campo, '9')
                        }));

                        asignaValorCamposHc(tabla, cambioValor, true)
                    }
                });
                break;

            case 'vih9':
                let c24 = document.getElementById('formulario.vih9.c24');
                let c23 = document.getElementById('formulario.vih9.c23');
                let c26Eal = document.getElementById('formulario.vih9.c26');
                let c33 = document.getElementById('formulario.vih9.c33');
                let c27 = document.getElementById('formulario.vih9.c27');
                let c28 = document.getElementById('formulario.vih9.c28');
                let c29 = document.getElementById('formulario.vih9.c29');
                let c30 = document.getElementById('formulario.vih9.c30');
                let c31 = document.getElementById('formulario.vih9.c31');
                let c32 = document.getElementById('formulario.vih9.c32');

                if(document.getElementById('formulario.vih9.c24').value =='SI' && document.getElementById('formulario.vih9.c2').value  != '1845-01-01'){
                    
                    try { document.getElementById('formulario.vih9.c34').disabled= false  } catch (error) { }
                }else {
                    try { document.getElementById('formulario.vih9.c34').disabled= true } catch (error) { }
                }

            
                switch (campo) {
                    case 'c24':
                        // V54
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c2'], c24, valorDependiente, tabla);
    
                        break;

                    case 'c23':
                        // V56
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c4'], c23, valorDependiente, tabla);

                        // V56.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '998');
                        asignaValorCamposHcDependiente(cambioValor, ['c5'], c23, valorDependiente, tabla);
                        break;

                    case 'c26':
                        // V57
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c6'], c26Eal, valorDependiente, tabla);

                        // V57.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '9998');
                        asignaValorCamposHcDependiente(cambioValor, ['c7'], c26Eal, valorDependiente, tabla);
                        break;

                    case 'c33':
                        // V58
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c8'], c33, valorDependiente, tabla);

                        // V58.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '9998');
                        asignaValorCamposHcDependiente(cambioValor, ['c9'], c33, valorDependiente, tabla);
                        break;


                    case 'c27':
                        // V59
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c10'], c27, valorDependiente, tabla);

                        // V59.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '9998');
                        asignaValorCamposHcDependiente(cambioValor, ['c11'], c27, valorDependiente, tabla);
                        break;

                    case 'c28':
                        // V60
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c12'], c28, valorDependiente, tabla);

                        // V60.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '9998');
                        asignaValorCamposHcDependiente(cambioValor, ['c13'], c28, valorDependiente, tabla);
                        break;

                    case 'c29':
                        // V61
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c14'], c29, valorDependiente, tabla);

                        // V61.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '9998');
                        asignaValorCamposHcDependiente(cambioValor, ['c15'], c29, valorDependiente, tabla);
                        break;

                    case 'c30':
                        // V62
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c16'], c30, valorDependiente, tabla);

                        // V62.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '998');
                        asignaValorCamposHcDependiente(cambioValor, ['c17'], c30, valorDependiente, tabla);
                        break;

                    case 'c31':
                        // V75
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c19'], c31, valorDependiente, tabla);
                        break;

                    case 'c32':
                        // V75.1
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '1799-01-01');
                        asignaValorCamposHcDependiente(cambioValor, ['c21'], c32, valorDependiente, tabla);
                        break;

                    default:
                        break;
                }
                break;

            case 'vihD':
                let v84 = document.getElementById('formulario.vihD.c7');
                let c21 = document.getElementById('formulario.vihD.c21');

                cambioValor.clear();
                cambioValor.set('c14', document.getElementById('txtc1Rc').value);
                asignaValorCamposHc('vihD', cambioValor, true)

                switch (campo) {
                    case 'c7':
                        //V84
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('6', '10');
                        asignaValorCamposHcDependiente(cambioValor, ['c8'], v84, valorDependiente, tabla);
                        break;

                    case 'c21':
                        //90
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NO', '98');
                        asignaValorCamposHcDependiente(cambioValor, ['c14'], c21, valorDependiente, tabla);
                        break;

                    default:
                        break;
                }
                break;


            case 'citv':
                let c3 = document.getElementById('formulario.citv.c3');
                let c5 = document.getElementById('formulario.citv.c5');
                switch (campo) {
                    case 'c3':
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NORMAL', 'NEGATIVO PARA LESION INTRAEPITELIAL O MALIGNIDAD');
                        asignaValorCamposHcDependiente(cambioValor, ['c7'], c3, valorDependiente, tabla);
                        break;

                    case 'c5':
                        cambioValor.clear()
                        valorDependiente.clear();
                        valorDependiente.set('NORMAL', 'NEGATIVO PARA LESION INTRAEPITELIAL O MALIGNIDAD');
                        asignaValorCamposHcDependiente(cambioValor, ['c6'], c5, valorDependiente, tabla);
                        break;
                }
                break;
        }
        resolve();
    });
}

function asignaValorCamposHcDependiente(dict, campos, variable, dependiente, tabla) {
    let bandera = false
    dict.clear();
    let contador = 0;
    dependiente.forEach((valor, condicion) => {
        contador++
        try {
            if (variable.value == condicion) {
                campos.forEach((campo) => {
                    dict.set(campo, valor);
                    bandera = true;
                })
            }
        } catch (error) {
            console.error(`Error en la funcion asignaValorCamposHcDependiente() porque no se puede extraer el valor de la variable porque no existe`)
        }
        if (contador == dependiente.size) {
            if (!bandera) {
                campos.forEach((campo) => {
                    dict.set(campo, '');
                })
            }
            asignaValorCamposHc(tabla, dict, bandera);
        }
    })
}

function asignaValorCamposHc(tabla, dict, disabled) {
    let variables = [];
    let contador = 0;
    let validador = [];
    dict.forEach((valor, campo) => {
        validador.push(campo);
        if (disabled) {
            variables.push({ 'id': `${tabla}.${campo}`, 'valor': valor.replaceAll("'", "''") });
            try {
                document.getElementById(`formulario.${tabla}.${campo}`).value = valor;
                if (document.getElementById(`formulario.${tabla}.${campo}`).parentNode.className == 'onoffswitch2') {
                    document.getElementById(`formulario.${tabla}.${campo}`).parentNode.style.pointerEvents = "none";
                }
            } catch (error) {
                console.error(`Fallo la asignar el valor porque no existe en el tab ${error}`);
            }
        }
        else {
            variables.push({ 'id': `${tabla}.${campo}`, 'valor': '' });
            //variables.push({'id': `${tabla}.${campo}`, 'valor': valor.replaceAll("'", "''")});
            try {
                document.getElementById(`formulario.${tabla}.${campo}`).value = '';
                if (document.getElementById(`formulario.${tabla}.${campo}`).parentNode.className == 'onoffswitch2') {
                    document.getElementById(`formulario.${tabla}.${campo}`).parentNode.style.pointerEvents = "";
                }
            } catch (error) {
                console.error(`Fallo la asignar el valor porque no existe en el tab ${error}`);
            }
        }
        try {
            document.getElementById(`formulario.${tabla}.${campo}`).disabled = disabled
        } catch (error) {

        }
        contador++

        if (contador == dict.size) {
            valores_a_mandar = '';
            add_valores_a_mandar(JSON.stringify(variables));
            add_valores_a_mandar(LoginSesion());
            add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
            var urls = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
            $.ajax({
                url: urls,
                type: "POST",
                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                data: {
                    // crearquery
                    "idQuery": 3054,
                    "parametros": valores_a_mandar,
                    "_search": false,
                    "nd": 1611873687449,
                    "rows": -1,
                    "page": 1,
                    "sidx": "NO FACT",
                    "sord": "asc"
                },
                beforeSend: function () {
                },
                success: function (data) {
                    let respuesta = data.getElementsByTagName('cell');
                    if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "B24X") {
                        console.log('entro vhi---')
                        validacionesVih(tabla, validador)
                    }
                    if (respuesta.length != 0) {

                    } else {

                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        //guardarDatosPlantilla(tabla, campo, valor)
    });
}

function traerDatoPlantilla(valores_a_mandar) {
    return new Promise((resolve = () => { }, reject = () => { }) => {
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 3053,
                "parametros": valores_a_mandar,
                "_search": false,
                "nd": 1611873687449,
                "rows": -1,
                "page": 1,
                "sidx": "NO FACT",
                "sord": "asc"
            },
            beforeSend: function () {
                //mostrarLoader(urlParams.get("accion"))
            },
            success: function (data) {
                resolve(data.getElementsByTagName("cell")[1].firstChild.data);
            },
            complete: function (jqXHR, textStatus) {
                //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                reject();
                swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            }
        });
    });
}
