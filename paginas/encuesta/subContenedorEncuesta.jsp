<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />

<!DOCTYPE HTML >
<head>    
    <link href="/clinica/paginas/encuesta/templateStyles.css " rel="stylesheet" type="text/css">    
</head>
<body>
<%
    ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
    Connection conexion = iConnection.getConnection();
    Conexion cn = new Conexion(conexion);

    ControlAdmin beanAdmin = new ControlAdmin();
    beanAdmin.setCn(cn);

    ArrayList resultaux = new ArrayList();
    ArrayList resulCombo = new ArrayList();

    String var = request.getParameter("tipo");
    String vari = request.getParameter("id_parametro");    
    String idencuestaPaciente = request.getParameter("id_encuestaPaciente");
    String id_sexo = request.getParameter("id_sexo");
    String rango = request.getParameter("rango");
    String rango2 = request.getParameter("rango2");
    String edad = request.getParameter("edade");
    String bloquear = "";
    int totalSecciones = 1;
    String cardClass = "card-total";
    String heightTextArea = "120px";
    String widthTextArea = "100%";

    Map<Integer,List<String>> mapCampos = new HashMap<>();
    Map<Integer,List<String>> mapTitulos = new HashMap<>();    
    Map<Integer,Integer> mapColumnas = new HashMap<>();
    
    List<String> campos = new ArrayList<>();
    List<String> titulos = new ArrayList<>();    
    
    Integer seccionActual = 1;
    String campo = "";
    String titulo = "";
    String maxWidth = "";
    String widthCard = "";
    String flexCard = "";
    Boolean isEmpty = false;
    try{
        String queryCol = beanSession.cn.traerElQuery(1129).toString();
        PreparedStatement stmt = (PreparedStatement) new LoggableStatement(conexion, queryCol, 1005, 1007);
        stmt.setString(1, idencuestaPaciente);
        stmt.setString(2, var);
        ResultSet rs = stmt.executeQuery();
        if(rs.next() == false){
            isEmpty = true;
        }else{
            do{
                mapColumnas.put(rs.getInt("seccion"),rs.getInt("numero_columnas"));
                widthCard = rs.getString("ancho_secciones").trim();
            }while(rs.next());
        }        
        if(mapColumnas.size()>1){
            totalSecciones = mapColumnas.size();
            cardClass = "card";
        }
        if(widthCard.equals("auto")){
            flexCard = "flex: 1";
        }
    }            
    catch (Exception e){
        System.out.println("Consulta 1:" + e.getMessage());
    }
    
    if(isEmpty){%>
        <div id="principal">
            <div class="card-total">
                <p>TAMIZAJE NO DISPONIBLE PARA ESTE PACIENTE</p>                
            </div>
        </div>
    <%}else{
        try{
            String SQL = beanSession.cn.traerElQuery(1110).toString();
            PreparedStatement stmt = (PreparedStatement) new LoggableStatement(conexion, SQL, 1005, 1007);
            stmt.setString(1, idencuestaPaciente);
            stmt.setString(2, var);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                if (rs.getString("id_estado_encuesta").trim().equals("A")) {
                    bloquear = " ";
                } else {
                    bloquear = "disabled";
                }
    
                String validacion = "";
    
                if (rs.getString("validaciones") != null) {
                    validacion = "validarCampoEncuesta(this.id,'" + rs.getString("validaciones") + "');";
                }
    
                String actualizar = "";
    
                if (rs.getInt("formulas") > 0) {
                    actualizar = "actualizarCampoFormulaEncuesta(this.id, this.value);";
                }
    
                Integer column = mapColumnas.get(rs.getInt("id_padre_orden"));
                if(totalSecciones == 1){
                    if(column == 1){
                        maxWidth = "990px";
                    }else if(column == 2){
                        maxWidth = "495px";
                    }else if (column == 3){
                        maxWidth = "330px";
                    }else{
                        maxWidth = "248px";
                    }
                }else if(totalSecciones == 2){
                    if(column == 1){
                        maxWidth = "500px";
                    }else if(column == 2){
                        maxWidth = "250px";
                    }else if (column == 3){
                        maxWidth = "125px";
                    }else{
                        maxWidth = "100px";
                    }
                }else if(totalSecciones == 3){
                    if(column == 1){
                        maxWidth = "330px";
                    }else if(column == 2){
                        maxWidth = "170px";
                    }else if (column == 3){
                        maxWidth = "125px";
                    }else{
                        maxWidth = "100px";
                    }
                }else if(totalSecciones == 4){
                    if(column == 1){
                        maxWidth = "100px";
                    }else if(column == 2){
                        maxWidth = "100px";
                    }else if (column == 3){
                        maxWidth = "80px";
                    }else{
                        maxWidth = "75px";
                    }
                }
                else{
                    if(column == 1){
                        maxWidth = "150px";
                    }else if(column == 2){
                        maxWidth = "100px";
                    }else if (column == 3){
                        maxWidth = "75px";
                    }else{
                        maxWidth = "40px";
                    }
                }
    
                if(rs.getInt("id_padre_orden")>seccionActual){
                    seccionActual = seccionActual+1;                
                    while(mapColumnas.get(seccionActual) == null ){
                        seccionActual = seccionActual+1;
                    }
                    campos.clear();
                    titulos.clear();
                }
    
                if (rs.getString("id_jerarquia").equals("TI")) {
                    String etiqueta = rs.getString("maxlenght");
                    if(rs.wasNull()){
                        etiqueta = "3";
                    }
                    if(etiqueta.trim().equals("")){
                        etiqueta = "3";
                    }
                    etiqueta = etiqueta.trim();
                    String descripcion = rs.getString("descripcion");
                    String descripcionCapitalizada = descripcion.substring(0, 1).toUpperCase() + descripcion.substring(1).toLowerCase();
                    titulo = "<h" + etiqueta + ">" + descripcionCapitalizada + "</h" + etiqueta +">";
                    titulos.add(titulo);
                    mapTitulos.put(seccionActual,new ArrayList<>(titulos));
                }
                else if (rs.getString("id_jerarquia").equals("PA")) {
                    String descripcion = rs.getString("descripcion");
                    String descripcionCapitalizada = descripcion.substring(0, 1).toUpperCase() + descripcion.substring(1).toLowerCase();
                    campo = "<strong style='padding-bottom: 10px;'>"+ descripcionCapitalizada +"</strong>";
                    campos.add(campo);
                    mapCampos.put(seccionActual,new ArrayList<>(campos));
                }
                else{
                    campo = "<div onmouseover='showFullMessage(this)' onmouseout='hideMessageBox(this)' class='tooltip-container'><label class='label-class tooltip' style='max-width:" + maxWidth + " ;' id='label.formulario." + var + "." + rs.getString("referencia") +"' for='formulario." + var + "." + rs.getString("referencia")  +"'>" + rs.getString("descripcion") + "</label><span class='tooltiptext'>" + rs.getString("descripcion") + "</span></div>";
                    if (rs.getString("tipo_input").trim().equals("input")) {

                        if(rs.getString("campo_bloquea")==null){
                            campo = campo + "<input class='input'   type='text' maxlength=' " + rs.getString("maxlenght") + "' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        }else if(rs.getString("campo_bloquea").trim().equals("1")){
                            campo = campo + "<input class='input'   type='text' maxlength=' " + rs.getString("maxlenght") + "' title=' " + rs.getString("referencia") + "' readonly id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        }else{
                            campo = campo + "<input class='input'   type='text' maxlength=' " + rs.getString("maxlenght") + "' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        }

                        if (rs.getInt("id_formula") > 0) {                        
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";                        
                        }
                    }else if (rs.getString("tipo_input").trim().equals("inputl")){
                        if(rs.getString("campo_bloquea")==null){
                            campo = campo + "<input class='inputl'   type='text' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        }else if(rs.getString("campo_bloquea").trim().equals("1")){
                            campo = campo + "<input class='inputl'   type='text' title='" + rs.getString("referencia") + "' readonly id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        }else{
                            campo = campo + "<input class='inputl'   type='text' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        }
                        campo = campo + "&nbsp; &nbsp; <img width='18px' height='18px' align='middle' title='VEN52' id='idLupitaVentanitaDxIngrT' onclick=\"traerVentanitaFuncionesHcPlantilla(this.id,'actualizarDxIngresoHC','divParaVentanita','formulario." + var + "." + rs.getString("referencia") +"','"+ rs.getString("id_query_ventanita") +"')\" src='/clinica/utilidades/imagenes/acciones/buscar.png'" + bloquear + ">";                                     
                    }else if (rs.getString("tipo_input").trim().equals("hora")) {
                        campo = campo + "<input class='input'  type='time' step='600' title='" + rs.getString("referencia") +"' id='formulario." + var + "." + rs.getString("referencia") + "' name='" + var + "' value='" +  rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value); " + validacion + "\"" + bloquear + "/>";                    
                    }else if (rs.getString("tipo_input").trim().equals("numeros")) {
                        campo = campo + "<input class='input'   type='number' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeypress='javascript:return soloTelefono(event);' oninput=\"maxLongitudNumero(this.id,'" + rs.getString("maxlenght") + "');\" onkeyup=\"ingresarLimites(this.id,'" + rs.getString("maximo_input") + "','" + rs.getString("minimo_input") + "',this.value); alertasGenerales(this.id,'" + rs.getString("valor_alerta") + "', this.value);" + actualizar + "\" onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        if (rs.getInt("id_formula") > 0) {                        
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("decimal")) {
                        campo = campo + "<input class='input'  type='number' step='" + rs.getString("cantdecimales") + "' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeypress=\"javascript:return NumCheck(event, this.id, '"+ rs.getString("cantdecimales") +"', '" + rs.getString("maxlenght") + "');\" oninput=\"maxLongitudNumero(this.id,'" + rs.getString("maxlenght") + "');\" onkeyup=\"ingresarLimites(this.id,'" + rs.getString("maximo_input") + "','" + rs.getString("minimo_input") + "',this.value); alertasGenerales(this.id,'" + rs.getString("valor_alerta") + "', this.value);" + actualizar + "\" onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + validacion + "\"" + bloquear + " />";
                        if (rs.getInt("id_formula") > 0) {                        
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }                                     
                    }else if (rs.getString("tipo_input").trim().equals("check")) {
                        String estado = " ";
                        if (rs.getString("dato").equals("SI")) {
                            estado = "checked";
                        }
                        campo = campo + "<input class='input'  type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitchsn(this.id, this.checked);guardarDatosEncuesta(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + "\"" + bloquear + estado +  "/>";
                        if (rs.getInt("id_formula") > 0) {                        
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("switchsn")) {
                        String estado = " ";
                        String cam_bloquear = "";
                        String parametros = "";
                        String class_span1 = "";
                        String class_span2 = "";
                        String activar = "";
                        String activarFormula = "";
                        String bloquear_aux = "";
    
                        if (rs.getString("dato").equals("SI") || rs.getString("dato").equals("NO")) {
                            class_span1 = "onoffswitch-inner2";
                            class_span2 = "onoffswitch-switch2";
                            activar = "";
                            activarFormula = "";
                            if (rs.getString("dato").equals("SI")) {
                                estado = "checked";
                            }
                        } else {
                            class_span1 = "onoffswitch-inner2-disabled";
                            class_span2 = "onoffswitch-switch2-disabled";
                            if (rs.getString("id_estado_encuesta").trim().equals("A")) {
                                bloquear_aux = bloquear;
                                bloquear = "disabled";
                                activar = "activarSwitchsnE('" + var + "', '" + rs.getString("referencia") + "')";
                                activarFormula = "setTimeout(()=> {activarFormulaSwitch('" + var + "', '" + rs.getString("referencia") + "','SN')},300)";
                            }
                        }
    
                        if (rs.getString("campo_bloquea") == null || rs.getString("campo_bloquea").equals("")) {
                            cam_bloquear = " ";
                        } else {
                            parametros = rs.getString("campo_bloquea");
                            cam_bloquear = "bloquearMisCampos('" + parametros + "','" + var + "', this.checked);";
                        }
                        campo = campo + "<div class='centerSwitch'>";
                        campo = campo + "<div class='onoffswitch2' onclick=\"" + activar + ";" + activarFormula + "\">";
                        campo = campo + "<input class='onoffswitch-checkbox2'  type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitchsn(this.id, this.checked); guardarDatosEncuesta(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + actualizar + cam_bloquear + "\"" + bloquear + " " + estado +  "/>";
                        if (rs.getInt("id_formula") > 0) {                        
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                        campo = campo + "<label class='onoffswitch-label2' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                        campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                        campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                        campo = campo + "</label></div></div>";
                        bloquear = bloquear_aux;
                    }else if (rs.getString("tipo_input").trim().equals("switchS10")){
                        String estado = " ";
                        String cam_bloquear = "";
                        String parametros = "";
                        String class_span1 = "";
                        String class_span2 = "";
                        String activar = "";
                        String activarFormula = "";
                        String bloquear_aux = "";
    
                        if (rs.getString("dato").equals("1") || rs.getString("dato").equals("0")) {
                            class_span1 = "onoffswitch-inner2";
                            class_span2 = "onoffswitch-switch2";
                            activar = "";
                            activarFormula = "";
                            if (rs.getString("dato").equals("1")) {
                                estado = "checked";
                            }
                        } else {
                            class_span1 = "onoffswitch-inner2-disabled";
                            class_span2 = "onoffswitch-switch2-disabled";
                            if (rs.getString("id_estado_encuesta").trim().equals("A")) {
                                bloquear_aux = bloquear;
                                bloquear = "disabled";
                                activar = "activarSwitchS10('" + var + "', '" + rs.getString("referencia") + "')";
                                activarFormula = "setTimeout(()=> {activarFormulaSwitch('" + var + "', '" + rs.getString("referencia") + "','SN')},300)";
                            }
                        }
                        campo = campo + "<div class='centerSwitch'>";
                        campo = campo + "<div class='onoffswitch2' onclick=\"" + activar + ";" + activarFormula + "\">";
                        campo = campo + "<input class='onoffswitch-checkbox2'  type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitch10(this.id, this.checked); guardarDatosEncuesta(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + actualizar + "\"" + bloquear + " " + estado +  "/>";
                        if (rs.getInt("id_formula") > 0) {                        
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                        campo = campo + "<label class='onoffswitch-label2' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                        campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                        campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                        campo = campo + "</label></div></div>";
                        bloquear = bloquear_aux;
                    } else if (rs.getString("tipo_input").trim().equals("switch10")) {
                        String estado1 = " ";
                        String cam_bloquear = "";
                        String parametros = "";
                        String class_span1 = "";
                        String class_span2 = "";
                        String activar = "";
                        String activarFormula = "";
                        String bloquear_aux = "";
    
                        if (rs.getString("dato").equals("1") || rs.getString("dato").equals("0")) {
                            class_span1 = "onoffswitch-inner3";
                            class_span2 = "onoffswitch-switch3";
                            activar = "";
                            activarFormula = "";
                            if (rs.getString("dato").equals("1")) {
                                estado1 = "checked";
                            }
                        } else {
                            class_span1 = "onoffswitch-inner2-disabled";
                            class_span2 = "onoffswitch-switch2-disabled";
                            if (rs.getString("id_estado_encuesta").trim().equals("A")) {
                                bloquear_aux = bloquear;
                                bloquear = "disabled";
                                activar = "activarSwitch10E('" + var + "', '" + rs.getString("referencia") + "')";
                                activarFormula = "setTimeout(()=> {activarFormulaSwitch('" + var + "', '" + rs.getString("referencia") + "','SN')},300)";
                            }
                        }
                        campo = campo + "<div class='centerSwitch'>";
                        campo = campo + "<div class='onoffswitch3' onclick=\"" + activar + ";" + activarFormula + "\">";
                        campo = campo + "<input class='onoffswitch-checkbox3'  type='checkbox' title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick=\"valorSwitch10(this.id, this.checked); guardarDatosEncuesta(this.name, '" + rs.getString("referencia") + "', this.value);" + validacion + actualizar + "\"" + bloquear + " " + estado1 +  "/>";
                        if (rs.getInt("id_formula") > 0) {                        
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                        campo = campo + "<label class='onoffswitch-label3' for='formulario." + var + "." + rs.getString("referencia") + "'>";
                        campo = campo + "<span class='" + class_span1 + "' id='" + var + "-" + rs.getString("referencia") + "-span1'></span>";
                        campo = campo + "<span class='" + class_span2 + "' id='" + var + "-" + rs.getString("referencia") + "-span2'></span>";
                        campo = campo + "</label></div></div>";
                        bloquear = bloquear_aux;                    
                    }else if (rs.getString("tipo_input").trim().equals("textadc")) {
                        if(!rs.getString("ancho").trim().equals("%") || !rs.getString("ancho").trim().equals("") || rs.getString("ancho").trim() != null){
                            widthTextArea = rs.getString("ancho").trim();
                        }
                        if(!rs.getString("altura").trim().equals("px") || !rs.getString("altura").trim().equals("") || rs.getString("altura").trim() != null){
                            heightTextArea = rs.getString("altura").trim();
                        }
                        campo = campo + "<textarea class='textArea' style='width:" + widthTextArea + " ; height: " + heightTextArea + ";'  title='" + rs.getString("referencia") + "' id='txtTexArea." + rs.getString("referencia") + "' name='" + rs.getString("referencia") + "' onkeypress='return validarKey(event, this.id)' placeholder='Ingresar texto...' onblur=\"guardarDatosEncuesta('" + var + "', this.name, this.value);\"" + bloquear  + ">" + rs.getString("dato") +"</textarea>";
                    }
                    else if(rs.getString("tipo_input").trim().equals("texta")){
                        if(!rs.getString("ancho").trim().equals("%") || !rs.getString("ancho").trim().equals("") || rs.getString("ancho").trim() != null){
                            widthTextArea = rs.getString("ancho").trim();
                        }
                        if(!rs.getString("altura").trim().equals("px") || !rs.getString("altura").trim().equals("") || rs.getString("altura").trim() != null){
                            heightTextArea = rs.getString("altura").trim();
                        }
                        campo = campo + "<textarea class='textArea' style='width: " + widthTextArea + "; height: " + heightTextArea + ";'  title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia") + "' name='" + rs.getString("referencia") + "' placeholder='Ingresar texto...' onblur=\"guardarDatosEncuesta('" + var + "', this.name, this.value);\"" + bloquear  + ">" + rs.getString("dato") +"</textarea>";
                    }else if (rs.getString("tipo_input").trim().equals("combo")){
                        campo = campo + "<select class='selectInput' size='1'  title='" + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia") + "' name='" + rs.getString("referencia") + "' onchange=\"guardarDatosEncuesta('" + var + "',this.name,this.value);" + actualizar + validacion + "\"" + bloquear + ">";
                        campo = campo + "<option value=''></option>";
                        resulCombo.clear();
                        resulCombo = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(rs.getString("id_query_combo")));
                        ComboVO cmb;
                        for (int i = 0; i < resulCombo.size(); i++) {
                            cmb = (ComboVO) resulCombo.get(i);
                            if (rs.getString("dato").trim().equals(cmb.getId())) {
                                campo = campo + "<option value='" + cmb.getId() + "' selected>" + cmb.getDescripcion() +"</option>";
                            }else{
                                campo = campo + "<option value='" + cmb.getId() + "'>" + cmb.getDescripcion() +"</option>";
                            }
                        }
                        campo = campo + "</select>";
                        if (rs.getInt("id_formula") > 0) {
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("inputFecha")) {
                        campo = campo + "<input class='input'   type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onkeyup='" + actualizar + "' onblur='validarFechaPatron(this.value, this.id)' onchange=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                        if (rs.getInt("id_formula") > 0) {
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("inputFechaPosterior")) {
                        campo = campo + "<input class='input'   type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 2);' onblur='validarFechaEscrita(this.value, this.id,2)' onchange=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                        if (rs.getInt("id_formula") > 0) {
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("inputFechaAnterior")) {
                        campo = campo + "<input class='input'   type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 1);' onblur='validarFechaEscrita(this.value, this.id,1)' onchange=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                        if (rs.getInt("id_formula") > 0) {
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("inputFechaNacimientoHoy")) {
                        campo = campo + "<input class='input'   type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 4);' onblur='validarFechaEscrita(this.value, this.id,4)' onchange=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                        if (rs.getInt("id_formula") > 0) {
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("inputFechaNacimiento")) {
                        campo = campo + "<input class='input'   type='date' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onclick='validarFechasPlantillas(this.id, 3);' onblur='validarFechaEscrita(this.value, this.id,3)' onchange=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);" + actualizar + validacion + "\"" + bloquear + " />";
                        if (rs.getInt("id_formula") > 0) {
                            campo = campo + "<input type='hidden' class='campoFormula' id='campoFormula" + rs.getInt("id_formula") + "' value='" + rs.getString("formula") + "'/>";
                            campo = campo + "<input type='hidden' class='input' name='"+ var +"' id='campoResultado" + rs.getInt("id_formula") + "' value='" + rs.getString("referencia_resultado") + "'/>";
                        }
                    }else if (rs.getString("tipo_input").trim().equals("input2")) {
                        campo = campo + "<input class='input'   type='text' maxlength=' " + rs.getString("maxlenght") + "' title=' " + rs.getString("referencia") + "' id='formulario." + var + "." + rs.getString("referencia")+ "' name='" + var + "' value='" + rs.getString("dato") + "' onblur=\"guardarDatosEncuesta(this.name,'" + rs.getString("referencia") + "',this.value);\"" + bloquear + " />";                    
                    }else if(rs.getString("tipo_input").trim().equals("button")) {
                        campo = campo + "<input type='button' value='GRAFICA' onclick='verImagen()' class='small button blue'" + bloquear + "/>" ;
                    }
                    campos.add(campo);                
                    mapCampos.put(seccionActual,new ArrayList<>(campos));
                }
            }
    %>
    <div id="version">
        <input value="v1" type="button" class="small button blue" 
            onclick="changeVersionPageTamizaje('v1','<%=var%>','<%=vari%>','<%=idencuestaPaciente%>','<%=id_sexo%>',
            '<%=rango%>','<%=rango2%>','<%=edad%>','desdeEncuesta')" style="width: 5%;">
    </div>
    <div id="principal">
        <%    
        for(Map.Entry<Integer, Integer> entry : mapColumnas.entrySet()){%>
            <div class='<%=cardClass%>' style='width: <%=widthCard%>; <%=flexCard%>'>
        <%
            List<String> renderTitulos = mapTitulos.get(entry.getKey());
            List<String> renderCampos = mapCampos.get(entry.getKey());        
            if(renderTitulos != null){%>
                <div class="titles">
                    <%
                    for(int t=0;t<renderTitulos.size();t++){%>                    
                        <%=renderTitulos.get(t)%>
                    <%}%>
                </div>
            <%}%>
            <br>        
            <%
            if(renderCampos != null){
                Integer columnas = entry.getValue();
                String col = "grid-template-columns: ";
                for(int c=0;c<columnas;c++){
                    col = col + "1fr ";
                }
                col = col + ";";
                %>
                <div class='card-content' style='<%=col%>'>
                    <%
                        for(int c=0;c<renderCampos.size();c++){%>
                            <div class="form-group">
                                <%=renderCampos.get(c)%>
                            </div>
                        <%}
                        %>
                </div>
            <%}
            %>
            </div>        
        <%}%>       
    </div>
    </body>
    <%  }catch(Exception e){
            System.out.println("Renderizado: " + e.getMessage());
        }
    
    }%>