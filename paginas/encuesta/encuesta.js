function ajaxDatosGenerales() {
    $.ajax({
        url: "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp",
        type: 'POST',
        data: valores_a_mandar,
        ifModified: true,
        datatype: "text",
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        beforeSend: function () {
            idLocal = idGlobal.split('-')[0];
            console.log(paginaActual);
            $('#' + idLocal + "-error").hide();
            $('#' + idLocal + "-ok").hide();
            $('#' + idLocal + "-loader").show();
        },
        success: function (datos, status, xhr) {
            var conn = datos.getElementsByTagName('respuesta')[0].firstChild.data;
            if (conn == "true") {
                $('#divNotificacionAlertFolioPlantilla').hide();
                $('#' + idLocal + "-error").hide();
                $('#' + idLocal + "-loader").hide();
                $('#' + idLocal + "-ok").show();
            } else {
                $('#' + idLocal + "-loader").hide();
                $('#' + idLocal + "-ok").hide();
                $('#' + idLocal + "-error").show();
                $('#divNotificacionAlertFolioPlantilla').show();
                //$('#alertRepBoton').hide();
                $('.status_error').show();
                $('.status_error').text('Conn: ' + conn);
            }
            console.log("conn: " + conn);
        },

        error: function (jqXHR, textStatus, errorThrown) {
            console.log('textStatus=' + textStatus);
            console.log('jqXHR=' + jqXHR.status);
            console.log('errorThrown=' + errorThrown);
            $('#divNotificacionAlertFolioPlantilla').show();
            $('.status_error').show();
            $('.status_error').text('Status: ' + jqXHR.status);
            $('#' + idLocal + "-loader").hide();
            $('#' + idLocal + "-ok").hide();
            $('#' + idLocal + "-error").show();
        }
    });
}

function ajaxModificarPlantilla(tipo, campo, value) {
    if (navigator.onLine) {
        console.log("conectado");
    } else {
        $('#divNotificacionAlertFolioPlantilla').show();
        $('.status_error').show();
        $('.status_error').text('Status: ' + '0');
    }
    urlParams = new URLSearchParams(valores_a_mandar);
    var accion = urlParams.get("accion")
    if (accion != null) {
        ajaxDatosGenerales()
    } else {
        var urls = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
        var idImag = tipo;
        var nameImag = campo;
        $.ajax({
            url: urls,
            type: "POST",
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            data: {
                // crearquery
                "idQuery": 2784,
                "parametros": valores_a_mandar,
                "_search": false,
                "nd": 1611873687449,
                "rows": -1,
                "page": 1,
                "sidx": "NO FACT",
                "sord": "asc"
            },
            beforeSend: function () {
                $('.' + idImag + "-" + nameImag + "-error").hide();
                $('.' + idImag + "-" + nameImag + "-ok").hide();
                $('.' + idImag + "-" + nameImag + "-loader").show();
            },
            success: function (data) {
                fechasHTA(campo, tipo);
                if (valores_a_mandar.match(/daan/) != null && valores_a_mandar.match(/c20_/) != null) {
                    verificarNotificacionAntesDeGuardar('verificarPerimetroc20');
                }

                else if (valores_a_mandar.match(/daan/) != null && (valores_a_mandar.match(/c1_/) != null || valores_a_mandar.match(/c2_/) != null)) {
                    verificarNotificacionAntesDeGuardar('verificarPesoTallaFol');
                }

                else if (valores_a_mandar.match(/daan/) != null && (valores_a_mandar.match(/c4_/) != null || valores_a_mandar.match(/c5_/) != null)) {
                    verificarNotificacionAntesDeGuardar('verificarPresiones');
                }

                else if (valores_a_mandar.match(/atpe/) != null && (valores_a_mandar.match(/c1_/) != null)) {
                    verificarNotificacionAntesDeGuardar('verificarTensionDiasIngreso');
                }

                else if (valores_a_mandar.match(/atpe/) != null && (valores_a_mandar.match(/c2_/) != null)) {
                    verificarNotificacionAntesDeGuardar('verificarTensionSistolicaIngreso');
                }


                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "B24X") {
                    console.log('entro vhi---1')
                    validacionesVih(tipo, campo)
                    traerInfomacionVihValidacion(traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO"))
                } else {
                    console.log('f')
                    try {
                        document.getElementById("divValidacionesVih").style.display = "none";
                        document.getElementById("divDescripcionRegla").style.display = "none";
                        document.getElementById("divValidacionesVariables").style.display = "none";
                    } catch (error) { }
                }
                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "EPOC" && tipo == 'pcep' && campo != 'c39' && campo != 'c40') {
                    // if (tipo =='pcep'){
                    //     let campoC22=document.getElementById('formulario.pcep.c22').parentNode.parentNode;
                    //     campoC22.setAttribute("style", "display: none;");
                    // }
                    if (campo != 'c48') {
                        calcularGOLD();
                    }
                }
                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "EPOC" && tipo == 'pcep' && campo != 'c40' && campo != 'c48') {
                    if (campo != 'c39') {
                        calcularBODE();
                    }
                }

                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "EPOC" && tipo == 'pcep' && campo != 'c39' && campo != 'c48') {
                    if (campo != 'c40') {
                        calcularBODEX();
                    }
                }
                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "EPOC" && tipo == 'haco' && campo != 'c20') {
                    if (campo != 'c19') {
                        indeceTabaquismo();
                    }
                }
                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "EPOC" && tipo == 'haco' && campo != 'c19') {
                    if (campo != 'c20') {
                        gradoTabaquismo();
                    }
                }
                if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "AEPP" && tipo == 'vaiep') {
                    validarAIEP();
                }

                let respuesta = data.getElementsByTagName('cell');
                if (respuesta.length != 0) {
                    $('#divNotificacionAlertFolioPlantilla').hide();
                    $('.' + idImag + "-" + nameImag + "-error").hide();
                    $('.' + idImag + "-" + nameImag + "-loader").hide();
                    $('.' + idImag + "-" + nameImag + "-ok").show();

                    if (tipo == 'vih0') {
                        ocultarTabsVih(value);
                    }


                } else {
                    $('.' + idImag + "-" + nameImag + "-loader").hide();
                    $('.' + idImag + "-" + nameImag + "-ok").hide();
                    $('.' + idImag + "-" + nameImag + "-error").show();
                    $('#divNotificacionAlertFolioPlantilla').show();
                    // $('#alertRepBoton').hide();
                    $('.status_error').show();
                }

                if (tipo == 'linda_fried'){
                    validacionesLindaFried(campo)
                }
                


                if (tipo == 'escala_findrisc'){
                    validacionFindrisc(campo);
                }
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log('textStatus=' + textStatus);
                console.log('jqXHR=' + XMLHttpRequest.status);
                console.log('errorThrown=' + errorThrown);
                $('#divNotificacionAlertFolioPlantilla').show();
                $('.status_error').show();
                $('.status_error').text('Status: ' + XMLHttpRequest.status);
                $('.' + idImag + "-" + nameImag + "-loader").hide();
                $('.' + idImag + "-" + nameImag + "-ok").hide();
                $('.' + idImag + "-" + nameImag + "-error").show();
            }
        });
        $('.' + idImag + "-" + nameImag + "-error").hide();
    }
}

function validacionFindrisc(name){
    if (name == 'c2' ||  name == 'c3'){
        talla =  document.getElementById('formulario.escala_findrisc.c2').value
        peso = document.getElementById('formulario.escala_findrisc.c3').value

        imc = peso / ( ( talla / 100 ) ** 2 )
        document.getElementById('formulario.escala_findrisc.c4').value = imc

        modificarPlantillaCRUD('guardarDatosEncuesta', 'escala_findrisc', 'c4', document.getElementById('formulario.escala_findrisc.c4').value)

    }
}

function validarAIEP(){

    let valorC1, valorC2, valorC3, valorC4, valorC5, valorC6;
    try {
        valorC1 = document.getElementById('formulario.vaiep.c1').value;
        valorC2 = document.getElementById('formulario.vaiep.c2').value;
        valorC3 = document.getElementById('formulario.vaiep.c3').value;
        valorC4 = document.getElementById('formulario.vaiep.c4').value;
        valorC5 = document.getElementById('formulario.vaiep.c5').value;
        valorC6 = document.getElementById('formulario.vaiep.c6').value;
        
    } catch (error) {
        console.log('Erro en folio aepp, captura de datos tab valoracion aiepi')
    }


    let taboido,tabfieb,tabtodr,tabdesarrollo_vacunas,tabvegi,tabrecomendaciones_aepi;
    try {
        taboido=document.getElementById('taboido');
        tabfieb=document.getElementById('tabfieb');
        tabtodr=document.getElementById('tabtodr');
        tabdesarrollo_vacunas=document.getElementById('tabdesarrollo_vacunas');
        tabvegi=document.getElementById('tabvegi');
        tabrecomendaciones_aepi=document.getElementById('tabrecomendaciones_aepi');

    } catch (error) {
        console.log('Erro en folio aepp, captura de datos tab valoracion aiepi')
    }
    
    if(valorC1 == 'SI'){
        taboido.style.display='' 
    }else{
        taboido.style.display='none' 
    }
    if(valorC2 == 'SI'){
        tabfieb.style.display='' 
    }else{
        tabfieb.style.display='none' 
    }
    if(valorC3 == 'SI'){
        tabtodr.style.display='' 
    }else{
        tabtodr.style.display='none' 
    }
    if(valorC4 == 'SI'){
        tabdesarrollo_vacunas.style.display='' 
    }else{
        tabdesarrollo_vacunas.style.display='none' 
    }
    if(valorC5 == 'SI'){
        tabvegi.style.display='' 
    }else{
        tabvegi.style.display='none' 
    }
    if(valorC6 == 'SI'){
        tabrecomendaciones_aepi.style.display='' 
    }else{
        tabrecomendaciones_aepi.style.display='none' 
    }
}

function calcularGOLD() {
    let exacerbaciones = document.getElementById("formulario.pcep.c20").value;
    let mmrc = document.getElementById("formulario.pcep.c18").value;
    let cat = document.getElementById("formulario.pcep.c19").value;
    let exacerbacionesHospitalizacion = document.getElementById("formulario.pcep.c34").value;
    let gold;
    if ((exacerbaciones != '') && (mmrc != '') && (cat != '')) {
        if (exacerbaciones >= 2 && (mmrc >= 0 && mmrc <= 1) && cat < 10) {
            gold = document.getElementById("formulario.pcep.c48").value = 'C';
        } else if (exacerbaciones >= 1 && (mmrc >= 0 && mmrc <= 1) && cat < 10 && exacerbacionesHospitalizacion == 'SI') {
            gold = document.getElementById("formulario.pcep.c48").value = 'C';
        } else if (exacerbaciones >= 2 && mmrc >= 2 && cat >= 10) {
            gold = document.getElementById("formulario.pcep.c48").value = 'D';
        } else if (exacerbaciones >= 1 && mmrc >= 2 && cat >= 10 && exacerbacionesHospitalizacion == 'SI') {
            gold = document.getElementById("formulario.pcep.c48").value = 'D';
        } else if ((exacerbaciones >= 0 && exacerbaciones <= 1) && (mmrc >= 0 && mmrc <= 1) && cat < 10) {
            gold = document.getElementById("formulario.pcep.c48").value = 'A';
        } else if ((exacerbaciones >= 0 && exacerbaciones <= 1) && (mmrc >= 0 && mmrc <= 1) && cat < 10 && exacerbacionesHospitalizacion == 'NO') {
            gold = document.getElementById("formulario.pcep.c48").value = 'A';
        } else if ((exacerbaciones >= 0 && exacerbaciones <= 1) && (mmrc >= 2) && cat >= 10) {
            gold = document.getElementById("formulario.pcep.c48").value = 'B';
        } else if ((exacerbaciones >= 0 && exacerbaciones <= 1) && (mmrc >= 2) && cat >= 10 && exacerbacionesHospitalizacion == 'NO') {
            gold = document.getElementById("formulario.pcep.c48").value = 'B';
        } else {
            gold = document.getElementById("formulario.pcep.c48").value = '';
        }

        guardarDatosPlantilla('pcep', 'c48', gold)
    }

}
// Funcion para calcular el indice de tabaquismo de la plantilla de habitos y comportamientos
// EPOC
function indeceTabaquismo() {
    let numeroCigarrillos = document.getElementById('formulario.haco.c22').value;
    let aniosTabaquismo = document.getElementById('formulario.haco.c23').value;
    let indeceTabaco;
    if (numeroCigarrillos != '' && aniosTabaquismo != '') {
        indeceTabaco = (numeroCigarrillos * aniosTabaquismo) / 20;
        document.getElementById('formulario.haco.c19').value = indeceTabaco;
        guardarDatosPlantilla('haco', 'c19', indeceTabaco.toString());


    } else {
        document.getElementById('formulario.haco.c19').value = '';
        document.getElementById('formulario.haco.c20').value = '';
        guardarDatosPlantilla('haco', 'c19', '');
        guardarDatosPlantilla('haco', 'c20', '');
    }


}

function calcularBODE() {
    let espirometriaD = document.getElementById("formulario.pcep.c35").value;


    let espirometriaC = document.getElementById("formulario.pcep.c36").value;
    let imc = document.getElementById("txtc3").value;
    let mmrc = document.getElementById("formulario.pcep.c18").value;
    let pruebaCaminata = document.getElementById("formulario.pcep.c37").value;

    let datoBode = document.getElementById("formulario.pcep.c40").value;

    let puntos = 0;

    espirometriaD.disabled = true;
    if ((espirometriaD != '' || espirometriaC != '') && (imc != '') && (mmrc != '') && (pruebaCaminata != '')) {
        if (parseFloat(imc) > 21) {
            puntos += 0;
        } else {
            puntos += 1;
        }
        //calcular puntuacion espiromitria EPOC
        if (parseFloat(espirometriaC) >= 65 || parseFloat(espirometriaD) >= 65) {
            puntos += 0;
        } else if ((parseFloat(espirometriaC) >= 50 && parseFloat(espirometriaC) <= 64) || (parseFloat(espirometriaD) >= 50 && parseFloat(espirometriaD) <= 64)) {
            puntos += 1;
        } else if ((parseFloat(espirometriaC) >= 36 && parseFloat(espirometriaC) <= 49) || (parseFloat(espirometriaD) >= 36 && parseFloat(espirometriaD) <= 49)) {
            puntos += 2;
        } else {
            puntos += 3;
        }

        //Calcular Mmrc
        if (parseFloat(mmrc) <= 1) {
            puntos += 0;
        } else if (parseFloat(mmrc) == 2) {
            puntos += 1;
        } else if (parseFloat(mmrc) == 3) {
            puntos += 2;
        } else if (parseFloat(mmrc) == 4) {
            puntos += 3;
        }

        //Calcular puntuacion prueba de caminata EPOC
        if (parseFloat(pruebaCaminata) >= 350) {
            puntos += 0;
        } else if ((parseFloat(pruebaCaminata) >= 250) && (parseFloat(pruebaCaminata) <= 349)) {
            puntos += 1;
        } else if ((parseFloat(pruebaCaminata) >= 150) && (parseFloat(pruebaCaminata) <= 249)) {
            puntos += 2;
        } else if (parseFloat(pruebaCaminata) <= 149) {
            puntos += 3;
        }

        let bode;
        if (puntos >= 0 && puntos <= 2) {
            bode = document.getElementById("formulario.pcep.c40").value = 'I. Leves';
        } else if (puntos >= 3 && puntos <= 4) {
            bode = document.getElementById("formulario.pcep.c40").value = 'II. Moderadas';
        } else if (puntos >= 5 && puntos <= 6) {
            bode = document.getElementById("formulario.pcep.c40").value = 'III. Graves';
        } else if (puntos >= 7 && puntos <= 10) {
            bode = document.getElementById("formulario.pcep.c40").value = 'IV. Muy graves';
        }
        // document.getElementById("formulario.pcep.c40").blur();
        guardarDatosPlantilla('pcep', 'c40', bode)
    } else {
        bode = document.getElementById("formulario.pcep.c40").value = '';
        guardarDatosPlantilla('pcep', 'c40', bode);
    }

}
function calcularBODEX() {
    let espirometriaD = document.getElementById("formulario.pcep.c35").value;
    let espirometriaC = document.getElementById("formulario.pcep.c36").value;
    let imc = document.getElementById("txtc3").value;
    let mmrc = document.getElementById("formulario.pcep.c18").value;
    let exacerbaciones = document.getElementById("formulario.pcep.c20").value;

    let datoBodex = document.getElementById("formulario.pcep.c40").value;

    let puntos = 0;

    if ((espirometriaD != '' || espirometriaC != '') && (imc != '') && (mmrc != '') && (exacerbaciones != '')) {

        if (parseFloat(imc) > 21) {
            puntos += 0;
        } else {
            puntos += 1;
        }
        //calcular puntuacion espiromitria EPOC
        if (parseFloat(espirometriaC) >= 65 || parseFloat(espirometriaD) >= 65) {
            puntos += 0;
        } else if ((parseFloat(espirometriaC) >= 50 && parseFloat(espirometriaC) <= 64) || (parseFloat(espirometriaD) >= 50 && parseFloat(espirometriaD) <= 64)) {
            puntos += 1;
        } else if ((parseFloat(espirometriaC) >= 36 && parseFloat(espirometriaC) <= 49) || (parseFloat(espirometriaD) >= 36 && parseFloat(espirometriaD) <= 49)) {
            puntos += 2;
        } else {
            puntos += 3;
        }
        //Calcular Mmrc
        if (parseFloat(mmrc) <= 1) {
            puntos += 0;
        } else if (parseFloat(mmrc) == 2) {
            puntos += 1;
        } else if (parseFloat(mmrc) == 3) {
            puntos += 2;
        } else if (parseFloat(mmrc) == 4) {
            puntos += 3;
        }
        //Calcular puntuacion prueba de caminata EPOC
        if (parseFloat(exacerbaciones) == 0) {
            puntos += 0;
        } else if ((parseFloat(exacerbaciones) >= 1) && (parseFloat(exacerbaciones) <= 2)) {
            puntos += 1;
        } else if (parseFloat(exacerbaciones) >= 3) {
            puntos += 2;
        }
        let bodex;
        if (puntos >= 0 && puntos <= 2) {
            bodex = document.getElementById("formulario.pcep.c39").value = 'I. Leves';
        } else if (puntos >= 3 && puntos <= 4) {
            bodex = document.getElementById("formulario.pcep.c39").value = 'II. Moderadas';
        } else if (puntos >= 5 && puntos <= 6) {
            bodex = document.getElementById("formulario.pcep.c39").value = 'III. Graves';
        } else if (puntos >= 7 && puntos <= 10) {
            bodex = document.getElementById("formulario.pcep.c39").value = 'IV. Muy graves';
        }
        // document.getElementById("formulario.pcep.c40").blur();
        guardarDatosPlantilla('pcep', 'c39', bodex)
    } else {
        bodex = document.getElementById("formulario.pcep.c39").value = '';
        guardarDatosPlantilla('pcep', 'c39', bodex);
    }

}
function gradoTabaquismo() {
    let gradoTabaco = document.getElementById('formulario.haco.c19').value;

    if (gradoTabaco != '') {
        if (gradoTabaco < 5) {
            guardarDatosPlantilla('haco', 'c20', '1');
            document.getElementById('formulario.haco.c20').value = '1';
        } else if (gradoTabaco >= 5 && gradoTabaco <= 15) {
            guardarDatosPlantilla('haco', 'c20', '2');
            document.getElementById('formulario.haco.c20').value = '2';
        } else if (gradoTabaco > 15 && gradoTabaco <= 25) {
            guardarDatosPlantilla('haco', 'c20', '3');
            document.getElementById('formulario.haco.c20').value = '3';
        } else if (gradoTabaco > 25) {
            guardarDatosPlantilla('haco', 'c20', '4');
            document.getElementById('formulario.haco.c20').value = '4';
        }
    } else {
        document.getElementById('formulario.haco.c20').value = '';
        guardarDatosPlantilla('haco', 'c20', '');
    }


}
var cbnut = {
    c1: 2,
    c2: 3,
    c3: 2,
    c5: 2,
    c6: 0
};
function calcularValorCbnut() {
    var suma = cbnut.c1 + cbnut.c2 + cbnut.c3 + cbnut.c5 + cbnut.c6;
    var resultado = '';
    if (suma >= 12 && suma <= 14) {
        resultado = 'Estado Nutricional Normal';
    } else if (suma >= 8 && suma <= 11) {
        resultado = 'Riesgo de malnutrición';
    } else if (suma >= 0 && suma <= 7) {
        resultado = 'Malnutrición';
    }
    document.getElementById('formulario.cbnut.c7').value = resultado;
    modificarPlantillaCRUD('guardarDatosEncuesta', 'cbnut', 'c7', resultado)
}

function calcularValorHYH() {
    let puntuacionTotal = 0;
    try {
        function sumarSiEsSI(valor, campo) {

            if (valor === 'SI') {

                if(campo == 'c12' || campo == 'c13' || campo == 'c18' || campo == 'c19'){
                    puntuacionTotal = puntuacionTotal + 2;
                }else if (campo == 'c14' || campo == 'c20' || campo == 'c21' || campo == 'c22'){
                    puntuacionTotal = puntuacionTotal + 3;
                }else{
                    puntuacionTotal++;
                }
            }
        }
        function sumarPuntuacion(valor, opciones) {
            if (opciones.hasOwnProperty(valor)) {
                puntuacionTotal += opciones[valor];
            }
        }
        for (let i = 3; i <= 28; i++) {
            campo = 'c' + i.toString();
            sumarSiEsSI(document.getElementById(`formulario.trb_hyh.c${i}`).value, campo);
        }
        const opcionesC1 = { '1': 1, '2': 0, '3': 2 };
        const opcionesC2 = { '1': 1, '2': 0, '3': 2 };
        const opcionesC15 = { '1': 1, '2': 2 };


        sumarPuntuacion(document.getElementById('formulario.trb_hyh.c1').value, opcionesC1);
        sumarPuntuacion(document.getElementById('formulario.trb_hyh.c2').value, opcionesC2);
        sumarPuntuacion(document.getElementById('formulario.trb_hyh.c15').value, opcionesC15);

        document.getElementById('formulario.trb_hyh.c29').value = puntuacionTotal;
        console.log('puntuacionTotal', puntuacionTotal, typeof puntuacionTotal)
        modificarPlantillaCRUD('guardarDatosEncuesta', 'trb_hyh', 'c29', puntuacionTotal.toString());
        
    } catch (error) {
        console.error(error);
    }
}


function guardarDatosEncuesta(id, name, value) {

    if (id === 'cbnut' && cbnut.hasOwnProperty(name)) {
        cbnut[name] = parseFloat(value);
        calcularValorCbnut();
    }
    if( id === 'trb_hyh'){
        calcularValorHYH();
    }

    varResultado = value;
    varOrden = id;
    varIdPlantilla = name;
    modificarPlantillaCRUD('guardarDatosEncuesta', id, name, varResultado);
}



function validacionesIndiceRiesgoReproductivo() {
    let c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12;
    try {
        c1 = document.getElementById('formulario.inrr.c1').value
        c2 = document.getElementById('formulario.inrr.c2').value
        c3 = document.getElementById('formulario.inrr.c3').value
        c4 = document.getElementById('formulario.inrr.c4').value
        c6 = document.getElementById('formulario.inrr.c6').value
        c7 = document.getElementById('formulario.inrr.c7').value
        c8 = document.getElementById('formulario.inrr.c8').value
        c9 = document.getElementById('formulario.inrr.c9').value
        c10 = document.getElementById('formulario.inrr.c10').value
        c11 = document.getElementById('formulario.inrr.c11').value
        c12 = document.getElementById('formulario.inrr.c12')
    } catch (error) {
        console.log(error)
    }



    let puntaje = 0;
    let valorC12 = '';
    c12.disabled = true;
    if (c1 != '' && c2 != '' && c3 != '' && c4 != '' && c6 != '' && c7 != '' && c8 != '' && c9 != '' && c10 != '' && c11 != '') {
        if (c1 == '1') {
            puntaje += 2;
        } else if (c1 == '2') {
            puntaje += 0;
        } else if (c1 == '3') {
            puntaje += 2;
        }

        if (c2 == '1') {
            puntaje += 0;
        } else if (c2 == '2') {
            puntaje += 1;
        } else if (c2 == '3') {
            puntaje += 4;
        }

        if (c3 == '1') {
            puntaje += 0;
        } else if (c3 == '2') {
            puntaje += 2;
        } else if (c3 == '3') {
            puntaje += 2;
        } else if (c3 == '4') {
            puntaje += 4;
        }
        if (c4 == '1') {
            puntaje += 0;
        } else if (c4 == '2') {
            puntaje += 4;
        } else if (c4 == '3') {
            puntaje += 1;
        } else if (c4 == '4') {
            puntaje += 0;
        }
        if (c6 == '1') {
            puntaje += 0;
        } else if (c6 == '2') {
            puntaje += 2;
        } else if (c6 == '3') {
            puntaje += 4;
        }
        if (c7 == '1') {
            puntaje += 0;
        } else if (c7 == '2') {
            puntaje += 2;
        } else if (c7 == '3') {
            puntaje += 4;
        }
        if (c8 == '1') {
            puntaje += 0;
        } else if (c8 == '2') {
            puntaje += 4;
        }

        if (c9 == '1') {
            puntaje += 0;
        } else if (c9 == '2') {
            puntaje += 2;
        } else if (c9 == '3') {
            puntaje += 4;
        }

        if (c10 == '1' || c10 == '2' || c10 == '3') {
            puntaje += 2;
        }

        if (c11 == '1' || c11 == '2' || c11 == '3' || c11 == '4') {
            puntaje += 4;
        }

        if (puntaje >= 7) {
            c12.style.backgroundColor = '#ff6969';
            // c12.setAttribute('style', 'background: ;')
            c12.value = 'Alto'
        } else if (puntaje >= 4 && puntaje <= 6) {
            c12.style.backgroundColor = 'yellow';
            // c12.setAttribute('style', 'background: yellow;')
            c12.value = 'Medio'
        } else if (puntaje <= 3) {
            c12.style.backgroundColor = '#00d700';
            // c12.setAttribute('style', 'background: #00d700#00d700;')
            c12.value = 'Bajo'
        }
    } else {
        c12.setAttribute('style', 'background: none;')
        c12.value = ''
    }
    valorC12 = c12.value;
    modificarPlantillaCRUD('guardarDatosPlantilla', 'inrr', 'c12', valorC12);

    return;

}

function cambiarValoresEdupi(tabla, value) {
    const maxFields = 44
    const exclusiones = new Set([
        'edupi.c40', 'eduado.c40', 'edu_juv.c40', 'edu_vejez.c45', 'edu_adul.c40'
    ]);

    for (let i = 1; i <= maxFields; i++) {
        let campoId = `formulario.${tabla}.c${i}`;

        if (!exclusiones.has(`${tabla}.c${i}`)) {
            try {
                let campo = document.getElementById(campoId);
                if(campo){
                    campo.value = value;
                    campo.dispatchEvent(new Event('change'));
                }
            } catch (error) {
                console.error(`Error en el campo ${campoId}: ${error}`);
            }
        }
    }
}

function cambiarColorDaan(value) {
    const id = 'formulario.daan.c14'
    try {
        const elemento = document.getElementById(id);
        if (value >= 106.67) {
            elemento.style.backgroundColor = '#ff4949'; 
        } else {
            elemento.style.backgroundColor = '';
        }
    } catch (error) {
        console.error(`No se encuentra ${id} en el DOM`)
    }
}


function guardarDatosPlantilla(id, name, value) {  //alert((id+':: '+name+' :: '+value ))

    const acciones = {
        'edupi': { 'c40': (id, value) => cambiarValoresEdupi(id, value) },
        'eduado': { 'c40': (id, value) => cambiarValoresEdupi(id, value) },
        'edu_juv': { 'c40': (id, value) => cambiarValoresEdupi(id, value) },
        'edu_vejez': { 'c45': (id, value) => cambiarValoresEdupi(id, value) },
        'edu_adul': { 'c40': (id, value) => cambiarValoresEdupi(id, value) },
        'daan': { 'c14': (_, value) => cambiarColorDaan(value) }
    };
    
    if (acciones[id] && acciones[id][name]) {
        acciones[id][name](id, value);
    }

    trasladarDaan(`formulario.${id}.${name}`, value)
    

    if (['HCPF'].includes(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO')) && id == 'inrr') {
        validacionesIndiceRiesgoReproductivo();
    }
    if (['EPOC'].includes(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'))) {
        //hay que eliminar las validaciones de BD para que funcione
        if (id == 'pcep' && name == 'c46') {
            if (parseFloat(value) <= 0 || value == '') {
                document.getElementById('formulario.pcep.c42').parentNode.parentElement.setAttribute('style', 'display:none !important;')
                document.getElementById('formulario.pcep.c43').parentNode.parentNode.parentNode.setAttribute('style', 'display:none !important;')
                document.getElementById('formulario.pcep.c42').value = ''
                document.getElementById('formulario.pcep.c46').value = '0'
                modificarPlantillaCRUD('guardarDatosPlantilla', id, name, '0');
                modificarPlantillaCRUD('guardarDatosPlantilla', id, 'c42', '');
                modificarPlantillaCRUD('guardarDatosPlantilla', id, 'c43', '');
            } else {
                document.getElementById('formulario.pcep.c42').parentNode.parentElement.setAttribute('style', 'display:true !important;')
                document.getElementById('formulario.pcep.c43').parentNode.parentNode.parentNode.setAttribute('style', 'display:true !important;')
            }
            if (parseFloat(value) < 0 || parseFloat(value) > 9.9) {
                document.getElementById('formulario.pcep.c46').value = '0'
                modificarPlantillaCRUD('guardarDatosPlantilla', id, name, '0');
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    showConfirmButton: false,
                    showCloseButton: true,
                    html: '<p style="color:black">El rango debe estar entre 0.1 y 9.9</style>',

                });
                return
            }
        }
        if (id == 'pcep' && name == 'c20') {
            if (parseFloat(value) == 0 || value == '') {
                document.getElementById('formulario.pcep.c21').parentNode.parentNode.setAttribute('style', 'display:none')
                document.getElementById('formulario.pcep.c21').value = ''
                modificarPlantillaCRUD('guardarDatosPlantilla', id, 'c21', '');

                //document.getElementById('formulario.pcep.c21').disabled = true
            } else {
                document.getElementById('formulario.pcep.c21').parentNode.parentNode.setAttribute('style', 'display:true')
                //document.getElementById('formulario.pcep.c21').disabled = false
            }
        }
        if (id == 'pcep' && name == 'c37') {
            if (parseFloat(value) < 0) {
                document.getElementById('formulario.pcep.c38').value = ''
                document.getElementById('formulario.pcep.c37').value = '0'
                modificarPlantillaCRUD('guardarDatosPlantilla', id, 'c37', '0');
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    showConfirmButton: false,
                    showCloseButton: true,
                    html: '<p style="color:black">El rango debe ser mayor o igual 0</style>',

                });
                return
            }
            /* if(parseFloat(value)  == 0){
                document.getElementById('formulario.pcep.c38').disabled = true
            }
            else{
                document.getElementById('formulario.pcep.c38').disabled = false
            } */
        }

        if (id == 'pcep' && name == 'c37') {
            if (parseFloat(value) == 0 || value == '') {
                document.getElementById('formulario.pcep.c38').parentNode.parentNode.setAttribute('style', 'display:none')
                document.getElementById('formulario.pcep.c38').value = ''
                modificarPlantillaCRUD('guardarDatosPlantilla', id, 'c38', '');
            } else {
                document.getElementById('formulario.pcep.c38').parentNode.parentNode.setAttribute('style', 'display:true')
            }
        }


    }


    if (id == 'haco' && name == 'c10') {
        if (parseFloat(value) < 5 || parseFloat(value) > 150) {
            document.getElementById('formulario.haco.c10').value = ''
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                showConfirmButton: false,
                showCloseButton: true,
                html: '<p style="color:black">El rango debe ser mayor o igual a 5 y menor o igual a 150</style>',

            });
            return
        }
    }

    if (['B24X', 'ENFV', 'NTSV', 'PCVH'].includes(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO'))) {
        validacionesHcVih(id, name).then(() => {
            varOrden = id
            varIdPlantilla = name
            varResultado = value
            modificarPlantillaCRUD('guardarDatosPlantilla', id, name, value);
        })
    } else {
        varOrden = id
        varIdPlantilla = name
        varResultado = value
        modificarPlantillaCRUD('guardarDatosPlantilla', id, name, value);
    }
}

function guardarDatosPlantillaHTML(id, name, value) {  //alert((id+':: '+name+' :: '+value ))    
    newid = name.substr(4)
    modificarPlantillaCRUD('guardarDatosPlantilla', id, newid, value);
}

function guardarDatosPlantillaCorreccionHC(id, name, value) {
    modificarPlantillaCRUD('guardarDatosPlantillaCorreccionHC', id, name, value)
}

function colocaridencuestacrear() {
    var id = document.getElementById('cmbEncuestaParaCrear').value;
    document.getElementById('lblIdEncuestaParaCrear').value = id;
}

function colocaridtiporecepcioncrear() {
    var id = document.getElementById('cmbTipoRecepccionEnc').value;
    document.getElementById('lblIdtiporecepcionEnc').value = id;
}


function comboEncuestas() {
    const tamizajePrenatal = ['GICP','HCPC']
    folioTipo = traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO");
    if (folioTipo == 'HCPI') {
        idQueryCombo = '10335'
        cargarComboGRALCondicion1('', '', 'cmbEncuestaParaCrear', idQueryCombo,document.getElementById('txtEdadCompleta').innerHTML.split('&')[0].trim()) 
    } else if (folioTipo == 'HCJU') {
        idQueryCombo = '9000'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    } else if (folioTipo == 'ESGO') {
        idQueryCombo = '10388'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    }else if(tamizajePrenatal.includes(folioTipo)){
        idQueryCombo = '2628'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    }else if (folioTipo == 'HCAZ'){
        idQueryCombo = '10337'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    } else if (folioTipo == 'HCVE') {
        idQueryCombo = '3820'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    } else if (folioTipo == 'HCAD') {     
        idQueryCombo = '93'
        cargarComboGRALCondicion1('', '', 'cmbEncuestaParaCrear', idQueryCombo,document.getElementById('txtEdadCompleta').innerHTML.split('&')[0].trim()) 
    } else if (folioTipo == 'HCIN' ) {    
        idQueryCombo = '10339'
        cargarComboGRALCondicion1('', '', 'cmbEncuestaParaCrear', idQueryCombo,document.getElementById('txtEdadCompleta').innerHTML.split('&')[0].trim()) 
        // cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    } else if (folioTipo == 'HCCX') {    
        idQueryCombo = '10363'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    } else if (folioTipo == 'HCPU') {    
        idQueryCombo = '180'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    } else if (folioTipo == 'MEDG') {    
        idQueryCombo = '10368'
        //cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)   
        cargarComboGRALCondicion1('', '', 'cmbEncuestaParaCrear', idQueryCombo,document.getElementById('txtEdadCompleta').innerHTML.split('&')[0].trim()) 
    } 
    else {
        idQueryCombo = '960'
        cargarComboGRAL('', '', 'cmbEncuestaParaCrear', idQueryCombo)
    }
    $("#" + 'cmbEncuestaParaCrear').empty();
    $("#" + 'cmbEncuestaParaCrear').append(new Option("[ SELECCIONE ]", "", true, true));
}

function modificarEncuestaCRUD(arg, id, name, value) {
    pagina = '/clinica/paginas/accionesXml/modificarEncuestaCRUD_xml.jsp';
    varajaxMenu.onreadystatechange = respuestamodificarEncuestaCRUD(agregarSede);
    paginaActual = arg;
    switch (arg) {

        case 'guardarDatosEncuesta':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = '';
                valores_a_mandar = valores_a_mandar + "tipo=" + id; //eno1(tipo)
                valores_a_mandar = valores_a_mandar + "&value=" + value; //holoa
                valores_a_mandar = valores_a_mandar + "&campo=" + name; // c22
                valores_a_mandar = valores_a_mandar + "&id_encuesta_paciente=" + valorAtributo('lblIdEncuestaPaciente'); //111
                ajaxModificar();
            }
            break;
    }
}

function modificarNotasCRUD(id, name, value) {
    tipoElem = 'cmb'
    valores_a_mandar = '';
    add_valores_a_mandar(id);
    add_valores_a_mandar(name);
    add_valores_a_mandar(value.replaceAll("'", "''"));
    add_valores_a_mandar(LoginSesion());
    add_valores_a_mandar(traerDatoFilaSeleccionada('listNotasVih', 'ID'));
    add_valores_a_mandar('true');

    ajaxModificarPlantilla(id, name, value);
}

function validacionesLindaFried(name){
    if (name == 'c6' ||  name == 'c5'){
        talla =  document.getElementById('formulario.linda_fried.c5').value
        peso = document.getElementById('formulario.linda_fried.c6').value
        c8 = document.getElementById('formulario.linda_fried.c8')

        imc = peso / ( ( talla / 100 ) ** 2  )
        document.getElementById('formulario.linda_fried.c7').value = imc

        if (imc <= 21){
            c8.value = 1
        }else{
            c8.value = 2
        }
        modificarPlantillaCRUD('guardarDatosEncuesta', 'linda_fried', 'c7', document.getElementById('formulario.linda_fried.c7').value)
        modificarPlantillaCRUD('guardarDatosEncuesta', 'linda_fried', 'c8', c8.value)

    }else if (name == 'c21' ||  name == 'c22'){
        trayecto = document.getElementById('formulario.linda_fried.c21').value
        tiempo = document.getElementById('formulario.linda_fried.c22').value
        c24 = document.getElementById('formulario.linda_fried.c24')        
        if ((( trayecto / 100 ) / (tiempo)) <= 0.8){
            c24.value = 1
        }else {
            c24.value = 2
        }
        modificarPlantillaCRUD('guardarDatosEncuesta', 'linda_fried', 'c24', c24.value)
    }
    contarValoresSiNO()
}

function contarValoresSiNO(){
    sumaSI = 0 
    sumaNO = 0
    divLindaFried = document.querySelector('#div_linda_fried');

    var inputs = divLindaFried.querySelectorAll('input');

    inputs.forEach(element => {

        if (element.value == 'SI'){
            sumaSI = sumaSI + 1;
        }
        if (element.value == 'NO'){
            sumaNO = sumaNO + 1;
        }
    });

    var selects = divLindaFried.querySelectorAll('select');
    selects.forEach(element => {

        if (element.value == '1'){
            sumaSI = sumaSI + 1;
        }
        if (element.value == '2'){
            sumaNO = sumaNO + 1;
        }
    });


    document.getElementById('formulario.linda_fried.c27').value = sumaSI;
    document.getElementById('formulario.linda_fried.c28').value = sumaNO;

    modificarPlantillaCRUD('guardarDatosEncuesta', 'linda_fried', 'c27', sumaSI)
    modificarPlantillaCRUD('guardarDatosEncuesta', 'linda_fried', 'c28', sumaNO)

}

function modificarPlantillaCRUD(arg, id, name, value) {
    switch (arg) {
        case 'guardarDatosEncuesta':
            const idEncuestaPaciente = document.getElementById('lblIdEncuestaPaciente').textContent.trim();
            valores_a_mandar = '';
            add_valores_a_mandar(id);
            add_valores_a_mandar(name);
            add_valores_a_mandar(value.replaceAll("'", "''"));
            add_valores_a_mandar(LoginSesion());
            add_valores_a_mandar(valorAtributo('lblIdEncuestaPaciente')==''? idEncuestaPaciente:valorAtributo('lblIdEncuestaPaciente'));
            add_valores_a_mandar('');

            ajaxModificarPlantilla(id, name);

            break;

        case 'guardarDatosPlantilla':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = '';
                add_valores_a_mandar(id);
                add_valores_a_mandar(name);
                add_valores_a_mandar(value.replaceAll("'", "''"));
                add_valores_a_mandar(LoginSesion());
                add_valores_a_mandar(valorAtributo('lblIdDocumento'));
                add_valores_a_mandar('true');

                ajaxModificarPlantilla(id, name, value);
            }
            break;

        case 'guardarDatosPlantillaCorreccionHC':
            valores_a_mandar = '_-' + id;
            valores_a_mandar += '_-' + name;
            valores_a_mandar += '_-' + value.replaceAll("'", "''");
            valores_a_mandar += '_-' + LoginSesion();
            valores_a_mandar += '_-' + valorAtributo('lblIdDocumento');
            valores_a_mandar += '_-' + 'false';

            ajaxModificarPlantilla(id, name);
            break;
    }
}

function obtenerValorPlantilla(objeto, id, name, correcion) {
    var value = '';
    value = $(objeto).parent().children().attr('value');
    if (value === undefined) {
        value = $(objeto).parent().children().children().attr('value');
    }
    if (correcion) {
        modificarPlantillaCRUD('guardarDatosPlantillaCorreccionHC', id, name, value);
    }
    else {
        modificarPlantillaCRUD('guardarDatosPlantilla', id, name, value);
    }
}



function guardarIdFormulaPlantillaDetalleCRUD(arg, id_plantilla, referencia_resultado) {
    pagina = '/clinica/paginas/accionesXml/guardarIdFormulaPlantillaDetalle_xml.jsp';
    paginaActual = arg;
    varajaxMenu.onreadystatechange = respuestaIdFormulaPlantillaDetalleCRUD;
    switch (arg) {
        case 'guardarIdFormula':
            valores_a_mandar = '';
            valores_a_mandar = valores_a_mandar + "id_plantilla=" + id_plantilla;
            valores_a_mandar = valores_a_mandar + "&referencia_resultado=" + referencia_resultado;
            ajaxModificar();
            break;
    }
}



function respuestaIdFormulaPlantillaDetalleCRUD(arg, xmlraiz) {
    switch (arg) {
        case 'guardarIdFormula':
            alert('guardado')
            break;

    }

}



function respuestamodificarEncuestaCRUD(arg, xmlraiz) {
    switch (arg) {
        case 'guardarDatosEncuesta':
            console.log("FUARDO")
            // alert('guardado')
            break;

    }

}

function respuestamodificarPlantillaCRUD(arg, xmlraiz) {
    switch (arg) {
        case 'guardarDatosPlantilla':
            //alert('guardado')
            break;

    }

}
function contenedorEncuesta() {
    $.ajax({
        url: '/clinica/paginas/encuesta/encuestaPaciente.jsp',
        data: '',
        contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",

        beforeSend: function () {
            //mostrarLoader(urlParams.get("accion"))
        },
        success: function (data) {
            document.getElementById("divParaContenedorEncuesta").innerHTML = data;
            if (traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_ESTADO") == '1') {

                desHabilitar('btnCrearEncuesta', 1)
                desHabilitar('btnEliminarEncuesta', 1)
                desHabilitar('btnImprimirEncuesta', 1)
                desHabilitar('btnCerrarEncuesta', 1)
            } else {

                desHabilitar('btnCrearEncuesta', 0)
                desHabilitar('btnEliminarEncuesta', 0)
                desHabilitar('btnImprimirEncuesta', 0)
                desHabilitar('btnCerrarEncuesta', 0)
            }

            buscarEncuesta('listEncuestaPaciente');

            $('#cmbEncuestaParaCrear').select2({dropdownCssClass : "set_ddl_size"})     
            comboEncuestas()
        },
        complete: function (jqXHR, textStatus) {
            //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    });
}


var sexo = '';
var rango = '';
var rango2 = '0';
var edad = '';
var dias = '';
var diasEnMeses = 0;

/**
 * Asigna a las variables globales dlos valores de sexo, rango rango2 y edad
 * 
 * @returns {Object} variables de sexo, rango, rango2 y edad
 */
function asignaSexoRangoEdad(modo) {
    let sexo = '';
    let rango = '';
    let rango2 = '0';
    let edad = '';
    if (valorAtributo('lblGeneroPaciente') != '') {
        sexo = valorAtributo('lblGeneroPaciente')
    }
    else {
        sexo = valorAtributo('cmbSexo')
    }
    if (valorAtributo('lblEdadPaciente') != '') {
        if (valorAtributo('lblEdadPaciente') < 3) rango2 = '1'
        if (valorAtributo('lblEdadPaciente') <= 5) rango = '1'
        if (valorAtributo('lblEdadPaciente') > 5 && valorAtributo('lblEdadPaciente') <= 10) rango = '2'
        if (valorAtributo('lblEdadPaciente') > 10 && valorAtributo('lblEdadPaciente') <= 17) rango = '3'
        if (valorAtributo('lblEdadPaciente') > 17 && valorAtributo('lblEdadPaciente') <= 28) rango = '4'
        if (valorAtributo('lblEdadPaciente') > 28 && valorAtributo('lblEdadPaciente') <= 59) rango = '5'
        if (valorAtributo('lblEdadPaciente') > 59) rango = '6'
        if (valorAtributo('lblEdadPaciente') > 12) rango2 = '3'
        if (ventanaActual.opc == 'modificarHistoria') {
            edad = parseInt(valorAtributo('lblEdadPaciente')) * 12;

        }
        else {

            edad = valorAtributo('lblMesesEdadPaciente');
            dias = $('#txtEdadCompleta').text().split(' ')[2].split(' ')[0].replace('D\u00EDas', '').trim()

            diasEnMeses = parseInt(dias) / 30

            edad = parseFloat(valorAtributo('lblMesesEdadPaciente')) + diasEnMeses
        }
        console.log('edad', edad)
    }
    else {
        if (valorAtributo('lblEdad') < 3) rango2 = '1'
        if (valorAtributo('lblEdad') <= 5) rango = '1'
        if (valorAtributo('lblEdad') > 5 && valorAtributo('lblEdad') <= 10) rango = '2'
        if (valorAtributo('lblEdad') > 10 && valorAtributo('lblEdad') <= 17) rango = '3'
        if (valorAtributo('lblEdad') > 17 && valorAtributo('lblEdad') <= 28) rango = '4'
        if (valorAtributo('lblEdad') > 28 && valorAtributo('lblEdad') <= 59) rango = '5'
        if (valorAtributo('lblEdad') > 59) rango = '6'
        if (valorAtributo('lblEdad') > 12) rango2 = '3'
        //if(valorAtributo('lblEdad') >18 ) rango2 = '2'   
        //f(valorAtributo('lblEdad') >25 ) rango2 = '9' 
        //if(valorAtributo('lblEdad') >40 ) rango2 = '15' 
        //if(valorAtributo('lblEdad') >50 ) rango2 = '10'      
        //if(valorAtributo('lblEdad') >=0 && valorAtributo('lblEdad') <=2) rango2 = '4'
        //if(valorAtributo('lblEdad') >2 && valorAtributo('lblEdad') <=5) rango2 = '5'
        //if(valorAtributo('lblEdad') >5 && valorAtributo('lblEdad') <=18) rango2 = '6'
        edad = traerDatoFilaSeleccionada('listEncuestaPaciente', 'edad_meses_actual');
        //edad = parseInt((valorAtributo('lblEdad') * 12)) + parseInt( valorAtributo('lblEdadCompleta').split(' ')[1][0]);
    }

    if (modo == 'desdeEncuesta') {
        edad = traerDatoFilaSeleccionada('listEncuestaPaciente', 'edad_meses_actual');
    }
    return {
        sexo: sexo,
        rango: rango,
        rango2: rango2,
        edad: edad
    };
}

function tabsContenidoEncuesta(modo) {
    let idEncuesta = traerDatoFilaSeleccionada('listEncuestaPaciente', 'id_encuesta');
    let idEncuestaPaciente = traerDatoFilaSeleccionada('listEncuestaPaciente', 'id');
    let pagina = ''
    const { sexo, rango, rango2, edad } = asignaSexoRangoEdad(modo);
    if (modo == 'desdeEncuesta') {
        cargarFormulasEncuesta(idEncuesta, idEncuestaPaciente);
        pagina = '/clinica/paginas/encuesta/tabsContenidoEncuesta.jsp';
        datos = {
            'id_parametro': idEncuesta,
            'modo': modo,
            'id_sexo': sexo,
            'rango': rango,
            'rango2': rango2,
            'edade': edad
        }
    }
    else if (modo == 'folioEspecialidad') {
        pagina = '/clinica/paginas/encuesta/tabscontenidoFolioEspecialidad.jsp';
        datos = {
            'id_parametro': valorAtributo('lblTipoDocumento'),
            'modo': modo,
            'id_sexo': sexo,
            'rango': rango,
            'rango2': rango2,
            'edade': edad
        }
    }
    else if (modo == 'desdeFamilia') {
        cargarFormulasEncuesta(idEncuesta, idEncuestaPaciente);
        pagina = '/clinica/paginas/adminModelo/tabsContenidoEncuestaVivienda.jsp';
        datos = {
            'id_parametro': idEncuesta,
            'modo': modo,
            'id_sexo': sexo,
            'rango': rango,
            'rango2': rango2,
            'edade': edad
        }
    }
    else if (modo == 'revisionSistemas') {
        pagina = '/clinica/paginas/encuesta/tabsDesdeFolioR.jsp';
        datos = {
            'id_parametro': valorAtributo('lblTipoDocumento'),
            'modo': modo,
            'id_sexo': sexo,
            'rango': rango,
            'rango2': rango2,
            'edade': edad
        }
    }
    else if (modo == 'analisisyplan') {
        pagina = '/clinica/paginas/encuesta/tabsDesdeFolioAyP.jsp';
        datos = {
            'id_parametro': valorAtributo('lblTipoDocumento'),
            'modo': modo,
            'id_sexo': sexo,
            'rango': rango,
            'rango2': rango2,
            'edade': edad
        }
    }
    else if (modo == 'desdeAntecedentes') {        
        pagina = '/clinica/paginas/encuesta/tabsDesdeFolioAntecedentes.jsp';
        datos = {
            'id_parametro': valorAtributo('lblTipoDocumento'),
            'modo': modo,
            'id_sexo': sexo,
            'rango': rango,
            'rango2': rango2,
            'edade': edad
        }
        
    }
    else {
        pagina = '/clinica/paginas/encuesta/tabsDesdeFolio.jsp';
        datos = {
            'id_parametro': valorAtributo('lblTipoDocumento'),
            'modo': modo,
            'id_sexo': sexo,
            'rango': rango,
            'rango2': rango2,
            'edade': edad
        }
    }
    $.ajax({
        url: pagina,
        type: "GET",
        data: datos,
        contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
        beforeSend: function () {
            //mostrarLoader(urlParams.get("accion"))
        },
        success: function (data) {
            if (modo == 'desdeEncuesta') {

                document.getElementById("divParaTabsContenidoEncuesta").innerHTML = data;
                $("#tabsContenidoEncuestaPaciente").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab'), modo);

            } else if (modo == 'folioEspecialidad') {

                document.getElementById("divParaTabsContenidoFolioEspecialidad").innerHTML = data;
                $("#tabsContenidoEncuestaPaciente33").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab33'), modo);
            }
            else if (modo == 'desdeFamilia') {
                document.getElementById("divParaTabsContenidoEncuesta2").innerHTML = data;
                $("#tabsContenidoEncuestaVivienda").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab11'), modo);

            }
            else if (modo == 'revisionSistemas') {
                document.getElementById("divParaTabsContenidoFolioPlantillaR").innerHTML = data;
                $("#tabsContenidoEncuestaPaciente222").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab222'), modo);
            }
            else if (modo == 'analisisyplan') {
                document.getElementById("divParaTabsContenidoFolioPlantillaAyP").innerHTML = data;
                $("#tabsContenidoEncuestaPaciente2222").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab2222'), modo);
            }
            else if (modo == 'desdeAntecedentes') {
                document.getElementById("divParaTabsContenidoAntecedentes").innerHTML = data;
                $("#tabsContenidoEncuestaPacienteAntecedentes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTabAntecedentes'), modo);
            }
            else {
                validarTabsGenero();
                document.getElementById("divParaTabsContenidoFolioPlantilla").innerHTML = data;
                $("#tabsContenidoEncuestaPaciente22").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                tabsSubContenidoEncuesta(valorAtributo('lblIdPrimerTab22'), modo,valorAtributo('lblTabActualTipoContenido'));
            }
        },
        complete: function (jqXHR, textStatus) {
            //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    });
}


function mostrarLoadresTabs(modo) {
    switch (modo) {
        case 'desdeEncuesta':
            mostrar('loadertabsContenidoEncuesta')
            break;

        case 'analisisyplan':
            mostrar('loaderTabsDesdeFolioAyP')
            break;

        case 'folioEspecialidad':
            mostrar('loaderTabsContenidoFolioEspecialidad')
            break;

        case 'desdeFolio':
            mostrar('loaderTabsDesdeFolio')
            break;

        default:
            break;
    }
}

function ocultarLoadresTabs() {
    ocultar('loadertabsContenidoEncuesta')
    ocultar('loaderTabsDesdeFolio')
    ocultar('loaderTabsContenidoFolioEspecialidad')
    ocultar('loaderTabsDesdeFolioAyP')
}


function tabsSubContenidoEncuesta(tipo, modo, tipoContenido) {
    return new Promise((resolve = () => { }, reject = () => { }) => {
        //En las plantillas de especialidad no existe el auxFocus por lo que debe consultarse antes
        $('#auxFocusFolio').focus();
        $('#auxFocus').focus();
        $('#auxFocusEncuesta').focus();
        $('#auxFocusAyP').focus();
    
        if (valorAtributo('lblTabActualModo') == modo) {
            $('#div_' + valorAtributo('lblTabActual')).empty();
        }
    
        mostrarLoadresTabs(modo);
    
        asignaAtributo("lblTabActual", tipo)
        asignaAtributo('lblTabActualModo', modo)
        console.log("tipo: " + tipo);
        console.log("modo: " + modo);
    
        let datos = {};
        let pagina = '';
        let tipo_folio = traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO")
        let id_evolucion = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_FOLIO")
        let id_estado_folio = traerDatoFilaSeleccionada("listDocumentosHistoricos", "ID_ESTADO")
        let versionRender = 'v1'
        const { sexo, rango, rango2, edad } = asignaSexoRangoEdad(modo);
        if (modo == 'desdeEncuesta' || modo == 'desdeFamilia') {
            const idParametro = document.getElementById('lblIdEncuesta').textContent.trim();
            const idEncuesta = valorAtributo('lblIdEncuesta')==''? idParametro : valorAtributo('lblIdEncuesta')
            const idEncuestaPaciente = document.getElementById('lblIdEncuestaPaciente').textContent.trim();
            getVersionRenderTamizaje(tipo,idEncuesta).then((result) => {
                const { page, version } = result
                pagina = page;
                versionRender = version
                datos = {
                    "tipo": tipo,
                    "id_parametro": idEncuesta,
                    "id_encuestaPaciente": valorAtributo('lblIdEncuestaPaciente')==''? idEncuestaPaciente:valorAtributo('lblIdEncuestaPaciente'),
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,versionRender)
            })                
        }
        else if(modo == 'desdeAntecedentes') {//antecedentes
            pagina = '/clinica/paginas/encuesta/subContenidoFolioAntecedentes.jsp'
                    datos = {
                        "tipo": tipo,
                        "tipo_folio": tipo_folio,
                        "id_evolucion": id_evolucion,
                        "id_sexo": sexo,
                        "rango": rango,
                        "rango2": rango2,
                        "edade": edad,
                        "profesional": IdSesion(),
                        "servicio_auditoria": valorAtributo("txtServicioAuditoria")
                    }
        }  
        else {
            if (tipo == 'c2ga') {      
                pagina = "/clinica/paginas/encuesta/CLAP2.jsp";
                datos = {
                    "tipo": tipo,
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');    
            } else if (tipo == 'vrl6') {
                pagina = "/clinica/paginas/encuesta/evaluacionPosturalTFOI.jsp";
                datos = {
                    "tipo": tipo,
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');
            }
            else if (tipo == 'c1ga') {
                pagina = "/clinica/paginas/encuesta/clap1.jsp";
                var dimensionx = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90'  
                datos = {
                    "tipo": tipo,
                    "idPaciente": valorAtributo("lblIdPaciente"),
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                //var dataClap1;
                //dataClap1 =  
                ///let arrayDataSend = []
                var nuevaVentanaClap = window.open(pagina, 'HISTORIA CLINICA PRENATAL',dimensionx);
                dataClap(datos).then(data =>{               
                    dataClapAll().then(dataClap =>{
                        traerDatosGrillaClap(2798,datos.idPaciente).then(datosTrabajoParto =>{
                            traerDatosGrillaClap(2799,datos.idPaciente).then(datosPuerperio =>{
                                let dataSend = [data,datos,dataClap,datosTrabajoParto,datosPuerperio]
                                setTimeout(() => {
                                    nuevaVentanaClap.postMessage(dataSend, '*');   
                                    console.log(dataSend);                 
                                }, 200);
                            })
                        })
                    })
                   /*  var encodedJsonArray = encodeURIComponent(dataClap1 );
                    // Combinar el JSON codificado con la URL
                    var urlWithJsonArray = pagina + '?data=' + encodedJsonArray; */
                    
                })
                
                // Codificar la cadena JSON
                
            /*     setTimeout(() => {
                    
                    var encodedJsonArray = encodeURIComponent(dataClap1 );
                    // Combinar el JSON codificado con la URL
                    var urlWithJsonArray = pagina + '?data=' + encodedJsonArray;
                    
                    var dimensionx = 'width=1150,height=952,scrollbars=NO,statusbar=NO,left=150,top=90'  
                    window.open(urlWithJsonArray, 'HISTORIA CLINICA PRENATAL',dimensionx);
                    
                }, 1000); */
                return
    
            }
    
            else if (tipo == 'vrl2') {
                pagina = "/clinica/paginas/encuesta/miembrosSuperioresTFOI.jsp";
                datos = {
                    "tipo": tipo,
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');
            }
            else if (tipo == 'vrl3') {
                pagina = "/clinica/paginas/encuesta/miembrosInferioresTFOI.jsp";
                datos = {
                    "tipo": tipo,
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');
            }
            else if (tipo == 'vrl4') {
                pagina = "/clinica/paginas/encuesta/columnaTFOI.jsp";
                datos = {
                    "tipo": tipo,
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');
            }
            else if (tipo == 'cardiopulmonar') {
                pagina = "/clinica/paginas/encuesta/cardiopulmonarTFOI.jsp";
                datos = {
                    "tipo": tipo,
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');
            }
            else if (tipo == 'laboratorios_nefro') {
                pagina = "/clinica/paginas/encuesta/labNefro.jsp";
                datos = {
                    "tipo": tipo,
                    "tipo_folio": tipo_folio,
                    "id_evolucion": id_evolucion,
                    "id_sexo": sexo,
                    "rango": rango,
                    "rango2": rango2,
                    "edade": edad
                }
                resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');
            }
            else {            
                getVersionRender(tipo,tipo_folio).then((result) =>{
                    const { page, version } = result
                    versionRender = version
                    if (ventanaActual.opc == 'modificarHistoria') {
                        pagina = '/clinica/paginas/encuesta/correccionHC/subContenidoFolioPlantilla.jsp'
                        datos = {
                            "tipo": tipo,
                            "tipo_folio": tipo_folio,
                            "id_evolucion": id_evolucion,
                            "id_sexo": sexo,
                            "rango": rango,
                            "rango2": rango2,
                            "edade": edad,
                            "profesional": IdSesion(),
                            "servicio_auditoria": valorAtributo("txtServicioAuditoria")
                        }
                    }
                    else {
                        if(tipoContenido==2){
                            pagina=  "/clinica/paginas/hc/documentosHistoriaClinica/" + tipo.toUpperCase() + ".jsp"
                            datos = {"tipo": tipo}
                        }
                        else{
                            pagina = page
                            datos = {
                                "tipo": tipo,
                                "tipo_folio": tipo_folio,
                                "id_evolucion": id_evolucion,
                                "id_sexo": sexo,
                                "rango": rango,
                                "rango2": rango2,
                                "edade": edad,
                                "profesional": IdSesion(),
                                "servicio_auditoria": valorAtributo("txtServicioAuditoria"),
                                "id_estado_folio": id_estado_folio,
                                "modo": modo
                            }
                            if (tipo == 'vih0') {
                                console.log('vih0 ---traer info')
                                traerInfomacionVihValidacion(id_evolucion)
                            }                    
                        }
                    }
                    resolvePromise(pagina,datos,tipo,id_estado_folio,modo,versionRender,tipoContenido)
                })
                .catch((error) => {
                    console.error(error)
                })                        
            }
        }
        
        if(modo != 'desdeFolio' && modo != 'revisionSistemas' && modo != 'analisisyplan' && modo != 'folioEspecialidad' &&
        modo != 'desdeEncuesta' && modo != 'desdeFamilia'){
            resolvePromise(pagina,datos,tipo,id_estado_folio,modo,'v1');
        }
        resolve();
    });
}

function llenartabsSubContenidoEncuesta(tipo, modo) {

    /* VARIABLE ENDPOINT servicios MODIFICAR SI ES NECESARIO !!!! */
    const endpointPhpServ = "http://10.0.0.3:9010";
    if (ventanaActual.opc == 'modificarHistoria') {
        if (tipo == 'pcnp') {
            console.log(tipo,"++++++++");
            $.ajax({
                url: `${endpointPhpServ}/categoriaHTACHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento"), 'modifica': LoginSesion() },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    document.getElementById('formulario.pcnp.c64').value = response.categoria
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/compensacionHTACHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c65').value = response.compensacion
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/formulaCHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c60').value = response.valor
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/rcvgCHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.rcv != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c44').value = response.rcv
                        document.getElementById('formulario.pcnp.c44').style.backgroundColor = response.color
                        document.getElementById('formulario.pcnp.c44').style.color = 'black'
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/framiCHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.frami != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c45').value = response.frami;
                        document.getElementById('formulario.pcnp.c46').value = response.clasificacion;
                        document.getElementById('formulario.pcnp.c46').style.backgroundColor = response.color;
                        document.getElementById('formulario.pcnp.c46').style.color = 'black';
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/formulaMDRCHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c61').value = response.valor
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/formulaTFGCHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento"), 'modifica': LoginSesion() },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c5').value = response.valor
                        document.getElementById('formulario.pcnp.c68').value = response.estadio_facturacion
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/algoritmoERCCHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.Estado == 2) {

                        document.getElementById('formulario.pcnp.c7').value = response.data.estadio;
                        document.getElementById('formulario.pcnp.c72').value = response.data.evolucionEstadio; //Regrecion!

                        if (response.data.erc != "" && response.data.erc != null) {
                            document.getElementById('formulario.pcnp.c6').value = response.data.erc;
                        }

                        if (response.data.alerta != "" && response.data.alerta != null) {
                            console.log("entra alerta ");
                            alert(response.data.alerta);
                        }

                        if (response.data.fecha_erc != "" && response.data.fecha_erc != null) {
                            document.getElementById('formulario.pcnp.c49').value = response.data.fecha_erc;
                        }

                        if (response.data.descripcion != "" && response.data.descripcion != null) {
                            document.getElementById('formulario.pcnp.c74').value = response.data.descripcion + " " + response.data.alerta;
                        }

                    } else {
                        console.log("No se Ejecuto el algoritmo");
                    }

                },
                error: function () {
                },
                complete: function (response) {
                    if (response.mensaje) {
                        alert("Error al Ejecutar Algoritmo ERC, vuelva a abrir el tab \" CRONICOS - NEFROPROTECCION \"");
                        console.log(response.mensaje)
                    }
                }
            });

        }
        if (tipo == 'atpe') {
            $.ajax({
                url: `${endpointPhpServ}/fechasCHC`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.atpe.c3').value = response.hta
                        document.getElementById('formulario.atpe.c4').value = response.fecha_hta
                        document.getElementById('formulario.atpe.c5').value = response.dm
                        document.getElementById('formulario.atpe.c6').value = response.fecha_dm
                    }
                    else {
                    }
                },
                error: function () {
                }
            });
        }
    }
    else {
        if (tipo == 'pcnp') {
            $.ajax({
                url: `${endpointPhpServ}/categoriaHTA`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    document.getElementById('formulario.pcnp.c64').value = response.categoria
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/compensacionHTA`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c65').value = response.compensacion
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            console.log("entro")
            $.ajax({
                url: `${endpointPhpServ}/formula`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c60').value = response.valor
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/rcvg`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.rcv != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c44').value = response.rcv
                        document.getElementById('formulario.pcnp.c44').style.backgroundColor = response.color
                        document.getElementById('formulario.pcnp.c44').style.color = 'black'
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/frami`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.frami != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c45').value = response.frami;
                        document.getElementById('formulario.pcnp.c46').value = response.clasificacion;
                        document.getElementById('formulario.pcnp.c46').style.backgroundColor = response.color;
                        document.getElementById('formulario.pcnp.c46').style.color = 'black';
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/formulaMDR`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c61').value = response.valor
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

            $.ajax({
                url: `${endpointPhpServ}/formulaTFG`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.pcnp.c5').value = response.valor
                        document.getElementById('formulario.pcnp.c68').value = response.estadio_facturacion
                    }
                    else {
                    }
                },
                error: function () {
                }
            });
            if(traerDatoFilaSeleccionada("listDocumentosHistoricos", "TIPO") == "CRNC"){
                $.ajax({
                    url: `${endpointPhpServ}/algoritmoERC`,
                    type: 'GET',
                    data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response.Estado == 2) {

                            document.getElementById('formulario.pcnp.c7').value = response.data.estadio;
                            document.getElementById('formulario.pcnp.c72').value = response.data.evolucionEstadio; //Regrecion!

                            if (response.data.erc != "" && response.data.erc != null) {
                                document.getElementById('formulario.pcnp.c6').value = response.data.erc;
                            }

                            if (response.data.alerta != "" && response.data.alerta != null) {
                                console.log("entra alerta ");
                                alert(response.data.alerta);
                            }

                            if (response.data.fecha_erc != "" && response.data.fecha_erc != null) {
                                document.getElementById('formulario.pcnp.c49').value = response.data.fecha_erc;
                            }

                            if (response.data.descripcion != "" && response.data.descripcion != null) {
                                document.getElementById('formulario.pcnp.c74').value = response.data.descripcion + " " + response.data.alerta;
                            }

                        } else {
                            console.log("No se Ejecuto el algoritmo");
                        }


                    },
                    error: function () {
                    },
                    complete: function (response) {
                        if (response.mensaje) {
                            alert("Error al Ejecutar Algoritmo ERC, vuelva a abrir el tab \" CRONICOS - NEFROPROTECCION \"");
                            console.log(response.mensaje)
                        }
                    }
                });
            }
        }
        if (tipo == 'atpe') {
            $.ajax({
                url: `${endpointPhpServ}/fechas`,
                type: 'GET',
                data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.valor != 'sin calculo') {
                        document.getElementById('formulario.atpe.c3').disabled = 'disabled'
                        document.getElementById('formulario.atpe.c5').disabled = 'disabled'
                        document.getElementById('formulario.atpe.c3').value = response.hta
                        document.getElementById('formulario.atpe.c4').value = response.fecha_hta
                        document.getElementById('formulario.atpe.c5').value = response.dm
                        document.getElementById('formulario.atpe.c6').value = response.fecha_dm
                    }
                    else {
                    }
                },
                error: function () {
                }
            });
        }
        if (tipo == 'inrr') {
            validacionesIndiceRiesgoReproductivo();
        }
        //Validaciones Aiepi}

        if (tipo == 'sigaiepi') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.sigaiepi.c6')
                setTimeout(() => {
                    ValidacionesAIEPI('formulario.sigaiepi.c33')
                }, 100);
            }
        }
        if (tipo == 'diarrea_sal_ment') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.diarrea_sal_ment.c23')
            }
        }
        if (tipo == 'fiapi') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.fiapi.c48')
            }
        }
        if (tipo == 'oido_garganta') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.oido_garganta.c14')
                setTimeout(() => {
                    ValidacionesAIEPI('formulario.oido_garganta.c23')
                }, 100);
            }
        }
        if (tipo == 'maltaiep') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.maltaiep.c73')
            }
        }
        //Menor de 2 meses AIEPI
        if (tipo == 'eferm_grave_infec') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.eferm_grave_infec.c44')
            }
        }
        if (tipo == 'diarrea_saludm') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.diarrea_saludm.c11')
            }
        }
        if (tipo == 'desarrollo_vacunas') {
            if (valorAtributo("txtIdEsdadoDocumento") == '0') {
                ValidacionesAIEPI('formulario.desarrollo_vacunas.c20')
            }
        }
    }
    if (tipo == "clastrge") {
        //alert("ingreso a clastrge...");
        comboMed();
    }
    if (tipo == 'agnut') {
        try {
            document.getElementById('formulario.agnut.c3').disabled = true
            document.getElementById('formulario.agnut.c4').disabled = true
            document.getElementById('formulario.agnut.c5').disabled = true
        } catch (error) { }

    }
}

function buscarRiesgoEncuesta(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {
        case 'listEncuestaPacienteRiesgo':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=879&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdEncuesta'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id_encuesta_paciente', 'nombre', 'semaforo'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_encuesta_paciente', index: 'id_encuesta_paciente', hidden: true },
                    { name: 'nombre', index: 'nombre', width: anchoP(ancho, 55) },
                    { name: 'semaforo', index: 'Semaforo', width: anchoP(ancho, 35) }
                ],
                height: 80,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    tabsContenidoEncuesta('desdeEncuesta');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;
    }
}

function TabActivoEncuesta(idTab) {
    asignaAtributo('lblIdConte', idTab, 0);
}
function graficaEncuesta() {
    mostrar('divVentanitaGrafica')
    contenedorGrafica()
}

function contenedorGrafica() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/encuesta/grafica.jsp?' + 'idGrafica=' + valorAtributo('lblIdGrafica'), true);
    varajaxMenu.onreadystatechange = llenarcontenedorGrafica;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);

}

function llenarcontenedorGrafica() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("contenedorGraf").innerHTML = varajaxMenu.responseText;
            //    buscarEncuesta('listEncuestaPaciente')

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function calculoAncho() {
    total = 100;
    ancho1 = valorAtributo('txtAnCol1');
    ancho2 = total - ancho1;
    document.getElementById('txtAnCol2').value = ancho2;

}

function ventanitaRecomendacionesEncuesta() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/hc/ventanitaRecomendaciones.jsp?' + '&recomendacion=' + valorAtributo('txtRecomendacionEncuesta'), true);
    varajaxMenu.onreadystatechange = function () { llenarventanitaRecomendacionesEncuesta() };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarventanitaRecomendacionesEncuesta() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaventanitaRecomendaciones").innerHTML = varajaxMenu.responseText;
            mostrar('divParaventanitaRecomendaciones');
            mostrar('divVentanitaPuestaRecomendaciones');
            formatoRecomendacionTexto();

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}


function getVersionRender(tipo,tipo_folio) {
    return new Promise((resolve,reject) => {        
        valores_a_mandar = "";
        add_valores_a_mandar(tipo);
        add_valores_a_mandar(tipo_folio);    
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 1131,
                "parametros": valores_a_mandar,
                "_search": false,
                "nd": 1611873687449,
                "rows": -1,
                "page": 1,
                "sidx": "NO FACT",
                "sord": "asc"
            },
            beforeSend: function () { },
            success: function (data) {
                let page = "";
                let version = "";
                const response = data.getElementsByTagName("cell");
                if(response[1].firstChild.data == 1){
                    page = "/clinica/paginas/encuesta/subContenidoFolioPlantillaV1.jsp";
                    version = "v1";
                }else{
                    page = "/clinica/paginas/encuesta/subContenidoFolioPlantilla.jsp";
                    version = "v2";
                }
                let versionRender = {
                    "page": page,
                    "version": version
                }
                resolve(versionRender)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('ERROR EN LA PETICION. POR FAVOR COMUNIQUESE CON SOPORTE');
            }
        });
    })
}

function resolvePromise(pagina,datos,tipo,id_estado_folio,modo,version,tipoContenido =1){    
    return new Promise((resolve = () => { }, reject = () => { }) => {        
        $.ajax({
            url: pagina,
            type: "GET",
            data: datos,
            contentType: "application/x-www-form-urlencoded;charset=iso-8859-1",
            beforeSend: function () {
                //mostrarLoader(urlParams.get("accion"))
            },
            success: function (data) {
                ocultarLoadresTabs();
                document.getElementById('div_' + tipo).innerHTML = data;
                if (tipoContenido == 2) {
                    traerContenidoDocumentoSeleccionado(tipo.toUpperCase(), traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'), "txt_")
                }

                if (tipo == 'c2ga') {
                    TabActivoConductaTratamiento('clap2')
                }

                if (id_estado_folio === '0' || modo == "desdeEncuesta") {
                    setTimeout(() => {
                        calculosIniciales(tipo, modo);
                    }, 200);
                    validacionesIniciales(tipo, modo, version);
                }

                llenartabsSubContenidoEncuesta(tipo, modo);
                validacionesHcVih(tipo);
                /*MP para ocultar campos de esta plantilla cuando su valor es 0  */
                // console.log('mig' , tipo)
                try {
                    if (['EPOC'].includes(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO')) && tipo == 'pcep') {
                        console.log('set')
                        console.log('set')
                        if (document.getElementById('formulario.pcep.c37').value == '0' || document.getElementById('formulario.pcep.c37').value == '') {
                            document.getElementById('formulario.pcep.c38').parentNode.parentNode.setAttribute('style', 'display:none')
                        }
                        if (document.getElementById('formulario.pcep.c46').value == '0' || document.getElementById('formulario.pcep.c46').value == '') {
                            document.getElementById('formulario.pcep.c42').parentNode.parentNode.setAttribute('style', 'display:none')
                            document.getElementById('formulario.pcep.c43').parentNode.parentNode.parentNode.setAttribute('style', 'display:none')
                        }
                        if (document.getElementById('formulario.pcep.c20').value == '0' || document.getElementById('formulario.pcep.c20').value == '') {
                            document.getElementById('formulario.pcep.c21').parentNode.parentNode.setAttribute('style', 'display:none');
                        }
                    }

                } catch (error) {

                }
                try {
                    if (tipo == 'vanenf') {
                        bloquearCamposvanenf()
                    }
                } catch (error) {

                }
                
                cargarSeclectChoice()
                resolve();
            },
            complete: function (jqXHR, textStatus) {
                //ocultarLoader(jqXHR.responseXML.getElementsByTagName('accion')[0].firstChild.data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                reject();
                swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
            }
        });

    });
}

/**
 * Inicializa las instancias de Choices.js para una lista de selects.
 * Itera sobre un array de IDs de elementos select, obtiene cada elemento select del DOM,
 * y luego inicializa una instancia de Choices.js para ese select. 
 * Si un select no se encuentra, lanza y registra un error.
 * Los IDs de los selects a inicializar están predefinidos en la función.
 *
 * @function cargarSelectChoice
 * @returns {void} No retorna ningún valor.
 */
function cargarSeclectChoice() {

    const selectIds = ['formulario.evfr.c46', 'formulario.evfr.c47', 'formulario.evfr.c48'];
    const choicesInstances = [];

    selectIds.forEach(selectId => {
        try {
            const selectElement = document.getElementById(selectId);
            if (!selectElement) {
                throw new Error(`El elemento select con ID ${selectId} no se encontró.`);
            }

            const choicesInstance = new Choices(selectElement, {
                placeholder: false,
                itemSelectText: '',
                shouldSort: true
            });

            choicesInstances.push(choicesInstance);
        } catch (error) {
            console.error('Error al inicializar Choices.js para el select:', selectId, error);
            // Manejo adicional del error, si se desea
        }
    });
    /* var selects = document.querySelectorAll('.selectInput[data-choices]');
    selects.forEach(function (select) {
        var choices = new Choices(select);
    }); */
    /* document.addEventListener('DOMContentLoaded', function () {
    }); */
    //var choices = new Choices('select[data-choices]')
}

function bloquearCampo(idCampo) {
    const campo = document.getElementById(idCampo);

    if (campo) {
        // Si el elemento existe, agregar el atributo 'disabled'
        campo.disabled = true;
    } else {
        // Si el elemento no existe, mostrar un mensaje en la consola
        console.log(`El elemento con ID '${idCampo}' no existe.`);
    }
}

function bloquearCamposvanenf() {
    if (edad > 17) {
        bloquearCampo("formulario.vanenf.c98");
        bloquearCampo("formulario.vanenf.c44");
        bloquearCampo("formulario.vanenf.c45");
        bloquearCampo("formulario.vanenf.c46");
        bloquearCampo("formulario.vanenf.c99");
        bloquearCampo("formulario.vanenf.c100");
    }
}

function changeVersionPage(version,tipo,tipo_folio,id_evolucion,sexo,rango,rango2,edad,id_estado_folio,modo){    
    let pagina = ''
    if(version == 'v1'){
        pagina = '/clinica/paginas/encuesta/subContenidoFolioPlantillaV1.jsp'
        $("#principal").hide()
    }else{
        pagina = '/clinica/paginas/encuesta/subContenidoFolioPlantilla.jsp'
        $("#div2").hide()
    }
    mostrarLoadresTabs(modo);            
    let datos = {
        "tipo": tipo,
        "tipo_folio": tipo_folio,
        "id_evolucion": id_evolucion,
        "id_sexo": sexo,
        "rango": rango,
        "rango2": rango2,
        "edade": edad,
        "profesional": IdSesion(),
        "servicio_auditoria": valorAtributo("txtServicioAuditoria"),
        "id_estado_folio": id_estado_folio,
        "modo": modo
    }
    resolvePromise(pagina,datos,tipo,id_estado_folio,modo,version);
}

function getVersionRenderTamizaje(tipo,idEncuesta){
    return new Promise((resolve,reject) => {        
        valores_a_mandar = "";
        add_valores_a_mandar(tipo);
        add_valores_a_mandar(idEncuesta);    
        $.ajax({
            url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
            type: "POST",
            data: {
                "idQuery": 1132,
                "parametros": valores_a_mandar,
                "_search": false,
                "nd": 1611873687449,
                "rows": -1,
                "page": 1,
                "sidx": "NO FACT",
                "sord": "asc"
            },
            beforeSend: function () { },
            success: function (data) {
                let page = "";
                let version = "";
                const response = data.getElementsByTagName("cell");
                if(response[1].firstChild.data == 1){
                    page = "/clinica/paginas/encuesta/subContenedorEncuestaV1.jsp";
                    version = "v1";
                }else{
                    page = "/clinica/paginas/encuesta/subContenedorEncuesta.jsp";
                    version = "v2";
                }
                let versionRender = {
                    "page": page,
                    "version": version
                }
                resolve(versionRender)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('ERROR EN LA PETICION. POR FAVOR COMUNIQUESE CON SOPORTE');
            }
        });
    })
}

function changeVersionPageTamizaje(version,tipo,idEncuesta,idEncuestaPaciente,sexo,rango,rango2,edad,modo){
    let pagina = ''
    if(version == 'v1'){
        pagina = '/clinica/paginas/encuesta/subContenedorEncuestaV1.jsp'
        $("#principal").hide()
    }else{
        pagina = '/clinica/paginas/encuesta/subContenedorEncuesta.jsp'
        $("#div1").hide()
    }
    mostrarLoadresTabs(modo);    
    let datos = {
        "tipo": tipo,
        "id_parametro": idEncuesta ? idEncuesta : valorAtributo('lblIdEncuesta'),
        "id_encuestaPaciente": idEncuestaPaciente,
        "id_sexo": sexo,
        "rango": rango,
        "rango2": rango2,
        "edade": edad
    }
    resolvePromise(pagina,datos,tipo,null,modo,version);
}




function trasladarDaan(id, daanValue) {
    try {
        const acciones = {
            'formulario.daan.c1': (value) => guardarDatosPlantilla('crecimien_prac_alim', 'c30', `${parseFloat(value) * 1000}`),
            'formulario.daan.c2': (value) => guardarDatosPlantilla('crecimien_prac_alim', 'c32', value),
            'formulario.daan.c15': (value) => guardarDatosPlantilla('crecimien_prac_alim', 'c83', value),
        };

        if (acciones[id]) {
            acciones[id](daanValue);
        }
    } catch (error) {
        console.error('fallo la funcion trasladarDaan')
    }
}


function fechasHTA(campo, tabla) {
    const endpointPhpServ = "http://10.0.0.3:9010";
    if (`${tabla}.${campo}` == 'atpe.c4' || `${tabla}.${campo}` == 'atpe.c6') {
        $.ajax({
            url: `${endpointPhpServ}/fechas`,
            type: 'GET',
            data: { 'id_evolucion': valorAtributo("lblIdDocumento") },
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.valor != 'sin calculo') {
                    document.getElementById('formulario.atpe.c3').value = response.hta
                    document.getElementById('formulario.atpe.c5').value = response.dm
                }
                else {
                }
            },
            error: function () {
            }
        });
    }
}