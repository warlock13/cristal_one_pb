<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<%  

Connection conexion = iConnection.getConnection();
Conexion cn = new Conexion(conexion);

ControlAdmin beanAdmin = new ControlAdmin();
beanAdmin.setCn(cn);

ArrayList resulCombo = new ArrayList();
ComboVO cmb;

String id_evolucion = request.getParameter("id_evolucion");
String cbColumnaCervicalFlexion = "";
String cbColumnaCervicalExtension = "";
String cbColumnaCervicalInclinacionLateralIz = "";
String cbColumnaCervicalInclinacionLateralDer = "";
String cbColumnaCervicalRotacionLateralIz = "";
String cbColumnaCervicalRotacionLateralDer = "";
String cbDorsoLumbarFlexion = "";
String cbDorsoLumbarExtension = "";
String cbDorsoLumbarInclinacionLateralIz = "";
String cbDorsoLumbarInclinacionLateralDer = "";
String cbDorsoLumbarRotacionLateralIz = "";
String cbDorsoLumbarRotacionLateralDer = "";

String flexionColumnaCervical = "";
String extensionColumnaCervical = "";
String inclinacionLateralColumnaCervicalIz = "";
String inclinacionLateralColumnaCervicalDer = "";
String rotacionLateralColumnaCervicalIz = "";
String rotacionLateralColumnaCervicalDer = "";
String flexionDorsoLumbar = "";
String extensionDorsoLumnar = "";
String inclinacionLateralDorsoLumbarIz = "";
String inclinacionLateralDorsoLumbarDer = "";
String rotacionDorsoLumnarIz = "";
String rotacionDorsoLumnarDer = "";

try{

Statement stmt = conexion.createStatement();
String SQL = " ";


    SQL = "select " +

    "coalesce(vrl5.c91,  '')  as cbColumnaCervicalFlexion, "+  
    "coalesce(vrl5.c92,  '')  as cbColumnaCervicalExtension, "+ 
    "coalesce(vrl5.c93,  '')  as cbColumnaCervicalInclinacionLateralIz, "+  
    "coalesce(vrl5.c94,  '')  as cbColumnaCervicalInclinacionLateralDer, "+ 
    "coalesce(vrl5.c95,  '')  as cbColumnaCervicalRotacionLateralIz, "+ 
    "coalesce(vrl5.c96,  '')  as cbColumnaCervicalRotacionLateralDer, "+
    "coalesce(vrl5.c97,  '')  as cbDorsoLumbarFlexion, "+ 
    "coalesce(vrl5.c98,  '')  as cbDorsoLumbarExtension, "+
    "coalesce(vrl5.c99,  '')  as cbDorsoLumbarInclinacionLateralIz, "+ 
    "coalesce(vrl5.c100, '')  as cbDorsoLumbarInclinacionLateralDer, "+ 
    "coalesce(vrl5.c101, '')  as cbDorsoLumbarRotacionLateralIz, "+
    "coalesce(vrl5.c102, '')  as cbDorsoLumbarRotacionLateralDer, "+
    "coalesce(vrl4.c1,  '')  as flexionColumnaCervical, "+  
    "coalesce(vrl4.c3,  '')  as extensionColumnaCervical, "+  
    "coalesce(vrl4.c5,  '')  as inclinacionLateralColumnaCervicalIz, "+ 
    "coalesce(vrl4.c6,  '')  as inclinacionLateralColumnaCervicalDer, "+
    "coalesce(vrl4.c7,  '')  as rotacionLateralColumnaCervicalIz, "+ 
    "coalesce(vrl4.c8,  '')  as rotacionLateralColumnaCervicalDer, "+
    "coalesce(vrl4.c17, '')  as flexionDorsoLumbar, "+ 
    "coalesce(vrl4.c19, '')  as extensionDorsoLumnar, "+ 
    "coalesce(vrl4.c21, '')  as inclinacionLateralDorsoLumbarIz, "+ 
    "coalesce(vrl4.c22, '')  as inclinacionLateralDorsoLumbarDer, "+ 
    "coalesce(vrl4.c23, '')  as rotacionDorsoLumnarIz, "+
    "coalesce(vrl4.c24, '')  as rotacionDorsoLumnarDer "+
    
    "from hc.evolucion as hc "+ 
    "left join formulario.vrl5 as vrl5 "+    
    "on hc.id = vrl5.id_evolucion "+
    "left join formulario.vrl4 as vrl4 "+
    "on hc.id = vrl4.id_evolucion "+
    "where hc.id = "+ id_evolucion;

    System.out.println(SQL);

  ResultSet rs = stmt.executeQuery(SQL);
  while(rs.next()){
    cbColumnaCervicalFlexion = rs.getString("cbColumnaCervicalFlexion");
    cbColumnaCervicalExtension = rs.getString("cbColumnaCervicalExtension");
    cbColumnaCervicalInclinacionLateralIz = rs.getString("cbColumnaCervicalInclinacionLateralIz");
    cbColumnaCervicalInclinacionLateralDer = rs.getString("cbColumnaCervicalInclinacionLateralDer");
    cbColumnaCervicalRotacionLateralIz = rs.getString("cbColumnaCervicalRotacionLateralIz");
    cbColumnaCervicalRotacionLateralDer = rs.getString("cbColumnaCervicalRotacionLateralDer");
    cbDorsoLumbarFlexion = rs.getString("cbDorsoLumbarFlexion");
    cbDorsoLumbarExtension = rs.getString("cbDorsoLumbarExtension");
    cbDorsoLumbarInclinacionLateralIz = rs.getString("cbDorsoLumbarInclinacionLateralIz");
    cbDorsoLumbarInclinacionLateralDer = rs.getString("cbDorsoLumbarInclinacionLateralDer");
    cbDorsoLumbarRotacionLateralIz = rs.getString("cbDorsoLumbarRotacionLateralIz");
    cbDorsoLumbarRotacionLateralDer = rs.getString("cbDorsoLumbarRotacionLateralDer");
    
    flexionColumnaCervical = rs.getString("flexionColumnaCervical");
    extensionColumnaCervical = rs.getString("extensionColumnaCervical");
    inclinacionLateralColumnaCervicalIz = rs.getString("inclinacionLateralColumnaCervicalIz");
    inclinacionLateralColumnaCervicalDer = rs.getString("inclinacionLateralColumnaCervicalDer");
    rotacionLateralColumnaCervicalIz = rs.getString("rotacionLateralColumnaCervicalIz");
    rotacionLateralColumnaCervicalDer = rs.getString("rotacionLateralColumnaCervicalDer");
    flexionDorsoLumbar = rs.getString("flexionDorsoLumbar");
    extensionDorsoLumnar = rs.getString("extensionDorsoLumnar");
    inclinacionLateralDorsoLumbarIz = rs.getString("inclinacionLateralDorsoLumbarIz");
    inclinacionLateralDorsoLumbarDer = rs.getString("inclinacionLateralDorsoLumbarDer");
    rotacionDorsoLumnarIz = rs.getString("rotacionDorsoLumnarIz");
    rotacionDorsoLumnarDer = rs.getString("rotacionDorsoLumnarDer");
  }
}
catch ( Exception e ){
  System.out.println(e.getMessage());
} %>   



<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->
</head>
<body>
    <div style="background-color: white; height: 300px; width: 100%; overflow: scroll;">
    <div>
        <h1 class="card-title">VALORACION DE ARCOS DE MOVILIDAD Y FUERZA DE COLUMNA </h1>
        <hr>
        <h2 class="text-center center">Columna Cervical</h2>
        <table class="table table-bordered mt-1" id="tfoi">
            <thead>
              <tr>
                
                <th class="text-center center" scope="col"> Movimiento </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza Muscular </th>
                <th class="text-center center" scope="col"> Rango normal </th>
              </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input  id="vrl4c1" value="<%=flexionColumnaCervical%>" type="number" max="60" min="-60" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c91" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbColumnaCervicalFlexion.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                    <th class="text-center center" scope="row">0 - 60</th>
                </tr>
                <tr>
                    <th class="text-center center" scope="row">Extensores</th>
                    <td class="text-center center"><input  id="vrl4c3" value="<%=extensionColumnaCervical%>" type="number" max="75" min="-75" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                  <td class="text-center" style="width: 20%;">
                    <select  id="vrl5c92" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                      <option value=""></option>
                      <%  resulCombo.clear();
                      resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                       for(int i=0;i<resulCombo.size();i++){
                          cmb=(ComboVO)resulCombo.get(i);
                          if(cbColumnaCervicalExtension.trim().equals(cmb.getId())){
                           %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                           <%}
                           else{
                           %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                           <%
                         }
                     }%>
                    </select> 
                  </td>
                    <th class="text-center center" scope="row">0 - 75</th>
                </tr>
 
             
            
            </tbody>
        </table>
    </div>
    <div>
        <hr>
        <h2 class="text-center center">Columna Cervical  (Izquierdo / Derecho)</h2>
        <table class="table table-bordered mt-1" id="tfoi">
            <thead>
              <tr>
                
                <th class="text-center center" scope="col"> Movimiento </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza muscular </th>
                <th class="text-center center" scope="col"> Rango normal </th>
                <th class="text-center center" scope="col"> Movimiento  </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza muscular </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th class="text-center center" scope="row">Inclinacion lateral Izquierda</th>
                <td class="text-center center"><input  id="vrl4c5" value="<%=inclinacionLateralColumnaCervicalIz%>" type="number" max="45" min="-45" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c93" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbColumnaCervicalInclinacionLateralIz.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                <td class="text-center">0 -45</td>
                <th class="text-center center" scope="row">Inclinacion lateral Derecha</th>
                <td class="text-center center"><input  id="vrl4c6" value="<%=inclinacionLateralColumnaCervicalDer%>" type="number" max="45" min="-45" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c94" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbColumnaCervicalInclinacionLateralDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
              <tr>
                <th class="text-center center" scope="row">Rotacion lateral Izquierda</th>
                <td class="text-center center"><input  id="vrl4c7" value="<%=rotacionLateralColumnaCervicalIz%>" type="number" max="90" min="-90" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c95" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbColumnaCervicalRotacionLateralIz.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                <td class="text-center">0 - 90</td>
                <th class="text-center center" scope="row">Rotacion Lateral Derecha</th>
                <td class="text-center center"><input  id="vrl4c8" value="<%=rotacionLateralColumnaCervicalDer%>" type="number" max="90" min="-90" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c96" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbColumnaCervicalRotacionLateralDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
              
             
             
            
            </tbody>
        </table>
    </div>
    <div>
        <hr>
        <h2 class="text-center center">Dorso Lumbar </h2>
        <table class="table table-bordered mt-1" id="tfoi">
            <thead>
              <tr>
                
                <th class="text-center center" scope="col"> Movimiento </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza Muscular </th>
                <th class="text-center center" scope="col"> Rango normal </th>
              </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center center" scope="row">FLEXORES</th>
                    <td class="text-center center"><input   id="vrl4c17" value="<%=flexionDorsoLumbar%>" type="number" max="60" min="-60" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c97" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDorsoLumbarFlexion.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                    <th class="text-center center" scope="row">0 - 60</th>
                </tr>
                <tr>
                    <th class="text-center center" scope="row">Extensores</th>
                    <td class="text-center center"><input  id="vrl4c19" value="<%=extensionDorsoLumnar%>" type="number" max="35" min="-35" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c98" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDorsoLumbarExtension.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                    <th class="text-center center" scope="row">0 - 35</th>
                </tr>
 
             
            
            </tbody>
        </table>
    </div>
    <div>
        <hr>
        <h2 class="text-center center">Dorso Lumbar  (Izquierdo / Derecho)</h2>
        <table class="table table-bordered mt-1" id="tfoi">
            <thead>
              <tr>
                
                <th class="text-center center" scope="col"> Movimiento </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza muscular </th>
                <th class="text-center center" scope="col"> Rango normal </th>
                <th class="text-center center" scope="col"> Movimiento  </th>
                <th class="text-center center" scope="col"> Rango encontrado </th>
                <th class="text-center center" scope="col"> Fuerza muscular </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th class="text-center center" scope="row">Inclinacion lateral Izquierda</th>
                <td class="text-center center"><input  id="vrl4c21" value="<%=inclinacionLateralDorsoLumbarIz%>" type="number" max="45" min="-45" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                    <td class="text-center" style="width: 20%;">
                      <select  id="vrl5c99" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                        <option value=""></option>
                        <%  resulCombo.clear();
                        resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                         for(int i=0;i<resulCombo.size();i++){
                            cmb=(ComboVO)resulCombo.get(i);
                            if(cbDorsoLumbarInclinacionLateralIz.trim().equals(cmb.getId())){
                             %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                             <%}
                             else{
                             %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                             <%
                           }
                       }%>
                      </select> 
                    </td>
                <td class="text-center">0 -45</td>
                <th class="text-center center" scope="row">Inclinacion lateral Derecha</th>
                <td class="text-center center"><input  id="vrl4c22" value="<%=inclinacionLateralDorsoLumbarDer%>" type="number" max="45" min="-45" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c100" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDorsoLumbarInclinacionLateralDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
              <tr>
                <th class="text-center center" scope="row">Rotacion lateral Izquierda</th>
                <td class="text-center center"><input  id="vrl4c23" value="<%=rotacionDorsoLumnarIz%>" type="number" max="35" min="-35" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c101" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDorsoLumbarRotacionLateralIz.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
                <td class="text-center">0 - 35</td>
                <th class="text-center center" scope="row">Rotacion Lateral Derecha</th>
                <td class="text-center center"><input  id="vrl4c24" value="<%=rotacionDorsoLumnarDer%>" type="number" max="35" min="-35" maxlength="2" style="width: 45%;" oninput="maxLongitudNumero(this.id,'2');"  onkeyup="ingresarLimites(this.id,this.max,this.min,this.value);" onblur="guardarDatosPlantillaHTML('vrl4',this.id,this.value)"></td>
                <td class="text-center" style="width: 20%;">
                  <select  id="vrl5c102" size="1" style="width: 90%;" onchange="guardarDatosPlantillaHTML('vrl5',this.id,this.value)">
                    <option value=""></option>
                    <%  resulCombo.clear();
                    resulCombo=(ArrayList)beanAdmin.combo.cargar(1041);                           
                     for(int i=0;i<resulCombo.size();i++){
                        cmb=(ComboVO)resulCombo.get(i);
                        if(cbDorsoLumbarRotacionLateralDer.trim().equals(cmb.getId())){
                         %><option value="<%= cmb.getId()%>" selected ><%= cmb.getDescripcion()%></option>   
                         <%}
                         else{
                         %><option value="<%= cmb.getId()%>"><%= cmb.getDescripcion()%></option>   
                         <%
                       }
                   }%>
                  </select> 
                </td>
              </tr>
             
             
            
            </tbody>
        </table>
    </div> 

</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</html>
