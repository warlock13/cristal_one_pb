<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/> 

<%  
ArrayList resulCombo = new ArrayList();

String id_evolucion = request.getParameter("id_evolucion");
String cabezaInclinada = "";
String cabezaRotada = "";
String cabezaAdelantadaVl = "";
String cabezaCentradaVl = "";
String cabezaInclinadaVp = "";
String cabezaRotadaVp = "";
String hmElevadoIzquierdoVa = "";
String hmElevadoDerechoVa = "";
String hmDescendidoDerechoVa = "";
String hmDescendidoIzquierdoVa = "";
String hmProtruidosDerechoVl = "";
String hmProtuidosIzquierdoVl = "";
String hmRetridosDerechoVl = "";
String hmRetraidosIzquierdo = "";
String hmElevadoDerechoVp = "";
String hmElevadoIzquierdoVp = "";
String hmDescendidoDerechoVp = "";
String hmDescendidoIzquierdoVp = "";
String hiperdolosisCervicalVl = "";
String dorsalHiperciFosisVa = "";
String dorsalColumnaRecta = "";
String toraxQuillaVa = "";
String pectusScavatumVa = "";
String toraxTonelVa = "";
String toraxTonelVl = "";
String hipercifosisVl = "";
String abdomenProminenteVa = "";
String abdomenProminenteVl = "";
String lumbarHiperlordosisVa = "";
String lumbarColumnaRecta = "";
String pelvisRotacionVa = "";
String pelvisAscendidaDerechaVa = "";
String pelvisAscendidaIzquierdaVa = "";
String pelvisDescendidaDerechaVa = "";
String pelvisDescendidaIzquierdaVa = "";
String pelvisRetrovesionVl = "";
String pelvisAnteversionVl = "";
String pelvisRotacionVp = "";
String rodillasGenuVaroVa = "";
String rodillasGenuValgusVa = "";
String rodillasHiperextensionDerechoVl = "";
String rodillasHiperextensionIzquierdoVl = "";
String rodillasFlexionDerechoVl = "";
String rodillasFlexionIzquierdoVl = "";
String rodillasGenuVaroVp = "";
String rodillasGeuValgusVp = "";
String tibiaTorsionFemAnt = "";
String tibiaTorisionTibialAnt = "";
String tibiaTorsionFemPos = "";
String tibiaTorsionTibialPos = "";
String piesInvertidosAnt = "";
String piesEvertidosAnt = "";
String piesHalluxValAnt = "";
String piePlanoDerechoPos = "";
String piePlanoIzquierdoPos = "";
String pieCavoDerechoPos = "";
String pieCavoIzquierdoPos = "";
String pieInvertidoDerechoPos = "";
String pieInvertidoIzquierdoPos = "";
String pieEvertidoDerechoPos = "";
String pieEvertidoIzquierdoPos = "";
String piePronadoDerechoPos = "";
String piePronadoIzquierdoPos = "";
String pieSupinadoDerechoPos = "";
String pieSupinadoIzquierdoPos = "";
Connection conexion = iConnection.getConnection();
try{

Statement stmt = conexion.createStatement();
String SQL = " ";


    SQL = "select " +

    "coalesce(c1,'')    as  cabezainclinada, " +
    "coalesce(c2, '')   as  cabezarotada, " +
    "coalesce(c25, '')  as  cabeza_adelantada_vl, "+
    "coalesce(c26, '')  as  cabeza_centrada_vl, "+
    "coalesce(c52, '')  as  cabeza_inclinada_vp, "+
    "coalesce(c53, '')  as  cabeza_rotada_vp, "+
    "coalesce(c5, '')   as  hm_elevado_izquierdo_va, "+
    "coalesce(c6, '')   as  hm_elevado_derecho_va, "+
    "coalesce(c7, '')   as  hm_descendido_derecho_va, "+
    "coalesce(c8, '')   as  hm_descendido_izquierdo_va, "+
    "coalesce(c29, '')  as  hm_protruidos_derecho_vl, "+
    "coalesce(c30, '')  as  hm_protruidos_izquierdo_vl, "+
    "coalesce(c31, '')  as  hm_retraidos_derecho_vl, "+
    "coalesce(c32, '')  as  hm_retraidos_izquierdo_vl, "+
    "coalesce(c55, '')  as  hm_elevado_derecho_vp, "+
    "coalesce(c56, '')  as  hm_elevado_izquierdo_vp, "+
    "coalesce(c57, '')  as  hm_descendido_derecho_vp, "+
    "coalesce(c58, '')  as  hm_descendido_izquierdo_vp, "+
    "coalesce(c33, '')  as  hiperlordosis_cervical_vl, "+
    "coalesce(c34, '')  as  dorsal_hipercifosis_va, "+
    "coalesce(c35, '')  as  dorsal_columna_recta_va, "+
    "coalesce(c9, '')   as  torax_en_quilla_va, "+
    "coalesce(c10, '')  as  pectus_escavatum_va, "+
    "coalesce(c11, '')  as  torax_en_tonel_va, "+
    "coalesce(c36, '')  as  torax_en_tonel_vl, "+
    "coalesce(c37, '')  as  hipercifosis_vl, "+
    "coalesce(c12, '')  as  abdomen_prominente_va, "+
    "coalesce(c38, '')  as  abdomen_prominente_vl, "+
    "coalesce(c39, '')  as  lumbar_hiperlordosis_va, "+
    "coalesce(c40, '')  as  lumbar_columna_recta, "+
    "coalesce(c13, '')  as  pelvis_rotacion_pelva_va, "+
    "coalesce(c14, '')  as  pelvis_ascendida_derecha_va, "+
    "coalesce(c15, '')  as  pelvis_ascendida_izquierda_va, "+
    "coalesce(c16, '')  as  pelvis_descendida_derecha_va, "+
    "coalesce(c17, '')  as  pelvis_descendida_izquierda_va, "+
    "coalesce(c41, '')  as  pelvis_retroversion_vl, "+
    "coalesce(c42, '')  as  pelvis_anteversion_vl, "+
    "coalesce(c61, '')  as  pelvis_rotacion_vp, "+
    "coalesce(c18, '')  as  rodillas_genu_varo_pelan_va, "+
    "coalesce(c19, '')  as  rodillas_genu_valgus_pelan_va, "+
    "coalesce(c43, '')  as  rodillas_hiperextension_derecho_vl, "+
    "coalesce(c44, '')  as  rodillas_hiperextension_izquierdo_vl, "+
    "coalesce(c45, '')  as  rodillas_flexion_derecho_vl, "+
    "coalesce(c46, '')  as  rodillas_flexion_izquierdo_vl, "+
    "coalesce(c62, '')  as  rodillas_genu_varo_pelpos_vp, "+
    "coalesce(c63, '')  as  rodillas_genu_valgus_pelpos_vp, "+
    "coalesce(c20, '')  as  tibia_torsion_femoral_ant, "+
    "coalesce(c21, '')  as  tibia_torsion_tibial_ant, "+
    "coalesce(c64, '')  as  tibia_torsion_femoral_pos, "+
    "coalesce(c65, '')  as  tibia_torsion_tibial_pos, "+
    "coalesce(c22, '')  as  pies_invertidos_ant, "+
    "coalesce(c23, '')  as  pies_evertidos_ant, "+
    "coalesce(c24, '')  as  pies_hallux_valgus_ant, "+
    "coalesce(c47, '')  as  pie_plano_derecho_pos, "+
    "coalesce(c48, '')  as  pie_plano_izquierdo_pos, "+
    "coalesce(c49, '')  as  pie_cavo_derecho_pos, "+
    "coalesce(c50, '')  as  pie_cavo_izquierdo_pos, "+
    "coalesce(c66, '')  as  pie_invertidos_derecho_pos, "+
    "coalesce(c67, '')  as  pie_invertidos_izquierdo_pos, "+
    "coalesce(c68, '')  as  pie_evertidos_derecho_pos, "+
    "coalesce(c69, '')  as  pie_evertidos_izquierdo_pos, "+
    "coalesce(c70, '')  as  pie_pronado_derecho_pos, "+
    "coalesce(c71, '')  as  pie_pronado_izquierdo_pos, "+
    "coalesce(c72, '')  as  pie_supinado_derecho_pos, "+
    "coalesce(c73, '')  as  pie_supinado_izquierdo_pos "+
    
    "from hc.evolucion as hc "+ 
    "left join formulario.vrl6 as vrl6 "+
    "on hc.id = vrl6.id_evolucion "+
    "where hc.id = "+ id_evolucion;

    System.out.println(SQL);

  ResultSet rs = stmt.executeQuery(SQL);
  while(rs.next()){
    cabezaInclinada = rs.getString("cabezainclinada");
    cabezaRotada = rs.getString("cabezarotada");    
    cabezaAdelantadaVl = rs.getString("cabeza_adelantada_vl");
    cabezaCentradaVl = rs.getString("cabeza_centrada_vl");
    cabezaInclinadaVp = rs.getString("cabeza_inclinada_vp");
    cabezaRotadaVp = rs.getString("cabeza_rotada_vp");
    hmElevadoIzquierdoVa = rs.getString("hm_elevado_izquierdo_va");
    hmElevadoDerechoVa = rs.getString("hm_elevado_derecho_va");
    hmDescendidoDerechoVa = rs.getString("hm_descendido_derecho_va");
    hmDescendidoIzquierdoVa = rs.getString("hm_descendido_izquierdo_va");
    hmProtruidosDerechoVl = rs.getString("hm_protruidos_derecho_vl");
    hmProtuidosIzquierdoVl = rs.getString("hm_protruidos_izquierdo_vl");
    hmRetridosDerechoVl = rs.getString("hm_retraidos_derecho_vl");
    hmRetraidosIzquierdo = rs.getString("hm_retraidos_izquierdo_vl");
    hmElevadoDerechoVp = rs.getString("hm_elevado_derecho_vp");
    hmElevadoIzquierdoVp = rs.getString("hm_elevado_izquierdo_vp");
    hmDescendidoDerechoVp = rs.getString("hm_descendido_derecho_vp");
    hmDescendidoIzquierdoVp = rs.getString("hm_descendido_izquierdo_vp");
    hiperdolosisCervicalVl = rs.getString("hiperlordosis_cervical_vl");
    dorsalHiperciFosisVa = rs.getString("dorsal_hipercifosis_va");
    dorsalColumnaRecta = rs.getString("dorsal_columna_recta_va");
    toraxQuillaVa = rs.getString("torax_en_quilla_va");
    pectusScavatumVa = rs.getString("pectus_escavatum_va");
    toraxTonelVa = rs.getString("torax_en_tonel_va");
    toraxTonelVl = rs.getString("torax_en_tonel_vl");
    hipercifosisVl = rs.getString("hipercifosis_vl");
    abdomenProminenteVa = rs.getString("abdomen_prominente_va");
    abdomenProminenteVl = rs.getString("abdomen_prominente_vl");
    lumbarHiperlordosisVa = rs.getString("lumbar_hiperlordosis_va");
    lumbarColumnaRecta = rs.getString("lumbar_columna_recta");
    pelvisRotacionVa = rs.getString("pelvis_rotacion_pelva_va");
    pelvisAscendidaDerechaVa = rs.getString("pelvis_ascendida_derecha_va");
    pelvisAscendidaIzquierdaVa = rs.getString("pelvis_ascendida_izquierda_va");
    pelvisDescendidaDerechaVa = rs.getString("pelvis_descendida_derecha_va");
    pelvisDescendidaIzquierdaVa = rs.getString("pelvis_descendida_izquierda_va");
    pelvisRetrovesionVl = rs.getString("pelvis_retroversion_vl");
    pelvisAnteversionVl = rs.getString("pelvis_anteversion_vl");
    pelvisRotacionVp = rs.getString("pelvis_rotacion_vp");
    rodillasGenuVaroVa = rs.getString("rodillas_genu_varo_pelan_va");
    rodillasGenuValgusVa = rs.getString("rodillas_genu_valgus_pelan_va");
    rodillasHiperextensionDerechoVl = rs.getString("rodillas_hiperextension_derecho_vl");
    rodillasHiperextensionIzquierdoVl = rs.getString("rodillas_hiperextension_izquierdo_vl");
    rodillasFlexionDerechoVl = rs.getString("rodillas_flexion_derecho_vl");
    rodillasFlexionIzquierdoVl = rs.getString("rodillas_flexion_izquierdo_vl");
    rodillasGenuVaroVp = rs.getString("rodillas_genu_varo_pelpos_vp");
    rodillasGeuValgusVp = rs.getString("rodillas_genu_valgus_pelpos_vp");
    tibiaTorsionFemAnt = rs.getString("tibia_torsion_femoral_ant");
    tibiaTorisionTibialAnt = rs.getString("tibia_torsion_tibial_ant");
    tibiaTorsionFemPos = rs.getString("tibia_torsion_femoral_pos");
    tibiaTorsionTibialPos = rs.getString("tibia_torsion_tibial_pos");
    piesInvertidosAnt = rs.getString("pies_invertidos_ant");
    piesEvertidosAnt = rs.getString("pies_evertidos_ant");
    piesHalluxValAnt = rs.getString("pies_hallux_valgus_ant");
    piePlanoDerechoPos = rs.getString("pie_plano_derecho_pos");
    piePlanoIzquierdoPos = rs.getString("pie_plano_izquierdo_pos");
    pieCavoDerechoPos = rs.getString("pie_cavo_derecho_pos");
    pieCavoIzquierdoPos = rs.getString("pie_cavo_izquierdo_pos");
    pieInvertidoDerechoPos = rs.getString("pie_invertidos_derecho_pos");
    pieInvertidoIzquierdoPos = rs.getString("pie_invertidos_izquierdo_pos");
    pieEvertidoDerechoPos = rs.getString("pie_evertidos_derecho_pos");
    pieEvertidoIzquierdoPos = rs.getString("pie_evertidos_izquierdo_pos");
    piePronadoDerechoPos = rs.getString("pie_pronado_derecho_pos");
    piePronadoIzquierdoPos = rs.getString("pie_pronado_izquierdo_pos");
    pieSupinadoDerechoPos = rs.getString("pie_supinado_derecho_pos");
    pieSupinadoIzquierdoPos = rs.getString("pie_supinado_izquierdo_pos");
  }
  System.out.println(cabezaInclinada);
}
catch ( Exception e ){
  System.out.println(e.getMessage());
} %>   



<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="./estilosTFOI.css">
        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->
</head>
<body>
    <div style="background-color: white; height: 300px; width: 100%; overflow: scroll;">
        <div>
            <h1 class="card-title">Valoracion Postural </h1>
            <hr>
            <h2 class="text-center">Cabeza</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
      
                    <th class="text-center" scope="col"> Vista anterior </th>
                    <th class="text-center" scope="col">   Si/No </th>
                    <th class="text-center" scope="col"> Vista lateral </th>
                    <th class="text-center" scope="col">  Si/No </th>
                    <th class="text-center" scope="col"> Vista Posterior </th>
                    <th class="text-center" scope="col"> Si/No </th>
      
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        Inclinada
                        <hr>
                        Rotada
                    </th>
                    <td class="text-center">
                      <% if(cabezaInclinada.trim().equals("SI"))
                      { %>
                        <input id="c1" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c1" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                      
                      <hr>

                      <% if(cabezaRotada.trim().equals("SI"))
                      { %>
                          <input id="c2" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                          <input id="c2" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%> 
                      
                    </td>

                    <th class="text-center" scope="row">
                      Adelantada
                      <hr>
                      Centrada
                    </th>
                    <td class="text-center">
                      <% if(cabezaAdelantadaVl.trim().equals("SI"))
                      { %>
                        <input id="c25" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c25" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                      
                      <hr>

                      <% if(cabezaCentradaVl.trim().equals("SI"))
                      { %>
                          <input id="c26" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                          <input id="c26" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%> 
                      
                    </td>

                  <th class="text-center" scope="row">
                    Inclinada
                    <hr>
                    Rotada
                  </th>
                  <td class="text-center">
                    <% if(cabezaInclinadaVp.trim().equals("SI"))
                    { %>
                        <input id="c52" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c52" type="checkbox"  onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>    
                      <hr>
                      <% if(cabezaRotadaVp.trim().equals("SI"))
                      { %>
                          <input id="c53" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c53" type="checkbox"  onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>    
                  </td>

      
                  </tr>
      
      
                </tbody>
            </table>
        </div>
        <div>
          <hr>
          <h2 class="text-center">Hombros</h2>
          <table class="table table-bordered mt-1" id="tfoi">
              <thead>
                <tr>
    
                  <th class="text-center" scope="col"> Vista anterior </th>
                  <th class="text-center" scope="col"> Izquierdo Si/No </th>
                  <th class="text-center" scope="col"> Derecho Si/No </th>
                  <th class="text-center" scope="col"> Vista lateral </th>
                  <th class="text-center" scope="col"> Izquierdo Si/No </th>
                  <th class="text-center" scope="col"> Derecho Si/No </th>
                  <th class="text-center" scope="col"> Vista Posterior </th>
                  <th class="text-center" scope="col"> Izquierdo Si/No </th>
                  <th class="text-center" scope="col"> Derecho Si/No </th>
    
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="text-center" scope="row">
                      Elevado
                      <hr>
                      Descendido 
                  </th>
                  <td class="text-center">
                    <% if(hmElevadoIzquierdoVa.trim().equals("SI"))
                    { %>
                      <input id="c5" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c5" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                      <hr>
                      <% if(hmDescendidoIzquierdoVa.trim().equals("SI"))
                      { %>
                        <input id="c8" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c8" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                  </td>
                  <td class="text-center">
                    <% if(hmElevadoDerechoVa.trim().equals("SI"))
                    { %>
                      <input id="c6" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c6" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                    <hr>
                    <% if(hmDescendidoDerechoVa.trim().equals("SI"))
                    { %>
                      <input id="c7" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c7" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                  </td>
                  <th class="text-center" scope="row">
                    Protruidos
                    <hr>
                    Retraidos
                  </th>
                  <td class="text-center">
                    <% if(hmProtuidosIzquierdoVl.trim().equals("SI"))
                    { %>
                      <input id="c30" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c30" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                    <hr>
                    <% if(hmRetraidosIzquierdo.trim().equals("SI"))
                    { %>
                      <input id="c32" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c32" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                  </td>
                  <td class="text-center">
                    <% if(hmProtruidosDerechoVl.trim().equals("SI"))
                    { %>
                      <input id="c29" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c29" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                    <hr>
                    <% if(hmRetridosDerechoVl.trim().equals("SI"))
                    { %>
                      <input id="c31" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c31" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                  </td>
                <th class="text-center" scope="row">
                  Elevado
                  <hr>
                  Descendido
                </th>
                <td class="text-center">
                  <% if(hmElevadoIzquierdoVp.trim().equals("SI"))
                    { %>
                      <input id="c56" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c56" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                  <%}%>
                  <hr>
                  <% if(hmDescendidoIzquierdoVp.trim().equals("SI"))
                    { %>
                      <input id="c58" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c58" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                </td>
                <td class="text-center">
                  <% if(hmElevadoDerechoVp.trim().equals("SI"))
                    { %>
                      <input id="c55" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c55" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                  <hr>
                  <% if(hmDescendidoDerechoVp.trim().equals("SI"))
                    { %>
                      <input id="c57" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c57" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                </td>
                
    
                </tr>
    
              
    
    
              </tbody>
          </table>
      </div>
        <div>
            <hr>
            <h2 class="text-center">Columna Cervical</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
      
      
                    <th class="text-center" scope="col"> Vista lateral </th>
                    <th class="text-center" scope="col"> Si/No </th>
      
      
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        Hiperdolosis Cervical
      
                    </th>
                    <td class="text-center">
                      <% if(hiperdolosisCervicalVl.trim().equals("SI"))
                      { %>
                        <input id="c33" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c33" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
          
                    </td>
                  </tr> 
      
                
      
      
                </tbody>
            </table>
        </div>
        <div>
            <hr>
            <h2 class="text-center">Columna Dorsal</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
      
      
                    <th class="text-center" scope="col"> Vista lateral </th>
                    <th class="text-center" scope="col"> Si/No </th>
      
      
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        Hipercifosis
                        <hr>
                        Columna Recta
      
                    </th>
                    <td class="text-center">
                      <% if(dorsalHiperciFosisVa.trim().equals("SI"))
                      { %>
                        <input id="c34" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c34" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                        <hr>
                        <% if(dorsalColumnaRecta.trim().equals("SI"))
                        { %>
                          <input id="c35" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c35" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
                    </td>
                  </tr> 
      
                
      
      
                </tbody>
            </table>
        </div>
        <div>
            <hr>
            <h2 class="text-center">Torax</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
      
                    <th class="text-center" scope="col"> Vista anterior </th>
                    <th class="text-center" scope="col"> Si/No </th>
                    <th class="text-center" scope="col"> Vista lateral </th>
                    <th class="text-center" scope="col"> Si/No </th>
                  
      
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        Torax En Quilla
                        <hr>
                        Pectus Escavatum
                        <hr>
                        Torax En Tonel
      
                    </th>
                    <td class="text-center">
                      <% if(toraxQuillaVa.trim().equals("SI"))
                      { %>
                        <input id="c9" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c9" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                        <hr>
                        <% if(pectusScavatumVa.trim().equals("SI"))
                        { %>
                          <input id="c10" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c10" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
                        <hr>
                        <% if(toraxTonelVa.trim().equals("SI"))
                        { %>
                          <input id="c11" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c11" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
                    </td>
                    <th class="text-center" scope="row">
                      Torax En Tonel
                      <hr>
                      Hipercifosis
                    </th>
                    <td class="text-center">
                      <% if(toraxTonelVl.trim().equals("SI"))
                        { %>
                          <input id="c36" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c36" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
                      <hr>
                      <% if(hipercifosisVl.trim().equals("SI"))
                        { %>
                          <input id="c37" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c37" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
                  </td>
      
      
                  </tr>
      
                
      
      
                </tbody>
            </table>
        </div>
        <div>
            <hr>
            <h2 class="text-center">Abdomen</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
      
                    <th class="text-center" scope="col"> Vista anterior </th>
                    <th class="text-center" scope="col"> Si/No </th>
                    <th class="text-center" scope="col"> Vista lateral </th>
                    <th class="text-center" scope="col"> Si/No </th>
                  
      
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                      Prominente
      
      
                    </th>
                    <td class="text-center">
                      <% if(abdomenProminenteVa.trim().equals("SI"))
                      { %>
                        <input id="c12" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c12" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    </td>
                    <th class="text-center" scope="row">
                      Prominente
      
                    </th>
                    <td class="text-center">
                      <% if(abdomenProminenteVl.trim().equals("SI"))
                      { %>
                        <input id="c38" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c38" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                  </td>
      
      
                  </tr>
      
                
      
      
                </tbody>
            </table>
        </div>
        <div>
            <hr>
            <h2 class="text-center">Columna Lumbar</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
      
      
                    <th class="text-center" scope="col"> Vista lateral </th>
                    <th class="text-center" scope="col"> Si/No </th>
      
      
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        Hiperlordosis 
                        <hr>
                        Columna Recta
                    </th>
                    <td class="text-center">
                      <% if(lumbarHiperlordosisVa.trim().equals("SI"))
                      { %>
                        <input id="c39" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c39" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                        <hr>
                        <% if(lumbarColumnaRecta.trim().equals("SI"))
                        { %>
                          <input id="c40" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c40" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
                    </td>
                  </tr> 
      
                
      
      
                </tbody>
            </table>
        </div>
        <div>
          <hr>
          <h2 class="text-center">Pelvis</h2>
          <table class="table table-bordered mt-1" id="tfoi">
              <thead>
                <tr>
      
                  <th class="text-center" scope="col"> Vista anterior </th>
                  <th class="text-center" scope="col">  Izquierdo Si/No </th>
                  <th class="text-center" scope="col">  Derecho Si/No </th>
                  <th class="text-center" scope="col">  Vista lateral </th>
                  <th class="text-center" scope="col">  Si/No </th>
                  <th class="text-center" scope="col">  Vista posterior </th>
                  <th class="text-center" scope="col">  Si/No </th>
      
                
      
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="text-center" scope="row">
  
                      Rotacion 
                      <hr>
                      Ascendida
                      <hr>
                      Descendida
      
                  </th>
                  <td class="text-center">
                    <% if(pelvisRotacionVa.trim().equals("SI"))
                    { %>
                      <input id="c13" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c13" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                      <hr>
                        <% if(pelvisAscendidaIzquierdaVa.trim().equals("SI"))
                        { %>
                          <input id="c15" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c15" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
                      <hr>
                      <% if(pelvisDescendidaIzquierdaVa.trim().equals("SI"))
                      { %>
                        <input id="c17" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c17" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
      
                  </td>
                  <td class="text-center">
                    <hr>
                    <hr>
                    <% if(pelvisAscendidaDerechaVa.trim().equals("SI"))
                      { %>
                        <input id="c14" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c14" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(pelvisDescendidaDerechaVa.trim().equals("SI"))
                      { %>
                        <input id="c16" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c16" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
      
                  </td>
                  <th class="text-center" scope="row">
                    Retroversion
                    <hr>
                    Anteversion
                  </th>
                  <td class="text-center">
                    <% if(pelvisRetrovesionVl.trim().equals("SI"))
                      { %>
                        <input id="c41" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c41" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(pelvisAnteversionVl.trim().equals("SI"))
                      { %>
                        <input id="c42" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c42" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                 </td>
                 <th class="text-center" scope="row">
                  Rotacion
                </th>
                <td class="text-center">
                  <% if(pelvisRotacionVp.trim().equals("SI"))
                      { %>
                        <input id="c61" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c61" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                </td>
      
                </tr>
      
              
      
      
              </tbody>
          </table>
        </div>
        <div>
            <hr>
            <h2 class="text-center">Rodillas</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
        
                    <th class="text-center" scope="col"> Vista anterior </th>
                    <th class="text-center" scope="col">  Si/No </th>
                    <th class="text-center" scope="col">  Vista lateral </th>
                    <th class="text-center" scope="col">  Izquierdo Si/No </th>
                    <th class="text-center" scope="col">  Derecho Si/No </th>
                    <th class="text-center" scope="col"> Vista posterior </th>
                    <th class="text-center" scope="col">  Si/No </th>
                  
        
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        GENU VARO
                        <hr>
                        GENU VALGUS
        
                    </th>
                    <td class="text-center">
                      <% if(rodillasGenuVaroVa.trim().equals("SI"))
                      { %>
                        <input id="c18" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c18" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                        <hr>
                        <% if(rodillasGenuValgusVa.trim().equals("SI"))
                        { %>
                          <input id="c19" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c19" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
        
                    </td>
        
                    <th class="text-center" scope="row">
                      Hiperextension
                      <hr>
                      Flexion
                    </th>
                    <td class="text-center">
                      <% if(rodillasHiperextensionIzquierdoVl.trim().equals("SI"))
                      { %>
                        <input id="c44" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c44" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                      <hr>
                      <% if(rodillasFlexionIzquierdoVl.trim().equals("SI"))
                      { %>
                        <input id="c46" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c46" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
        
                    </td>
                    <td class="text-center">
                      <% if(rodillasHiperextensionDerechoVl.trim().equals("SI"))
                      { %>
                        <input id="c43" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c43" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                      <hr>
                      <% if(rodillasFlexionDerechoVl.trim().equals("SI"))
                      { %>
                        <input id="c45" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c45" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                   </td>
                   <th class="text-center" scope="row">
                    GENU VARO
                    <hr>
                    GENU VALGUS
        
                  </th>
                   <td class="text-center">
                    <% if(rodillasGenuVaroVp.trim().equals("SI"))
                    { %>
                      <input id="c62" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c62" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
                    <hr>
                    <% if(rodillasGeuValgusVp.trim().equals("SI"))
                    { %>
                      <input id="c63" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}else
                    {%>
                      <input id="c63" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                    <%}%>
        
                  </td>
        
        
                  </tr>
        
                
        
        
                </tbody>
            </table>
        </div>
        <div >
            <hr>
            <h2 class="text-center">Piernas Tibia</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
        
                    <th class="text-center" scope="col"> Vista anterior </th>
                    <th class="text-center" scope="col">  Si/No </th>
                    <th class="text-center" scope="col"> Vista posterior </th>
                    <th class="text-center" scope="col">  Si/No </th>
        
                  
        
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        Torsion femoral
                        <hr>
                        Torsion Tibial
        
                    </th>
                    <td class="text-center">
                      <% if(tibiaTorsionFemAnt.trim().equals("SI"))
                      { %>
                        <input id="c20" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c20" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                        <hr>
                        <% if(tibiaTorisionTibialAnt.trim().equals("SI"))
                        { %>
                          <input id="c21" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}else
                        {%>
                          <input id="c21" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                        <%}%>
        
                    </td>
        
                    <th class="text-center" scope="row">
                      Torsion femoral
                      <hr>
                      Torsion tibial
                    </th>
                    <td class="text-center">
                      <% if(tibiaTorsionFemPos.trim().equals("SI"))
                      { %>
                        <input id="c64" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c64" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                      <hr>
                      <% if(tibiaTorsionTibialPos.trim().equals("SI"))
                      { %>
                        <input id="c65" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c65" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                   </td>
        
        
        
                  </tr>
        
                
        
        
                </tbody>
            </table>
        </div>
        <div >
            <h2 class="card-title">Valoracion Postural </h2>
            <hr>
            <h2 class="text-center">Pies</h2>
            <table class="table table-bordered mt-1" id="tfoi">
                <thead>
                  <tr>
        
                    <th class="text-center" scope="col"> Vista anterior </th>
                    <th class="text-center" scope="col"> Si/No </th>
                    <th class="text-center" scope="col"> Vista lateral </th>
                    <th class="text-center" scope="col"> Izquierdo Si/No </th>
                    <th class="text-center" scope="col"> Derecho Si/No </th>
                    <th class="text-center" scope="col"> Vista Posterior </th>
                    <th class="text-center" scope="col"> Izquierdo Si/No </th>
                    <th class="text-center" scope="col"> Derecho Si/No </th>
        
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="text-center" scope="row">
                        Invertidos
                        <hr>
                        Evertidos
                        <hr>
                        Hallux Valgus
                    </th>
                    <td class="text-center">
                      <% if(piesInvertidosAnt.trim().equals("SI"))
                      { %>
                        <input id="c22" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c22" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                        <hr>
                        <% if(piesEvertidosAnt.trim().equals("SI"))
                      { %>
                        <input id="c23" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c23" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                        <hr>
                        <% if(piesHalluxValAnt.trim().equals("SI"))
                      { %>
                        <input id="c24" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c24" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    </td>
        
                    <th class="text-center" scope="row">
                      Pie plano
                      <hr>
                      Pie Cavo
                    </th>
                    <td class="text-center">
                      <% if(piePlanoIzquierdoPos.trim().equals("SI"))
                      { %>
                        <input id="c48" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c48" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                      <hr>
                      <% if(pieCavoIzquierdoPos.trim().equals("SI"))
                      { %>
                        <input id="c50" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c50" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    </td>
                    <td class="text-center">
                      <% if(piePlanoDerechoPos.trim().equals("SI"))
                      { %>
                        <input id="c47" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c47" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                      <hr>
                      <% if(pieCavoIzquierdoPos.trim().equals("SI"))
                      { %>
                        <input id="c49" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c49" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    </td>
                  <th class="text-center" scope="row">
                    Invertidos
                    <hr>
                    Evertidos
                    <hr>
                    Pie pronado
                    <hr>
                    Pie supinado
                  </th>
                  <td class="text-center">
                    <% if(pieInvertidoIzquierdoPos.trim().equals("SI"))
                      { %>
                        <input id="c67" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c67" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(pieEvertidoIzquierdoPos.trim().equals("SI"))
                      { %>
                        <input id="c69" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c69" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(piePronadoIzquierdoPos.trim().equals("SI"))
                      { %>
                        <input id="c71" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c71" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(pieSupinadoIzquierdoPos.trim().equals("SI"))
                      { %>
                        <input id="c73" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c73" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
        
        
                  </td>
                  <td class="text-center">
                    <% if(pieInvertidoDerechoPos.trim().equals("SI"))
                      { %>
                        <input id="c66" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c66" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(pieEvertidoDerechoPos.trim().equals("SI"))
                      { %>
                        <input id="c68" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c68" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(piePronadoDerechoPos.trim().equals("SI"))
                      { %>
                        <input id="c70" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c70" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                    <hr>
                    <% if(pieSupinadoDerechoPos.trim().equals("SI"))
                      { %>
                        <input id="c72" type="checkbox" checked onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}else
                      {%>
                        <input id="c72" type="checkbox" onclick="valorSwitchsn(this.id,this.checked);guardarDatosPlantilla('vrl6',this.id,this.value)">
                      <%}%>
                  </td>
        
                  </tr>
        
                
        
        
                </tbody>
            </table>
        </div>
    </div>
   
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</html>

