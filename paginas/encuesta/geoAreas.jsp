<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%  beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1350"  align="center" cellpadding="0" cellspacing="0" >
    <tr>
        <td>
            <div align="center" id="tituloForma" >
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="CARTOGRAFIA" />
                </jsp:include>
            </div>  
        </td>
    </tr> 
    <tr>
      <td valign="top">    
        <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0" >  
          <tr class="titulos" >
             <td width="65%">
               <table width="101%" class="fondoTabla" cellpadding="0" cellspacing="0" >
                <caption>Gestion de Areas</caption>
                <tr>
                  <td width="20%">Municipio</td>
                  <td width="20%">Nombre de Area</td>
                  <td width="8%">Orden</td>  
                  <td width="10%">latitud</td>  
                  <td width="10%">longitud</td>  
                  <td width="8%">zoom</td>  
                  <td width="8%">color</td> 
                  <td width="8%">color Opacidad</td> 
                  <td width="6%">Vigente</td> 
                  <td width="2%"></td>

                </tr>
                <tr>
                  <td>                   
                    <input type="text" id="txtMunicipio" size="60"  maxlength="60"  style="width:70%"  /> 
                    <img width="18px" height="18px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                  <div id="divParaVentanita"></div>                    
                 </td> 
                 <td width="20%">
                   <label id="lblIdArea" style="display: none;" >0</label>
                   <input type="text" id="txtArea"  style="width:80%"   size="100" maxlength="100" />
                 </td>
                 <td ><input type="text" id="txtOrden"  style="width:80%"   size="20" maxlength="20" /></td>  
                 <td ><input type="text" id="txtLatitud"  style="width:80%"   size="20" maxlength="20" /></td>  
                 <td ><input type="text" id="txtLongitud"  style="width:80%"   size="20" maxlength="20" /></td>  
                 <td ><input type="text" id="lblIdZoom"  style="width:40%"   size="20" maxlength="20" /></td>  
                 <td ><select size="1" id="txtColor" style="width:80%" >
                  <option value=""></option>
                  <%   resultaux.clear();
                        resultaux=(ArrayList)beanAdmin.combo.cargar(2037);	
                        ComboVO cmbGR; 
                        for(int k=0;k<resultaux.size();k++){ 
                        cmbGR=(ComboVO)resultaux.get(k);
                   %>
                  <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                    <%=cmbGR.getDescripcion()%></option>
                  <%}%>>
                </select></td> 
                 <td ><select size="1" id="txtColorOpacidad" style="width:80%" >
                  <option value=0></option>
                  <option value=0.2>MUY POCA</option>
                  <option value=0.4>POCA</option>
                  <option value=0.6>NORMAL</option>
                  <option value=0.8>ALTA</option>
                  <option value=1>MUY ALTA</option>
                </td> 
                 <td >
                      <select size="1" id="cmbVigente" style="width:90%" >
                        <option value="S">SI</option>
                        <option value="N">NO</option>
                      </td>  
                </tr>
                <tr>
                  <td colspan="9">
                    <label id="lblIdlatitud" style="display: none;" >0</label>
                    <label id="lblIdlongitud" style="display: none;" >0</label>
                    <input title="GE86" type="button" class="small button blue" value="CREAR AREA" onclick="modificarMapCRUD('crearAreaMapa')"/>  
                    <input title="GE87" type="button" class="small button blue" value="ELIMINAR AREA" onclick="modificarMapCRUD('eliminarArea')"/>
                    <input title="GE87" type="button" class="small button blue" value="MODIFICAR AREA" onclick="modificarMapCRUD('modificarAreaMapa')"/>
                    <input title="GE88" type="button" class="small button blue" value="BUSCAR AREAS" onclick="buscarEncuesta('listMunicipioAreas'); setearMunicipio(valorAtributoIdAutoCompletar('txtMunicipio'))"/>
                    <input title="GE88" type="button" class="small button blue" value="TRAER PUNTO" onclick="traerPuntoMapa()"/>
                    <!--  <input title="GE88" type="button" class="small button blue" value="UBICAR MAPA" onclick="ubicarMapaMunicipio()"  /> -->
                 </td> 
                </tr>
               </table>
             </td>
             <td width="35%" valign="top">
              <table width="100%" height='74px' class="fondoTabla" cellpadding="0" cellspacing="0">
                <caption>Gestion de Vertices</caption>
                <tr>
                  <td width="20%">Orden</td>  
                  <td width="40%">latitud</td>  
                  <td width="40%">longitud</td>
                </tr>
                <tr>
                  <td>
                    <input type="text" id="txtOrdenReferencia"  style="width:20%"   maxlength="20" />
                  </td>
                  <td><input type="text" id="txtLatitudReferencia"  style="width:80%"   maxlength="20" /></td>
                  <td><input type="text" id="txtLongitudReferencia"  style="width:80%"   maxlength="20" /></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <input title="GE96" type="button" class="small button blue" value="Adicionar" onclick="modificarMapCRUD('adicionarReferencia')"/>
                    <input title="GE97" type="button" class="small button blue" value="Modificar" onclick="modificarMapCRUD('modificarReferencia')"/>
                    <input title="GE98" type="button" class="small button blue" value="eliminar" onclick="modificarMapCRUD('eliminarReferencia')"/>
                   </td> 
                </tr>
              </table>
             </td>
          </tr>        
          <tr class="titulos">
            <td colspan="1" valign="top" height='250px'>
                <TABLE width="102%"  height='235px' class="fondoTabla" cellpadding="0" cellspacing="0" >  
                  <TR>
                    <TD  width="100%" height='232px' valign="top">
                      <table id="listMunicipioAreas"  width="100%"  class="scroll" cellpadding="0" cellspacing="0" align="center" ></table>              
                    </TD>
                  </TR>
                </TABLE>    
            </td>
            <td colspan="1" valign="top" height='250px'>   
                <table width="100%" height='236px' class="fondoTabla" cellpadding="0" cellspacing="0" >  
                  <tr class="titulos">
                     <td colspan="3"  valign="top"  valign="top" height='169px'>  
                        <table id="listAreasRerefencias"  width="100%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                     </td> 
                  </tr>                                    
                </table>                  
            </td>            
          </tr>
          <tr class="titulos">
            <td height="1000px" colspan="2" valign="top" >
                <table id="idTableContenedorMapa" style="height: 90%; width: 100%" class="scroll" cellpadding="0" cellspacing="0" align="center">
                   <tr>
                    <td  valign="top"  style="height: 100%; width: 100%" id="idTableContenedorMapaTd">
                     <div id="mapaGeoArea" style="height: 90%; width: 100%"></div>
                   </td>
                 </tr> 
                </table>              
            </td>                              
          </tr>  
        </table>   
      </td>    
    </tr>        
</table>