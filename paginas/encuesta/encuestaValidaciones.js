
//ACTIVAR SWITCH

$("#abc").mousedown(function (e) {
    alert("#")
    //1: izquierda, 2: medio/ruleta, 3: derecho
    if (e.which == 3) {
        alert("$")
    }
});

function activarFormulaSwitch(tabla, referencia) {
    var valor = document.getElementById("formulario." + tabla + "." + referencia).value
    var camp = "formulario." + tabla + "." + referencia;
    actualizarCampoFormulaEncuesta(camp, valor)

}

function activarSwitchsn(tabla, referencia) {

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner2");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch2");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitchsn("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200);
}

function activarSwitchsnE(tabla, referencia) {

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner2");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch2");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitchsn("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosEncuesta(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200);
}

function activarSwitchS10(tabla, referencia) {

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner2");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch2");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitch10("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200);
}

function activarSwitch10(tabla, referencia) {

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner3");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch3");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitch10("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200);
}

function activarSwitch10E(tabla, referencia) {

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner3");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch3");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitch10("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosEncuesta(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200);
}

function activarSwitchobs(tabla, referencia) {

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner4");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch4");

    setTimeout(() => {
        document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
        valorSwitchObservacion("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
        setTimeout(() => {
            guardarDatosPlantilla(tabla, referencia, document.getElementById("formulario." + tabla + "." + referencia).value)
        }, 200);
    }, 200);
}


//  clases para el funcionamiento de la notacion formulas

class Stack {
    constructor() {
        this.items = {}
        this.top = 0
    }

    push(data) {
        this.top++
        this.items[this.top] = data
    }

    pop() {
        let outData
        if (this.top) {
            outData = this.items[this.top]
            delete this.items[this.top]
            this.top--
            return outData
        }
    }

    getSize() {
        return this.top
    }

    isEmpty() {
        return this.top == 0
    }

    peek() {
        return (this.getSize() == 1) ? null : this.items[this.top - 1]
    }

    getTop() {
        return (this.isEmpty()) ? null : this.items[this.top]
    }

    getPos(pos) {
        return (this.isEmpty()) ? null : this.items[pos]
    }

    print() {
        return Object.assign([], this.items).reverse()
    }
}

const isOperator = (operator) => ['(', ')', ')', '*', '/', '^', '+', '-', '>', '<', '>=', '<=', '!', '&&', '||', '==', '!=', 'IF', 'THEN', 'ELSE', 'ENDIF'].includes(operator)

const hightPrecedence = (operator) => (getPrecedence(stack.getTop()) < getPrecedence(operator))

const getPrecedence = (operator) => {
    if ('!^'.includes(operator)) return 8
    if ('*/'.includes(operator)) return 7
    if ('+-'.includes(operator)) return 6
    if ('<><=>='.includes(operator)) return 5
    if ('==!='.includes(operator)) return 4
    if (operator == '&&') return 3
    if (operator == '||') return 2
    if ('IFTHENELSEENDIF('.includes(operator)) return 1
}

const conversor = (parameter) => {
    output = ''
    stack = new Stack()

    parameter.split(' ').map(e => {
        if (isOperator(e)) {
            if (['(', 'IF'].includes(e)) {
                stack.push(e)
            } else if ([')', 'THEN', 'ELSE', 'ENDIF'].includes(e)) {
                if (e == ')') {
                    while (stack.getTop() != '(' && !stack.isEmpty()) {
                        output += `${stack.pop()} `;
                    }
                    stack.pop();
                }
                else if (e == 'ENDIF') {
                    while (stack.getTop() != 'IF' && !stack.isEmpty()) {
                        output += `${stack.pop()} `;
                    }
                    output += `${stack.pop()} `;
                }
            } else {
                if (hightPrecedence(e)) {
                    stack.push(e);
                } else {
                    if (!stack.isEmpty())
                        output += `${stack.pop()} `;
                    stack.push(e);
                }
            }
        } else {
            output += `${e} `;
        }
    })

    if (!stack.isEmpty()) {
        stack.print().map(st => {
            if (st != '(')
                output += `${stack.pop()} `;
            else
                stack.pop();
        });
    }
    return output
}

const evaluator = (expresion_rpn) => {
    stack = new Stack()
    entry = expresion_rpn.split(' ')
    entry.pop()
    entry.map(e => {
        if (!isOperator(e)) {
            if (isNaN(e))
                e = "'" + e + "'";
            stack.push(e)
        } else {
            if (e == 'IF') {
                var ifFalse = stack.pop();
                var ifTrue = stack.pop();
                if (stack.getTop() == null) {
                    if (ifTrue)
                        stack.push(ifFalse);
                } else {
                    if (stack.pop())
                        stack.push(ifTrue);
                    else
                        stack.push(ifFalse);
                }
            } else if (e == '!') {
                aux = !stack.pop();
                stack.push(aux);
            } else {
                aux = eval(`${stack.peek()} ${e == '^' ? '**' : e} ${stack.getTop()}`);
                stack.pop();
                stack.pop();
                stack.push(aux);
            }
        }
    })
    return stack.getTop()
}

const setOperation = (event) => {


    if (event.key == 'Enter') {
        rpn = conversor(event.target.value)

        //eso es para evaluar
        //res = evaluator(rpn)

        document.getElementById('main_entry').innerText = event.target.value
        document.getElementById('conversion').innerText = rpn


        //para evaluar 
        //document.getElementById('operation').innerText = res
        event.target.value = ''
    }
}


// fin de las clases para la notacion 


function calFormula(formula) {
    if (document.getElementById('campoResultado' + formula) == null)
        return;
    camResultado = document.getElementById('campoResultado' + formula).value;
    var tabla = document.getElementById('campoResultado' + formula).getAttribute('name');
    output = ''
    stack = new Stack()
    var formula = document.getElementById('campoFormula' + formula).value;
    var campoVacio = false;
    formula.split(' ').map(e => {

        if (e != ' ') {
            if (!isOperator(e)) {
                if ($('#listGrillaFormulas td[title=' + e + ']').closest('tr').find('td:nth-child(2)').html() == e) {
                    var va = 0;
                    va = $('td[title=' + e + ']').closest('tr').find('td:nth-child(3)').html();
                    if (va == '' || va == '&nbsp;') {
                        campoVacio = true;
                        va = 0;
                        output += `${va} `
                    } else {
                        output += `${va} `
                    }
                } else {
                    output += `${e} `
                }
            }
            else {
                output += `${e} `
            }
        }
    })
    if (campoVacio)
        return;
    console.log(output)
    fds = evaluator(output)
    var campo = 'formulario.' + tabla + '.' + camResultado;
    var campoTest = $(document.getElementById(campo));
    var res = fds;
    if (!isNaN(fds)) {
        if (fds % 1 == 0) {
            res = fds;
        } else {
            var fds2 = parseFloat(Math.round(fds * 100) / 100).toFixed(2);
            res = fds2;
        }
    }
    else {
        res = fds.replace(/'/g, '');
    }
    if (document.getElementById(campo).type == "text" || document.getElementById(campo).type == "number" || document.getElementById(campo).type == "date") {
        document.getElementById(campo).value = res;
        campoTest.trigger("keyup");
        campoTest.trigger("blur");
    }
    else if (document.getElementById(campo).type == "checkbox") {
        if (("SI1".includes(res) && !document.getElementById(campo).checked) || ("NO0".includes(res) && document.getElementById(campo).checked)) {
            document.getElementById(campo).click();
        }
    }
    else {
        document.getElementById(campo).value = res;
        campoTest.trigger("change");
    }
    //guardarDatosPlantilla(tabla,camResultado,document.getElementById('formulario.'+tabla+'.'+camResultado).value);
    //actualizarCampoFormula('formulario.'+tabla+'.'+camResultado, document.getElementById('formulario.'+tabla+'.'+camResultado).value);     
}

function calFormulaEncuesta(formula) {
    if (document.getElementById('campoResultado' + formula) == null)
        return;
    camResultado = document.getElementById('campoResultado' + formula).value;
    var tabla = document.getElementById('campoResultado' + formula).getAttribute('name');
    output = ''
    stack = new Stack()
    var formula = document.getElementById('campoFormula' + formula).value;
    var campoVacio = false;
    formula.split(' ').map(e => {
        if (e != ' ') {
            if (!isOperator(e)) {
                if ($('#listGrillaFormulasE td[title=' + e + ']').closest('tr').find('td:nth-child(2)').html() == e) {
                    var va = 0;
                    va = $('td[title=' + e + ']').closest('tr').find('td:nth-child(3)').html();
                    if (va == '' || va == '&nbsp;') {
                        campoVacio = true;
                        va = 0;
                        output += `${va} `
                    } else {
                        output += `${va} `
                    }
                } else {
                    output += `${e} `
                }
            }
            else {
                output += `${e} `
            }
        }
    })
    if (campoVacio)
        return;
    fds = evaluator(output)
    var campo = 'formulario.' + tabla + '.' + camResultado;
    var campoTest = $(document.getElementById(campo));
    var res = fds;
    if (!isNaN(fds)) {
        if (fds % 1 == 0) {
            res = fds;
        } else {
            var fds2 = parseFloat(Math.round(fds * 100) / 100).toFixed(2);
            res = fds2;
        }
    }
    else {
        res = fds.replace(/'/g, '');
    }

    if (document.getElementById(campo).type == "text" || document.getElementById(campo).type == "number" || document.getElementById(campo).type == "date") {
        document.getElementById(campo).value = res;
        campoTest.trigger("keyup");
        campoTest.trigger("blur");
    }
    else if (document.getElementById(campo).type == "checkbox") {
        if (("SI1".includes(res) && !document.getElementById(campo).checked) || ("NO0".includes(res) && document.getElementById(campo).checked)) {
            document.getElementById(campo).click();
        }
    }
    else {
        document.getElementById(campo).value = res;
        campoTest.trigger("change");
    }

    //guardarDatosPlantilla(tabla,camResultado,document.getElementById('formulario.'+tabla+'.'+camResultado).value);
    //actualizarCampoFormula('formulario.'+tabla+'.'+camResultado, document.getElementById('formulario.'+tabla+'.'+camResultado).value);     
}

//Validacion para campos formula
function actualizarCampoFormula(campo, val) {
    if ($('#listGrillaFormulas td[title=' + campo + ']').html() == null)
        return;
    $('#listGrillaFormulas td[title=' + campo + ']').closest('tr').find('td:nth-child(3)').html(val);
    $('#listGrillaFormulas td[title=' + campo + ']').closest('tr').find('td:nth-child(4)').html().split(',').map(e => {
        if (e != ' ') {
            calFormula(e);
        }
    });
}

function actualizarCampoFormulaEncuesta(campo, val) {
    if ($('#listGrillaFormulasE td[title=' + campo + ']').html() == null)
        return;
    $('#listGrillaFormulasE td[title=' + campo + ']').closest('tr').find('td:nth-child(3)').html(val);
    $('#listGrillaFormulasE td[title=' + campo + ']').closest('tr').find('td:nth-child(4)').html().split(',').map(e => {
        if (e != ' ') {
            calFormulaEncuesta(e);
        }
    });
}

//realiza los cálculos en los campos de formula al abrir el folio
function calculosIniciales(tipo, modo) {
    $('#div_' + tipo + ' .campoFormula').each(function () {
        var formula = $(this).attr("id").substring(12, $(this).attr("id").length);
        if (formula != null && modo == 'desdeEncuesta')
            calFormulaEncuesta(formula);
        else
            calFormula(formula);
    });

    /*$('#listGrillaFormulas > tbody > tr').each(function(){
        var campo = $(this).find('td:nth-child(2)').html();
        if(campo != null)
            if(campo.includes(tipo)){
                var valor = $(this).find('td:nth-child(3)').html();
                actualizarCampoFormula(campo,valor);
            }
    });*/
}

//realiza los cálculos en los campos de formula al abrir el folio
function validacionesIniciales(tipo, modo,version) {
    var validaciones = "";
    var validacionActual = "";
    if (modo == 'desdeEncuesta') {
        $('#listGrillaValidacionesE > tbody > tr').each(function () {
            if ($(this).find('td:nth-child(3)').html().includes(tipo)) {
                if (validacionActual != $(this).find('td:nth-child(2)').html()) {
                    validacionActual = $(this).find('td:nth-child(2)').html();
                    validaciones = validaciones + validacionActual + ",";
                }
            }
        });
    }
    else {
        $('#listGrillaValidaciones > tbody > tr').each(function () {
            if ($(this).find('td:nth-child(3)').html().includes(tipo)) {
                if (validacionActual != $(this).find('td:nth-child(2)').html()) {
                    validacionActual = $(this).find('td:nth-child(2)').html();
                    validaciones = validaciones + validacionActual + ",";
                }
            }
        });
    }
    if (validaciones != "" && modo == 'desdeEncuesta')
        ejecutarValidacionesEncuesta(validaciones);
    else
        ejecutarValidaciones(validaciones,version);
}

function validarCampo(referencia, validaciones,version) {
    if ($('#listGrillaValoresValidaciones td[title=' + referencia + ']').html() == null)
        return;
    var val = document.getElementById(referencia).value;
    $('#listGrillaValoresValidaciones td[title=' + referencia + ']').closest('tr').find('td:nth-child(3)').html(val);
    ejecutarValidaciones(validaciones,version);
}

function validarCampoEncuesta(referencia, validaciones) {
    console.log(referencia)
    console.log(validaciones)
    if ($('#listGrillaValoresValidacionesE td[title=' + referencia + ']').html() == null)
        return;
    var val = document.getElementById(referencia).value;
    console.log("AQUII")
    $('#listGrillaValoresValidacionesE td[title=' + referencia + ']').closest('tr').find('td:nth-child(3)').html(val);
    ejecutarValidacionesEncuesta(validaciones);
}

//Ejecuta las validaciones correspondientes a dicho campo
function ejecutarValidaciones(validaciones,version) {
    console.log('ejecutar valida')
    let bandera = 0;
    validaciones.split(',').map(e => {
        if (e != "") {
            var output = "";
            var validacion = "";
            //$('#listGrillaValidaciones > tbody > tr').each(function(){
            $('.' + e).each(function () {
                var campo = $(this).find('td:nth-child(3)').html();
                if ($(this).find('td:nth-child(7)').html() == '1') {
                    validacion = $(this).find('td:nth-child(2)').html();
                    var operando = $(this).find('td:nth-child(4)').html();
                    if (operando == '&gt;')
                        operando = '>';
                    if (operando == '&lt;')
                        operando = '<';
                    var valor = $(this).find('td:nth-child(5)').html();
                    var campovalor = $('#listGrillaValoresValidaciones td[title=' + campo + ']').closest('tr').find('td:nth-child(3)').html();
                    if (campovalor == ''){
                        campovalor = '&nbsp;'
                    }
                    if (output != "")
                        output += ' && '
                    output += campovalor + ' ' + operando + ' ' + valor;
                }
            });
            output = conversor(output);
            var condicion = evaluator(output);
            $('.' + e).each(function () {
                var codval = $(this).find('td:nth-child(2)').html();
                if (codval != null) {
                    if (codval.includes(validacion)) {
                        if ($(this).find('td:nth-child(7)').html() == '2') {
                            var campo = $(this).find('td:nth-child(3)').html();
                            if (document.getElementById(campo) == null)
                                return;
                            var operando = $(this).find('td:nth-child(4)').html();
                            var valor = $(this).find('td:nth-child(5)').html();

                            if (operando == 'V') {
                                if (valor == 'DISABLED' && condicion) {
                                    document.getElementById(campo).setAttribute('disabled', 'true');
                                    try {
                                        document.getElementById(campo).value = ''
                                        tabla = campo.split('.')[1]
                                        columna = campo.split('.')[2]
                                        guardarDatosPlantilla( tabla,columna, '')    

                                    } catch (error) {
                                        console.log(error,'encuestaValidaciones.js');
                                    }

                                }
                                else {
                                    document.getElementById(campo).removeAttribute('disabled');
                                }
                                if (valor == 'HIDDEN' && condicion) {
                                    if(version == 'v1'){
                                        if (document.getElementById(campo).type == "text"  || document.getElementById(campo).type == "textarea" || document.getElementById(campo).type == "checkbox" || document.getElementById(campo).type == 'select-one' ||  document.getElementById(campo).type == 'number' || document.getElementById(campo).type == "date" ) {
                                            document.getElementById(campo).parentNode.parentNode.style.display = 'none';
                                        }
                                        if( document.getElementById(campo).type == "checkbox"){
                                            document.getElementById(campo).parentNode.parentNode.parentNode.style.display = 'none';
                                        }    
                                    }else{
                                        document.getElementById(campo).parentNode.style.display = 'none';
                                        document.getElementById(`label.${campo}`).parentNode.style.display = 'none';
                                    }                                    
                                }
                                else {
                                    if(version == 'v1'){
                                        if (document.getElementById(campo).type == "text" || document.getElementById(campo).type == "textarea" || document.getElementById(campo).type == "checkbox" || document.getElementById(campo).type == 'select-one' ||  document.getElementById(campo).type == 'number'|| document.getElementById(campo).type == "date") {
                                            document.getElementById(campo).parentNode.parentNode.style.display = '';
                                        }
                                        if( document.getElementById(campo).type == "checkbox"){
                                            document.getElementById(campo).parentNode.parentNode.parentNode.style.display = '';
                                        }    
                                    }else{
                                        document.getElementById(campo).parentNode.style.display = '';
                                        document.getElementById(`label.${campo}`).parentNode.style.display = '';
                                    }                                    
                                }
                            }
                            else if (operando == '=') {
                                if (condicion) {
                                    if (document.getElementById(campo).type == "text" || document.getElementById(campo).type == "textarea") {
                                        document.getElementById(campo).value = valor;
                                        $(document.getElementById(campo)).trigger("change");
                                        $(document.getElementById(campo)).trigger("blur");
                                    }
                                    else if (document.getElementById(campo).type == "checkbox") {
                                        if (("SI1".includes(valor) && !document.getElementById(campo).checked) || ("NO0".includes(valor) && document.getElementById(campo).checked)) {
                                            document.getElementById(campo).click();
                                        }
                                    }
                                    else {
                                        document.getElementById(campo).value = valor;
                                        $(document.getElementById(campo)).trigger("change");
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    });
}

function ejecutarValidacionesEncuesta(validaciones) {
    console.log('valido');
    validaciones.split(',').map(e => {
        if (e != "") {
            var output = "";
            var validacion = "";
            //$('#listGrillaValidaciones > tbody > tr').each(function(){
            $('.' + e).each(function () {
                var campo = $(this).find('td:nth-child(3)').html();
                if ($(this).find('td:nth-child(7)').html() == '1') {
                    validacion = $(this).find('td:nth-child(2)').html();
                    var operando = $(this).find('td:nth-child(4)').html();
                    if (operando == '&gt;')
                        operando = '>';
                    if (operando == '&lt;')
                        operando = '<';
                    var valor = $(this).find('td:nth-child(5)').html();
                    var campovalor = $('#listGrillaValoresValidacionesE td[title=' + campo + ']').closest('tr').find('td:nth-child(3)').html();
                    if (output != "")
                        output += ' && '
                    output += campovalor + ' ' + operando + ' ' + valor;
                }
            });
            output = conversor(output);
            var condicion = evaluator(output);
            $('.' + e).each(function () {
                var codval = $(this).find('td:nth-child(2)').html();
                if (codval != null) {
                    if (codval.includes(validacion)) {
                        if ($(this).find('td:nth-child(7)').html() == '2') {
                            var campo = $(this).find('td:nth-child(3)').html();
                            if (document.getElementById(campo) == null)
                                return;
                            var operando = $(this).find('td:nth-child(4)').html();
                            var valor = $(this).find('td:nth-child(5)').html();
                            if (operando == 'V') {
                                if (valor == 'DISABLED' && condicion){
                                    document.getElementById(campo).setAttribute('disabled', 'true');
                                }else{
                                    document.getElementById(campo).removeAttribute('disabled');}

                                    if (valor == 'HIDDEN' && condicion) {
                                        console.log('tyope', document.getElementById(campo).type)
                                         if (document.getElementById(campo).type == "text"  || document.getElementById(campo).type == "textarea" || document.getElementById(campo).type == "checkbox" || document.getElementById(campo).type == 'select-one' ||  document.getElementById(campo).type == 'number' || document.getElementById(campo).type == "date" ) {
                                             document.getElementById(campo).parentNode.parentNode.style.display = 'none';
                                         }
                                         if( document.getElementById(campo).type == "checkbox"){
                                             document.getElementById(campo).parentNode.parentNode.parentNode.style.display = 'none';
                                         }
                                     }else {
                                        if (document.getElementById(campo).type == "text" || document.getElementById(campo).type == "textarea" || document.getElementById(campo).type == "checkbox" || document.getElementById(campo).type == 'select-one' ||  document.getElementById(campo).type == 'number'|| document.getElementById(campo).type == "date") {
                                            document.getElementById(campo).parentNode.parentNode.style.display = '';
                                        }
                                        if( document.getElementById(campo).type == "checkbox"){
                                            document.getElementById(campo).parentNode.parentNode.parentNode.style.display = '';
                                        }
                                    }
                            }
                            else if (operando == '=') {
                                if (condicion) {
                                    if (document.getElementById(campo).type == "text") {
                                        document.getElementById(campo).value = valor;
                                        $(document.getElementById(campo)).trigger("change");
                                        $(document.getElementById(campo)).trigger("blur");
                                    }
                                    else if (document.getElementById(campo).type == "checkbox") {
                                        if (("SI1".includes(valor) && !document.getElementById(campo).checked) || ("NO0".includes(valor) && document.getElementById(campo).checked)) {
                                            document.getElementById(campo).click();
                                        }
                                    }
                                    else {
                                        document.getElementById(campo).value = valor;
                                        $(document.getElementById(campo)).trigger("change");
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    });
}

// valida el tipo de campo por su jerarquia para bloquear los campos que no seran necesarios
function valTipoCampo() {
    var jerarquia = document.getElementById('cmbJerarquia').value;

    if (jerarquia == 'TI' || jerarquia == 'PA') {
        $("#cmbTipoEntrada").attr("disabled", 'true');
        $("#txtReferencia").attr("disabled", 'true');
        $("#cmbConsultaCombo").attr("disabled", 'true');
        $("#txtAncho").attr("disabled", 'true');
        $("#txtMaxlenght").attr("disabled", 'true');
        $("#txtValorDefecto").attr("disabled", 'true');

        document.getElementById('cmbTipoEntrada').value = " ";
        document.getElementById('txtReferencia').value = " ";
        document.getElementById('cmbConsultaCombo').value = "0";
        document.getElementById('txtAncho').value = " ";
        document.getElementById('txtMaxlenght').value = " ";
        document.getElementById('txtValorDefecto').value = " ";
    } else if (jerarquia == 'HI') {
        $("#cmbTipoEntrada").removeAttr("disabled");
        $("#txtReferencia").removeAttr("disabled");
        $("#cmbConsultaCombo").removeAttr("disabled");
        $("#txtAncho").removeAttr("disabled");
        $("#txtMaxlenght").removeAttr("disabled");
        $("#txtValorDefecto").removeAttr("disabled");
    }

}

//bloquea el combo consulta si los parametros de entrada son textarea o input
function validacionTipoEntrada() {

    var entrada = document.getElementById('cmbTipoEntrada').value;

    if (entrada == 'input' || entrada == 'texta') {

        $("#cmbConsultaCombo").attr("disabled", 'true');
        document.getElementById('cmbConsultaCombo').value = "0";

    } else if (entrada == 'combo') {
        $("#cmbConsultaCombo").removeAttr("disabled");
    }
}


//coloca el value y funcion del boton dependiendo si el item de la plantilla tiene o no formula
function valBotonFormula() {

    var numFormula = document.getElementById('txtidFormula').value;
    if (numFormula < 1 || numFormula == ' ') {

        document.getElementById('btnFormula').removeAttribute('hidden');
        document.getElementById('btnFormula').value = "CREAR FORMULA";
        document.getElementById('btnFormula').setAttribute('onclick', 'crearFormula()');
        document.getElementById('btnFormula').setAttribute('class', 'small button blue');
    } else if (numFormula > 0) {

        document.getElementById('btnFormula').removeAttribute('hidden');
        document.getElementById('btnFormula').value = "VER FORMULA";
        document.getElementById('btnFormula').setAttribute('onclick', 'modificarFormula()');
        document.getElementById('btnFormula').setAttribute('class', 'small button blue');

    }

}

//carga los analitos según el laboratorio
function cargarLaboratorioDetalle(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 1252, valorAtributo(comboOrigen));
}

//Carga las tablas según el esquema
function cargarTablasEsquema(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 900, valorAtributo(comboOrigen));
}

//Carga los campos según la tabla
function cargarCamposDesdeEsquemaTabla(comboOrigen1, comboOrigen2, comboDestino) {
    cargarComboGRALCondicion2('cmbPadre', '1', comboDestino, 898, valorAtributo(comboOrigen1), valorAtributo(comboOrigen2));
    if (comboDestino != 'cmbIdentificador') {
        $('#chkIdentificador').attr('checked', true);
        $('#chkIdentificador').trigger('change');
        $('#cmbIdentificador').empty().append('<option>Seleccione Identificador</option>');
        $('#txtIdentificador').val('');
    }
}

//Carga los campos según la tabla actual
function cargarCampos(esquema, tabla, comboDestino) {
    cargarComboGRALCondicion2('cmbPadre', '1', comboDestino, 898, esquema, valorAtributo(tabla));
}

//Cambia el identificador para la tabla
function cambiarIdentificador() {
    if ($('#chkIdentificador').attr('checked')) {
        document.getElementById('cmbIdentificador').setAttribute('hidden', 'true');
        document.getElementById('txtIdentificador').setAttribute('hidden', 'true');
    }
    else {
        document.getElementById('cmbIdentificador').removeAttribute('hidden');
        document.getElementById('txtIdentificador').removeAttribute('hidden');
    }
}

//Cargar campos validaciones
function cargarCampoValidacion(esquema, tabla, campo, resultado) {
    var esquema = $("#" + esquema).val();
    var tabla = $("#" + tabla).val();
    var campo = $("#" + campo).val();
    $("#" + resultado).val(esquema + "." + tabla + "." + campo);
}

//hacen visibles los campos para la formula
function crearFormula() {

    limpiarFormula(0)

    document.getElementById('agregarFormula1').removeAttribute('hidden');
    document.getElementById('agregarFormula2').removeAttribute('hidden');
    document.getElementById('agregarFormula3').removeAttribute('hidden');
    document.getElementById('agregarValorFormula').removeAttribute('hidden');
    document.getElementById('separadorFormula').removeAttribute('hidden');
    document.getElementById('campoResultadoFormula').removeAttribute('hidden');
    document.getElementById('construirFormula').removeAttribute('hidden');
    document.getElementById('construirFormula2').removeAttribute('hidden');

    construirFormula

    document.getElementById('btnGuardarFormula').removeAttribute('hidden');
    document.getElementById('btnGuardarFormula').value = "GUARDAR FORMULA";
    document.getElementById('btnGuardarFormula').setAttribute('onclick', 'modificarCRUD("crearFormula", "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp")');
    document.getElementById('btnGuardarFormula').setAttribute('class', 'small button blue');


    var campoResultado = document.getElementById('txtReferencia').value;
    document.getElementById('txtCampoResultado').value = campoResultado

}


function modificarFormula() {

    limpiarFormula(0)

    document.getElementById('agregarFormula1').removeAttribute('hidden');
    document.getElementById('agregarFormula2').removeAttribute('hidden');
    document.getElementById('agregarFormula3').removeAttribute('hidden');
    document.getElementById('agregarValorFormula').removeAttribute('hidden');
    document.getElementById('separadorFormula').removeAttribute('hidden');
    document.getElementById('campoResultadoFormula').removeAttribute('hidden');
    document.getElementById('construirFormula').removeAttribute('hidden');
    document.getElementById('construirFormula2').removeAttribute('hidden');

    document.getElementById('btnGuardarFormula').removeAttribute('hidden');
    document.getElementById('btnGuardarFormula').value = "MODIFICAR FORMULA";
    document.getElementById('btnGuardarFormula').setAttribute('onclick', 'modificarCRUD("modificarFormula", "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp")');
    document.getElementById('btnGuardarFormula').setAttribute('class', 'small button blue');

    var campoResultado = document.getElementById('txtOcultoCampoResultado').value
    document.getElementById('txtCampoResultado').value = campoResultado

    var formula = document.getElementById('txtOcultoFormulaEditar').value;
    document.getElementById('txtFormulaCompleta').value = formula;

    var campos = document.getElementById('txtOcultoCamposFormula').value;
    document.getElementById('txtCamposFormula').value = campos;

    formula.split(' ').map(e => {
        if (e != ' ') {

            var valor = `${e}`
            var agregaFormula = document.getElementById('construFormula');
            var newOption = document.createElement('option');
            newOption.value = valor;
            newOption.text = valor;
            if (campos.includes(e))
                newOption.className = "optionFormula optOperando";
            else
                newOption.className = "optionFormula";
            agregaFormula.add(newOption, null);

        }
    })

}


function limpiarFormula(estado) {


    if (estado == 1) {
        document.getElementById('agregarFormula1').setAttribute('hidden', 'true');
        document.getElementById('agregarFormula2').setAttribute('hidden', 'true');
        document.getElementById('agregarFormula3').setAttribute('hidden', 'true');
        document.getElementById('agregarValorFormula').setAttribute('hidden', 'true');
        document.getElementById('separadorFormula').setAttribute('hidden', 'true');
        document.getElementById('campoResultadoFormula').setAttribute('hidden', 'true');
        document.getElementById('construirFormula').setAttribute('hidden', 'true');
        document.getElementById('construirFormula2').setAttribute('hidden', 'true');

        document.getElementById('txtCampoResultado').value = "";
        document.getElementById('valorAdicional').value = "";
        document.getElementById('txtFormulaCompleta').value = "";
        document.getElementById('txtCamposFormula').value = "";
        var selec = document.getElementById('construFormula')
        var i;
        for (i = selec.options.length - 1; i >= 0; i--) {
            selec.remove(i);
        }

        document.getElementById('btnGuardarFormula').removeAttribute('class', 'small button blue');
        document.getElementById('btnGuardarFormula').setAttribute('hidden', 'true');

        buscarHistoria('listGrillaPlantillaElemento')
        valBotonFormula();


    } else {

        document.getElementById('txtCampoResultado').value = "";
        document.getElementById('valorAdicional').value = "";
        document.getElementById('txtFormulaCompleta').value = "";
        var selec = document.getElementById('construFormula')
        var i;
        for (i = selec.options.length - 1; i >= 0; i--) {
            selec.remove(i);
        }

        document.getElementById('btnGuardarFormula').removeAttribute('class', 'small button blue');
        document.getElementById('btnGuardarFormula').setAttribute('hidden', 'true');

        valBotonFormula();

    }


}


//agrega el laboratorio al campo para construir la formula
function pasarLaboratorioFormula() {
    var campo = document.getElementById('construFormula');
    camposelect = campo.selectedIndex;
    numcampos = campo.options.length;

    var operando = document.getElementById('cmbLaboratorioDetalle');
    var agregaFormula = document.getElementById('construFormula');
    var valor = "laboratorio." + document.getElementById('cmbLaboratorio').value + "." + document.getElementById('cmbLaboratorioDetalle').value;
    var newOption = document.createElement('option');
    newOption.value = valor;
    newOption.text = valor;
    newOption.className = "optionFormula optOperando";

    if (numcampos == camposelect + 1) {
        agregaFormula.add(newOption, null);
    } else if (camposelect == 0) {
        agregaFormula.prepend(newOption);
    } else {
        agregaFormula.insertBefore(newOption, agregaFormula.options[camposelect]);
    }
    sacarvalorsele();
}

//remplaza el laboratorio seleccionada en el campo de contruccion de formula 
function cambiarOperando() {
    var operador = document.getElementById('cmbLaboratorioDetalle');
    var valor = "laboratorio." + document.getElementById('cmbLaboratorio').value + "." + document.getElementById('cmbLaboratorioDetalle').value;
    var cambiarcampo = document.getElementById('construFormula');
    cambiarcampo = cambiarcampo.options[cambiarcampo.selectedIndex]
    cambiarcampo.value = valor;
    cambiarcampo.text = valor;
    sacarvalorsele();
}


//agrega la variable al campo para construir la formula
function pasarOperando() {
    var campo = document.getElementById('construFormula');
    camposelect = campo.selectedIndex;
    numcampos = campo.options.length;

    var operando = document.getElementById('cmbRefCampo');
    var agregaFormula = document.getElementById('construFormula');
    var valor = document.getElementById('cmbEsquema').value + "." + document.getElementById('cmbTabla').value + "." + document.getElementById('cmbRefCampo').value;
    if (!$('#chkIdentificador').attr('checked'))
        if ($('#txtIdentificador').val() == '')
            valor += '$' + $('#cmbIdentificador').val();
        else
            valor += '$' + $('#cmbIdentificador').val() + '.' + $('#txtIdentificador').val();
    var newOption = document.createElement('option');
    newOption.value = valor;
    newOption.text = valor;
    newOption.className = "optionFormula optOperando";

    if (numcampos == camposelect + 1) {
        agregaFormula.add(newOption, null);
    } else if (camposelect == 0) {
        agregaFormula.prepend(newOption);
    } else {
        agregaFormula.insertBefore(newOption, agregaFormula.options[camposelect]);
    }
    sacarvalorsele();
}

//agrega el simbolo al campo para construir la formula
function pasarOperadorFormula() {
    var campo = document.getElementById('construFormula');
    camposelect = campo.selectedIndex;
    numcampos = campo.options.length;
    var operador = document.getElementById('cmbOperador');
    var agregaFormula = document.getElementById('construFormula');
    var valor = operador.value;
    var newOption = document.createElement('option');
    newOption.value = valor;
    newOption.text = valor;
    newOption.className = "optionFormula";

    if (numcampos == camposelect + 1) {
        agregaFormula.add(newOption, null);
    } else if (camposelect == 0) {
        agregaFormula.prepend(newOption);
    } else {
        agregaFormula.insertBefore(newOption, agregaFormula.options[camposelect]);
    }
    sacarvalorsele();

}


//remplaza la variable seleccionada en el campo de contruccion de formula 
function cambiarOperando() {
    var operador = document.getElementById('cmbRefCampo');
    var valor = document.getElementById('cmbEsquema').value + "." + document.getElementById('cmbTabla').value + "." + document.getElementById('cmbRefCampo').value;
    if (!$('#chkIdentificador').attr('checked'))
        if ($('#txtIdentificador').val() == '')
            valor += '$' + $('#cmbIdentificador').val();
        else
            valor += '$' + $('#cmbIdentificador').val() + '.' + $('#txtIdentificador').val();
    var cambiarcampo = document.getElementById('construFormula');
    cambiarcampo = cambiarcampo.options[cambiarcampo.selectedIndex]
    cambiarcampo.value = valor;
    cambiarcampo.text = valor;
    sacarvalorsele();
}

//remplaza la variable seleccionada en el campo de contruccion de formula 
function cambiarOperador() {

    var operador = document.getElementById('cmbOperador');
    var valor = operador.value;

    var cambiarcampo = document.getElementById('construFormula');
    cambiarcampo = cambiarcampo.options[cambiarcampo.selectedIndex]
    cambiarcampo.value = valor;
    cambiarcampo.text = valor;
    sacarvalorsele();
}

function eliminarCampo() {

    var campo = document.getElementById('construFormula');
    camposelect = campo.selectedIndex;
    numcampos = campo.options.length;
    if (camposelect != -1 && camposelect + 1 < numcampos) {

        var eliminarcampo = document.getElementById('construFormula');
        eliminarcampo = eliminarcampo.options[eliminarcampo.selectedIndex]
        eliminarcampo.remove();

    } else {
        var eliminarcampo = document.getElementById('construFormula');
        eliminarcampo = eliminarcampo.options[numcampos - 1]
        eliminarcampo.remove();
    }
    sacarvalorsele();
}

function adicionarValorFormula() {
    var valorAdicional = document.getElementById('valorAdicional');
    var agregaFormula = document.getElementById('construFormula');
    var valor = valorAdicional.value;
    var newOption = document.createElement('option');
    newOption.value = valor;
    newOption.text = valor;
    newOption.className = "optionFormula";

    agregaFormula.add(newOption, null);
    sacarvalorsele();

}

function cambiarValor() {

    var valorAdicional = document.getElementById('valorAdicional');
    var valor = valorAdicional.value;

    var cambiarcampo = document.getElementById('construFormula');
    cambiarcampo = cambiarcampo.options[cambiarcampo.selectedIndex]
    cambiarcampo.value = valor;
    cambiarcampo.text = valor;
    sacarvalorsele();
}

function sacarvalorsele() {
    var miformula = '';
    var misCampos = '';
    $("#construFormula option").each(function () {
        //alert('opcion '+$(this).text()+' valor '+ $(this).attr('value'))
        var campo = $(this).attr('value');
        miformula += `${campo} `;
        if ($(this).hasClass('optOperando') && !misCampos.includes($(this).attr('value')))
            misCampos += campo + ",";

    });
    $('#txtCamposFormula').val(misCampos);
    return document.getElementById('txtFormulaCompleta').value = miformula;
}




function sacarposicion() {
    var posiciones = '';
    $("#construFormula option").each(function () {
        //alert('opcion '+$(this).text()+' valor '+ $(this).attr('value'))
        var campo = $(this).index();
        posiciones += `${campo} `

    });
    alert('pos= ' + posiciones)

}
//OCULTA TABS EN CONDUCTA Y TRATAMIENTO PARA FOLIOS DE ENFERMERIA..
function validacionTabsFolios() { /*alert(555)*/
    setTimeout(() => {
        //alert("Ingresé a tabs")
        if (valorAtributo('lblTipoDocumento') == 'ENFE') {
            //alert("INGRESO A DIVS"); 
            console.log("Ingreso a ENFE")
            if (document.getElementById('idproc') != null) ocultar('idproc'); ocultar('divProcedimiento');//procedimientos
            if (document.getElementById('idmed') != null) ocultar('idmed'); //medicamentos
            if (document.getElementById('idconmed') != null) ocultar('idconmed'); //conciliación medicamentosa
            if (document.getElementById('idrefcon') != null) ocultar('idrefcon'); //referencia - contrarref
            if (document.getElementById('idcons') != null) ocultar('idcons'); //Consentimientos
            if (document.getElementById('idadmpac') != null) ocultar('idadmpac'); //Gestion administrativa    
            if (document.getElementById('idedpac') != null) mostrar('idedpac'); //educacion paciente
            if (document.getElementById('idimpr') != null) ocultar('idimpr'); //impresión


        }
        else if (valorAtributo('lblTipoDocumento') == 'RGDE') {
            // alert("INGRESO A DIVS"); 
            console.log("Ingreso a RGDE")
            if (document.getElementById('idproc') != null) ocultar('idproc'); ocultar('divProcedimiento'); //procedimientos
            if (document.getElementById('idmed') != null) ocultar('idmed'); //medicamentos
            if (document.getElementById('idconmed') != null) ocultar('idconmed'); //conciliación medicamentosa
            if (document.getElementById('idrefcon') != null) ocultar('idrefcon'); //referencia - contrarref
            if (document.getElementById('idcons') != null) ocultar('idcons'); //Consentimientos
            if (document.getElementById('idadmpac') != null) ocultar('idadmpac'); //Gestion administrativa    
            if (document.getElementById('idedpac') != null) mostrar('idedpac'); //educacion paciente
            if (document.getElementById('idimpr') != null) ocultar('idimpr'); //impresión

        } else if (valorAtributo('lblTipoDocumento') == 'EVNE') {
            //alert("INGRESO A DIVS"); 
            console.log("Ingreso a EVNE")
            if (document.getElementById('idproc') != null) ocultar('idproc'); ocultar('divProcedimiento');//procedimientos
            if (document.getElementById('idmed') != null) ocultar('idmed'); //medicamentos
            if (document.getElementById('idconmed') != null) ocultar('idconmed'); //conciliación medicamentosa
            if (document.getElementById('idrefcon') != null) ocultar('idrefcon'); //referencia - contrarref
            if (document.getElementById('idcons') != null) ocultar('idcons'); //Consentimientos
            if (document.getElementById('idadmpac') != null) ocultar('idadmpac'); //Gestion administrativa    
            if (document.getElementById('idedpac') != null) mostrar('idedpac'); //educacion paciente
            if (document.getElementById('idimpr') != null) ocultar('idimpr'); //impresión


        } else if (valorAtributo('lblTipoDocumento') == 'EVDT') {
            //alert("Ingreso a EVDT")
            if (document.getElementById('idproc') != null) ocultar('idproc'); ocultar('divProcedimiento');//procedimientos
            if (document.getElementById('idmed') != null) ocultar('idmed'); //medicamentos
            if (document.getElementById('idconmed') != null) ocultar('idconmed'); //conciliación medicamentosa
            if (document.getElementById('idrefcon') != null) ocultar('idrefcon'); //referencia - contrarref
            if (document.getElementById('idcons') != null) ocultar('idcons'); //Consentimientos
            if (document.getElementById('idadmpac') != null) mostrar('idadmpac'); //Gestion administrativa    
            if (document.getElementById('idedpac') != null) mostrar('idedpac'); //educacion paciente
            if (document.getElementById('idimpr') != null) ocultar('idimpr'); //impresión

        } else if (valorAtributo('lblTipoDocumento') == 'MEDG') {
            console.log("Ingreso a MEDG")
            if (document.getElementById('idproc') != null) { mostrar('idproc'); /*alert(666); *//*ocultar('divProcedimiento');*/ }//procedimientos
            if (document.getElementById('idmed') != null) mostrar('idmed'); //medicamentos
            if (document.getElementById('idconmed') != null) ocultar('idconmed'); //conciliación medicamentosa
            if (document.getElementById('idrefcon') != null) mostrar('idrefcon'); //referencia - contrarref
            if (document.getElementById('idcons') != null) mostrar('idcons'); //Consentimientos
            if (document.getElementById('idadmpac') != null) mostrar('idadmpac'); //Gestion administrativa    
            if (document.getElementById('idedpac') != null) mostrar('idedpac'); //educacion paciente
            if (document.getElementById('idimpr') != null) mostrar('idimpr'); //impresión 
        } else if (valorAtributo('lblTipoDocumento') == 'HPSP') {
            console.log("Ingreso a HPSP")
            if (document.getElementById('idproc') != null) { ocultar('idproc'); /*alert(666); *//*ocultar('divProcedimiento');*/ }//procedimientos
            if (document.getElementById('idmed') != null) ocultar('idmed'); //medicamentos
            if (document.getElementById('idconmed') != null) ocultar('idconmed'); //conciliación medicamentosa
            if (document.getElementById('idrefcon') != null) ocultar('idrefcon'); //referencia - contrarref
            if (document.getElementById('idcons') != null) ocultar('idcons'); //Consentimientos
            if (document.getElementById('idadmpac') != null) ocultar('idadmpac'); //Gestion administrativa    
            if (document.getElementById('idedpac') != null) mostrar('idedpac'); //educacion paciente
            if (document.getElementById('idimpr') != null) ocultar('idimpr'); //impresión
            if (document.getElementById('idIncap') != null) ocultar('idIncap'); //impresión
        }
        else if (valorAtributo('lblTipoDocumento') == 'TFOI' || valorAtributo('lblTipoDocumento') == 'PARF' || valorAtributo('lblTipoDocumento') == 'HCTO'
            || valorAtributo('lblTipoDocumento') == 'HCTP' || valorAtributo('lblTipoDocumento') == 'HCFA' || valorAtributo('lblTipoDocumento') == 'HCFP'
            || valorAtributo('lblTipoDocumento') == 'NEUA' || valorAtributo('lblTipoDocumento') == 'NEUR' || valorAtributo('lblTipoDocumento') == 'HCVO'
            || valorAtributo('lblTipoDocumento') == 'TODA' || valorAtributo('lblTipoDocumento') == 'TODP' || valorAtributo('lblTipoDocumento') == 'HFAD'
            || valorAtributo('lblTipoDocumento') == 'HFPD' || valorAtributo('lblTipoDocumento') == 'NEAD' || valorAtributo('lblTipoDocumento') == 'NEPD'
            || valorAtributo('lblTipoDocumento') == 'TERR') {
            if (document.getElementById('idproc') != null) { ocultar('idproc'); /*alert(666); *//*ocultar('divProcedimiento');*/ }//procedimientos                
            if (document.getElementById('idmed') != null) ocultar('idmed'); //medicamentos
            if (document.getElementById('idconmed') != null) ocultar('idconmed'); //conciliación medicamentosa
            if (document.getElementById('idrefcon') != null) ocultar('idrefcon'); //referencia - contrarref
            if (document.getElementById('idcons') != null) ocultar('idcons'); //Consentimientos
            if (document.getElementById('idadmpac') != null) ocultar('idadmpac'); //Gestion administrativa    
            if (document.getElementById('idedpac') != null) ocultar('idedpac'); //educacion paciente
            if (document.getElementById('idimpr') != null) ocultar('idimpr'); //impresión
            if (document.getElementById('idIncap') != null) ocultar('idIncap'); //impresión
            if (document.getElementById('idedpac') != null) ocultar('idedpac');
            mostrar('idPlanTratamientoNut')
            ocultar('divProcedimiento')
            mostrar('divPlanTratmientoNut')
        }
    }, 300);
}

function valorSwitch10(id, check) {

    if (check == true) {
        document.getElementById(id).value = "1";
    } else {
        document.getElementById(id).value = "0";
    }
}

/**
    * validaciones para antecedentes
    * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
*/
function updateCheckboxGroup(id, checked) {
    // Obtenemos el contenedor principal de antecedentes
    let contenedor = document.getElementById('tabsContenidoEncuestaPacienteAntecedentes');
    
    // Si el contenedor existe y contiene el elemento con el ID proporcionado
    if (contenedor && contenedor.contains(document.getElementById(id))) {
        let inputsCheck = contenedor.querySelectorAll('input[type="checkbox"]');
        
        // Si el ID termina con '.c1'
        if (/\.c1$/.test(id)) {
            // Obtenemos datos relevantes
            let id_folio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO');
            let tipo_Folio = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO');
            let inputCheck = document.getElementById(id).checked;
            
            // Cargamos validaciones y datos correspondientes
            cargarValidaciones(id_folio, tipo_Folio);

            inputsCheck.forEach((checkbox) => {
                checkbox.checked = inputCheck;
                let name = checkbox.name.replace('formulario.', '');
                let idInput = checkbox.id.split('.')[2];
                document.getElementById(id).value = inputCheck ? 'SI' : 'NO';
                let value = document.getElementById(id).value;
                guardarDatosPlantilla(name, idInput, value);
            });

            let textAreas = contenedor.querySelectorAll('textarea[type="text"]');
            textAreas.forEach((textArea) => {
                let textValue = textArea.value;
                let name = textArea.name;
                let idTextA = textArea.id.split('.')[1];
                if (!inputCheck) {
                    textArea.value = '';
                    textArea.parentNode.parentNode.style.display = 'none';
                    guardarDatosPlantilla(idTextA, name, '');
                } else {
                    textArea.parentNode.parentNode.style.display = '';
                }
            });
        } else {
            // Si el ID no termina con '.c1'
            let input = document.getElementById(id);
            let inputIsChecked = input.checked;

            if (!inputIsChecked) {
                let idinput = input.id.split('.')[2].replace('c', '');
                let idTexA = input.id.split('.')[0] + '.' + input.id.split('.')[1] + '.c' + (parseInt(idinput) + 1);
                document.getElementById(idTexA).value = '';
                guardarDatosPlantilla(input.id.split('.')[1], 'c' + (parseInt(idinput) + 1), '');
            }
        }
    }
}


function valorSwitchsn(ids, checks) {
    if (checks == true) {
        document.getElementById(ids).value = "SI";
    } else {
        document.getElementById(ids).value = "NO";
    }

    updateCheckboxGroup(ids, checks)

}

function campoHabilitaCheck(estado, campo){
    if(estado){
        try {
            document.getElementById(campo).disabled = false;
            
        } catch (error) {
            console.log(error);
        }
    }else{
        try {
            document.getElementById(campo).disabled = true;
            document.getElementById(campo).value = '';
            guardarDatosPlantillaHTML('cardiopulmonar',campo,document.getElementById(campo).value )
        } catch (error) {
            console.log(error);
        }
    }

}

function valorSwitchObservacion(ids, checks) {
    rec = ids;
    if (checks == true) {
        document.getElementById('switchobservacion' + ids).removeAttribute('hidden');
        document.getElementById(ids).value = "";


    } else if (checks == false) {
        document.getElementById(ids).value = "Normal";
        document.getElementById('switchobservacion' + ids).setAttribute('hidden', 'true');
    }

}

function colocarRiesgoEncuesta(riesgo, semaforo, id_diagnostico_conducta, diagnostico, conducta, medio_recepcion) {



    var campoRiesgo = document.getElementById('lblSemaforoRiesgoEncuesta');
    var boton = document.getElementById('btnRecomendaciones');
    var campoSede = document.getElementById('cmbSede1');
    var boton1 = document.getElementById('btnCrearAdmision');
    var campoDiagnostico = document.getElementById('divDiagnostico');
    var campoConducta = document.getElementById('divConducta');

    if (id_diagnostico_conducta == 17 || id_diagnostico_conducta == 13 || id_diagnostico_conducta == 13
        || riesgo == 'BAJO' || riesgo == 'MUY BAJO' || id_diagnostico_conducta == 3) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoBajoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 7 || id_diagnostico_conducta == 9 || id_diagnostico_conducta == 18
        || riesgo == 'MODERADO') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    }
    else if (riesgo == 'MALNUTRICION') {
        campoRiesgo.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoAltoEncuesta');// color rojo
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
    }
    else if (riesgo == 'RIESGO DE MALNUTRICION') {
        campoRiesgo.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');// color naranja
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
    }
    else if (riesgo == 'ESTADO NUTRICIONAL NORMAL') {
        campoRiesgo.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoBajoEncuesta');// color verde
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)

        //apgar familiar
    } else if (riesgo == 'NORMAL') {
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoBajoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (riesgo == 'DISFUNCION LEVE') {

        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    }
    else if (riesgo == 'DISFUNCION MODERADA') {

        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    }
    else if (riesgo == 'DISFUNCION SEVERA') {

        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoAltoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    }




    //NUEVAS REGLAS covid 2.0
    else if (id_diagnostico_conducta == 44) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 46) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'sinRiesgoColorBlanco');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 41 || riesgo == 'ALTO') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'sinRiesgoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 40 || riesgo == 'MODERADO') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoAltoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 39 || riesgo == 'LEVE') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 38) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 20 || riesgo == 'ALTO') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'sinRiesgoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 27) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoBajoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 28) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 29) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoBajoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 37 || riesgo == 'MODERADO') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoAltoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 34) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'sinRiesgoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 36 || riesgo == 'LEVE') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 35) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 42) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 33) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 43) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoModeradoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)

    } else if (id_diagnostico_conducta == 24 || id_diagnostico_conducta == 19 || id_diagnostico_conducta == 23 || id_diagnostico_conducta == 21 ||
        id_diagnostico_conducta == 20 || id_diagnostico_conducta == 10 || id_diagnostico_conducta == 16 ||
        id_diagnostico_conducta == 15 || id_diagnostico_conducta == 8 || id_diagnostico_conducta == 25
        || riesgo == 'ALTO') {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoAltoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)
    }
    else if (id_diagnostico_conducta == 22 || id_diagnostico_conducta == 14) {
        boton.removeAttribute('hidden');
        campoSede.removeAttribute('hidden');
        boton1.removeAttribute('hidden');
        boton.setAttribute('class', 'small button blue');
        boton1.setAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        campoDiagnostico.removeAttribute('hidden');
        campoConducta.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'riesgoColorTomateEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', 'Alerta: ' + riesgo + ' - ' + semaforo, 0)
        asignaAtributo('lbldiagnostico', diagnostico, 0)
        asignaAtributo('lblconducta', conducta, 0)
    }
    else {
        campoConducta.setAttribute('hidden', 'true');
        campoDiagnostico.setAttribute('hidden', 'true');
        boton.setAttribute('hidden', 'true');
        campoSede.setAttribute('hidden', 'true');
        boton1.setAttribute('hidden', 'true');
        boton.removeAttribute('class', 'small button blue');
        boton1.removeAttribute('class', 'small button blue');
        campoRiesgo.removeAttribute('hidden');
        document.getElementById('rowSemaforoRiesgo').setAttribute('class', 'sinRiesgoEncuesta');
        asignaAtributo('lblSemaforoRiesgoEncuesta', ' No presenta alerta. No hay ninguna encuesta seleccionada o la encuesta no a sido cerrada', 0)
    }
    if (valorAtributo('txtIdMedioRecepcion') == 3) {
        boton1.setAttribute('hidden', 'true');
        campoSede.setAttribute('hidden', 'true');
        boton1.removeAttribute('class', 'small button blue');
    }
}

function formatoRecomendacionTexto() {
    var texto = document.getElementById('paRecomendacionesModal').innerHTML
    var mitexto = '';
    var textoMostar = texto.split('.')
    for (let i = 0; i < textoMostar.length; i++) {
        mitexto += textoMostar[i];
    }
    document.getElementById('paRecomendacionesModal').innerText = mitexto;

}

function validarTabsGeneroEspecialidad(divAcordionFolioEspecialidad) {
    setTimeout(() => {
        console.log("validando folios de otros antecedentes...");
        if (valorAtributo('lblGeneroPaciente') == 'M') {
            //Validacion de edad par elpaciente: oculta plantillas de salud sexual y reproductiva para menores de 12 años
            if (valorAtributo('lblEdadPaciente') < 12) {
                console.log("menor de 12 años: ");
                console.log(document.getElementById('agom'));
                if (document.getElementById('espetabagom') != null) { document.getElementById('espetabagom').setAttribute('hidden', 'true'); }
                if (document.getElementById('espetabappfih') != null) { document.getElementById('espetabappfih').setAttribute('hidden', 'true'); }
                if (document.getElementById('agom') != null) { ocultar('agom'); }
                if (document.getElementById('appfih') != null) { ocultar('appfih'); }

            } else {
                if (document.getElementById('espetabagom') != null) { document.getElementById('espetabagom').setAttribute('hidden', 'true'); }
                if (document.getElementById('div_agom') != null) { document.getElementById('div_agom').setAttribute('hidden', 'true'); }
            }

        } else if (valorAtributo('lblGeneroPaciente') == 'F') {
            //Validacion de edad par elpaciente: oculta plantillas de salud sexual y reproductiva para menores de 12 años
            if (valorAtributo('lblEdadPaciente') < 12) {
                if (document.getElementById('espetabagom') != null) { document.getElementById('espetabagom').setAttribute('hidden', 'true'); }
                if (document.getElementById('espetabappfih') != null) { document.getElementById('espetabappfih').setAttribute('hidden', 'true'); }
                if (document.getElementById('agom') != null) { ocultar('agom'); }
                if (document.getElementById('appfih') != null) { ocultar('appfih'); }
            } else {
                if (document.getElementById('espetabappfih') != null) { document.getElementById('espetabappfih').setAttribute('hidden', 'true'); }
            }
        } 
      

        if( traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO') == 'MEDG'){
            if(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ESPECIALIDAD') != 'PEDIATRIA' ){
                if (document.getElementById('esvne1') != null) { ocultar('esvne1'); ocultar('espetabesvne1'); }
            }
        }

    }, 600);


    //alert("ingreso a genero :" +valorAtributo('lblGeneroPaciente') + " Especialidad  encontrada: "+document.getElementById('espetabagnut'))

}

function validarTabsEvaluacionGeneral() {
    setTimeout(() => {
        console.log("validando folios de Evaluacion general..");
        if (valorAtributo('lblTipoDocumento') == 'TFOI' || valorAtributo('lblTipoDocumento') == 'PARF' || valorAtributo('lblTipoDocumento') == 'HCTO'
            || valorAtributo('lblTipoDocumento') == 'HCTP' || valorAtributo('lblTipoDocumento') == 'HCFA' || valorAtributo('lblTipoDocumento') == 'HCFP'
            || valorAtributo('lblTipoDocumento') == 'NEUA' || valorAtributo('lblTipoDocumento') == 'NEUR' || valorAtributo('lblTipoDocumento') == 'HCVO'
            || valorAtributo('lblTipoDocumento') == 'TODA' || valorAtributo('lblTipoDocumento') == 'TODP' || valorAtributo('lblTipoDocumento') == 'HFAD'
            || valorAtributo('lblTipoDocumento') == 'HFPD' || valorAtributo('lblTipoDocumento') == 'NEAD' || valorAtributo('lblTipoDocumento') == 'NEPD'
            || valorAtributo('lblTipoDocumento') == 'TERR') {
            //Validacion Ocultar Plan Tratamiento TERAPIAS
            console.log(valorAtributo('lblTipoDocumento'))
            if (document.getElementById('neu10') != null) { ocultar('neu10'); ocultar('tabneu10'); }
            if (document.getElementById('vrl5') != null) { ocultar('vrl5'); ocultar('tabvrl5'); }

        }
        else if (valorAtributo('lblTipoDocumento') == 'CRNC' || valorAtributo('lblTipoDocumento') == 'CRNH') {
            traerDatosPlantilla(2791, 'CRNC');
        }
        else if (traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO') == 'B24X') {
            traerDatosPlantilla(3052, 'B24X');

        }
        if( traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO') == 'MEDG'){
            if(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ESPECIALIDAD') != 'TUBERCULOSIS' ){
                if (document.getElementById('tcle1') != null) { ocultar('tcle1'); ocultar('tabtcle1'); }
            }
        }
        if( traerDatoFilaSeleccionada('listDocumentosHistoricos', 'TIPO') == 'AEPP'){
            traerDatosPlantilla(3061, 'AEPP');
        }
    }, 300);
}


/**
 * Trae informacion de las plantillas dependiendo del id del folio y del tipo de folio seleccionado
 * Es necesario crear un query desde base de datos y la respuesta de lo que se quiere hacer
 * 
 * @author NICOLAS CAICEDO <nicolas.caicedo1799@gmail.com>
 * @param {Int} query - El query usado para traer informacion
 * @param {String} tipo - Tipo del folio con el que se utilizara la informacion
 * @returns {void}
 */
function traerDatosPlantilla(query, tipo) {
    valores_a_mandar = "";
    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
    $.ajax({
        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
        type: "POST",
        data: {
            "idQuery": query,
            "parametros": valores_a_mandar,
            "_search": false,
            "nd": 1611873687449,
            "rows": -1,
            "page": 1,
            "sidx": "NO FACT",
            "sord": "asc"
        },
        beforeSend: function () { },
        success: function (data) {
            respuesta = data.getElementsByTagName("cell");
            if (tipo == 'CRNC' || tipo == 'CRNH') {
                if (respuesta[1].firstChild.data == 'NO') {
                    if (document.getElementById('tabhaco') != null) ocultar('tabhaco');
                    if (document.getElementById('tabtmgl') != null) ocultar('tabtmgl');
                    if (document.getElementById('tabpcrs') != null) ocultar('tabpcrs');
                    if (document.getElementById('tabpcep') != null) ocultar('tabpcep');
                    if (document.getElementById('tabpcnp') != null) ocultar('tabpcnp');
                    if (document.getElementById('tabefpaz') != null) ocultar('tabefpaz');
                    if (document.getElementById('tabcrta') != null) ocultar('tabcrta');
                }
            }
            else if (tipo == 'B24X') {
                ocultarTabsVih(respuesta[1].firstChild.data);
            }
            if(tipo == 'AEPP'){
                let array = ['taboido','tabfieb','tabtodr','tabdesarrollo_vacunas','tabvegi','tabrecomendaciones_aepi']
                for (let i = 1; i < respuesta.length; i++) {
                    if (respuesta[i].firstChild.data != 'SI') {
                        if (document.getElementById(array[i-1]) != null) ocultar(array[i-1]);
                    }
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

function bloquearMisCampos(campos, tabla, checks) {

    if (checks == true) {
        campos.split(',').map(e => {
            va = document.getElementById(tabla + ',' + e).setAttribute('disabled', 'true');
        })
    } else {
        campos.split(',').map(e => {
            va = document.getElementById(tabla + ',' + e).removeAttribute('disabled');
        })
    }
}

function comboMed() {
    var valor = document.getElementById('idMedEvaluacion').value;
    cargarComboGRALCondicion1('', valor, 'miComboMedEvaluacion', 1021, IdSede());
    document.getElementById('miComboMedEvaluacion').removeAttribute('onclick', 'comboMed()');
}

function validarTabsGenero() {
    //alert("dato guardado de sexo paciente: "+valorAtributo('lblGeneroPaciente')+" Edad paciente: "+valorAtributo('lblEdadPaciente'))
    if (valorAtributo('lblGeneroPaciente') == 'F') {
        //alert("ingresa femenino");
        validarGraficasMujer();
        if (document.getElementById('tabpsenf') != null) { document.getElementById('tabpsenf').setAttribute('hidden', 'true'); }
        if (document.getElementById('tabappfih') != null) { document.getElementById('tabappfih').setAttribute('hidden', 'true'); }
        if (valorAtributo('lblEdadPaciente') < 12) {
            if (document.getElementById('tabcenf') != null) { document.getElementById('tabcenf').setAttribute('hidden', 'true'); }
            if (document.getElementById('tabagom') != null) { document.getElementById('tabagom').setAttribute('hidden', 'true'); }
            if (document.getElementById('agnut') != null) { document.getElementById('agnut').setAttribute('hidden', 'true'); }
        }

    } else if (valorAtributo('lblGeneroPaciente') == 'M') {
        validarGraficasHombre()
        //alert("valor encontrado: "+document.getElementById('tabagnut'))
        if (document.getElementById('tabcenf') != null) { document.getElementById('tabcenf').setAttribute('hidden', 'true'); }
        if (document.getElementById('tabagom') != null) { document.getElementById('tabagom').setAttribute('hidden', 'true'); }
        if (document.getElementById('tabagnut') != null) { document.getElementById('tabagnut').setAttribute('hidden', 'true'); }

        if (valorAtributo('lblEdadPaciente') < 12) {
            if (document.getElementById('tabpsenf') != null) { document.getElementById('tabpsenf').setAttribute('hidden', 'true'); }
            if (document.getElementById('tabappfih') != null) { document.getElementById('tabappfih').setAttribute('hidden', 'true'); }
        }
    }

    //tabcydenf-- tabpjenf
    if (valorAtributo('lblEdadPaciente') < 18) {
        //alert("ingresa 17...");
        if (document.getElementById('tabpjenf') != null) { document.getElementById('tabpjenf').setAttribute('hidden', 'true'); }
        //
    } else if (valorAtributo('lblEdadPaciente') < 28) {
        //alert("ingresa 27...");
        if (document.getElementById('tabcydenf') != null) { document.getElementById('tabcydenf').setAttribute('hidden', 'true'); }
        //
    } else if (valorAtributo('lblEdadPaciente') >= 28) {
        //alert("ingresa 28..+");
        if (document.getElementById('tabcydenf') != null) { document.getElementById('tabcydenf').setAttribute('hidden', 'true'); }
        if (document.getElementById('tabpjenf') != null) { document.getElementById('tabpjenf').setAttribute('hidden', 'true'); }
    }

}

function validarGraficasMujer() {
    if (document.getElementById('tabgpm02') != null) {

        //ocultar tabs para graficas de niño
        //alert("ingresa validacion mujer...");
        ocultar('tabgph02');
        ocultar('tabgph25');
        ocultar('tabgph518');

        //Validacion edad para tabs de graficas
        if (valorAtributo('lblEdadPaciente') >= 0 && valorAtributo('lblEdadPaciente') < 2) {
            //alert("ingresa 0 - 2...");
            document.getElementById('tabgpm25').setAttribute('hidden', 'true');
            document.getElementById('tabgpm518').setAttribute('hidden', 'true');

        } else if (valorAtributo('lblEdadPaciente') > 1 && valorAtributo('lblEdadPaciente') < 5) {
            //alert("ingresa 2 - 5...");
            document.getElementById('tabgpm02').setAttribute('hidden', 'true');
            document.getElementById('tabgpm518').setAttribute('hidden', 'true');

        } else if (valorAtributo('lblEdadPaciente') > 4 && valorAtributo('lblEdadPaciente') < 19) {
            //alert("ingresa 5 - 18...");
            document.getElementById('tabgpm02').setAttribute('hidden', 'true');
            document.getElementById('tabgpm25').setAttribute('hidden', 'true');
        } else {
            document.getElementById('tabgpm02').setAttribute('hidden', 'true');
            document.getElementById('tabgpm25').setAttribute('hidden', 'true');
            document.getElementById('tabgpm518').setAttribute('hidden', 'true');
        }
    }
}

function errorEncuesta() {//no permitir cerrar encuesta 

    if (valorAtributo('lblIdEncuesta') == '29') {
        //Switches de la encuesta de covid 2 clínicos
        c9 = document.getElementById('formulario.cvidd.c9').value;
        c10 = document.getElementById('formulario.cvidd.c10').value;
        c11 = document.getElementById('formulario.cvidd.c11').value;
        c12 = document.getElementById('formulario.cvidd.c12').value;
        c13 = document.getElementById('formulario.cvidd.c13').value;
        c14 = document.getElementById('formulario.cvidd.c14').value;
        c15 = document.getElementById('formulario.cvidd.c15').value;
        //Validar que alguno está en si
        if (c9 == 'SI' || c10 == 'SI' || c11 == 'SI' || c12 == 'SI' || c13 == 'SI' || c14 == 'SI' || c15 == 'SI') {
            //Switches sobre sintomas en covid 2
            c1 = document.getElementById('formulario.cvidd.c1').value;
            c2 = document.getElementById('formulario.cvidd.c2').value;
            c3 = document.getElementById('formulario.cvidd.c3').value;
            c4 = document.getElementById('formulario.cvidd.c4').value;
            c5 = document.getElementById('formulario.cvidd.c5').value;
            c17 = document.getElementById('formulario.cvidd.c17').value;
            //Validar si está alguno en si
            if (c1 == 'SI' || c2 == 'SI' || c3 == 'SI' || c4 == 'SI' || c5 == 'SI' || c17 == 'SI') {
                modificarMapCRUD('cerrarEncuestaPaciente');
            } else {

                alert("Recuerde marcar los síntomas");
            }
        } else {
            modificarMapCRUD('cerrarEncuestaPaciente');
        }

    } else {
        modificarMapCRUD('cerrarEncuestaPaciente');
    }

}

function sinDatoEncuesta() {//Alertar si desea cerrar encuesta con datos NO

    console.log("entra a funcion sin datosEncuesta");
    if (valorAtributo('lblIdEncuesta') == '29') {
        //Switches de la encuesta de covid 2 clínicos
        c1 = document.getElementById('formulario.cvidd.c1').value;
        c2 = document.getElementById('formulario.cvidd.c2').value;
        c3 = document.getElementById('formulario.cvidd.c3').value;
        c4 = document.getElementById('formulario.cvidd.c4').value;
        c17 = document.getElementById('formulario.cvidd.c17').value;
        c5 = document.getElementById('formulario.cvidd.c5').value;
        c6 = document.getElementById('formulario.cvidd.c6').value;
        c7 = document.getElementById('formulario.cvidd.c7').value;
        c8 = document.getElementById('formulario.cvidd.c8').value;
        c9 = document.getElementById('formulario.cvidd.c9').value;
        c10 = document.getElementById('formulario.cvidd.c10').value;
        c11 = document.getElementById('formulario.cvidd.c11').value;
        c12 = document.getElementById('formulario.cvidd.c12').value;
        c13 = document.getElementById('formulario.cvidd.c13').value;
        c14 = document.getElementById('formulario.cvidd.c14').value;
        c15 = document.getElementById('formulario.cvidd.c15').value;
        //Validar que todos esten en no
        if (c1 == 'NO' && c2 == 'NO' && c3 == 'NO' && c4 == 'NO' && c17 == 'NO' && c5 == 'NO' && c6 == 'NO' && c7 == 'NO' && c8 == 'NO' &&
            c9 == 'NO' && c10 == 'NO' && c11 == 'NO' && c12 == 'NO' && c13 == 'NO' && c14 == 'NO' && c15 == 'NO') {
            var mensaje = "\u00BFDesea cerrar la encuesta sin diligenciar la informaci\u00F3n?";
            var opcion = confirm(mensaje);
            if (opcion == true) {
                errorEncuesta();

            } else {
            }

        } else {
            errorEncuesta();
        }

    } else {
        modificarMapCRUD('cerrarEncuestaPaciente');
    }
    /*if (id_diagnostico_conducta == 3) {
        alert('Datos ingresados no validos, recuerde marcar sintomas');return false;
    }*/
}

function validarGraficasHombre() {
    if (document.getElementById('tabgph02') != null) {

        //ocultar tabs para graficas de niña
        //alert("ingresa validacion Hombre...");
        document.getElementById('tabgpm02').setAttribute('hidden', 'true');
        document.getElementById('tabgpm25').setAttribute('hidden', 'true');
        document.getElementById('tabgpm518').setAttribute('hidden', 'true');

        //Validacion edad para tabs de graficas
        if (valorAtributo('lblEdadPaciente') >= 0 && valorAtributo('lblEdadPaciente') < 2) {
            //alert("ingresa 0 - 2...");
            document.getElementById('tabgph25').setAttribute('hidden', 'true');
            document.getElementById('tabgph518').setAttribute('hidden', 'true');

        } else if (valorAtributo('lblEdadPaciente') > 1 && valorAtributo('lblEdadPaciente') < 5) {
            //alert("ingresa 2 - 5...");
            document.getElementById('tabgph02').setAttribute('hidden', 'true');
            document.getElementById('tabgph518').setAttribute('hidden', 'true');

        } else if (valorAtributo('lblEdadPaciente') > 4 && valorAtributo('lblEdadPaciente') < 19) {
            //alert("ingresa 5 - 18...");
            document.getElementById('tabgph02').setAttribute('hidden', 'true');
            document.getElementById('tabgph25').setAttribute('hidden', 'true');
        } else {
            document.getElementById('tabgph02').setAttribute('hidden', 'true');
            document.getElementById('tabgph25').setAttribute('hidden', 'true');
            document.getElementById('tabgph518').setAttribute('hidden', 'true');
        }
        //  
    }

}

function calcularCantidadMed() {
    console.log("ingreso metodo de clculo de medicamentos");
    var dosis = valorAtributo('txtCant');
    var horas = valorAtributo('txtFrecuencia');
    var dias = valorAtributo('txtDias');
    var entrga = 0
    if (horas > 24){
        entrga = dosis * Math.trunc(dias/(horas/24)) ;
    } else {
        entrga = dosis * (Math.trunc(24 / horas)) * dias;
    }
    asignaAtributo('txtCantidad', entrga, 0);
}

function calcularCantidadMedEditar() {
    console.log("ingreso metodo de clculo de medicamentos");
    var dosis = valorAtributo('cmbCantEditar');
    var horas = valorAtributo('cmbFrecuenciaEditar');
    var dias = valorAtributo('cmbDiasEditar');
    var entrga = 0
    if (horas > 24){
        entrga = dosis * Math.trunc(dias/(horas/24)) ;
    } else {
        entrga = dosis * (Math.trunc(24 / horas)) * dias;
    }

    asignaAtributo('txtCantidadEditar', entrga, 0);
}

function validarDatosEncuesta() {

    if (valorAtributo('lblIdEncuesta') == '28') {
        console.log("tipo operador: " + typeof (valorAtributo('lblIdEncuesta')));
        if (document.getElementById('cbnut,c4').value == 'SI') {
            var c4 = 0;
        } else {
            var c4 = 2;
        }
        var calculado = parseInt(document.getElementById('cbnut,c1').value) +
            parseInt(document.getElementById('cbnut,c2').value) + parseInt(document.getElementById('cbnut,c3').value)
            + parseInt(document.getElementById('cbnut,c5').value) + parseInt(document.getElementById('cbnut,c6').value)
            + parseInt(c4);
        //document.getElementById("c7").value = 'calculado';
        console.log("calculado: " + calculado);
        if (calculado >= 0 && calculado < 8) { document.getElementById("c7").value = 'MALNUTRICION'; document.getElementById("c7").style.color = "red"; }
        else if (calculado >= 8 && calculado < 12) { document.getElementById("c7").value = 'RIESGO DE MALNUTRICION'; document.getElementById("c7").style.color = "orange"; }
        else if (calculado >= 12) {
            document.getElementById("c7").value = 'ESTADO NUTRICIONAL NORMAL'; document.getElementById("c7").style.color = "green";
            //var el = document.getElementById("some-element");
            /// el.setAttribute("style", "background-color:darkblue;");
        }
        else { document.getElementById("c7").value = 'Datos insuficientes para el calculo...'; }
    }
}

function validarFechasPlantillas(id, tipo) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    //console.log(today, id, tipo)

    switch (tipo) {
        case 1:
            document.getElementById(id).max = today;
            break;
        case 2:
            document.getElementById(id).min = today;
            break;
        case 3:
            //Habilita fecha en un rango que va desde la fecha de nacimiento hasta la fecha de creacion de folio
            let fechaCrea = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'FECHA_CREA');
            let FechaNacimiento = document.getElementById('lblFechaNacimiento').textContent
            fechaCrea = fechaCrea.split(' ')
            document.getElementById(id).max = fechaCrea[0];
            document.getElementById(id).min = FechaNacimiento;
            
            break;
        case 4:
            //Habilita fecha en un rango que va desde la fecha de nacimiento hasta la fecha el dia de hoy
            let FechaNacimiento4 = document.getElementById('lblFechaNacimiento').textContent
            document.getElementById(id).max = today;
            document.getElementById(id).min = FechaNacimiento4;
            //console.log(today)
            
            break;
    }
}


function validarNumero1(id, numero, maxEnteros, maxDecimales) {
    // Expresión regular que valida un número con 2 enteros y 1 decimal
    //const regex = /^[0-9]{1,2}(\.[0-9]{1,1})?$/;
    // console.log(maxDecimales,'maxDecimales1')
    //const regex = new RegExp()
    if (maxDecimales == 0) {
        // console.log(maxDecimales,'maxDecimales1')
        var regex = new RegExp(`^[0-9]{1,${maxEnteros}}?$`);
    }else{
        // console.log(maxDecimales,'maxDecimales2')
        var regex = new RegExp(`^[0-9]{1,${maxEnteros}}(\\.[0-9]{1,${maxDecimales}})?$`);
    }


    // console.log(numero,'numero')
    // Comprobar si el número cumple con la expresión regular
    if (regex.test(numero)) {
        // console.log(numero,'valido')
        return true;

    } else {
        mensaje = `este campo recibe ${maxEnteros} numeros enteros y ${maxDecimales} numero decimales`
        Swal.fire(mensaje)
        // console.log(numero,'invalido')
        document.getElementById(id).value = ''
        return false;
    }
}

function justNumber(event){
    // console.log(event.key,'event')
    numeros = '0123456789.'
    numeros = numeros.split('')
    if (!numeros.includes(event.key)) {
        event.preventDefault()
    }
}

/**
 * Valida que al escribir las fechas se realicen de forma correcta
 * Afecta si se escriben mas de cuatro digitos o si es menor a 1700
 * 
 * @author NICOLAS CAICEDO <nicolas.caicedo1799@gmail.com>
 * @param {String} val - El valor con el cual se va a comparar
 * @param {String} id - Id del campo que se va a validad
 * @returns {void}
 */
function validarFechaPatron(val, id, fecha_defaul=1700){
    let year = val.split('-')[0]
    let patron = /^\d{4}\-\d{2}\-\d{2}$/
    if(val === ''){
        document.getElementById(id).value = ''
    }
    if(!patron.test(val) && val != ''){
        Swal.fire(
            'Fecha Invalida',
            'Digite la fecha en formato mm/dd/yyyy',
            'error'
        )
        document.getElementById(id).value = ''
    }

    if(year < fecha_defaul && year != '') {
        Swal.fire(
            'Fecha Invalida',
            `La fecha no puede ser menor a ${fecha_defaul}`,
            'error'
        )
        document.getElementById(id).value = ''
    }
}
/**
 * Valida que al escribir las fechas se realicen de forma correcta
 * Afecta si es mayor a la fecha actual o si es menor a la fecha actual
 * 
 * @author NICOLAS CAICEDO <nicolas.caicedo1799@gmail.com>
 * @param {String} val - El valor con el cual se va a comparar
 * @param {String} id - Id del campo que se va a validad
 * @param {Int} tipo - Tipo para saber si no se permiten valores mayores (1) o menores (2)
 * @returns {void}
 */
function validarFechaEscrita(val, id, tipo, anio = 1700 ,fechaMaxima = 0, fechaMinima = 0) {
    
    let day = val.split('-')[2]
    let month = val.split('-')[1]
    let year = val.split('-')[0]
    let today = new Date().toJSON().slice(0,10)
    if(val == ''){
        document.getElementById(id).value = ''
    }

    if(year < anio && year != '') {
        Swal.fire(
            'Fecha Invalida',
            'La fecha no puede ser menor a '+ anio,
            'error'
        )
        document.getElementById(id).value = ''
    }
    switch (tipo) {
        case 1:
            if (val > today){
                Swal.fire(
                    'Fecha Invalida',
                    'No puede ser mayor a la fecha actual',
                    'error'
                )
                document.getElementById(id).value = ''
            }
            break;
        case 2:
            if (val < today && val != ''){
                Swal.fire(
                    'Fecha Invalida',
                    'No puede ser menor a la fecha actual',
                    'error'
                )
                document.getElementById(id).value = ''
            }
            break;
        case 3:
            if(document.getElementById(id).value == ''){
                return
            }
            let fechaMinima = document.getElementById(id).min;
            let fechaMax = document.getElementById(id).max;
            if (val < fechaMinima ||  val > fechaMax ){
                Swal.fire(
                    'Fecha Invalida, Fuera Del Rango Establecido',
                    'la fecha debe ser mayor a ' + fechaMinima + ' y menor a ' +fechaMax 
                )
                document.getElementById(id).value = ''
            }
            break;
        case 4:
            fechaActual = new Date()
            const fechaSelecionada = new Date(val)

            fechaM = new Date(
                fechaActual.getFullYear() + fechaMaxima,
                fechaActual.getMonth(),
                fechaActual.getDate()
                )
            console.log(fechaM,fechaActual,fechaMaxima,'feechas')

            if (fechaSelecionada < fechaActual || fechaSelecionada > fechaM){
                Swal.fire(
                    'Fecha Invalida',
                    'No puede ser menor a la fecha actual y mayor a '+ fechaMaxima + ' años',
                    'error'
                )
                document.getElementById(id).value = ''
            }
            break;
}
}

function cargarExamenFisico() {
    cabeza = document.getElementById('formulario.fp01.c1');
    ojos = document.getElementById('formulario.fp01.c2');
    oidos = document.getElementById('formulario.fp01.c3');
    nariz = document.getElementById('formulario.fp01.c4');
    boca = document.getElementById('formulario.fp01.c5');
    garganta = document.getElementById('formulario.fp01.c6');
    cuello = document.getElementById('formulario.fp01.c7');
    endocrino = document.getElementById('formulario.fp01.c19');
    torax = document.getElementById('formulario.fp01.c8');
    pulmones = document.getElementById('formulario.fp01.c10');
    corazon = document.getElementById('formulario.fp01.c9');
    espalda = document.getElementById('formulario.fp01.c17');
    abdomen = document.getElementById('formulario.fp01.c11');
    pelvis = document.getElementById('formulario.fp01.c12');
    einferior = document.getElementById('formulario.fp01.c16');
    esuperior = document.getElementById('formulario.fp01.c15');
    genitourinario = document.getElementById('formulario.fp01.c14');
    tacto_rectal = document.getElementById('formulario.fp01.c13');
    piel = document.getElementById('formulario.fp01.c18');
    snervioso = document.getElementById('formulario.fp01.c20');

    cabeza.value = "Normo cefalico, cabello con implantacion adecuada para la edad, sin masas palpables ni visibles.";
    ojos.value = "Cejas y pesta\u00F1as integras, conjuntiva palpebral y bulbar normocoloreada, pupilas  isocoricas normoreactivas, tamizajes de agudeza visual sin alteraciones.";
    oidos.value = "Implantacion adecuada de pabellones auriculares, sin salida secreciones, sin masas palpables ni visibles, otoscopia, conducto y membrana timpanica sin alteraciones, tamizajes de perdida en la audicion sin alteracion.";
    nariz.value = "Sin desviaciones del dorso nasal, conductos permeable, sin secreciones, no dolor a la palpacion de senos paranasales.";
    boca.value = "Mucosas humedas, sin lesiones, normocoloreada, labios y lengua moviles, higiene adecuada.";
    garganta.value = "Faringe y amigdalas sin inflamacion ni eritema.";
    cuello.value = "Simetrico, no masas palpables ni visibles, sin regurgitacion yugular, no se auscultan soplos, palapacion pulsos carotideos adecuado, Glandulas tiroides: sin masas visibles y sin nodulos a la palpacion.";
    endocrino.value = "No nodulos palpables ni visibles.";
    torax.value = "Simetrico expansible, no dolor a la palpacion, no masas palpables ni visibles, mamas: no masas palpables ni visibles sin cambios en la coloracion en la textura y coloracion de la piel.";
    pulmones.value = "Campos pulmonares bien ventilados, murmullo vesicular audible sin ruidos agregados, (roncos, sibilancias y estertores).";
    corazon.value = "Ruidos cardiacos, ritmicos, regulares, ausencia de soplos.";
    espalda.value = "Movil sin limitacion funcional, no dolor a la palpacion no masas palpables ni visibles.";
    abdomen.value = "Blando, depresible, no doloroso a la palpacion, ausencia de masas visibles o palpables, ruidos preristalticos audible sin alteraciones, no se auscultan soplos vasculares, no hay signos de irritacion peritoneal y no hay cambios en la coloracion de la piel.";
    pelvis.value = "Simetrica, sin alteraciones en la movilidad.";
    einferior.value = "Simetricas, moviles, sin limitaciones funcionales, no dolor a la palpacion, no masas palpables ni visibles, no cambios en piel, pulsos perifericos palpables, pies: sin deformidades, no lesiones en piel, no cambios de la coloracion, uñas sin alteraciones, pruebas de sensibilidad adecuadas.";
    esuperior.value = "Simetricas, moviles, sin limitaciones funcionales, no dolor a la palpacion, no masas, no cambios en piel.";
    genitourinario.value = "Uretra permeable, genitales sin masas palpables o visibles, sin lesiones en la piel, no secreciones visibles en el momento de la inspeccion.";
    tacto_rectal.value = "Permeable, integro, sin lesiones, no se observan masas, ni hemorroides, ni fisuras, no se observan secreciones.";
    piel.value = "Normocoloreada, Normotermica, hidratacion adecuada, sin lesiones visibles o palpables.";
    snervioso.value = "Paciente conciente, orientado en tiempo y espacio, memoria conservada, coordinacion adecuada, tono y fuerza muscular adecuada, reflejos y pruebas de sensibilidad adecuada.";

    modificarCRUD('guardarExamenFisicoBloque')
}

function ValidacionesAIEPI(campoDestino,version) {
    switch (campoDestino) {
        case 'formulario.sigaiepi.c6'://SIGNOS EN GENERAL 2 MESES 5 AÑOS
            var sigac2 = document.getElementById('formulario.sigaiepi.c2');
            var sigac3 = document.getElementById('formulario.sigaiepi.c3');
            var sigac4 = document.getElementById('formulario.sigaiepi.c4');
            var sigac5 = document.getElementById('formulario.sigaiepi.c5');

            texto = "SIGNOS DE PELIGRO\nA) Referir URGENTEMENTE al hospital según las normas de Estabilización y transporte REFIERA\nB) Completar de inmediato la evaluación y el examen físico\nC) Administrar oxígeno\nD) Evaluar y clasificar la convulsión según el capítulo EPILEPSIA e inicie el plan de manejo adecuado\n";

            if (sigac2.value == 'NO' && sigac3.value == 'NO' && sigac4.value == 'NO' && sigac5.value == 'NO') {
                var value = document.getElementById(campoDestino)
                document.getElementById(campoDestino).style.backgroundColor = 'white';
                value.value = 'NO TIENE SIGNOS DE PELIGRO';
                guardarDatosPlantilla('sigaiepi', 'c6', value.value);
            }
            else {
                var value = document.getElementById(campoDestino)
                value.value = texto
                document.getElementById(campoDestino).style.backgroundColor = '#ffcccc';
                guardarDatosPlantilla('sigaiepi', 'c6', value.value);
            }
            break;
        case 'formulario.sigaiepi.c33':            //TOS O DIFICULTAD PARA RESPIRAR 2 MESES 5 AÑOS
            var sigac7 = document.getElementById('formulario.sigaiepi.c7');
            var sigac10 = document.getElementById('formulario.sigaiepi.c10');
            var sigac11 = document.getElementById('formulario.sigaiepi.c11');
            var sigac12 = document.getElementById('formulario.sigaiepi.c12');
            var sigac13 = document.getElementById('formulario.sigaiepi.c13');
            var sigac14 = document.getElementById('formulario.sigaiepi.c14');
            var sigac15 = document.getElementById('formulario.sigaiepi.c15');
            var sigac16 = document.getElementById('formulario.sigaiepi.c16');
            var sigac17 = document.getElementById('formulario.sigaiepi.c17');
            var sigac18 = document.getElementById('formulario.sigaiepi.c18');
            var sigac19 = document.getElementById('formulario.sigaiepi.c19');
            var sigac20 = document.getElementById('formulario.sigaiepi.c20');
            var sigac21 = document.getElementById('formulario.sigaiepi.c21');
            var sigac22 = document.getElementById('formulario.sigaiepi.c22');
            var sigac23 = document.getElementById('formulario.sigaiepi.c23');
            var sigac24 = document.getElementById('formulario.sigaiepi.c24');
            var sigac25 = document.getElementById('formulario.sigaiepi.c25');
            var sigac26 = document.getElementById('formulario.sigaiepi.c26');
            var sigac27 = document.getElementById('formulario.sigaiepi.c27');
            var sigac28 = document.getElementById('formulario.sigaiepi.c28');
            var sigac29 = document.getElementById('formulario.sigaiepi.c29');
            var sigac30 = document.getElementById('formulario.sigaiepi.c30');
            var sigac31 = document.getElementById('formulario.sigaiepi.c31');
            var sigac32 = document.getElementById('formulario.sigaiepi.c32');

            var crup = "";
            var colorCrup = "white";
            var bronquitis = "";
            var sibilancia = "";
            var neumonia = "";
            var tos = "";

            if (sigac7.value == 'SI' && sigac16.value == 'SI' && sigac19.value == 'SI')//CRUP GRAVE
            {
                crup = "CRUP GRAVE\nA) Referir URGENTEMENTE al hospital siguiendo las normas de estabilizacion y transporte REFIERA\nB) Administrar oxigeno\nC) Administrar Dexametasona\nD) Nebulizacion con adrenalina\n"
                colorCrup = "red";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c24",
                        color: "#ffcccc"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c28",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c24td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.sigaiepi.c28td").style.backgroundColor = 'white';
                sigac24.value = 'SI';
                sigac24.checked = true;
                sigac28.value = 'NO';
                sigac28.checked = false;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c28', sigac28.value), 100);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c24', sigac24.value), 200);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 300);
            }
            else if (sigac7.value == 'SI' && sigac16.value == 'SI') // CRUP
            {
                crup = "CRUP\nA) Administrar dosis de Dexametasona\nB) Clasificar la severidad del CRUP y tratar de acuerdo con la severidad de la obstruccion\n"
                colorCrup = "#ffffcc";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c24",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c28",
                        color: "#ffffcc"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.sigaiepi.c28td").style.backgroundColor = '#ffffcc';
                sigac28.value = 'SI';
                sigac28.checked = true;
                sigac24.value = 'NO';
                sigac24.checked = false;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c24', sigac24.value), 100);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c28', sigac28.value), 200);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 300);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c24",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c28",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.sigaiepi.c24td").style.backgroundColor = 'white';
                //document.getElementById("formulario.sigaiepi.c28td").style.backgroundColor = 'white';
                sigac28.value = 'NO';
                sigac28.checked = false;
                sigac24.value = 'NO';
                sigac24.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c24', sigac24.value), 100);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c28', sigac28.value), 200);
            }

            if (sigac7.value == 'SI' && parseInt(valorAtributo('lblEdadPaciente')) < 2 && sigac22.value == 'SI' && sigac12.value == 'SI'
                && (sigac11.value == 'SI' || sigac10.value == 'SI' || sigac14.value == 'SI' || sigac21.value == 'SI'))//Bronquitis Grave
            {
                bronquitis = "Bronquitis Grave\nA- Referir URGENTEMENTE al hospital siguiendo las normas de estabilización y transporte \nB- Administrar oxígeno \nC- Si tolera la vía oral, aumentar ingesta de líquidos y leche materna\n"
                colorBronquitis = "#ffcccc";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c25",
                        color: "#ffcccc"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c29",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c25td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.sigaiepi.c29td").style.backgroundColor = 'white';
                sigac25.value = 'SI';
                sigac25.checked = true;
                sigac29.value = 'NO';
                sigac29.checked = false;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c29', sigac29.value), 100);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c25', sigac25.value), 200);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 300);
            }
            else if (sigac7.value == 'SI' && valorAtributo('lblEdadPaciente') < 2 && sigac22.value == 'SI' && sigac12.value == 'SI'
                && (sigac11.value == 'NO' || sigac10.value == 'NO' || sigac14.value == 'SI' || sigac21.value == 'NO'))//Bronquitis
            {
                bronquitis = "Bronquitis\nA) Aseo nasal con suero fisiológico cada 3 a 4 horas\nB) Aumentar ingesta de líquidos y leche materna\nC) Enseñar a la madre a cuidar al niño en casa\nD) Enseñar a la madre signos de alarma para regresar de inmediato\nE) Consulta de seguimiento dos días despues\n"
                colorBronquitis = "yellow";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c25",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c29",
                        color: "#ffffcc"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.sigaiepi.c29td").style.backgroundColor = '#ffffcc';
                sigac29.value = 'SI';
                sigac29.checked = true;
                sigac25.value = 'NO';
                sigac25.checked = false;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c25', sigac25.value), 100);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c29', sigac29.value), 200);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 300);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c25",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c29",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.sigaiepi.c29td").style.backgroundColor = 'white';
                sigac29.value = 'NO';
                sigac29.checked = false;
                sigac25.value = 'NO';
                sigac25.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c25', sigac25.value), 100);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c29', sigac29.value), 200);
            }

            if (sigac7.value == 'SI' && sigac17.value == 'SI' && sigac14.value == 'SI' && sigac15.value == "SI")//sibilancia Recurrente Grave
            {
                sibilancia = "Sibilancia Recurrente Grave\nA) Referir URGENTEMENTE al hospital siguiendo las normas de estabilización y transporte REFIERA\nB) Administrar oxígeno\nC) Administrar un B2 agonista cada 20 minutos por 3 veces\nD) Si es sibilancia recurrente administrar la primera dosis de corticosteroide\n";
                colorSibilancia = "#ffcccc";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c26",
                        color: "#ffffcc"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c30",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c26td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.sigaiepi.c30td").style.backgroundColor = 'white';
                sigac26.value = "SI";
                sigac26.checked = true;
                sigac30.value = "NO";
                sigac30.checked = false;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c26', sigac26.value), 300);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c30', sigac30.value), 400);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 500);
            }
            else if (sigac7.value == 'SI' && sigac15.value == "SI") {
                sibilancia = "Sibilancia Recurrente\nA) Iniciar tratamiento en sala ERA con esquema de B2 agonista\nB) Si es sibilancia recurrente administrar la primera dosis de un corticosteriode\nC) Volver a clasificar según esquema de tratamiento del niño con SIBILANCIAS o CRISIS DE ASMA\n";
                colorSibilancia = "yellow";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c26",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c30",
                        color: "#ffffcc"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.sigaiepi.c30td").style.backgroundColor = '#ffffcc';
                sigac26.value = "NO";
                sigac26.checked = false;
                sigac30.value = "SI";
                sigac30.checked = true;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c26', sigac26.value), 300);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c30', sigac30.value), 400);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 500);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c26",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c30",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.sigaiepi.c30td").style.backgroundColor = 'white';
                sigac26.value = "NO";
                sigac26.checked = false;
                sigac30.value = "NO";
                sigac30.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c26', sigac26.value), 300);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c30', sigac30.value), 400);
            }

            if (sigac7.value == 'SI' && sigac11.value == "SI")//NEUMONIA GRAVE
            {
                neumonia = "NEUMONIA GRAVE \nA) Referir URGENTEMENTE al hospital siguiendo las normas de estabilización y transporte REFIERA\nB) Administrar oxígeno\nC) Administrar la primera dosis de un antibiótico apropiado\nD) Tratar la fiebre\nE) Si hay neumonía severa que requiere UCI descarte VIH\n"
                colorNeumonia = "#ffcccc";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c27",
                        color: "#ffffcc"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c31",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c27td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.sigaiepi.c31td").style.backgroundColor = 'white';
                sigac27.value = "SI";
                sigac27.checked = true;
                sigac31.value = "NO";
                sigac31.checked = false;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c27', sigac27.value), 500);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c31', sigac31.value), 600);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 700);
            }
            else if (sigac7.value == 'SI' && sigac10.value == "SI") {//NEUMONIA
                neumonia = "NEUMONIA\nA) Dar un antibiotico apropiado\nB) Tratar la fiebre\nC) Aliviar los sintomas (obstruccion nasal y tos) con aseo nasal y bebidas endulzadas\nD) Ensenar a la madre a cuidar el nino en casa\nE) Ensenar a la madre signos de alarma para volver de inmediato\nF) Hacer consulta de seguimiento 2 dias despues\nG) Ensenar medidas preventivas especificas\nH) Si hay neumonia a repeticion (mas de 2 al año) siga recomendacion de protocolo de VIH\n";
                colorNeumonia = "yellow";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c27",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c31",
                        color: "#ffffcc"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.sigaiepi.c31td").style.backgroundColor = '#ffffcc';
                sigac27.value = "NO";
                sigac27.checked = false;
                sigac31.value = "SI";
                sigac31.checked = true;
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c27', sigac27.value), 500);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c31', sigac31.value), 600);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 700);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c27",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.sigaiepi.c31",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.sigaiepi.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.sigaiepi.c31td").style.backgroundColor = 'white';
                sigac27.value = "NO";
                sigac27.checked = false;
                sigac31.value = "NO";
                sigac31.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c27', sigac27.value), 500);
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c31', sigac31.value), 600);
            }

            if (sigac7.value == "SI" && sigac24.value == 'NO' && sigac25.value == 'NO' && sigac26.value == 'NO' && sigac27.value == 'NO' && sigac28.value == 'NO' && sigac29.value == 'NO' && sigac30.value == 'NO' && sigac31.value == 'NO') {
                tos = "TOS\nA) Tratar la fiebre\nB) Aliviar los sintomas (obstruccion nasal y tos) con aseo nasal y bebidas endulzadas\nC) Ensenar a la madre a cuidar el nino en casa\nD) Ensenar a la madre los signos de alarma para regresar de inmediato\nE) Si no mejora, consulta de seguimiento 5 dias despues\nF) Si hace mas de 21 dias que el nino tiene tos, evaluelo segun el cuadro de clasificacion de Tuberculosis.\n"
                colortos = "green";
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c32",
                        color: "#ccffcc"                        
                    }
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.sigaiepi.c32td").style.backgroundColor = '#ccffcc';
                sigac32.value = "SI";
                sigac32.checked = true;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 700);
            }
            else {
                tos = "NO TIENE TOS"
                let data = [
                    {
                        campoDestino:"formulario.sigaiepi.c32",
                        color: "white"                        
                    }
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.sigaiepi.c32td").style.backgroundColor = 'white';
                sigac32.value = "NO";
                sigac32.checked = false;
                setTimeout(guardarDatosPlantilla('sigaiepi', 'c32', sigac32.value), 700);
            }

            texto = crup + bronquitis + sibilancia + neumonia + tos;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('sigaiepi', 'c33', value.value), 800);

            break;
        case 'formulario.diarrea_sal_ment.c23': // DIARREA 2 MESES 5 AÑOS
            var diarreac1 = document.getElementById('formulario.diarrea_sal_ment.c1');
            var diarreac2 = document.getElementById('formulario.diarrea_sal_ment.c2');
            var diarreac3 = document.getElementById('formulario.diarrea_sal_ment.c3');
            var diarreac4 = document.getElementById('formulario.diarrea_sal_ment.c4');
            var diarreac5 = document.getElementById('formulario.diarrea_sal_ment.c5');
            var diarreac6 = document.getElementById('formulario.diarrea_sal_ment.c6');
            var diarreac7 = document.getElementById('formulario.diarrea_sal_ment.c7');
            var diarreac8 = document.getElementById('formulario.diarrea_sal_ment.c8');
            var diarreac9 = document.getElementById('formulario.diarrea_sal_ment.c9');
            var diarreac10 = document.getElementById('formulario.diarrea_sal_ment.c10');
            var diarreac11 = document.getElementById('formulario.diarrea_sal_ment.c11');
            var diarreac12 = document.getElementById('formulario.diarrea_sal_ment.c12');
            var diarreac13 = document.getElementById('formulario.diarrea_sal_ment.c13');
            var diarreac14 = document.getElementById('formulario.diarrea_sal_ment.c14');
            var diarreac15 = document.getElementById('formulario.diarrea_sal_ment.c15');
            var diarreac16 = document.getElementById('formulario.diarrea_sal_ment.c16');
            var diarreac17 = document.getElementById('formulario.diarrea_sal_ment.c17');
            var diarreac18 = document.getElementById('formulario.diarrea_sal_ment.c18');
            var diarreac19 = document.getElementById('formulario.diarrea_sal_ment.c19');
            var diarreac20 = document.getElementById('formulario.diarrea_sal_ment.c20');
            var diarreac21 = document.getElementById('formulario.diarrea_sal_ment.c21');
            var diarreac22 = document.getElementById('formulario.diarrea_sal_ment.c22');

            var deshidratacion = "";
            var persistente = "";
            var disenteria = "";

            if (diarreac1.value == "SI" && ((diarreac4.value == "SI" && diarreac6.value == "SI") || (diarreac4.value == "SI" && diarreac7.value == "SI") // DESHIDRATACION GRAVE
                || (diarreac4.value == "SI" && diarreac9.value == "3") || (diarreac6.value == "SI" && diarreac7.value == "SI")
                || (diarreac6.value == "SI" && diarreac9.value == "3") || (diarreac7.value == "SI" && diarreac9.value == "3"))) {
                deshidratacion = "DIARREA CON DESHIDRATACION GRAVE\nA) Si tiene otra clasificación grave: referir URGENTEMENTE al hospital según las normas de estabilización y transporte REFIERA\nB) Si el niño no tiene otra clasificación grave: Hidratar como se describe en el PLAN C\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c16",
                        color: "#ffcccc"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c18",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c19",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c22",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c16td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.diarrea_sal_ment.c18td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c19td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c22td").style.backgroundColor = 'white';
                diarreac16.value = "SI";
                diarreac16.checked = true;
                diarreac18.value = "NO";
                diarreac18.checked = false;
                diarreac19.value = "NO";
                diarreac19.checked = false;
                diarreac22.value = "NO";
                diarreac22.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c16', diarreac16.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c18', diarreac18.value), 200);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c19', diarreac19.value), 300);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c22', diarreac22.value), 400);

            }
            else if (diarreac1.value == "SI" && ((diarreac5.value == "SI" && diarreac6.value == "SI") || (diarreac5.value == "SI" && diarreac8.value == "SI") // DESHIDRATACION CON ALGUN GRADO
                || (diarreac5.value == "SI" && diarreac9.value == "LENTO") || (diarreac6.value == "SI" && diarreac8.value == "SI")
                || (diarreac6.value == "SI" && diarreac9.value == "LENTO") || (diarreac8.value == "SI" && diarreac9.value == "LENTO"))) {
                deshidratacion = "DIARREA CON ALGUN GRADO DE DESHIDRATACION \nA) Si tiene otra clasificación grave referir URGENTEMENTE al hospital según las normas de estabilización y transporte REFIERA, con la madre administrando SRO.\nB) Hidratar como se describe en el PLAN B con SRO\nC) Suplementación terapéutica con zinc\nD) Continuar la lactancia materna\nE) Enseñar a la madre los signos de alarma para regresar de inmediato\nF) Consulta de control 2 días después si la diarrea continúa\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c16",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c18",
                        color: "#ffcccc"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c19",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c22",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c16td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c18td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.diarrea_sal_ment.c19td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c22td").style.backgroundColor = 'white';
                diarreac16.value = "NO";
                diarreac16.checked = false;
                diarreac18.value == "SI";
                diarreac18.checked = true;
                diarreac19.value = "NO";
                diarreac19.checked = false;
                diarreac22.value = "NO";
                diarreac22.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c16', diarreac16.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c18', diarreac18.value), 200);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c19', diarreac19.value), 300);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c22', diarreac22.value), 400);
            }
            else if (diarreac1.value == "SI" && (diarreac10.value == 'SI' || diarreac11.value == 'SI' || diarreac12.value == 'SI')) {
                deshidratacion = "DIARREA CON RIESGO DE DESHIDRATACION \nA) Si tiene otra clasificación grave referir URGENTEMENTE al hospital según las normas de estabilización y transporte REFIERA, con la madre administrando SRO.\nB) Hidratar como se describe en el PLAN B con SRO\nC) Suplementación terapéutica con zinc\nD) Continuar la lactancia materna\nE) Enseñar a la madre los signos de alarma para regresar de inmediato\nF) Consulta de control 2 días después si la diarrea continúa";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c16",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c18",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c19",
                        color: "#ffcccc"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c22",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c16td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c18td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c19td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.diarrea_sal_ment.c22td").style.backgroundColor = 'white';
                diarreac16.value = "NO";
                diarreac16.checked = false;
                diarreac18.value = "NO";
                diarreac18.checked = false;
                diarreac19.value = "SI";
                diarreac19.checked = true;
                diarreac22.value = "NO";
                diarreac22.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c16', diarreac16.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c18', diarreac18.value), 200);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c19', diarreac19.value), 300);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c22', diarreac22.value), 400);
            }
            else if (diarreac1.value == "SI") {
                deshidratacion = "DIARREA SIN DESHIDRATACION\nA) Dar líquidos y alimentos para tratar la diarrea en casa PLAN A\nB) Suplementación terapéutica con zinc\nC) Enseñar a la madre los signos de alarma para volver de inmediato\nD) Consulta de control 2 días después si la diarrea continua\nE) Enseñar medidas preventivas específicas\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c16",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c18",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c19",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c22",
                        color: "green"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c16td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c18td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c19td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c22td").style.backgroundColor = 'green';
                diarreac16.value = "NO";
                diarreac16.checked = false;
                diarreac18.value = "NO";
                diarreac18.checked = false;
                diarreac19.value = "NO";
                diarreac19.checked = false;
                diarreac22.value = "SI";
                diarreac22.checked = true;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c16', diarreac16.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c18', diarreac18.value), 200);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c19', diarreac19.value), 300);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c22', diarreac22.value), 400);
            }
            else {
                deshidratacion = "NO TIENE DIARREA"
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c16",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c18",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c19",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c22",
                        color: "white"                        
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c16td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c18td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c19td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c22td").style.backgroundColor = 'white';
                diarreac16.value = "NO";
                diarreac16.checked = false;
                diarreac18.value = "NO";
                diarreac18.checked = false;
                diarreac19.value = "NO";
                diarreac19.checked = false;
                diarreac22.value = "NO";
                diarreac22.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c16', diarreac16.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c18', diarreac18.value), 200);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c19', diarreac19.value), 300);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c22', diarreac22.value), 400);
            }

            if (diarreac1.value == "SI" && (diarreac16.value == "SI" || diarreac18.value == "SI") && parseInt(valorAtributo('lblMesesEdadPaciente') < 6)) {
                persistente = "DIARREA PERSISTENTE GRAVE\nA) Referir al hospital siguiendo las normas de estabilización y transporte REFIERA\nB) Tratar la deshidratación antes de enviar al hospital a menos que tenga otra clasificación grave\nC) Administrar dosis adicional de vitamina A\nD) Descartar VIH según protocolo.\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c17",
                        color: "#ffcccc"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c20",
                        color: "white"                        
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c17td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.diarrea_sal_ment.c20td").style.backgroundColor = 'white';
                diarreac17.value = "SI";
                diarreac17.checked = true;
                diarreac20.value = "NO";
                diarreac20.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c17', diarreac17.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c20', diarreac20.value), 200);
            }
            else if (diarreac1.value == "SI" && diarreac16.value == "NO" && diarreac18.value == "NO" && parseInt(valorAtributo('lblMesesEdadPaciente')) >= 6 && (diarreac9.value == "2" || diarreac9.value == "3")) {
                persistente = "DIARREA PERSISTENTE \nA) Enseñar a la madre como alimentar al niño con DIARREA PERSISTENTE\nB) Suplementación terapéutica con Zinc\nC) Administrar una dosis adicional de vitamina A\nD) Administrar suplemento de vitaminas y minerales,\nE) Enseñar a la madre signos de alarma para regresar de inmediato\nF) Consulta de seguimiento 2 días después\nG) Enseñar medidas preventivas\nH) Descartar VIH según protocolo\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c17",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c20",
                        color: "#ffffcc"                        
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c17td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c20td").style.backgroundColor = '#ffffcc';
                diarreac17.value = "NO";
                diarreac17.checked = false;
                diarreac20.value = "SI";
                diarreac20.checked = true;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c17', diarreac15.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c20', diarreac18.value), 200);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c17",
                        color: "white"                        
                    },
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c20",
                        color: "white"                        
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c17td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_sal_ment.c20td").style.backgroundColor = 'white';
                diarreac17.value = "NO";
                diarreac17.checked = false;
                diarreac20.value = "NO";
                diarreac20.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c17', diarreac17.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c20', diarreac20.value), 200);
            }

            if (diarreac1.value == "SI" && diarreac3.value == "SI") {

                disenteria = "DISENTERIA\nA) Administrar un antibiótico apropiado\nB) Suplementación terapéutica con zinc\nC) Tratar la deshidratación según el plan indicado\nD) Hacer consulta de seguimiento 2 días después\nE) Enseñar a la madre signos de alarma para regresar de inmediato si empeora\nF) Enseñar medidas preventivas específicas\n"
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c21",
                        color: "#ffffcc"                        
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_sal_ment.c21td").style.backgroundColor = '#ffffcc';
                diarreac21.value = "SI";
                diarreac21.checked = true;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c21', diarreac21.value), 100);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.diarrea_sal_ment.c21",
                        color: "white"                        
                    }                    
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.diarrea_sal_ment.c21td").style.backgroundColor = 'white';
                diarreac21.value = "NO";
                diarreac21.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c21', diarreac21.value), 100);
            }

            texto = deshidratacion + persistente + disenteria;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('diarrea_sal_ment', 'c23', value.value), 500);
            break;
        case 'formulario.fiapi.c48': // FIEBRE 2 MESES 5 AÑOS
            var fiapic1 = document.getElementById('formulario.fiapi.c1');
            var fiapic2 = document.getElementById('formulario.fiapi.c2');
            var fiapic3 = document.getElementById('formulario.fiapi.c3');
            var fiapic4 = document.getElementById('formulario.fiapi.c4');
            var fiapic5 = document.getElementById('formulario.fiapi.c5');
            var fiapic6 = document.getElementById('formulario.fiapi.c6');
            var fiapic7 = document.getElementById('formulario.fiapi.c7');
            var fiapic8 = document.getElementById('formulario.fiapi.c8');
            var fiapic9 = document.getElementById('formulario.fiapi.c9');
            var fiapic10 = document.getElementById('formulario.fiapi.c10');
            var fiapic11 = document.getElementById('formulario.fiapi.c11');
            var fiapic12 = document.getElementById('formulario.fiapi.c12');
            var fiapic13 = document.getElementById('formulario.fiapi.c13');
            var fiapic14 = document.getElementById('formulario.fiapi.c14');
            var fiapic15 = document.getElementById('formulario.fiapi.c15');
            var fiapic16 = document.getElementById('formulario.fiapi.c16');
            var fiapic17 = document.getElementById('formulario.fiapi.c17');
            var fiapic18 = document.getElementById('formulario.fiapi.c18');
            var fiapic19 = document.getElementById('formulario.fiapi.c19');
            var fiapic20 = document.getElementById('formulario.fiapi.c20');
            var fiapic21 = document.getElementById('formulario.fiapi.c21');
            var fiapic22 = document.getElementById('formulario.fiapi.c22');
            var fiapic23 = document.getElementById('formulario.fiapi.c23');
            var fiapic24 = document.getElementById('formulario.fiapi.c24');
            var fiapic25 = document.getElementById('formulario.fiapi.c25');
            var fiapic26 = document.getElementById('formulario.fiapi.c26');
            var fiapic27 = document.getElementById('formulario.fiapi.c27');
            var fiapic28 = document.getElementById('formulario.fiapi.c28');
            var fiapic29 = document.getElementById('formulario.fiapi.c29');
            var fiapic30 = document.getElementById('formulario.fiapi.c30');
            var fiapic31 = document.getElementById('formulario.fiapi.c31');
            var fiapic32 = document.getElementById('formulario.fiapi.c32');
            var fiapic33 = document.getElementById('formulario.fiapi.c33');
            var fiapic34 = document.getElementById('formulario.fiapi.c34');
            var fiapic35 = document.getElementById('formulario.fiapi.c35');
            var fiapic36 = document.getElementById('formulario.fiapi.c36');
            var fiapic37 = document.getElementById('formulario.fiapi.c37');
            var fiapic38 = document.getElementById('formulario.fiapi.c38');
            var fiapic39 = document.getElementById('formulario.fiapi.c39');
            var fiapic40 = document.getElementById('formulario.fiapi.c40');
            var fiapic41 = document.getElementById('formulario.fiapi.c41');
            var fiapic42 = document.getElementById('formulario.fiapi.c42');
            var fiapic43 = document.getElementById('formulario.fiapi.c43');
            var fiapic44 = document.getElementById('formulario.fiapi.c44');
            var fiapic45 = document.getElementById('formulario.fiapi.c45');
            var fiapic46 = document.getElementById('formulario.fiapi.c46');
            var fiapic47 = document.getElementById('formulario.fiapi.c47');

            var febril = "";
            var malaria = "";
            var dengue = "";
            var riesgobajo = "";

            if (fiapic4.value == "SI" || fiapic5.value == "SI" || fiapic7.value == "SI" || fiapic14.value == "SI"
                || fiapic26.value == "SI" || fiapic8.value == "SI" || fiapic13.value == "SI" || fiapic20.value == "SI")//alto riesgo febril
            {
                febril = "ENFERMEDAD FEBRIL DE RIESGO ALTO\nA) Referir URGENTEMENTE al hospital siguiendo las normas de estabilización y transporte REFIERA\nB) Administrar la primera dosis de un antibiótico adecuado\nC) Tratar la fiebre\nD) Tratar las convulsiones\nE) Prevenir la hipoglicemia\nF) Garantizar adecuada hidratación\nG) Administrar oxígeno\n";
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c40",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.fiapi.c44",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c47",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c40td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.fiapi.c44td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c47td").style.backgroundColor = 'white';
                fiapic40.value = "SI";
                fiapic40.checked = true;
                fiapic44.value = "NO";
                fiapic44.checked = false;
                fiapic47.value = "NO";
                fiapic47.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c40', fiapic40.value), 100);
                setTimeout(guardarDatosPlantilla('fiapi', 'c44', fiapic44.value), 200);
                setTimeout(guardarDatosPlantilla('fiapi', 'c47', fiapic47.value), 300);
            }
            else if (fiapic3.value == "SI" || fiapic21.vales == "SI" || (fiapic5.value == "SI" && valorAtributo('lblMesesEdadPaciente') >= 6 && valorAtributo('lblMesesEdadPaciente' <= 24))) {
                febril = "ENFERMEDAD FEBRIL DE RIESGO INTERMEDIO\nA) Realizar Hemograma y Parcial de Orina, si no es posible referir\nB) Hemograma: >15.000 leucocitos o >10.000 neutrófilos, tratar como ENFERMEDAD FEBRIL DE RIESGO ALTO\nC) Hemograma: <4000 leucocitos o <100.000 plaquetas REFERIR\nD) Parcial de orina compatible con infección urinaria (pielonefritis) REFERIR\nE) Tratar la fiebre\nF) Asegurar adecuada hidratación por vía oral\nG) Si ha tenido fiebre más de 7 días REFERIR\nH) Hacer consulta de seguimiento en 2 día\nI) Enseñar a la madre los signos de alarma para regresar de inmediato\n";
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c40",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c44",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.fiapi.c47",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c40td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c44td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.fiapi.c47td").style.backgroundColor = 'white';
                fiapic40.value = "NO";
                fiapic40.checked = false;
                fiapic44.value = "SI";
                fiapic44.checked = true;
                fiapic47.value = "NO";
                fiapic47.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c40', fiapic40.value), 100);
                setTimeout(guardarDatosPlantilla('fiapi', 'c44', fiapic44.value), 200);
                setTimeout(guardarDatosPlantilla('fiapi', 'c47', fiapic47.value), 300);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c40",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c44",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c47",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c40td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c44td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c47td").style.backgroundColor = 'white';
                fiapic40.value = "NO";
                fiapic40.checked = false;
                fiapic44.value = "NO";
                fiapic44.checked = false;
                fiapic47.value = "NO";
                fiapic47.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c40', fiapic40.value), 100);
                setTimeout(guardarDatosPlantilla('fiapi', 'c44', fiapic44.value), 200);
                setTimeout(guardarDatosPlantilla('fiapi', 'c47', fiapic47.value), 300);
            }

            if (fiapic40.value == "SI" && fiapic27.value == "SI") {
                malaria = "MALARIA COMPLICADA\n A) Referir URGENTEMENTE al hospital siguiendo las normas de estabilización y transporte REFIERA\nB) Tomar gota gruesa, si es positiva administrar primera dosis de Artesunato sódico IV (contraindicado en menores de 6 meses), si no está disponible utilizar 2° línea: Diclorhidrato de quinina IV.\nC) Si es negativa, administrar la primera dosis de un antibiótico apropiado para ENFERMEDAD FEBRIL DE RIESGO ALTO\nD) Prevenir la hipoglicemia\nE) Tratar la fiebre\nF) Tratar las convulsiones\nG) Garantizar adecuada hidratación\nH) Administrar oxígeno\n"
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c41",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.fiapi.c45",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c41td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.fiapi.c45td").style.backgroundColor = 'white';
                fiapic41.value = "SI";
                fiapic41.checked = true;
                fiapic45.value = "NO";
                fiapic45.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c41', fiapic41.value), 400);
                setTimeout(guardarDatosPlantilla('fiapi', 'c45', fiapic45.value), 500);
            }
            else if (fiapic40.value == "NO" && fiapic27.value == "SI") {
                malaria = "MALARIA\n A) Tomar gota gruesa: si es positiva iniciar tratamiento para MALARIA , según el plasmodium\nB) Si gota gruesa negativa tratar como ENFERMEDAD FEBRIL DE RIESGO INTERMEDIO O BAJO y realizar gota gruesa cada 12 horas por 48 horas\nC) Tratar la fiebre\nD) Asegurar adecuada hidratación por vía oral\nE) Hacer consulta de seguimiento en 2 días\nF) Enseñar a la madre los signos de alarma para regresar de inmediato\nG) Enseñar medidas preventivas específicas\n"
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c41",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c45",
                        color: "#ffcccc"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c41td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c45td").style.backgroundColor = '#ffffcc';
                fiapic41.value = "NO";
                fiapic41.checked = false;
                fiapic45.value = "SI";
                fiapic45.checked = true;
                setTimeout(guardarDatosPlantilla('fiapi', 'c41', fiapic41.value), 400);
                setTimeout(guardarDatosPlantilla('fiapi', 'c45', fiapic45.value), 500);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c41",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c45",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c41td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c45td").style.backgroundColor = 'white';
                fiapic41.value = "NO";
                fiapic41.checked = false;
                fiapic45.value = "NO";
                fiapic45.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c41', fiapic41.value), 400);
                setTimeout(guardarDatosPlantilla('fiapi', 'c45', fiapic45.value), 500);
            }

            if (fiapic27.value == "SI" && fiapic5.value == "SI" && ((fiapic16.value == "SI" && fiapic22.value == "SI")
                || (fiapic16.value == "SI" && fiapic23.value == "SI") || (fiapic16.value == "SI" && fiapic10.value == "SI")
                || (fiapic16.value == "SI" && fiapic17.value == "SI") || (fiapic22.value == "SI" && fiapic23.value == "SI")
                || (fiapic22.value == "SI" && fiapic10.value == "SI") || (fiapic22.value == "SI" && fiapic17.value == "SI")
                || (fiapic23.value == "SI" && fiapic10.value == "SI") || (fiapic23.value == "SI" && fiapic17.value == "SI")
                || (fiapic10.value == "SI" && fiapic17.value == "SI")) && (fiapic25.value == "SI" || fiapic12.value == "SI"
                    || fiapic19.value == "SI" || fiapic7.value == "SI" || fiapic31.value == "SI" || fiapic36.value == "1"
                    || fiapic32.value == "SI")) {
                dengue = "DENGUE GRAVE\nA) Referir URGENTEMENTE a Unidad de Cuidado Intensivo según las normas de estabilización y transporte REFIERA\nB) Iniciar hidratación IV según el plan de hidratación del paciente con dengue\nC) Administrar oxígeno\nD) Tratar la fiebre y el dolor con Acetaminofén\nE) Notificación inmediata\n";
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c42",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.fiapi.c43",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c46",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c42td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.fiapi.c43td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c46td").style.backgroundColor = 'white';
                fiapic42.value = "SI";
                fiapic42.checked = true;
                fiapic43.value = "NO";
                fiapic43.checked = false;
                fiapic46.value = "NO";
                fiapic46.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c42', fiapic42.value), 600);
                setTimeout(guardarDatosPlantilla('fiapi', 'c43', fiapic43.value), 700);
                setTimeout(guardarDatosPlantilla('fiapi', 'c46', fiapic46.value), 800);
            }
            else if (fiapic27.value == "SI" && fiapic5.value == "SI" && ((fiapic16.value == "SI" && fiapic22.value == "SI")
                || (fiapic16.value == "SI" && fiapic23.value == "SI") || (fiapic16.value == "SI" && fiapic10.value == "SI")
                || (fiapic16.value == "SI" && fiapic17.value == "SI") || (fiapic22.value == "SI" && fiapic23.value == "SI")
                || (fiapic22.value == "SI" && fiapic10.value == "SI") || (fiapic22.value == "SI" && fiapic17.value == "SI")
                || (fiapic23.value == "SI" && fiapic10.value == "SI") || (fiapic23.value == "SI" && fiapic17.value == "SI")
                || (fiapic10.value == "SI" && fiapic17.value == "SI")) && (fiapic15.value == "SI" || fiapic11.value == "SI"
                    || fiapic18.value == "SI" || fiapic6.value == "SI" || fiapic28.value == "SI" || fiapic29.value == "SI"
                    || fiapic33.value == "2" || fiapic37.value == "1" || fiapic35.value == "1" || valorAtributo('lblEdadPaciente') < 5)) {
                dengue = "DENGUE CON SIGNOS DE ALARMA\nA) Dejar en observación o Referir para hospitalización y tratamiento según las normas de estabilización y transporte REFIERA\nB) Hidratación IV según el plan de hidratación del niño con dengue\nC) Dar abundantes líquidos orales\nD) Tratar la fiebre y el dolor con Acetaminofén\nE) Reposo en cama\nF) Notificación inmediata\nG) Monitoreo de signos vitales\nH) Monitoreo de gasto urinario\nI) Control de laboratorios según esquema de seguimiento del paciente con Dengue y signos de alarma\n";
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c42",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c43",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.fiapi.c46",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c42td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c43td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.fiapi.c46td").style.backgroundColor = 'white';
                fiapic42.value = "NO";
                fiapic42.checked = false;
                fiapic43.value = "SI";
                fiapic43.checked = true;
                fiapic46.value = "NO";
                fiapic46.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c42', fiapic42.value), 600);
                setTimeout(guardarDatosPlantilla('fiapi', 'c43', fiapic43.value), 700);
                setTimeout(guardarDatosPlantilla('fiapi', 'c46', fiapic46.value), 800);
            }
            else if (fiapic27.value == "SI" && fiapic5.value == "SI" && ((fiapic16.value == "SI" && fiapic22.value == "SI")
                || (fiapic16.value == "SI" && fiapic23.value == "SI") || (fiapic16.value == "SI" && fiapic10.value == "SI")
                || (fiapic16.value == "SI" && fiapic17.value == "SI") || (fiapic22.value == "SI" && fiapic23.value == "SI")
                || (fiapic22.value == "SI" && fiapic10.value == "SI") || (fiapic22.value == "SI" && fiapic17.value == "SI")
                || (fiapic23.value == "SI" && fiapic10.value == "SI") || (fiapic23.value == "SI" && fiapic17.value == "SI")
                || (fiapic10.value == "SI" && fiapic17.value == "SI"))) {
                dengue = "PROBABLE DENGUE\nA) Dar abundantes líquidos por vía oral, según plan B de hidratación\nB) Reposo en cama\nC) Tratar la fiebre y el dolor con Acetaminofén\nD) Control en el servicio cada 24 horas hasta 48 horas después de la caída de la fiebre\nE) Enseñar signos de alarma para consultar de inmediato\nF) Hemograma inicial y de control al caer la fiebre\nG) Enseñar medidas preventivas específicas\nH) Notificar según indicación de vigilancia en Salud Pública\n";
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c42",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c43",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c46",
                        color: "#ffcccc"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c42td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c43td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c46td").style.backgroundColor = '#ccffcc';
                fiapic42.value = "NO";
                fiapic42.checked = false;
                fiapic43.value = "NO";
                fiapic43.checked = false;
                fiapic46.value = "SI";
                fiapic46.checked = true;
                setTimeout(guardarDatosPlantilla('fiapi', 'c42', fiapic42.value), 600);
                setTimeout(guardarDatosPlantilla('fiapi', 'c43', fiapic43.value), 700);
                setTimeout(guardarDatosPlantilla('fiapi', 'c46', fiapic46.value), 800);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c42",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c43",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.fiapi.c46",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.fiapi.c42td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c43td").style.backgroundColor = 'white';
                // document.getElementById("formulario.fiapi.c46td").style.backgroundColor = 'white';
                fiapic42.value = "NO";
                fiapic42.checked = false;
                fiapic43.value = "NO";
                fiapic43.checked = false;
                fiapic46.value = "NO";
                fiapic46.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c42', fiapic42.value), 600);
                setTimeout(guardarDatosPlantilla('fiapi', 'c43', fiapic43.value), 700);
                setTimeout(guardarDatosPlantilla('fiapi', 'c46', fiapic46.value), 800);
            }

            if (fiapic1.value == "SI") {
                riesgobajo = "ENFERMEDAD FEBRIL DE RIESGO BAJO\nA) Tratar la fiebre\nB) Asegurar adecuada hidratación por vía oral\nC) Hacer consulta de seguimiento en 2 días si persiste la fiebre\nD) Enseñar a la madre los signos de alarma para regresar de inmediato\nE) Enseñar medidas preventivas específicas\n";
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c47",
                        color: "green"
                    },
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.fiapi.c47td").style.backgroundColor = 'green';
                fiapic47.value = "SI";
                fiapic47.checked = true;
                setTimeout(guardarDatosPlantilla('fiapi', 'c47', fiapic47.value), 900);
            }
            else {
                riesgobajo = "NO TIENE FIEBRE"
                let data = [
                    {
                        campoDestino:"formulario.fiapi.c47",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.fiapi.c47td").style.backgroundColor = 'white';
                fiapic47.value = "NO";
                fiapic47.checked = false;
                setTimeout(guardarDatosPlantilla('fiapi', 'c47', fiapic47.value), 900);
            }

            texto = febril + malaria + dengue + riesgobajo;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('fiapi', 'c48', value.value), 1000);

            break;

        case 'formulario.oido_garganta.c14': //OIDO 2 MESES 5 AÑOS
            var oidoc1 = document.getElementById("formulario.oido_garganta.c1");
            var oidoc2 = document.getElementById("formulario.oido_garganta.c2");
            var oidoc3 = document.getElementById("formulario.oido_garganta.c3");
            var oidoc4 = document.getElementById("formulario.oido_garganta.c4");
            var oidoc5 = document.getElementById("formulario.oido_garganta.c5");
            var oidoc6 = document.getElementById("formulario.oido_garganta.c6");
            var oidoc7 = document.getElementById("formulario.oido_garganta.c7");
            var oidoc8 = document.getElementById("formulario.oido_garganta.c8");
            var oidoc9 = document.getElementById("formulario.oido_garganta.c9");
            var oidoc10 = document.getElementById("formulario.oido_garganta.c10");
            var oidoc11 = document.getElementById("formulario.oido_garganta.c11");
            var oidoc12 = document.getElementById("formulario.oido_garganta.c12");
            var oidoc13 = document.getElementById("formulario.oido_garganta.c13");

            matoiditis = "";
            otitis = "";

            if (oidoc1.value == "SI" && oidoc6.value == "SI") {
                matoiditis = "MASTOIDITIS\nA) Referir URGENTEMENTE al hospital siguiendo las normas de estabilización y transporte REFIERA\nB) Administrar la primera dosis de un antibiótico apropiado\nC) Tratar la fiebre y el dolor\n";
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c9",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c13",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c9td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.oido_garganta.c13td").style.backgroundColor = 'white';
                oidoc9.value = "SI";
                oidoc9.checked = true;
                oidoc13.value = "NO";
                oidoc13.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c9', oidoc9.value), 100);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c13', oidoc13.value), 200);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c9",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c13",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c9td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c13td").style.backgroundColor = 'white';
                oidoc9.value = "NO";
                oidoc9.checked = false;
                oidoc13.value = "NO";
                oidoc13.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c9', oidoc9.value), 100);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c13', oidoc13.value), 200);
            }

            if (oidoc1.value == "SI" && oidoc3.value == "SI" && oidoc4.value >= 14) {
                otitis = "OTITIS MEDIA CRONICA\n A) Administrar un antibiótico tópico apropiado\nB) Secar el oído que supura con mecha\nC) Tratar la fiebre y el dolor\nD) Referir a consulta especializada\nE) Enseñar a la madre signos de alarma para volver de inmediato\nF) Enseñar medidas preventivas\nG) Hacer control 14 días después\nH) Sospeche inmunodeficiencia, realice prueba de VIH y remita a infectología\n";
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c10",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c12",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c13",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c10td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.oido_garganta.c11td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c12td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c13td").style.backgroundColor = 'white';
                oidoc10.value = "SI";
                oidoc10.checked = true;
                oidoc11.value = "NO";
                oidoc11.checked = false;
                oidoc12.value = "NO";
                oidoc12.checked = false;
                oidoc13.value = "NO";
                oidoc13.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c10', oidoc10.value), 300);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c11', oidoc11.value), 400);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c12', oidoc12.value), 500);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c13', oidoc13.value), 600);
            }
            else if (oidoc1.value == "SI" && oidoc5.value >= 3) {
                otitis = "OTITIS MEDIA RECURRENTE\n A) Administrar un antibiótico apropiado\nB) Secar el oído que supura con mecha\nC) Tratar la fiebre y el dolor\nD) Enseñar a la madre signos de alarma para regresar de inmediato\nE) Hacer consulta de seguimiento 2 días después\nF) Enseñar medidas preventivas\nG) Solicite prueba de VIH y remita a infectología\n";
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c10",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c12",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c13",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c10td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c11td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.oido_garganta.c12td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c13td").style.backgroundColor = 'white';
                oidoc10.value = "NO";
                oidoc10.checked = false;
                oidoc11.value = "SI";
                oidoc11.checked = true;
                oidoc12.value = "NO";
                oidoc12.checked = false;
                oidoc13.value = "NO";
                oidoc13.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c10', oidoc10.value), 500);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c11', oidoc11.value), 600);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c12', oidoc12.value), 700);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c13', oidoc13.value), 800);
            }
            else if (oidoc1.value == "SI" && (oidoc7.value == "SI" || oidoc2.value == "SI" || oidoc8.value == "SI")) {
                otitis = "OTITIS MEDIA AGUDA\n A) Administrar un antibiótico apropiado\nB) Secar el oído que supura con mecha\nC) Tratar la fiebre y el dolor\nD) Enseñar a la madre signos de alarma para regresar de inmediato\nE) Hacer consulta de seguimiento 2 días después\nF) Enseñar medidas preventivas\n";
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c10",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c12",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c13",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c10td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c11td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c12td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.oido_garganta.c13td").style.backgroundColor = 'white';
                oidoc10.value = "NO";
                oidoc10.checked = false;
                oidoc11.value = "NO";
                oidoc11.checked = false;
                oidoc12.value = "SI";
                oidoc12.checked = true;
                oidoc13.value = "NO";
                oidoc13.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c10', oidoc10.value), 500);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c11', oidoc11.value), 600);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c12', oidoc12.value), 700);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c13', oidoc13.value), 800);
            }
            else if (oidoc1.value == "SI") {
                otitis = "NO TIENE OTITIS MEDIA\nA) Ningún tratamiento adicional\nB) Enseñar a la madre signos de alarma para regresar de inmediato\n"
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c10",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c12",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c13",
                        color: "#ccffcc"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c10td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c11td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c12td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c13td").style.backgroundColor = '#ccffcc';
                oidoc10.value = "NO";
                oidoc10.checked = false;
                oidoc11.value = "NO";
                oidoc11.checked = false;
                oidoc12.value = "NO";
                oidoc12.checked = false;
                oidoc13.value = "SI";
                oidoc13.checked = true;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c10', oidoc10.value), 500);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c11', oidoc11.value), 600);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c12', oidoc12.value), 700);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c13', oidoc13.value), 800);
            }
            else {
                otitis = "NO TIENE PROBLEMA DE OIDO"
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c10",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c12",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c13",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c10td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c11td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c12td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c13td").style.backgroundColor = 'white';
                oidoc10.value = "NO";
                oidoc10.checked = false;
                oidoc11.value = "NO";
                oidoc11.checked = false;
                oidoc12.value = "NO";
                oidoc12.checked = false;
                oidoc13.value = "NO";
                oidoc13.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c10', oidoc10.value), 500);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c11', oidoc11.value), 600);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c12', oidoc12.value), 700);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c13', oidoc13.value), 800);
            }

            texto = matoiditis + otitis;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('oido_garganta', 'c14', value.value), 900);


            break;

        case 'formulario.oido_garganta.c23': // garganta 2 MESES 5 AÑOS
            var gargantac16 = document.getElementById("formulario.oido_garganta.c16")
            var gargantac17 = document.getElementById("formulario.oido_garganta.c17")
            var gargantac18 = document.getElementById("formulario.oido_garganta.c18")
            var gargantac19 = document.getElementById("formulario.oido_garganta.c19")
            var gargantac20 = document.getElementById("formulario.oido_garganta.c20")
            var gargantac21 = document.getElementById("formulario.oido_garganta.c21")
            var gargantac22 = document.getElementById("formulario.oido_garganta.c22")

            var faringo = "";
            console.log(parseInt(valorAtributo('lblEdadPaciente')))

            if (gargantac16.value == "SI" && gargantac17.value == "SI" && gargantac18.value == "SI" && gargantac19.value == "SI" && parseInt(valorAtributo('lblEdadPaciente')) >= 3) {
                faringo = "FARINGOAMIGDALITIS ESTREPTOCOCICA\n A) Aplicar dosis de P. Benzatínica\nB) Tratar la fiebre y el dolor\nC) Dar abundantes líquidos fríos\nD) Enseñar a la madre signos de alarma para regresar de inmediato\n";
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c20",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c22",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c20td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.oido_garganta.c21td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c22td").style.backgroundColor = 'white';
                gargantac20.value = "SI";
                gargantac20.checked = true;
                gargantac21.value = "NO";
                gargantac21.checked = false;
                gargantac22.value = "NO";
                gargantac22.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c20', gargantac20.value), 100);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c21', gargantac21.value), 200);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c22', gargantac22.value), 300);
            }
            else if (gargantac18.value == "SI" && gargantac16.value == "SI") {
                faringo = "FARINGOAMIGDALITIS VIRAL\n A) Tratar la fiebre y el dolor\nB) Dar abundantes líquidos fríos\nC) Enseñar a la madre signos de alarma para regresar de inmediato\n";
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c20",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "#ccffcc"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c22",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c20td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c21td").style.backgroundColor = '#ccffcc';
                // document.getElementById("formulario.oido_garganta.c22td").style.backgroundColor = 'white';
                gargantac20.value = "NO";
                gargantac20.checked = false;
                gargantac21.value = "SI";
                gargantac21.checked = true;
                gargantac22.value = "NO";
                gargantac22.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c20', gargantac20.value), 100);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c21', gargantac21.value), 200);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c22', gargantac22.value), 300);
            }
            else if (gargantac16.value == "SI") {
                faringo = "NO TIENE FARINGOAMIGDALITIS\n A) Dar abundantes líquidos\nB) Enseñar a la madre signos de alarma para regresar de inmediato\n";
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c20",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c22",
                        color: "#ccffcc"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c20td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c21td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c22td").style.backgroundColor = '#ccffcc';
                gargantac20.value = "NO";
                gargantac20.checked = false;
                gargantac21.value = "NO";
                gargantac21.checked = false;
                gargantac22.value = "SI";
                gargantac22.checked = true;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c20', gargantac20.value), 100);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c21', gargantac21.value), 200);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c22', gargantac22.value), 300);
            }
            else {
                faringo = "NO TIENE PROBLEMAS DE GARGANTA"
                let data = [
                    {
                        campoDestino:"formulario.oido_garganta.c20",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c11",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.oido_garganta.c22",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.oido_garganta.c20td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c21td").style.backgroundColor = 'white';
                // document.getElementById("formulario.oido_garganta.c22td").style.backgroundColor = 'white';
                gargantac20.value = "NO";
                gargantac20.checked = false;
                gargantac21.value = "NO";
                gargantac21.checked = false;
                gargantac22.value = "NO";
                gargantac22.checked = false;
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c20', gargantac20.value), 100);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c21', gargantac21.value), 200);
                setTimeout(guardarDatosPlantilla('oido_garganta', 'c22', gargantac22.value), 300);
            }

            texto = faringo;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('oido_garganta', 'c23', value.value), 400);

            break;

        case 'formulario.salud_bucal.c38': //saludbucal 2 MESES 5 AÑOS
            var bucalc1 = document.getElementById('formulario.salud_bucal.c1');
            var bucalc2 = document.getElementById('formulario.salud_bucal.c2');
            var bucalc3 = document.getElementById('formulario.salud_bucal.c3');
            var bucalc4 = document.getElementById('formulario.salud_bucal.c4');
            var bucalc5 = document.getElementById('formulario.salud_bucal.c5');
            var bucalc6 = document.getElementById('formulario.salud_bucal.c6');
            var bucalc7 = document.getElementById('formulario.salud_bucal.c7');
            var bucalc8 = document.getElementById('formulario.salud_bucal.c8');
            var bucalc9 = document.getElementById('formulario.salud_bucal.c9');
            var bucalc10 = document.getElementById('formulario.salud_bucal.c10');
            var bucalc11 = document.getElementById('formulario.salud_bucal.c11');
            var bucalc12 = document.getElementById('formulario.salud_bucal.c12');
            var bucalc13 = document.getElementById('formulario.salud_bucal.c13');
            var bucalc14 = document.getElementById('formulario.salud_bucal.c14');
            var bucalc15 = document.getElementById('formulario.salud_bucal.c15');
            var bucalc16 = document.getElementById('formulario.salud_bucal.c16');
            var bucalc17 = document.getElementById('formulario.salud_bucal.c17');
            var bucalc18 = document.getElementById('formulario.salud_bucal.c18');
            var bucalc19 = document.getElementById('formulario.salud_bucal.c19');
            var bucalc20 = document.getElementById('formulario.salud_bucal.c20');
            var bucalc21 = document.getElementById('formulario.salud_bucal.c21');
            var bucalc22 = document.getElementById('formulario.salud_bucal.c22');
            var bucalc23 = document.getElementById('formulario.salud_bucal.c23');
            var bucalc24 = document.getElementById('formulario.salud_bucal.c24');
            var bucalc25 = document.getElementById('formulario.salud_bucal.c25');
            var bucalc26 = document.getElementById('formulario.salud_bucal.c26');
            var bucalc27 = document.getElementById('formulario.salud_bucal.c27');
            var bucalc28 = document.getElementById('formulario.salud_bucal.c28');
            var bucalc29 = document.getElementById('formulario.salud_bucal.c29');
            var bucalc30 = document.getElementById('formulario.salud_bucal.c30');
            var bucalc31 = document.getElementById('formulario.salud_bucal.c31');
            var bucalc32 = document.getElementById('formulario.salud_bucal.c32');
            var bucalc33 = document.getElementById('formulario.salud_bucal.c33');
            var bucalc34 = document.getElementById('formulario.salud_bucal.c34');
            var bucalc35 = document.getElementById('formulario.salud_bucal.c35');
            var bucalc36 = document.getElementById('formulario.salud_bucal.c36');
            var bucalc37 = document.getElementById('formulario.salud_bucal.c37');

            var celulitis = "";
            var enfermedadbucal = "";
            var enfermedaddental = "";
            var trauma = "";
            var estomatits = "";
            var riesgo = "";

            if (bucalc6.value == "SI") {
                celulitis = "CELULITIS FACIAL\nA) Referir URGENTEMENTE según normas de estabilización y transporte REFIERA\nB) Inicie primera dosis de antibióticos\nC) Valoración para manejo por odontólogo\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c31",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c31td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = 'white';
                bucalc31.value = "SI";
                bucalc31.checked = true;
                bucalc37.value = "NO";
                bucalc37.checked = false;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c31', bucalc31.value), 100);
            }
            else {
                riesgo = "BAJO RIESGO ENFERMEDAD BUCAL\nA) Felicitar al niño y a los padres como FAMILIA PROTECTORA DE LA SALUD BUCAL\nB) Reforzamiento de hábitos adecuados de higiene bucal y adecuada alimentación\nC) Asegurar consulta de crecimiento y desarrollo\nD) Asegurar control por odontólogo cada 6 meses\nE) Enseñar medidas preventivas\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c31",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "#ccffcc"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c31td").style.backgroundColor = 'white';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = '#ccffcc';
                bucalc31.value = "NO";
                bucalc31.checked = false;
                bucalc37.value = "SI";
                bucalc37.checked = true;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c31', bucalc31.value), 100);
            }

            if (bucalc16.value == "SI" || bucalc7.value == "1" || bucalc18.value == "SI" || bucalc25.value == "SI" || bucalc3.value == "SI") {
                enfermedadbucal = "ENFERMEDADA BUCAL GRAVE\nA) Remitir URGENTEMENTE para tratamiento por odontología en las siguientes 24 horas\nB) Iniciar antibiótico oral si hay absceso, según recomendaciones\nC) Tratar el dolor\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c32",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c32td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = 'white';
                bucalc32.value = "SI";
                bucalc32.checked = true;
                bucalc37.value = "NO";
                bucalc37.checked = false;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c32', bucalc32.value), 200);
            }
            else {
                riesgo = "BAJO RIESGO ENFERMEDAD BUCAL\nA) Felicitar al niño y a los padres como FAMILIA PROTECTORA DE LA SALUD BUCAL\nB) Reforzamiento de hábitos adecuados de higiene bucal y adecuada alimentación\nC) Asegurar consulta de crecimiento y desarrollo\nD) Asegurar control por odontólogo cada 6 meses\nE) Enseñar medidas preventivas\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c32",
                        color: "#ccffcc"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c32td").style.backgroundColor = 'white';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = '#ccffcc';
                bucalc32.value = "NO";
                bucalc32.checked = false;
                bucalc37.value = "SI";
                bucalc37.checked = true;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c32', bucalc32.value), 200);
            }

            if (bucalc4.value == "SI" && (bucalc9.value == "SI" || bucalc20.value == "SI" || bucalc22.value == "SI" || bucalc23.value == "SI" || bucalc12.value == "SI" || bucalc11.value == "SI" || bucalc21.value == "SI")) {
                trauma = "TRAUMA BUCODENTAL\nA)Remitir URGENTEMENTE para tratamiento por odontología en el menor tiempo dentro de las siguientes 24 horas\nB) Descartar compromiso mayor\nC) Realizar manejo inicial de la lesión\nD) Tratar el dolor con Acetaminofén\nE) Dieta líquida\nF) Manejo inmediato de avulsión de diente permanente según recomendaciones y remisión inmediata a odontólogo\n"
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c33",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c33td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = 'white';
                bucalc33.value = "SI";
                bucalc33.checked = true;
                bucalc37.value = "NO";
                bucalc37.checked = false;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c33', bucalc33.value), 300);
            }
            else {
                riesgo = "BAJO RIESGO ENFERMEDAD BUCAL\nA) Felicitar al niño y a los padres como FAMILIA PROTECTORA DE LA SALUD BUCAL\nB) Reforzamiento de hábitos adecuados de higiene bucal y adecuada alimentación\nC) Asegurar consulta de crecimiento y desarrollo\nD) Asegurar control por odontólogo cada 6 meses\nE) Enseñar medidas preventivas\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c33",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "#ccffcc"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c33td").style.backgroundColor = 'white';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = '#ccffcc';
                bucalc33.value = "NO";
                bucalc33.checked = false;
                bucalc37.value = "SI";
                bucalc37.checked = true;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c33', bucalc33.value), 300);
            }

            if (bucalc8.value == "SI" || bucalc7.value == "2") {
                estomatits = "ESTOMATITIS\nA) Remitir a odontología\nB) Tratar la fiebre y el dolor con Acetaminofén\nC) Manejo tópico de las lesiones\nD) Dar abundantes líquidos fríos\nE) Según tipo de estomatitis tratar con antibioticoterapia o con nistatina\nF) Signos de alarma para regresar de inmediato\nG) Control en 2 días si no mejora la estomatitis\nH) Enseñanza, refuerzo y motivación para prácticas de higiene bucal y adecuada alimentación\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c34",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c34td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = 'white';
                bucalc34.value = "SI";
                bucalc34.checked = true;
                bucalc37.value = "NO";
                bucalc37.checked = false;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c34', bucalc34.value), 400);
            }
            else {
                riesgo = "BAJO RIESGO ENFERMEDAD BUCAL\nA) Felicitar al niño y a los padres como FAMILIA PROTECTORA DE LA SALUD BUCAL\nB) Reforzamiento de hábitos adecuados de higiene bucal y adecuada alimentación\nC) Asegurar consulta de crecimiento y desarrollo\nD) Asegurar control por odontólogo cada 6 meses\nE) Enseñar medidas preventivas\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c34",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "#ccffcc"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c34td").style.backgroundColor = 'white';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = '#ccffcc';
                bucalc34.value = "NO";
                bucalc34.checked = false;
                bucalc37.value = "SI";
                bucalc37.checked = true;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c34', bucalc34.value), 400);
            }

            if (bucalc7.value == "1" || bucalc14.value == "SI" || bucalc24.value == "SI") {
                enfermedaddental = "ENFERMEDAD DENTAL Y GINGIVAL\nA) Remitir a consulta odontológica en un período no mayor a 8 días\nB) Enseñanza, refuerzo y motivación para prácticas de higiene bucal y adecuada alimentación\n"
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c35",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c35td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = 'white';
                bucalc35.value = "SI";
                bucalc35.checked = true;
                bucalc37.value = "NO";
                bucalc37.checked = false;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c35', bucalc35.value), 500);
            }
            else {
                riesgo = "BAJO RIESGO ENFERMEDAD BUCAL\nA) Felicitar al niño y a los padres como FAMILIA PROTECTORA DE LA SALUD BUCAL\nB) Reforzamiento de hábitos adecuados de higiene bucal y adecuada alimentación\nC) Asegurar consulta de crecimiento y desarrollo\nD) Asegurar control por odontólogo cada 6 meses\nE) Enseñar medidas preventivas\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c35",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "#ccffcc"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c35td").style.backgroundColor = 'white';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = '#ccffcc';
                bucalc35.value = "NO";
                bucalc35.checked = false;
                bucalc37.value = "SI";
                bucalc37.checked = true;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c35', bucalc35.value), 500);
            }

            if (bucalc19.value == "SI" || bucalc27.value == "2" || bucalc26.value == "1" || bucalc26.value == "2" || bucalc26.value == "3" || bucalc29.value == "SI" || bucalc5.value == "SI") {
                enfermedaddental = "ALTO RIESGO DE ENFERMEDAD BUCAL\nA) Remitir a consulta odontológica en LOS SIGUIENTES 15 días\nB) Enseñanza, refuerzo y motivación para prácticas de higiene bucal y adecuada alimentación\nC) Control al mes para evaluar la corrección de hábitos\nD) Asegurar consulta de crecimiento y desarrollo\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c36",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c36td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = 'white';
                bucalc36.value = "SI";
                bucalc36.checked = true;
                bucalc37.value = "NO";
                bucalc37.checked = false;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c36', bucalc36.value), 600);
            }
            else {
                riesgo = "BAJO RIESGO ENFERMEDAD BUCAL\nA) Felicitar al niño y a los padres como FAMILIA PROTECTORA DE LA SALUD BUCAL\nB) Reforzamiento de hábitos adecuados de higiene bucal y adecuada alimentación\nC) Asegurar consulta de crecimiento y desarrollo\nD) Asegurar control por odontólogo cada 6 meses\nE) Enseñar medidas preventivas\n";
                let data = [
                    {
                        campoDestino:"formulario.salud_bucal.c36",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.salud_bucal.c37",
                        color: "#ccffcc"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.salud_bucal.c36td").style.backgroundColor = 'white';
                // document.getElementById("formulario.salud_bucal.c37td").style.backgroundColor = '#ccffcc';
                bucalc36.value = "NO";
                bucalc36.checked = false;
                bucalc37.value = "SI";
                bucalc37.checked = true;
                setTimeout(guardarDatosPlantilla('salud_bucal', 'c36', bucalc36.value), 600);
            }

            texto = celulitis + enfermedadbucal + enfermedaddental + trauma + estomatits + riesgo;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('salud_bucal', 'c37', bucalc37.value), 700);
            setTimeout(guardarDatosPlantilla('salud_bucal', 'c38', value.value), 800);

            break;

        case 'formulario.maltaiep.c73': //maltrato 2 MESES 5 AÑOS
            var maltratoc2 = document.getElementById('formulario.maltaiep.c2');
            var maltratoc3 = document.getElementById('formulario.maltaiep.c3');
            var maltratoc4 = document.getElementById('formulario.maltaiep.c4');
            var maltratoc5 = document.getElementById('formulario.maltaiep.c5');
            var maltratoc6 = document.getElementById('formulario.maltaiep.c6');
            var maltratoc7 = document.getElementById('formulario.maltaiep.c7');
            var maltratoc8 = document.getElementById('formulario.maltaiep.c8');
            var maltratoc9 = document.getElementById('formulario.maltaiep.c9');
            var maltratoc10 = document.getElementById('formulario.maltaiep.c10');
            var maltratoc11 = document.getElementById('formulario.maltaiep.c11');
            var maltratoc12 = document.getElementById('formulario.maltaiep.c12');
            var maltratoc13 = document.getElementById('formulario.maltaiep.c13');
            var maltratoc14 = document.getElementById('formulario.maltaiep.c14');
            var maltratoc15 = document.getElementById('formulario.maltaiep.c15');
            var maltratoc16 = document.getElementById('formulario.maltaiep.c16');
            var maltratoc17 = document.getElementById('formulario.maltaiep.c17');
            var maltratoc18 = document.getElementById('formulario.maltaiep.c18');
            var maltratoc19 = document.getElementById('formulario.maltaiep.c19');
            var maltratoc20 = document.getElementById('formulario.maltaiep.c20');
            var maltratoc21 = document.getElementById('formulario.maltaiep.c21');
            var maltratoc22 = document.getElementById('formulario.maltaiep.c22');
            var maltratoc23 = document.getElementById('formulario.maltaiep.c23');
            var maltratoc24 = document.getElementById('formulario.maltaiep.c24');
            var maltratoc25 = document.getElementById('formulario.maltaiep.c25');
            var maltratoc26 = document.getElementById('formulario.maltaiep.c26');
            var maltratoc27 = document.getElementById('formulario.maltaiep.c27');
            var maltratoc28 = document.getElementById('formulario.maltaiep.c28');
            var maltratoc29 = document.getElementById('formulario.maltaiep.c29');
            var maltratoc30 = document.getElementById('formulario.maltaiep.c30');
            var maltratoc31 = document.getElementById('formulario.maltaiep.c31');
            var maltratoc32 = document.getElementById('formulario.maltaiep.c32');
            var maltratoc33 = document.getElementById('formulario.maltaiep.c33');
            var maltratoc34 = document.getElementById('formulario.maltaiep.c34');
            var maltratoc35 = document.getElementById('formulario.maltaiep.c35');
            var maltratoc36 = document.getElementById('formulario.maltaiep.c36');
            var maltratoc37 = document.getElementById('formulario.maltaiep.c37');
            var maltratoc38 = document.getElementById('formulario.maltaiep.c38');
            var maltratoc39 = document.getElementById('formulario.maltaiep.c39');
            var maltratoc40 = document.getElementById('formulario.maltaiep.c40');
            var maltratoc41 = document.getElementById('formulario.maltaiep.c41');
            var maltratoc42 = document.getElementById('formulario.maltaiep.c42');
            var maltratoc43 = document.getElementById('formulario.maltaiep.c43');
            var maltratoc44 = document.getElementById('formulario.maltaiep.c44');
            var maltratoc45 = document.getElementById('formulario.maltaiep.c45');
            var maltratoc46 = document.getElementById('formulario.maltaiep.c46');
            var maltratoc47 = document.getElementById('formulario.maltaiep.c47');
            var maltratoc48 = document.getElementById('formulario.maltaiep.c48');
            var maltratoc49 = document.getElementById('formulario.maltaiep.c49');
            var maltratoc50 = document.getElementById('formulario.maltaiep.c50');
            var maltratoc51 = document.getElementById('formulario.maltaiep.c51');
            var maltratoc52 = document.getElementById('formulario.maltaiep.c52');
            var maltratoc53 = document.getElementById('formulario.maltaiep.c53');
            var maltratoc54 = document.getElementById('formulario.maltaiep.c54');
            var maltratoc55 = document.getElementById('formulario.maltaiep.c55');
            var maltratoc56 = document.getElementById('formulario.maltaiep.c56');
            var maltratoc57 = document.getElementById('formulario.maltaiep.c57');
            var maltratoc58 = document.getElementById('formulario.maltaiep.c58');
            var maltratoc59 = document.getElementById('formulario.maltaiep.c59');
            var maltratoc60 = document.getElementById('formulario.maltaiep.c60');
            var maltratoc61 = document.getElementById('formulario.maltaiep.c61');
            var maltratoc62 = document.getElementById('formulario.maltaiep.c62');
            var maltratoc63 = document.getElementById('formulario.maltaiep.c63');
            var maltratoc64 = document.getElementById('formulario.maltaiep.c64');
            var maltratoc65 = document.getElementById('formulario.maltaiep.c65');
            var maltratoc66 = document.getElementById('formulario.maltaiep.c66');
            var maltratoc67 = document.getElementById('formulario.maltaiep.c67');
            var maltratoc68 = document.getElementById('formulario.maltaiep.c68');
            var maltratoc69 = document.getElementById('formulario.maltaiep.c69');
            var maltratoc70 = document.getElementById('formulario.maltaiep.c70');
            var maltratoc71 = document.getElementById('formulario.maltaiep.c71');
            var maltratoc72 = document.getElementById('formulario.maltaiep.c72');

            fisico = "";
            abuso = "";
            abandono = "";
            no_maltrato = "";

            if (maltratoc2.value == "SI" || maltratoc5.value != "" || maltratoc6.value != "" || maltratoc8.value != ""
                || maltratoc10.value == "SI" || maltratoc11.value == "SI" || maltratoc12.value == "SI" || maltratoc13.value == "SI"
                || maltratoc16.value == "SI")//FISICO
            {
                fisico = "MALTRATO FISICO MUY GRAVE\n A) Referir URGENTEMENTE según normas de estabilización y transporte REFIERA\nB) Reanimacion y estabilizacion del paciente segun lesion\nC) Diagramas Corporales y Fotos de las lesiones\nD) Garantia de Seguridad por el sistema de proteccion\nE)Aviso a Justicia\n";
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c67",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c69",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c67td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.maltaiep.c69td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc67.value = "SI";
                maltratoc67.checked = true;
                maltratoc69.value = "NO";
                maltratoc69.checked = false;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c67', maltratoc67.value), 100);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c69', maltratoc69.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }
            else if (maltratoc15.value == "SI" || maltratoc39.value == "1" || maltratoc46.value != "") {
                fisico = "MALTRATO FISICO\n A) Corregir las prácticas de crianza inadecuadas\nB) Promover el Buen Trato y el respeto por los derechos de la niñez\nC) Escuela de padres\nD) Visita domiciliaria\nE) Informar al Sistema de Protección para acompañamiento y apoyo\nF) Enseñar cuando volver de inmediato\nG) Hacer control en 14 días\n";
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c67",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c69",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c67td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c69td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc67.value = "NO";
                maltratoc67.checked = false;
                maltratoc69.value = "SI";
                maltratoc69.checked = true;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c67', maltratoc67.value), 100);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c69', maltratoc69.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c67",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c69",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c67td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c69td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc67.value = "NO";
                maltratoc67.checked = false;
                maltratoc69.value = "NO";
                maltratoc69.checked = false;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c67', maltratoc67.value), 100);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c69', maltratoc69.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }

            if (maltratoc19.value == "SI" || maltratoc20.value == "SI" || maltratoc22.value == "SI" || maltratoc26.value == "SI"
                || maltratoc28.value == "SI" || maltratoc27.value == "SI" || maltratoc30.value == "SI" || maltratoc32.value == "SI"
                || maltratoc35.value == "SI" || maltratoc38.value == "SI" || maltratoc39.value == "2")//ABUSO SEXUAL
            {
                abuso = "ABUSO SEXUAL\nA) Remisión URGENTE según normas de estabilización y transporte REFIERA\nB) Tratar las lesiones traumáticas\nC) Profilaxis de infección\nD) Toma de evidencias forenses (si el caso tiene menos de 72 horas)\nE) Aviso inmediato a Justicia\nF) Garantía de seguridad por el Sistema de Protección\nG) Apoyo Psicológico, legal y manejo por grupo interdisciplinario. CAIVAS\n";
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c68",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c70",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c68td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.maltaiep.c70td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc68.value = "SI";
                maltratoc68.checked = true;
                maltratoc70.value = "NO";
                maltratoc70.checked = false;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c68', maltratoc68.value), 100);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c70', maltratoc70.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }
            else if (maltratoc33.value == "SI" || maltratoc36.value == "SI" || maltratoc37.value == "SI" || maltratoc21.value == "SI"
                || maltratoc34.value == "SI" || maltratoc54.value == "SI" || maltratoc56.value == "SI" || maltratoc50.value == "3" || maltratoc23.value == "SI") {
                abuso = "SOSPECHA DE ABUSO SEXUAL\nA) Evaluación por Pediatría para tratar lesiones\nB) Evaluación por Psicología y grupo interdisciplinario en las siguientes 24 horas. Si se confirma abuso, tratar según la clasificación ABUSO SEXUAL.\nC) Si no se puede confirmar o descartar el abuso, seguimiento mediante visitas domiciliarias\nD) Aseguramiento del niño en situación de calle\nE) Informar al Sistema de Protección\nF) Enseñar cuándo volver de inmediato\nG) Hacer control en 14 días\n";
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c68",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c70",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c68td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c70td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc68.value = "NO";
                maltratoc68.checked = false;
                maltratoc70.value = "SI";
                maltratoc70.checked = true;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c68', maltratoc68.value), 100);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c70', maltratoc70.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c68",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c70",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c68td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c70td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc68.value = "NO";
                maltratoc68.checked = false;
                maltratoc70.value = "NO";
                maltratoc70.checked = false;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c68', maltratoc68.value), 100);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c70', maltratoc70.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }

            if ((maltratoc39.value == "3" && maltratoc46.value != "") || (maltratoc39.value == "3" && maltratoc55.value == "SI") || (maltratoc39.value == "3" && maltratoc57.value == "SI")
                || (maltratoc39.value == "3" && maltratoc58.value == "SI") || (maltratoc39.value == "3" && maltratoc64.value == "SI") || (maltratoc39.value == "3" && maltratoc50.value != "") || (maltratoc39.value == "3" && maltratoc48.value == "SI")) {
                abandono = "MALTRATO EMOCIONAL NEGLIGENCIA O ABANDONO\n A) Promover los Derechos del niño\nB) Promover la Crianza Humanizada\nC) Escuela de Padres\nD) Abordaje de la violencia intrafamiliar\nE) Remisión del niño y de los cuidadores a Psicología y a grupo interdisciplinario\nF) Educar en salud y en el cuidado de niños\nG) Visitas domiciliarias\nH) Aseguramiento del niño en situación de calle\nI) Informar al sistema de Protección\nJ) Enseñar cuándo volver de inmediato\nK) Hacer control en 14 días\n";
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c71",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c71td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc71.value = "SI";
                maltratoc71.checked = true;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c71', maltratoc71.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c71",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c71td").style.backgroundColor = 'white';
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = 'white';
                maltratoc71.value = "NO";
                maltratoc71.checked = false;
                maltratoc72.value = "NO";
                maltratoc72.checked = false;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c71', maltratoc71.value), 200);
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 300);
            }

            if (maltratoc67.value == "NO" && maltratoc68.value == "NO" && maltratoc69.value == "NO" && maltratoc70.value == "NO" && maltratoc71.value == "NO") {
                no_maltrato = "NO HAY SOSPECHA DE MALTRATO\nA) Felicitar a la madre y dar pautas de Crianza Humanizada\nB) Asegurar la consulta de Crecimiento y Desarrollo\n"
                let data = [
                    {
                        campoDestino:"formulario.maltaiep.c72",
                        color: "#ccffcc"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.maltaiep.c72td").style.backgroundColor = '#ccffcc';
                maltratoc72.value = "SI";
                maltratoc72.checked = true;
                setTimeout(guardarDatosPlantilla('maltaiep', 'c72', maltratoc72.value), 400);
            }

            texto = fisico + abuso + abandono + no_maltrato;
            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('maltaiep', 'c73', value.value), 500);

            break;

        case 'formulario.crecimiento_anemia.c32': //crecimiento 2 MESES 5 AÑOS
            /**
             * Obtiene los valores de la plantilla de formulario.crecimiento_anemia
             */
            var crecimientoc1 = document.getElementById('formulario.crecimiento_anemia.c1');
            var crecimientoc2 = document.getElementById('formulario.crecimiento_anemia.c2');
            var crecimientoc4 = document.getElementById('formulario.crecimiento_anemia.c4');
            var crecimientoc9 = document.getElementById('formulario.crecimiento_anemia.c9');
            var crecimientoc11 = document.getElementById('formulario.crecimiento_anemia.c11');
            if (parseInt(valorAtributo('lblMesesEdadPaciente')) >= 6) {
                var crecimientoc12 = document.getElementById('formulario.crecimiento_anemia.c12');
            }
            else {
                var crecimientoc12 = ""
            }
            var crecimientoc13 = document.getElementById('formulario.crecimiento_anemia.c13');
            var crecimientoc14 = document.getElementById('formulario.crecimiento_anemia.c14');
            var crecimientoc15 = document.getElementById('formulario.crecimiento_anemia.c15');
            var crecimientoc16 = document.getElementById('formulario.crecimiento_anemia.c16');
            var crecimientoc17 = document.getElementById('formulario.crecimiento_anemia.c17');
            var crecimientoc18 = document.getElementById('formulario.crecimiento_anemia.c18');
            var crecimientoc19 = document.getElementById('formulario.crecimiento_anemia.c19');
            if (parseInt(valorAtributo('lblMesesEdadPaciente')) < 6) {
                var crecimientoc20 = document.getElementById('formulario.crecimiento_anemia.c20');
                var crecimientoc21 = document.getElementById('formulario.crecimiento_anemia.c21');
                var crecimientoc22 = document.getElementById('formulario.crecimiento_anemia.c22');
            }
            else {
                var crecimientoc20 = "";
                var crecimientoc21 = "";
                var crecimientoc22 = "";
            }
            var crecimientoc23 = document.getElementById('formulario.crecimiento_anemia.c23');
            var crecimientoc24 = document.getElementById('formulario.crecimiento_anemia.c24');
            var crecimientoc25 = document.getElementById('formulario.crecimiento_anemia.c25');
            var crecimientoc26 = document.getElementById('formulario.crecimiento_anemia.c26');
            var crecimientoc27 = document.getElementById('formulario.crecimiento_anemia.c27');
            var crecimientoc28 = document.getElementById('formulario.crecimiento_anemia.c28');
            var crecimientoc29 = document.getElementById('formulario.crecimiento_anemia.c29');

            var estado = "";


            /**
             * Se evalua de Lo Peor a lo Mejor Es Decir, de Rojo a Verde
             * 
             */

            /**
             * Verificacion si el niño es OBESO. Se evalua con la variable crecimientoc9 en plantilla sera Peso/Talla Desviacion estandar
             * Si Peso/Talla > 3 Tiene es Obeso
             */

            if (parseFloat(crecimientoc9.value) > 3) {
                estado = "OBESO\n A) Si hay una clasificacion amarilla para tos, diarrea o fiebre REFERIR para manejo en un hospital.\nB) REFERIR y asegurar consulta por pediatria y nutricion\nC) Dar Albendazol si es >1 a\u00F1o y no ha recibido en 6 meses\nD) Educacion alimentaria / alimentacion saludable\nE) Reforzar actividad fisica\nF) Evitar habitos sedentarios\nG) Ense\u00F1ar a la madre signos de alarma para volver de inmediato\nH) Consulta de control cada 14 dias por 3 meses\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "SI";
                crecimientoc23.checked = true;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "NO";
                crecimientoc26.checked = false;
                crecimientoc27.value = "NO";
                crecimientoc27.checked = false;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }
            /**
             * Se Evalua Si el niño tiene Desnutricion Aguda Severa.
             * Se evalua con las variables: 
             *      - crecimientoc9 en plantilla sera Peso/Talla Desviacion estandar             
             * 
             * Si Se Cumple uno de los Siguientes: 
             * Peso/Talla < -3                           
             *                           
             */
            else if (parseFloat(crecimientoc9.value) < -3) {
                estado = "DESNUTRICION AGUDA MODERADA O SEVERA CON ALGUNA COMPLICACION\nA) Referir URGENTEMENTE al hospital segun las normas de estabilizacion y transporte REFIERA\nB) Administrar vitamina A\nC) Dar primera dosis de un antibiotico apropiado\nD) Descartar infeccion para VIH\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "SI";
                crecimientoc26.checked = true;
                crecimientoc27.value = "NO";
                crecimientoc27.checked = false;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }
            /**
             * Se Evalua Si el niño tiene Desnutricion Aguda Moderada o Severa con complicaciones.
             * Se evalua con las variables: 
             *      - crecimientoc9 en plantilla sera Peso/Talla Desviacion estandar,
             *      - crecimientoc12 en plantilla sera Perimetro Braquial
             *      - lblMesesEdadPaciente que es la Edad del paciente en Meses
             *      - crecimientoc4 en plantilla sera Peso(Gramos)
             *      - crecimientoc2 en plantilla sera Edema Generlizado
             *      - crecimientoc13 en plantilla sera Hipotermia
             *      - crecimientoc14 en plantilla sera Fiebre
             *      - crecimientoc15 en plantilla sera Hemoglobina <4G/dl
             *      - crecimientoc16 en plantilla sera Piel con lesiones ulcerativas o extensas
             *      - crecimientoc17 en plantilla sera riesgo de deshidratacion
             *      - crecimientoc18 en plantilla sera prueba de apetito negativa
             *      - crecimientoc19 en plantilla sera frecuencia respiratoria aumentada para la edad
             *      - crecimientoc20 en plantilla sera Perdida Reciente de Peso (Menor de 6 Meses)
             *      - crecimientoc21 en plantilla sera Alimentacion Inefectiva (Menor de 6 Meses)
             *      - crecimientoc22 en plantilla sera Condicion Medica o Social del Niño (Menor de 6 Meses)
             * 
             * Si Se Cumple uno de los Siguientes: 
             * Peso/Talla < -2 
             * (Perimetro Braquial < 11.5 y Edad Mayor de 6 Meses) 
             * (Mayor de 6 Meses y Peso < 4000Gr)
             * Edema Generalizado = SI
             * 
             * Y Alguno de los siguientes:
             *
             * Hipotermia
             * Fiebre
             * Hemoglobina < 4G/dl
             * Piel con Lesiones 
             * Riesgo de Deshidratacion
             * Prueba de Apetito Negativa
             * Frecuencia respiratoria aumentada para la edad
             * Perdida Reciente de Peso y Edad Menor de 6 Meses
             * Alimentacion Inefectiva y Edad Menor de 6 Meses
             * Condicion Medica o Social del Niño y Menor de 6 Meses
             * 
             * 
             */
            else if ((parseFloat(crecimientoc9.value) < -2 || (parseFloat(crecimientoc12.value) < 11.5 && parseFloat(valorAtributo('lblMesesEdadPaciente')) >= 6)
                || (parseFloat(crecimientoc4.value) < 4000 && parseFloat(valorAtributo('lblMesesEdadPaciente')) >= 6) || crecimientoc2.value == "SI")
                && (crecimientoc13.value == "SI" || crecimientoc14.value == "SI" || crecimientoc15.value == "SI" || crecimientoc16.value == "SI"
                    || crecimientoc17.value == "SI" || crecimientoc18.value == "SI" || crecimientoc19.value == "SI" || crecimientoc20.value == "SI"
                    || crecimientoc21.value == "SI" || crecimientoc22.value == "SI")) {
                estado = "DESNUTRICION AGUDA MODERADA O SEVERA CON ALGUNA COMPLICACION\nA) Referir URGENTEMENTE al hospital segun las normas de estabilizacion y transporte REFIERA\nB) Administrar vitamina A\nC) Dar primera dosis de un antibiotico apropiado\nD) Descartar infeccion para VIH\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "SI";
                crecimientoc26.checked = true;
                crecimientoc27.value = "NO";
                crecimientoc27.checked = false;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);

            }
            /**
             * Se Evalua si el Niño tiene Sobrepeso.Se evalua con la variable crecimientoc9 en plantilla sera Peso/Talla Desviacion estandar
             * Si Peso/Talla >2 y <= 3 Entonces Tiene SobrePeso
             */
            else if (parseFloat(crecimientoc9.value) > 2 && parseFloat(crecimientoc9.value) <= 3) {
                estado = "SOBREPESO\n A) Dar Albendazol si es > 1 a\u00F1o y no ha recibido en 6 meses\nB) Educacion alimentaria / alimentacion saludable\nC) Reforzar actividad fisica\nD) Evitar habitos sedentarios\nE) Ense\u00F1ar a la madre signos de alarma para volver de inmediato\nF) Consulta de control cada 30 dias por 3 meses\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "SI";
                crecimientoc24.checked = true;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "NO";
                crecimientoc26.checked = false;
                crecimientoc27.value = "NO";
                crecimientoc27.checked = false;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }
            /**
             * Se evalua si el Niño tiene Desnutricion Aguda Moderada Sin Complicaciones.
             * Se evalua con la variables:
             *      - crecimientoc9 en plantilla sera Peso/Talla
             *      - crecimientoc2 en plantilla sera Edema Generalizado
             *      - crecimientoc1 en plantilla sera Alerta y Conciente
             *      - crecimientoc18 en plantilla sera Prueba de Apetito Positiva
             * 
             * Si se Cumplen Una de Las siguientes Condiciones:
             * Peso/Talla < -2
             * Edema Generalizado = SI
             * 
             * Y se cumple todo lo siguiente
             * Prueba de Apetito Positiva = SI
             * Alerta y Conciente = SI
             */
            else if ((parseFloat(crecimientoc9.value) <= -2 || crecimientoc2.value == "SI") && crecimientoc1.value == "SI" && crecimientoc18.value == "NO") {
                estado = "DESNUTRICION AGUDA MODERADA SIN COMPLICACIONES\nA) Si hay una clasificacion amarilla para tos, diarrea o fiebre REFERIR para manejo en un hospital\nB) REFERIR y asegurar consulta de pediatria y nutricion\nC) Dar vitamina A si no ha recibido en los ultimos 6 meses\nD) Administrar Zinc por 3 meses y micronutrientes\nE) Dar Albendazol si es > 1 a\u00F1o y no ha recibido en 6 meses\nF) Referir a un programa de recuperacion nutricional\nG) Ense\u00F1ar a la madre los signos de alarma para volver de inmediato\nH) Hacer consulta de control cada14 dias\nI) Descartar infeccion por VIH\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "NO";
                crecimientoc26.checked = false;
                crecimientoc27.value = "SI";
                crecimientoc27.checked = true;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }
            /**
             * Se evalua si el Niño tiene riesgo de Desnutricion.
             * Se evaulua con las Variables:
             *      - crecimientoc9 en plantilla Sera Peso/Talla DE
             *      - crecimientoc11 en plantilla sera Tendencia de Peso
             * 
             * Si se Cumple una de las siguientes condiciones
             * Peso/Talla >= -2 y < -1
             * Tendencia de Peso descendente U Horizontal
             */
            else if ((parseFloat(crecimientoc9.value) >= -2 && parseFloat(crecimientoc9.value) < -1) || (crecimientoc11.value == "2" || crecimientoc11.value == "3")) {
                estado = "RIESGO DE DESNUTRICION \nA) Dar vitamina A si no ha recibido en los ultimos 6 meses\nB) Dar Albendazol si es > 1 a\u00F1o y no ha recibido en 6 meses\nC) Administrar Zinc por 3 meses\nD) Evaluar la alimentacion y aconsejar a la madre sobre la alimentacion como se indica en el modulo ACONSEJAR\nE) Ense\u00F1ar a la madre signos de alarma para regresar de inmediato\nF) Consulta de seguimiento cada 30 dias\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "NO";
                crecimientoc26.checked = false;
                crecimientoc27.value = "NO";
                crecimientoc27.checked = false;
                crecimientoc28.value = "SI";
                crecimientoc28.checked = true;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }
            /**
            * Se evaulua si el Niño está en Riesgo de Sobrepeso. Se evalua con la variable crecimientoc9 en plantilla sera Peso/Talla Desviacion estandar
            * Si Peso/Talla DE >1 y <=2 Entonces el Niño esta en Riesgo de Sobrepeso
            */
            else if (parseFloat(crecimientoc9.value) > 1 && parseFloat(crecimientoc9.value) <= 2) {
                estado = "RIESGO DE SOBREPESO\nA) Dar micronutriente en polvo a ni\u00F1os de 6 a 23 meses segun esquema\nB)Dar Vitamina A y Hierro si no ha recibido en los ultimos 6 meses en ni\u00F1os\nde 2 a 5 a\u00F1os\nC) Educacion alimentaria / alimentacion saludable\nD) Reforzar actividad fisica\nE) Evitar habitos sedentarios\nF) Ense\u00F1ar a la madre signos de alarma para volver de inmediato\n G) Consulta de control segun la programacion de la consulta de valoracion integral.\n ";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "#ccffcc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = '#ccffcc';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "SI";
                crecimientoc25.checked = true;
                crecimientoc26.value = "NO";
                crecimientoc26.checked = false;
                crecimientoc27.value = "NO";
                crecimientoc27.checked = false;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }
            /**
            * Se evalua si el Niño tiene Desnutricion Aguda Moderada Sin Complicaciones.
            * Se evalua con la variables:
            *      - crecimientoc9 en plantilla sera Peso/Talla             
            * 
            * Si se Cumplen Una de Las siguientes Condiciones:
            * Peso/Talla >= -3 && Peso/Talla < -2
            * 
            */
            else if (parseFloat(crecimientoc9.value) >= -3 && parseFloat(crecimientoc9.value) < -2) {
                estado = "DESNUTRICION AGUDA MODERADA SIN COMPLICACIONES\nA) Si hay una clasificacion amarilla para tos, diarrea o fiebre REFERIR para manejo en un hospital\nB) REFERIR y asegurar consulta de pediatria y nutricion\nC) Dar vitamina A si no ha recibido en los ultimos 6 meses\nD) Administrar Zinc por 3 meses y micronutrientes\nE) Dar Albendazol si es > 1 a\u00F1o y no ha recibido en 6 meses\nF) Referir a un programa de recuperacion nutricional\nG) Ense\u00F1ar a la madre los signos de alarma para volver de inmediato\nH) Hacer consulta de control cada14 dias\nI) Descartar infeccion por VIH\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = 'white';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "NO";
                crecimientoc26.checked = false;
                crecimientoc27.value = "SI";
                crecimientoc27.checked = true;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "NO";
                crecimientoc29.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }
            /**
             * Si no se cumple Ninguna de las Anteriores Condiciones Se da Por Hecho que el Niño esta en Adecuado Estado Nutricional
             */
            else {
                estado = "ADECUADO ESTADO NUTRICIONAL \nA) Dar vitamina A si no ha recibido en los ultimos 6 meses\nB) Dar Albendazol si es > 1 a\u00F1o y no ha recibido en 6 meses\nC) Si el ni\u00F1o es < 2 a\u00F1os evaluar la alimentacion y aconsejar a la madre como se indica en el modulo ACONSEJAR\nD) Reforzar actividad fisica y evitar habitos sedentarios\nE) Asegurar consulta de crecimiento y desarrollo\nF) Ense\u00F1ar a la madre cuando volver de inmediato\nG) Felicite a la madre por los cuidados con su hijo\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c23",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c24",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c25",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c26",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c27",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c28",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c29",
                        color: "#ccffcc"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c23td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c24td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c25td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c26td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c27td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c28td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c29td").style.backgroundColor = '#ccffcc';
                crecimientoc23.value = "NO";
                crecimientoc23.checked = false;
                crecimientoc24.value = "NO";
                crecimientoc24.checked = false;
                crecimientoc25.value = "NO";
                crecimientoc25.checked = false;
                crecimientoc26.value = "NO";
                crecimientoc26.checked = false;
                crecimientoc27.value = "NO";
                crecimientoc27.checked = false;
                crecimientoc28.value = "NO";
                crecimientoc28.checked = false;
                crecimientoc29.value = "SI";
                crecimientoc29.checked = true;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c23', crecimientoc23.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c24', crecimientoc24.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c25', crecimientoc25.value), 300);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c26', crecimientoc26.value), 400);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c27', crecimientoc27.value), 500);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c28', crecimientoc28.value), 600);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c29', crecimientoc29.value), 700);
            }

            texto = estado;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c32', value.value), 1000);

            break;

        case 'formulario.crecimiento_anemia.c38':  //ANEMIA    2 MESES 5 AÑOS      
            var anemiac33 = document.getElementById('formulario.crecimiento_anemia.c33');
            var anemiac34 = document.getElementById('formulario.crecimiento_anemia.c34');
            var anemiac35 = document.getElementById('formulario.crecimiento_anemia.c35');
            var anemiac36 = document.getElementById('formulario.crecimiento_anemia.c36');
            var anemiac37 = document.getElementById('formulario.crecimiento_anemia.c37');

            anemia = "";

            if (anemiac33.value == "1" || anemiac34.value == "3") {
                anemia = "ANEMIA SEVERA\n A) Referir URGENTEMENTE al hospital según las normas de referencia y transporte REFIERA\nB) Si no hay causa clara o sin respuesta terapéutica recuerde descartar VIH\n"
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c35",
                        color: "#ffcccc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c36",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c37",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c35td").style.backgroundColor = '#ffcccc';
                // document.getElementById("formulario.crecimiento_anemia.c36td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c37td").style.backgroundColor = 'white';
                anemiac35.value = "SI";
                anemiac35.checked = true;
                anemiac36.value = "NO";
                anemiac36.checked = false;
                anemiac37.value = "NO";
                anemiac37.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c35', anemiac35.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c36', anemiac36.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c37', anemiac37.value), 300);
            }
            else if (anemiac33.value == "2" || anemiac34.value == "2") {
                anemia = "ANEMIA \n A) Dar hierro para tratamiento por 3 meses\nB) Aconsejar a la madre sobre alimentación adecuada\nC) Enseñar signos de alarma para regresar de inmediato\nD) Enseñar medidas preventivas especificas\nE) Control cada 14 dias en el servicio\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c35",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c36",
                        color: "#ffffcc"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c37",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c35td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c36td").style.backgroundColor = '#ffffcc';
                // document.getElementById("formulario.crecimiento_anemia.c37td").style.backgroundColor = 'white';
                anemiac35.value = "NO";
                anemiac35.checked = false;
                anemiac36.value = "SI";
                anemiac36.checked = true;
                anemiac37.value = "NO";
                anemiac37.checked = false;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c35', anemiac35.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c36', anemiac36.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c37', anemiac37.value), 300);
            }
            else {
                anemia = "NO TIENE ANEMIA SEVERA\nA) Dar hierro preventivo durante un mes, cada 6 meses, a partir de los 6 meses de edad.\nB) Enseñar a la madre cuándo volver de inmediato\nC) Asegurar consulta de crecimiento y desarrollo\nD) Felicitar a la madre\n";
                let data = [
                    {
                        campoDestino:"formulario.crecimiento_anemia.c35",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c36",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimiento_anemia.c37",
                        color: "#ccffcc"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimiento_anemia.c35td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c36td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimiento_anemia.c37td").style.backgroundColor = '#ccffcc';
                anemiac35.value = "NO";
                anemiac35.checked = false;
                anemiac36.value = "NO";
                anemiac36.checked = false;
                anemiac37.value = "SI";
                anemiac37.checked = true;
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c35', anemiac35.value), 100);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c36', anemiac36.value), 200);
                setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c37', anemiac37.value), 300);
            }

            texto = anemia;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('crecimiento_anemia', 'c38', value.value), 400);

            break;

        case 'formulario.eferm_grave_infec.c44': //ENFERMEDAD GRAVE MENOR 2 MESES
            var enferc1 = document.getElementById('formulario.eferm_grave_infec.c1');
            var enferc2 = document.getElementById('formulario.eferm_grave_infec.c2');
            var enferc3 = document.getElementById('formulario.eferm_grave_infec.c3');
            var enferc4 = document.getElementById('formulario.eferm_grave_infec.c4');
            var enferc5 = document.getElementById('formulario.eferm_grave_infec.c5');
            var enferc6 = document.getElementById('formulario.eferm_grave_infec.c6');
            var enferc7 = document.getElementById('formulario.eferm_grave_infec.c7');
            var enferc8 = document.getElementById('formulario.eferm_grave_infec.c8');
            var enferc9 = document.getElementById('formulario.eferm_grave_infec.c9');
            var enferc10 = document.getElementById('formulario.eferm_grave_infec.c10');
            var enferc11 = document.getElementById('formulario.eferm_grave_infec.c11');
            var enferc12 = document.getElementById('formulario.eferm_grave_infec.c12');
            var enferc13 = document.getElementById('formulario.eferm_grave_infec.c13');
            var enferc14 = document.getElementById('formulario.eferm_grave_infec.c14');
            var enferc15 = document.getElementById('formulario.eferm_grave_infec.c15');
            var enferc16 = document.getElementById('formulario.eferm_grave_infec.c16');
            var enferc17 = document.getElementById('formulario.eferm_grave_infec.c17');
            var enferc18 = document.getElementById('formulario.eferm_grave_infec.c18');
            var enferc19 = document.getElementById('formulario.eferm_grave_infec.c19');
            var enferc20 = document.getElementById('formulario.eferm_grave_infec.c20');
            var enferc21 = document.getElementById('formulario.eferm_grave_infec.c21');
            var enferc22 = document.getElementById('formulario.eferm_grave_infec.c22');
            var enferc23 = document.getElementById('formulario.eferm_grave_infec.c23');
            var enferc24 = document.getElementById('formulario.eferm_grave_infec.c24');
            var enferc25 = document.getElementById('formulario.eferm_grave_infec.c25');
            var enferc26 = document.getElementById('formulario.eferm_grave_infec.c26');
            var enferc27 = document.getElementById('formulario.eferm_grave_infec.c27');
            var enferc28 = document.getElementById('formulario.eferm_grave_infec.c28');
            var enferc29 = document.getElementById('formulario.eferm_grave_infec.c29');
            var enferc30 = document.getElementById('formulario.eferm_grave_infec.c30');
            var enferc31 = document.getElementById('formulario.eferm_grave_infec.c31');
            var enferc32 = document.getElementById('formulario.eferm_grave_infec.c32');
            var enferc33 = document.getElementById('formulario.eferm_grave_infec.c33');
            var enferc34 = document.getElementById('formulario.eferm_grave_infec.c34');
            var enferc35 = document.getElementById('formulario.eferm_grave_infec.c35');
            var enferc36 = document.getElementById('formulario.eferm_grave_infec.c36');
            var enferc37 = document.getElementById('formulario.eferm_grave_infec.c37');
            var enferc38 = document.getElementById('formulario.eferm_grave_infec.c38');
            var enferc39 = document.getElementById('formulario.eferm_grave_infec.c39');
            var enferc40 = document.getElementById('formulario.eferm_grave_infec.c40');
            var enferc41 = document.getElementById('formulario.eferm_grave_infec.c41');
            var enferc42 = document.getElementById('formulario.eferm_grave_infec.c42');
            var enferc43 = document.getElementById('formulario.eferm_grave_infec.c43');

            enfermedad = "";

            if (enferc1.value == 'SI' || enferc3.value == 'SI' || enferc7.value == 'SI' || enferc10.value == 'SI'
                || enferc22.value == 'SI' || enferc33.value == 'SI' || enferc21.value == 'SI' || enferc11.value == 'SI'
                || enferc12.value == 'SI' || enferc38.value == 'SI' || enferc37.value == 'SI' || enferc13.value == 'SI'
                || enferc36.value == 'SI' || enferc14.value == 'SI' || enferc26.value == 'SI' || enferc35.value == 'SI'
                || enferc25.value == 'SI' || enferc30.value == 'SI' || enferc15.value == 'SI' || enferc20.value == '1'
                || enferc34.value == 'SI' || enferc37.value == 'SI' || enferc28.value == 'SI' || enferc29.value == 'SI'
                || enferc39.value == 'SI' || enferc17.value == 'SI' || parseInt(enferc8.value) < 4 || enferc16.value == 'SI'
                || enferc40.value == 'SI') {
                enfermedad = "ENFERMEDAD GRAVE\nA) ACONSEJAR A LA MADRE PARA QUE SIGA DÁNDOLE LACTANCIA MATERNA EXCLUSIVA\nB) ENSEÑAR LOS SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nC) ENSEÑAR A LA MADRE MEDIDAS PREVENTIVAS\nD) VERIFICAR INMUNIZACIONES\nE) INCLUIR EN PROGRAMA DE CRECIMIENTO Y DESARROLLO\nF) REVISE RESULTADOS DE TAMIZAJE PRENATAL Y NEONATAL (SEROLOGiA: VIH, SÍFILIS Y TSH), SI NO LOS TIENE SOLICÍTELOS. SI ES POSITIVO REFIÉRALO\n";
                let data = [
                    {
                        campoDestino:"formulario.eferm_grave_infec.c41",
                        color: "red"
                    },
                    {
                        campoDestino:"formulario.eferm_grave_infec.c42",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.eferm_grave_infec.c43",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.eferm_grave_infec.c41td").style.backgroundColor = 'red';
                // document.getElementById("formulario.eferm_grave_infec.c42td").style.backgroundColor = 'white';
                // document.getElementById("formulario.eferm_grave_infec.c43td").style.backgroundColor = 'white';
                enferc41.value = "SI";
                enferc41.checked = true;
                enferc42.value = "NO";
                enferc42.checked = false;
                enferc43.value = "NO";
                enferc43.checked = false;
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c41', enferc41.value), 100);
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c42', enferc42.value), 200);
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c43', enferc43.value), 300);
            }
            else if (enferc18.value == 'SI' || enferc19.value == 'SI' || enferc31.value == 'SI' || enferc20.value == '2' || enferc27.value == 'SI') {
                enfermedad = "INFECCION LOCAL\nA) ADMINISTRAR UN ANTIBIÓTICO RECOMENDADO O NISTATINA SEGÚN CORRESPONDA\nB) CONTINUAR LACTANCIA MATERNA EXCLUSIVA\nC) ENSEÑAR A LA MADRE A TRATAR LAS INFECCIONES LOCALES\nD) ENSEÑAR A LA MADRE LAS MEDIDAS PREVENTIVAS\nE) ENSEÑAR A LA MADRE LOS SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nF) INCLUIR EN EL PROGRAMA DE CRECIMIENTO Y DESARROLLO\nG) HACER EL SEGUIMIENTO 2 DÍAS DESPUÉS\nH) REVISE RESULTADOS DE TAMIZAJE PRENATAL Y NEONATAL (SEROLOGÍA: VIH, SÍFILIS Y TSH), SI NO LOS TIENE SOLICÍTELOS. SI ES POSITIVO REFIÉRALO.\n";
                let data = [
                    {
                        campoDestino:"formulario.eferm_grave_infec.c41",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.eferm_grave_infec.c42",
                        color: "yellow"
                    },
                    {
                        campoDestino:"formulario.eferm_grave_infec.c43",
                        color: "white"
                    }                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.eferm_grave_infec.c41td").style.backgroundColor = 'white';
                // document.getElementById("formulario.eferm_grave_infec.c42td").style.backgroundColor = 'yellow';
                // document.getElementById("formulario.eferm_grave_infec.c43td").style.backgroundColor = 'white';
                enferc41.value = "NO";
                enferc41.checked = false;
                enferc42.value = "SI";
                enferc42.checked = true;
                enferc43.value = "NO";
                enferc43.checked = false;
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c41', enferc41.value), 100);
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c42', enferc42.value), 200);
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c43', enferc43.value), 300);
            }
            else {
                enfermedad = "NO TIENE ENEFERMEDAD MUY GRAVE NI INFECCION LOCAL\nA) ACONSEJAR A LA MADRE PARA QUE SIGA DÁNDOLE LACTANCIA MATERNA EXCLUSIVA\nB) ENSEÑAR LOS SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nC) ENSEÑAR A LA MADRE MEDIDAS PREVENTIVAS\nD) VERIFICAR INMUNIZACIONES\nE) INCLUIR EN PROGRAMA DE CRECIMIENTO Y DESARROLLO\nF) REVISE RESULTADOS DE TAMIZAJE PRENATAL Y NEONATAL (SEROLOGÍA: VIH, SÍFILIS Y TSH), SI NO LOS TIENE SOLICÍTELOS. SI ES POSITIVO REFIÉRALO\n";
                let data = [
                    {
                        campoDestino:"formulario.eferm_grave_infec.c41",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.eferm_grave_infec.c42",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.eferm_grave_infec.c43",
                        color: "green"
                    }                    
                ]
                changeColorAIEPI(data,version)
                document.getElementById("formulario.eferm_grave_infec.c41td").style.backgroundColor = 'white';
                document.getElementById("formulario.eferm_grave_infec.c42td").style.backgroundColor = 'white';
                document.getElementById("formulario.eferm_grave_infec.c43td").style.backgroundColor = 'green';
                enferc41.value = "NO";
                enferc41.checked = false;
                enferc42.value = "NO";
                enferc42.checked = false;
                enferc43.value = "SI";
                enferc43.checked = true;
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c41', enferc41.value), 100);
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c42', enferc42.value), 200);
                setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c43', enferc43.value), 300);
            }

            texto = enfermedad;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('eferm_grave_infec', 'c44', value.value), 400);


            break;

        case 'formulario.diarrea_saludm.c11': //DIARREA MENOR 2 MESES
            var diarreac1 = document.getElementById('formulario.diarrea_saludm.c1');
            var diarreac2 = document.getElementById('formulario.diarrea_saludm.c2');
            var diarreac3 = document.getElementById('formulario.diarrea_saludm.c3');
            var diarreac4 = document.getElementById('formulario.diarrea_saludm.c4');
            var diarreac5 = document.getElementById('formulario.diarrea_saludm.c5');
            var diarreac6 = document.getElementById('formulario.diarrea_saludm.c6');
            var diarreac7 = document.getElementById('formulario.diarrea_saludm.c7');
            var diarreac8 = document.getElementById('formulario.diarrea_saludm.c8');
            var diarreac9 = document.getElementById('formulario.diarrea_saludm.c9');
            var diarreac10 = document.getElementById('formulario.diarrea_saludm.c10');
            var diarreac11 = document.getElementById('formulario.diarrea_saludm.c11');

            diarrea = "";
            diarrea_pro = "";
            diarrea_sangre = "";

            if (diarreac1.value == 'SI' && (((diarreac4.value == '1' && diarreac6.value == 'SI') || (diarreac4.value == '2' && diarreac6.value == 'SI') || (diarreac4.value == '1' && diarreac5.value == '2') || (diarreac4.value == '1' && diarreac5.value == '3') || (diarreac4.value == '2' && diarreac5.value == '2') || (diarreac4.value == '2' && diarreac5.value == '3')))) {
                diarrea = "DESHIDRATACION\n A) REFERIR URGENTEMENTE AL HOSPITAL SEGÚN LAS NORMAS DE ESTABILIZACIÓN Y TRANSPORTE REFIERA\nB) DAR LÍQUIDOS PARA LA DESHIDRATACIÓN, INICIAR PLAN B O C SEGÚN CORRESPONDA\nC) ACONSEJAR A LA MADRE QUE CONTINÚE DANDO EL PECHO SI ES POSIBLE\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_saludm.c9",
                        color: "red"
                    },
                    {
                        campoDestino:"formulario.diarrea_saludm.c10",
                        color: "white"
                    }                       
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_saludm.c9td").style.backgroundColor = 'red';
                // document.getElementById("formulario.diarrea_saludm.c10td").style.backgroundColor = 'white';
                diarreac9.value = "SI";
                diarreac9.checked = true;
                diarreac10.value = "NO";
                diarreac10.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c9', diarreac9.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c10', diarreac10.value), 200);
            }
            else if (diarreac1.value == 'SI') {
                diarrea = "NO DESHIDRATACION\n  A) DAR LACTANCIA MATERNA EXCLUSIVA\nB) DAR RECOMENDACIONES PARA TRATAR LA DIARREA EN CASA (PLAN A), SIN INICIAR NINGÚN ALIMENTO\nC) SUPLEMENTACIÓN TERAPÉUTICA CON ZINC POR 14 DÍAS\nD) ENSEÑAR A LA MADRE SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nE) ENSEÑAR A LA MADRE LAS MEDIDAS PREVENTIVAS\nF) HACER SEGUIMIENTO 2 DÍAS DESPUÉS\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_saludm.c9",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.diarrea_saludm.c10",
                        color: "green"
                    }                       
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_saludm.c9td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_saludm.c10td").style.backgroundColor = 'green';
                diarreac9.value = "NO";
                diarreac9.checked = false;
                diarreac10.value = "SI";
                diarreac10.checked = true;
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c9', diarreac9.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c10', diarreac10.value), 200);
            }
            else {
                diarrea = "NO TIENE DIARREA"
                let data = [
                    {
                        campoDestino:"formulario.diarrea_saludm.c9",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.diarrea_saludm.c10",
                        color: "white"
                    }                       
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_saludm.c9td").style.backgroundColor = 'white';
                // document.getElementById("formulario.diarrea_saludm.c10td").style.backgroundColor = 'white';
                diarreac9.value = "NO";
                diarreac9.checked = false;
                diarreac10.value = "NO";
                diarreac10.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c9', diarreac9.value), 100);
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c10', diarreac10.value), 200);
            }

            if (diarreac1.value == 'SI' && parseInt(diarreac2.value) > 6) {
                diarrea_pro = "DIARREA PROLONGADA\n A) REFERIR URGENTEMENTE AL HOSPITAL, SEGÚN LAS NORMAS DE ESTABILIZACIÓN Y TRANSPORTE REFIERA\nB) RECOMENDAR A LA MADRE QUE CONTINÚE DÁNDOLE LACTANCIA MATERNA\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_saludm.c8",
                        color: "red"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_saludm.c8td").style.backgroundColor = 'red';
                diarreac8.value = "SI";
                diarreac8.checked = true;
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c8', diarreac8.value), 100);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.diarrea_saludm.c8",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.diarrea_saludm.c8td").style.backgroundColor = 'white';
                diarreac8.value = "NO";
                diarreac8.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c8', diarreac8.value), 100);
            }

            if (diarreac1.value == 'SI' && diarreac3.value == 'SI') {
                diarrea_sangre = "DIARREA CON SANGRE \n  A) REFERIR URGENTEMENTE A UN HOSPITAL, SEGÚN LAS NORMAS DE ESTABILIZACIÓN Y TRANSPORTE REFIERA\nB) ADMINISTRAR DOSIS DE VITAMINA K\nC) ADMINISTRAR PRIMERA DOSIS DE LOS ANTIBIÓTICOS RECOMENDADOS\nD) RECOMENDAR A LA MADRE QUE CONTINÚE DÁNDOLE LACTANCIA MATERNA\n";
                let data = [
                    {
                        campoDestino:"formulario.diarrea_saludm.c7",
                        color: "red"
                    },
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.diarrea_saludm.c7td").style.backgroundColor = 'red';
                diarreac7.value = "SI";
                diarreac7.checked = true;
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c7', diarreac7.value), 100);
            }
            else {
                let data = [
                    {
                        campoDestino:"formulario.diarrea_saludm.c7",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.diarrea_saludm.c7td").style.backgroundColor = 'white';
                diarreac7.value = "NO";
                diarreac7.checked = false;
                setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c7', diarreac7.value), 100);
            }

            texto = diarrea + diarrea_pro + diarrea_sangre;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('diarrea_saludm', 'c11', value.value), 400);
            break;

        case 'formulario.crecimien_prac_alim.c50': //CRECIMIENTO MENOR 2 MESES
            var crecic1 = document.getElementById('formulario.crecimien_prac_alim.c1');
            var crecic2 = document.getElementById('formulario.crecimien_prac_alim.c2');
            var crecic3 = document.getElementById('formulario.crecimien_prac_alim.c3');
            var crecic4 = document.getElementById('formulario.crecimien_prac_alim.c4');
            var crecic5 = document.getElementById('formulario.crecimien_prac_alim.c5');
            var crecic6 = document.getElementById('formulario.crecimien_prac_alim.c6');
            var crecic7 = document.getElementById('formulario.crecimien_prac_alim.c7');
            var crecic8 = document.getElementById('formulario.crecimien_prac_alim.c8');
            var crecic9 = document.getElementById('formulario.crecimien_prac_alim.c9');
            var crecic10 = document.getElementById('formulario.crecimien_prac_alim.c10');
            var crecic11 = document.getElementById('formulario.crecimien_prac_alim.c11');
            var crecic12 = document.getElementById('formulario.crecimien_prac_alim.c12');
            var crecic13 = document.getElementById('formulario.crecimien_prac_alim.c13');
            var crecic14 = document.getElementById('formulario.crecimien_prac_alim.c14');
            var crecic15 = document.getElementById('formulario.crecimien_prac_alim.c15');
            var crecic16 = document.getElementById('formulario.crecimien_prac_alim.c16');
            var crecic17 = document.getElementById('formulario.crecimien_prac_alim.c17');
            var crecic18 = document.getElementById('formulario.crecimien_prac_alim.c18');
            var crecic19 = document.getElementById('formulario.crecimien_prac_alim.c19');
            var crecic20 = document.getElementById('formulario.crecimien_prac_alim.c20');
            var crecic21 = document.getElementById('formulario.crecimien_prac_alim.c21');
            var crecic22 = document.getElementById('formulario.crecimien_prac_alim.c22');
            var crecic23 = document.getElementById('formulario.crecimien_prac_alim.c23');
            var crecic24 = document.getElementById('formulario.crecimien_prac_alim.c24');
            var crecic25 = document.getElementById('formulario.crecimien_prac_alim.c25');
            var crecic26 = document.getElementById('formulario.crecimien_prac_alim.c26');
            var crecic27 = document.getElementById('formulario.crecimien_prac_alim.c27');
            var crecic28 = document.getElementById('formulario.crecimien_prac_alim.c28');
            var crecic29 = document.getElementById('formulario.crecimien_prac_alim.c29');
            var crecic30 = document.getElementById('formulario.crecimien_prac_alim.c30');
            var crecic31 = document.getElementById('formulario.crecimien_prac_alim.c31');
            var crecic32 = document.getElementById('formulario.crecimien_prac_alim.c32');
            var crecic33 = document.getElementById('formulario.crecimien_prac_alim.c33');
            var crecic34 = document.getElementById('formulario.crecimien_prac_alim.c34');
            var crecic35 = document.getElementById('formulario.crecimien_prac_alim.c35');
            var crecic36 = document.getElementById('formulario.crecimien_prac_alim.c36');
            var crecic37 = document.getElementById('formulario.crecimien_prac_alim.c37');
            var crecic38 = document.getElementById('formulario.crecimien_prac_alim.c38');
            var crecic39 = document.getElementById('formulario.crecimien_prac_alim.c39');
            var crecic40 = document.getElementById('formulario.crecimien_prac_alim.c40');
            var crecic41 = document.getElementById('formulario.crecimien_prac_alim.c41');
            var crecic42 = document.getElementById('formulario.crecimien_prac_alim.c42');
            var crecic43 = document.getElementById('formulario.crecimien_prac_alim.c43');
            var crecic44 = document.getElementById('formulario.crecimien_prac_alim.c44');
            var crecic45 = document.getElementById('formulario.crecimien_prac_alim.c45');
            var crecic46 = document.getElementById('formulario.crecimien_prac_alim.c46');
            var crecic47 = document.getElementById('formulario.crecimien_prac_alim.c47');
            var crecic48 = document.getElementById('formulario.crecimien_prac_alim.c48');
            var crecic49 = document.getElementById('formulario.crecimien_prac_alim.c49');


            crecimiento = ""
            if ((parseFloat(crecic33.value) < -2 || crecic15.value == 'SI' || crecic16.value == 'SI') && (crecic17.value == 'SI'
                || crecic18.value == 'SI' || crecic19.value == 'SI' || crecic20.value == 'SI' || crecic21.value == 'SI' || crecic22.value == 'SI'
                || crecic23.value == 'SI' || crecic24.value == 'SI' || crecic25.value == 'SI' || crecic26.value == 'SI' || crecic27.value == 'SI')) {
                crecimiento = "DESNUTRCION AGUDA MODERADA O SEVERA DEL MENOR\nA)Referir URGENTEMENTE al hospital siguiendo las normas de estabilización “REFIERA” para niños con desnutrición aguda severa o moderada con complicaciones considerando que se trata de una URGENCIA VITAL\nB)Continúe con lactancia materna\nC)Trate la hipoglicemia\nD)Trate y prevenga la hipotermia\nE)Prevenga y trate la deshidratación especialmente si hay diarrea\F)Dar primera dosis de antibiótico apropiado según la infección\nG)Administre Acido fólico\nH)Tratar la anemia grave si cuenta con globulos rojos empaquetados\nI)Debe estar alerta sobre la posibilidad de Síndrome de Realimentación y de Sobrehidratación al iniciar el manejo nutricional.\nJ)Dar dosis de vacunas que requiera según esquema.\n"
                let data = [
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c45",
                        color: "red"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c46",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c47",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c48",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c49",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                //document.getElementById("formulario.crecimien_prac_alim.c45td").style.backgroundColor = 'red';
                //document.getElementById("formulario.crecimien_prac_alim.c46td").style.backgroundColor = 'white';
                //document.getElementById("formulario.crecimien_prac_alim.c47td").style.backgroundColor = 'white';
                //document.getElementById("formulario.crecimien_prac_alim.c48td").style.backgroundColor = 'white';
                //document.getElementById("formulario.crecimien_prac_alim.c49td").style.backgroundColor = 'white';
                crecic45.value = "SI";
                crecic45.checked = true;
                crecic46.value = "NO";
                crecic46.checked = false;
                crecic47.value = "NO";
                crecic47.checked = false;
                crecic48.value = "NO";
                crecic48.checked = false;
                crecic49.value = "NO";
                crecic49.checked = false;
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c45', crecic45.value), 100);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c46', crecic46.value), 200);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c47', crecic47.value), 300);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c48', crecic48.value), 400);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c49', crecic49.value), 500);
            }
            else if (crecic28.value == 'NO' || crecic44.value == '1' || parseInt(crecic34.value) > 10 || crecic35.value == '2') {
                crecimiento = "PROBLEMA SEVERO DE ALIMENTACION\nA)Referir URGENTEMENTE al hospital siguiendo las normas de estabilización “REFIERA” para niños con desnutrición aguda severa o moderada con complicaciones considerando que se trata de una URGENCIA VITAL\nB)Continúe con lactancia materna\nC)Trate la hipoglicemia\nD)Trate y prevenga la hipotermia\nE)Prevenga y trate la deshidratación especialmente si hay diarrea\F)Dar primera dosis de antibiótico apropiado según la infección\nG)Administre Acido fólico\nH)Tratar la anemia grave si cuenta con globulos rojos empaquetados\nI)Debe estar alerta sobre la posibilidad de Síndrome de Realimentación y de Sobrehidratación al iniciar el manejo nutricional.\nJ)Dar dosis de vacunas que requiera según esquema.\n"
                let data = [
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c45",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c46",
                        color: "red"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c47",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c48",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c49",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimien_prac_alim.c45td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c46td").style.backgroundColor = 'red';
                // document.getElementById("formulario.crecimien_prac_alim.c47td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c48td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c49td").style.backgroundColor = 'white';
                crecic45.value = "NO";
                crecic45.checked = false;
                crecic46.value = "SI";
                crecic46.checked = true;
                crecic47.value = "NO";
                crecic47.checked = false;
                crecic48.value = "NO";
                crecic48.checked = false;
                crecic49.value = "NO";
                crecic49.checked = false;
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c45', crecic45.value), 100);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c46', crecic46.value), 200);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c47', crecic47.value), 300);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c48', crecic48.value), 400);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c49', crecic49.value), 500);
            }
            else if ((parseFloat(crecic33.value) >= -2 && parseFloat(crecic33.value) < -1)) {
                crecimiento = "EN RIESGO DE DESNUTRICION\nA) ACONSEJAR A LA MADRE QUE LE DÉ EL PECHO LAS VECES Y EL TIEMPO QUE EL NIÑO QUIERA DE DÍA Y DE NOCHE, MÍNIMO 8 VECES AL DÍA\nB) SI EL NIÑO TIENE AGARRE DEFICIENTE O NO MAMA BIEN, ENSEÑAR A LA MADRE LA POSICIÓN Y EL AGARRE CORRECTOS\nC) SI RECIBE OTROS ALIMENTOS O LÍQUIDOS: ACONSEJAR A LA MADRE QUE LE DÉ EL PECHO MÁS VECES, REDUCIENDO LOS OTROS ALIMENTOS O LÍQUIDOS HASTA ELIMINARLOS Y QUE NO USE BIBERÓN\nD) SI LA MADRE PRESENTA MOLESTIAS EN LAS MAMAS, TRATARLA\nSI EL NIÑO NO SE ALIMENTA AL PECHO:\nA) REFERIR PARA ASESORAMIENTO SOBRE LACTANCIA MATERNA\nB) INICIAR UN SUPLEMENTO VITAMÍNICO RECOMENDADO\nC) EN CASO NECESARIO ENSEÑAR A PREPARAR UNA FÓRMULA Y A USAR UNA TAZA\nEN TODOS LOS NIÑOS:\nA) HACER EL SEGUIMIENTO PARA PROBLEMA DE ALIMENTACIÓN 2 DÍAS DESPUÉS\nB) HACER EL SEGUIMIENTO DE PESO 7 DÍAS DESPUÉS\nC) ENSEÑAR A LA MADRE SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nD) REMITIR A CONSULTA DE PEDIATRÍA\nE) INCLUIR EN CONSULTA DE CRECIMIENTO Y DESARROLLO\nF) ENSEÑAR MEDIDAS PREVENTIVAS ESPECÍFICAS\n"
                let data = [
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c45",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c46",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c47",
                        color: "yellow"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c48",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c49",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimien_prac_alim.c45td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c46td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c47td").style.backgroundColor = 'yellow';
                // document.getElementById("formulario.crecimien_prac_alim.c48td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c49td").style.backgroundColor = 'white';
                crecic45.value = "NO";
                crecic45.checked = false;
                crecic46.value = "NO";
                crecic46.checked = false;
                crecic47.value = "SI";
                crecic47.checked = true;
                crecic48.value = "NO";
                crecic48.checked = false;
                crecic49.value = "NO";
                crecic49.checked = false;
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c45', crecic45.value), 100);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c46', crecic46.value), 200);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c47', crecic47.value), 300);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c48', crecic48.value), 400);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c49', crecic49.value), 500);
            }
            else if (crecic35.value == '3' || parseFloat(crecic35.value) < 10 || crecic29.value == 'SI' || crecic44.value == '1' || parseInt(crecic8.value) < 8 || crecic9.value == 'SI') {
                crecimiento = "PROBLEMA DE ALIMENTACION\nA) ACONSEJAR A LA MADRE QUE LE DÉ EL PECHO LAS VECES Y EL TIEMPO QUE EL NIÑO QUIERA DE DÍA Y DE NOCHE, MÍNIMO 8 VECES AL DÍA\nB) SI EL NIÑO TIENE AGARRE DEFICIENTE O NO MAMA BIEN, ENSEÑAR A LA MADRE LA POSICIÓN Y EL AGARRE CORRECTOS\nC) SI RECIBE OTROS ALIMENTOS O LÍQUIDOS: ACONSEJAR A LA MADRE QUE LE DÉ EL PECHO MÁS VECES, REDUCIENDO LOS OTROS ALIMENTOS O LÍQUIDOS HASTA ELIMINARLOS Y QUE NO USE BIBERÓN\nD) SI LA MADRE PRESENTA MOLESTIAS EN LAS MAMAS, TRATARLA\nSI EL NIÑO NO SE ALIMENTA AL PECHO:\nA) REFERIR PARA ASESORAMIENTO SOBRE LACTANCIA MATERNA\nB) INICIAR UN SUPLEMENTO VITAMÍNICO RECOMENDADO\nC) EN CASO NECESARIO ENSEÑAR A PREPARAR UNA FÓRMULA Y A USAR UNA TAZA\nEN TODOS LOS NIÑOS:\nA) HACER EL SEGUIMIENTO PARA PROBLEMA DE ALIMENTACIÓN 2 DÍAS DESPUÉS\nB) HACER EL SEGUIMIENTO DE PESO 7 DÍAS DESPUÉS\nC) ENSEÑAR A LA MADRE SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nD) REMITIR A CONSULTA DE PEDIATRÍA\nE) INCLUIR EN CONSULTA DE CRECIMIENTO Y DESARROLLO\nF) ENSEÑAR MEDIDAS PREVENTIVAS ESPECÍFICAS\n"
                let data = [
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c45",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c46",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c47",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c48",
                        color: "yellow"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c49",
                        color: "white"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimien_prac_alim.c45td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c46td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c47td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c48td").style.backgroundColor = 'yellow';
                // document.getElementById("formulario.crecimien_prac_alim.c49td").style.backgroundColor = 'white';
                crecic45.value = "NO";
                crecic45.checked = false;
                crecic46.value = "NO";
                crecic46.checked = false;
                crecic47.value = "NO";
                crecic47.checked = false;
                crecic48.value = "SI";
                crecic48.checked = true;
                crecic49.value = "NO";
                crecic49.checked = false;
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c45', crecic45.value), 100);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c46', crecic46.value), 200);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c47', crecic47.value), 300);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c48', crecic48.value), 400);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c49', crecic49.value), 500);
            }
            else {
                crecimiento = "ADECUADAS PRÁCTICAS DE ALIMENTACIÓN Y PESO ADECUADO\n A) ENSEÑAR A LA MADRE LOS CUIDADOS DEL NIÑO EN EL HOGAR\nB) ELOGIAR A LA MADRE PORQUE LO ALIMENTA BIEN E INDICAR LA IMPORTANCIA DE LACTANCIA EXCLUSIVA POR 6 MESES\nC) ENSEÑAR SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nD) CONTROL DE PESO EN 30 DÍAS EN CONSULTA DE CRECIMIENTO Y DESARROLLO\nF) ENSEÑAR MEDIDAS PREVENTIVAS ESPECÍFICAS\n"
                let data = [
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c45",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c46",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c47",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c48",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.crecimien_prac_alim.c49",
                        color: "green"
                    },
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.crecimien_prac_alim.c45td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c46td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c47td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c48td").style.backgroundColor = 'white';
                // document.getElementById("formulario.crecimien_prac_alim.c49td").style.backgroundColor = 'green';
                crecic45.value = "NO";
                crecic45.checked = false;
                crecic46.value = "NO";
                crecic46.checked = false;
                crecic47.value = "NO";
                crecic47.checked = false;
                crecic48.value = "NO";
                crecic48.checked = false;
                crecic49.value = "SI";
                crecic49.checked = true;
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c45', crecic45.value), 100);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c46', crecic46.value), 200);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c47', crecic47.value), 300);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c48', crecic48.value), 400);
                setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c49', crecic49.value), 500);
            }

            texto = crecimiento;

            var value = document.getElementById(campoDestino)
            value.value = texto

            setTimeout(guardarDatosPlantilla('crecimien_prac_alim', 'c50', value.value), 600);
            break;

        case 'formulario.desarrollo_vacunas.c20': // VACUNAS MENOR 2 MESES            
            var desarrolloc8 = document.getElementById('formulario.desarrollo_vacunas.c8');
            var desarrolloc9 = document.getElementById('formulario.desarrollo_vacunas.c9');
            var desarrolloc10 = document.getElementById('formulario.desarrollo_vacunas.c10');
            var desarrolloc11 = document.getElementById('formulario.desarrollo_vacunas.c11');
            var desarrolloc12 = document.getElementById('formulario.desarrollo_vacunas.c12');
            if (parseInt(valorAtributo('lblMesesEdadPaciente')) >= 1) {
                var desarrolloc13 = document.getElementById('formulario.desarrollo_vacunas.c13');
                var desarrolloc14 = document.getElementById('formulario.desarrollo_vacunas.c14');
                var desarrolloc15 = document.getElementById('formulario.desarrollo_vacunas.c15');
                var desarrolloc16 = document.getElementById('formulario.desarrollo_vacunas.c16');
            }
            else {
                var desarrolloc13 = ""
                var desarrolloc14 = ""
                var desarrolloc15 = ""
                var desarrolloc16 = ""
            }
            var desarrolloc17 = document.getElementById('formulario.desarrollo_vacunas.c17');
            var desarrolloc18 = document.getElementById('formulario.desarrollo_vacunas.c18');
            var desarrolloc19 = document.getElementById('formulario.desarrollo_vacunas.c19');
            var desarrolloc20 = document.getElementById('formulario.desarrollo_vacunas.c20');

            desarrollo = "";

            if ((parseInt(valorAtributo('lblMesesEdadPaciente')) < 1 && (desarrolloc8.value == 'NO' && desarrolloc9.value == 'NO' && desarrolloc10.value == 'NO' && desarrolloc11.value == 'NO' && desarrolloc12.value == 'NO'))) {
                desarrollo = "PROBABLE RETRASO EN EL DESARROLLO\nA) REFIERA A UNA EVALUACIÓN DEL NEURODESARROLLO POR ESPECIALISTA (PEDIATRA)\nB) CONSULTA DE SEGUIMIENTO EN LA SIGUIENTE SEMANA PARA EVALUAR QUÉ SUCEDIÓ EN LA CONSULTA DE REFERENCIA\nC) ENSEÑE SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nD) RECOMENDACIÓN DE CUIDADOS EN CASA Y MEDIDAS PREVENTIVAS ESPECÍFICAS\n"
                let data = [
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c17",
                        color: "red"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c18",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c19",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.desarrollo_vacunas.c17td").style.backgroundColor = 'red';
                // document.getElementById("formulario.desarrollo_vacunas.c18td").style.backgroundColor = 'white';
                // document.getElementById("formulario.desarrollo_vacunas.c19td").style.backgroundColor = 'white';
                desarrolloc17.value = "SI";
                desarrolloc17.checked = true;
                desarrolloc18.value = "NO";
                desarrolloc18.checked = false;
                desarrolloc19.value = "NO";
                desarrolloc19.checked = false;
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c17', desarrolloc17.value), 100);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c18', desarrolloc18.value), 200);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c19', desarrolloc19.value), 300);
            }
            else if (parseInt(valorAtributo('lblMesesEdadPaciente')) >= 1 && desarrolloc13.value == 'NO' && desarrolloc14.value == 'NO' && desarrolloc15.value == 'NO' && desarrolloc16.value == 'NO') {
                desarrollo = "PROBABLE RETRASO EN EL DESARROLLO\nA) REFIERA A UNA EVALUACIÓN DEL NEURODESARROLLO POR ESPECIALISTA (PEDIATRA)\nB) CONSULTA DE SEGUIMIENTO EN LA SIGUIENTE SEMANA PARA EVALUAR QUÉ SUCEDIÓ EN LA CONSULTA DE REFERENCIA\nC) ENSEÑE SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nD) RECOMENDACIÓN DE CUIDADOS EN CASA Y MEDIDAS PREVENTIVAS ESPECÍFICAS\n"
                let data = [
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c17",
                        color: "red"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c18",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c19",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.desarrollo_vacunas.c17td").style.backgroundColor = 'red';
                // document.getElementById("formulario.desarrollo_vacunas.c18td").style.backgroundColor = 'white';
                // document.getElementById("formulario.desarrollo_vacunas.c19td").style.backgroundColor = 'white';
                desarrolloc17.value = "SI";
                desarrolloc17.checked = true;
                desarrolloc18.value = "NO";
                desarrolloc18.checked = false;
                desarrolloc19.value = "NO";
                desarrolloc19.checked = false;
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c17', desarrolloc17.value), 100);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c18', desarrolloc18.value), 200);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c19', desarrolloc19.value), 300);
            }
            else if (parseInt(valorAtributo('lblMesesEdadPaciente')) < 1 && (desarrolloc8.value == 'NO' || desarrolloc9.value == 'NO' || desarrolloc10.value == 'NO' || desarrolloc11.value == 'NO' || desarrolloc12.value == 'NO')) {
                desarrollo = "RIESGO DE PROBLEMA O CON FACTORES DE RIESGO\n A) ACONSEJE A LA MADRE SOBRE ESTIMULACIÓN DE SU HIJO DE ACUERDO A LA EDAD\nB) REALICE CONSULTA DE SEGUIMIENTO A LOS 30 DÍAS\nC) ENSEÑE A LA MADRE SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nD) MEDIDAS PREVENTIVAS DIRIGIDAS ESPECÍFICAMENTE A LOS FACTORES DE RIESGO MODIFICABLES\n";
                let data = [
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c17",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c18",
                        color: "yellow"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c19",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.desarrollo_vacunas.c17td").style.backgroundColor = 'white';
                // document.getElementById("formulario.desarrollo_vacunas.c18td").style.backgroundColor = 'yellow';
                // document.getElementById("formulario.desarrollo_vacunas.c19td").style.backgroundColor = 'white';
                desarrolloc17.value = "NO";
                desarrolloc17.checked = false;
                desarrolloc18.value = "SI";
                desarrolloc18.checked = true;
                desarrolloc19.value = "NO";
                desarrolloc19.checked = false;
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c17', desarrolloc17.value), 100);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c18', desarrolloc18.value), 200);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c19', desarrolloc19.value), 300);
            }
            else if (parseInt(valorAtributo('lblMesesEdadPaciente')) >= 1 && (desarrolloc13.value == 'NO' || desarrolloc14.value == 'NO' || desarrolloc15.value == 'NO' || desarrolloc16.value == 'NO')) {
                desarrollo = "RIESGO DE PROBLEMA O CON FACTORES DE RIESGO\n A) ACONSEJE A LA MADRE SOBRE ESTIMULACIÓN DE SU HIJO DE ACUERDO A LA EDAD\nB) REALICE CONSULTA DE SEGUIMIENTO A LOS 30 DÍAS\nC) ENSEÑE A LA MADRE SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\nD) MEDIDAS PREVENTIVAS DIRIGIDAS ESPECÍFICAMENTE A LOS FACTORES DE RIESGO MODIFICABLES\n";
                let data = [
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c17",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c18",
                        color: "yellow"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c19",
                        color: "white"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.desarrollo_vacunas.c17td").style.backgroundColor = 'white';
                // document.getElementById("formulario.desarrollo_vacunas.c18td").style.backgroundColor = 'yellow';
                // document.getElementById("formulario.desarrollo_vacunas.c19td").style.backgroundColor = 'white';
                desarrolloc17.value = "NO";
                desarrolloc17.checked = false;
                desarrolloc18.value = "SI";
                desarrolloc18.checked = true;
                desarrolloc19.value = "NO";
                desarrolloc19.checked = false;
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c17', desarrolloc17.value), 100);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c18', desarrolloc18.value), 200);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c19', desarrolloc19.value), 300);
            }
            else {
                desarrollo = "DESARROLLO NORMAL\n   A) FELICITE A LA MADRE\nB) ACONSEJE A LA MADRE PARA QUE CONTINÚE ESTIMULANDO A SU HIJO DE ACUERDO A SU EDAD\nC) ENSEÑE A LA MADRE LOS SIGNOS DE ALARMA PARA REGRESAR DE INMEDIATO\n";
                let data = [
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c17",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c18",
                        color: "white"
                    },
                    {
                        campoDestino:"formulario.desarrollo_vacunas.c19",
                        color: "green"
                    },                    
                ]
                changeColorAIEPI(data,version)
                // document.getElementById("formulario.desarrollo_vacunas.c17td").style.backgroundColor = 'white';
                // document.getElementById("formulario.desarrollo_vacunas.c18td").style.backgroundColor = 'white';
                // document.getElementById("formulario.desarrollo_vacunas.c19td").style.backgroundColor = 'green';
                desarrolloc17.value = "NO";
                desarrolloc17.checked = false;
                desarrolloc18.value = "NO";
                desarrolloc18.checked = false;
                desarrolloc19.value = "SI";
                desarrolloc19.checked = true;
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c17', desarrolloc17.value), 100);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c18', desarrolloc18.value), 200);
                setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c19', desarrolloc19.value), 300);
            }

            texto = desarrollo;

            var value = document.getElementById(campoDestino);
            value.value = texto;

            setTimeout(guardarDatosPlantilla('desarrollo_vacunas', 'c20', value.value), 600);
            break;
    }
}

function ocultarTabs() {
    var aplica = document.getElementById('formulario.aphp.aplica');
    if (aplica.value == 'NO') {
        if (document.getElementById('tabhaco') != null) ocultar('tabhaco');
        if (document.getElementById('tabtmgl') != null) ocultar('tabtmgl');
        if (document.getElementById('tabpcrs') != null) ocultar('tabpcrs');
        if (document.getElementById('tabpcep') != null) ocultar('tabpcep');
        if (document.getElementById('tabpcnp') != null) ocultar('tabpcnp');
        if (document.getElementById('tabefpaz') != null) ocultar('tabefpaz');
        if (document.getElementById('tabcrta') != null) ocultar('tabcrta');
    }
    else {
        if (document.getElementById('tabhaco') != null) mostrar('tabhaco');
        if (document.getElementById('tabtmgl') != null) mostrar('tabtmgl');
        if (document.getElementById('tabpcrs') != null) mostrar('tabpcrs');
        if (document.getElementById('tabpcep') != null) mostrar('tabpcep');
        if (document.getElementById('tabpcnp') != null) mostrar('tabpcnp');
        if (document.getElementById('tabefpaz') != null) mostrar('tabefpaz');
        if (document.getElementById('tabcrta') != null) mostrar('tabcrta');
    }
}

/**
 * Servicio  de validaciones  utilizado para guardar y eliminar valiaciones y reglas en la nase de datos
 * 
 * 
 * @author CHRISTIAN ANGULO <christiancamilo1221@gmail.com>
 * @returns {void}
 */
 function validacionesVih(id, name) {
    var id_evolucion = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO')
    var variable
    var validaciones_false = []
    var validaciones_eliminacion = []
    
    if (id != '' || name != '') {
        variable = []

        if (Array.isArray(name)) {
            console.log('is array')

            name.forEach((v) => {

                if (document.getElementById('formulario' + '.' + id + '.' + v) != null) {

                    if (document.getElementById('formulario' + '.' + id + '.' + v).attributes[1].value == 'checkbox') {
                        if(document.getElementById('formulario' + '.' + id + '.' + v).parentNode.parentNode.parentNode.childNodes[1].innerHTML.split('-')[1] != undefined){
                            variable.push(document.getElementById('formulario' + '.' + id + '.' + v).parentNode.parentNode.parentNode.childNodes[1].innerHTML.split('-')[0].toLowerCase().trim())
                        }
                       
                    } else {
                        if(document.getElementById('formulario' + '.' + id + '.' + v).parentNode.parentNode.childNodes[1].innerHTML.split('-')[1] != undefined){
                            variable.push(document.getElementById('formulario' + '.' + id + '.' + v).parentNode.parentNode.childNodes[1].innerHTML.split('-')[0].toLowerCase().trim())
                        }
                        
                        console.log('split ',document.getElementById('formulario' + '.' + id + '.' + v).parentNode.parentNode.childNodes[1].innerHTML.split('-')[0].toLowerCase().trim())
                    }
                }

            })
            console.log('variable a mandar array: ', variable)



        } else {
           
            variable = ""
            if (document.getElementById('formulario' + '.' + id + '.' + name) != null) {

                if (document.getElementById('formulario' + '.' + id + '.' + name).attributes[1].value == 'checkbox') {
                    variable = document.getElementById('formulario' + '.' + id + '.' + name).parentNode.parentNode.parentNode.childNodes[1].innerHTML.split('-')[0].toLowerCase().trim()
                } else {
                    variable = document.getElementById('formulario' + '.' + id + '.' + name).parentNode.parentNode.childNodes[1].innerHTML.split('-')[0].toLowerCase().trim()

                }

                variable = [variable]
                console.log('variable a mandar: ', variable)

            }

        }

        fetch('https://cristalweb.emssanar.org.co:8000/services/vih/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                query: `
      mutation
      b24x($parameters:[DictionaryType],$variables:[String]){
        b24x(parameters:$parameters, variables:$variables){
            variables {
                id
                description
                value
                state
                rules {
                  id
                  evalcondition
                  description
                  condition
                  validation
                  evalvalidation
                  variables
                  state
                  error
                }
                error
            }
       }
    }`,
                variables: {
                    parameters: [{ "key": "id_evolucion", "value": `${id_evolucion}` }],
                    variables: variable
                },
            }),
        })
            .then((res) => res.json())
            .then((result) => {
                var resultVih = result.data.b24x


                resultVih.variables.forEach((data) => {

                    if (data.rules.length > 0) {
                        data.rules.forEach((dataRules) => {
                            if (dataRules.state == false) {
                                dataRules.id_evolucion = id_evolucion
                                dataRules.variable = data.id
                                dataRules.id_regla = dataRules.id
                                validaciones_false.push(dataRules)

                            } else {
                                dataRules.id_evolucion = id_evolucion
                                dataRules.variable = data.id
                                dataRules.id_regla = dataRules.id
                                validaciones_eliminacion.push(dataRules)

                            }

                        })
                    }
                })

                if (validaciones_false.length > 0) {
                    pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
                    valores_a_mandar = "accion=validacionVih";
                    valores_a_mandar = valores_a_mandar + `&idQuery=4021&parametros=`
                    tipoElem = 'cmb';
                    add_valores_a_mandar(encodeURIComponent(JSON.stringify(validaciones_false)));
                    ajaxModificar();

                }

                if (validaciones_eliminacion.length > 0) {

                    pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
                    valores_a_mandar = "accion=validacionVih";
                    valores_a_mandar = valores_a_mandar + `&idQuery=4022&parametros=`
                    tipoElem = 'cmb';
                    add_valores_a_mandar(encodeURIComponent(JSON.stringify(validaciones_eliminacion)));
                    ajaxModificar();


                }


            })



    }else{
        console.log('ENTRO A REGISTRO ALL: ' ,id_evolucion)
        variable = []
        fetch('https://cristalweb.emssanar.org.co:8000/services/vih/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                query: `
      mutation
      b24x($parameters:[DictionaryType],$variables:[String]){
        b24x(parameters:$parameters, variables:$variables){
            variables {
                id
                description
                value
                state
                rules {
                  id
                  evalcondition
                  description
                  condition
                  validation
                  evalvalidation
                  variables
                  state
                  error
                }
                error
            }
       }
    }`,
                variables: {
                    parameters: [{ "key": "id_evolucion", "value": `${id_evolucion}` }],
                    variables: variable
                },
            }),
        })
            .then((res) => res.json())
            .then((result) => {
                var resultVih = result.data.b24x


                resultVih.variables.forEach((data) => {

                    if (data.rules.length > 0) {
                        data.rules.forEach((dataRules) => {
                            if (dataRules.state == false) {
                                dataRules.id_evolucion = id_evolucion
                                dataRules.variable = data.id
                                dataRules.id_regla = dataRules.id
                                validaciones_false.push(dataRules)

                            } else {
                                dataRules.id_evolucion = id_evolucion
                                dataRules.variable = data.id
                                dataRules.id_regla = dataRules.id
                                validaciones_eliminacion.push(dataRules)

                            }

                        })
                    }
                })

                if (validaciones_false.length > 0) {
                    pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
                    valores_a_mandar = "accion=validacionVih";
                    valores_a_mandar = valores_a_mandar + `&idQuery=4021&parametros=`
                    tipoElem = 'cmb';
                    add_valores_a_mandar(encodeURIComponent(JSON.stringify(validaciones_false)));
                    ajaxModificar();

                }

                if (validaciones_eliminacion.length > 0) {

                    pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
                    valores_a_mandar = "accion=validacionVih";
                    valores_a_mandar = valores_a_mandar + `&idQuery=4022&parametros=`
                    tipoElem = 'cmb';
                    add_valores_a_mandar(encodeURIComponent(JSON.stringify(validaciones_eliminacion)));
                    ajaxModificar();


                }
                traerInfomacionVihValidacion(id_evolucion)

            })

    }

}

/**
 * Trae informacion  de  las validaciones de vih y las muestra en una grilla  
 * 
 * 
 * @author CHRISTIAN ANGULO <christiancamilo1221@gmail.com>
 * @returns {void}
 */
function traerInfomacionVihValidacion(id_evolucion) {
    document.getElementById("divDescripcionRegla").style.display = "none";
    document.getElementById("divValidacionesVariables").style.display = "none";
    pagina = "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp";
    valores_a_mandar = pagina;
    valores_a_mandar = valores_a_mandar + "?idQuery=4023&parametros=";
    add_valores_a_mandar(id_evolucion);


    $("#drag" + ventanaActual.num)
        .find("#listValidacionesVih")
        .jqGrid({
            caption: 'AUDITORIA HISTORIA CLINICA',
            url: valores_a_mandar,
            datatype: "xml",
            mtype: "GET",
            colNames: ["#", "VARIABLE", "REGLA", "DESCRIPCION", "ERROR", "VARIABLES", "UBICACION", "CONDITION", "EVALCONDITION",
                "VALIDATION", "EVALVALIDATION", "ERROR"],
            colModel: [

                { name: "#", index: "#", hidden: true },
                { name: "VARIABLE", index: "VARIABLE", width: 80 },
                { name: "REGLA", index: "REGLA", width: 80 },
                { name: "DESCRIPCION", index: "DESCRIPCION", width: 370, hidden: true },
                { name: "ERROR", index: "ERROR", width: 180, hidden: true },
                { name: "variables", index: "variables", hidden: true },
                { name: "UBICACION", index: "UBICACION", width: 325 },
                { name: "CONDITION", index: "CONDITION", hidden: true },
                { name: "EVALCONDITION", index: "EVALCONDITION", hidden: true },
                { name: "VALIDATION", index: "VALIDATION", hidden: true },
                { name: "EVALVALIDATION", index: "EVALVALIDATION", hidden: true },
                { name: "ERROR", index: "ERROR", hidden: true },



            ],

            height: 180,
            autowidth: true,

            onSelectRow: function (rowid) {
                var idDivRegla = document.getElementById("divDescripcionRegla")
                var datosRow = jQuery('#drag' + ventanaActual.num).find('#listValidacionesVih').getRowData(rowid);
                document.getElementById("divDescripcionRegla").style.display = "block";
                document.getElementById("divValidacionesVariables").style.display = "block";

                divDescripcionRegla(idDivRegla, datosRow.REGLA, datosRow.DESCRIPCION, datosRow.CONDITION, datosRow.EVALCONDITION, datosRow.VALIDATION, datosRow.EVALVALIDATION, datosRow.ERROR)
                traerInfomacionVariablesVih(id_evolucion, datosRow.VARIABLE, datosRow.REGLA)
            },


        });



    $("#drag" + ventanaActual.num)

        .find("#listValidacionesVih")

        .setGridParam({ url: valores_a_mandar })

        .trigger("reloadGrid");

    document.getElementById("divValidacionesVih").style.display = "flex";
    //document.getElementById("divValidacionesVih1").style.top = "30%";
    //document.getElementById("divValidacionesVih1").style.left = "75%";
    $("#divValidacionesVih").draggable()


}
/**
 * Trae informacion  de  las variables de una regla y las muestra en una grilla  validaciones vih
 * 
 * 
 * @author CHRISTIAN ANGULO <christiancamilo1221@gmail.com>
 * @returns {void}
 */
function traerInfomacionVariablesVih(id_evolucion, variable, id_regla) {
    console.log(id_evolucion, variable, id_regla)

    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=4025&parametros=";
    add_valores_a_mandar(id_evolucion);
    add_valores_a_mandar(variable.trim());
    add_valores_a_mandar(id_regla.trim());


    $("#drag" + ventanaActual.num)
        .find("#listVariablesVih")
        .jqGrid({


            url: valores_a_mandar,
            caption: 'VARIABLES',
            datatype: "xml",
            mtype: "GET",
            colNames: ["#", "VARIABLE", "UBICACION", "CAMPO"],
            colModel: [

                { name: "#", index: "#", hidden: true },
                { name: "VARIABLE", index: "VARIABLE", width: 40, hidden: true },
                { name: "UBICACION", index: "UBICACION", width: 80 },
                { name: "CAMPO", index: "CAMPO", width: 200 },



            ],

            height: 180,

            autowidth: true,

            onSelectRow: function (rowid) { },

        });



    $("#drag" + ventanaActual.num)

        .find("#listVariablesVih")

        .setGridParam({ url: valores_a_mandar })

        .trigger("reloadGrid");

}

/**
 * Genera una div con nombre y descripcion de la regla de validaciones vih 
 * 
 * 
 * @author CHRISTIAN ANGULO <christiancamilo1221@gmail.com>
 * @returns {void}
 */
function divDescripcionRegla(id, regla, descripcion, condicion, evalcondicion, validation, evalvalidation, error) {
    id.innerHTML = `
    <br>
    <div style="align-items: center;  justify-content: center;   display: flex;  text-decoration: underline overline;">
        <span>REGLA : ${regla} </span>  
    </div>
    <br>
    <div style = "height:100px; overflow: scroll ;text-align: center; font-size:11px">
        <div> <span>${descripcion}</span>  </div>
       
        <br>
        <div style="align-items: center;  justify-content: center;  display: flex;  text-decoration: underline overline; font-size:15px">
        
            <span>CONDICION </span>  
        </div>
        <br>
        <div style = "height:30px ;text-align: center; font-size:12px">
            <span>${condicion}</span>   
        </div> 
       
        <div style="align-items: center;  justify-content: center;  display: flex;  text-decoration: underline overline; font-size:15px">
        
            <span>EVALUACION CONDICION </span>  
        </div>
        <br>
        <div style = "height:30px ;text-align: center; font-size:12px">
            <span>${evalcondicion}</span>   
        </div> 

        <div style="align-items: center;  justify-content: center;  display: flex;  text-decoration: underline overline; font-size:15px">
        
            <span>VALIDACION </span>  
        </div>
        <br>
        <div style = "height:auto ;text-align: center; font-size:12px">
            <span>${validation}</span>   
        </div> 
        <br>
        <div style="align-items: center;  justify-content: center;  display: flex;  text-decoration: underline overline; font-size:15px">
        <span> EVALUACION VALIDACION </span>  
        </div>
        <br>
        <div style = "height:auto ;text-align: center; font-size:12px">
        <span>${evalvalidation}</span>   
        </div> 

        <br>
        <div style="align-items: center;  justify-content: center;  display: flex;  text-decoration: underline overline; font-size:15px">
        <span> ERROR </span>  
        </div>
        <br>
        <div style = "height:30px ;text-align: center; font-size:12px">
        <span>${error}</span>   
        </div> 
    
    </div>
  
     
     `

}


/**
 * Oculta los tabs del folio B24X dependiendo de lo que se seleccione en el tab vih0 
 * (CLASIFICACION DEL PACIENTE B24X)
 * 
 * @author NICOLAS CAICEDO <nicolas.caicedo1799@gmail.com>
 * @returns {void}
 */
function ocultarTabsVih(respuesta) {
    try {
        let clasPaciente = respuesta;
        mostrarTabsVih();
        if (document.getElementById('tabdaan') != null) ocultar('tabdaan');
        // Se oculta tab VIH MUJER GESTANTE si la opcion escogida no es mujer gestante
        if (clasPaciente == '1') {
            if (document.getElementById('tabvih3') != null) ocultar('tabvih3');
        }
        if (clasPaciente != '1' && clasPaciente != '') {
            if (document.getElementById('tabvih2') != null) ocultar('tabvih2');
            if (document.getElementById('tabdaan') != null) ocultar('tabdaan');
        }
        if (clasPaciente == '3') {
            if (document.getElementById('tabvih3') != null) ocultar('tabvih3');
            if (document.getElementById('tabdaan') != null) ocultar('tabdaan');
        }
        if (clasPaciente == '4') {
            if (document.getElementById('tabvih2') != null) ocultar('tabvih2');
            if (document.getElementById('tabvih3') != null) ocultar('tabvih3');
            if (document.getElementById('tabdaan') != null) ocultar('tabdaan');
        }
        if (clasPaciente == '2') {
            if (document.getElementById('tabvanenf') != null) ocultar('tabvanenf');
            if (document.getElementById('tabvihg') != null) ocultar('tabvihg');
            if (document.getElementById('tabtdla') != null) ocultar('tabtdla');
            if (document.getElementById('tabcitv') != null) ocultar('tabcitv');
            if (document.getElementById('tabvih2') != null) ocultar('tabvih2');
            if (document.getElementById('tabfp01') != null) ocultar('tabfp01');
            if (document.getElementById('tabvihC') != null) ocultar('tabvihC');
            if (document.getElementById('tabdaan') != null) ocultar('tabdaan');
            if (document.getElementById('tabvihf') != null) ocultar('tabvihf');
            if (document.getElementById('tabvih6') != null) ocultar('tabvih6');
            if (document.getElementById('tabvih7') != null) ocultar('tabvih7');
            if (document.getElementById('tabvih9') != null) ocultar('tabvih9');
            if (document.getElementById('tabvihD') != null) ocultar('tabvihD');
            if (document.getElementById('tabvihE') != null) ocultar('tabvihE');
        }
        let edad = valorAtributo('lblMesesEdadPaciente');
        if (edad < 12) {
            if (document.getElementById('tabvanenf') != null) ocultar('tabvanenf');
            if (document.getElementById('tabvih2') != null) ocultar('tabvih2');
            //if (document.getElementById('tabfp01') != null) ocultar('tabfp01');
            if (document.getElementById('tabvihC') != null) ocultar('tabvihC');
            if (document.getElementById('tabdaan') != null) ocultar('tabdaan');
            if (document.getElementById('tabvihf') != null) ocultar('tabvihf');
            if (document.getElementById('tabvih6') != null) ocultar('tabvih6');
            if (document.getElementById('tabvih7') != null) ocultar('tabvih7');
            if (document.getElementById('tabvih9') != null) ocultar('tabvih9');
            if (document.getElementById('tabvihD') != null) ocultar('tabvihD');
            if (document.getElementById('tabvihE') != null) ocultar('tabvihE');
            if (document.getElementById('tabvih0') != null) ocultar('tabvih0');
        }
    } catch (error) {
        console.error(error);
    }


}

/**
 * Muestra todos los tabs del folio B24X
 * 
 * @author NICOLAS CAICEDO <nicolas.caicedo1799@gmail.com>
 * @returns {void}
 */
function mostrarTabsVih() {
    try {
        if (document.getElementById('tabvanenf') != null) mostrar('tabvanenf');
        if (document.getElementById('tabvih2') != null) mostrar('tabvih2');
        if (document.getElementById('tabvih3') != null) mostrar('tabvih3');
        if (document.getElementById('tabfp01') != null) mostrar('tabfp01');
        if (document.getElementById('tabvihC') != null) mostrar('tabvihC');
        if (document.getElementById('tabdaan') != null) mostrar('tabdaan');
        if (document.getElementById('tabvih4') != null) mostrar('tabvih4');
        if (document.getElementById('tabvihf') != null) mostrar('tabvihf');
        if (document.getElementById('tabvih6') != null) mostrar('tabvih6');
        if (document.getElementById('tabvih7') != null) mostrar('tabvih7');
        if (document.getElementById('tabvih9') != null) mostrar('tabvih9');
        if (document.getElementById('tabvihD') != null) mostrar('tabvihD');
        if (document.getElementById('tabvihE') != null) mostrar('tabvihE');
        if (document.getElementById('tabvihg') != null) mostrar('tabvihg');
        if (document.getElementById('tabtdla') != null) mostrar('tabtdla');
        if (document.getElementById('tabcitv') != null) mostrar('tabcitv');
    } catch (error) {
        console.error(error);
    }
}


function actualizarDiagnostico(valor) {
    var id_evolucion = document.getElementById('lblIdEvolucionIncapacidad').outerText
    pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
    valores_a_mandar = "accion=actualizarDiagnostico";

    if (valor == ''){
        valores_a_mandar = valores_a_mandar + `&idQuery=4052&parametros=`
        add_valores_a_mandar(valor);
        add_valores_a_mandar(id_evolucion);
        add_valores_a_mandar(id_evolucion);
        add_valores_a_mandar(id_evolucion);
    }
    else{
        valores_a_mandar = valores_a_mandar + `&idQuery=4051&parametros=`
        add_valores_a_mandar(valor);
        add_valores_a_mandar(id_evolucion);
        add_valores_a_mandar(id_evolucion);
        add_valores_a_mandar(valor);
        add_valores_a_mandar(id_evolucion);

    }
  
    tipoElem = 'cmb';

    ajaxModificar();

}


function changeColorAIEPI(data,version){
    data.forEach(element => {
        let campoDestino
        if(version == 'v1'){
            campoDestino = `${element.campoDestino}td`
        }else{
            campoDestino = `label.${element.campoDestino}`
        }        
        document.getElementById(campoDestino).style.backgroundColor = element.color;
    });
}