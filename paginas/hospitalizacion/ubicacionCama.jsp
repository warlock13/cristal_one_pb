<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1000px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="UBICACION CAMA PACIENTE" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO  aqui empieza el cambio-->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <div style="overflow:auto; height:500px; width:100%" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <table width="100%">
                <tr>
                  <td valign="top" width="90%">
                    <table width="100%" cellpadding="0" cellspacing="0" border="1">
                      <tr class="titulos">
                        <td width="20%">SERVICIO</td>
                        <td width="20%">PISO/AREA</td>
                        <td width="30%">HABITACION</td>
                        <td width="30%">ESTADO</td>
                        <td width="10%"></td>
                      </tr>
                      <tr class="estiloImput">
                        

                      	<td><select size="1" id="cmbServicio" style="width:80%"     onchange="asignaAtributo('cmbIdArea', '', 0);asignaAtributo('cmbIdHabitacion', '', 0);asignaAtributo('cmbIdEstadoCama', '', 0)">	                                        
                              <option value=""></option> 
                              <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(852);	
                                         ComboVO cmb5; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmb5=(ComboVO)resultaux.get(k);
                                  %>
                            <option value="<%= cmb5.getId()%>" title="<%= cmb5.getTitle()%>">
                              <%= cmb5.getDescripcion()%></option>
                            <%}%>	                                 
                             </select>	                   
                         </td> 

                        <td><select id="cmbIdArea" style="width:80%"  onfocus="cargarAreaDesdeServicio('cmbServicio', 'cmbIdArea')"    
                            onchange="asignaAtributo('cmbIdHabitacion', '', 0);asignaAtributo('cmbIdEstadoCama', '', 0)">
                            <option value=""></option>
                            <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(976);	
                                         ComboVO cmb32; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmb32=(ComboVO)resultaux.get(k);
                                  %>
                            <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>">
                              <%= cmb32.getDescripcion()%></option>
                            <%}%>						
                             </select>	                   
                         </td> 
                          
                         
                         <td><select size="1" id="cmbIdHabitacion" style="width:80%"  onfocus="cargarHabitacionDesdeArea('cmbIdArea', 'cmbIdHabitacion')"   >	                                        
                              <option value=""></option>                                  
                             </select>	                   
                         </td> 
                         <td>
                            <select id="cmbIdEstadoCama" style="width:80%"  >
                              <option value=""></option>
                              <%     resultaux.clear();
                                          resultaux=(ArrayList)beanAdmin.combo.cargar(164);	
                                          for(int k=0;k<resultaux.size();k++){ 
                                                cmb32=(ComboVO)resultaux.get(k);
                                    %>
                              <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>">
                                <%= cmb32.getDescripcion()%></option>
                              <%}%>						
                              </select>	                   
                          </td>   
                         <td><input class="small button blue" value="CONSULTAR" title="BOLA7"  type="button" onclick="buscarAGENDA('listUbicacionCama')"></td>             
                     </tr>  
                     <tr class="titulos"  >   
                        <td colspan="4">                            
                            <div id="divListContenedor" style="overflow:auto; height:100%; width:100%">
                                <table id="listUbicacionCama" class="scroll"></table>  
                            </div>    
                        </td>
                     </tr> 
                   </table>  
                   </td> 
                 </tr>
               </table>   
                  
              </div> 
         </td>
       </tr>
      </table>
   </td>
 </tr>  
</table>

<div id="divVentanitaUbicacionPaciente" style="position:absolute; display:none; top:0px; left:0px; width:70%; height:90px; z-index:999">
    <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div style="z-index:2002; position:fixed; top:100px; left:180px; width:40%">
      <table width="100%"  class="fondoTabla" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <table width="100%">
              <tr>
                <td align="left"><input name="btn_cerrar" type="button" align="left" value="CERRAR" onclick="ocultar('divVentanitaUbicacionPaciente');buscarAGENDA('listAgendaProgramacion');" /></td>  
                <td align="right"><input name="btn_cerrar" type="button" value="CERRAR" onclick="ocultar('divVentanitaUbicacionPaciente');buscarAGENDA('listAgendaProgramacion');" /></td>  
              </tr>
            </table>
          </td>
        </tr>           
        <tr id="trBusPaciente">   
            <td colspan="2">
              <table width="100%">
                <tr class="titulos">
                  <td><strong>PACIENTE</strong></td>
                </tr>
                <tr>
                  <td colspan="2" align="CENTER">
                    <input type="text" size="110"  maxlength="210"  id="txtIdBusPaciente"  style="width:90%"   /> 
                    <img width="18px" height="18px" align="middle" title="VEN42" id="idLupitaVentanitaBusPaciente" onclick="traerVentanitaHos(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '18')" src="/clinica/utilidades/imagenes/acciones/buscar.png">                    
                    <div id="divParaVentanita"></div> 
                  </td>  
                </tr>
              </table>
            </td>                                 
        </tr>     
        <tr id="trInfoPaciente">
          <td colspan="2" align="CENTER">
            <table width="100%">
              <tr class="titulos">
                <td width="25%"><strong>ADMISION</strong></td>
                <td width="25%"><strong>ID PACIENTE</strong></td>
                <td width="50%"><strong>PACIENTE</strong></td>
              </tr>
              <tr class="estiloImput">
                <td><strong><label id="lblIdAdmisionUbicacionPaciente"></label></strong></td>
                <td><strong><label id="lblIdPacienteUbicacionPaciente" style="color: red;"></label></strong></td>
                <td>
                  <strong>
                    <label id="lblTipoIdUbicacionPaciente"></label><label id="docUbicacionPaciente"></label>
                    <label id="lblNomUbicacionPaciente"></label>
                  </strong>
                </td>
              </tr>
            </table>
          </td>
        </tr> 
        <tr>
          <td colspan="2"><hr></td>
        </tr>          
        <tr>
          <td colspan="2">                
              <table width="100%"  cellpadding="0" cellspacing="0">
                  <tr class="titulos">
                      <td width="25%"><strong>AREA</strong></td> 
                      <td width="25%"><strong>HABITACI&Oacute;N</strong></td>                
                      <td width="25%"><strong>CAMA</strong></td>                                    
                  </tr>     
                  <tr class="estiloImput">
                      <td><input type="hidden" id="txtIdArea"><label id="lblArea"></label></td>   
                      <td><input type="hidden" id="txtIdHabitacion"><label id="lblHabitacion"></label></td>   
                      <td><input type="hidden" id="txtIdCama"><label id="lblCama"></label></td>                           
                  </tr>           
              </table>          
            </td>
        </tr>
        <tr id="trAccionCama">
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="33%"><hr></td>
                <td align="CENTER">
                  <input class="small button blue" value="OCUPAR CAMA" style="width: 95%;"  type="button" onclick="modificarCRUD('modificarAdmisionCama');">  
                </td>
                <td width="33%"><hr></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr id="trAccionDejarDisponible">
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="33%"><hr></td>
                <td align="CENTER">
                  <input class="small button blue" value="DEJAR DISPONIBLE" style="width: 95%;"  type="button" onclick="modificarCRUD('modificarDisponibilidadCama');">  
                </td>
                <td width="33%"><hr></td>
              </tr>
            </table>
          </td>
        </tr>
      </table> 
    </div>
  <input type="hidden" id="txtIdFacturaUbicacionPaciente">
  <input type="hidden" id="txtIdCamaActual">
</div>

<!-------------------------para la vista previa------------------------------>
<input type="hidden" id="txt_banderaOpcionCirugia" value="PROG_FARMACIA"/>              
<input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
<input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />    
<input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />                
<!-------------------------fin para la vista previa--------------------------->      