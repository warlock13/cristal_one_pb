<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />

<%
    if (request.getServerName().equals("cristalweb.emssanar.org.co") && request.getServerPort() == 8383) {
        response.sendRedirect("http://10.0.0.3:9010/cristal");
    } else {
		if (beanSession.getAcceso()) {
			request.getRequestDispatcher("principal.jsp").forward(request, response);
		}
	}
%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Pragma" CONTENT="no-cache"/>
        <title>CRISTAL WEB</title>
        <link REL="SHORTCUT ICON" HREF="../utilidades/css/favicon.png"/>
        <link href="../utilidades/css/estiloClinica.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="/clinica/utilidades/css/jquery.treeview.css"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js"></script>
		<script src="../utilidades/javascript/sweetalert2-11.4.8/dist/sweetalert2.all.min.js"></script>
        <script src="../utilidades/javascript/login.js"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/brands.js" integrity="sha384-sCI3dTBIJuqT6AwL++zH7qL8ZdKaHpxU43dDt9SyOzimtQ9eyRhkG3B7KMl6AO19" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/1c70883c8e.js" crossorigin="anonymous"></script>
        <script src="../utilidades/javascript/login.js?<%=beanSession.getVersion()%>"></script>
    </head>
    <body id="bodyLogin">

        <nav id="navLogin">
            <img src="../../clinica/utilidades/imagenes/menu/CRYSTAL_Logo_nav.png" alt="">
            <a href="https://crystalitservice.com/" target="_blank">www.crystalitservice.com</a>
        </nav>

        <iframe name="iframeUpload" id="iframeUpload" style="border:none; display:none "></iframe>
        <div id="inicial">		
            <form id="login_form" action="iniciar_sesion.jsp" method="post">
                <table width="90%" border="1" align="center" id="idTableLogin">
                    <tr>
                        <td width="50%" align="center">
                            <!-- <FONT FACE="impact" color="#336699">PUBLICACIONES</font> -->
                        </td>
                        <td width="50%" align="center"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="td_iframe">
                            <div class="container" style="height:390px; width:auto">
                                <iframe src="../publicaciones/index.html" width="100%" height="100%" align="center"
                                        class="container_"></iframe>
                            </div>
                        </td>
                        <td width="10%" align="CENTER" valign="top">
                            <div class="container" id="contLogin">

                                <img src="../logoCW.png" width="100%" style="margin-top:1%;"
                                     alt="Clinica">
                                <input type="hidden" name="tabID" id="tabID">
                                <div>
                                <label for="">Usuario</label>
                                <input id="txtLogin" name="usuario" type="text" style="height:20px;" value="" size="25"
                                       onKeyPress="javascript:return teclearsoloDigitosYMinuscYMayusRayaPiso(event);"
                                        />
                               </div>
                                
                               <div>
                                <label for="">Contrase&ntilde;a</label>
                                <input id="txtContrasena" name="clave" type="password" style="height:20px" value="" size="25"
                                       onkeypress="checkKey(event);" placeholder=""  />
                               </div>
           
                               <div id="divBtn">
                                <!-- <input id="btn_ENTRAR" type="button" class="blue buttonini" value="INICIAR SESION"
                                style="width:190px" onclick="iniciar_sesion()"/> -->

                                <button id="btn_ENTRAR" type="button" class="blue buttonini" value="INICIAR SESION"
                                style="width:190px" onclick="iniciar_sesion()">
                                    Ingresar
                                    <div>
                                        <i class="fa-solid fa-chevron-right"></i>
                                    </div>
                                </button>


                                <img src="../utilidades/imagenes/ajax-loader.gif" id="loader_login" hidden>
                                <input type="hidden" disabled="no" id="format" width="70px"/>
                               </div>

                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
    </body>
</html>