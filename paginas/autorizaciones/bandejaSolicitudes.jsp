<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>
<table width="100%">
    <tr>
        <td>
            <table width="100%">
                <tr class="titulos">
                    <td style="width: 33%;">FECHA DESDE</td>
                    <td style="width: 33%;">FECHA HASTA</td>
                    <td style="width: 33%;">ESTADO</td>
                </tr>
                <tr class="estiloImputIzq2">
                    <td align="center">
                        <input type="date" value="<%=LocalDate.now()%>" size="10" maxlength="10" id="txtFechaDesdeBandeja" />
                    </td>
                    <td align="center">
                        <input type="date" value="<%=LocalDate.now()%>" size="10" maxlength="10" id="txtFechaHastaBandeja" />
                    </td>
                    <td>
                        <select id="cmbEstadoBandeja" style="width: 90%;" onchange="mostrarComboErrores(this.value)">
                            <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(3697);	
                            ComboVO cmbEs; 
                            for(int k=0;k<resultaux.size();k++){ 
                                    cmbEs=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbEs.getId()%>"><%= cmbEs.getDescripcion()%></option>
                        <%}%>
                        </select>
                    </td>
                </tr>
                <tr class="titulos">
                    <td style="width: 33%;">SEDE</td>
                    <td style="width: 33%;">ESPECIALIDAD</td>
                    <td style="width: 33%;">PROFESIONAL</td>                    
                </tr>
                <tr class="estiloImputIzq2">
                    <td style="width: 33%;">
                        <select id="cmbSedeBandeja" style="width: 90%;">
                            <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(963);	
                            ComboVO cmbS; 
                            for(int k=0;k<resultaux.size();k++){ 
                                    cmbS=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbS.getId()%>"><%= cmbS.getDescripcion()%></option>
                        <%}%>
                        </select>
                    </td>
                    <td style="width: 33%;">
                        <select size="1" id="cmbIdEspecialidadBandeja" style="width:90%"
                        onfocus="cargarEspecialidadDesdeSede('cmbSedeBandeja', 'cmbIdEspecialidadBandeja')" >
                        <option value=""></option>
                        </select>
                    </td>
                    <td style="width: 33%;">
                        <input type="text" id="txtMedicoBandeja" maxlength="200"  style="width:90%" oninput="llenarElementosAutoCompletarKey(this.id, 3698, this.value,valorAtributo('cmbIdEspecialidadBandeja'))" />
                        <img width="18px" height="18px" align="middle" title="VEN3" id="idLupitaVentanitaProce" onclick="traerVentanita(this.id,'','divParaVentanita','txtMedico','32')" src="/clinica/utilidades/imagenes/acciones/buscar.png">                                                         
                    </td>                
                </tr> 
                
                <tr id = "tituloErroresSoli" class="titulos" style="display:none">
                    <td style="width: 33%;">Tipo de error</td>                 
                </tr>
                <tr  id = "comboErroresSoli" class="estiloImputIzq2" style="display:none">
                    <td style="width: 33%;">
                        <select id="cmbErroresSolicitudes" style="width: 90%;">
                            <option></option>
                            <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(5000);	
                            ComboVO cmbErr; 
                            for(int k=0;k<resultaux.size();k++){ 
                                    cmbErr=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbErr.getId()%>"><%= cmbErr.getDescripcion()%></option>
                        <%}%>
                        </select>
                    </td>               
                </tr> 
                <tr class="titulos">
                    <td colspan="3">
                        <div id="divNormales">
                        <table id="listGrillaSolicitudes" class="scroll"></table>
                        </div>
                    </td>
                </tr>                                                                                                 
                <tr class="estiloImputIzq2">
                    <td colspan="3" align="CENTER">
                        <input type="button" class="small button blue" value="CONSULTAR"
                        onclick="GrillasSolicitudes()"/>                                                                                                                    
                    </td>
                </tr>                                                                                                                                                                                              
            </table>
        </td>                                        
    </tr>    
</table>

<div id="divModificarSolicitud" style="position:absolute; display:none; top:0px; left:0px; width:600px; height:90px; z-index:2055">
    <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"></div>
    <div style="z-index:2057; position:absolute; top:100px; left:200px; height:auto; width:800px">

        <table width="100%" class="fondoTabla">

            <tr class="estiloImput">
                <td align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divModificarSolicitud');ocultar('divJustificacionDevolucion');ocultar('errorIntegracion');ocultar('devoluciones');ocultar('anulaciones')" /></td>
                <td align="right"><input type="button" align="right" value="CERRAR" onclick="ocultar('divModificarSolicitud');ocultar('divJustificacionDevolucion');ocultar('errorIntegracion');ocultar('devoluciones');ocultar('anulaciones')" /></td>
            <tr>

            <tr class="titulosListaEspera">
                <td colspan="2" width="100%">NUMERO SOLICITUD: <label id="lblIdNumeroSolicitudEditar" style="color: red;"></label><label style="display: none;" id="lblEvolucionSolicitud"></label></td>                
            </tr>

            <tr class="titulosListaEspera" id="errorIntegracion" style="display: none;">
                <td colspan="2" width="100%">MENSAJE ERROR</td>
            </tr>

            <tr class="estiloImput" id="errorIntegracion" style="display: none;">
                <td colspan="2" width="100%" align="center">
                    <textarea id="txtMensajeError" style="width: 90%; height: auto; border:solid 1px black;"></textarea>
                </td>
            </tr>
            <tr class="titulosListaEspera" id="errorIntegracion" style="display: none;">
                <td>CAMPO ERROR</td>
                <td>VALOR ERROR</td>
            </tr>
            <tr class="estiloImput" id="errorIntegracion" style="display: none;">
                <td>
                    <textarea id="txtCampoError" style="width: 90%; height: auto; border:solid 1px black;"></textarea>
                </td>
                <td>
                    <textarea id="txtValorError" style="width: 90%; height: auto; border:solid 1px black;"></textarea>
                </td>
            </tr>
            <tr class="titulosListaEspera" id="devoluciones" style="display: none;">
                <td colspan="2">DEVOLUCIONES</td>
            </tr>
            <tr class="titulos"  id="devoluciones" style="display: none;">
                <td colspan="2">
                    <table id="listGrillaDevolucionesSolicitudes" class="scroll"></table>
                </td>
            </tr>
            <tr class="titulosListaEspera" id="anulaciones" style="display: none;">
                <td colspan="2">ANULACIONES</td>
            </tr>
            <tr class="titulos"  id="anulaciones" style="display: none;">
                <td colspan="2">
                    <table id="listGrillaAnulacionesSolicitudes" class="scroll"></table>
                </td>
            </tr>
            <tr class="titulosListaEspera">
                <td colspan="2">PROCEDIMIENTOS ORDENADOS</td>
            </tr>
            <tr class="titulos">
                <td colspan="2">
                    <table id="listProcedimientosOrdenadosSolicitud" class="scroll"></table>
                </td>
            </tr>
            <tr class="titulosListaEspera" id="anulaciones" style="display: none;">
                <td colspan="2">AGREGAR PROCEDIMIENTOS</td>
            </tr>
            <tr style="display: none;" id="anulaciones">
                <td colspan="2">
                    <table style="width: 100%;">
                        <tr class="titulosListaEspera">
                            <td style="width: 60%;">PROCEDIMIENTO</td>
                            <td style="width: 20%;">DIAGNOSTICO ASOCIADO</td>
                            <td style="width: 20%;">CANTIDAD</td>
                        </tr>
            
                        <tr class="estiloImput">
                            <td>
                                <input type="text" id="txtProcedimientoAgregarSolicitud" onkeypress="llenarElementosAutoCompletarKey(this.id, 221, this.value)"
                                style="width:90%">
                                <label hidden id="lblIdProcedimientoAgregar"></label>
                            </td>                
                            <td>
                                <select id="cmbProcedimientoDiagnotiscoAgregarSolicitud" style="width:95%; overflow-y: auto;" >
                                    <option value=""></option>
                                </select>
                            </td>
                            <td>
                                <input type="text" id="txtProcedimientoCantidadAgregarSolicitud">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <input id="btnTratamientoAgregar" type="button" class="small button blue" value="AGREGAR" onclick="modificarCRUD('AgregarProcedimientoSolicitudNueva');" >                                
                                <input id="btnTratamientoEliminar" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('EliminarProcedimientoSolicitudNueva');" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="titulos" id="anulaciones" style="display: none;">
                <td colspan="2">
                    <table id="listProcedimientosAuxiliares" class="scroll"></table>
                </td>
            </tr>
            <tr class="titulosListaEspera" id="anulaciones" style="display: none;">
                <td>DIAS ESPERA SOLICITUD</td>
                <td class="estiloImput">
                    <select id="cmbDiasEsperarSolicitud">
                        <option value="0">0 Dias - 1 Mes</option>
                        <option value="30">2 Meses</option>
                        <option value="60">3 Meses</option>
                        <option value="90">4 Meses</option>
                        <option value="120">5 Meses</option>
                        <option value="150">6 Meses</option>
                        <option value="180">7 Meses</option>
                        <option value="210">8 Meses</option>
                        <option value="240">9 Meses</option>
                        <option value="270">10 Meses</option>
                        <option value="300">11 Meses</option>
                        <option value="330">12 Meses</option>
                    </select>
                </td>
            </tr>
            <tr class="titulosListaEspera">
                <td colspan="2">JUSTIFICACION CLINICA</td>
            </tr>
            <tr class="estiloImput">
                <td colspan="2" width="100%" align="center">
                    <textarea id="txtJustificacionClinica" style="width: 90%; height: 200px; border:solid 1px black;"></textarea>
                </td>
            </tr>
            <tr class="titulosListaEspera">
                <td colspan="2">
                    <input type="button" id="reenviar" style="display: none;" class="small button blue" value="REENVIAR SOLICITUD" onclick="modificarCRUD('reenviarSolicitud');" />
                    <input type="button" id="anulaciones" style="display: none;" class="small button blue" value="GENERAR SOLICITUD" onclick="modificarCRUD('generarNuevaSolicitud');" />
                </td>
            </tr>
            
        </table>
    </div>        
    <div id="divJustificacionDevolucion" style="z-index:3000; position:absolute; top:150px; left:1000px; height:auto; width: 360px; display: none;">
    
            <table width="100%" class="fondoTabla">                
                <tr class="estiloImput">
                    <td>
                        <textarea id="txtJustificacionDevolucion" style="width: 345px; height: 115px; font-size: small;"></textarea>
                    </td>                
                </tr>
            </table>
    
    </div>
    <div id="divJustificacionAnulacion" style="z-index:3000; position:absolute; top:150px; left:1000px; height:auto; width: 360px; display: none;">
    
        <table width="100%" class="fondoTabla">                
            <tr class="estiloImput">
                <td>
                    <textarea id="txtJustificacionAnulacion" style="width: 345px; height: 115px; font-size: small;"></textarea>
                </td>                
            </tr>
        </table>
    </div>        
</div>

<div id="divEditarProcedimientosSolicitud" style="position:absolute; display:none; top:0px; left:0px; width:600px; height:90px; z-index:2055">
    <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"></div>
    <div style="z-index:2057; position:absolute; top:200px; left:200px; height:auto; width:800px">

        <table width="100%" class="fondoTabla">

            <tr class="estiloImput">
                <td colspan="2" align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divEditarProcedimientosSolicitud')" /></td>
                <td align="right"><input type="button" align="right" value="CERRAR" onclick="ocultar('divEditarProcedimientosSolicitud')" /></td>
            <tr>

            <tr class="titulosListaEspera">
                <td width="75%"><label id="lblProcedimientoEditarSolicitud"></label> </td>
                <td width="15%">CUPS: <label id="lblCupsProcedimientoEditarSolicitud"></label></td>
                <td width="10%">ID: <label id="lblIdProcedimientoEditarSolicitud"></label></td>                
            </tr>
            <tr class="titulosListaEspera">
                <td>PROCEDIMIENTO</td>
                <td>DIAGNOSTICO ASOCIADO</td>
                <td>CANTIDAD</td>
            </tr>

            <tr class="estiloImput">
                <td>
                    <input type="text" id="txtProcedimientoCupsEditarSolicitud" onkeypress="llenarElementosAutoCompletarKey(this.id, 3701, this.value)"
                    style="width:90%">
                </td>                
                <td>
                    <select id="cmbProcedimientoDiagnotiscoEditarSolicitud" style="width:95%; overflow-y: auto;" >
                        <option value=""></option>
                    </select>
                </td>
                <td>
                    <input type="text" id="txtProcedimientoCantidadEditarSolicitud">
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <input id="btnTratamientoEliminar" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarProcedimientoSolicitud');" >
                    <input id="btnTratamientoEditar" type="button" class="small button blue" value="EDITAR" onclick="modificarCRUD('modificarProcedimientoSolicitud');" />
                </td>
            </tr>

        </table>

    </div>
</div>