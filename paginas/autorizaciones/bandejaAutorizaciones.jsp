<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>
<table width="100%">
    <tr>
        <td>
            <table width="100%">
                <tr class="titulos">
                    <td style="width: 33%;">FECHA DESDE</td>
                    <td style="width: 33%;">FECHA HASTA</td>
                    <td style="width: 33%;">TIPO SOLICITUD</td>
                </tr>
                <tr class="estiloImputIzq2">
                    <td align="center">
                        <input type="date" value="<%=LocalDate.now()%>" size="10" maxlength="10" id="txtFechaDesdeBandejaAutorizacion" />
                    </td>
                    <td align="center">
                        <input type="date" value="<%=LocalDate.now()%>" size="10" maxlength="10" id="txtFechaHastaBandejaAutorizacion" />
                    </td>
                    <td>
                        <select id="cmbTipoAutorizacion" style="width: 90%;">
                            <option value="1">AUTORIZACIONES A COOEMSSANAR IPS</option>
                            <option value="2">AUTORIZACIONES DE SOLICITUDES EXTERNAS</option>
                            <option value="3">AUTORIZACIONES A EXTERNAS DESDE COOEMSSANAR IPS</option>
                        </select>
                    </td>
                </tr>
                <tr class="titulos">
                    <td style="width: 33%;">SEDE</td>
                    <td style="width: 33%;">NUMERO DE SOLICITUD(ORIGEN)</td>
                    <td style="width: 33%;">IDENTIFICACION PACIENTE</td>                    
                </tr>
                <tr class="estiloImputIzq2">
                    <td style="width: 33%;">
                        <select id="cmbSedeBandejaAutorizaciones" style="width: 90%;">
                            <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(963);	
                            ComboVO cmbS; 
                            for(int k=0;k<resultaux.size();k++){ 
                                    cmbS=(ComboVO)resultaux.get(k);
                            %>
                        <option value="<%= cmbS.getId()%>"><%= cmbS.getDescripcion()%></option>
                        <%}%>
                        </select>
                    </td>
                    <td style="width: 33%;">
                        <input type="text" id="txtNumeroSolicitudBuscar" style="width: 90%;" onkeypress="javascript:return soloTelefono(event);">
                    </td>
                    <td style="width: 33%;">
                        <input type="text" id="txtIdentificacionBuscar" style="width: 90%;" onkeypress="javascript:return soloTelefono(event);">
                    </td>                
                </tr> 
                <tr class="titulos">
                    <td colspan="3">
                        <div id="divNormales">
                        <table id="listGrillaAutorizaciones" class="scroll"></table>
                        </div>
                    </td>
                </tr>                                                                                                 
                <tr class="estiloImputIzq2">
                    <td colspan="3" align="CENTER">
                        <input type="button" class="small button blue" value="CONSULTAR"
                        onclick="buscarHC('listGrillaAutorizaciones')"/>                        
                    </td>
                </tr>                                                                                                                                                                                              
            </table>
        </td>                                        
    </tr>    
</table>

<div id="divDetalleAutorizacion" style="position:absolute; display:none; top:0px; left:0px; width:600px; height:90px; z-index:2055">
    <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"></div>
    <div style="z-index:2057; position:absolute; top:100px; left:200px; height:auto; width:800px">

        <table width="100%" class="fondoTabla">

            <tr class="estiloImput">
                <td align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divDetalleAutorizacion');ocultar('divNotasAutorizacion')" /></td>
                <td align="right"><input type="button" align="right" value="CERRAR" onclick="ocultar('divDetalleAutorizacion');ocultar('divNotasAutorizacion')" /></td>
            <tr>

            <tr class="titulosListaEspera">
                <td colspan="2" width="100%">NUMERO AUTORIZACION: <label id="lblNumeroAutorizacion" style="color: red;"></label><label style="display: none;" id="lblIdAutorizacion"></label></td>                
            </tr>
            
            <tr class="titulosListaEspera">
                <td>PRESTADOR</td>
            </tr>
            <tr class="titulos" >
                <td colspan="2">
                    <table id="listDetalleAutorizacionPrestador" class="scroll"></table>
                </td>
            </tr>            
            <tr class="titulosListaEspera">
                <td colspan="2">TECNOLOGIAS AUTORIZADAS</td>
            </tr>
            <tr class="titulos">
                <td colspan="2">
                    <table id="listDetalleAutorizacionProcedimiento" class="scroll"></table>
                </td>
            </tr>            
        </table>
    </div>        
    <div id="divNotasAutorizacion" style="z-index:3000; position:absolute; top:150px; left:1000px; height:auto; width: 360px; display: none;">    
            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td>
                        <label id="lblProcedimientoAutorizacion"></label>
                    </td>
                </tr>                
                <tr class="estiloImput">
                    <td>
                        <textarea id="txtNotaAutorizacion" style="width: 345px; height: 115px; font-size: small;"></textarea>
                    </td>                
                </tr>
            </table>    
    </div>      
</div>