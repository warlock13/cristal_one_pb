<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.*,java.text.*,java.sql.*" %>
<%@ page import="Sgh.Utilidades.*" %>
<%@ page import="Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import="Clinica.Presentacion.*" %>
<%@ page import="java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% beanAdmin.setCn(beanSession.getCn()); ArrayList resultaux=new ArrayList(); %>

<table width="100%">
    <tr class="titulos" align="center">
        <td colspan="2">Paciente</td>
        <td width="15%">Fecha Desde</td>
        <td width="15%">Fecha Hasta</td>
    </tr>
    <tr class="estiloImput">
        <td  style="width: 40%;">
            <select size="1" id="cmbTipoId" style="width:50%" >
                <option value="CC">Cedula de Ciudadania</option>
                <option value="CE">Cedula de Extranjeria</option>
                <option value="MS">Menor sin Identificar</option>
                <option value="PA">Pasaporte</option>
                <option value="RC">Registro Civil</option>
                <option value="TI">Tarjeta de identidad</option>
                <option value="CD">Carnet Diplomatico</option>
                <option value="NV">Certificado nacido vivo</option>
                <option value="AS">Adulto sin Identificar</option>
            </select>
            <input type="text" id="txtIdentificacion" placeholder="N&uacute;mero de Documento" size="21" maxlength="20"
                onKeyPress="javascript:checkKey2(event);javascript:return soloTelefono(event);"  style="width: 30%;"/>
            <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT"
                onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
            <div id="divParaVentanita"></div>
        </td>
        <td style="width: 30%;">
            <input type="text" maxlength="70" id="txtIdBusPaciente" style="width:99%"  />
        </td>
        <td style="width: 15%;"><input type="date" value="<%=LocalDate.now()%>" size="10" maxlength="10" id="txtDocFechaDesde" /></td>
        <td style="width: 15%;"><input type="date" value="<%=LocalDate.now()%>" size="10" maxlength="10" id="txtDocFechaHasta" /></td>
    </tr>
    <tr class="titulos">
        <td width="20%">Sede</td>
        <td width="25%">Especialidad</td>
        <td width="20%">Profesional</td>
    </tr>
    <tr class="estiloImput">
        <td>
            <select size="1" id="cmbSede" style="width:60%" title="GD38" onfocus="comboDependienteSede('cmbSede','557')"
                ochange="asignaAtributo('cmbSede', '', 0);">
            </select>
        </td>
        <td><select size="1" id="cmbIdEspecialidad" style="width:60%"
                onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')" >
                <option value=""></option>
            </select>
        </td>
        <td><select size="1" id="cmbIdProfesionales" onfocus="cargarProfesionalesDesdeEspecialidadSede(this.id)"
                style="width:95%" >
                <option value=""></option>
            </select>
        </td>
        <td>
            <input name="btn_busDoc" title="BT56E" type="button" class="small button blue" value="BUSCAR FOLIOS"
                onclick="buscarHC('listFoliosProcedimientos','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');" />
        </td>
    </tr>
    <tr>
        <td colspan="5" id="idContenedorGrillaDoc" width="100%" height="400" class="titulos">
            <table id="listFoliosProcedimientos" class="scroll"></table>
        </td>
    </tr>
    <label hidden id="lblIdDocumento"></label>
    <label hidden id="lblIdPaciente"></label>
    <label hidden id="lblIdentificacionPaciente"></label>
</table>
<table width="100%">
    <tr class="titulos">
        <td colspan="6">
            <table id="listaProcedimientoConductaTratamientoS" class="scroll"></table>
        </td>
    </tr>
    <tr class="titulos">
        <td style="width: 90%;">
            <center>Justificaci&oacute;n</center>
            <textarea type="text" id="txtJustificacionFolio" rows="2" maxlength="3000" style="width:90%; height: 150px;"
                > </textarea>
            <input style="display: none;" id="txtIdProcedimiento" />
        </td>
        <td>
            <center>Esperar Procedimiento</center>
            <select id="cmbEsperarProcAutoriza" size="1" style="width:60%"  onchange="modificarCRUD('cambiarDiasEsperaProcedimiento')">
                <option value="0">0 Dias - 1 Mes</option>
                <option value="30">2 Meses</option>
                <option value="60">3 Meses</option>
                <option value="90">4 Meses</option>
                <option value="120">5 Meses</option>
                <option value="150">6 Meses</option>
                <option value="180">7 Meses</option>
                <option value="210">8 Meses</option>
                <option value="240">9 Meses</option>
                <option value="270">10 Meses</option>
                <option value="300">11 Meses</option>
                <option value="330">12 Meses</option>
            </select>
        </td>
    </tr>
    <tr class="titulos">
        <td colspan="2">
            <input type="button" class="small button blue" value="Ver Historia Clinica" onclick="formatoPDFHC()" />
            <input id="idBtnSolicitAutoriza" title="AUT34" type="button" class="small button blue"
                value="Generar Solicitud" onClick="verificarNotificacionAntesDeGuardar('enviaSolicitud')" />
            <input id="idBtnSolicitAutoriza" title="AUT34" type="button" class="small button blue"
                value="Generar Voucher" onClick="generarVoucher(valorAtributo('lblNumeroSolicitudVoucher'))" />
            <input id="idBtnSolicitAutoriza" title="AUT34" type="button" class="small button blue"
                value="Imprimir Orden Medica" onClick="imprimirOrden()" />
            <input id="idBtnSolicitAutoriza" title="AUT34" type="button" class="small button blue"
                value="Imprimir Medicamentos" onClick="imprimirMedicamentos()" />
        </td>
    </tr>
</table>

<label hidden id="lblIdDocumento"></label>
<label hidden id="lblTipoDocumento"></label>
<label hidden id="lblSolicitud"></label>
<label hidden id="lblPaciente"></label>
<label hidden id="lblMedico"></label>
<label hidden id="txtNumeroSolicitud"></label>
<label hidden id="lblTipoDiagnostico"></label>
<label hidden id="lblDiagnosticoP"></label>
<label hidden id="lblDiagnosticoR"></label>
<label hidden id="lblJustificacion"></label> 
<label hidden id="lblNumeroSolicitudVoucher"></label>

<input type="text" style="display: none;" id="txtTipoCita"/>
<input type="text" style="display: none;" id="txtProfesion"/>

<div id="divVentanitaEditProcedimientos" style="position:absolute; display:none; top:0px; left:0px; width:600px; height:90px; z-index:2055">
    <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"></div>
    <div style="z-index:2057; position:absolute; top:400px; left:200px; height:auto; width:800px">

        <table width="100%" class="fondoTabla">

            <tr class="estiloImput">
                <td colspan="2" align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditProcedimientos')" /></td>
                <td colspan="2" align="right"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditProcedimientos')" /></td>
            <tr>

            <tr class="titulosListaEspera">
                <td width="10%">ID: <label id="lblIdProcedimientoEditar"></label></td>
                <td width="15%">CUPS: <label id="lblCupsProcedimientoEditar"></label></td>
                <td width="45%"> <label id="lblProcedimientoEditar"></label> </td>
                <td width="30%">CLASE: <label id="lblProcedimientoClaseEditar"></label></td>
            </tr>


            <tr class="titulosListaEspera">
                <td>CANTIDAD</td>
                <td>GRADO NECESIDAD</td>
                <td>INDICACI&Oacute;N</td>
                <td>DIAGNOSTICO ASOCIADO</td>
            </tr>

            <tr class="estiloImput">
                <td>
                    <select id="cmbCantidadProcedimientoEditar" style="width:90%">
                        <option value=""></option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                    </select>
                </td>
                    <td>
                        <select id="cmbNecesidadProcedimientoEditar" style="width:90%" >
                            <option value=""></option>
                            <%resultaux.clear();
                                resultaux = (ArrayList) beanAdmin.combo.cargar(113);
                                ComboVO cmbProE;
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmbProE = (ComboVO) resultaux.get(k);%>
                            <option value="<%= cmbProE.getId()%>" title="<%= cmbProE.getTitle()%>"><%=cmbProE.getDescripcion()%></option><%}%>           
                        </select> 
                    </td>
                <td>
                    <textarea id="txtIndicacionProcedimientoEditar" style="width:90%" maxlength="5000"  onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea>
                </td>
                <td>
                    <select id="cmbProcedimientoDiagnotiscoEditar" style="width:95%; overflow-y: auto;"  onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');">
                        <option value=""></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <input id="btnTratamientoEditar" type="button" class="small button blue" value="EDITAR" onclick="modificarCRUD('listProcedimientosDetalleEditar');" />        
                </td>
            </tr>
        </table>
    </div>
</div>