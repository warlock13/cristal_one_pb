<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1250px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="SOLICITUDES Y AUTORIZACIONES" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>                        
                        <div id="tabsAutorizaciones" style="width:98%; height:auto">
                            <ul>
                                <li>
                                    <a href="#divFoliosProcedimientos">
                                        <center>GENERAR SOLICITUDES</center>
                                    </a>
                                </li>
                                <li>
                                    <a href="#divSolicitudes">
                                        <center>LISTADO SOLICITUDES</center>
                                    </a>
                                </li>
                                <li>
                                    <a href="#divAutorizaciones">
                                        <center>LISTADO DE AUTORIZACIONES</center>
                                    </a>
                                </li>                                
                            </ul>

                            <div id="divFoliosProcedimientos">
                                <jsp:include page="generarSolicitudes.jsp" flush="true"  />
                            </div>
                            <div id="divSolicitudes">
                                <jsp:include page="bandejaSolicitudes.jsp" flush="true"  />
                            </div>
                            <div id="divAutorizaciones">
                                <jsp:include page="bandejaAutorizaciones.jsp" flush = "true" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
