<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Pragma" CONTENT="no-cache" />
    <title>CRISTAL WEB</title>
    <link REL="SHORTCUT ICON" HREF="../utilidades/css/favicon.png" />
    <link href="../utilidades/css/estiloClinica.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/clinica/utilidades/css/jquery.treeview.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js"></script>
    <script src="../utilidades/javascript/sweetalert2-11.4.8/dist/sweetalert2.all.min.js"></script>
    <script src="../utilidades/javascript/login.js"></script>
</head>

<body>
    <div class="container" style="text-align: center">
        <img src="../logo.png" width="256px" height="120px" style="margin-left:7%; margin-top:1%;" alt="Cl�nica">
        <hr>
        <label for="">Ya tiene una sesion activa</label>
        <hr>
        <a rel="stylesheet" href="/clinica/paginas/login.jsp">Intentar nuevamente</a>
        <hr>
        <input type="hidden" disabled="no" id="format" width="70px" />
    </div>
</body>

</html>