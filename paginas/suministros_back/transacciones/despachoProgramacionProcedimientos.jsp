<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<!-- instanciar bean de session -->
<%  beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux=new ArrayList();
    ComboVO cmb;
%>
<table width="1320px" height="1000px" align="center" style="border: 1px solid black;" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="DESPACHO A QUIROFANOS"/>
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO  aqui empieza el cambio-->
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="100%" height="1000px" cellpadding="0" cellspacing="0" class="fondoTabla" border="0">
                <tr>
                    <td valign="top">
                        <div style="overflow:auto; height:1100px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <table width="100%" border="0">
                                <tr>
                                    <td valign="top" width="90%">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr class="titulos">
                                                <td width="30%">Sede</td>
                                                <td width="30%">Especialidad</td>
                                                <td width="40%">Profesional</td>
                                            </tr>
                                            <tr class="estiloImput">
                                                <td>
                                                    <select size="1" id="cmbSede" style="width:90%" title="GD38"
                                                        onfocus="comboDependienteSede('cmbSede','557')"
                                                        onchange="asignaAtributo('cmbIdEspecialidad', '', 0);
                                                        asignaAtributo('cmbIdProfesionales', '', 0);
                                                        cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad')">
                                                    </select>
                                                </td>
                                                <td><select id="cmbIdEspecialidad"
                                                        onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"
                                                        style="width:90%" 
                                                        onchange="limpiaAtributo('cmbIdProfesionales');
                                                                  buscarAGENDA('listDiasProgramacion')">
                                                        <option value=""></option>


                                                    </select>
                                                </td>
                                                <td><select size="1" id="cmbIdProfesionales" style="width:90%"
                                                        onfocus="cargarProfesionalesDesdeEspecialidadSede(this.id)"
                                                         
                                                        onchange="buscarAGENDA('listDiasProgramacion')">
                                                        <option value=""></option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="titulos">
                                                <td colspan="4">
                                                    <div id="divListAgenda">
                                                        <table id="listAgenda"></table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="40%" valign="top">
                                        <table border="0">
                                            <tr id="divAjustarEspacioAgenda" hidden>
                                                <td>
                                                    <div style="height: 100px;"></div>
                                                </td>
                                            </tr>
                                            <tr class="estiloImputListaEspera">
                                                <td align="CENTER">
                                                    <img width="15" height="15" style="cursor:pointer"
                                                        src="/clinica/utilidades/imagenes/acciones/izquierda.png"
                                                        onclick="irMesAnteriorAgenda(); buscarAGENDA('listDiasProgramacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                                    <label id="lblMes"></label>/<label id="lblAnio"></label>
                                                    <img width="15" height="15" style="cursor:pointer"
                                                        src="/clinica/utilidades/imagenes/acciones/derecha.png"
                                                        onclick="irMesSiguienteAgenda(); buscarAGENDA('listDiasProgramacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                                    <input name="btn_cerrar" type="button" class="small button blue" align="right"
                                                        value="Refrescar"
                                                        onclick="buscarAGENDA('listDiasProgramacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                                    Exito:
                                                    <select size="1" id="cmbExito" style="width:20%;"
                                                        onchange="buscarAGENDA('listDiasProgramacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                                                        >
                                                        <option value="S">Si</option>
                                                        <option value="N">No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="CENTER">
                                                    <label id="lblFechaSeleccionada"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="divListadoDias" style="width:100%">
                                                        <table id="listDiasProgramacion" class="scroll"></table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>" />
        </td>
    </tr>
</table>


  <div id="divVentanitaAgenda" style="position:absolute; display:none; background-color:#E2E1A5; top:200px; left:70px; width:95%; height:90px; z-index:999">
        <table width="100%"    class="fondoTabla" border="0">           
           <tr class="estiloImput" >
               <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgenda');buscarAGENDA('listAgendaProgramacion');" /></td>  
               <td  align="CENTER" colspan="2" ><div style="display:none"> <label id="lblUsuariosDato"></label> </div></td>
               <td align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAgenda');buscarAGENDA('listAgendaProgramacion');" /></td>  
           <tr>     
           <tr class="estiloImput" >
               <td >Cita=<label id="lblIdAgendaDetalle"></label></td>            
               <td >&nbsp;</td>
               <td >&nbsp;</td> 
               <td >&nbsp;</td> 
           <tr> 
           <tr class="estiloImput" style="display:none">        
               <td colspan="1" >
                     <label id="lblIdListaEspera"></label>
               </td>      
               <td colspan="3" >
                    <input type="text" size="110"  maxlength="210"  id="txtIdBusPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente',307)"  style="width:70%"   /> 
               </td>    
           </tr> 
 
           <tr class="estiloImputListaEspera">
             <td colspan="4">
<!-- DATOS BASICOS PACIENTE-->              
               <table width="100%"  cellpadding="0" cellspacing="0" >
                 <tr>
                   <td width="10%" >Tipo Id</td>
                   <td >Identificacion</td>
                   <td >Apellidos</td>  
                   <td >Nombres</td>                      
                   <td >Fech.Nacim</td>   
                   <td >Sexo</td>                                            
                 </tr>               
               
                 <tr>
                   <td width="10%">

					<select size="1" id="cmbTipoId" style="width:55%"  >
                       <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                               ComboVO cmb105; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb105=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>"><%= cmb105.getId()+"  "+cmb105.getDescripcion()%></option>
                                    <%}%>						
                   </select></td> 
                   <td  ><input  type="text" id="txtIdentificacion"  style="width:80%"   size="20" maxlength="20"  onKeyPress="javascript:checkKey2(event);javascript:return soloTelefono(event);"   />
                   </td>
                   <td>
                     <input type="text" id="txtApellido1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />
                     <input type="text" id="txtApellido2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />
                   </td> 
                   <td>
                     <input type="text" id="txtNombre1" title="BF67" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"   style="width:45%" />
                     <input type="text" id="txtNombre2" title="BGH6" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />  
                   </td>   
                   <td><input id="txtFechaNac"  type="text"  size="10"  maxlength="10"  /> </td>                      	 
                   <td> <select size="1" id="cmbSexo" style="width:95%"  >	                                        
                          <option value="F">FEMENINO</option>
                          <option value="M">MASCULINO</option>                          
                          <option value="O">OTRO</option>                                                    
                     </select>
                   </td>
                 </tr>
               </table>       
<!-- FIN DATOS BASICOS -->        
             </td>  
           <tr> 
           <tr class="estiloImput" style="display:none">
               <td><input type="text" id="txtMunicipio" size="60"  maxlength="60"  oninput="llenarElementosAutoCompletarKey(this.id, 3704, this.value)" style="width:80%"    /></td> 
               <td><input type="text" id="txtDireccionRes" size="100" maxlength="100"  style="width:99%"  onkeypress="javascript:return teclearsoloan(event);"  /></td>   
               <td><input type="text" id="txtNomAcompanante"  size="100" maxlength="100"  style="width:95%"  onkeypress="javascript:return teclearExcluirCaracter(event);"   /> </td>                    
               <td>
                   <input type="text" id="txtCelular1" size="30"  style="width:30%" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  />
                   <input type="text" id="txtTelefonos" size="100"  style="width:30%" maxlength="100" onkeypress="javascript:return teclearsoloan(event);"  />                    
                   <input type="text" id="txtCelular2" size="30"  style="width:30%" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  />
               </td>                                       
           <tr>
            <tr>
                <td>
                  <table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0" class="fondoTabla">
                    <tr>
                      <td class="tdTitulo">
                        <p class="pTitulo">INFORMACION DE LA CIRUJIA</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
           <tr class="estiloImput" >
               <td colspan="4">
                   <TABLE width="100%">           
                       <tr class="titulos" >
                           <td width="25%">Tipo Anestecia</td>   
                           <td width="25%">Lente intraocular</td>
                       </tr>                 
                       <tr class="estiloImput" >
                             
                           <td>
                             <select id="cmbTipoAnestesia" style="width:80%"  >
                               <option value=""></option>
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(119);	
                                         ComboVO cmbTA; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmbTA=(ComboVO)resultaux.get(k);
                                  %>
                               <option value="<%= cmbTA.getId()%>" title="<%= cmbTA.getTitle()%>"><%= cmbTA.getDescripcion()%></option>
                                         <%}%>
                             </select>               
                           </td>  
                           <td><input type="text" id="txtNoLente"  size="60"  maxlength="60"  style="width:80%"   />
                             <img id="imgObservacion" width="15" height="15" style="cursor:pointer" src="/clinica/utilidades/imagenes/acciones/help.gif"  >  
                           </td>
                       </tr> 
             
                      <input type="hidden" id="txtInstrumentador"  size="200" maxlength="200"  style="width:98%"  />
                           <input type="hidden" id="txtCirculante"  size="200" maxlength="200"  style="width:98%"  />

                           <input type="hidden" title="RY67" id="txtAnestesiologo"  size="200" maxlength="200"  style="width:78%"  />               
                       
                           <tr class="titulos">   

              <td width="100%" colspan="3">
              <div style="overflow:auto; height:100px; width:100%" id="divPersonalCirugia">             
                   <table id="listPersonalCirugia" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
              </div>    
              </td>
           </tr> 


           <tr>
            <td>
              <table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0" class="fondoTabla">
                <tr>
                  <td class="tdTitulo">
                    <p class="pTitulo">PROCEDIMIENTO CIRUJIA</p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
                       <tr class="titulos">   
                          <td width="100%" colspan="4">
                          <div style="overflow:auto; height:130px; width:100%" id="divCitaCirugiaProcedimientos">             
                              <table id="listCitaCirugiaProcedimiento" class="scroll"></table>  
                          </div>    
                          </td>
                       </tr>  
                                                                               
                   </TABLE>

               </td>
            </tr>     

  
          
          
          
          <tr class="titulos">
              <td colspan="4" width="100%">
               <!--   <div id="divEditar" style="display:block; width:100%" >   -->                                 
                            <div id="divAcordionDocTransacciones" style="display:BLOCK">      
                                     <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                                            <jsp:param name="titulo" value="Documento y Transacciones" />
                                            <jsp:param name="idDiv" value="divAcordionDocumentoTransacciones" />
                                            <jsp:param name="pagina" value="contenidoDocumentosTransacciones.jsp" />                
                                            <jsp:param name="display" value="NONE" />  
                                            <jsp:param name="funciones" value="cargarDocumentosYTransaccionesDespachosProcedimientos()" />                                                                                                                                                   
                                     </jsp:include> 
                            </div>                                       
                <!--  </div> divEditar-->                 
              </td>   
          </tr>   






                   
        </table> 
  </div>





<!-------------------------para la vista previa---------------------------- -->



    <input type="hidden" id="txt_banderaOpcionCirugia" value="PROG_FARMACIA" />              
    <input type="hidden"  id="txtIdDocumentoVistaPrevia" value="" />
    <input type="hidden"  id="txtTipoDocumentoVistaPrevia" value="" />                        
    <input type="hidden"  id="lblTituloDocumentoVistaPrevia" value=""  />    
    <input type="hidden"  id="lblIdAdmisionVistaPrevia" value=""  />                
<!-------------------------fin para la vista previa---------------------------- -->      
