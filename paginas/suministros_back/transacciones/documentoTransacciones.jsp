
<!-- ARCHIVO EN DOCUMENTOS PROGRAMACION -->
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr>
      <td>                          
         <table width="100%">
            <tr   class="estiloImputIzq2"> 
              <td>Bodega:
                  <select size="1" id="cmbIdBodega" style="width:48%"  onfocus="comboBodegasDespachos(this.id,'516')" onchange="buscarHistoria('listHojaGastosBodegas')">
                    <option value="1">Farmacia</option> 
                  </select>                
              </td>                      
              <td>ID DOCUMENTO:<label id="lblIdDoc"></label> </td>
              <td> ESTADO=<label id="lblIdEstado"></label></td>                                             
              <td>TIPO:</td>
              <td><select id="cmbIdTipoDocumento" style="width:80%" >
                      <option value="7">EGRESO POR CONSUMO</option>
                  </select>
                  <input type="hidden" id="txtNaturaleza" value ="S" />
              </td>			  
            </tr>
            <tr class="titulos">
               <td colspan="5" align="center">
                 <input class="small button blue" value="NUEVO" title="BCR65"  type="button" onclick="guardarYtraerDatoAlListado('nuevoDocumentoInventarioProgramacion')" >
                 <input class="small button blue" value="BUSCAR" title="Btb65"  type="button" onclick="buscarSuministros('listGrillaDocumentosBodegaProgramacion');" >
                 <input class="small button blue" value="CERRAR"  title="BT945" type="button" onClick="guardarYtraerDatoAlListado('cerrarDocumentoProgramacion')" >                                
                 <input name="btn_cierra" title="BTRim" type="button" class="small button blue" value="IMPRIME" onclick="impresionDocFarmaciaPaciente();"   />  
               </td>
            </tr>                                               
        </table>
        <table id="listGrillaDocumentosBodegaProgramacion" class="scroll"></table>    
      </td>  
    </tr>  
  </table> 
  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr>
      <td>
          <table width="100%" cellpadding="0" class="tablaTitulo"
              cellspacing="0" class="fondoTabla">
              <tr>
                  <td class="tdTitulo">
                      <p class="pTitulo">TRANSACCIONES</p>
                  </td>
              </tr>
          </table>
      </td>
  </tr>
    <tr>
      <td>
        <table width="100%">            	
            <tr   class="estiloImputIzq2">                         
              <td width="8%" align="right">ID TRANSACCION:</td>
              <td width="8%">  
                    <label id="lblIdTransaccion"></label>			  					
              </td> 
              <td width="8%" align="right">NATURALEZA:</td>
              <td width="8%">  
                    <label id="lblNaturaleza">S</label>			  					
              </td>                                             
            </tr>								                                                
                                                     
             <tr class="estiloImputIzq2"> 
                <td colspan="4" align="center">
                    <table  width="100%"  cellpadding="0"   align="center">
                        <tr class="estiloImputIzq2"> 
                          <td colspan="8">
                           <table  width="100%"  cellpadding="0"   align="center">
                            <tr class="estiloImputIzq2"> 	
                                <td width="4%" >Articulo:</td>
                                <td width="20%" > 	              	
                                  <input type="text" id="txtIdArticulo" style="width:90%"   />
                                  <img width="18px" height="18px" align="middle" src="/clinica/utilidades/imagenes/acciones/buscar.png" onclick="traerVentanitaArticulo('txtIdArticulo','11')" title="VEN240">
                                  <div id="divParaVentanitaArticulo"></div>								
                                </td>  
                                <td width="7%">LOTE: </td>
                                <td width="7%">
                                    <select id="cmbIdLote" style="width:95%"  onfocus="cargarLote()" onblur="traerExistenciaLote()">
                                        <option value=""></option>
                                    </select>
                                    <input type="hidden" id="txtFV" />
                                </td> 
                                <td width="7%">Existencias: </td>
                                <td width="7%"><label id="lblExistencias">0</label></td> 
                                <td width="7%">Cantidad</td>
                                <td width="7%"><input type="text" id="txtCantidad" style="width:90%"  onKeyPress="javascript:return soloNumeros(event)"  onkeyup="calcularImpuestoSalida()" onblur="calcularImpuestoSalida()"/></td>
                                <td width="7%">Serial :</td>
                                <td width="10%"><input type="text" id="txtSerial" style="width:70%" /></td>
                            </tr>
                           </table> 
                          </td>  
                        </tr>
                        <tr>
                          <td>
                              <div style="display:none">
                                  <table>
                                    <tr class="estiloImputIzq2">  
                                        <td width="10%"> Valor Unitario:</td>
                                        <td width="7%"><label id="lblValorUnitario">0</label></td>
                                        <td width="7%">IVA VENTA:</td>
                                        <td width="12%"><label id="lblIva">0</label> %</td>
                                        <td width="10%">Impuesto IVA $:</td>
                                        <td> <label id="lblValorImpuesto"></label></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    
                                    <tr class="estiloImputIzq2" >  
                                        <td >SubTotal $:</td>
                                        <td colspan="3"><label id="lblSubTotal">0</label></td>
                                        <td >Total $:</td>
                                        <td colspan="3"> <label id="lblTotal">0</label></td>
                                    </tr>
                                  </table>
                              </div>			
                          </td>	 
                        </tr>		  
                    </table>
                </td>
                
                                                          
             </tr>			 		  				
             
             <tr>
               <td colspan="4" align="center">
                 <input id="btn_limpia" title="btn_78TR"class="small button blue" type="button" value="Limpiar"  onclick="limpiarDivEditarJuan('limpCamposTransaccion')">              
                 <input id="btn_crea" title="btn_TR60" class="small button blue" type="button"  value="Crear"  onclick="modificarCRUD('crearTransaccion');" >                               
                 <input name="btn_modifica" title="btn_TR75" class="small button blue" type="button" value="Modificar" onclick="modificarCRUD('modificaTransaccion');"   />  
                 <input name="btn_elimina" title="btn_tr8" type="button" class="small button blue" value="Eliminar" onclick="modificarCRUD('eliminaTransaccion');"  />          
               </td>
             </tr>                                                     
        </table>
        <table id="listGrillaTransaccionDocumento" class="scroll"></table> 	  
      </td>
    </tr>
    <tr>  
      <td>&nbsp;  
      </td>
    </tr>    
    <tr>  
      <td>
    <div id="divAcordionDocTransaccionesCanasta" style="display:BLOCK">      
         <jsp:include page="divsAcordeon.jsp" flush="FALSE" >
                <jsp:param name="titulo" value="Canasta" />
                <jsp:param name="idDiv" value="divAcordionDocumentoTransaccionesCanasta" />
                <jsp:param name="pagina" value="documentoTransaccionesCanasta.jsp" />                
                <jsp:param name="display" value="NONE" />  
         </jsp:include> 
    </div>    
      </td>
    </tr>  
   </table>
   
  <input type="hidden" id="txtBanderaDevolucion" value ="NO" />     
    <input type="hidden" id="txtIdLote1" value="" />
     <input type="hidden" id="txtIdLoteBan" value="NO" />