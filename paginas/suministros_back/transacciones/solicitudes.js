function validarFechaVencimiento(){
    let infoLotes = document.getElementById('cmbIdLote')
    let infoLote = infoLotes.options[infoLotes.selectedIndex].text;
    let fechaVencimiento = infoLote.split("FECHA_VENCIMIENTO:")[1].split('-');
    let diaVencimiento =  parseInt(fechaVencimiento[2]);
    let mesVencimiento = parseInt(fechaVencimiento[1]);
    let anioVencimiento  = parseInt(fechaVencimiento[0]);
    let fecha = new Date(); 
    let dia = parseInt(fecha.getDate()); 
    let mes = parseInt(fecha.getMonth())+1;
    let anio = parseInt(fecha.getFullYear());
    console.log(dia, mes, anio);
    console.log(diaVencimiento, mesVencimiento, anioVencimiento);
    if(anio > anioVencimiento){
        return 'false'
    }
    if(anio === anioVencimiento && mes > mesVencimiento){
        return 'false'
    }
    if(anio === anioVencimiento && mes === mesVencimiento && dia > diaVencimiento){
        return 'false'
    }
    if(anio === anioVencimiento && mes === mesVencimiento && dia === diaVencimiento){
        return 'warning'
    }

    
}

function validarCantidad(){
    let infoCantidades = document.getElementById('cmbIdLote');
    let infoCantidad = infoCantidades.options[infoCantidades.selectedIndex].text;
    let cantidad = infoCantidad.split("EXISTENCIAS:")[1];
    let cantidadFinal = parseInt(cantidad.split("::"));
    if(cantidadFinal < parseInt(valorAtributo('txtCantidad'))){
        return 'false';
    }
}

function modificarCRUDDespachoPedidos(arg){
    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {
        case 'enviarProductosASalida':
            if(valorAtributo('lblTipoSolicitud') === 'D'){
                alert('Haga doble clic en la fila para realizar la devolucion de este producto');
                return
            }else{
                var mensaje_error = '';
                if(validarFechaVencimiento() === 'false'){
                    mensaje_error = 'ADVERTENCIA#1. El lote del producto esta vencido, selecciona otro lote\n';
                } 
                if(validarFechaVencimiento() === 'warning'){
                    mensaje_error = mensaje_error + 'ADVERTENCIA#2. El producto vence el dia de hoy, selecciona otro lote \n';
                }
                if(validarCantidad() === 'false'){
                    mensaje_error = mensaje_error + 'ADVERTENCIA#3. La cantidad que desea entregar supera las existencia del lote'
                }
                if(mensaje_error !== ''){
                    alert(mensaje_error);
                    return
                }
                var rows = jQuery("#listTransaccionesAdmision").jqGrid('getDataIDs');
                var obj = []
                alert(rows);
                for (var i = 0; i < rows.length; i++) {
                    var row =jQuery("#listTransaccionesAdmision").getRowData(rows[i]);
                    if(row.id_articulo === valorAtributoIdAutoCompletar("txtIdArticulo") && row.id_lote === valorAtributo("cmbIdLote") && row.sw_inventario === 'N'){
                        alert('Ya tiene un articulo con igual lote pendiente por entregar, no puedes adicionar del mismo lote hasta entregar el anterior');
                        return 
                    }
                
                }
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2293&parametros=";
                add_valores_a_mandar(valorAtributo("lblAdmisionGrilla"));
                add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdArticulo"));
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("cmbIdLote"));
                let infoLotes = document.getElementById('cmbIdLote')
                let infoLote = infoLotes.options[infoLotes.selectedIndex].text;
                let fechaVencimiento = infoLote.split("FECHA_VENCIMIENTO:")[1].split('-');
                let diaVencimiento =  parseInt(fechaVencimiento[2]);
                let mesVencimiento = parseInt(fechaVencimiento[1]);
                let anioVencimiento  = parseInt(fechaVencimiento[0]);
                let fecha_final = diaVencimiento + '/' + mesVencimiento + '/' + anioVencimiento;
                add_valores_a_mandar(fecha_final);
                add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                //actualizar cantidades de la tabla
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                ajaxModificar();
            }
        break;


        case 'enviarProductosHojaConsumo':
            if(valorAtributo('txtCantidadRestante') === '0'){
                alert('Ya has suministrado todas las cantidades del producto');
                return 
            }
            if(valorAtributo("lblIdArticuloTransaccion") === ''){
                alert('Seleccione un producto');
                return
            }
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2297&parametros=";
            add_valores_a_mandar(document.getElementById("lblIdAdmisionAgen").textContent); 
            add_valores_a_mandar(valorAtributo("lblIdArticuloTransaccion"));
            add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(valorAtributo("cmbIdTipoServicio"));


            add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
            add_valores_a_mandar(valorAtributo("lblCantidadDosis"));
            add_valores_a_mandar(valorAtributo("lblTotal"));


            ajaxModificar();
        break;

        case 'revertirHojaConsumo':
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2299&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdTransHojaDeGastos"));
            add_valores_a_mandar(valorAtributo("lblTransaccionResta"));
            ajaxModificar();
        break;


        case 'anadirUltimoConsumo':
            var rows = jQuery("#listHojaGastos").jqGrid('getDataIDs');
            var count = 0;
            for (var i = 0; i < rows.length; i++) {
                var row =jQuery("#listHojaGastos").getRowData(rows[i]);
                if(row.id_transaccion.trim() === valorAtributo("lblIdTransaccion")){
                    count = count+1;
                }
            }
            if(count > 0){
                if(confirm("Estas seguro es la ultima cantidad ?")){
                    anadirUltimoConsumoConfirmada();
                }else {
                    console.log('tarabajando.......');
                }
            }
            if(count === 0){
                alert('No puedes poner este articulo en ultimo consumo porque no lo has usado ni una solo vez !!!');
                return 
            }

            function anadirUltimoConsumoConfirmada(){
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2300&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
                ajaxModificar();
            }
            
        break;


        case 'devolverABodega':
            var rows = jQuery("#listHojaGastos").jqGrid('getDataIDs');
            var count = 0;
            for (var i = 0; i < rows.length; i++) {
                var row =jQuery("#listHojaGastos").getRowData(rows[i]);
                if(row.id_transaccion.trim() === valorAtributo("lblIdTransaccion")){
                    count = count+1;
                }
            }
            if(valorAtributo('lblSwPresentacion') === 'N' && count > 0){  
                alert('Este producto no se puede devolver, por ser una solucion liquida y ya ha sido usada');
                return;
            }else{
                if(confirm("Esta seguro de querer devolver esta cantidad a Bodega ?")){
                    valores_a_mandar = "accion=" + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2304&parametros=";
                    add_valores_a_mandar(document.getElementById('lblIdAdmisionAgen').textContent.trim());
                    add_valores_a_mandar(valorAtributo('lblIdArticuloTransaccion'));
                    add_valores_a_mandar(valorAtributo('txtCantidadRestante'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(document.getElementById('lblIdSede').textContent.trim());
                    add_valores_a_mandar(valorAtributo('lblIdArticuloTransaccion'));
                    add_valores_a_mandar(document.getElementById('cmbIdTipoServicio').value);
                    add_valores_a_mandar(valorAtributo('lblLoteDespacho'));
                    add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                    add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
                    ajaxModificar();
                }else {
                    return
                }
            }
        break; 


        case 'cerrarTransacciones':
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2295&parametros=";
                add_valores_a_mandar("35");
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
                add_valores_a_mandar(document.getElementById('cmbIdBodega').value);
                add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblAdmisionGrilla'));
                ajaxModificar();
        break;


        case 'recibirBodega':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2307&parametros=";
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(document.getElementById('lblAdmisionDevolucion').textContent);
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            
            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('lblLoteDevolucion'));
            add_valores_a_mandar(document.getElementById('lblAdmisionDevolucion').textContent);
            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));


            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));

            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            add_valores_a_mandar(valorAtributo('lblLoteDevolucion'));

            add_valores_a_mandar(valorAtributo('lblCantidadTotal'));
            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));

            add_valores_a_mandar(valorAtributo('lblArticuloLabel'));

            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

            ajaxModificar();
            break
    }
}

function respuestaModificarCRUDDespachoPedidos(arg, xmlraiz){
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case "enviarProductosASalida":
                alert('Producto adiconado correctamente');
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listaSolicitudesFarmacia');
                }, 100);
                setTimeout(() => {
                    crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
                }, 150);
                break;


                case 'recibirBodega':
                    alert('Productos devueltos con exito');
                    setTimeout(() => {
                        crearTablasSolicitudesFarmacia('listaSolicitudesFarmacia');
                    }, 100);
                    setTimeout(() => {
                        crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
                    }, 150);
                    break;
            
                case "cerrarTransacciones":
                    alert("Documento creado cone exito");
                    setTimeout(() => {
                        crearTablasSolicitudesFarmacia('listTransaccionesAdmision');
                    }, 100);
                break;

                case "anadirUltimoConsumo":
                    setTimeout(() => {
                        buscarHC('listHojaGastosDespachos');
                    }, 100);
                    setTimeout(() => {
                        buscarHC('listHojaGastos');
                    }, 150);
                    break;

                case "enviarProductosHojaConsumo":
                    document.getElementById('lblIdArticuloTransaccion').textContent = '';
                    document.getElementById('lblCantidadDosis').textContent = '';
                    document.getElementById('lblTotal').textContent = '';
                    document.getElementById('txtCantidadRestante').value = '';
                    setTimeout(() => {
                        buscarHC('listHojaGastosDespachos');
                    }, 100);
                    setTimeout(() => {
                        buscarHC('listHojaGastos');
                    }, 150);
                    break;

                case 'devolverABodega':
                    alert('Exito !!! Entrega los productos en almacen o bedaga de la sede')
                    setTimeout(() => {
                        buscarHC('listHojaGastosDespachos');
                    }, 100);
                    setTimeout(() => {
                        buscarHC('listHojaGastos');
                    }, 150);
                    break;
                
                case "revertirHojaConsumo":
                    setTimeout(() => {
                        buscarHC('listHojaGastos');
                    }, 100);
                    break
        }
    }
}