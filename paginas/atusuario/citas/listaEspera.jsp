<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1280" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td width="100%">
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="LISTAS DE ESPERA" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO  aqui empieza el cambio-->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" id="CabeceraListaEspera" border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <table width="100%">
              <tr class="titulosListaEspera">
                <td>Paciente</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td>
                  <input type="text" size="110" maxlength="210" id="txtIdBusPaciente"
                    onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente', 307)" style="width:50%"
                     />
                  <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaMuni"
                    onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanitaListaEspera', 'txtIdBusPaciente', '27')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                  <div id="divParaVentanitaListaEspera"></div>
                </td>
              </tr>
            </table>
            <table>
              <tr class="titulosListaEspera">
                <td width="15%">Tipo Id</td>
                <td width="10%">Identificacion</td>
                <td width="25%">Apellidos</td>
                <td width="25%">Nombres</td>
                <td width="15%">Fecha Nacimiento</td>
                <td width='5%'>Edad</td>
                <td width="5%">Sexo</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td>
                  <select size="1" id="cmbTipoId" style="width:85%" >
                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(105); ComboVO cmb105; for (int
                      k=0; k < resultaux.size(); k++) { cmb105=(ComboVO) resultaux.get(k); %>
                      <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                        <%= cmb105.getId() + "  " + cmb105.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
                <td><input type="number" id="txtIdentificacion" style="width:80%" size="20" maxlength="20"
                    onKeyPress="javascript:checkKey2(event);javascript:return soloTelefono(event);"  />
                </td>
                <td>
                  <input type="text" id="txtApellido1" size="30" maxlength="30"
                    onkeypress="return excluirEspeciales(event);"
                    onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
                  <input type="text" id="txtApellido2" size="30" maxlength="30"
                    onkeypress="return excluirEspeciales(event);"
                    onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
                </td>
                <td>
                  <input type="text" id="txtNombre1" size="30" maxlength="30"
                    onkeypress="javascript:return teclearsoloan(event);"
                    onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
                  <input type="text" id="txtNombre2" size="30" maxlength="30"
                    onkeypress="javascript:return teclearsoloan(event);"
                    onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
                </td>
                <td><input id="txtFechaNac" tabindex="106" type="text" style="width:95%" /> </td>
                <td>
                  <label id="txtEdad" class="bloque w15" disabled="false"></label>
                </td>
                <td>
                  <select size="1" id="cmbSexo" style="width:95%" >
                    <option value=""></option>
                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(110); ComboVO cmbS; for (int k=0;
                      k < resultaux.size(); k++) { cmbS=(ComboVO) resultaux.get(k); %>
                      <option value="<%= cmbS.getId()%>" title="<%= cmbS.getTitle()%>">
                        <%=cmbS.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
              </tr>
            </table>
            <table>
              <tr class="titulosListaEspera">
                <td width="20%">Municipio</td>
                <td width="30%">Direcci&oacute;n</td>
                <td width="20%">Acompa&ntilde;ante</td>
                <td width="10%">Tel&eacute;fono</td>
                <td width="10%">Celular 1</td>
                <td width="10%">Celular 2</td>
              </tr>
              <tr class="estiloImput">
                <td><input type="text" id="txtMunicipio" size="60" maxlength="60"
                    oninput="llenarElementosAutoCompletarKey(this.id, 3704, this.value)" style="width:80%"
                     />
                  <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni"
                    onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                  <div id="divParaVentanita"></div>
                </td>
                <td><input type="text" id="txtDireccionRes" size="100" maxlength="100" style="width:99%"
                    onkeyup="javascript: this.value = this.value.toUpperCase();"
                    onkeypress="javascript:return teclearsoloan(event);"  /></td>
                <td><input type="text" id="txtNomAcompanante" size="100" maxlength="100" style="width:95%"
                    onkeyup="javascript: this.value = this.value.toUpperCase();"
                    onkeypress="javascript:return teclearExcluirCaracter(event);"  /> </td>
                <td>
                  <input type="text" id="txtTelefonos" size="100" style="width:90%" minlength="7" maxlength="7"
                    onkeypress="javascript:return soloTelefono(event);"  />
                </td>
                <td>
                  <input type="text" id="txtCelular1" size="30" style="width:90%" minlength="10" maxlength="10"
                    onkeypress="javascript:return soloTelefono(event);"  />
                </td>
                <td>
                  <input type="text" id="txtCelular2" size="30" style="width:90%" minlength="10" maxlength="10"
                    onkeypress="javascript:return soloTelefono(event);"  />
                </td>
              </tr>
            </table>
            <table>
              <tr class="titulosListaEspera">
                <td width="20%">Email</td>
                <td width="20%">Administradora Paciente</td>
                <td width="30%">IPS Primaria</td>
                <td width="10%">Tipo de Regimen</td>
                <td width="10%">Nivel Sisben</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td><input type="text" id="txtEmail" size="40" maxlength="60" style="width:90%"
                    onkeypress="javascript:return email(event);"  /> </td>
                <td>
                  <input type="text" id="txtAdministradoraPaciente"
                    onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraPaciente', 308)" style="width:80%"
                     />
                  <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaAdPaciente"
                    onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtAdministradoraPaciente', '21')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td>
                  <input type="text" id="txtIpsPaciente" style="width:90%"  />
                </td>
                <td>
                  <select id="cmbTipoRegimen" style="width:90%;">
                    <option value=""></option>
                    <%resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(518); ComboVO cmbPro1; for (int
                      k=0; k < resultaux.size(); k++) { cmbPro1=(ComboVO) resultaux.get(k);%>
                      <option value="<%= cmbPro1.getId()%>" title="<%= cmbPro1.getTitle()%>">
                        <%=cmbPro1.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
                <td>
                  <select id="cmbNivelSisben" style="width:90%;">
                    <option value=""></option>
                    <%resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(3505); ComboVO cmbNS; for (int
                      k=0; k < resultaux.size(); k++) { cmbNS=(ComboVO) resultaux.get(k);%>
                      <option value="<%= cmbNS.getId()%>" title="<%= cmbNS.getTitle()%>">
                        <%=cmbNS.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr class="estiloImputListaEspera">
                <td>
                  <% if (beanSession.usuario.preguntarMenu("6_b2")) {%>
                    <input id="btnCrearNuevoPaciente" type="button" class="small button blue"
                      value="Crear nuevo paciente" onclick="modificarCRUD('crearNuevoPaciente');" />
                    <% }%>
                      <input name="btn_MODIFICAR" type="button" class="small button blue"
                        value="Modificar Información Paciente" title="bt55Q5"
                        onclick="modificarCRUD('pacienteModifica');" />
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr class="titulosListaEspera">
                <td width="25%" class="campoRequerido">Sede</td>
                <td width="20%">Administradora Agenda</td>
                <td width="15%">Tipo R&eacute;gimen - Plan</td>
                <td width="40%" class="campoRequerido">Plan de Contrataci&oacute;n</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td>
                  <input id="cmbSede" onchange="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"
                    style="width:70%;" placeholder="Escriba la prestadora que desea elegir" />
                </td>
                <td>
                  <input type="text" id="txtAdministradora1"
                    onchange="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                    onkeypress="llamarAutocompletarEmpresa('txtAdministradora1', 184)" style="width: 85%;" />
                  <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem"
                    onclick="traerVentanitaEmpresa(this.id, 'divParaVentanita', 'txtAdministradora1', '24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td>
                  <select id="cmbIdTipoRegimen" style="width: 95%;" onclick="limpiarDivEditarJuan('limpiarDatosPlan')"
                    onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura');
                                                    cargarComboGRALCondicion2('', '', this.id, 79, valorAtributoIdAutoCompletar('txtAdministradora1'), valorAtributo('cmbSede'))"
                    >
                    <option value=""></option>
                  </select>
                </td>
                <td>
                  <label id="lblIdPlanContratacion" hidden></label><label id="lblNomPlanContratacion" hidden></label>
                  <select id="cmbIdPlan" style="width: 90%;"
                    onfocus="traerPlanesDeContratacionListaEspera(this.id, '165', 'txtAdministradora1', 'cmbIdTipoRegimen')"
                    onchange="asignaAtributo('lblIdPlanContratacion', valorAtributo(this.id), 0)" >
                    <option value=""></option>
                  </select>
                </td>
              </tr>
              <tr class="titulosListaEspera">
                <td style="max-width: 100px;" width="20%" class="campoRequerido">Especialidad</td>
                <!-- <td style="max-width: 100px;" width="20%">Sub Especialidad</td> -->
                <td style="max-width: 100px;" width="15%" class="campoRequerido">Tipo de Cita</td>
                <!-- <td style="max-width: 100px;" width="15%">Discapacidad Física</td>
                <td style="max-width: 100px;" width="15%">Embarazo</td> -->
                <td style="max-width: 100px;" width="15%">Condición Especial</td>
                <td style="max-width: 100px;" width="15%" class="campoRequerido">Esperar días</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td>
                  <!-- <select id="cmbIdEspecialidad" style="width:95%"
                    onchange="cargarSubEspecialidadDesdeEspecialidad(this.id, 'cmbIdSubEspecialidad')">
                    <option value="">[ SELECCIONE ]</option>

                  </select> -->

                  <select size="1" id="cmbIdEspecialidad" onfocus="limpiarDivEditarJuan('limpiarTipoCitaLE')"
                    style="width:95%"  onchange="EsCirugia(this);cargarProfesionalesDesdeEspecialidadSede_1('cmbIdProfesionales')"
                    onclick="cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad');">
                    <option value=""></option>
                  </select>
                </td>
                <!-- <td style="max-width: 100px;">
                  <select size="1" id="cmbIdSubEspecialidad" onfocus="limpiarDivEditarJuan('limpiarTipoCitaLE')"
                    style="width:95%" 
                    onclick="cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad');">
                    <option value=""></option>
                  </select>
                </td> -->
                <td style="max-width: 100px;">
                  <select id="cmbTipoCita" onchange="buscarAGENDA('listEsperaCEXProcedimientoTraer')" style="width:90%"
                    >
                    <option value="">[ SELECCIONE ]</option>
                  </select>
                </td>
                <td>
                  <select id="cmbCondicionFisica" size="1" style="width:90%" >
                    <option value=""></option>
                    <option value="NoAplica" selected="selected">No aplica</option>
                    <option value="embarazo">Embarazo</option>
                    <option value="discapacidad_fisica">Discapacidad Física</option>
                  </select>
                </td>
                <!-- <td style="max-width: 100px;">
                  <select id="cmbDiscapaciFisica" size="1" style="width:40%" >
                    <option value=""></option>
                    <option value="N" selected="selected">No</option>
                    <option value="S">Si</option>
                  </select>
                </td>
                <td style="max-width: 100px;">
                  <select id="cmbEmbarazo" size="1" style="width:70%" >
                    <option value=""></option>
                    <option value="N" selected="selected">No</option>
                    <option value="S">Si</option>
                  </select>
                </td> -->
                <td style="max-width: 100px;">
                  <select id="cmbEsperar" size="1" style="width:90%" >
                    <option value=""></option>
                    <option value="0">0 Dias</option>
                    <option value="30">30 dias (1 mes)</option>
                    <option value="60">60 dias (2 meses)</option>
                    <option value="90">90 dias (3 meses)</option>
                    <option value="120">120 dias(4 meses)</option>
                    <option value="150">150 dias(5 meses)</option>
                    <option value="180">180 dias(6 meses)</option>
                    <option value="210">210 dias(7 meses)</option>
                    <option value="240">240 dias(8 meses)</option>
                    <option value="270">270 dias(9 meses)</option>
                    <option value="300">300 dias(10 meses)</option>
                    <option value="330">330 dias(11 meses)</option>
                    <option value="365">365 dias(12 meses)</option>
                  </select>
                </td>
              </tr>
            </table>
            <table width="100%" id="tbCirugia" hidden>
              <tr class="titulosListaEspera">
                <td>Profesional que remite</td>
                <td>Institución que remite</td>
              </tr>
              <tr>
                <td width="50%">
                  <select size="1" id="cmbIdProfesionales" style="width:90%"
                    >
                    <option value="">[ SELECCIONE ]</option>
                  </select>
                </td>
                <td>
                  <input type="text" id="txtIPSRemite"   size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtIPSRemite',313)" style="width:98%"   />
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr class="titulosListaEspera">
                <td id="IdTdProcedimientos" width="100%" colspan="7">PROCEDIMIENTOS</td>
              </tr>

              
              <tr class="titulos">
                <td width="100%" colspan="6">
                  <div style="overflow:auto; height:100px; width:100%" id="divProcedimientosLE">
                    <table id="listEsperaCEXProcedimientoTraer" class="scroll"></table>
                  </div>
                </td>
              </tr>


              <tr class="titulosListaEspera">
                <td colspan="1">Sitio Quirurgico</td>
                <td colspan="2" width="50%">Procedimiento CUPS</td>
                <td colspan="3">Observaciones Procedimiento</td>
                <td></td>
              </tr>
              <tr class="titulos">
                <td colspan="1">
                  <select id="cmbIdSitioQuirurgicoLE" style="width:80%">
                    <!-- <option value="10">NoAplica</option> --!>
                    <% resultaux.clear(); resultaux=(ArrayList)beanAdmin.combo.cargar(120); ComboVO cmbL; for(int
                      k=0;k<resultaux.size();k++){ cmbL=(ComboVO)resultaux.get(k); %>
                      <option value="<%= cmbL.getId()%>" title="<%= cmbL.getTitle()%>">
                        <%= cmbL.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
                <td colspan="2"><input type="text" id="txtIdProcedimientoLE" size="200" maxlength="200"
                    style="width:90%"
                    onkeypress="llamarAutocomIdDescripcionParametro('txtIdProcedimientoLE', 420, 'lblIdPlanContratacion')"
                     />
                  <img width="18px" height="18px" align="middle" title="VEN3" id="idLupitaVentanitaProce"
                    onclick="traerVentanitaProcedimientosListaEspera(this.id,'','divParaVentanita','txtIdProcedimientoLE','3')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td colspan="3">
                  <input type="text" id="txtObservacionProcedimientoCirugiaLE"  style="width:95%"/>
                  
                </td>
                <td>
                  <input name="btn_crear_cita" type="button" class="small button blue" value="Adicionar" title="btn823"
                    onclick="modificarCRUD('listProcedimientoLE','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                     />
                </td>
              </tr>
              <tr class="titulos">
                <td width="100%" 100px colspan="7">
                  <div style="overflow:auto; height:100px; width:100%" id="divListaEsperaProcedimientosLE">
                    <table id="listProcedimientoLE" class="scroll"></table>
                  </div>
                  <input name="btn_eliminar_procedimiento" type="button" class="small button blue" value="Limpiar tabla" title="btnl133" style="margin-bottom: 15px;"
                    onclick="limpiarListadosTotales('listProcedimientoLE')"  />
                </td>
              </tr>

              <input type="hidden" id="txtColumnaIdSitioQuirur" />
              <input type="hidden" id="txtColumnaIdProced" />
              <input type="hidden" id="txtColumnaObservacion" />

              
              <tr class="titulosListaEspera">
                <!-- <td>Sitio</td> -->
                <td colspan="2">Estado</td>
                <td>Motivo de espera</td>
                <!-- <td>Fecha deseada</td> -->
                <td colspan="4" class="campoRequerido">Grado de Necesidad</td>
              </tr>

              <tr class="estiloImputListaEspera">
                <!-- <td>
                  <select size="1" id="cmbSitio" style="width:95%" title="GD48">
                    <% resultaux.clear(); resultaux=(ArrayList)beanAdmin.combo.cargar(120); ComboVO cmbSQ; for(int
                      k=0;k<resultaux.size();k++){ cmbSQ=(ComboVO)resultaux.get(k); %>
                      <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>">
                        <%= cmbSQ.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td> --!>
                <td colspan="2">
                  <select id="cmbEstado" size="1" style="width:95%">
                    <% resultaux.clear(); resultaux=(ArrayList)beanAdmin.combo.cargar(112); ComboVO cmbEL; for(int
                      k=0;k<resultaux.size();k++){ cmbEL=(ComboVO)resultaux.get(k); %>
                      <option value="<%= cmbEL.getId()%>" title="<%= cmbEL.getTitle()%>">
                        <%=cmbEL.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
                <td>
                  <select id="cmbMotivo" style="width:95%">
                    <option value="">[ SELECCIONE ]</option>
                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(222); ComboVO cmbG0; for (int
                      k=0; k < resultaux.size(); k++) { cmbG0=(ComboVO) resultaux.get(k); %>
                      <option value="<%= cmbG0.getId()%>" title="<%= cmbG0.getTitle()%>">
                        <%=cmbG0.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
                <!--  <td>
                                    <input type="date" id="txtDiasEsperar" style="width: 85%;"/>
                                </td>  -->
                <td colspan="4">
                  <select id="cmbNecesidad" size="1" style="width:80%" >
                    <option value="">[ SELECCIONE ]</option>
                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(113); ComboVO cmbG; for (int k=0;
                      k < resultaux.size(); k++) { cmbG=(ComboVO) resultaux.get(k); %>
                      <option value="<%= cmbG.getId()%>" title="<%= cmbG.getTitle()%>">
                        <%=cmbG.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr class="titulosListaEspera">
                <td width="20%">Nº Autorización</td>
                <td width="15%">Fecha de Vigencia Autorización</td>
                <td width="15%">Preferencial</td>
                <td width="30%">Observacion</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td>
                  <input type="text" id="txtNoAutorizacion" style="width:90%" />
                </td>
                <td>
                  <input type="text" id="txtFechaVigencia" style="width:90%" />
                </td>
                <td>
                  <select id="cmbPreferencial" style="width:90%">
                    <option value="N">NO</option>
                    <option value="S">SI</option>
                  </select>
                </td>
                <td>
                  <textarea id=txtObservacion row=2 style="width:90%"></textarea>
                </td>                                
              </tr>
              <tr>
                <td align="center" colspan="5">
                  <input type="button" class="small button blue" value="GUARDAR EN LISTA DE ESPERA"
                    onclick="guardarYtraerDatoAlListado('buscarPendienteLE_LE')" />
                </td>
              </tr>
            </table>
            <table width="100%" style="margin-top: 15px;">
              <tr class="titulosListaEspera" style="background: #8eccf6; font-size: 11px;letter-spacing: 1px;">
                <td colspan="2">
                  <label for="">Filtros de busqueda</label>
                </td>
              </tr>
              <tr>
                <td style="width: 80%;">
                  <table width="100%">
                    <tr class="titulosListaEspera">
                      <td style="text-align:left; width:20%;">Especialidad</td>
                      <td style="width:80%; text-align:left;">
                        <input type="text" id="cmbEspecialidadAtusuarios" style="width: 80%;"
                          onchange="buscarAGENDA('listHistoricoListaEspera')">
                        <a
                          onclick="limpiarSelect2('cmbEspecialidadAtusuarios'); buscarAGENDA('listHistoricoListaEspera')">
                          <i class="fas fa-backspace"></i>
                        </a>
                      </td>
                    </tr>
                    <!-- <tr class="titulosListaEspera">
                      <td style="text-align:left;">
                        Sub Especialidad
                      </td>
                      <td style="text-align:left;">
                        <input type="text" id="cmbSubEspecialidadAtusuarios" style="width: 80%;"
                          onchange="buscarAGENDA('listHistoricoListaEspera')">
                        <a
                          onclick="limpiarSelect2('cmbSubEspecialidadAtusuarios'); buscarAGENDA('listHistoricoListaEspera')">
                          <i class="fas fa-backspace"></i>
                        </a>

                      </td>
                    </tr> -->
                    <tr class="titulosListaEspera">

                      <td style="text-align:left; width:20%; ">Tipo de cita</td>
                      <td style="width:80%; text-align:left;">
                        <input type="text" id="cmbTipoCitaAtusuarios" style="width: 80%;"
                          onchange="buscarAGENDA('listHistoricoListaEspera')">
                        <a onclick="limpiarSelect2('cmbTipoCitaAtusuarios'); buscarAGENDA('listHistoricoListaEspera')">
                          <i class="fas fa-backspace"></i>
                        </a>
                      </td>
                    </tr>
                    <tr class="titulosListaEspera">

                      <td style="text-align:left; width:20%; ">Plan de contratación</td>
                      <td style="width:80%; text-align:left;">
                        <input type="text" id="cmbPlanContratacion" style="width: 80%;"
                          onchange="buscarAGENDA('listHistoricoListaEspera')">
                        <a onclick="limpiarSelect2('cmbPlanContratacion'); buscarAGENDA('listHistoricoListaEspera')">
                          <i class="fas fa-backspace"></i>
                        </a>
                      </td>
                    </tr>
                  </table>
                  <table width="100%" style="align-content: center !important;">
                    <tr class="titulosListaEspera">
                      <td style="text-align:center; width:10%;">Motivo de espera</td>
                      <td style="width:25%; text-align:center;">
                        <select id="cmbMotivoAtusuarios" style="width:60%"
                          onchange="buscarAGENDA('listHistoricoListaEspera')">
                          <option value=""></option>
                          <option value="1">CITA NEGADA</option>
                          <option value="2">CITA DE CONTROL</option>
                          <option value="3">TUTELA</option>
                        </select>
                      </td>
                      <td style="text-align:center; width:10%;">Estado de Cita</td>
                      <td style="width:25%; text-align:center;">
                        <select id="cmbEstadoCitaAtusuarios" style="width:60%"
                        onchange="buscarAGENDA('listHistoricoListaEspera')">
                          <option value=""></option>
                          <option value="A">ASIGNADA</option>
                          <option value="C">CONFIRMADA</option>
                          <option value="L">CANCELADA</option>
                          <option value="N">NO ASISTIO</option>
                          <option value="R">REPROGRAMA</option>
                        </select>

                      </td>
                      <td style="text-align:center; width:10%;">
                        <label for="idSoloPaciente">Solo Paciente</label>
                      </td>
                      <td style="width:20%; text-align:center;">
                        <input type="checkbox" name="" id="idSoloPaciente" onclick="buscarAGENDA('listHistoricoListaEspera')">
                      </td>
                    </tr>
              </tr>
            </table>
          </td>
          <td style="width: 20%;">
            <table width="100%">
              <tr class="titulosListaEspera">
                <td style="text-align:left;">Prioridad
                  <dl>
                    <dt><input type="checkbox"
                        onclick="limpiaAtributo('chkEstadoLE_1'); limpiaAtributo('chkEstadoLE_2'); limpiaAtributo('chkEstadoLE_3'); 
                                                        limpiaAtributo('chkEstadoLE_4'); buscarAGENDA('listHistoricoListaEspera');"
                        id="chkEstadoLE" name="chkEstadoLE" value="">[TODO]</dt>

                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(326); ComboVO cmbList; for (int
                      k=0; k < resultaux.size(); k++) { cmbList=(ComboVO) resultaux.get(k); %>
                      <dt><input type="checkbox"
                          onclick="buscarAGENDA('listHistoricoListaEspera'); limpiaAtributo('chkEstadoLE') "
                          id="chkEstadoLE_<%= cmbList.getId()%>" name="chkEstadoLE_<%= cmbList.getId()%>"
                          value="<%= cmbList.getId()%>">
                        <%= cmbList.getDescripcion()%>
                      </dt>
                      <%}%>
                  </dl>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </table>
      <div id="divContenidoListaEsperaHist">
        <table id="listHistoricoListaEspera"></table>
      </div>
      <div id="divContenidoHistoricoAtencion">
        <table id="listDocumentosHistoricos"></table>
      </div>
  <tr>
    <td colspan="4">
      <table width="1080" align="center" border="0" cellspacing="0" cellpadding="1">
        <tr>
          <div id="divAcordionAdjuntos" style="display:BLOCK">
            <jsp:include page="divsAcordeon.jsp" flush="FALSE">
              <jsp:param name="titulo" value="Archivos adjuntos" />
              <jsp:param name="idDiv" value="divArchivosAdjuntos" />
              <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />
              <jsp:param name="display" value="NONE" />
              <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
            </jsp:include>
          </div>
        </tr>
      </table>
    </td>
  </tr>
  </td>
  </tr>
</table>
</td>
</tr>
</table>





<div id="divVentanitaEliminarListaEspera" class="ventanita">

  <div class="transParencia fondoVentanita">
  </div>
  <div class="fondoVentanitaContenido w70 t200 l250 fondoTabla">
    <table id="tableListaEspera">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEliminarListaEspera')" /></td>
        <td colspan="3"></td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEliminarListaEspera')" /></td>
      <tr>
      <tr class="titulosListaEspera">
        <td class="w20">Id Lista Espera</td>
        <td class="w20">Esperar</td>
        <td class="w20">Especialidad</td>
        <!--<td class="w20">Sub Especialidad</td> -->
        <td class="w20">Elaboro</td>

      </tr>

      <tr class="estiloImput">
        <td><label id="lblListaEspera"></label></td>
        <td> <label id="lblLEEsperar"></label>Dias</td>
        <td><label id="lblEspecialidad"></label></td>
        <!--<td><label id="lblSubEspecialidad"></label></td>   -->
        <td><label id="lblUsuarioElaboro"></label></td>

      </tr>
      <tr class="titulosListaEspera">
        <td>Grado Necesidad</td>
        <td>Administradora Lista Espera</td>
        <td>Tipo Régimen - Plan</td>
        <td>Plan de Contratación</td>
        <td>Estado</td>
      </tr>
      <tr class="estiloImput">
        <td>
          <select id="cmbGradoNecesidadMod" size="1" style="width:80%" >
            <option value=""></option>
            <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(113); ComboVO cmbG2; for (int k=0; k <
              resultaux.size(); k++) { cmbG2=(ComboVO) resultaux.get(k); %>
              <option value="<%= cmbG2.getId()%>" title="<%= cmbG2.getTitle()%>">
                <%=cmbG2.getDescripcion()%>
              </option>
              <%}%>
          </select>
        </td>
        <td>
          <input type="text" id="txtAdministradoraLE" size="60" maxlength="60"
            onkeyup="javascript: this.value = this.value.toUpperCase();" style="width:85%"  />
          <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaMuni"
            onclick="traerVentanitaHistoricosListaEspera(this.id, '', 'divParaVentanita', 'txtAdministradoraLE', '21')"
            src="/clinica/utilidades/imagenes/acciones/buscar.png">
        </td>
        <td>
          <select id="cmbIdTipoRegimenLE" class="bloque w25 m20" style="width:97%"
            onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura');comboDependienteAutocomplete('cmbIdTipoRegimenLE', 'txtAdministradoraLE', '521')"
            onchange="cargarUnLabelPlanDeContratacionListaEspera('lblIdPlanContratacionLE', 'lblNomPlanContratacionLE', 'txtAdministradoraLE', 'cmbIdTipoRegimenLE', '29')"
            >
            <option value=""></option>
          </select>
        </td>
        <td>
          <label id="lblIdPlanContratacionLE"></label>-<label id="lblNomPlanContratacionLE"></label>
        </td>

        <td>
          <select id="cmbEstadoLEMod" size="1" style="width:90%" >
            <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(112); ComboVO cmb2; for (int k=0; k <
              resultaux.size(); k++) { cmb2=(ComboVO) resultaux.get(k); %>
              <option value="<%= cmb2.getId()%>" title="<%= cmb2.getTitle()%>">
                <%=cmb2.getDescripcion()%>
              </option>
              <%}%>
          </select>
        </td>
      </tr>

      <tr class="titulosListaEspera">
        <td>No Autorizacion </td>
        <td>Fecha Vigencia Autorización</td>
        <td colspan="3">Observacion</td>
      </tr>


      <tr class="estiloImput">
        <td><input type="text" id="txtNoAutorizacionLE" size="60" maxlength="60" style="width:50%"  />
        </td>
        <td><input type="text" id="txtFechaVigenciaLE" size="10" maxlength="10" style="width:50%"  /></td>
        <td colspan="3"><textarea type="text" id="txtObservacionLE" size="1000" maxlength="1000" style="width:90%"
            onKeyPress="javascript:return teclearExcluirCaracter(event, '%');"
            onkeyup="javascript: this.value = this.value.toUpperCase();"> </textarea>
        </td>
      </tr>




      <tr class="estiloImputListaEspera">
        <td colspan="5">
          <input name="btn_crear_cita" type="button" class="small button blue" title="b773" value="ELIMINAR"
            onclick="modificarCRUD('eliminaListaEspera', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
             />

          <input name="btn_crear_cita" type="button" class="small button blue" title="b772" value="MODIFICAR"
            onclick="modificarCRUD('modificarObservacionListaEspera', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
             />

        </td>

      </tr>

      <tr class="camposRepInp">
        <td colspan="5">PROCEDIMIENTOS LISTA DE ESPERA</td>
      <tr>
      <tr class="titulos">
        <td colspan="1">Sitio</td>
        <td colspan="1">Procedimiento CUPS</td>
        <td colspan="1">Observaciones</td>
        <td colspan="1"></td>
      </tr>
      <tr class="titulos">
        <td colspan="1">
          <select id="cmbIdSitioQuirurgico" style="width:80%" >
            <option value=""></option>
            <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(120); ComboVO cmbSQ2; for (int k=0; k <
              resultaux.size(); k++) { cmbSQ2=(ComboVO) resultaux.get(k); %>
              <option value="<%= cmbSQ2.getId()%>" title="<%= cmbSQ2.getTitle()%>">
                <%= cmbSQ2.getDescripcion()%>
              </option>
              <%}%>
          </select>
        </td>
        <td>
          <input type="text" id="txtIdProcedimiento" size="200" maxlength="200" style="width:98%"
            onkeyup="javascript: this.value = this.value.toUpperCase();"
            onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimiento', 324)"  />
        </td>
        <td colspan="1">
          <input type="text" id="txtObservacionLEsp" size="200" maxlength="200" style="width:98%" 
            onkeyup="javascript: this.value = this.value.toUpperCase();" />
        </td>
        <td colspan="1">
          <input name="btn_crear_cita" type="button" class="small button blue" value="Adici&oacute;n proc." title="B762"
            onclick="modificarCRUD('adicionarListaEsperaProcedimientos', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
             />
        </td>
      </tr>
      <tr class="titulos">
        <td colspan="5">
          <div style="overflow:auto;height:180px;" id="divListaEsperaProcedimientosLE">
            <table id="listListaEsperaProcedimiento" class="scroll"></table>
          </div>
        </td>
      </tr>
    </table>
  </div>
</div>


<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />
<input type="hidden" id="txt_banderaOpcionCirugiaGestion" value="NO" />


<!-------------------------para la vista previa---------------------------- -->

<div id="divVentanitaPdf" style="display:none; z-index:2050; top:1px; left:190px;">

  <div id="idDivTransparencia" class="transParencia"
    style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div id="idDivTransparencia2" style="z-index:2055; position:absolute; top:5px; left:15px; width:100%">
    <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaPdf')" /></td>
        <td>&nbsp;</td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaPdf')" /></td>
      </tr>
      <tr class="estiloImput">
        <td colspan="3">
          <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td>
                <div id="divParaPaginaVistaPrevia"></div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>






<div id="divVentanitaListaEsperaProcedimientos" class="ventanita2">

  <div class="transParencia fondoVentanita2">
  </div>
  <div class="fondoVentanitaContenido2 w40 t300 l300 fondoTabla">
    <table width="95%" height="90" cellpadding="0" cellspacing="0">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaListaEsperaProcedimientos')" /></td>
        <td>&nbsp;</td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaListaEsperaProcedimientos')" /></td>
      <tr>
      <tr class="titulosListaEspera">
        <td width="15%">LEProcedimiento</td>
        <td width="25%">Sitio quirurgico</td>
        <td width="60%">Procedimiento</td>
      </tr>
      <tr class="estiloImput">
        <td><label id="lblListaEsperaProcedimiento"></label></td>
        <td><label id="lblSitioQuirurgico"></label></td>
        <td><label id="lblLEProcedimiento"></label></td>
      </tr>
      <tr class="estiloImputListaEspera">
        <td colspan="3">
          <input name="btn_eliminar_prodedimiento" type="button" class="small button blue"
            value="ELIMINAR PROCEDIMIENTO"
            onclick="modificarCRUD('eliminarListaEsperaProcedimientos', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
             />
        </td>
      </tr>
    </table>
  </div>
</div>

<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />
<input type="hidden" id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden" id="txtTipoDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblTituloDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblIdAdmisionVistaPrevia" value="" />
<input type="hidden" id="txtPrincipalHC" value="NO" />
<!-------------------------fin para la vista previa---------------------------- -->