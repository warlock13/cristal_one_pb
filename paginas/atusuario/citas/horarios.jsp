<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<table width="1200px" id="idAgendas" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="AGENDAS"/>
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO  aqui empieza el cambio-->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td valign="top" width="80%">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr class="titulosListaEspera">
                                            <td width="30%">Sede</td>
                                            <td width="30%">Especialidad</td>
                                            <td width="20%">Profesional</td>
                                        </tr>
                                        <tr class="estiloImput">
                                            <td>
                                        
                                                        
                                                <!-- <select id="cmbSede" style="width:90%"
                                                onfocus="comboDependienteSede('cmbSede','557')"
                                                onchange="asignaAtributo('cmbIdEspecialidad', '', 0); 
                                                          asignaAtributo('cmbIdProfesionales', '', 0); 
                                                          limpiaAtributo('cmbConsultorio', 0);
                                                          limpiaAtributo('cmbIdEspecialidadMover', 0);
                                                          limpiaAtributo('cmbIdProfesionalesMov', 0);
                                                          limpiarGrilla('listDiasC'); 
                                                          limpiarGrilla('listAgenda');">
                                            </select> -->
                                            <input id="cmbSede" onchange="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')" style="width:70%;" placeholder="Escriba la prestadora que desea elegir" />
                                            </td>
                                            <td>
                                                <!-- <select id="cmbIdEspecialidad" style="width:90%"
                                                    onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"
                                                    onchange="limpiaAtributo('cmbIdProfesionales', 0);
                                                              limpiaAtributo('cmbConsultorio', 0); 
                                                              limpiarGrilla('listDiasC'); 
                                                              limpiarGrilla('listAgenda');"
                                                    style="width:80%" >
                                                    <option value=""></option>

                                                </select> -->
                                                <select id="cmbIdEspecialidad" onchange="cargarProfesionalesDesdeEspecialidadSede_1('cmbIdProfesionales')" style="width:70%;" placeholder="Escriba la Especialidad que desea elegir" />
                                            </td>
                                            <td>
                                                <select id="cmbIdProfesionales" style="width:90%;" placeholder="Escriba el Profesional que desea elegir" onchange="buscarAGENDA('listDiasC');"/>
                                                <!-- <select size="1" id="cmbIdProfesionales" style="width:90%"
                                                    onfocus="cargarProfesionalesDesdeEspecialidadSede(this.id)"
                                                    onchange="buscarAGENDA('listDiasC');">
                                                    <option value=""></option> -->
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <caption class="camposRepInp">Crear una nueva agenda</caption>
                                        <tr class="titulosListaEspera">
                                            <td width="25%"></td>
                                            <td width="10%">Minutos de duraci&oacute;n</td>
                                            <td width="15%">Citas en bloque</td>
                                            <td width="15%" id="tdElementosCitasBloque" hidden>N&uacute;mero de citas por bloque</td>
                                            <td width="20%">Lugar</td>
                                            <td width="20%"></td>
                                        </tr>
                                        <tr class="estiloImput">
                                            <td>
                                                <table width="100%">
                                                    <tr class="titulosListaEspera">
                                                        <td align="right">
                                                            Hora de inicio: <input type="time" style="width: 40%;" id="txtHoraInicio">
                                                        </td>
                                                    </tr>
                                                    <tr class="titulosListaEspera">
                                                        <td align="right">
                                                            Hora de fin: <input type="time" style="width: 40%;" id="txtHoraFin">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="CENTER">
                                                <select size="1" id="cmbDuracionMinutos" style="width: 80%"
                                                    title="Mnibutos de duración" >
                                                    <option value=""></option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="8">8</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                    <option value="20">20</option>
                                                    <option value="25">25</option>
                                                    <option value="30">30</option>
                                                    <option value="35">35</option>
                                                    <option value="40">40</option>
                                                    <option value="45">45</option>
                                                    <option value="50">50</option>
                                                    <option value="55">55</option>
                                                    <option value="60">60</option>
                                                    <option value="120">2 HORAS</option>
                                                    <option value="180">3 HORAS</option>
                                                    <option value="240">4 HORAS</option>
                                                    <option value="300">5 HORAS</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select size="1" id="cmbBloqueCita" style="width:80%;" onchange="reaccionAEvento(this.id)">
                                                    <option value="N">NO</option>
                                                    <option value="S">SI</option>
                                                </select>
                                            </td>
                                            <td align="CENTER" id="tdElementosCitasBloque" hidden>
                                                <input type="number" id="txtCantidadCitasBloque" min="1">
                                            </td>
                                            <td>
                                                <select size="1" id="cmbConsultorio" style="width:100%;"
                                                    onfocus="cargarComboGRALCondicion2('', '', this.id, 8001, valorAtributo('cmbIdEspecialidad'), valorAtributo('cmbSede'))"
                                                    >
                                                </select>
                                            </td>
                                            <td align="center"><input id="BT33E" name="btn_cerrar" type="button"
                                                class="small button blue" align="right" value=" Crear Horario"
                                                title="BT33E" onclick="cargabotonloader(this); verificarNotificacionAntesDeGuardar('verificarFechasCrearAgenda');" />
                                        </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr id="divOpciones" show>
                                            <td>
                                                <table width="100%">
                                                    <tr class="titulosListaEspera">
                                                        <td width="20%">Eliminar disponibles</td>
                                                        <td width="5%"></td>
                                                        <td width="40%">Modificar disponibilidad</td>
                                                        <td width="5%"></td>
                                                        <td width="30%">Copiar/mover agenda</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="estiloImput">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="right" style="font-weight: bold">
                                                                        Hora de inicio: <input type="time" style="width: 80%;" id="txtHoraInicioEliminar">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" style="font-weight: bold">
                                                                        Hora de fin: <input type="time" style="width: 80%;" id="txtHoraFinEliminar">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="CENTER">
                                                                        <input id="BT34E" name="btn_cerrar" class="small button blue"
                                                                            type="button"
                                                                            title="Permite Eliminar Horario disponible"
                                                                            align="right" value="Eliminar Disponibles"
                                                                            onclick="cargabotonloader(this); verificarNotificacionAntesDeGuardar('verificarFechasEliminarDisponiblesAgenda')"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td></td>
                                                        <td class="titulosListaEspera">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%">
                                                                            <tr class="titulosListaEspera">
                                                                                <td align="right" style="font-weight: bold">
                                                                                    Hora de inicio: <input type="time" style="width: 80%; " id="txtHoraInicioNoDisponibles">
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="titulosListaEspera" style="font-weight: bold">
                                                                                <td align="right">
                                                                                    Hora de fin: <input type="time" style="width: 80%;" id="txtHoraFinNoDisponibles">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td>
                                                                        <textarea id="txtObservacionNoDisponible" cols="25" rows="2" placeholder="Observaciones"></textarea>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="CENTER">
                                                                        <input class="small button blue" id="BT35E" type="button" value="No diponible" onclick="cargabotonloader(this); verificarNotificacionAntesDeGuardar('verificarFechasNoDisponibleAgenda')"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td class="titulosListaEspera">Fecha Destino</td>
                                                                    <td width="20%"> 
                                                                        <input type="text" size="10" maxlength="10"
                                                                            title="Fecha Destino" id="txtFechaDestino"
                                                                             />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="titulosListaEspera">Especialidad</td>
                                                                    <td width="20%"> 
                                                                        <select id="cmbIdEspecialidadMover" style="width:90%"
                                                                            onfocus="cargarEspecialidadDesdeSede('cmbSede', this.id)"
                                                                            onchange="limpiaAtributo('cmbIdProfesionalesMov', 0)">
                                                                            <option value="">[ SELECCIONE ]</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="titulosListaEspera">Profesional</td>
                                                                    <td width="70%">
                                                                        <select id="cmbIdProfesionalesMov" style="width:90%"
                                                                            onfocus="cargarComboGRALCondicion2('', 
                                                                                                                '', 
                                                                                                                this.id, 
                                                                                                                197, 
                                                                                                                valorAtributo('cmbIdEspecialidadMover'), 
                                                                                                                valorAtributo('cmbSede'))">
                                                                            <option value="">[ SELECCIONE ]</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="2">
                                                                        <input id="BT36E" name="btn_cerrar"
                                                                            title="Permite copiar Horario a fecha destino Seleccionada"
                                                                            class="small button blue" type="button" align="right"
                                                                            value="Copiar horario"
                                                                            onclick="cargabotonloader(this); verificarNotificacionAntesDeGuardar('verificarFechasCopiarAgenda')" />
                                                                        <input id="BT37E" name="btn_cerrar" class="small button blue"
                                                                            title="Permite Mover Agenda a fecha destino seleccionada"
                                                                            type="button" align="center" value="Mover Horario"
                                                                            onclick="cargabotonloader(this); verificarNotificacionAntesDeGuardar('verificarFechasMoverAgenda')"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="divListAgenda" style="background-color:White;">
                                        <table id="listAgenda" class="scroll">
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%">
                            <tr class="titulos">
                                <td align="CENTER">
                                    <img width="15" height="15" style="cursor:pointer"
                                        src="/clinica/utilidades/imagenes/acciones/izquierda.png"
                                        onclick="irMesAnteriorHorario(); buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                    <label id="lblMes"></label>/<label id="lblAnio"></label>
                                    <img width="15" height="15" style="cursor:pointer"
                                        src="/clinica/utilidades/imagenes/acciones/derecha.png"
                                        onclick="irMesSiguienteHorario(); buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                                    <input name="btn_cerrar" type="button" class="small button blue" align="right"
                                        value="Refrescar"
                                        onclick="buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                    Exito:
                                    <select size="1" id="cmbExito" style="width:20%;"
                                        onchange="buscarAGENDA('listDiasC', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
                                        >
                                        <option value="S">Si</option>
                                        <option value="N">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="CENTER">
                                    <label id="lblFechaSeleccionada"></label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <table id="listDiasC" class="scroll"></table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>" />
        </td>
    </tr>
</table>


<div id="divVentanitaAgenda"
    style="position:absolute; display:none; background-color:#E2E1A5; top:200px; left:340px; width:50%; height:100px; z-index:999">
    <table width="100%" id="CabeceraSublistadoCalendario" border="1" class="fondoTabla">
        <tr class="estiloImput">
            <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaAgenda')" /></td>
            <td align="right" colspan="3"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                    onclick="ocultar('divVentanitaAgenda')" /></td>
        </tr>
        <tr class="titulosListaEspera">
            <td align="CENTER" colspan="2">ID HORARIO</td>
            <td align="CENTER" colspan="2">Minutos de duraci&oacute;n</td>
        </tr>
        <tr class="estiloImput">
            <td align="CENTER" colspan="2">
                <strong><label id="lblIdAgendaDetalle" style="color: red;"></label></strong>
            </td>
            <td align="CENTER" colspan="2">
                <strong><label id="lblDuracion"></label></strong>
            </td>
        </tr>
        <tr class="titulosListaEspera">
            <td width="20%">Fecha</td>
            <td width="20%">Hora</td>
            <td width="30%">Profesional</td>
            <td width="30%">Consultorio</td>
        </tr>
        <tr class="titulosListaEspera">
            <td>
                <input type="text" size="10" maxlength="10" title="Fecha Destino Cita" id="txtFechaDestinoCita" width="80%"/>
            </td>
            <td>
                <input type="time" id="txtHoraCitaDiv" width="90%">
            </td>
            <td>
                <select id="cmbIdProfesionalesAg" style="width:100%"
                    onfocus="cargarProfesionalesDesdeEspecialidadSede(this.id)" title='Seleccione Profesional para el cambio de cita'>
                    <option value=""></option>
                </select>
            </td>
            <td class="titulos">
                <select id="cmbConsultorioAg" style="width:80%"
                    onfocus="cargarComboGRALCondicion2('', '', this.id, 312, valorAtributo('cmbIdEspecialidad'), valorAtributo('cmbSede'))">
                    <option value=""></option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4"><hr></td>
        </tr>
        <tr class="titulosListaEspera">
            <td colspan="4">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <input id="btnEliminarCita" type="button" class="small button blue" value="ELIMINAR DISPONIBLE"
                                onclick="verificarNotificacionAntesDeGuardar('eliminarAgendaDetalle');"/>
                        </td>
                        <td>
                            <input class="small button blue" type="button" align="right" value="No diponible"
                                onclick="verificarNotificacionAntesDeGuardar('verificarFechasAgendaIndividualNoDisponible')"/>
                        </td>
                        <td>
                            <input class="small button blue" title="Permite Mover cita" type="button"
                                align="right" value="Modificar"
                                onclick="verificarNotificacionAntesDeGuardar('modificarAgenda')"/>
                        </td>
                        <td align="center">
                            Minutos:<select onfocus="calcularMinutosParaDividir()" id="cmbDuracionMinutosSubDividir" style="width:20%">
                                <option value="1">1</option>
                            </select>
                            <input id="btnSubDividirCita" type="button" class="small button blue" value="SUBDIVIDIR"
                                onclick="verificarNotificacionAntesDeGuardar('subdividirAgenda')"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<div id="divVentanitaObservacionNoDisponible"
    style="position:absolute; display:none; background-color:#E2E1A5; top:200px; left:100px; width:600px; height:100px; z-index:999">
    <table width="100%" class="fondoTabla">
        <tr class="titulosListaEspera">
            <td colspan="2">
                <textarea id="txtObservacionesNoDisponibleDiv" cols="60" rows="10" placeholder="Observaciones"></textarea>
            </td>
        </tr>
        <tr class="titulosListaEspera">
            <td colspan="2">
                <input type="button" class="small button blue" value="GUARDAR"
                    onclick="modificarCRUD('noDisponibleUnicoEspacio');"/>
                <input type="button" class="small button blue" value="CANCELAR"
                    onclick="ocultar('divVentanitaObservacionNoDisponible')"/>
            </td>
        </tr>
    </table>
</div>


<div  id="loaderEliminarDis" align="center" title="MENSAJE" style="z-index:3000; top:0px; left:0px; position:fixed; display:none;" >
    <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
    </div> 
    <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%;">     
        <table bgcolor="#FFFFFF" align="center"  width="350px" height="100px" id="tablaNotificacionesAlert" style="border: 1px solid #000;" >
            <tr>
                <td colspan="2" bgcolor="#7CD9FC" align="center" background="/clinica/utilidades/imagenes/menu/six_0_.gif" >
                    <b>ALERTA</b>
                </td>      
            </tr>	   
            <tr>
                <td>
                  <i><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="100px" height="100px"></i>
                </td>
                <td>
                    <i><label style="font-family: Helvetica; text-align: justify" id="lblTotNotifVentanillaAlert">Por favor espere mientras se eliminan los espacios disponible.<br><br>Esto puede tomar unos minutos.</label></i>
                </td>      
            </tr>
            <tr>
                <td align="center">  
                    <TABLE id="idTableBotonNotificaAlert" width="50%" border="1">
                    </TABLE>
                </td>        	               
            </tr>            
        </table>
        <br/>
    </div>   
</div>
