<%@ page import="java.util.ArrayList" %>
<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<!-- instanciar bean de session -->
<%  beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>" />

<table id="IdAgendaCita" width="1320px" height="1000px" align="center">
  <tr>
    <td>
      <div align="center" id="tituloForma">
        <jsp:include page="../../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="ASIGNACION DE CITAS." />
        </jsp:include>
      </div>
      <table width="100%" height="1000px" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td valign="top" width="40%">
            <table width="100%">
              <tr class="titulosListaEspera">
                <td width="30%">Sede</td>
                <td width="30%">Especialidad</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td>
                  <!-- <select size="1" id="cmbSede" style="width:90%" title="GD38"
                                            onfocus="comboDependienteSede('cmbSede', '557')"
                                            onchange="asignaAtributo('cmbIdEspecialidad', '', 0);asignaAtributo('cmbIdProfesionales', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad')">
                                    </select> -->
                  <input id="cmbSede" onchange="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"
                    style="width:70%;" placeholder="Escriba la prestadora que desea elegir" />
                </td>
                <td>
                  <!-- <select id="cmbIdEspecialidad"
                                            onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')"
                                            style="width:90%" 
                                            onchange="limpiaAtributo('cmbIdProfesionales');
                                                    limpiarListadosTotales('listAgenda');
                                                    buscarAGENDA('listDias')
                                            "
                                            >
                                        <option value=""></option>
                                    </select> -->

                  <select id="cmbIdEspecialidad"
                    onchange="cargarProfesionalesDesdeEspecialidadSede_1('cmbIdProfesionales');limpiarListadosTotales('listAgenda');buscarAGENDA('listDias')"
                    style="width:70%;" placeholder="Escriba la Especialidad que desea elegir" />


                </td>
                <!--<td>
                                    <input type="button" class="small button blue" style="margin: 5px 5px 5px 5px;" value="MOSTRAR PROGRAMACION CITAS" onclick="mostrarProgramacion()"/>   
                                    <input type="button" class="small button blue" style="margin: 5px 5px 5px 5px;" value="PROGRAMAR ORDENES" onclick="mostrarAgendaOrdenes()"/>           
                                </td>-->
            </table>
            <table width="100%">
              <tr class="titulosListaEspera">
                <td width="50%">Profesional</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td align="center">
                  <select size="1" id="cmbIdProfesionales" style="width:90%"
                    onfocus="cargarProfesionalesDesdeEspecialidadSede(this.id)" 
                    onchange="limpiarListadosTotales('listAgenda'); buscarAGENDA('listDias')">
                    <option value="">[ SELECCIONE ]</option>
                  </select>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr class="titulosListaEspera">
                <td width="33%">Estado</td>
                <td width="33%">Fecha Desde</td>
                <td width="33%">Rango (Días)</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td>
                  <select size="1" id="cmbEstadoAgenda" style="width:90%" onfocus="" 
                    onchange="buscarAGENDA('listDias')">
                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(329); ComboVO cmb329; for (int
                      k=0; k < resultaux.size(); k++) { cmb329=(ComboVO) resultaux.get(k); %>
                      <option value="<%= cmb329.getId()%>" title="<%= cmb329.getTitle()%>">
                        <%= cmb329.getId() + " " + cmb329.getDescripcion()%>
                      </option>
                      <%}%>
                  </select>
                </td>
                <td>
                  <input type="date" id="txtFechaDesdeAgenda" value="<%=LocalDate.now()%>" style="width:90%"
                    onchange="buscarAGENDA('listDias');">
                </td>
                <td>
                  <table width="80%">
                    <tr>
                      <td>
                        <input type="number" id="txtRangoAgenda" value="30" style="width:90%"
                          onchange="buscarAGENDA('listDias');">
                      </td>
                      <td>
                        <img width="20" height="20" style="cursor:pointer"
                          src="/clinica/utilidades/imagenes/icons/icons/arrow-left-square.svg"
                          onclick="cambiarPaginaCalendario('-');">
                      </td>
                      <td>
                        <img width="20" height="20" style="cursor:pointer"
                          src="/clinica/utilidades/imagenes/icons/icons/arrow-right-square.svg"
                          onclick="cambiarPaginaCalendario('+');" onchange="">
                      </td>
                    </tr>
                    <label id="lblMes" hidden></label><label id="lblAnio" hidden></label>
                    <input type="hidden" id="txtPagina" value="0">
                  </table>
                </td>
              </tr>
            </table>
          </td>
          <td valign="bottom" width="60%">
            <table width="100%">
              <tr class="titulosListaEspera">
                <td>Paciente</td>
              </tr>
              <tr class="estiloImputListaEspera">
                <td align="center">
                  <img width="18px" height="18px" align="middle" title="VEN32"
                    id="idLupitaVentanitaBuscarPacienteFiltro"
                    onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPacienteAgenda', '27')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                  <input readonly type="text" id="txtIdBusPacienteAgenda" onchange="buscarAGENDA('listAgenda')"
                    style="width:80%">
                  <img onclick="limpiaAtributo('txtIdBusPacienteAgenda'); buscarAGENDA('listAgenda')"
                    src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px"
                    align="middle">
                  <div id="divParaVentanita"></div>
                </td>
              </tr>
            </table>
            <table width="100%">
              <tr class="titulosListaEspera">
                <td><label id="lblFechaSeleccionada"></label></td>
                <td>
                  <fieldset>
                    <div>
                      <input type="radio" id="chk1" name="agruparAgendaPor" value="porProfesional" checked
                        onchange="buscarAGENDA('listAgenda')" />
                      <label for="contactChoice1">Por profesional</label>

                      <input type="radio" id="chk2" name="agruparAgendaPor" value="porJornada"
                        onchange="buscarAGENDA('listAgenda')" />
                      <label for="contactChoice2">Por jornada</label>
                    </div>
                  </fieldset>
                </td>
                <td align="center">
                  <label for="ckBolbeTurno">Doble turno:</label>
                  <input type="checkbox" id="ckDobleTurno" onchange="activarModDoblerCita();" />

                  <input id="btnUnificarCitas" type="button" class="small button blue"
                    style="margin: 5px 5px 5px 5px; visibility: hidden;" value="UNIFICAR CITAS"
                    onclick="verificarNotificacionAntesDeGuardar('validarCitasaUnificar');" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td valign="top">
            <div class="clsCalendarioAgenda" id="divListadoDias"></div>
          </td>
          <td valign="top">
            <div class="clsAgendaMedicos" id="divListAgenda">
              <table id="listAgenda"></table>
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<div id="divVentanitaAgenda"
  style="position:absolute; display:none; top:20px; left: 245px;  width:75%; height:90px; z-index:100">
  <label id="lblgestante" hidden=true></label>
  <table width="90%" class="fondoTabla">
    <tr class="estiloImput">
      <td align="left"><input name="btn_cerrar" title="divVentanitaAgenda" type="button" align="right" value="CERRAR"
          onclick="ocultarDivVentanitaAgenda(), cerrarTargeta()" /></td>
      <td align="CENTER" colspan="3"><label id="lblUsuariosDato"></label></td>
      <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
          onclick="ocultarDivVentanitaAgenda()" /></td>
    <tr>
    <tr class="titulosListaEspera">
      <td width="20%">ID CITA</td>
      <td width="15%">HORA</td>
      <td width="15%">DURACION MINUTOS</td>
      <td width="15%">FECHA</td>
      <td width="20%">CONSULTORIO</td>
    <tr>
    <tr class="estiloImputListaEspera">
      <td><label id="lblIdAgendaDetalle"></label></td>
      <td><label id="lblHora"></label></td>
      <td><label id="lblDuracion"></label></td>
      <td><label id="lblFechaCita"></label></td>
      <td><label id="lblConsultorio"></label></td>
    <tr>
    <tr class="camposRepInp">
      <td width="100%" colspan="5">Informaci&oacute;n del paciente</td>
    <tr>
    <tr class="titulosListaEspera">
      <td>
        <input id="idTraerListaEspera" type="button" class="small button blue" value="Lista Espera" title="btn_96u"
          onclick="limpiarListadosTotales('listListaEsperaTraer');  abrirVentanaDesdeListaEspera();" />
      </td>
      <td colspan="2" align="CENTER">PACIENTE<a style="color:#FF0000;">(*)</a></td>
      <td></td>
      <td></td>
    <tr>
    <tr class="estiloImput">
      <td>
        Id LE: <label id="lblIdListaEspera"></label>
      </td>
      <td colspan="2">
        <input type="text" id="txtIdBusPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdBusPaciente', 307)"
          style="width:90%" />
        <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaBuscarPaciente"
          onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')"
          src="/clinica/utilidades/imagenes/acciones/buscar.png">
      </td>
      <td>
        <input id="btn_busPacienteCita___" type="button" title="55GH_" class="small button blue" value="BUSCAR"
          onclick="buscarInformacionBasicaPaciente()" />
      </td>
      <td>
        <label class="parpadea" style="color: #ff0000; display: none; font-size: medium;" id="lblEstadio"></label>
      </td>
    </tr>
    <tr class="estiloImputListaEspera">
      <td colspan="6">
        <!-- DATOS BASICOS PACIENTE-->
        <table width="100%">
          <tr class="titulosListaEspera">
            <td width="15%">Tipo Id</td>
            <td width="10%">Identificacion</td>
            <td width="24%">Apellidos</td>
            <td width="24%">Nombres</td>
            <td width="10%">Fecha Nacimiento</td>
            <td width='12%'>Edad</td>
            <td width="15%">Sexo</td>
          </tr>
          <tr>
            <td>
              <img src="/clinica/utilidades/imagenes/acciones/buscar.png" width="16px" height="16px"
                onclick="guardarYtraerDatoAlListado('buscarIdentificacionPaciente'); limpTablas('listDocumentosHistoricos');ocultar('divArchivosAdjuntos')"
                align="center" title="Boton buscari" />
              <select size="1" id="cmbTipoId" style="width:85%" >
                <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(105); ComboVO cmb105; for (int k=0; k
                  < resultaux.size(); k++) { cmb105=(ComboVO) resultaux.get(k); %>
                  <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                    <%= cmb105.getId() + "  " + cmb105.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <!--onkeypress="return valida(event)"  onkeypress="return validaEspacios();"  -->
            <td><input type="text" id="txtIdentificacion" style="width:80%" size="20" maxlength="20"
                onKeyPress="javascript:checkKey2(event);javascript:return soloTelefono(event);"  />
            </td>
            <td>
              <input type="text" id="txtApellido1" size="30" maxlength="30"
                onkeypress="return excluirEspeciales(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
              <input type="text" id="txtApellido2" size="30" maxlength="30"
                onkeypress="return excluirEspeciales(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
            </td>
            <td>
              <input type="text" id="txtNombre1" size="30" maxlength="30"
                onkeypress="javascript:return teclearsoloan(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
              <input type="text" id="txtNombre2" size="30" maxlength="30"
                onkeypress="javascript:return teclearsoloan(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
            </td>
            <td><input id="txtFechaNac" type="text" style="width:95%" /> </td>
            <td style="text-align: center;">
              <label id="txtEdad" onlyread style="display:none;"></label>
              <label id="txtEdadCompleta"><label>
            </td>
            <td>
              <select size="1" id="cmbSexo" style="width:95%" >
                <option value=""></option>
                <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(110); ComboVO cmbS; for (int k=0; k <
                  resultaux.size(); k++) { cmbS=(ComboVO) resultaux.get(k); %>
                  <option value="<%= cmbS.getId()%>" title="<%= cmbS.getTitle()%>">
                    <%=cmbS.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
          </tr>
        </table>
        <!-- FIN DATOS BASICOS -->
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <table>
          <tr class="titulosListaEspera">
            <td width="17%">Nacionalidad</td>
            <td width="17%">Municipio</td>
            <td width="20%">Direcci&oacute;n</td>
            <td width="16%">Acompa&ntilde;ante</td>
            <td width="10%">Tel&eacute;fono</td>
            <td width="10%">Celular 1</td>
            <td width="10%">Celular 2</td>
          </tr>
          <tr class="estiloImput">
            <td><input type="text" id="txtNacionalidad" size="60" maxlength="60"
                oninput="llenarElementosAutoCompletarKey(this.id, 3704, this.value)" style="width:80%"  />
              <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni"
                onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtNacionalidad', '60')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
              <div id="divParaVentanita"></div>
            </td>
            <td><input type="text" id="txtMunicipio" size="60" maxlength="60"
                oninput="llenarElementosAutoCompletarKey(this.id, 3704, this.value)" style="width:80%"  />
              <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni"
                onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
              <div id="divParaVentanita"></div>
            </td>
            <td><input type="text" id="txtDireccionRes" size="100" maxlength="100" style="width:99%"
                onkeyup="javascript: this.value = this.value.toUpperCase();"
                onkeypress="javascript:return teclearsoloan(event);"  /></td>
            <td><input type="text" id="txtNomAcompanante" size="100" maxlength="100" style="width:95%"
                onkeyup="javascript: this.value = this.value.toUpperCase();"
                onkeypress="javascript:return teclearExcluirCaracter(event);"  /> </td>
            <td>
              <input type="text" id="txtTelefonos" size="100" style="width:90%" minlength="7" maxlength="7"
                onkeypress="javascript:return soloTelefono(event);"  />
            </td>
            <td>
              <input type="text" id="txtCelular1" size="30" style="width:90%" minlength="10" maxlength="10"
                onkeypress="javascript:return soloTelefono(event);"  />
            </td>
            <td>
              <input type="text" id="txtCelular2" size="30" style="width:90%" minlength="10" maxlength="10"
                onkeypress="javascript:return soloTelefono(event);"  />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <table>
          <tr class="titulosListaEspera">
            <td width="20%">Email</td>
            <td width="20%">Administradora Paciente</td>
            <td width="30%">IPS Primaria</td>
            <td width="10%">Tipo de Regimen</td>
            <td width="10%">Nivel Sisben</td>
            <td width="10%"></td>
          </tr>
          <tr class="estiloImputListaEspera">
            <td><input type="text" id="txtEmail" size="40" maxlength="60" style="width:90%"
                onkeypress="javascript:return email(event);"  /> </td>
            <td>
              <input type="text" id="txtAdministradoraPaciente"
                onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraPaciente', 308)" style="width:80%"
                 />
              <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaAdPaciente"
                onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtAdministradoraPaciente', '21')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
            </td>
            <td>
              <input type="text" id="txtIpsPaciente" style="width:90%"  />
            </td>
            <td>
              <select id="cmbTipoRegimen" style="width:90%;">
                <option value=""></option>
                <%resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(518); ComboVO cmbPro1; for (int k=0; k
                  < resultaux.size(); k++) { cmbPro1=(ComboVO) resultaux.get(k);%>
                  <option value="<%= cmbPro1.getId()%>" title="<%= cmbPro1.getTitle()%>">
                    <%=cmbPro1.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <td>
              <select id="cmbNivelSisben" style="width:90%;">
                <option value=""></option>
                <%resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(3505); ComboVO cmbNS; for (int k=0; k
                  < resultaux.size(); k++) { cmbNS=(ComboVO) resultaux.get(k);%>
                  <option value="<%= cmbNS.getId()%>" title="<%= cmbNS.getTitle()%>">
                    <%=cmbNS.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <td>
              <% if (beanSession.usuario.preguntarMenu("6_b1")) {%>
                <input id="btnCrearNuevoPaciente" type="button" class="small button blue" value="Crear nuevo paciente"
                  onclick="modificarCRUD('crearNuevoPaciente')" />
                <%}%>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr class="camposRepInp">
      <td width="100%" colspan="6">Informaci&oacute;n de cita</td>
    </tr>
    <tr>
      <td colspan="6">
        <table style="width: 100%;">
          <tr class="titulosListaEspera">
            <td width="30%">Administradora Agenda</td>
            <td width="30%">Tipo R&eacute;gimen - Plan<a style="color:#FF0000;">(*)</a></td>
            <td width="40%">Plan de Contrataci&oacute;n</td>
          </tr>
          <tr class="estiloImputListaEspera">
            <td>
              <input type="text" id="txtAdministradora1"
                onchange="limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                onkeypress="llamarAutocompletarEmpresa('txtAdministradora1', 184)" style="width: 87%;"  />
              <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem"
                onclick="traerVentanitaEmpresa(this.id, 'divParaVentanita', 'txtAdministradora1', '24');limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
            </td>
            <td>
              <select id="cmbIdTipoRegimen" style="width: 95%;" onclick="limpiarDivEditarJuan('limpiarDatosPlan')"
                onfocus="cargarComboGRALCondicion3('', '', this.id, 223, valorAtributoIdAutoCompletar('txtAdministradora1'), valorAtributo('cmbSede'), valorAtributoIdAutoCompletar('txtIdBusPaciente'))"
                >
                <option value="">[ SELECCIONE ]</option>
              </select>
            </td>
            <td>
              <label id="lblIdPlanContratacion" hidden></label><label id="lblNomPlanContratacion" hidden></label>
              <select id="cmbIdPlan" style="width: 95%;"
                onfocus="limpiarDivEditarJuan('limpiarDatosRegimenFactura');
                                                                        limpiarDivEditarJuan('limpiarDatosPlan')
                                                                        traerPlanesDeContratacionAgenda(this.id, '165', 'txtAdministradora1', 'cmbIdTipoRegimen')"
                onchange="asignaAtributo('lblIdPlanContratacion', valorAtributo(this.id), 0)" >
                <option value=""></option>
              </select>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="6">
        <table style="width: 100%">
          <tr class="titulosListaEspera">
            <td width="20%">N&uacute;mero autorizaci&oacute;n</td>
            <td width="20%">Fecha vigencia autorizaci&oacute;n</td>
            <td>Fecha que paciente desea la cita<a style="color:#FF0000;">(*)</a></td>
            <td width="40%">Instituci&oacute;n</td>
          </tr>
          <tr class="estiloImputListaEspera">
            <td>
              <input type="text" id="txtNoAutorizacion" size="30" maxlength="50" style="width:94%"
                onKeyPress="javascript:return teclearExcluirCaracter(event, '%');"  />
            </td>
            <td>
              <input id="txtFechaVigencia" type="date" size="10" maxlength="10" style="width:70%"
                onkeydown="return false;" />
            </td>
            <td><input id="txtFechaPacienteCita" type="date" style="width:70%"
                onchange="verificarNotificacionAntesDeGuardar('verificarFechaPaciente')" onkeydown="return false;">
            </td>
            <td><input type="text" id="txtIPSRemite" size="60" maxlength="60"
                onkeypress="llamarAutocomIdDescripcionConDato('txtIPSRemite', 313)" style="width:90%"  />
              <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaMuni"
                onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIPSRemite', '4')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <table width="100%">
          <tr class="titulosListaEspera">
            <td width="40%">Programa/Tipo:<a style="color:#FF0000;">(*)</a></td>
            <td width='15%'>Modalidad:<a title="Debe seleccionar un tipo de cita primero."
                style="color:#FF0000;">(*)</a></td>
            <td width="15%">Estado:</td>
            <td id="lbVia_Atribuido" width="15%">Via:<a style="color:#FF0000;">(*)</a></td>
            <td width="15%">Clasificaci�n:</td>
            <!--<td width="15%">Nro. de Referencia</td>-->
          </tr>
          <tr class="estiloImput">
            <td valign="top">
              <table style="width:100%">
                <tr>
                  <td>
                    <select id="cmbProgramaCita" style="width:95%"  on
                      onfocus="cargarComboGRALCondicion1('cmbPadre', '1', this.id, 3566, traerDatoFilaSeleccionada('listAgenda', 'ID_ESPECIALIDAD'));"
                      onchange="mostrarTipoCita(this.id)">
                      <option value=""></option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    <select id="cmbTipoCita" onchange="mostrarRadioModalidad(this.id);"
                      onfocus="cargarTipoCitaSegunPrograma(this.id)" style="width:95%; display:none;" >
                      <option value=""></option>
                    </select>
                  </td>
                </tr>
              </table>
            </td>
            <td rowspan="2">
              <center>
                <table id="idRadioDiv"></table>
                <!--
                                                    <table>                                                                           
                                                    <%  resultaux.clear();
                                                        resultaux = (ArrayList) beanAdmin.combo.cargar(3529);
                                                        ComboVO rdMod;
                                                        for (int k = 0; k < resultaux.size(); k++) {
                                                            rdMod = (ComboVO) resultaux.get(k);
                                                    %>                                
                                                    <tr>
                                                        <td><input type="radio" id="radioMod<%=rdMod.getId()%>" name="checkModalidad" value="<%=rdMod.getId()%>"/></td>
                                                        <td><label for="radioMod<%=rdMod.getId()%>"><%=rdMod.getDescripcion()%></label></td>
                                                    </tr>
                                                    <%}%>                           
                                                </table>
                                                    -->
              </center>
            </td>
            <td valign="top">
              <input type="hidden" id="txtEstadoCita" />
              <select id="cmbEstadoCita" onfocus="limpiarDivEditarJuan('limpiarMotivoAD')"
                onchange="cambiarLbVia_Atribuido();" style="width:100%" >
                <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(107); ComboVO cmbEst; for (int k=0; k
                  < resultaux.size(); k++) { cmbEst=(ComboVO) resultaux.get(k); %>
                  <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                    <%= cmbEst.getId() + "  " + cmbEst.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <td valign="top">
              <select id="cmbMotivoConsulta" style="width:95%" 
                onfocus="comboDependiente('cmbMotivoConsulta', 'cmbEstadoCita', '96');limpiarDivEditarJuan('limpiarMotivoClase');">
                <option value=""></option>
                <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(106); ComboVO cmbM; for (int k=0; k <
                  resultaux.size(); k++) { cmbM=(ComboVO) resultaux.get(k); %>
                  <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                    <%= cmbM.getId() + "  " + cmbM.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <td valign="top">
              <select id="cmbMotivoConsultaClase" style="width:95%" 
                onfocus="comboDependienteDosCondiciones('cmbMotivoConsultaClase', 'cmbMotivoConsulta', 'cmbIdEspecialidad', '563')">
                <option value=""></option>
              </select>
            </td>
            <!--<td>
                                            <input type="text" id="txtNoLente" size="60" maxlength="60" style="width:80%"  onkeypress="javascript:return teclearsoloDigitos(event);" class="">
                                            <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaNoLente"  onclick="traerVentanitaCondicion1Autocompletar(this.id, 'txtIdBusPaciente','divParaVentanitaCondicion1','txtNoLente','17')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                                            <div id="divParaVentanitaCondicion1"></div>
                                        </td>-->
          </tr>
          <tr>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <table width="100%">
          <tr class="titulosListaEspera">
            <td colspan="5">Observaciones</td>
          </tr>
          <tr class="estiloImput">
            <td colspan="5"><input type="text" id="txtObservacion" size="200" maxlength="2000" style="width:80%"
                onKeyPress="javascript:return teclearExcluirCaracter(event, '%');"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  />
              <input name="btn_crear_cita" type="button" class="small button blue" title="B777" value="Ver"
                onclick="mostrar('divVentanitaNotificaciones');buscarAGENDA('listAgendaNotifica')" />
              <input id="txtObservacionConfirma" size="200" maxlength="2000" type="hidden"
                onKeyPress="javascript:return teclearExcluirCaracter(event, '%');"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  />
            </td>
          </tr>
          <tr class="titulos">
            <td width="100%" colspan="4">
              <div style="overflow:auto; height:110px; width:100%" id="divProcedimientosLE">
                <table id="listProcedimientosAgenda" class="scroll"></table>
              </div>
            </td>
          </tr>
        </table>

        <table width="100%" id="idInformacionCirugia">
            
          <tr class="titulosCentrados" >
            <td width="100%" colspan="5">Informaci&oacute;n de la cirugia</td> &nbsp;
          <tr>


          <tr>
            <td colspan="5">
              <div id="divAcordionPersonalCirugia" style="display:BLOCK">      
                  <jsp:include page="divsAcordeonCirugia.jsp" flush="FALSE" >
                      <jsp:param name="titulo" value="Personal Cirugia" />
                      <jsp:param name="idDiv" value="divPersonalCirugia" />
                      <jsp:param name="pagina" value="personalProcedimientosCirugia.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="buscarAGENDA('listPersonalCirugia')" />
                    </jsp:include>
              </div>  
            </td>
          </tr>

          <tr class="titulosListaEspera" >
            <td width="50%" >Elemento a implantar</td>     
            <td width="50%">Tipo Anestesia</td>   
          </tr>                 
          <tr class="estiloImput" >
              <td><input type="text" id="txtNoLente" value="NA"  size="60"  maxlength="60"  style="width:80%"   />
                  <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaNoLente"  onclick="traerVentanitaCondicion1Autocompletar(this.id, 'txtIdBusPaciente','divParaVentanitaCondicion1','txtNoLente','17')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                  <div id="divParaVentanitaCondicion1"></div>
              </td>  
              <td>
                <select id="cmbTipoAnestesia" style="width:80%"  >
                  <option value=""></option>
                    <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(119);	
                            ComboVO cmbTA; 
                            for(int k=0;k<resultaux.size();k++){ 
                                  cmbTA=(ComboVO)resultaux.get(k);
                    %>
                  <option value="<%= cmbTA.getId()%>" title="<%= cmbTA.getTitle()%>"><%= cmbTA.getDescripcion()%></option>
                            <%}%>
                </select>               
              </td>  
                  <td colspan="2"><input value="NA" type="hidden" id="txtAnestesiologo"  size="200" maxlength="200"  style="width:98%"  onkeypress="llamarAutocomIdDescripcionConDato('txtAnestesiologo',298)" /></td>
                                              
          </tr>  
          <td colspan="2"><input type="text" id="txtObservacionFolio"  size="1000" maxlength="1000"  style="width:98%"   /></td> 

          <tr class="estiloImput" >
            <td colspan="2"><input value="NA" type="hidden" id="txtInstrumentador"  size="200" maxlength="200"  style="width:98%"  onkeypress="llamarAutocomIdDescripcionConDato('txtInstrumentador',298)"  /></td>
            <td colspan="2"><input value="NA" type="hidden" id="txtCirculante"  size="200" maxlength="200"  style="width:98%"  onkeypress="llamarAutocomIdDescripcionConDato('txtCirculante',298)" /></td>               
          </tr>  

        </table>
        <table width="100%">                
          <tr class="titulosCentrados" >
            <td width="100%" colspan="5">Informaci&oacute;n de Procedimientos</td> 
          <tr>
          <tr class="titulosListaEspera">
            <td>Procedimiento CUPS</td>
            <td>Sitio</td>
            <td colspan="2">Observaciones Procedimiento</td>
            <td></td>
          </tr>
          <tr class="titulos">
            <td><input type="text" id="txtIdProcedimiento" style="width:98%"
                onkeypress="llamarAutocomIdDescripcionParametro('txtIdProcedimiento', 420, 'lblIdPlanContratacion')"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  />
            </td>
            <td>
              <select id="cmbIdSitioQuirurgicoLE" style="width:80%" >                
                <% resultaux.clear(); resultaux=(ArrayList)beanAdmin.combo.cargar(120); ComboVO cmbL; for(int
                  k=0;k<resultaux.size();k++){ cmbL=(ComboVO)resultaux.get(k); %>
                  <option value="<%= cmbL.getId()%>" title="<%= cmbL.getTitle()%>">
                    <%= cmbL.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <td colspan="2">
              <input type="text" id="txtObservacionProcedimientoCirugia"
                onkeyup="javascript: this.value = this.value.toUpperCase();" style="width:95%" />
            </td>
            <td>
              <input name="btn_crear_cita" type="button" class="small button blue" value="ADICIONAR" title="BT722"
                onclick="adicionarProcedimientoAgenda()"  />
            </td>
          </tr>
          <tr class="titulos">
            <td width="100%" colspan="5">
              <div style="width:100%" id="divCitaCirugiaProcedimientos">
                <table id="listCitaCexProcedimientoTraer" class="scroll"></table>
              </div>
            </td>
          </tr>
          <tr class="titulosListaEspera">
            <td align="CENTER" width="48%">
              <input type="checkbox" id="guardarCitasBloque" onchange="reaccionAEvento(this.id)"> Crear cita en bloque ?
              <input type="hidden" id="txtColumnaIdSitioQuirur" />
              <input type="hidden" id="txtColumnaIdProced" />
              <input type="hidden" id="txtColumnaObservacion" />
            </td>
            <td colspan="4" align="LEFT" width="48%">
              <input name="btn_crear_cita" id="btnGuardarCita" type="button" class="small button blue" value="GUARDAR"
                onclick="guardarUnaCitaAgenda()" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>

<!--divs eliminados ?-->
<div id="divVentanitaEliminarOrdenExt" style="display:none; z-index:2000;">
  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2004; position:relative; top:-700px;left:-80px; width:100%">
    <table width="50%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEliminarOrdenExt')" /></td>
        <td align="right" colspan="2"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEliminarOrdenExt')" /></td>
      </tr>
      <tr class="titulosListaEspera">
        <td colspan="2" align="CENTER">DESEA ELIMINAR ESTA ORDEN ?</td>
      </tr>
      <tr class="estiloImput">
        <td align="CENTER">
          <input type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarOrdenExt')" />
        </td>
        <td align="CENTER">
          <input type="button" class="small button blue" value="CANCELAR"
            onclick="ocultar('divVentanitaEliminarOrdenExt')" />
        </td>
      </tr>
    </table>
  </div>
</div>

<div id="divVentanitaEliminarOrdenInt" style="display:none; z-index:2000;">
  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2004; position:relative; top:-700px;left:-80px; width:100%">
    <table width="50%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEliminarOrdenInt')" /></td>
        <td align="right" colspan="2"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEliminarOrdenInt')" /></td>
      </tr>
      <tr class="titulosListaEspera">
        <td colspan="2" align="CENTER">DESEA ELIMINAR ESTA ORDEN ?</td>
      </tr>
      <tr class="estiloImput">
        <td align="CENTER">
          <input type="button" class="small button blue" value="ELIMINAR"
            onclick="verificarNotificacionAntesDeGuardar('eliminarOrdenInt')" />
        </td>
        <td align="CENTER">
          <input type="button" class="small button blue" value="CANCELAR"
            onclick="ocultar('divVentanitaEliminarOrdenInt')" />
        </td>
      </tr>
    </table>
  </div>
</div>

<div id="divVentanitaEditarTerapia" style="display:none; z-index:2000;">
  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2004; position:relative; top:-450px;left:-100px; width:100%">
    <table width="80%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarTerapia')" /></td>
        <td align="right" colspan="5"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarTerapia')" /></td>
      </tr>
      <tr class="titulosListaEspera">
        <td width="20%">ESPECIALIDAD</td>
        <td width="25%">TIPO CITA</td>
        <td width="25%">PROFESIONAL</td>
        <td width="10%">FECHA</td>
        <td width="10%">HORA</td>
        <td></td>
      </tr>
      <tr class="estiloImput">
        <td>
          <select id="cmbIdEspecialidadProgramacionEditar"
            onchange="limpiaAtributo('cmbTipoCitaProgramacionEditar', 0); limpiaAtributo('cmbIdProfesionalProgramacionEditar', 0)"
            onfocus="cargarEspecialidadDesdeSede('lblIdSedeAgendaOrdenes', this.id)" style="width:90%">
            <option value="">[ SELECCIONE ]</option>
          </select>
        </td>
        <td>
          <select id="cmbTipoCitaProgramacionEditar"
            onfocus="cargarCitaSegunEspecialidad(this.id, 'cmbIdEspecialidadProgramacionEditar')" style="width:90%">
            <option value="">[ SELECCIONE ]</option>
          </select>
        </td>
        <td><select id="cmbIdProfesionalProgramacionEditar" style="width:90%"
            onfocus="cargarComboGRALCondicion2('',
                                                                                '',
                                                                                this.id,
                                                                                197,
                                                                                valorAtributo('cmbIdEspecialidadProgramacionEditar'),
                                                                                valorAtributo('lblIdSedeAgendaOrdenes'));">
            <option value="">[ SELECCIONE ]</option>
          </select>
        </td>
        <td><input id="txtFechaTerapiaEditar" type="date" style="width:90%" /></td>
        <td><input id="txtHoraTerapiaEditar" type="time" style="width:95%" /></td>
        <td align="center">
          <input type="button" class="small button blue" value="MODIFICAR"
            onclick="verificarNotificacionAntesDeGuardar('modificarProgramacionTerapiaAgenda')" />
        </td>
      </tr>
      <tr>
        <td colspan="6">
          <hr>
        </td>
      </tr>
      <tr class="titulosListaEspera">
        <td colspan="6">
          <table id="elementosModificarCitaBorrador" width="100%">
            <tr>
              <td>ESTADO</td>
              <td>VIA</td>
              <td>CLASIFICACION</td>
              <td>OBSERVACIONES</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <select id="cmbEstadoCitaModificarCitaProgramacion"
                  onchange="limpiaAtributo('cmbMotivoConsulModificarCitaProgramacion');
                                                                                    limpiaAtributo('cmbMotivoConsultaClaseModificarCitaProgramacion')" style="width:80%">
                  <option>[ SELECCIONE ]</option>
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(107); for (int k=0; k <
                    resultaux.size(); k++) { cmbEst=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                      <%= cmbEst.getId() + "  " + cmbEst.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
              <td>
                <select id="cmbMotivoConsulModificarCitaProgramacion" style="width:80%"
                  onfocus="comboDependiente(this.id, 'cmbEstadoCitaModificarCitaProgramacion', '96');"
                  onchange="limpiaAtributo('cmbMotivoConsultaClaseModificarCitaProgramacion')">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <select id="cmbMotivoConsultaClaseModificarCitaProgramacion" style="width:90%" onfocus="cargarComboGRALCondicion2('',
                                                                                            '',
                                                                                            this.id,
                                                                                            563,
                                                                                            valorAtributo('cmbMotivoConsulModificarCitaProgramacion'),
                                                                                            12);">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <textarea id="txtObservacionModificarCitaProgramacion" style="width:90%" rows="2"
                  placeholder="Observaciones"></textarea>
              </td>
              <td align="CENTER">
                <input type="button" class="small button blue" value="MODIFICAR ESTADO"
                  onclick="verificarNotificacionAntesDeGuardar('modificarEstadoCitaProgramacion')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="6">
          <table id="elementosEliminarCitaBorrador" width="100%" hidden>
            <tr>
              <td align="CENTER">
                <input type="button" class="small button blue" value="ELIMINAR BORRADOR"
                  onclick="modificarCRUD('eliminarCitasBorrador')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>
<!--asta aqui divs que se eliminaron ??-->

<div id="divVentanitaEditarProgramacionCitasBloque" style="display:none; z-index:2000;">
  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2004; position:relative; top:-500px;left:-100px; width:100%">
    <table width="60%" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarProgramacionCitasBloque')" /></td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarProgramacionCitasBloque')" /></td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr class="titulosListaEspera">
              <td width="30%">ESPECIALIDAD</td>
              <td width="30%">TIPO CITA</td>
              <td width="30%">PROFESIONAL</td>
              <td></td>
            </tr>
            <tr class="estiloImput">
              <td>
                <select id="cmbIdEspecialidadProgramacioCitasEditarBloque"
                  onchange="limpiaAtributo('cmbTipoCitaProgramacionCitasEditarBloque', 0);
                                                                                    limpiaAtributo('cmbIdProfesionalProgramacionCitasEditarBloque', 0)"
                  onfocus="cargarEspecialidadDesdeSede('lblIdSedeAgendaOrdenes', 'cmbIdEspecialidadProgramacioCitasEditarBloque')"
                  style="width:90%">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <select id="cmbTipoCitaProgramacionCitasEditarBloque"
                  onfocus="cargarCitaSegunEspecialidad(this.id, 'cmbIdEspecialidadProgramacioCitasEditarBloque')"
                  style="width:90%">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td><select id="cmbIdProfesionalProgramacionCitasEditarBloque" style="width:90%"
                  onfocus="cargarComboGRALCondicion2('',
                                                                                            '',
                                                                                            this.id,
                                                                                            197,
                                                                                            valorAtributo('cmbIdEspecialidadProgramacioCitasEditarBloque'),
                                                                                            valorAtributo('lblIdSedeAgendaOrdenes'));">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <input type="button" class="small button blue" value="MODIFICAR"
                  onclick="verificarNotificacionAntesDeGuardar('modificarProgramacionCitasBloque')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <hr>
        </td>
      </tr>
      <tr class="titulosListaEspera">
        <td colspan="2">
          <table id="elementosModificarCitaBorrador" width="100%">
            <tr>
              <td>ESTADO</td>
              <td>VIA</td>
              <td>CLASIFICACION</td>
              <td>OBSERVACIONES</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <select id="cmbEstadoCitaModificarBloque"
                  onchange="limpiaAtributo('cmbMotivoConsulModificarBloque');
                                                                            limpiaAtributo('cmbMotivoConsultaClaseModificarBloque')" style="width:80%"
                  >
                  <option>[ SELECCIONE ]</option>
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(107); for (int k=0; k <
                    resultaux.size(); k++) { cmbEst=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                      <%= cmbEst.getId() + "  " + cmbEst.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
              <td>
                <select id="cmbMotivoConsulModificarBloque" style="width:80%"
                  onfocus="comboDependiente(this.id, 'cmbEstadoCitaModificarBloque', '96');"
                  onchange="limpiaAtributo('cmbMotivoConsultaClaseModificarBloque')">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <select id="cmbMotivoConsultaClaseModificarBloque" style="width:90%" onfocus="cargarComboGRALCondicion2('',
                                                                                            '',
                                                                                            this.id,
                                                                                            563,
                                                                                            valorAtributo('cmbMotivoConsulModificarBloque'),
                                                                                            12);">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <textarea id="txtObservacionReprogramarCitasBloque" style="width:90%" rows="2"
                  placeholder="Observaciones"></textarea>
              </td>
              <td align="CENTER">
                <input type="button" class="small button blue" value="MODIFICAR ESTADO"
                  onclick="verificarNotificacionAntesDeGuardar('modificarEstadoProgramacionCitasBloque')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>


<div id="divVentanitaEditarProgramacionLE" style="display:none; z-index:2000;">
  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2004; position:relative; top:-250px;left:-120px; width:100%">
    <table width="80%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarProgramacionLE')" /></td>
        <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarProgramacionLE')" /></td>
      </tr>
      <tr class="titulosListaEspera">
        <td width="25%">ESPECIALIDAD</td>
        <td width="25%">TIPO CITA</td>
        <td width="30%">PROFESIONAL</td>
        <td width="10%">FECHA</td>
        <td width="10%"></td>
      </tr>
      <tr class="estiloImput">
        <td>
          <select id="cmbIdEspecialidadProgramacioLEEditar"
            onchange="limpiaAtributo('cmbTipoCitaProgramacionLEEditar', 0);
                                                                        limpiaAtributo('cmbIdProfesionalProgramacionLEEditar', 0)"
            onfocus="cargarEspecialidadDesdeSede('lblIdSedeAgendaOrdenes', 'cmbIdEspecialidadProgramacioLEEditar')"
            style="width:90%">
            <option value="">[ SELECCIONE ]</option>
          </select>
        </td>
        <td>
          <select id="cmbTipoCitaProgramacionLEEditar"
            onfocus="cargarCitaSegunEspecialidad(this.id, 'cmbIdEspecialidadProgramacioLEEditar')" style="width:90%">
            <option value="">[ SELECCIONE ]</option>
          </select>
        </td>
        <td><select id="cmbIdProfesionalProgramacionLEEditar" style="width:90%"
            onfocus="cargarComboGRALCondicion2('',
                                                                                '',
                                                                                this.id,
                                                                                197,
                                                                                valorAtributo('cmbIdEspecialidadProgramacioLEEditar'),
                                                                                valorAtributo('lblIdSedeAgendaOrdenes'));">
            <option value="">[ SELECCIONE ]</option>
          </select>
        </td>
        <td><input id="txtFechaProgramacionLEEditar" type="date" style="width:90%" /></td>
        <td>
          <input id="BTN_MO" title="BI101" type="button" class="small button blue" value="MODIFICAR"
            onclick="verificarNotificacionAntesDeGuardar('modificarProgramacionLE')" />
        </td>
      </tr>
      <!--<tr class="estiloImputListaEspera">
                                                    <td colspan="2">
                                                        <input id="BTN_MO" title="BI101" type="button" class="small button blue"
                                                               value="MODIFICAR"
                                                               onclick="verificarNotificacionAntesDeGuardar('modificarProgramacionLE')"/>
                                                    </td>
                                                    <td colspan="2">
                                                        <div id="divEliminarLEProgramacion" hidden>
                                                            <input type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarProgramacionLE')"/> 
                                                        </div>
                                                    </td>
                                                </tr>-->
      <tr>
        <td colspan="5">
          <hr>
        </td>
      </tr>
      <tr>
        <td colspan="5">
          <table width="100%">
            <tr>
              <td width="90%" align="RIGHT">
                <textarea id="txtObservacionReprogramarLE" cols="90" rows="2" placeholder="Observaciones"></textarea>
              </td>
              <td align="RIGHT">
                <input type="button" class="small button blue" value="REPROGRAMAR"
                  onclick="verificarNotificacionAntesDeGuardar('reprogramarProgramacionLE')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>

<div id="divVentanitaEditarProgramacionLEBloque" style="display:none; z-index:2000;">
  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2004; position:relative; top:-250px;left:-100px; width:100%">
    <table width="60%" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarProgramacionLEBloque')" /></td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarProgramacionLEBloque')" /></td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr class="titulosListaEspera">
              <td width="30%">ESPECIALIDAD</td>
              <td width="30%">TIPO CITA</td>
              <td width="30%">PROFESIONAL</td>
              <td></td>
            </tr>
            <tr class="estiloImput">
              <td>
                <select id="cmbIdEspecialidadProgramacioLEEditarBloque"
                  onchange="limpiaAtributo('cmbTipoCitaProgramacionLEEditarBloque', 0);
                                                                                    limpiaAtributo('cmbIdProfesionalProgramacionLEEditarBloque', 0)"
                  onfocus="cargarEspecialidadDesdeSede('lblIdSedeAgendaOrdenes', 'cmbIdEspecialidadProgramacioLEEditarBloque')"
                  style="width:90%">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <select id="cmbTipoCitaProgramacionLEEditarBloque"
                  onfocus="cargarCitaSegunEspecialidad(this.id, 'cmbIdEspecialidadProgramacioLEEditarBloque')"
                  style="width:90%">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td><select id="cmbIdProfesionalProgramacionLEEditarBloque" style="width:90%"
                  onfocus="cargarComboGRALCondicion2('',
                                                                                            '',
                                                                                            this.id,
                                                                                            197,
                                                                                            valorAtributo('cmbIdEspecialidadProgramacioLEEditarBloque'),
                                                                                            valorAtributo('lblIdSedeAgendaOrdenes'));">
                  <option value="">[ SELECCIONE ]</option>
                </select>
              </td>
              <td>
                <input type="button" class="small button blue" value="MODIFICAR"
                  onclick="verificarNotificacionAntesDeGuardar('modificarProgramacionLEBloque')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <hr>
        </td>
      </tr>
      <tr class="titulosListaEspera">
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="90%" align="RIGHT">
                <textarea id="txtObservacionReprogramarBloque" cols="90" rows="2"
                  placeholder="Observaciones"></textarea>
              </td>
              <td align="RIGHT">
                <input type="button" class="small button blue" value="REPROGRAMAR"
                  onclick="verificarNotificacionAntesDeGuardar('reprogramarProgramacionLEBloque')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <!--<tr class="estiloImputListaEspera">
                                                    <td colspan="2">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%">
                                                                        <tr><td class="" align="right">ESPECIALIDAD Y TIPO DE CITA <input type="checkbox" id="chkModificarEspecialidadLE"></td></tr>
                                                                        <tr>
                                                                            <td class="" align="right">PROFESIONAL <input type="checkbox" id="chkModificarProfesionalLE"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    <input type="button" class="small button blue"
                                                                        value="MODIFICAR"
                                                                        onclick="verificarNotificacionAntesDeGuardar('modificarProgramacionLEBloque')"/>
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                    </td>
                                                </tr>-->
    </table>
  </div>
</div>

<!--  -->
<div id="divVentanitaListaEspera" class="listaEsperaAngendamiento"
  style="display:none; position: absolute; z-index:5900; top:0px; left:250px;">
  <div class="transParencia" style="z-index:1001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div class="cnt listaEsperaAngendamientoGrilla"
    style="z-index:5902; position:absolute; top:15px; left:-340px; width:2000px">
    <table width="65%" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input type="button" align="right" value="CERRAR" onclick="limpiarListadosTotales('listListaEsperaTraer');
        ocultar('divVentanitaListaEspera')" /></td>
        <td align="right"><input type="button" align="right" value="CERRAR" onclick="limpiarListadosTotales('listListaEsperaTraer');
        ocultar('divVentanitaListaEspera')" /></td>
      </tr>
      <tr>
        <td>
          <table width="100%">
            <tr class="titulosListaEspera">
              <td colspan="2">PACIENTE</td>              
              <td colspan="2" style="max-width: 100px;">TIPO DE CITA</td>
            </tr>
            <tr class="titulosListaEspera">
              <td colspan="2">
                <input type="text" id="txtIdBusPacienteLE"
                oninput="llenarElementosAutoCompletarKey(this.id, 217, this.value)" style="width:80%"
                onblur="buscarAGENDA('listListaEsperaTraer') ">
              </td>              
              <td colspan="2" style="max-width: 100px;">
                <select id="cmbTipoCitaLE" onfocus="cargarTipoCitaSegunEspecialidad(this.id)" style="width:80%" onchange="buscarAGENDA('listListaEsperaTraer') ">
                  <option value="">[ TODO ]</option>
                </select>
              </td>
            </tr>
            <tr class="titulosListaEspera">
              <td>DÍAS ESPERA</td>
              <td>ESTADO LE</td>
              <td>CON CITA </td>
              <td>GRADO NECESIDAD</td>
            </tr>
            <tr class="titulosListaEspera">
              <td>
                <select id="cmbEsperar" size="1" style="width:90%"  onchange="buscarAGENDA('listListaEsperaTraer') ">
                  <option value=""></option>
                  <option value="0">0 Dias</option> 
                  <option value="30">30 dias (1 mes)</option>                                        
                  <option value="60">60 dias (2 meses)</option> 
                  <option value="90">90 dias (3 meses)</option>                                        
                  <option value="120">120 dias(4 meses)</option>
                  <option value="150">150 dias(5 meses)</option>
                  <option value="180">180 dias(6 meses)</option>
                  <option value="210">210 dias(7 meses)</option>
                  <option value="240">240 dias(8 meses)</option>
                  <option value="270">270 dias(9 meses)</option>
                  <option value="300">300 dias(10 meses)</option>
                  <option value="330">330 dias(11 meses)</option>
                  <option value="365">365 dias(12 meses)</option>                                                          
                </select>
              </td>
              <td>
                <select id="cmbEstadoLE" style="width:80%" onchange="buscarAGENDA('listListaEsperaTraer') " >	                                        
                  <%  resultaux.clear();
                      resultaux=(ArrayList)beanAdmin.combo.cargar(114);	
                      ComboVO cmbELE1; 
                      for(int k=0;k<resultaux.size();k++){ 
                         cmbELE1=(ComboVO)resultaux.get(k);
                  %>
                  <option value="<%= cmbELE1.getId()%>" title="<%= cmbELE1.getId()%>"><%= cmbELE1.getDescripcion()%></option>
                 <%}%>						
             </select> 

              </td>
              <td>
                <select id="cmbBusSinCita" style="width:80%"  onchange="buscarAGENDA('listListaEsperaTraer') ">
                  <option value="NO">NO</option>
                  <option value="SI">SI</option>
                </select>
              </td>
              <td>
                <select id="cmbNecesidad" style="width:80%" onchange="buscarAGENDA('listListaEsperaTraer') ">
                  <option value="">[ TODO ]</option>
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(113);
                    ComboVO cmbG;
                    for (int k=0; k < resultaux.size(); k++) {
                      cmbG=(ComboVO) resultaux.get(k); 
                  %>
                    <option value="<%= cmbG.getId()%>" title="<%= cmbG.getTitle()%>">
                      <%=cmbG.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
              
            </tr>
            <tr class="titulosListaEspera">
              <td>ADMINISTRADORA</td>
              <td>PREFERENCIAL</td>
              <td>RANGO DE EDAD</td>
              <td>MOTIVO DE ESPERA</td>
            </tr>
            <tr class="titulosListaEspera">
              <td>
                <input type="text" id="txtAdministradoraPacienteListEspera"
                onblur="buscarAGENDA('listListaEsperaTraer') "
                onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraPacienteListEspera', 308)" 
                style="width:80%"  />
              <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaAdPaciente"
                onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtAdministradoraPacienteListEspera', '21')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
              </td>
              <td>
                <select id="cmbPreferencialLE" style="width:80%" onchange="buscarAGENDA('listListaEsperaTraer') ">
                  <option value="">[ TODO ]</option>
                  <option value="S">SI</option>
                  <option value="N">NO</option>
                </select>
              </td>
              <td>
                <select id="cmbRangosEdad" style="width:80%" onchange="buscarAGENDA('listListaEsperaTraer') ">
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(5003); ComboVO cmbEdad; for
                    (int k=0; k < resultaux.size(); k++) { cmbEdad=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbEdad.getId()%>" title="<%= cmbEdad.getTitle()%>">
                      <%= cmbEdad.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
              <td>

                <select id="cmbMotivo" style="width:95%" onchange="buscarAGENDA('listListaEsperaTraer') ">
                  <option value="">[ SELECCIONE ]</option>
                  <%     resultaux.clear();
                      resultaux = (ArrayList) beanAdmin.combo.cargar(222);
                      ComboVO cmbG0;
                      for (int k = 0; k < resultaux.size(); k++) {
                          cmbG0 = (ComboVO) resultaux.get(k);
                  %>
                  <option value="<%= cmbG0.getId()%>" title="<%= cmbG0.getTitle()%>"><%=cmbG0.getDescripcion()%></option>
                  <%}%>  
              </select> 
                
              </td>
              
            </tr>
  
          </table>

        </td>

        <td>
          <table width="100%">
            <tr class="titulosListaEspera">
              <td>Estado espera</td>
            </tr>
            <tr class=estiloImputListaEspera>
              <td align="LEFT">
                <dl>
                  <dt><input type="checkbox" id="chkEstadoLE" name="chkEstadoLE">TODO</dt>
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(326); ComboVO cmbList; for
                    (int k=0; k < resultaux.size(); k++) { cmbList=(ComboVO) resultaux.get(k); %>
                    <dt><input type="checkbox" id="chkEstadoLE_<%= cmbList.getId()%>"
                        onclick="buscarAGENDA('listListaEsperaTraer') " name="chkEstadoLE_<%= cmbList.getId()%>" value="<%= cmbList.getId()%>">
                      <%= cmbList.getDescripcion()%>
                    </dt>
                    <%}%>
                </dl>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td colspan=2>
          <div id="divContenidoLETraer" style="width:100%">
            <table align="center" id="listListaEsperaTraer"></table>
          </div>
        </td>
      </tr>
      
    <!-- 
      <tr>
        <td colspan=2>
          <table width="100%">
            <tr class="titulosListaEspera">
              <td valign="baseline" width="60%">
                <table width="100%">
                  <tr class="titulosListaEspera">
                    <td colspan="3">Paciente</td>
                  </tr>
                  <tr class="estiloImputListaEspera">
                    <td colspan="3">
                      
                    </td>
                  </tr>
                  <tr class="titulosListaEspera">
                    <td colspan="3">Tipo de cita</td>
                  </tr>
                  <tr class="estiloImputListaEspera">
                    <td colspan="3">
                      
                    </td>
                    <td colspan="3">
                      <select id="cmbTipoCitaLE" onfocus="cargarTipoCitaSegunEspecialidad(this.id)" style="width:80%">
                        <option value="">[ TODO ]</option>
                      </select>
                    </td>
                  </tr>
                  <tr class="titulosListaEspera">
                    <td>Motivo</td>
                    <td>Grado de necesidad</td>
                    <td>Preferencial</td>
                    <td>Rango edad</td>
                  </tr>
                  <tr class="estiloImputListaEspera">
                    <td>
                      
                    </td>
                    <td>
                      
                    </td>
                    <td>
                      
                    </td>
                    <td>
                      
                    </td>
                  </tr>
                </table>
              </td>
              
            </tr>
            -->
            <!--
            <tr class="estiloImput" >
              <td>
                <input type="text" id="txtIdBusPacienteLE" oninput="llenarElementosAutoCompletarKey(this.id, 205, this.value)"
                  style="width:80%" />
              </td>
              <td>
                <select id="cmbDiasEsperaLE" style="width:98%">
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(115); ComboVO cmbDias; for (int k=0; k <
                    resultaux.size(); k++) { cmbDias=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbDias.getId()%>" title="<%= cmbDias.getTitle()%>">
                      <%= cmbDias.getDescripcion()%>
                    </option>
                    <%}%>
              </td>
              <td>
                <select id="cmbEstadoLE" style="width:80%" >
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(114); ComboVO cmbELE; for (int k=0; k <
                    resultaux.size(); k++) { cmbELE=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbELE.getId()%>" title="<%= cmbELE.getId()%>">
                      <%= cmbELE.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
              <td>
                <select id="cmbBusSinCita" style="width:80%" >
                  <option value="NO">NO</option>
                  <option value="SI">SI</option>
                </select>
              </td>
            </tr> 
          -->
          <!--
          </table>
        </td>
      </tr> 
    -->
      
    </table>
  </div>
</div>


<div id="divVentanitaEditarListaEspera" style="display:none; z-index:2000; top:1px; left:-211px;">
  <div class="transParencia" style="z-index:2001; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div style="z-index:2002; position:absolute; top:180px; left:70px; width:95%">
    <table width="95%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" title="divVentanitaEditarListaEspera" type="button" align="right"
            value="CERRAR" onclick="ocultar('divVentanitaEditarListaEspera')" /></td>
        <td>&nbsp;Id Lista Espera= <label id="lblListaEsperaModificar"></label></td>
        <td>&nbsp;</td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaEditarListaEspera')" /></td>
      <tr>
      <tr class="estiloImput">
        <td></td>
        <td>Tel&eacute;fono=&nbsp;<label id="lblTelefono"></label></td>
        <td>PACIENTE=&nbsp;<label id="lblNombrePaciente"></label>&nbsp&nbsp Edad:<label id="lblEdad"></label></td>
        <td>Dias Esperando=&nbsp;<label id="lblDias"></label></td>
      <tr>
      <tr class="titulosListaEspera">
        <td colspan="2">*Administradora</td>
        <td colspan="2">Observaciones</td>
      <tr>
      <tr class="estiloImputListaEspera">
        <td colspan="2"><input type="text" id="txtAdministradora1LE" size="60" maxlength="60"
            onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradora1LE', 308)" style="width:98%"
             />
        </td>
        <td colspan="2"><input type="text" id="txtObservacionLE" size="100" maxlength="2000" style="width:90%"
            onKeyPress="javascript:return teclearExcluirCaracter(event, '%');"
            onkeyup="javascript: this.value = this.value.toUpperCase();"  /></td>
      <tr>
      <tr class="titulosListaEspera">
        <td colspan="4">
          <table width="100%">
            <tr>
              <td width="15%">Especialidad</td>
              <!--<td width="15%">Sub Especialidad</td>        -->
              <td width="15%">Discapacidad F&iacute;sica</td>
              <td width="10%">Embarazo</td>
              <td width="12%">Tipo Cita</td>
              <td width="15%">Grado Necesidad</td>
              <td width="10%">Estado</td>
            </tr>
            <tr class="estiloImputListaEspera">
              <td width="15%">
                <select id="cmbIdEspecialidadLE" style="width:99%"
                  onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')" 
                  onchange="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
                  <option value=""></option>
                  <!--<%     resultaux.clear();
                                                                            resultaux = (ArrayList) beanAdmin.combo.cargar(100);
                                                                            ComboVO cmb32_;
                                                                            for (int k = 0; k < resultaux.size(); k++) {
                                                                                cmb32_ = (ComboVO) resultaux.get(k);
                                                                        %>
                                                                      <option value="<%= cmb32_.getId()%>" title="<%= cmb32_.getTitle()%>">
                                                                        <%= cmb32_.getDescripcion()%></option>
                                                                        <%}%>   -->
                </select>
              </td>
              <!-- <td>
                                                                     <select id="cmbIdSubEspecialidadLE" style="width:90%"   >
                                                                         <option value=""></option>
                                                                <%     resultaux.clear();
                                                                    resultaux = (ArrayList) beanAdmin.combo.cargar(117);
                                                                    ComboVO cmbSLE;
                                                                    for (int k = 0; k < resultaux.size(); k++) {
                                                                        cmbSLE = (ComboVO) resultaux.get(k);
                                                                %>
                                                              <option value="<%= cmbSLE.getId()%>" title="<%= cmbSLE.getTitle()%>">
                                                                <%= cmbSLE.getDescripcion()%></option>
                                                                <%}%>                                                                 
                                                              </select>                    
                                                          </td>       -->
              <td>
                <select id="cmbDiscapaciFisicaLE" style="width:40%" >
                  <option value="N">No</option>
                  <option value="S">Si</option>
                </select>
              </td>
              <td>
                <select id="cmbEmbarazoLE" style="width:70%" >
                  <option value="N">No</option>
                  <option value="S">Si</option>
                </select>
              </td>
              <td>
                <select id="cmbTipoCitaLEM" style="width:98%" >
                  <option value="C">Control</option>
                  <option value="P">Primera Vez</option>
                </select>
              </td>
              <td>
                <select id="cmbNecesidadLE" style="width:80%" >
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(113); ComboVO cmbG_; for (int k=0;
                    k < resultaux.size(); k++) { cmbG_=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbG_.getId()%>" title="<%= cmbG_.getTitle()%>">
                      <%=cmbG_.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
              <td>
                <select id="cmbEstadoLEM" style="width:95%" >
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(112); ComboVO cmbELe; for (int k=0;
                    k < resultaux.size(); k++) { cmbELe=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbELe.getId()%>" title="<%= cmbELe.getTitle()%>">
                      <%=cmbELe.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="estiloImputListaEspera">
        <td colspan="4">
          <input name="btn_crear_cita" type="button" class="small button blue" value="MODIFICAR"
            onclick="modificarCRUD('modificarListaEspera', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"
             />
        </td>
      </tr>
    </table>
  </div>
</div>

<div id="divVentanitaPacienteYaExiste" style="display:none; z-index:2050; top:1px; left:0px;">
  <div class="transParencia" style="z-index:2051; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div style="z-index:2052; position:absolute; top:100px; left:-55px; width:100%">
    <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" title="divVentanitaPacienteYaExiste" type="button" align="right"
            value="CERRAR" onclick="ocultar('divVentanitaPacienteYaExiste')" /></td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaPacienteYaExiste')" /></td>
      <tr>
      <tr class="estiloImputListaEspera">
        <td colspan="2"><strong><label id="lblIdPacienteExiste"></label></strong></td>
        <!--<td align="left"><label style="animation-delay; color:#F00" id="lblLetrero"></label></td>-->
      <tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr class="titulosListaEspera">
              <td width="15%">ESPECIALIDAD</td>
              <td width="15%">TIPO CITA</td>
              <td width="5%">EXITO</td>
              <td width="10%">FECHA DESDE</td>
              <td width="10%">FECHA HASTA</td>
              <td width="5%"></td>
              <td width="5%"></td>
            </tr>
            <tr class="estiloImputListaEspera">
              <td>
                <select id="cmbIdEspecialidadCitasPaciente" style="width: 95%;"
                  onchange="limpiaAtributo('cmbIdTipoCitasPaciente')">
                  <%-- <option value="">[ TODO ]</option> --%>
                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(123); ComboVO cmb32; for (int
                      k=0; k < resultaux.size(); k++) { cmb32=(ComboVO) resultaux.get(k); %>
                      <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>">
                        <%= cmb32.getDescripcion()%>
                      </option>
                      <%}%>
                </select>
              </td>
              <td>
                <select id="cmbIdTipoCitasPaciente" style="width: 95%;"
                  onfocus="cargarComboGRALCondicion1('', '', this.id, 551, valorAtributo('cmbIdEspecialidadCitasPaciente'));">
                  <option value=""></option>
                </select>
              </td>
              <td>
                <select id="cmbExitoCitasPaciente" style="width: 95%;">
                  <option value="S" selected>SI</option>
                  <option value="N">NO</option>
                </select>
              </td>
              <td>
                <input type="date" value="<%=LocalDate.now()%>" id="txtFechaInicioCitasPaciente">
              </td>
              <td>
                <input type="date" id="txtFechaFinCitasPaciente">
              </td>
              <td>
                <input name="btn_crear_cita" type="button" class="small button blue" value="BUSCAR" title="b8k67"
                  onclick="buscarAGENDA('listaCitasPaciente');" />
              </td>
              <td>
                <input name="btn_crear_cita" type="button" class="small button blue" value="IMPRIMIR" title="b8k67"
                  onclick="imprimirAgendaPaciente()"  />
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <div style="overflow:auto;height:250px; width:100%" id="divContenidoListaCitasPaciente">
            <table id="listaCitasPaciente"></table>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr class="titulosListaEspera">
              <td>ESTADO</td>
              <td>VIA</td>
              <td>CLACIFICACION</td>
              <td>OBSERVACIONES</td>
              <td></td>
            </tr>
            <tr class="estiloImputListaEspera">
              <td>
                <select id="cmbEstadoCitaPacienteExiste" style="width:90%"
                  onchange="limpiaAtributo('cmbMotivoConsultaPacienteExiste');
                                                                            limpiaAtributo('cmbMotivoConsultaClasePacienteExiste')">
                  <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(107); for (int k=0; k <
                    resultaux.size(); k++) { cmbEst=(ComboVO) resultaux.get(k); %>
                    <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                      <%= cmbEst.getId() + "  " + cmbEst.getDescripcion()%>
                    </option>
                    <%}%>
                </select>
              </td>
              <td>
                <select id="cmbMotivoConsultaPacienteExiste" style="width:90%"
                  onfocus="comboDependiente('cmbMotivoConsultaPacienteExiste', 'cmbEstadoCitaPacienteExiste', '96');">
                  <option value="">[ SELECCONE ]</option>
                </select>
              </td>
              <td>
                <select id="cmbMotivoConsultaClasePacienteExiste" style="width:90%"
                  onfocus="cargarComboGRALCondicion2('', '', this.id, 563, valorAtributo('cmbMotivoConsultaPacienteExiste'), valorAtributo('cmbIdEspecialidadCitasPaciente'))">
                  <option value="">[ SELECCONE ]</option>
                </select>
              </td>
              <td>
                <textarea id="txtObservacionesCambioEstadoPacienteExiste" rows="3" style="width:90%"
                  placeholder="Observaciones"></textarea>
              </td>
              <td>
                <input type="button" class="small button blue" value="MODIFICAR"
                  onclick="modificarCRUD('modificarCitasBloqueAgenda')" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="titulos">
        <td colspan="2">
          <div style="overflow:auto;width:100%" id="divContenidoListaEsperaHist">
            <table id="listHistoricoListaEsperaEnAgenda" class="scroll"></table>
          </div>
        </td>
      </tr>
      <tr class="titulosListaEspera">
        <td width="50%">
          <input style="margin: 8px;" name="btn_crear_cita" type="button" class="small button blue" value="TRAER"
            onclick="asignaAtributo('txtIdBusPaciente', valorAtributo('lblIdPacienteExiste'), 0);
                                                                buscarInformacionBasicaPaciente();
                                                                ocultar('divVentanitaPacienteYaExiste')"
             />
        </td>
        <% if (beanSession.usuario.preguntarMenu("CITB03")) { %>
          <td width="50%">
            <input style="margin: 8px;" id="CITB03" name="btn_crear_cita" type="button" class="small button blue"
              value="VER HISTORIA CLINICA" title="CIT06"
              onclick="buscarHC('listDocumentosHistoricosTodos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"
               />
          </td>
          <%}%>
      </tr>
      <tr class="titulos">
        <td colspan="2">
          <div id="divdatosbasicoscredesa" style="height:130px; width:100%; overflow-y: scroll;">
            <table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0"
              align="center">
              <tr>
                <th></th>
              </tr>
            </table>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="1080" align="center" border="0" cellspacing="0" cellpadding="1">
            <tr class="titulosListaEspera">
              <div id="divAcordionAdjuntos" style="display:BLOCK">
                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                  <jsp:param name="titulo" value="Archivos adjuntos" />
                  <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                  <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />
                  <jsp:param name="display" value="NONE" />
                  <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
                </jsp:include>
              </div>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>

<!-------------------------para la vista previa---------------------------- -->
<div id="divVentanitaPdf" style="display:none; z-index:2050; top:1px; left:190px;">
  <div id="idDivTransparencia" class="transParencia"
    style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div id="idDivTransparencia2" style="z-index:2055; position:absolute; top:5px; left:15px; width:100%">
    <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" title="divVentanitaPdf" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaPdf')" /></td>
        <td>&nbsp;</td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaPdf')" /></td>
      </tr>
      <tr class="estiloImput">
        <td colspan="3">
          <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td>
                <div id="divParaPaginaVistaPrevia"></div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>

<div id="divVentanitaNotificaciones" style="display:none; z-index:7000; top:1px; width:900px; left:150px;">
  <div class="transParencia" style="z-index:7001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:7002; position:fixed; top:200px; left:180px; width:70%">
    <table width="90%" height="110" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left" width="10%">
          <input name="btn_cerrar" title="divVentanitaNotificaciones" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaNotificaciones')" />
        </td>
        <td align="right" width="10%">
          <input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onclick="ocultar('divVentanitaNotificaciones')" />
        </td>
      </tr>
      <tr class="titulos">
        <td colspan="2">
          <div id="idDivAgendaNotifica" style="width:100%">
            <table id="listAgendaNotifica" class="scroll"></table>
          </div>
        </td>
      </tr>

    </table>
  </div>
</div>

<div id="divVentanitaAgendaOrdenes"
  style="position:absolute; display:none; top:20px; left: 50%;  width:63%; height:90px; z-index:100; margin-left: -30%;">
  <table width="90%" class="fondoTabla">
    <tr class="estiloImput">
      <td align="left"><input name="btn_cerrar" title="divVentanitaAgenda" type="button" align="right" value="CERRAR"
          onclick="ocultarAgendaOrdenes()" /></td>
      <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
          onclick="ocultarAgendaOrdenes()" /></td>
    <tr>
    <tr hidden>
      <td>
        <input type="checkbox" id="copiarCitas" hidden>
      </td>
    </tr>
    <tr class="camposRepInp">
      <td width="100%" colspan="5">Informaci&oacute;n de la agenda</td>
    <tr>
    <tr>
      <td colspan="5">
        <table width="100%">
          <tr class="titulosListaEspera">
            <td align="CENTER">SEDE</td>
          </tr>
          <tr class="titulosListaEspera">
            <td align="CENTER"><label id="lblIdSedeAgendaOrdenes"></label>-<label id="lblSedeAgendaOrdenes"></label>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="5">
        <div id="divAcordionOrdenesAgenda" style="display:BLOCK">
          <jsp:include page="divsAcordeon.jsp" flush="FALSE">
            <jsp:param name="titulo" value="ORDENES ENVIADAS" />
            <jsp:param name="idDiv" value="divOrdenesAgendas" />
            <jsp:param name="pagina" value="ordenesAgenda.jsp" />
            <jsp:param name="display" value="NONE" />
            <jsp:param name="funciones" value="buscarAGENDA('procedimientosOrdenadosAgenda')" />
          </jsp:include>
        </div>
      </td>
    </tr>
    <tr class="camposRepInp">
      <td width="100%" colspan="5">Informaci&oacute;n del paciente</td>
    <tr>
    <tr class="titulosListaEspera">
      <td colspan="5">PACIENTE</td>
    <tr>
    <tr class="estiloImput">
      <td colspan="5">
        <input type="text" size="110" maxlength="210" id="txtIdBusPacienteOrdenes"
          oninput="llenarElementosAutoCompletarKey('txtIdBusPacienteOrdenes', 205, this.value)" style="width:50%"
           />
        <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaMuni"
          onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanitaOrdenes', 'txtIdBusPacienteOrdenes', '27')"
          src="/clinica/utilidades/imagenes/acciones/buscar.png">
        <input type="button" title="55GH_" class="small button blue" value="BUSCAR"
          onclick="buscarInformacionBasicaPacienteAgendaOrdenes()" />
      </td>
    </tr>
    <tr class="estiloImputListaEspera">
      <td colspan="5">
        <!-- DATOS BASICOS PACIENTE-->
        <table width="100%">
          <tr class="titulosListaEspera">
            <td width="15%">Tipo Id</td>
            <td width="10%">Identificacion</td>
            <td width="25%">Apellidos</td>
            <td width="25%">Nombres</td>
            <td width="10%">Fecha Nacimiento</td>
            <td width="5%">Edad</td>
            <td width="10%">Sexo</td>
          </tr>
          <tr>
            <td>
              <img src="/clinica/utilidades/imagenes/acciones/buscar.png" width="16px" height="16px" onclick="guardarYtraerDatoAlListado('buscarIdentificacionPaciente');
                                                                             limpTablas('listDocumentosHistoricos');
                                                                             ocultar('divArchivosAdjuntos')"
                align="center" />
              <select size="1" id="cmbTipoIdOrdenes" style="width:85%" >
                <option value=""></option>
                <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(105); for (int k=0; k <
                  resultaux.size(); k++) { cmb105=(ComboVO) resultaux.get(k); %>
                  <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                    <%= cmb105.getId() + "  " + cmb105.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <!--onkeypress="return valida(event)"  onkeypress="return validaEspacios();"  -->
            <td><input type="number" id="txtIdentificacionOrdenes" style="width:80%" size="20" maxlength="20"
                onKeyPress="javascript:checkKey2(event);
                                                                    javascript:return soloTelefono(event);"
                 />
            </td>
            <td>
              <input type="text" id="txtApellido1Ordenes" size="30" maxlength="30"
                onkeypress="return excluirEspeciales(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
              <input type="text" id="txtApellido2Ordenes" size="30" maxlength="30"
                onkeypress="return excluirEspeciales(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
            </td>
            <td>
              <input type="text" id="txtNombre1Ordenes" size="30" maxlength="30"
                onkeypress="javascript:return teclearsoloan(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
              <input type="text" id="txtNombre2Ordenes" size="30" maxlength="30"
                onkeypress="javascript:return teclearsoloan(event);"
                onkeyup="javascript: this.value = this.value.toUpperCase();"  style="width:45%" />
            </td>
            <td><input id="txtFechaNacOrdenes" type="text" size="10" maxlength="10"  style="width:95%" />
            </td>
            <td><!-- Edad PROG ORDENES -->
              <label id="txtEdad" class="bloque w15" disabled="false"></label>
              <!---------->
            </td>

            <td> <select size="1" id="cmbSexoOrdenes" style="width:95%" >
                <option value=""></option>
                <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(110); for (int k=0; k <
                  resultaux.size(); k++) { cmbS=(ComboVO) resultaux.get(k); %>
                  <option value="<%= cmbS.getId()%>" title="<%= cmbS.getTitle()%>">
                    <%=cmbS.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
          </tr>
        </table>
        <!-- FIN DATOS BASICOS -->
      </td>
    </tr>
    <tr>
      <td colspan="5">
        <table>
          <tr class="titulosListaEspera">
            <td width="20%">Municipio</td>
            <td width="30%">Direcci&oacute;n</td>
            <td width="20%">Acompa&ntilde;ante</td>
            <td width="10%">Tel&eacute;fono</td>
            <td width="10%">Celular 1</td>
            <td width="10%">Celular 2</td>
          </tr>
          <tr class="estiloImput">
            <td><input type="text" id="txtMunicipioOrdenes" size="60" maxlength="60"
                oninput="llenarElementosAutoCompletarKey('txtMunicipioOrdenes', 310, this.id)" style="width:80%"
                 />
              <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni"
                onclick="traerVentanita(this.id, '', 'divParaVentanitaOrdenes', 'txtMunicipioOrdenes', '1')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
              <div id="divParaVentanitaOrdenes"></div>
            </td>
            <td><input type="text" id="txtDireccionResOrdenes" size="100" maxlength="100" style="width:99%"
                onkeyup="javascript: this.value = this.value.toUpperCase();"
                onkeypress="javascript:return teclearsoloan(event);"  /></td>
            <td><input type="text" id="txtNomAcompananteOrdenes" size="100" maxlength="100" style="width:95%"
                onkeyup="javascript: this.value = this.value.toUpperCase();"
                onkeypress="javascript:return teclearExcluirCaracter(event);"  /> </td>
            <td>
              <input type="text" id="txtTelefonosOrdenes" size="100" style="width:90%" minlength="7" maxlength="7"
                onkeypress="javascript:return soloTelefono(event);"  />
            </td>
            <td>
              <input type="text" id="txtCelular1Ordenes" size="30" style="width:90%" minlength="10" maxlength="10"
                onkeypress="javascript:return soloTelefono(event);"  />
            </td>
            <td>
              <input type="text" id="txtCelular2Ordenes" size="30" style="width:90%" minlength="10" maxlength="10"
                onkeypress="javascript:return soloTelefono(event);"  />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="5">
        <table width="100%">
          <tr class="titulosListaEspera">
            <td width="30%">Email</td>
            <td width="30%">Administradora Paciente</td>
            <td width="20%">Tipo de regimen</td>
            <td></td>
          </tr>
          <tr class="estiloImputListaEspera">
            <td><input type="text" id="txtEmailOrdenes" maxlength="60" style="width:99%"
                onkeypress="javascript:return email(event);" /> </td>
            <td>
              <input type="text" id="txtAdministradoraPacienteOrdenes" maxlength="60"
                oninput="llenarElementosAutoCompletarKey('txtAdministradoraPacienteOrdenes', 1013, this.id)"
                style="width:90%" />
              <img width="18px" height="16px" align="middle" id="idLupitaVentanitaAdPaciente"
                onclick="traerVentanita(this.id, '', 'divParaVentanitaOrdenes', 'txtAdministradoraPacienteOrdenes', '21')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
            </td>
            <td>
              <select id="cmbTipoRegimenOrdenes" style="width:90%">
                <option value=""></option>
                <%resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(518); ComboVO cmbPro; for (int k=0; k
                  < resultaux.size(); k++) { cmbPro=(ComboVO) resultaux.get(k);%>
                  <option value="<%= cmbPro.getId()%>" title="<%= cmbPro.getTitle()%>">
                    <%=cmbPro.getDescripcion()%>
                  </option>
                  <%}%>
              </select>
            </td>
            <td>
              <input type="button" class="small button blue" title="FUNCIONA45" value="Crear nuevo paciente"
                onclick="modificarCRUD('crearNuevoPacienteT')" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="5" align="CENTER">
        <table width="100%" style="background-color: #E8E8E8;">
          <tr>
            <td height="200px">
              <div id="tabsCoordinacionTerapia" style="width:99%; height:auto">
                <ul>
                  <li><a href="#divOrdenesPendientes" onclick="tabActivoOrdenesAgenda('divOrdenesPendientes')">
                      <center>ORDENES PENDIENTES</center>
                    </a></li>
                  <li><a href="#divOrdenesHistoricas" onclick="tabActivoOrdenesAgenda('divOrdenesHistoricas')">
                      <center>ORDENES GESTIONADAS</center>
                    </a></li>
                  <li><a href="#divOrdenesExternas" onclick="tabActivoOrdenesAgenda('divOrdenesExternas')">
                      <center>ORDEN EXTERNA</center>
                    </a></li>
                </ul>
                <div id="divOrdenesPendientes" class="titulos">
                  <table width="40%">
                    <tr class="titulosListaEspera">
                      <td style="width: 80%;">TIPO</td>
                    </tr>
                    <tr class="estiloImputListaEspera">
                      <td>
                        <select id="cmbTipoOrden" style="width: 90%;">
                          <option value="">[ TODO ]</option>
                          <option value="0" selected>Ordenes externas</option>
                          <option value="1">Historia clinica</option>
                        </select>
                      </td>
                      <td>
                        <input type="button" class="small button blue" value="BUSCAR"
                          onclick="buscarAGENDA('listCoordinacionTerapiaOrdenadosAgenda')" />
                      </td>
                    </tr>
                  </table>
                  <table id="listCoordinacionTerapiaOrdenadosAgenda"></table>
                </div>
                <div id="divOrdenesHistoricas" class="titulos">
                  <table width="40%">
                    <tr class="titulosListaEspera">
                      <td style="width: 80%;">TIPO</td>
                    </tr>
                    <tr class="estiloImputListaEspera">
                      <td>
                        <select id="cmbTipoOrdenHistoricos" style="width: 90%;">
                          <option value="">[ TODO ]</option>
                          <option value="0" selected>Ordenes externas</option>
                          <option value="1">Historia clinica</option>
                        </select>
                      </td>
                      <td>
                        <input type="button" class="small button blue" value="BUSCAR"
                          onclick="buscarAGENDA('listCoordinacionTerapiaOrdenadosAgenda')" />
                      </td>
                    </tr>
                  </table>
                  <table id="listaOrdenesHistoricas"></table>
                </div>
                <div id="divOrdenesExternas">
                  <table width="100%" style="background-color: #E8E8E8;">
                    <tr class="titulosListaEspera">
                      <td width="30%">PROCEDIMIENTO</td>
                      <td width="5%">CANTIDAD</td>
                      <td width="10%">GRADO NECESIDAD</td>
                      <td width="20%">INDICACI&Oacute;N</td>
                      <td width="30%">DIAGNOSTICO ASOCIADO</td>
                      <td width="5%"></td>
                    </tr>
                    <tr class="estiloImput">
                      <td>
                        <input id="txtIdProcedimientoOrdenExt"
                          oninput="llenarElementosAutoCompletarKey(this.id, 204, this.value)" style="width:95%">
                      </td>
                      <td>
                        <input type="number" id="txtCantidadOrdenExt" style="width:70%">
                      </td>
                      <td>
                        <select id="cmbNecesidadOrdenExt" style="width:90%">
                          <option value=""></option>
                          <%resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(113); for (int k=0; k <
                            resultaux.size(); k++) { cmbPro=(ComboVO) resultaux.get(k);%>
                            <option value="<%= cmbPro.getId()%>" title="<%= cmbPro.getTitle()%>">
                              <%=cmbPro.getDescripcion()%>
                            </option>
                            <%}%>
                        </select>
                      </td>
                      <td class="titulos">
                        <textarea id="txtIndicacionOrdenExt" rows="5" style="width:95%"
                          onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea>
                      </td>
                      <td>
                        <input id="txtDxRelacionadoOrdenExt"
                          oninput="llenarElementosAutoCompletarKey(this.id, 206, this.value)" style="width:95%">
                      </td>
                      <td>
                        <input id="btnProcedimiento" type="button" class="small button blue" value="ADICIONAR"
                          onclick="verificarNotificacionAntesDeGuardar('verificarProcedimientoContratadoSede')" />
                      </td>
                    </tr>
                    <tr class="titulos">
                      <td colspan="6">
                        <table width="100%" style="background-color: #E8E8E8;">
                          <tr class="titulosListaEspera">
                            <td>
                              <label for="ckLaboratoriosNefro">Orden Externa Laboratorios de Ingreso:</label>
                              <input type="checkbox" id="ckLaboratoriosNefro"
                                onchange="activarModoLaboratoriosNefroproteccion();" />
                            </td>
                            <td style="display: none;" id="laboratoriosNefro">Tipo R&eacute;gimen - Plan<a
                                style="color:#FF0000;">(*)</a></td>
                            <td style="display: none;" id="laboratoriosNefro">Plan de Contrataci&oacute;n<a
                                style="color:#FF0000;">(*)</a></td>
                          </tr>
                          <tr class="estiloImput">
                            <td>&nbsp;
                            </td>
                            <td style="display: none;" id="laboratoriosNefro">
                              <select id="cmbIdTipoRegimenLaboratoriosNefroproteccion" style="width: 95%;"
                                onclick="limpiarDivEditarJuan('limpiarDatosPlanLaboratoriosNefroproteccion')"
                                onfocus="cargarComboGRALCondicion3('', '', this.id, 223, valorAtributoIdAutoCompletar('txtAdministradoraPacienteOrdenes'), valorAtributo('cmbSede'), valorAtributoIdAutoCompletar('txtIdBusPacienteOrdenes'))"
                                >
                                <option value="">[ SELECCIONE ]</option>
                              </select>
                            </td>
                            <td style="display: none;" id="laboratoriosNefro">
                              <label id="lblIdPlanContratacionLaboratoriosNefroproteccion" hidden></label><label
                                id="lblNomPlanContratacionLaboratoriosNefroproteccion" hidden></label>
                              <select id="cmbIdPlanlaboratoriosNefroproteccion" style="width: 95%;"
                                onfocus="limpiarDivEditarJuan('limpiarDatosPlanLaboratoriosNefroproteccion')
                                                                                                                traerPlanesDeContratacionAgenda(this.id, '165', 'txtAdministradoraPacienteOrdenes', 'cmbIdTipoRegimenLaboratoriosNefroproteccion')"
                                onchange="asignaAtributo('lblIdPlanContratacionLaboratoriosNefroproteccion', valorAtributo(this.id), 0)"
                                >
                                <option value=""></option>
                              </select>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr class="titulos">
                      <td colspan="6">
                        <table id="listaProcedimientosOrdenExterna"></table>
                        <div id="pager1"></div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="6" align="CENTER">
                        <input type="button" class="small button blue" value="FINALIZAR"
                          onclick="modificarCRUD('guardarOrdenesExternas')" />
                        <input type="button" class="small button blue" value="CARGAR ADJUNTOS"
                          onclick="acordionArchivosAdjuntosOrdenExterna();" />
                        <input id="laboratoriosNefro" style="display: none;" type="button" class="small button blue"
                          value="IMPRIMIR ORDEN EXTERNA" onclick="ImprimirOrdenExterna()" />
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </td>
          </tr>
        </table>
        <table width="100%">
          <tr class="camposRepInp">
            <td width="100%" colspan="5">Informaci&oacute;n de cita(s)</td>
          <tr>
          <tr class="titulosListaEspera">
            <td width="30%">ESPECIALIDAD</td>
            <td width="40%">PROFESIONAL</td>
            <td width="30%">TIPO CITA</td>
          </tr>
          <tr class="estiloImput">
            <td><select id="cmbIdEspecialidadOrdenes"
                onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidadOrdenes')" style="width:90%" onchange="buscarAGENDA('listDiasOrdenes', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');
                                                                                limpiaAtributo('cmbIdProfesionalesOrdenes');
                                                                                limpiaAtributo('cmbTipoCitaTerapia')
                                                                        ">
                <option value=""></option>
              </select>
            </td>
            <!--<td>
                                                                <select size="1" id="cmbIdSubEspecialidad"  onfocus="cargarSubEspecialidadDesdeEspecialidad('cmbIdEspecialidad', 'cmbIdSubEspecialidad')" onchange="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  style="width:70%"    >                                        
                                                                    <option value=""></option>
                                                                </select>                          
                                                            </td> -->
            <td><select size="1" id="cmbIdProfesionalesOrdenes" style="width:90%" onfocus="cargarComboGRALCondicion2('',
                                                                                        '',
                                                                                        this.id,
                                                                                        197,
                                                                                        valorAtributo('cmbIdEspecialidadOrdenes'),
                                                                                        valorAtributo('cmbSede'));"
                onchange="buscarAGENDA('listDiasOrdenes')">
                <option value=""></option>
              </select>
            </td>
            <td>
              <select id="cmbTipoCitaTerapia" onfocus="cargarTipoCitaProgramacion(this.id, 'cmbIdEspecialidadOrdenes')"
                style="width:90%" onchange="mostrarOpcionesProgramacion()">
                <option value="">[ SELECCIONE ]</option>
              </select>
            </td>
          </tr>
        </table>
        <table width="100%">
          <tr class="titulosListaEspera">
            <td width="35%">Administradora Agenda</td>
            <td width="20%">Tipo R&eacute;gimen - Plan<a style="color:#FF0000;">(*)</a></td>
            <td width="45%">Plan de Contrataci&oacute;n<a style="color:#FF0000;">(*)</a></td>
          </tr>
          <tr class="estiloImputListaEspera">
            <td>
              <input type="text" id="txtAdministradora1Ordenes"
                onchange="limpiaAtributo('lblIdPlanContratacionOrdenes', 0);
                                                                               limpiaAtributo('cmbIdPlanOrdenes', 0);
                                                                               limpiaAtributo('cmbIdTipoRegimenOrdenes', 0);"
                onclick="llamarAdministradorasProgramacionOrden()" style="width: 85%;" />
              <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaAdminRem"
                onclick="traerVentanitaBuscarHC(this.id, 'divParaVentanitaOrdenes', 'txtAdministradora1Ordenes', 'listGrillaAdministradorasOrdenes');
                                                                             limpiarDivEditarJuan('limpiarDatosAdministradoraFactura')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
            </td>
            <td>
              <select id="cmbIdTipoRegimenOrdenes" style="width: 95%;" onchange="limpiaAtributo('lblIdPlanContratacionOrdenes', 0);
                                                                                limpiaAtributo('cmbIdPlanOrdenes', 0);"
                onfocus=cargarComboRegimenOrdenes() >
                <option value="">[ SELECCIONE ]</option>
              </select>
            </td>
            <td>
              <label id="lblIdPlanContratacionOrdenes" hidden></label><label id="lblNomPlanContratacion" hidden></label>
              <select id="cmbIdPlanOrdenes" style="width: 70%;" onfocus=traerPlanesDeContratacionAgendaOrdenes()
                onchange="asignaAtributo('lblIdPlanContratacion', valorAtributo(this.id), 0);">
                <option value="">[ SELECCIONE ]</option>
              </select>
            </td>
          </tr>
        </table>
        <table width="100%">
          <tr class="titulosListaEspera">
            <td width="15%">N&uacute;mero autorizaci&oacute;n</td>
            <td width="15%">Fecha vigencia autorizaci&oacute;n</td>
            <td width="35%">Instituci&oacute;n que Remite</td>
          </tr>
          <tr class="estiloImput">
            <td>
              <input type="text" id="txtNoAutorizacionOrdenes" size="30" maxlength="50" style="width:90%"
                onKeyPress="javascript:return teclearExcluirCaracter(event, '%');"  />
            </td>
            <td>
              <input id="txtFechaVigenciaOrdenes" type="date" size="10" maxlength="10" style="width:80%" 
                onkeydown="return false" />
            </td>
      </td>
      <td><input type="text" id="txtIPSRemiteOrdenes" size="60" maxlength="60"
          oninput="llenarElementosAutoCompletarKey('txtIPSRemiteOrdenes', 203, this.value)" style="width:90%"
           />
        <img width="18px" height="18px" align="middle" title="VEN2" id="idLupitaVentanitaMuni"
          onclick="traerVentanita(this.id, '', 'divParaVentanitaOrdenes', 'txtIPSRemiteOrdenes', '4')"
          src="/clinica/utilidades/imagenes/acciones/buscar.png">
      </td>
    </tr>
  </table>
  <table width="100%" id="opcionesProgramacion" hidden>
    <tr class="titulosListaEspera">
      <td width="10%" id="elementoOrdenes">HORA DE CITA(S)</td>
      <td width="10%" id="elementoOrdenes">DURACION</td>
      <td width="10%" id="elementoOrdenes">CONSULTORIO</td>
      <td width="10%">CANTIDAD AL DIA</td>
      <td width="10%">FRECUENCIA <input type="checkbox" id="chkFrecuencia" onchange="reaccionAEvento(this.id)"></td>
      <td width="10%" id="tdFrecuencia" hidden>REPETICIONES</td>
      <td width="10%"></td>
    </tr>
    <tr class="estiloImput">
      <td id="elementoOrdenes">
        <input type="time" id="txtHoraCitas" style="width: 90%;">
      </td>
      <td id="elementoOrdenes">
        <select id="cmbDuracionMinutosOrdenes" style="width: 90%">
          <option value="">[ SELECCIONE ]</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="8">8</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
          <option value="25">25</option>
          <option value="30">30</option>
          <option value="35">35</option>
          <option value="40">40</option>
          <option value="45">45</option>
          <option value="50">50</option>
          <option value="55">55</option>
          <option value="60">60</option>
          <option value="120">2 HORAS</option>
          <option value="180">3 HORAS</option>
          <option value="240">4 HORAS</option>
          <option value="300">5 HORAS</option>
        </select>
      </td>
      <td id="elementoOrdenes">
        <select id="cmbConsultorio" style="width:90%;"
          onfocus="cargarComboGRALCondicion2('', '', this.id, 312, valorAtributo('cmbIdEspecialidadOrdenes'), valorAtributo('lblIdSedeAgendaOrdenes'))">
          <option value="">[ SELECCIONE ]</option>
        </select>
      </td>
      <td>
        <select id="cmbCantidadCitasTerapia" style="width:95%" onfocus="actualizarCantidadTerapiasAgenda()">
          <option value="">[ SELECCIONE ]</option>
        </select>
      </td>
      <td id="tdFrecuencia" hidden>
        <select id="cmbFrecuenciaTerapia" style="width:95%">
          <option value="">[ SELECCIONE ]</option>
          <option value="1">Diaria</option>
          <option value="2">Cada 2 D�as</option>
          <option value="3">Cada 3 D�as</option>
          <option value="4">Cada 4 D�as</option>
          <option value="5">Cada 5 D�as</option>
          <option value="6">Cada 6 D�as</option>
          <option value="7">Cada 7 D�as</option>
          <option value="8">Cada 8 D�as</option>
          <option value="9">Cada 9 D�as</option>
          <option value="10">Cada 10 D�as</option>
          <option value="15">Cada 15 D�as</option>
          <option value="30">Cada 30 D�as</option>
        </select>
      </td>
      <td id="tdFrecuencia" hidden>
        <select id="cmbRepeticiones" style="width:95%" onfocus="calcularRepeticionesProgramacion()">
          <option value="">[ SELECCIONE ]</option>
        </select>
      </td>
      <td align="CENTER" id="elementoHomeCare">
        <input type="button" class="small button blue" style="width: 40%;" value="PROGRAMAR LE"
          onClick="verificarNotificacionAntesDeGuardar('crearProgramacionLE')" />
      </td>
      <td align="CENTER" id="elementoOrdenes">
        <input type="button" class="small button blue" style="width: 70%;" value="PROGRAMAR CITAS"
          onClick="verificarNotificacionAntesDeGuardar('crearProgramacionOrdenes')" />
      </td>
    </tr>
  </table>
  <table width="100%">
    <tr></tr>
  </table>
  <table width="100%">
    <tr>
      <td colspan="4">
        <table id="listaProgramacionTerapiasAgenda"></table>
      </td>
    </tr>
    <tr>
      <td colspan="4" align="CENTER">
        <input type="button" class="small button blue" value="MODIFICAR CITAS"
          onClick="mostrarVentanitaModificarCitasBloque()" />
      </td>
    </tr>
  </table>
  <table width="100%">
    <tr>
      <td colspan="2">
        <table id="listaProgramacionLE"></table>
      </td>
    </tr>
    <tr>
      <td align="CENTER">
        <input type="button" class="small button blue" value="GUARDAR LISTAS DE ESPERA"
          onclick="verificarNotificacionAntesDeGuardar('guardarProgramacionLE')" />
      </td>
      <td>
        <input type="button" class="small button blue" value="MODIFICAR LE"
          onclick="mostrarVentanitaModificarLEBloque()" />
      </td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
</div>

<div id="divVentanitaAdjuntos" style="display:none; z-index:2000; top:1px; width:900px; left:150px;">
  <div class="transParencia" style="z-index:2001; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2002; position:fixed; top:100px; left:210px; width:70%">
    <table width="100%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left">
          <input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
        </td>
        <td align="right">
          <input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaAdjuntos')" />
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <div id="divParaArchivosAdjuntosOrdenExterna"></div>
        </td>
      </tr>
    </table>
  </div>
</div>

<div id="divVentanitaMostrarAgenda"
  style="position:absolute; display:none; top:70px; left: 10%;  width:1205px; height:90px; z-index:100">
  <table width="90%" class="fondoTabla">
    <tr class="estiloImput">
      <td align="left"><input name="btn_cerrar" title="divVentanitaMostrarAgenda" type="button" align="right"
          value="CERRAR" onclick="ocultarMostarAgenda()" /></td>
      <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
          onclick="ocultarMostarAgenda()" /></td>
    </tr>
    <tr class="camposRepInp">
      <td width="100%" colspan="5">Informaci&oacute;n de la agenda</td>
    </tr>
    <tr>
      <td colspan="5">
        <table width="100%">
          <tr class="titulosListaEspera">
            <td align="CENTER">SEDE</td>
            <!--<td>ESPECIALIDAD</td>
                                                            <td>PROFESIONAL</td>-->
          </tr>
          <tr class="titulosListaEspera">
            <td align="CENTER"><label id="lblIdSedeAgendaOrdenes"></label>-<label id="lblSedeAgendaOrdenes"></label>
            </td>
          </tr>
          <tr class="titulosListaEspera">
            <td align="CENTER"><label id="lblIdEspecialidadAgendaOrdenes"></label>-<label
                id="lblEspecialidadAgendaOrdenes"></label></td>
          </tr>
          <tr class="titulosListaEspera">
            <td align="CENTER"><label id="lblIdProfesionalAgendaOrdenes"></label>-<label
                id="lblProfesionalAgendaOrdenes"></label></td>
          </tr>
        </table>
        <table width="40%" cellpadding="0" cellspacing="0" align="center">
          <tr class="titulosListaEspera" align="center">
            <td aling="center" width="25%">FECHA INICIO</td>
            <td aling="center" width="25%">FECHA FIN</td>
            <td align="center" width="25">EXITO</td>
            <!--<td width="22%">MUNICIPIO</td>-->
            <!--<td width="22%">COMUNA / LOCALIDAD</td>-->
            <td width="25%"></td>
          </tr>
          <tr class="estiloImput">
            <td>
              <input type="date" value="<%=LocalDate.now()%>" id="txtFechaInicioA" min="2020-01-01">
            </td>
            <td>
              <input type="date" value="<%=LocalDate.now()%>" id="txtFechaFinA" min="2020-01-01">
            </td>
            <!--<td>
                                                                <input type="text" id="txtIdMunicipioA"  onkeypress="llamarAutocomIdDescripcionConDato('txtMunicipioA', 2013)" style="width:60%"/></td> 
                                                            </td>  
                                                            <td>
                                                                <input type="text" id="txtIdLocalidadA"  onkeypress="llamarAutocomIdDescripcionConDato('txtLocalidadA', 898)" style="width:60%"/></td> 
                                    
                                                            </td> -->
            <td>
              <select id="cmbExitoAgenda">
                <option value="S">SI</option>
                <option value="N">NO</option>
              </select>
            </td>
            <td>
              <input type="button" class="small button blue" value="BUSCAR"
                onclick="buscarAGENDA('listaVerProgramacion')">
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="5">
        <div style="height:-500px; width:100%;">
          <!--<div id="pager1" ></div>-->
          <table width="100%" id="listaVerProgramacion"></table>

        </div>
      </td>
    </tr>
  </table>
</div>
<div class="bubble" id="bubble" onclick="maximizarVentanita('divInfoPacienteVentana')">
  <i class="fa-solid fa-address-card"></i>
</div>

<div id="divInfoPacienteVentana"
  style="display: none; top: 75px; right: 3%; width: 170px; position: fixed; white-space: normal;"
  onclick="moverTarjeta()">
  <div class="card-container">

    <div class="interno-card" id="miTarjeta">
      <div class="class-card barbarian">
        <div class="class-card__image class-card__image--barbarian">
          <!-- <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divInfoPacienteVentana')" /> -->
          <label onclick="ocultar('divInfoPacienteVentana')" class="cerrarInfoPaciente">
            <i class="fas fa-times-circle"></i>
          </label>
          <label onclick="minimizarVentanita('divInfoPacienteVentana')" class="minimizarInfoPaciente">
            <i aria-hidden="true" class="fas fa-chevron-circle-down"></i>
          </label>
          <i class="fas fa-hospital-user fasCard"></i>
        </div>
        <div class="class-card__level class-card__level--barbarian">NOMBRE PACIENTE:</div>
        <div class="class-card__unit-name"><label id="lblNombrePacienteVentanita" class=""
            style="color:rgb(0, 0, 0);"></label></div>
        <div class="grRh">
          <label for="">RH:</label>
          <label for="" id="lblRH"></label>
        </div>
        <div class="class-card__unit-description">
          <label id="lblEdadPacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label><label class=""
            style="color:white;"></label>
        </div>
        <div class="parentCard">

          <div id="divDatosGestantes" style="display: flex; padding-top: 5%; margin-left: 13%; gap:5px;">
            <label style="color:rgb(231, 58, 58);"> <b>Gestante</b> </label>

            <div class="onoffswitch2">
              <input type="checkbox" name="chkgestante" class="onoffswitch-checkbox2" id="chkgestante"
                onclick="valorSwitchsn(this.id, this.checked);guardarGestanteVentanita(document.getElementById('lblIdPaciente').innerHTML , this.value,'ventana principal'); ">
              <label class="onoffswitch-label2" for="chkgestante">
                <span  class="onoffswitch-inner2" id="chkgestante-span1"></span>
                <span  class="onoffswitch-switch2" id="chkgestante-span2"></span>
              </label>
            </div>

          </div>

          <div>
            <!-- <h5>Creatinina</h5> -->
            <p><label id="lblCreatininaVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
          </div>
          <div>
            <!-- <h5>TFG Actual</h5> -->
            <p><label id="lblTFGVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
          </div>
          <div>
            <!-- <h5>GRADO</h5> -->
            <p><label id="lblEstadioVentanita" class="" style="color:rgb(1, 1, 1);"></label></p>
          </div>
        </div>
      </div> <!-- end class-card barbarian-->
    </div> <!-- end wrapper -->
  </div> <!-- end container -->
</div>

<!-------------------------------------------------------------------->
<input type="hidden" id="txtOrdenSeleccionada">
<input type="hidden" id="txtClaseProcedimientoOrdenSeleccionada">
<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />
<input type="hidden" id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden" id="txtTipoDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblTituloDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblIdAdmisionVistaPrevia" value="" />

<input type="hidden" id="lblIdProcedimientoAutoriza" />
<input type="hidden" id="lblNomProcedimientoAutoriza" />
<input type="hidden" id="lblCantidadLab" />
<input type="hidden" id="lblDiagnosticoLab" />
<input type="hidden" id="lblIdProcedimientoLE" />
<!-------------------------fin para la vista previa---------------------------- -->
