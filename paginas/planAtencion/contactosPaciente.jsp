<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1300" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div align="center" id="tituloForma">
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="CENTRO DE CONTACTOS" />
                </jsp:include>
            </div>
        </td>
    </tr>

    <tr>
        <td valign="top">
            <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0">
                <tr class="titulos" style="display:none ">
                    <td colspan="2">Id</td>
                    <td colspan="3">Paciente</td>
                </tr>
                <tr>

                <tr class="titulos">
                    <td width="20%">Tipo Id</td>
                    <td width="10%">Identificacion</td>
                    <td width="10%">Tipo encuesta</td>
                    <td width="10%">Estado encuesta</td>
                    <td width="15%">Alerta</td>
                    <td width="15%">Medio Recepcion</td>
                    <td width="15%">Admision</td>
                    <td></td>
                </tr>

                <tr class="estiloImput">
                    <td>
                        <select size="1" id="cmbTipoIdBus" style="width:70%;" 
                            title="Permite realizar la búsqueda por Tipo de Documento">
                            <option value=""></option>
                            <%     resultaux.clear();
                                   resultaux=(ArrayList)beanAdmin.combo.cargar(105);    
                                   ComboVO cmb105; 
                                   for(int k=0;k<resultaux.size();k++){ 
                                         cmb105=(ComboVO)resultaux.get(k);
                            %>
                            <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                <%=cmb105.getDescripcion()%></option>
                            <%}%>                       
                       </select>
                   </td> 
                   <td><input  type="text" id="txtIdentificacionBus" title="Permite realizar la búsqueda por Identificación" style="width:80%"  onkeypress="javascript:return soloTelefono(event);" size="20" maxlength="20"    /></td>
                   <td>
                    <select size="1" id="cmbIdEncuestasBus" title="Permite realizar la búsqueda por Tipo de  Encuesta" style="width:80%"  >                        
                        <option value="">[ TODO ]</option>  
                        <%     resultaux.clear();
                                resultaux=(ArrayList)beanAdmin.combo.cargar(960);    
                                ComboVO cmb1; 
                                for(int k=0;k<resultaux.size();k++){ 
                                    cmb1=(ComboVO)resultaux.get(k);
                         %>
                            <option value="<%= cmb1.getId()%>" title="<%= cmb1.getTitle()%>">
                                <%= cmb1.getId()+"  "+cmb1.getDescripcion()%></option>
                            <%}%>                                  
                    </select>                       
                   </td>  
                   <td>
                    <select size="1" id="cmbIdEstadoEncuestasBus" title="Permite realizar la búsqueda por estado de la Encuesta" style="width:80%"  >
                        <option value="">[ TODO ]</option>
                        <option value="A">ABIERTA</option>
                        <option value="C">CERRADA</option>
                    </select>                       
                   </td>                   
                   <td>
                    <select id="cmbIdRiesgoBus" style="width:80%" onfocus="cargarComboGRAL('', '', this.id, 171)">
                        <option value="">[ TODO ]</option>  
                        
                    </select>                       
                   </td>  
                   <td>
                    <select id="cmbIdMedioRecepcion" style="width:80%">
                        <option value="">[ TODO ]</option>  
                        <%     resultaux.clear();
                                resultaux=(ArrayList)beanAdmin.combo.cargar(172);    
                                for(int k=0;k<resultaux.size();k++){ 
                                    cmb1=(ComboVO)resultaux.get(k);
                         %>
                            <option value="<%= cmb1.getId()%>" title="<%= cmb1.getTitle()%>">
                                <%= cmb1.getId()+"  "+cmb1.getDescripcion()%></option>
                            <%}%> 
                    </select> 
                   </td>
                    <td>
                        <select size="1" id="cmbIdAdmision" title="Permite realizar la búsqueda por Admisión"style="width:80%"  >
                            <option value="">[ TODO ]</option>
                            <option value="S">CON ADMISION</option>
                            <option value="N">SIN ADMISION</option>
                            
                        </select>                       
                    </td>  
                    <td>
                        <input name="btn_BUSCAR" title="CC63-Permite realizar la busqueda de pacientes encuestados" type="button" class="small button blue" value="BUSCAR"  onclick=" buscarRutas('listPacienteEncuestas')" />
                    </td>                                        
                </tr>
                <tr class="titulos">
                    <td colspan="8" width="100%">
                          <div id="idDivListPaciEncue"  style="height:430px; width:100%">
                            <table  width="100%" id="listPacienteEncuestas" ></table>
                          </div> 
                    </td>                    
                </tr>
                <tr class="titulos">
                    <td colspan="8" width="100%">
                            <table width="100%" >                  
                                <tr  class="estiloImput">
                                    <caption class="camposRepInp"> <label id="lblIdPaciente"></label>  </caption>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2">Paciente <input  type="text" id="txtIdBusPaciente" title="Ingrese Identificacion o Nombre del paciente" style="width:80%"   size="40"     />
                                                    <img width="18px" height="18px" align="middle" title="Seleccione Buscar " id="idLupitaVentanitaPacienC" onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                </td><div id="divParaVentanita"></div> 
                                                <td><input name="btn_BUSCAR PACIENTE" title="Permite buscar Pacientes Registrados" 
                                                    type="button" class="small button blue" value="BUSCAR"  
                                                    onclick=" buscarInformacionBasicaPaciente(); setTimeout('cargarInformacionContactoPaciente()', 200)"/></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <input type="checkbox" id="sw_nuevo_paciente" onchange="elementosNuevoPacienteEncuesta()"> Nuevo paciente
                                    </td>
                                    <td colspan="2" id="elementosNuevoPaciente" hidden>
                                        <table width="100%">
                                            <tr>
                                                <th>Tipo de documento</th>
                                                <th>Numero de documento</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbTipoIdUrgencias" width="90%">
                                                        <option value=""></option>
                                                            <%     
                                                            resultaux.clear();
                                                            ComboVO cmbM; 
                                                            resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                                            for(int k=0;k<resultaux.size();k++){ 
                                                                    cmbM=(ComboVO)resultaux.get(k);
                                                            %>
                            <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                <%=cmbM.getId() + " " + cmbM.getDescripcion()%></option>
                            <%}%>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" width="90%" id="txtIdentificacionUrgencias" onkeypress="javascript:return soloTelefono(event);">
                                                </td>
                                                <td>
                                                    <input type="button" class="small button blue" value="CREAR" onclick="nuevoPacienteEncuesta()">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                 </tr>
                                 <tr>
                                    <td colspan="6"><hr></td>
                                </tr>
                                 <tr class="titulos">   
                                    <td width="20%">Tipo Id</td>
                                    <td width="20%">Identificacion</td>
                                    <td width="17%">Apellidos</td>  
                                    <td width="17%">Nombres</td>                      
                                    <td width="17%">Fecha Nacimiento / edad </td>   
                                    <td width="17%">Sexo</td>                                            
                                </tr>            
                                <tr class="estiloImput">
                                    <td>                   
                                            <select size="1" id="cmbTipoId" style="width:70%"  >
                                            <option value=""></option>
                                                <%     resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(105);    
                                                    ComboVO cmb5; 
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmb5=(ComboVO)resultaux.get(k);
                                                %>
                            <option value="<%= cmb5.getId()%>" title="<%= cmb5.getTitle()%>">
                                <%= cmb5.getId()+"  "+cmb5.getDescripcion()%></option>
                            <%}%>                       
                                        </select>
                                    </td> 
                                    <td><input  type="text" id="txtIdentificacion"  style="width:95%"   size="60" maxlength="60"    onkeypress="javascript:return soloTelefono(event);"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtApellido1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />
                                        <input type="text" id="txtApellido2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />
                                    </td> 
                                    <td>
                                        <input type="text" id="txtNombre1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"   style="width:45%" />
                                        <input type="text" id="txtNombre2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />  
                                    </td>   
                                    <td><input id="txtFechaNac" maxlength="10" type="text"  size="8" value=""  maxlength="10"  /> /
                                        <label id="lblEdad" style="font-size: 13PX; display:none;"> - </label>
                                        <label id="txtEdadCompleta" style="font-size: 10px;"> - </label>
                                    </td>                           
                                    <td> <select size="1" id="cmbSexo" maxlength="30" style="width:95%"  >                                            
                                            <option value=""></option>
                                                <%  resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(110); 
                                                    ComboVO cmbS; 
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmbS=(ComboVO)resultaux.get(k);
                                                %>
                            <option value="<%= cmbS.getId()%>" title="<%= cmbS.getTitle()%>"><%=cmbS.getDescripcion()%>
                            </option>
                            <%}%>                     
                                        </select>
                                    </td>
                                </tr>
                            <tr class="titulos">
                                <td width="20%">Municipio</td> 
                                <td width="20%">Direcci&oacute;n</td>   
                                <td width="17%">Acompañante</td>                    
                                <td width="17%">Celular1</td>
                                <td width="17%">Celular2</td>
                                <td width="17%">Telefono</td>
                            </tr>                            
                            <tr class="estiloImput">
                                <td><input type="text" id="txtMunicipio" size="20"   oninput="llenarElementosAutoCompletarKey(this.id, 3704, this.value)" style="width:70%"    /></td> 
                                <td><input type="text" id="txtDireccionRes" size="60" maxlength="60"  style="width:95%"  onkeypress="javascript:return teclearsoloan(event);"  /></td>   
                                <td><input type="text" id="txtNomAcompanante"  size="100" maxlength="100"  style="width:95%"  onkeypress="javascript:return teclearExcluirCaracter(event);"   /> </td>                    
                                <td>
                                    <input type="text" id="txtCelular1" size="30"  style="width:95%" maxlength="10"  onKeyPress="javascript:return teclearsoloDigitos(event);"  />
                                </td>
                                <td>
                                    <input type="text" id="txtCelular2" size="30" maxlength="10" style="width:95%"   onKeyPress="javascript:return teclearsoloDigitos(event);"  />                   
                                </td>
                                <td>   
                                    <input type="text" id="txtTelefonos" size="30" maxlength="7" style="width:95%"   onKeyPress="javascript:return teclearsoloDigitos(event);"  />                    
                                </td>                                       
                            </tr>
                            <tr class="titulos">
                                <td>Email</td>                                          
                            </tr>   
                            <tr class="estiloImput">
                                <td><input type="text" id="txtEmail" size="50"  maxlength="50" onkeypress="javascript:return email(event);" style="width:70%"  /> </td>     
                            </tr> 
                            <tr>
                                <td><hr></td>
                                <% if(beanSession.usuario.preguntarMenu("CITB01") ){%>    
                                <td>
                                    <input type="button" id="CITB01" title="CITB01" onclick="modificarCRUD('modificarPacienteEncuesta')" class="small button blue" value="MODIFICAR PACIENTE"/>
                                </td>
                                <%}%> 
                                <% if(beanSession.usuario.preguntarMenu("CITB02") ){%> 
                                <td colspan="3">
                                    SEDE:
                                    <select id="cmbSede" onfocus="comboDependienteEmpresa('cmbSede','1265')" style="width: 40%;">
                                        <option value="">[SELECCIONE SEDE]</option>
                                    </select>
                                   SERVICIO:
                                    <select id="cmbServicioEntrada" onfocus="cargarServicioDesdeSede('cmbSede','cmbServicioEntrada')" style="width: 40%;">
                                        <option value="">[SELECCIONE SERVICIO]</option>
                                    </select>
                                 
                                </td colspan="2">
                                
                                <td>
                                    <input style="width:90%;" class="small button blue" id="CITB02" title="CITB02" type="button" value="CREAR ADMISION SIN ENCUESTA" onclick="verificarNotificacionAntesDeGuardar('crearAdmisionSinEncuesta')">
                                </td>
                                <%}%> 
                                <td><hr></td>
                            </tr> 
                            <tr class="titulos"  >
                                <td colspan="6" valing= "top">
                                    <table width="100%"  align="center" > 
                                        <tr class="titulos" >
                                            <td  width="100%"  valing= "top" > 
                                                <div id="divDepositoContactosPaciente"  ></div>     
                                            </td>
                                        </tr>   
                                    </table> 
                                </td>
                            </tr>               
                        </table>      <label id="lblTipoDocumento">EVMD</label>                   

                    </td>                    
                </tr>                
            </table>            
    </tr>        
</table> 

<div id="divValidaciones" hidden>   
    <table id="listGrillaFormulasE" class="scroll"></table>    
    <table id="listGrillaValoresValidacionesE" class="scroll"></table>    
    <table id="listGrillaValidacionesE" class="scroll"></table>
  </div>   

<div id="divCrearPacienteUrgencias"  style="display:none; z-index:2000; top:100px; left:50px; ">
    <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>  
    <div style="z-index:2004; position:fixed; top:200px;left:200px; width:70%">  
        <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="
                    ocultar('divCrearPacienteUrgencias'); 
                    reiniciarElementosNuevoPacienteEncuesta()"/></td>              
                <td align="right" colspan="4"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="
                    ocultar('divCrearPacienteUrgencias'); 
                    reiniciarElementosNuevoPacienteEncuesta()"/></td>  
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Identificación</td>	                                                                                               
                            <td width="10%">Apellido 1</td>                                 
                            <td width="10%">Apellido 2</td>   
                            <td width="10%">Nombre 1</td> 
                            <td width="10%">Nombre 2</td>
                            <td width="40%">Acompañante</td>                                                               
                        </tr>		
                        <tr class="estiloImput"> 
                            <td>
                                <strong><label id="lblTipoIdCrearPacienteURG"></label></strong>
                                <strong><label id="lblIdCrearPacienteURG"></label></strong>
                            </td>    
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtApellido1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>                            
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtApellido2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>                                                                                                                                 
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNombre1Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>      
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNombre2Urgencias" style="width:95%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);"></td>   
                            <td><input type="text" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtAcompananteUrgencias" style="width:95%" onblur="v28(this.value, this.id);"></td>                            
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table>
                                    <tr class="titulos"> 
                                        <td width="10%">Sexo</td>	
                                        <td width="15%">Fecha Nacimiento</td>
                                        <td width="35%">Telefonos</td>  
                                        <td width="30%">Administradora EPS</td>                                                                                                                                                                     
                                    </tr>		
                                    <tr class="estiloImput"> 
                                        <td>
                                            <select id="cmbSexoUrgencias" style="width:95%">	                                        
                                                <option value=""> </option>  
                                                <option value="F">FEMENINO</option> 
                                                <option value="M">MASCULINO</option>                              
                                                <option value="O">OTRO</option>                                                          
                                            </select>	                   
                                        </td>
                                        <td><input style="width:95%" type="text" id="txtFechaNacimientoUrgencias"/></td>      
                                        <td>
                                            <input type="text"  id="txtTelefonoUrgencias" style="width:30%"  size="50"  maxlength="50"  >
                                            <input type="text"  id="txtCelular1Urgencias" style="width:30%"  size="50"  maxlength="50"  >
                                            <input type="text"  id="txtCelular2Urgencias" style="width:30%"  size="50"  maxlength="50"  >
                                        </td>                                                             
                                        <td><input type="text"  id="txtAdministradoraUrgencias" onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradoraUrgencias', 308)" style="width:95%"/>                                  	 </td>                                                                                                                                                                
                                    </tr>  
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table width=681.9>
                                    <tr class="titulos"> 
                                        <td width="30%">Municipio</td> 
                                        <td width="30%">Direccion</td>   
                                        <td width="20%">Regimen Afiliacion</td>
                                        <td width="20%">Tipo Regimen</td>
                                    </tr>		
                                    <tr class="estiloImput"> 
                                        <td><input type="text" id="txtMunicipioResidenciaUrgencias"  style="width:85%">
                                            <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipioResidenciaUrgencias', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                                            <div id="divParaVentanita"></div>                                                    
                                        </td>    
                                        <td><input type="text" id="txtDireccionUrgencias" style="width:95%" onblur="v28(this.value, this.id);" ></td>                            
                                        <td>
                                            <select id="cmbRegimenUrgencias" style="width:95%">
                                                <option value="A">PARTICULAR</option>
                                                <option value="C">CONTRIBUTIVO</option>
                                                <option value="S">SUBSIDIADO</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="cmbTipoRegimenUrgencias" style="width:95%">
                                                <option value="0">OTRO</option>
                                                <option value="1">BENEFICIARIO</option>
                                                <option value="2">COTIZANTE</option>
                                            </select> 
                                        </td>
                                    </tr> 
                                </table> 
                            </td>
                        </tr>
                        <tr class="titulos">
                            <td align="CENTER" colspan="6"> 
                                <input type="button" class="small button blue" value="CREAR PACIENTE" onclick="modificarCRUD('crearPacienteEncuesta');"/>                         
                            </td>
                        </tr>
                    </table> 
                </td>
            </tr>
        </table>  
    </div>     
</div>