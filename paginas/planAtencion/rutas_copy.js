
var motivos = ":Please Select";

function contenedorRutaPacienteHC() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/planAtencion/enrutarPacienteDetalle.jsp?', true);
    varajaxMenu.onreadystatechange = llenarcontenedorRutaPacienteHC;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcontenedorRutaPacienteHC() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaContenedorHojaRuta").innerHTML = varajaxMenu.responseText;


            asignaAtributo('txtPrestadorAsignadoRuta', IdEmpresa() + '-' + NombreEmpresa(), 0)
            asignaAtributo('txtPrestadorAsignado', IdEmpresa() + '-' + NombreEmpresa(), 0)

            buscarRutas('listGrillaRutaPaciente')


        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function buscarRutas(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    //var r = httpGet('http://192.168.0.170:8383/clinica/paginas/planAtencion/consulta_motivo.jsp?id_motivo=C'); 
    //alert(r);

    // $.get('http://192.168.0.170:8383/clinica/paginas/planAtencion/consulta_motivo2.jsp?id_motivo=C', function(data) {
    //     var res = $(data).html();
    //     //$("#"+rowid+"_Motivo").html(res);
    //     alert(res);
    // });

    switch (arg) {

        case 'listGrillaListaEsperaRuta':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 20;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=891&parametros=";
            add_valores_a_mandar(valorAtributo('cmbTipoCita'));
            add_valores_a_mandar(valorAtributo('cmbConCita'));
            var lastSel = -1; 
            var estados = httpGet('http://192.168.0.170:8383/clinica/paginas/planAtencion/consulta.jsp');
            var estados_limpio = estados.trim();
            estados_limpio = estados_limpio.substring(0,estados_limpio.length-1);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id_lista', 'id_plan_atencion', 'Plan Atencion', 'id_ruta', 'Ruta', 'id_especialidad', 'Especialidad', 'id_sub_especialidad',
                    'Sub Especialidad', 'id_tipo_cita', 'Tipo Cita', 'id_paciente', 'Identificacion', 'Nombre', 'id_cita', 'Fecha',
                    'Hora', 'h', 'm', 'd', 'id_estado', 'Estado',
                    'id_motivo_consulta', 'Motivo', 'id_motivo_consulta_clase', 'Clasificacion',
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'id_lista', index: 'id_lista', width: anchoP(ancho, 10) },
                    { name: 'id_plan_atencion', index: 'id_plan_atencion', align: 'center', hidden: true },
                    { name: 'Plan Atencion', index: 'nombre_plan', align: 'center', hidden: true },
                    { name: 'id_ruta', index: 'id_ruta', hidden: true },
                    { name: 'Ruta', index: 'ruta', hidden: true },
                    { name: 'id_especialidad', index: 'id_especialidad', hidden: true },
                    { name: 'Especialidad', index: 'Especialidad', hidden: true },
                    { name: 'id_sub_especialidad', index: 'id_sub_especialidad', hidden: true },
                    { name: 'Sub Especialidad', index: 'sub_especialidad', hidden: true },
                    { name: 'id_tipo_cita', index: 'id_tipo_cita', hidden: true },
                    { name: 'Tipo Cita', index: 'tipo_cita', width: anchoP(ancho, 20) },
                    { name: 'id_paciente', index: 'id_paciente', hidden: true },
                    { name: 'Identificacion', index: 'Identificacion', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 30), align: 'left'},
                    { name: 'id_cita', index: 'id_cita', hidden: true },
                    { name: 'Fecha', index: 'Fecha', width: anchoP(ancho, 10), align: 'left', editable: true},
                    { name: 'Hora', index: 'Hora', width: anchoP(ancho, 10), align: 'left', editable: true },
                    { name: 'h', index: 'h', hidden: true },
                    { name: 'm', index: 'm', hidden: true },
                    { name: 'd', index: 'd', hidden: true },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'Estado',
                      index: 'Estado',
                      width: anchoP(ancho, 10),
                      align: 'left',
                      editable:true,
                      edittype:"select",
                      formatter: 'select',
                      editoptions:
                      {value:estados_limpio},
                      dataEvents: [
                          {
                            type: "change",
                            fn: function(e){ 
                                var thisval = $(e.target).val();
                                alert(thisval); 
                                $.get('http://192.168.0.170:8383/clinica/paginas/planAtencion/consulta_motivo2.jsp?id_motivo='+thisval, function(data) {
                                    var res = $(data).html();
                                    $('#'+rowid + '_Motivo').html(res);
                                });
                            }
                          }
                      ]
                    },
                    { name: 'id_motivo_consulta',
                      index: 'id_motivo_consulta',
                      hidden: true 
                    },
                    { name: 'Motivo',
                      index: 'Motivo',
                      width: anchoP(ancho, 20),
                      align: 'left',
                      editable: true,
                      edittype: 'select',
                      editoptions: {value:motivos}

                    },
                    { name: 'id_motivo_consulta_clase', index: 'id_motivo_consulta_clase', hidden: true },
                    { name: 'Clasificacion',
                      index: 'Clasificacion',
                      width: anchoP(ancho, 20),
                      align: 'left',
                      editable: true
                    },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdAgenda', datosRow.id_cita, 0)
                    asignaAtributo('txtIdListaEspera', datosRow.id_lista, 0)
                    asignaAtributo('txtFechaCita', datosRow.Fecha, 0)
                    asignaAtributo('cmbHoraCita', datosRow.h, 0)
                    asignaAtributo('cmbMinutoCita', datosRow.m, 0)
                    asignaAtributo('cmbPeriodoCita', datosRow.d, 0)
                    asignaAtributo('cmbEstadoCita', datosRow.id_estado, 0);
                    comboDependiente('cmbMotivoConsulta', 'cmbEstadoCita', '96');
                    asignaAtributo('txtIdEspecialidad', datosRow.id_especialidad, 0);
                    asignaAtributo('txtIdSubEspecialidad', datosRow.id_sub_especialidad, 1);
                    if(rowid && rowid!==lastSel){ 
                        jQuery('#drag' + ventanaActual.num).find('#' + arg).restoreRow(lastSel); 
                        lastSel=rowid; 
                     }
                    
                     console.log("Fila #"+ rowid +" ha sido seleccionada"); 
                },
                ondblClickRow: function(id, ri, ci){
                    if(id && id!==lastSel){ 
                        jQuery('#drag' + ventanaActual.num).find('#' + arg).restoreRow(lastSel); 
                        lastSel=id; 
                    }

                    alert("EL identificador de la columna es " + id); 
                    alert("Indice fila "+ri); 
                    alert("Indice de la celda " + ci); 
                    jQuery('#drag' + ventanaActual.num).find('#' + arg).editRow(id, true, null, null, 'clientArray', null,
                    function (rowid, response) {  // aftersavefunc
                        jQuery('#drag' + ventanaActual.num).find('#' + arg).setColProp('Estado', { editoptions: { value: estado_limpio} });
                    });
                    return;  
                },
                editurl: 'clientArray',
                height: 250,
                width: ancho,
                sortorder: "desc",
                pager: '#pager', 
                viewrecords: true,
                rownumbers: true,
                caption: "Probando funciones de jqGrid"
            });

            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaCertificadoPaciente':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=865&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId', 'Identificacion', 'Nombre1', 'Nombre2', 'Apellido1', 'Apellido2',
                    'Fecha Nacimiento', 'Sexo', 'Direccion', 'Municipio Residencia',
                    'celular1', 'certificado celular 1', 'celular2', 'certificado celular 2', 'celular3', 'email', 'certificado email'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 5) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 15) },
                    { name: 'Nombre1', index: 'Nombre1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Nombre2', index: 'Nombre2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Apellido1', index: 'Apellido1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Apellido2', index: 'Apellido2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'FechaNacimiento', index: 'FechaNacimiento', hidden: true },
                    { name: 'Sexo', index: 'Sexo', hidden: true },
                    { name: 'Direccion', index: 'Direccion', hidden: true },
                    { name: 'MunicipioResi', index: 'MunicipioResi', hidden: true },
                    { name: 'celular1', index: 'celular1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular1_certificado', index: 'celular1_certificado', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular2', index: 'celular2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular2_certificado', index: 'celular2_certificado', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular3', index: 'celular3', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'email', index: 'email', hidden: true },
                    { name: 'email_certificado', index: 'email_certificado', width: anchoP(ancho, 20), align: 'left' },
                ],
                height: 250,
                width: ancho,
                onSelectRow: function (rowid) {
                    mostrar('divEditar')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblId', datosRow.id, estad);
                    asignaAtributo('cmbTipoId', datosRow.TipoId, 1);
                    asignaAtributo('txtIdentificacion', datosRow.identificacion, 1);
                    asignaAtributo('txtNombre1', datosRow.Nombre1, estad);
                    asignaAtributo('txtNombre2', datosRow.Nombre2, estad);
                    asignaAtributo('txtApellido1', datosRow.Apellido1, estad);
                    asignaAtributo('txtApellido2', datosRow.Apellido2, estad);
                    asignaAtributo('cmbSexo', datosRow.Sexo, estad);
                    asignaAtributo('txtFechaNacimiento', datosRow.FechaNacimiento, estad);
                    asignaAtributo('txtCelular1', datosRow.celular1, estad);
                    asignaAtributo('txtCelular2', datosRow.celular2, estad);
                    asignaAtributo('txtTelefonos', datosRow.celular3, estad);
                    asignaAtributo('txtMunicipio', datosRow.MunicipioResi, estad);
                    asignaAtributo('txtDireccionRes', datosRow.Direccion, estad);
                    asignaAtributo('txtNomAcompanante', datosRow.Observaciones, estad);
                    asignaAtributo('lblIdAnteriores', datosRow.idAnteriores, estad);
                    asignaAtributo('txtEmail', datosRow.email, estad);
                    traerDatos('buscarIdentificacionPaciente');
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listNotificacionesPaciente':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=864" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID NOT', 'ASUNTO', 'CONTENIDO DEL MENSAJE', 'ESTADO NOT', 'TIPO', 'FECHA', 'ESTADO CITA', 'OBSERVACION CITA'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_NOT_ESCRITA', index: 'ID_NOT_ESCRITA', width: anchoP(ancho, 3) },
                    { name: 'ASUNTO', index: 'ASUNTO', width: anchoP(ancho, 5) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', width: anchoP(ancho, 35) },
                    { name: 'ESTADO_NOT', index: 'ESTADO_NOT', width: anchoP(ancho, 4) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 4) },
                    { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 6) },
                    { name: 'ESTADO_CITA', index: 'ESTADO_CITA', width: anchoP(ancho, 4) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 6) }
                ],
                height: 250,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('txtObservaciones', datosRow.OBSERVACIONES, 0)
                    asignaAtributo('txtDuracion', datosRow.DURACION_DIAS, 0)
                    asignaAtributo('cmbCiclica', datosRow.CICLICA, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    buscarRutas('listGrillaRutaTipoCita')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listagendarDesdeLERuta': //plan
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#divContenidoLETraer").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=861&parametros=";
            add_valores_a_mandar(valorAtributo('cmbBusSinCita'));
            add_valores_a_mandar(valorAtributo('cmbTipoCitaLE'));
            add_valores_a_mandar(valorAtributo('cmbEstadoLE'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignadoAgenda'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadLE'));
            add_valores_a_mandar(valorAtributo('cmbNecesidad'));
            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));
            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteLE'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Cuantos', 'ID LISTA', 'ID PACIENTE', 'TIPO ID', 'IDENTIFICACION', 'NOMBRE PACIENTE', 'FECHA_NACIMIENTO', 'EDAD',
                    'TELEFONO', 'ID_MUNICIPIO', 'MUNICIPIO', 'DIRECCION',
                    'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'PROFESIONAL', 'DIAS ESPERA', 'TIPO_CITA',
                    'NO_AUTORIZACION', 'FECHA_VIGENCIA', 'ID_GRADO_NECESIDAD', 'OBSERVACION', 'ID_ESTADO_LE', 'NOM_ESTADO_LE', 'ID_ELABORO', 'FECHA_ELABORO',
                    'CON CITA', 'ID_CITA', 'FECHA_CITA', 'HORA_CITA', 'HH_CITA', 'MIN_CITA', 'ID_ESTADO_CITA', 'NOM_ESTADO_CITA'
                ],
                colModel: [
                    { name: 'Cuantos', index: 'Cuantos', width: anchoP(ancho, 2) },
                    { name: 'ID_LISTA', index: 'ID_LISTA', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 2) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 7) },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 20) },
                    { name: 'FECHA_NACIMIENTO', index: 'FECHA_NACIMIENTO', hidden: true },
                    { name: 'EDAD', index: 'EDAD', width: anchoP(ancho, 3) },
                    { name: 'TELEFONO', index: 'TELEFONO', hidden: true },
                    { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                    { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 10) },
                    { name: 'DIRECCION', index: 'DIRECCION', hidden: true },

                    { name: 'ID_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    { name: 'NOM_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    { name: 'PROFESIONAL', index: 'PROFESIONAL', hidden: true },
                    { name: 'DIAS_ESPERA', index: 'DIAS_ESPERA', width: anchoP(ancho, 4) },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', hidden: true },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', hidden: true },
                    { name: 'FECHA_VIGENCIA', index: 'FECHA_VIGENCIA', hidden: true },
                    { name: 'ID_GRADO_NECESIDAD', index: 'ID_GRADO_NECESIDAD', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 15) },
                    { name: 'ID_ESTADO_LE', index: 'ID_ESTADO_LE', hidden: true },
                    { name: 'NOM_ESTADO_LE', index: 'NOM_ESTADO_LE', hidden: true },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                    { name: 'CON_CITA', index: 'ID_CITA', width: anchoP(ancho, 5) },
                    { name: 'ID_CITA', index: 'ID_CITA', hidden: true },
                    { name: 'FECHA_CITA', index: 'FECHA_CITA', width: anchoP(ancho, 5) },
                    { name: 'HORA_CITA', index: 'HORA_CITA', width: anchoP(ancho, 5) },
                    { name: 'HH_CITA', index: 'HH_CITA', hidden: true },
                    { name: 'MIN_CITA', index: 'MIN_CITA', hidden: true },
                    { name: 'ID_ESTADO_CITA', index: 'ID_ESTADO_CITA', hidden: true },
                    { name: 'NOM_ESTADO_CITA', index: 'NOM_ESTADO_CITA', width: anchoP(ancho, 10) }



                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    //  if (confirm('TRAER DE LA LISTA DE ESPERA A UNA CITA?')) {

                    // if (datosRow.CON_CITA == 'NO') {
                    mostrar('divVentanitaAgenda')
                    asignaAtributo('lblIdListaEspera', datosRow.ID_LISTA, 0)
                    asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.NOMBRE_PACIENTE), 0)
                    asignaAtributo('cmbTipoCita', datosRow.TIPO_CITA, 0)
                    asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                    asignaAtributo('txtFechaVigencia', datosRow.FECHA_VIGENCIA, 0)


                    asignaAtributo('lblIdAgendaDetalle', datosRow.ID_CITA, 0)
                    asignaAtributo('txtFechaCita', datosRow.FECHA_CITA, 0)
                    asignaAtributo('cmbHoraInicio', datosRow.HH_CITA, 0)
                    asignaAtributo('cmbMinutoInicio', datosRow.MIN_CITA, 0)
                    asignaAtributo('cmbEstadoCita', datosRow.ID_ESTADO_CITA, 0)



                    ocultar('divVentanitaListaEspera');
                    limpiarDatosPacientePaciente();
                    buscarInformacionBasicaPaciente();


                    /*} else
                        alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA !!!')*/


                },
                height: 500,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRutas':

            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 50;

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=868" + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbPlan'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'OBSERVACIONES', 'DURACION_DIAS', 'CICLICA', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 8) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', width: anchoP(ancho, 20) },
                    { name: 'DURACION_DIAS', index: 'DURACION_DIAS', width: anchoP(ancho, 8) },
                    { name: 'CICLICA', index: 'CICLICA', width: anchoP(ancho, 8) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 8) },
                ],
                height: 200,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 1);
                    ponerTitulo('txtId','No se puede modificar el ID'); 
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0);
                    asignaAtributo('txtObservaciones', datosRow.OBSERVACIONES, 0);
                    asignaAtributo('txtDuracion', datosRow.DURACION_DIAS, 0);
                    asignaAtributo('cmbCiclica', datosRow.CICLICA, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                    buscarRutas('listGrillaRutaTipoCita');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRutaTipoCita':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 60;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=869&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_PARAMETRO', 'ESPECIALIDAD', 'SUBESPECIALIDAD', 'TIPO', 'ESPERA', 'ORDEN', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PARAMETRO', index: 'ID_PARAMETRO', width: anchoP(ancho, 10) },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: anchoP(ancho, 15) },
                    { name: 'SUBESPECIALIDAD', index: 'SUBESPECIALIDAD', width: anchoP(ancho, 15) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 10) },
                    { name: 'ESPERA', index: 'ESPERA', width: anchoP(ancho, 10) },
                    { name: 'ORDEN', index: 'ORDEN', width: anchoP(ancho, 10) },
                    { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 10) }
                ],
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdParametr', datosRow.ID_PARAMETRO, 1);
                }
            });

            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRutaPaciente':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=870&parametros=";
            add_valores_a_mandar(valorAtributo('cmbRuta'));
            add_valores_a_mandar(valorAtributo('cmbRuta'));

            add_valores_a_mandar(valorAtributo('cmbPlan'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_PLAN', 'PLAN', 'ID_RUTA', 'RUTA', 'ID_PACIENTE', 'TIPO_ID', 'IDENTIFICACION', 'PACIENTE', 'EDAD (A&#209OS)', 'FECHA DE INGRESO', '% GESTIONADO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    { name: 'PLAN', index: 'PLAN', width: anchoP(ancho, 15) },
                    { name: 'ID_RUTA', index: 'ID_RUTA', hidden: true },
                    { name: 'RUTA', index: 'RUTA', width: anchoP(ancho, 15) },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO_ID', index: 'TIPO_ID', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 10) },
                    { name: 'PACIENTE', index: 'PACIENTE', width: anchoP(ancho, 15) },
                    { name: 'EDAD', index: 'EDAD (A&#209OS)', width: anchoP(ancho, 10) },
                    { name: 'FECHA_INGRESO', index: 'FECHA DE INGRESO', width: anchoP(ancho, 10) },
                    { name: 'GESTIONADO', index: '% GESTIONADO', width: anchoP(ancho, 10) }
                ],
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdPaciente', datosRow.ID_PACIENTE, 0);
                    asignaAtributo('cmbTipoId', datosRow.TIPO_ID, 0);
                    asignaAtributo('txtIdentificacion', datosRow.IDENTIFICACION, 0);
                    asignaAtributo('lblFechaIngreso', datosRow.FECHA_INGRESO, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                    asignaAtributo('lblIdRutaSeleccionada', datosRow.ID_RUTA, 0);
                    asignaAtributo('txtIdBusPaciente',
                        datosRow.ID_PACIENTE + "-" +
                        datosRow.TIPO_ID +
                        datosRow.IDENTIFICACION + " " +
                        datosRow.PACIENTE, 0);
                    buscarRutas('listGrillaRutaDelPaciente');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRutaDelPaciente':
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=871&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdRutaSeleccionada'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'Orden', 'idEspecialidad', 'Especialidad', 'idSubEspecialidad', 'ID_TIPO', 'Tipo Cita', 'Estado', 'id_prestado', 'Prestador Asignado',
                    'ListaEspera', 'grado_necesidad', 'observaciones', 'Dias Espera', 'Fecha limite de cita', 'Id cita', 'Fecha cita', 'Admision', 'Fecha Admision',

                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ORDEN', index: 'ORDEN', width: anchoP(ancho, 3) },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: anchoP(ancho, 15) },
                    { name: 'ID_SUBESPECIALIDAD', index: 'ID_SUBESPECIALIDAD', hidden: true },
                    { name: 'ID_TIPO_CITA', index: 'ID_TIPO_CITA', hidden: true },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', width: anchoP(ancho, 20) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 10) },
                    { name: 'ID_PRESTADOR', index: 'ID_PRESTADOR', hidden: true },
                    { name: 'PRESTADOR', index: 'PRESTADOR', width: anchoP(ancho, 20) },
                    { name: 'LISTA_ESPERA', index: 'LISTA DE ESPERA', width: anchoP(ancho, 7) },
                    { name: 'GRADO_NECESIDAD', index: 'GRADO_NECESIDAD', hidden: true },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', width: anchoP(ancho, 7) },
                    { name: 'DIAS_ESPERA', index: 'DIAS ESPERA', width: anchoP(ancho, 8) },
                    { name: 'FECHA_LIMITE', index: 'FECHA LIMITE DE CITA', width: anchoP(ancho, 10) },
                    { name: 'ID_CITA', index: 'ID CITA', width: anchoP(ancho, 5) },
                    { name: 'FECHA_CITA', index: 'FECHA DE CITA', width: anchoP(ancho, 10) },
                    { name: 'ADMISION', index: 'ADMISION', width: anchoP(ancho, 5) },
                    { name: 'FECHA_ADMISION', index: 'FECHA ADMISION', width: anchoP(ancho, 10) }

                ],
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdRutaDelPaciente', datosRow.ID, 1);
                    asignaAtributo('cmbIdEspecialidadEdit', datosRow.ID_ESPECIALIDAD, 0);
                    asignaAtributo('cmbIdSubEspecialidadEdit', datosRow.ID_SUBESPECIALIDAD, 0);
                    asignaAtributo('cmbTipoCitaEdit', datosRow.ID_TIPO_CITA, 0);
                    asignaAtributo('cmbEsperarEdit', datosRow.DIAS_ESPERA, 0);
                    asignaAtributo('txtOrdenEdit', datosRow.ORDEN, 0);
                    asignaAtributo('cmbNecesidadEdit', datosRow.GRADO_NECESIDAD, 0);
                    asignaAtributo('txtPrestadorAsignadoEdit', datosRow.ID_PRESTADOR + '-' + datosRow.PRESTADOR, 0);
                    asignaAtributo('txtPrestadorObservacionesEdit', datosRow.OBSERVACIONES, 0);
                    mostrar('divVentanitaEditTecnologias');
                    //alert('falta eliminar e institucion prestadora...mejor ventanita')

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'limpiarInputPaciente':
            limpiaAtributo('txtIdPaciente', 0);
            limpiaAtributo('txtIdentificacion', 0);
            limpiaAtributo('txtApellido1', 0);
            limpiaAtributo('txtApellido2', 0);
            limpiaAtributo('txtNombre1', 0);
            limpiaAtributo('txtNombre2', 0);
            limpiaAtributo('txtIngreso', 0);
            break;
    }
}


function modificarCRUDRutas(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
        switch (arg) {

            case 'modificarCitaListaEsperaRuta':
                if (true) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=892&parametros=";
                    add_valores_a_mandar(valorAtributo('txtIdAgenda'));
                    add_valores_a_mandar(valorAtributo('txtIdListaEspera'));
                    add_valores_a_mandar(valorAtributo('txtFechaCita') + ' ' + valorAtributo('cmbHoraCita') + ':' + valorAtributo('cmbMinutoCita') + ' ' + valorAtributo('cmbPeriodoCita'));
                    add_valores_a_mandar(valorAtributo('txtIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('txtIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsultaClase'));
                    ajaxModificar();
                }
                break;

            case 'eliminarPacienteRuta':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=895&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdRutaSeleccionada'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                add_valores_a_mandar(valorAtributo('lblIdRutaSeleccionada'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                ajaxModificar();
                break;

            case 'notificacionManual':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=867&parametros=";
                    add_valores_a_mandar(valorAtributo('txtContenidoNotificacionManual'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    ajaxModificar();
                }
                break;

            case 'notificacionManualDesdeFarmacia': // con base en el id de admision
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=880&parametros=";
                    add_valores_a_mandar(valorAtributo('txtContenidoNotificacionManual'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('cmbIdAdmision'));
                    alert('SE ENVIARA AL PACIENTE COMO MENSAJE')
                    ajaxModificar();
                }
                break;

            case 'celular1':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=866&parametros=";
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    ajaxModificar();
                }
                break;

            case 'guardarCitaDesdeLE':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    if (valorAtributo('lblIdAgendaDetalle') == 0) {
                        valores_a_mandar = valores_a_mandar + "&idQuery=862&parametros="; /*crea en estado asignado*/
                        add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                        add_valores_a_mandar(valorAtributo('txtFechaCita') + ' ' + valorAtributo('cmbHoraInicio') + ':' + valorAtributo('cmbMinutoInicio'));
                        add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                        add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                        add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                        add_valores_a_mandar(valorAtributo('txtFechaVigencia'));
                        add_valores_a_mandar(valorAtributo('txtObservacion'));
                        add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                        add_valores_a_mandar(valorAtributo('cmbIdEspecialidadLE'));
                        add_valores_a_mandar(IdSesion());
                        add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                        add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                    }
                    else {
                        valores_a_mandar = valores_a_mandar + "&idQuery=863&parametros="; /*crea en estado asignado*/
                        add_valores_a_mandar(valorAtributo('txtFechaCita') + ' ' + valorAtributo('cmbHoraInicio') + ':' + valorAtributo('cmbMinutoInicio'));
                        add_valores_a_mandar(valorAtributo('txtObservacion'));
                        add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                        add_valores_a_mandar(IdSesion());
                        add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                    }
                    ajaxModificar();
                }
                break;

            case 'eliminarTipoCitaARutaPaciente':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=860&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdRutaDelPaciente'));
                ajaxModificar();
                break;
            case 'modificarTipoCitaARutaPaciente':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=859&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidadEdit'));
                    add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidadEdit'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCitaEdit'));
                    add_valores_a_mandar(valorAtributo('cmbEsperarEdit'));
                    add_valores_a_mandar(valorAtributo('txtOrdenEdit'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignadoEdit'));

                    add_valores_a_mandar(valorAtributo('cmbNecesidadEdit'));
                    add_valores_a_mandar(valorAtributo('txtPrestadorObservacionesEdit'));
                    add_valores_a_mandar(valorAtributo('lblIdRutaDelPaciente'));
                    //alert(valores_a_mandar);
                    ajaxModificar();
                }
                break;
            case 'adicionarTipoCitaARutaPaciente':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=855&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdRutaSeleccionada'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                    add_valores_a_mandar(valorAtributo('cmbEsperar'));
                    add_valores_a_mandar(valorAtributo('txtOrden'));

                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignado'));
                    add_valores_a_mandar(valorAtributo('cmbNecesidad'));
                    add_valores_a_mandar(valorAtributo('txtPrestadorObservaciones'));
                    ajaxModificar();
                    limpiaAtributo('cmbIdEspecialidad', 0); $('#drag' + ventanaActual.num).find('#txtIdProcedimiento').attr('disabled', '');
                    limpiaAtributo('cmbIdSubEspecialidad', 0);
                    limpiaAtributo('cmbTipoCita', 0);
                    limpiaAtributo('cmbEsperar', 0);
                    limpiaAtributo('txtOrden', 0);
                    limpiaAtributo('txtPrestadorAsignado', 0);
                    limpiaAtributo('txtPrestadorObservaciones', 0);
                    limpiaAtributo('cmbNecesidad', 0);
                }
                break;
            case 'eliminarRutaTipoCita':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=854&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdParametr'));
                    ajaxModificar();
                }
                break;
            case 'adicionarRutaTipoCita':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=873&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                    add_valores_a_mandar(valorAtributo('cmbEsperar'));
                    add_valores_a_mandar(valorAtributo('txtOrden'));
                    add_valores_a_mandar(valorAtributo('cmbEstado'));
                    ajaxModificar();
                }
                break;
            case 'crearPacienteRuta':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=872&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbRuta'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignadoRuta'));
                    ajaxModificar();
                }
                break;
            case 'crearRuta':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=852&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));
                    add_valores_a_mandar(valorAtributo('txtNombre'));
                    add_valores_a_mandar(valorAtributo('txtObservaciones'));
                    add_valores_a_mandar(valorAtributo('txtDuracion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('cmbPlan'));
                    add_valores_a_mandar(valorAtributo('cmbCiclica'));
                    ajaxModificar();
                    quitarTitulo('txtId'); 
                }

                break;
            case 'modificaRuta':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=874&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('txtObservaciones'));
                    add_valores_a_mandar(valorAtributo('txtDuracion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('cmbPlan'));
                    add_valores_a_mandar(valorAtributo('cmbCiclica'));
                    add_valores_a_mandar(valorAtributo('txtId'));
                    ajaxModificar();
                }
                break;
        }

    }
}

function respuestaCRUDRutas(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
        switch (arg) {
            case 'modificarCitaListaEsperaRuta':
                ocultar('divVentanitaEditarCita')
                //mostrar('divVentanitaEventosPaciente')
                buscarRutas('listGrillaListaEsperaRuta')
                break;
            case 'notificacionManual':
                buscarRutas('listNotificacionesPaciente')
                break;
            case 'eliminarPacienteRuta':
                buscarRutas('listGrillaRutaPaciente');
                buscarRutas('listGrillaRutaDelPaciente');
                break;
            case 'celular1':
                buscarRutas('listGrillaCertificadoPaciente')
                break;
            case 'celular2':
                buscarRutas('listGrillaCertificadoPaciente')
                break;
            case 'email':
                buscarRutas('listGrillaCertificadoPaciente')
                break;

            case 'guardarCitaDesdeLE':
                buscarRutas('listagendarDesdeLERuta')
                ocultar('divVentanitaAgenda')
                break;

            case 'eliminarTipoCitaARutaPaciente':
                buscarRutas('listGrillaRutaDelPaciente');
                limpiaAtributo('cmbIdEspecialidadEdit')
                limpiaAtributo('cmbIdSubEspecialidadEdit')
                limpiaAtributo('cmbTipoCitaEdit')
                limpiaAtributo('cmbEsperarEdit')
                limpiaAtributo('txtOrdenEdit')
                ocultar('divVentanitaEditTecnologias')
                break;
            case 'modificarTipoCitaARutaPaciente':
                buscarRutas('listGrillaRutaDelPaciente');
                limpiaAtributo('cmbIdEspecialidadEdit')
                limpiaAtributo('cmbIdSubEspecialidadEdit')
                limpiaAtributo('cmbTipoCitaEdit')
                limpiaAtributo('cmbEsperarEdit')
                limpiaAtributo('txtOrdenEdit')
                ocultar('divVentanitaEditTecnologias')
                break;
            case 'adicionarTipoCitaARutaPaciente':
                buscarRutas('listGrillaRutaDelPaciente');
                limpiaAtributo('cmbIdEspecialidad', 0)
                limpiaAtributo('cmbIdSubEspecialidad', 0)
                limpiaAtributo('cmbTipoCita', 0)
                limpiaAtributo('cmbEsperar', 0)
                limpiaAtributo('txtOrden', 0)
                break;
            case 'eliminarRutaTipoCita':
                buscarRutas('listGrillaRutaTipoCita');
                limpiaAtributo('lblIdParametr')
                break;
            case 'adicionarRutaTipoCita':
                limpiarDivEditarJuan('limpiarParametrosRuta'); 
                buscarRutas('listGrillaRutaTipoCita');
                break;
            case 'crearPacienteRuta':
                limpiaAtributo('txtPrestadorAsignadoRuta',0);
                limpiaAtributo('cmbPlan',0);  
                limpiaAtributo('cmbRuta',0);
                buscarRutas('listGrillaRutaPaciente');
                setTimeout("buscarRutas('listGrillaRutaDelPaciente')", 400);


                break;
            case 'crearRuta':
                limpiaAtributo('txtId', 0);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('txtObservaciones', 0);
                limpiaAtributo('txtDuracion', 0);
                limpiaAtributo('cmbVigente', 0);
                limpiaAtributo('cmbPlan', 0);
                limpiaAtributo('cmbCiclica', 0);
                cmbReset('cmbVigente', 0);
                cmbReset('cmbCiclica', 0);
                buscarRutas('listGrillaRutas');
                break;
            case 'modificaRuta':
                buscarRutas('listGrillaRutas');
                break;
        }
    }
}

function cargarRutasAPartirPlan(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 587, valorAtributo(comboOrigen));
}









function buscarPacienteConIdentificacion(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }  //alert(unicode); // Para saber que codigo de tecla presiono , descomentar
    if (unicode == 13) {
        traerDatos('buscarIdentificacionPaciente')
    }
}

function traerDatos(opcion) {

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestatraerDatosPacient(opcion) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=" + opcion;

    switch (opcion) {
        case 'buscarIdentificacionPaciente':
            if (verificarCamposGuardar('buscarIdentificacionPaciente')) {
                valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoId');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');

            } else break;
            break;
    }
    varajaxInit.send(valores_a_mandar);

}
function respuestatraerDatosPacient(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {

                    switch (opcion) {
                        case 'buscarIdentificacionPaciente':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') {
                                    alert('PACIENTE NO EXISTE !!!')
                                } else {
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    buscarInformacionBasicaPaciente()

                                    setTimeout("cargarInformacionContactoPaciente()", 800)
                                }
                            }
                            break;
                    }

                } else {
                    alert("No se pudo ingresar la informaci�n");
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}


function cargarInformacionContactoPaciente() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/planAtencion/tabsContactosPaciente.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarInformacionContactoPaciente;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarInformacionContactoPaciente() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divDepositoContactosPaciente").innerHTML = varajaxMenu.responseText;
            $("#tabsContactosPacientes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
            setTimeout("TabActivoRutas('divNotificaciones')", 300);
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function TabActivoRutas(idTab) {

    tabActivo = idTab;
    switch (idTab) {
        case 'divNotificaciones':
            buscarRutas('listNotificacionesPaciente')
            break;
    }
}

function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}


