
function contenedorRutaPacienteHC() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/planAtencion/enrutarPacienteDetalle.jsp?', true);
    varajaxMenu.onreadystatechange = llenarcontenedorRutaPacienteHC;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcontenedorRutaPacienteHC() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaContenedorHojaRuta").innerHTML = varajaxMenu.responseText;
            asignaAtributo('txtPrestadorAsignadoRuta', IdEmpresa() + '-' + NombreEmpresa(), 0)
            asignaAtributo('txtPrestadorAsignado', IdEmpresa() + '-' + NombreEmpresa(), 0)
            buscarRutas('listGrillaRutaPacienteHC')
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function buscarRutas(arg, callback) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {

        case "listaOrdenesHistoriaClinicaLaboratorio": 
         valores_a_mandar = pag;
         valores_a_mandar = valores_a_mandar + "?idQuery=460&parametros="; 
         add_valores_a_mandar(valorAtributo('txtIdHCL'));
         
         $('#'+arg)
             .jqGrid({
                 url: valores_a_mandar,
                 datatype: "xml",
                 mtype: "GET",

                 colNames: ["#", "ID ORDEN","CUPS", "PROCEDIMIENTO", "CANTIDAD","GRADO NECESIDAD","CLASE","DIAGNOSTICO"], 
                 colModel: [
                   
                     { name: "C", index: "C", hidden: true }, 
                     { name: "ID", index: "ID", hidden: true },
                     { name: "CUPS", index: "CUPS", width: 20 },
                     { name: "PROCEDIMIENTO", index: "PROCEDIMIENTO", width: 40 },
                     { name: "CANTIDAD", index: "CANTIDAD", width: 10 },
                     { name: "GRADO_NECESIDAD", index: "GRADO_NECESIDAD", width: 20 },
                     { name: "CLASE", index: "CLASE", width: 20 },
                     { name: "DIAGNOSTICO", index: "DIAGNOSTICO", width: 20 }
                   ],
                 height: 100,
                 //width: ancho + 40,
                 autowidth: true ,
                 onSelectRow: function (rowid) {
                   
                     var datosRow = $('#'+arg).getRowData(rowid);
                 },
             });
         $("#drag" + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
         break;

        case "listaHistoriaClinicaLaboratorio": 
        // ancho =               $("#drag" + ventanaActual.num)                    .find("#divContenido")                    .width() - 50;
         valores_a_mandar = pag;
         valores_a_mandar = valores_a_mandar + "?idQuery=458&parametros="; 
         add_valores_a_mandar(IdEmpresa());
         
         $('#'+arg)
             .jqGrid({
                 url: valores_a_mandar,
                 datatype: "xml",
                 mtype: "GET",

                 colNames: ["#", "HISTORIA CLINICA","TIPO", "PACIENTE", "MEDICO","ESTADO","ID_ESTADO"], 
                 colModel: [
                   
                     { name: "C", index: "C", hidden: true }, 
                     { name: "ID", index: "ID", width: 15 },
                     { name: "TIPO", index: "TIPO", width: 20 },
                     { name: "PACIENTE", index: "PACIENTE", width: 40 },
                     { name: "MEDICO", index: "MEDICO", width: 40 },
                     { name: "ESTADO", index: "ESTADO", width: 15 },
                     { name: "ID_ESTADO", index: "ID_ESTADO", hidden: true },

                   ],
                 height: 250,
                 //width: ancho + 40,
                 autowidth: true ,
                 onSelectRow: function (rowid) {
                   
                     var datosRow = $('#'+arg).getRowData(rowid);
                     asignaAtributo("txtIdHCL", datosRow.ID, 0);
                     asignaAtributo("txtIdHCLE", datosRow.ID_ESTADO, 0);
                     buscarRutas('listaOrdenesHistoriaClinicaLaboratorio')
                    
                 },
             });
         $("#drag" + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger("reloadGrid");
         break;


        case 'listPacienteEncuestas':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 10;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=914&parametros=";
            add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
            add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
            add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
            add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
            add_valores_a_mandar(valorAtributo('cmbIdEncuestasBus'));
            add_valores_a_mandar(valorAtributo('cmbIdEncuestasBus'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoEncuestasBus'));
            add_valores_a_mandar(valorAtributo('cmbIdEstadoEncuestasBus'));
            add_valores_a_mandar(valorAtributo('cmbIdRiesgoBus'));
            add_valores_a_mandar(valorAtributo('cmbIdRiesgoBus'));
            add_valores_a_mandar(valorAtributo('cmbIdMedioRecepcion'));
            add_valores_a_mandar(valorAtributo('cmbIdMedioRecepcion'));
            add_valores_a_mandar(valorAtributo('cmbIdAdmision'));
            add_valores_a_mandar(valorAtributo('cmbIdAdmision'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId', 'Identificacion', 'Nombre1', 'Nombre2', 'Apellido1', 'Apellido2',
                    'Fecha Nacimiento', 'edad','Sexo', 'Direccion', 'Municipio Residencia',
                    'celular1', 'celular2', 'celular3', 'email',
                    'Encuesta', 'id_riesgo', 'Riesgo', 'valor', 'Alerta', 'Tiemp cierre','Medio Recepcion','Admision'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 3) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 8) },
                    { name: 'Nombre1', index: 'Nombre1', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'Nombre2', index: 'Nombre2', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'Apellido1', index: 'Apellido1', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'Apellido2', index: 'Apellido2', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'FechaNacimiento', index: 'FechaNacimiento', hidden: true },
                    { name: 'edad', index: 'edad', hidden: true },
                    { name: 'Sexo', index: 'Sexo', hidden: true },
                    { name: 'Direccion', index: 'Direccion', hidden: true },
                    { name: 'MunicipioResi', index: 'MunicipioResi', hidden: true },
                    { name: 'celular1', index: 'celular1', hidden: true, align: 'left' },
                    { name: 'celular2', index: 'celular2', hidden: true, align: 'left' },
                    { name: 'celular3', index: 'celular3', hidden: true, align: 'left' },
                    { name: 'email', index: 'email', hidden: true },
                    { name: 'nom_encuesta', index: 'nom_encuesta', width: anchoP(ancho, 12) },
                    { name: 'id_riesgo', index: 'id_riesgo', hidden: true },
                    { name: 'nom_riesgo', index: 'nom_riesgo', width: anchoP(ancho, 10) },
                    { name: 'valor', index: 'valor', hidden: true },
                    { name: 'nomValor', index: 'nomValor', width: anchoP(ancho, 22) },
                    { name: 'tiempCierre', index: 'tiempCierre', width: anchoP(ancho, 15) },
                    { name: 'med_recepcion', index: 'med_recepcion', width: anchoP(ancho, 15) },
                    { name: 'Admision', index: 'Admision', align: 'center', width: anchoP(ancho, 8)},
                    
                ],
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {
                    mostrar('divEditar')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtIdBusPaciente', datosRow.id + "-" + 
                                                       datosRow.TipoId + "-" + 
                                                       datosRow.identificacion + "-" + 
                                                       datosRow.Apellido1 + " " +
                                                       datosRow.Apellido2 + " " +
                                                       datosRow.Nombre1 + " " +
                                                       datosRow.Nombre2 + " ", 0);
                    
                    setTimeout(() => {
                        buscarInformacionBasicaPaciente(); 
                        setTimeout(() => {
                            cargarInformacionContactoPaciente()
                        }, 200);
                    }, 200); 
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaListaEsperaRuta':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=891&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
            add_valores_a_mandar(valorAtributo('cmbTipoCita'));
            add_valores_a_mandar(valorAtributo('cmbConCita'));
            add_valores_a_mandar(valorAtributo('cmbConCita'));

            var estados = 'http://10.0.0.3:9010/combo/107';
            var motivos = ":Escoja un estado";
            var medicos = ":cargando";

            var editOptions = {
                editData: {
                    LoginSesion: LoginSesion(),
                    IdSesion: IdSesion(),
                    IdSede: IdSede()
                },
                width: 400,
                recreateForm: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                afterShowForm: function (formid) {
                    var id = $('#' + arg).jqGrid('getGridParam', 'selrow');
                    datosRow = $('#' + arg).getRowData(id);

                    var e = datosRow.ID_ESTADO == '' ? 'A' : datosRow.ID_ESTADO;

                    motivos = 'accionesXml/obtenerCombos.jsp?idQueryCombo=96&c1=' + e;
                    $.get(motivos, function (data) {
                        $('#MOTIVO').html($(data).html());
                        $('#MOTIVO').val(datosRow.ID_MOTIVO_CONSULTA);
                    });

                    $.get(estados, function (data) {
                        $('#ESTADO').html($(data).html());
                        $('#ESTADO').val(datosRow.ID_ESTADO);

                    });

                    medicos = 'accionesXml/obtenerCombos.jsp?idQueryCombo=101&c1=' + datosRow.ID_ESPECIALIDAD;
                    $.get(medicos, function (data) {
                        $('#MEDICO').html($(data).html());
                        $('#MEDICO').val(datosRow.ID_MEDICO);
                    });
                }, onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    var a = ret.ID_AGENDA_DETALLE === '' ? 'modificarCitaRuta' : 'actualizarCitaRuta';
                    var q = ret.ID_AGENDA_DETALLE === '' ? '892' : '280';
                    return {
                        txtIdPaciente: ret.ID_PACIENTE,
                        txtTipoCita: ret.ID_TIPO,
                        txtIdListaEspera: ret.ID_LISTA_ESPERA,
                        txtIdEspecialidad:ret.ID_ESPECIALIDAD,
                        txtIdCita: ret.ID_AGENDA_DETALLE,
                        accion: a,
                        idQuery: q
                    }
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                }
            };

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'RUTA', 'PROGRAMA', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_TIPO_CITA', 'TIPO CITA', 'ID_PACIENTE',
                    'IDENTIFICACION', 'NOMBRE', 'ID_CITA', 'FECHA CITA', 'HORA', 'ID_ESTADO', 'ESTADO', 'ID_MOTIVO_CONSULTA', 'MOTIVO', 
                    'ID_LISTA_ESPERA', 'ID_MEDICO', 'MEDICO', 'OBSERVACIONES','ID_AGENDA_DETALLE'],
                colModel: [
                    { name: 'C', index: 'C',  hidden: true },
                    { name: 'RUTA', index: 'RUTA', width: 15 },
                    { name: 'PROGRAMA', index: 'PROGRAMA', width: 15 },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    { name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: 15 },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    { name: 'TIPO', index: 'TIPO', width: 15 },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: 10 },
                    { name: 'NOMBRE', index: 'NOMBRE', width: 15 },
                    { name: 'ID_CITA', index: 'ID_CITA', hidden: true },
                    {
                        name: 'FECHA', index: 'FECHA', width: 8, editable: true, editrules: { required: true },
                        editoptions: {
                            dataInit: function (elem) {
                                $(elem).datepicker();
                            }
                        }
                    },
                    {
                        name: 'HORA', index: 'HORA', width: 8, editable: true, editrules: { required: true }
                        , editoptions: {
                            dataInit: function (elem) {
                                $(elem).timepicker({
                                    showPeriod: true,
                                    showLeadingZero: true
                                });
                            }
                        },
                    },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    {
                        name: 'ESTADO', index: 'ESTADO', width: 10, editable: true, edittype: "select", editrules: { required: true },
                        editoptions: {
                            dataUrl: estados,
                            dataEvents: [{
                                type: "change",
                                fn: function (elem) {
                                    var pl = $(elem.target).val();
                                    motivos = 'accionesXml/obtenerCombos.jsp?idQueryCombo=96&c1=' + pl;
                                    $.get(motivos, function (data) { $('#MOTIVO').html($(data).html()); });
                                }
                            }],
                            dataInit: function (elem) { $(elem).width(200); }
                        }
                    },
                    { name: 'ID_MOTIVO_CONSULTA', index: 'ID_MOTIVO_CONSULTA', hidden: true },
                    {
                        name: 'MOTIVO', index: 'MOTIVO', width: 10, editable: true, edittype: 'select',
                        editrules: { required: true }, editoptions: { value: motivos, dataInit: function (elem) { $(elem).width(200); } }
                    },
                    { name: 'ID_LISTA_ESPERA', index: 'ID_LISTA_ESPERA', hidden: true },
                    { name: 'ID_MEDICO', index: 'ID_MEDICO', hidden: true },
                    {
                        name: 'MEDICO', index: 'MEDICO', width: 10, editable: true, edittype: "select", editrules: { required: true },
                        editoptions: { value: medicos, dataInit: function (elem) { $(elem).width(200); } }
                    },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true, editable: true, edittype: "textarea", editrules: { edithidden: true } },
                    { name: 'ID_AGENDA_DETALLE', index: 'ID_AGENDA_DETALLE', hidden: true, editable: true },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdPaciente', datosRow.ID_PACIENTE, 0);
                    asignaAtributo('txtTipoCita', datosRow.ID_TIPO, 0);
                    asignaAtributo('txtIdListaEspera', datosRow.ID_LISTA_ESPERA, 0);
                    asignaAtributo('txtIdEspecialidad', datosRow.ID_ESPECIALIDAD, 0);

                    /*
                    
                    asignaAtributo('txtFechaCita', datosRow.Fecha, 0)
                    asignaAtributo('cmbHoraCita', datosRow.h, 0)
                    asignaAtributo('cmbMinutoCita', datosRow.m, 0)
                    asignaAtributo('cmbPeriodoCita', datosRow.d, 0)
                    asignaAtributo('cmbEstadoCita', datosRow.id_estado, 0);
                   */
                   // mostrar('divVentanitaEditarCita');
                },
                //autowidth: true,
                width: 1245,
                height: 520,
                pager: '#pager1',
                caption: "Listado Pacientes",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                rownumbers: true
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid("#pager1", {
                    del: false, add: false, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, {}, {});

            break;

        case 'listGrillaCertificadoPaciente':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=865&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'TipoId', 'Identificacion', 'Nombre1', 'Nombre2', 'Apellido1', 'Apellido2',
                    'Fecha Nacimiento', 'Sexo', 'Direccion', 'Municipio Residencia',
                    'celular1', 'certificado celular 1', 'celular2', 'certificado celular 2', 'celular3', 'email', 'certificado email'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'id', index: 'id', align: 'center', hidden: true },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 5) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 15) },
                    { name: 'Nombre1', index: 'Nombre1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Nombre2', index: 'Nombre2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Apellido1', index: 'Apellido1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'Apellido2', index: 'Apellido2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'FechaNacimiento', index: 'FechaNacimiento', hidden: true },
                    { name: 'Sexo', index: 'Sexo', hidden: true },
                    { name: 'Direccion', index: 'Direccion', hidden: true },
                    { name: 'MunicipioResi', index: 'MunicipioResi', hidden: true },
                    { name: 'celular1', index: 'celular1', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular1_certificado', index: 'celular1_certificado', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular2', index: 'celular2', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular2_certificado', index: 'celular2_certificado', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'celular3', index: 'celular3', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'email', index: 'email', hidden: true },
                    { name: 'email_certificado', index: 'email_certificado', width: anchoP(ancho, 20), align: 'left' },
                ],
                height: 250,
                width: ancho,
                onSelectRow: function (rowid) {
                    mostrar('divEditar')
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblId', datosRow.id, estad);
                    asignaAtributo('cmbTipoId', datosRow.TipoId, 1);
                    asignaAtributo('txtIdentificacion', datosRow.identificacion, 1);
                    asignaAtributo('txtNombre1', datosRow.Nombre1, estad);
                    asignaAtributo('txtNombre2', datosRow.Nombre2, estad);
                    asignaAtributo('txtApellido1', datosRow.Apellido1, estad);
                    asignaAtributo('txtApellido2', datosRow.Apellido2, estad);
                    asignaAtributo('cmbSexo', datosRow.Sexo, estad);
                    asignaAtributo('txtFechaNacimiento', datosRow.FechaNacimiento, estad);
                    asignaAtributo('txtCelular1', datosRow.celular1, estad);
                    asignaAtributo('txtCelular2', datosRow.celular2, estad);
                    asignaAtributo('txtTelefonos', datosRow.celular3, estad);
                    asignaAtributo('txtMunicipio', datosRow.MunicipioResi, estad);
                    asignaAtributo('txtDireccionRes', datosRow.Direccion, estad);
                    asignaAtributo('txtNomAcompanante', datosRow.Observaciones, estad);
                    asignaAtributo('lblIdAnteriores', datosRow.idAnteriores, estad);
                    asignaAtributo('txtEmail', datosRow.email, estad);
                    traerDatos('buscarIdentificacionPaciente');
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listNotificacionesPaciente':
            ancho = 1000; //($('#drag' + ventanaActual.num).find("#divNotificaciones").width()) - 10;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=864" + "&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID NOT', 'ASUNTO', 'CONTENIDO DEL MENSAJE', 'ESTADO NOT', 'TIPO', 'FECHA', 'ESTADO CITA', 'OBSERVACION CITA'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_NOT_ESCRITA', index: 'ID_NOT_ESCRITA', width: anchoP(ancho, 3) },
                    { name: 'ASUNTO', index: 'ASUNTO', width: anchoP(ancho, 5) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', width: anchoP(ancho, 35) },
                    { name: 'ESTADO_NOT', index: 'ESTADO_NOT', width: anchoP(ancho, 4) },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 4) },
                    { name: 'FECHA', index: 'FECHA', width: anchoP(ancho, 6) },
                    { name: 'ESTADO_CITA', index: 'ESTADO_CITA', width: anchoP(ancho, 4) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 6) }
                ],
                height: 250,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtId', datosRow.ID, 0)
                    asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                    asignaAtributo('txtObservaciones', datosRow.OBSERVACIONES, 0)
                    asignaAtributo('txtDuracion', datosRow.DURACION_DIAS, 0)
                    asignaAtributo('cmbCiclica', datosRow.CICLICA, 0)
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)
                    buscarRutas('listGrillaRutaTipoCita')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listagendarDesdeLERuta': //plan
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#divContenidoLETraer").width() - 50);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=861&parametros=";
            add_valores_a_mandar(valorAtributo('cmbBusSinCita'));
            add_valores_a_mandar(valorAtributo('cmbTipoCitaLE'));
            add_valores_a_mandar(valorAtributo('cmbEstadoLE'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignadoAgenda'));
            add_valores_a_mandar(valorAtributo('cmbIdEspecialidadLE'));
            add_valores_a_mandar(valorAtributo('cmbNecesidad'));
            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));
            add_valores_a_mandar(valorAtributo('cmbDiasEsperaLE'));

            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPacienteLE'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['Cuantos', 'ID LISTA', 'ID PACIENTE', 'TIPO ID', 'IDENTIFICACION', 'NOMBRE PACIENTE', 'FECHA_NACIMIENTO', 'EDAD',
                    'TELEFONO', 'ID_MUNICIPIO', 'MUNICIPIO', 'DIRECCION',
                    'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'PROFESIONAL', 'DIAS ESPERA', 'TIPO_CITA',
                    'NO_AUTORIZACION', 'FECHA_VIGENCIA', 'ID_GRADO_NECESIDAD', 'OBSERVACION', 'ID_ESTADO_LE', 'NOM_ESTADO_LE', 'ID_ELABORO', 'FECHA_ELABORO',
                    'CON CITA', 'ID_CITA', 'FECHA_CITA', 'HORA_CITA', 'HH_CITA', 'MIN_CITA', 'ID_ESTADO_CITA', 'NOM_ESTADO_CITA'
                ],
                colModel: [
                    { name: 'Cuantos', index: 'Cuantos', width: anchoP(ancho, 2) },
                    { name: 'ID_LISTA', index: 'ID_LISTA', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 2) },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: anchoP(ancho, 7) },
                    { name: 'NOMBRE_PACIENTE', index: 'NOMBRE_PACIENTE', width: anchoP(ancho, 20) },
                    { name: 'FECHA_NACIMIENTO', index: 'FECHA_NACIMIENTO', hidden: true },
                    { name: 'EDAD', index: 'EDAD', width: anchoP(ancho, 3) },
                    { name: 'TELEFONO', index: 'TELEFONO', hidden: true },
                    { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                    { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 10) },
                    { name: 'DIRECCION', index: 'DIRECCION', hidden: true },

                    { name: 'ID_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    { name: 'NOM_ESPECIALIDAD', index: 'NOM_ESPECIALIDAD', hidden: true },
                    { name: 'PROFESIONAL', index: 'PROFESIONAL', hidden: true },
                    { name: 'DIAS_ESPERA', index: 'DIAS_ESPERA', width: anchoP(ancho, 4) },
                    { name: 'TIPO_CITA', index: 'TIPO_CITA', hidden: true },
                    { name: 'NO_AUTORIZACION', index: 'NO_AUTORIZACION', hidden: true },
                    { name: 'FECHA_VIGENCIA', index: 'FECHA_VIGENCIA', hidden: true },
                    { name: 'ID_GRADO_NECESIDAD', index: 'ID_GRADO_NECESIDAD', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 15) },
                    { name: 'ID_ESTADO_LE', index: 'ID_ESTADO_LE', hidden: true },
                    { name: 'NOM_ESTADO_LE', index: 'NOM_ESTADO_LE', hidden: true },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', hidden: true },
                    { name: 'CON_CITA', index: 'ID_CITA', width: anchoP(ancho, 5) },
                    { name: 'ID_CITA', index: 'ID_CITA', hidden: true },
                    { name: 'FECHA_CITA', index: 'FECHA_CITA', width: anchoP(ancho, 5) },
                    { name: 'HORA_CITA', index: 'HORA_CITA', width: anchoP(ancho, 5) },
                    { name: 'HH_CITA', index: 'HH_CITA', hidden: true },
                    { name: 'MIN_CITA', index: 'MIN_CITA', hidden: true },
                    { name: 'ID_ESTADO_CITA', index: 'ID_ESTADO_CITA', hidden: true },
                    { name: 'NOM_ESTADO_CITA', index: 'NOM_ESTADO_CITA', width: anchoP(ancho, 10) }



                ],

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    //  if (confirm('TRAER DE LA LISTA DE ESPERA A UNA CITA?')) {

                    // if (datosRow.CON_CITA == 'NO') {
                    mostrar('divVentanitaAgenda')
                    asignaAtributo('lblIdListaEspera', datosRow.ID_LISTA, 0)
                    asignaAtributo('txtIdBusPaciente', concatenarCodigoNombre(datosRow.ID_PACIENTE, datosRow.NOMBRE_PACIENTE), 0)
                    asignaAtributo('cmbTipoCita', datosRow.TIPO_CITA, 0)
                    asignaAtributo('txtNoAutorizacion', datosRow.NO_AUTORIZACION, 0)
                    asignaAtributo('txtFechaVigencia', datosRow.FECHA_VIGENCIA, 0)


                    asignaAtributo('lblIdAgendaDetalle', datosRow.ID_CITA, 0)
                    asignaAtributo('txtFechaCita', datosRow.FECHA_CITA, 0)
                    asignaAtributo('cmbHoraInicio', datosRow.HH_CITA, 0)
                    asignaAtributo('cmbMinutoInicio', datosRow.MIN_CITA, 0)
                    asignaAtributo('cmbEstadoCita', datosRow.ID_ESTADO_CITA, 0)



                    ocultar('divVentanitaListaEspera');
                    limpiarDatosPacientePaciente();
                    buscarInformacionBasicaPaciente();


                    /*} else
                        alert('ESTA LISTA DE ESPERA YA TIENE ASIGNADO UNA CITA !!!')*/


                },
                height: 500,
                width: ancho,

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrillaRutas':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=868" + "&parametros=";
            add_valores_a_mandar(valorAtributo('cmbPlan'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));

            var editOptions = {
                editData: {
                    accion: "modificarRuta",
                    idQuery: "874"
                },
                /*beforeShowForm: function (formid) {
                    $('#ID', formid).attr("readonly", "readonly").addClass("ui-state-disabled");
                    return true;
                },*/
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#listGrillaRutas").jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#listGrillaRutas").jqGrid('getRowData', id);
                    return { idEdit: ret.ID }
                },
                recreateForm: true,
                closeAfterEdit: true
            };

            var addOptions = {
                editData: {
                    accion: "crearRuta",
                    idQuery: "852",
                    cmbPlan: valorAtributo('cmbPlan')
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    // var id = jQuery("#listGrillaRutas").jqGrid('getGridParam', 'selrow');
                    //var ret = jQuery("#listGrillaRutas").jqGrid('getRowData', id);
                    return { cmbPlan: valorAtributo('cmbPlan') }
                },
                recreateForm: true,
                closeAfterAdd: true
            };

            var delOptions = {
                delData: {
                    accion: "eliminarRuta",
                    idQuery: "186"
                }, afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#listGrillaRutas").jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#listGrillaRutas").jqGrid('getRowData', id);
                    return { idDel: ret.ID }
                }
            };

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID', 'NOMBRE', 'OBSERVACIONES', 'DURACION (DIAS)', 'EDAD INICIO MESES', 'EDAD INICIO AÑOS',
                    'EDAD FIN MESES', 'EDAD FIN AÑOS', 'EDAD INICIO', 'EDAD FIN','SEXO PACIENTE','SEXO',
                    'CICLICA', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: 20, editrules: { required: true }, editable: true },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', width: 30, editable: true, edittype: "textarea" },
                    { name: 'DURACION_DIAS', index: 'DURACION_DIAS', width: 10, editrules: { required: true, number: true }, editable: true },
                    { name: 'EDAD_INICIO_MES', index: 'EDAD_INICIO_MES', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 11 } },
                    { name: 'EDAD_INICIO_ANIO', index: 'EDAD_INICIO_ANIO', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 120 } },
                    { name: 'EDAD_FIN_MES', index: 'EDAD_FIN_MES', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 11 } },
                    { name: 'EDAD_FIN_ANIO', index: 'EDAD_FIN_ANIO', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 120 } },
                    { name: 'EDAD_INICIO', index: 'EDAD_INICIO', width: 15 },
                    { name: 'EDAD_FIN', index: 'EDAD_FIN', width: 15 },
                    { name: 'ID_SEXO', index: 'ID_SEXO', hidden: true,editable: true,editrules: { edithidden: true,required: true }, edittype: "select", editoptions: { value: ":;F:FEMENINO;M:MASCULINO;A:AMBOS" } },
                    { name: 'SEXO', index: 'SEXO', width: 15 },
                    { name: 'CICLICA', index: 'CICLICA', width: 6, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "S:SI;N:NO" } },
                    { name: 'VIGENTE', index: 'VIGENTE', width: 6, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "S:SI;N:NO" } },
                ],
                height: 320,
                autowidth: true,
                caption: "Programas",
                pager: "#pager1",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdRuta', datosRow.ID, 0);
                    buscarRutas('listGrillaRutaTipoCita');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid("#pager1", {
                    del: true, add: true, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar", deltext: "Eliminar", addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, addOptions, delOptions);
            break;

        case 'listGrillaRutaTipoCita':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=869&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdRuta'));

            console.log("EEEE" + IdSede())

            var especialidad = 'planAtencion/obtenerCombos.jsp?idQueryCombo=849&c1=' + IdSede();
            var tipoCita = ":Seleccione especialidad";
            var dias = ":;0:0 Dias;15:15 dias;30:30 dias (1 mes);60:60 dias (2 meses);90:90 dias (3 meses);120:120 dias(4 meses);150:150 dias(5 meses);180:180 dias(6 meses);210:210 dias(7 meses);240:240 dias(8 meses);270:270 dias(9 meses);300:300 dias(10 meses);330:330 dias(11 meses);365:365 dias(12 meses)";

            var addOptions = {
                editData: {
                    accion: "crearRutaTipoCita",
                    idQuery: "873"
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    return { txtIdRuta: valorAtributo('txtIdRuta') }
                },
                recreateForm: true,
                closeAfterAdd: true,
                width: 400
            };

            var editOptions = {
                editData: {
                    accion: "modificarRutaTipoCita",
                    idQuery: "187"
                },
                recreateForm: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                width: 400,
                afterShowForm: function (formid) {
                    var id = $('#' + arg).jqGrid('getGridParam', 'selrow');
                    datosRow = $('#' + arg).getRowData(id);

                    tipoCita = 'planAtencion/obtenerCombos.jsp?idQueryCombo=551&c1=' + datosRow.ID_ESPECIALIDAD;
                    $.get(tipoCita, function (data) {
                        $('#TIPO').html($(data).html());
                        $('#TIPO').val(datosRow.ID_TIPO);
                    });

                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    return { txtIdParametro: ret.ID_PARAMETRO }
                }
            };

            var delOptions = {
                delData: {
                    accion: "eliminarRutaParametro",
                    idQuery: "854"
                }, afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    return { idDel: ret.ID_PARAMETRO }
                }
            };

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_PARAMETRO', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_TIPO', 'TIPO', 'DIAS ESPERA', 'ORDEN', 'VIGENTE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_PARAMETRO', index: 'ID_PARAMETRO', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    {
                        name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: 100, editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: especialidad,
                            dataEvents: [{
                                type: "change",
                                fn: function (elem) {
                                    var es = $(elem.target).val();
                                    tipoCita = 'planAtencion/obtenerCombos.jsp?idQueryCombo=551&c1=' + es;
                                    $.get(tipoCita, function (data) { $('#TIPO').html($(data).html()); });
                                }
                            }],
                            dataInit: function (elem) { $(elem).width(250); }
                        }
                    },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    {
                        name: 'TIPO', index: 'TIPO', width: 100, editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: tipoCita,
                            dataInit: function (elem) { $(elem).width(250); }
                        }
                    },
                    { name: 'DIAS_ESPERA', index: 'ESPERA', width: 20, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: dias } },
                    { name: 'ORDEN', index: 'ORDEN', width: 20, editrules: { required: true, number: true }, editable: true },
                    { name: 'VIGENTE', index: 'VIGENTE', width: 20, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "S:SI;N:NO" } }
                ],
                height: "170",
                autowidth: true,
                caption: "Tecnologias/Citas",
                pager: "#pager2",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                onSelectRow: function (rowid) {
                    //var datosRow = jQuery('#' + arg).getRowData(rowid);
                    //asignaAtributo('txtIdRuta', datosRow.ID_PARAMETRO, 0);
                }
            });

            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid("#pager2", {
                    alerttop: $('#' + arg).offset().top, alertleft: $('#' + arg).offset().left,
                    del: true, add: true, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar", deltext: "Eliminar", addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, addOptions, delOptions);

            break;

        case'listGrillaRutaPacienteHC':
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=870&parametros=";
        add_valores_a_mandar(valorAtributo('cmbRuta'));
        add_valores_a_mandar(valorAtributo('cmbRuta'));
        add_valores_a_mandar(valorAtributo('cmbPlan'));
        add_valores_a_mandar(valorAtributo("lblIdPaciente"));

        $("#" + arg).jqGrid('GridUnload');
        $("#" + arg).jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['c', 'ID', 'ID PACIENTE', 'IDENTIFICACION', 'PACIENTE', 'EDAD (A&#209OS)', 'ID_PLAN', 'RUTA', 'ID_RUTA', 'PROGRAMA', 'ID_EMPRESA', 'PRESTADOR', 'FECHA DE INGRESO', '% GESTIONADO', 'OBSERVACIONES'],
            colModel: [

                { name: 'c', index: 'c', hidden: true },
                { name: 'ID', index: 'ID', hidden: true },
                {
                    name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true, editable: true, editrules: { edithidden: true },
                    editoptions: { dataInit: function (elem) { $(elem).width(250); } }
                },
                { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: 6 ,hidden:true},
                { name: 'PACIENTE', index: 'PACIENTE', width: 15,hidden:true },
                { name: 'EDAD', index: 'EDAD (A&#209OS)', width: 5 },
                { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                {
                    name: 'PLAN', index: 'PLAN', width: 15, editrules: { required: true }, editable: true, edittype: "select",
                    editoptions: {
                        dataUrl: planes, dataInit: function (elem) { $(elem).width(250); },
                        dataEvents: [{
                            type: "change",
                            fn: function (elem) {
                                var pl = $(elem.target).val();
                                rutas = 'accionesXml/obtenerCombos.jsp?idQueryCombo=587&c1=' + pl;
                                $.get(rutas, function (data) { $('#RUTA').html($(data).html()); });
                            }
                        }]
                    }
                },
                { name: 'ID_RUTA', index: 'ID_RUTA', hidden: true },
                {
                    name: 'RUTA', index: 'RUTA', width: 15, editrules: { required: true }, editable: true, edittype: "select",
                    editoptions: {
                        value: rutas, dataInit: function (elem) { $(elem).width(250); }
                    }
                },
                { name: 'ID_EMPRESA', index: 'ID_EMPRESA', hidden: true },
                {
                    name: 'EMPRESA', index: 'EMPRESA', width: 10,hidden:true, editrules: { required: true }, editable: true, edittype: "text",
                    editoptions: {
                        dataEvents: [{
                            type: "keypress",
                            fn: function (elem) {

                                $('#EMPRESA').trigger("focus");
                                $("#EMPRESA").autocomplete(jsonEmpresas, {
                                    autoFocus: true,
                                    multiple: false,
                                    matchContains: true,
                                    cacheLength: 1,
                                    width: 600,
                                    height: 600,
                                    deferRequestBy: 0,
                                    selSeparator: '|',
                                    minLength: 0,
                                    delay: 0,
                                    search: function (event, ui) {
                                        window.pageIndex = 0;
                                    }
                                });
                            }
                        }], dataInit: function (elem) { $(elem).width(250).val('1-COOEMSSANAR'); }
                    },
                },
                { name: 'FECHA_INGRESO', index: 'FECHA DE INGRESO', width: 10 },
                { name: 'GESTIONADO', index: '% GESTIONADO', width: 6 },
                { name: 'OBSERVACIONES', index: 'OBSERVACIONES',hidden:true, editable: true, edittype: "textarea", editrules: { edithidden: true } }
            ],
            height: 250,
            caption: "Pacientes Enrutados",
            pager: '#pager1',
            autowidth: true,
            editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
            onSelectRow: function (rowid) {
                var datosRow = $("#" + arg).getRowData(rowid);

                asignaAtributo('txtIdRutaPaciente', datosRow.ID, 0);
                asignaAtributo('txtIdPaciente', datosRow.ID_PACIENTE, 0);
                asignaAtributo('txtIdRuta', datosRow.ID_RUTA, 0);
                asignaAtributo('lblIdRutaSeleccionada', datosRow.ID, 0);
                /*asignaAtributo('cmbTipoId', datosRow.TIPO_ID, 0);
                asignaAtributo('txtIdentificacion', datosRow.IDENTIFICACION, 0);
                asignaAtributo('lblFechaIngreso', datosRow.FECHA_INGRESO, 0);
                asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                
                asignaAtributo('txtIdBusPaciente',
                    datosRow.ID_PACIENTE + "-" +
                    datosRow.TIPO_ID +
                    datosRow.IDENTIFICACION + " " +
                    datosRow.PACIENTE, 0);*/
                buscarRutas('listGrillaRutaDelPaciente');
            },
        });
        $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
            .navGrid("#pager1", {
                del: true, add: false, edit: true, view: true, refresh: true, search: false,
                edittext: "Editar", deltext: "Eliminar", viewtext: "Ver", refreshtext: "Refrescar"
            }, editOptions, addOptions, delOptions)
            .navButtonAdd('#pager1', {
                caption: "Adicionar",
                //recreateForm: true,
                // closeAfterAdd: true,
                buttonicon: "ui-icon-plus",
                onClickButton: function () {
                    //traerVentanita(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '7');
                    //jQuery("#"+arg).editGridRow( "new" );
                    mostrar('divVentanitaRuta');
                },
                position: "first"
            });
        break;    

        case 'listGrillaRutaPaciente':

            var planes = 'accionesXml/obtenerCombos.jsp?idQueryCombo=841';
            var rutas = ":Seleccione un plan";

            var empresas = 'http://10.0.0.3:9010/autocomplete/213';
            var jsonEmpresas = new Array();

            window.setTimeout(function () {
                $.get(empresas, function (data) {
                    jsonEmpresas = data;
                });
            }, 500);


            var addOptions = {
                recreateForm: true,
                closeAfterAdd: true,
                width: 500
            };
            var editOptions = {
                editData: {
                    accion: "modificarPacienteRuta",
                    idQuery: "256"
                },
                zIndex: 90,
                width: 500,
                recreateForm: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                afterShowForm: function (formid) {
                    var g = $("#" + arg);
                    var myInfo = '<div class="ui-state-highlight ui-corner-all">' +
                        '<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>' +
                        '<strong>Atencion:</strong><br/>' +
                        'Si edita la ruta del paciente se perderan todas sus citas/listas de espera creadas' +
                        '</div>',
                        infoTR = $("table#TblGrid_" + g[0].id + ">tbody>tr.tinfo"),
                        infoTD = infoTR.children("td.topinfo");
                    infoTD.html(myInfo);
                    infoTR.show();

                    var id = $('#' + arg).jqGrid('getGridParam', 'selrow');
                    datosRow = $('#' + arg).getRowData(id);

                    rutas = 'accionesXml/obtenerCombos.jsp?idQueryCombo=587&c1=' + datosRow.ID_PLAN;
                    $.get(rutas, function (data) {
                        $('#RUTA').html($(data).html());
                        $('#RUTA').val(datosRow.ID_RUTA);
                    });

                },
                beforeShowForm: function (formid) {
                    $('#ID_PACIENTE', formid).attr("readonly", "readonly").addClass("ui-state-disabled");
                    return true;
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    return { txtIdRutaPaciente: ret.ID }
                }
            };
            var delOptions = {
                delData: {
                    accion: "eliminarPacienteRuta",
                    idQuery: "895"
                }, afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    return { txtIdRutaPaciente: ret.ID }
                }
            };

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=870&parametros=";
            add_valores_a_mandar(valorAtributo('cmbRuta'));
            add_valores_a_mandar(valorAtributo('cmbRuta'));
            add_valores_a_mandar(valorAtributo('cmbPlan'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));

            $("#" + arg).jqGrid('GridUnload');
            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['c', 'ID', 'ID PACIENTE', 'IDENTIFICACION', 'PACIENTE', 'EDAD (A&#209OS)', 'ID_PLAN', 'RUTA', 'ID_RUTA', 'PROGRAMA', 'ID_EMPRESA', 'PRESTADOR', 'FECHA DE INGRESO', '% GESTIONADO', 'OBSERVACIONES'],
                colModel: [

                    { name: 'c', index: 'c', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    {
                        name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true, editable: true, editrules: { edithidden: true },
                        editoptions: { dataInit: function (elem) { $(elem).width(250); } }
                    },
                    { name: 'IDENTIFICACION', index: 'IDENTIFICACION', width: 6 },
                    { name: 'PACIENTE', index: 'PACIENTE', width: 15 },
                    { name: 'EDAD', index: 'EDAD (A&#209OS)', width: 5 },
                    { name: 'ID_PLAN', index: 'ID_PLAN', hidden: true },
                    {
                        name: 'PLAN', index: 'PLAN', width: 15, editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: planes, dataInit: function (elem) { $(elem).width(250); },
                            dataEvents: [{
                                type: "change",
                                fn: function (elem) {
                                    var pl = $(elem.target).val();
                                    rutas = 'accionesXml/obtenerCombos.jsp?idQueryCombo=587&c1=' + pl;
                                    $.get(rutas, function (data) { $('#RUTA').html($(data).html()); });
                                }
                            }]
                        }
                    },
                    { name: 'ID_RUTA', index: 'ID_RUTA', hidden: true },
                    {
                        name: 'RUTA', index: 'RUTA', width: 15, editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: rutas, dataInit: function (elem) { $(elem).width(250); }
                        }
                    },
                    { name: 'ID_EMPRESA', index: 'ID_EMPRESA', hidden: true },
                    {
                        name: 'EMPRESA', index: 'EMPRESA', width: 10, editrules: { required: true }, editable: true, edittype: "text",
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {

                                    $('#EMPRESA').trigger("focus");
                                    $("#EMPRESA").autocomplete(jsonEmpresas, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }], dataInit: function (elem) { $(elem).width(250).val('1-COOEMSSANAR'); }
                        },
                    },
                    { name: 'FECHA_INGRESO', index: 'FECHA DE INGRESO', width: 10 },
                    { name: 'GESTIONADO', index: '% GESTIONADO', width: 6 },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true, editable: true, edittype: "textarea", editrules: { edithidden: true } }
                ],
                height: 250,
                caption: "Pacientes Enrutados",
                pager: '#pager1',
                autowidth: true,
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                onSelectRow: function (rowid) {
                    var datosRow = $("#" + arg).getRowData(rowid);

                    asignaAtributo('txtIdRutaPaciente', datosRow.ID, 0);
                    asignaAtributo('txtIdPaciente', datosRow.ID_PACIENTE, 0);
                    asignaAtributo('txtIdRuta', datosRow.ID_RUTA, 0);
                    /*asignaAtributo('cmbTipoId', datosRow.TIPO_ID, 0);
                    asignaAtributo('txtIdentificacion', datosRow.IDENTIFICACION, 0);
                    asignaAtributo('lblFechaIngreso', datosRow.FECHA_INGRESO, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                    
                    asignaAtributo('txtIdBusPaciente',
                        datosRow.ID_PACIENTE + "-" +
                        datosRow.TIPO_ID +
                        datosRow.IDENTIFICACION + " " +
                        datosRow.PACIENTE, 0);*/
                    buscarRutas('listGrillaRutaDelPaciente');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid("#pager1", {
                    del: true, add: false, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar", deltext: "Eliminar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, addOptions, delOptions)
                .navButtonAdd('#pager1', {
                    caption: "Adicionar",
                    //recreateForm: true,
                    // closeAfterAdd: true,
                    buttonicon: "ui-icon-plus",
                    onClickButton: function () {
                        //traerVentanita(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '7');
                        //jQuery("#"+arg).editGridRow( "new" );
                        mostrar('divVentanitaRuta');
                    },
                    position: "first"
                });
            break;

        case 'listGrillaRutaDelPaciente':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=871&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdRutaPaciente'));

            var editOptions = {
                editData: {
                    accion: "modificarPacienteTecnologia",
                    idQuery: "859"
                },
                width: 450,
                recreateForm: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                afterShowForm: function (formid) {
                    var id = $('#' + arg).jqGrid('getGridParam', 'selrow');
                    datosRow = $('#' + arg).getRowData(id);

                    tipoCita = 'planAtencion/obtenerCombos.jsp?idQueryCombo=551&c1=' + datosRow.ID_ESPECIALIDAD;
                    $.get(tipoCita, function (data) {
                        $('#TIPO').html($(data).html());
                        $('#TIPO').val(datosRow.ID_TIPO);
                    });
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    return { txtIdTecnologiaPaciente: ret.ID }
                }
            };

            var addOptions = {
                editData: {
                    accion: "crearPacienteTecnologia",
                    idQuery: "855"
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                recreateForm: true,
                closeAfterAdd: true,
                width: 450,
                onclickSubmit: function (params) {
                    return {
                        txtIdRuta: valorAtributo('txtIdRuta'),
                        txtIdRutaPaciente: valorAtributo('txtIdRutaPaciente'),
                        txtIdPaciente: valorAtributo('txtIdPaciente')
                    }
                }
            };

            var delOptions = {
                delData: {
                    accion: "eliminarPacienteTecnologia",
                    idQuery: "860"
                }, afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    return { txtIdTecnologiaPaciente: ret.ID }
                }
            };

            console.log( "AAAA" + IdSede())
            var especialidad = 'planAtencion/obtenerCombos.jsp?idQueryCombo=849&c1=' + IdSede();
            var nesecidad = 'accionesXml/obtenerCombos.jsp?idQueryCombo=113';
            var tipoCita = ":Seleccione especialidad";
            var dias = ":;0:0 Dias;15:15 dias;30:30 dias (1 mes);60:60 dias (2 meses);90:90 dias (3 meses);120:120 dias(4 meses);150:150 dias(5 meses);180:180 dias(6 meses);210:210 dias(7 meses);240:240 dias(8 meses);270:270 dias(9 meses);300:300 dias(10 meses);330:330 dias(11 meses);365:365 dias(12 meses)";

            var empresas = 'accionesXml/obtenerAutocomplete.jsp?idQuery=213';
            var jsonEmpresas = new Array();

            window.setTimeout(function () {
                $.get(empresas, function (data) {
                    jsonEmpresas = JSON.parse(data.trim());
                });
            }, 500);


            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['c', 'ID', 'ID_ESPECIALIDAD', 'ESPECIALIDAD', 'ID_TIPO_CITA', 'TIPO CITA', 'SEMAFORO','ESTADO', 'DIAS ESPERA',
                    'ORDEN', 'ID_GRADO_NECESIDAD', 'GRADO NECESIDAD', 'PRESTADOR ASIGNADO', 'OBSERVACIONES', 'FECHA LIMITE CITA', 'FECHA_CITA'],
                colModel: [
                    { name: 'c', index: 'c', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_ESPECIALIDAD', index: 'ID_ESPECIALIDAD', hidden: true },
                    {
                        name: 'ESPECIALIDAD', index: 'ESPECIALIDAD', width: 20, editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: especialidad,
                            dataEvents: [{
                                type: "change",
                                fn: function (elem) {
                                    var es = $(elem.target).val();
                                    tipoCita = 'planAtencion/obtenerCombos.jsp?idQueryCombo=551&c1=' + es;
                                    $.get(tipoCita, function (data) { $('#TIPO').html($(data).html()); });
                                }
                            }],
                            dataInit: function (elem) { $(elem).width(250); }
                        }
                    },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    {
                        name: 'TIPO', index: 'TIPO', width: 20, editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: {
                            value: tipoCita,
                            dataInit: function (elem) { $(elem).width(250); }
                        }
                    },
                    { name: 'SEMAFORO', index: 'SEMAFORO', hidden: true},
                    { name: 'ESTADO', index: 'ESTADO', width: 10 },
                    { name: 'DIAS_ESPERA', index: 'DIAS_ESPERA', width: 8, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: dias } },
                    { name: 'ORDEN', index: 'ORDEN', width: 4, editrules: { required: true, number: true }, editable: true },
                    { name: 'ID_GRADO_NECESIDAD', index: 'ID_GRADO_NECESIDAD', hidden: true },
                    {
                        name: 'GRADO_NECESIDAD', index: 'GRADO_NECESIDAD', width: 10, editrules: { required: true }, editable: true, edittype: "select",
                        editoptions: {
                            dataUrl: nesecidad
                        }
                    },
                    {
                        name: 'EMPRESA', index: 'EMPRESA', width: 15, editrules: { required: true }, editable: true, edittype: "text",
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {

                                    $('#EMPRESA').trigger("focus");
                                    $("#EMPRESA").autocomplete(jsonEmpresas, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 600,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }], dataInit: function (elem) { $(elem).width(250).val('1-COOEMSSANAR'); }
                        }
                    },
                    { name: 'OBSERVACIONES', index: 'OBSERVACIONES', hidden: true, editable: true, edittype: "textarea", editrules: { edithidden: true } },
                    { name: 'FECHA_LIMITE', index: 'FECHA_LIMITE', width: 10 },
                    { name: 'FECHA_CITA', index: 'FECHA_CITA', width: 10 }
                ],
                height: 250,
                caption: "tecnologias",
                //autowidth: true,
                width: 1192.5,
                pager: '#pager2',
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
        
                afterInsertRow: function (rowid, aData) {
                    switch (aData.SEMAFORO) {
                        case 'Esperando cita':
                            jQuery('#' + arg).jqGrid('setCell', rowid, 'ESTADO', '', { color: 'black' });
                            break;
                        case 'Cita asignada':
                            jQuery('#' + arg).jqGrid('setCell', rowid, 'ESTADO', '', { color: 'blue' });
                            break;
                        case 'Cita vencida':
                            jQuery('#' + arg).jqGrid('setCell', rowid, 'ESTADO', '', { color: 'red' });
                            break;
                            case 'Cita confirmada':
                                jQuery('#' + arg).jqGrid('setCell', rowid, 'ESTADO', '', { color: 'green' });
                                break;

                    }
                },
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid)
                    mostrar('divVentanitaEditTecnologias');
                }
            });
            $("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid("#pager2", {
                    del: true, add: true, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar", deltext: "Eliminar", addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, addOptions, delOptions);

            break;

        case 'limpiarInputPaciente':
            limpiaAtributo('txtIdPaciente', 0);
            limpiaAtributo('txtIdentificacion', 0);
            limpiaAtributo('txtApellido1', 0);
            limpiaAtributo('txtApellido2', 0);
            limpiaAtributo('txtNombre1', 0);
            limpiaAtributo('txtNombre2', 0);
            limpiaAtributo('txtIngreso', 0);
            break;
    }
}

function datoActual(arg) {
    var c = $('#drag' + ventanaActual.num).find("#" + arg).jqGrid('getGridParam', 'selrow');
    console.log(c);
    return c;
}

function modificarCRUDRutas(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
        switch (arg) {

            case 'finalizarHistoriaClinicaLaboratorio':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=506&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdHCL'));
                add_valores_a_mandar(valorAtributo('txtIdHCL'));
                add_valores_a_mandar(valorAtributo('txtIdHCL'));
                add_valores_a_mandar(valorAtributo('txtIdHCL'));
                ajaxModificar();
            break;


            case 'crearProcedimientoHistoriaClinicaLaboratorio':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=459&parametros=";
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoLaboratorio'));
                add_valores_a_mandar(valorAtributo('cmbCantidadLaboratorio'));
                add_valores_a_mandar(valorAtributo('txtIdHCL'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoLaboratorio'));
                  add_valores_a_mandar(IdEmpresa());
                ajaxModificar();

            break;

            case 'crearHistoriaClinicaLaboratorio':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=457&parametros=";
                add_valores_a_mandar(IdEmpresa());
                ajaxModificar();
            break;

            case 'modificarCitaListaEsperaRuta':
                if (true) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=892&parametros=";
                    add_valores_a_mandar(valorAtributo('txtIdAgenda'));
                    add_valores_a_mandar(valorAtributo('txtIdListaEspera'));
                    add_valores_a_mandar(valorAtributo('txtFechaCita') + ' ' + valorAtributo('cmbHoraCita') + ':' + valorAtributo('cmbMinutoCita') + ' ' + valorAtributo('cmbPeriodoCita'));
                    add_valores_a_mandar(valorAtributo('txtIdEspecialidad'));
                    //add_valores_a_mandar(valorAtributo('txtIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                    add_valores_a_mandar(valorAtributo('cmbMotivoConsultaClase'));
                    ajaxModificar();
                }
                break;

            case 'eliminarPacienteRuta':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=895&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdRutaSeleccionada'));                
                add_valores_a_mandar(valorAtributo('lblIdRutaSeleccionada'));                
                ajaxModificar();
                break;

            case 'notificacionManual':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=867&parametros=";
                    add_valores_a_mandar(valorAtributo('txtContenidoNotificacionManual'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    ajaxModificar();
                }
                break;

            case 'notificacionManualDesdeFarmacia': // con base en el id de admision
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=880&parametros=";
                    add_valores_a_mandar(valorAtributo('txtContenidoNotificacionManual'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('cmbIdAdmision'));
                    alert('SE ENVIARA AL PACIENTE COMO MENSAJE')
                    ajaxModificar();
                }
                break;

            case 'celular1':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=866&parametros=";
                    add_valores_a_mandar(valorAtributo('txtCelular1'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    ajaxModificar();
                }
                break;

            case 'guardarCitaDesdeLE':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    if (valorAtributo('lblIdAgendaDetalle') == 0) {
                        valores_a_mandar = valores_a_mandar + "&idQuery=862&parametros="; /*crea en estado asignado*/
                        add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                        add_valores_a_mandar(valorAtributo('txtFechaCita') + ' ' + valorAtributo('cmbHoraInicio') + ':' + valorAtributo('cmbMinutoInicio'));
                        add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                        add_valores_a_mandar(valorAtributo('cmbMotivoConsulta'));
                        add_valores_a_mandar(valorAtributo('txtNoAutorizacion'));
                        add_valores_a_mandar(valorAtributo('txtFechaVigencia'));
                        add_valores_a_mandar(valorAtributo('txtObservacion'));
                        add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                        add_valores_a_mandar(valorAtributo('cmbIdEspecialidadLE'));
                        add_valores_a_mandar(IdSesion());
                        add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                        add_valores_a_mandar(valorAtributo('lblIdListaEspera'));
                    }
                    else {
                        valores_a_mandar = valores_a_mandar + "&idQuery=863&parametros="; /*crea en estado asignado*/
                        add_valores_a_mandar(valorAtributo('txtFechaCita') + ' ' + valorAtributo('cmbHoraInicio') + ':' + valorAtributo('cmbMinutoInicio'));
                        add_valores_a_mandar(valorAtributo('txtObservacion'));
                        add_valores_a_mandar(valorAtributo('cmbEstadoCita'));
                        add_valores_a_mandar(IdSesion());
                        add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));
                    }
                    ajaxModificar();
                }
                break;

            case 'eliminarTipoCitaARutaPaciente':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=860&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdRutaDelPaciente'));
                ajaxModificar();
                break;
            case 'modificarTipoCitaARutaPaciente':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=859&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    // add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidadEdit'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCitaEdit'));
                    add_valores_a_mandar(valorAtributo('cmbEsperarEdit'));
                    add_valores_a_mandar(valorAtributo('txtOrdenEdit'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignadoEdit'));
                    add_valores_a_mandar(valorAtributo('cmbNecesidadEdit'));
                    add_valores_a_mandar(valorAtributo('txtPrestadorObservacionesEdit'));
                    add_valores_a_mandar(valorAtributo('lblIdRutaDelPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbNumCitas'));
                    add_valores_a_mandar(valorAtributo('cmbFrecuencia'));
                    add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                    //alert(valores_a_mandar);
                    ajaxModificar();
                }
                break;
            case 'adicionarTipoCitaARutaPaciente':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=855&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdRutaSeleccionada'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    //add_valores_a_mandar(valorAtributo('cmbIdSubEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                    add_valores_a_mandar(valorAtributo('cmbEsperar'));
                    add_valores_a_mandar(valorAtributo('txtOrden'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignado'));
                    add_valores_a_mandar(valorAtributo('cmbNecesidad'));
                    add_valores_a_mandar(valorAtributo('cmbNumCitas'));
                    add_valores_a_mandar(valorAtributo('cmbFrecuencia'));
                    add_valores_a_mandar(valorAtributo('cmbIdProfesionales'));
                    add_valores_a_mandar(valorAtributo('txtPrestadorObservaciones'));
                    ajaxModificar();
                    limpiaAtributo('cmbIdEspecialidad', 0); $('#drag' + ventanaActual.num).find('#txtIdProcedimiento').attr('disabled', '');
                    //limpiaAtributo('cmbIdSubEspecialidad', 0);
                    limpiaAtributo('cmbTipoCita', 0);
                    limpiaAtributo('cmbEsperar', 0);
                    limpiaAtributo('txtOrden', 0);
                    limpiaAtributo('txtPrestadorAsignado', 0);
                    limpiaAtributo('txtPrestadorObservaciones', 0);
                    limpiaAtributo('cmbNecesidad', 0);
                    limpiaAtributo('cmbNumCitas', 0);
                    limpiaAtributo('cmbFrecuencia', 0);
                    limpiaAtributo('txtPrestadorObservaciones', 0);
                }
                break;
            case 'eliminarRutaTipoCita':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=854&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdParametr'));
                    ajaxModificar();
                }
                break;
            case 'adicionarRutaTipoCita':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=873&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));
                    add_valores_a_mandar(valorAtributo('cmbIdEspecialidad'));
                    add_valores_a_mandar(valorAtributo('cmbTipoCita'));
                    add_valores_a_mandar(valorAtributo('cmbEsperar'));
                    add_valores_a_mandar(valorAtributo('txtOrden'));
                    add_valores_a_mandar(valorAtributo('cmbEstado'));
                    ajaxModificar();
                }
                break;
            case 'crearPacienteRuta':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=872&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbRuta'));
                    add_valores_a_mandar(valorAtributo("lblIdPaciente"));
                    add_valores_a_mandar('');
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtPrestadorAsignadoRuta'));
                    ajaxModificar();
                }
                break;
            case 'crearRuta':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=852&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));
                    add_valores_a_mandar(valorAtributo('txtNombre'));
                    add_valores_a_mandar(valorAtributo('txtObservaciones'));
                    add_valores_a_mandar(valorAtributo('txtDuracion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('cmbPlan'));
                    add_valores_a_mandar(valorAtributo('cmbCiclica'));
                    ajaxModificar();
                    quitarTitulo('txtId');
                }

                break;
            case 'modificaRuta':
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=874&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'))
                    add_valores_a_mandar(valorAtributo('txtObservaciones'));
                    add_valores_a_mandar(valorAtributo('txtDuracion'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('cmbPlan'));
                    add_valores_a_mandar(valorAtributo('cmbCiclica'));
                    add_valores_a_mandar(valorAtributo('txtId'));
                    ajaxModificar();
                }
                break;
        }

    }
}

function respuestaCRUDRutas(arg, xmlraiz) {

    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
        switch (arg) {

            case 'finalizarHistoriaClinicaLaboratorio':
                buscarRutas('listaHistoriaClinicaLaboratorio')
                limpiaAtributo('txtIdHCL',0)
                limpiaAtributo('txtIdHCLE',0)
            break;

            case 'crearProcedimientoHistoriaClinicaLaboratorio':
                limpiaAtributo('cmbCantidadLaboratorio',0)
                limpiaAtributo('txtIdProcedimientoLaboratorio',0)
                buscarRutas('listaOrdenesHistoriaClinicaLaboratorio')
            break;

          
            case 'crearHistoriaClinicaLaboratorio':
                buscarRutas('listaHistoriaClinicaLaboratorio')
            break;

            case 'modificarCitaListaEsperaRuta':
                ocultar('divVentanitaEditarCita')
                //mostrar('divVentanitaEventosPaciente')
                buscarRutas('listGrillaListaEsperaRuta')
                break;
            case 'notificacionManual':
                buscarRutas('listNotificacionesPaciente')
                break;
            case 'eliminarPacienteRuta':
                alert('PROGRAMA ELIMINADO CORRECTAMENTE');
                buscarRutas('listGrillaRutaPacienteHC');
                buscarRutas('listGrillaRutaDelPaciente');
                break;
            case 'celular1':
                buscarRutas('listGrillaCertificadoPaciente')
                break;
            case 'celular2':
                buscarRutas('listGrillaCertificadoPaciente')
                break;
            case 'email':
                buscarRutas('listGrillaCertificadoPaciente')
                break;

            case 'guardarCitaDesdeLE':
                buscarRutas('listagendarDesdeLERuta')
                ocultar('divVentanitaAgenda')
                break;

            case 'eliminarTipoCitaARutaPaciente':
                buscarRutas('listGrillaRutaDelPaciente');
                limpiaAtributo('cmbIdEspecialidadEdit')
                //limpiaAtributo('cmbIdSubEspecialidadEdit')
                limpiaAtributo('cmbTipoCitaEdit')
                limpiaAtributo('cmbEsperarEdit')
                limpiaAtributo('txtOrdenEdit')
                ocultar('divVentanitaEditTecnologias')
                break;
            case 'modificarTipoCitaARutaPaciente':
                buscarRutas('listGrillaRutaDelPaciente');
                limpiaAtributo('cmbIdEspecialidadEdit')
                //limpiaAtributo('cmbIdSubEspecialidadEdit')
                limpiaAtributo('cmbTipoCitaEdit')
                limpiaAtributo('cmbEsperarEdit')
                limpiaAtributo('txtOrdenEdit')
                ocultar('divVentanitaEditTecnologias')
                break;
            case 'adicionarTipoCitaARutaPaciente':
                buscarRutas('listGrillaRutaDelPaciente');
                limpiaAtributo('cmbIdEspecialidad', 0)
                //limpiaAtributo('cmbIdSubEspecialidad', 0)
                limpiaAtributo('cmbTipoCita', 0)
                limpiaAtributo('cmbEsperar', 0)
                limpiaAtributo('txtOrden', 0)
                break;
            case 'eliminarRutaTipoCita':
                buscarRutas('listGrillaRutaTipoCita');
                limpiaAtributo('lblIdParametr')
                break;
            case 'adicionarRutaTipoCita':
                limpiarDivEditarJuan('limpiarParametrosRuta');
                buscarRutas('listGrillaRutaTipoCita');
                break;
            case 'crearPacienteRuta':
                alert('PROGRAMA AGREGADO CORRECTAMENTE');                
                limpiaAtributo('cmbPlan', 0);
                limpiaAtributo('cmbRuta', 0);
                buscarRutas('listGrillaRutaPacienteHC');
                setTimeout("buscarRutas('listGrillaRutaDelPaciente')", 400);


                break;
            case 'crearRuta':
                limpiaAtributo('txtId', 0);
                limpiaAtributo('txtNombre', 0);
                limpiaAtributo('txtObservaciones', 0);
                limpiaAtributo('txtDuracion', 0);
                limpiaAtributo('cmbVigente', 0);
                limpiaAtributo('cmbPlan', 0);
                limpiaAtributo('cmbCiclica', 0);
                cmbReset('cmbVigente', 0);
                cmbReset('cmbCiclica', 0);
                buscarRutas('listGrillaRutas');
                break;
            case 'modificaRuta':
                buscarRutas('listGrillaRutas');
                break;
        }
    }
}

function cargarRutasAPartirPlan(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 587, valorAtributo(comboOrigen));
}









function buscarPacienteConIdentificacion(key) {
    var unicode
    if (key.charCode) { unicode = key.charCode; } else { unicode = key.keyCode; }  //alert(unicode); // Para saber que codigo de tecla presiono , descomentar
    if (unicode == 13) {
        traerDatos('buscarIdentificacionPaciente')
    }
}

function traerDatos(opcion) {

    varajaxInit = crearAjax();
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardarHc_xml.jsp', true);
    varajaxInit.onreadystatechange = function () { respuestatraerDatosPacient(opcion) };
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    valores_a_mandar = "accion=" + opcion;

    switch (opcion) {
        case 'buscarIdentificacionPaciente':
            if (verificarCamposGuardar('buscarIdentificacionPaciente')) {
                valores_a_mandar = valores_a_mandar + "&cmbTipoId=" + valorAtributo('cmbTipoId');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');
                valores_a_mandar = valores_a_mandar + "&txtIdentificacion=" + valorAtributo('txtIdentificacion');

            } else break;
            break;
    }
    varajaxInit.send(valores_a_mandar);

}
function respuestatraerDatosPacient(opcion) {

    if (varajaxInit.readyState == 4) {
        if (varajaxInit.status == 200) {
            raiz = varajaxInit.responseXML.documentElement;
            if (raiz.getElementsByTagName('raiz') != null) {
                if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {

                    switch (opcion) {
                        case 'buscarIdentificacionPaciente':
                            if (raiz.getElementsByTagName('existe')[0].firstChild.data != '') {
                                if (raiz.getElementsByTagName('existe')[0].firstChild.data == 'N') { 
                                    alert('PACIENTE NO EXISTE !!!')
                                } else {
                                    asignaAtributo('txtIdBusPaciente', raiz.getElementsByTagName('dato')[0].firstChild.data, 0)
                                    buscarInformacionBasicaPaciente()
                                    //setTimeout("cargarInformacionContactoPaciente()", 800)
                                }
                            }
                            break;
                    }

                } else {
                    alert("No se pudo ingresar la informaci�n");
                }
            }
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxInit.readyState == 1) {
        //abrirVentana(260, 100);
        //VentanaModal.setSombra(true);
    }
}


function cargarInformacionContactoPaciente() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", '/clinica/paginas/planAtencion/tabsContactosPaciente.jsp', true);
    varajaxMenu.onreadystatechange = llenarcargarInformacionContactoPaciente;
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarInformacionContactoPaciente() { 
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divDepositoContactosPaciente").innerHTML = varajaxMenu.responseText;
            $("#tabsContactosPacientes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
            setTimeout("TabActivoRutas('divEncuestas')", 300);
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function TabActivoRutas(idTab) {
    tabActivo = idTab;
    switch (idTab) {
        case 'divNotificaciones':
            buscarRutas('listNotificacionesPaciente')
            break;
        case 'divEncuestas':
            contenedorEncuesta()
            break;
    }
}


function respuestaModificarRutas(response) {


    var raiz = response.responseXML;
    MsgAlerta = raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data;

    if (raiz.getElementsByTagName('MsgAlerta')[0].firstChild.data == '') {
        if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {

            respuestaCRUDRutas(paginaActual, raiz);

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    else {
        alert('PROBLEMAS AL REGISTRAR\n' + MsgAlerta)
    }

    return MsgAlerta;



}

