 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
//    beanPersonal.setCn(beanSession.getCn());
    ArrayList resultaux=new ArrayList();
	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
%>

<table width="1000px" height="800px" align="center"   border="0" cellspacing="0" cellpadding="1" >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="Reportes" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

 	 <div id="divContenido" style="overflow:auto; width:100%; height:1000px";  >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
        <div id="divBuscar" style="display:block; width:100%"   >
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer" >
                <tr><td width="99%" class="titulosCentrados">BUSCAR
                </td></tr>
              </table>
 
              <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
                       <tr class="titulos" align="center">
                          <td>Tipo</td>
                          <td >Nombre Reporte</td>
                          <td >Procesos</td>                          
                        <td>Estado</td>                              
                       </tr>
                       <tr class="estiloImput">
                          <td>
							<select  id="cmbBusTipo" style="width:90%"   >	                                        
								 <option value="">[Todos]</option>                          
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(7);	
                                         ComboVO cmb7; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmb7=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmb7.getId()%>" title="<%= cmb7.getTitle()%>"><%=cmb7.getId()+'-'+ cmb7.getDescripcion()%></option>
                                  <%}%>						
                             </select>                                                                         
                          </td>
                          <td><input type="text" value=""  size="70"  maxlength="70"  id="txtBusNomReporte" name="txtBusNomReporte"  /> </td> 

                        <td align="center">
                          <select name="cmbBusProceso" size="1" id="cmbBusProceso" style="width:90%"   >	                                        
								 <option value="">[Todos]</option>                          
                                  <%     resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.combo.cargar(6);	
                                         ComboVO cmb32; 
                                         for(int k=0;k<resultaux.size();k++){ 
                                               cmb32=(ComboVO)resultaux.get(k);
                                  %>
                                <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>"><%= cmb32.getDescripcion()%></option>
                                  <%}%>						
                             </select>		
                        </td>                                
                        <td>
                           <input type="button" class="small button buscar" title="Buscar" value="   Buscar" onclick="buscarReportes('reportes','')"   />
                        </td> 
                      </tr>	                          
              </table>
              <div id="divListado" >
                  <table id="list_reportes" class="scroll"></table>  
              </div>

          </div>              
        <div id="divEditar" style="display:none; width:100%" >  
  
               <table width="100%"  cellpadding="0" cellspacing="0" >
                  <tr class="estiloImput">   
                      <td width="25%">Buscar desde Fecha:&nbsp;<input type="text" value=""  size="10"  maxlength="10"  id="txtBusFechaDesde"   /></td>
                      <td width="25%">Buscar hasta Fecha:&nbsp;<input type="text" value=""  size="10"  maxlength="10"  id="txtBusFechaHasta"  /></td>	
                      <td width="40%">
                      <center>
                       <form id="paraExcel" name="paraExcel" action="/clinica/reportes/generic_doc2.jsp"   method="post"> <!-- target="reporte_xls"> -->
                       <input type="hidden" id="datos" name="datos" />	<input type="hidden" id="archivo" name="archivo" value="listado" />
                       </form>	
                         <label id="lblGuardandoDoc" style="size:1PX"></label>
                         <input type="button" class="small button blue" value="GENERAR INFORME" title="btnp22" onclick="llenarTablaDeReporte()"/>&nbsp;&nbsp;
                         <input type="button" class="small button blue" value="VOLVER A REPORTES"  onclick="ocultar('divEditar');mostrar('divBuscar')"/> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;      
                           <img src="/clinica/utilidades/imagenes/acciones/icono_pdf.gif" width="18" height="18" style="cursor:pointer" title="Generar reporte Word" onclick="formatoPDFReport();"/>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;                                                                                                                   
                       </center>
                       </td>    
                       <td width="10%">   
                           <div id="REPORTE_EXCEL" style="display:none">
                              1.<img src="/clinica/utilidades/imagenes/acciones/excel.JPG" width="18" height="18" style="cursor:pointer"  onclick="javascript:formatoExcelCC('listado')"/>&nbsp;&nbsp;                           
                           </div>
                           <div id="REPORTE_EXCEL_IREPORT" style="display:none">                           
                              2.<img src="/clinica/utilidades/imagenes/acciones/excel.JPG" title="formatoExcelXLS IREPORT" width="18" height="18" style="cursor:pointer"  onclick="formatoXLSReport()"/>&nbsp;&nbsp;                           
                           </div>       
                       </td>                    
                  </tr>
                  <tr>
                    <td colspan="4" align="center">
                        <div id="listado"> 
                             <table width="100%" >
                                  <tr class="titulos"> 
                                    <td colspan="2" align="center">DETALLE DEL REPORTE</td>  
                                  </tr>	               
                                  <tr class="estiloImput"> 
                                    <td width="20%" align="right" >NOMBRE REPORTE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>  
                                    <td width="80%" align="left" ><label id="lblId"></label> - <label id="lblNombre"></label></td>                        
                                  </tr>
                                  <tr class="estiloImput"> 
                                    <td align="right" >EXPLICACION:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td align="left" ><label id="lblExplicacion"></label></td>                      
                                  </tr>                    	
                             </table>     
                             <table id="tablaReportes" class="grillaReporte"  width="70%" border="1">
                                 <tr><td >&nbsp;</td></tr>                      
                             </table> 
                        </div>  
                    </td>
                  </tr>
               </table>              
          </div><!-- divEditar-->	  
     </div><!-- div contenido-->
    
        </td>
       </tr>
      </table>
      <input type="hidden" id="IdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>"/>    
      <input type="hidden" id="CantColumnas" />          
   </td>
 </tr> 
 
</table>



</DIV>

















     

