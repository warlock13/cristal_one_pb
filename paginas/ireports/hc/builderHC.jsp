<%@page import="Sgh.Utilidades.ConnectionClinica"%>
<%@page import="JRBuilderHC.ReportBuilder"%>
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<%
    try {
        System.out.println("*** GENERAR REPORTE DE HISTORIA CLINICA VERSION 2.0 ***");
        
        int evo = Integer.parseInt(request.getParameter("evo"));
        String pdfFileName = request.getParameter("nombre");
        
        ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
        ReportBuilder builder = new ReportBuilder(evo, request.getServletContext(), iConnection.getConnection());
        builder.setQueryReportType(beanSession.getCn().traerElQuery(6011).toString());
        builder.setQueryReportsExtra(beanSession.getCn().traerElQuery(6012).toString());
        builder.setQueryReportsTemplate(beanSession.getCn().traerElQuery(6013).toString());
        byte[] bytes = builder.getPDF();

        response.addHeader("Content-Disposition", "inline; filename=" + pdfFileName);
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);

        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);

        ouputStream.flush();
        ouputStream.close();

    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>