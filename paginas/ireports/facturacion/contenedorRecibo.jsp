<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="net.sf.jasperreports.engine.*" %> 
<%@ page import="net.sf.jasperreports.engine.util.*" %>
<%@ page import="net.sf.jasperreports.engine.export.*" %>

<%@ page import="net.sf.jasperreports.view.JasperViewer" %>
<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->  
<% 	 
        System.out.println("-----------------IREPORTS-------------------------");	
        System.out.println("Conectado a: " + beanSession.cn.getServidor());
        System.out.println("Usuario a::: " +beanSession.usuario.getIdentificacion());		
		
		
        boolean estado;	
%>
<SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.js"></SCRIPT>
<link href="/clinica/utilidades/css/estiloClinica.css" rel="stylesheet" type="text/css">
<SCRIPT language=javascript>
    function numeraRecibo(frm) {
        
        //window.opener.modificarCRUD('numerarRecibo');
        numRecibo = document.getElementById('lblIdReciboVentana').innerHTML;
        cedula = document.getElementById('lblIdCedulaFacturador').innerHTML;
        frm.action = "contenedorRecibo.jsp?numRecibo=" + numRecibo + '&banFinaliza=SI'+'&cedula='+cedula;        //&numCuenta='+numCuenta;
        frm.submit();

        //window.opener.buscarFacturacion('listProcedimientosDeFactura');
        window.opener.setTimeout("guardarYtraerDatoAlListado('consultarValoresCuenta')",500);
        window.opener.setTimeout("buscarFacturacion('listGrillaFacturasCartera')",700);
        window.opener.setTimeout("verificarEnvioRecibo()",900);
        window.opener.setTimeout("limpiarDivEditarJuan('limpiarRecibosCartera')",1100);    
    }

    function  numerarRecibo() {
        $.ajax({
            url: "numerarRecibo.jsp",
            type: "GET",
            data: {
                "id_usuario": document.getElementById('lblIdCedulaFacturador').innerHTML,
                "id_recibo": document.getElementById('lblIdReciboVentana').innerHTML,
            },
            beforeSend: function () {
                $("#generaFacturaContenido").hide();
                $("#loader").show()
            },
            success: function (data) {
                if (data.respuesta) {
                    $("#btnNumerar").hide()
                    setTimeout(() => {
                        window.opener.buscarFacturacion('listRecibosFactura');          
                    }, 100);
                } else {
                    alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (E000)")
                }
            },
            complete: function () {
                document.getElementById("generaFacturaContenido").src = "generaRecibos.jsp?numRecibo=" + document.getElementById('lblIdReciboVentana').innerHTML + "&cedula=" + document.getElementById('lblIdCedulaFacturador').innerHTML + "&consecutivo=" + "";
                $("#generaFacturaContenido").show();
                $("#loader").hide()
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (E001)")
            }
        });
    }
</SCRIPT>


<table width="100%" cellpadding="0" cellspacing="0"  align="center" >
    <tr class="camposRepInp" height="50px">
        <!-- <td align="center" width="30%">  
            <form name="frmFinalizar" action="contenedorRecibo.jsp" method="post">                                          
                <input type="button" class="small button blue" onClick="numeraRecibo(document.forms['frmFinalizar'])" title="TOD34" value="NUMERAR RECIBO">                                 
            </form>  		
        </td> -->
        <td align="center" width="30%">                                     
                <input type="button" class="small button blue" onClick="numerarRecibo()" value="NUMERAR RECIBO">                                 		
        </td> 
        <td width="70%"> 
            <H4> 
                USUARIO = <label id="lblIdCedulaFacturador"><%=request.getParameter("cedula")%></label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                  
                ID RECIBO = <label id="lblIdReciboVentana"><%=request.getParameter("numRecibo")%></label>   	  	  
            </H4> 
        </td>
    </tr> 
    <tr class="camposRepInp" >
        <td colspan="2" width="100%" height="95%" valign="top">
            <iframe  id="generaFacturaContenido" width="100%" height="900px" src="generaRecibos.jsp?numRecibo=<%=request.getParameter("numRecibo")%>&banFinaliza=<%=request.getParameter("banFinaliza")%>&cedula=<%=request.getParameter("cedula")%>&consecutivo=<%=request.getParameter("consecutivo")%>" ></iframe>
        </td> 
    </tr>     
</table>  

