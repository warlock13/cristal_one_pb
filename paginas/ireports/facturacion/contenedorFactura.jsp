<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="net.sf.jasperreports.engine.*" %> 
<%@ page import="net.sf.jasperreports.engine.util.*" %>
<%@ page import="net.sf.jasperreports.engine.export.*" %>

<%@ page import="net.sf.jasperreports.view.JasperViewer" %>
<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->  
<%
    System.out.println("-----------------IREPORTS-------------------------");
    ArrayList resultaux = new ArrayList();
    boolean estado;
%>

<SCRIPT language=javascript src="/clinica/utilidades/javascript/jquery.js"></SCRIPT>
<link href="/clinica/utilidades/css/estiloClinica.css" rel="stylesheet" type="text/css">
<SCRIPT language=javascript>

    function numerarFactura() {
        if ($("#lblTipoPlan").text() == "EVENTO" && false) {
            numeracionExterna($("#lblIdSede").text());
        } else {
            $.ajax({
                url: "numerarFactura.jsp",
                type: "GET",
                data: {
                    "numFactura": document.getElementById('lblIdFactura').innerHTML,
                    "cedula": document.getElementById('lblIdCedulaFacturador').innerHTML,
                    "lblIdDoc": document.getElementById('lblIdDoc').innerHTML
                },
                beforeSend: function () {
                    $("#generaFacturaContenido").hide();
                    $("#loader").show()
                },
                success: function (data) {
                    if (data.respuesta) {
                        document.getElementById("generaFacturaContenido").src = "generaFactura.jsp?numFactura=" + document.getElementById('lblIdFactura').innerHTML;
                        $("#btnNumerar").hide()
                        setTimeout(() => {
                            switch (document.getElementById('ventana').innerHTML){
                                case "admisiones":
                                    window.opener.buscarAGENDA('listAdmisionCuenta');
                                    break;

                                case "soloFactura":
                                    window.opener.buscarFacturacion('listSoloFacturas');
                                    break;
                            }           
                        }, 100);
                    } else {
                        alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (E000)")
                    }
                },
                complete: function () {
                    $("#generaFacturaContenido").show();
                    $("#loader").hide()
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (E001)")
                }
            });
        }
    }

    function verificarFechaDePrestacion() {
        if ($("#cmbMesPrestacion").val() == "") {
            alert("FALTA SELECCIONAR EL MES DE PRESTACION")
            return
        }
        window.opener.verificarNotificacionAntesDeGuardar('validarPaqueteEventoNumerarFactura')
    }

    function limpiarMes() {
        $("#cmbMesPrestacion").empty()
        $("#cmbMesPrestacion").append($("<option>", {
            value: "",
            text: "[ SELECCIONE ]"
        }));
    }

    function mostrarMeses() {
        $("#cmbMesPrestacion").empty()
        meses = [
            {id: 1, nombre: "Enero"},
            {id: 2, nombre: "Febrero"},
            {id: 3, nombre: "Marzo"},
            {id: 4, nombre: "Abril"},
            {id: 5, nombre: "Mayo"},
            {id: 6, nombre: "Junio"},
            {id: 7, nombre: "Julio"},
            {id: 8, nombre: "Agosto"},
            {id: 9, nombre: "Septiembre"},
            {id: 10, nombre: "Octubre"},
            {id: 11, nombre: "Noviembre"},
            {id: 12, nombre: "Diciembre"},
        ]

        var aux = new Date();
        var anio_actual = aux.getFullYear()
        var mes_actual = aux.getMonth() + 1;

        for (i = 0; i < meses.length; i++) {
        if (anio_actual == $("#cmbAnioPrestacion").val() && i + 1 > mes_actual) {
            return;
        }

        var option = $("<option>", {
            value: meses[i].id,
            text: meses[i].nombre
        });

        if (meses[i].id === mes_actual) {
            option.attr("selected", "selected");
        }

        $("#cmbMesPrestacion").append(option);
    }


    }

    function numeracionExterna(idsede) {
        var sede;
        switch (idsede) {
            case "1":
                sede = "AURORA"
                break;

            case "4":
                sede = "TUQUERRES"
                break;

            case "21":
                sede = "TERAPIAS"
                break;

            case "10":
                sede = "FATIMA"
                break;

            case "13":
                sede = "ACACIAS"
                break;

            case "35":
                sede = "MEDICRON"
                break;

            case "11":
                sede = "BUESACO"
                break;

            case "8":
                sede = "LA CRUZ"
                break;

            case "17":
                sede = "VILLACOLOMBIA"
                break;

            case "5":
                sede = "IPIALES"
                break;

            case "15":
                sede = "RENAL Y PULMONAR"
                break;

            case "16":
                sede = "TEQUENDAMA CONTRIBUTIVO CONSULTA EXTERNA"
                break;

            case "19":
                sede = "PALMIRA"
                break;

            case "12":
                sede = "TUMACO"
                break;

            case "22":
                sede = "BUENAVENTURA"
                break;

            case "25":
                sede = "SEVILLA"
                break;

            case "6":
                sede = "PARQUE BOLIVAR"
                break;

            case "3":
                sede = "ESPECIALIDADES"
                break;

            case "27":
                sede = "TULUA"
                break;

            case "38":
                sede = "LORENZO"
                break;

            case "24":
                sede = "MEDICRON TEQUENDAMA"
                break;
	   
	        case "28":
                sede = "CAMBULOS"
                break;

            case "14":
                sede = "LABORATORIO CLINICO VALLE"
                break;

            default:
                sede = "";
                break;
        }

        var query = "mutation($sede: String!, $incrementar:Boolean){ numeracionFactura(sede:$sede, incrementar:$incrementar){ numero }}";
        var variables = {"sede": sede, "incrementar": true}

        $.ajax({
            url: "http://190.60.243.113:8081/graphql",
            type: "POST",
            contentType: "application/json",
            crossDomain: true,
            data: JSON.stringify({"query": query, "variables": variables}),
            beforeSend: function () {
                $("#generaFacturaContenido").hide();
                $("#loader").show()
            },
            success: function (data) {
                var respuesta = data.data.numeracionFactura
                if (respuesta != null) {
                    //PROCESAR NUMERACION
                    var parametros = "_-" +
                            respuesta.numero + "_-" +
                            window.opener.IdSesion() + "_-" +
                            window.opener.valorAtributo("lblIdFactura") + "_-" +
                            window.opener.IdSesion() + "_-" +
                            window.opener.valorAtributo("lblIdFactura");
                    $.ajax({
                        url: "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp",
                        type: "POST",
                        data: {"accion": "numerarFacturaSIPS",
                            "idQuery": 1041,
                            "parametros": parametros},
                        beforeSend: function () {
                            $("#generaFacturaContenido").hide();
                            $("#loader").show()
                        },
                        success: function (data) {
                            var respuesta = data.getElementsByTagName("respuesta")[0].firstChild.data
                            if (respuesta) {
                                numFactura = document.getElementById('lblIdFactura').innerHTML;
                                cedula = document.getElementById('lblIdCedulaFacturador').innerHTML;
                                idDoc = document.getElementById('lblIdDoc').innerHTML;
                                idEstadoFactura = 'F';
                                $('#formNumerar').attr("action", "contenedorFactura.jsp?numFactura=" + numFactura +
                                        '&idEstadoFactura=' + idEstadoFactura +
                                        '&cedula=' + cedula +
                                        '&lblIdDoc=' + idDoc +
                                        '&idsede=' + idsede);
                                $('#formNumerar').submit();
                                switch (document.getElementById('ventana').innerHTML){
                                    case "admisiones":
                                        window.opener.buscarAGENDA('listAdmisionCuenta');
                                        break;

                                    case "soloFactura":
                                        window.opener.buscarFacturacion('listSoloFacturas');
                                        break;
                                }   
                            } else {
                                alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (E002)")
                            }
                        },
                        complete: function () {
                            $("#generaFacturaContenido").show();
                            $("#loader").hide()
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (E003)")
                        }
                    });
                } else {
                    alert("No se puede obtener numeracion externa. Por favor ponte en contacto con soporte tecnico.")
                }
            },
            complete: function () {
                $("#generaFacturaContenido").show();
                $("#loader").hide()
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Error con la solicitud. Por favor ponte en contacto con soporte tecnico. (E004)")
            }
        });
    }
</SCRIPT>

<body>
    <table width="100%" cellpadding="0" cellspacing="0"  align="center" >
        <tr class="estiloImputListaEspera" height="50px">
            <td align="center" width="20%">  
                <form name="frmFinalizar" action="../facturacion/contenedorFactura.jsp" method="post" id="formNumerar">
                    <% if (request.getParameter("idEstadoFactura").equals("P") || request.getParameter("idEstadoFactura").equals("D") || request.getParameter("idEstadoFactura").equals("R")) {%>  
                    <input type="button" class="small button blue" onClick="window.opener.verificarNotificacionAntesDeGuardar('foliosHcBorrador')" title="TOD34" value="NUMERAR FACTURA">
                    <%}%>       
                </form>  		
            </td> 
            <td width="60%"> 
                <table width="100%">
                    <tr class="titulosListaEspera">
                        <td><strong>ID SEDE</strong></td>
                        <td><strong>TIPO PLAN</strong></td>
                        <td><strong>USUARIO</strong></td>
                        <td><strong>ID FACTURA</strong></td>
                        <td hidden><strong>ESTADO</strong></td>
                        <td hidden><strong>ID DOC</strong></td>
                        <td hidden><strong>VENTANA</strong></td>
                    </tr>
                    <tr class="estiloImputListaEspera">
                        <td><label id="lblIdSede"><%=request.getParameter("idsede")%></label></td>
                        <td><label id="lblTipoPlan"><%=request.getParameter("tipoPlan")%></label></td>
                        <td><label id="lblIdCedulaFacturador"><%=request.getParameter("cedula")%></label></td>
                        <td><label id="lblIdFactura"><%=request.getParameter("numFactura")%></label></td>
                        <td hidden><label id="lblIdEstadoFactura"><%=request.getParameter("idEstadoFactura")%></label></td>
                        <td hidden><label id="lblIdDoc"><%=request.getParameter("lblIdDoc")%></label></td>
                        <td hidden><label id="ventana"><%=request.getParameter("ventana")%></label></td>
                    </tr>
                </table>
            </td>
            <td  align="center" width="20%"><input type="button" class="small button blue" onClick="facturaPendiente(document.forms['frmFinalizar'])" title="TOD35" value="ESTADO PENDIENTE"></td>
        </tr>   
    </table>  
    <div id="loader" hidden>
        <div style="display: flex; justify-content: center; align-items: center; height: 80%;">
            <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="120px" height="120px">
        </div>
    </div>
    <iframe id="generaFacturaContenido" width="100%" height="90%" src="generaFactura.jsp?numFactura=<%=request.getParameter("numFactura")%>"></iframe>    
</body>

<div  id="divNotificacionAlert" align="center" title="MENSAJE" style="z-index:3000; top:0px; left:0px; position:fixed; display:none;" >
    <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
    </div> 
    <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%;">     
        <table bgcolor="#FFFFFF" align="center"  width="300px" height="100px" id="tablaNotificacionesAlert" style="border: 1px solid #000;" >
            <tr>
                <td bgcolor="#7CD9FC" align="center" background="/clinica/utilidades/imagenes/menu/six_0_.gif" >
                    <img align="left" height="20" width="20" src="/clinica/utilidades/imagenes/acciones/alertaIntermitente.gif"/>
                    <b>ALERTA</b>
                    <img align="right" height="16" width="16" style="padding-right: 5px;" src="/clinica/utilidades/imagenes/acciones/cerrar2.png" onclick="$('#divNotificacionAlert').hide()"/>
                </td>      
            </tr>	   
            <tr>
                <td>
                    <i><label style="font-family: Helvetica;" id="lblTotNotifVentanillaAlert">_</label></i>
                </td>      
            </tr>
            <tr>
                <td align="center">  
                    <TABLE id="idTableBotonNotificaAlert" width="50%" border="1">
                        <TR>
                            <TD align="center">
                            </TD>
                        </TR>
                    </TABLE>
                </td>        	               
            </tr>            
        </table>
        <br/>
    </div>   
</div>

<div id="divActualizarPeriodoPrestacion" style="position:absolute; display:none; top:0px; left:0; width:100%; height:100%; z-index:2004">
    <div class="transParencia" style="z-index:2005; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
    </div> 
    <div style="z-index:2006; position:absolute; top: 20%; left: 35%; width:500px">          
        <table width="100%" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left">
                    <input name="btn_cerrar" type="button" value="CERRAR" onclick="$('#divActualizarPeriodoPrestacion').hide()"/>
                </td>
                <td align="right">
                    <input name="btn_cerrar" type="button" value="CERRAR" onclick="$('#divActualizarPeriodoPrestacion').hide()"/>
                </td>  
            <tr>     
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr class="titulosListaEspera">
                            <td colspan="3">PERIODO DE PRESTACION DE LOS SERVICIOS FACTURADOS</td>
                        </tr>
                        <td>
                        <td colspan="3"><br></td>
                </td>
            <tr class="titulosListaEspera">
                <td>A&Ntilde;O</td>
                <td>MES</td>
                <td></td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td>
                    <select id="cmbAnioPrestacion" onchange="limpiarMes()">
                        <option value="<%=LocalDate.now().getYear() - 1%>"><%=LocalDate.now().getYear() - 1%></option>
                        <option value="<%=LocalDate.now().getYear()%>" selected><%=LocalDate.now().getYear()%></option>
                    </select>
                </td>
                <td>
                    <select id="cmbMesPrestacion" onfocus="mostrarMeses()" style="width: 60%;">
                        <option value="">[ SELECCIONE ]</option>
                    </select>
                </td>
                <td>
                    <input type="button" class="small button blue" value="ACEPTAR" onclick="verificarFechaDePrestacion()">
                </td>
            </tr>
        </table>  
    </div>            
</div> 


