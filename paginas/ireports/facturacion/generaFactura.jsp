<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>

<%@ page import="net.sf.jasperreports.engine.*" %>
<%@ page import="net.sf.jasperreports.engine.util.*" %>
<%@ page import="net.sf.jasperreports.engine.export.*" %>

<%@ page import="net.sf.jasperreports.view.JasperViewer" %>
<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %>
<%@ page import="java.awt.Font" %>

<%@ page import = "Sgh.Utilidades.ConnectionClinica" %>

<jsp:useBean id="conexion" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<%
    System.out.println("-----------------IREPORTS PDF (FACTURA)-----------------------" );
%>

<!DOCTYPE HTML>
<% 
    String ruta = "paginas/ireports/facturacion/";

    String cadenaJasper = ruta + "FACTURACION.jasper";

    int numFact = Integer.parseInt(request.getParameter("numFactura"));

    File reportFile = new File(application.getRealPath(cadenaJasper));
    HashMap<String, Object> parametros = new HashMap<String, Object>();

    parametros.put("P1", numFact);

    byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parametros, conexion.getConnection());

    response.setContentType("application/pdf");
    response.setContentLength(bytes.length);

    ServletOutputStream ouputStream = response.getOutputStream();
    ouputStream.write(bytes, 0, bytes.length);

    ouputStream.flush();
    ouputStream.close();
%>