<%@page import="Sgh.Utilidades.LoggableStatement"%>
<%@ page session="true" contentType="application/json; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "Sgh.Utilidades.Conexion"%>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<%
    String sql = null;
    String mensaje = null;
    Boolean resultado = false;

    Conexion cn = new Conexion(iConnection.getConnection());
    sql = cn.traerElQuery(160).toString();

    try {
        String idUsuario = request.getParameter("id_usuario");
        String idRecibo = request.getParameter("id_recibo");
        
        PreparedStatement ps = new LoggableStatement(cn.getConexion(), sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ps.setString(1,idUsuario);
        ps.setString(2,idRecibo);
        ps.setString(3,idRecibo);
        int cant = ps.executeUpdate();
        ps.close();
        
        resultado = true;
        
    } catch (Exception ex) {
        System.out.println("********* Error al numerar::Exception " + ex.getMessage());
        resultado = false;
    }

%>
{"respuesta":<%= resultado %>}
