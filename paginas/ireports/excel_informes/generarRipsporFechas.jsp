<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<%@ page import="javax.naming.*" %>

<%@ page import="java.io.*" %> 
<%@ page import="java.awt.Font" %> 

<%@ page import="java.util.zip.ZipEntry" %> 
<%@ page import="java.util.zip.ZipOutputStream" %> 
<%@ page import="java.util.List" %> 
<%@ page import="java.util.ArrayList" %> 

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 


<!DOCTYPE HTML >  
<%   
    boolean hay_datos = false;
    String ruta = "/opt/tomcat8/webapps/ripsFechas/";
    String archivo = "";
    Connection conexion = beanSession.cn.getConexion();

    String ruta_tipificacion = "/opt/tomcat8/webapps/tipificacion/";
    String ruta_archivo_tipificacion_comprimido = ruta_tipificacion + "000_" + ".zip";

    List<String> srcFiles = new ArrayList();

    FileWriter fichero = null;

    PreparedStatement pr = null;
    String sql = "";
    String sqlT = "";
    String separador = ",";
    String cad_datos = "";

    /*
          RIPS facturacion.vi_rips_ac_f 
     */
    sql = "select   \n" +
            "trim(c1) numero_factura, \n" +
            "trim(c2) cod_habilitacion, \n" +
            "trim(c3) tipo_id, \n" +
            "trim(c4) identificacion, \n" +
            "c5 fecha, \n" +
            "trim(c6) no_autorizacion, \n" +
            "trim(c7) cod_externo, \n" +
            "trim(c8) finalidad_consulta, \n" +
            "trim(c9) causa_externa, \n" +
            "trim(c10) dx_principal, \n" +
            "trim(c11) dx_relacionado1, \n" +
            "trim(c12) dx_relacionado2, \n" +
            "trim(c13) dx_relacionado3, \n" +
            "trim(c14) tipo_dx_principal, \n" +
            "c15 valor_unitario, \n" +
            "trim(c16) c16, \n" +
            "c17 valor_unitario2,  \n" +
            "id_factura , \n" +
            "id_empresa  \n" +
            "from facturacion.vi_rips_ac_f vraf "; 
          

    try {

        System.out.println("RIPS AC_F");
        System.out.println(sql);

        archivo = ruta + "AC" + ".txt";

        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("numero_factura") + separador;
            cad_datos += resultado.getString("cod_habilitacion");
            cad_datos += separador + resultado.getString("tipo_id");
            cad_datos += separador + resultado.getString("identificacion");
            cad_datos += separador + resultado.getString("fecha");
            cad_datos += separador + resultado.getString("no_autorizacion");
            cad_datos += separador + resultado.getString("cod_externo");
            cad_datos += separador + resultado.getString("finalidad_consulta");
            cad_datos += separador + resultado.getString("causa_externa");
            cad_datos += separador + resultado.getString("dx_principal");
            cad_datos += separador + resultado.getString("dx_relacionado1");
            cad_datos += separador + resultado.getString("dx_relacionado2");
            cad_datos += separador + resultado.getString("dx_relacionado3");
            cad_datos += separador + resultado.getString("tipo_dx_principal");
            cad_datos += separador + resultado.getString("c16");
            cad_datos += separador + resultado.getString("valor_unitario2");
            cad_datos += separador + resultado.getString("id_factura");
            cad_datos += separador + resultado.getString("id_empresa");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }

    /*
          RIPS facturacion.vi_rips_af_f 
     */
    sql = "select   \n" +
            "trim(c1)  cod_habilitacion, \n" +
            "trim(c2)  razon_social, \n" +
            "trim(c3)  NI, \n" +
            "trim(c4)  id_empresa, \n" +
            "trim(c5)  numero_factura, \n" +
            "trim(c6)  fecha_numeracion, \n" +
            "c7  fecha_desde, \n" +
            "c8  fecha_hasta, \n" +
            "trim(c9)  codigo_administradora, \n" +
            "trim(c10) nombre, \n" +
            "trim(c11) num_contrato, \n" +
            "trim(c12) tipo, \n" +
            "trim(c13) c13, \n" +
            "c14 descuenta, \n" +
            "trim(c15) c15, \n" +
            "trim(c16) c16, \n" +
            "c17 valor, \n" +
            "id_factura id_factura, \n" +
            "trim(id_empresa) id_empresa, \n" +
            "id_sede id_sede \n" +
            "from facturacion.vi_rips_af_f vraf2 ";
          

    try {

        System.out.println("RIPS AF_F");
        System.out.println(sql);

        archivo = ruta + "AF" + ".txt";
        cad_datos = "";
        pr = conexion.prepareStatement(sql);
        ResultSet resultado = pr.executeQuery();
        hay_datos = false;

        while (resultado.next()) {
            hay_datos = true;
            cad_datos += resultado.getString("cod_habilitacion") + separador;
            cad_datos += resultado.getString("razon_social");
            cad_datos += separador + resultado.getString("NI");
            cad_datos += separador + resultado.getString("id_empresa");
            cad_datos += separador + resultado.getString("numero_factura");
            cad_datos += separador + resultado.getString("fecha_numeracion");
            cad_datos += separador + resultado.getString("fecha_desde");
            cad_datos += separador + resultado.getString("fecha_hasta");
            cad_datos += separador + resultado.getString("codigo_administradora");
            cad_datos += separador + resultado.getString("nombre");
            cad_datos += separador + resultado.getString("num_contrato");
            cad_datos += separador + resultado.getString("tipo");
            cad_datos += separador + resultado.getString("c13");
            cad_datos += separador + resultado.getString("descuenta");
            cad_datos += separador + resultado.getString("c15");
            cad_datos += separador + resultado.getString("c16");
            cad_datos += separador + resultado.getString("valor");
            cad_datos += separador + resultado.getString("id_factura");
            cad_datos += separador + resultado.getString("id_empresa");
            cad_datos += separador + resultado.getString("id_sede");
            cad_datos += "\r\n";
        }

        if (hay_datos) {
            srcFiles.add(archivo);
            fichero = new FileWriter(archivo);
            fichero.write(cad_datos);
            fichero.close();
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }
    
    /*
    RIPS facturacion.vi_rips_ap_f 
    */

        sql = "select   \n" +
              "trim(c1)  numero_factura, \n" +
              "trim(c2)  cod_habilitacion, \n" +
              "trim(c3)  tipo_id, \n" +
              "trim(c4)  identificacion, \n" +
              "c5  fecha, \n" +
              "trim(c6)  no_autorizacion, \n" +
              "trim(c7)  cod_externo, \n" +
              "trim(c8)  c8, \n" +
              "trim(c9)  c9, \n" +
              "trim(c10) c10, \n" +
              "trim(c11) dx_principal, \n" +
              "trim(c12) c12, \n" +
              "trim(c13) c13, \n" +
              "trim(c14) c14, \n" +
              "c15 valor_unitario, \n" +
              "n cantidad, \n" +
              "id_factura id_factura, \n" +
              "id_empresa id_empresa  \n" +
              "from  facturacion.vi_rips_ap_f vraf3";       

        try {
        
          System.out.println("RIPS AP_F");
          System.out.println(sql);
          cad_datos = "";
          archivo = ruta + "AP" + ".txt";
        
          pr = conexion.prepareStatement(sql);
          ResultSet resultado = pr.executeQuery();
          hay_datos = false;
        
          while (resultado.next()) {
              hay_datos = true;
              cad_datos += resultado.getString("numero_factura") + separador;
              cad_datos += resultado.getString("cod_habilitacion");
              cad_datos += separador + resultado.getString("tipo_id");
              cad_datos += separador + resultado.getString("identificacion");
              cad_datos += separador + resultado.getString("fecha");
              cad_datos += separador + resultado.getString("no_autorizacion");
              cad_datos += separador + resultado.getString("cod_externo");
              cad_datos += separador + resultado.getString("c8");
              cad_datos += separador + resultado.getString("c9");
              cad_datos += separador + resultado.getString("c10");
              cad_datos += separador + resultado.getString("dx_principal");
              cad_datos += separador + resultado.getString("c12");
              cad_datos += separador + resultado.getString("c13");
              cad_datos += separador + resultado.getString("c14");
              cad_datos += separador + resultado.getString("valor_unitario");
              cad_datos += separador + resultado.getString("cantidad");
              cad_datos += separador + resultado.getString("id_factura");
              cad_datos += separador + resultado.getString("id_empresa");
              cad_datos += "\r\n";
          }
      
          if (hay_datos) {
              srcFiles.add(archivo);
              fichero = new FileWriter(archivo);
              fichero.write(cad_datos);
              fichero.close();
          }
        } catch (IOException ioe) {
          ioe.printStackTrace();
        }

        /*
        RIPS facturacion.vi_rips_at_f 
        */
    
            sql = "select   \n" +
                    "trim(c1)  numero_factura, \n" +
                    "trim(c2)  cod_habilitacion, \n" +
                    "trim(c3)  tipo_id, \n" +
                    "trim(c4)  identificacion, \n" +
                    "trim(c5)  no_autorizacion, \n" +
                    "trim(c6)  c6, \n" +
                    "trim(c7)  cod_externo, \n" +
                    "trim(c8)  nombre, \n" +
                    "c9  cantidad, \n" +
                    "c10 valor_unitario, \n" +
                    "c11 sub_total, \n" +
                    "id_factura id_factura, \n" +
                    "id_empresa id_empresa  \n" +
                    "from  facturacion.vi_rips_at_f vraf4";      
    
            try {
            
              System.out.println("RIPS AP_F");
              System.out.println(sql);
              cad_datos = "";  
              archivo = ruta + "AT" + ".txt";
            
              pr = conexion.prepareStatement(sql);
              ResultSet resultado = pr.executeQuery();
              hay_datos = false;
            
              while (resultado.next()) {
                  hay_datos = true;
                  cad_datos += resultado.getString("numero_factura") + separador;
                  cad_datos += resultado.getString("cod_habilitacion");
                  cad_datos += separador + resultado.getString("tipo_id");
                  cad_datos += separador + resultado.getString("identificacion");
                  cad_datos += separador + resultado.getString("no_autorizacion");
                  cad_datos += separador + resultado.getString("c6");
                  cad_datos += separador + resultado.getString("cod_externo");
                  cad_datos += separador + resultado.getString("nombre");
                  cad_datos += separador + resultado.getString("cantidad");
                  cad_datos += separador + resultado.getString("valor_unitario");
                  cad_datos += separador + resultado.getString("sub_total");
                  cad_datos += separador + resultado.getString("id_factura");
                  cad_datos += separador + resultado.getString("id_empresa");
                  cad_datos += "\r\n";
              }
          
              if (hay_datos) {
                  srcFiles.add(archivo);
                  fichero = new FileWriter(archivo);
                  fichero.write(cad_datos);
                  fichero.close();
              }
            } catch (IOException ioe) {
              ioe.printStackTrace();
            }
            
            /*
            RIPS facturacion.vi_rips_US_f 
            */
        
                sql = "select   \n" +
                        "trim(c1)  tipo_id_cliente, \n" +
                        "trim(c2)  identificacion_cliente, \n" +
                        "trim(c3)  codigo_administradora, \n" +
                        "trim(c4)  tipo_rips, \n" +
                        "trim(c5)  apellido1, \n" +
                        "trim(c6)  apellido2, \n" +
                        "trim(c7)  nombre1, \n" +
                        "trim(c8)  nombre2, \n" +
                        "trim(c9)  fecha_uno, \n" +
                        "trim(c10) fecha_dos, \n" +
                        "trim(c11) id_sexo, \n" +
                        "trim(c12) id_departamento, \n" +
                        "trim(c13) id_municipio, \n" +
                        "trim(c14) id_zona, \n" +
                        "trim(id_empresa) id_empresa  \n" +
                        "from facturacion.vi_rips_us_f vruf";       
        
                try {
                
                  System.out.println("RIPS US_F");
                  System.out.println(sql);
                
                  archivo = ruta + "US" + ".txt";
                  cad_datos = "";
                  pr = conexion.prepareStatement(sql);
                  ResultSet resultado = pr.executeQuery();
                  hay_datos = false;
                
                  while (resultado.next()) {
                      hay_datos = true;
                      cad_datos += resultado.getString("tipo_id_cliente") + separador;
                      cad_datos += resultado.getString("identificacion_cliente");
                      cad_datos += separador + resultado.getString("codigo_administradora");
                      cad_datos += separador + resultado.getString("tipo_rips");
                      cad_datos += separador + resultado.getString("apellido1");
                      cad_datos += separador + resultado.getString("apellido2");
                      cad_datos += separador + resultado.getString("nombre1");
                      cad_datos += separador + resultado.getString("nombre2");
                      cad_datos += separador + resultado.getString("fecha_uno");
                      cad_datos += separador + resultado.getString("fecha_dos");
                      cad_datos += separador + resultado.getString("id_sexo");
                      cad_datos += separador + resultado.getString("id_departamento");
                      cad_datos += separador + resultado.getString("id_municipio");
                      cad_datos += separador + resultado.getString("id_zona");
                      cad_datos += separador + resultado.getString("id_empresa");
                      cad_datos += "\r\n";
                  }
              
                  if (hay_datos) {
                      srcFiles.add(archivo);
                      fichero = new FileWriter(archivo);
                      fichero.write(cad_datos);
                      fichero.close();
                  }
                } catch (IOException ioe) {
                  ioe.printStackTrace();
                }

        





        /*
        RIPS facturacion.vi_rips_ct_f 
        */
    
            sql = "select   \n" +
                    "trim(c1) cod_habilitacion, \n" + 
                    "trim(c2) fecha, \n" + 
                    "trim(c3) tipo, \n" + 
                    "c4 total, \n" + 
                    "id_empresa id_empresa  \n" + 
                    "from facturacion.vi_rips_ct_f vrcf";      
    
            try {
            
              System.out.println("RIPS CT_F");
              System.out.println(sql);
              cad_datos = "";
              archivo = ruta + "CT" + ".txt";
            
              pr = conexion.prepareStatement(sql);
              ResultSet resultado = pr.executeQuery();
              hay_datos = false;
            
              while (resultado.next()) {
                  hay_datos = true;
                  cad_datos += resultado.getString("cod_habilitacion") + separador;
                  cad_datos += resultado.getString("fecha");
                  cad_datos += separador + resultado.getString("tipo");
                  cad_datos += separador + resultado.getString("total");
                  cad_datos += separador + resultado.getString("id_empresa");
                  cad_datos += "\r\n";
              }
              if (hay_datos) {
                  srcFiles.add(archivo);
                  fichero = new FileWriter(archivo);
                  fichero.write(cad_datos);
                  fichero.close();
              }
            } catch (IOException ioe) {
              ioe.printStackTrace();
            }


    FileOutputStream fos = new FileOutputStream(ruta + "rips.zip");
    ZipOutputStream zipOut = new ZipOutputStream(fos);
    for (String srcFile : srcFiles) {
        File fileToZip = new File(srcFile);
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
        zipOut.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }



    zipOut.close();
    fos.close();

    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=rips.zip");

    File file = new File(ruta + "rips.zip");
    FileInputStream fileIn = new FileInputStream(file);
    ServletOutputStream out2 = response.getOutputStream();

    byte[] outputByte = new byte[4096];
    while (fileIn.read(outputByte, 0, 4096) != -1) {
        out2.write(outputByte, 0, 4096);
    }
    fileIn.close();
    out2.flush();
    out2.close();
    
%>