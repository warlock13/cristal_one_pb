  <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
  <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
  <%@ page import = "Sgh.Utilidades.*" %>
  <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
  <%@ page import = "Clinica.Presentacion.*" %>
  
  <%@ page import="net.sf.jasperreports.engine.*" %> 
  <%@ page import="net.sf.jasperreports.engine.util.*" %>
  <%@ page import="net.sf.jasperreports.engine.export.*" %>

  <%@ page import="net.sf.jasperreports.view.JasperViewer" %>
  <%@ page import="javax.naming.*" %>
  
  <%@ page import="java.io.*" %> 
  <%@ page import="java.awt.Font" %> 
  
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->
  
<% 	 
	System.out.println("-----------------IREPORTS-------------------------"+request.getParameter("banFinaliza"));	
	String ruta="";
%>  
 
 <!DOCTYPE HTML >  
 <% 

    Connection conexion = beanSession.cn.getConexion(); 
	
	ruta = "paginas/ireports/excel_informes/";
		
	String reporte = request.getParameter("reporte");
	
	String fechaDesde = request.getParameter("fechaDesde") + " 00:00:00";		
	String fechaHasta = request.getParameter("fechaHasta") + " 23:59:59";	
	
/*	int evo = Integer.parseInt(request.getParameter("ev"));	*/
	
	String cadenaJasper = "paginas/ireports/excel_informes/"+reporte+".jasper";
	
    File reportFile = new File(application.getRealPath(cadenaJasper));  

		HashMap<String, Object> parametros = new HashMap<String, Object>(); 
		
		parametros.put("P1", fechaDesde);  
		parametros.put("P2", fechaHasta);  

		
	//REPORTE EN EXCEL

	JasperPrint print = null;
	JasperReport report = null;
	
	try {
		print = JasperFillManager.fillReport(reportFile.getPath(), parametros, conexion);
	} catch (JRException e1) {
		e1.printStackTrace();
	}
	
	//String ArchExcel = "/home/administrador/archivos/PruebaExcel.xls";
	String ArchExcel = "/opt/tomcat8/webapps/clinica/paginas/ireports/excel_informes/Excel"+beanSession.usuario.getLogin()+".xls";
		
	JRXlsExporter exportador = new JRXlsExporter();
	exportador.setParameter(JRExporterParameter.JASPER_PRINT, print);
	exportador.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, ArchExcel);
	exportador.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, true);	

	exportador.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
	exportador.setParameter(JRXlsAbstractExporterParameter.IS_IGNORE_CELL_BORDER, false);
	exportador.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,true);
	exportador.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,true);
	exportador.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,true);
	exportador.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,true);
	
	try {
		exportador.exportReport();
	} catch (JRException e) {
		e.printStackTrace();
	}
	
	FileInputStream entrada = new FileInputStream(ArchExcel);
	byte[] lectura = new byte[entrada.available()];
	entrada.read(lectura);
	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition","attachment; filename=ArchivoExcel"+beanSession.usuario.getLogin()+".xls");
	response.setContentLength(lectura.length);
	response.getOutputStream().write(lectura);
	response.getOutputStream().flush();
	response.getOutputStream().close();
	entrada.close();

%>
