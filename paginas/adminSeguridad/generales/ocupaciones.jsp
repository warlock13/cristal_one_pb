<%--
-Author: Juan Angel Santacruz
-Date: 8 enero 2008
-
-Description: comentarios de la pagina

--%>
<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

 
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.Sesion" %>
 <% //@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
 <%// @ page import = "Sgh.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->

<%
   //inicializar conexion de base de datos y demas informacion de los bean
   //beanAdmin.setCn(beanSession.getCn());
%>   

<table width="650px"  border="0" cellspacing="0" cellpadding="0"   >
  <tr>
    <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="GESTIONAR OCUPACIONES" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
  <tr>
    <td>
  
   <div style="overflow:auto; height:100px" id="divContenido">   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
   <!--******************************** inicio de la tabla con los datos de busqueda ***************************-->	
   <div id="divBuscar">  
	 	<table width="100%" border="0" cellpadding="1" cellspacing="1" >
      		<tr>
      			<td colspan="2" class="titulos" align="center">BUSQUEDA DE OCUPACIONES</td>
	      	</tr>
		  	<tr class="titulos">
      			<td width="30%">C&oacute;digo</td>
      			<td >Nombre</td>
     		</tr>
     		<tr class="estiloImput">
              <td align="center"><input id="txtBusCodigo" name="txtBusCodigo" type="text" style="width:50%" maxlength="5"   onkeypress="javascript:return teclearsoloan(event);"/></td>
              <td align="center"><input id="txtBusNombre" name="txtBusNombre" type="text" style="width:70%"   onkeypress="javascript:return teclearsoloan(event);"/></td>
           </tr>
    	</table>
    	<div id="divListado" align="center" >
        	 <table id="list" class="scroll"></table>  
         	<div id="pager" class="scroll" style="text-align:center;"></div>
    	</div>
  </div>
  <!--********************** fin de la tabla con los datos de busqueda ***********************************************-->
  <!--******************************** inicio de la tabla con los datos del formulario ***************************-->	
    <div id="divEditar"  style="display:none" > 
     <table width="100%" border="0" cellpadding="1" cellspacing="1" >
	 <tr class="titulos">
       <td width="30%" colspan="2">EDITAR OCUPACIONES</td>
     </tr>
	 <tr class="titulos">
     <td width="30%">C&oacute;digo</td>
     <td >Nombre</td>
    </tr>
    <tr class="estiloImput">
     <td align="center"><input id="txtCodigo" name="txtCodigo" type="text" style="width:50%" maxlength="5"   onkeypress="javascript:return teclearsoloan(event);"/></td>
     <td align="center"><input id="txtNombre" name="txtNombre" type="text" style="width:70%"   onkeypress="javascript:return teclearsoloan(event);"/></td>
    </tr>
   </table>
    </div>
     <!--********************** fin de la tabla con los datos del formulario ***********************************************-->
 	</td>
  </tr>
</table> 
	  <!-- inicio borde esquinas redondas-->
	  <div class='filoInferiorTabla'>&nbsp;</div> 
	  <div id=nifty><B class=rbottom><B class=r4></B><B class=r3></B><B class=r2></B><B class=r1></B></B></DIV>
	  <!-- fin borde esquinas redondas-->
   </td>
</tr> 
</table>



