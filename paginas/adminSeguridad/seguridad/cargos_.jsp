<%--
-Author: Piedad Ceron Diaz
-Date: 16 de Diciembre de 2008
-
-Description: Esta p�gina fu� creada con el fin de servir como gui para el proceso gestionar la informaci�n de cargos
--%>
<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>

 
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Max.Utilidades.Sesion" %>
 <% //@ page import = "Max.AdminSeguridad.ControlAdmin" %>
 <%// @ page import = "Max.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Max.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->


<%
   //inicializar conexion de base de datos y demas informacion de los bean
   //beanAdmin.setCn(beanSession.getCn());
%>   

<table width="580px" border="0" cellspacing="0" cellpadding="0" > <!-- en el atributo width se ubica el ancho de la p�gina preferiblemente en pixels -->
  <tr>
    <td>
	  <!-- AQUI COMIENZA EL TITULO -->	 <!-- en value del parametro ubicar el titulo de la pantalla -->
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="CARGOS" />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
       <table width="106%" border="1" cellpadding="0" cellspacing="0" class="fondoTabla">
          <tr>
            <td>
               <div style=" overflow:auto; height:180px"> <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->

                  <!--******************************** inicio de la tabla con los datos del formulario ***************************-->
                    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
                      <tr class="titulos">
                        <td width="40%" height="19"><label class="asterisco" style="font-size:12px;" >*</label>&nbsp;&nbsp;C&oacute;digo&nbsp;&nbsp <img src="/max/utilidades/imagenes/acciones/lupa.gif" height="20" width="20" />                         </td>
                        <td colspan="2"><label class="asterisco" style="font-size:12px;" >*</label>&nbsp;&nbsp;Cargo&nbsp;&nbsp <img src="/max/utilidades/imagenes/acciones/lupa.gif" height="20" width="20" />                     </td>
                      </tr>
                      <tr class="estiloImput">
                         <td height="26" align="center"><input id="txtCodigo" name="txtCodigo" type="text" style="width:90%"  onkeypress="javascript:return teclearsoloan(event);"/></td>
                         <td align="center" colspan="2"><input id="txtNombre" name="txtNombre" type="text" style="width:90%"   /></td>
                      </tr>                   
                      <tr class="titulos">
                         <td colspan="3" height="19"><label class="asterisco" style="font-size:12px;" >*</label>&nbsp;&nbsp;Descripci&oacute;n&nbsp;&nbsp <img src="/max/utilidades/imagenes/acciones/lupa.gif" height="20" width="20" />                         </td>
                      </tr>
                      <tr class="estiloImput">
                         <td colspan="3" height="26" align="center"><input id="txtDescripcion" name="txtDescripcion" type="text" style="width:90%"    onkeypress="javascript:return teclearsoloan(event);"/></td>
                      </tr>
					  <tr class="titulos">
                        <td width="40%" ><label class="asterisco" style="font-size:12px;" >*</label>&nbsp;&nbsp;Sueldo&nbsp;&nbsp <img src="/max/utilidades/imagenes/acciones/lupa.gif" height="20" width="20" />                     </td>
	                     <td colspan="2"><label class="asterisco" style="font-size:12px;" >*</label>&nbsp;&nbsp;Estado&nbsp;&nbsp <img src="/max/utilidades/imagenes/acciones/lupa.gif" height="20" width="20" />                     </td>
                      </tr>
                      <tr class="estiloImput">
                         <td align="center">$ <input id="txtSueldo" name="txtSueldo" type="text"  style="width:90%"     /></td>
					    <td width="29%" align="center"><input id="optEstadoA" name="optEstado" type="radio"  /> Activo	 					</td>
                        <td width="31%" align="center"><input id="optEstadoI" name="optEstado" type="radio"  />
Inactivo </td>
                      </tr>
                   </table>
                  <!--******************************** fin de la tabla con los datos del formulario ***************************-->

               </div>  
	 	    </td>
         </tr>
      </table>
    <!-- inicio borde esquinas redondas-->
	<div class='filoInferiorTabla'>&nbsp;</div> 
	<div id=nifty><B class=rbottom id='rbottom'><B class=r4></B><B class=r3></B><B class=r2></B><B class=r1></B></B></DIV>
	<!-- fin borde esquinas redondas-->
   </td>
  </tr> 
</table>