 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanEventoA" class="Clinica.GestionarEvento.ControlEventos" scope="session" /> <!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
    beanEventoA.setCn(beanSession.getCn());
    ArrayList resultaux=new ArrayList();
	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
%>


<table width="1070px"  align="center"   border="0" cellspacing="0" cellpadding="1"   >
 <tr>
   <td>
	  <!-- AQUI COMIENZA EL TITULO -->	
		 <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
			 <jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="Gesti�n del Riesgo " />
			 </jsp:include>
		 </div>	
		<!-- AQUI TERMINA EL TITULO -->
   </td>
 </tr> 
 <tr>
  <td>
     <table width="100%"  cellpadding="0" cellspacing="0" class="fondoTabla">
       <tr>
        <td>

 	 <div style="overflow:auto;height=600px" id="divContenido">   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
     <!--******************************** inicio de la tabla con los datos de busqueda ***************************-->	
	  <div id="divBuscar"  > 
             <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer" >
                    <tr><td width="99%" class="titulosCentrados">BUSCAR</td></tr>
             </table>  
			 <table width="100%" border="0"  cellpadding="0" cellspacing="0"  align="center">
                            <tr class="titulos" align="center">
							  <td width="30%">Proceso (Responsable)</td>
							  <td width="15%">Clase</td>                              
							  <td width="30%">eventos adversos del proceso</td>
							  <td width="25%" colspan="2">Area donde se presenta el evento</td>		
							</tr>
							<tr class="estiloImput">
							  <td ><select name="cmbBusproceso" size="1" id="cmbBusproceso" style="width:90%"   >	                                        
                                        <option value="00"></option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.proceso.CargarTodosLosProceso();	
												 ProcesoVO proceso;
												 for(int k=0;k<resultaux.size();k++){ 
													   proceso=(ProcesoVO)resultaux.get(k);
										  %>
										<option value="<%= proceso.getCodigo()%>"><%=proceso.getNombre()%></option>
												<%}%>						
 	                             </select>	                   
							  </td>
                              <td ><select name="cmbBusClaseEA" size="1" id="cmbBusClaseEA" style="width:80%"    onchange="
												cargarEAHospitalConClase('cmbBusproceso','00','cmbBusEventoProceso',this.options[this.selectedIndex].value,'<%= response.encodeURL("/clinica/paginas/accionesXml/cargarEventosAdversosClase_xml.jsp") %>');
									">	    
                                    <option value="00"></option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.claseEA.CargarClaseEA();	
												 ClaseEAVO p2;
												 for(int k=0;k<resultaux.size();k++){ 
													   p2=(ClaseEAVO)resultaux.get(k);
										  %>
										<option value="<%= p2.getCodigo()%>"><%= p2.getNombre()%></option>
												<%}%>						
 	                             </select>	
                                </td>                              
							  <td ><select name="cmbBusEventoProceso" size="1" id="cmbBusEventoProceso" style="width:99%"   >
							    <option value="00"></option>
						      </select></td>
							  <td colspan="2" ><select name="cmbBusUbicacionArea" size="1" id="cmbBusUbicacionArea" style="width:80%"   >
							    <option value="00"></option>
							    <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.cargarUbicacionArea();	
												 UbicacionAreaVO ubicArea;
												 for(int k=0;k<resultaux.size();k++){ 
													   ubicArea=(UbicacionAreaVO)resultaux.get(k);
										  %>
							    <option value="<%= ubicArea.getCodigo()%>"><%= ubicArea.getNombre()%></option>
							    <%}%>
							    </select>
                              </td>  
				  </tr>
						    <tr class="titulos"> 
                              <td colspan="1">Fecha desde&nbsp;&nbsp;&nbsp;&nbsp; Fecha hasta</td>	                   
							  <td colspan="1">Estado&nbsp;</td>
							  <td colspan="1">Identificaci&oacute;n o Nombre paciente</td> 
                              <td>Unidad que pertenece</td>                               
							  <td colspan="1">&nbsp;&nbsp;&nbsp; Id No evento</td>          
							</tr>								
							<tr class="estiloImput">   
                              <td colspan="1"><input type="text" value=""  size="15"  maxlength="10"  id="txtBusFechaDesde" name="txtBusFechaDesde"  />
                              <input type="text" value=""  size="15"  maxlength="10"  id="txtBusFechaHasta" name="txtBusFechaHasta"  /></td>	                   
							  <td colspan="1"><select name="cmbBusEstadoEvento" size="1" id="cmbBusEstadoEvento" style="width:90%"   >
							    <option value="00"></option>
							    <% resultaux.clear();
                                               resultaux=(ArrayList)beanAdmin.estadoEvento.cargarEstadoEvento();	//subtipos plan de accion: preventivo correctivo mejora
                                               EstadoEventoVO estEven;
                                               for(int k=0;k<resultaux.size();k++){ 
                                                    estEven=(EstadoEventoVO)resultaux.get(k);
                                            %>
							    <option value="<%= estEven.getIdEstado()%>" title="<%= estEven.getDefinicion()%>"><%= estEven.getDescripcion()%></option>
							    <%}%>
						      </select></td>  
                              <td><input type="text" value="" size="50"  maxlength="50"  id="txtBusIdPaciente" title="txt_buscat_paciente"   />  
                              <td align="center"><select size="1" id="cmbBusUnidadPertenece" style="width:80%" title="20"  >	                                        
                                        <option value="00"></option>
                                          <%     resultaux.clear();
                                                 resultaux=(ArrayList)beanAdmin.combo.cargar(28);	
                                                 ComboVO cmb0;
                                                 for(int k=0;k<resultaux.size();k++){ 
                                                       cmb0=(ComboVO)resultaux.get(k);
                                          %>
                                        <option value="<%= cmb0.getId()%>" title="<%= cmb0.getTitle()%>"><%= cmb0.getDescripcion()%></option>
                                                <%}%>						
                                 </select>	                   
                              </td>                                                                                 
							  <td><input type="text" value=""  size="15"  maxlength="15"  id="txtBusIdEvento" name="txtBusIdEvento" onfocus="limpiarFiltros()"  /></td> 
                            </tr>	
			</table>
			<div id="divListadoEventosA" style="height:500px; width:100%" >
				 <table id="listadoEventoA" class="scroll"></table>  
				<div id="pager" class="scroll" style="text-align:center;"></div>
			</div>
	  </div>
		  
	  <div id="divEditar" style="display:none; ">   
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer" >
                    <tr><td width="99%" class="titulosCentrados">EDITAR</td></tr>
                 </table>      
				 <table width="100%"    cellpadding="0" cellspacing="0"  align="center">
                    <tr class="titulos" align="center">
                      <td width="30%">Proceso</td>
                      <td width="15%">Clase</td>  
                      <td width="45%" >Eventos del proceso</td>
                      <td width="25%">Area donde se presenta</td>							  
                    </tr>
                    <tr class="estiloImput">
                      <td ><select name="cmbproceso" size="1" id="cmbproceso" style="width:90%"   disabled="disabled" onchange="
                                                cargarEAHospitalConClasePadre('cmbClaseEA','00','cmbEventoProceso',this.options[this.selectedIndex].value,'<%= response.encodeURL("/clinica/paginas/accionesXml/cargarEventosAdversosClase_xml.jsp") %>');
                            ">	                                        
                                    <option value="00">[Todos los procesos]</option>
                                      <%    resultaux.clear();
                                             resultaux=(ArrayList)beanAdmin.proceso.CargarTodosLosProceso();	
                                             ProcesoVO proc2;
                                             for(int k=0;k<resultaux.size();k++){ 
                                                   proc2=(ProcesoVO)resultaux.get(k);
                                      %>
                                    <option value="<%= proc2.getCodigo()%>"><%= proc2.getNombre()%></option>
                                            <%}%>						
                         </select>	                   
                      </td>
                              <td ><select name="cmbClaseEA" id="cmbClaseEA" size="1"  style="width:90%"    onchange="
                                                cargarEAHospitalConClase('cmbproceso','00','cmbEventoProceso',this.options[this.selectedIndex].value,'<%= response.encodeURL("/clinica/paginas/accionesXml/cargarEventosAdversosClase_xml.jsp") %>');
                                    ">	    
                                    <option value="00">[Todas las clases]</option>
                                          <%    resultaux.clear();
                                                 resultaux=(ArrayList)beanAdmin.claseEA.CargarClaseEA();	
                                                 ClaseEAVO p3;
                                                 for(int k=0;k<resultaux.size();k++){ 
                                                       p3=(ClaseEAVO)resultaux.get(k);
                                          %>
                                        <option value="<%= p3.getCodigo()%>"><%= p3.getNombre()%></option>
                                                <%}%>						
                                 </select>	
                                </td>                      
                      <td  ><select name="cmbEventoProceso" size="1" id="cmbEventoProceso" style="width:90%"   >

                      </select></td>
                      <td ><select name="cmbUbicacionArea" size="1" id="cmbUbicacionArea" disabled="disabled" style="width:90%"   >
                        <option value="00">[TODAS LAS AREAS]</option>
                        <%    resultaux.clear();
                                         resultaux=(ArrayList)beanAdmin.cargarUbicacionArea();	
                                         UbicacionAreaVO ubicAreaEdit;
                                         for(int k=0;k<resultaux.size();k++){ 
                                               ubicAreaEdit=(UbicacionAreaVO)resultaux.get(k);
                                  %>
                        <option value="<%= ubicAreaEdit.getCodigo()%>"><%= ubicAreaEdit.getNombre()%></option>
                        <%}%>
                        </select>
                      </td>      
				  </tr>
                      <tr class="titulos"> 
                         <td colspan="3" width="40%">
                              <table width="95%">
                                <tr>
                                  <td width="14%">C�digo Evento</td>
                                  <td>Descripci�n del Evento</td>
                                </tr>
                              </table>                         
                         </td>
                         <td colspan="1" align="center">Fecha evento</td>                      
                      </tr>									
                  <tr class="estiloImputIzq">   
                    <td colspan="3">
                          <table width="90%" >
                            <tr>
                              <td width="14%"><label  id="lblIdEvento">&nbsp;</label></td>
                              <td><label  id="lblDescripEvento">&nbsp;</label></td>
                            </tr>
                          </table>                         
                         </td>
                    <td  align="center"><label  id="lblFechaEvento">&nbsp;</label>
                    </td>                                                                                           
                  </tr>		
                  <tr class="titulos"> 
                         <td colspan="4" width="80%">
                              <table width="100%" >
                                <tr>
                                  <td>Descripci�n del Tratamiento</td>
                                </tr>
                              </table>                         
                         </td>	                   
                  </tr>		
                  <tr class="estiloImputIzq">   
                         <td colspan="4">
                          <table width="100%" >
                            <tr>
                              <td><label  id="lblTratamiento">&nbsp;</label></td>
                            </tr>
                          </table>                         
                         </td>
                  </tr>	
                  <tr class="titulos"> 
                    <td align="center">Nombre completo del paciente</td>
                    <td >Estado Evento</td>   
                    <td align="center" title="Segundo Actor quien gestiona y realiza la oportunidad de mejora">Responsable Gesti�n</td>                                        
                    <td title="">Recursos</td>                                	                   
                  </tr>                  
                  <tr class="estiloImput">   
                    <td colspan="1" align="center"><label  id="lblIdPaciente">&nbsp;</label>-<label  id="lblNomPaciente">&nbsp;</label></td>
                    <td colspan="1" align="center"><label  id="lblEstadoEvent">&nbsp;</label></td>   
				    <td colspan="1" align="center"><label  id="lblNomElavoroGestion">&nbsp;</label></td>
                    <td > <input name="btn_recursos" type="button" value="Ver Diagn�sticos y Medicamentos" onclick="buscarHC('listMedicaDiagnosticos','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp'); mostrar('divSublistadoConteo')" /></td>
                  </tr>	
                  <tr>
                    <td colspan="4">
                     <div id="divEditarFases" style="display:none; width:100%;" >   

                    <div id="tabsEventosEA">
                      <ul>    
                         <li><a href="#divGestionEA" onclick="activarTab('divGestionEA')"  title="ANALISIS DEL EVENTO"><center>ANALISIS DEL EVENTO</center></a></li> 
                         <li><a href="#divOportMejoraEA"   title="Actividades que se realizan ante el evento (Se obedece a un protocolo de atenci�n inmediata)"><center>ACCIONES CORRECTIVAS</center></a></li>                                                                              
						<li><a href="#divSeparador"><center>&nbsp;</center></a></li>                         

                         <li><a href="#divSeguimientoEA"  onclick="listadoTabla('listSeguimientoOportMejora')"  title="Seguimiento al evento adverso"><center>SEGUIMIENTO PLAN DE MEJORA</center></a></li>
                         <li><a href="#divResumenYConclus" onclick="activarTab('divResumenYConclus');listadoTabla('listNivelResponsabili')"  title="Conclusiones del evento adverso"><center>RESUMEN Y CONCLUSIONES</center></a></li>                                              
                         <li><a href="#divPlanEA" onclick="mostrar('divContenido2');BuscarrelacionPlanAccionEvento()"  title="Cuando el evento amerita por parte del administrador una oportunidad de mejora para minimizar la frecuencia"><center>OPORTUNIDAD DE MEJORA</center></a></li>                     
                      </ul>                         
                         
                      <div id="divGestionEA" style="height:250px" >  
                         <table width="100%">                  
                              <tr class="titulos"> 
                                <td colspan="2" width="20%" title="Una acci�n o circunstancia que reduce la probabilidad de presentaci�n del evento">Barrera y defensa</td>  
                                <td colspan="1" width="20%" title="FALLA POR ACCION: Se ejecutan acciones que no se deben realizar y no est�n previstas en los procesos
FALLA POR OMISI�N: Se omiten acciones que se deben realizar y est�n previstas en los procesos. ">Acci�n insegura</td>                                                                                                                                    
                                <td colspan="2" width="60%" title="Se refiere a la identificaci�n de los factores que contribuyeron a la presencia del evento (Pacientes, Tareas y tecnolog�a, Individuo, Equipo, Ambiente)">Origenes y Factores Contributivos</td>	
                                                                                  
                              </tr>								
                              <tr class="estiloImput"> 
                              <td colspan="2">
                                <select name="cmbBarreraYDefensa"  id="cmbBarreraYDefensa" style="width:90%"   >
							     <option value="00">[Toda barrera]</option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.CargarBarreraDefensa();	
												 BarreraDefensaVO barr;
												 for(int k=0;k<resultaux.size();k++){ 
													   barr=(BarreraDefensaVO)resultaux.get(k);
										  %>
								 <option value="<%= barr.getCodigo()%>" title="<%= barr.getDefinicion()%>"><%= barr.getNombre()%></option>
												<%}%>	
							    </select>                                                                            
                              </td> 
                              <td colspan="1">
                                <select name="cmbAccionInsegura"  id="cmbAccionInsegura" style="width:80%"   >
							       <option value="00">[Toda Acci�n]</option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.CargarAccionInsegura();	
												 AccionInseguraVO acci;
												 for(int k=0;k<resultaux.size();k++){ 
													   acci=(AccionInseguraVO)resultaux.get(k);
										  %>
								   <option value="<%= acci.getCodigo()%>"><%= acci.getNombre()%></option>
												<%}%>	
							    </select>                              
                              </td>                             
                                
                                <td colspan="2">
                                 <select name="cmbOrigenesFactContrib"  id="cmbOrigenesFactContrib" style="width:80%"  >
							     <option value="00">[Todos origen factores]</option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.CargarOrigenFactorContribut();	
												 OrigenFactorContributVO orig;
												 for(int k=0;k<resultaux.size();k++){ 
													   orig=(OrigenFactorContributVO)resultaux.get(k);
										  %>
								   <option  value="<%= orig.getCodigo()%>" title="<%= orig.getNombre()%>"><%= orig.getNombre()%></option>
												<%}%>
							     </select> 
                                  <a href="#" onclick="agregarElementoListGral('listFactContribu');" title="Adicionar"><img src="/clinica/utilidades/imagenes/acciones/edit_add.png" id="imgAdd" height="18" /></a>&nbsp;&nbsp;
                                  <a href="#" onclick="quitarDatosListasST2('listFactContribu');" title="Eliminar"><img src="/clinica/utilidades/imagenes/acciones/edit_remove.png" height="18" id="imgDel" /></a>&nbsp;&nbsp;
                                </td>	                                        
                              </tr>	
                              <tr class="titulos"> 
                                <td colspan="1" title="SAFETY: Hace referencia a errores humanos asociados al equipo de salud que brinda el cuidado al paciente
Ej: Ca�da de paciente
SECURITY: Hace referencia a los procesos de seguridad de los establecimientos
Ej: P�rdida de celular o de pr�tesis dental">Ambito</td>   
                                <td colspan="1" title="Si: La gran mayor�a de los eventos adversos son prevenibles, el evento se pudo haber evitado de haberse tomado las medidas establecidas.
No: Son situaciones que pese a haberse tomado las medidas establecidas producen el evento">Prevenible?</td> 
                                <td colspan="1" title="DECISION GRUPAL: Hace referencia a que el equipo de trabajo toma decisiones erradas
DECISION INDIVIDUAL: Es un acto inseguro 
NO APLICA: Cuando el evento no esta relacionado a una decisi�n del equipo asistencial">Organizaci�n y cultura</td>                            
                                <td colspan="2" rowspan="5" width="35%">
                                   <div id="divdatosPersonasPlanAccion" style=" height:100px; width:100%">
                                       <table align="center" id="listFactContribu" class="scroll" cellpadding="0" cellspacing="0"></table>
                                   </div>    
                                </td> 
                                 
                              </tr>	
                               <tr class="estiloImput">
                                <td colspan="1">
                                <select name="cmbAmbito"  id="cmbAmbito" style="width:80%"   >
							     <option value="00">[Todo �mbito]</option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.CargarAmbito();	
												 AmbitoVO ambit;
												 for(int k=0;k<resultaux.size();k++){ 
													   ambit=(AmbitoVO)resultaux.get(k);
										  %>
								 <option value="<%= ambit.getCodigo()%>" title="<%= ambit.getDefinicion()%>"><%= ambit.getNombre()%></option>
												<%}%>	
							    </select> 
                                </td>
                               
                                <td colspan="1">
                                <select name="cmbPrevenible"  id="cmbPrevenible" style="width:80%"   >
							     <option value="00">[Todos]</option>
                                 <option value="SI">SI</option>
                                 <option value="NO">NO</option>
							    </select>
                                </td>
                                <td>
                                <select name="cmbOrganizCultura"  id="cmbOrganizCultura" style="width:80%"   >
                                   <option value="00">[Todos]</option>
										  <%    resultaux.clear();
												 resultaux=(ArrayList)beanAdmin.CargarOrganizacionCultura();	
												 OrganizacionCulturaVO orgCul;
												 for(int k=0;k<resultaux.size();k++){ 
													   orgCul=(OrganizacionCulturaVO)resultaux.get(k);
										  %>
								   <option  value="<%= orgCul.getCodigo()%>" title="<%= orgCul.getNombre()%>"><%= orgCul.getNombre()%></option>
												<%}%>
							    </select> 
                                </td> 
                              </tr>	
                              <tr>
                                <td colspan="1">&nbsp;</td>   
                                <td colspan="1">&nbsp;</td> 
                                <td colspan="1">&nbsp;</td>                            
                              </tr>
                              <tr>
                                <td colspan="1">&nbsp;</td>   
                                <td colspan="1">&nbsp;</td> 
                                <td colspan="1">&nbsp;</td>                            
                              </tr>                              
                         </table> 
                            <table border="1" align="center">
                              <tr>
                               <td align="center" colspan="4">
                                  <div id="divBotonGuardGestionEA" style="width:100%;  display:none"  >  
                                    <% if(beanSession.usuario.preguntarMenu("b3121") ){%>  
                                    <CENTER><input title="boton No=b3121" value="Guardar Gesti�n" name="G" onclick="guardarPestanasListSTV('listFactContribu','')" type="button" />                                                         </CENTER>
                                   <% }%> 
                                  </div>  
                               </td>
                              </tr>  
                            </table>                           
                                                 
                      </div> 
                      <div id="divOportMejoraEA" style="height:250px" >                          
                            <table width="100%" height="125" >
                              <tr class="titulos">
                                <td width="30%">Actividad-Tarea</td>
                                <td width="5%">Fecha tarea</td>	                                
                                <td width="10%">Personas responsables</td>
                                <td width="5%">Costo</td>	                                
                                <td width="2%">Horas</td>	
                                <td width="5%">&nbsp;</td>
                              </tr>
                              <tr>
                                <td >
                                <textarea name="txtAccionesOportunidadMejora" id="txtAccionesOportunidadMejora" maxlength="285" cols="1"  style="width:97%" ></textarea>
                                </td>
                                <td align="center">
                                    <input type="text" value=""  size="11"  maxlength="10"  id="txtFechaCuandoOportMejora" name="txtFechaCuandoOportMejora"  />
                                </td>   
                                <td align="center" > <select name="cmbQuienRealizaoportMejora" id="cmbQuienRealizaoportMejora"  style=" width:90%" >
                                    <option value="00"></option>
                                      <%     resultaux.clear();
                                             resultaux=(ArrayList)beanAdmin.combo.cargar(2);	
                                             ComboVO cmb;
                                             for(int k=0;k<resultaux.size();k++){ 
                                                   cmb=(ComboVO)resultaux.get(k);
                                      %>
                                    <option value="<%= cmb.getId()%>" title="<%= cmb.getTitle()%>"><%= cmb.getDescripcion()%></option>
												  <%}%>	
                                    </select>                                            
                                </td>
                                <td width="5%">$&nbsp;<input type="text" value="0"  size="11"  maxlength="10"  id="txtCostos"  placeholder="0"  onKeyPress="javascript:return teclearsoloDigitos(event); " /></td>   
								<td align="center" > <select id="cmbHoras" >
                                      <%     resultaux.clear();
                                             resultaux=(ArrayList)beanAdmin.combo.cargar(3);	
                                             ComboVO cmb1;
                                             for(int k=0;k<resultaux.size();k++){ 
                                                   cmb1=(ComboVO)resultaux.get(k);
                                      %>
                                    <option value="<%= cmb1.getId()%>" title="<%= cmb1.getTitle()%>"><%= cmb1.getDescripcion()%></option>
												  <%}%>	
                                    </select>                                            
                                </td>                                                            
                                <td>
                                  <center>
                                        <a href="#" onclick="agregarElementoListGral('listPersonasOportMejora');" title="Adicionar"><img src="/clinica/utilidades/imagenes/acciones/edit_add.png" id="imgAdd" height="18" /></a>&nbsp;&nbsp;
                                        <a href="#" onclick="quitarDatosListasST2('listPersonasOportMejora');" title="Eliminar"><img src="/clinica/utilidades/imagenes/acciones/edit_remove.png" height="18" id="imgDel" /></a>&nbsp;&nbsp;
                                  <a href="#" onclick="modifiDatosListasSTGral('listPersonasOportMejora');" title="Modificar"><img src="/clinica/utilidades/imagenes/acciones/modificar.png" height="18" id="imgUpd" /></a>&nbsp;&nbsp;                                                                
                                  </center>
                                </td>    
                              </tr>
                              <tr>
                                <td colspan="6" >
                                   <div id="divdatosPersonasPlanAccion" style=" height:180px; width:98%">
                                       <table align="center" id="listPersonasOportMejora" class="scroll" cellpadding="0" cellspacing="0"></table>
                                   </div>   
                                </td>    
                              </tr>  
                            </table> 
                            <table align="center">
                              <tr>
                               <td align="center" colspan="4">
                                  <div id="divBotonGuardGestionEA2" style="width:100%; display:none"   >  
                                    <% if(beanSession.usuario.preguntarMenu("b3121") ){%>  
                                    <CENTER><input value="Guardar Gesti�n" name="G" title="b3121 -Segundo actor" onclick="guardarPestanasListSTV('listFactContribu','')" type="button" />  </CENTER>
                                   <% }%> 
                                  </div>  
                               </td>
                              </tr>  
                            </table>                                                                      
                      </div> 
                      <div id="divSeparador"></div> 
                      <div id="divSeguimientoEA" style="height:250px" > 
                            <table width="100%" height="80" >                          
                              <tr class="titulos" >
                                   <td width="10%" title="Pertinencia a laOportunidad de mejora">pertinencia</td>
                                   <td width="10%" title="Oportunidad al tiempo de ejecuci�n">oportunidad</td>                                   
                                   <td width="35%">Actividad - Tarea</td> 
                                   <td width="35%" colspan="2">Seguimiento a la Tarea</td>                                
                                   <td width="3%">&nbsp;</td>                                                                                                                                                                                                             
                              </tr>
                              <tr class="estiloImput" height="7">
                                     <td><select name="cmbOportMejora" id="cmbOportMejora"  style=" width:80%" >
                                         <option value="00">Sin escojer</option>
                                         <option value="SI">Si</option>
                                         <option value="NO">No</option>                                                           
                                        </select>                               
                                      </td>
                                     <td><select name="cmbTiempoEjecucion" id="cmbTiempoEjecucion"  style=" width:80%" >
                                         <option value="00">Sin escojer</option>
                                         <option value="01">Oportuno</option>
                                         <option value="02">Permanente</option>  
                                         <option value="03">Constante</option>   
                                         <option value="04">Inmediato</option> 
                                         <option value="05">Diario</option>                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                                        </select>                               
                                      </td>                                      
                                      <td ><textarea name="txtTareaOport" id="txtTareaOport" disabled="disabled"  maxlength="285" cols="2"  style="width:99%" ></textarea>                                            
                                      <td colspan="2">
                                        <textarea name="txtTareaOportSeguimient"  id="txtTareaOportSeguimient" maxlength="500" cols="2" style="width:99%" ></textarea>
                                      </td>  
                                      <td>
                                            <center>
                                                  <a href="#" onclick="modifiDatosListasSTGral('listSeguimientoOportMejora');" title="Modificar"><img src="/clinica/utilidades/imagenes/acciones/modificar.png" height="18" id="imgUpd" /></a>&nbsp;&nbsp;
                                            </center>
                                      </td>                                           
                               </tr>  
                             </table> 
                                       <div id="divdatosPlanSeguimientoEA" style="overflow:scroll;  height:170px; width:100%">
                                               <table id="listSeguimientoOportMejora" class="scroll" cellpadding="0" cellspacing="0"></table>
                                              <!-- <div id="pagerlistPlanAccion" class="scroll" style="overflow:scroll; text-align:center; " >  </div>              -->                                
                                       </div>   
                                  <div id="divBotonseguimientoOporMejora" style="width:100%; display:none"   >  
                                    <% if(beanSession.usuario.preguntarMenu("b3122") ){%>  
                                    <CENTER><input title="id=b3122"   value="Guardar Seguimiento Oportunidad" name="G" onclick="guardarPestanasListSTV('listSeguimientoOportMejora','')" type="button" /></CENTER>
                                   <% }%> 
                                  </div>   
                      </div> 
                      <div id="divResumenYConclus" style="height:500">
                            <table width="100%" height="155" >
                              <tr class="titulos">
                                <td width="50%">Descripci�n responsabilidad</td>
                                <td width="35%">Niveles de responsabilidad</td>
                                <td width="10%">&nbsp;</td>
                              </tr>
                              <tr>
                                <td >
                                <textarea name="txtDescResponsabili" id="txtDescResponsabili" maxlength="1000" cols="1"  style="width:97%" ></textarea>
                                </td>
                                <td align="center" > <select name="cmbNivelResponsabili" id="cmbNivelResponsabili"  style=" width:90%" >
                                    <option value="00">[Escoja nivel]</option>
                                    <%    resultaux.clear();
                                                     resultaux=(ArrayList)beanAdmin.CargarNivelResponsabilidad();	
                                                     NivelResponsabilidadVO nivRes;
                                                     for(int k=0;k<resultaux.size();k++){ 
                                                           nivRes=(NivelResponsabilidadVO)resultaux.get(k);
                                              %>
                                    <option value="<%= nivRes.getCodigo()%>" title="<%= nivRes.getDefinicion()%>"><%= nivRes.getNombre()%></option>
                                    <%}%>
                                    </select>                                            
                                </td>
                                <td>
                                  <center>
                                        <a href="#" onclick="agregarElementoListGral('listNivelResponsabili');" title="Adicionar"><img src="/clinica/utilidades/imagenes/acciones/edit_add.png" id="imgAdd" height="18" /></a>&nbsp;&nbsp;
                                        <a href="#" onclick="quitarDatosListasST2('listNivelResponsabili');" title="Eliminar"><img src="/clinica/utilidades/imagenes/acciones/edit_remove.png" height="18" id="imgDel" /></a>&nbsp;&nbsp;
                                  <a href="#" onclick="modifiDatosListasSTGral('listNivelResponsabili');" title="Modificar"><img src="/clinica/utilidades/imagenes/acciones/modificar.png" height="18" id="imgUpd" /></a>&nbsp;&nbsp;                                                                
                                  </center>
                                </td>    
                              </tr>
                              <tr>
                                <td colspan="4" >
                                   <div id="divNivelResponsabili" style=" height:140px; width:98%">
                                       <table align="center" id="listNivelResponsabili" class="scroll" cellpadding="0" cellspacing="0"></table>
                                   </div>   
                                </td>    
                              </tr>  
                            </table> 
                      
                            <table width="100%">
                                <tr class="titulos">
                                  <td width="80%">Conclusiones</td> <td width="20%">Gravedad de la lesi�n</td> 
                                </tr>
                                <tr class="estiloImput">
                                  <td><textarea type="text"  value="" id="txtConclusiones" name="txtConclusiones"  style="width:100%"  size="700"  maxlength="700"   /></textarea></td>
                                  <TD><select name="cmbCritico" size="1" id="cmbCritico"  style="width:50%"   >
                                      <option value="00"></option> 
                                      <option value="0">Leve</option>                                                                                                                                                        
                                      <option value="1">Moderada</option>
                                      <option value="2">Grave</option>
                                      <option value="3">Severa</option>

                                     </select></TD>
                                </tr>                                      
                            </table> 
                                   <div id="divBotonResumenConclusiones" style="width:100%; display:none"   >  
                                    <% if(beanSession.usuario.preguntarMenu("b3122") ){%>  
                                    <CENTER><input  value="Guardar resumen y conclusiones" name="G" onclick="guardarPestanasListSTV('listNivelResponsabili','')" type="button" /></CENTER>
                                   <% }%>  
                                   </div>                                                                 
                      </div>   
                      <div id="divPlanEA"  style="height:400">

                          <div id="divEditar2"  style="display:block" style="height:400" >   
                                       <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                                         <tr class="titulos" align="center">
                                            <td width="25%" >Proceso</td>
                                            <td colspan="2" title="Es la oportunidad de mejora, Ejemplo:  Remodelaci�n del Auditorio Principal">Oportunidad de Mejora</td>
                                            <td width="15%" colspan="1">&nbsp;</td>                          
                                         </tr>
                                         <tr class="estiloImput">
                                            <td  ><select name="cmbprocesoEA" size="1" id="cmbprocesoEA" style="width:60%"  >	                                        
                                                      <option value="00">[TODO PROCESO]</option>
                                                        <%    resultaux.clear();
                                                               resultaux=(ArrayList)beanAdmin.CargarProcesoAcreditacion();	
                                                               ProcesoVO procesoEdit;
                                                               for(int k=0;k<resultaux.size();k++){ 
                                                                     procesoEdit=(ProcesoVO)resultaux.get(k);
                                                        %>
                                                      <option value="<%= procesoEdit.getCodigo()%>"><%= procesoEdit.getNombre()%></option>
                                                              <%}%>						
                                               </select>&nbsp;&nbsp;<label  id="lblIdEventoEA"></label>	<img src="/clinica/utilidades/imagenes/acciones/modificar.png" title="Con este c�digo de pl�n se abrir� la ventana de Oportunida de Mejora para continuar la gesti�n" width="20" height="20" onclick="abrirVentanasOportunidadMejora()" />                   
                                            </td>
                                            <td colspan="2" >
                                               <input type="text" value=""  size="100" style="width:99%"  maxlength="200"  id="txtAspectoMejorar" name="txtAspectoMejorar"  />&nbsp;                                                     
                                            </td> 
                                            <td align="center">&nbsp;</td>          
                                        </tr>
                                        <tr class="titulos"> 
                                            <td title="Se refiere a">Tipo Pl�n</td>
                                            <td  colspan="2">priorizaci�n</td>
                                            <td>&nbsp;</td>                          
                                        </tr>
                                         <tr class="estiloImputIzq"> 
                                            <td align="center"><select name="cmbTipoPlan" size="1" id="cmbTipoPlan" style="width:70%"   >
                                                      <option value="00">[Todos]</option>
                                                      <%    resultaux.clear();
                                                                       resultaux=(ArrayList)beanAdmin.CargarTipoPlan("aam");	
                                                                       TipoPlanVO tip;
                                                                       for(int k=0;k<resultaux.size();k++){ 
                                                                             tip=(TipoPlanVO)resultaux.get(k);
                                                      %>
                                                      <option value="<%= tip.getCodigo()%>"><%= tip.getNombre()%></option>
                                                      <%}%>
                                                      </select></td>
                                            <td colspan="2">
                                                <table width="100%">
                                                  <tr>
                                                    <td width="25%" align="center" title="Riesgo alque se expone el usuario y/o la instituci�n y/o los clientes internos si no se lleva a cabo el mejoramiento">Riesgo:
                                                        <select name="cmbRiesgo" size="1" id="cmbRiesgo" style="width:35%" onchange="calcularTotalPriorizacion()"   >	                                        
                                                           <option value="00">--</option>
                                                           <option value="1" title="califique 1 o 2 cuando la instituci�n, el usuario y/o los clientes internos no corren ning�n riesgo o existe un riesgo leve si no se efectua la acci�n de mejoramiento ">1</option> 
                                                           <option value="2" title="califique 1 o 2 cuando la instituci�n, el usuario y/o los clientes internos no corren ning�n riesgo o existe un riesgo leve si no se efectua la acci�n de mejoramiento ">2</option> 
                                                           <option value="3" title="Califique 3 cuando la institucion, el usuario y/o los clientes internos corren un riesgo medio si no se efect�a la acci�n de mejoramiento">3</option>  
                                                           <option value="4" title="Califique 4 o 5 cuando la institucion, el usuario y/o los clientes internos corren un riesgo alto o se puede presentar un evento adverso o incidente si no se efect�a la acci�n de mejoramiento">4</option>  
                                                           <option value="5" title="Califique 4 o 5 cuando la institucion, el usuario y/o los clientes internos corren un riesgo alto o se puede presentar un evento adverso o incidente si no se efect�a la acci�n de mejoramiento">5</option>                                                                                                                                                                                                                                                                                                                                       
                                                        </select>                                  
                                                    </td>
                                                    <td width="25%" align="center" title="Posible impacto econ�mico de no realizar el mejoramiento">Costo:
                                                        <select name="cmbCosto" size="1" id="cmbCosto" style="width:35%"  onchange="calcularTotalPriorizacion()"   >	                                        
                                                           <option value="00">--</option>
                                                           <option value="1" title="Califique 1 o 2 si al no realizarse el mejoramiento no se afectan o se afectan levemente las finanzas y la imagen de la instituci�n">1</option> 
                                                           <option value="2" title="Califique 1 o 2 si al no realizarse el mejoramiento NO se afectan o se afectan levemente las finanzas y la imagen de la instituci�n">2</option> 
                                                           <option value="3" title="Califique 3 si al no realizarse el mejoramiento se afectan moderadamente las finanzas y la imagen de la instituci�n">3</option>  
                                                           <option value="4" title="Califique 4 o 5 si al no realizarse el mejoramiento se afectan notablemente las finanzas y la imagen de la instituci�n">4</option>  
                                                           <option value="5" title="Califique 4 o 5 si al no realizarse el mejoramiento se afectan notablemente las finanzas y la imagen de la instituci�n">5</option>                                                                                                                                                                                                                                                                                                                                       
                                                        </select>                                  
                                                    </td>
                                                    <td width="25%" align="center" title="Alcance del mejoramiento / cobertura">Volumen:
                                                        <select name="cmbVolumen" size="1" id="cmbVolumen" style="width:35%"  onchange="calcularTotalPriorizacion()"   >	                                        
                                                           <option value="00">--</option>
                                                           <option value="1" title="Califique 1 o 2 si la ejecuci�n del mejoramiento no tendr�a una cobertura o alcance amplio en la instituci�n o en los usuarios internos o externos o el impacto es leve">1</option> 
                                                           <option value="2" title="Califique 1 o 2 si la ejecuci�n del mejoramiento no tendr�a una cobertura o alcance amplio en la instituci�n o en los usuarios internos o externos o el impacto es leve">2</option> 
                                                           <option value="3" title="Califique 3 si la ejecuci�n de la acci�n de mejoramiento tendr�a una cobertura o alcance medio en la instituci�n o en los usuarios internos o externos">3</option>  
                                                           <option value="4" title="Califique 4 o 5 si la ejecuci�n de la acci�n de mejoramiento tendr�a una cobertura o alcance amplio en la instituci�n o en los usuarios internos o externos">4</option>  
                                                           <option value="5" title="Califique 4 o 5 si la ejecuci�n de la acci�n de mejoramiento tendr�a una cobertura o alcance amplio en la instituci�n o en los usuarios internos o externos">5</option>                                                                                                                                                                                                                                                                                                                                       
                                                        </select>                                  
                                                    </td>
                                                    <td width="25%" align="center" bgcolor="#99CCFF">TOTAL=&nbsp;<label  id="lblTotPrioriza">0</label>                                 
                                                    </td>                                  
                                                  </tr>
                                                </table>
                                            </td> 
                                            <td colspan="1" align="center">&nbsp;</td> 
                                                                                                                       
                                        </tr>
                                        <tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>								
                                        
                                  </table>
                      
                            </div><!-- divEditar-->	  
                           <div id="divBotonAspectoaMejorar" style="width:100%; height:500; display:block"   >  
                            <% if(beanSession.usuario.preguntarMenu("b3122") ){%>  
                            <CENTER><input  value="GUARDAR" name="G" onclick="guardarJuan('administrarPlanMejoraEA','/clinica/paginas/accionesXml/guardar_xml.jsp')" type="button" /></CENTER>
                           <% }%>  
                           </div>                                                                 

                      </div>  
                                         
                    </td>   
                  </tr>    		
								  
			</table>

      </div><!-- divEditar-->	
    </div><!-- div contenido-->
        </td>
       </tr>
      </table>
      <input type="hidden"  id="IdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>"/>
    
   </td>
 </tr> 
</table>

      <div id="divSublistadoConteo"  onmouseover="mostrar('divSublistadoConteo')" onMouseOut="ocultar('divSublistadoConteo2')" style="position:absolute; display:none; background-color:#699; top:200px; left:200px;  z-index:999">
            <table width="100%"  border="1" >
                <tr class="titulos" >
                   <td width="98%">Diagnosticos por Medicos</td> 
                   <td width="2%" > <input name="btn_recursos_ocultar" type="button" value="Cerrar" onclick=" ocultar('divSublistadoConteo')" /></td>
                <tr>                         
                <tr class="estiloImput" >
                   <td colspan="2" >                
                       <div id="divMedicaDiagnosticos" style="height:250px">                          
                              <table width="100%" height="125" >
                                <tr>
                                  <td colspan="5" >
                                     <div id="divMedicaDiagnosticos" style=" height:180px; width:98%">
                                         <table align="center" id="listMedicaDiagnosticos" class="scroll" cellpadding="0" cellspacing="0"></table>
                                     </div>   
                                  </td>    
                                </tr>  
                              </table> 
                       </div>  
                   </td>     
               <tr>  
            </table>                             
       </div>
       
     
       
       
       
       
