<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="Sgh.Utilidades.Conexion"%>
<%@ page import="Sgh.Utilidades.ConnectionClinica"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>

<%
    ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");

    try {
        Connection conexion = iConnection.getConnection();
        Conexion crystal = new Conexion(iConnection.getConnection());

        String sql = crystal.traerElQuery(Integer.parseInt(request.getParameter("idQuery"))).toString();

        PreparedStatement pstmt = conexion.prepareStatement(sql);

        Map<String, String[]> parametrosSQL = request.getParameterMap();

        for (Map.Entry<String, String[]> entry : parametrosSQL.entrySet()) {
            String nombreParametro = entry.getKey();
            String[] valoresParametro = entry.getValue();

            if (nombreParametro.equals("parametros[]")) {
                int aux = 1;
                for (String valor : valoresParametro) {
                    pstmt.setString(aux, valor);
                    aux += 1;
                }
            }
        }

        ResultSet resultado = pstmt.executeQuery();

        out.print("[");
        boolean firstRow = true;

        while (resultado.next()) {
            if (!firstRow) {
                out.print(",");
            } else {
                firstRow = false;
            }

            out.print("{");
            ResultSetMetaData metaData = resultado.getMetaData();
            int columnCount = metaData.getColumnCount();

            for (int i = 1; i <= columnCount; i++) {
                String columnName = metaData.getColumnName(i);
                Object value = resultado.getObject(i);

                out.print("\"" + columnName + "\":");

                // Manejar valores nulos
                if (value == null) {
                    out.print("null");
                } else {
                    // Escapar comillas y agregar valor
                    out.print("\"" + value.toString().replace("\"", "\\\"") + "\"");
                }

                // Agregar coma entre pares clave-valor
                if (i < columnCount) {
                    out.print(",");
                }
            }

            out.print("}");
        }

        out.print("]");

        resultado.close();
        pstmt.close();
    } catch (Exception e) {
        e.printStackTrace();
        // Manejar excepciones según tus necesidades
    }
%>
