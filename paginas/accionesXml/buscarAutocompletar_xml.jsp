<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<%@ page contentType="text/xml"%>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="conexion" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>
<rows>
    <%

        Fecha fecha = new Fecha();
        Conexion cn = new Conexion(conexion.getConnection());
        
        ControlAdmin beanAdmin = new ControlAdmin();
        beanAdmin.setCn(cn);

        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
        java.util.Date date = cal.getTime();
        java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("###,##0.00");
        ArrayList resultaux = new ArrayList();

        if (request.getParameter("idQuery") != null) {

            if (request.getParameter("parametro1") == null) {
                resultaux = (ArrayList) beanAdmin.autocomplete.cargarDatosAutocompletar(Integer.parseInt(request.getParameter("idQuery")));
                AutocompleteVO parametro = new AutocompleteVO();
                int i = 0;
                while (i < resultaux.size()) {
                    parametro = (AutocompleteVO) resultaux.get(i);
                    i++;
    %><item> 
        <seguimiento> 
            <cod><![CDATA[<%= parametro.getId()%>]]></cod>                   
            <text><![CDATA[<%= parametro.getDescripcion()%>]]></text>   
        </seguimiento>
    </item>    
    <%
        }
    } else {

        beanAdmin.autocomplete.setParametro1(request.getParameter("parametro1"));
        resultaux = (ArrayList) beanAdmin.autocomplete.cargarDatosAutocompletarParametro1(Integer.parseInt(request.getParameter("idQuery")));
        AutocompleteVO parametro = new AutocompleteVO();
        int i = 0;
        while (i < resultaux.size()) {
            parametro = (AutocompleteVO) resultaux.get(i);
            i++;
    %><item> 
        <seguimiento> 
            <cod><![CDATA[<%= parametro.getId()%>]]></cod>                   
            <text><![CDATA[<%= parametro.getDescripcion()%>]]></text>   
        </seguimiento>
    </item>    
    <%
                }

            }

        }

    %>
</rows>            