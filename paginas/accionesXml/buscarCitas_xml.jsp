<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.Presentacion.HistoriaClinica.*" %>    
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanCitas" class="Clinica.GestionAtUsuario.ControlAtUsuario" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanHC" class="Clinica.GestionHistoriaC.ControlHistoriaC" scope="session" /> <!-- instanciar bean de session -->    

    
    

<rows>
<%
   Fecha fecha = new Fecha();
   beanCitas.setCn(beanSession.getCn());
   beanHC.setCn(beanSession.getCn());
   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();
   ArrayList resultDocs=new ArrayList();
   if(request.getParameter("accion")!=null ){

    
   //	---------------------------------------------------------------------------------------- buscar ventanas jquery
	 
	if(request.getParameter("accion").equals("listadoCitas")){
		beanCitas.citas.setMes(request.getParameter("mes"));
        beanCitas.citas.setAnio(request.getParameter("anio"));	
        beanCitas.citas.setJornada(request.getParameter("tipoJornada"));	
		beanCitas.citas.setEstadoExitoCita(request.getParameter("EstadoExitoCita"));
		beanCitas.citas.setIdProfesional(request.getParameter("idProfesional"));
		beanCitas.citas.setIdResponsable(beanSession.usuario.getIdentificacion());	
		
		//System.out.println("setNoIdElavoro "+beanSession.usuario.getIdentificacion());
        
		resultaux=(ArrayList)beanCitas.asignaBuscarPag(request.getParameter("accion"));	
	   
		CitasVO parametro=new CitasVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(CitasVO)resultaux.get(i);
				   i++;	
			%>
				   <indicador> 
                       <fecha><![CDATA[<%=parametro.getFecha()%>]]></fecha>   
                       <diaf><![CDATA[<%=parametro.getDiaF()%>]]></diaf>   
                       <num_dia><![CDATA[<%=parametro.getNumDia()%>]]></num_dia>  
                       <tipoAgenda><![CDATA[<%=parametro.getTipoAgenda()%>]]></tipoAgenda>            
                       <planeada><![CDATA[<%=parametro.getPlaneada()%>]]></planeada>                         
                       <idPaciente><![CDATA[<%=parametro.getIdPaciente()%>]]></idPaciente>                          
                       <nombrePaciente><![CDATA[<%=parametro.getNombrePaciente()%>]]></nombrePaciente>   
                       <estadoCita><![CDATA[<%=parametro.getEstadoCita()%>]]></estadoCita>   
                       <desEtadoTarea><![CDATA[<%=parametro.getDesEtadoCita()%>]]></desEtadoTarea>  
                       <tipoCita><![CDATA[<%=parametro.getTipoCita()%>]]></tipoCita>   
                       <title><![CDATA[<%=parametro.getTitle()%>]]></title>                          

				   </indicador>
			<%
		}
	}	
if(request.getParameter("accion").equals("listadoCitasHoyVigilante")){
		beanCitas.citas.setIdProfesional(request.getParameter("idProfesional"));
		
		//System.out.println("setNoIdElavoro "+beanSession.usuario.getIdentificacion());
        
		resultaux=(ArrayList)beanCitas.asignaBuscarPag(request.getParameter("accion"));	
	   
		CitasVO parametro=new CitasVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(CitasVO)resultaux.get(i);
				   i++;	
			%>
				   <indicador> 
                       <IdCita><![CDATA[<%=parametro.getIdCita()%>]]></IdCita>   
                       <EstadoCita><![CDATA[<%=parametro.getEstadoCita()%>]]></EstadoCita>   
                       <DesEtadoCita><![CDATA[<%=parametro.getDesEtadoCita()%>]]></DesEtadoCita>  
                       <TipoCita><![CDATA[<%=parametro.getTipoCita()%>]]></TipoCita>            
                       <Fecha><![CDATA[<%=parametro.getFecha()%>]]></Fecha>                         
                       <Jornada><![CDATA[<%=parametro.getJornada()%>]]></Jornada>                          
                       <IdTurno><![CDATA[<%=parametro.getIdTurno()%>]]></IdTurno>   
                       <HoraCita><![CDATA[<%=parametro.getHoraCita()%>]]></HoraCita>   
                       <IdPaciente><![CDATA[<%=parametro.getIdPaciente()%>]]></IdPaciente>  
                       <NombrePaciente><![CDATA[<%=parametro.getNombrePaciente()%>]]></NombrePaciente>   
                       <IdProfesional><![CDATA[<%=parametro.getIdProfesional()%>]]></IdProfesional>   
                       <NomProfesional><![CDATA[<%=parametro.getNomProfesional()%>]]></NomProfesional>   
                       <CodTipoServicio><![CDATA[<%=parametro.getCodTipoServicio()%>]]></CodTipoServicio>  
                       <DesTipoServicio><![CDATA[<%=parametro.getDesTipoServicio()%>]]></DesTipoServicio>   
                       <FechaAsistencia><![CDATA[<%=parametro.getFechaAsistencia()%>]]></FechaAsistencia>   
                       <Asistencia><![CDATA[<%=parametro.getAsistencia()%>]]></Asistencia>                                                 
				   </indicador>
			<%
		}
	}	
    else if(request.getParameter("accion").equals("listCitasHoy")){	
	System.out.println("IdPaciente="+request.getParameter("IdPaciente"));
	System.out.println("idProfesional="+request.getParameter("idProfesional"));	
	
   			 if(request.getParameter("IdPaciente").equals(""))
		           beanCitas.citas.setIdPaciente("%");				
		     else  beanCitas.citas.setIdPaciente(request.getParameter("IdPaciente"));				

   			 if(request.getParameter("idProfesional").equals("00"))
		           beanCitas.citas.setIdProfesional("%");							 
		     else  beanCitas.citas.setIdProfesional(request.getParameter("idProfesional"));							 		   
				
		     
			 if(request.getParameter("BusFechaDesde").equals(""))
			        beanCitas.citas.setFechaDesde("01/01/1900");
			 else   beanCitas.citas.setFechaDesde(request.getParameter("BusFechaDesde"));
			 if(request.getParameter("BusFechaHasta").equals(""))
			        beanCitas.citas.setFechaHasta("01/01/1900");
			 else   beanCitas.citas.setFechaHasta(request.getParameter("BusFechaHasta"));
			 
			 
			 beanCitas.citas.setFechaHasta(request.getParameter("BusFechaHasta"));			 			 
			 
			 beanCitas.setPage(request.getParameter("page"));  // paginas que se pide
			 beanCitas.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 beanCitas.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanCitas.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
		     resultaux=(ArrayList)beanCitas.asignaBuscarPag(request.getParameter("accion"));	
			 CitasVO parametro=new CitasVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanHC.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(CitasVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[<%= i %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getIdCita() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getEstadoCita() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getDesEtadoCita() %>]]></cell>                                  
                              <cell><![CDATA[<%= parametro.getTipoCita() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getFecha() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getJornada() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getIdTurno() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getHoraCita() %>]]></cell>                                  
                              <cell><![CDATA[<%= parametro.getIdPaciente() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getNombrePaciente() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getIdProfesional() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getNomProfesional() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getCodTipoServicio() %>]]></cell>                                  
                              <cell><![CDATA[<%= parametro.getDesTipoServicio() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getFechaAsistencia() %>]]></cell>                                                                                                                                                       
                              <cell><![CDATA[<%= parametro.getAsistencia() %>]]></cell>                                                                                                                                                                                     
                        </row> 
					<%	
			}												
    }		
	
    else if(request.getParameter("accion").equals("listMisCitasHoy")){	
	
   			 if(request.getParameter("IdPaciente").equals(""))
		           beanCitas.citas.setIdPaciente("%");				
		     else  beanCitas.citas.setIdPaciente(request.getParameter("IdPaciente"));				

   			 if(request.getParameter("idProfesional").equals("00"))
		           beanCitas.citas.setIdProfesional("%");							 
		     else  beanCitas.citas.setIdProfesional(request.getParameter("idProfesional"));							 		   
				
		     
			 if(request.getParameter("BusFechaDesde").equals(""))
			        beanCitas.citas.setFechaDesde("01/01/1900");
			 else   beanCitas.citas.setFechaDesde(request.getParameter("BusFechaDesde"));
			 if(request.getParameter("BusFechaHasta").equals(""))
			        beanCitas.citas.setFechaHasta("01/01/1900");
			 else   beanCitas.citas.setFechaHasta(request.getParameter("BusFechaHasta"));
			 
			 
			 beanCitas.citas.setFechaHasta(request.getParameter("BusFechaHasta"));			 			 
			 
			 beanCitas.setPage(request.getParameter("page"));  // paginas que se pide
			 beanCitas.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 beanCitas.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanCitas.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
		     resultaux=(ArrayList)beanCitas.asignaBuscarPag(request.getParameter("accion"));	
			 CitasVO parametro=new CitasVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanHC.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(CitasVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[<%= i %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getIdCita() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getEstadoCita() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getDesEtadoCita() %>]]></cell>                                  
                              <cell><![CDATA[<%= parametro.getTipoCita() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getFecha() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getJornada() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getIdTurno() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getHoraCita() %>]]></cell>                                  
                              <cell><![CDATA[<%= parametro.getIdPaciente() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getNombrePaciente() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getIdProfesional() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getNomProfesional() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getCodTipoServicio() %>]]></cell>                                  
                              <cell><![CDATA[<%= parametro.getDesTipoServicio() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getFechaAsistencia() %>]]></cell>                                                                                                                                                       
                              <cell><![CDATA[<%= parametro.getAsistencia() %>]]></cell>                                                                                                                                                                                     
                        </row> 
					<%	
			}												
    }	
	
	if(request.getParameter("accion").equals("InformacionPacienteCitaRegistrada")){
		beanCitas.citas.setIdPaciente(request.getParameter("idPaciente"));  
		beanCitas.citas.setFecha(request.getParameter("fecha"));	
		beanCitas.citas.setIdTurno(request.getParameter("idTurno"));
		//System.out.println("setNoIdElavoro "+beanSession.usuario.getIdentificacion());        
		resultaux=(ArrayList)beanCitas.citas.asignaBuscarInformacionPacienteCitaRegistrada();	
	   
		CitasVO parametro=new CitasVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(CitasVO)resultaux.get(i);
				   i++;	
			%>
				   <citasPaciente> 
                       <IdCita><![CDATA[<%=parametro.getIdCita()%>]]></IdCita>   
                       <Municipio><![CDATA[<%=parametro.getMunicipio()%>]]></Municipio>   
                       <Direccion><![CDATA[<%=parametro.getDireccion()%>]]></Direccion>  
                       <Acompanante><![CDATA[<%=parametro.getAcompanante()%>]]></Acompanante>            
                       <Telefonos><![CDATA[<%=parametro.getTelefonos()%>]]></Telefonos>                         
                       <Administradora1><![CDATA[<%=parametro.getAdministradora1()%>]]></Administradora1>                          
                       <Administradora2><![CDATA[<%=parametro.getAdministradora2()%>]]></Administradora2>   
                       <TipoPaciente><![CDATA[<%=parametro.getTipoPaciente()%>]]></TipoPaciente>   
                       <TipoCita><![CDATA[<%=parametro.getTipoCita()%>]]></TipoCita>  
                       <EstadoCita><![CDATA[<%=parametro.getEstadoCita()%>]]></EstadoCita>   
                       <Observacion><![CDATA[<%=parametro.getObservacion()%>]]></Observacion>
                       <IdElaboro><![CDATA[<%=parametro.getIdElaboro()%>]]></IdElaboro> 
				   </citasPaciente>
			<%
		}
	}	
	
	if(request.getParameter("accion").equals("InformacionPaciente")){ 
		beanCitas.citas.setIdContacto(request.getParameter("idContacto"));  
		resultaux=(ArrayList)beanCitas.citas.buscarInformacionPaciente();	
	   
		CitasVO parametro=new CitasVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(CitasVO)resultaux.get(i);
				   i++;	
			%>
				   <citasPaciente> 

                       <TipoIdPaciente><![CDATA[<%=parametro.getTipoIdPaciente()%>]]></TipoIdPaciente>   
                       <IdPaciente><![CDATA[<%=parametro.getIdPaciente()%>]]></IdPaciente>                                             
                       <Nombre1><![CDATA[<%=parametro.getNombre1()%>]]></Nombre1>   
                       <Nombre2><![CDATA[<%=parametro.getNombre2()%>]]></Nombre2>   
                       <Apellido1><![CDATA[<%=parametro.getApellido1()%>]]></Apellido1>   
                       <Apellido2><![CDATA[<%=parametro.getApellido2()%>]]></Apellido2>                                                                        
                       
                       <idMunicipio><![CDATA[<%=parametro.getVariableAux5()%>]]></idMunicipio>                         
                       <Municipio><![CDATA[<%=parametro.getMunicipio()%>]]></Municipio>   
                       <Administradora1><![CDATA[<%=parametro.getAdministradora1()%>]]></Administradora1>                       
                       <Direccion><![CDATA[<%=parametro.getDireccion()%>]]></Direccion>  
                       <Telefonos><![CDATA[<%=parametro.getTelefonos()%>]]></Telefonos> 
                       <TipoPaciente><![CDATA[<%=parametro.getTipoPaciente()%>]]></TipoPaciente>                                                       
                       <fechaNac><![CDATA[<%=parametro.getFecha()%>]]></fechaNac>   
                       <edad><![CDATA[<%=parametro.getVariableAux6()%>]]></edad>                          
                       <tipoServicio><![CDATA[<%=parametro.getCodTipoServicio()%>]]></tipoServicio>  
                       
                       <fechaInicioIngreso><![CDATA[<%=parametro.getFechaAux()%>]]></fechaInicioIngreso>   
                       <triageIngreso><![CDATA[<%=parametro.getVariableAux()%>]]></triageIngreso>    
                       <idOrigenAtencion><![CDATA[<%=parametro.getVariableAux4()%>]]></idOrigenAtencion>                         
                       <origenAtencion><![CDATA[<%=parametro.getVariableAux2()%>]]></origenAtencion>  
                       <destinoPaciente><![CDATA[<%=parametro.getVariableAux3()%>]]></destinoPaciente>                                              
				   </citasPaciente>
			<%
		}
	}		
	if(request.getParameter("accion").equals("InformacionPacienteExistente")){
		beanCitas.citas.setIdPaciente(request.getParameter("idPaciente"));  
		beanCitas.citas.setFecha(request.getParameter("fecha"));	
		beanCitas.citas.setIdTurno(request.getParameter("idTurno"));
		//System.out.println("setNoIdElavoro "+beanSession.usuario.getIdentificacion());        
		resultaux=(ArrayList)beanCitas.citas.asignaBuscarInformacionPacienteExistente();	
	   
		CitasVO parametro=new CitasVO();									
		int i=0;
		 while(i<resultaux.size()){
				parametro=(CitasVO)resultaux.get(i);
				   i++;	
			%>
				   <citasPaciente> 
                       <Municipio><![CDATA[<%=parametro.getMunicipio()%>]]></Municipio>   
                       <Direccion><![CDATA[<%=parametro.getDireccion()%>]]></Direccion>  
                       <Acompanante><![CDATA[<%=parametro.getAcompanante()%>]]></Acompanante>            
                       <Telefonos><![CDATA[<%=parametro.getTelefonos()%>]]></Telefonos>                              
                       <fechaNac><![CDATA[<%=parametro.getFecha()%>]]></fechaNac>  
                       <sexo><![CDATA[<%=parametro.getSexo()%>]]></sexo>                                               
                       <Administradora1><![CDATA[<%=parametro.getAdministradora1()%>]]></Administradora1>                          
                       <Administradora2><![CDATA[<%=parametro.getAdministradora2()%>]]></Administradora2>   
                       <TipoPaciente><![CDATA[<%=parametro.getTipoPaciente()%>]]></TipoPaciente>   
                       <Observacion><![CDATA[<%=parametro.getObservacion()%>]]></Observacion>
                       <IdElaboro><![CDATA[<%=parametro.getIdElaboro()%>]]></IdElaboro> 
				   </citasPaciente>
			<%
		}
	}	
    else if(request.getParameter("accion").equals("listadoDocumenHC")){	
		     beanHC.documentoHC.setIdPaciente(request.getParameter("idPaciente"));  
			 beanHC.setPage(request.getParameter("page"));  // paginas que se pide
			 beanHC.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 
/*			 if(request.getParameter("sidx").equals("idPaciente")) beanHC.setSidx("1");  //indice por el campo por el cual se ordena
			 else beanHC.setSidx("2");*/
			 
			 beanHC.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanHC.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 //totalcount = (int)beanHC.asignaContar(request.getParameter("accion"));
			 //totalpages = (int)beanHC.totalPages(totalcount);//podria estar almacenado en el bean.
			 resultaux=(ArrayList)beanHC.documentoHC.asignaBuscarDocumentosPaciente();	
			 DocumentoHCVO parametro=new DocumentoHCVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanHC.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(DocumentoHCVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[<%= parametro.getTipoDocumento() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getTotalDocumento() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getNomDocumento() %>]]></cell>                                  
                              <cell><![CDATA[<%= parametro.getNomProfesional() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getFechaDocumento() %>]]></cell>                                                                                            
                        </row> 
					<%	
			}												
    }		
    else if(request.getParameter("accion").equals("listHistoricoListaEspera")){	

		     beanCitas.listaEspera.setIdPaciente(request.getParameter("idPaciente"));  
			 beanCitas.setPage(request.getParameter("page"));  // paginas que se pide
			 beanCitas.setRows(request.getParameter("rows"));  //limite de filas por paginas
             beanCitas.setSidx("1");  
			 if(request.getParameter("sidx").equals("idCita")) beanCitas.setSidx("7");  //indice por el campo por el cual se ordena^			 
			 if(request.getParameter("sidx").equals("Estado")) beanCitas.setSidx("8");  
			 if(request.getParameter("sidx").equals("tipo")) beanCitas.setSidx("9");  
			 if(request.getParameter("sidx").equals("fechaCita")) beanCitas.setSidx("10");  
			 if(request.getParameter("sidx").equals("servicio")) beanCitas.setSidx("11");  			  

			 beanCitas.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			// resultaux=(ArrayList)beanCitas.listaEspera.buscarHistoricoLisaEspera();
			 resultaux=(ArrayList)beanCitas.asignaBuscarPag(request.getParameter("accion"));						 
			 ListaEsperaVO parametro=new ListaEsperaVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanCitas.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(ListaEsperaVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[<%= parametro.getIdLista() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getFecha() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.tipoServicioVO.getDescripcion() %>]]></cell>   
                              <cell><![CDATA[<%= parametro.getNomProfesional() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.listaEsperaEstadoVO.getDescripcion() %>]]></cell>                                                                
                              <cell><![CDATA[<%= parametro.getObservacion()%>]]></cell>                               
                              <cell><![CDATA[]]></cell>                                                              
                              <cell><![CDATA[<%= parametro.citasVO.getIdCita()%>]]></cell>  
                              <cell><![CDATA[<%= parametro.citasVO.getDesEtadoCita()%>]]></cell>                                
                              <cell><![CDATA[<%= parametro.citasVO.getTipoCita()%>]]></cell>  
                              <cell><![CDATA[<%= parametro.citasVO.getFecha()%>]]></cell>  
                              <cell><![CDATA[<%= parametro.citasVO.getJornada()%>]]></cell>  
                              <cell><![CDATA[<%= parametro.citasVO.getIdTurno()%>]]></cell>  
                              <cell><![CDATA[<%= parametro.citasVO.getHoraCita()%>]]></cell>  
                              <cell><![CDATA[<%= parametro.citasVO.getDesTipoServicio()%>]]></cell>                                                                                                                          
                              <cell><![CDATA[<%= parametro.citasVO.getNomProfesional()%>]]></cell>  
                              <cell><![CDATA[<%= parametro.citasVO.getObservacion()%>]]></cell>                                                                                                                                                                                      
                              
                        </row> 
					<%	
			}												
    }		
    else if(request.getParameter("accion").equals("listPacientesListaEspera")){	

			 beanCitas.setPage(request.getParameter("page"));  // paginas que se pide
			 beanCitas.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 beanCitas.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanCitas.setSord(request.getParameter("sord"));  // sentido de la ordenacion		 
			 int totalcount=0,totalpages=0;
			 resultaux=(ArrayList)beanCitas.listaEspera.buscarLisaEspera();	
			 ListaEsperaVO parametro=new ListaEsperaVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanCitas.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		
			 while(i<resultaux.size()){
				parametro=(ListaEsperaVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[<%= (resultaux.size()+1)-i %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getIdLista() %>]]></cell>                                                                                
                              <cell><![CDATA[<%= parametro.getIdPaciente() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getNombrePaciente() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getNomProfesional() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getDiasEspera()%>]]></cell>                                                              
                              <cell><![CDATA[<%= parametro.getFecha()%>]]></cell>               
                              <cell><![CDATA[<%= parametro.getNomElaboro()%>]]></cell>                                                         
                        </row> 
					<%	
			}												
    }		
  else if(request.getParameter("accion").equals("listPacientesListaEsperaTraer")){	

			 beanCitas.setPage(request.getParameter("page"));  // paginas que se pide
			 beanCitas.setRows(request.getParameter("rows"));  //limite de filas por paginas
			 beanCitas.setSidx(request.getParameter("sidx"));  //indice por el campo por el cual se ordena
			 beanCitas.setSord(request.getParameter("sord"));  // sentido de la ordenacion		
			 

   			 if(request.getParameter("IdPaciente").equals(""))
		           beanCitas.listaEspera.setIdPaciente("%");				
		     else  beanCitas.listaEspera.setIdPaciente(request.getParameter("IdPaciente"));	
			 
		           beanCitas.listaEspera.setEdad(request.getParameter("Edad"));				
   			 if(request.getParameter("idProfesional").equals("00"))
		           beanCitas.listaEspera.setIdProfesional("%");							 
		     else  beanCitas.listaEspera.setIdProfesional(request.getParameter("idProfesional"));							 		   
				
		     
			 if(request.getParameter("BusFechaDesde").equals(""))
			        beanCitas.listaEspera.setFechaDesde("01/01/1900");
			 else   beanCitas.listaEspera.setFechaDesde(request.getParameter("BusFechaDesde"));
			 if(request.getParameter("BusFechaHasta").equals(""))
			        beanCitas.listaEspera.setFechaHasta("01/01/1900");
			 else   beanCitas.listaEspera.setFechaHasta(request.getParameter("BusFechaHasta"));
			 
   			 if(request.getParameter("estado").equals("00"))
		           beanCitas.listaEspera.setEstado("%");							 
		     else  beanCitas.listaEspera.setEstado(request.getParameter("estado"));		

   			 if(request.getParameter("administradora1LE").equals(""))
		           beanCitas.listaEspera.setAdministradora1("%");							 
		     else  beanCitas.listaEspera.setAdministradora1(request.getParameter("administradora1LE"));		

		     beanCitas.listaEspera.setSinCita(request.getParameter("SinCita"));  
   			 if(request.getParameter("disponible").equals("00"))
		           beanCitas.listaEspera.setDisponible("%");							 
		     else  beanCitas.listaEspera.setDisponible(request.getParameter("disponible"));		


   			 if(request.getParameter("TipoCita").equals("00"))
		           beanCitas.listaEspera.setTipoCita("%");							 
		     else  beanCitas.listaEspera.setTipoCita(request.getParameter("TipoCita"));		


			 int totalcount=0,totalpages=0;
			 resultaux=(ArrayList)beanCitas.listaEspera.buscarLisaEsperaTraer();	
			 
			 ListaEsperaVO parametro=new ListaEsperaVO();	
			 int i=0, j=0,ban=1;
			 %>
                        <page><%=beanCitas.getPagina()%></page> 
                        <total><%=totalpages%></total> 
                        <records><%=totalcount%></records> 
			 <%		

			 while(i<resultaux.size()){
				parametro=(ListaEsperaVO)resultaux.get(i);
				i++;
					%> 					
                        <row id = '<%= i%>'> 
                              <cell><![CDATA[<%= (resultaux.size()+1)-i %>]]></cell>                                                  
                              <cell><![CDATA[<%= parametro.getIdLista() %>]]></cell>                                
                              <cell><![CDATA[<%= parametro.getIdPaciente() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getNombrePaciente() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getFechaNacimientoPaciente() %>]]></cell>   
                              <cell><![CDATA[<%= parametro.getSexo() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getEdad() %>]]></cell>                                                                         
                              <cell><![CDATA[<%= parametro.getAdministradora1() %>]]></cell>    
                              <cell><![CDATA[<%= parametro.getNomAdministradora1() %>]]></cell>                              
                              <cell><![CDATA[<%= parametro.getAdministradora2() %>]]></cell>                                                                                                                                                                                                                          
                              <cell><![CDATA[<%= parametro.getTelefonos() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getCodMunicipio() %>]]></cell>                                         
                              <cell><![CDATA[<%= parametro.getMunicipio() %>]]></cell>           
                              <cell><![CDATA[<%= parametro.getDireccion() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getIdProfesional() %>]]></cell>                                              
                              <cell><![CDATA[<%= parametro.getNomProfesional() %>]]></cell>      
                              <cell><![CDATA[<%= parametro.getTipoCita() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getObservacion() %>]]></cell>                                                                                        
                              <cell><![CDATA[<%= parametro.getFecha() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getAcompanante() %>]]></cell>
                              <cell><![CDATA[<%= parametro.getTipoPaciente() %>]]></cell>
                              <cell><![CDATA[<%= parametro.getDiasEspera() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getDiscapacidadFisica() %>]]></cell>
                              <cell><![CDATA[<%= parametro.getEmbarazo() %>]]></cell>  
                              <cell><![CDATA[<%= parametro.getZonaResidencia() %>]]></cell>                                                                                                                                                                                                                                                         
                              <cell><![CDATA[<%= parametro.getGradoNecesidad() %>]]></cell>                                                                                                                                                                                                                                                                                         
                              <cell><![CDATA[<%= parametro.getIdDisponible() %>]]></cell> 
                              <cell><![CDATA[<%= parametro.getDisponible() %>]]></cell>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                              <cell><![CDATA[<%= parametro.getIdElaboro() %>]]></cell>
                              <cell><![CDATA[<%= parametro.getEstado() %>]]></cell>                                                                                                                 
                        </row> 
					<%	
			}												
    }		

	//Aqui uno nuevo
}
	
	//System.out.println("4");
%>
 	 
</rows>            