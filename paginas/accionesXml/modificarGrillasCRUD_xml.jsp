<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
  <%@ page contentType="text/xml; charset=ISO-8859-1"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
	<%@ page import = "java.text.NumberFormat" %>
    <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> 
     <raiz>	  	

<% 
   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());      

  
   ArrayList resultaux=new ArrayList();

   boolean resultado=false;

    String idAccion = request.getParameter("oper");
    String accion = request.getParameter("accion");
    String parametros = "";
    String paciente, empresa;
    String a[];
    String fechaCita;

    switch(idAccion){

        case "add":

        switch (accion){

          case "crearRuta":
          parametros +=  "_-" +request.getParameter("NOMBRE");
          parametros +=  "_-" + request.getParameter("OBSERVACIONES");
          parametros +=  "_-" + request.getParameter("DURACION_DIAS");
          parametros +=  "_-" + request.getParameter("CICLICA");
          parametros += "_-" + request.getParameter("VIGENTE");
          parametros +=  "_-" + request.getParameter("EDAD_INICIO_MES");
          parametros +=  "_-" + request.getParameter("EDAD_INICIO_ANIO");
          parametros +=  "_-" + request.getParameter("EDAD_FIN_MES");
          parametros +=  "_-" + request.getParameter("EDAD_FIN_ANIO");
          parametros +=  "_-" + request.getParameter("ID_SEXO");
          parametros +=  "_-" + request.getParameter("TIPO");

          break;

          case "crearRutaTipoCita":

          String idProcedicimento = request.getParameter("Procedimiento");
          String[] aux10 = idProcedicimento.split("-");
          idProcedicimento = aux10[0];

          String idTalento = request.getParameter("TalentoHumano");
          String[] aux11 = idTalento.split("-");
          idTalento = aux11[0];

          parametros +=  "_-" + request.getParameter("txtIdRuta");
          parametros +=  "_-" +idProcedicimento;
          parametros +=  "_-" +idTalento;
          parametros +=  "_-" + request.getParameter("ORDEN");
          parametros +=  "_-" + request.getParameter("OBLIGATORIA");
          parametros +=  "_-" + request.getParameter("FRECUENCIA");
          parametros +=  "_-" + request.getParameter("UNICA");

          break;

            case "crearPaciente":
            String idMunicipio = request.getParameter("MunicipioResi");            
            String[] aux = idMunicipio.split("-");            
            idMunicipio = aux[0];
            
            String idDep = idMunicipio.substring(0,2);

            String idAdministradora = request.getParameter("administradora");            
            String[] aux2 = idAdministradora.split("-");            
            idAdministradora = aux2[0];

            String idBarrio = request.getParameter("Barrio");
            aux = idBarrio.split("-");
            idBarrio = aux[0];

            String idLocalidad = request.getParameter("Localidad");
            aux = idLocalidad.split("-");
            idLocalidad = aux[0];

            String idIps = request.getParameter("ipsprimaria");            
            aux2 = idIps.split("-");            
            idIps = aux2[0];

            String idOcupacion = request.getParameter("Ocupacion");            
            aux2 = idOcupacion.split("-");            
            idOcupacion = aux2[0];

            String idLugarN = request.getParameter("lugarNacimiento");            
            aux2 = idLugarN.split("-");            
            idLugarN = aux2[0];

            parametros +=  "_-" +request.getParameter("TipoId");
            parametros +=  "_-" +request.getParameter("identificacion");
            parametros +=  "_-" +request.getParameter("afiliacion");
            parametros +=  "_-" +request.getParameter("Nombre1");
            parametros +=  "_-" +request.getParameter("Nombre2");
            parametros +=  "_-" +request.getParameter("Apellido1");
            parametros +=  "_-" +request.getParameter("Apellido2");
            parametros +=  "_-" +request.getParameter("FechaNacimiento");
            parametros +=  "_-" +request.getParameter("Sexo");
            parametros +=  "_-" +idDep;
            parametros +=  "_-" +idMunicipio;            
            parametros +=  "_-" +request.getParameter("Direccion");
            parametros +=  "_-" +idBarrio;
            parametros +=  "_-" +idLocalidad;
            parametros +=  "_-" +request.getParameter("Sisben");
            parametros +=  "_-" +request.getParameter("idestrato");
            parametros +=  "_-" +request.getParameter("Telefono");
            parametros +=  "_-" +request.getParameter("celular1");
            parametros +=  "_-" +request.getParameter("celular2");
            parametros +=  "_-" +request.getParameter("email");
            parametros +=  "_-" +idAdministradora;
            parametros +=  "_-" +idIps;
            parametros +=  "_-" +request.getParameter("idregimen");
            parametros +=  "_-" +request.getParameter("idregia");
            parametros +=  "_-" +idOcupacion;
            parametros +=  "_-" +request.getParameter("idec");
            parametros +=  "_-" +idLugarN;
            parametros +=  "_-" +request.getParameter("idet");
            parametros +=  "_-" +request.getParameter("Pueblo");
            parametros +=  "_-" +request.getParameter("iddis");
            parametros +=  "_-" +request.getParameter("idne");
            parametros +=  "_-" +request.getParameter("idgp");
            parametros +=  "_-" +request.getParameter("acudiente");
            parametros +=  "_-" +request.getParameter("idps");
            parametros +=  "_-" +request.getParameter("tipo_id_acudiente");
            parametros +=  "_-" +request.getParameter("identificacionA");
            parametros +=  "_-" +request.getParameter("acudienteDir");
            parametros +=  "_-" +request.getParameter("acudienteTel");
            parametros +=  "_-" +request.getParameter("acudienteEmail");
            parametros +=  "_-" +request.getParameter("idcv");
            parametros +=  "_-" +request.getParameter("idpso");
            parametros +=  "_-" +request.getParameter("HV");
            parametros +=  "_-" +request.getParameter("intersexual");
            parametros +=  "_-" +request.getParameter("usuarioCrea");                                 

            break;

            case "crearPlanAtencion":
            parametros +=  "_-" +request.getParameter("NOMBRE");
            parametros +=  "_-" +request.getParameter("VIGENTE");

            break;

            case "crearPacienteRuta":

            paciente = request.getParameter("ID_PACIENTE");
            a = paciente.split("-");            
            paciente = a[0];

            empresa = request.getParameter("EMPRESA"); 
            a = empresa.split("-");
            empresa = a[0];

            parametros +=  "_-" +request.getParameter("RUTA");
            parametros +=  "_-" +paciente;
            parametros +=  "_-" +request.getParameter("OBSERVACIONES");
            parametros +=  "_-" +empresa;          
            
            break;

            case "crearPacienteTecnologia":

            paciente = request.getParameter("txtIdPaciente");
            a = paciente.split("-");            
            paciente = a[0];

           empresa = request.getParameter("EMPRESA"); 
            a = empresa.split("-");
            empresa = a[0];

            parametros +=  "_-" +request.getParameter("txtIdRuta");
            parametros +=  "_-" +paciente;
            parametros +=  "_-" +request.getParameter("ESPECIALIDAD");
            parametros +=  "_-" +request.getParameter("TIPO");
            parametros +=  "_-" +request.getParameter("DIAS_ESPERA");
            parametros +=  "_-" +request.getParameter("ORDEN");
            parametros +=  "_-" +request.getParameter("GRADO_NECESIDAD");
            parametros +=  "_-" +empresa;  
            parametros +=  "_-" +request.getParameter("OBSERVACIONES");
            parametros +=  "_-" +request.getParameter("txtIdRutaPaciente");
            

            break;

        }

        break;

        case "edit":

        switch (accion){
          
            case "modificarRuta":
            parametros += "_-" +request.getParameter("NOMBRE");
            parametros += "_-" + request.getParameter("OBSERVACIONES");
            parametros += "_-" + request.getParameter("DURACION_DIAS");
            parametros += "_-" + request.getParameter("CICLICA");
            parametros += "_-" + request.getParameter("VIGENTE");
            parametros +=  "_-" + request.getParameter("EDAD_INICIO_MES");
            parametros +=  "_-" + request.getParameter("EDAD_INICIO_ANIO");
            parametros +=  "_-" + request.getParameter("EDAD_FIN_MES");
            parametros +=  "_-" + request.getParameter("EDAD_FIN_ANIO");
            parametros +=  "_-" + request.getParameter("ID_SEXO");
            parametros +=  "_-" + request.getParameter("TIPO");
            parametros += "_-" + request.getParameter("idEdit");


            break;

            case "modificarRutaTipoCita":

            String idProcedicimento = request.getParameter("Procedimiento");
            String[] aux10 = idProcedicimento.split("-");
            idProcedicimento = aux10[0];

            String idTalento = request.getParameter("TalentoHumano");
            String[] aux11 = idTalento.split("-");
            idTalento = aux11[0];

            parametros +=  "_-" +idProcedicimento;
            parametros +=  "_-" +idTalento;
            parametros +=  "_-" + request.getParameter("ORDEN");
            parametros +=  "_-" + request.getParameter("OBLIGATORIA");
            parametros +=  "_-" + request.getParameter("FRECUENCIA");
            parametros +=  "_-" + request.getParameter("UNICA");

            parametros +=  "_-" + request.getParameter("idEdit");

		        break;

            case "editarPaciente":
            String idMunicipio = request.getParameter("MunicipioResi");            
            String[] aux = idMunicipio.split("-");            
            idMunicipio = aux[0];

            String idAdministradora = request.getParameter("administradora");            
            String[] aux2 = idAdministradora.split("-");            
            idAdministradora = aux2[0];

            String idBarrio = request.getParameter("Barrio");
            aux = idBarrio.split("-");
            idBarrio = aux[0];

            String idLocalidad = request.getParameter("Localidad");
            aux = idLocalidad.split("-");
            idLocalidad = aux[0];

            String idIps = request.getParameter("ipsprimaria");            
            aux2 = idIps.split("-");            
            idIps = aux2[0];

            String idOcupacion = request.getParameter("Ocupacion");            
            aux2 = idOcupacion.split("-");            
            idOcupacion = aux2[0];

            String idLugarN = request.getParameter("lugarNacimiento");            
            aux2 = idLugarN.split("-");            
            idLugarN = aux2[0];

            parametros +=  "_-" +request.getParameter("Nombre1");
            parametros +=  "_-" +request.getParameter("Nombre2");
            parametros +=  "_-" +request.getParameter("Apellido1");
            parametros +=  "_-" +request.getParameter("Apellido2");
            parametros +=  "_-" +request.getParameter("FechaNacimiento");
            parametros +=  "_-" +request.getParameter("Sexo");
            parametros +=  "_-" +idMunicipio;            
            parametros +=  "_-" +request.getParameter("Direccion");
            parametros +=  "_-" +idBarrio;
            parametros +=  "_-" +idLocalidad;
            parametros +=  "_-" +request.getParameter("Sisben");
            parametros +=  "_-" +request.getParameter("idestrato");
            parametros +=  "_-" +request.getParameter("Telefono");
            parametros +=  "_-" +request.getParameter("celular1");
            parametros +=  "_-" +request.getParameter("celular2");
            parametros +=  "_-" +request.getParameter("email");
            parametros +=  "_-" +idAdministradora;
            parametros +=  "_-" +idIps;
            parametros +=  "_-" +request.getParameter("idregimen");
            parametros +=  "_-" +request.getParameter("idregia");
            parametros +=  "_-" +idOcupacion;
            parametros +=  "_-" +request.getParameter("idec");
            parametros +=  "_-" +idLugarN;
            parametros +=  "_-" +request.getParameter("Pueblo");
            parametros +=  "_-" +request.getParameter("idet");
            parametros +=  "_-" +request.getParameter("iddis");
            parametros +=  "_-" +request.getParameter("idne");
            parametros +=  "_-" +request.getParameter("idgp");
            parametros +=  "_-" +request.getParameter("acudiente");
            parametros +=  "_-" +request.getParameter("idps");
            parametros +=  "_-" +request.getParameter("tipo_id_acudiente");
            parametros +=  "_-" +request.getParameter("identificacionA");
            parametros +=  "_-" +request.getParameter("acudienteDir");
            parametros +=  "_-" +request.getParameter("acudienteTel");
            parametros +=  "_-" +request.getParameter("acudienteEmail");
            parametros +=  "_-" +request.getParameter("idcv");
            parametros +=  "_-" +request.getParameter("idpso");
            parametros +=  "_-" +request.getParameter("HV");
            parametros +=  "_-" +request.getParameter("usuarioModifica");
            parametros +=  "_-" +request.getParameter("intersexual");
            parametros +=  "_-" +request.getParameter("id");


            break;
            
            case "editarPlanAtencion":
            parametros +=  "_-" +request.getParameter("NOMBRE");
            parametros +=  "_-" +request.getParameter("VIGENTE");
            parametros +=  "_-" +request.getParameter("id");

            break;


            case "modificarPacienteRuta":

            parametros += "_-" +request.getParameter("txtIdRutaPaciente");
            parametros +=  "_-" +request.getParameter("RUTA");
            parametros +=  "_-" +request.getParameter("OBSERVACIONES");
            
            empresa = request.getParameter("EMPRESA"); 
            a = empresa.split("-");
            empresa = a[0];

            
            parametros +=  "_-" +empresa;          
            parametros += "_-" +request.getParameter("txtIdRutaPaciente");
            
            break;

            case "modificarPacienteTecnologia":

            
           empresa = request.getParameter("EMPRESA"); 
            a = empresa.split("-");
            empresa = a[0];

            parametros +=  "_-" +request.getParameter("ESPECIALIDAD");
            parametros +=  "_-" +request.getParameter("TIPO");
            parametros +=  "_-" +request.getParameter("DIAS_ESPERA");
            parametros +=  "_-" +request.getParameter("ORDEN");
            parametros +=  "_-" +empresa; 
            parametros +=  "_-" +request.getParameter("OBSERVACIONES");
            parametros +=  "_-" +request.getParameter("GRADO_NECESIDAD");
            parametros +=  "_-" +request.getParameter("txtIdTecnologiaPaciente");

            break;


            case "modificarCitaRuta":
                     

              fechaCita = request.getParameter("FECHA") + " "+  request.getParameter("HORA");
              parametros =parametros+ "_-" + fechaCita;
              parametros =parametros+ "_-" + request.getParameter("txtIdPaciente");
              parametros =parametros+ "_-" + request.getParameter("txtTipoCita");
              parametros =parametros+ "_-" + request.getParameter("MOTIVO");
              parametros =parametros+ "_-" + request.getParameter("OBSERVACIONES");
              parametros =parametros+ "_-" + request.getParameter("ESTADO");
              parametros =parametros+ "_-" + request.getParameter("txtIdListaEspera");
              parametros =parametros+ "_-" + request.getParameter("LoginSesion");
              parametros =parametros+ "_-" + request.getParameter("txtIdEspecialidad");
              parametros =parametros+ "_-" + request.getParameter("MEDICO");
              parametros =parametros+ "_-" + request.getParameter("IdSesion");
              parametros =parametros+ "_-" + request.getParameter("IdSede");
                       
           

            break;

            case "actualizarCitaRuta":

            fechaCita = request.getParameter("FECHA") + " "+  request.getParameter("HORA");
            parametros =parametros+ "_-" + fechaCita;
            parametros =parametros+ "_-" + request.getParameter("ESTADO");
            parametros =parametros+ "_-" + request.getParameter("MOTIVO");
            parametros =parametros+ "_-" + request.getParameter("OBSERVACIONES");
            parametros =parametros+ "_-" + request.getParameter("LoginSesion");
            parametros =parametros+ "_-" + request.getParameter("MEDICO");
            parametros =parametros+ "_-" + request.getParameter("txtIdCita");
            break;

            case "crearTerapiaCita":
            fechaCita = request.getParameter("FECHA_CITA") + " "+  request.getParameter("HORA");
            parametros =parametros+ "_-" + fechaCita;
            parametros =parametros+ "_-" + request.getParameter("MOTIVO_CONSULTA");
            parametros =parametros+ "_-" + request.getParameter("OBSERVACIONES");
            parametros =parametros+ "_-" + request.getParameter("ESTADO_CITA");
            parametros =parametros+ "_-" + request.getParameter("LoginSesion");
            parametros =parametros+ "_-" + request.getParameter("txtIdListaEspera");
            break;

            case "modificarTerapiaCita":
            fechaCita = request.getParameter("FECHA_CITA") + " "+  request.getParameter("HORA");

            parametros =parametros+ "_-" + fechaCita;
            parametros =parametros+ "_-" + request.getParameter("txtIdCita");
            parametros =parametros+ "_-" + request.getParameter("ESTADO_CITA");

            parametros =parametros+ "_-" + request.getParameter("txtIdCita");
            parametros =parametros+ "_-" + request.getParameter("ESTADO_CITA");

            parametros =parametros+ "_-" + fechaCita;
            parametros =parametros+ "_-" + request.getParameter("ESTADO_CITA");
            parametros =parametros+ "_-" + request.getParameter("MOTIVO_CONSULTA");
            parametros =parametros+ "_-" + request.getParameter("OBSERVACIONES");
            parametros =parametros+ "_-" + request.getParameter("LoginSesion");
            parametros =parametros+ "_-" + request.getParameter("txtIdCita");
            break;

            case "modificarAnalitoManual":

            parametros =parametros+ "_-" + request.getParameter("CODIGO_ANALITO");
            parametros =parametros+ "_-" + request.getParameter("NOMBRE_ANALITO");
            parametros =parametros+ "_-" + request.getParameter("RESULTADO");
            parametros =parametros+ "_-" + request.getParameter("VALOR_MINIMO");
            parametros =parametros+ "_-" + request.getParameter("VALOR_MAXIMO");
            parametros =parametros+ "_-" + request.getParameter("UNIDADES");
            parametros =parametros+ "_-" + request.getParameter("VALOR_REFERENCIA");
            parametros =parametros+ "_-" + request.getParameter("txtIdLa01");
            parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");
            
            break;

            case "crearAnalitoManual":

            parametros =parametros+ "_-" + request.getParameter("CODIGO_ANALITO");
            parametros =parametros+ "_-" + request.getParameter("NOMBRE_ANALITO");
            parametros =parametros+ "_-" + request.getParameter("RESULTADO");
            parametros =parametros+ "_-" + request.getParameter("VALOR_MINIMO");
            parametros =parametros+ "_-" + request.getParameter("VALOR_MAXIMO");
            parametros =parametros+ "_-" + request.getParameter("UNIDADES");
            parametros =parametros+ "_-" + request.getParameter("VALOR_REFERENCIA");
            parametros =parametros+ "_-" + request.getParameter("txtIdOrden");
            parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");
            parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");
            
            break;

            
            case "crearAnalitoManualExterno":
            parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");
              parametros =parametros+ "_-" + request.getParameter("FECHA_ELABORO");
              parametros =parametros+ "_-" + request.getParameter("txtIdOrden");
              parametros =parametros+ "_-" + request.getParameter("RESULTADO");
              parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");
              parametros =parametros+ "_-" + request.getParameter("txtIdPlantilla");
              parametros =parametros+ "_-" + request.getParameter("txtIdOrden");
              parametros =parametros+ "_-" + request.getParameter("FECHA_ELABORO");
              parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");              

            break;

            case "modificarAnalitoManualExterno":
              parametros =parametros+ "_-" + request.getParameter("FECHA_ELABORO");
              parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");
              parametros =parametros+ "_-" + request.getParameter("txtIdOrden");

              parametros =parametros+ "_-" + request.getParameter("RESULTADO");              
              parametros =parametros+ "_-" + request.getParameter("txtIdLa01");
              parametros =parametros+ "_-" + request.getParameter("txtIdProgramacion");              
            break;
        }
            
        break;

        case "del":

         switch (accion){

            case "eliminarRuta":
              parametros =parametros+ "_-" + request.getParameter("idDel");
            break;

            case "eliminarRutaParametro":
              parametros =parametros+ "_-" + request.getParameter("idDel");
            break;

            case "eliminarPlanAtencion":
              parametros +=  "_-" +request.getParameter("idDel");
            break;

            case "eliminarPacienteRuta":
            parametros +=  "_-" +request.getParameter("txtIdRutaPaciente");
            parametros +=  "_-" +request.getParameter("txtIdRutaPaciente");

            break;
            case "eliminarPacienteTecnologia":
            parametros +=  "_-" +request.getParameter("txtIdTecnologiaPaciente");

            break;
        }

        break;

    }

    System.out.println("IdAccion: "+idAccion); 
    System.out.println("Accion: "+accion); 
    System.out.println("Parametros: "+parametros); 

    if(request.getParameter("accion")!=null ){   
	  beanAdmin.grilla.setId(request.getParameter("idQuery")); 	  
	  beanAdmin.grilla.setParametros(parametros); 
      resultado = beanAdmin.grilla.modificarCRUD();
   }	
		
%>
   <respuesta><![CDATA[<%= resultado%>]]></respuesta>
   <MsgAlerta><![CDATA[<%= beanSession.cn.getMsgAlerta()%>]]></MsgAlerta>
 </raiz>	  	          