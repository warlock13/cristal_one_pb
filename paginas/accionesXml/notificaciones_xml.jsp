<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<%@ page contentType="text/xml"%>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="conexion" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<rows>
    <%
        ControlAdmin beanAdmin = new ControlAdmin();
        Conexion cn = new Conexion(conexion.getConnection());
        beanAdmin.setCn(cn);

        beanAdmin.notificacion.setIdUsuarioSesion(beanSession.usuario.getIdentificacion());

        ArrayList resultaux = new ArrayList();
    if (request.getParameter("tipoNotificacion").equals("escrita")) {
            System.out.println("escrita");
            beanAdmin.notificacion.setIdUsuarioSesion(beanSession.usuario.getIdentificacion());
            beanAdmin.notificacion.notificacionEscrita();
    %>
    <total>
        <totalNot><![CDATA[<%= beanAdmin.notificacion.getTotal()%>]]></totalNot>                              
        <id><![CDATA[<%= beanAdmin.notificacion.getId()%>]]></id>                   
        <descripcionNot><![CDATA[<%= beanAdmin.notificacion.getDescripcion()%>]]></descripcionNot>                   
    </total>    
    <%
    } else if (request.getParameter("tipoNotificacion").equals("alert") || request.getParameter("tipoNotificacion").equals("soloMensaje") || request.getParameter("tipoNotificacion").equals("notificacionAlert") || request.getParameter("tipoNotificacion").equals("escrita2")) 
    {
        String sql = "SELECT sw_vigente FROM notificaciones.query WHERE id = ?";

        PreparedStatement ps = conexion.getConnection().prepareStatement(sql);        
        ps.setInt(1,Integer.parseInt(request.getParameter("idQuery")));
        ResultSet rs = ps.executeQuery();

        String total = "y";

        while (rs.next()) {
            String vigente = rs.getString("sw_vigente");

            if (vigente.equals("N")){ 
                    System.out.println("escrita");
                    beanAdmin.notificacion.setIdUsuarioSesion(beanSession.usuario.getIdentificacion());
                    beanAdmin.notificacion.notificacionEscrita();
                    %>
                    <total>
                        <totalNot><![CDATA[<%= beanAdmin.notificacion.getTotal()%>]]></totalNot>                              
                        <id><![CDATA[<%= beanAdmin.notificacion.getId()%>]]></id>                   
                        <descripcionNot><![CDATA[<%= beanAdmin.notificacion.getDescripcion()%>]]></descripcionNot>                   
                    </total>        
                    <%
                }
            else
                {
                    beanAdmin.notificacion.setParametro1(request.getParameter("parametro1"));
                    beanAdmin.notificacion.setParametro2(request.getParameter("parametro2"));
                    beanAdmin.notificacion.setParametro3(request.getParameter("parametro3"));
                    beanAdmin.notificacion.setParametro4(request.getParameter("parametro4"));
                    beanAdmin.notificacion.setParametro5(request.getParameter("parametro5"));
                    beanAdmin.notificacion.setParametro6(request.getParameter("parametro6"));
                    beanAdmin.notificacion.notificacionAlert(Integer.parseInt(request.getParameter("idQuery")));
                    %>
                    <total>
                        <totalNot><![CDATA[<%= beanAdmin.notificacion.getTotal()%>]]></totalNot>                              
                        <descripcionNot><![CDATA[<%= beanAdmin.notificacion.getDescripcion()%>]]></descripcionNot>   
                        <accion><![CDATA[<%= request.getParameter("accion") %>]]></accion>                   
                    </total>    
                    <%  
                }       
            }        
        }
    %>
</rows>  

<%
    try {
        conexion.close();
    } catch (Exception var14) {
        System.err.println("*********** ERROR AL CERRAR CONEXION ***************");
    }
%>