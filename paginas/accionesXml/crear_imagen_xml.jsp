<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<%@ page contentType="text/xml"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Base64"%>
<%@page import=" javax.xml.bind.DatatypeConverter"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.awt.image.BufferedImage"%>
<raiz>	
<%

String base64String = request.getParameter("base64");
String ext = request.getParameter("ext");
String firma = request.getParameter("firma");
String mensaje = "";
Boolean resultado = true;

base64String = base64String.replaceAll(" ", "+");
String[] strings = base64String.split(",");

//byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);

//se decodifica la imagen base64 a bytes
byte[] data = Base64.getDecoder().decode(strings[1]);

String strPath = "/opt/tomcat8/webapps/clinica/paginas/ireports/imagenes/firmas/."+ext;
File strFile = new File(strPath);

 try {
    
    //Se guarda la imagen con el nombre prueba.+(extension)
    FileOutputStream outputStream = new FileOutputStream(strFile);
    outputStream.write(data);
    outputStream.flush();
    outputStream.close();

    //Se convierte la imagen a png y se guarda como la identificacion
    BufferedImage bImage = ImageIO.read(strFile);
    ImageIO.write(bImage, "png", new File("/opt/tomcat8/webapps/clinica/paginas/ireports/imagenes/firmas/"+firma));
   

} catch (IOException e) {
    e.printStackTrace();
    mensaje = "Error al subir la imagen, intente nuevamente";
    resultado = false;
}
 
%> 

    <respuesta><![CDATA[<%= resultado%>]]></respuesta>
   <MsgAlerta><![CDATA[<%= mensaje%>]]></MsgAlerta>
</raiz>