<%@ page contentType="application/json charset=UTF-8"%>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>

<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="conexion" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>
<%
    boolean resultado = false;
    String alerta = "";
    Connection cn = null;
    try {
        cn = conexion.getConnection();
        Statement stmt = cn.createStatement();
        String sql = "UPDATE formulario." + request.getParameter("tipo") + " SET " + request.getParameter("campo") + " = '" + request.getParameter("value") + "', usuario_modifica =  '" + beanSession.usuario.getLogin() + "' WHERE id_evolucion = " + request.getParameter("id_evolucion") + ";";

        stmt.executeUpdate(sql);

        System.out.println("se ejecutoSQL: " + sql);

        resultado = true;

    } catch (SQLException ex) {
        System.err.println(" ERROR SQL " + ex.getMessage());
        resultado = false;
    } catch (Exception e) {
        System.err.println(" ERROR TECNICO " + e.getMessage());
        resultado = false;
    } finally {
        try {
            conexion.close();
        } catch (Exception var14) {
            System.err.println("*********** ERROR AL CERRAR CONEXION ***************");
        }
    }
%>
{"resultado":<%= resultado%>}