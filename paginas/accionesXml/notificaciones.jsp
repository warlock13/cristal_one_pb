<%@ page session="true" contentType="application/json; charset=UTF-8" language="java"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import = "org.apache.commons.text.StringSubstitutor"%>
<%@ page import = "org.json.JSONObject"%>
<%@ page import = "org.json.JSONException"%>
<%@ page import = "java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "org.apache.commons.text.StringSubstitutor" %>

<jsp:useBean id="iConnection" class="Sgh.Utilidades.ConnectionClinica" scope="request"/>

<%
    Connection cn = iConnection.getConnection();
    Conexion conexion = new Conexion(cn);
    JSONObject respuesta = new JSONObject();

    String evento = request.getParameter("evento");

    String sql_traer_notificaciones_evento = "SELECT id_notificacion, bloqueante FROM notificaciones.notificaciones_evento WHERE id_evento = ? AND vigente = 'S' ORDER BY orden;";
    PreparedStatement ps0 = null;

    Map<String, Object> parametrosMap = new HashMap<>();

    for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
        parametrosMap.put(entry.getKey(), entry.getValue()[0]);
    }

    try {
        ps0 = cn.prepareStatement(sql_traer_notificaciones_evento);
        ps0.setString(1, evento);

        ResultSet rs0 = ps0.executeQuery();

        // RECORRER Y EJECUTAR CADA NOTIFICACION
        while (rs0.next()) {
            StringBuffer sql_notificacion = conexion.traerElQueryNotificacion(rs0.getInt("id_notificacion"));

            StringSubstitutor sub = new StringSubstitutor(parametrosMap);
            String resolvedString = sub.replace(sql_notificacion);

            PreparedStatement ps1 = cn.prepareStatement(resolvedString);
            try {
                ResultSet rs1 = ps1.executeQuery();
                while (rs1.next()) {
                    String mensaje = rs1.getString("mensaje");
                    if (mensaje != null) {
                        respuesta.put("error", false);
                        respuesta.put("notificacion", true);
                        respuesta.put("bloqueante", rs0.getBoolean("bloqueante"));
                        respuesta.put("mensaje", rs1.getString("mensaje") + " (" + rs0.getInt("id_notificacion") + ")");
                        %> <%= respuesta%> <%
                        return;
                    }
                }
            } catch (SQLException e) {
                System.err.println("***** ERROR AL EJECUTAR NOTIFICACION " + rs0.getInt("id_notificacion") + " EVENTO::" + evento + "*******");
                System.err.println(resolvedString);
                System.err.println(e.getMessage());
                respuesta.put("error", true);
                respuesta.put("notificacion", false);
                respuesta.put("mensaje", "No fue posible verificar la notificacion #" + rs0.getInt("id_notificacion") + ". Por favor ponte en contacto con soporte. EC (" + e.getSQLState() + ")");
                %> <%= respuesta%> <%
                return;
            }
        }
    } catch (SQLException e) {
        System.err.println("******** ERROR AL TRAER NOTIFICACIONES EVENTO::" + evento + "*******");
        e.printStackTrace();
        System.err.println(e.getMessage());
        respuesta.put("error", true);
        respuesta.put("notificacion", false);
        respuesta.put("mensaje", "Problemas al validar la acción. Por favor ponte en contacto con soporte. EC (" + e.getSQLState() + ")");
        %> <%= respuesta%> <%
        return;
    }
%>

<%
    respuesta.put("error", false);
    respuesta.put("notificacion", false);
    respuesta.put("mensaje", "");
%>

<%= respuesta%>