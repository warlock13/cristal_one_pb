<%@ page contentType="application/json"%>
<%@ page import = "Clinica.Utilidades.*" %>
<%@ page import = "java.sql.*" %>
<%@ page import = "java.io.File" %>
<%@ page import = "java.io.FileInputStream" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.util.Iterator" %>
<%@ page import = "java.util.List" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFRow" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>
<%@ page import = "org.apache.poi.ss.usermodel.DataFormatter" %>
<%@ page import = "java.sql.ResultSet" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> 
<%--funciones y variables--%>
<%!
    public void imprimirQuery(PreparedStatement preStm) {
        System.out.println(preStm.toString());
    }

%>
<%
    System.out.println("============Entrando a Subir Facturacion Excel================");

    String nombreFile = "", idPlan = "", usuario_crea = "", ruta = "";
    String mensaje = "Documento Excel cargado Exitosamente.";

    try {
        //capturar parametros
        nombreFile = request.getParameter("nombreArchivo");
        ruta = request.getParameter("ruta");
        idPlan = request.getParameter("idPlan");
        usuario_crea = request.getParameter("Usuario");
        ruta += "/" + nombreFile;

        Connection conexion = beanSession.cn.getConexion();
        // eliminar planes detalle del plan de contratacion
        String query = beanSession.cn.traerElQuery(2729).toString();
        PreparedStatement preparedStm = conexion.prepareStatement(query);
        preparedStm.setString(1, usuario_crea);
        preparedStm.setString(2, idPlan);
        preparedStm.setString(3, idPlan);
        imprimirQuery(preparedStm);
        preparedStm.executeUpdate();

        File archivoLectura = new File(ruta);
        if (archivoLectura.exists()) {
            FileInputStream fileInputStream = new FileInputStream(archivoLectura);
            XSSFWorkbook workBook = new XSSFWorkbook(fileInputStream);
            XSSFSheet excelHoja = workBook.getSheetAt(0);
            Iterator filaIterator = excelHoja.rowIterator();
            int filaCont = 2;

            if (filaIterator.hasNext()) {
                filaIterator.next();    //salta las cabezeras
                while (filaIterator.hasNext()) {
                    XSSFRow excelFila = (XSSFRow) filaIterator.next();
                    Iterator celdaIterator = excelFila.cellIterator();
                    List celdaTmp = new ArrayList();

                    while (celdaIterator.hasNext()) {
                        DataFormatter formatter = new DataFormatter();
                        celdaTmp.add(formatter.formatCellValue((Cell) celdaIterator.next()));
                    }

                    if (celdaTmp.size() < 3 || celdaTmp.get(0) == "" || celdaTmp.get(1) == "" || celdaTmp.get(2) == "") {
                        mensaje += "\\n El procedimiento en la fila: " + filaCont + " No fue agregado, favor verificar el docuemento Excel.";
                    }else{
                         //verificar que exista procedimiento
                        query = beanSession.cn.traerElQuery(2730).toString();
                        preparedStm = conexion.prepareStatement(query);
                        preparedStm.setString(1, (String) celdaTmp.get(0));
                        imprimirQuery(preparedStm);
                        ResultSet queryRes = preparedStm.executeQuery();
                        if (queryRes.next()) {
                            if (queryRes.getString("id") != null) {

                                // incertar procedimientos en planes_detalle                           
                                query = beanSession.cn.traerElQuery(2731).toString();
                                preparedStm = conexion.prepareStatement(query);
                                preparedStm.setString(1, idPlan);
                                preparedStm.setString(2, (String) celdaTmp.get(0));
                                preparedStm.setString(3, (String) celdaTmp.get(1));
                                preparedStm.setString(4, (String) celdaTmp.get(1));
                                preparedStm.setString(5, (String) celdaTmp.get(2));
                                preparedStm.setString(6, usuario_crea);
                                preparedStm.setString(7, usuario_crea);
                                imprimirQuery(preparedStm);
                                preparedStm.executeUpdate();
                            }
                        } else if (celdaTmp.get(0) != "") {
                            mensaje += "\\n El procedimiento #" + celdaTmp.get(0) + " En la fila #" + filaCont + " no existe!";
                        }
                    }           
                    filaCont ++;
                }
            } else {
                mensaje = "Archivo Excel vacio!";
                System.out.println(mensaje);
            }
        } else {
            mensaje = "No se puedo leer el archivo desde el servidor";
            System.out.println(mensaje);
        }

    } catch (Exception Exp) {
        mensaje = "Ocurrio un error: " + Exp.getMessage() + "\\n Es posible que la informacion no se haya cargado o este incompleta.";
        System.out.println(mensaje);
    }
%>
{"respuesta":"<%=(String) mensaje%>"}
