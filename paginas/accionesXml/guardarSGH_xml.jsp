<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Sgh.Presentacion.*" %>
	<%@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
   <%@ page import = "Sgh.GestionHistoria.ControlHistoria" %>
   <%@ page import = "Sgh.Laboratorios.*" %>
   <%@ page import = "SinergiaBasic.Utilidades.Mensajes" %>
    
	 <%@ page import = "java.text.NumberFormat" %>
	 

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Sgh.AdminSeguridad.ControlAdmin" scope="session" />
    <jsp:useBean id="beanHisto" class="Sgh.GestionHistoria.ControlHistoria" scope="session" /> 	 
    <jsp:useBean id="beanLab" class="Sgh.Laboratorios.ControlLab" scope="session" />

 <raiz>	  	


<%
    beanAdmin.setCn(beanSession.getCn());
    beanHisto.setCn(beanSession.getCn());	
	 beanLab.setCn(beanSession.getCn());//lab	
   
   //beanRep.setCn(beanSession.getCn());
    java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
    NumberFormat nf = NumberFormat.getInstance(Locale.US);
    DecimalFormat df = (DecimalFormat)nf;
    df.applyPattern("###,##0.00");
    ArrayList resultaux=new ArrayList();
	
	System.out.print("guardarSGH_xml: accion "+request.getParameter("accion"));

	///// OJO PONER LO DE LA SESSION EXPIRADA AQUI 

    boolean resultado=false;   
    if(request.getParameter("accion")!=null ){
		
	if(request.getParameter("accion").equals("Usuario__")){  
		 
		// beanAdmin.usuario.setRolUsuarioSession(beanSession.usuario.getRolUsuarioSession());
		 
		 beanAdmin.usuario.setTipoId(request.getParameter("tipoId"));
		 beanAdmin.usuario.setIdentificacion(request.getParameter("id"));  
		 beanAdmin.usuario.setLogin(request.getParameter("login"));
		 beanAdmin.usuario.setContrasena(request.getParameter("contrasena"));  
		 beanAdmin.usuario.setRol(request.getParameter("rol"));
		 beanAdmin.usuario.setEstado(request.getParameter("estado"));  
		 
		 resultado=beanAdmin.asignaGuardar(request.getParameter("accion"));
	 }
	
	  
	  /*****		 LABORATORIOS 			*****/
	  
	  /*  coprologico */
	  else if(request.getParameter("accion").equals("coprologico")){ 
		 /*
		  codigoexamen,  consistencia, color, moco, sangreoculta, phreaccion, leucocitos, hematies,
		acidosgrasos, grasasneutras, almidones, fibrasmasculares, levaduras, blastocystis, florabacteriana,
		protozoos, trofozoitosquistes, e_histo, e_coli, e_nana, lodamoeba, giardia, helmintos,
		ascaris, tricocefalo, uncinaria, oxiuros, taenia, larvasstercoralis, otros
		*/
			beanLab.copro.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.copro.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.copro.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.copro.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.copro.setCodigoexamen(request.getParameter(""));
			  beanLab.copro.setConsistencia(request.getParameter("txtConsistencia"));
			  beanLab.copro.setColor(request.getParameter("txtColor"));			 
			  beanLab.copro.setMoco(request.getParameter("txtMoco"));
			  beanLab.copro.setSangreoculta(request.getParameter("txtSangreOculta"));
			  beanLab.copro.setPhreaccion(request.getParameter("txtPHReaccion"));			  
			  beanLab.copro.setLeucocitos(request.getParameter("txtLeucocitos"));
			  beanLab.copro.setHematies(request.getParameter("txtHematies"));
			  beanLab.copro.setAcidosgrasos(request.getParameter("txtAcidosGrasos"));
			  beanLab.copro.setGrasasneutras(request.getParameter("txtGrasasNeutras"));
			  beanLab.copro.setAlmidones(request.getParameter("txtAlmidones"));  
			  beanLab.copro.setFibrasmasculares(request.getParameter("txtFibrasMasculares"));
			  beanLab.copro.setLevaduras(request.getParameter("txtLevaduras"));
			  beanLab.copro.setBlastocystis(request.getParameter("txtBlastocystis"));
			  beanLab.copro.setFlorabacteriana(request.getParameter("txtFloraBacteriana"));
			  beanLab.copro.setProtozoos(request.getParameter("txtProtozoos"));
			  beanLab.copro.setTrofozoitosquistes(request.getParameter("txtTrofozoitos"));
			  beanLab.copro.setE_histo(request.getParameter("txtEHistolytica"));
			  beanLab.copro.setE_coli(request.getParameter("txtEColi"));
			  beanLab.copro.setE_nana(request.getParameter("txtENana"));
			  beanLab.copro.setLodamoeba(request.getParameter("txtLodamoeba"));
			  beanLab.copro.setGiardia(request.getParameter("txtGiardia"));
			  beanLab.copro.setHelmintos(request.getParameter("txtHelmintos"));
			  beanLab.copro.setAscaris(request.getParameter("txtAscaris"));
			  beanLab.copro.setTricocefalo(request.getParameter("txtTricocefalo"));
			  beanLab.copro.setUncinaria(request.getParameter("txtUncinaria"));
			  beanLab.copro.setOxiuros(request.getParameter("txtOxiuros"));
			  beanLab.copro.setTaenia(request.getParameter("txtTaenia"));
			  beanLab.copro.setLarvasstercoralis(request.getParameter("txtStercoralis"));
			  beanLab.copro.setOtros(request.getParameter("txaObservaciones"));
			  beanLab.copro.setMicelios(request.getParameter("txtMicelios"));

			  
			  //beanHisto.paciente.setTipoIdUsuaCreaHistori(beanSession.usuario.getTipoId());			  
           //beanHisto.paciente.setIdUsuaCreaHistori(beanSession.usuario.getIdentificacion());			  
			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.copro.msg = new Mensajes();
			  resultado=beanLab.copro.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.copro.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }

	  /*  cuadroHematico */
	  else if(request.getParameter("accion").equals("cuadroHematico")){ 
		 /*
		codigoexamen, hematocrito, hemoglobina, plaquetas, reticulocitos,
		eritrosedim, leucocitos, neutrofilos, linfocitos, eosinofilos,
		monocitos, basofilos, cayados, observaciones	
		 */
			beanLab.cdrhem.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.cdrhem.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.cdrhem.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.cdrhem.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.cdrhem.setCodigoexamen(request.getParameter(""));
			  beanLab.cdrhem.setHematocrito(request.getParameter("txtHematocrito"));
			  beanLab.cdrhem.setHemoglobina(request.getParameter("txtHemoglobina"));			 
			  beanLab.cdrhem.setPlaquetas(request.getParameter("txtPlaquetas"));
			  beanLab.cdrhem.setReticulocitos(request.getParameter("txtReticulocitos"));
			  beanLab.cdrhem.setEritrosedim(request.getParameter("txtEritrosedimentacion"));			  
			  beanLab.cdrhem.setLeucocitos(request.getParameter("txtLeucocitos"));
			  beanLab.cdrhem.setNeutrofilos(request.getParameter("txtNeutrofilos"));
			  beanLab.cdrhem.setLinfocitos(request.getParameter("txtLinfocitos"));
			  beanLab.cdrhem.setEosinofilos(request.getParameter("txtEosinofilos"));
			  beanLab.cdrhem.setMonocitos(request.getParameter("txtMonocitos"));  
			  beanLab.cdrhem.setBasofilos(request.getParameter("txtBasofilos"));
			  beanLab.cdrhem.setCayados(request.getParameter("txtCayados"));
			  beanLab.cdrhem.setObservaciones(request.getParameter("txaObservaciones"));
			  beanLab.cdrhem.setGruposanguineo(request.getParameter("txtGrupoSanguineo"));
			  beanLab.cdrhem.setFactorrh(request.getParameter("txtFactorRH"));

			  
			  //beanHisto.paciente.setTipoIdUsuaCreaHistori(beanSession.usuario.getTipoId());			  
           //beanHisto.paciente.setIdUsuaCreaHistori(beanSession.usuario.getIdentificacion());			  
			  
			  //resultado=beanLab.asignaGuardar(request.getParameter("accion"));
			  beanLab.cdrhem.msg = new Mensajes();
			  resultado=beanLab.cdrhem.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.cdrhem.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }
	  	  
	  /*  cuadroHematicoAdmEstandar */
	  else if(request.getParameter("accion").equals("cuadroHematicoAdmEstandar")){ 
		 /*
		codigoexamen, hematocrito, hemoglobina, plaquetas, reticulocitos,
		eritrosedim, leucocitos, neutrofilos, linfocitos, eosinofilos,
		monocitos, basofilos, cayados, observaciones	
		 */
			//beanLab.cdrhem.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.cdrhem.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.cdrhem.setCodigoexamen(request.getParameter(""));
			  beanLab.cdrhem.setHematocrito(request.getParameter("txtHematocrito"));
			  beanLab.cdrhem.setHemoglobina(request.getParameter("txtHemoglobina"));			 
			  beanLab.cdrhem.setPlaquetas(request.getParameter("txtPlaquetas"));
			  beanLab.cdrhem.setReticulocitos(request.getParameter("txtReticulocitos"));
			  beanLab.cdrhem.setEritrosedim(request.getParameter("txtEritrosedimentacion"));			  
			  beanLab.cdrhem.setLeucocitos(request.getParameter("txtLeucocitos"));
			  beanLab.cdrhem.setNeutrofilos(request.getParameter("txtNeutrofilos"));
			  beanLab.cdrhem.setLinfocitos(request.getParameter("txtLinfocitos"));
			  beanLab.cdrhem.setEosinofilos(request.getParameter("txtEosinofilos"));
			  beanLab.cdrhem.setMonocitos(request.getParameter("txtMonocitos"));  
			  beanLab.cdrhem.setBasofilos(request.getParameter("txtBasofilos"));
			  beanLab.cdrhem.setCayados(request.getParameter("txtCayados"));
			  beanLab.cdrhem.setObservaciones(request.getParameter("txaObservaciones"));
			  
			  //beanHisto.paciente.setTipoIdUsuaCreaHistori(beanSession.usuario.getTipoId());			  
           //beanHisto.paciente.setIdUsuaCreaHistori(beanSession.usuario.getIdentificacion());			  
			  
			  //resultado=beanLab.asignaGuardar(request.getParameter("accion"));
			  beanLab.cdrhem.msg = new Mensajes();
			  resultado=beanLab.cdrhem.guardarAdmEstandar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.cdrhem.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }

		/*  quimicaSanguinea */
	  else if(request.getParameter("accion").equals("quimicaSanguinea")){ 
		 /*
		 codigoexamen, acidourico, glicemia, creatinina, nitrog_ureico,
       bilirrubinatotal, bilirrubinadirecta, bilirrubinaindirecta, tgo,
       tgp, fosfa_alcalina, colesteroltotal, trigliceridos, colesterolhdl,
       colesterolldl, colesterolvldl, indicearterial, observaciones
		*/
			beanLab.quisan.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.quisan.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.quisan.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.quisan.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.quisan.setCodigoexamen(request.getParameter(""));
			  beanLab.quisan.setAcidourico(request.getParameter("txtAcidoUrico"));
			  beanLab.quisan.setGlicemia(request.getParameter("txtGlicemia"));			 
			  beanLab.quisan.setCreatinina(request.getParameter("txtCreatinina"));
			  beanLab.quisan.setNitrog_ureico(request.getParameter("txtNitroUreico"));
			  beanLab.quisan.setBilirrubinatotal(request.getParameter("txtBiliTotal"));			  
			  beanLab.quisan.setBilirrubinadirecta(request.getParameter("txtBiliDirecta"));
			  beanLab.quisan.setBilirrubinaindirecta(request.getParameter("txtBiliIndirecta"));
			  beanLab.quisan.setTgo(request.getParameter("txtTGO"));
			  beanLab.quisan.setTgp(request.getParameter("txtTGP"));
			  beanLab.quisan.setFosfa_alcalina(request.getParameter("txtFosfaAlcalina"));	  
			  beanLab.quisan.setColesteroltotal(request.getParameter("txtColesterolTotal"));
			  beanLab.quisan.setTrigliceridos(request.getParameter("txtTrigliceridos"));
			  beanLab.quisan.setColesterolhdl(request.getParameter("txtColesterolHDL"));
			  beanLab.quisan.setColesterolldl(request.getParameter("txtColesterolLDL"));
			  beanLab.quisan.setColesterolvldl(request.getParameter("txtColesterolVLDL"));
			  beanLab.quisan.setIndicearterial(request.getParameter("txtIndiceArterial"));
			  beanLab.quisan.setObservaciones(request.getParameter("txaObservaciones"));
			  
			  //beanHisto.paciente.setTipoIdUsuaCreaHistori(beanSession.usuario.getTipoId());			  
           //beanHisto.paciente.setIdUsuaCreaHistori(beanSession.usuario.getIdentificacion());			  
			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.quisan.msg = new Mensajes();
			  resultado=beanLab.quisan.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.quisan.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }
	  
	  /*  quimicaSanguineaAdmEstandar */
	  else if(request.getParameter("accion").equals("quimicaSanguineaAdmEstandar")){ 
		 /*
		 codigoexamen, acidourico, glicemia, creatinina, nitrog_ureico,
       bilirrubinatotal, bilirrubinadirecta, bilirrubinaindirecta, tgo,
       tgp, fosfa_alcalina, colesteroltotal, trigliceridos, colesterolhdl,
       colesterolldl, colesterolvldl, indicearterial, observaciones
		*/
			//beanLab.quisan.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.quisan.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.quisan.setCodigoexamen(request.getParameter(""));
			  beanLab.quisan.setAcidourico(request.getParameter("txtAcidoUrico"));
			  beanLab.quisan.setGlicemia(request.getParameter("txtGlicemia"));			 
			  beanLab.quisan.setCreatinina(request.getParameter("txtCreatinina"));
			  beanLab.quisan.setNitrog_ureico(request.getParameter("txtNitroUreico"));
			  beanLab.quisan.setBilirrubinatotal(request.getParameter("txtBiliTotal"));			  
			  beanLab.quisan.setBilirrubinadirecta(request.getParameter("txtBiliDirecta"));
			  beanLab.quisan.setBilirrubinaindirecta(request.getParameter("txtBiliIndirecta"));
			  beanLab.quisan.setTgo(request.getParameter("txtTGO"));
			  beanLab.quisan.setTgp(request.getParameter("txtTGP"));
			  beanLab.quisan.setFosfa_alcalina(request.getParameter("txtFosfaAlcalina"));	  
			  beanLab.quisan.setColesteroltotal(request.getParameter("txtColesterolTotal"));
			  beanLab.quisan.setTrigliceridos(request.getParameter("txtTrigliceridos"));
			  beanLab.quisan.setColesterolhdl(request.getParameter("txtColesterolHDL"));
			  beanLab.quisan.setColesterolldl(request.getParameter("txtColesterolLDL"));
			  beanLab.quisan.setColesterolvldl(request.getParameter("txtColesterolVLDL"));
			  beanLab.quisan.setIndicearterial(request.getParameter("txtIndiceArterial"));
			  beanLab.quisan.setObservaciones(request.getParameter("txaObservaciones"));
			  			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.quisan.msg = new Mensajes();
			  resultado=beanLab.quisan.guardarAdmEstandar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.quisan.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }
	  
	  /*  inmunologia */
	  else if(request.getParameter("accion").equals("inmunologia")){ 
		 /*
		 codigoexamen, factor_rematoideo, pcr, astos, serologia, tecnica,
       ant_tifico_o, ant_tifico_h, ant_paratifico_a, ant_paratifico_b,
       ant_proteus, ant_brucella, observaciones
		*/
			beanLab.inmuno.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.inmuno.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.inmuno.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.inmuno.setProfesional(request.getParameter("txtProfesionalRegistro"));

			  beanLab.inmuno.setCodigoexamen(request.getParameter(""));
			  beanLab.inmuno.setFactor_rematoideo(request.getParameter("factor_rematoideo"));
			  beanLab.inmuno.setPcr(request.getParameter("pcr"));
			  beanLab.inmuno.setAstos(request.getParameter("astos"));
			  beanLab.inmuno.setSerologia(request.getParameter("serologia"));			  
			  beanLab.inmuno.setTecnica(request.getParameter("tecnica"));
			  beanLab.inmuno.setAnt_tifico_o(request.getParameter("ant1"));
			  beanLab.inmuno.setAnt_tifico_h(request.getParameter("ant2"));
			  beanLab.inmuno.setAnt_paratifico_a(request.getParameter("ant3"));
			  beanLab.inmuno.setAnt_paratifico_b(request.getParameter("ant4"));	  
			  beanLab.inmuno.setAnt_proteus(request.getParameter("ant5"));
			  beanLab.inmuno.setAnt_brucella(request.getParameter("ant6"));
			  beanLab.inmuno.setObservaciones(request.getParameter("observaciones"));
			  			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.inmuno.msg = new Mensajes();
			  resultado=beanLab.inmuno.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.inmuno.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }
	  
	  /*  inmunologiaAdmEstandar */
	  else if(request.getParameter("accion").equals("inmunologiaAdmEstandar")){ 
		 /*
		 codigoexamen, factor_rematoideo, pcr, astos, serologia, tecnica,
       ant_tifico_o, ant_tifico_h, ant_paratifico_a, ant_paratifico_b,
       ant_proteus, ant_brucella, observaciones
		*/
			//beanLab.quisan.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.inmuno.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.inmuno.setCodigoexamen(request.getParameter(""));
			  beanLab.inmuno.setFactor_rematoideo(request.getParameter("factor_rematoideo"));
			  beanLab.inmuno.setPcr(request.getParameter("pcr"));
			  beanLab.inmuno.setAstos(request.getParameter("astos"));
			  beanLab.inmuno.setSerologia(request.getParameter("serologia"));			  
			  beanLab.inmuno.setTecnica(request.getParameter("tecnica"));
			  beanLab.inmuno.setAnt_tifico_o(request.getParameter("ant1"));
			  beanLab.inmuno.setAnt_tifico_h(request.getParameter("ant2"));
			  beanLab.inmuno.setAnt_paratifico_a(request.getParameter("ant3"));
			  beanLab.inmuno.setAnt_paratifico_b(request.getParameter("ant4"));	  
			  beanLab.inmuno.setAnt_proteus(request.getParameter("ant5"));
			  beanLab.inmuno.setAnt_brucella(request.getParameter("ant6"));
			  beanLab.inmuno.setObservaciones(request.getParameter("observaciones"));
			  			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.inmuno.msg = new Mensajes();
			  resultado=beanLab.inmuno.guardarAdmEstandar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.inmuno.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }
	  
	  /*  frotisGram */
	  else if(request.getParameter("accion").equals("frotisGram")){ 
		 /*
		 TABLE lab.frotisgram
		  codigoexamen,
		  ph, aminas, hematies, trichomonas, koh,
		  celulasguia, rpmn_vaginal, levaduras, micelios, mobiluncos,
		  floranormal, florapredominante, rpmn_cervical, cocosgramnextra, cocosgramnintra,
		  observaciones
		*/
			beanLab.gram.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.gram.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.copro.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.gram.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.gram.setCodigoexamen(request.getParameter(""));
			  beanLab.gram.setPh(request.getParameter("txtPH"));
			  beanLab.gram.setAminas(request.getParameter("cmbAminas"));			 
			  beanLab.gram.setHematies(request.getParameter("cmbHematies"));
			  beanLab.gram.setTrichomonas(request.getParameter("cmbTrichomonas"));
			  beanLab.gram.setKoh(request.getParameter("cmbKOH"));			  
			  beanLab.gram.setCelulasguia(request.getParameter("cmbCelulaGuia"));
			  beanLab.gram.setRpmn_vaginal(request.getParameter("cmbPMNvaginal"));
			  beanLab.gram.setLevaduras(request.getParameter("cmbLevaduras"));
			  beanLab.gram.setMicelios(request.getParameter("cmbSeudoMicelios"));
			  beanLab.gram.setMobiluncos(request.getParameter("cmbMobiluncos"));  
			  beanLab.gram.setFloranormal(request.getParameter("cmbFloraNormal"));
			  beanLab.gram.setFlorapredominante(request.getParameter("txaFloraPredominante"));
			  beanLab.gram.setRpmn_cervical(request.getParameter("cmbPMNcervical"));
			  beanLab.gram.setCocosgramnextra(request.getParameter("cmbDiplococosExtra"));
			  beanLab.gram.setCocosgramnintra(request.getParameter("cmbDiplococosIntra"));
			  beanLab.gram.setObservaciones(request.getParameter("txaObservaciones"));
			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.gram.msg = new Mensajes();
			  resultado=beanLab.gram.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.gram.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }

	  /*  pruebaEmbarazo */
	  else if(request.getParameter("accion").equals("pruebaEmbarazo")){ 
		 /*
		 TABLE 
		*/
			beanLab.pruebaemb.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.pruebaemb.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.copro.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.pruebaemb.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.pruebaemb.setCodigoexamen(request.getParameter(""));
			  beanLab.pruebaemb.setHormona(request.getParameter("cmbHormona"));
			  beanLab.pruebaemb.setClasetecnica(request.getParameter("cmbTecnica"));			 
			  beanLab.pruebaemb.setFum(request.getParameter("txtFUM"));
			  beanLab.pruebaemb.setCualtecnica(request.getParameter("txtCualTecnica"));
			  beanLab.pruebaemb.setSensibilidad(request.getParameter("txtSensibilidad"));			  
			  beanLab.pruebaemb.setObservaciones(request.getParameter("txaObservaciones"));
			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.pruebaemb.msg = new Mensajes();
			  resultado=beanLab.pruebaemb.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.pruebaemb.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }

		/*  uroanalisis */
	  else if(request.getParameter("accion").equals("uroanalisis")){ 
		 /*
		 codigoexamen, aspecto, color, densidad, ph, albumina, glucosa,
       cetonas, est_leucocitarias, hemoglobina, bilirrubina, urobilinogeno,
       nitritos, cel_epiteliales, cel_renales, hematies, leucocitos,
       piocitos, bacterias, levaduras, micelios, moco, cil_granulososfinos,
       cil_granulososgruesos, cil_hialinos, cil_cereos, cil_leucocitarios,
       cil_eritrocitarios, cri_uratosamorfos, cri_fosfatosamorfos, cri_otros,
       observaciones
		*/
			beanLab.uro.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.uro.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.uro.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.uro.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.uro.setCodigoexamen(request.getParameter(""));
			  beanLab.uro.setAspecto(request.getParameter("txtAspecto"));
			  beanLab.uro.setDensidad(request.getParameter("txtDensidad"));			 
			  beanLab.uro.setColor(request.getParameter("txtColor"));
			  beanLab.uro.setPh(request.getParameter("txtPH"));
			  beanLab.uro.setAlbumina(request.getParameter("txtAlbumina"));			  
			  beanLab.uro.setGlucosa(request.getParameter("txtGlucosa"));
			  beanLab.uro.setCetonas(request.getParameter("txtCetonas"));
			  beanLab.uro.setEst_leucocitarias(request.getParameter("txtEstLeucocitarias"));
			  beanLab.uro.setBilirrubina(request.getParameter("txtBilirrubina"));
			  beanLab.uro.setHemoglobina(request.getParameter("txtHemoglobina"));	  
			  beanLab.uro.setUrobilinogeno(request.getParameter("txtUrobilinogeno"));
			  beanLab.uro.setNitritos(request.getParameter("txtNitritos"));
			  beanLab.uro.setCel_epiteliales(request.getParameter("txtCelEpiteliales"));
			  beanLab.uro.setCel_renales(request.getParameter("txtCelRenales"));
			  beanLab.uro.setHematies(request.getParameter("txtHematies"));
			  beanLab.uro.setLeucocitos(request.getParameter("txtLeucocitos"));
			  beanLab.uro.setPiocitos(request.getParameter("txtPiocitos"));
			  beanLab.uro.setBacterias(request.getParameter("txtBacterias"));
			  beanLab.uro.setLevaduras(request.getParameter("txtLevaduras"));
			  beanLab.uro.setMicelios(request.getParameter("txtMicelios"));
			  beanLab.uro.setMoco(request.getParameter("txtMoco"));
			  beanLab.uro.setCil_granulososfinos(request.getParameter("txtCilGranulososFin"));
			  beanLab.uro.setCil_granulososgruesos(request.getParameter("txtCilGranulososGru"));
			  beanLab.uro.setCil_hialinos(request.getParameter("txtCilHialinos"));
			  beanLab.uro.setCil_cereos(request.getParameter("txtCilCereos"));
			  beanLab.uro.setCil_leucocitarios(request.getParameter("txtCilLeucocitarios"));
			  beanLab.uro.setCil_eritrocitarios(request.getParameter("txtCilEritrocitarios"));
			  beanLab.uro.setCri_uratosamorfos(request.getParameter("txtCriUratos"));
			  beanLab.uro.setCri_fosfatosamorfos(request.getParameter("txtCriFosfatos"));
			  beanLab.uro.setCri_otros(request.getParameter("txaCriOtros"));
			  beanLab.uro.setObservaciones(request.getParameter("txaObservaciones"));
			  
			  //beanHisto.paciente.setTipoIdUsuaCreaHistori(beanSession.usuario.getTipoId());			  
           //beanHisto.paciente.setIdUsuaCreaHistori(beanSession.usuario.getIdentificacion());			  
			  
			  //resultado=beanLab.asignaGuardar(request.getParameter("accion"));
			  beanLab.uro.msg = new Mensajes();
			  resultado=beanLab.uro.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.uro.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }
	  
	  /* solicitudCupsLaboratorio */
	  else if(request.getParameter("accion").equals("solicitudCupsLaboratorio")){ 
		 /*
		
		*/
			beanLab.copro.setTipoid(request.getParameter("txtEncTipoId"));
			beanLab.copro.setIdentificacion(request.getParameter("txtEncIdentificacion"));
			//beanLab.copro.setFechaRegistro(request.getParameter("txtFechaRegistro"));
			beanLab.copro.setProfesional(request.getParameter("txtProfesionalRegistro"));
	  		  
			  beanLab.copro.setCodigoexamen(request.getParameter(""));
			  beanLab.copro.setConsistencia(request.getParameter("txtConsistencia"));
			  beanLab.copro.setColor(request.getParameter("txtColor"));			 
			  beanLab.copro.setMoco(request.getParameter("txtMoco"));
			  beanLab.copro.setOtros(request.getParameter("txaObservaciones"));

			  
			  //beanHisto.paciente.setTipoIdUsuaCreaHistori(beanSession.usuario.getTipoId());			  
           //beanHisto.paciente.setIdUsuaCreaHistori(beanSession.usuario.getIdentificacion());			  
			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.copro.msg = new Mensajes();
			  resultado=beanLab.copro.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.copro.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }
	  
	  /***** terceros *****/
	  
	  /*  admTerceros */
	  else if(request.getParameter("accion").equals("admTerceros")){ 
		 /*
		 id_tercero, tipoid_tercero, identificacion_tercero, razonsocial, 
       direccion, telefono, contacto, usuario, estado, observaciones
		*/
			  beanLab.tercero.setId_tercero(request.getParameter(""));
			  beanLab.tercero.setTipoid_tercero(request.getParameter("cmbTipoId"));
			  beanLab.tercero.setIdentificacion_tercero(request.getParameter("txtIdentificacion"));
			  beanLab.tercero.setRazonsocial(request.getParameter("txtRazonSocial"));
			  beanLab.tercero.setDireccion(request.getParameter("txtDireccion"));			  
			  beanLab.tercero.setTelefono(request.getParameter("txtTelefono"));
			  beanLab.tercero.setContacto(request.getParameter("txtContacto"));
			  beanLab.tercero.setUsuario(request.getParameter("txtUsuarioSistema"));
			  beanLab.tercero.setEstado(request.getParameter("cmbEstado"));
			  beanLab.tercero.setObservaciones(request.getParameter("txaObservaciones"));
			  			  
			  //resultado=beanLab.copro.asignaGuardar(request.getParameter("accion"));
			  beanLab.tercero.msg = new Mensajes();
			  resultado=beanLab.tercero.guardar();
 			%>
			<sis_mensaje><![CDATA[<%= beanLab.tercero.msg.SIS_MENSAJE %>]]></sis_mensaje>
			<%														 			  
			  
	  }	  

	  /*********** fin laboratorios  *************/

	 /*  no se */ 

//	  aqui un nuevo
    }	
	
%>

   <respuesta><![CDATA[<%= resultado%>]]></respuesta>	  
 	 
 </raiz>	  	          