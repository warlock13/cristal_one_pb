<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>

<%@page import="Clinica.GestionHistoriaC.Tipificacion_v2"%>
<%@page import="Clinica.GestionHistoriaC.Citas"%>
<%@page import="Clinica.GestionHistoriaC.ControlHistoriaC"%>
<%@ page contentType="text/xml"%>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>
<%@ page import = "Clinica.Presentacion.*"%>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin"%>
<%@ page import = "java.text.NumberFormat"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@page import="Clinica.GestionHistoriaC.TipificacionExterna"%>
<%@page import="WebServicesSF.WebServicesSF"%>
<%@ page import="FacturacionElectronica.EnviarFactura.FacturacionElectronica"%>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->    

<raiz>	  	
    <%
        Fecha fecha = new Fecha();
        ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
        Conexion cn = new Conexion(iConnection.getConnection());
        cn.usuario = beanSession.getUsuario();

        ControlHistoriaC beanHC = new ControlHistoriaC();
        Citas beanCitas = new Citas();

        beanHC.setCn(cn);
        beanCitas.setCn(cn);

        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
        java.util.Date date = cal.getTime();
        java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("###,##0.00");

        SimpleDateFormat fdia = new SimpleDateFormat("dd");
        SimpleDateFormat fmes = new SimpleDateFormat("MM");
        SimpleDateFormat fanio = new SimpleDateFormat("yyyy");

        String dia = fdia.format((java.util.Calendar.getInstance(Locale.US)).getTime());
        String mes = fmes.format((java.util.Calendar.getInstance(Locale.US)).getTime());
        String anio = fanio.format((java.util.Calendar.getInstance(Locale.US)).getTime());
        String fechaHoy = dia + "/" + mes + "/" + anio;

        ArrayList resultaux = new ArrayList();
        ArrayList resultDocs = new ArrayList();

        boolean resultado = false;
        String dato = "dddd";
        if (request.getParameter("accion") != null) {
            if (request.getParameter("accion").equals("buscarIdentificacionPaciente")
                    || request.getParameter("accion").equals("buscarIdentificacionPacienteUrgencias")
                    || request.getParameter("accion").equals("buscarIdPacienteEncuesta")) {
                beanHC.paciente.setTipoId(request.getParameter("cmbTipoId"));
                beanHC.paciente.setIdentificacion(request.getParameter("txtIdentificacion"));
                resultado = beanHC.paciente.buscarIdentificacionPaciente();
    %>
    <existe><![CDATA[<%= beanHC.paciente.getExiste()%>]]></existe> 
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("auditarFE")) {
            FacturacionElectronica FE = new FacturacionElectronica(request.getParameter("lblIdFactura"));
            FE.enviarFactura();
        %>
    <diligenciado><![CDATA[<%= true %>]]></diligenciado>  
        <%
        } else if (request.getParameter("accion").equals("auditarNotaCreditoFE")) {
            FacturacionElectronica FE = new FacturacionElectronica(request.getParameter("lblIdFactura"));
            resultado = FE.enviarNotaCredito();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>  
        <%
        } else if (request.getParameter("accion").equals("validaNumeracionFactura")) {
            beanHC.facturacion.setIdFactura(request.getParameter("lblIdFactura"));
            beanHC.facturacion.setId(Integer.parseInt(request.getParameter("txtIdPaciente")));
            resultado = beanHC.facturacion.validaNumeracionFactura();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.facturacion.getNoticia()%>]]></NOTICIA>          
    <estadoAjuste><![CDATA[<%= beanHC.facturacion.getEstadoAjuste()%>]]></estadoAjuste>          
        <%
        } else if (request.getParameter("accion").equals("auditarFactura")) {
            beanHC.facturacion.setIdFactura(request.getParameter("lblIdFactura"));
            resultado = beanHC.facturacion.auditarFactura();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
        <%
        } else if (request.getParameter("accion").equals("totalEnvioRips")) {
            String totalCuenta = beanHC.facturacion.totalEnvioRips(request.getParameter("idEnvio"));
        %>
    <totalEnvioRips><![CDATA[<%= totalCuenta%>]]></totalEnvioRips>
        <%
        } else if (request.getParameter("accion").equals("auditarRecibo")) {
            if (beanHC.facturacion.auditarRecibo(request.getParameter("lblIdRecibo")) != null) {
                resultado = true;
            } else {
                resultado = false;
            }
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>             
        <%
        } else if (request.getParameter("accion").equals("anularFacturaWS")) {
            beanHC.facturacion.setIdFactura(request.getParameter("lblIdFactura"));
            resultado = beanHC.facturacion.anularFacturas();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>         

    <%
    } else if (request.getParameter("accion").equals("enviarSF")) {
        WebServicesSF ws = new WebServicesSF(request.getParameter("lblIdEvolucion"));
        ws.setConexion(cn);
        ws.enviarMedicamentos();
        resultado = true;
    %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>         
        <%
        } else if (request.getParameter("accion").equals("consultarSF")) {
            WebServicesSF ws = new WebServicesSF();
            ws.setConexion(cn);
            ws.consultaOrdenes();
            resultado = true;
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>         
        <%
        } else if (request.getParameter("accion").equals("cerrarDocumentoSolicitudesInventario_")) {
            /*beanHC.paciente.setVar2( request.getParameter("lblIdDoc"));*/
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdDoc")));
            resultado = beanHC.paciente.cerrarDocumentoSolicitudesInventario();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>           
        <%
        } else if (request.getParameter("accion").equals("modificarTransaccionDocumento")) {
            /*boton de modificar transaccion  inventario*/
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdTr")));
            beanHC.paciente.setVar6(Integer.parseInt(request.getParameter("txtTrCantidadAnt")));
            beanHC.paciente.setVar7(Integer.parseInt(request.getParameter("txtTrCantidad")));
            beanHC.paciente.setVar8(Float.parseFloat(request.getParameter("txtVlrTr")));
            beanHC.paciente.setVar9(Float.parseFloat(request.getParameter("lblVlrImpuesto")));
            beanHC.paciente.setVar10(Float.parseFloat(request.getParameter("cmbVlrIva")));
            beanHC.paciente.setVar2(request.getParameter("txtLoteTr"));
            beanHC.paciente.setVar3(request.getParameter("txtFechaTr"));
            beanHC.paciente.setVar4(request.getParameter("txtLoteAnt"));
            resultado = beanHC.paciente.modificarTransaccionDocumento();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>          
        <%
        } else if (request.getParameter("accion").equals("eliminarTransaccionDocumento")) {
            /*boton de modificar transaccion  inventario*/
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdTr")));
            beanHC.paciente.setVar6(Integer.parseInt(request.getParameter("txtTrCantidadAnt")));
            beanHC.paciente.setVar7(Integer.parseInt(request.getParameter("txtTrCantidad")));
            beanHC.paciente.setVar8(Float.parseFloat(request.getParameter("txtVlrTr")));
            beanHC.paciente.setVar9(Float.parseFloat(request.getParameter("lblVlrImpuesto")));
            beanHC.paciente.setVar10(Float.parseFloat(request.getParameter("cmbVlrIva")));
            beanHC.paciente.setVar2(request.getParameter("txtLoteTr"));
            beanHC.paciente.setVar3(request.getParameter("txtFechaTr"));
            beanHC.paciente.setVar4(request.getParameter("txtLoteAnt"));
            resultado = beanHC.paciente.eliminarTransaccionDocumento();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>          
        <%
        } else if (request.getParameter("accion").equals("cerrarDocumentoProgramacion")) {
            /*beanHC.paciente.setVar2( request.getParameter("lblIdDoc"));*/
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdDoc")));
            resultado = beanHC.paciente.verificaTransaccionesInventario();

        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>          
        <%
        } else if (request.getParameter("accion").equals("cerrarDocumentoSolicitudesInventario")) {
            /*beanHC.paciente.setVar2( request.getParameter("lblIdDoc"));*/
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdDoc")));
            resultado = beanHC.paciente.verificaTransaccionesInventario();

        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>          
        <%
        } else if (request.getParameter("accion").equals("verificaTransaccionesInventario")) {
            /*boton de salida de inventario*/
 /*beanHC.paciente.setVar2( request.getParameter("lblIdDoc"));*/
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdDoc")));
            resultado = beanHC.paciente.verificaTransaccionesInventario();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>          
        <%
        } else if (request.getParameter("accion").equals("verificaTransaccionesInventarioTrasladoBodega")) {
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdDoc")));
            beanHC.paciente.setVar6(Integer.parseInt(request.getParameter("cmbIdBodegaDestino")));
            resultado = beanHC.paciente.hacerTrasladoEntreBodegas();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>          
        <%
        } else if (request.getParameter("accion").equals("consultaExamenFisico")) {
            beanHC.documentoHC.setIdEvolucion(request.getParameter("lblIdDocumento"));
            resultado = beanHC.documentoHC.consultaExamenFisico();
        %>  
    <examenFisicoDescripcion><![CDATA[<%= beanHC.documentoHC.getExamenFisico()%>]]></examenFisicoDescripcion>          
        <%
        } else if (request.getParameter("accion").equals("elementosDelFolio")) {
            beanHC.paciente.setVar2(request.getParameter("lblTipoDocumento"));
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("lblIdDocumento")));
            resultado = beanHC.paciente.buscarElementosObligatoriosFolio();
        %>
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>
    <NOTICIA><![CDATA[<%= beanHC.paciente.getNoticia()%>]]></NOTICIA>          
        <%
        } else if (request.getParameter("accion").equals("consultarDisponibleDesdeAgenda")) {
            beanHC.citas.setVar7(Integer.parseInt(request.getParameter("lblIdAgendaDetalle")));
            beanHC.citas.setVar2(request.getParameter("txtIdBusPaciente"));
            beanHC.citas.setVar3(request.getParameter("cmbTipoCita"));
            beanHC.citas.setVar11(request.getParameter("lblIdListaEspera"));
            //  System.out.println("existe= " + beanHC.paciente.consultarDisponibleDesdeAgenda() );		  
%>
    <existe><%= beanHC.citas.consultarDisponibleDesdeAgenda()%></existe>
        <%
        } else if (request.getParameter("accion").equals("consultarDisponibleDesdeListaEspera")) {

            beanHC.citas.setVar2(request.getParameter("txtIdBusPaciente"));
            beanHC.citas.setVar3(request.getParameter("cmbTipoCita"));
            // beanHC.citas.setVar4(request.getParameter("cmbIdSubEspecialidad"));	

        %>
    <existe><%= beanHC.citas.consultarDisponibleDesdeListaEspera()%></existe>
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPos")) {
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("txtIdArticulo")));
            resultado = beanHC.paciente.preguntarMedicamentoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosProcedim")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosProcedimientoNotificacion")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosAyudaDiagnostica")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosAyudaDiagnosticaNotificacion")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosLaboratorio")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosLaboratorioNotificacion")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosTerapia")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("buscarSiEsNoPosTerapiaNotificacion")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
            resultado = beanHC.paciente.preguntarProcedimientoNoPost();
        %>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("nuevoPaciente")) {
            beanHC.paciente.setTipoId(request.getParameter("cmbTipoId"));
            beanHC.paciente.setIdentificacion(request.getParameter("txtIdentificacion"));
            beanHC.paciente.setApellido1(request.getParameter("txtApellido1"));
            beanHC.paciente.setApellido2(request.getParameter("txtApellido2"));
            beanHC.paciente.setNombre1(request.getParameter("txtNombre1"));
            beanHC.paciente.setNombre2(request.getParameter("txtNombre2"));
            beanHC.paciente.setIdMunicipio(request.getParameter("txtMunicipio"));
            beanHC.paciente.setDireccion(request.getParameter("txtDireccionRes"));
            beanHC.paciente.setFechaNacimiento(request.getParameter("txtFechaNac"));
            beanHC.paciente.setSexo(request.getParameter("cmbSexo"));
            beanHC.paciente.setAcompanante(request.getParameter("txtNomAcompanante"));
            beanHC.paciente.setTelefono(request.getParameter("txtTelefonos"));
            beanHC.paciente.setCelular1(request.getParameter("txtCelular1"));
            beanHC.paciente.setCelular2(request.getParameter("txtCelular2"));
            beanHC.paciente.setEmail(request.getParameter("txtEmail"));
            resultado = beanHC.paciente.crearPaciente();
        %>
    <existe><![CDATA[<%= beanHC.paciente.getExiste()%>]]></existe>
    <dato><![CDATA[<%= beanHC.paciente.getPacienteTodo()%>]]></dato>          
        <%
        } else if (request.getParameter("accion").equals("nuevoDocumentoHC")) {
            if (request.getParameter("opcionTipo").equals("MI_AGENDA")) {
                beanHC.documentoHC.setTipoDocumento(request.getParameter("TipoFolio"));
                beanHC.documentoHC.setIdAdmision(request.getParameter("lblIdAdmision"));
                beanHC.documentoHC.setIdPaciente(request.getParameter("lblIdPaciente"));
                beanHC.documentoHC.setIdIdAuxiliar(request.getParameter("lblIdAuxiliar"));
                beanHC.documentoHC.setIdTipoAdmision(request.getParameter("lblIdTipoAdmision"));
                if (request.getParameter("lblIdCita").equals("")) {
                    beanHC.documentoHC.setId_cita(-1);
                } else {
                    beanHC.documentoHC.setId_cita(Integer.parseInt(request.getParameter("lblIdCita")));
                }
                resultado = beanHC.documentoHC.crearFolioConAdmisionEnviada();
            } else if (request.getParameter("opcionTipo").equals("INTERCONSULTA")) {
                beanHC.documentoHC.setTipoDocumento(request.getParameter("TipoFolio"));
                beanHC.documentoHC.setIdPaciente(request.getParameter("lblIdPaciente"));
                beanHC.documentoHC.setIdEspecialidad(request.getParameter("lblIdEspecialidad"));
                beanHC.documentoHC.setIdIdAuxiliar(request.getParameter("lblIdAuxiliar"));
                resultado = beanHC.documentoHC.crearFolioUltimaAdmisionDelPaciente();
            }
        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>
    <id_admision><![CDATA[<%= beanHC.documentoHC.getIdAdmision()%>]]></id_admision>          
        <%
        } else if (request.getParameter("accion").equals("nuevaAdmisionCuenta")) {
            beanHC.documentoHC.setVar3(request.getParameter("txtNoAutorizacion"));
            resultado = beanHC.documentoHC.consultarCuentaConAutorizacion();
        %>
    <no_autorizacion><![CDATA[<%= beanHC.documentoHC.getVar6()%>]]></no_autorizacion>           
        <%
        } else if (request.getParameter("accion").equals("consultarValoresCuenta")) {
            beanHC.facturacion.setIdQuery(request.getParameter("idQuery"));
            beanHC.facturacion.setIdFactura(request.getParameter("lblIdFactura"));
            resultado = beanHC.facturacion.consultarValoresCuenta();
        %>
    <total_cuenta><![CDATA[<%= beanHC.facturacion.getTotalCuenta()%>]]></total_cuenta>          
    <valor_nocubierto><![CDATA[<%= beanHC.facturacion.getValorNoCubierto()%>]]></valor_nocubierto>          
    <valor_cubierto><![CDATA[<%= beanHC.facturacion.getValorCubierto()%>]]></valor_cubierto>          
    <valor_descuento><![CDATA[<%= beanHC.facturacion.getValorDescuento()%>]]></valor_descuento>          
    <total_factura><![CDATA[<%= beanHC.facturacion.getTotalFactura()%>]]></total_factura>
    <medio_pago><![CDATA[<%= beanHC.facturacion.getMedioPago()%>]]></medio_pago>            
    <id_estado><![CDATA[<%= beanHC.facturacion.getIdEstado()%>]]></id_estado>          
    <estado><![CDATA[<%= beanHC.facturacion.getEstado()%>]]></estado>          
    <id_factura><![CDATA[<%= beanHC.facturacion.getIdFactura()%>]]></id_factura>  
    <numero_factura><![CDATA[<%= beanHC.facturacion.getNumeroFactura()%>]]></numero_factura> 
    <prefijo_factura><![CDATA[<%= beanHC.facturacion.getPrefijoFactura()%>]]></prefijo_factura>   
    <forma_pago><![CDATA[<%= beanHC.facturacion.getFormaPago()%>]]></forma_pago>     
    <id_estado_cuenta><![CDATA[<%= beanHC.facturacion.getIdEstadoCuenta()%>]]></id_estado_cuenta>   
    <estado_cuenta><![CDATA[<%= beanHC.facturacion.getEstadoCuenta()%>]]></estado_cuenta>     
        <%
            for (int i = 0; i < beanHC.facturacion.getRecibosCopagoFactura().size(); i++) {
                if (!beanHC.facturacion.getRecibosCopagoFactura().get(i).equals("")) {
        %><recibos_copago><![CDATA[<%= beanHC.facturacion.getRecibosCopagoFactura().get(i)%>]]></recibos_copago><%
                }
            }

            for (int i = 0; i < beanHC.facturacion.getOtrosConceptosFactura().size(); i++) {
                if (!beanHC.facturacion.getOtrosConceptosFactura().get(i).equals("")) {
        %><otros_recibos><![CDATA[<%= beanHC.facturacion.getOtrosConceptosFactura().get(i)%>]]></otros_recibos><%
                }
            }

            if (!beanHC.facturacion.getReciboAjuste().equals("")) {
        %><recibo_ajuste><![CDATA[<%= beanHC.facturacion.getReciboAjuste()%>]]></recibo_ajuste><%
            }
        } else if (request.getParameter("accion").equals("verificaCantidadInventario")) {
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("txtCantidad")));
            resultado = beanHC.documentoHC.verificaCantidadInventario();
        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>  
    <cantidad><![CDATA[<%= beanHC.documentoHC.getVar2()%>]]></cantidad>          	
        <%
        } else if (request.getParameter("accion").equals("verificaCantidadInventarioMinimo")) {
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("txtCantidad")));
            resultado = beanHC.documentoHC.verificaCantidadInventarioMinimo();
        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>  
    <cantidad><![CDATA[<%= beanHC.documentoHC.getVar2()%>]]></cantidad>          	
        <%
        } else if (request.getParameter("accion").equals("verificaValorInventario")) {
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("txtCantidad")));
            resultado = beanHC.documentoHC.verificaValorInventario();
        %>          
    <cantidad><![CDATA[<%= beanHC.documentoHC.getVar2()%>]]></cantidad>          	
        <%
        } else if (request.getParameter("accion").equals("verificaValorInventarioMinimo")) {
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("txtCantidad")));
            resultado = beanHC.documentoHC.verificaValorInventarioMinimo();
        %>          
    <cantidad><![CDATA[<%= beanHC.documentoHC.getVar2()%>]]></cantidad>          	
        <%
        } else if (request.getParameter("accion").equals("buscarConciliacion")) {
            beanHC.documentoHC.setVar3(request.getParameter("lblIdDocumento"));
            beanHC.documentoHC.buscarConciliacion();
        %>
    <dato1><![CDATA[<%= beanHC.documentoHC.getVar4()%>]]></dato1>
    <dato2><![CDATA[<%= beanHC.documentoHC.getVar5()%>]]></dato2>
        <%
        } else if (request.getParameter("accion").equals("nuevoDocumentoInventarioBodegaConsumo")) {
            beanHC.documentoHC.setTipoDocumento(request.getParameter("cmbIdTipoDocumento"));
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("idBodega")));
            beanHC.documentoHC.setVar2(Integer.parseInt(request.getParameter("idAdmision")));
            beanHC.documentoHC.setVar3(request.getParameter("sw_origen"));
            /* nw var */
            resultado = beanHC.documentoHC.crearDocumentoInventarioBodegaConsumo();

        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("nuevoDocumentoInventarioProgramacion")) {
            beanHC.documentoHC.setTipoDocumento(request.getParameter("cmbIdTipoDocumento"));
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("idBodega")));
            beanHC.documentoHC.setVar2(Integer.parseInt(request.getParameter("idCita")));
            resultado = beanHC.documentoHC.crearDocumentoInventarioProgramacion();

        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("crearDocInvBodega")) {
            beanHC.documentoHC.setTipoDocumento(request.getParameter("cmbIdTipoDocumento"));
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("idBodega")));
            resultado = beanHC.documentoHC.crearDocInvBodega();

        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("existeDocumentoInventarioBodega")) {

            beanHC.documentoHC.setVar4(request.getParameter("txtNumero"));
            beanHC.documentoHC.setVar7(Integer.parseInt(request.getParameter("txtIdTercero")));

            resultado = beanHC.documentoHC.existeFacturaDocumento();

        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("nuevoDocumentoInventarioBodega")) {
            beanHC.documentoHC.setTipoDocumento(request.getParameter("cmbIdTipoDocumento"));
            beanHC.documentoHC.setVar1(Integer.parseInt(request.getParameter("idBodega")));
            beanHC.documentoHC.setVar3(request.getParameter("txtObservacion"));
            beanHC.documentoHC.setVar4(request.getParameter("txtNumero"));
            beanHC.documentoHC.setVar7(Integer.parseInt(request.getParameter("txtIdTercero")));
            beanHC.documentoHC.setVar5(request.getParameter("txtFechaDocumento"));
            beanHC.documentoHC.setVar2(Integer.parseInt(request.getParameter("txtValorFlete")));

            resultado = beanHC.documentoHC.crearDocumentoInventarioBodega();

        %>
    <dato><![CDATA[<%= beanHC.documentoHC.getIdDocumento()%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("ocuparCandadoReporte")) {
            resultado = beanHC.documentoHC.ocuparCandadoReporte();
        %>
    <dato><![CDATA[<%= resultado%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("liberarCandadoReporte")) {
            resultado = beanHC.documentoHC.liberarCandadoReporte();
        %>
    <dato><![CDATA[<%= resultado%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("agregarTurno")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdTurno"));
            beanHC.paciente.setVar3(request.getParameter("lblPacienteParlante"));
            beanHC.paciente.setVar4(request.getParameter("lblConsultorioParlante"));
            beanHC.paciente.setVar5(request.getParameter("lblEstadoParlante"));
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("cmbTurno")));
            resultado = beanHC.paciente.agregarTurno();
        %>
    <dato><![CDATA[<%= resultado%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("eliminarTurno")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdTurno"));
            beanHC.paciente.setVar3(request.getParameter("lblPacienteParlante"));
            beanHC.paciente.setVar4(request.getParameter("lblConsultorioParlante"));
            beanHC.paciente.setVar5(request.getParameter("lblEstadoParlante"));
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("cmbTurno")));
            resultado = beanHC.paciente.eliminarTurno();
        %>
    <dato><![CDATA[<%= resultado%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("llamarTurno")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdTurno"));
            beanHC.paciente.setVar3(request.getParameter("lblPacienteParlante"));
            beanHC.paciente.setVar4(request.getParameter("lblConsultorioParlante"));
            beanHC.paciente.setVar5(request.getParameter("lblEstadoParlante"));
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("cmbTurno")));
            resultado = beanHC.paciente.llamarTurno();
        %>
    <dato><![CDATA[<%= resultado%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("modificarTurno")) {
            beanHC.paciente.setVar2(request.getParameter("txtIdTurno"));
            beanHC.paciente.setVar3(request.getParameter("lblPacienteParlante"));
            beanHC.paciente.setVar4(request.getParameter("lblConsultorioParlante"));
            beanHC.paciente.setVar5(request.getParameter("lblEstadoParlante"));
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("cmbTurno")));
            resultado = beanHC.paciente.ModificarTurno();
        %>
    <dato><![CDATA[<%= resultado%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("estaTipificando")) {
            Tipificacion_v2 beanTipificacion3 = new Tipificacion_v2(request.getServletContext());
            String mensaje = "";
            if (beanTipificacion3.isTipificando()) {
                mensaje = "SI";
            } else {
                mensaje = "NO";
            }
        %>
    <dato><![CDATA[<%= mensaje%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("adicionarFaturasProcesoTipificacionActivo")) {
            Tipificacion_v2 beanTipificacion3 = new Tipificacion_v2(request.getServletContext());
            beanTipificacion3.adicionarFacturas(request.getParameter("lblIdFacturas"));
        %>
    <dato><![CDATA[<%= "YA EXITE UN PROCESO DE TIPIFICACION ACTIVO.\n\nLAS FACTURAS SE ADICIONARAN A LA COLA DEL PROCESO EXISTENTE."%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("iniciarProcesoTipificacion")) {
            Tipificacion_v2 beanTipificacion3 = new Tipificacion_v2(request.getServletContext());
            beanTipificacion3.setCn(cn);
            beanTipificacion3.inciar_tipificacion(request.getParameter("lblIdFacturas"));
        %>
    <dato><![CDATA[<%= "EL PROCESO DE TIPIFICACION HA TERMINADO."%>]]></dato>
        <% } else if (request.getParameter("accion").equals("iniciarProcesoTipificacionExterna")) {
            TipificacionExterna tipificacionExterna = new TipificacionExterna();
            tipificacionExterna.setCn(cn);
            Integer procesos = (Integer) application.getAttribute("procesosTipificacionExterna");
            if (procesos == null) {
                procesos = 1;
            } else {
                procesos += 1;
            }
            System.out.println("NUEVO PROCESOS *****" + procesos);
            application.setAttribute("procesosTipificacionExterna", procesos);
            tipificacionExterna.inciar_tipificacion(request.getParameter("lblIdFacturas"));
        %>
    <dato><![CDATA[<%= "EL PROCESO DE TIPIFICACION HA TERMINADO."%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("cancelarTipificacion")) {
            Tipificacion_v2 beanTipificacion3 = new Tipificacion_v2(request.getServletContext());
            beanTipificacion3.cancelarProcesoTipificacion();
            String mensaje = "PROCESO DE TIPIFICACION CANCELADO POR EL USUARIO.";
        %>
    <dato><![CDATA[<%= mensaje%>]]></dato>
        <%
        } else if (request.getParameter("accion").equals("copiarCitas")) {
            beanCitas.copiarCitas(request.getParameter("fechas"), request.getParameter("citas"));
            resultado = true;
        %>
    <dato><![CDATA[<%= resultado%>]]></dato>
        <%
                }
            }
        %>
    <respuesta><![CDATA[<%= resultado%>]]></respuesta>
    <accion><![CDATA[<%=request.getParameter("accion")%>]]></accion>		  
</raiz>	  	          

