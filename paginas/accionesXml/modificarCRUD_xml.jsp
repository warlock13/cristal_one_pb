<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<%@ page contentType="text/xml"%>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->

<raiz>	  	

    <%
        Fecha fecha = new Fecha();
        ControlAdmin controlAdmin = new ControlAdmin();
        ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
        Conexion cn = new Conexion(iConnection.getConnection());
        controlAdmin.setCn(cn);

        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
        java.util.Date date = cal.getTime();
        java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("###,##0.00");
        ArrayList resultaux = new ArrayList();

        boolean resultado = false;
        if (request.getParameter("accion") != null) {
            controlAdmin.grilla.setId(request.getParameter("idQuery"));
            controlAdmin.grilla.setParametros(request.getParameter("parametros"));
            resultado = controlAdmin.grilla.modificarCRUD();
        }
    %>
    <respuesta><![CDATA[<%= resultado%>]]></respuesta>
    <MsgAlerta><![CDATA[<%= cn.getMsgAlerta()%>]]></MsgAlerta>
    <accion><![CDATA[<%= request.getParameter("accion")%>]]></accion>
</raiz>	 
