<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<%@ page contentType="text/xml"%>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->

<raiz>
    <%
        if (request.getParameter("idQueryCombo") != null) {
            ControlAdmin beanAdmin = new ControlAdmin();
            ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
            Conexion cn = new Conexion(iConnection.getConnection());
            beanAdmin.setCn(cn);
            ArrayList resultaux = new ArrayList();
            resultaux.clear();

            if (request.getParameter("cantCondiciones").equals("0")) {
                resultaux = (ArrayList) beanAdmin.combo.cargar(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("1")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCodicion1(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("2")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                beanAdmin.combo.setCondicion2(request.getParameter("condicion2"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCodicion2(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("3")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                beanAdmin.combo.setCondicion2(request.getParameter("condicion2"));
                beanAdmin.combo.setCondicion3(request.getParameter("condicion3"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCodicionTres(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("4")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                beanAdmin.combo.setCondicion2(request.getParameter("condicion2"));
                beanAdmin.combo.setCondicion3(request.getParameter("condicion3"));
                beanAdmin.combo.setCondicion4(request.getParameter("condicion4"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCondicionCuatro(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("5")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                beanAdmin.combo.setCondicion2(request.getParameter("condicion2"));
                beanAdmin.combo.setCondicion3(request.getParameter("condicion3"));
                beanAdmin.combo.setCondicion4(request.getParameter("condicion4"));
                beanAdmin.combo.setCondicion5(request.getParameter("condicion5"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCondicionCinco(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("uno")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCodicionUno(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("dos")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                beanAdmin.combo.setCondicion2(request.getParameter("condicion2"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCodicionDos(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("tres")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                beanAdmin.combo.setCondicion2(request.getParameter("condicion2"));
                beanAdmin.combo.setCondicion3(request.getParameter("condicion3"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCodicionTres(Integer.parseInt(request.getParameter("idQueryCombo")));
            } else if (request.getParameter("cantCondiciones").equals("cuatro")) {
                beanAdmin.combo.setCondicion1(request.getParameter("condicion1"));
                beanAdmin.combo.setCondicion2(request.getParameter("condicion2"));
                beanAdmin.combo.setCondicion3(request.getParameter("condicion3"));
                beanAdmin.combo.setCondicion4(request.getParameter("condicion4"));
                resultaux = (ArrayList) beanAdmin.combo.cargarCondicionCuatro(Integer.parseInt(request.getParameter("idQueryCombo")));
            }

            ComboVO cmbGral;
            for (int k = 0; k < resultaux.size(); k++) {
                cmbGral = (ComboVO) resultaux.get(k);
    %> 
    <depto>
        <id><![CDATA[<%= cmbGral.getId()%>]]></id>
        <nom><![CDATA[<%= cmbGral.getDescripcion()%>]]></nom>
        <title><![CDATA[<%= cmbGral.getTitle()%>]]></title>            
    </depto>
    <%  }

        }%>
</raiz>            
