<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.sql.*,java.math.*,java.util.*" %>
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Sgh.Presentacion.*" %>
	<%@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
	
    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Sgh.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
	
<%
   beanAdmin.setCn(beanSession.getCn());// con el codigo del depto busca sus municipios
   ArrayList result=new ArrayList();
   beanAdmin.municipio.setDeptoactual(request.getParameter("codDepto"));
   result=(ArrayList)beanAdmin.cargarMunicipios();
   
%>


 <raiz>
    <% 
 		MunicipioVO municipioVO=new MunicipioVO();									
		int i=0;
		while(i<result.size()){
		   municipioVO=(MunicipioVO)result.get(i);	
		   i++;	
	 %> 
	 <municipio>
 	  <codMunicipio><![CDATA[<%= municipioVO.getCod_muni() %>]]></codMunicipio>
 	  <nomMunicipio><![CDATA[<%= municipioVO.getNombre_muni() %>]]></nomMunicipio>	  
    </municipio>
	<%
        }
	%>
 </raiz>            