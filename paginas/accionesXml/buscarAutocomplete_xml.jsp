<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Clinica.Presentacion.*" %>
	<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>

	<%@ page import = "java.text.NumberFormat" %>
   <%@ page import = "Sgh.Utilidades.*" %>

    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanHistoriaC" class="Clinica.GestionHistoriaC.ControlHistoriaC" scope="session" /> <!-- instanciar bean de session -->
<rows>
<%


   Fecha fecha = new Fecha();
   beanAdmin.setCn(beanSession.getCn());
   beanHistoriaC.setCn(beanSession.getCn());


   java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
   java.util.Date date = cal.getTime();
   java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
   SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");	
   NumberFormat nf = NumberFormat.getInstance(Locale.US);
   DecimalFormat df = (DecimalFormat)nf;
   df.applyPattern("###,##0.00");
   ArrayList resultaux=new ArrayList();

   if(request.getParameter("accion")!=null ){

    
	//	---------------------------------------------------------------------------------------- buscar ventanas jquery

	
   if(request.getParameter("accion").equals("txtBusIdPaciente")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanHistoriaC.cargarDatosPacienteAutocompletar(request.getParameter("textoAescribir"));	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		PacienteVO parametro=new PacienteVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(PacienteVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getIdentificacion() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombrecompleto() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}	
	 if(request.getParameter("accion").equals("txtBusIdPaciente2")){
  //		beanHC.noConformidad.setIdISORef("%");
		  resultaux=(ArrayList)beanHistoriaC.cargarDatosPacienteAutocompletar(request.getParameter("textoAescribir"));	
		  //result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		  PacienteVO parametro=new PacienteVO();									
		  int i=0;
		  while(i<resultaux.size()){
				  parametro=(PacienteVO)resultaux.get(i);
					 i++;					   
			  %><item> 
					 <seguimiento> 
						 <cod><![CDATA[<%= parametro.getIdentificacion() %>]]></cod>                   
						 <text><![CDATA[<%= parametro.getNombrecompleto() %>]]></text>   
					 </seguimiento>
				  </item>    
			  <%
		  }
	  }		

		
   if(request.getParameter("accion").equals("txtBusIdPaciente2")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanHistoriaC.cargarDatosPacienteAutocompletar(request.getParameter("textoAescribir"));	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		PacienteVO parametro=new PacienteVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(PacienteVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getIdentificacion() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombrecompleto() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}	
	
	
   if(request.getParameter("accion").equals("txtMunicipio")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanAdmin.municipio.cargarTodosMunicipios();	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		MunicipioVO parametro=new MunicipioVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(MunicipioVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getCodMunicipio() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombMunicipio() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}	
   if(request.getParameter("accion").equals("txtMunicipioListaEspera")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanAdmin.municipio.cargarTodosMunicipios();	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		MunicipioVO parametro=new MunicipioVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(MunicipioVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getCodMunicipio() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombMunicipio() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}		


   if(request.getParameter("accion").equals("txtAdministradora1")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanAdmin.administradora.CargarAdministradora();	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		AdministradoraVO parametro=new AdministradoraVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(AdministradoraVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getCodigo() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombre() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}	
   if(request.getParameter("accion").equals("txtAdministradora1LE")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanAdmin.administradora.CargarAdministradora();	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		AdministradoraVO parametro=new AdministradoraVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(AdministradoraVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getCodigo() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombre() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}	
   if(request.getParameter("accion").equals("txtAdministradora1ListaEspera")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanAdmin.administradora.CargarAdministradora();	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		AdministradoraVO parametro=new AdministradoraVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(AdministradoraVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getCodigo() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombre() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}		
   if(request.getParameter("accion").equals("txtAdministradora2")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanAdmin.administradora.CargarAdministradora();	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		AdministradoraVO parametro=new AdministradoraVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(AdministradoraVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getCodigo() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombre() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}	
   if(request.getParameter("accion").equals("txtAdministradora2ListaEspera")){
//		beanHC.noConformidad.setIdISORef("%");
		resultaux=(ArrayList)beanAdmin.administradora.CargarAdministradora();	
		//result=(ArrayList)beanHistoriaC.cargarDatosPaciente();  
		AdministradoraVO parametro=new AdministradoraVO();									
		int i=0;
		while(i<resultaux.size()){
				parametro=(AdministradoraVO)resultaux.get(i);
				   i++;					   
			%><item> 
				   <seguimiento> 
                       <cod><![CDATA[<%= parametro.getCodigo() %>]]></cod>                   
                       <text><![CDATA[<%= parametro.getNombre() %>]]></text>   
				   </seguimiento>
                </item>    
			<%
		}
	}	
	//Aqui uno nuevo
}
	
	//System.out.println("4");
%>

 	  
 	 
 </rows>            