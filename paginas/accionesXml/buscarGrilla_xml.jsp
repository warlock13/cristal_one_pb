<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<%@ page contentType="text/xml" pageEncoding="UTF-8" %>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->

<rows>
    <%
        Fecha fecha = new Fecha();
        ControlAdmin admin = new ControlAdmin();
        ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
        Conexion cn = new Conexion(iConnection.getConnection());
        admin.setCn(cn);

        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
        java.util.Date date = cal.getTime();
        java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("###,##0.00");
        ArrayList resultaux = new ArrayList();
        ArrayList resultDocs = new ArrayList();

        if (request.getParameter("idQuery") != null) {

            //	---------------------------------------------------------------------------------------- buscar ventanas jquery	
            admin.grilla.setId(request.getParameter("idQuery"));
            admin.grilla.setParametros(request.getParameter("parametros"));

            admin.setPage(request.getParameter("page"));  // paginas que se pide
            admin.setRows(request.getParameter("rows"));  //limite de filas por paginas			 
            admin.setSord(request.getParameter("sord"));  // sentido de la ordenacion		

            int totalcount = 0, totalpages = 0;
            totalcount = (int) admin.asignaContar(request.getParameter("idQuery"));
            totalpages = (int) admin.totalPages(totalcount);//podria estar almacenado en el bean.
            resultaux = (ArrayList) admin.grilla.buscarDatosDelReporte();

            GrillaVO parametro = new GrillaVO();
            int i = 0, j = 0, ban = 1;
    %>
    <page><%=admin.getPagina()%></page> 
    <total><%=totalpages%></total> 
    <records><%=totalcount%></records> 
        <%
            while (i < resultaux.size()) {
                parametro = (GrillaVO) resultaux.get(i);
                i++;
        %> 					
    <row id = '<%= i%>'> 
        <cell><![CDATA[<%= i%>]]></cell>       
            <%           if (admin.grilla.getNumColumnas() >= 1) {%>	         		   
        <cell><![CDATA[<%= parametro.getC1()%>]]></cell>                                       
            <%           }
                 if (admin.grilla.getNumColumnas() >= 2) {%> 			   
        <cell><![CDATA[<%= parametro.getC2()%>]]></cell>         
            <%           }
                 if (admin.grilla.getNumColumnas() >= 3) {%> 			   
        <cell><![CDATA[<%= parametro.getC3()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 4) {%> 			   
        <cell><![CDATA[<%= parametro.getC4()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 5) {%> 			   
        <cell><![CDATA[<%= parametro.getC5()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 6) {%> 			   
        <cell><![CDATA[<%= parametro.getC6()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 7) {%> 			   
        <cell><![CDATA[<%= parametro.getC7()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 8) {%> 			   
        <cell><![CDATA[<%= parametro.getC8()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 9) {%> 			   
        <cell><![CDATA[<%= parametro.getC9()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 10) {%> 			   
        <cell><![CDATA[<%= parametro.getC10()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 11) {%> 			   
        <cell><![CDATA[<%= parametro.getC11()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 12) {%> 			   
        <cell><![CDATA[<%= parametro.getC12()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 13) {%> 			   
        <cell><![CDATA[<%= parametro.getC13()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 14) {%> 			   
        <cell><![CDATA[<%= parametro.getC14()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 15) {%> 			   
        <cell><![CDATA[<%= parametro.getC15()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 16) {%> 			   
        <cell><![CDATA[<%= parametro.getC16()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 17) {%> 			   
        <cell><![CDATA[<%= parametro.getC17()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 18) {%> 			   
        <cell><![CDATA[<%= parametro.getC18()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 19) {%> 			   
        <cell><![CDATA[<%= parametro.getC19()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 20) {%> 			   
        <cell><![CDATA[<%= parametro.getC20()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 21) {%> 			   
        <cell><![CDATA[<%= parametro.getC21()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 22) {%> 			   
        <cell><![CDATA[<%= parametro.getC22()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 23) {%> 			   
        <cell><![CDATA[<%= parametro.getC23()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 24) {%> 			   
        <cell><![CDATA[<%= parametro.getC24()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 25) {%> 			   
        <cell><![CDATA[<%= parametro.getC25()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 26) {%> 			   
        <cell><![CDATA[<%= parametro.getC26()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 27) {%> 			   
        <cell><![CDATA[<%= parametro.getC27()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 28) {%> 			   
        <cell><![CDATA[<%= parametro.getC28()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 29) {%> 			   
        <cell><![CDATA[<%= parametro.getC29()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 30) {%> 			   
        <cell><![CDATA[<%= parametro.getC30()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 31) {%> 			   
        <cell><![CDATA[<%= parametro.getC31()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 32) {%> 			   
        <cell><![CDATA[<%= parametro.getC32()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 33) {%> 			   
        <cell><![CDATA[<%= parametro.getC33()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 34) {%> 			   
        <cell><![CDATA[<%= parametro.getC34()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 35) {%> 			   
        <cell><![CDATA[<%= parametro.getC35()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 36) {%> 			   
        <cell><![CDATA[<%= parametro.getC36()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 37) {%> 			   
        <cell><![CDATA[<%= parametro.getC37()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 38) {%> 			   
        <cell><![CDATA[<%= parametro.getC38()%>]]></cell>    
            <%           }
                 if (admin.grilla.getNumColumnas() >= 39) {%> 			   
        <cell><![CDATA[<%= parametro.getC39()%>]]></cell>    
            <%           }
                if (admin.grilla.getNumColumnas() >= 40) {%> 			   
        <cell><![CDATA[<%= parametro.getC40()%>]]></cell>  
            <%           }
                if (admin.grilla.getNumColumnas() >= 41) {%>          
        <cell><![CDATA[<%= parametro.getC41()%>]]></cell>     
            <%           }
                if (admin.grilla.getNumColumnas() >= 42) {%>          
        <cell><![CDATA[<%= parametro.getC42()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 43) {%>          
        <cell><![CDATA[<%= parametro.getC43()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 44) {%>          
        <cell><![CDATA[<%= parametro.getC44()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 45) {%>          
        <cell><![CDATA[<%= parametro.getC45()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 46) {%>          
        <cell><![CDATA[<%= parametro.getC46()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 47) {%>          
        <cell><![CDATA[<%= parametro.getC47()%>]]></cell> 
            <%           }
                if (admin.grilla.getNumColumnas() >= 48) {%>          
        <cell><![CDATA[<%= parametro.getC48()%>]]></cell> 
            <%           }
                if (admin.grilla.getNumColumnas() >= 49) {%>          
        <cell><![CDATA[<%= parametro.getC49()%>]]></cell>    
            <%           }
                if (admin.grilla.getNumColumnas() >= 50) {%>         
        <cell><![CDATA[<%= parametro.getC50()%>]]></cell>                                           
            <%           }
                if (admin.grilla.getNumColumnas() >= 51) {%>        
        <cell><![CDATA[<%= parametro.getC51()%>]]></cell>      
            <%           }
                if (admin.grilla.getNumColumnas() >= 52) {%>          
        <cell><![CDATA[<%= parametro.getC52()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 53) {%>          
        <cell><![CDATA[<%= parametro.getC53()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 54) {%>          
        <cell><![CDATA[<%= parametro.getC54()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 55) {%>          
        <cell><![CDATA[<%= parametro.getC55()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 56) {%>          
        <cell><![CDATA[<%= parametro.getC56()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 57) {%>          
        <cell><![CDATA[<%= parametro.getC57()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 58) {%>          
        <cell><![CDATA[<%= parametro.getC58()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 59) {%>          
        <cell><![CDATA[<%= parametro.getC59()%>]]></cell>
            <%           }
                if (admin.grilla.getNumColumnas() >= 60) {%>          
        <cell><![CDATA[<%= parametro.getC60()%>]]></cell>
            <%}%>                                                                  
    </row>                         
    <%
            }
        }
    %>
</rows>    

