<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<%@ page contentType="text/xml"%>
<%@ page errorPage=""%>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>	
<%@ page import = "Sgh.Utilidades.Sesion" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "Clinica.Presentacion.HistoriaClinica.*" %>    
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.GestionHistoriaC.ControlHistoriaC"%>
<%@ page import = "java.text.NumberFormat" %>
<%@ page import = "Sgh.Utilidades.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->

<rows>
    <%
        Fecha fecha = new Fecha();

        ControlHistoriaC beanHC = new ControlHistoriaC();
        ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
        Conexion cn = new Conexion(iConnection.getConnection());
        beanHC.setCn(cn);

        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
        java.util.Date date = cal.getTime();
        java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance(java.text.DateFormat.MEDIUM);
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("###,##0.00");
        ArrayList resultaux = new ArrayList();
        ArrayList resultDocs = new ArrayList();
        boolean resultado = false;
        if (request.getParameter("accion") != null) {

            //	---------------------------------------------------------------------------------------- buscar ventanas jquery	
            if (request.getParameter("accion").equals("consultaOdontograma")) {
                beanHC.odontograma.setIdPaciente(request.getParameter("idPaciente"));
                beanHC.odontograma.setNumTrataHistorico(Integer.parseInt(request.getParameter("num_trata_historico")));
                beanHC.odontograma.setIdTipoTratamiento(request.getParameter("id_tipo_tratamiento"));
                resultaux = (ArrayList) beanHC.odontograma.cargarOdontograma();

                OdontogramaVO parametro = new OdontogramaVO();
                int i = 0;
                while (i < resultaux.size()) {
                    parametro = (OdontogramaVO) resultaux.get(i);
                    i++;
    %>
    <diente> 
        <idDiente><![CDATA[<%=parametro.getIdDiente()%>]]></idDiente>                            
        <v><![CDATA[<%=parametro.getV()%>]]></v> 
        <m><![CDATA[<%=parametro.getM()%>]]></m> 
        <l><![CDATA[<%=parametro.getL()%>]]></l> 
        <d><![CDATA[<%=parametro.getD()%>]]></d> 
        <o><![CDATA[<%=parametro.getO()%>]]></o>   
        <bis><![CDATA[<%=parametro.getBIS()%>]]></bis>   
        <bii><![CDATA[<%=parametro.getBII()%>]]></bii> 
        <idTratam><![CDATA[<%=parametro.getIdTratamDiente()%>]]></idTratam>                                                                   
    </diente>
    <%
        }
    } else if (request.getParameter("accion").equals("consultaOdontogramaPlaca")) {
        beanHC.odontograma.setIdPaciente(request.getParameter("idPaciente"));
        beanHC.odontograma.setNumTrataHistorico(Integer.parseInt(request.getParameter("num_trata_historico")));
        beanHC.odontograma.setIdTipoTratamiento(request.getParameter("id_tipo_tratamiento"));
        beanHC.odontograma.setIdEvolucion(Integer.parseInt(request.getParameter("id_evolucion")));
        resultaux = (ArrayList) beanHC.odontograma.cargarOdontogramaPlaca();

        OdontogramaVO parametro = new OdontogramaVO();
        int i = 0;
        while (i < resultaux.size()) {
            parametro = (OdontogramaVO) resultaux.get(i);
            i++;
    %>
    <diente> 
        <idDiente><![CDATA[<%=parametro.getIdDiente()%>]]></idDiente>                            
        <v><![CDATA[<%=parametro.getV()%>]]></v> 
        <m><![CDATA[<%=parametro.getM()%>]]></m> 
        <l><![CDATA[<%=parametro.getL()%>]]></l> 
        <d><![CDATA[<%=parametro.getD()%>]]></d> 
        <o><![CDATA[<%=parametro.getO()%>]]></o>  
        <bis><![CDATA[<%=parametro.getBIS()%>]]></bis>   
        <bii><![CDATA[<%=parametro.getBII()%>]]></bii> 
        <idTratam><![CDATA[<%=parametro.getIdTratamDiente()%>]]></idTratam>                                                                   
    </diente>
    <%
        }
    } else if (request.getParameter("accion").equals("datosOdontologiaPlaca")) {
        beanHC.odontograma.setIdEvolucion(Integer.parseInt(request.getParameter("id_evolucion")));
        resultado = beanHC.odontograma.consultaPlaca();
    %>  
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>        
    <cantDientesPlaca><![CDATA[<%= beanHC.odontograma.getCantDientesPlaca()%>]]></cantDientesPlaca>
    <superficiesPorDiente><![CDATA[<%= beanHC.odontograma.getSuperficiesPorDiente()%>]]></superficiesPorDiente>
    <observacionPlaca><![CDATA[<%= beanHC.odontograma.getObservacionPlaca()%>]]></observacionPlaca> 
        <%
        } else if (request.getParameter("accion").equals("datosOdontologiacops")) {
            beanHC.odontograma.setIdEvolucion(Integer.parseInt(request.getParameter("id_evolucion")));
            resultado = beanHC.odontograma.consultaCOPS();
        %>  
    <diligenciado><![CDATA[<%= resultado%>]]></diligenciado>        
    <cariados><![CDATA[<%= beanHC.odontograma.getcariados()%>]]></cariados>
    <obturados><![CDATA[<%= beanHC.odontograma.getobturados()%>]]></obturados>
    <perdidos><![CDATA[<%= beanHC.odontograma.getperdidos()%>]]></perdidos>
    <sanos><![CDATA[<%= beanHC.odontograma.getsanos()%>]]></sanos>
    <presentes><![CDATA[<%= beanHC.odontograma.getpresentes()%>]]></presentes>
    <cavitacional><![CDATA[<%= beanHC.odontograma.getcavitacional()%>]]></cavitacional>
    <perdidosXCaries><![CDATA[<%= beanHC.odontograma.getperdidosXCaries()%>]]></perdidosXCaries>
    <observacionCops><![CDATA[<%= beanHC.odontograma.getobservacionCops()%>]]></observacionCops>
        <%
        } else if (request.getParameter("accion").equals("actualizarEstadoFacturacionCita")) {
            beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("id_cita").replace(" ", "")));
            beanHC.paciente.setVar2(request.getParameter("usuario").replace(" ", ""));
            beanHC.paciente.setVar3(request.getParameter("estado").replace(" ", ""));
            beanHC.paciente.actualizarEstadoFacturacionCita();
        } else if (request.getParameter("accion").equals("InformacionBasicaDelPaciente")) {
            beanHC.paciente.setId(Integer.parseInt(request.getParameter("id").replace(" ", "")));
            resultaux = (ArrayList) beanHC.paciente.buscarInformacionPaciente();

            PacienteVO parametro = new PacienteVO();
            int i = 0;
            while (i < resultaux.size()) {
                parametro = (PacienteVO) resultaux.get(i);
                i++;

        %>
    <infoBasicaPaciente> 
        <IdPaciente><![CDATA[<%=parametro.getId()%>]]></IdPaciente>

        <TipoIdPaciente><![CDATA[<%=parametro.getTipoId()%>]]></TipoIdPaciente>
        <identificacion><![CDATA[<%=parametro.getIdentificacion()%>]]></identificacion> 
        <Nombre1><![CDATA[<%=parametro.getNombre1()%>]]></Nombre1>
        <Nombre2><![CDATA[<%=parametro.getNombre2()%>]]></Nombre2>
        <Apellido1><![CDATA[<%=parametro.getApellido1()%>]]></Apellido1>
        <Apellido2><![CDATA[<%=parametro.getApellido2()%>]]></Apellido2>
        <idMunicipio><![CDATA[<%=parametro.getIdMunicipio()%>]]></idMunicipio>
        <nomMunicipio><![CDATA[<%=parametro.getNomMunicipio()%>]]></nomMunicipio>                 
        <Direccion><![CDATA[<%=parametro.getDireccion()%>]]></Direccion>
        <Telefonos><![CDATA[<%=parametro.getTelefono()%>]]></Telefonos>
        <Celular1><![CDATA[<%=parametro.getCelular1()%>]]></Celular1>                 
        <Celular2><![CDATA[<%=parametro.getCelular2()%>]]></Celular2>  
        <email><![CDATA[<%=parametro.getEmail()%>]]></email>                   
        <Etnia><![CDATA[<%=parametro.getIdEtnia()%>]]></Etnia>
        <NivelEscolar><![CDATA[<%=parametro.getIdNivelEscolar()%>]]></NivelEscolar>
        <idOcupacion><![CDATA[<%=parametro.getIdOcupacion()%>]]></idOcupacion>
        <Ocupacion><![CDATA[<%=parametro.getNomOcupacion()%>]]></Ocupacion>                 
        <Estrato><![CDATA[<%=parametro.getIdEstrato()%>]]></Estrato>                 

        <fechaNac><![CDATA[<%=parametro.getFechaNacimiento()%>]]></fechaNac>
        <edad><![CDATA[<%=parametro.getEdad()%>]]></edad>
        <sexo><![CDATA[<%=parametro.getSexo()%>]]></sexo>                 
        <acompanante><![CDATA[<%=parametro.getAcompanante()%>]]></acompanante>    
        <administradora><![CDATA[<%=parametro.getVar1()%>]]></administradora>
        <id_regimen><![CDATA[<%=parametro.getId_tipo_regimen()%>]]></id_regimen>    
        <regimen><![CDATA[<%=parametro.getTipo_regimen()%>]]></regimen>                            
        <email><![CDATA[<%=parametro.getEmail()%>]]></email>   
        <gestante><![CDATA[<%=parametro.getVar3()%>]]></gestante>          
        <nacionalidad><![CDATA[<%=parametro.getVar2()%>]]></nacionalidad>   
        <rh><![CDATA[<%=parametro.getVar4()%>]]></rh>   
                                             
    </infoBasicaPaciente>
    <%
        }
    } else if (request.getParameter("accion").equals("InformacionArticuloTemporal")) {
        beanHC.paciente.setId(Integer.parseInt(request.getParameter("id")));
        resultaux = (ArrayList) beanHC.paciente.buscarInformacionArticuloTemporal(Integer.parseInt(request.getParameter("id")));

        PacienteVO parametro = new PacienteVO();
        int i = 0;
        while (i < resultaux.size()) {
            parametro = (PacienteVO) resultaux.get(i);
            i++;

    %>
    <infoVAR>              
        <VAR1><![CDATA[<%=parametro.getVar1()%>]]></VAR1>                              
        <VAR2><![CDATA[<%=parametro.getVar2()%>]]></VAR2>
        <VAR3><![CDATA[<%=parametro.getVar3()%>]]></VAR3>   
        <VAR4><![CDATA[<%=parametro.getVar4()%>]]></VAR4>                              
        <VAR5><![CDATA[<%=parametro.getVar5()%>]]></VAR5>
        <VAR6><![CDATA[<%=parametro.getVar6()%>]]></VAR6>                                                             
    </infoVAR>
    <%
        }
    } else if (request.getParameter("accion").equals("InformacionBasicaDelPacienteIdEvolucion")) {
        beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("idEvolucion")));
        resultaux = (ArrayList) beanHC.paciente.buscarInformacionPacienteDesdeIdEvolucion();
        PacienteVO parametro = new PacienteVO();
        int i = 0;
        while (i < resultaux.size()) {
            parametro = (PacienteVO) resultaux.get(i);
            i++;

    %>
    <infoBasicaPaciente> 

        <FECHA_ELABORO><![CDATA[<%=parametro.getVar1()%>]]></FECHA_ELABORO>                              
        <FECHA_INGRESO><![CDATA[<%=parametro.getVar2()%>]]></FECHA_INGRESO>                              
        <ID_FOLIO><![CDATA[<%=parametro.getVar3()%>]]></ID_FOLIO>                              
        <ID_HC><![CDATA[<%=parametro.getVar4()%>]]></ID_HC>                              
        <NOM_ADMINISTRADORA><![CDATA[<%=parametro.getVar5()%>]]></NOM_ADMINISTRADORA>                              
        <IDENTIFICACION_PACIENTE><![CDATA[<%=parametro.getVar6()%>]]></IDENTIFICACION_PACIENTE>                                                                                                                   
        <ID_PROFESIONAL><![CDATA[<%=parametro.getVar7()%>]]></ID_PROFESIONAL>                                                                                                                                    

        <Nombre1><![CDATA[<%=parametro.getNombre1()%>]]></Nombre1>
        <Nombre2><![CDATA[<%=parametro.getNombre2()%>]]></Nombre2>
        <Apellido1><![CDATA[<%=parametro.getApellido1()%>]]></Apellido1>
        <Apellido2><![CDATA[<%=parametro.getApellido2()%>]]></Apellido2>
        <fechaNac><![CDATA[<%=parametro.getFechaNacimiento()%>]]></fechaNac>
        <edad><![CDATA[<%=parametro.getEdad()%>]]></edad>
        <sexo><![CDATA[<%=parametro.getSexo()%>]]></sexo>                 

        <Ocupacion><![CDATA[<%=parametro.getIdOcupacion()%>]]></Ocupacion> 
        <NivelEscolar><![CDATA[<%=parametro.getIdNivelEscolar()%>]]></NivelEscolar>                                 
        <Etnia><![CDATA[<%=parametro.getIdEtnia()%>]]></Etnia>
        <nomMunicipio><![CDATA[<%=parametro.getNomMunicipio()%>]]></nomMunicipio>
        <Direccion><![CDATA[<%=parametro.getDireccion()%>]]></Direccion>   
        <acompanante><![CDATA[<%=parametro.getAcompanante()%>]]></acompanante>                                                       

        <Telefonos><![CDATA[<%=parametro.getTelefono()%>]]></Telefonos>

    </infoBasicaPaciente>
    <%
        }
    } else if (request.getParameter("accion").equals("noPos")) {
        beanHC.paciente.setVar1(Integer.parseInt(request.getParameter("txtIdArticulo")));
        resultaux = (ArrayList) beanHC.paciente.buscarInformacionParaMedicamentoNoPos();
        PacienteVO parametro = new PacienteVO();
        int i = 0;
        while (i < resultaux.size()) {
            parametro = (PacienteVO) resultaux.get(i);
            i++;
    %>
    <infoVAR>              
        <VAR1><![CDATA[<%=parametro.getVar1()%>]]></VAR1>                              
        <VAR2><![CDATA[<%=parametro.getVar2()%>]]></VAR2>
        <VAR3><![CDATA[<%=parametro.getVar3()%>]]></VAR3>   
        <VAR4><![CDATA[<%=parametro.getVar4()%>]]></VAR4>                              
        <VAR5><![CDATA[<%=parametro.getVar5()%>]]></VAR5>
        <VAR6><![CDATA[<%=parametro.getVar6()%>]]></VAR6>                                                             
    </infoVAR>
    <%
        }
    } else if (request.getParameter("accion").equals("noPosProcedimientos")) {
        System.out.println("buscarInforHHHHHHHHHHH" + request.getParameter("txtIdArticulo"));
        beanHC.paciente.setVar2(request.getParameter("txtIdArticulo"));
        resultaux = (ArrayList) beanHC.paciente.buscarInformacionParaProcedimientoNoPos();
        PacienteVO parametro = new PacienteVO();
        int i = 0;
        while (i < resultaux.size()) {
            parametro = (PacienteVO) resultaux.get(i);
            i++;
    %>
    <infoVAR>              
        <VAR1><![CDATA[<%=parametro.getVar1()%>]]></VAR1>                              
        <VAR2><![CDATA[<%=parametro.getVar2()%>]]></VAR2>
        <VAR3><![CDATA[<%=parametro.getVar3()%>]]></VAR3>   
        <VAR4><![CDATA[<%=parametro.getVar4()%>]]></VAR4>                              
        <VAR5><![CDATA[<%=parametro.getVar5()%>]]></VAR5>
        <VAR6><![CDATA[<%=parametro.getVar6()%>]]></VAR6>                                                             
    </infoVAR>
    <%
        }
    } else if (request.getParameter("accion").equals("contenidoDocumentoHC")) {
        beanHC.documentoHC.setIdDocumento(request.getParameter("idDocumento"));
        beanHC.documentoHC.setTipoDocumento(request.getParameter("TipoDocumento"));
        resultaux = (ArrayList) beanHC.documentoHC.traerContenidoDocumentoSeleccionado();
        DocumentoHCVO parametro = new DocumentoHCVO();
        int i = 0, j = 0, ban = 1;

        while (i < resultaux.size()) {
            parametro = (DocumentoHCVO) resultaux.get(i);
            i++;
    %> 					
    <datos> 

        <%           if (beanHC.documentoHC.getNumColumnas() >= 1) {%>	         		   
        <cell><![CDATA[<%= parametro.getC1()%>]]></cell>                                       
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 2) {%> 			   
        <cell><![CDATA[<%= parametro.getC2()%>]]></cell>         
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 3) {%> 			   
        <cell><![CDATA[<%= parametro.getC3()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 4) {%> 			   
        <cell><![CDATA[<%= parametro.getC4()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 5) {%> 			   
        <cell><![CDATA[<%= parametro.getC5()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 6) {%> 			   
        <cell><![CDATA[<%= parametro.getC6()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 7) {%> 			   
        <cell><![CDATA[<%= parametro.getC7()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 8) {%> 			   
        <cell><![CDATA[<%= parametro.getC8()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 9) {%> 			   
        <cell><![CDATA[<%= parametro.getC9()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 10) {%> 			   
        <cell><![CDATA[<%= parametro.getC10()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 11) {%> 			   
        <cell><![CDATA[<%= parametro.getC11()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 12) {%> 			   
        <cell><![CDATA[<%= parametro.getC12()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 13) {%> 			   
        <cell><![CDATA[<%= parametro.getC13()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 14) {%> 			   
        <cell><![CDATA[<%= parametro.getC14()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 15) {%> 			   
        <cell><![CDATA[<%= parametro.getC15()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 16) {%> 			   
        <cell><![CDATA[<%= parametro.getC16()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 17) {%> 			   
        <cell><![CDATA[<%= parametro.getC17()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 18) {%> 			   
        <cell><![CDATA[<%= parametro.getC18()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 19) {%> 			   
        <cell><![CDATA[<%= parametro.getC19()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 20) {%> 			   
        <cell><![CDATA[<%= parametro.getC20()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 21) {%> 			   
        <cell><![CDATA[<%= parametro.getC21()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 22) {%> 			   
        <cell><![CDATA[<%= parametro.getC22()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 23) {%> 			   
        <cell><![CDATA[<%= parametro.getC23()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 24) {%> 			   
        <cell><![CDATA[<%= parametro.getC24()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 25) {%> 			   
        <cell><![CDATA[<%= parametro.getC25()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 26) {%> 			   
        <cell><![CDATA[<%= parametro.getC26()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 27) {%> 			   
        <cell><![CDATA[<%= parametro.getC27()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 28) {%> 			   
        <cell><![CDATA[<%= parametro.getC28()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 29) {%> 			   
        <cell><![CDATA[<%= parametro.getC29()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 30) {%> 			   
        <cell><![CDATA[<%= parametro.getC30()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 31) {%> 			   
        <cell><![CDATA[<%= parametro.getC31()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 32) {%> 			   
        <cell><![CDATA[<%= parametro.getC32()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 33) {%> 			   
        <cell><![CDATA[<%= parametro.getC33()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 34) {%> 			   
        <cell><![CDATA[<%= parametro.getC34()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 35) {%> 			   
        <cell><![CDATA[<%= parametro.getC35()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 36) {%> 			   
        <cell><![CDATA[<%= parametro.getC36()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 37) {%> 			   
        <cell><![CDATA[<%= parametro.getC37()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 38) {%> 			   
        <cell><![CDATA[<%= parametro.getC38()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 39) {%> 			   
        <cell><![CDATA[<%= parametro.getC39()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 40) {%> 			   
        <cell><![CDATA[<%= parametro.getC40()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 41) {%> 			   
        <cell><![CDATA[<%= parametro.getC41()%>]]></cell>                                      
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 42) {%> 			   
        <cell><![CDATA[<%= parametro.getC42()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 43) {%> 			   
        <cell><![CDATA[<%= parametro.getC43()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 44) {%> 			   
        <cell><![CDATA[<%= parametro.getC44()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 45) {%> 			   
        <cell><![CDATA[<%= parametro.getC45()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 46) {%> 			   
        <cell><![CDATA[<%= parametro.getC46()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 47) {%> 			   
        <cell><![CDATA[<%= parametro.getC47()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 48) {%> 			   
        <cell><![CDATA[<%= parametro.getC48()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 49) {%> 			   
        <cell><![CDATA[<%= parametro.getC49()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 50) {%> 			   
        <cell><![CDATA[<%= parametro.getC50()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 51) {%> 			   
        <cell><![CDATA[<%= parametro.getC51()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 52) {%> 			   
        <cell><![CDATA[<%= parametro.getC52()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 53) {%> 			   
        <cell><![CDATA[<%= parametro.getC53()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 54) {%> 			   
        <cell><![CDATA[<%= parametro.getC54()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 55) {%> 			   
        <cell><![CDATA[<%= parametro.getC55()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 56) {%> 			   
        <cell><![CDATA[<%= parametro.getC56()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 57) {%> 			   
        <cell><![CDATA[<%= parametro.getC57()%>]]></cell>                                      
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 58) {%> 			   
        <cell><![CDATA[<%= parametro.getC58()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 59) {%> 			   
        <cell><![CDATA[<%= parametro.getC59()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 60) {%> 			   
        <cell><![CDATA[<%= parametro.getC60()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 61) {%> 			   
        <cell><![CDATA[<%= parametro.getC61()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 62) {%> 			   
        <cell><![CDATA[<%= parametro.getC62()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 63) {%> 			   
        <cell><![CDATA[<%= parametro.getC63()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 64) {%> 			   
        <cell><![CDATA[<%= parametro.getC64()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 65) {%> 			   
        <cell><![CDATA[<%= parametro.getC65()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 66) {%> 			   
        <cell><![CDATA[<%= parametro.getC66()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 67) {%> 			   
        <cell><![CDATA[<%= parametro.getC67()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 68) {%> 			   
        <cell><![CDATA[<%= parametro.getC68()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 69) {%> 			   
        <cell><![CDATA[<%= parametro.getC69()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 70) {%> 			   
        <cell><![CDATA[<%= parametro.getC70()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 71) {%> 			   
        <cell><![CDATA[<%= parametro.getC71()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 72) {%> 			   
        <cell><![CDATA[<%= parametro.getC72()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 73) {%> 			   
        <cell><![CDATA[<%= parametro.getC73()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 74) {%> 			   
        <cell><![CDATA[<%= parametro.getC74()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 75) {%> 			   
        <cell><![CDATA[<%= parametro.getC75()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 76) {%> 			   
        <cell><![CDATA[<%= parametro.getC76()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 77) {%> 			   
        <cell><![CDATA[<%= parametro.getC77()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 78) {%> 			   
        <cell><![CDATA[<%= parametro.getC78()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 79) {%> 			   
        <cell><![CDATA[<%= parametro.getC79()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 80) {%> 			   
        <cell><![CDATA[<%= parametro.getC80()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 81) {%> 			   
        <cell><![CDATA[<%= parametro.getC81()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 82) {%> 			   
        <cell><![CDATA[<%= parametro.getC82()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 83) {%> 			   
        <cell><![CDATA[<%= parametro.getC83()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 84) {%> 			   
        <cell><![CDATA[<%= parametro.getC84()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 85) {%> 			   
        <cell><![CDATA[<%= parametro.getC85()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 86) {%> 			   
        <cell><![CDATA[<%= parametro.getC86()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 87) {%> 			   
        <cell><![CDATA[<%= parametro.getC87()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 88) {%> 			   
        <cell><![CDATA[<%= parametro.getC88()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 89) {%> 			   
        <cell><![CDATA[<%= parametro.getC89()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 90) {%> 			   
        <cell><![CDATA[<%= parametro.getC90()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 91) {%> 			   
        <cell><![CDATA[<%= parametro.getC91()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 92) {%> 			   
        <cell><![CDATA[<%= parametro.getC92()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 93) {%> 			   
        <cell><![CDATA[<%= parametro.getC93()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 94) {%> 			   
        <cell><![CDATA[<%= parametro.getC94()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 95) {%> 			   
        <cell><![CDATA[<%= parametro.getC95()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 96) {%> 			   
        <cell><![CDATA[<%= parametro.getC96()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 97) {%> 			   
        <cell><![CDATA[<%= parametro.getC97()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 98) {%> 			   
        <cell><![CDATA[<%= parametro.getC98()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 99) {%> 			   
        <cell><![CDATA[<%= parametro.getC99()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 100) {%> 			   
        <cell><![CDATA[<%= parametro.getC100()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 101) {%> 			   
        <cell><![CDATA[<%= parametro.getC101()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 102) {%> 			   
        <cell><![CDATA[<%= parametro.getC102()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 103) {%> 			   
        <cell><![CDATA[<%= parametro.getC103()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 104) {%> 			   
        <cell><![CDATA[<%= parametro.getC104()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 105) {%> 			   
        <cell><![CDATA[<%= parametro.getC105()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 106) {%> 			   
        <cell><![CDATA[<%= parametro.getC106()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 107) {%> 			   
        <cell><![CDATA[<%= parametro.getC107()%>]]></cell>    
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 108) {%> 			   
        <cell><![CDATA[<%= parametro.getC108()%>]]></cell>  
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 109) {%> 			   
        <cell><![CDATA[<%= parametro.getC109()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 110) {%> 			   
        <cell><![CDATA[<%= parametro.getC110()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 111) {%> 			   
        <cell><![CDATA[<%= parametro.getC111()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 112) {%> 			   
        <cell><![CDATA[<%= parametro.getC112()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 113) {%> 			   
        <cell><![CDATA[<%= parametro.getC113()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 114) {%> 			   
        <cell><![CDATA[<%= parametro.getC114()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 115) {%> 			   
        <cell><![CDATA[<%= parametro.getC115()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 116) {%> 			   
        <cell><![CDATA[<%= parametro.getC116()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 117) {%> 			   
        <cell><![CDATA[<%= parametro.getC117()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 118) {%> 			   
        <cell><![CDATA[<%= parametro.getC118()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 119) {%> 			   
        <cell><![CDATA[<%= parametro.getC119()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 120) {%> 			   
        <cell><![CDATA[<%= parametro.getC120()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 121) {%> 			   
        <cell><![CDATA[<%= parametro.getC121()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 122) {%> 			   
        <cell><![CDATA[<%= parametro.getC122()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 123) {%> 			   
        <cell><![CDATA[<%= parametro.getC123()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 124) {%> 			   
        <cell><![CDATA[<%= parametro.getC124()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 125) {%> 			   
        <cell><![CDATA[<%= parametro.getC125()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 126) {%> 			   
        <cell><![CDATA[<%= parametro.getC126()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 127) {%> 			   
        <cell><![CDATA[<%= parametro.getC127()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 128) {%> 			   
        <cell><![CDATA[<%= parametro.getC128()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 129) {%> 			   
        <cell><![CDATA[<%= parametro.getC129()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 130) {%> 			   
        <cell><![CDATA[<%= parametro.getC130()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 131) {%> 			   
        <cell><![CDATA[<%= parametro.getC131()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 132) {%> 			   
        <cell><![CDATA[<%= parametro.getC132()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 133) {%> 			   
        <cell><![CDATA[<%= parametro.getC133()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 134) {%> 			   
        <cell><![CDATA[<%= parametro.getC134()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 135) {%> 			   
        <cell><![CDATA[<%= parametro.getC135()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 136) {%> 			   
        <cell><![CDATA[<%= parametro.getC136()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 137) {%> 			   
        <cell><![CDATA[<%= parametro.getC137()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 138) {%> 			   
        <cell><![CDATA[<%= parametro.getC138()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 139) {%> 			   
        <cell><![CDATA[<%= parametro.getC139()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 140) {%> 			   
        <cell><![CDATA[<%= parametro.getC140()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 141) {%> 			   
        <cell><![CDATA[<%= parametro.getC141()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 142) {%> 			   
        <cell><![CDATA[<%= parametro.getC142()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 143) {%> 			   
        <cell><![CDATA[<%= parametro.getC143()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 144) {%> 			   
        <cell><![CDATA[<%= parametro.getC144()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 145) {%> 			   
        <cell><![CDATA[<%= parametro.getC145()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 146) {%> 			   
        <cell><![CDATA[<%= parametro.getC146()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 147) {%> 			   
        <cell><![CDATA[<%= parametro.getC147()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 148) {%> 			   
        <cell><![CDATA[<%= parametro.getC148()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 149) {%> 			   
        <cell><![CDATA[<%= parametro.getC149()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 150) {%> 			   
        <cell><![CDATA[<%= parametro.getC150()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 151) {%> 			   
        <cell><![CDATA[<%= parametro.getC151()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 152) {%> 			   
        <cell><![CDATA[<%= parametro.getC152()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 153) {%> 			   
        <cell><![CDATA[<%= parametro.getC153()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 154) {%> 			   
        <cell><![CDATA[<%= parametro.getC154()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 155) {%> 			   
        <cell><![CDATA[<%= parametro.getC155()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 156) {%> 			   
        <cell><![CDATA[<%= parametro.getC156()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 157) {%> 			   
        <cell><![CDATA[<%= parametro.getC157()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 158) {%> 			   
        <cell><![CDATA[<%= parametro.getC158()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 159) {%> 			   
        <cell><![CDATA[<%= parametro.getC159()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 160) {%> 			   
        <cell><![CDATA[<%= parametro.getC160()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 161) {%> 			   
        <cell><![CDATA[<%= parametro.getC161()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 162) {%> 			   
        <cell><![CDATA[<%= parametro.getC162()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 163) {%> 			   
        <cell><![CDATA[<%= parametro.getC163()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 164) {%> 			   
        <cell><![CDATA[<%= parametro.getC164()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 165) {%> 			   
        <cell><![CDATA[<%= parametro.getC165()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 166) {%> 			   
        <cell><![CDATA[<%= parametro.getC166()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 167) {%> 			   
        <cell><![CDATA[<%= parametro.getC167()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 168) {%> 			   
        <cell><![CDATA[<%= parametro.getC168()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 169) {%> 			   
        <cell><![CDATA[<%= parametro.getC169()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 170) {%> 			   
        <cell><![CDATA[<%= parametro.getC170()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 171) {%> 			   
        <cell><![CDATA[<%= parametro.getC171()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 172) {%> 			   
        <cell><![CDATA[<%= parametro.getC172()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 173) {%> 			   
        <cell><![CDATA[<%= parametro.getC173()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 174) {%> 			   
        <cell><![CDATA[<%= parametro.getC174()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 175) {%> 			   
        <cell><![CDATA[<%= parametro.getC175()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 176) {%> 			   
        <cell><![CDATA[<%= parametro.getC176()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 177) {%> 			   
        <cell><![CDATA[<%= parametro.getC177()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 178) {%> 			   
        <cell><![CDATA[<%= parametro.getC178()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 179) {%> 			   
        <cell><![CDATA[<%= parametro.getC179()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 180) {%> 			   
        <cell><![CDATA[<%= parametro.getC180()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 181) {%> 			   
        <cell><![CDATA[<%= parametro.getC181()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 182) {%> 			   
        <cell><![CDATA[<%= parametro.getC182()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 183) {%> 			   
        <cell><![CDATA[<%= parametro.getC183()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 184) {%> 			   
        <cell><![CDATA[<%= parametro.getC184()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 185) {%> 			   
        <cell><![CDATA[<%= parametro.getC185()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 186) {%> 			   
        <cell><![CDATA[<%= parametro.getC186()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 187) {%> 			   
        <cell><![CDATA[<%= parametro.getC187()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 188) {%> 			   
        <cell><![CDATA[<%= parametro.getC188()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 189) {%> 			   
        <cell><![CDATA[<%= parametro.getC189()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 190) {%> 			   
        <cell><![CDATA[<%= parametro.getC190()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 191) {%> 			   
        <cell><![CDATA[<%= parametro.getC191()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 192) {%> 			   
        <cell><![CDATA[<%= parametro.getC192()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 193) {%> 			   
        <cell><![CDATA[<%= parametro.getC193()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 194) {%> 			   
        <cell><![CDATA[<%= parametro.getC194()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 195) {%> 			   
        <cell><![CDATA[<%= parametro.getC195()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 196) {%> 			   
        <cell><![CDATA[<%= parametro.getC196()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 197) {%> 			   
        <cell><![CDATA[<%= parametro.getC197()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 198) {%> 			   
        <cell><![CDATA[<%= parametro.getC198()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 199) {%> 			   
        <cell><![CDATA[<%= parametro.getC199()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 200) {%> 			   
        <cell><![CDATA[<%= parametro.getC200()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 201) {%> 			   
        <cell><![CDATA[<%= parametro.getC201()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 202) {%> 			   
        <cell><![CDATA[<%= parametro.getC202()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 203) {%> 			   
        <cell><![CDATA[<%= parametro.getC203()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 204) {%> 			   
        <cell><![CDATA[<%= parametro.getC204()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 205) {%> 			   
        <cell><![CDATA[<%= parametro.getC205()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 206) {%> 			   
        <cell><![CDATA[<%= parametro.getC206()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 207) {%> 			   
        <cell><![CDATA[<%= parametro.getC207()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 208) {%> 			   
        <cell><![CDATA[<%= parametro.getC208()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 209) {%> 			   
        <cell><![CDATA[<%= parametro.getC209()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 210) {%> 			   
        <cell><![CDATA[<%= parametro.getC210()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 211) {%> 			   
        <cell><![CDATA[<%= parametro.getC211()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 212) {%> 			   
        <cell><![CDATA[<%= parametro.getC212()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 213) {%> 			   
        <cell><![CDATA[<%= parametro.getC213()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 214) {%> 			   
        <cell><![CDATA[<%= parametro.getC214()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 215) {%> 			   
        <cell><![CDATA[<%= parametro.getC215()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 216) {%> 			   
        <cell><![CDATA[<%= parametro.getC216()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 217) {%> 			   
        <cell><![CDATA[<%= parametro.getC217()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 218) {%> 			   
        <cell><![CDATA[<%= parametro.getC218()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 219) {%> 			   
        <cell><![CDATA[<%= parametro.getC219()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 220) {%> 			   
        <cell><![CDATA[<%= parametro.getC220()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 221) {%> 			   
        <cell><![CDATA[<%= parametro.getC221()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 222) {%> 			   
        <cell><![CDATA[<%= parametro.getC222()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 223) {%> 			   
        <cell><![CDATA[<%= parametro.getC223()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 224) {%> 			   
        <cell><![CDATA[<%= parametro.getC224()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 225) {%> 			   
        <cell><![CDATA[<%= parametro.getC225()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 226) {%> 			   
        <cell><![CDATA[<%= parametro.getC226()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 227) {%> 			   
        <cell><![CDATA[<%= parametro.getC227()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 228) {%> 			   
        <cell><![CDATA[<%= parametro.getC228()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 229) {%> 			   
        <cell><![CDATA[<%= parametro.getC229()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 230) {%> 			   
        <cell><![CDATA[<%= parametro.getC230()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 231) {%> 			   
        <cell><![CDATA[<%= parametro.getC231()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 232) {%> 			   
        <cell><![CDATA[<%= parametro.getC232()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 233) {%> 			   
        <cell><![CDATA[<%= parametro.getC233()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 234) {%> 			   
        <cell><![CDATA[<%= parametro.getC234()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 235) {%> 			   
        <cell><![CDATA[<%= parametro.getC235()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 236) {%> 			   
        <cell><![CDATA[<%= parametro.getC236()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 237) {%> 			   
        <cell><![CDATA[<%= parametro.getC237()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 238) {%> 			   
        <cell><![CDATA[<%= parametro.getC238()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 239) {%> 			   
        <cell><![CDATA[<%= parametro.getC239()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 240) {%> 			   
        <cell><![CDATA[<%= parametro.getC240()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 241) {%> 			   
        <cell><![CDATA[<%= parametro.getC241()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 242) {%> 			   
        <cell><![CDATA[<%= parametro.getC242()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 243) {%> 			   
        <cell><![CDATA[<%= parametro.getC243()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 244) {%> 			   
        <cell><![CDATA[<%= parametro.getC244()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 245) {%> 			   
        <cell><![CDATA[<%= parametro.getC245()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 246) {%> 			   
        <cell><![CDATA[<%= parametro.getC246()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 247) {%> 			   
        <cell><![CDATA[<%= parametro.getC247()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 248) {%> 			   
        <cell><![CDATA[<%= parametro.getC248()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 249) {%> 			   
        <cell><![CDATA[<%= parametro.getC249()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 250) {%> 			   
        <cell><![CDATA[<%= parametro.getC250()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 251) {%> 			   
        <cell><![CDATA[<%= parametro.getC251()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 252) {%> 			   
        <cell><![CDATA[<%= parametro.getC252()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 253) {%> 			   
        <cell><![CDATA[<%= parametro.getC253()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 254) {%> 			   
        <cell><![CDATA[<%= parametro.getC254()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 255) {%> 			   
        <cell><![CDATA[<%= parametro.getC255()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 256) {%> 			   
        <cell><![CDATA[<%= parametro.getC256()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 257) {%> 			   
        <cell><![CDATA[<%= parametro.getC257()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 258) {%> 			   
        <cell><![CDATA[<%= parametro.getC258()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 259) {%> 			   
        <cell><![CDATA[<%= parametro.getC259()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 260) {%> 			   
        <cell><![CDATA[<%= parametro.getC260()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 261) {%> 			   
        <cell><![CDATA[<%= parametro.getC261()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 262) {%> 			   
        <cell><![CDATA[<%= parametro.getC262()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 263) {%> 			   
        <cell><![CDATA[<%= parametro.getC263()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 264) {%> 			   
        <cell><![CDATA[<%= parametro.getC264()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 265) {%> 			   
        <cell><![CDATA[<%= parametro.getC265()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 266) {%> 			   
        <cell><![CDATA[<%= parametro.getC266()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 267) {%> 			   
        <cell><![CDATA[<%= parametro.getC267()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 268) {%> 			   
        <cell><![CDATA[<%= parametro.getC268()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 269) {%> 			   
        <cell><![CDATA[<%= parametro.getC269()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 270) {%> 			   
        <cell><![CDATA[<%= parametro.getC270()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 271) {%> 			   
        <cell><![CDATA[<%= parametro.getC271()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 272) {%> 			   
        <cell><![CDATA[<%= parametro.getC272()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 273) {%> 			   
        <cell><![CDATA[<%= parametro.getC273()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 274) {%> 			   
        <cell><![CDATA[<%= parametro.getC274()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 275) {%> 			   
        <cell><![CDATA[<%= parametro.getC275()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 276) {%> 			   
        <cell><![CDATA[<%= parametro.getC276()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 277) {%> 			   
        <cell><![CDATA[<%= parametro.getC277()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 278) {%> 			   
        <cell><![CDATA[<%= parametro.getC278()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 279) {%> 			   
        <cell><![CDATA[<%= parametro.getC279()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 280) {%> 			   
        <cell><![CDATA[<%= parametro.getC280()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 281) {%> 			   
        <cell><![CDATA[<%= parametro.getC281()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 282) {%> 			   
        <cell><![CDATA[<%= parametro.getC282()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 283) {%> 			   
        <cell><![CDATA[<%= parametro.getC283()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 284) {%> 			   
        <cell><![CDATA[<%= parametro.getC284()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 285) {%> 			   
        <cell><![CDATA[<%= parametro.getC285()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 286) {%> 			   
        <cell><![CDATA[<%= parametro.getC286()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 287) {%> 			   
        <cell><![CDATA[<%= parametro.getC287()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 288) {%> 			   
        <cell><![CDATA[<%= parametro.getC288()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 289) {%> 			   
        <cell><![CDATA[<%= parametro.getC289()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 290) {%> 			   
        <cell><![CDATA[<%= parametro.getC290()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 291) {%> 			   
        <cell><![CDATA[<%= parametro.getC291()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 292) {%> 			   
        <cell><![CDATA[<%= parametro.getC292()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 293) {%> 			   
        <cell><![CDATA[<%= parametro.getC293()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 294) {%> 			   
        <cell><![CDATA[<%= parametro.getC294()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 295) {%> 			   
        <cell><![CDATA[<%= parametro.getC295()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 296) {%> 			   
        <cell><![CDATA[<%= parametro.getC296()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 297) {%> 			   
        <cell><![CDATA[<%= parametro.getC297()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 298) {%> 			   
        <cell><![CDATA[<%= parametro.getC298()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 299) {%> 			   
        <cell><![CDATA[<%= parametro.getC299()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 300) {%> 			   
        <cell><![CDATA[<%= parametro.getC300()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 301) {%> 			   
        <cell><![CDATA[<%= parametro.getC301()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 302) {%> 			   
        <cell><![CDATA[<%= parametro.getC302()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 303) {%> 			   
        <cell><![CDATA[<%= parametro.getC303()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 304) {%> 			   
        <cell><![CDATA[<%= parametro.getC304()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 305) {%> 			   
        <cell><![CDATA[<%= parametro.getC305()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 306) {%> 			   
        <cell><![CDATA[<%= parametro.getC306()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 307) {%> 			   
        <cell><![CDATA[<%= parametro.getC307()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 308) {%> 			   
        <cell><![CDATA[<%= parametro.getC308()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 309) {%> 			   
        <cell><![CDATA[<%= parametro.getC309()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 310) {%> 			   
        <cell><![CDATA[<%= parametro.getC310()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 311) {%> 			   
        <cell><![CDATA[<%= parametro.getC311()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 312) {%> 			   
        <cell><![CDATA[<%= parametro.getC312()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 313) {%> 			   
        <cell><![CDATA[<%= parametro.getC313()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 314) {%> 			   
        <cell><![CDATA[<%= parametro.getC314()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 315) {%> 			   
        <cell><![CDATA[<%= parametro.getC315()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 316) {%> 			   
        <cell><![CDATA[<%= parametro.getC316()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 317) {%> 			   
        <cell><![CDATA[<%= parametro.getC317()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 318) {%> 			   
        <cell><![CDATA[<%= parametro.getC318()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 319) {%> 			   
        <cell><![CDATA[<%= parametro.getC319()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 320) {%> 			   
        <cell><![CDATA[<%= parametro.getC320()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 321) {%> 			   
        <cell><![CDATA[<%= parametro.getC321()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 322) {%> 			   
        <cell><![CDATA[<%= parametro.getC322()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 323) {%> 			   
        <cell><![CDATA[<%= parametro.getC323()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 324) {%> 			   
        <cell><![CDATA[<%= parametro.getC324()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 325) {%> 			   
        <cell><![CDATA[<%= parametro.getC325()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 326) {%> 			   
        <cell><![CDATA[<%= parametro.getC326()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 327) {%> 			   
        <cell><![CDATA[<%= parametro.getC327()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 328) {%> 			   
        <cell><![CDATA[<%= parametro.getC328()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 329) {%> 			   
        <cell><![CDATA[<%= parametro.getC329()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 330) {%> 			   
        <cell><![CDATA[<%= parametro.getC330()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 331) {%> 			   
        <cell><![CDATA[<%= parametro.getC331()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 332) {%> 			   
        <cell><![CDATA[<%= parametro.getC332()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 333) {%> 			   
        <cell><![CDATA[<%= parametro.getC333()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 334) {%> 			   
        <cell><![CDATA[<%= parametro.getC334()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 335) {%> 			   
        <cell><![CDATA[<%= parametro.getC335()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 336) {%> 			   
        <cell><![CDATA[<%= parametro.getC336()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 337) {%> 			   
        <cell><![CDATA[<%= parametro.getC337()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 338) {%> 			   
        <cell><![CDATA[<%= parametro.getC338()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 339) {%> 			   
        <cell><![CDATA[<%= parametro.getC339()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 340) {%> 			   
        <cell><![CDATA[<%= parametro.getC340()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 341) {%> 			   
        <cell><![CDATA[<%= parametro.getC341()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 342) {%> 			   
        <cell><![CDATA[<%= parametro.getC342()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 343) {%> 			   
        <cell><![CDATA[<%= parametro.getC343()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 344) {%> 			   
        <cell><![CDATA[<%= parametro.getC344()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 345) {%> 			   
        <cell><![CDATA[<%= parametro.getC345()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 346) {%> 			   
        <cell><![CDATA[<%= parametro.getC346()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 347) {%> 			   
        <cell><![CDATA[<%= parametro.getC347()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 348) {%> 			   
        <cell><![CDATA[<%= parametro.getC348()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 349) {%> 			   
        <cell><![CDATA[<%= parametro.getC349()%>]]></cell>
            <%           }
                if (beanHC.documentoHC.getNumColumnas() >= 350) {%> 			   
        <cell><![CDATA[<%= parametro.getC350()%>]]></cell> 

        <%}%> 


    </datos>                         
    <%
                }/*while*/


            }// else if
        }

        //System.out.println("4");
    %>
</rows>

