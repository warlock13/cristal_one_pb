<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
	<%@ page contentType="text/xml"%>
	<%@ page errorPage=""%>
	<%@ page import = "java.sql.*,java.math.*,java.util.*" %>
	<%@ page import = "Sgh.Utilidades.Sesion" %>
	<%@ page import = "Sgh.Presentacion.*" %>
	<%@ page import = "Sgh.AdminSeguridad.ControlAdmin" %>
	
    <jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
    <jsp:useBean id="beanAdmin" class="Sgh.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
<ajaxresponse>	
	<%
       beanAdmin.setCn(beanSession.getCn());
       ArrayList result=new ArrayList();
	   if(request.getParameter("accion")!=null ){	    
		   if(request.getParameter("accion").equals("txtObsTratamEjecut")){     
			   beanAdmin.autocomplete.setcodAutoc(request.getParameter("cod"));
			   beanAdmin.autocomplete.setnomAutoc(request.getParameter("des"));   
			   beanAdmin.autocomplete.setDescriptivo(request.getParameter("descrip"));   		   
			   beanAdmin.autocomplete.setTipo(request.getParameter("cod_trat_aplica"));   
			   result=(ArrayList)beanAdmin.cargarAutocomplete();           
			%>
			<item> 
				<% 
					AutocompleteVO AutocompleteVO=new AutocompleteVO();									
					int i=0;
					while(i<result.size()){
					   AutocompleteVO=(AutocompleteVO)result.get(i);	
					   i++;	
					  // System.out.println("-----"+AutocompleteVO.getCodAutoc());
				 %> 
				  <cod><![CDATA[<%= AutocompleteVO.getCodAutoc()%>]]></cod>
				  <text><![CDATA[<%= AutocompleteVO.getDesAutoc()%>]]></text>	  
				 <%
					}
				 %>
			</item>  
			   <%
				}
				//Diagnosticos
			  else if(request.getParameter("accion").equals("txtCodigoDiagnosticos")){     
			   beanAdmin.diagnosticos.setTipo(request.getParameter("tipo"));   
			   result=(ArrayList)beanAdmin.diagnosticos.cargarAutocompleteDiagnosticos();           
				%>
				<item> 
					<% 
						AutocompleteVO AutocompleteVO=new AutocompleteVO();									
						int i=0;
						while(i<result.size()){
						   AutocompleteVO=(AutocompleteVO)result.get(i);	
						   i++;	
						   //System.out.println("-----"+AutocompleteVO.getCodAutoc());
					 %> 
					  <cod><![CDATA[<%= AutocompleteVO.getCodAutoc()%>]]></cod>
					  <text><![CDATA[<%= AutocompleteVO.getDesAutoc()%>]]></text>	  
					 <%
						}
					 %>
				</item>  
				   <%
			}
		//fin diagnosticos
     }
    %>      
 </ajaxresponse>            