<%@ page contentType="application/json; charset=UTF-8"%>
<%@ page errorPage=""%>	 	  
<%@ page import = "Sgh.Utilidades.Sesion" %>
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session"/> 

<%
    String mensaje = "";
    int codigo_respuesta;
    boolean ingreso = false;
    int numero_sesion=beanSession.getId_sesion();
    if (numero_sesion == -1){
        ingreso = beanSession.iniciar_sesion(request.getParameter("login"), request.getParameter("password"), Integer.parseInt(request.getParameter("idSesion")));

        if(ingreso){
            codigo_respuesta = 1;
            mensaje = "Bienvenido a Cristal Web";
            //request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            codigo_respuesta = 0;
            mensaje = "Usuario y/o contraseña incorrectos por favor intenta nuevamente.";
        }
    } else {
        codigo_respuesta = -1;
        mensaje = "Ya tienes una sesión activa";
    }
%>

{
    "codigo_respuesta": <%= codigo_respuesta %>,
    "estado": <%= ingreso %>,
    "mensaje": "<%= mensaje %>"
}