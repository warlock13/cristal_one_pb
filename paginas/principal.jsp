<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage=""%>
<%@ page import = "Sgh.Utilidades.Sesion"%>

<% request.getSession().setMaxInactiveInterval(-1); %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session"/>

<%
    System.out.println(beanSession.usuario.preguntarMenu("HIS08"));
%>

<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CRISTAL WEB</title>
        <link REL="SHORTCUT ICON" HREF="../utilidades/css/favicon.png"/>

        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/base.min.css" /> -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css" />
        <link href="../utilidades/css/estiloClinica.css" rel="stylesheet" type="text/css">
        <link href="../utilidades/css/extras.css" rel="stylesheet" type="text/css">
        <link href="reportes/estiloPcweb_imp.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../utilidades/css/menu.css" />
        <link rel="stylesheet" type="text/css" href="../utilidades/css/jquery.treeview.css" />
        <link href="../utilidades/css/custom-theme/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="../utilidades/css/jquery.autocomplete.css" />
        <link href="../paginas/planAtencion/leaflet/leaflet.css" rel="stylesheet" type="text/css">
        <link href="../paginas/hc/saludOral/diente/css/odontograma.css" rel="stylesheet" type="text/css"/>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <link href="../utilidades/timepicker-1.3.5/jquery.timepicker.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../utilidades/javascript/jquery.ui.timepicker.css?<%=beanSession.getVersion()%>"/>
        <link rel="stylesheet" type="text/css" href="../utilidades/jqGrid/jquery-ui2.css">
        <link rel="stylesheet" type="text/css" href="../utilidades/jqGrid-3.8.2/css/ui.jqgrid.css">
       

        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/brands.js" integrity="sha384-sCI3dTBIJuqT6AwL++zH7qL8ZdKaHpxU43dDt9SyOzimtQ9eyRhkG3B7KMl6AO19" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/1c70883c8e.js" crossorigin="anonymous"></script>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js"></script>
        <script type="text/javascript" src="../utilidades/timepicker-1.3.5/jquery.timepicker.js"></script>
        <script type="text/javascript" src="../utilidades/javascript/alert/jquery.alerts.js"></script>
        <script language=javascript src="../utilidades/jqGrid-4.4.3/jquery.jqGrid.min.js"></script>
        <script language=javascript src="../utilidades/jqGrid-4.4.3/grid.locale-es.js"></script>
        <script src="../utilidades/javascript/sweetalert2-11.4.8/dist/sweetalert2.all.min.js"></script>
        <script language=javascript src="../utilidades/javascript/fisheye.js"></script>
        <script language=javascript src="../utilidades/javascript/jquery.corner.js"></script>
        <script language=javascript src="../utilidades/javascript/jquery.treeview.js"></script>
        <script language=javascript src="../utilidades/javascript/dropShadow.js"></script>
        <script language=javascript src="../utilidades/javascript/jquery.blockUI.js"></script>
        <script language=javascript src="../utilidades/javascript/jquery.ui.all.js"></script>	
        <script language=javascript src="../utilidades/javascript/tableexport.js"></script>
        <script type="text/javascript" src="../utilidades/javascript/jqgridExcelExportClientSide.js" ></script>
        <script type="text/javascript" src="../utilidades/javascript/jqgridExcelExportClientSide-libs.js" ></script>
        <script language=javascript src="../paginas/adminModelo/funcionesModelo.js"></script>		
        <script language=javascript src="../paginas/adminModelo/funcionesEncuestaModelo.js"></script>	
        <script language=javascript src="../paginas/inventarios/transacciones/comprobanteEntrada.js"></script> 
        <script language=javascript src="../paginas/inventarios/transacciones/comprobanteEntradaTablas.js"></script> 
        <script language=javascript src="../paginas/inventarios/bodega/funciones_bodegas.js"></script>
        <script language=javascript src="../paginas/inventarios/inventarios.js"></script>
        <script src="../paginas/inventarios/transacciones/ordenesCompra.js"></script>
        <script src="../paginas/inventarios/transacciones/tablasOrdenCompra.js"></script>
        <script language=javascript src="../paginas/inventarios/articulo/funciones_articulos.js"></script>
        <script language=javascript src="../paginas/facturacion/terceros/funcionesTerceros.js"></script> 
        <script language=javascript src="../paginas/facturacion/terceros/tablasTerceros.js"></script> 
        <script language=javascript src="../paginas/autorizaciones/solicitudes.js"></script>	
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.3.2/select2.js" crossorigin="anonymous"></script>
        <script language=javascript
        src="../utilidades/javascript/funcionesBotones.js?<%beanSession.getVersion();%>"></script>
        <script language=javascript
        src="../utilidades/javascript/funciones.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/funcionesMenu.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript src="../paginas/planAtencion/rutas.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesAdicionales.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesBotonesJuan.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesAgenda.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesFacturacion.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesEvento.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesBotonesHc.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesBotonesReportes.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesBotonesSuministros.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/limpiarCampos.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesRecepcionTecnica.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesSinergiaCC.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/validarcampos.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/subirArchivos.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesBotonesPaciente.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript src="../utilidades/javascript/js/coda.js"></script>
        <script language=javascript src="../utilidades/javascript/jquery.autocomplete.pack.js"></script>
        <script language=javascript
        src="../utilidades/javascript/cronometro.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="hc/documentosHistoriaClinica/documentosHistoriaClinica.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../paginas/referencia/funcionesReferencia.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../paginas/hc/saludOral/odontologia.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript src="../paginas/parametros/parametros.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../paginas/planAtencion/leaflet/leaflet-src.esm.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../paginas/planAtencion/leaflet/leaflet-src.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../paginas/hc/manejoAdministrativo/encuestas/plantillaEncuesta.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript src="../paginas/encuesta/validacionesVih.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript src="../paginas/encuesta/encuesta.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../paginas/encuesta/encuestaValidaciones.js?<%=beanSession.getVersion()%>"></script>
        <script language=javascript
        src="../utilidades/javascript/OTRAS/funcionesBotonesPaciente.js?<%=beanSession.getVersion()%>"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <script language=javascript
        src="../utilidades/javascript/jquery.ui.timepicker.js?<%=beanSession.getVersion()%>"></script>
        <script src="../utilidades/javascript/login.js"></script>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.3.2/select2.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.3.2/select2.js" crossorigin="anonymous"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.15.0/lodash.min.js"></script>

        <script language=javascript
        src="../utilidades/javascript/OTRAS/gestionFacturacion.js?<%=beanSession.getVersion()%>"></script>

        <script language=javascript
        src="../utilidades/javascript/OTRAS/gestionPlantillasHtml.js?<%=beanSession.getVersion()%>"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqgrid/4.6.0/plugins/jquery.contextmenu.js"></script>

        <script language=javascript src="/clinica/utilidades/javascript/jszip.min.js?<%=beanSession.cn.getVersion()%>"> </script>

        
        

        
     <!--   <script  src ="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.3.2/html2canvas.js" ></script>  

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script> -->
        <script language=javascript
            src="../utilidades/javascript/OTRAS/gestionFacturacion.js?<%=beanSession.getVersion()%>"></script>
       

        <script>
            $(window).load(function () {
                $(".loader_main").fadeOut("slow");
                $("#main_table").show()
            });
            $(document).ready(function () {
                iniciarDiccionarioConNotificaciones(1);

                $("#msgBienvenido").dropShadow({left: 2, top: 2, blur: 1, opacity: 1, color: "WHITE", swap: false});

                $('#divMenuTreeTitle').corner("tear 10px").parent().css('padding', '4px').corner("fray 5px");
                $("#browser").treeview({
                    toggle: function () {
                        console.log("%s was toggled.", $(this).find(">span").text());
                    }
                });

                //funciones para ocultar y mostrar el menu					
                lauchTimer();
                mueveReloj();
                sedes();

                var puedeDesaparecer = false
                $('#disparaMenu').mousemove(function (event) {
                    setTimeout("", 50);
                    if (puedeDesaparecer == false) {
                        $("#menuDesplegable").animate({width: "180px"}, {queue: false, duration: 30});
                        $('#divMenuTreeTitle').show();
                    }
                    puedeDesaparecer = false;
                });
                $('#main').mousemove(function (event) {
                    if (puedeDesaparecer == true) {
                        $("#menuDesplegable").animate({width: "0px"}, {queue: false, duration: 30});
                        $('#divMenuTreeTitle').hide();
                    }
                    puedeDesaparecer = false;
                });
                $('#menuDesplegable').mousemove(function (event) {
                    //$("#menuDesplegable").animate( { width:"180px" }, { queue:false, duration:30 } );
                    puedeDesaparecer = false
                });
                //fin funciones mostrar y ocultar menu
                if (document.getElementById('lblContrasena').innerHTML == '123') {
                    setTimeout("cargarMenu('<%= response.encodeURL("/clinica/paginas/adminSeguridad/password.jsp")%>', '5-0-0', 'password', 'password', 's', 's', 's', 's', 's', 's')", 2000);
                } else {
                }
            });
        </script>
    </head>

    <body>

        <div class="loader_main"></div>
        <table id="main_table" width="90%" align="left" border="0" cellspacing="0" cellpadding="0" style="margin-top:1px; z-index:1500" hidden>
            <tr>
                <td valign="top"> <input name="ventanasActivas" id="ventanasActivas" type="hidden" class="nomayusculas"
                                          value="" size="25" />
                    <label id="lblContrasena" style="display: none;"><%=beanSession.usuario.getContrasena()%></label>
                    <label id="lblIdentificacion" style="display: none;"><%=beanSession.usuario.getIdentificacion()%></label>
                    <label id="lblIdSede0" style="display: none;"><%=beanSession.usuario.getIdSede()%></label>
                    <label id="lblIdNombreProfesionalA" style="display: none;"><%= beanSession.usuario.getNombre1() + " " + beanSession.usuario.getApellido1()%></label>
                    <label id="lblNombreProfesionA" style="display: none;"><%= beanSession.usuario.getProfesion()%></label>
                    <label id="lblIdProfesionA" style="display: none;"><%= beanSession.usuario.getIdProfesion()%></label>
                    <jsp:include page="menu.jsp" flush="true" />
                    <table id="tablaMain" width="100%" height="100%" align="left" cellpadding="0" cellspacing="0"
                           bgcolor="#CCCCCC">
                        <tr>
                            <td align="center" valign="top">                        
                                <div id="menuAux"
                                     style="width:132px; position:absolute; left:9px; z-index:1;  top: 40px; height: 12px; display:none;">
                                    <div style="background:#FFFFFF">
                                        <div
                                            style="background:#FC3;  height:100%; font-family:Arial, Helvetica, sans-serif; font-size:10px; "
                                            align="center">
                                            <label id="lblMenuTreeTitle" onclick="javascript:mostrarMenu();" style="color:#193b56;">MEN&Uacute;</label>
                                        </div>
                                    </div>
                                    <br />                            
                                </div>
                                <input id="hamburger"  class="hamburger" type="checkbox" />
                                <label id="labelHamburger" for="hamburger" onclick="desplazarHC()" class="hamburger">
                                    <i style="left: 0px;top: 10px;"></i>
                                    <text>
                                    <close></close>
                                    <open>menu</open>
                                    </text>
                                </label>

                                <nav class="primnav" id="menuLateral">
                                    <div class="infoMedico ocultarInfo">
                                        <% if (beanSession.usuario.getProfesion().equals("MEDICO INTERNISTA")) {%>
                                        <i class="fas fa-user-md iconoInfo"></i>
                                        <%}%>
                                        <% if (beanSession.usuario.getProfesion().equals("ENFERMERA JEFE") || beanSession.usuario.getProfesion().equals("AUXILIAR ENFERMERIA")) {%>
                                        <i class="fas fa-user-nurse iconoInfo"></i>
                                        <%}%><%else {%>
                                        <i class="fas fa-user-md iconoInfo"></i>
                                        <%}%>
                                        <h2 ><label id="lblIdProfesion"><%=beanSession.usuario.getIdProfesion()%> </label> &nbsp;<strong><%=beanSession.usuario.getProfesion()%></strong></h2>
                                        <h3 >&nbsp;<strong><%= beanSession.usuario.getNombre1() + " " + beanSession.usuario.getApellido1()%></strong></h3>
                                    </div>
                                    <ul>
                                        <% if (beanSession.usuario.preguntarMenu("HIS")) {%>
                                        <li class="closed" id="menuTitle" >
                                            <span class="folderHistoriaClinica" title="HIS" id="padClick" onclick="ocultarMenuSlider('#contentMenu')">
                                                <i class="fas fa-clipboard-list"></i>
                                                HISTORIAS CLINICAS</span>
                                            <ul class="secnav" id="contentMenu" >
                                                <% if (beanSession.usuario.preguntarMenu("HIS01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li2" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalHC', 'principalHC', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HIS01">Folios Historia Clinica</a></span>
                                                </li>
                                              <!--   <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li3" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/tamizajes.jsp")%>', '4-0-0', 'tamizajes', 'tamizajes', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HIS01">Tamizajes</a></span>
                                                </li>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li4" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/notasNefro.jsp")%>', '4-0-0', 'notasNefro', 'notasNefro', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HIS04">Notas Seguimiento Nefroproteccion</a></span>
                                                </li> -->
                                                <!--
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                    class="activeP" id="li5" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/notasVih.jsp")%>', '4-0-0', 'notasVih', 'notasVih', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                    style="cursor:pointer" title="HIS03">Notas Seguimiento VIH</a></span>
                                                </li>
                                                -->
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li6" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hcAdjuntos.jsp")%>', '4-0-0', 'archivosAdjuntos', 'archivosAdjuntos', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HIS01">Archivos Adjuntos</a></span>
                                                </li>

                                                <li>
                                                    <span class="file">
                                                        <i class="far fa-circle"></i>
                                                        <a class="activeP" id="li137" 
                                                            onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/formatosImpresionHC.jsp")%>', '4-0-0', 'formatoImpresion', 'formatoImpresion', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HIS01">Formatos de impresion
                                                        </a>
                                                    </span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("HIS0222")) {%>

                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li7" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/suministros/panelMedicamentos.jsp")%>', '4-0-0', 'panelMedicamentos', 'panelMedicamentos', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HIS0222">Panel Medicamentos</a></span>
                                                </li>

                                                <%}%>

                                                <% if (beanSession.usuario.preguntarMenu("HIS0223")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li8" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/modificarHistoriaClinica.jsp")%>', '4-0-0', 'modificarHistoria', 'modificarHistoria', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HIS0223">Modificar Historia Clinica</a></span>
                                                </li>
                                                <%}%>


                                                <% if (beanSession.usuario.preguntarMenu("HIS09")) {%>                                            
                                                <li class="closed">
                                                    <span class="folder" title="HIS09" id="padClick" onclick="ocultarMenuSlider('#contentMenuReferencia')">
                                                        <i class="fas fa-folder"></i>
                                                        REFERENCIA
                                                    </span>
                                                    <ul class="secnav" id="contentMenuReferencia" style="display:none ;" >
                                                        <% if (beanSession.usuario.preguntarMenu("HIS10")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i>
                                                                <a class="activeP" id="li9" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/referencia.jsp")%>',
                                                                                '4-0-0', 'referencia', 'referencia', 's', 's', 's', 's', 's', 's');
                                                                        activarLI(this.id);"
                                                                   style="cursor:pointer" title="HIS10">Referencia Contra Referencia</a>
                                                            </span>
                                                        </li>
                                                        <%}%>
                                                    </ul>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </li>
                                        <%}%>
                                        <!-- 1 menu -->
                                        <% if (beanSession.usuario.preguntarMenu("APD") || beanSession.usuario.preguntarMenu("HOS")) {%>
                                        <li class="closed" >
                                            <span class="folderApoyoDx" title="APD" id="padClick" onclick="ocultarMenuSlider('#contentMenuApoyo')">
                                                <i class="fas fa-user-md"></i>
                                                APOYO DIAGN&Oacute;STICO
                                            </span>
                                            <ul class="secnav" id="contentMenuApoyo">
                                                <% if (beanSession.usuario.preguntarMenu("APD01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li10" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/apoyoDiagnostico.jsp")%>', '4-0-0', 'apoyoDx', 'apoyoDx', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="APD01">Laboratorios y ayudas diagn&oacute;sticas</a></span>
                                                </li>
                                                <%}%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li11" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/apoyoDx.jsp")%>', '4-0-0', 'apoyoDx02', 'apoyoDx02', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer">Apoyo Diagnostico</a></span>
                                                </li>
                                                <% if (beanSession.usuario.preguntarMenu("ADM15")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li12" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/pruebaLaboratorio.jsp")%>', '4-0-0', 'pruebaLaboratorio', 'pruebaLaboratorio', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="APD02">Pruebas Laboratorio</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("ADM15")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li13" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaLaboratorio.jsp")%>',
                                                                            '4-0-0', 'plantillaLaboratorio', 'plantillaLaboratorio', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="ADM15" >Plantillas Laboratorio</a></span>
                                                </li>
                                                <%}%>                                                                                    
                                            </ul>
                                        </li>
                                        <%}%>
                                        <% if (beanSession.usuario.preguntarMenu("APD") || beanSession.usuario.preguntarMenu("HOS")) {%>
                                        <li class="closed">
                                            <span class="folderApoyoDx" title="APD" id="padClick" onclick="ocultarMenuSlider('#contentMenuAutorizaciones')">
                                                <i class="fas fa-clipboard-check"></i>
                                                AUTORIZACIONES
                                            </span>
                                            <ul class="secnav" id="contentMenuAutorizaciones">
                                                <% if (beanSession.usuario.preguntarMenu("APD01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li14" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/autorizaciones/autorizaciones.jsp")%>', '4-0-0', 'autorizaciones', 'autorizaciones', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="APD01">Autorizaciones</a></span>
                                                </li>
                                                <%}%>                                                                                 
                                            </ul>
                                        </li>
                                        <%}%>
                                        <!-- 2 menu -->
                                        <% if (beanSession.usuario.preguntarMenu("POS")) {%>
                                        <li class="closed">
                                            <span class="folderPostConsulta" title="POS" id="padClick" onclick="ocultarMenuSlider('#contentMenuPostConsulta')">
                                                <i class="fas fa-notes-medical"></i>
                                                POST CONSULTA
                                            </span>
                                            <ul class="secnav" id="contentMenuPostConsulta" >
                                                <% if (beanSession.usuario.preguntarMenu("POS01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li15" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/postConsulta/documentos.jsp")%>', '4-0-0', 'documentos', 'documentos', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="POSF">Folios</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("POS02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li16" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="POS02" >Plantilla Procedimientos NoPos</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("POS03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li17" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="POS03" >Plantilla Medicamentos NoPos</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("POS04")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li18" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionAyudaDiagnostica.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer"  title="POS04" >Gestión Ayudas
                                                            Diagnosticas</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("POS05")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li19" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionSolicitudes.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="POS05">Gestión Solicitudes
                                                            Intercambio</a></span>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </li>
                                        <% }%>
                                        <!-- 3 menu -->
                                        <% if (beanSession.usuario.preguntarMenu("HOS")) {%>
                                        <li class="closed">
                                            <span class="folderHospitalizacion" title="HOS" id="padClick" onclick="ocultarMenuSlider('#contentMenuHospitalizacion')">
                                                <i class="fas fa-procedures"></i>
                                                HOSPITALIZACION
                                            </span>
                                            <ul class="secnav" id="contentMenuHospitalizacion">

                                                <% if (beanSession.usuario.preguntarMenu("HOS01") || beanSession.usuario.preguntarMenu("HOS")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i>
                                                        <a 
                                                            class="activeP" id="li20" onclick="javascript:cargarMenu('<%= response.encodeURL("hospitalizacion/ubicacionCama.jsp")%>', '4-0-0', 'ubicacionCama', 'ubicacionCama', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="HOS01">Ubicacion Cama
                                                            paciente</a></span>
                                                </li>
                                                <% }%>
                                            </ul>
                                        </li>
                                        <%}%>
                                        <!-- 4 menu  -->
                                        <% if (beanSession.usuario.preguntarMenu("RUT")) {%>
                                        <li class="closed">
                                            <span class="folderRutas" title="RUT" id="padClick" onclick="ocultarMenuSlider('#contentMenuRutas')" >
                                                <i class="fas fa-route"></i>
                                                RUTAS
                                            </span>
                                            <ul class="secnav" id="contentMenuRutas">
                                                <% if (beanSession.usuario.preguntarMenu("RUT01")) {%> 
                                                <li class="closed">
                                                    <span class="folder" title="RUT01" id="padClick" onclick="ocultarMenuSlider('#contentMenuParametros')">
                                                        <i class="fas fa-folder"></i>
                                                        PARAMETROS
                                                    </span>
                                                    <ul class="secnav" id="contentMenuParametros" >
                                                        <% if (beanSession.usuario.preguntarMenu("RUT02")) {%> 
                                                        <li><span class="file"><i class="far fa-circle"></i>
                                                                <a class="activeP" id="li21" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/planAtencionRuta.jsp")%>', '4-0-0', 'planAtencionRuta', 'planAtencionRuta', 's', 's', 's', 's', 's', 's');
                                                                        activarLI(this.id);"
                                                                   style="cursor:pointer" title="RUT02">Rutas</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("RUT03")) {%> 
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li22" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/planParametros.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="RUT03">Programas</a></span>
                                                        </li>
                                                        <%}%>
                                                    </ul> 
                                                    <%}%>                                            


                                                    <!--li><span class="file"><i class="far fa-circle"></i><a
                                                                class="activeP" id="li23" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionServicio.jsp")%>', '4-0-0', 'gestionServicio', 'gestionServicio', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                style="cursor:pointer"
                                                                title="Crear contactos Paciente">Gestión del
                                                                Servicio</a></span></li-->
                                                    <% if (beanSession.usuario.preguntarMenu("RUT05")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li24" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/cerfificarContactos.jsp")%>', '4-0-0', 'cerfificarContactos', 'cerfificarContactos', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="RUT05">Certificar  Contactos</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("RUT06")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li25" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/enrutarPaciente.jsp")%>', '4-0-0', 'enrutarPaciente', 'enrutarPaciente', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="RUT06">Enrutar al Paciente</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("RUT07")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li26" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/listaEsperaAgendamiento.jsp")%>', '4-0-0', 'listaEsperaAgendamiento', 'listaEsperaAgendamiento', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"  title="RUT07">Agendas desde lista de espera</a></span>
                                                </li>
                                                <%}%>


                                                <!--li><span class="file"><i class="far fa-circle"></i><a
                                                                class="activeP" id="li27" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionPaciente.jsp")%>', '4-0-0', 'rutaPaciente', 'rutaPaciente', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                style="cursor:pointer">Citas pendientes</a></span>
                                                </li>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                    class="activeP" id="li28" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '4-0-0', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                    style="cursor:pointer"
                                                    title="Usted puede administrar paciente">Paciente</a></span></li-->                                                    

                                            </ul>
                                        </li>
                                        <%}%>  
                                        <!-- 5 menu -->
                                        <% if (beanSession.usuario.preguntarMenu("PROC")) {%>
                                        <li class="closed"><span class="folderCirugia" title="CITP"  >CITAS PROCEDIMIENTOS</span>
                                            <ul class="secnav">
                                                <% if (beanSession.usuario.preguntarMenu("PROC01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li29" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="PROC01">Horario Procedimientos</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("PROC02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li30" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="PROC02">Agenda</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("PRO03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li31" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/cirugia/listaEspera.jsp")%>', '4-0-0', 'listaEsperaCirugia', 'listaEsperaCirugia', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="PRO03">Lista de
                                                            Espera</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("PROC04")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li32" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/gestionProcedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="PROC04">Gestión Procedimientos</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("PROC05")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li33" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/hc/principalHC.jsp")%>', '4-0-0', 'principalOrdenes', 'principalOrdenes', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="PROC05">Evolucion
                                                            Cirugia</a></span>
                                                </li>
                                                <%}%>          
                                            </ul>
                                        </li>
                                        <%}%>
                                        <!-- menu 6 -->
                                        <% if (beanSession.usuario.preguntarMenu("CIT")) {%>
                                        <li class="closed">
                                            <span class="folderAtUsuario" title="CIT" id="padClick" onclick="ocultarMenuSlider('#contentMenuAtencionUser')">
                                                <i class="fas fa-phone-alt"></i>
                                                ATENCION AL USUARIO
                                            </span>
                                            <ul class="secnav" id="contentMenuAtencionUser">

                                                <% if (beanSession.usuario.preguntarMenu("CIT01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li34" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/horarios.jsp")%>', '4-0-0', 'horarios', 'horarios', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="CIT01">Agendamiento</a></span>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("CIT02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li35" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="CIT02">Asignacion de Citas</a></span>
                                                </li>
                                                <% }%> 
                                                <% if (beanSession.usuario.preguntarMenu("CIT03")) {%> 
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li36" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/misCitasHoy.jsp")%>', '6-1-3', 'misCitasHoy', 'misCitasHoy', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="CIT03">Mis Citas de Hoy</a></span>
                                                </li>
                                                <% }%> 
                                                <% if (beanSession.usuario.preguntarMenu("PRO03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li37" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="PRO03">Lista de
                                                            Espera</a></span>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("CIT04")) {%>                                            
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li38" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/contactosPaciente.jsp")%>', '4-0-0', 'contactosPaciente', 'contactosPaciente', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="CIT04">Centro de Contacto</a></span>
                                                </li>
                                                <%}%>   
                                                <% if (beanSession.usuario.preguntarMenu("CIT05")) {%>    
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li39" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/paciente.jsp")%>', '4-0-0', 'paciente', 'paciente', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="CIT05">Paciente</a></span>
                                                </li>
                                                <%}%>                                       

                                            </ul>
                                        </li>
                                        <%}%>
                                        <!-- menu 7 -->
                                        <% if (beanSession.usuario.preguntarMenu("HOM")) {%>
                                        <li class="closed">
                                            <span class="folderAtUsuario" title="HOM" id="padClick" onclick="ocultarMenuSlider('#contentMenuHomeCare')">
                                                <i class="fas fa-hand-holding-medical"></i>
                                                HOME CARE
                                            </span>
                                            <ul class="secnav" id="contentMenuHomeCare">
                                                <% if (beanSession.usuario.preguntarMenu("HOM01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li40" onclick="javascript:cargarMenu('<%= response.encodeURL("homecare/coordinacion.jsp")%>', '4-0-0', 'coordinacionHomecare', 'coordinacionHomecare', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="HOM01">Programacion terapias</a></span>
                                                </li>   
                                                <%}%>  
                                                <% if (beanSession.usuario.preguntarMenu("HOM02")) {%>                                   
                                                <li ><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li41" onclick="javascript:cargarMenu('<%= response.encodeURL("homecare/agenda.jsp")%>', '6-1-3', 'agendaHomecare', 'agendaHomecare', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="HOM02">Agenda HOMECARE</a></span>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </li>
                                        <%}%>
                                        <% if (beanSession.usuario.preguntarMenu("ORI")) {%>
                                        <li class="closed">
                                            <span class="folderOrientacion" title="ORI" id="padClick" onclick="ocultarMenuSlider('#contentMenuOrientacion')">
                                                <i class="fas fa-people-arrows"></i>
                                                ORIENTACION
                                            </span>
                                            <ul class="secnav" id="contentMenuOrientacion">
                                                <% if (beanSession.usuario.preguntarMenu("ORI01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li42" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaPacientes.jsp")%>', '4-0-0', 'asistenciaPacientes', 'asistenciaPacientes', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="ORI01">Asistencia de Pacientes</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("ORI02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li43" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/parlante.jsp")%>', '4-0-0', 'parlante', 'parlante', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="ORI02">Parlante</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("ORI03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li44" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/asistenciaDePacientes.jsp")%>', '4-0-0', 'asitenciaMedia', 'asitenciaMedia', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="ORI03">Visitantes</a></span>
                                                </li>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li45" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/asistencia/turnos.jsp")%>', '4-0-0', 'demo', 'demo', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="ORI02">DEMO</a></span>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </li>
                                        <%}%>
                                        <!-- menu 8 -->
                                        <% if (beanSession.usuario.preguntarMenu("FAC")) {%>
                                        <li class="closed">
                                            <span class="folderFacturacion" title="FAC" id="padClick" onclick="ocultarMenuSlider('#contentMenuFacturacion')">
                                                <i class="fas fa-dollar-sign"></i>
                                                FACTURACION
                                            </span>
                                            <ul class="secnav" id="contentMenuFacturacion">
                                                <% if (beanSession.usuario.preguntarMenu("FAC01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li46" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admisiones.jsp")%>', '4-0-0', 'admisiones', 'admisiones', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="FAC01">Admisiones</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("FAC02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li47" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/soloFactura.jsp")%>', '4-0-0', 'soloFactura', 'soloFactura', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="FAC02">Crear Solo Factura</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("PRO03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li48" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="PRO03">Lista de
                                                            Espera</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("FAC03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li49" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/rips.jsp")%>', '4-0-0', 'rips', 'rips', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="FAC03">Generar
                                                            RIPS</a></span>
                                                </li>
                                                <%}%>


                                                <% if (beanSession.usuario.preguntarMenu("FAC04")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="FAC04" id="padClick" onclick="ocultarMenuSlider('#contentMenufactParametros')">
                                                        <i class="fas fa-folder"></i>
                                                        PARAMETROS
                                                    </span>
                                                    <ul class="secnav" id="contentMenufactParametros">
                                                        <% if (beanSession.usuario.preguntarMenu("FAC05")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li50" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="FAC05">Procedimiento</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("FAC06")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li51" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/articulo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="FAC06">Art&iacute;culos</a></span>
                                                        </li>
                                                        <%}%> 
                                                        <% if (beanSession.usuario.preguntarMenu("FAC07")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li52" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/copago_maximo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="FAC07">M&aacuteximo
                                                                    evento</a></span>
                                                        </li>
                                                        <%}%>
                                                    </ul>
                                                </li>
                                                <%}%>
                                                <!-- <% if (beanSession.usuario.preguntarMenu("FAC04")) {%>
                                                    <li class="closed"><span class="folder" title="FAC04" >PARAMETROS</span>
                                                        <ul class="secnav">
                                                <% if (beanSession.usuario.preguntarMenu("FAC05")) {%>
                                                    <li><span class="file"><i class="far fa-circle"></i><a
                                                                class="activeP" id="li53" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                style="cursor:pointer" title="FAC05">Procedimiento</a></span>
                                                    </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("FAC06")) {%>
                                                    <li><span class="file"><i class="far fa-circle"></i><a
                                                                class="activeP" id="li54" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/articulo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                style="cursor:pointer"
                                                                title="FAC06">Articulos</a></span>
                                                    </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("FAC07")) {%>
                                                    <li><span class="file"><i class="far fa-circle"></i><a
                                                                class="activeP" id="li55" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/copago_maximo.jsp")%>', '4-0-0', 'articulosPlan', 'articulosPlan', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                style="cursor:pointer"
                                                                title="FAC07">Massassasas</a></span>
                                                    </li>
                                                <%}%>
                                            </ul>
                                        </li>
                                                <%}%>     -->
                                                <% if (beanSession.usuario.preguntarMenu("FAC08")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="FAC08" id="padClick" onclick="ocultarMenuSlider('#contentMenuFactContratacion')">
                                                        <i class="fas fa-folder"></i>
                                                        CONTRATACI&Oacute;N
                                                    </span>
                                                    <ul class="secnav" id="contentMenuFactContratacion">
                                                        <!-- <% if (beanSession.usuario.preguntarMenu("FAC09")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li56" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/tarifarios.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="FAC09">Tarifarios</a></span>
                                                        </li>
                                                        <%}%> -->
                                                        <% if (beanSession.usuario.preguntarMenu("FAC10")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li57" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/planes.jsp")%>', '4-0-0', 'planes', 'planes', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="FAC10">Planes de Contratación</a></span>
                                                        </li>
                                                        <%}%>   
                                                    </ul>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("FAC11")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="FAC14" id="padClick" onclick="ocultarMenuSlider('#contentMenuFactFacturaTipi')">
                                                        <i class="fas fa-folder"></i>
                                                        TIPIFICACIÓN
                                                    </span>
                                                    <ul class="secnav" id="contentMenuFactFacturaTipi">
                                                        <% if (beanSession.usuario.preguntarMenu("FAC12")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li136" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/documentos_tipificacion.jsp")%>', '4-0-0', 'documentos', 'documentos', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                                    style="cursor:pointer" title="FAC12">Tipificación Documentos</a></span>
                                                        </li>
                                                        <%}%><!--
                                                        <% if (beanSession.usuario.preguntarMenu("FAC13")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li137" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/tipificacion_externa.jsp")%>', '4-0-0', 'tipificacion_externa', 'tipificacion_externa', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                                    style="cursor:pointer" title="FAC13">Tipificacion Externa</a></span>
                                                        </li>
                                                        <%}%>--!>
                                                    </ul>
                                                </li>
                                                <%}%>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("FAC14")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="FAC14" id="padClick" onclick="ocultarMenuSlider('#contentMenuFactFactura')">
                                                        <i class="fas fa-folder"></i>
                                                        FACTURA
                                                    </span>
                                                    <ul class="secnav" id="contentMenuFactFactura">
                                                        <% if (beanSession.usuario.preguntarMenu("FAC15")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a class="activeP" id="li60" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturas.jsp")%>',
                                                                        '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's');
                                                                activarLI(this.id);"
                                                                                                               style="cursor:pointer" title="FAC15">Facturas</a></span>
                                                        </li>
                                                        <%}%>   
                                                        <!--li><span class="file" ><a class="activeP" id="li61" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasAnuladas.jsp")%>', '4-0-0', 'Facturas', 'Facturas', 's', 's', 's', 's', 's', 's'); activarLI(this.id);" style="cursor:pointer">Facturas Anuladas</a></span>
                                                        </li-->
                                                        <% if (beanSession.usuario.preguntarMenu("FAC16")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li62" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/recibos.jsp")%>', '4-0-0', 'recibos', 'recibos', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="FAC16" >Recibos</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("FAC17")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li63" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/admisiones/admision.jsp")%>', '4-0-0', 'admision', 'admision', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="FAC17">Rips</a></span>
                                                        </li>
                                                        <%}%>
                                                    </ul>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("FAC14")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="FAC14" id="padClick" onclick="ocultarMenuSlider('#contentMenuFactNominal')">
                                                        <i class="fas fa-folder"></i>
                                                        NOMINAL
                                                    </span>
                                                    <ul class="secnav" id="contentMenuFactNominal">
                                                        <% if (beanSession.usuario.preguntarMenu("FAC17")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a class="activeP" id="li64" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/nominalNefro.jsp")%>',
                                                                        '4-0-0', 'nominalNefro', 'Facturas', 's', 's', 's', 's', 's', 's');
                                                                activarLI(this.id);"
                                                                                                               style="cursor:pointer" title="FAC17">Base nominal nefroproteccion</a></span>
                                                        </li>
                                                        <%}%>   
                                                    </ul>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </li>
                                        <!-- menu 9 -->
                                        <% if (beanSession.usuario.preguntarMenu(
                                                    "VEN")) {%>
                                        <li class="closed">
                                            <span class="folderVentas" title="VEN" id="padClick" onclick="ocultarMenuSlider('#contentMenuVentas')">
                                                <i class="fas fa-shopping-cart"></i>
                                                VENTAS
                                            </span>
                                            <ul class="secnav" id="contentMenuVentas">
                                                <% if (beanSession.usuario.preguntarMenu("VEN01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li65" onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/salidasVentas.jsp")%>', '4-0-0', 'salidasVentas', 'salidasVentas', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="VEN01">Facturas</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("VEN02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li66" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/facturasTrabajo.jsp")%>', '4-0-0', 'facturasTrabajo', 'facturasTrabajo', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer" title="VEN02">Trabajos</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("VEN03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li67" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/cartera.jsp")%>', '4-0-0', 'cartera', 'cartera', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="VEN03">Cartera</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("VEN04")) {%>
                                                <li class="closed">
                                                    <span class="folder"  title="VEN04" id="padClick" onclick="ocultarMenuSlider('#contentMenuVentasPatametros')">
                                                        <i class="fas fa-folder"></i>
                                                        PARAMETROS
                                                    </span>
                                                    <ul class="secnav" id="contentMenuVentasPatametros">
                                                        <% if (beanSession.usuario.preguntarMenu("VEN05")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li68" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/parametrosTrabajos.jsp")%>', '4-0-0', 'Funcionarios', 'Funcionarios', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="VEN05">Funcionarios</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("VEN06")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li69" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/terceros/principalTerceros.jsp")%>', '4-0-0', 'Terceros', 'Terceros', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="VEN06">Terceros</a></span>
                                                        </li>
                                                        <%}%>
                                                    </ul>
                                                </li>
                                                <%}%>

                                            </ul>
                                        </li>    
                                        <%}%>
                                        <!-- menu 10 -->
                                        <% if (beanSession.usuario.preguntarMenu(
                                                    "ARC")) {%>
                                        <li class="closed">
                                            <span class="folderArchivo" title="ARC" id="padClick" onclick="ocultarMenuSlider('#contentMenuArchivo')">
                                                <i class="fas fa-folder-open"></i>
                                                ARCHIVO
                                            </span>
                                            <ul class="secnav" id="contentMenuArchivo">
                                                <% if (beanSession.usuario.preguntarMenu("PRO03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li70" onclick="javascript:cargarMenu('<%= response.encodeURL("atusuario/citas/listaEspera.jsp")%>', '4-0-0', 'listaEspera', 'listaEspera', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="PRO03 ">Lista de
                                                            Espera</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("ARC01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li71" onclick="javascript:cargarMenu('<%= response.encodeURL("archivo/agenda.jsp")%>', '4-0-0', 'agenda', 'agenda', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="ARC01 ">Agenda Archivo</a></span>
                                                </li>

                                                <%}%>
                                            </ul>
                                        </li>
                                        <%}%>
                                        <% if (beanSession.usuario.preguntarMenu(
                                                    "INV")) {%>
                                        <li class="closed">
                                            <span class="folderFarmacia" title="INV" id="padClick" onclick="ocultarMenuSlider('#contentMenuInventarios')">
                                                <i class="fas fa-archive"></i>
                                                INVENTARIOS
                                            </span>

                                            <ul class="secnav" id="contentMenuInventarios">
                                                <% if (beanSession.usuario.preguntarMenu("INV06")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li72" onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/despachoProgramacionProcedimientos.jsp")%>', '4-0-0', 'despachoProgramacionProcedimientos', 'despachoProgramacionProcedimientos', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="INV06">Despacho a Procedimientos</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("INV02")) {%>   
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li73" onclick="javascript:cargarMenu('<%= response.encodeURL("suministros/transacciones/solicitudes.jsp")%>', '4-0-0', 'solicitudes', 'solicitudes', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="INV02">Solicitudes</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("INV14")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="INV14" id="padClick" onclick="ocultarMenuSlider('#contentMenuInvEntradas')">
                                                        <i class="fas fa-folder"></i>
                                                        ENTRADAS
                                                    </span>
                                                    <ul class="secnav" id="contentMenuInvEntradas">
                                                        <% if (beanSession.usuario.preguntarMenu("INV01")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li74" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/articulo/pedidos.jsp")%>', '4-0-0', 'Pedidos', 'Pedidos', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="INV01">Ordenes de Pedido Internas</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("INV01")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li75" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/articulo/recepcionPedidos.jsp")%>', '4-0-0', 'recepcionPedidos', 'recepcionPedidos', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="INV01">Recepcion de Ordenes de Pedidos</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("INV03")) {%>   
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li76" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/transacciones/ordenesCompra.jsp")%>', '10-0-0', 'ordenesCompra', 'ordenesCompra', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="INV03">Ordenes de Compra </a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("INV03")) {%>   
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li77" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/transacciones/comprobanteEntrada.jsp")%>', '4-0-0', 'comprobanteEntrada', 'comprobanteEntrada', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="INV03">Comprobante Entrada </a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("INV03")) {%>   
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li78" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/transacciones/entradasDeBodega.jsp")%>', '4-0-0', 'entradasDeBodega', 'entradasDeBodega', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="INV03">Entradas a
                                                                    Bodega </a></span>
                                                        </li>
                                                        <%}%>
                                                    </ul>
                                                </li>
                                                <% }%> 


                                                <% if (beanSession.usuario.preguntarMenu("INV04")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="INV14" id="padClick" onclick="ocultarMenuSlider('#contentMenuInvSalidas')" >
                                                        <i class="fas fa-folder"></i>
                                                        SALIDAS
                                                    </span>
                                                    <ul class="secnav" id="contentMenuInvSalidas">
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li79" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/transacciones/salidasDeBodega.jsp")%>', '4-0-0', 'salidasDeBodega', 'salidasDeBodega', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="INV04">Salidas de
                                                                    Bodega </a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("INV05")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li80" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/transacciones/trasladoBodegas.jsp")%>', '4-0-0', 'trasladoBodegas', 'trasladoBodegas', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="INV05">Traslado entre Bodegas</a></span>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("INV14")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="INV14" id="padClick" onclick="ocultarMenuSlider('#contentMenuInvParametros')">
                                                        <i class="fas fa-folder"></i>
                                                        PARAMETROS
                                                    </span>
                                                    <ul class="secnav" id="contentMenuInvParametros">
                                                        <% if (beanSession.usuario.preguntarMenu("INV15")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li81" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/articulo/principalArticulo.jsp")%>', '4-0-0', 'principalArticulo', 'principalArticulo', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="INV15" title="INV15">Productos</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("INV16")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li82" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/canastas/plantillaCanastas.jsp")%>', '4-0-0', 'plantillaCanastas', 'plantillaCanastas', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="INV16" >
                                                                    Canastas</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("INV17")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li83" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/bodega/crearBodega.jsp")%>', '4-0-0', 'CrearBodega', 'CrearBodega', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="INV17">
                                                                    Bodegas</a></span>
                                                        </li>
                                                        <% }%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li84" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/terceros/principalTerceros.jsp")%>', '4-0-0', 'Terceros', 'Terceros', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="VEN06">Terceros</a></span>
                                                        </li>
                                                        <!--
                                                        <% if (beanSession.usuario.preguntarMenu("INV18")) {%>
                                                            <li><span class="file"><i class="far fa-circle"></i><a
                                                                        class="activeP" id="li85" onclick="javascript:cargarMenu('<%= response.encodeURL("inventarios/bodega/usuarios_bodega.jsp")%>', '4-0-0', 'CrearBodega', 'CrearBodega', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                        style="cursor:pointer"
                                                                        title="INV17">
                                                                        Gestion de usuarios Bodega</a></span>
                                                            </li>
                                                        <% }%> 
                                                        !-->
                                                    </ul>
                                                </li>
                                                <% }%> 
                                            </ul>
                                        </li>
                                        <%}%>
                                        <!-- menu 10 -->
                                        <% if (beanSession.usuario.preguntarMenu("GEO")) {%>
                                        <li class="closed">
                                            <span class="folderGeo" title="GEO" id="padClick" onclick="ocultarMenuSlider('#contentMenuModelo')" >
                                                <i class="fas fa-briefcase-medical"></i>
                                                MODELO</span>
                                            <ul class="secnav" id="contentMenuModelo">
                                                <% if (beanSession.usuario.preguntarMenu("GEO01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li86" onclick="javascript:cargarMenu('<%= response.encodeURL("encuesta/geoAreas.jsp")%>', '4-0-0', 'geoAreas', 'geoAreas', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="GEO01">Cartografia</a></span>
                                                </li>
                                                <%}%> 
                                                <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li87" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/gestion_marcadores.jsp")%>', '4-0-0', 'marcadores', 'marcadores', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="GEO04">Marcadores</a></span>
                                                </li>
                                                <%}%> 
                                                <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>       
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li88" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/gestion_grupos.jsp")%>', '4-0-0', 'gestionGrupo', 'Gestion grupos', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="ADM26">Gestion de grupos</a></span>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>       
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li89" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/asignar_personal_grupo.jsp")%>', '4-0-0', 'gestionPersonal', 'Gestion personal', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="ADM25">Grupo Personal</a></span>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li90" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/asignar_paciente_grupo.jsp")%>', '4-0-0', 'gestionPacientes', 'Gestion pacientes', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="ADM24">Grupo Paciente</a></span>
                                                </li>
                                                <% }%>

                                                <% if (beanSession.usuario.preguntarMenu("GEO03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li91" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/gestion_viviendas.jsp")%>', '4-0-0', 'gestionViviendas', 'gestionViviendas', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="GEO05">Gestion Viviendas</a></span>
                                                </li>
                                                <%}%> 
                                                <% if (beanSession.usuario.preguntarMenu("GEO03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li92" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/gestion_familiar.jsp")%>', '4-0-0', 'gestionFamiliar', 'gestionFamiliar', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="GEO05">Gestion Familiar</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("GEO03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li93" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/georeferenciacion.jsp")%>', '4-0-0', 'georeferenciacion', 'georeferenciacion', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="GEO05">Georeferenciacion</a></span>
                                                </li>
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="ADM23" id="padClick" onclick="ocultarMenuSlider('#contentMenuRutas1')" >
                                                        <i class="fas fa-folder"></i>
                                                        RUTAS
                                                    </span>
                                                    <ul class="secnav" id="contentMenuRutas1">   
                                                        <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>       
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li94" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/rutas_programas.jsp")%>', '4-0-0', 'RutasProgramas', 'RutasProgramas', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM26">Rutas y Programas</a></span>
                                                        </li>
                                                        <% }%>
                                                    </ul>
                                                </li>
                                                <% }%>
                                                <!-- <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>
                                                    <li><span class="file"><i class="far fa-circle"></i><a
                                                                class="activeP" id="li95" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/gestionServicio.jsp")%>', '4-0-0', 'gestionServicio', 'gestionServicio', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                style="cursor:pointer" title="GEO02" >Asignar Medico Area</a></span>
                                                    </li>       
                                                <%}%>
                                                <% if (beanSession.usuario.preguntarMenu("GEO03")) {%>
                                                    <li><span class="file"><i class="far fa-circle"></i><a
                                                                class="activeP" id="li96" onclick="javascript:cargarMenu('<%= response.encodeURL("planAtencion/planParametros.jsp")%>', '4-0-0', 'tipoCitaProcedimiento', 'Gestionar Facturacion', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                style="cursor:pointer" title="GEO03" >Gestion Encuestas</a></span>
                                                    </li>
                                                <%}%> -->

                                                <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="ADM23" id="padClick" onclick="ocultarMenuSlider('#contentMenuModRiesgos')">
                                                        <i class="fas fa-folder"></i>
                                                        RIESGOS
                                                    </span>
                                                    <ul class="secnav" id="contentMenuModRiesgos">   
                                                        <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>       
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li97" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/gestion_riesgos.jsp")%>', '4-0-0', 'gestionRiesgos', 'gestionRiesgos', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM25">Gestion de Riesgos</a></span>
                                                        </li>
                                                        <% }%>

                                                        <% if (beanSession.usuario.preguntarMenu("GEO02")) {%>       
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li98" onclick="javascript:cargarMenu('<%= response.encodeURL("adminModelo/gestion_riesgo_general.jsp")%>', '4-0-0', 'gestionRiesgoGeneral', 'gestionRiesgoGeneral', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM25">Riesgo General</a></span>
                                                        </li>
                                                        <% }%>
                                                    </ul>
                                                </li>
                                                <% }%>
                                            </ul>
                                        </li>
                                        <% }%>
                                        <!-- menu 11 -->
                                        <% if (beanSession.usuario.preguntarMenu("NOT")) {%>
                                        <li class="closed">
                                            <span class="folderNotificaciones" title="NOT" id="padClick" onclick="ocultarMenuSlider('#contentMenuNotificaciones')">
                                                <i class="fas fa-exclamation"></i>
                                                NOTIFICACIONES
                                            </span>
                                            <ul class="secnav" id="contentMenuNotificaciones">
                                                <% if (beanSession.usuario.preguntarMenu("NOT01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li99" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/crearNotificacion.jsp")%>', '4-0-0', 'crearNotificacion', 'crearNotificacion', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="NOT01" >Crear</a></span>
                                                </li>
                                                <%}%> 
                                                <% if (beanSession.usuario.preguntarMenu("NOT02")) {%> 
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li100" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/leerNotificacion.jsp")%>', '4-0-0', 'leerNotificacion', 'leerNotificacion', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="NOT02">Leer</a></span>
                                                </li>
                                                <%}%> 
                                            </ul>
                                        </li>
                                        <%}%> 
                                        <% if (beanSession.usuario.preguntarMenu(
                                                    "INF")) {%>
                                        <li class="closed">
                                            <span class="folderInformes"title="INF" id="padClick" onclick="ocultarMenuSlider('#contentMenuInformes')">
                                                <i class="far fa-file-alt"></i>
                                                INFORMES
                                            </span>
                                            <ul class="secnav" id="contentMenuInformes">
                                                <% if (beanSession.usuario.preguntarMenu("INF01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li101" onclick="javascript:cargarMenu('<%= response.encodeURL("reportes/reportes_parametros.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="INF01">Listado de
                                                            Informes</a></span>
                                                </li>
                                                <%}%>

                                                <% if (beanSession.usuario.preguntarMenu("INFNM")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                    class="activeP" id="li102" onclick="javascript:cargarMenu('<%= response.encodeURL("reportes/nominales.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's');
                                                            activarLI(this.id);"
                                                    style="cursor:pointer"
                                                    title="INFNM">Nominales</a></span>
                                                </li>
                                                <%}%>





                                                <!-- VERIFICAR notificacion.jsp
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li102" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/notificaciones/notificacion.jsp")%>', '4-0-0', 'reportes', 'reportes', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                            style="cursor:pointer"
                                                            title="Usted puede consultar Reportes de todas las areas">notificar</a></span>
                                                </li>
                                                -->
                                            </ul>
                                        </li>
                                        <%}%> 
                                        <% if (beanSession.usuario.preguntarMenu(
                                                    "HEL")) {%>
                                        <li class="closed">
                                            <span class="folderCalidad" title="HEL" id="padClick" onclick="ocultarMenuSlider('#contentMenuHelpDesk')" >
                                                <i class="fas fa-file-invoice"></i>
                                                HELP DESK
                                            </span>
                                            <ul class="secnav" id="contentMenuHelpDesk">
                                                <% if (beanSession.usuario.preguntarMenu("HEL01")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a class="activeP" id="li103" onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/crearEvento.jsp")%>',
                                                                '2-1-3', 'crearEvento', 'crearEvento', 's', 's', 's', 's', 's', 's');
                                                        activarLI(this.id);"
                                                                                                       style="cursor:pointer" title="HEL01">Reportar
                                                            Evento</a></span>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("HEL02")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li104" onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/gestionarEventos.jsp")%>', '4-0-0', 'gestionarEvento', 'gestionarEvento', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="HEL02">Gestion de
                                                            Evento</a></span>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("HEL03")) {%>
                                                <li><span class="file"><i class="far fa-circle"></i>
                                                        <a class="activeP" id="li105" onclick="javascript:cargarMenu('<%= response.encodeURL("calidad/evento/respuestaEventos.jsp")%>', '4-0-0', 'gestionarEvento', 'gestionarEvento', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                           style="cursor:pointer" title="HEL03">Respuesta a Eventos</a></span>
                                                </li>
                                                <% }%>
                                            </ul>
                                        </li>
                                        <% }%>
                                        <% if (beanSession.usuario.preguntarMenu("ADM")) {%>
                                        <li class="closed">
                                            <span class="folderAdministracion" title="ADM" id="padClick" onclick="ocultarMenuSlider('#contentMenuAdministracion')">
                                                <i class="fas fa-users-cog"></i>
                                                ADMINISTRACION
                                            </span>
                                            <ul class="secnav" id="contentMenuAdministracion">
                                                <% if (beanSession.usuario.preguntarMenu("ADM01")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="ADM01" id="padClick" onclick="ocultarMenuSlider('#contentMenuAdminSeguridad')" >
                                                        <i class="fas fa-folder"></i>
                                                        Seguridad
                                                    </span>
                                                    <ul class="secnav" id="contentMenuAdminSeguridad">   
                                                        <% if (beanSession.usuario.preguntarMenu("ADM02")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li106" onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/perfiles.jsp")%>', '4-0-0', 'perfiles', 'Perfiles de Usuarios', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM02">Roles</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM03")) {%>       
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li107" onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/crearUsuario.jsp")%>', '4-0-0', 'crearUsuario', 'Creacion de Usuarios', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM03">Gestionar Usuarios</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM04")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li108" onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/usuario.jsp")%>', '4-0-0', 'usuario', 'Gestionar Información de Usuarios', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM04">Credencial de Usuario</a></span>
                                                        </li>
                                                        <% }%>
                                                    </ul>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("ADM23")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="ADM23" id="padClick" onclick="ocultarMenuSlider('#contentMenuAdminModelo')">
                                                        <i class="fas fa-folder"></i>
                                                        Modelo
                                                    </span>
                                                    <ul class="secnav" id="contentMenuAdminModelo">   
                                                        <% if (beanSession.usuario.preguntarMenu("ADM24")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li109" onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/perfiles.jsp")%>', '4-0-0', 'perfiles', 'Perfiles de Usuarios', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM24">Grupo Paciente</a></span>
                                                        </li>
                                                        <% }%>

                                                        <% if (beanSession.usuario.preguntarMenu("ADM25")) {%>       
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li110" onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/crearUsuario.jsp")%>', '4-0-0', 'crearUsuario', 'Creacion de Usuarios', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"
                                                                    title="ADM25">Grupo Personal</a></span>
                                                        </li>
                                                        <% }%>

                                                    </ul>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("ADM05")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="ADM05" id="padClick" onclick="ocultarMenuSlider('#contentMenuAdminParametros')">
                                                        <i class="fas fa-folder"></i>
                                                        Parametros
                                                    </span>
                                                    <ul class="secnav" id="contentMenuAdminParametros">
                                                        <% if (beanSession.usuario.preguntarMenu("ADM07")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li111" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarProfesion.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM07" >Profesiones</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM08")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li112" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/sedeEspecialidades.jsp")%>', '4-0-0', 'sedeEspecialidades', 'sedeEspecialidades', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM08">Especialidades</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM09")) {%> 
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li113" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/tipoCitaProcedimiento.jsp")%>', '4-0-0', 'sedeEspecialidades', 'sedeEspecialidades', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM09">Tipos cita</a></span>
                                                        </li>                                                        
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM11")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li114" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/centroCosto.jsp")%>', '4-0-0', 'Centro de costo', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM11">Centro
                                                                    de costo</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM22")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li115" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/antecedentesFolio.jsp")%>', '4-0-0', 'Centro de costo', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM22">Antecentes Folio</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM23")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li116" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarprocesoHelpDesk.jsp")%>', '4-0-0', 'Centro de costo', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM23">Gestion de Procesos Help Desk </a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM23")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li117" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarencuestaHelpDesk.jsp")%>', '4-0-0', 'encuestaHd', 'encuestaHd', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM23">Gestion de Encuesta Help Desk </a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM26")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li118" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarModulo.jsp")%>', '4-0-0', 'Centro de costo', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM26">Gestion de Modulos</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM25")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li119" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/planTratamiento.jsp")%>', '4-0-0', 'planT', 'planT', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM25">Gestion Planes de Tratamiento</a></span>
                                                        </li>
                                                        <%}%>

                                                    </ul>
                                                </li>
                                                <%}%>
                                                <% if (true) {%>
                                                <li class="closed">
                                                    <span class="folder" title="ADM17" id="padClick" onclick="ocultarMenuSlider('#contentMenuAdminUbicaciones')" >
                                                        <i class="fas fa-folder"></i>
                                                        Ubicaciones
                                                    </span>
                                                    <ul class="secnav" id="contentMenuAdminUbicaciones">
                                                        <% if (beanSession.usuario.preguntarMenu("ADM18")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li120" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configuracionSedes.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM18" >Sede</a></span>
                                                        </li>                                                            
                                                        <% }%> 
                                                        <% if (beanSession.usuario.preguntarMenu("ADM19")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li121" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configuracionArea.jsp")%>', '4-0-0', 'configuracionArea', 'configuracionArea', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM19" >Area</a></span>
                                                        </li>                                                            
                                                        <% }%>                                                                                                                                                                               
                                                        <% if (beanSession.usuario.preguntarMenu("ADM20")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li122" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarHabitacion.jsp")%>', '4-0-0', 'configuracionHabitacion', 'configuracionHabitacion', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM20">Lugar</a></span>
                                                        </li>                                                            
                                                        <% }%>

                                                        <% if (beanSession.usuario.preguntarMenu("ADM21")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li123" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/configurarElemento.jsp")%>', '4-0-0', 'configuracionElemento', 'configuracionElemento', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM21">Cama</a></span>
                                                        </li>  
                                                        <% }%>                                                         
                                                    </ul>
                                                </li>
                                                <% }%>
                                                <% if (beanSession.usuario.preguntarMenu("HIS02") || beanSession.usuario.preguntarMenu("HISTORIACLINICA")) {%>
                                                <li class="closed">
                                                    <span class="folder" title="HIS02" id="padClick" onclick="ocultarMenuSlider('#contentMenuAdminHC')">
                                                        <i class="fas fa-folder"></i>
                                                        Historia Clinica
                                                    </span>
                                                    <ul class="secnav" id="contentMenuAdminHC">
                                                        <% if (beanSession.usuario.preguntarMenu("HIS03")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li124" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/folios.jsp")%>', '4-0-0', 'folios', 'folios', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="HIS03">Tipos de folio</a></span>
                                                        </li>                                                            
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("HIS04")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li125" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/diagnostico2.jsp")%>', '4-0-0', 'diagnostico2', 'diagnostico2', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="HIS04">Diagnostico</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("HIS05")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li126" onclick="javascript:cargarMenu('<%= response.encodeURL("facturacion/procedimiento.jsp")%>', '4-0-0', 'procedimientos', 'procedimientos', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="HIS05">Procedimiento</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("HIS06")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li127" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaProcedimientosNoPos.jsp")%>', '4-0-0', 'plantillaProcedimientosNoPos', 'plantillaProcedimientosNoPos', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="HIS06">Plantilla Procedimientos
                                                                    NoPos</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("HIS07")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li128" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaMedicamentosNoPos.jsp")%>', '4-0-0', 'plantillaMedicamentosNoPos', 'plantillaMedicamentosNoPos', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="HIS07">Plantilla Medicamentos
                                                                    NoPos</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM10")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li129" onclick="javascript:cargarMenu('<%= response.encodeURL("parametros/piePagina.jsp")%>', '4-0-0', 'Pie de Pagina', 'Gestionar Información de xxx', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM10">Pie
                                                                    pagina</a></span>
                                                        </li>
                                                        <% }%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM12")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li130" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFormulario.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM12">Plantillas</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM13")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li131" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM13">Plantillas Folio</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM14")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li132" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaEncuesta.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's'); activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM14">Plantillas Encuesta</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM15")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li133" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/plantillaLaboratorio.jsp")%>', '4-0-0', 'plantillaLaboratorio', 'plantillaLaboratorio', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="ADM15">Plantillas Laboratorio</a></span>
                                                        </li>
                                                        <%}%>
                                                        <% if (beanSession.usuario.preguntarMenu("ADM16")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    class="activeP" id="li134" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/encuestaTipoFolio.jsp")%>', '4-0-0', 'plantillaFormulario', 'plantillaFormulario', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer"  title="ADM16">Encuesta tipo folio</a></span>
                                                        </li>
                                                        <%}%>

                                                        <% if (beanSession.usuario.preguntarMenu("HIS08")) {%>
                                                        <li><span class="file"><i class="far fa-circle"></i><a
                                                                    onclick="mostrarGrafica()"
                                                                    style="cursor:pointer" title="HIS08" >Ejemplo grafica</a></span>
                                                        </li>
                                                        <%}%>

                                                        <% if (beanSession.usuario.preguntarMenu("HIS09")) {%>
                                                        <li>
                                                            <span class="file">
                                                                <i class="far fa-circle"></i>
                                                                <a class="activeP" id="li135" onclick="javascript:cargarMenu('<%= response.encodeURL("/clinica/paginas/hc/manejoAdministrativo/clonarFolio.jsp")%>', '4-0-2', 'clonarFolios', 'clonarFolios', 's', 's', 's', 's', 's', 's');
                                                                            activarLI(this.id);"
                                                                    style="cursor:pointer" title="HIS09">
                                                                    
                                                                    Clonar Folio
                                                                </a>
                                                            </span>
                                                        </li>                                                            
                                                        <%}%>

                                                    </ul>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </li>
                                        <%}%>
                                        <li class="closed">
                                            <span class="folderCalidad" id="padClick" onclick="ocultarMenuSlider('#contentMenuConfiguracion')">
                                                <i class="fas fa-tools"></i>
                                                CONFIGURACION
                                            </span>
                                            <ul class="secnav" id="contentMenuConfiguracion">
                                                <li><span class="file"><i class="far fa-circle"></i><a
                                                            class="activeP" id="li135" onclick="javascript:cargarMenu('<%= response.encodeURL("adminSeguridad/password.jsp")%>', '4-0-0', 'password', 'password', 's', 's', 's', 's', 's', 's');
                                                                    activarLI(this.id);"
                                                            style="cursor:pointer" title="">Cambio de Contrase&ntilde;a</a></span>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- <li class="closed">
                                            <span class="folderCierre" id="padClick" onclick="ocultarMenuSlider('#contentMenuLogOut')">
                                                <i class="fas fa-sign-out-alt"></i>
                                                Cerrar Sesion
                                            </span>
                                            <ul class="secnav" id="contentMenuLogOut">
                                                <li><span class="file"><i class="far fa-circle"></i>
                                                    <a onclick='cerrar_sesion()'>Salir del aplicativo</a>
                                                </li>
                                            </ul>
                                        </li> -->
                                    </ul>
                                    <div id="LogOut">
                                        <a onclick='cerrar_sesion()'>Cerrar Sesi&oacute;n</a>
                                        <i class="fas fa-sign-out-alt"></i>
                                    </div>
                                </nav>

                                <!-- style="width:100%; left:auto; top:70px; overflow: auto; white-space: nowrap; min-height: 100vh;" -->
                                <div id="main"
                                     align="center">
                                    <div id="loaderPrincipal" hidden>
                                        <table>
                                            <tr>
                                                <td align="center"><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="100px" height="100px"></td>
                                            </tr>
                                            <tr>
                                                <td><label style="font-size: small; font-style: italic;">Cargando ventana. Por favor espere...</label></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div
                                    style="top:701px; display:none; left:10px; position:absolute; color:#3C3; font-weight:bold; width:1136px; height:9px; background-color:#8c0050">
                                </div>
                                <div class="mensaje" id="mensaje"
                                     style="top:666px; left:7px; position:absolute; color:#FFFFFF; font-weight:bold ">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <footer class="footer">
            <div class="footerInf">
                Copyright © 2023. Crystal It Service. Todos los derechos reservados
            </div>

        </footer>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/brands.js" integrity="sha384-sCI3dTBIJuqT6AwL++zH7qL8ZdKaHpxU43dDt9SyOzimtQ9eyRhkG3B7KMl6AO19" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/1c70883c8e.js" crossorigin="anonymous"></script>

        <!-- <script src="/public/assets/scripts/choices.min.js"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>


    </body>

</html> 
