<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1320" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div align="center" id="tituloForma">
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="RIESGO GENERAL (FOCALIZADO)" />
                </jsp:include>
            </div>
        </td>
    </tr>

    <tr>
        <td valign="top">
            <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0">
                <tr class="titulos" style="display:none ">
                    <td colspan="1">Id</td>
                    <td colspan="1">Paciente</td>
                </tr>
                <tr>

                <tr class="titulos">
                    <td width="10%">Tipo Id</td>
                    <td width="10%">Identificacion</td>
                    <td width="10%">Alerta</td>
                    <td width="10%">Fecha desde</td>
                    <td width="10%">Fecha hasta</td>
                    <td width="10%"></td>
                </tr>

                <tr class="estiloImput">
                    <td>
                        <select size="1" id="cmbTipoIdBus" style="width:70%;" 
                            title="Permite realizar la búsqueda por Tipo de Documento">
                            <option value=""></option>
                            <%     resultaux.clear();
                                   resultaux=(ArrayList)beanAdmin.combo.cargar(105);    
                                   ComboVO cmb105; 
                                   for(int k=0;k<resultaux.size();k++){ 
                                         cmb105=(ComboVO)resultaux.get(k);
                            %>
                            <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                <%=cmb105.getDescripcion()%></option>
                            <%}%>                       
                       </select>
                   </td> 
                   <td><input  type="text" id="txtIdentificacionBus" title="Permite realizar la búsqueda por Identificación" style="width:80%"  onkeypress="javascript:return soloTelefono(event);" size="20" maxlength="20"    /></td>                   
                   <td>
                    <select size="1"  id="cmbIdPrioridad" title="Permite realizar la búsqueda por prioridad" style="width:80%"  >
                        <option value="0">[ TODO ]</option>  
                        <option value="1">PRIORIRDAD 1</option>
                        <option value="2">PRIORIRDAD 2</option>
                        <option value="3">PRIORIRDAD 3</option>
                        <option value="4">PRIORIRDAD 4</option>
                        
                    </select>                       
                   </td>  
                   <td><input type="date" id="txtFechaDesdeRiesgoGeneral"  style="width:80%" /></td>
                   <td><input type="date" id="txtFechaHastaRiesgoGeneral"  style="width:80%" /></td>
                   <td>
                        <input name="btn_BUSCAR" title="CC63-Permite realizar la busqueda de pacientes encuestados" type="button" class="small button blue" value="BUSCAR"  onclick=" generaRiesgoGeneral('listRiesgoGeneral')" />
                    </td>                                        
                </tr>
                <tr class="titulos">
                  <td >MUNICIPIO</td>
                   <td >LOCALIDAD</td>
                   <td >BARRIO/VEREDA</td>
                </tr>
                <tr class="estiloImput">
                  <td>                   
                    <input type="text" id="txtMunicipio" size="60" value="52838-TUQUE" maxlength="60"  style="width:70%"  /> 
                    <img width="18px" height="18px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                  <div id="divParaVentanita"></div>                    
                  </td>
                  <td><select id="cmbIdLoc2" style="width:80%" 
                   onfocus="cargarDepDesdeMunicipioGrupo('txtMunicipio', 'cmbIdLoc2')"
                   onchange=" limpiaAtributo('cmbIdBar2', 0);">	                                        
                   <option value="">[ TODOS ]</option>
                 </select>	 	                   
                 </td> 
                  <td><select size="1" id="cmbIdBar2" style="width:80%"  
                   onfocus="cargarDepDesdeLocalidadGrupo('cmbIdLoc2', 'cmbIdBar2')">	                                        
                   <option value="">[ TODOS ]</option>                                  
                 </select>	                   
                  </td>
                </tr>
                <tr class="titulos">
                    <td colspan="9" width="100%">
                          <div id="idDivListPaciEncue"  style="height:325px; width:100%">
                            <table  width="100%" id="listRiesgoGeneral" ></table>
                          </div> 
                    </td>                    
                </tr>
                <tr>
                    <td colspan="9">
                      <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                        <tr class="titulosCentrados">
                          <td colspan="9"> INFORMACION PACIENTE
                          </td>   
                        </tr>  
                        <tr>
                        <td colspan="9">
                          <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                            <tr>
                              <td>
                                <table width="100%">
                                  <tr class="estiloImputIzq2" style="display: none;">
                                    <td><input id="txtIdBusPaciente" ></td>  
                                  </tr> 
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">IDENTIFICACION</td><td align="center"><input id="txtlIdentifi" readonly style="width:40%; align-self: center;" ></td> 
                                  </tr> 
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">NOMBRE</td><td align="center"><input id="txtNombre" readonly style="width:40%; align-self: center;" ></td> 
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">MUNICIPI DE RESIDENCIA</td><td align="center"><input id="txtMunicipio" readonly style="width:40%; align-self: center;" ></td> 
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">DIRECCION</td><td align="center"><input id="txtDireccionRes" style="width:40%; align-self: center;" ></td> 
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">TELEFONO</td><td align="center"><input id="txtTelefonos" style="width:40%; align-self: center;" ></td> 
                                  </tr>
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">CELULAR</td><td align="center"><input id="txtCelular1" style="width:40%; align-self: center;" ></td> 
                                  </tr>
                                  <tr class="estiloImputIzq2" style="display: none;">
                                    <td width="30%" align="center">ENCUESTA</td><td align="center"><input id="txtEncuesta" readonly style="width:40%; align-self: center;" ></td> 
                                  </tr> 
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">PRIORIDAD</td><td align="center"><label id="lblImgAlerta"></label><input  readonly id="txtAlerta" style="width:40%; align-self: center;"></td> 
                                  </tr>   
                                  <tr class="estiloImputIzq2">
                                    <td width="30%" align="center">EDAD</td><td align="center"><input id="txtEdad" readonly style="width:40%;" ><input type="hidden" id="txt_banderaOpcionCirugia" value="NO" /> 
                                      <input type="hidden" id="txt_banderaOpcionCirugiaGestion" value="NO" />       </td> 
                                  </tr>                                                                                                                    
                                </table> 
                              </td>   
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td align="center" >
                                <input name="btn_MODIFICAR" title="AC05" type="button" class="small button blue" value="ACTUALIZAR INFORMACION" onclick="actualizarDatosDesdeRiesgos();" />
                              </td>
                            </tr>   
                          </table>
                        </td>
                        </tr>
                        <tr class="titulos">
                          <td colspan="9" width="100%">
                                <div id="idDivListPaciEncue"  style="height:230px; width:100%">
                                  <table  width="100%" id="listRiesgosFocalizados" ></table>
                                </div> 
                          </td>                    
                      </tr> 
                        <tr class="titulosCentrados" >   
                          <td width="50%">LISTA ESPERA</td>
                          <td width="50%">CITAS</td>                
                        </tr>              
                        <tr class="titulos" >   
                          <td colspan="4">
                            <div style="overflow:auto;" id="divContenidoListaEsperaHist">             
                              <table id="listHistoricoListaEspera" class="scroll"></table>                        
                            </div> 
                          </td>
                       </tr>  
                      </table> 
                    </td>
                    <tr>          
                      <td colspan="9">
                        <table width="100%"  cellpadding="0" cellspacing="0"  align="right">
                          <tr>
                            <td> 
                              <div id="divParaRiesgos"></div>     
                            </td>
                          </tr>   
                        </table> 
                      </td>
                  </tr>            
        </table>                       
    </tr>        
</table>    
