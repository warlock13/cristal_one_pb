<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1150px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <div align="center" id="tituloForma">
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="RUTAS Y PROGRAMAS" />
        </jsp:include>
      </div>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <div style="overflow:auto;height:50px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
                  <tr class="titulos" align="center">
                    <td width="50%">CLASIFICACION</td>
                    <td width="20%">&nbsp;</td>                            
                  </tr>
                  <tr class="estiloImput">
                    <td>  
                      <select size="1" id="cmbClasificacionRutasProgramas" title="Permite realizar la búsqueda por Tipo de ruta o programa style="width:80%"  >                        
                        <option value="">[ TODO ]</option>  
                        <%     resultaux.clear();
                                resultaux=(ArrayList)beanAdmin.combo.cargar(1369);    
                                ComboVO cmb2; 
                                for(int k=0;k<resultaux.size();k++){ 
                                    cmb2=(ComboVO)resultaux.get(k);
                         %>
                            <option value="<%= cmb2.getId()%>" title="<%= cmb2.getTitle()%>">
                                <%= cmb2.getId()+"  "+cmb2.getDescripcion()%></option>
                            <%}%>                                  
                      </select>      
                    </td>  
                    <td>
                    <input name="btn_BUSCAR" title="Seleccione rutas a Buscar" type="button" class="small button blue" value="BUSCAR"  onclick="rutasProgramas('listGrillaRutasModelo')"/>                    
                    </td>         
                  </tr>	                          
              </table>
            </div>
            <table width="100%"  cellpadding="0" cellspacing=""  align="center">
              <tr>
                <td> 
                  <table width="100%" cellpadding="0" cellspacing="0">
                    <tr class="titulos">
                        <td width="100%">                    
                            <table id="listGrillaRutasModelo" class="scroll"></table>                     
                            <div id="pager71"></div> 
                        </td>
                    </tr>
                    <tr class="titulosCentrados">
                        <td>TECNOLOGIAS
                        </td>
                    </tr>
                    <tr class="titulos">
                        <td width="100%"> 
                            <table id="listGrillaRutaTecnologia" class="scroll"></table>
                            <div id="pager72"></div>         
                        </td>
                      </tr>
                </table>
                <input type="hidden" id="txtIdRuta"  />
                <input type="hidden"  id="txtIdParametro"  />
                </td>
              </tr>   
            </table>  
          </td>
        </tr>
      </table> 
    </td>
  </tr>
</table> 