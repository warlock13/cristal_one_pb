var mymap;
    

function tabActivoGrupos(idDiv) {
    switch (idDiv) {
        case 'divGrupoIntegrantes':
            buscarUsuarioGrupo('listGrillaGruposPersonal')   
        break;
            
        case 'divGruposAreas':
            buscarGrillaMapa('listMunicipioAreas');
        break;

        case 'divGruposCarac':
            //caracterizacionGrupo('listCaract');
        break;

        case 'divIntegrantesFamilia':
            buscarFamiliares('listGrillaFamiliares');
        break;

        case 'divGeoreferenciaFamilia':
            cargaMapaMunicipioFamilia();
        break;

        case 'divEncFamiliar':
            preparacionTabsVivienda();  
        break;

        case 'divViviendas':
            buscarParametrosModelo('listViviendas');
        break;

        case 'divInfoVivienda':
            buscarParametrosModelo('listViviendas');
        break;

        case 'divInfoViviendadesdeFamilia':
            cargarFichaViviendaFamilia();
        break;
        
        case 'divGruposMoviles':
            cargarListPacientesMoviles();
        break;

        case 'divGruposRiesgos':
            //cargarListPacientesMoviles();
        break;

        case 'divGruposEstadisticoRiesgos':
            estadisticoRiesgosGrupo('listEstadisticoRiesgosGrupo')
        break;

            
    }
}
function buscarUsuarioGrupo(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width() - 100; 
        ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=986&parametros=";
        add_valores_a_mandar(valorAtributo('txtId'));
            $('#drag' + ventanaActual.num).find("#listGrillaGruposPersonal").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['TOTAL', 'IDENTIFICACION', 'NOMBRE', 'id_rol', 'ROL', 'TRANSVERSAL'],
                colModel: [
                        {name: 'total', index: 'total', width: anchoP(ancho, 10) },
                        { name: 'id', index: 'id', width: anchoP(ancho, 10) },
                        { name: 'nombre', index: 'nombre', width: anchoP(ancho, 30) },
                        { name: 'id_rol', index: 'id_rol', hidden: true },
                        { name: 'rol', index: 'rol', width: anchoP(ancho, 30) },
                        { name: 'transversal', index: 'transversal', width: anchoP(ancho, 10) },
                        ],                 
                height: 350,
                width: ancho - 59,
                caption: "PERSONAL",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaGruposPersonal').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtIdPersonal', concatenarCodigoNombre(datosRow.id,datosRow.nombre), 0);
                    asignaAtributo('cmbIdRol', datosRow.id_rol, 0);
                },
            });
        $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
                 
}

function buscarPersonalGrupo(arg){

    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    
            // limpiarDivEditarJuan(arg); 
            //ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2531&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusId'));
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            add_valores_a_mandar(valorAtributo('cmbSE'));
            add_valores_a_mandar(valorAtributo('cmbSE'));

            $('#drag' + ventanaActual.num).find("#listGrillaUsuarios").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'TIPO ID', 'tipo_doc', 'IDENTIFICACION', 'pnombre', 'snombre', 'papellido', 'sapellido', 'NOMBRE PERSONAL', 'id_prof', 'PROFESION', 'registro', 'estado', 'usuario', 'Correo', 'Telefono','id_sede', 'SEDE', 'Estado', 'Firma'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'tipo_id', index: 'tipo_id', width: 20 },
                    { name: 'tipo_doc', index: 'tipo_doc', hidden: true },
                    { name: 'id', index: 'id', width: 38 },
                    { name: 'pnombre', index: 'pnombre', hidden: true },
                    { name: 'snombre', index: 'snombre', hidden: true },
                    { name: 'papellido', index: 'papellido', hidden: true },
                    { name: 'sapellido', index: 'sapellido', hidden: true },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 25) },
                    { name: 'id_profesion', index: 'id_profesion', hidden: true },
                    { name: 'Profesiones', index: 'Profesiones', width: anchoP(ancho, 15) },
                    { name: 'noregistro', index: 'noregistro', hidden: true },
                    { name: 'estado', index: 'estado', hidden: true },
                    { name: 'usuario', index: 'usuario', hidden: true },
                    { name: 'Correo', index: 'Correo', hidden: true },
                    { name: 'Telefono', index: 'Telefono', hidden: true },
                    { name: 'id_sede', index: 'id_sede', hidden: true },
                    { name: 'Sede', index: 'Sede', width: anchoP(ancho, 15) },
                    { name: 'Estado', index: 'Estado', hidden: true },
                    { name: 'Firma', index: 'Firma', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 255,
                width: ancho - 5,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaUsuarios').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtTipoId', datosRow.tipo_doc, 0);
                    asignaAtributo('txtIdentificacion', datosRow.id, 0);
                    asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                    asignaAtributo('txtProfesion', datosRow.Profesiones, 0);
                    asignaAtributo('cmbVigente', datosRow.estado, 0);
                    asignaAtributo('txtCorreo', datosRow.Correo, 0);
                    asignaAtributo('txtPhone', datosRow.Telefono, 0);
                    asignaAtributo('cmbSede', datosRow.id_sede, 0);

                   setTimeout(listGruposPersonal(), 2000); 
                                       
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            
}

function buscarPacientesGrupo(arg) {
            pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';

            // limpiarDivEditarJuan(arg); 
            //ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1009&parametros=";
            add_valores_a_mandar(valorAtributo('txtBusId'));
            add_valores_a_mandar(valorAtributo('txtBusNombre'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));

    
            $('#drag' + ventanaActual.num).find("#listGrillaPacientes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
    
                colNames: ['Tot', 'TIPO ID', 'IDENTIFICACION', 'pnombre', 'snombre', 'papellido', 'sapellido', 'NOMBRE PERSONAL',  'Correo', 'Telefono', 'celular', 'id_municipio','MUNICIPIO','id_localidad', 'COMUNA/CORREGIMIENTO', 'id_barrio', 'BARRIO/VEREDA','DIRECCION', 'id', 'semaforo'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'tipo_id', index: 'tipo_id', width: 7 },
                    { name: 'identificacion', index: 'identificacion', width: 12 },
                    { name: 'pnombre', index: 'pnombre', hidden: true },
                    { name: 'snombre', index: 'snombre', hidden: true },
                    { name: 'papellido', index: 'papellido', hidden: true },
                    { name: 'sapellido', index: 'sapellido', hidden: true },
                    { name: 'nompersonal', index: 'nompersonal', width: 21 },
                    { name: 'Correo', index: 'Correo', hidden: true },
                    { name: 'Telefono', index: 'Telefono', hidden: true },
                    { name: 'celular', index: 'celular', hidden: true },
                    { name: 'id_municipio', index: 'id_municipio',  hidden: true },
                    { name: 'Municipio', index: 'Municipio',  width: 10 },
                    { name: 'id_localidad', index: 'id_localidad',  hidden: true },
                    { name: 'locaclidad', index: 'locaclidad',  width: 15 },
                    { name: 'id_barrio', index: 'id_barrio',  hidden: true },
                    { name: 'Barrio', index: 'Barrio',  width: 15 },
                    { name: 'direccion', index: 'direccion',  width: 20 },
                    { name: 'id', index: 'id',  hidden: true },
                    { name: 'semaforo', index: 'semaforo',  width: 20 },
                   

                ],
    
                //  pager: jQuery('#pagerGrilla'), 
                height: 256,
                width: ancho - 2,
                onSelectRow: function (rowid) {
                //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPacientes').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtTipoId', datosRow.tipo_id, 0);
                    asignaAtributo('txtIdentificacion', datosRow.identificacion, 0);
                    asignaAtributo('txtPNombre', datosRow.pnombre, 0);
                    asignaAtributo('txtSNombre', datosRow.snombre, 0);
                    asignaAtributo('txtPApellido', datosRow.papellido, 0);
                    asignaAtributo('txtSApellido', datosRow.sapellido, 0);
                    asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                    asignaAtributo('txtCorreo', datosRow.Correo, 0);
                    asignaAtributo('txtTelefono', datosRow.Telefono, 0);
                    asignaAtributo('txtCelular', datosRow.celular, 0);
                    asignaAtributo('cmbMunicipioModelo', datosRow.id_municipio, 0);
                    asignaAtributoCombo('cmbLocalidadModelo', datosRow.id_localidad, datosRow.locaclidad)
                    asignaAtributoCombo('cmbBariioModelo', datosRow.id_barrio, datosRow.Barrio)
                    asignaAtributo('txtDireccion', datosRow.direccion, 0);  
                    asignaAtributo('txtIdPaciente', datosRow.id, 0);  
                                               
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
           
}
function caracterizacionGrupo(arg) {
    var rango1 = 0;
    var rango2 = 180;
    switch(valorAtributo('cmbCursoVida')){

        case '0':
            rango1 = 0;
            rango2 = 180;
        break;
        case '1':
            rango1 = 0;
            rango2 = 5;
        break;
        case '2':
            rango1 = 6;
            rango2 = 11;
        break;
        case '3':
            rango1 = 12;
            rango2 = 17;
        break;
        case '4':
            rango1 = 18;
            rango2 = 28;
        break;
        case '5':
            rango1 = 29;
            rango2 = 59;
        break;
        case '6':
            rango1 = 60;
            rango2 = 180;
        break;

    }
    var id = valorAtributo('cmbIdArea2'); 
    if (id == 0){
        return alert('Seleccione una area');
    }else{
        var url = 'http://10.0.0.3:9010/filtroCaracterizacion/'+id
    }
    
    
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {                        
            if (object[0] == 0){
                pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
            pagina = pag;
            ancho =
                $("#drag" + ventanaActual.num)
                    .find("#divContenido")
                    .width() - 100; 
                ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=1020&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('cmbIdMun'));
                add_valores_a_mandar(valorAtributo('cmbIdArea2'));
                add_valores_a_mandar(valorAtributo('cmbIdArea2'));
                add_valores_a_mandar(valorAtributo('cmbEstadoModelo'));
                add_valores_a_mandar(valorAtributo('cmbEstadoModelo'));
                add_valores_a_mandar(rango1);
                add_valores_a_mandar(rango2);
        
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
        
                colNames: ['No', 'TIPO ID', 'IDENTIFICACION', 'NOMBRE PERSONAL',  'COMUNA / CORREGIMIENTO', 'BARRIO / VEREDA','DIRECCION', 'TELEFONO', 'CELULAR', 'ID AFILIACION', 'SEMAFORO', 'ENCUESTA', 'CORREO', 'ID_MUNICIPIO', 'ID_LOCALIDAD', 'ID_BARRIO', 'ID_UNICO', 'id'],
                colModel: [
                    { name: 'total', index: 'total', width: anchoP(ancho, 3) },
                    { name: 'tipo_id', index: 'tipo_id', width: anchoP(ancho, 5) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 10) },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 20) },
                    { name: 'localidad', index: 'localidad', width: anchoP(ancho, 20) },
                    { name: 'barrio', index: 'barrio',width: anchoP(ancho, 20) },
                    { name: 'direccion', index: 'direccion',  width: anchoP(ancho, 20) },
                    { name: 'telefono', index: 'telefono',  width: anchoP(ancho, 20) },
                    { name: 'celular', index: 'celular',  width: anchoP(ancho, 20) },
                    { name: 'id_afiliacion', index: 'id_afiliacion',  hidden: true },  
                    { name: 'semaforo', index: 'semaforo',  width: anchoP(ancho, 15) }, 
                    { name: 'encuesta', index: 'encuesta',  width: anchoP(ancho, 10) },
                    { name: 'correo', index: 'correo',  hidden: true},
                    { name: 'id_municipio', index: 'id_municipio',  hidden: true },
                    { name: 'id_localidad', index: 'id_localidad',  hidden: true},
                    { name: 'id_barrio', index: 'id_barrio',  hidden: true},
                    { name: 'id_unico', index: 'id_unico',  hidden: true},
                    { name: 'id', index: 'id',  hidden: true },
        
                ],
        
                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho - 60,
                multiselect: true,
                multiboxonly: true,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        
                        asignaAtributo('txtTipoId', datosRow.tipo_id, 0);
                        asignaAtributo('txtIdentificacion', datosRow.identificacion, 0);
                        asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                        asignaAtributo('txtTelefono', datosRow.telefono, 0);
                        asignaAtributo('txtCelular', datosRow.celular, 0);
                        asignaAtributo('txtCorreo', datosRow.correo, 0);
                        asignaAtributo('cmbMunicipioModelo', datosRow.id_municipio, 0); 
                        asignaAtributoCombo('cmbLocalidadModelo', datosRow.id_localidad, datosRow.localidad)
                        asignaAtributoCombo('cmbBariioModelo', datosRow.id_barrio, datosRow.barrio)
                        asignaAtributo('txtDireccion', datosRow.direccion, 0);  
                        asignaAtributo('txtunico', datosRow.id_unico, 0);  
                        asignaAtributo('txtIdPaciente', datosRow.id, 0); 

                        riesgosConsolidadoGeneralHC(datosRow.id);
                                                   
                                                   
                    },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        
            }else{
                pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
            pagina = pag;
            ancho =
                $("#drag" + ventanaActual.num)
                    .find("#divContenido")
                    .width() - 100; 
                ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=2561&parametros=";
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('txtId'));
                add_valores_a_mandar(valorAtributo('cmbIdMun'));
                add_valores_a_mandar(valorAtributo('cmbIdArea2'));
                add_valores_a_mandar(valorAtributo('cmbIdArea2'));
                add_valores_a_mandar(valorAtributo('cmbEstadoModelo'));
                add_valores_a_mandar(valorAtributo('cmbEstadoModelo'));
                add_valores_a_mandar(rango1);
                add_valores_a_mandar(rango2);
        
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
        
                colNames: ['No', 'TIPO ID', 'IDENTIFICACION', 'NOMBRE PERSONAL',  'COMUNA / CORREGIMIENTO', 'BARRIO / VEREDA','DIRECCION', 'TELEFONO', 'CELULAR', 'ID AFILIACION', 'SEMAFORO', 'ENCUESTA', 'CORREO', 'ID_MUNICIPIO', 'ID_LOCALIDAD', 'ID_BARRIO', 'ID_UNICO', 'id'],
                colModel: [
                    { name: 'total', index: 'total', width: anchoP(ancho, 3) },
                    { name: 'tipo_id', index: 'tipo_id', width: anchoP(ancho, 5) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 10) },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 20) },
                    { name: 'localidad', index: 'localidad', width: anchoP(ancho, 20) },
                    { name: 'barrio', index: 'barrio',width: anchoP(ancho, 20) },
                    { name: 'direccion', index: 'direccion',  width: anchoP(ancho, 20) },
                    { name: 'telefono', index: 'telefono',  width: anchoP(ancho, 20) },
                    { name: 'celular', index: 'celular',  width: anchoP(ancho, 20) },
                    { name: 'id_afiliacion', index: 'id_afiliacion',  hidden: true },  
                    { name: 'semaforo', index: 'semaforo',  width: anchoP(ancho, 15) }, 
                    { name: 'encuesta', index: 'encuesta',  width: anchoP(ancho, 10) },
                    { name: 'correo', index: 'correo',  hidden: true},
                    { name: 'id_municipio', index: 'id_municipio',  hidden: true },
                    { name: 'id_localidad', index: 'id_localidad',  hidden: true},
                    { name: 'id_barrio', index: 'id_barrio',  hidden: true},
                    { name: 'id_unico', index: 'id_unico',  hidden: true},
                    { name: 'id', index: 'id',  hidden: true },
        
                ],
        
                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho - 60,
                multiselect: true,
                multiboxonly: true,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        
                        asignaAtributo('txtTipoId', datosRow.tipo_id, 0);
                        asignaAtributo('txtIdentificacion', datosRow.identificacion, 0);
                        asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                        asignaAtributo('txtTelefono', datosRow.telefono, 0);
                        asignaAtributo('txtCelular', datosRow.celular, 0);
                        asignaAtributo('txtCorreo', datosRow.correo, 0);
                        asignaAtributo('cmbMunicipioModelo', datosRow.id_municipio, 0); 
                        asignaAtributo('cmbLocalidadModelo', datosRow.id_localidad, 0); 
                        asignaAtributo('cmbBariioModelo', datosRow.id_barrio, 0);   
                        asignaAtributo('txtDireccion', datosRow.direccion, 0);  
                        asignaAtributo('txtunico', datosRow.id_unico, 0);  
                        asignaAtributo('txtIdPaciente', datosRow.id, 0); 
                                                   
                                                   
                    },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        
            }
        }
    });         

       
}

function buscarGrillaMapa(arg){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {
        case 'listAreasRerefencias':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=800&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdArea'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Orden', 'latitud', 'longitud'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'Orden', index: 'Orden', width: anchoP(ancho, 20) },
                    { name: 'latitud', index: 'latitud', width: anchoP(ancho, 40) },
                    { name: 'longitud', index: 'longitud', width: anchoP(ancho, 40) },
                ],
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtOrdenReferencia', datosRow.Orden, 0);
                    asignaAtributo('lblOrdenReferencia', datosRow.Orden, 0);
                    asignaAtributo('txtLatitudReferencia', datosRow.latitud, 0);
                    asignaAtributo('txtLongitudReferencia', datosRow.longitud, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listAreasRerefenciasMarcadores':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=843&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdArea'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id', 'latitud', 'longitud','id_tipo', 'tipo','icono','descripcion', 'id_estado', 'estado'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_marcador', index: 'id_marcador', width: anchoP(ancho, 5) },
                    { name: 'latitud', index: 'latitud', width: anchoP(ancho, 15) },
                    { name: 'longitud', index: 'longitud', width: anchoP(ancho, 15) },
                    { name: 'id_tipo', index: 'id_tipo', hidden: true },
                    { name: 'tipo', index: 'tipo', width: anchoP(ancho, 30) },
                    { name: 'icono', index: 'icono', width: anchoP(ancho, 10) },
                    { name: 'descripcion', index: 'descripcion', width: anchoP(ancho, 70) },
                    { name: 'id_estado', index: 'id_estado', hidden: true },
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 30) },
                ],
                height: "200",
                width: ancho,
                caption: "MARCADORES",
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('cmbTipoMarcador', datosRow.id_tipo, 0);
                    asignaAtributo('txtLatitudReferencia', datosRow.latitud, 0);
                    asignaAtributo('txtLongitudReferencia', datosRow.longitud, 0);
                    asignaAtributo('txtDescripcionMarcador', datosRow.descripcion, 0);
                    asignaAtributo('txtIdMarcador', datosRow.id_marcador, 0);

                    //----------------------------------------------------------
                    asignaAtributo('txtIdMarcador',datosRow.id_marcador,0);
                    asignaAtributo('txtDescripcion',datosRow.descripcion,0);
                    asignaAtributo('cmbIdEstadoMarcador',datosRow.id_estado,0);
                    //----------------------------------------------------------

                    volarhasta(datosRow.latitud,datosRow.longitud,datosRow.descripcion);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listMunicipioAreas':
            ancho =
                $("#drag" + ventanaActual.num)
                .find("#divContenido")
                .width() - '10%'; 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=987&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));
            add_valores_a_mandar(valorAtributo('txtId'));

        $('#drag' + ventanaActual.num).find("#" +  arg).jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['contador', 'id_municipio', 'Nombre del Municipio', 'latitud_muni', 'longitud_muni', 'ID de Area', 'Nombre del Area', 'Orden', 'latitud', 'longitud', 'zoom', 'color', 'color_opacidad', 'vigente', 'id_magia', 'id_localidad', 'id_barrio', 'Total Pacientes', 'Asignados', 'Adscritos', 'Gestionados'
            ],
            colModel: [
                { name: 'contador', index: 'contador', hidden: true },
                { name: 'id_municipio', index: 'id_municipio', hidden: true  },
                { name: 'nom_municipio', index: 'nom_municipio', hidden: true },
                { name: 'latitud_muni', index: 'latitud_muni', hidden: true },
                { name: 'longitud_muni', index: 'longitud_muni', hidden: true },
                { name: 'id_area', index: 'id_area', width: anchoP(ancho, 2) },
                { name: 'NombreArea', index: 'Nombre', width: anchoP(ancho, 10) },
                { name: 'Orden', index: 'Orden', hidden: true },
                { name: 'latitud', index: 'latitud', hidden: true },
                { name: 'longitud', index: 'longitud', hidden: true },
                { name: 'zoom', index: 'zoom', hidden: true },
                { name: 'color', index: 'color', hidden: true },
                { name: 'color_opacidad', index: 'color_opacidad', hidden: true },
                { name: 'vigente', index: 'vigente', hidden: true },
                { name: 'id_magia', index: 'id_magia', hidden: true },
                { name: 'id_localidad', index: 'id_localidad', hidden: true },
                { name: 'id_barrio', index: 'id_barrio', hidden: true },
                { name: 'totalAfiliados', index: 'totalAfiliados', width: anchoP(ancho, 5)  },
                { name: 'pacientesAsignados', index: 'pacienteAsignados', width: anchoP(ancho, 5)  },
                { name: 'pacienteAdscritos', index: 'pacienteAdscritos', width: anchoP(ancho, 5)  },
                { name: 'pacienteCaracterizados', index: 'pacienteCaracterizados', width: anchoP(ancho, 5)  },
            ],
            height: 200,
            width: ancho -60,
            caption: "AREAS",
            onSelectRow: function (rowid) {

                var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                asignaAtributo('txtMunicipio', datosRow.id_municipio + '-' + datosRow.nom_municipio, 0);
                asignaAtributo('lblIdArea', datosRow.id_area, 0);
                asignaAtributo('lblIdlatitud', datosRow.latitud, 0);
                asignaAtributo('lblIdlongitud', datosRow.longitud, 0);
                asignaAtributo('txtArea', datosRow.NombreArea, 0);
                asignaAtributo('txtOrden', datosRow.Orden, 0);
                asignaAtributo('txtLatitud', datosRow.latitud, 0);
                asignaAtributo('txtLongitud', datosRow.longitud, 0);
                asignaAtributo('lblIdZoom', datosRow.zoom, 0);
                asignaAtributo('txtColor', datosRow.color, 0);
                asignaAtributo('txtColorOpacidad', datosRow.color_opacidad, 0);
                asignaAtributo('cmbVigente', datosRow.vigente, 0);
                asignaAtributo('lblIdMagia', datosRow.id_magia, 0);
                asignaAtributo('cmbIdArea', datosRow.id_area, 0);
                asignaAtributo('cmbIdLoc', datosRow.id_localidad, 0);
                asignaAtributo('cmbIdBar', datosRow.id_barrio, 0);

                ubicarMapaMunicipioGrupo();
                setTimeout("buscarGrillaMapa('listAreasRerefencias')", 1000);

            },
        });     
        $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        break;

        case 'listaAreasMunicipiosMarcadores':

            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1025&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador','id_Mun','NombreMun','id_area', 'NombreArea', 'latitud', 'longitud', 'zoom', 'color', 'color_opacidad'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_Mun', index: 'id_Mun', hidden: true },
                    { name: 'NombreMun', index: 'NombreMun', hidden: true },
                    { name: 'id_area', index: 'id_area', width: anchoP(ancho, 5) },
                    { name: 'NombreArea', index: 'NombreArea', width: anchoP(ancho, 60) },
                    { name: 'latitud', index: 'latitud', width: anchoP(ancho, 10) },
                    { name: 'longitud', index: 'longitud', width: anchoP(ancho, 10) },
                    { name: 'zoom', index: 'zoom', hidden: true },
                    { name: 'color', index: 'color', hidden: true },
                    { name: 'color_opacidad', index: 'color_opacidad', hidden: true },

                ],
                height: "200",
                width: ancho,
                caption: "AREAS",
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtMunicipio', datosRow.id_Mun+ '-' + datosRow.NombreMun, 0);
                    asignaAtributo('lblIdArea', datosRow.id_area, 0);
                    asignaAtributo('txtArea', datosRow.NombreArea, 0);
                    asignaAtributo('lblIdlatitud', datosRow.latitud, 0);
                    asignaAtributo('lblIdlongitud', datosRow.longitud, 0);
                    asignaAtributo('lblIdZoom', datosRow.zoom, 0);
                    asignaAtributo('txtColor', datosRow.color, 0);
                    asignaAtributo('txtColorOpacidad', datosRow.color_opacidad, 0);


                    ubicarMapaMunicipio()
                   setTimeout("buscarGrillaMapa('listAreasRerefenciasMarcadores')", 1000)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

    }
}

function ubicarMapaMunicipioGrupo() {
    cargaPuntoLatitudLongitudGrupo('lblLatitudMuni', 'lblLongitudMuni', 'lblIdArea', valorAtributo('lblIdArea'), 846)
}

function cargaPuntoLatitudLongitudGrupo(idLabelDestino1, idLabelDestino2, idLabelDestino3, condicion1, idQueryCombo) {
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaPuntoLatitudLongitudGrupo(idLabelDestino1, idLabelDestino2, idLabelDestino3) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaPuntoLatitudLongitudGrupo(idLabelDestino1, idLabelDestino2, idLabelDestino3) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino1);
            limpiaAtributo(idLabelDestino2);
            limpiaAtributo(idLabelDestino3);

            asignaAtributo(idLabelDestino1, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(idLabelDestino2, raiz.getElementsByTagName('nom')[0].firstChild.data)
            asignaAtributo(idLabelDestino3, raiz.getElementsByTagName('title')[0].firstChild.data)

            centroPuntoDelMapaGrupo()

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function centroPuntoDelMapaGrupo() { 
    recargarMapa()
    var estiloPopup = { 'maxWidth': '300' }
    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [5, 8], popupAnchor: [2, -6] }});

    mymap = L.map('mapaGeoArea').setView([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], valorAtributo('lblIdZoom'));
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

    mymap.on('click', function (e) {
        asignaAtributo('txtLatitudReferencia', e.latlng.lat.toString(), 0);
        asignaAtributo('txtLongitudReferencia', e.latlng.lng.toString(), 0);
    });

    urlIconoCentroPunto = 'https://image.flaticon.com/icons/png/128/291/291236.png'

    L.marker([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup('Punto buscado', estiloPopup).addTo(mymap);

    cargaLineasDelAreaGrupo(valorAtributo('txtId'), 1325) // txtidgrupo 1017
}

function cargaLineasDelAreaGrupo(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaLineasDelAreaGrupo(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaLineasDelAreaGrupo(condicion1) {
    
    var iconoBase = L.Icon.extend({ options: { iconSize: [35, 35],  iconAnchor: [5, 8], popupAnchor: [2, -6] } });
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            totalRegistros = raiz.getElementsByTagName('id').length
            var polygon = [];
            contador = 0;
            puntos = 0;
            for (i = 0; i < totalRegistros; i++) {
                if(i === 0){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                else {
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    contador = contador +1
                    polygon.push(i-puntos);
                    puntos=i;    
                }
                if( i === totalRegistros-1){
                    contador = contador+1
                    polygon.push(i-puntos+1)
                }
            }
            ban = 0;
            k = 0;
            for ( j = 0; j < contador; j++){
                var subPolygonPoints = Array.from(Array(polygon[j]), () => new Array(2));
                for (i = ban; i < totalRegistros; i++) {
                    
                    if(i === 0 || i === ban-1){
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    }
                    if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                        latitud = raiz.getElementsByTagName('id')[i].firstChild.data
                        longitud = raiz.getElementsByTagName('nom')[i].firstChild.data
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        subPolygonPoints[k][0] = latitud
                        subPolygonPoints[k][1] = longitud
                       
                       // fabricaMarcadorGrupo(latitud, longitud, titulo, 'Lat=' + latitud + '  lon=' + longitud, mymap)
                        k = k + 1;
                    }
                    else {
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        L.polygon(
                            subPolygonPoints,
                            {
                            weight: 2, /*grosor de la linea*/
                            lineJoin: "miter",
                            color: "blue",
                            fillColor: '#f03',  /* color de llenar fillColor: "none",*/
                            fillOpacity: 0.05
                            }
                        ).addTo(mymap)
                        ban = i;
                        k = 0;
                        break;
                    }
                    if( i === totalRegistros -1 ){
                        L.polygon(
                            subPolygonPoints,
                            {
                                weight: 2, /*grosor de la linea*/
                                lineJoin: "miter",
                                color: "blue",
                                fillColor: '#f03',  /* color de llenar fillColor: "none",*/
                                fillOpacity: 0.05
                            }
                        ).addTo(mymap)
                        break;
                    }
                }
            }
            cargaMarcadoresDelAreaGrupo(valorAtributo('lblIdArea'), 843) 
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}
function fabricaMarcadorGrupo(latitud, longitud, titulo, textoPopup, mymap) {
    var estiloPopup = { 'maxWidth': '100' }
    urlIconoCentroPunto = 'http://190.60.242.160:8374/clinica/utilidades/imagenes/marcadores/vertice.png'
    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15],  iconAnchor: [5, 8], popupAnchor: [2, -6] } });

    var textoPopup = '<b>' + titulo + '</b><br/>' + textoPopup;

    L.marker([latitud, longitud], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup(textoPopup, estiloPopup).addTo(mymap);
}
function cargaMarcadoresDelAreaGrupoFamilia(condicion1, condicion2, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaMarcadoresDelAreaGrupo(condicion2) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function cargaMarcadoresDelAreaGrupo(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaMarcadoresDelAreaGrupo(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaMarcadoresDelAreaGrupo(condicion1) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;

            var estiloPopup = { 'maxWidth': '300' }
            var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15],  iconAnchor: [5, 8], popupAnchor: [2, -6] } });


            for (i = 0; i < raiz.getElementsByTagName('id').length; i++) {

                marcador = raiz.getElementsByTagName('id')[i].firstChild.data
                marcador_tipo = raiz.getElementsByTagName('nom')[i].firstChild.data

                var m = marcador.split('-_');
                var id_marcador = m[0];
                var latitud = m[1];
                var longitud = m[2];
                var descripcion = m[3];

                var mt = marcador_tipo.split('-_');
                var id_marcador_tipo = mt[0];

                var textoPopup = '<b>' + m[0] + ' - ' + mt[1] + '</b><br/>' + m[3];
                var urlIcono = mt[2];
                

                L.marker([latitud, longitud], { icon: new iconoBase({ iconUrl: urlIcono }) }).bindPopup(textoPopup, estiloPopup).addTo(mymap);
                                
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}


function cargarTableGrupoEms() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", "/clinica/paginas/adminModelo/tabsGruposEms.jsp", true);
    varajaxMenu.onreadystatechange = llenarcargarTableGrupoEms;
    varajaxMenu.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarTableGrupoEms() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaGruposEms").innerHTML = varajaxMenu.responseText;              
            $("#tabsParametrosGrupos").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
            buscarUsuarioGrupo('listGrillaGruposPersonal');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) { swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');}
}

function traerVentanitaPersonal(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/adminModelo/ventanitaPersonal.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaPersonal(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaPersonal(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function buscarPersonal(arg){
    pag = "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp";

            limpiarDivEditarJuan(arg);
            pag = "/clinica/paginas/accionesXml/buscarGrillaVentana_xml.jsp";
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar =
                valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo("txtCodBus"));
            add_valores_a_mandar(valorAtributo("txtNomBus"));

            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .jqGrid({
                    url: valores_a_mandar,
                    datatype: "xml",
                    mtype: "GET",
                    colNames: ["contador", "ID", "NOMBRE"],
                    colModel: [
                        { name: "contador", index: "contador", hidden: true },
                        { name: "ID", index: "ID", width: anchoP(ancho, 10) },
                        { name: "NOMBRE", index: "NOMBRE", width: anchoP(ancho, 70) },
                    ],
                    height: 200,
                    width: ancho,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery("#drag" + ventanaActual.num)
                            .find("#" + arg)
                            .getRowData(rowid);
                        estad = 0;
                        asignaAtributo(
                            elemInputDestino,
                            concatenarCodigoNombre(datosRow.ID, datosRow.NOMBRE),
                            0
                        );
                        ocultar("divVentanitaPuesta");
                    },
                });
            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .setGridParam({ url: valores_a_mandar })
                .trigger("reloadGrid");
           
}
function cargaPuntoMunicipio() {  //  alert(  condicion1+'::'+idQueryCombo )
    var id = valorAtributoIdAutoCompletar('txtMunicipio'); 
    var url = 'http://10.0.0.3:9010/referencia/'+id
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {	
            asignaAtributo('lblIdlatitud',object[0],0);
            asignaAtributo('lblIdlongitud',object[1],0);
            asignaAtributo('lblIdZoom',object[2],0);             								
        }
    });

    setTimeout(()=>{
        centroPuntoDelMapaMunicipio();
        setTimeout(()=>{
            estadisticosGrupos(id);
            setTimeout(estadisticoMarcadores(id), 300);
        }, 400);
    }, 1000);
    
}

function centroPuntoDelMapaMunicipio() { 
    recargarMapa()
    var estiloPopup = { 'maxWidth': '300' }
    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15],  iconAnchor: [5, 8], popupAnchor: [2, -6] } });    

    mymap = L.map('mapaGeoArea').setView([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], valorAtributo('lblIdZoom'));
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

    mymap.on('click', function (e) {
        asignaAtributo('txtLatitudReferencia', e.latlng.lat.toString(), 0);
        asignaAtributo('txtLongitudReferencia', e.latlng.lng.toString(), 0);
    });

    urlIconoCentroPunto = 'https://image.flaticon.com/icons/png/128/291/291236.png'

    L.marker([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup('Punto buscado', estiloPopup).addTo(mymap);

    cargaAreasMunicipio(valorAtributoIdAutoCompletar('txtMunicipio'), 1360) // txtidgrupo 1017
}

function cargaAreasMunicipio(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaAreasMunicipio(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaAreasMunicipio(condicion1) {
    console.log('aqui vamos');
    var iconoBase = L.Icon.extend({ options: { iconSize: [35, 35],  iconAnchor: [5, 8], popupAnchor: [2, -6] } });
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            totalRegistros = raiz.getElementsByTagName('id').length
            var polygon = [];
            contador = 0;
            puntos = 0;
            for (i = 0; i < totalRegistros; i++) {
                if(i === 0){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                else {
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    contador = contador +1
                    polygon.push(i-puntos);
                    puntos=i;    
                }
                if( i === totalRegistros-1){
                    contador = contador+1
                    polygon.push(i-puntos+1)
                }
            }
            ban = 0;
            k = 0;
            for ( j = 0; j < contador; j++){
                var subPolygonPoints = Array.from(Array(polygon[j]), () => new Array(2));
                for (i = ban; i < totalRegistros; i++) {
                    
                    if(i === 0 || i === ban-1){
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    }
                    if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                        pol = raiz.getElementsByTagName('id')[i].firstChild.data
                        var p = pol.split('-_');
                        var latitud = p[0];
                        var longitud = p[1];
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        subPolygonPoints[k][0] = latitud
                        subPolygonPoints[k][1] = longitud
                       
                       // fabricaMarcadorGrupo(latitud, longitud, titulo, 'Lat=' + latitud + '  lon=' + longitud, mymap)
                        k = k + 1;
                    }
                    else {
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        var colorrelleno = raiz.getElementsByTagName('nom')[i].firstChild.data
                        L.polygon(
                            subPolygonPoints,
                            {
                            weight: 2, /*grosor de la linea*/
                            lineJoin: "miter",
                            color: "blue",
                            fillColor: "#" + colorrelleno,  /* color de llenar fillColor: "none",*/
                            fillOpacity: 0.2
                            }
                        ).addTo(mymap)
                        ban = i;
                        k = 0;
                        break;
                    }
                    if( i === totalRegistros -1 ){
                        L.polygon(
                            subPolygonPoints,
                            {
                                weight: 2, /*grosor de la linea*/
                                lineJoin: "miter",
                                color: "blue",
                                fillColor: "#" + colorrelleno,  /* color de llenar fillColor: "none",*/
                                fillOpacity: 0.05
                            }
                        ).addTo(mymap)
                        break;
                    }
                }
            }
            
            if (valorAtributo('lblIdGeo')==0){
                cargaMarcadoresDelAreaGrupoFamilia(valorAtributo('lblMarcador'), $("#cmbIdMun").val(), 3206) 

            }else if(valorAtributo('lblIdGeo')==1){
                cargaMarcadoresDelAreaGrupo(valorAtributoIdAutoCompletar('txtMunicipio'), 3209) 
            }
            

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function buscarParametrosModelo(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {

        case 'listGrupo':
                ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=992" + "&parametros=";          
                add_valores_a_mandar(valorAtributo('txtBusNombre'));
                add_valores_a_mandar(valorAtributo('txtBusNombre'));
                add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
                add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
                add_valores_a_mandar(valorAtributo('txtBusIdGrupo'));
                add_valores_a_mandar(valorAtributo('txtBusIdGrupo'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                
    
                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['TOTAL', 'ID', 'NOMBRE GRUPO','ID_MUNICIPIO', 'MUNICIPIO', 'PERSONA RESPONSABLE', 'VIGENTE'
                    ],
                    colModel: [
                        { name: 'TOTAL', index: 'TOTAL', hidden: true },
                        { name: 'ID', index: 'ID',  width: anchoP(ancho, 10) },
                        { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 50) },
                        { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                        { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 50)},
                        { name: 'RESPONSABLE', index: 'RESPONSABLE', width: anchoP(ancho, 40) },
                        { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 10) }
                        
                    ],
                    height: 335,
                    width: ancho,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        asignaAtributo('txtId', datosRow.ID, 0)
                        asignaAtributo('txtNombre', datosRow.NOMBRE, 0)
                        asignaAtributo('cmbIdMun', datosRow.ID_MUNICIPIO, 0)
                        asignaAtributo('txtMun', datosRow.MUNICIPIO, 0)
                        asignaAtributo('txtNombre2', datosRow.RESPONSABLE, 0)
                        asignaAtributo('cmbVigente', datosRow.VIGENTE, 0)

                        cargarTableGrupoEms();

                    //  $("#tabGestionGruposIntegrantes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        
                        
    
                    }
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
                break;

        case 'listFamilias':
                ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=2536" + "&parametros=";          
                add_valores_a_mandar(valorAtributo('txtIdFamilia'));
                add_valores_a_mandar(valorAtributo('txtIdFamilia'));
                add_valores_a_mandar(valorAtributo('txtNombreFamilia'));
                add_valores_a_mandar(valorAtributo('txtNombreFamilia'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('cmbIdLoc'));
                add_valores_a_mandar(valorAtributo('cmbIdLoc'));
                add_valores_a_mandar(valorAtributo('cmbIdBar'));
                add_valores_a_mandar(valorAtributo('cmbIdBar'));
                
    
                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['TOTAL', 'ID', 'NOMBRE FAMILIA','ID_MUNICIPIO', 'MUNICIPIO', 'ID_LOCALIDAD', 'LOCALIDAD', 'ID_BARRIO', 'BARRIO', 'DIRECCION', 'id_marcador', 'id_cabeza', 'INTEGRANTES', 'id_lugar'
                    ],
                    colModel: [
                        { name: 'TOTAL', index: 'TOTAL', hidden: true },
                        { name: 'ID', index: 'ID',  width: anchoP(ancho, 10) },
                        { name: 'NOMBRE_FAMILIA', index: 'NOMBRE_FAMILIA', width: anchoP(ancho, 50) },
                        { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                        { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 30)},
                        { name: 'ID_LOCALIDAD', index: 'ID_LOCALIDAD', hidden: true },
                        { name: 'LOCALIDAD', index: 'LOCALIDAD', width: anchoP(ancho, 30) },
                        { name: 'ID_BARRIO', index: 'ID_BARRIO', hidden: true },
                        { name: 'BARRIO', index: 'BARRIO', width: anchoP(ancho, 30)},
                        { name: 'DIRECCION', index: 'DIRECCION', width: anchoP(ancho, 30)},
                        { name: 'id_marcador', index: 'id_marcador', hidden: true },
                        { name: 'id_cabeza', index: 'id_cabeza', hidden: true },
                        { name: 'INTEGRANTES', index: 'INTEGRANTES', width: anchoP(ancho, 20) },
                        { name: 'id_lugar', index: 'id_lugar', hidden: true },
                    ],
                    height: 335,
                    width: ancho,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        asignaAtributo('txtId', datosRow.ID, 0)
                        asignaAtributo('txtNombre', datosRow.NOMBRE_FAMILIA, 0)
                        asignaAtributo('cmbIdMun', datosRow.ID_MUNICIPIO, 0)
                        asignaAtributo('txtMun', datosRow.MUNICIPIO, 0)
                        asignaAtributo('txtDireccion', datosRow.DIRECCION, 0)
                        asignaAtributoCombo('cmbIdLoc', datosRow.ID_LOCALIDAD, datosRow.LOCALIDAD)
                        asignaAtributoCombo('cmbIdBar', datosRow.ID_BARRIO, datosRow.BARRIO)
                        asignaAtributo('lblMarcador', datosRow.id_marcador, 0)
                        asignaAtributo('lblCabeza', datosRow.id_cabeza, 0)
                        asignaAtributo('lblIdLugar', datosRow.id_lugar, 0)
                        

                        cargarTabsNucleoFamiliar();

                    //  $("#tabGestionGruposIntegrantes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        
                        
    
                    }
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
                break;
        
        case 'listLugares':
                ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                                
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=2550" + "&parametros=";          
                add_valores_a_mandar(valorAtributo('txtBusIdVivienda'));
                add_valores_a_mandar(valorAtributo('txtBusIdVivienda'));
                add_valores_a_mandar(valorAtributo('txtBusDireccion'));
                add_valores_a_mandar(valorAtributo('txtBusDireccion'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                
    
                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['TOTAL', 'ID','ID_MUNICIPIO', 'MUNICIPIO', 'ID_LOCALIDAD', 'LOCALIDAD', 'ID_BARRIO', 'BARRIO', 'DIRECCION', 'id_marcador'
                    ],
                    colModel: [
                        { name: 'TOTAL', index: 'TOTAL', hidden: true },
                        { name: 'ID', index: 'ID',  width: anchoP(ancho, 10) },
                        { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                        { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 30)},
                        { name: 'ID_LOCALIDAD', index: 'ID_LOCALIDAD', hidden: true },
                        { name: 'LOCALIDAD', index: 'LOCALIDAD', width: anchoP(ancho, 30) },
                        { name: 'ID_BARRIO', index: 'ID_BARRIO', hidden: true },
                        { name: 'BARRIO', index: 'BARRIO', width: anchoP(ancho, 30)},
                        { name: 'DIRECCION', index: 'DIRECCION', width: anchoP(ancho, 30)},
                        { name: 'id_marcador', index: 'id_marcador', hidden: true },
                    ],
                    height: 324,
                    width: ancho-2,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        asignaAtributo('txtId', datosRow.ID, 0)
                        asignaAtributo('cmbIdMun', datosRow.ID_MUNICIPIO, 0)
                        asignaAtributo('txtMun', datosRow.MUNICIPIO, 0)
                        asignaAtributo('txtDireccion', datosRow.DIRECCION, 0)
                        asignaAtributoCombo('cmbIdLoc', datosRow.ID_LOCALIDAD, datosRow.LOCALIDAD)
                        asignaAtributoCombo('cmbIdBar', datosRow.ID_BARRIO, datosRow.BARRIO)
                        asignaAtributo('lblMarcador', datosRow.id_marcador, 0)

                        cargarTabsViviendas();                        

                    //  $("#tabGestionGruposIntegrantes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' })                       
    
                    }
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
                break;

        case 'listViviendas':

                ancho = ($('#drag' + ventanaActual.num).find("#divViviendas").width());
                
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=2552" + "&parametros=";          
                add_valores_a_mandar(valorAtributo('txtId'));               
    
                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['TOTAL', 'ID','DESCRIPCION','ENCUESTA', 'responde', 'CEDULA','PARENTESCO', 'r1', 'r2', 'r3', 'r4', 'r5', 'r6', 'r7', 'r8', 'r9', 'r10', 'r11', 'r12', 'r13', 'r14', 'r15', 'r16', 'r17', 'fecha'
                    ],
                    colModel: [
                        { name: 'TOTAL', index: 'TOTAL', hidden: true },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
                        { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 10)},
                        { name: 'ENCUESTA', index: 'ENCUESTA', width: anchoP(ancho, 5)},
                        { name: 'responde', index: 'responde', hidden: true },
                        { name: 'CEDULA', index: 'CEDULA', hidden: true },
                        { name: 'PARENTESCO', index: 'PARENTESCO', hidden: true },
                        { name: 'r1', index: 'r1', hidden: true },
                        { name: 'r2', index: 'r2', hidden: true },
                        { name: 'r3', index: 'r3', hidden: true },
                        { name: 'r4', index: 'r4', hidden: true },
                        { name: 'r5', index: 'r5', hidden: true },
                        { name: 'r6', index: 'r6', hidden: true },
                        { name: 'r7', index: 'r7', hidden: true },
                        { name: 'r8', index: 'r8', hidden: true },
                        { name: 'r9', index: 'r9', hidden: true },
                        { name: 'r10', index: 'r10', hidden: true },
                        { name: 'r11', index: 'r11', hidden: true },
                        { name: 'r12', index: 'r12', hidden: true },
                        { name: 'r13', index: 'r13', hidden: true },
                        { name: 'r14', index: 'r14', hidden: true },
                        { name: 'r15', index: 'r15', hidden: true },
                        { name: 'r16', index: 'r16', hidden: true },
                        { name: 'r17', index: 'r17', hidden: true },
                        { name: 'fecha', index: 'fecha', hidden: true },

                    ],
                    height: 150,
                    width: ancho,
                    caption: "Viviendas",
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);lblExisteEncuesta
                        asignaAtributo('txtIdVivienda', datosRow.ID, 0)
                        asignaAtributo('txtDescripccion', datosRow.DESCRIPCION, 0)
                        asignaAtributo('lblExisteEncuesta', datosRow.ENCUESTA, 0)
                        asignaAtributo('txtNombreR', datosRow.responde, 0) 
                        asignaAtributo('txtCedulaR', datosRow.CEDULA, 0) 

                        asignaAtributo('cmbVr2', datosRow.r1, 0)
                        asignaAtributo('cmbVr3', datosRow.r2, 0)  
                        asignaAtributo('cmbVr4', datosRow.r3, 0)  
                        asignaAtributo('cmbVr5', datosRow.r4, 0)  
                        asignaAtributo('cmbVr6', datosRow.r5, 0)  
                        asignaAtributo('cmbVr7', datosRow.r6, 0)  
                        activarSwitchVivienda('encf_v', 'r8')
                        if (datosRow.r7 == 'SI'){
                            document.getElementById('formulario.encf_v.r8').checked = true
                            document.getElementById('formulario.encf_v.r8').value = 'SI'   
                        }
                        if (datosRow.r7 == 'NO'){
                            document.getElementById('formulario.encf_v.r8').checked = false   
                            document.getElementById('formulario.encf_v.r8').value = 'NO'   
                        }
                        asignaAtributo('cmbVr9', datosRow.r8, 0)
                        activarSwitchVivienda('encf_v', 'r10')    
                        if (datosRow.r9 == 'SI'){
                            document.getElementById('formulario.encf_v.r10').checked = true  
                            document.getElementById('formulario.encf_v.r10').value = 'SI'    
                        }
                        if (datosRow.r9 == 'NO'){
                            document.getElementById('formulario.encf_v.r10').checked = false  
                            document.getElementById('formulario.encf_v.r10').value = 'NO'   
                        }
                        activarSwitchVivienda('encf_v', 'r11')
                        if (datosRow.r10 == 'SI'){
                            document.getElementById('formulario.encf_v.r11').checked = true   
                            document.getElementById('formulario.encf_v.r11').value = 'SI'   
                        }
                        if (datosRow.r10 == 'NO'){
                            document.getElementById('formulario.encf_v.r11').checked = false
                            document.getElementById('formulario.encf_v.r11').value = 'NO'  
                        }
                        activarSwitchVivienda('encf_v', 'r12')
                        if (datosRow.r11 == 'SI'){
                            document.getElementById('formulario.encf_v.r12').checked = true 
                            document.getElementById('formulario.encf_v.r12').value = 'SI'     
                        }
                        if (datosRow.r11 == 'NO'){
                            document.getElementById('formulario.encf_v.r12').checked = false 
                            document.getElementById('formulario.encf_v.r12').value = 'NO'    
                        }
                        activarSwitchVivienda('encf_v', 'r13')
                        if (datosRow.r12 == 'SI'){
                            document.getElementById('formulario.encf_v.r13').checked = true   
                            document.getElementById('formulario.encf_v.r13').value = 'SI'   
                        }
                        if (datosRow.r12 == 'NO'){
                            document.getElementById('formulario.encf_v.r13').checked = false
                            document.getElementById('formulario.encf_v.r13').value = 'NO'  
                        }   
                        asignaAtributo('cmbVr14', datosRow.r13, 0)  
                        asignaAtributo('cmbVr15', datosRow.r14, 0)  
                        activarSwitchVivienda('encf_v', 'r16')
                        if (datosRow.r15 == 'SI'){
                            document.getElementById('formulario.encf_v.r16').checked = true  
                            document.getElementById('formulario.encf_v.r16').value = 'SI'    
                        }
                        if (datosRow.r15 == 'NO'){
                            document.getElementById('formulario.encf_v.r16').checked = false 
                            document.getElementById('formulario.encf_v.r16').value = 'NO'  
                        }
                        activarSwitchVivienda('encf_v', 'r17')
                        if (datosRow.r16 == 'SI'){
                            document.getElementById('formulario.encf_v.r17').checked = true  
                            document.getElementById('formulario.encf_v.r17').value = 'SI'    
                        }
                        if (datosRow.r16 == 'NO'){
                            document.getElementById('formulario.encf_v.r17').checked = false  
                            document.getElementById('formulario.encf_v.r17').value = 'NO'  
                        }
                        activarSwitchVivienda('encf_v', 'r18')
                        if (datosRow.r17 == 'SI'){
                            document.getElementById('formulario.encf_v.r18').checked = true   
                            document.getElementById('formulario.encf_v.r18').value = 'SI'   
                        }
                        if (datosRow.r17 == 'NO'){
                            document.getElementById('formulario.encf_v.r18').checked = false 
                            document.getElementById('formulario.encf_v.r18').value = 'NO'  
                        }

                        console.log(datosRow);
                        if(datosRow.ENCUESTA === 'SI'){
                            $("#btnGuardadEncuestaViv").hide();                            
                        }else{
                            $("#btnActualizarEncuesta").hide();
                        }

                        setTimeout(buscarParametrosModelo('listFamiliasVivienda'), 500);

                    //  $("#tabGestionGruposIntegrantes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        
                    }
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
                break;

        case 'listFamiliasVivienda':
                ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=2553" + "&parametros=";          
                add_valores_a_mandar(valorAtributo('txtIdVivienda'));               
    
                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
                    colNames: ['TOTAL', 'ID','DESCRIPCION', 'TOTAL INTEGRANTES'
                    ],
                    colModel: [
                        { name: 'TOTAL', index: 'TOTAL', hidden: true },
                        { name: 'ID', index: 'ID', width: anchoP(ancho, 5) },
                        { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 10)},
                        { name: 'TOTAL_INTEGRANTES', index: 'TOTAL_INTEGRANTES', width: anchoP(ancho, 5) },
                    ],
                    height: 150,
                    autowidth: true,
                    caption: "Familias",
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);                

                    //  $("#tabGestionGruposIntegrantes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        
                    }
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
                break;
    }
}

function modificarCRUDParametrosModelo(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

        switch (arg) {  

            case 'crearGrupo':
                if (verificarCamposGuardar(arg)) {   
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=997&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'));
                    add_valores_a_mandar(valorAtributo('cmbIdMun'));
                    add_valores_a_mandar(valorAtributo('txtNombre2'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));       
                    ajaxModificar();   
                }

                buscarParametrosModelo('listGrupo')
                    break;

            case 'modificarGrupo':
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=998&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombre'));
                    add_valores_a_mandar(valorAtributo('cmbIdMun'));
                    add_valores_a_mandar(valorAtributo('txtNombre2'));
                    add_valores_a_mandar(valorAtributo('cmbVigente'));
                    add_valores_a_mandar(valorAtributo('txtId'));
                    ajaxModificar(); 

                    buscarParametrosModelo('listGrupo')

                    break;

            case 'asignarPersonalGrupo':
                
                    if (verificarCamposGuardar(arg)) {   
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=1000&parametros=";
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                        add_valores_a_mandar(valorAtributo('cmbIdRol'));
                        ajaxModificar();  
                    }
                   
                    break;

            case 'asignarPersonalGrupoVentana':
                
                    if (verificarCamposGuardar(arg)) {   
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=1000&parametros=";
                        add_valores_a_mandar(valorAtributo('txtId'));
                        add_valores_a_mandar(valorAtributo('txtIdPersonal').split("-",1));
                        add_valores_a_mandar(valorAtributo('cmbIdRol'));
                        ajaxModificar();  
                    }
                    buscarUsuarioGrupo('listGrillaGruposPersonal');
                    break;

            case 'eliminarPersonalGrupo':
                if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=984&parametros=";              
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        add_valores_a_mandar(valorAtributo('txtIdentificacion'));                        
                        ajaxModificar();
                }
            
                        
                    break;

            case 'eliminarPersonalGrupoVentana':

                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=984&parametros=";              
                        add_valores_a_mandar(valorAtributo('txtId'));
                        add_valores_a_mandar(valorAtributo('txtIdPersonal').split("-",1));                        
                        ajaxModificar();
                    
                    buscarUsuarioGrupo('listGrillaGruposPersonal');     
                    break;

            case 'asignarPacienteGrupo':
                
                    if (verificarCamposGuardar(arg)) {   
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=1007&parametros=";
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        add_valores_a_mandar(valorAtributo('txtIdPaciente'));
                        
                        ajaxModificar();
                       
                        }
                    
                    break;
        
            case 'adcribirPacienteGrupo':
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=1008&parametros="; 
                        add_valores_a_mandar(valorAtributo('txtIdPaciente'));             
                        add_valores_a_mandar(valorAtributo('cmbIdGrupo'));
                        ajaxModificar();
        
                    //buscarUsuario('listGrillaGruposMulti');
                                    
                    break;

            case 'asignarAreaGrupo':            
                    if (valorAtributo('cmbIdBar') == ''){
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=2562&parametros=";
                        add_valores_a_mandar(valorAtributo('txtId'));
                        add_valores_a_mandar(valorAtributo('cmbIdLoc'));
                        add_valores_a_mandar(valorAtributo('cmbIdArea'));
                        
                        ajaxModificar();
                        
                    }else {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=988&parametros=";
                        add_valores_a_mandar(valorAtributo('txtId'));
                        add_valores_a_mandar(valorAtributo('cmbIdLoc'));
                        add_valores_a_mandar(valorAtributo('cmbIdBar'));
                        add_valores_a_mandar(valorAtributo('cmbIdArea'));
                        
                        ajaxModificar();
                        
                    }                          
                                           
                    break;

            case 'eliminarAreaGrupo':
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=989&parametros=";              
                        add_valores_a_mandar(valorAtributo('lblIdMagia'));
                        ajaxModificar();
        
                    buscarGrillaMapa('listMunicipioAreas');
                                    
                    break;

            case 'modificarAreaGrupo':
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=1019&parametros=";
                        add_valores_a_mandar(valorAtributo('cmbIdLoc'));
                        add_valores_a_mandar(valorAtributo('cmbIdBar'));
                        add_valores_a_mandar(valorAtributo('cmbIdArea'));
                        add_valores_a_mandar(valorAtributo('lblIdMagia'));  
                        add_valores_a_mandar(valorAtributo('txtId'));
                        ajaxModificar(); 

                    buscarGrillaMapa('listMunicipioAreas');
                    
                    break;
            
            case 'modificarPersonalDesdeModelo':
                    if (verificarCamposGuardar(arg)) {   
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=2533&parametros=";
                        add_valores_a_mandar(valorAtributo('txtCorreo'));
                        add_valores_a_mandar(valorAtributo('cmbSede'));
                        add_valores_a_mandar(valorAtributo('txtPhone'));
                        add_valores_a_mandar(valorAtributo('cmbEstado'));  
                        add_valores_a_mandar(valorAtributo('txtIdentificacion'));
                        ajaxModificar(); 

                    }

                    break;

            case 'crearFamilia':
                
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2535&parametros=";
                    add_valores_a_mandar(valorAtributo('txtNombreFamilia'));
                    add_valores_a_mandar(valorAtributo('txtIdVivienda'));

         
                    ajaxModificar();   
               
                    break;

            case 'modificarFamilia':
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2542&parametros=";
                    add_valores_a_mandar(valorAtributo('txtIdFamilia'));
                    add_valores_a_mandar(valorAtributo('txtNombreFamilia'));
                    add_valores_a_mandar(valorAtributo('txtIdVivienda'));
                    ajaxModificar(); 

                //buscarParametrosModelo('listFamilias')

                    break;

            case 'eliminarFamilia':
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2557&parametros=";
                    add_valores_a_mandar(valorAtributo('txtIdFamilia'));

                    ajaxModificar(); 

                    break;

            case 'asignarFamiliarVentana':

                if ($('#sw_cabeza_familia').attr('checked')) {
                    if (verificarCamposGuardar(arg)) {   
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=2544&parametros=";
                        add_valores_a_mandar(valorAtributo('txtId'));
                        add_valores_a_mandar(valorAtributo('txtIdPaciente').split("-",1));
                        add_valores_a_mandar(valorAtributo('cmbIdRol'));
                        ajaxModificar();  
                    }
                } else {
                    if (verificarCamposGuardar(arg)) {   
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=2537&parametros=";
                        add_valores_a_mandar(valorAtributo('txtId'));
                        add_valores_a_mandar(valorAtributo('txtIdPaciente').split("-",1));
                        add_valores_a_mandar(valorAtributo('cmbIdRol'));
                        ajaxModificar();  
                    }
                }
                buscarFamiliares('listGrillaFamiliares');
                    break;

            case 'eliminarFamiliarVentana':

                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=2538&parametros=";              
                        add_valores_a_mandar(valorAtributo('txtId'));
                        add_valores_a_mandar(valorAtributo('txtIdPaciente').split("-",1));
                        add_valores_a_mandar(valorAtributo('cmbIdRol'));
                        ajaxModificar();
                        
                    buscarFamiliares('listGrillaFamiliares');    
                        
                    break;
                
            case 'crearLugar':
                
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2549&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbIdMun'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar'));
                    add_valores_a_mandar(valorAtributo('txtDireccion'));                     
                    ajaxModificar();   
               
                    break;

            case 'eliminarLugar':

                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2551&parametros=";              
                    add_valores_a_mandar(valorAtributo('txtId'));
                    ajaxModificar();
                                    
                    break;

            case 'crearVivienda':
                
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2554&parametros=";
                    add_valores_a_mandar(valorAtributo('txtId'));
                    add_valores_a_mandar(valorAtributo('txtDescripccion'));                   
                    ajaxModificar();   
               
                    break;

            case 'eliminarVivienda':

                    //debe existir un servicio que verifique si la vivienda tiene familias y solo en caso de no tener debe dejar eliminar
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=2555&parametros=";              
                    add_valores_a_mandar(valorAtributo('txtIdVivienda'));
                    ajaxModificar();
                                    
                    break;

                }
           
}

function respuestaModificarCRUDParametrosModelo(arg, xmlraiz) {

    if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {

        switch (arg) {
            case 'modificarGrupo':
                alert('Modificacion Exitosa');
                buscarParametros('listGrupo');

            break;

            case 'crearGrupo':
                alert('Creacion de Grupo Exitosa');
                buscarParametros('listGrupo');

            break;

            case 'eliminarPersonalGrupo':
                alert('Se elimino el personal del grupo');
                listGruposPersonal();
            
            break;

            case 'asignarPersonalGrupo':
                alert('Se agrego el personal a el grupo');
                listGruposPersonal();
            
            break;

            case 'asignarPersonalGrupoVentana':
                alert('Se agrego exitosamente el integrante al grupo');
                listGruposPersonal();

                   
            break;

            case 'eliminarPersonalGrupoVentana':
                alert('Se elimino exitosamente el integrante');
                listGruposPersonal();
                   
            break;

            case 'modificarPersonalDesdeModelo':
                alert('Datos modificados exitosamente');  
                
            break;

            case 'crearFamilia':
                alert('Familia creada exitosamente');
                buscarParametrosModelo('listFamiliasVivienda');
            break;

            case 'modificarFamilia':
                alert('Modificacion exitosa');
                buscarParametrosModelo('listFamiliasVivienda');
            break;

            case 'asignarFamiliarVentana':
                alert('Miembro vinculado exitosamente')
            break;

            case 'eliminarFamiliarVentana':
                alert('Miembro desvinculado exitosamente')
            break;

            case 'crearLugar':
                alert('Lugar creado exitosamente');
                buscarParametrosModelo('listLugares');
            break;

            case 'asignarAreaGrupo':
                alert('Area vinculada exitosamente');
                buscarGrillaMapa('listMunicipioAreas');

            break;

            case 'eliminarLugar':
                alert('Lugar eliminado exitosamente');
                buscarParametrosModelo('listLugares');
            break;

            case 'crearVivienda':
                alert('Vivienda creada exitosamente');
                buscarParametrosModelo('listViviendas');
            break;

            case 'eliminarVivienda':
                alert('Vivienda eliminada exitosamente');
                buscarParametrosModelo('listViviendas');
            break;

            case 'asignarPacienteGrupo':
                alert('PACIENTE ASIGNADO EXITOSAMENTE');
                buscarPacientesGrupo('listGrillaPacientes');
            break;

            case 'adcribirPacienteGrupo':
                alert('PACIENTE ADSCRITO EXITOSAMENTE');
                buscarPacientesGrupo('listGrillaPacientes');
            break;

        }

    }
    else console.log('NO SE LOGRO GUARDAR, VERIFIQUE CONEXIÓN CON SERVIDOR')
}

function cargarDepDesdeMunicipioGrupo(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 930, valorAtributoIdAutoCompletar(comboOrigen));
}

function cargarDepDesdeLocalidadGrupo(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 931, valorAtributo(comboOrigen));
}

function cargarDepDesdeMunicipioGrupo2(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 930, valorAtributo(comboOrigen));
}

function cargarDepDesdeMunicipioAreaGrupo(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 932, valorAtributo(comboOrigen));
}

function cargarDepDesdeGrupoAreaGrupo(comboOrigen, comboDestino) {
    cargarComboGRALCondicion1('cmbPadre', '1', comboDestino, 1326, valorAtributo(comboOrigen));
}

//-------------------------------------------------------------------------------------------14/12/2020----------------------------------

function buscarRutasModelo(arg, callback) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {

        case 'listPacienteEncuestas':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=925&parametros=";
            //add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
            //add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
            add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
            add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
            add_valores_a_mandar(valorAtributo('cmbIdEncuestasBus'));
            add_valores_a_mandar(valorAtributo('cmbIdEncuestasBus'));   
            //add_valores_a_mandar(valorAtributo('cmbIdEstadoEncuestasBus'));
            //add_valores_a_mandar(valorAtributo('cmbIdEstadoEncuestasBus'));
            add_valores_a_mandar(valorAtributo('cmbIdRiesgoBus'));
            add_valores_a_mandar(valorAtributo('cmbIdRiesgoBus'));
            add_valores_a_mandar(valorAtributo('cmbIdRiesgo'));
            add_valores_a_mandar(valorAtributo('cmbIdRiesgo'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeRiesgo'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaRiesgo'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio2'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio2'));
            add_valores_a_mandar(valorAtributo('cmbIdGrupoM'));
            add_valores_a_mandar(valorAtributo('cmbIdGrupoM'));
            add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
            add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
            add_valores_a_mandar(valorAtributo('cmbIdBar2'));
            add_valores_a_mandar(valorAtributo('cmbIdBar2'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'TipoId', 'Identificacion', 'Nombre', 'Encuesta','Fecha cierre',
                    'id_riesgo', 'Riesgo', 'valor', 'Alerta', 'Estado', 'Edad', 'idPaciente', 'municipio', 
                    'direccion', 'telefono', 'celular'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 3) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 8) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 20), align: 'left' },
                    { name: 'nom_encuesta', index: 'nom_encuesta', width: anchoP(ancho, 25) },
                    { name: 'fecha cierre', index: 'fecha cierre', width: anchoP(ancho, 12) },
                    { name: 'id_riesgo', index: 'id_riesgo', hidden: true },
                    { name: 'nom_riesgo', index: 'nom_riesgo', width: anchoP(ancho, 10) },
                    { name: 'valor', index: 'valor', hidden: true },
                    { name: 'nomValor', index: 'nomValor', width: anchoP(ancho, 10) },  
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 22) },   
                    { name: 'edad', index: 'edad', hidden: true },  
                    { name: 'idPaciente', index: 'idPaciente', hidden: true }, 
                    { name: 'municipio', index: 'municipio', hidden: true }, 
                    { name: 'direccion', index: 'direccion', hidden: true }, 
                    { name: 'telefono', index: 'telefono', hidden: true }, 
                    { name: 'celular', index: 'celular', hidden: true },        
                ],
                height: 400,
                width: ancho,

                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtlIdentifi', datosRow.identificacion, 0)
                    asignaAtributo('txtNombre', datosRow.Nombre, 0)  
                    asignaAtributo('txtMunicipio', datosRow.municipio, 0)
                    asignaAtributo('txtDireccionRes', datosRow.direccion, 0)
                    asignaAtributo('txtTelefonos', datosRow.telefono, 0)
                    asignaAtributo('txtCelular1', datosRow.celular, 0)
                    asignaAtributo('txtEncuesta', datosRow.nom_encuesta, 0)
                    asignaAtributo('txtRiesgo', datosRow.nom_riesgo, 0)
                    document.getElementById("txtAlerta").style.width = "38.7%";
                    var p = datosRow.nomValor.split('-');
                    asignaAtributo('lblImgAlerta', p[0], 0)
                    asignaAtributo('txtAlerta', p[1], 0)
                    asignaAtributo('txtEstado', datosRow.estado, 0)
                    asignaAtributo('txtEdad', datosRow.edad, 0)
                    asignaAtributo('txtIdBusPaciente', datosRow.idPaciente, 0)
                    
                    buscarAGENDA('listHistoricoListaEspera');
                    cargarTableRiesgos();

                    //  $("#tabGestionGruposIntegrantes").tabs().find(".ui-tabs-nav").sortable({ axis: 'x' });
                        
                        
    
                    }
                
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        }
        
}
function cargarTableRiesgos() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", "/clinica/paginas/adminModelo/tabsRiesgos.jsp", true);
    varajaxMenu.onreadystatechange = llenarcargarTableRiesgos;
    varajaxMenu.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarTableRiesgos() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaRiesgos").innerHTML = varajaxMenu.responseText;              
            $("#tabsControlRiesgos").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });

            //buscarRutaPromocionMantenimiento(valorAtributo());
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) { swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');}
}


function rutasProgramas(arg){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
    switch (arg) {

    case 'listGrillaRutasModelo':
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2507" + "&parametros=" ;
        add_valores_a_mandar(valorAtributo('cmbClasificacionRutasProgramas'));
        add_valores_a_mandar(valorAtributo('cmbClasificacionRutasProgramas'));

        var editOptions = {
            editData: {
                accion: "modificarRuta",
                idQuery: "2509"
            },
            /*beforeShowForm: function (formid) {
                $('#ID', formid).attr("readonly", "readonly").addClass("ui-state-disabled");
                return true;
            },*/
            afterSubmit: function (response, postdata) {
                mensaje = respuestaModificarRutas(response);
                return [mensaje === '', mensaje, "new_id"];
            },
            onclickSubmit: function (params) {
                var id = jQuery("#listGrillaRutasModelo").jqGrid('getGridParam', 'selrow');
                var ret = jQuery("#listGrillaRutasModelo").jqGrid('getRowData', id);
                return { idEdit: ret.ID }
            },
            recreateForm: true,
            closeAfterEdit: true
        };

        var addOptions = {
            editData: {
                accion: "crearRuta",
                idQuery: "2512",
                
            },
            afterSubmit: function (response, postdata) {
                mensaje = respuestaModificarRutas(response);
                return [mensaje === '', mensaje, "new_id"];
            },
            onclickSubmit: function (params) {
                // var id = jQuery("#listGrillaRutas").jqGrid('getGridParam', 'selrow');
                //var ret = jQuery("#listGrillaRutas").jqGrid('getRowData', id);
                return true
            },
            recreateForm: true,
            closeAfterAdd: true
        };


        $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['contador', 'ID', 'NOMBRE', 'OBSERVACIONES', 'DURACION (DIAS)', 'EDAD INICIO MESES', 'EDAD INICIO AÑOS',
                'EDAD FIN MESES', 'EDAD FIN AÑOS', 'EDAD INICIO', 'EDAD FIN','SEXO PACIENTE','SEXO', 
                'CICLICA', 'VIGENTE', 'TIPO'],
            colModel: [
                { name: 'contador', index: 'contador', hidden: true },
                { name: 'ID', index: 'ID', hidden: true },
                { name: 'NOMBRE', index: 'NOMBRE', width: 20, editrules: { required: true }, editable: true },
                { name: 'OBSERVACIONES', index: 'OBSERVACIONES', width: 12, editable: true, edittype: "textarea" },
                { name: 'DURACION_DIAS', index: 'DURACION_DIAS', width: 8, editrules: { required: true, number: true }, editable: true },
                { name: 'EDAD_INICIO_MES', index: 'EDAD_INICIO_MES', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 11 } },
                { name: 'EDAD_INICIO_ANIO', index: 'EDAD_INICIO_ANIO', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 120 } },
                { name: 'EDAD_FIN_MES', index: 'EDAD_FIN_MES', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 11 } },
                { name: 'EDAD_FIN_ANIO', index: 'EDAD_FIN_ANIO', hidden: true, editable: true, editrules: { edithidden: true, number: true, minValue: 0, maxValue: 120 } },
                { name: 'EDAD_INICIO', index: 'EDAD_INICIO', width: 15 },
                { name: 'EDAD_FIN', index: 'EDAD_FIN', width: 15 },
                { name: 'ID_SEXO', index: 'ID_SEXO', hidden: true,editable: true,editrules: { edithidden: true,required: true }, edittype: "select", editoptions: { value: ":;F:FEMENINO;M:MASCULINO;A:AMBOS" } },
                { name: 'SEXO', index: 'SEXO', width: 10 },
                { name: 'CICLICA', index: 'CICLICA', width: 7, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "SI:SI;NO:NO" } },
                { name: 'VIGENTE', index: 'VIGENTE', width: 7, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "S:SI;N:NO" } },
                { name: 'TIPO', index: 'TIPO', width: 20, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "1:Promocion y mantenimiento;2:Programas IPS;3:Programas Nefroproteccion; 4:Maternoperinatal" } },
            ],
            height: 300,
            width: ancho,
            pager: "#pager71",
            editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
            onSelectRow: function (rowid) {
                var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdRuta', datosRow.ID, 0);
                    rutasProgramas('listGrillaRutaTecnologia');

            },
        });
        $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
            .navGrid("#pager71", {
                del:false,  add: true, edit: true, view: true, refresh: true, search: false,
                edittext: "Editar", addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar"
            }, editOptions, addOptions);
        break;

        case 'listGrillaRutaTecnologia':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2508" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdRuta'));

            var procedimientos = 'http://10.0.0.3:9010/autocomplete/2221';
            var jsonProcedimientos = new Array();

            var talentoHumano = 'http://10.0.0.3:9010/autocomplete/2222';
            var jsonTalentoHumano = new Array();

            $.get(procedimientos,function(data)
            {
                jsonProcedimientos = data;
            });

            $.get(talentoHumano,function(data)
            {
                jsonTalentoHumano = data;
            });


            
            var addOptions = {
                editData: {
                    accion: "crearRutaTipoCita",
                    idQuery: "873"
                },
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    return { txtIdRuta: valorAtributo('txtIdRuta') }
                },
                recreateForm: true,
                closeAfterAdd: true,
                width: 400
            };

            var editOptions = {
                editData: {
                    accion: "modificarRutaTipoCita",
                    idQuery: "187"
                },
                recreateForm: true,
                closeAfterEdit: true,
                viewPagerButtons: false,
                width: 400,
                afterSubmit: function (response, postdata) {
                    mensaje = respuestaModificarRutas(response);
                    return [mensaje === '', mensaje, "new_id"];
                },
                onclickSubmit: function (params) {
                    var id = jQuery("#" + arg).jqGrid('getGridParam', 'selrow');
                    var ret = jQuery("#" + arg).jqGrid('getRowData', id);
                    return { idEdit: ret.ID  }
                }
            };;

            $("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador','ID', 'PROCEDIMIENTOS', 'NOMBRE', 'TIPO', 'CUPS', 'TALENTO HUMANO', 'FRECUENCIA (DIAS)','UNICA', 'OBLIGATORIA', 'ORDEN'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', hidden: true },
                    {
                        name: 'Procedimiento', index: 'Procedimiento', width: 8, align: 'left', editrules: { required: true }, editable: true,
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {
                                    $('#Procedimiento').trigger("focus");
                                    $('#Procedimiento').autocomplete(jsonProcedimientos, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 800,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }]
                        }, formoptions: { rowpos: 1, colpos: 1 }
                    },
                    { name: 'NOMBRE', index: 'NOMBRE', width : 40 },
                    { name: 'TIPO', index: 'TIPO', width : 10 },
                    { name: 'CUPS', index: 'CUPS', width : 5 },
                    {
                        name: 'TalentoHumano', index: 'TalentoHumano', width: 10, align: 'left', editrules: { required: true }, editable: true,
                        editoptions: {
                            dataEvents: [{
                                type: "keypress",
                                fn: function (elem) {
                                    $('#TalentoHumano').trigger("focus");
                                    $('#TalentoHumano').autocomplete(jsonTalentoHumano, {
                                        autoFocus: true,
                                        multiple: false,
                                        matchContains: true,
                                        cacheLength: 1,
                                        width: 800,
                                        height: 600,
                                        deferRequestBy: 0,
                                        selSeparator: '|',
                                        minChars: 1,
                                        minLength: 0,
                                        delay: 0,
                                        search: function (event, ui) {
                                            window.pageIndex = 0;
                                        }
                                    });
                                }
                            }]
                        }, formoptions: { rowpos: 2, colpos: 1 }
                    },
                    { name: 'FRECUENCIA', index: 'FRECUENCIA', width: 6, editrules: { required: true }, editable: true, formoptions: { rowpos: 3, colpos: 1 } },
                    { name: 'UNICA', index: 'UNICA', width: 8, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "NO:NO;SI:UNICA;SC:SEGUN CRITERIO" },  formoptions: { rowpos: 4, colpos: 1 } },
                    { name: 'OBLIGATORIA', index: 'OBLIGATORIA', width: 6, editrules: { required: true }, editable: true, edittype: "select", editoptions: { value: "SI:SI;NO:NO" },  formoptions: { rowpos: 5, colpos: 1 } },
                    { name: 'ORDEN', index: 'ORDEN', width : 5,  editrules: { required: true }, editable: true, formoptions: { rowpos: 6, colpos: 1 } }
                ],
                height: "300",
                width: ancho,
                pager: "#pager72",
                editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
                onSelectRow: function (rowid) {
                    //var datosRow = jQuery('#' + arg).getRowData(rowid);
                    //asignaAtributo('txtIdRuta', datosRow.ID_PARAMETRO, 0);
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid')
                .navGrid("#pager72", {
                    alerttop: $('#' + arg).offset().top, alertleft: $('#' + arg).offset().left,
                    del:false, add: true, edit: true, view: true, refresh: true, search: false,
                    edittext: "Editar",  addtext: "Adicionar", viewtext: "Ver", refreshtext: "Refrescar"
                }, editOptions, addOptions);
            break;
    } 
}


function cargarTableRutasProgramas() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", "/clinica/paginas/adminModelo/tabsRutasProgramas.jsp", true);
    varajaxMenu.onreadystatechange = llenarcargarTableRutasProgramas;
    varajaxMenu.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarTableRutasProgramas() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaRutasProgramas").innerHTML = varajaxMenu.responseText;              
            $("#tabsRutasProgramas").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
            buscarUsuarioGrupo('listGrillaGruposPersonal');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) { swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');}
}

function buscarRutaPromocionMantenimiento(){
    
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    ancho = ($('#drag' + ventanaActual.num).find("#divGestionRiesgos").width());
    valores_a_mandar = pag;
    valores_a_mandar = valores_a_mandar + "?idQuery=2523" + "&parametros=" ;
    add_valores_a_mandar(valorAtributo('lblEdad'));

    $('#drag' + ventanaActual.num).find("#listTecnologiasPromocion").jqGrid({
        url: valores_a_mandar,
        datatype: 'xml',
        mtype: 'GET',
        colNames: ['contador','ID', 'PROCEDIMIENTOS', 'NOMBRE', 'TIPO', 'CUPS', 'TALENTO HUMANO', 'ORDEN'
        ],
        colModel: [
            { name: 'contador', index: 'contador', hidden: true },
            { name: 'ID', index: 'ID', hidden: true },
            { name: 'Procedimiento', index: 'Procedimiento', width: 10 },
            { name: 'NOMBRE', index: 'NOMBRE', width : 40 },
            { name: 'TIPO', index: 'TIPO', width : 10 },
            { name: 'CUPS', index: 'CUPS', width : 5 },
            { name: 'TalentoHumano', index: 'TalentoHumano', width: 10},
            { name: 'ORDEN', index: 'ORDEN', width : 5 },
        ],
        height: "200",
        width: ancho,
        editurl: 'accionesXml/modificarGrillasCRUD_xml.jsp',
        onSelectRow: function (rowid) {
            //var datosRow = jQuery('#' + arg).getRowData(rowid);
            //asignaAtributo('txtIdRuta', datosRow.ID_PARAMETRO, 0);
        }
    });

    $('#drag' + ventanaActual.num).find("#listTecnologiasPromocion").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
}
var varaux = 0; 


function obtenerListaAfiliados(idTabla) {
    var lista_afiliados= [];
    var ids = $("#" + idTabla).getDataIDs();
    for (var i = 0; i < ids.length; i++) {
        var c = "jqg_" + idTabla + "_" + ids[i];
        if ($("#" + c).is(":checked")) {
            datosSedeRow = $("#" + idTabla).getRowData(ids[i]);
            lista_afiliados.push(datosSedeRow.id);
        }
    }
    return lista_afiliados;
}

function asignarPacientes(lista_afiliados) {
    tam = lista_afiliados.length;
    varaux = 0;
    if (lista_afiliados <= 0) {
        alert("DEBE SELECCIONAR AL MENOS UN PACIENTE");
    }
    else {
        for(i=0; i< tam; i++){        
        varajaxInit = crearAjax();
        valores_a_mandar = "";
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        
        valores_a_mandar = 'accion=asignarpacientes';
        valores_a_mandar = valores_a_mandar + "&idQuery=2525&parametros=";
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(lista_afiliados[i]);
        varajaxInit.send(valores_a_mandar);
        varaux = varaux + 1;
        }

        varajaxInit.onreadystatechange = function () { respuestaAsignarPacientes(tam, varaux) }
    }
}


function respuestaAsignarPacientes(tam, varaux) {

    if (varajaxInit.readyState == 4) {
       if(tam == varaux){
        setTimeout("caracterizacionGrupo('listCaract')", 1000);
        alert("Pacientes Asignados");
        varaux = 0;
       }
    }
}

function adscribirPaciente() {  
        varajaxInit = crearAjax();
        valores_a_mandar = "";
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.onreadystatechange = function () { respuestaAdscribirPaciente() }
        valores_a_mandar = 'accion=adcribirpaciente';
        valores_a_mandar = valores_a_mandar + "&idQuery=2526&parametros=";
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtIdPaciente'));
        varajaxInit.send(valores_a_mandar);
}


function respuestaAdscribirPaciente() {
    if (varajaxInit.readyState == 4) {
        caracterizacionGrupo('listCaract');
        alert("Paciente Adscrito");
    }
}

function actualizarDatosContactoModelo() {  
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.onreadystatechange = function () { respuestaActualizarDatosContactoModelo() }
    valores_a_mandar = 'accion=actualizarpaciente';
    valores_a_mandar = valores_a_mandar + "&idQuery=2527&parametros=";
    add_valores_a_mandar(valorAtributo('txtCorreo'));
    add_valores_a_mandar(valorAtributo('txtTelefono'));
    add_valores_a_mandar(valorAtributo('txtCelular'));
    add_valores_a_mandar(valorAtributo('cmbMunicipioModelo'));
    add_valores_a_mandar(valorAtributo('cmbLocalidadModelo'));
    add_valores_a_mandar(valorAtributo('cmbBariioModelo'));
    add_valores_a_mandar(valorAtributo('txtDireccion'));
    add_valores_a_mandar(valorAtributo('txtIdPaciente'));
    varajaxInit.send(valores_a_mandar);
}


function respuestaActualizarDatosContactoModelo() {
if (varajaxInit.readyState == 4) {
    setTimeout(buscarPacientesGrupo('listGrillaPacientes'), 500);
    caracterizacionGrupo('listCaract');
    alert("Datos Actualizados");
    }
}

function traerPuntoMapa(){
    asignaAtributo('txtLatitud', valorAtributo('txtLatitudReferencia'), 0);
    asignaAtributo('txtLongitud', valorAtributo('txtLongitudReferencia'), 0);
}

function estadisticoMarcadores(id){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag; 
        ancho = ($('#drag' + ventanaActual.num).find("#listEstadisticoMarcadores").width());
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2529&parametros=";
        add_valores_a_mandar(id);
            $('#drag' + ventanaActual.num).find("#listEstadisticoMarcadores").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['TOTAL','ID_TIPO', 'TIPO', 'ICONO', 'CANTIDAD'],
                colModel: [
                        {name: 'total', index: 'total', hidden: true },
                        {name: 'id_tipo', index: 'id_tipo', hidden: true },
                        { name: 'tipo', index: 'tipo', width: 30 },
                        { name: 'icono', index: 'icono', width: 10 },
                        { name: 'cantidad', index: 'cantidad', width: 10 },
                        ],                 
                height: 300,
                width: ancho,
                caption: "ESTADISTICO",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listEstadisticoMarcadores').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblTipoMarcador', datosRow.id_tipo, 0);     
                    centroPuntoDelMapaMunicipioMarcador();
                    MarcadoresEspecificos(id);
                },
            });
        $('#drag' + ventanaActual.num).find("#listEstadisticoMarcadores").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}

function MarcadoresEspecificos(id){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag; 
        ancho = ($('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").width());
        if(valorAtributo('lblTipoMarcador') === '4'){

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2568&parametros=";
            add_valores_a_mandar(id);
            add_valores_a_mandar(valorAtributo('lblTipoMarcador'));
            
            $('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['TOTAL', 'ID', 'LATITUD', 'LONGITUD', 'ICONO', 'DESCRIPCCION'],
                colModel: [
                        { name: 'total', index: 'total', hidden: true },
                        { name: 'id', index: 'id', width: 5 },
                        { name: 'latitud', index: 'latitud', width: 8 },
                        { name: 'longitud', index: 'longitud', width: 9 },
                        { name: 'icono', index: 'icono', width: 4 },
                        { name: 'descripcion', index: 'descripcion', width: 40 },
                        ],                 
                height: 200,
                width: ancho,
                caption: "MARCADORES",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listMarcadoresEspeciicos').getRowData(rowid);
                    estad = 0;

                    asignaAtributo('txtIdMarcador',datosRow.id,0);
                    asignaAtributo('txtDescripcion',datosRow.descripcion,0);
                volarhasta(datosRow.latitud,datosRow.longitud,datosRow.descripcion);
                },
            });
        $('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}else{
          /*
           var element = document. getElementById("gview_listGrillaAutorizaciones");
    var element2 = document. getElementById("gbox_listGrillaAutorizaciones");
    element. parentNode. removeChild(element);
    element2.parentNode.removeChild(element2);
    contenedorDiv = document.getElementById('divNormales');
    tabla = document.createElement('table');  // se crean los div hijos
    tabla.id = 'listGrillaAutorizaciones'
    tabla.className = 'scroll';
    contenedorDiv.appendChild(tabla); */      
        
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2530&parametros=";
        add_valores_a_mandar(id);
        add_valores_a_mandar(valorAtributo('lblTipoMarcador'));
            
            $('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['TOTAL', 'ID', 'LATITUD', 'LONGITUD', 'ICONO', 'DESCRIPCCION', 'ESTADO', 'SEMAFORO'],
                colModel: [
                        { name: 'total', index: 'total', hidden: true },
                        { name: 'id', index: 'id', width: 5 },
                        { name: 'latitud', index: 'latitud', width: 8 },
                        { name: 'longitud', index: 'longitud', width: 9 },
                        { name: 'icono', index: 'icono', width: 4 },
                        { name: 'descripcion', index: 'descripcion', width: 40 },
                        { name: 'estado', index: 'estado', hidden: true },
                        { name: 'SEMAFORO', index: 'SEMAFORO', width: 20 },
                        ],                 
                height: 200,
                width: ancho,
                caption: "MARCADORES",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listMarcadoresEspeciicos').getRowData(rowid);
                    estad = 0;

                    asignaAtributo('txtIdMarcador',datosRow.id,0);
                    asignaAtributo('txtDescripcion',datosRow.descripcion,0);
                    asignaAtributo('cmbIdEstadoMarcador',datosRow.estado,0);
                volarhasta(datosRow.latitud,datosRow.longitud,datosRow.descripcion);
                },
            });
        $('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
    }
}

function cargaInfoGrupoParaPersonal(){
    var id = valorAtributo('cmbIdGrupo'); 
    var url = 'http://10.0.0.3:9010/infogrupo/'+id
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {	
            asignaAtributo('txtMunicipioGrupo',object[0],0);
            asignaAtributo('txtLiderGrupo',object[1],0);
            asignaAtributo('txtVigente',object[2],0);             								
        }
    });

}

function listGruposPersonal(){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag; 
              ancho = ($('#drag' + ventanaActual.num).find("#listGrillaGruposMulti").width());
        
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2534" + "&parametros=";          
        add_valores_a_mandar(valorAtributo('txtIdentificacion'));
        

        $('#drag' + ventanaActual.num).find("#listGrillaGruposMulti").jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['TOTAL', 'ID', 'NOMBRE GRUPO','ID_MUNICIPIO', 'MUNICIPIO',  'ID_ROL', 'ROL', 'VIGENTE'
            ],
            colModel: [
                { name: 'TOTAL', index: 'TOTAL', hidden: true },
                { name: 'ID', index: 'ID',  width: anchoP(ancho, 10) },
                { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                { name: 'ID_MUNICIPIO', index: 'ID_MUNICIPIO', hidden: true },
                { name: 'MUNICIPIO', index: 'MUNICIPIO', width: anchoP(ancho, 20)},
                { name: 'ID_ROL', index: 'ID_ROL',  hidden: true  },
                { name: 'ROL', index: 'ROL', width: anchoP(ancho, 40) },
                { name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 10) }
                
            ],
            height: 200,
            autowidth: true,
            onSelectRow: function (rowid) {
                var datosRow = jQuery('#drag' + ventanaActual.num).find("#listGrillaGruposMulti").getRowData(rowid);
                asignaAtributo('cmbIdGrupo', datosRow.ID, 0)
                asignaAtributo('cmbIdRol', datosRow.ID_ROL, 0)
 
            }
        });
        $('#drag' + ventanaActual.num).find("#listGrillaGruposMulti").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}

//---------------------------------------------------------------------------------------------------------------------------------


function centroPuntoDelMapaMunicipioMarcador() { 
    recargarMapa()
    var estiloPopup = { 'maxWidth': '300' }
    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [5, 8], popupAnchor: [2, -6] } });    

    mymap = L.map('mapaGeoArea').setView([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], valorAtributo('lblIdZoom'));
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

    mymap.on('click', function (e) {
        asignaAtributo('txtLatitudReferencia', e.latlng.lat.toString(), 0);
        asignaAtributo('txtLongitudReferencia', e.latlng.lng.toString(), 0);
    });

    urlIconoCentroPunto = 'https://image.flaticon.com/icons/png/128/291/291236.png'

    L.marker([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup('Punto buscado', estiloPopup).addTo(mymap);

    cargaAreasMunicipioMarcador(valorAtributoIdAutoCompletar('txtMunicipio'), 1360) // txtidgrupo 1017
}

function cargaAreasMunicipioMarcador(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaAreasMunicipioMarcador(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaAreasMunicipioMarcador(condicion1) {
    
    var iconoBase = L.Icon.extend({ options: { iconSize: [35, 35],  iconAnchor: [5, 8], popupAnchor: [2, -6] } });
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            totalRegistros = raiz.getElementsByTagName('id').length
            var polygon = [];
            contador = 0;
            puntos = 0;
            for (i = 0; i < totalRegistros; i++) {
                if(i === 0){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                else {
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    contador = contador +1
                    polygon.push(i-puntos);
                    puntos=i;    
                }
                if( i === totalRegistros-1){
                    contador = contador+1
                    polygon.push(i-puntos+1)
                }
            }
            ban = 0;
            k = 0;
            for ( j = 0; j < contador; j++){
                var subPolygonPoints = Array.from(Array(polygon[j]), () => new Array(2));
                for (i = ban; i < totalRegistros; i++) {
                    
                    if(i === 0 || i === ban-1){
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    }
                    if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                        pol = raiz.getElementsByTagName('id')[i].firstChild.data
                        var p = pol.split('-_');
                        var latitud = p[0];
                        var longitud = p[1];
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        subPolygonPoints[k][0] = latitud
                        subPolygonPoints[k][1] = longitud
                       
                       // fabricaMarcadorGrupo(latitud, longitud, titulo, 'Lat=' + latitud + '  lon=' + longitud, mymap)
                        k = k + 1;
                    }
                    else {
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        var colorrelleno = raiz.getElementsByTagName('nom')[i].firstChild.data
                        L.polygon(
                            subPolygonPoints,
                            {
                            weight: 2, /*grosor de la linea*/
                            lineJoin: "miter",
                            color: "blue",
                            fillColor: '#' + colorrelleno,  /* color de llenar fillColor: "none",*/
                            fillOpacity: 0.2
                            }
                        ).addTo(mymap)
                        ban = i;
                        k = 0;
                        break;
                    }
                    if( i === totalRegistros -1 ){
                        L.polygon(
                            subPolygonPoints,
                            {
                                weight: 2, /*grosor de la linea*/
                                lineJoin: "miter",
                                color: "blue",
                                fillColor: '#' + colorrelleno,  /* color de llenar fillColor: "none",*/
                                fillOpacity: 0.05
                            }
                        ).addTo(mymap)
                        break;
                    }
                }
            }
            
            cargaMarcadoresEspecificos(valorAtributo('lblTipoMarcador'),valorAtributoIdAutoCompletar('txtMunicipio'), 3207) 
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function cargaMarcadoresEspecificos(condicion1, condicion2,  idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=dos&condicion1=" + condicion1 + "&condicion2=" + condicion2 ;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaMarcadoresEspecificos(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaMarcadoresEspecificos(condicion1) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;

            var estiloPopup = { 'maxWidth': '300' }
            var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [5, 8], popupAnchor: [2, -6] } });


            for (i = 0; i < raiz.getElementsByTagName('id').length; i++) {

                marcador = raiz.getElementsByTagName('id')[i].firstChild.data
                marcador_tipo = raiz.getElementsByTagName('nom')[i].firstChild.data

                var m = marcador.split('-_');
                var id_marcador = m[0];
                var latitud = m[1];
                var longitud = m[2];
                var descripcion = m[3];

                var mt = marcador_tipo.split('-_');
                var id_marcador_tipo = mt[0];

                var textoPopup = '<b>' + m[0] + ' - ' + mt[1] + '</b><br/>' + m[3];
                var urlIcono = mt[2];
                L.marker([latitud, longitud], { icon: new iconoBase({ iconUrl: urlIcono }) }).bindPopup(textoPopup, estiloPopup).addTo(mymap);
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

//----------------------------------------------------------

function cargarTabsNucleoFamiliar() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", "/clinica/paginas/adminModelo/tabsNucleoFamilia.jsp", true);
    varajaxMenu.onreadystatechange = llenarcargarTabsNucleoFamiliar;
    varajaxMenu.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarTabsNucleoFamiliar() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaNucleoFamiliar").innerHTML = varajaxMenu.responseText;              
            $("#tabsNucleoFamiliar").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
            buscarFamiliares('listGrillaFamiliares');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) { swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');}
}

function traerVentanitaFamiliar(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    var pos = getAbsoluteElementPositionY(idLupitaVentanita)
    topY = pos.top;

    idQuery = query;
    elemInputDestino = elemInputDes;
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    if (filtro == '')
        varajaxMenu.open("POST", '/clinica/paginas/adminModelo/ventanitaFamiliar.jsp', true);

    varajaxMenu.onreadystatechange = function () { llenartraerVentanitaFamiliar(divPaVentanita) };
    varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxMenu.send(valores_a_mandar);
}

function llenartraerVentanitaFamiliar(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function buscarFamiliar(arg){
    pag = "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp";

            limpiarDivEditarJuan(arg);
            pag = "/clinica/paginas/accionesXml/buscarGrillaVentana_xml.jsp";
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());
            valores_a_mandar = pag;
            valores_a_mandar =
                valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo("txtCodBus"));
            add_valores_a_mandar(valorAtributo("txtNomBus"));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));

            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .jqGrid({
                    url: valores_a_mandar,
                    datatype: "xml",
                    mtype: "GET",
                    colNames: ["contador", "ID", "NOMBRE"],
                    colModel: [
                        { name: "contador", index: "contador", hidden: true },
                        { name: "ID", index: "ID", width: anchoP(ancho, 10) },
                        { name: "NOMBRE", index: "NOMBRE", width: anchoP(ancho, 70) },
                    ],
                    height: 200,
                    width: ancho - 80,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery("#drag" + ventanaActual.num)
                            .find("#" + arg)
                            .getRowData(rowid);
                        estad = 0;
                        asignaAtributo(
                            elemInputDestino,
                            concatenarCodigoNombre(datosRow.ID, datosRow.NOMBRE),
                            0
                        );
                        ocultar("divVentanitaPuesta");
                    },
                });
            $("#drag" + ventanaActual.num)
                .find("#" + arg)
                .setGridParam({ url: valores_a_mandar })
                .trigger("reloadGrid");
           
}

function buscarFamiliares(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width(); 
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2539&parametros=";
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtId'));
            $('#drag' + ventanaActual.num).find("#listGrillaFamiliares").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['TOTAL', 'ID_PACIENTE', 'TIPO', 'IDENTIFICACION', 'NOMBRE', 'id_rol', 'ROL',  'EDAD', 'SEXO', 'id_encuesta_paciente', 'APGAR', 'CABEZA', 'EMSSANAR' ],
                colModel: [
                        {name: 'total', index: 'total', hidden: true },
                        { name: 'id_paciente', index: 'id_paciente', hidden: true },
                        { name: 'tipo_id', index: 'tipo_id', width: anchoP(ancho, 5) },
                        { name: 'id', index: 'id', width: anchoP(ancho, 10) },
                        { name: 'nombre', index: 'nombre', width: anchoP(ancho, 30) },
                        { name: 'id_rol', index: 'id_rol', hidden: true },
                        { name: 'rol', index: 'rol', width: anchoP(ancho, 12) },
                        { name: 'EDAD', index: 'EDAD', width: anchoP(ancho, 3) },
                        { name: 'SEXO', index: 'SEXO', width: anchoP(ancho, 3) },             
                        { name: 'id_encuesta_paciente', index: 'id_encuesta_paciente', hidden: true },
                        { name: 'APGAR', index: 'APGAR', width: anchoP(ancho, 7) },
                        { name: 'CABEZA', index: 'CABEZA', width: anchoP(ancho, 5) },
                        { name: 'EMSSANAR', index: 'EMSSANAR', width: anchoP(ancho, 5) },
                        ],                 
                height: 350,
                width: ancho/2,
                caption: "INTEGRANTES",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaFamiliares').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtIdPaciente', concatenarCodigoNombre(datosRow.id_paciente,datosRow.nombre), 0);
                    asignaAtributo('cmbIdRol', datosRow.id_rol, 0);
                    asignaAtributo('lblGeneroPaciente', datosRow.SEXO, 0);
                    asignaAtributo('lblEdadPaciente', datosRow.EDAD, 0);
                    asignaAtributo('lblIdEncuestaPaciente', datosRow.id_encuesta_paciente, 0);
                    asignaAtributo('lblIdEncuesta', 27, 0);

                    tabsContenidoEncuesta('desdeEncuesta');
                        
                },
            });
        $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
                 
}

function cargaMapaMunicipioFamilia() {  //  alert(  condicion1+'::'+idQueryCombo )
    //var id = valorAtributoIdAutoCompletar('txtMunicipio'); 

    var idMunicipio = $("#cmbIdMun").val();
    var url = 'http://10.0.0.3:9010/referencia/'+idMunicipio;
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {	
            asignaAtributo('lblIdlatitud',object[0],0);
            asignaAtributo('lblIdlongitud',object[1],0);
            asignaAtributo('lblIdZoom',object[2],0);             								
        }
    });

    setTimeout("centroPuntoDelMapaMunicipio()", 1000);
    

}
function cargaMapaMunicipioFamiliaDesdeHc() {  //  alert(  condicion1+'::'+idQueryCombo )

    var id = valorAtributo('lblIdPaciente'); 
    var url = 'http://10.0.0.3:9010/georeferenciahc/'+id
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {

            if( object[0] != null){
            asignaAtributo('lblMarcador',object[0],0);	
            asignaAtributo('lblIdlatitud',object[1],0);
            asignaAtributo('lblIdlongitud',object[2],0);
            asignaAtributo('lblIdLugar',object[3],0); 
            asignaAtributo('txtMunicipio',(object[4]+'-'+object[5]),0);  
            asignaAtributo('lblIdZoom',14,0); 
            }else{
                
                var url = 'http://10.0.0.3:9010/georeferenciahcno/'+id

                $.ajax({
                    url: url,
                    dataType: 'json',
                    success: function (object) {

                        asignaAtributo('txtMunicipio',(object[0]+'-'+object[1]),0);  
                        asignaAtributo('lblIdZoom',12,0); 

                        setTimeout("cargaMapaMunicipioFamilia()", 500);

                        return;

                    }
                });

            }
                  

        }
    });
   
    setTimeout("centroPuntoDelMapaMunicipio()", 1000);

}

function guardarNucleoCasa(arg, pag){
    if(valorAtributo('lblMarcador')==0){
        if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
        valores_a_mandar = 'accion=' + arg;
        valores_a_mandar = valores_a_mandar + "&idQuery=2540&parametros=";
        add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
        add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
        add_valores_a_mandar(valorAtributo('txtDireccion'));
        add_valores_a_mandar(valorAtributo('cmbIdMun'));
        add_valores_a_mandar(IdSesion());
        add_valores_a_mandar(valorAtributo('txtDireccion'));
        add_valores_a_mandar(valorAtributo('txtId'));
        ajaxModificar();
    } 
    alert('Marcador de lugar creado exitosamente');
    var id = valorAtributo('txtId');
    var url = 'http://10.0.0.3:9010/marcadorVivienda/'+id
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {	
            asignaAtributo('lblMarcador',object[0],0);   
            setTimeout("cargaMapaMunicipioFamilia()", 1000);
        }
    });

    }else{
        alert('El lugar ya tiene un marcador');
    }
}

function actualizarNucleoCasa(arg, pag){
    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
        valores_a_mandar = 'accion=' + arg;
        valores_a_mandar = valores_a_mandar + "&idQuery=2543&parametros=";
        add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
        add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
        add_valores_a_mandar(valorAtributo('lblMarcador'));
        ajaxModificar();
    } 
    alert('Marcador de lugar actualizado exitosamente');

    setTimeout("cargaMapaMunicipioFamilia()", 1000);
}

var circle = 0;

function radioRiesgo(){
    if(circle == 0){
        var id = valorAtributo('txtId'); 
        var url = 'http://10.0.0.3:9010/lugar/'+id // camiar lugar por familia en gestion familia aplica para el else tambien
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (object) { 	
                circle = L.circle([object[0],object[1]], {
                    color: 'yellow',
                    fillColor: 'red',
                    fillOpacity: 0.2,
                    radius: valorAtributo('txtRadio')
            }).addTo(mymap);  
            circle.bindPopup("Area de riesgo");
            asignaAtributo('txtLatitudReferencia',object[0],0);
            asignaAtributo('txtLongitudReferencia',object[1],0); 				
        }
    });
    }else{
        circle.remove();
        var id = valorAtributo('txtId'); 
        var url = 'http://10.0.0.3:9010/lugar/'+id
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (object) { 	
                circle = L.circle([object[0],object[1]], {
                    color: 'yellow',
                    fillColor: 'red',
                    fillOpacity: 0.2,
                    radius: valorAtributo('txtRadio')
                }).addTo(mymap);  
                circle.bindPopup("Area de riesgo");
                asignaAtributo('txtLatitudReferencia',object[0],0);
                asignaAtributo('txtLongitudReferencia',object[1],0); 			
        }
    });
    }
    setTimeout("marcadoresRiesgoFamilia()", 1000);
}
function radioRiesgo2(){
    if(circle == 0){
        var id = valorAtributo('lblIdLugar'); 
        var url = 'http://10.0.0.3:9010/lugar/'+id // camiar lugar por familia en gestion familia aplica para el else tambien
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (object) { 	
                circle = L.circle([object[0],object[1]], {
                    color: 'yellow',
                    fillColor: 'red',
                    fillOpacity: 0.2,
                    radius: valorAtributo('txtRadio')
            }).addTo(mymap);  
            circle.bindPopup("Area de riesgo");
            asignaAtributo('txtLatitudReferencia',object[0],0);
            asignaAtributo('txtLongitudReferencia',object[1],0); 				
        }
    });
    }else{
        circle.remove();
        var id = valorAtributo('lblIdLugar'); 
        var url = 'http://10.0.0.3:9010/lugar/'+id
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (object) { 	
                circle = L.circle([object[0],object[1]], {
                    color: 'yellow',
                    fillColor: 'red',
                    fillOpacity: 0.2,
                    radius: valorAtributo('txtRadio')
                }).addTo(mymap);  
                circle.bindPopup("Area de riesgo");
                asignaAtributo('txtLatitudReferencia',object[0],0);
                asignaAtributo('txtLongitudReferencia',object[1],0); 			
        }
    });
    }
    setTimeout("marcadoresRiesgoFamilia()", 1000);
}

function marcadoresRiesgoFamilia(id){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag; 
        ancho = ($('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").width());
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2541&parametros=";
        add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
        add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
        add_valores_a_mandar(valorAtributo('txtRadio'));
            $('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['TOTAL', 'ID', 'LATITUD', 'LONGITUD', 'ICONO', 'DESCRIPCCION'],
                colModel: [
                        {name: 'total', index: 'total', hidden: true },
                        { name: 'id', index: 'id', width: 5 },
                        { name: 'latitud', index: 'latitud', width: 12 },
                        { name: 'longitud', index: 'longitud', width: 12 },
                        { name: 'icono', index: 'icono', width: 7 },
                        { name: 'descripccion', index: 'descripccion', width: 30 },
                        ],                 
                height: 520,
                autowidth: true,
                caption: "MARCADORES",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listMarcadoresEspeciicos').getRowData(rowid);
                    estad = 0;

                    volarhasta(datosRow.latitud,datosRow.longitud,datosRow.descripccion);
                },
            });
        $('#drag' + ventanaActual.num).find("#listMarcadoresEspeciicos").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}

function preparacionTabsVivienda(){
    var id = valorAtributo('txtId'); 
    var url = 'http://10.0.0.3:9010/encuestavivienda/'+id
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {	

            asignaAtributo('lblGeneroPaciente',object[0],0);
            asignaAtributo('lblEdadPaciente',object[1],0); 
            asignaAtributo('lblIdEncuestaPaciente',object[2],0); 
        					
        }
    });
    asignaAtributo('lblIdEncuesta', 56, 0);
  
    setTimeout("tabsContenidoEncuesta('desdeFamilia')", 500);  
}

function volarhasta(latitud, longitud, descripcion){
    mymap.flyTo({lat:latitud, lng:longitud}, 16);
    var popup = L.popup()
    .setLatLng({lat:latitud, lng:longitud})
    .setContent(descripcion)
    .openOn(mymap);
}

function actuliazarEstadoMarcador(arg, pag){
    if (pag == 'undefined')
    pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
    valores_a_mandar = 'accion=' + arg;
    valores_a_mandar = valores_a_mandar + "&idQuery=2545&parametros=";
    add_valores_a_mandar(valorAtributo('cmbIdEstadoMarcador'));
    add_valores_a_mandar(valorAtributo('txtIdMarcador'));

    add_valores_a_mandar(valorAtributo('txtIdMarcador'));
    add_valores_a_mandar(valorAtributo('cmbIdEstadoMarcador'));
    add_valores_a_mandar(valorAtributo('txtNotaGestionMarcador'));  
    add_valores_a_mandar(IdSesion());
    
    ajaxModificar();
    }   
    alert('Estado actualizado');
    ocultar('divVentanitaPuesta');
    setTimeout("ubicarMapaMunicipio()", 500);
    setTimeout("buscarGrillaMapa('listAreasRerefenciasMarcadores')",500);

}

//-----------------------------------------------------------------------------------------------------------------------------

function traerVentanitaGestionMarcador(idLupitaVentanita, filtro, divPaVentanita, elemInputDes, query) {
    if(valorAtributo('txtIdMarcador')==''){
        alert('Seleccione un marcador')
    }else{
        var pos = getAbsoluteElementPositionY(idLupitaVentanita)
        topY = pos.top;

        idQuery = query;
        elemInputDestino = elemInputDes;
        varajaxMenu = crearAjax();
        valores_a_mandar = "";
        if (filtro == '')
            varajaxMenu.open("POST", '/clinica/paginas/adminModelo/ventanitaGestionMarcador.jsp', true);
        varajaxMenu.onreadystatechange = function () { llenartraerVentanitaGestionMarcador(divPaVentanita) };
        varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxMenu.send(valores_a_mandar);
    }
}

// onclick="actuliazarEstadoMarcador('marcadorCambio')"

function llenartraerVentanitaGestionMarcador(divPaVentanita) {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById(divPaVentanita).innerHTML = varajaxMenu.responseText;
            mostrar(divPaVentanita);
            mostrar('divVentanitaPuesta');

            var d = document.getElementById('ventanitaHija');
            d.style.position = "fixed";

            $('#drag' + ventanaActual.num).find('#txtNomBus').focus();
            limpiarListadosTotales('listGrillaVentana');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) {
        swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
    }
}

function apareceEstadisticosGrupos(){
    document.getElementById("tablaMarcadoresGeo").style.display = "none";
    $("#listEstadisticoGruposGeo_nombre").trigger('reloadGrid');
    document.getElementById("tablaGruposGeo").style.display = "flex";
}

function apareceEstadisticosMarcadores(){
    document.getElementById("tablaGruposGeo").style.display = "none";
    $("#listEstadisticoMarcadores").trigger('reloadGrid');
    document.getElementById("tablaMarcadoresGeo").style.display = "flex";
}

function estadisticosGrupos(id){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag; 
        ancho = ($('#drag' + ventanaActual.num).find("#listEstadisticoGruposGeo").width());
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2546&parametros=";
        add_valores_a_mandar(id);
            $('#drag' + ventanaActual.num).find("#listEstadisticoGruposGeo").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['TOTAL','ID GRUPO', 'NOMBRE', 'CANTIDAD AREAS', 'TOTAL PACIENTES'],
                colModel: [
                        {name: 'total', index: 'total', hidden: true },
                        {name: 'id_grupo', index: 'id_grupo',  width: 10 },
                        { name: 'nombre', index: 'nombre', width: 30 },
                        { name: 'total_areas', index: 'total_areas', width: 10 },
                        { name: 'total_pacientes', index: 'total_pacientes', width: 10 },
                        ],                 
                height: 300,
                width: ancho,
                caption: "ESTADISTICO",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listEstadisticoGruposGeo').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtId', datosRow.id_grupo, 0); 
                    centroPuntoDelMapaMunicipioGrupo();
                    estadisticoGruposEspecifico();
                },
            });
        $('#drag' + ventanaActual.num).find("#listEstadisticoGruposGeo").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}

function estadisticoGruposEspecifico(){
        pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
        pagina = pag; 
        ancho = ($('#drag' + ventanaActual.num).find("#listEstadisticoGruposGeo").width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=987&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));
            add_valores_a_mandar(valorAtributo('txtId'));

        $('#drag' + ventanaActual.num).find("#" +  "listEstadisticoGruposEspecifico").jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['contador', 'id_municipio', 'Nombre del Municipio', 'latitud_muni', 'longitud_muni', 'ID de Area', 'Nombre del Area', 'Orden', 'latitud', 'longitud', 'zoom', 'color', 'color_opacidad', 'vigente', 'id_magia', 'id_localidad', 'id_barrio', 'Total', 'Asignados', 'Adscritos', 'Gestionados'
            ],
            colModel: [
                { name: 'contador', index: 'contador', hidden: true },
                { name: 'id_municipio', index: 'id_municipio', hidden: true  },
                { name: 'nom_municipio', index: 'nom_municipio', hidden: true },
                { name: 'latitud_muni', index: 'latitud_muni', hidden: true },
                { name: 'longitud_muni', index: 'longitud_muni', hidden: true },
                { name: 'id_area', index: 'id_area', width: 10 },
                { name: 'NombreArea', index: 'Nombre', width: 50 },
                { name: 'Orden', index: 'Orden', hidden: true },
                { name: 'latitud', index: 'latitud', hidden: true },
                { name: 'longitud', index: 'longitud', hidden: true },
                { name: 'zoom', index: 'zoom', hidden: true },
                { name: 'color', index: 'color', hidden: true },
                { name: 'color_opacidad', index: 'color_opacidad', hidden: true },
                { name: 'vigente', index: 'vigente', hidden: true },
                { name: 'id_magia', index: 'id_magia', hidden: true },
                { name: 'id_localidad', index: 'id_localidad', hidden: true },
                { name: 'id_barrio', index: 'id_barrio', hidden: true },
                { name: 'totalAfiliados', index: 'totalAfiliados', width: 10 },
                { name: 'pacientesAsignados', index: 'pacienteAsignados', width: 10 },
                { name: 'pacienteAdscritos', index: 'pacienteAdscritos', width: 10 },
                { name: 'pacienteCaracterizados', index: 'pacienteCaracterizados', width: 10 },
            ],
            height: 200,
            width: ancho,
            caption: "AREAS",
            onSelectRow: function (rowid) {

                var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + "listEstadisticoGruposEspecifico").getRowData(rowid);

                asignaAtributo('txtMunicipio', datosRow.id_municipio + '-' + datosRow.nom_municipio, 0);
                asignaAtributo('lblIdArea', datosRow.id_area, 0);
                asignaAtributo('lblIdlatitud', datosRow.latitud, 0);
                asignaAtributo('lblIdlongitud', datosRow.longitud, 0);
                asignaAtributo('txtArea', datosRow.NombreArea, 0);
                asignaAtributo('txtOrden', datosRow.Orden, 0);
                asignaAtributo('txtLatitud', datosRow.latitud, 0);
                asignaAtributo('txtLongitud', datosRow.longitud, 0);
                asignaAtributo('lblIdZoom', datosRow.zoom, 0);
                asignaAtributo('txtColor', datosRow.color, 0);
                asignaAtributo('txtColorOpacidad', datosRow.color_opacidad, 0);
                asignaAtributo('cmbVigente', datosRow.vigente, 0);
                asignaAtributo('lblIdMagia', datosRow.id_magia, 0);
                asignaAtributo('cmbIdArea', datosRow.id_area, 0);
                asignaAtributo('cmbIdLoc', datosRow.id_localidad, 0);
                asignaAtributo('cmbIdBar', datosRow.id_barrio, 0);

                
                setTimeout("buscarGrillaMapa('listAreasRerefencias')", 1000);

            },
        });     
        $('#drag' + ventanaActual.num).find("#" + "listEstadisticoGruposEspecifico").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
}


//-----------------------------------------------------------------------------------------------------


function generaRiesgoGeneral(arg, callback) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {

        case 'listRiesgoGeneral': 
            limpiarDivEditarJuan(arg);
            switch (valorAtributo('cmbIdPrioridad')){

                case '1':
                ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=2547&parametros=";
                add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
                add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));  
                add_valores_a_mandar(valorAtributo('txtFechaDesdeRiesgoGeneral'));
                add_valores_a_mandar(valorAtributo('txtFechaHastaRiesgoGeneral'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                add_valores_a_mandar(1200);
                add_valores_a_mandar(5000);
                break;

                case '2':
                    ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                    valores_a_mandar = pag;
                    valores_a_mandar = valores_a_mandar + "?idQuery=2547&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));  
                    add_valores_a_mandar(valorAtributo('txtFechaDesdeRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributo('txtFechaHastaRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(800);
                    add_valores_a_mandar(1199);
                        break;

                case '3':
                    ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                    valores_a_mandar = pag;
                    valores_a_mandar = valores_a_mandar + "?idQuery=2547&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));  
                    add_valores_a_mandar(valorAtributo('txtFechaDesdeRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributo('txtFechaHastaRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(400);
                    add_valores_a_mandar(799);
                        break;
                case '4':
                    ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                    valores_a_mandar = pag;
                    valores_a_mandar = valores_a_mandar + "?idQuery=2547&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));  
                    add_valores_a_mandar(valorAtributo('txtFechaDesdeRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributo('txtFechaHastaRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(0);
                    add_valores_a_mandar(399);
                        break;

                case '0':
                    ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
                    valores_a_mandar = pag;
                    valores_a_mandar = valores_a_mandar + "?idQuery=2547&parametros=";
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('cmbTipoIdBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));
                    add_valores_a_mandar(valorAtributo('txtIdentificacionBus'));  
                    add_valores_a_mandar(valorAtributo('txtFechaDesdeRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributo('txtFechaHastaRiesgoGeneral'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdLoc2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(valorAtributo('cmbIdBar2'));
                    add_valores_a_mandar(0);
                    add_valores_a_mandar(5000);
                        break;
            }

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'TipoId', 'Identificacion', 'Nombre', 'Encuesta',  'Fecha Cierre', 'Prioridad', 'edad', 'idPaciente', 'municipio', 'direccion', 'telefono', 'celular'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', hidden: true },
                    { name: 'TipoId', index: 'TipoId', align: 'center', width: anchoP(ancho, 3) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 8) },
                    { name: 'Nombre', index: 'Nombre', width: anchoP(ancho, 10), align: 'left' },
                    { name: 'id_encuesta', index: 'id_encuesta', width: anchoP(ancho, 12) },
                    { name: 'fecha_cierre', index: 'fecha_cierre', width: anchoP(ancho, 12) },
                    { name: 'prioridad', index: 'prioridad', width: anchoP(ancho, 22) },
                    { name: 'edad', index: 'edad', hidden: true }, 
                    { name: 'idPaciente', index: 'idPaciente', hidden: true }, 
                    { name: 'municipio', index: 'municipio', hidden: true }, 
                    { name: 'direccion', index: 'direccion', hidden: true }, 
                    { name: 'telefono', index: 'telefono', hidden: true }, 
                    { name: 'celular', index: 'celular', hidden: true },     
                           
                ],
                height: 300,
                width: ancho,

                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtEncuesta', datosRow.id_encuesta, 0);
                    asignaAtributo('txtlIdentifi', datosRow.identificacion, 0)
                    asignaAtributo('txtNombre', datosRow.Nombre, 0) 
                    document.getElementById("txtAlerta").style.width = "38.7%";
                    var p = datosRow.prioridad.split('-');
                    asignaAtributo('lblImgAlerta', p[1], 0)
                    asignaAtributo('txtAlerta', p[0], 0)
                    asignaAtributo('txtEdad', datosRow.edad, 0);
                    asignaAtributo('txtMunicipio', datosRow.municipio, 0)
                    asignaAtributo('txtDireccionRes', datosRow.direccion, 0)
                    asignaAtributo('txtTelefonos', datosRow.telefono, 0)
                    asignaAtributo('txtCelular1', datosRow.celular, 0)
                    asignaAtributo('txtIdBusPaciente', datosRow.idPaciente, 0)
                    
                    buscarRiesgosFocalizados('listRiesgosFocalizados');
                    buscarAGENDA('listHistoricoListaEspera');
                    cargarTableRiesgos();
    
                    }
                
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        }
        
}

function buscarRiesgosFocalizados(arg, callback) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {

        case 'listRiesgosFocalizados':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width());
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2548&parametros=";
            add_valores_a_mandar(valorAtributo('txtEncuesta'));


            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Riesgo', 'valor', 'Alerta', 'Estado'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', align: 'left', width: anchoP(ancho, 2) },
                    { name: 'nom_riesgo', index: 'nom_riesgo', width: anchoP(ancho, 10) },
                    { name: 'valor', index: 'valor', hidden: true },
                    { name: 'nomValor', index: 'nomValor', width: anchoP(ancho, 22) },  
                    { name: 'estado', index: 'estado', width: anchoP(ancho, 22) },         
                ],
                height: 180,
                width: ancho,
                caption: 'RIESGOS'

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        }
        
}

function cargarTabsViviendas() {
    varajaxMenu = crearAjax();
    valores_a_mandar = "";
    varajaxMenu.open("POST", "/clinica/paginas/adminModelo/tabsViviendas.jsp", true);
    varajaxMenu.onreadystatechange = llenarcargarTabsViviendas;
    varajaxMenu.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    varajaxMenu.send(valores_a_mandar);
}

function llenarcargarTabsViviendas() {
    if (varajaxMenu.readyState == 4) {
        if (varajaxMenu.status == 200) {
            document.getElementById("divParaViviendas").innerHTML = varajaxMenu.responseText;              
            $("#tabsViviendas").tabs().find(".ui-tabs-nav").sortable({ axis: "x" });
            buscarParametrosModelo('listViviendas');
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajaxMenu.readyState == 1) { swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');}
}

function activarSwitchVivienda(tabla, referencia){

    $("#" + tabla + "-" + referencia + "-span1").removeClass("onoffswitch-inner2-disabled");
    $("#" + tabla + "-" + referencia + "-span2").removeClass("onoffswitch-switch2-disabled");

    $("#" + tabla + "-" + referencia + "-span1").addClass("onoffswitch-inner2");
    $("#" + tabla + "-" + referencia + "-span2").addClass("onoffswitch-switch2");

    document.getElementById("formulario." + tabla + "." + referencia).disabled = false;
    valorSwitchsn("formulario." + tabla + "." + referencia, document.getElementById("formulario." + tabla + "." + referencia).checked)
}

function guardarEncuestaVivienda(arg, pag){
    if(valorAtributo('lblExisteEncuesta') == 'NO')
    {
        if (pag == 'undefined')
    pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
    valores_a_mandar = 'accion=' + arg;
    valores_a_mandar = valores_a_mandar + "&idQuery=2558&parametros=";
    add_valores_a_mandar(valorAtributo('txtIdVivienda'));
    add_valores_a_mandar(valorAtributo('txtNombreR'));
    add_valores_a_mandar(valorAtributo('txtCedulaR'));

    add_valores_a_mandar(valorAtributo('cmbVr2'));
    add_valores_a_mandar(valorAtributo('cmbVr3'));
    add_valores_a_mandar(valorAtributo('cmbVr4'));
    add_valores_a_mandar(valorAtributo('cmbVr5'));
    add_valores_a_mandar(valorAtributo('cmbVr6'));
    add_valores_a_mandar(valorAtributo('cmbVr7'));
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r8').value);
    add_valores_a_mandar(valorAtributo('cmbVr9'));
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r10').value);
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r11').value);
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r12').value);
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r13').value);
    add_valores_a_mandar(valorAtributo('cmbVr14'));
    add_valores_a_mandar(valorAtributo('cmbVr15'));
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r16').value);
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r17').value);
    add_valores_a_mandar(document.getElementById('formulario.encf_v.r18').value);

    ajaxModificar();
    }   
    alert('DATOS GUARDADOS');

    }else if (valorAtributo('lblExisteEncuesta') == 'SI'){

    alert('LA VIVIENDA YA TIENE UNA ENCUESTA, ACTUALICE LOS DATOS');
    }
}
function actualizarEncuestaVivienda(arg, pag){
    if(valorAtributo('lblExisteEncuesta') == 'NO')
    {
        alert('NO HAY DATOS PARA ACTUALIZAR, PRIMERO GUARDE LA ENCUESTA');

    }else if (valorAtributo('lblExisteEncuesta') == 'SI'){
        if (pag == 'undefined')
            pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

        pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
        paginaActual = arg;

        if (verificarCamposGuardar(arg)) {
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2559&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdVivienda'));
            add_valores_a_mandar(valorAtributo('txtNombreR'));
            add_valores_a_mandar(valorAtributo('txtCedulaR'));

            add_valores_a_mandar(valorAtributo('cmbVr2'));
            add_valores_a_mandar(valorAtributo('cmbVr3'));
            add_valores_a_mandar(valorAtributo('cmbVr4'));
            add_valores_a_mandar(valorAtributo('cmbVr5'));
            add_valores_a_mandar(valorAtributo('cmbVr6'));
            add_valores_a_mandar(valorAtributo('cmbVr7'));
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r8').value);
            add_valores_a_mandar(valorAtributo('cmbVr9'));
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r10').value);
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r11').value);
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r12').value);
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r13').value);
            add_valores_a_mandar(valorAtributo('cmbVr14'));
            add_valores_a_mandar(valorAtributo('cmbVr15'));
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r16').value);
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r17').value);
            add_valores_a_mandar(document.getElementById('formulario.encf_v.r18').value);
            add_valores_a_mandar(valorAtributo('txtIdVivienda'));

    ajaxModificar();

    alert('DATOS ACTUALIZADOS');
        }
    }
}

function cargarFichaViviendaFamilia(){
    var id = valorAtributo('txtId'); 
    var url = 'http://10.0.0.3:9010/idVivienda/'+id
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {

            asignaAtributo('txtNombreR', object[0],0)
            asignaAtributo('txtCedulaR', object[1],0) 

            asignaAtributo('cmbVr2', object[3], 0)
            asignaAtributo('cmbVr3', object[4], 0)  
            asignaAtributo('cmbVr4', object[5], 0)  
            asignaAtributo('cmbVr5', object[6], 0)  
            asignaAtributo('cmbVr6', object[7], 0)  
            asignaAtributo('cmbVr7', object[8], 0)  
            activarSwitchVivienda('encf_v', 'r8')
            if (object[9] == 'SI'){
                document.getElementById('formulario.encf_v.r8').checked = true   
            }
            if (object[9] == 'NO'){
                document.getElementById('formulario.encf_v.r8').checked = false   
            }
            asignaAtributo('cmbVr9', object[10], 0)
            activarSwitchVivienda('encf_v', 'r10')    
            if (object[11] == 'SI'){
                document.getElementById('formulario.encf_v.r10').checked = true   
            }
            if (object[11] == 'NO'){
                document.getElementById('formulario.encf_v.r10').checked = false  
            }
            activarSwitchVivienda('encf_v', 'r11')
            if (object[12] == 'SI'){
                document.getElementById('formulario.encf_v.r11').checked = true   
            }
            if (object[12] == 'NO'){
                document.getElementById('formulario.encf_v.r11').checked = false
            }
            activarSwitchVivienda('encf_v', 'r12')
            if (object[13] == 'SI'){
                document.getElementById('formulario.encf_v.r12').checked = true   
            }
            if (object[13] == 'NO'){
                document.getElementById('formulario.encf_v.r12').checked = false   
            }
            activarSwitchVivienda('encf_v', 'r13')
            if (object[14] == 'SI'){
                document.getElementById('formulario.encf_v.r13').checked = true   
            }
            if (object[14] == 'NO'){
                document.getElementById('formulario.encf_v.r13').checked = false
            }   
            asignaAtributo('cmbVr14', object[15], 0)  
            asignaAtributo('cmbVr15', object[16], 0)  
            activarSwitchVivienda('encf_v', 'r16')
            if (object[17] == 'SI'){
                document.getElementById('formulario.encf_v.r16').checked = true   
            }
            if (object[17] == 'NO'){
                document.getElementById('formulario.encf_v.r16').checked = false 
            }
            activarSwitchVivienda('encf_v', 'r17')
            if (object[18] == 'SI'){
                document.getElementById('formulario.encf_v.r17').checked = true   
            }
            if (object[18] == 'NO'){
                document.getElementById('formulario.encf_v.r17').checked = false  
            }
            activarSwitchVivienda('encf_v', 'r18')
            if (object[19] == 'SI'){
                document.getElementById('formulario.encf_v.r18').checked = true   
            }
            if (object[19] == 'NO'){
                document.getElementById('formulario.encf_v.r18').checked = false 
            }
            console.log(object[0])
            if(object[0]){
                $("#btnGuardadEncuestaViv").hide();                            
            }else{
                $("#btnActualizarEncuesta").hide();
            }
           								
        }
    });

}

function formularioMiembroFamilia(){
    document.getElementById('formulariomiembro').style.display = "flex";
}

function guardarMiembroFamiliaNoEmssanar(arg, pag){

    
    pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    if (verificarCamposGuardar(arg)) {
    valores_a_mandar = 'accion=' + arg;
    valores_a_mandar = valores_a_mandar + "&idQuery=2560&parametros=";
    add_valores_a_mandar(valorAtributo('txtId'));
    add_valores_a_mandar(valorAtributo('txtTipoIdNIM'));
    add_valores_a_mandar(valorAtributo('txtIdentificacionNIM'));
    add_valores_a_mandar(valorAtributo('txtNombreNIM'));
    add_valores_a_mandar(valorAtributo('cmbSexoNIM'));
    add_valores_a_mandar(valorAtributo('txtFechaNaciemientoNIM'));
    add_valores_a_mandar(valorAtributo('cmbParentescoNIM'));
    add_valores_a_mandar(valorAtributo('cmbEpsNIM'));
    add_valores_a_mandar(valorAtributo('cmbRegimenNIM'));

    ajaxModificar();
 
    }
    alert('MIEMBRO AGREGADO EXITOSAMENTE');
    ocultar('divVentanitaPuesta');
    buscarFamiliares('listGrillaFamiliares');
}

//---------------------------------------------------------------------------------------------------------------------------------------

function centroPuntoDelMapaMunicipioGrupo() { 
    recargarMapa()
    var estiloPopup = { 'maxWidth': '300' }
    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [5, 8], popupAnchor: [2, -6] } });    

    mymap = L.map('mapaGeoArea').setView([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], valorAtributo('lblIdZoom'));
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

    mymap.on('click', function (e) {
        asignaAtributo('txtLatitudReferencia', e.latlng.lat.toString(), 0);
        asignaAtributo('txtLongitudReferencia', e.latlng.lng.toString(), 0);
    });

    urlIconoCentroPunto = 'https://image.flaticon.com/icons/png/128/291/291236.png'

    L.marker([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup('Punto buscado', estiloPopup).addTo(mymap);

    cargaAreasMunicipioGrupo(valorAtributo('txtId'), 2508) // txtidgrupo 1017
}

function cargaAreasMunicipioGrupo(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaAreasMunicipioGrupo(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaAreasMunicipioGrupo(condicion1) {
    
    var iconoBase = L.Icon.extend({ options: { iconSize: [35, 35],  iconAnchor: [5, 8], popupAnchor: [2, -6] } });
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            totalRegistros = raiz.getElementsByTagName('id').length
            var polygon = [];
            contador = 0;
            puntos = 0;
            for (i = 0; i < totalRegistros; i++) {
                if(i === 0){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                }
                else {
                    codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    contador = contador +1
                    polygon.push(i-puntos);
                    puntos=i;    
                }
                if( i === totalRegistros-1){
                    contador = contador+1
                    polygon.push(i-puntos+1)
                }
            }
            ban = 0;
            k = 0;
            for ( j = 0; j < contador; j++){
                var subPolygonPoints = Array.from(Array(polygon[j]), () => new Array(2));
                for (i = ban; i < totalRegistros; i++) {
                    
                    if(i === 0 || i === ban-1){
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                    }
                    if(codigo_area === raiz.getElementsByTagName('title')[i].firstChild.data ){
                        pol = raiz.getElementsByTagName('id')[i].firstChild.data
                        var p = pol.split('-_');
                        var latitud = p[0];
                        var longitud = p[1];
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        subPolygonPoints[k][0] = latitud
                        subPolygonPoints[k][1] = longitud
                       
                       // fabricaMarcadorGrupo(latitud, longitud, titulo, 'Lat=' + latitud + '  lon=' + longitud, mymap)
                        k = k + 1;
                    }
                    else {
                        codigo_area = raiz.getElementsByTagName('title')[i].firstChild.data
                        var colorrelleno = raiz.getElementsByTagName('nom')[i].firstChild.data
                        L.polygon(
                            subPolygonPoints,
                            {
                            weight: 2, /*grosor de la linea*/
                            lineJoin: "miter",
                            color: "blue",
                            fillColor: '#' + colorrelleno,  /* color de llenar fillColor: "none",*/
                            fillOpacity: 0.2
                            }
                        ).addTo(mymap)
                        ban = i;
                        k = 0;
                        break;
                    }
                    if( i === totalRegistros -1 ){
                        L.polygon(
                            subPolygonPoints,
                            {
                                weight: 2, /*grosor de la linea*/
                                lineJoin: "miter",
                                color: "blue",
                                fillColor: '#' + colorrelleno,  /* color de llenar fillColor: "none",*/
                                fillOpacity: 0.05
                            }
                        ).addTo(mymap)
                        break;
                    }
                }
            }
            
            cargaMarcadoresDelAreaGrupo(valorAtributoIdAutoCompletar('txtMunicipio'), 3209);
        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------

function riesgosConsolidadoGeneralHC(idPaciente){
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    aux = idPaciente;
    vivienda = aux;
        pagina = pag; 
        ancho = ($('#drag' + ventanaActual.num).find("#listRiesgosPaciente").width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2563&parametros=";
            add_valores_a_mandar(idPaciente);
            //add_valores_a_mandar(aux);
            //add_valores_a_mandar(vivienda);

        $('#drag' + ventanaActual.num).find("#" +  "listRiesgosPaciente").jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['contador', 'ENTRADA', 'FECHA DE CIERRE', 'id_riesgo', 'RIESGO', 'valor_riesgo', 'ALERTA', 'GESTION'
            ],
            colModel: [
                { name: 'contador', index: 'contador', width: 3 },
                { name: 'ENTRADA', index: 'ENTRADA', hidden: true  },
                { name: 'fecha_cierre', index: 'fecha_cierre', hidden: true },
                { name: 'id_riesgo', index: 'id_riesgo', hidden: true },
                { name: 'RIESGO', index: 'RIESGO', width: 15  },
                { name: 'valor_riesgo', index: 'valor_riesgo', hidden: true },
                { name: 'ALERTA', index: 'ALERTA', width: 15  },
                { name: 'GESTION', index: 'GESTION', width: 10  },
            ],
            height: 150,
            width: ancho,
            onSelectRow: function (rowid) {

                var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + "listRiesgosPaciente").getRowData(rowid);

            },
        });     
        $('#drag' + ventanaActual.num).find("#" + "listRiesgosPaciente").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}

function cargarListPacientesMoviles(){

    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
        pagina = pag; 
        ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width());

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2564&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));
            add_valores_a_mandar(valorAtributo('txtId'));

        $('#drag' + ventanaActual.num).find("#" +  "listCaractMoviles").jqGrid({
            url: valores_a_mandar,
            datatype: 'xml',
            mtype: 'GET',
            colNames: ['No', 'TIPO ID', 'IDENTIFICACION', 'NOMBRE PERSONAL', 'MUNICIPIO', 'COMUNA / CORREGIMIENTO', 'BARRIO / VEREDA','DIRECCION', 'TELEFONO', 'CELULAR', 'ID AFILIACION', 'SEMAFORO', 'ENCUESTA', 'CORREO', 'ID_MUNICIPIO', 'ID_LOCALIDAD', 'ID_BARRIO', 'ID_UNICO', 'id', 'GRUPO_PREVISTO', 'id_previsto'],
            colModel: [
                { name: 'total', index: 'total', width: anchoP(ancho, 3) },
                { name: 'tipo_id', index: 'tipo_id', width: anchoP(ancho, 5) },
                { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 10) },
                { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 20) },
                { name: 'municipio', index: 'municipio',  width: anchoP(ancho, 20) },
                { name: 'localidad', index: 'localidad', width: anchoP(ancho, 20) },
                { name: 'barrio', index: 'barrio',width: anchoP(ancho, 20) },
                { name: 'direccion', index: 'direccion',  width: anchoP(ancho, 20) },
                { name: 'telefono', index: 'telefono',  width: anchoP(ancho, 20) },
                { name: 'celular', index: 'celular',  width: anchoP(ancho, 20) },
                { name: 'id_afiliacion', index: 'id_afiliacion',  hidden: true },  
                { name: 'semaforo', index: 'semaforo',  width: anchoP(ancho, 15) }, 
                { name: 'encuesta', index: 'encuesta',  width: anchoP(ancho, 10) },
                { name: 'correo', index: 'correo',  hidden: true},
                { name: 'id_municipio', index: 'id_municipio',  hidden: true },
                { name: 'id_localidad', index: 'id_localidad',  hidden: true},
                { name: 'id_barrio', index: 'id_barrio',  hidden: true},
                { name: 'id_unico', index: 'id_unico',  hidden: true},
                { name: 'id', index: 'id',  hidden: true },
                { name: 'grupo_previsto', index: 'grupo_previsto',  hidden: true },
                { name: 'id_previsto', index: 'id_previsto',  hidden: true },
    
            ],
    
            height: 152,
            width: ancho - 60,
            caption: "CARACTERIZACION ESPECIAL",
            onSelectRow: function (rowid) {

                var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + "listCaractMoviles").getRowData(rowid);

                asignaAtributo('txtTipoId2', datosRow.tipo_id, 0);
                asignaAtributo('txtIdentificacion2', datosRow.identificacion, 0);
                asignaAtributo('txtNomPersonal2', datosRow.nompersonal, 0);
                asignaAtributo('txtTelefono2', datosRow.telefono, 0);
                asignaAtributo('txtCelular2', datosRow.celular, 0);
                asignaAtributo('txtCorreo2', datosRow.correo, 0);
                asignaAtributo('cmbMunicipioModelo2', datosRow.id_municipio, 0); 
                asignaAtributoCombo('cmbLocalidadModelo2', datosRow.id_localidad, datosRow.localidad)
                asignaAtributoCombo('cmbBarioModelo2', datosRow.id_barrio, datosRow.barrio)
                asignaAtributo('txtDireccion2', datosRow.direccion, 0);  
                asignaAtributo('txtunico2', datosRow.id_unico, 0);  
                asignaAtributo('txtIdPaciente2', datosRow.id, 0); 
                asignaAtributo('txtGrupoPrevisto', datosRow.grupo_previsto, 0);
                asignaAtributo('txtIdPrevisto', datosRow.id_previsto, 0); 
                                                   

            },
        });     
        $('#drag' + ventanaActual.num).find("#" + "listCaractMoviles").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}

function actualizarDatosContactoModelo2() {  
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.onreadystatechange = function () { respuestaActualizarDatosContactoModelo2() }
    valores_a_mandar = 'accion=actualizarpaciente';
    valores_a_mandar = valores_a_mandar + "&idQuery=2527&parametros=";
    add_valores_a_mandar(valorAtributo('txtCorreo2'));
    add_valores_a_mandar(valorAtributo('txtTelefono2'));
    add_valores_a_mandar(valorAtributo('txtCelular2'));
    add_valores_a_mandar(valorAtributo('cmbMunicipioModelo2'));
    add_valores_a_mandar(valorAtributo('cmbLocalidadModelo2'));
    add_valores_a_mandar(valorAtributo('cmbBarioModelo2'));
    add_valores_a_mandar(valorAtributo('txtDireccion2'));
    add_valores_a_mandar(valorAtributo('txtIdPaciente2'));
    varajaxInit.send(valores_a_mandar);
}
function respuestaActualizarDatosContactoModelo2(){
    if (varajaxInit.readyState == 4) {
        setTimeout(buscarPacientesGrupo('listGrillaPacientes'), 500);
        cargarListPacientesMoviles();
        alert("Datos Actualizados");
        }

}

function moverPacienteModelo(){
    if (valorAtributo('txtIdPrevisto') === ''){
        alert ('NO EXISTE GRUPO EN ESTA LOCALIDA');
    }else{
        varajaxInit = crearAjax();
        valores_a_mandar = "";
        varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
        varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        varajaxInit.onreadystatechange = function () { respuestamoverPacienteModelo() }
        valores_a_mandar = 'accion=moverPaciente';
        valores_a_mandar = valores_a_mandar + "&idQuery=2565&parametros=";
        add_valores_a_mandar(valorAtributo('txtIdPrevisto'));
        add_valores_a_mandar(valorAtributo('txtIdPaciente2'));
        varajaxInit.send(valores_a_mandar);
    }
}

function respuestamoverPacienteModelo(){
    if (varajaxInit.readyState == 4) {
        cargarListPacientesMoviles();
        alert("PACIENTE TRASLADADO AL NUEVO GRUPO");
        }

}

function desvincularPacienteModelo(){
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.onreadystatechange = function () { respuestadesvincularPacienteModelo() }
    valores_a_mandar = 'accion=desvicularPaciente';
    valores_a_mandar = valores_a_mandar + "&idQuery=2566&parametros=";
    add_valores_a_mandar(valorAtributo('txtIdPaciente2'));
    add_valores_a_mandar(valorAtributo('txtId'));
    varajaxInit.send(valores_a_mandar);
}

function respuestadesvincularPacienteModelo(){
    if (varajaxInit.readyState == 4) {
        cargarListPacientesMoviles();
        alert("PACIENTE DESVINCULADO DE MODELO");
        }

}

function actualizarDatosDesdeRiesgos() {  
    varajaxInit = crearAjax();
    valores_a_mandar = "";
    varajaxInit.open("POST", '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', true);
    varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajaxInit.onreadystatechange = function () { respuestaActualizarDatosDesdeRiesgos() }
    valores_a_mandar = 'accion=actualizarpaciente';
    valores_a_mandar = valores_a_mandar + "&idQuery=2567&parametros=";
    add_valores_a_mandar(valorAtributo('txtTelefonos'));
    add_valores_a_mandar(valorAtributo('txtCelular1'));
    add_valores_a_mandar(valorAtributo('txtDireccionRes'));
    add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdBusPaciente'));
    varajaxInit.send(valores_a_mandar);
}


function respuestaActualizarDatosDesdeRiesgos() {
if (varajaxInit.readyState == 4) {
    buscarRutasModelo('listPacienteEncuestas');
    alert("Datos Actualizados");
    }
}

function volarcasa(){
    var id = valorAtributo('txtId'); 
    var url = 'http://10.0.0.3:9010/familia/' + id
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (object) {	
            let latitud = object[0];
            let longitud = object[1];
            let descripcion = object[2];  
            
            mymap.flyTo({lat:latitud, lng:longitud}, 16);
            var popup = L.popup()
            .setLatLng({lat:latitud, lng:longitud})
            .setContent(descripcion)
            .openOn(mymap);
        }
    });
    
}

function estadisticoRiesgosGrupo(arg, pag){

    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width(); 
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2569&parametros=";
        add_valores_a_mandar(valorAtributo('txtId'));
            $('#drag' + ventanaActual.num).find("#listEstadisticoRiesgosGrupo").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['#', 'ID_RIESGO', 'NOMBRE', 'TOTAL', 'BAJO', 'MODERADO', 'ALTO', 'MUY ALTO' ],
                colModel: [
                        {name: '#', index: '#', hidden: true },
                        { name: 'id_riesgo', index: 'id_riesgo', hidden: true },
                        { name: 'nombre', index: 'nombre', width: anchoP(ancho, 30) },
                        { name: 'total', index: 'total', width: anchoP(ancho, 8) },
                        { name: 'bajo', index: 'bajo', width: anchoP(ancho, 8) },
                        { name: 'moderado', index: 'moderado', width: anchoP(ancho, 8) },
                        { name: 'alto', index: 'alto', width: anchoP(ancho, 8) },
                        { name: 'muy_alto', index: 'muy_alto', width: anchoP(ancho, 8) },
  
                        ],                 
                height: 200,
                width: ancho - 60,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listEstadisticoRiesgosGrupo').getRowData(rowid);
                    estad = 0;
                        
                },

                onCellSelect: function (rowid, iCol, cellcontent, e) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    if (iCol == 3) {
                        estadisticoRiesgosGrupoGenerico(datosRow.id_riesgo)
                    }
                    if (iCol == 4) {
                        estadisticoRiesgosGrupoEspecifico(datosRow.id_riesgo,3)
                    }
                    if (iCol == 5) {
                        estadisticoRiesgosGrupoEspecifico(datosRow.id_riesgo,2)
                    }
                    if (iCol == 6) {
                        estadisticoRiesgosGrupoEspecifico(datosRow.id_riesgo,1)
                    }
                    if (iCol == 7) {
                        estadisticoRiesgosGrupoEspecifico(datosRow.id_riesgo,32)
                    }
                },
            });
        $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

}

function estadisticoRiesgosGrupoEspecifico(id_riesgo,valor){

    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width(); 
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2570&parametros=";
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(id_riesgo);
        add_valores_a_mandar(valor);
            $('#drag' + ventanaActual.num).find("#listPacientesRiesgoEspecifico").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['No', 'TIPO ID', 'IDENTIFICACION', 'NOMBRE PERSONAL',  'COMUNA / CORREGIMIENTO', 'BARRIO / VEREDA','DIRECCION', 'TELEFONO', 'CELULAR', 'ID AFILIACION', 'SEMAFORO', 'ENCUESTA', 'CORREO', 'ID_MUNICIPIO', 'ID_LOCALIDAD', 'ID_BARRIO', 'ID_UNICO', 'id', 'ALERTA'],
                colModel: [
                    { name: 'total', index: 'total', width: anchoP(ancho, 3) },
                    { name: 'tipo_id', index: 'tipo_id', width: anchoP(ancho, 5) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 10) },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 20) },
                    { name: 'localidad', index: 'localidad', width: anchoP(ancho, 20) },
                    { name: 'barrio', index: 'barrio',width: anchoP(ancho, 20) },
                    { name: 'direccion', index: 'direccion',  width: anchoP(ancho, 20) },
                    { name: 'telefono', index: 'telefono',  width: anchoP(ancho, 20) },
                    { name: 'celular', index: 'celular',  width: anchoP(ancho, 20) },
                    { name: 'id_afiliacion', index: 'id_afiliacion',  hidden: true },  
                    { name: 'semaforo', index: 'semaforo',  width: anchoP(ancho, 15) }, 
                    { name: 'encuesta', index: 'encuesta',  width: anchoP(ancho, 10) },
                    { name: 'correo', index: 'correo',  hidden: true},
                    { name: 'id_municipio', index: 'id_municipio',  hidden: true },
                    { name: 'id_localidad', index: 'id_localidad',  hidden: true},
                    { name: 'id_barrio', index: 'id_barrio',  hidden: true},
                    { name: 'id_unico', index: 'id_unico',  hidden: true},
                    { name: 'id', index: 'id',  hidden: true },
                    { name: 'alerta', index: 'alerta',  width: anchoP(ancho, 10) },
        
                ],
        
                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho - 60,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#listPacientesRiesgoEspecifico').getRowData(rowid);
                        
                        asignaAtributo('txtTipoId', datosRow.tipo_id, 0);
                        asignaAtributo('txtIdentificacion', datosRow.identificacion, 0);
                        asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                        asignaAtributo('txtTelefono', datosRow.telefono, 0);
                        asignaAtributo('txtCelular', datosRow.celular, 0);
                        asignaAtributo('txtCorreo', datosRow.correo, 0);
                        asignaAtributo('cmbMunicipioModelo', datosRow.id_municipio, 0); 
                        asignaAtributoCombo('cmbLocalidadModelo', datosRow.id_localidad, datosRow.localidad)
                        asignaAtributoCombo('cmbBariioModelo', datosRow.id_barrio, datosRow.barrio)
                        asignaAtributo('txtDireccion', datosRow.direccion, 0);  
                        asignaAtributo('txtunico', datosRow.id_unico, 0);  
                        asignaAtributo('txtIdPaciente', datosRow.id, 0); 
                                                                                            
                    },
            });
            $('#drag' + ventanaActual.num).find("#listPacientesRiesgoEspecifico").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        

}

function estadisticoRiesgosGrupoGenerico(id_riesgo){

    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho =
        $("#drag" + ventanaActual.num)
            .find("#divContenido")
            .width(); 
        valores_a_mandar = pag;
        valores_a_mandar = valores_a_mandar + "?idQuery=2571&parametros=";
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(valorAtributo('txtId'));
        add_valores_a_mandar(id_riesgo);
            $('#drag' + ventanaActual.num).find("#listPacientesRiesgoEspecifico").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
            
                colNames: ['No', 'TIPO ID', 'IDENTIFICACION', 'NOMBRE PERSONAL',  'COMUNA / CORREGIMIENTO', 'BARRIO / VEREDA','DIRECCION', 'TELEFONO', 'CELULAR', 'ID AFILIACION', 'SEMAFORO', 'ENCUESTA', 'CORREO', 'ID_MUNICIPIO', 'ID_LOCALIDAD', 'ID_BARRIO', 'ID_UNICO', 'id', 'ALERTA'],
                colModel: [
                    { name: 'total', index: 'total', width: anchoP(ancho, 3) },
                    { name: 'tipo_id', index: 'tipo_id', width: anchoP(ancho, 5) },
                    { name: 'identificacion', index: 'identificacion', width: anchoP(ancho, 10) },
                    { name: 'nompersonal', index: 'nompersonal', width: anchoP(ancho, 20) },
                    { name: 'localidad', index: 'localidad', width: anchoP(ancho, 20) },
                    { name: 'barrio', index: 'barrio',width: anchoP(ancho, 20) },
                    { name: 'direccion', index: 'direccion',  width: anchoP(ancho, 20) },
                    { name: 'telefono', index: 'telefono',  width: anchoP(ancho, 20) },
                    { name: 'celular', index: 'celular',  width: anchoP(ancho, 20) },
                    { name: 'id_afiliacion', index: 'id_afiliacion',  hidden: true },  
                    { name: 'semaforo', index: 'semaforo',  width: anchoP(ancho, 15) }, 
                    { name: 'encuesta', index: 'encuesta',  width: anchoP(ancho, 10) },
                    { name: 'correo', index: 'correo',  hidden: true},
                    { name: 'id_municipio', index: 'id_municipio',  hidden: true },
                    { name: 'id_localidad', index: 'id_localidad',  hidden: true},
                    { name: 'id_barrio', index: 'id_barrio',  hidden: true},
                    { name: 'id_unico', index: 'id_unico',  hidden: true},
                    { name: 'id', index: 'id',  hidden: true },
                    { name: 'alerta', index: 'alerta',  width: anchoP(ancho, 10) },
        
                ],
        
                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho - 60,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#listPacientesRiesgoEspecifico').getRowData(rowid);
                        
                        asignaAtributo('txtTipoId', datosRow.tipo_id, 0);
                        asignaAtributo('txtIdentificacion', datosRow.identificacion, 0);
                        asignaAtributo('txtNomPersonal', datosRow.nompersonal, 0);
                        asignaAtributo('txtTelefono', datosRow.telefono, 0);
                        asignaAtributo('txtCelular', datosRow.celular, 0);
                        asignaAtributo('txtCorreo', datosRow.correo, 0);
                        asignaAtributo('cmbMunicipioModelo', datosRow.id_municipio, 0); 
                        asignaAtributoCombo('cmbLocalidadModelo', datosRow.id_localidad, datosRow.localidad)
                        asignaAtributoCombo('cmbBariioModelo', datosRow.id_barrio, datosRow.barrio)
                        asignaAtributo('txtDireccion', datosRow.direccion, 0);  
                        asignaAtributo('txtunico', datosRow.id_unico, 0);  
                        asignaAtributo('txtIdPaciente', datosRow.id, 0); 
                                                                                            
                    },
            });
            $('#drag' + ventanaActual.num).find("#listPacientesRiesgoEspecifico").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
        

}

//----------------------------------------------------------------
// NO BORRAR FUNCION SIRVE PARA GENERAR EXCEL (FALTA ENCABEZADOS)
// function fnExcelReport()
// {
//     var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
//     var textRange; var j=0;
//     tab = document.getElementById('listGrillaNominalNefro'); // id of table
//     console.log( "ver el tab",tab);
//     for(j = 0 ; j < tab.rows.length ; j++) 
//     {     
//         tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
//         //tab_text=tab_text+"</tr>";
//     }

//     tab_text=tab_text+"</table>";
//     tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
//     tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
//     tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

//     var ua = window.navigator.userAgent;
//     var msie = ua.indexOf("MSIE "); 

//     if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
//     {
//         txtArea1.document.open("txt/html","replace");
//         txtArea1.document.write(tab_text);
//         txtArea1.document.close();
//         txtArea1.focus(); 
//         sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
//     }  
//     else                 //other browser not tested on IE 11
//         sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

//     return (sa);
// }


function cargabotonloader(boton){
    idBoton = boton; 
}