<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<div id="tabsParametrosGrupos" style="width:99%; height:auto">
    <ul>
        <li>
            <a href="#divGruposIntegrantes" id="tabGestionGruposIntegrantes" onclick="tabActivoGrupos('divGruposIntegrantes')">
                <center>GESTION DE INTEGRANTES</center>
            </a>
        </li>
        <li>
            <a href="#divGruposAreas" onclick="tabActivoGrupos('divGruposAreas')">
                <center>GESTION DE AREAS</center>
            </a>
        </li>
        <li>
          <a href="#divGruposCarac" onclick="tabActivoGrupos('divGruposCarac')">
              <center>ADSCRIPCION</center>
          </a>
      </li>
      <li>
        <a href="#divGruposMoviles" onclick="tabActivoGrupos('divGruposMoviles')">
            <center>MOVILES</center>
        </a>
    </li>
    <li>
      <a href="#divGruposEstadisticoRiesgos" onclick="tabActivoGrupos('divGruposEstadisticoRiesgos')">
          <center>ESTADISTICO</center>
      </a>
  </li>
    </ul>

    <div id="divGruposIntegrantes">
      <table width="100%" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td>
            <table width="100%">
              <tr class="estiloImputIzq2">
                <td width="15%">INTEGRANTE</td>
                <td width="40%">
                    <input type="text" id="txtIdPersonal" onkeypress="llamarAutocomIdDescripcionConDato('txtIdPersonal',307)" style="width:80%"/> 
                    <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPersonal"
                    onclick="traerVentanitaPersonal(this.id,'','divParaVentanita','txtIdPersonal','35')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                    <div id="divParaVentanita"></div>
                </td >
                <td width="10%">ROL </td>
                <td width="35%">
                  <select size="1" id="cmbIdRol" style="width:80%" >
                    <option value=""></option>
                    <%   resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(929);	
                          ComboVO cmbGR; 
                          for(int k=0;k<resultaux.size();k++){ 
                          cmbGR=(ComboVO)resultaux.get(k);
                     %>
                    <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                      <%=cmbGR.getDescripcion()%></option>
                    <%}%>>
                  </select>
                </td >
                <td width="5%"> <label id="lblIdGrupo"></label> </td>
              </tr> 
              <tr   class="estiloImputIzq2"> 
                <td class="titulos" colspan ="5">                      
                  <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" value="VINCULAR INTEGRANTE" onclick="modificarCRUDParametrosModelo('asignarPersonalGrupoVentana','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                  <input name="btn_MODIFICAR" title="btn_mo785" type="button" class="small button blue" value="DESVINCULAR INTEGRANTE" onclick="modificarCRUDParametrosModelo('eliminarPersonalGrupoVentana','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                </td>
              </tr>                                                       		           
            </table> 
          </td>   
        </tr>   
      </table>
      <table width="100%">
        <tr class="titulos">
          <td width="100%">
            <table id="listGrillaGruposPersonal" class="scroll"></table>
          </td>
        </tr>
      </table>
    </div>
    <div id="divGruposAreas">
      <div id="idElemArea" style="width: 100%;">                                              
        <table width="100%" cellpadding="0" cellspacing="0" border="1">
          <tr class="titulos">
              <td width="33%">COMUNA / CORREGIMIENTO</td> 
              <td width="33%">BARRIO / VEREDA</td>
              <td width="33%">AREA</td>                                     
          </tr>     
          <tr class="estiloImput" >
              <td>
                <select id="cmbIdLoc" style="width:80%" 
                  onfocus="cargarDepDesdeMunicipioGrupo2('cmbIdMun', 'cmbIdLoc')"
                  onchange=" limpiaAtributo('cmbIdBar', 0);">	                                        
                  <option value=''></option>
                </select>	                   
              </td>   
              <td>
                <select size="1" id="cmbIdBar" style="width:80%"  
                  onfocus="cargarDepDesdeLocalidadGrupo('cmbIdLoc', 'cmbIdBar')">	                                        
                  <option value=''></option>                                  
                </select>	                   
              </td>  
              <td>
                <select id="cmbIdArea" style="width:80%" 
                  onfocus="cargarDepDesdeMunicipioAreaGrupo('cmbIdMun', 'cmbIdArea')">	                                        
                  <option value=''></option>
                </select>	                   
              </td> 
          </tr>
          <tr>
            <td class="titulos" colspan ="3">                      
              <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" value="VINCULAR AREA" onclick="modificarCRUDParametrosModelo('asignarAreaGrupo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
              <input name="btn_ELIMINAR" title="btn_el785" type="button" class="small button blue" value="DESVINCULAR AREA" onclick="modificarCRUDParametrosModelo('eliminarAreaGrupo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />
              <input name="btn_MODIFICAR" title="btn_mo786" type="button" class="small button blue" value="MODIFICA AREA" onclick="modificarCRUDParametrosModelo('modificarAreaGrupo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
            </td>
          </tr>                            
        </table>
      </div>
      <table width="100%">
        <tr class="titulos">
          <td width="100%">
            <table id="listMunicipioAreas"  width="100%"  class="scroll" ></table>  
          </td>
        </tr>
      </table>  
        <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0">
          <tr class="titulos">
            <td height="1000px" valign="top" >
                <table id="idTableContenedorMapa" style="height: 100%; width: 100%" class="scroll" cellpadding="0" cellspacing="0" align="center">
                   <tr>
                    <td  valign="top"  style="height: 100%; width: 100%" id="idTableContenedorMapaTd">
                     <div id="mapaGeoArea" style="height: 90%; width: 100%"></div>
                   </td>
                 </tr> 
                </table>              
            </td>                              
          </tr>  
        </table>
    </div>
    <div id="divGruposCarac">
      <table width="100%" cellpadding="0" cellspacing="0" border="1" >
        <tr class="titulos">
          <td width="22%">AREAS</td>
          <td width="22%">ESTADO</td>
          <td width="22%">CURSO DE VIDA</td> 
          <td width="22%">&nbsp;</td> 
        </tr>
        <tr class="titulos">
          <td>
            <select id="cmbIdArea2" style="width:80%" 
              onfocus="cargarDepDesdeGrupoAreaGrupo('txtId', 'cmbIdArea2')">	                                               
              <option value="">[SELECCIONE ]</option>
            </select>	                   
          </td>    
          <td> 
            <select size="1" id="cmbEstadoModelo"  style="width:60%"   >                         
              <option value="">[ TODO ]</option>  
              <%     resultaux.clear();
                  resultaux=(ArrayList)beanAdmin.combo.cargar(2036);    
                  ComboVO cmb2; 
                  for(int k=0;k<resultaux.size();k++){ 
                  cmb2=(ComboVO)resultaux.get(k);
              %>
              <option value="<%= cmb2.getId()%>" title="<%= cmb2.getTitle()%>">
              <%= cmb2.getDescripcion()%></option>
              <%}%>                                  
            </select>                        
          </td>
          <td>
            <select size="1" id="cmbCursoVida" style="width:80%" >
              <option value=0>[ TODO ]</option>
              <option value=1>PRIMERA INFANCIA</option>
              <option value=2>INFANCIA</option>
              <option value=3>ADOLESCENCIA</option>
              <option value=4>JUVENTUD</option>
              <option value=5>ADULTEZ</option>
              <option value=6>VEJEZ</option>
          </td>
          <td>
            <input name="btn_Buscar" title="btn_B11" type="button" class="small button blue" value="BUSCAR" onclick="caracterizacionGrupo('listCaract')" />
          </td> 
        </tr>
      </table>
      <table width="100%">
        <tr class="titulos">
          <td width="100%">
            <table id="listCaract"  width="100%"  class="scroll" ></table>  
          </td>
        </tr>
      </table>
      <table width="100%" cellpadding="0" cellspacing="0" align="center" id="infoPaciente" style="font-size: 9px;">
        <tr class="titulosCentrados">
          <td colspan="3">INFORMACION DEL PACIENTE
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" cellpadding="0" cellspacing="0" align="center">
              <tr>
                <td>
                  <table width="100%">
                    <tr class="estiloImputIzq2">
                      <td width="30%">TIPO IDENTIFICACION</td>
                      <td width="50%" colspan="2"><input type="text" id="txtTipoId" style="width:80%;height: 15px;" readonly  />
                      </td>
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="30%"> IDENTIFICACION</td>
                      <td width="50%" colspan="2"><input type="text" id="txtIdentificacion" style="width:80%;height: 15px;" readonly  />
                      </td>
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="30%">NOMBRE PERSONAL</td>
                      <td width="50%" colspan="2"><input type="text" readonly style="width:80%;height: 15px;" onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNomPersonal" />
                      </td>
                    </tr>                                              
                    <tr   class="estiloImputIzq2">                         
                      <td width="30%">CORREO ELECTRONICO</td>
                      <td width="50%" colspan="2"><input type="text" id="txtCorreo" style="width:80%;height: 15px;" onkeypress="javascript:return email(event);" />
                      </td>                                             
                    </tr>  
                    <tr   class="estiloImputIzq2">                         
                      <td width="30%">TELEFONO</td>
                      <td width="50%" colspan="2"><input type="text" id="txtTelefono" style="width:80%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                      </td>                                             
                    </tr> 
                    <tr   class="estiloImputIzq2">                         
                      <td width="30%">CELULAR</td>
                      <td width="50%" colspan="2"><input type="text" id="txtCelular" style="width:80%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                      </td>                                             
                    </tr>             
                    <tr class="estiloImputIzq2">
                      <td width="30%">MUNICIPIO RESIDENCIA</td><td width="50%" colspan="2">
                        <select size="1" id="cmbMunicipioModelo"  style="width:80%"   >                         
                          <%     resultaux.clear();
                              resultaux=(ArrayList)beanAdmin.combo.cargar(614);    
                              ComboVO cmb3; 
                              for(int k=0;k<resultaux.size();k++){ 
                              cmb3=(ComboVO)resultaux.get(k);
                          %>
                          <option value="<%= cmb3.getId()%>" title="<%= cmb3.getTitle()%>">
                          <%= cmb3.getDescripcion()%></option>
                          <%}%>                                  
                        </select>                        
                      </td>
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="30%">COMUNA/CORREGIMIENTO</td><td width="50%" colspan="2">
                          <select id="cmbLocalidadModelo" style="width:80%" 
                          onfocus="cargarDepDesdeMunicipioGrupo2('cmbMunicipioModelo', 'cmbLocalidadModelo')"
                          onchange=" limpiaAtributo('cmbIdBar', 0);">	                                        
                          <option value=0></option>
                        </select>	                          
                      </td>
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="30%">BARRIO/VEREDA</td><td width="50%" colspan="2">
                        <select size="1" id="cmbBariioModelo" style="width:80%"  
                          onfocus="cargarDepDesdeLocalidadGrupo('cmbLocalidadModelo', 'cmbBariioModelo')">	                                        
                          <option value=0></option>                                  
                        </select>	                      
                      </td>
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="30%">DIRECCION</td><td td width="50%" colspan="2">              
                        <input type="text" id="txtDireccion" style="width:80%;height: 15px;"  />
                        <input type="hidden" id="txtunico" />
                        <input type="hidden" id="txtIdPaciente" />  
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
          </table>
          <table width="100%" style="cursor:pointer">
            <tr>
              <td width="99%" class="titulos">
                <input name="btn_asignar" title="btn_ad78" type="button" class="small button blue" value="ASIGNAR PACIENTES" onclick="asignarPacientes(obtenerListaAfiliados('listCaract'))"  />  
                <input name="btn_MODIFICAR" title="AD76" type="button" class="small button blue" value="ACTUALIZAR INFORMACION" onclick="actualizarDatosContactoModelo();" />
                <input name="btn_adscribir" title="btn_ad79" type="button" class="small button blue" value="ADSCRIBIR PACIENTE" onclick="adscribirPaciente()" />  
              </td>
            </tr>
          </table>
          <table width="100%" style="cursor:pointer">
            <tr class="titulosCentrados">
              <td>RIESGOS DEL PACIENTE
              </td>
            </tr>
            <tr>
              <td>
                <table  width="100%" id="listRiesgosPaciente" ></table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table> 
   </div>
   <div id="divGruposMoviles">
    <table width="100%">
      <tr class="titulos">
        <td width="100%">
          <table id="listCaractMoviles"  width="100%"  class="scroll" ></table>  
        </td>
      </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" align="center" id="infoPaciente" style="font-size: 9px;">
      <tr class="titulosCentrados">
        <td colspan="3">INFORMACION DEL PACIENTE
        </td>
      </tr>
      <tr>
        <td>
          <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td>
                <table width="100%">
                  <tr class="estiloImputIzq2">
                    <td width="30%">TIPO IDENTIFICACION</td>
                    <td width="50%" colspan="2"><input type="text" id="txtTipoId2" style="width:80%;height: 15px;" readonly  />
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="30%"> IDENTIFICACION</td>
                    <td width="50%" colspan="2"><input type="text" id="txtIdentificacion2" style="width:80%;height: 15px;" readonly  />
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="30%">NOMBRE PERSONAL</td>
                    <td width="50%" colspan="2"><input type="text" readonly style="width:80%;height: 15px;" onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNomPersonal2" />
                    </td>
                  </tr>                                              
                  <tr   class="estiloImputIzq2">                         
                    <td width="30%">CORREO ELECTRONICO</td>
                    <td width="50%" colspan="2"><input type="text" id="txtCorreo2" style="width:80%;height: 15px;" onkeypress="javascript:return email(event);" />
                    </td>                                             
                  </tr>  
                  <tr   class="estiloImputIzq2">                         
                    <td width="30%">TELEFONO</td>
                    <td width="50%" colspan="2"><input type="text" id="txtTelefono2" style="width:80%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                    </td>                                             
                  </tr> 
                  <tr   class="estiloImputIzq2">                         
                    <td width="30%">CELULAR</td>
                    <td width="50%" colspan="2"><input type="text" id="txtCelular2" style="width:80%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                    </td>                                             
                  </tr>             
                  <tr class="estiloImputIzq2">
                    <td width="30%">MUNICIPIO RESIDENCIA</td><td width="50%" colspan="2">
                      <select size="1" id="cmbMunicipioModelo2"  style="width:80%"   >                         
                        <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(614);    
                            ComboVO cmb4; 
                            for(int k=0;k<resultaux.size();k++){ 
                            cmb4=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmb4.getId()%>" title="<%= cmb4.getTitle()%>">
                        <%= cmb4.getDescripcion()%></option>
                        <%}%>                                  
                      </select>                        
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="30%">COMUNA/CORREGIMIENTO</td><td width="50%" colspan="2">
                        <select id="cmbLocalidadModelo2" style="width:80%" 
                        onfocus="cargarDepDesdeMunicipioGrupo2('cmbMunicipioModelo2', 'cmbLocalidadModelo2')"
                        onchange=" limpiaAtributo('cmbIdBar', 0);">	                                        
                        <option value=0></option>
                      </select>	                          
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="30%">BARRIO/VEREDA</td><td width="50%" colspan="2">
                      <select size="1" id="cmbBarioModelo2" style="width:80%"  
                        onfocus="cargarDepDesdeLocalidadGrupo('cmbLocalidadModelo2', 'cmbBarioModelo2')">	                                        
                        <option value=0></option>                                  
                      </select>	                      
                    </td>
                  </tr>
                  <tr class="estiloImputIzq2">
                    <td width="30%">DIRECCION</td><td td width="50%" colspan="2">              
                      <input type="text" id="txtDireccion2" style="width:80%;height: 15px;"  />
                      <input type="hidden" id="txtunico2" />
                      <input type="hidden" id="txtIdPaciente2" />  
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%">
                  <tr class="estiloImputIzq2">
                    <td width="30%">GRUPO PREVISTO</td>
                    <td width="50%" colspan="2">
                      <input type="hidden" id="txtIdPrevisto" />  
                      <input type="text" id="txtGrupoPrevisto" style="width:80%;height: 15px;" readonly  />
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
        </table>
        <table width="100%" style="cursor:pointer">
          <tr>
            <td width="99%" class="titulos">
              <input name="btn_asignar" title="btn_ad78" type="button" class="small button blue" value="MOVER PACIENTE" onclick="moverPacienteModelo()"  />  
              <input name="btn_MODIFICAR" title="AD76" type="button" class="small button blue" value="ACTUALIZAR INFORMACION" onclick="actualizarDatosContactoModelo2();" />
              <input name="btn_desvincular" title="btn_ad79" type="button" class="small button blue" value="DESVINCULAR PACIENTE DE MODELO" onclick="desvincularPacienteModelo()"  />
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table> 
  </div>
  <div id="divGruposEstadisticoRiesgos">
    <table width="100%">
      <tr class="titulos">
        <td width="100%">
          <table id="listEstadisticoRiesgosGrupo"  width="100%"  class="scroll" ></table>  
        </td>
      </tr>
    </table>
    <table width="100%" style="cursor:pointer">
      <tr class="titulosCentrados">
        <td>PACIENTES CON RIESGO
        </td>
      </tr>
    </table> 
    <table width="100%">
      <tr class="titulos">
        <td width="100%">
          <table id="listPacientesRiesgoEspecifico"  width="100%"  class="scroll" ></table>  
        </td>
      </tr>
    </table>
  </div>
</div>