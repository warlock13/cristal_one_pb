<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<div id="tabsNucleoFamiliar" style="width:99%; height:auto">
    <ul>
        <li>
            <a href="#divFamiliaIntegrantes" id="tabGestionIntegrantesFamilia" onclick="tabActivoGrupos('divIntegrantesFamilia')">
                <center>INTEGRANTES</center>
            </a>
        </li>
        <li>
            <a href="#divFamiliaArea" id="divGeoreferenciaFamilia" onclick="tabActivoGrupos('divGeoreferenciaFamilia')">
                <center>GEOREFERENCIA</center>
            </a>
        </li>
        <li>
          <a href="#divFamiliaVivienda" id="divEncVivienda"  onclick="tabActivoGrupos('divEncFamiliar')">
              <center>ENCUESTA FAMILIAR</center>
          </a>
      </li>
      <li>
        <a href="#divInfoVivienda" id="divEncVivienda"  onclick="tabActivoGrupos('divInfoViviendadesdeFamilia')">
          <center>FICHA VIVIENDA</center>
      </a>
    </li>
    </ul>

    <div id="divFamiliaIntegrantes">
      <table width="100%" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td>
            <table width="100%">
              <tr class="estiloImputIzq2">
                <td width="15%">INTEGRANTE</td>
                <td width="30%">
                    <input type="text" id="txtIdPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdPaciente',307)" style="width:80%"/> 
                    <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPersonal"
                    onclick="traerVentanitaFamiliar(this.id,'','divParaVentanita','txtIdPaciente','44')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                    <div id="divParaVentanita"></div>
                </td >
                <td width="10%">ROL </td>
                <td width="25%">
                  <select size="1" id="cmbIdRol" style="width:80%" >
                    <option value=""></option>
                    <%   resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(2509);	
                          ComboVO cmbGR; 
                          for(int k=0;k<resultaux.size();k++){ 
                          cmbGR=(ComboVO)resultaux.get(k);
                     %>
                    <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                      <%=cmbGR.getDescripcion()%></option>
                    <%}%>>
                  </select>
                </td >
                <td width="5%"> <label id="lblIdFamilia"></label>
                  <label id="lblGeneroPaciente" style="display: none;"></label>
                  <label id="lblEdadPaciente" style="display: none;"></label>
                  <label id="lblIdEncuesta" style="display: none;">27</label>
                  <label id="lblIdEncuestaPaciente" style="display: none;"></label> </td>
                <td width="10%">
                  <input type="checkbox" id="sw_cabeza_familia" > Cabeza Familiar
                </td>
              </tr> 
              <tr   class="estiloImputIzq2"> 
                <td class="titulos" colspan ="6">                      
                  <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" value="VINCULAR INTEGRANTE" onclick="modificarCRUDParametrosModelo('asignarFamiliarVentana','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                  <input name="btn_MODIFICAR" title="btn_mo785" type="button" class="small button blue" value="DESVINCULAR INTEGRANTE" onclick="modificarCRUDParametrosModelo('eliminarFamiliarVentana','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                </td>
              </tr>                                                       		           
            </table> 
          </td>   
        </tr>   
      </table>
      <table width="100%">
        <tr class="titulos">
          <td width="50%">
            <table id="listGrillaFamiliares" class="scroll"></table>
          </td>
          <td width="50%">
            <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
              <tr>
                <td> 
                  <div id="divParaTabsContenidoEncuesta" ></div>     
                </td>
              </tr>   
            </table>
          </td>
        </tr>
      </table>
    </div>
    <div id="divFamiliaArea">
      <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0" >  
        <tr class="titulos" >
           <td width="60%">
           <td rowspan="2" width="100%" height='1060px' valign="top">
            <table width="100%" height='1060px' class="fondoTabla" cellpadding="0" cellspacing="0" >  
              <tr>
                <td>
                  <input style="margin:10px 0 10px" title="GE90" type="button" class="small button blue" value="CENTRAR CASA" onclick="volarcasa()"/>
                </td>
              </tr>
              <tr class="titulos" height='25px'>
                <input type="text" id="txtLatitudReferencia"  style="width:80%; display: none;"   size="20" maxlength="20" />
                <input type="text" id="txtLongitudReferencia"  style="width:80%; display: none;"   size="20" maxlength="20" />
                <label id="lblIdlatitud" style="display: none;">0</label>
                <label id="lblIdlongitud" style="display: none;">0</label>
                <label id="lblIdZoom" style="display: none;">0</label>
                <label id="txtColor" style="display: none;">0</label>
                <label id="txtColorOpacidad" style="display: none;">0</label>
                <td width="70%" >RADIO DE EFECTO (METROS)</td>
                <td width="20%">&nbsp;</td>  
              </tr>
              <tr class="titulos" height='25px' >
                <td width="60%" ><input type="number" id="txtRadio" style="width:50%" /></td>                                             
                <td width="30%"><input title="GE89" type="button" class="small button blue" value="CALCULAR RIESGO" onclick="radioRiesgo2()"/></td>  
              </tr> 
              <tr class="titulos" height='1020px'>
                <td   valign="top"  valign="top" height='520px' colspan="2">  
                   <table id="listMarcadoresEspeciicos"  width="100%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td> 
             </tr>                                    
            </table>                         
          </td>   
        </tr>        
        <tr class="titulos">
          <td height="1000px" width="650px"  valign="top" >
              <table id="idTableContenedorMapa" style="height: 100%; width: 100%" class="scroll" cellpadding="0" cellspacing="0" align="center">
                 <tr>
                  <td  valign="top"  style="height: 100%; width: 100%" id="idTableContenedorMapaTd">
                   <div id="mapaGeoArea" style="height: 100%; width: 100%"></div>
                 </td>
               </tr> 
              </table>              
          </td>                            
        </tr>  
      </table> 
    </div>
    <div id="divFamiliaVivienda">
      <table width="100%">
        <tr class="titulos">
          <td width="50%">
            <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
              <tr>
                <td> 
                  <div id="divParaTabsContenidoEncuesta2" ></div>     
                </td>
              </tr>   
            </table>
          </td>
        </tr>
      </table>
    </div>
    <div id="divInfoVivienda">
      <table width="100%"  cellpadding="0" cellspacing="0"  align="center" id="infoVivienda"  >
        <tr class="titulosCentrados">
          <td colspan="5" style="font-size: 20px;"> FICHA VIVIENDA</td>   
        </tr>
        <tr>
          <td>
            <table width="100%"  class="scroll" cellpadding="0" cellspacing="2">
              <tr class="estiloImputIzq2">
                <td width="50%"><input type="text" id="txtIdVivienda" readonly onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="150"  style="display: none;"/></td>
              </tr> 
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">NOMBRE DE LA PERSONA QUE RESPONDE ENCUESTA</td><td width="50%"><input type="text" id="txtNombreR"  maxlength="150"  style="width:80%; font-size: 12px;"/></td> 
              </tr>
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">CEDULA DE LA PERSONA QUE RESPONDE ENCUESTA</td>
                <td width="50%"><input type="text" id="txtCedulaR"  maxlength="150"  style="width:80%; font-size: 12px;"/></td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td colspan="2" align="center" style="font-size: 15px; color: #336699;"><b>GENERALIDADES VIVIENDA</b></td> 
              </tr>
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">ZONA</td><td width="50%"><select size="1" id="cmbVr2"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(615);  
                   ComboVO cmbVr2; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr2=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr2.getId()%>" title="<%= cmbVr2.getTitle()%>">
                        <%=cmbVr2.getDescripcion()%></option>
                      <%} %>
                </select></td>
              </tr>
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">ESTRATO</td><td width="50%"><select size="1" id="cmbVr3"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(616);  
                   ComboVO cmbVr3; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr3=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr3.getId()%>" title="<%= cmbVr3.getTitle()%>">
                        <%=cmbVr3.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td colspan="2" align="center" style="font-size: 15px; color: #336699;" ><b> EVALUACI&Oacute;N DE VIVIENDA </b></td> 
              </tr>
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">Tipo de vivienda</td><td width="50%"><select size="1" id="cmbVr4"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(599);  
                   ComboVO cmbVr4; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr4=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr4.getId()%>" title="<%= cmbVr4.getTitle()%>">
                        <%=cmbVr4.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr>  
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;Cu&aacute;l es el material predominante de las paredes de la vivienda?</td><td width="50%"><select size="1" id="cmbVr5"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(601);  
                   ComboVO cmbVr5; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr5=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr5.getId()%>" title="<%= cmbVr5.getTitle()%>">
                        <%=cmbVr5.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;Cu&aacute;l es el material predominante del techo de la vivienda?</td><td width="50%"><select size="1" id="cmbVr6"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(602);  
                   ComboVO cmbVr6; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr6=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr6.getId()%>" title="<%= cmbVr6.getTitle()%>">
                        <%=cmbVr6.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr>  
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;Cu&aacute;l es el material predominante de los pisos de la vivienda?</td><td width="50%"><select size="1" id="cmbVr7"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(603);  
                   ComboVO cmbVr7; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr7=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr7.getId()%>" title="<%= cmbVr7.getTitle()%>">
                        <%=cmbVr7.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;El lugar donde viven cuenta con espacios independientes para dormitorio, cocina y ba&ntilde;os?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r8')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r8" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r8">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r8-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r8-span2"></span>
                    </label>
                  </div>
                </td> 
              </tr>  
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;Qu&eacute; tipo de servicio sanitario (inodoro) tiene esta vivienda?</td><td width="50%"><select size="1" id="cmbVr9"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(617);  
                   ComboVO cmbVr9; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr9=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr9.getId()%>" title="<%= cmbVr9.getTitle()%>">
                        <%=cmbVr9.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td colspan="2" align="center" style="font-size: 15px; color: #336699;"> <b> SERVICIOS </b> </td> 
              </tr>
              <tr class="estiloImputIzq2">
                <td style="font-size: 14px;"><u><b>&iquest;La vivienda cuenta con servicios de:</b></u></td> 
              </tr>
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">Acueducto?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r10')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r10" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r10">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r10-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r10-span2"></span>
                    </label>
                  </div>
                  </td> 
              </tr>  
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">Energ&iacute;a el&eacute;ctrica?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r11')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r11" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r11">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r11-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r11-span2"></span>
                    </label>
                  </div>
                </td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">Gas natural conectado a red p&uacute;blica?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r12')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r12" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r12">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r12-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r12-span2"></span>
                    </label>
                  </div>
                </td> 
              </tr>  
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">Alcantarillado?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r13')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r13" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r13">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r13-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r13-span2"></span>
                    </label>
                  </div>
                </td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td colspan="2" align="center" style="font-size: 15px; color: #336699;"><b>CONDICIONES AMBENTALES  DE LA VIVIENDA</b></td> 
              </tr>
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">¿Cerca a la vivienda hay?</td><td width="50%"><select size="1" id="cmbVr14"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(2507);  
                   ComboVO cmbVr14; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr14=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr14.getId()%>" title="<%= cmbVr14.getTitle()%>">
                        <%=cmbVr14.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr>  
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">Donde est&aacute; ubicada la vivienda tiene riesgo de:</td><td width="50%"><select size="1" id="cmbVr15"  maxlength="150"  style="width:80%; font-size: 12px;"  >                                         
                  <option value=""></option>
                  <%     resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(604);  
                   ComboVO cmbVr15; 
                   for(int k=0;k<resultaux.size();k++){ 
                    cmbVr15=(ComboVO)resultaux.get(k);
                  %>
                      <option value="<%= cmbVr15.getId()%>" title="<%= cmbVr15.getTitle()%>">
                        <%=cmbVr15.getDescripcion()%></option>
                      <%} %>
                </select></td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;Se evidencia humedad en la vivienda?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r16')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r16" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r16">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r16-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r16-span2"></span>
                    </label>
                  </div>
                </td> 
              </tr>  
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;Vivienda con adecuada ventilaci&oacute;n?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r17')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r17" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r17">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r17-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r17-span2"></span>
                    </label>
                  </div>
                </td> 
              </tr> 
              <tr class="estiloImputIzq2">
                <td width="50%" style="font-size: 12px;">&iquest;Vivienda con grietas o goteras?</td><td width="50%"><div class="onoffswitch2" onclick="activarSwitchVivienda('encf_v', 'r18')">
                  <input type="checkbox" name="encf_v" class="onoffswitch-checkbox2" id="formulario.encf_v.r18" value="" onclick="valorSwitchsn(this.id,this.checked); " disabled="">         
                    <label class="onoffswitch-label2" for="formulario.encf_v.r18">
                    <span class="onoffswitch-inner2-disabled" id="encf_v-r18-span1"></span>
                    <span class="onoffswitch-switch2-disabled" id="encf_v-r18-span2"></span>
                    </label>
                  </div>
                </td> 
              </tr>                                                                                                     
            </table> 
          </td>   
        </tr>
        <tr>
          <td colspan="5">  
            <input style="margin: 5px 0 5px;" id="btnGuardadEncuestaViv" name="btn_BUSCAR" type="button" class="small button blue" value="GUARDAR DATOS" onclick="guardarEncuestaVivienda()" />
            <input style="margin: 5px 0 5px;" id="btnActualizarEncuesta" name="btn_BUSCAR" type="button" class="small button blue" value="ACTUALIZAR DATOS" onclick="actualizarEncuestaVivienda()" />
          </td>  
        </tr>   
      </table>
    </div>
  </div>