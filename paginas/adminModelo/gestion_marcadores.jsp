<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%  beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1350"  align="center" cellpadding="0" cellspacing="0" >
    <tr>
        <td>
            <div align="center" id="tituloForma" >
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="MARCADORES" />
                </jsp:include>
            </div>  
        </td>
    </tr> 
    <tr>
      <td valign="top">    
        <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0" >  
          <tr class="titulos" >
             <td width="35%">
               <table width="101%" height='73px' class="fondoTabla" cellpadding="0" cellspacing="0" >
                <tr>
                  <td width="20%">Municipio</td>
                  <td width="20%">Nombre de Area</td>  

                </tr>
                <tr>
                  <td>                   
                    <input type="text" id="txtMunicipio" size="60" maxlength="60"  style="width:70%"  /> 
                    <img width="18px" height="20px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                  <div id="divParaVentanita"></div>                    
                 </td> 
                 <td width="20%">
                   <label id="lblIdArea" style="display: none;" >0</label>
                   <input type="text" id="txtArea"  style="width:80%"   size="100" maxlength="100" />
                 </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <label id="lblIdlatitud" style="display: none;">0</label>
                    <label id="lblIdlongitud" style="display: none;">0</label>
                    <label id="lblIdZoom" style="display: none;">0</label>
                    <label id="txtColor" style="display: none;">0</label>
                    <label id="txtColorOpacidad" style="display: none;">0</label>
                    <input title="GE89" type="button" class="small button blue" value="BUSCAR AREAS" onclick="buscarGrillaMapa('listaAreasMunicipiosMarcadores')"/>
                 </td> 
                </tr>
               </table>
             </td>
             <td width="65%" valign="top">
              <table width="100%" height='73px' class="fondoTabla" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="30%">Tipo</td>  
                  <td width="15%">Latitud</td>  
                  <td width="15%">Longitud</td>
                  <td width="40%">Descripcion</td>
                </tr>
                <tr>
                  <td><select size="1" id="cmbTipoMarcador" style="width:80%" >
                    <option value=""></option>
                    <%   resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(1345);	
                          ComboVO cmbGR; 
                          for(int k=0;k<resultaux.size();k++){ 
                          cmbGR=(ComboVO)resultaux.get(k);
                     %>
                    <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                      <%=cmbGR.getDescripcion()%></option>
                    <%}%>>
                  </select></td>
                  <td><input type="text" id="txtLatitudReferencia"  style="width:80%"   maxlength="20" /></td>
                  <td><input type="text" id="txtLongitudReferencia"  style="width:80%"   maxlength="20" /></td>
                  <td><input type="text" id="txtDescripcionMarcador"  style="width:90%" onkeyup="javascript: this.value= this.value.toUpperCase();"  maxlength="50" /></td>
                  <td><input type="text" id="txtIdMarcador"  style="display: none;"/></td>
                </tr>
                <tr>
                  <td colspan="4">
                    <input title="GE96" type="button" class="small button blue" value="Adicionar Marcador" onclick="modificarMapCRUD('adicionarMarcador')"/>
                    <input title="GE97" type="button" class="small button blue" value="Modificar Marcador" onclick="modificarMapCRUD('modificarMarcador')"/>
                    <input title="GE98" type="button" class="small button blue" value="eliminar Marcador" onclick="modificarMapCRUD('eliminarMarcador')"/>
                    <input title="GE91" type="button" class="small button blue" value="Gestionar marcador" onclick="traerVentanitaGestionMarcador(this.id,'','divParaVentanita','txtMunicipio','1')"/>
                   </td> 
                </tr>
              </table>
             </td>
          </tr>        
          <tr class="titulos">
            <td colspan="1" valign="top" height='250px'>
                <TABLE width="102%"  height='250px' class="fondoTabla" cellpadding="0" cellspacing="0" >  
                  <TR>
                    <TD  width="100%" height='250px' valign="top">
                      <table id="listaAreasMunicipiosMarcadores"  width="100%"  class="scroll" cellpadding="0" cellspacing="0" align="center" ></table>              
                    </TD>
                  </TR>
                </TABLE>    
            </td>
            <td colspan="1" valign="top" height='250px'>   
                <table width="100%" height='250px' class="fondoTabla" cellpadding="0" cellspacing="0" >  
                  <tr class="titulos">
                     <td colspan="3" valign="top" height='250px'>  
                        <table id="listAreasRerefenciasMarcadores"  width="100%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                     </td> 
                  </tr>                                    
                </table>                  
            </td>            
          </tr> 
          <tr class="titulos">
            <td height="1000px" colspan="2" valign="top" >
                <table id="idTableContenedorMapa" style="height: 90%; width: 100%" class="scroll" cellpadding="0" cellspacing="0" align="center">
                   <tr>
                    <td  valign="top"  style="height: 100%; width: 100%" id="idTableContenedorMapaTd">
                     <div id="mapaGeoArea" style="height: 90%; width: 100%"></div>
                   </td>
                 </tr> 
                </table>              
            </td>                              
          </tr>  
        </table>   
      </td>    
    </tr>        
</table>