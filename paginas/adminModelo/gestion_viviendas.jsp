<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
<tr>
  <td>
   <!-- AQUI COMIENZA EL TITULO -->	
    <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
      <jsp:include page="../titulo.jsp" flush="true">
         <jsp:param name="titulo" value="GESTION DE LUGARES" />
      </jsp:include>
    </div>	
   <!-- AQUI TERMINA EL TITULO -->
  </td>
</tr> 
<tr>
 <td>
    <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr>
       <td>
          <div style="overflow:auto;height:400px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
             <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
                 <tr class="titulos" align="center">
                   <td width="5%">ID LUGAR</td>
                   <td width="20%">DIRECCION</td>
                   <td width="15%">MUNICIPIO</td>
                   <td width="15%">LOCALIDAD</td>
                   <td width="15%">BARRIO/VEREDA</td>
                   <td width="10%">&nbsp;</td>                            
                 </tr>
                  <tr class="estiloImput">
                   <td><input type="number" id="txtBusIdVivienda" style="width:80%" /> </td>
                   <td><input type="text" id="txtBusDireccion"  onkeyup="javascript: this.value= this.value.toUpperCase();" style="width:80%"   /> </td>   
                   <td>                   
                     <input type="text" id="txtMunicipio" size="60" maxlength="60"  style="width:70%"  /> 
                     <img width="18px" height="18px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                   <div id="divParaVentanita"></div>                    
                   </td>
                   <td><select id="cmbIdLoc2" style="width:80%" 
                    onfocus="cargarDepDesdeMunicipioGrupo('txtMunicipio', 'cmbIdLoc2')"
                    onchange=" limpiaAtributo('cmbIdBar2', 0);">	                                        
                    <option value="">[ TODOS ]</option>
                  </select>	 	                   
                  </td> 
                   <td><select size="1" id="cmbIdBar2" style="width:80%"  
                    onfocus="cargarDepDesdeLocalidadGrupo('cmbIdLoc2', 'cmbIdBar2')">	                                        
                    <option value="">[ TODOS ]</option>                                  
                  </select>	                   
                   </td>  
                   <td>
                     <input name="btn_BUSCAR" title="Lugares a Buscar" type="button" class="small button blue" value="BUSCAR"  onclick="buscarParametrosModelo('listLugares')"/>                    
                   </td>         
                 </tr>	                          
             </table>
             <table id="listLugares" class="scroll" width="100%"></table>  
         </div>


         <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
           <tr class="titulosCentrados">
             <td colspan="5"> LUGARES
             </td>   
           </tr>  
           <tr>
           <td>
             <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
               <tr>
                 <td>
                   <table width="100%">
                     <tr class="estiloImputIzq2">
                       <td width="30%">ID LUGAR</td><td width="50%"><input type="text" id="txtId" readonly onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="150"  style="width:8%"/></td> 
                     </tr> 
                     <tr class="estiloImputIzq2">
                       <td width="30%">DIRECCION</td><td width="50%"><input type="text" id="txtDireccion" onkeyup="javascript: this.value= this.value.toUpperCase();" maxlength="500"  style="width:80%"/></td> 
                     </tr> 
                     <tr class="estiloImputIzq2">
                       <td width="10%">MUNICIPIO</td>
                       <td width="70%">
                            <select size="1" id="cmbIdMun" style="width:80%"  >                                         
                              <option value=""></option>
                              <%resultaux.clear();
                                ComboVO cmbMun;
                                resultaux = (ArrayList) beanAdmin.combo.cargar(3571);
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmbMun = (ComboVO) resultaux.get(k);%>
                              <option value="<%= cmbMun.getId()%>" title="<%= cmbMun.getTitle()%>"><%=cmbMun.getDescripcion()%></option>
                              <%}%>
                            </select>
                        </td>
                     </tr>
                     <tr class="estiloImputIzq2">
                      <td width="10%">LOCALIDAD</td>
                      <td width="70%">
                        <select id="cmbIdLoc" style="width:80%" 
                          onfocus="cargarDepDesdeMunicipioGrupo2('cmbIdMun', 'cmbIdLoc')"
                          onchange=" limpiaAtributo('cmbIdBar', 0);">	                                        
                          <option value=0></option>
                        </select>	                   
                      </td>   
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="10%">BARRIO/VEREDA</td>
                      <td width="70%">
                        <select size="1" id="cmbIdBar" style="width:80%"  
                          onfocus="cargarDepDesdeLocalidadGrupo('cmbIdLoc', 'cmbIdBar')">	                                        
                          <option value=0></option>                                  
                        </select>	 
                        <label id="lblMarcador" >0</label>                  
                      </td>
                    </tr> 
                     <input type="hidden" id="txtMun"  style="width:90%"/>    
                     <tr class="estiloImputIzq2" style="display: none;">
                       <td width="30%"><label id="lblIdMagia" style="display:none;" >0</label><label id="lblIdArea" style="display:none;" >0</label><label id="lblIdZoom" style="display:none;" >0</label><label id="lblIdlatitud" style="display:none;" >0</label><label id="lblIdlongitud" style="display:none;" >0</label>
                        <label id="lblMarcador" >0</label>
                        <label id="lblIdGeo" style="display: none;">0</label></td>      
                     </tr>                                                                                                
                   </table> 
                 </td>   
               </tr>   
             </table>
             <table width="100%"  style="cursor:pointer" >
               <tr><td width="99%" class="titulos">
                 <input name="btn_limpiar_new" title="Permite limpiar Campos"  type="button" class="small button blue" value="LIMPIAR "   onclick="limpiaAtributo('txtId', 0); limpiaAtributo('txtDireccion', 0), limpiaAtributo('cmbIdMun', 0), limpiaAtributo('cmbIdLoc', 0), limpiaAtributo('cmbIdBar', 0)" />                  
                 <input name="btn_CREAR"       title="Permite crear Viviendas"          type="button" class="small button blue" value="CREAR LUGAR"     onclick="modificarCRUDParametrosModelo('crearLugar','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                 <input name="btn_MODIFICAR"   title="Permite eliminar Viviendas"  type="button" class="small button blue" value="ELIMINAR LUGAR" onclick="modificarCRUDParametrosModelo('eliminarLugar','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
               </td></tr>
             </table>  
           </td>
           </tr> 
         </table> 
         <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
           <tr>
             <td> 
             <div id="divParaViviendas"></div>     
             </td>
           </tr>   
         </table>  
       </td>
     </tr> 
   </table> 
 </td>
</tr>
</table> 
 

