<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="ASIGNAR PACIENTE" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>

            <div style="overflow:auto;height:320px; width:1050px" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <div id="divBuscar" style="display:block">
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                  <tr class="titulos" align="center">
                    <td width="20%">IDENTIFICACION </td>
                    <td width="40%">NOMBRE</td>
                    <td width="20%">MUNICIPIO</td> 
                    <td width="20%">&nbsp;</td>                                 
                  </tr>
                  <tr class="estiloImput">
                    <td><input type="text" id="txtBusId" style="width:90%"  onkeypress="javascript:return soloTelefono(event);"/> </td>
                    <td><input type="text" id="txtBusNombre" style="width:90%" onkeyup="javascript: this.value= this.value.toUpperCase();"  maxlength="80"  /> </td>
                    <td><input type="text" id="txtMunicipio" size="60"  maxlength="60"  style="width:70%"  /> 
                      <img width="18px" height="18px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                    <div id="divParaVentanita"></div>  </td>                   
                    <td>
                      <input name="btn_MODIFICAR" title="BT83t" type="button" class="small button blue" value="BUSCAR"
                        onclick="buscarPacientesGrupo('listGrillaPacientes')" />
                    </td>
                  </tr>
                </table>
                <table id="listGrillaPacientes" class="scroll"></table>
              </div>
            </div><!-- div contenido-->
            <div id="divEditar" style="display:block; width:100%">
              <table width="100%" cellpadding="0" cellspacing="0" align="center" id="infoPaciente">
                <tr class="titulosCentrados">
                  <td colspan="3">INFORMACION DEL PERSONAL
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td>TIPO IDENTIFICACION</td>
                              <td width="70%" colspan="2"><input type="text" id="txtTipoId" style="width:40%;height: 15px;" readonly  />
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%"> IDENTIFICACION</td>
                              <td width="70%" colspan="2"><input type="text" id="txtIdentificacion" style="width:40%;height: 15px;" readonly  />
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">NOMBRE PERSONAL</td>
                              <td width="70%" colspan="2"><input type="text" readonly style="width:40%;height: 15px;" onkeypress="return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" id="txtNomPersonal" />
                              </td>
                            </tr>                                              
                            <tr   class="estiloImputIzq2">                         
                              <td>CORREO ELECTRONICO</td><td>              
                              <input type="text" id="txtCorreo" style="width:40%;height: 15px;" onkeypress="javascript:return email(event);" />
                              </td>                                             
                            </tr>  
                            <tr   class="estiloImputIzq2">                         
                              <td>TELEFONO</td><td>              
                              <input type="text" id="txtTelefono" style="width:40%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                              </td>                                             
                            </tr>
                            <tr   class="estiloImputIzq2">                         
                              <td>CELULAR</td><td>              
                              <input type="text" id="txtCelular" style="width:40%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                              </td>                                             
                            </tr>              
                            <tr class="estiloImputIzq2">
                              <td width="10%">MUNICIPIO</td><td width="50%">
                                <select size="1" id="cmbMunicipioModelo"  style="width:50%"   >                         
                                  <%     resultaux.clear();
                                          resultaux=(ArrayList)beanAdmin.combo.cargar(614);    
                                          ComboVO cmb3; 
                                          for(int k=0;k<resultaux.size();k++){ 
                                              cmb3=(ComboVO)resultaux.get(k);
                                   %>
                                      <option value="<%= cmb3.getId()%>" title="<%= cmb3.getTitle()%>">
                                          <%= cmb3.getDescripcion()%></option>
                                      <%}%>                                  
                                </select>                        
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="10%">COMUNA/CORREGIMIENTO</td><td width="50%">
                                <select id="cmbLocalidadModelo" style="width:50%" 
                                  onfocus="cargarDepDesdeMunicipioGrupo2('cmbMunicipioModelo', 'cmbLocalidadModelo')"
                                  onchange=" limpiaAtributo('cmbBariioModelo', 0);">	                                        
                                  <option value=0></option>
                                </select>	                                                       
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="10%">BARRIO/VEREDA</td>
                              <td width="70%">
                                <select size="1" id="cmbBariioModelo" style="width:50%"  
                                  onfocus="cargarDepDesdeLocalidadGrupo('cmbLocalidadModelo', 'cmbBariioModelo')">	                                        
                                  <option value=0></option>                                  
                                </select>	                        
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td>DIRECCION</td><td>              
                                <input type="text" id="txtDireccion" style="width:40%;height: 15px;"  />
                                <input type="hidden" id="txtunico" />
                                <input type="hidden" id="txtIdPaciente" />                                
                              </td>
                            </tr>                           
                          </table>
                        </td>
                      </tr>
                    </table>
                    <table width="100%" style="cursor:pointer">
                      <tr>
                        <td width="99%" class="titulos">
                          <input name="btn_MODIFICAR" title="AD76" type="button" class="small button blue" value="ACTUALIZAR INFORMACION" onclick="actualizarDatosContactoModelo();" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table> 
              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulosCentrados">
                  <td colspan="3">GRUPOS
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td width="10%">GRUPO MULTIDISIPLINARIO</td>
                              <td width="35%">
                                <select size="1" id="cmbIdGrupo" style="width:70%"  onchange="cargaInfoGrupoParaPersonal()">
                                  <option value=""></option>
                                  <%   resultaux.clear();
                                        resultaux=(ArrayList)beanAdmin.combo.cargar(1271);	
                                        ComboVO cmbGR; 
                                        for(int k=0;k<resultaux.size();k++){ 
                                        cmbGR=(ComboVO)resultaux.get(k);
                                   %>
                                  <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                                    <%=cmbGR.getDescripcion()%></option>
                                  <%}%>
							                  </select>
                              </td >
                              <!-- div contenido 
                              <td width="10%"></td>-->
                              <td width="20%" > MUNICIPIO</td>
                              <td width="30%" ><input type="text" id="txtMunicipioGrupo" readonly style="width:70%" /></td>
                              <td width="5%"> <label id="lblIdGrupo"></label>
                              </td>
                            </tr>
				                    <tr   class="estiloImputIzq2"> 
				                      <td class="titulos" colspan ="5">                      
				                        <input name="btn_ASIGNAR" title="btn_ad755" type="button" class="small button blue" value="ASIGNAR GRUPO" onclick="modificarCRUDParametrosModelo('asignarPacienteGrupo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
				                        <input name="btn_DESVINCULAR" title="btn_mo785" type="button" class="small button blue" value="ADSCRIBIR GRUPO" onclick="modificarCRUDParametrosModelo('adcribirPacienteGrupo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
				                      </td>				  			  
				                    </tr>                                                       		           
                          </table> 
                          <table id="listGrillaGruposMulti" class="scroll"  width="100%" cellpadding="0" cellspacing="0"  ></table>  
		                    </td>   
		                  </tr>   
	                  </table>   
                  </td>
                </tr>
              </table>
            </div>
        </td>
       </tr>
      </table>

   </td>
 </tr> 
 
</table>

<input type="hidden" id="lblFirmaProfesional" /> 