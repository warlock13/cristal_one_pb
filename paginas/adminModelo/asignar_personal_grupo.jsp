<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="ASIGNAR PERSONAL" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <div style="overflow:auto;height:320px; width:1050px" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
              <div id="divBuscar" style="display:block">
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                  <tr class="titulos" align="center">
                    <td width="10%">IDENTIFICACION </td>
                    <td width="35%">NOMBRE</td>
                    <td width="35%">SEDE PRINCIPAL</td>                    
                    <td width="10%">VIGENTE</td>                    
                  </tr>
                  <tr class="estiloImput">
                    <td><input type="text" id="txtBusId" style="width:90%"  onkeypress="javascript:return soloTelefono(event);"/> </td>
                    <td><input type="text" id="txtBusNombre" style="width:90%" onkeyup="javascript: this.value= this.value.toUpperCase();"  maxlength="80"  /> </td>
                    <td>
                      <select size="1" id="cmbSE" style="width:80%"  title="576" >                                         
                        <option value=""></option>
                            <%     resultaux.clear();
                              resultaux=(ArrayList)beanAdmin.combo.cargar(963);  
                              ComboVO cmbSE; 
                              for(int k=0;k<resultaux.size();k++){ 
                              cmbSE=(ComboVO)resultaux.get(k);
                             %>
                              <option value="<%= cmbSE.getId()%>" title="<%= cmbSE.getTitle()%>">
                              <%=cmbSE.getDescripcion()%></option>
                              <%} %>
                        </select>
                    </td>                    
                    <td><select size="1" id="cmbVigenteBus" style="width:80%" >
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                      </select>
                    </td>
                    <td>
                      <input name="btn_MODIFICAR" title="BT83t" type="button" class="small button blue" value="BUSCAR"
                        onclick="buscarPersonalGrupo('listGrillaUsuarios')" />
                    </td>
                  </tr>
                </table>
                <table id="listGrillaUsuarios" width="60%"></table>
              </div>
            </div><!-- div contenido-->
            <div id="divEditar" style="display:block; width:100%">
              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulosCentrados">
                  <td colspan="3">INFORMACION DEL PERSONAL
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td>TIPO IDENTIFICACION</td>
                              <td width="70%" colspan="2"><input type="text" id="txtTipoId" readonly  style="width:60%" />
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%"> IDENTIFICACION</td>
                              <td width="70%" colspan="2"><input type="text" id="txtIdentificacion" readonly  style="width:60%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">NOMBRE PERSONAL</td>
                              <td width="70%" colspan="2"><input type="text" readonly   id="txtNomPersonal" style="width:60%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">PROFESION</td>
                              <td width="70%" colspan="2"><input type="text" id="txtProfesion" readonly  style="width:60%" />
                              </td> 
                            </tr>                                             
                            <tr   class="estiloImputIzq2">                         
                              <td width="30%">CORREO ELECTRONICO</td>
                              <td width="70%" colspan="2"><input type="text" id="txtCorreo" style="width:60%" onkeypress="javascript:return email(event);" />
                              </td>                                             
                            </tr>  
                            <tr   class="estiloImputIzq2">                         
                              <td width="30%">TELEFONO</td>
                              <td width="70%" colspan="2"><input type="text" id="txtPhone" style="width:60%" onkeypress="javascript:return soloTelefono(event);" />
                              </td>                                             
                            </tr>          
                            <tr class="estiloImputIzq2">
                              <td width="30%">SEDE PRINCIPAL</td><td width="70%" colspan="2">
                                <select size="1" id="cmbSede" style="width:60%"  title="576" >                                         
                                  <option value=""></option>
                                  <%     resultaux.clear();
                                    resultaux=(ArrayList)beanAdmin.combo.cargar(963);  
                                    ComboVO cmbBod; 
                                    for(int k=0;k<resultaux.size();k++){ 
                                    cmbBod=(ComboVO)resultaux.get(k);
                                  %>
                                  <option value="<%= cmbBod.getId()%>" title="<%= cmbBod.getTitle()%>">
                                  <%=cmbBod.getDescripcion()%></option>
                                  <%} %>
                                </select>
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="30%">TRABAJA ACTUALMENTE</td>
                              <td width="70%" colspan="2"><select size="1" id="cmbEstado" style="width:60%" >

                                  <option value="1">SI</option>
                                  <option value="0">NO</option>
                                </select></td>             
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <table width="100%" style="cursor:pointer">
                      <tr>
                        <td width="99%" class="titulos">
                          <input name="btn_limpiar_new" type="button" title="btn_lm784" class="small button blue"
                            value="LIMPIAR CAMPOS" onclick="limpiarDivEditarJuan('limpiarCrearUsuario')" />
                          <input name="btn_MODIFICAR" title="AD76" type="button" class="small button blue"
                            value="MODIFICAR PERSONAL"
                            onclick="modificarCRUDParametrosModelo('modificarPersonalDesdeModelo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />  
                        </td>                       
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="titulosCentrados">
                  <td colspan="3">GRUPOS
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                      <tr>
                        <td>
                          <table width="100%">
                            <tr class="estiloImputIzq2">
                              <td width="10%">GRUPO MULTIDISIPLINARIO</td>
                              <td width="35%">
                                <select size="1" id="cmbIdGrupo" style="width:70%"  onchange="cargaInfoGrupoParaPersonal()">
                                  <option value=""></option>
                                  <%   resultaux.clear();
                                        resultaux=(ArrayList)beanAdmin.combo.cargar(1271);	
                                        ComboVO cmbGR; 
                                        for(int k=0;k<resultaux.size();k++){ 
                                        cmbGR=(ComboVO)resultaux.get(k);
                                   %>
                                  <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                                    <%=cmbGR.getDescripcion()%></option>
                                  <%}%>
							                  </select>
                              </td >
                              <!-- div contenido 
                              <td width="10%"></td>-->
                              <td width="10%">ROL </td>
                              <td width="35%">
                                <select size="1" id="cmbIdRol" style="width:80%"  	>
                                  <option value=""></option>
                                  <%   resultaux.clear();
                                        resultaux=(ArrayList)beanAdmin.combo.cargar(929);	
                                        ComboVO cmbROL; 
                                        for(int k=0;k<resultaux.size();k++){ 
                                        cmbGR=(ComboVO)resultaux.get(k);
                                   %>
                                  <option value="<%= cmbGR.getId()%>" title="<%= cmbGR.getTitle()%>">
                                    <%=cmbGR.getDescripcion()%></option>
                                  <%}%>
							                  </select>
                              </td >        
                              <td width="5%"> <label id="lblIdGrupo"></label>
                              </td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="20%" > MUNICIPIO</td>
                              <td width="30%" colspan="4"><input type="text" id="txtMunicipioGrupo" readonly style="width:30%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="20%">LIDER</td>
                              <td width="30%" colspan="4"><input type="text" readonly  id="txtLiderGrupo" style="width:30%" /></td>
                            </tr>
                            <tr class="estiloImputIzq2">
                              <td width="20%">VIGENTE</td>
                              <td width="30%" colspan="4"><input type="text" id="txtVigente" readonly  style="width:30%" />
                              </td> 
                            </tr> 
				                    <tr   class="estiloImputIzq2"> 
				                      <td class="titulos" colspan ="5">                      
				                        <input name="btn_ASIGNAR" title="btn_ad755" type="button" class="small button blue" value="ASIGNAR GRUPO" onclick="modificarCRUDParametrosModelo('asignarPersonalGrupo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
				                        <input name="btn_DESVINCULAR" title="btn_mo785" type="button" class="small button blue" value="DESVINCULAR GRUPO" onclick="modificarCRUDParametrosModelo('eliminarPersonalGrupo','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
				                      </td>				  			  
				                    </tr>                                                       		           
                          </table> 
                          <table id="listGrillaGruposMulti" class="scroll"  width="100%" cellpadding="0" cellspacing="0"  ></table>  
		                    </td>   
		                  </tr>   
	                  </table>   
                  </td>
                </tr>
              </table>
            </div><!-- divEditar--> 
          </td>
        </tr>
      </table>
    </td>
  </tr> 
</table>
<input type="hidden" id="lblFirmaProfesional" /> 