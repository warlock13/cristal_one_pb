<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%  beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1350"  align="center" cellpadding="0" cellspacing="0" >
    <tr>
        <td>
            <div align="center" id="tituloForma" >
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="GEOREFERENCIACION" />
                </jsp:include>
            </div>  
        </td>
    </tr> 
    <tr>
      <td valign="top">    
        <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0" >  
          <tr class="titulos" >
             <td width="60%">
               <table width="102%" height='70px' class="fondoTabla" cellpadding="0" cellspacing="0" >
                <tr>
                  <td width="70%">Municipio</td>
                  <td width="20%">&nbsp;</td> 
                </tr>
                <tr>
                  <td>                   
                    <input type="text" id="txtMunicipio" size="60" maxlength="60"  style="width:70%"  /> 
                    <img width="18px" height="20px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id,'','divParaVentanita','txtMunicipio','1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                  <div id="divParaVentanita"></div>                    
                 </td> 
                  <td>
                    <label id="lblIdArea" style="display: none;">0</label>
                    <label id="lblIdlatitud" style="display: none;">0</label>
                    <label id="lblIdlongitud" style="display: none;">0</label>
                    <label id="lblIdZoom" style="display: none;">0</label>
                    <label id="txtColor" style="display: none;">0</label>
                    <label id="txtColorOpacidad" style="display: none;">0</label>
                    <label id="lblTipoMarcador" style="display: none;">0</label>
                    <label id="lblIdGeo" style="display: none;">1</label>
                    <label id="txtId" style="display: none;">0</label>
                    <input title="GE89" type="button" class="small button blue" value="BUSCAR AREAS" onclick="cargaPuntoMunicipio()"/>
                 </td> 
                </tr>
               </table>
             </td>
             <td rowspan="2" width="100%" height='1030px' valign="top">
               <table table width="100%" height='1030px' class="fondoTabla" cellpadding="0" cellspacing="0" >
                 <tr class="titulos">
                   <td valign="top">
                    <input title="GE93" type="button" class="small button blue" value="GRUPOS" onclick="apareceEstadisticosGrupos()" style="margin-top: 20px; margin-bottom: 21px;"/>
                    </td>
                    <td valign="top">
                    <input title="GE94" type="button" class="small button blue" value="MARCADORES" onclick="apareceEstadisticosMarcadores()" style="margin-top: 20px; margin-bottom: 22px;"/>
                   </td>
                 </tr>
                 <tr class="titulos">
                   <td colspan="2">
                    <table width="100%" height='986px' class="fondoTabla" cellpadding="0" cellspacing="0" id="tablaMarcadoresGeo" style="display: none;" >  
                      <tr class="titulos">
                         <td valign="top"   height='300px'>  
                            <table id="listEstadisticoMarcadores"  width="550%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                         </td> 
                      </tr>  
                      <tr class="titulos">
                        <td valign="top"   height='200px'>  
                           <table id="listMarcadoresEspeciicos"  width="100%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                        </td> 
                     </tr>
                     <tr><td>&nbsp;</td></tr>                           
                    </table> 
                    <table width="100%" height='986px' class="fondoTabla" cellpadding="0" cellspacing="0" id="tablaGruposGeo" style="display: none;" >  
                      <tr class="titulos">
                         <td valign="top"   height='300px'>  
                            <table id="listEstadisticoGruposGeo"  width="550%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                         </td> 
                      </tr>  
                      <tr class="titulos">
                        <td valign="top"   height='200px'>  
                           <table id="listEstadisticoGruposEspecifico"  width="100%" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                        </td> 
                     </tr>
                     <tr><td>&nbsp;</td></tr>                           
                    </table> 
                   </td>
                 </tr>
               </table>                          
            </td>   
          </tr>        
          <tr class="titulos">
            <td height="990px" width="650px"  valign="top" >
                <table id="idTableContenedorMapa" style="height: 100%; width: 100%" class="scroll" cellpadding="0" cellspacing="0" align="center">
                   <tr>
                    <td  valign="top"  style="height: 100%; width: 100%" id="idTableContenedorMapaTd">
                     <div id="mapaGeoArea" style="height: 100%; width: 100%"></div>
                   </td>
                 </tr> 
                </table>              
            </td>                            
          </tr>  
        </table>   
      </td>    
    </tr>        
</table>