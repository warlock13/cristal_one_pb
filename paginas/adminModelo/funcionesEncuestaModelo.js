
function ubicarMapa() {
    cargaPuntoLatitudLongitud('lblIdlongitud', 'lblIdlatitud', 'lblIdArea', valorAtributo('lblIdPaciente'), 842)
}

function setearMunicipio(municipio) {
    console.log(municipio);
    switch (municipio) {

        case '52001':
            asignaAtributo('lblIdArea', '52001', 0);
            asignaAtributo('lblIdlatitud', '1.21150', 0);
            asignaAtributo('lblIdlongitud', '-77.27998', 0);
            asignaAtributo('txtArea', 'MUNICIPIO DE PASTO', 0);
            asignaAtributo('txtOrden', '1', 0);
            asignaAtributo('txtLatitud', '1.21150', 0);
            asignaAtributo('txtLongitud', '-77.27998', 0);
            document.getElementById('lblIdZoom').value = '11';
            asignaAtributo('lblIdZoom', '13', 0);
            asignaAtributo('txtColor', '000ffe', 0);
            asignaAtributo('txtColorOpacidad', '0.2', 0);
            asignaAtributo('cmbVigente', 'S', 0);

            centroPuntoDelMapa()
            break;

        case '52838':
            asignaAtributo('lblIdArea', '52838', 0);
            asignaAtributo('lblIdlatitud', '1.14482', 0);
            asignaAtributo('lblIdlongitud', '-77.62029', 0);
            asignaAtributo('txtArea', 'MUNICIPIO DE TUQUERRES', 0);
            asignaAtributo('txtOrden', '1', 0);
            asignaAtributo('txtLatitud', '1.14482', 0);
            asignaAtributo('txtLongitud', '-77.62029', 0);
            document.getElementById('lblIdZoom').value = '11';
            asignaAtributo('lblIdZoom', '12', 0);
            asignaAtributo('txtColor', 'fe0000', 0);
            asignaAtributo('txtColorOpacidad', '0.1', 0);
            asignaAtributo('cmbVigente', 'S', 0);

            centroPuntoDelMapa()
            break;
        case '76001':
            asignaAtributo('lblIdArea', '76001', 0);
            asignaAtributo('lblIdlatitud', '3.44762', 0);
            asignaAtributo('lblIdlongitud', '-76.53008', 0);
            asignaAtributo('txtArea', 'CALI VALLE', 0);
            asignaAtributo('txtOrden', '1', 0);
            asignaAtributo('txtLatitud', '3.44762', 0);
            asignaAtributo('txtLongitud', '-76.53008', 0);
            document.getElementById('lblIdZoom').value = '11';
            asignaAtributo('lblIdZoom', '12', 0);
            asignaAtributo('txtColor', 'fe0000', 0);
            asignaAtributo('txtColorOpacidad', '0.1', 0);
            asignaAtributo('cmbVigente', 'S', 0);

            centroPuntoDelMapa()
            break;


    }
}

function ubicarMapaMunicipio() {
    cargaPuntoLatitudLongitud('lblLatitudMuni', 'lblLongitudMuni', 'lblIdArea', valorAtributo('lblIdArea'), 846)
}


function recargarMapa() {

    limpTablasDiv('idTableContenedorMapaTd')


    contenedorDiv = document.getElementById('idTableContenedorMapaTd');
    cuerpoDiv = document.createElement('DIV');  // se crean los div hijos pa cada diente
    cuerpoDiv.id = 'mapaGeoArea'
    cuerpoDiv.style.height = '90%';
    cuerpoDiv.style.width = '100%';
    contenedorDiv.appendChild(cuerpoDiv);
}

var mymap;
function centroPuntoDelMapa() {
    recargarMapa()


    var estiloPopup = { 'maxWidth': '300' }
    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [5, 8], popupAnchor: [2, -6] } });
    console.log('Zoom:' + valorAtributo('lblIdZoom'));

    mymap = L.map('mapaGeoArea').setView([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], valorAtributo('lblIdZoom'));
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);

    mymap.on('click', function (e) {
        asignaAtributo('txtLatitudReferencia', e.latlng.lat.toString(), 0);
        asignaAtributo('txtLongitudReferencia', e.latlng.lng.toString(), 0);
    });

    urlIconoCentroPunto = 'https://cristalweb.emssanar.org.co:8383/clinica/utilidades/imagenes/marcadores/CentroPunto.png'

    L.marker([valorAtributo('lblIdlatitud'), valorAtributo('lblIdlongitud')], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup('Centro Area', estiloPopup).addTo(mymap);

    cargaLineasDelArea(valorAtributo('lblIdArea'), 844)
    //------------MAPA TUQUERES-----------------------------------------------------------------------------
    //var imageUrl = 'http://190.60.242.160:8372/clinica/utilidades/imagenes/mapas/mapaTuquerres.jpg',
    //imageBounds = [[1.2983355281519715, -77.72947311401369], [0.9852877385504372, -77.50416755676271]];
    //L.imageOverlay(imageUrl, imageBounds).setOpacity(0.5).addTo(mymap);
    //------------------------------------------------------------------------------------------------------

}

function cargaLineasDelArea(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaLineasDelArea(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaLineasDelArea(condicion1) {

    var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [5, 8], popupAnchor: [2, -6] } });
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            totalRegistros = raiz.getElementsByTagName('id').length

            var polygonPoints = Array.from(Array(totalRegistros), () => new Array(2));

            for (i = 0; i < totalRegistros; i++) {
                latitud = raiz.getElementsByTagName('id')[i].firstChild.data
                longitud = raiz.getElementsByTagName('nom')[i].firstChild.data

                polygonPoints[i][0] = latitud
                polygonPoints[i][1] = longitud

                titulo = 'Orden=' + (i + 1)
                fabricaMarcador(latitud, longitud, titulo, 'Lat=' + latitud + '  lon=' + longitud, mymap)
            }

            L.polygon(
                polygonPoints,
                {
                    weight: 2, /*grosor de la linea*/
                    lineJoin: "miter",
                    color: "blue",
                    fillColor: "#" + valorAtributo('txtColor'),  /* color de llenar fillColor: "none",*/
                    fillOpacity: valorAtributo('txtColorOpacidad')
                }
            ).addTo(mymap)

            cargaMarcadoresDelArea(valorAtributo('lblIdArea'), 843)

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}
function fabricaMarcador(latitud, longitud, titulo, textoPopup, mymap) {
    var estiloPopup = { 'maxWidth': '100' }
    urlIconoCentroPunto = 'https://cristalweb.emssanar.org.co:8383/clinica/utilidades/imagenes/marcadores/Vertice.png'
    var iconoBase = L.Icon.extend({ options: { iconSize: [5, 5], iconAnchor: [3, 3], popupAnchor: [0, -3] } });

    var textoPopup = '<b>' + titulo + '</b><br/>' + textoPopup;

    L.marker([latitud, longitud], { icon: new iconoBase({ iconUrl: urlIconoCentroPunto }) }).bindPopup(textoPopup, estiloPopup).addTo(mymap);
}

function cargaMarcadoresDelArea(condicion1, idQueryCombo) {  //  alert(  condicion1+'::'+idQueryCombo )
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaMarcadoresDelArea(condicion1) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaMarcadoresDelArea(condicion1) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;

            var estiloPopup = { 'maxWidth': '300' }
            var iconoBase = L.Icon.extend({ options: { iconSize: [15, 15], iconAnchor: [5, 8], popupAnchor: [2, -6] } });


            for (i = 0; i < raiz.getElementsByTagName('id').length; i++) {

                marcador = raiz.getElementsByTagName('id')[i].firstChild.data
                marcador_tipo = raiz.getElementsByTagName('nom')[i].firstChild.data

                var m = marcador.split('-_');
                var id_marcador = m[0];
                var latitud = m[1];
                var longitud = m[2];
                var descripcion = m[3];

                var mt = marcador_tipo.split('-_');
                var id_marcador_tipo = mt[0];

                var textoPopup = '<b>' + m[0] + ' - ' + mt[1] + '</b><br/>' + m[3];
                var urlIcono = mt[2];
                L.marker([latitud, longitud], { icon: new iconoBase({ iconUrl: urlIcono }) }).bindPopup(textoPopup, estiloPopup).addTo(mymap);
            }

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function cargaPuntoLatitudLongitud(idLabelDestino1, idLabelDestino2, idLabelDestino3, condicion1, idQueryCombo) {
    valores_a_mandar = "idQueryCombo=" + idQueryCombo + "&cantCondiciones=uno&condicion1=" + condicion1;
    varajax1 = crearAjax();
    varajax1.open("POST", '/clinica/paginas/accionesXml/cargarComboGRAL_xml.jsp', true);
    varajax1.onreadystatechange = function () { respuestacargaPuntoLatitudLongitud(idLabelDestino1, idLabelDestino2, idLabelDestino3) };
    varajax1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    varajax1.send(valores_a_mandar);
}

function respuestacargaPuntoLatitudLongitud(idLabelDestino1, idLabelDestino2, idLabelDestino3) {
    if (varajax1.readyState == 4) {
        if (varajax1.status == 200) {
            raiz = varajax1.responseXML.documentElement;
            limpiaAtributo(idLabelDestino1);
            limpiaAtributo(idLabelDestino2);
            limpiaAtributo(idLabelDestino3);

            asignaAtributo(idLabelDestino1, raiz.getElementsByTagName('id')[0].firstChild.data)
            asignaAtributo(idLabelDestino2, raiz.getElementsByTagName('nom')[0].firstChild.data)
            asignaAtributo(idLabelDestino3, raiz.getElementsByTagName('title')[0].firstChild.data)

            centroPuntoDelMapa()

        } else {
            swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
        }
    }
    if (varajax1.readyState == 1) {
        // document.getElementById('inicial').innerHTML =   crearMensaje();
    }
}

function georeferenciarPaciente() {  //alert((id+':: '+name+' :: '+value )) 
    mostrar('divVentanitaGeoPaciente')
    ubicarMapa()
}

function modificarMapCRUD(arg, pag) {
    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;

    switch (arg) {
        case 'eliminarArea':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=803&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                ajaxModificar();
            }
            break;
        case 'crearAreaMapa':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=799&parametros=";
                add_valores_a_mandar(valorAtributo('txtArea'));
                add_valores_a_mandar(valorAtributo('txtOrden'));
                add_valores_a_mandar(valorAtributo('txtLatitud'));
                add_valores_a_mandar(valorAtributo('txtLongitud'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));
                add_valores_a_mandar(valorAtributo('cmbVigente'))
                add_valores_a_mandar(document.getElementById('lblIdZoom').value)
                add_valores_a_mandar(valorAtributo('txtColor'))
                add_valores_a_mandar(valorAtributo('txtColorOpacidad'))
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            }
            break;
        case 'modificarAreaMapa':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1022&parametros=";
                add_valores_a_mandar(valorAtributo('txtArea'));
                add_valores_a_mandar(valorAtributo('txtOrden'));
                add_valores_a_mandar(valorAtributo('txtLatitud'));
                add_valores_a_mandar(valorAtributo('txtLongitud'));
                add_valores_a_mandar(valorAtributo('txtMunicipio').split("-", 1));
                add_valores_a_mandar(valorAtributo('cmbVigente'))
                add_valores_a_mandar(document.getElementById('lblIdZoom').value)
                add_valores_a_mandar(valorAtributo('txtColor'))
                add_valores_a_mandar(valorAtributo('txtColorOpacidad'))
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                ajaxModificar();
            }
            break;
        case 'adicionarReferencia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=802&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
                nuevo_orden = parseInt(valorAtributo('txtOrdenReferencia')) + 1;
                asignaAtributo('txtOrdenReferencia', nuevo_orden, 0);
            }
            break;
        case 'modificarReferencia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=801&parametros=";

                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                ajaxModificar();
            }
            break;
        case 'eliminarReferencia':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1021&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(valorAtributo('txtOrdenReferencia'));
                ajaxModificar();
            }
            break;
        case 'crearEncuestaPaciente':

            if (document.getElementById('cmbEncuestaParaCrear').value == 56) {
                var id = valorAtributo('lblIdPaciente');
                var url = 'http://10.0.0.3:9010/pacienteFamilia/' + id
                $.ajax({
                    url: url,
                    dataType: 'json',
                    success: function (object) {
                        if (object[0] == null) {
                            alert('EL PACIENTE NO PERTENECE A UNA FAMILIA.');
                            return false;
                        } else {
                            if (verificarCamposGuardar(arg)) {
                                valores_a_mandar = 'accion=' + arg;
                                valores_a_mandar = valores_a_mandar + "&idQuery=888&parametros=";
                                add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                                add_valores_a_mandar(document.getElementById('cmbEncuestaParaCrear').value);
                                add_valores_a_mandar(valorAtributo('txtIdMedioRecepcion')); // 3 = hiscotria clinica // 2 = linea de frente
                                add_valores_a_mandar(document.getElementById('cmbTipoRecepccionEnc').value);
                                add_valores_a_mandar(LoginSesion());
                                add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
                                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                                add_valores_a_mandar(document.getElementById('cmbEncuestaParaCrear').value);
                                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                                add_valores_a_mandar(document.getElementById('cmbEncuestaParaCrear').value);
                                ajaxModificar();
                            }
                        }
                    }
                });
            } else {
                if (verificarCamposGuardar(arg)) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=888&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdPaciente'));
                    add_valores_a_mandar(document.getElementById('cmbEncuestaParaCrear').value);
                    add_valores_a_mandar(valorAtributo('txtIdMedioRecepcion')); // 3 = hiscotria clinica // 2 = linea de frente
                    add_valores_a_mandar(document.getElementById('cmbTipoRecepccionEnc').value);
                    add_valores_a_mandar(LoginSesion());
                    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'));
                    add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                    add_valores_a_mandar(document.getElementById('cmbEncuestaParaCrear').value);
                    add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                    add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
                    add_valores_a_mandar(document.getElementById('cmbEncuestaParaCrear').value);
                    ajaxModificar();
                }
            }

            break;

        case 'eliminarEncuestaPaciente':
            if (verificarCamposGuardar(arg)) {
                if (confirm("Esta seguro de eliminar esta encuesta")) {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=1300&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdEncuestaPaciente'));
                    ajaxModificar();
                } else {
                    return
                }

            }
            break;

        case 'cerrarEncuestaPaciente':
            let idEncuesta = traerDatoFilaSeleccionada('listEncuestaPaciente', 'id_encuesta')
            let tipo = ''
            switch (idEncuesta) {
                case '36':
                    tipo = 'ead_ob'
                    break;

                case '47':
                    if (valorAtributo('lblGeneroPaciente') != '') {
                        let sexo = valorAtributo('lblGeneroPaciente')
                        if (sexo == 'M') { tipo = 'tablas_framingham_ob_m' }
                        if (sexo == 'F') { tipo = 'tablas_framingham_ob_f' }
                    }
                    else {
                        let sexo = valorAtributo('cmbSexo')
                        if (sexo == 'M') { tipo = 'tablas_framingham_ob_m' }
                        if (sexo == 'F') { tipo = 'tablas_framingham_ob_f' }
                    }
                    break;

                case '39':
                    tipo = 'assist_c'
                    break;

                case '37':
                    tipo = 'vale_cpp'
                    break;

                default:
                    break;
            }
            if (verificarCamposGuardar(arg)) {
                if (tipo != '') {
                    tabsSubContenidoEncuesta(tipo, 'desdeEncuesta').then(() => {
                        valores_a_mandar = 'accion=' + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=883&parametros=";
                        add_valores_a_mandar(valorAtributo('lblIdEncuestaPaciente'));
                        ajaxModificar();
                    });
                }
                else {
                    valores_a_mandar = 'accion=' + arg;
                    valores_a_mandar = valores_a_mandar + "&idQuery=883&parametros=";
                    add_valores_a_mandar(valorAtributo('lblIdEncuestaPaciente'));
                    ajaxModificar();
                }
            }


            break;
        case 'adicionarMarcador':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1026&parametros=";
                add_valores_a_mandar(valorAtributo('cmbTipoMarcador'));
                add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtDescripcionMarcador'));
                add_valores_a_mandar(valorAtributo('lblIdArea'));
                add_valores_a_mandar(IdSesion());
                ajaxModificar();
            }
            break;
        case 'modificarMarcador':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1028&parametros=";

                add_valores_a_mandar(valorAtributo('cmbTipoMarcador'));
                add_valores_a_mandar(valorAtributo('txtLatitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtLongitudReferencia'));
                add_valores_a_mandar(valorAtributo('txtDescripcionMarcador'));
                add_valores_a_mandar(valorAtributo('txtIdMarcador'));
                ajaxModificar();
            }
            break;
        case 'eliminarMarcador':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=1027&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdMarcador'));
                ajaxModificar();
            }
            break;

    }
}

/**
 * funcion para copiar datos de una plantilla a una plantilla de encuestaTamizaje  
 * @author Miguel Pasaje <miguelandrespasajeurbano@gmail.com>
 */
function traerDatosPlantillaAEncuesta(param) {

    switch (param) {
        case 'traerDatosAEncuesta11':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2001&parametros=";
            let idEvolucion = traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO')
            let idEncuestaPaciente = $("#listEncuestaPaciente").jqGrid("getCell", 1, "id");
            add_valores_a_mandar(idEncuestaPaciente);
            add_valores_a_mandar(idEvolucion);
            ajaxModificar();
            break;

        default:
            break;
    }

}

function respuestaMapCRUD(arg, xmlraiz) {

    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'crearAreaMapa':
                buscarEncuesta('listMunicipioAreas')
                alert('Se ha creado el Area');
                break;
            case 'modificarAreaMapa':
                buscarEncuesta('listMunicipioAreas');
                alert('Modificacion Exitosa');
                break;
            case 'eliminarArea':
                buscarEncuesta('listMunicipioAreas');
                alert('Se elimino el Area');
                break;
            case 'adicionarReferencia':
                buscarEncuesta('listAreasRerefencias')
                setTimeout("ubicarMapaMunicipio()", 1000)
                break;
            case 'modificarReferencia':
                buscarEncuesta('listAreasRerefencias')
                setTimeout("ubicarMapaMunicipio()", 1000)
                break;
            case 'eliminarReferencia':
                buscarEncuesta('listAreasRerefencias')
                setTimeout("ubicarMapaMunicipio()", 1000)
                break;
            case 'crearEncuestaPaciente':

                alert('ENCUESTA CREADA EXITOSAMENTE');
                //  arg = 'listEncuestaPaciente';ç
                paginaActual = '';
                buscarEncuesta('listEncuestaPaciente');
                if (document.getElementById('cmbEncuestaParaCrear').value == 11) {
                    setTimeout(() => {
                        traerDatosPlantillaAEncuesta('traerDatosAEncuesta11')
                    }, 1000);
                }


                break;

            case 'eliminarEncuestaPaciente':
                alert('Encuesta Elimina Correctamente');
                limpiaAtributo('lblIdEncuestaPaciente');
                ocultar('rowSemaforoRiesgo');
                ocultar('divParaTabsContenidoEncuesta');
                buscarEncuesta('listEncuestaPaciente');
                break;

            case 'cerrarEncuestaPaciente':
                if (traerDatoFilaSeleccionada('listEncuestaPaciente', 'id_encuesta') == 47 && ventanaActual.opc == 'principalHC' && valorAtributo('lblMesesEdadPaciente') >= 12) {
                    valores_a_mandar = '';
                    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'))
                    add_valores_a_mandar(traerDatoFilaSeleccionada('listDocumentosHistoricos', 'ID_FOLIO'))
                    $.ajax({
                        url: "/clinica/paginas/accionesXml/buscarGrilla_xml.jsp",
                        type: "POST",
                        data: {
                            "idQuery": 3044,
                            "parametros": valores_a_mandar,
                            "_search": false,
                            "nd": 1611873687449,
                            "rows": -1,
                            "page": 1,
                            "sidx": "NO FACT",
                            "sord": "asc"
                        },
                        beforeSend: function () { },
                        success: function (data) {
                            respuesta = data.getElementsByTagName("cell");
                            console.log(respuesta)
                            asignaAtributo("txtc1Rc", respuesta[1].firstChild.data, 1);
                            guardarDatosPlantilla('vihd', 'c14', respuesta[1].firstChild.data)
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert('ERROR EN LA PETICION. POR FAVOR COMUNIQUESE CON SOPORTE');
                        }
                    });
                }
                //alert('ENCUESTA CERRADA CORRECTAMENTE ');
                paginaActual = '';
                //arg = 'listEncuestaPaciente';
                tabsContenidoEncuesta('desdeEncuesta');
                buscarEncuesta('listEncuestaPaciente');

                //colocarRiesgoEncuesta('SIN RIESGO','1');

                break;
            case 'adicionarMarcador':
                alert("Marcador agregado exitosamente");
                buscarGrillaMapa('listAreasRerefenciasMarcadores');
                setTimeout("ubicarMapaMunicipio()", 1000);
                break;
            case 'modificarMarcador':
                alert("Marcador modificado exitosamente");
                buscarGrillaMapa('listAreasRerefenciasMarcadores');
                setTimeout("ubicarMapaMunicipio()", 1000);
                break;
            case 'eliminarMarcador':
                alert("El Marcador fue eliminado");
                buscarGrillaMapa('listAreasRerefenciasMarcadores');
                setTimeout("ubicarMapaMunicipio()", 1000);
                break;
        }

    }
    else console.log('NO SE LOGRO GUARDAR, VERIFIQUE CONEXIÓN CON SERVIDOR')

}


function buscarEncuesta(arg) {

    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    switch (arg) {
        case 'listAreasRerefencias':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=800&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdArea'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Orden', 'latitud', 'longitud'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'Orden', index: 'Orden', width: anchoP(ancho, 20) },
                    { name: 'latitud', index: 'latitud', width: anchoP(ancho, 40) },
                    { name: 'longitud', index: 'longitud', width: anchoP(ancho, 40) },
                ],
                height: 202,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtOrdenReferencia', datosRow.Orden, 0);
                    asignaAtributo('txtLatitudReferencia', datosRow.latitud, 0);
                    asignaAtributo('txtLongitudReferencia', datosRow.longitud, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
        case 'listMunicipioAreas':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=798&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtMunicipio'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'id_municipio', 'nom_municipio', 'latitud_muni', 'longitud_muni', 'id_area', 'Nombre Area', 'Orden', 'latitud', 'longitud', 'zoom', 'color', 'color_opacidad', 'vigente'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id_municipio', index: 'id_municipio', width: anchoP(ancho, 10) },
                    { name: 'nom_municipio', index: 'nom_municipio', width: anchoP(ancho, 10) },
                    { name: 'latitud_muni', index: 'latitud_muni', hidden: true },
                    { name: 'longitud_muni', index: 'longitud_muni', hidden: true },
                    { name: 'id_area', index: 'id_area', width: anchoP(ancho, 5) },
                    { name: 'NombreArea', index: 'Nombre', width: anchoP(ancho, 60) },
                    { name: 'Orden', index: 'Orden', width: anchoP(ancho, 5) },
                    { name: 'latitud', index: 'latitud', width: anchoP(ancho, 10) },
                    { name: 'longitud', index: 'longitud', width: anchoP(ancho, 10) },
                    { name: 'zoom', index: 'zoom', hidden: true },
                    { name: 'color', index: 'color', hidden: true },
                    { name: 'color_opacidad', index: 'color_opacidad', hidden: true },
                    { name: 'vigente', index: 'vigente', hidden: true },
                ],
                height: 202,
                width: ancho,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

                    asignaAtributo('txtMunicipio', datosRow.id_municipio + '-' + datosRow.nom_municipio, 0);
                    asignaAtributo('lblIdArea', datosRow.id_area, 0);
                    asignaAtributo('lblIdlatitud', datosRow.latitud, 0);
                    asignaAtributo('lblIdlongitud', datosRow.longitud, 0);
                    asignaAtributo('txtArea', datosRow.NombreArea, 0);
                    asignaAtributo('txtOrden', datosRow.Orden, 0);
                    asignaAtributo('txtLatitud', datosRow.latitud, 0);
                    asignaAtributo('txtLongitud', datosRow.longitud, 0);
                    document.getElementById('lblIdZoom').value = datosRow.zoom;
                    asignaAtributo('lblIdZoom', datosRow.zoom, 0);
                    asignaAtributo('txtColor', datosRow.color, 0);
                    asignaAtributo('txtColorOpacidad', datosRow.color_opacidad, 0);
                    asignaAtributo('cmbVigente', datosRow.vigente, 0);

                    ubicarMapaMunicipio()
                    setTimeout("buscarEncuesta('listAreasRerefencias')", 2000)

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listEncuestaPaciente':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
            valores_a_mandar = valores_a_mandar + "?idQuery=877&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));

            //alert(valorAtributo('lblTipoDocumento'))
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'Encuesta', 'Diligenciado', 'id_encuesta', 'id_estado_encuesta', 'Alertas',
                    'Fecha de cierre', 'Estado admision', 'Medio recepcion', 'Abierta por', 'Edad meses actual', 'Fecha de creacion'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', width: anchoP(ancho, 5) },
                    { name: 'Encuesta', index: 'Encuesta', width: anchoP(ancho, 30) },
                    { name: 'Diligenciado', index: 'Diligenciado', width: anchoP(ancho, 10) },
                    { name: 'id_encuesta', index: 'id_encuesta', hidden: true },
                    { name: 'id_estado_encuesta', index: 'id_estado_encuesta', hidden: true },
                    { name: 'alertas', index: 'alertas', width: anchoP(ancho, 5) },
                    { name: 'fecha_cierre', index: 'fecha_cierre', width: anchoP(ancho, 10) },
                    { name: 'estado_admision', index: 'estado_admision', width: anchoP(ancho, 15) },
                    { name: 'medio_recepcion', index: 'medio_recepcion', width: anchoP(ancho, 15) },
                    { name: 'autor', index: 'autor', width: anchoP(ancho, 10) },
                    { name: 'edad_meses_actual', index: 'edad_meses_actual', hidden: true },
                    { name: 'fecha_cierre', index: 'fecha_cierre', width: anchoP(ancho, 10) }
                ],
                height: 80,
                width: ancho + 40,
                caption: "ENCUESTAS",
                beforeSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdEncuesta', datosRow.id_encuesta, 0);
                    asignaAtributo('lblIdEncuestaPaciente', datosRow.id, 0);
                    asignaAtributo('lblDiligenciado', datosRow.Diligenciado, 0);
                    asignaAtributo('txtEstadoEncuesta', datosRow.id_estado_encuesta, 0);
                    buscarEncuesta('listEncuestaPacienteAlertas');
                    mostrar('divParaTabsContenidoEncuesta');
                    return true;
                },
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    tabsContenidoEncuesta('desdeEncuesta');

                    //alert("id encuesta: "+valorAtributo('lblIdEncuestaPaciente'));

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listEncuestaPacienteAlertas':
            limpiarDivEditarJuan(arg);
            ancho = ($('#drag' + ventanaActual.num).find("#" + arg).width()) - 5;
            valores_a_mandar = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
            valores_a_mandar = valores_a_mandar + "?idQuery=2205&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            add_valores_a_mandar(valorAtributo('lblIdPaciente'));
            if (valorAtributo('lblTipoDocumento') == '') {
                var lbltipofolio = 'ASMB';
                asignaAtributo('lblTipoDocumento', lbltipofolio, 0)
                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
            }
            else {
                add_valores_a_mandar(valorAtributo('lblTipoDocumento'));
            }
            add_valores_a_mandar(valorAtributo('lblIdEncuestaPaciente'));
            //alert(valorAtributo('lblTipoDocumento'))
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'Id', 'Encuesta', 'Diligenciado', 'id_encuesta', 'id_estado_encuesta', 'Grafica', 'Riesgo', 'Alertas', 'Pre-Diagnostico', 'Conducta', 'Fecha de cierre', 'recomendacion', 'Estado admision', 'Medio recepcion', 'Abierta por', 'soloRiesgo', 'semaforo', 'id_diagnostico_conducta'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'id', index: 'id', hidden: true },
                    { name: 'Encuesta', index: 'Encuesta', hidden: true },
                    { name: 'Diligenciado', index: 'Diligenciado', hidden: true },
                    { name: 'id_encuesta', index: 'id_encuesta', hidden: true },
                    { name: 'id_estado_encuesta', index: 'id_estado_encuesta', hidden: true },
                    { name: 'grafica', index: 'grafica', hidden: true },
                    { name: 'riesgo', index: 'riesgo', width: anchoP(ancho, 15) },
                    { name: 'alerta', index: 'alerta', width: anchoP(ancho, 10) },
                    { name: 'diagnostico', index: 'diagnostico', width: anchoP(ancho, 40) },
                    { name: 'conducta', index: 'conducta', width: anchoP(ancho, 35) },
                    { name: 'fecha_cierre', index: 'fecha_cierre', hidden: true },
                    { name: 'recomendacion', index: 'recomendacion', hidden: true },
                    { name: 'estado_admision', index: 'estado_admision', hidden: true },
                    { name: 'medio_recepcion', index: 'medio_recepcion', hidden: true },
                    { name: 'autor', index: 'autor', hidden: true },
                    { name: 'soloRiesgo', index: 'soloRiesgo', hidden: true },
                    { name: 'semaforo', index: 'semaforo', hidden: true },
                    { name: 'id_diagnostico_conducta', index: 'id_diagnostico_conducta', hidden: true },

                ],
                height: 60,
                width: ancho + 40,
                caption: "RIESGOS",
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdEncuesta', datosRow.id_encuesta, 0);
                    asignaAtributo('lblIdEncuestaPaciente', datosRow.id, 0);
                    asignaAtributo('lblDiligenciado', datosRow.Diligenciado, 0);
                    asignaAtributo('txtEstadoEncuesta', datosRow.id_estado_encuesta, 0);
                    asignaAtributo('lblIdGrafica', datosRow.grafica, 0);
                    //colocarRiesgoEncuesta(datosRow.soloRiesgo, datosRow.semaforo, datosRow.id_diagnostico_conducta, 
                    //  datosRow.diagnostico, datosRow.conducta, datosRow.medio_recepcion);
                    asignaAtributo('txtRecomendacionEncuesta', datosRow.recomendacion)
                    //tabsContenidoEncuesta('desdeEncuesta');
                    //alert("id encuesta: "+valorAtributo('lblIdEncuestaPaciente'));

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listGrafica':

            alert(5454)
            break;

    }
}







