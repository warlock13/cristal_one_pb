<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    // Obtener cada parámetro de la URL
    String todo = request.getParameter("chkEstadoLE");
    String hc = request.getParameter("chkEstadoLE_hc");
    String conductaTratamiento = request.getParameter("chkEstadoLE_conducta_tratamiento");
    String anexo3Procedimiento = request.getParameter("chkEstadoLE_anexo3");

    String procedimientosSolicitud = request.getParameter("procedimientosSolicitud");
    String laboratoriosContratados = request.getParameter("laboratoriosContratados");
    String laboratoriosNoContratados = request.getParameter("laboratoriosNoContratados");

    String listaMedicamentos = request.getParameter("listaMedicamentos");

    String indicaciones = request.getParameter("chkEstadoLE_indicaciones");

    String concentYDisentimiento = request.getParameter("chkEstadoLE_consentimiento_disentimiento");
    String listaConsentimientos = request.getParameter("listaConsentimientos");

    String remisiones = request.getParameter("chkEstadoLE_remisiones");
    String listaRemisiones = request.getParameter("listaRemisiones");

    String incapacidad = request.getParameter("chkEstadoLE_incapacidad");
    String listaIncapacidades = request.getParameter("listaIncapacidades");

    String recomendaciones = request.getParameter("chkEstadoLE_recomendaciones");

    String idPaciente = request.getParameter("idPaciente");
    String evo = request.getParameter("evo");
    String idReporte = request.getParameter("idReporte");
    String nombrepdf = request.getParameter("nombrepdf");
%>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Visualización de PDFs</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        header {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 1rem;
        }

        section {
            margin: 2rem;
        }

        h2 {
            border-bottom: 2px solid #333;
            padding-bottom: 0.5rem;
            margin-top: 2rem;
        }

        .pdf-container {
            width: 100%;
            margin-top: 1rem;
            overflow: hidden;
        }

        .accordion {
            background-color: #333;
            color: white;
            cursor: pointer;
            padding: 10px;
            width: 100%;
            text-align: left;
            border: none;
            outline: none;
        }

        .iframe-container {
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
        }

        .active, .accordion:hover {
            background-color: #555;
        }

        iframe {
            width: 100%;
            height: 500px; /* Ajusta la altura según tus necesidades */
            border: none;
        }

        .accordion::before {
            content: '\25B6'; /* Unicode para el símbolo de flecha derecha */
            font-size: 20px;
            float: right;
            margin-left: 5px;
        }

        .accordion.active::before {
            content: '\25BC'; /* Unicode para el símbolo de flecha abajo */
        }

        .loader {
            border: 5px solid #f3f3f3; /* Light grey */
            border-top: 5px solid #3498db; /* Azul */
            border-radius: 50%;
            width: 30px;
            height: 30px;
            animation: spin 1s linear infinite;
            display: none;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -15px;
            margin-left: -15px;
            z-index: 1;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
</head>
<body>

    <header>
        <h1>Impresión de PDFs</h1>
    </header>

    <section>
        <% if (hc.equals("true")) { %>
            <div class="pdf-container">
                <button class="accordion">Folio Historia Clínica</button>
                <div class="iframe-container">
                    <iframe id="hcFrame" src="/clinica/paginas/ireports/hc/builderHC.jsp?reporte=<%=idReporte%>&evo=<%=evo%>&nombre=<%=nombrepdf%>"></iframe>
                </div>
            </div>
        <% } %>

        <% if (anexo3Procedimiento.equals("true")) { %>
            <div class="pdf-container">
                <button class="accordion">Anexo 3 de Procedimientos</button>
                <div class="iframe-container">
                    <iframe id="hcFrame" src="/clinica/paginas/ireports/hc/generaHC.jsp?reporte=formula_procedimientos&evo=<%=evo%>&nombre=PROCEDIMIENTOS-<%=nombrepdf%>"></iframe>
                </div>
            </div>
        <% } %>

        <% if (procedimientosSolicitud != null &&  !procedimientosSolicitud.equals("")) { %>
            <div class="pdf-container">
                <button class="accordion">Procedimientos</button>
                <div class="iframe-container">
                    <iframe id="hcFrameProcedimientos" src="/clinica/paginas/ireports/hc/generaImpresionProMed.jsp?procedimientos=<%=procedimientosSolicitud%>&jasper=formula_procedimientos_seleccionados&nombre=PROCEDIMIENTOS-<%=nombrepdf%>&evo=<%=evo%>"></iframe>
                </div>
            </div>
        <% } %>

        <% if (laboratoriosContratados != null && !laboratoriosContratados.equals("")) { %>
            <div class="pdf-container">
                <button class="accordion">Laboratorios (Contratados)</button>
                <div class="iframe-container">
                    <iframe id="hcFrameLaboratoriosContratados" src="/clinica/paginas/ireports/hc/generaImpresionProMed.jsp?procedimientos=<%=laboratoriosContratados%>&jasper=formula_procedimientos_seleccionados&nombre=LABORATORIOS-<%=nombrepdf%>&evo=<%=evo%>"></iframe>
                </div>
            </div>
        <% } %>

        <% if (laboratoriosNoContratados != null &&  !laboratoriosNoContratados.equals("")) { %>
            <div class="pdf-container">
                <button class="accordion">Laboratorios (No contratados)</button>
                <div class="iframe-container">
                    <iframe id="hcFrameLaboratoriosNoContratados" src="/clinica/paginas/ireports/hc/generaImpresionProMed.jsp?procedimientos=<%=laboratoriosNoContratados%>&jasper=formula_procedimientos_seleccionados&nombre=LABORATORIOS-<%=nombrepdf%>&evo=<%=evo%>"></iframe>
                </div>
            </div>
        <% } %>

        <% if (listaMedicamentos != null && !listaMedicamentos.equals("")) { %>
            <div class="pdf-container">
                <button class="accordion">Medicamentos</button>
                <div class="iframe-container">
                    <iframe id="hcFrameMedicamentos" src="/clinica/paginas/ireports/hc/generaImpresionProMed.jsp?procedimientos=<%=listaMedicamentos%>&jasper=formula_medicamentos_seleccionados&nombre=MEDICAMENTOS-<%=nombrepdf%>&evo=<%=evo%>"></iframe>
                </div>
            </div>
        <% } %>

        <% if (concentYDisentimiento.equals("true")) { %>
            <div class="pdf-container">
                <button class="accordion">Consentimientos/Disentimientos</button>
                <div class="iframe-container">
                    <% if (listaConsentimientos != null && !listaConsentimientos.equals("")) {
                        String[] listaConsentimientosArray = listaConsentimientos.split(",");
                        for (String id_consentimiento : listaConsentimientosArray) { %>
                            <iframe id="hcFrameConsentimientos<%=id_consentimiento%>" src="/clinica/paginas/ireports/hc/generaHC.jsp?reporte=consentimiento_informado&evo=<%=id_consentimiento%>"></iframe>
                    <% }
                    } %>
                </div>
            </div>
        <% } %>

        <% if (remisiones.equals("true")) { %>
            <div class="pdf-container">
                <button class="accordion">Remisiones</button>
                <div class="iframe-container">
                    <% if (listaRemisiones != null && !listaRemisiones.equals("")) {
                        String[] listaRemisionesArray = listaRemisiones.split(",");
                        for (String id_remision : listaRemisionesArray) { %>
                            <iframe id="hcFrameRemisiones<%=id_remision%>" src="/clinica/paginas/ireports/hc/generaHC.jsp?evo=<%=id_remision%>&reporte=REFERENCIA&nombre=REMISION-<%=nombrepdf%>"></iframe>
                    <% }
                    } %>
                </div>
            </div>
        <% } %>

        <% if (incapacidad.equals("true")) { %>
            <div class="pdf-container">
                <button class="accordion">Incapacidad</button>
                <div class="iframe-container">
                    <% if (listaIncapacidades != null && !listaIncapacidades.equals("")) {
                        String[] listaIncapacidadesArray = listaIncapacidades.split(",");
                        for (String id_incapacidad : listaIncapacidadesArray) { %>
                            <iframe id="hcFrameIncapacidad<%=id_incapacidad%>" src="/clinica/paginas/ireports/hc/generaHC.jsp?reporte=INCAPACIDAD&evo=<%=id_incapacidad%>&nombre=INCAPACIDAD - <%=nombrepdf%>"></iframe>
                    <% }
                    } %>
                </div>
            </div>
        <% } %>
    </section>

    <script>
        var accordions = document.getElementsByClassName("accordion");
    
        for (var i = 0; i < accordions.length; i++) {
            accordions[i].classList.add("active");
            var iframeContainer = accordions[i].nextElementSibling;
            iframeContainer.style.maxHeight = iframeContainer.scrollHeight + "px";
    
            accordions[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var iframeContainer = this.nextElementSibling;
                if (iframeContainer.style.maxHeight) {
                    iframeContainer.style.maxHeight = null;
                } else {
                    iframeContainer.style.maxHeight = iframeContainer.scrollHeight + "px";
                }
            });
        }
    </script>
    
</body>
</html>
