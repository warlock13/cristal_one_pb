<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="titulos" >
     <td width="100%" colspan="4">PRESCRIPCION DE HEMODIALISIS </td> 
  </tr>   
  <tr class="inputBlanco">
     <td>     
		FILTRO
     </td>
	 <td>
        <label id="lbl_RSHA_C1" ></label>
	 </td>
	 <td>     
		HEPARINA
     </td>
	 <td>
        <label id="lbl_RSHA_C2" ></label>
	 </td>
  </tr>
  <tr class="inputBlanco">
     <td>     
		ACCESO VASCULAR
     </td>
	 <td>
        <label id="lbl_RSHA_C3" ></label>
	 </td>
	 <td>     
		ULTRAFILTRACION
     </td>
	 <td>
        <label id="lbl_RSHA_C4" ></label>
	 </td>
  </tr>
  <tr class="inputBlanco">
     <td>     
		VELOCIDAD DE FLUJO
     </td>
	 <td>
        <label id="lbl_RSHA_C5" ></label>
	 </td>
	 <td>     
		TIEMPO
     </td>
	 <td>
        <label id="lbl_RSHA_C6" ></label>
	 </td>
  </tr>  
</table>
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">  
  <tr class="titulos" >
     <td width="100%" colspan="3">PESO </td> 
  </tr>  
  <tr class="inputBlanco">
     <td>INICIAL</td>
     <td>FINAL</td>
     <td>ACUMULADO</td>
  </tr>
  <tr class="inputBlanco">
     <td><label id="lbl_RSHA_C7" ></label>&nbsp; kg</td>
     <td><label id="lbl_RSHA_C8" ></label>&nbsp; kg</td>
     <td><label id="lbl_RSHA_C9" ></label></td>
  </tr>  
</table>  
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="titulos" >
     <td width="100%" colspan="3">Nota de enfermeria</td> 
  </tr>                     
  <tr class="inputBlanco">
     <td colspan="3">     
         <label id="lbl_RSHA_C10" > </label>     
     </td> 
  </tr>   
  </tr>
</table>  

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="titulos" >
     <td width="100%" colspan="4">Laboratorios</td> 
  </tr>
  <tr class="inputBlanco">
     <td>HEMATOCRITO</td>
     <td><label id="lbl_RSHA_C11" ></label></td>     
     <td>HEMOGLOBINA</td>
     <td><label id="lbl_RSHA_C12" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>NITROGENO UREICO PRE/POS</td>
     <td><label id="lbl_RSHA_C13" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>CREATININA</td>
     <td><label id="lbl_RSHA_C14" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>SODIO</td>
     <td><label id="lbl_RSHA_C15" ></label></td>     
     <td>POTASIO</td>
     <td><label id="lbl_RSHA_C16" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>BICARBONATO</td>
     <td><label id="lbl_RSHA_C17" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>ANTIGENO DE SUPERICIE</td>
     <td><label id="lbl_RSHA_C18" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>HEPATITIS C: </td>
     <td><label id="lbl_RSHA_C19" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>HIV</td>
     <td><label id="lbl_RSHA_C20" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>BALANCE LIQUIDOS</td>
     <td colspan="3"><label id="lbl_RSHA_C21" ></label></td>     
  </tr>
  <tr class="inputBlanco">
     <td>GASTO UNITARIO</td>
     <td><label id="lbl_RSHA_C22" ></label></td>     
  </tr>
</table> 
 





