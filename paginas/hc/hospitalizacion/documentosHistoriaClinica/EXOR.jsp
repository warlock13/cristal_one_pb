


<table width="100%"  align="center">
<tr>
  <td width="50%" >&nbsp;
  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="20%">LENSOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="50%">Adici&oacute;n</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EXOR_C1" size="100"  maxlength="25" style="width:90%" onkeypress="return validarKey(event,this.id)"/></td>
              <td>
                 <select size="1" id="txt_EXOR_C2" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+1.75">+1.75</option>
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+2.75">+2.75</option>                    
                    <option value="+3.00">+3.00</option>
                 </select>               
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EXOR_C3" size="100"  maxlength="25" style="width:90%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/></td>
              <td>
                 <select size="1" id="txt_EXOR_C4" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+1.75">+1.75</option>                    
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+2.75">+2.75</option>                    
                    <option value="+3.00">+3.00</option>
                 </select>              
              </td>                
           </tr>     
            <tr class="estiloImput"> 
              <td>Observaciones</td>                               
              <td colspan="2"><input type="text" id="txt_EXOR_C5" size="100"  maxlength="200" style="width:96%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/></td>
           </tr>     
           
      </table> 
  </td>   
</tr>   
</table> 

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos1"> 
              <td width="25%">AGUDEZA VISUAL Sin Correcci&oacute;n</td>                               
              <td width="37%">Visi&oacute;n Lejana</td>
              <td width="37%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td>
                 <select size="1" id="txt_EXOR_C6" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>                 
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>   -->
					<option value="Otro">Otro</option>		
                 </select> 
				 <input type="text"  value="" id="txt_EXOR_C59"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)" >
              </td>
              <td>
				  <select size="1" id="txt_EXOR_C7" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)">	
                    <option value=""></option>  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                  <!--  <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>   -->
					<option value="Otro">Otro</option>		
                 </select> 
				 <input type="text"  value="" id="txt_EXOR_C60"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)">
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td>
				  <select size="1" id="txt_EXOR_C8" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  >	
                    <option value=""></option>                    
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>    -->
					<option value="Otro">Otro</option>	
                 </select> 
				 <input type="text"  value="" id="txt_EXOR_C61"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  >
              </td>
              <td>
				  <select size="1" id="txt_EXOR_C9" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  >	
                    <option value=""></option>                    
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option> -->
					<option value="Otro">Otro</option>		
                 </select>   
				<input type="text"  value="" id="txt_EXOR_C62"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)">	
              </td>                
           </tr> 
            <tr class="estiloImput"> 
              <td>Pinhole Ojo derecho</td>                               
              <td colspan="2">
				  <select size="1" id="txt_EXOR_C10" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >
                    <option value=""></option>                  	  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select> 
              </td>
           </tr> 
           <tr class="estiloImput" >
              <td>Pinhole Ojo Izquierdo</td>
              <td colspan="2">
				  <select size="1" id="txt_EXOR_C11" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>                  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>              
              </td>             
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="25%">AGUDEZA VISUAL Con Correcci&oacute;n</td>                               
              <td width="37%">Visi&oacute;n Lejana</td>
              <td width="37%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td>
			     <select size="1" id="txt_EXOR_C12" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" >
                    <option value=""></option>                 	  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option> -->
					<option value="Otro">Otro</option>	
                 </select> 
				 <input type="text"  value="" id="txt_EXOR_C63"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)">					 
              </td>
              <td><input type="text" id="txt_EXOR_C13" size="50"  maxlength="50" style="width:90%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"/></td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td>
			     <select size="1" id="txt_EXOR_C14" style="width:40%;" title="32" onblur="guardarContenidoDocumento()" >	
                    <option value=""></option>                   
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                   <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>   -->
					<option value="Otro">Otro</option>		
                 </select>    
				<input type="text"  value="" id="txt_EXOR_C64"  style="width:50%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)">					 	
              </td>
              <td><input type="text" id="txt_EXOR_C15" size="50"  maxlength="50" style="width:90%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"/></td>                
           </tr>  
             <tr class="estiloImput" >
		      <td colspan="3">...</td>                               
              
           </tr>  
            <tr class="estiloImput"> 
              <td>Agudeza visual binocular</td>                               
              <td colspan="2">
               <input type="text" id="txt_EXOR_C67" size="50"  maxlength="100" style="width:90%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"  />
              </td>
           </tr>                   
      </table> 
  </td>   
</tr>   
</table> 
 

<table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
  <tr class="camposRepInp" >
      <td colspan="4">Examen Motor</td>                                             
  </tr>  
  <tr class="camposRepInp" >
     <td width="50%" colspan="2">CUADRO DE MEDIDAS SIN CORRECCION</td> 
     <td width="50%" colspan="2">CUADRO DE MEDIDAS CON CORRECCION</td> 
  <tr>                     
  <tr class="estiloImput">
     <td colspan="2">
         <textarea type="text" id="txt_EXOR_C16"   size="1000"  maxlength="1000" style="width:95%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
     </td>          
     <td colspan="2">     
         <textarea type="text" id="txt_EXOR_C17"  size="4000"  maxlength="4000" style="width:95%"    onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
 
  </tr> 
  <tr class="camposRepInp" >
     <td width="20%">.</td>    
     <td >.</td>         
     <td>Ducciones</td> 
     <td>Versiones</td>
  <tr>                     
  <tr class="estiloImput">
     <td >
		 Ojo Dominante:<select id="txt_EXOR_C18"  style="width:80%"  onblur="guardarContenidoDocumento()"  >
							<option value="" ></option>
							<option value="Ojo Derecho" >Ojo Derecho </option>
							<option value="Ojo Izquierdo" >Ojo Izquierdo </option>                            
                            <option value="Bilateral" >Bilateral</option>
         				</select>
         
     </td> 
     <td>
     	Test Color:<input type="text"  value="" id="txt_EXOR_C19"  style="width:20%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)">         
         Test Binocularidad:<input type="text"  value="" id="txt_EXOR_C20"  style="width:20%"   size="20" maxlength="20" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)">                  
     </td>
     <td >     
         OD:<textarea type="text" id="txt_EXOR_C21"   size="100"  maxlength="100" style="width:95%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
         OI:<textarea type="text" id="txt_EXOR_C22"   size="100"  maxlength="100" style="width:95%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
     </td> 
     <td>     
         OD:<textarea type="text" id="txt_EXOR_C23"   size="100"  maxlength="100" style="width:95%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
         OI:<textarea type="text" id="txt_EXOR_C24"   size="100"  maxlength="100" style="width:95%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
     </td> 
  </tr> 
  <tr class="camposRepInp" >
     <td colspan="2">Amplitud de Acomodacion</td> 
     <td colspan="2">Flexibilidad de Acomodacion</td> 
  <tr class="estiloImput">
     <td valign="middle">         
         OD:<textarea type="text" id="txt_EXOR_C25"   size="1000"  maxlength="2000" style="width:90%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>         
     </td>
     <td valign="middle">         
         OI:<textarea type="text" id="txt_EXOR_C26"   size="1000"  maxlength="2000" style="width:90%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>         
     </td> 
     <td>         
         OD:<textarea type="text" id="txt_EXOR_C27"   size="1000"  maxlength="2000" style="width:90%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>         
     </td>
     <td>         
         OI:<textarea type="text" id="txt_EXOR_C28"   size="1000"  maxlength="2000" style="width:90%"  onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>         
     </td> 
    </tr>
</table>  
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos1"> 
              <td width="40%">REFRACCION</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EXOR_C29" style="width:70%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /> </td>
              <td>
			     <select size="1" id="txt_EXOR_C30" style="width:99%;" title="30" onblur="guardarContenidoDocumento()" >
                    <option value=""></option>                 	  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>              
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EXOR_C31" style="width:70%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>
              <td>
			     <select size="1" id="txt_EXOR_C32" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >
                    <option value=""></option>                 	  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>              
              </td>                
           </tr>     
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="40%"> QUERATOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="30%">&nbsp;</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EXOR_C33" style="width:70%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>
              <td>&nbsp;</td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EXOR_C34" style="width:70%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>
              <td>&nbsp;</td>                
           </tr>     
      </table> 
  </td>   
</tr> 

<tr>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos1"> 
              <td width="40%">SUBJETIVO</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EXOR_C35" style="width:70%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>
              <td>
			     <select size="1" id="txt_EXOR_C36" style="width:99%;" title="36" onblur="guardarContenidoDocumento()" >
                    <option value=""></option>                 	  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>              
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EXOR_C37" style="width:70%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>
              <td>
			     <select size="1" id="txt_EXOR_C38" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >
                    <option value=""></option>                 	  
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                 </select>              
                            
              </td>                
           </tr> 
            <tr class="estiloImput"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
				 <input type="text" id="txt_EXOR_C39" style="width:50%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> 
               <!--  <select size="1" id="txt_EXOR_C39" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+3.00">+3.00</option>
                 </select>      -->          
               </td>
           </tr>               
            <tr class="estiloImput"> 
              <td>Distancia Pupilar</td>                               
              <td colspan="2"><input type="text" id="txt_EXOR_C40" style="width:40%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>
           </tr>
      </table>  
  </td>
  <td width="50%" >
      <table width="100%" align="center">
            <tr class="titulos2"> 
              <td width="40%">RX FINAL</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td><input type="text" id="txt_EXOR_C41" style="width:70%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>
              <td><input type="text" id="txt_EXOR_C42" style="width:70%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /> </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td><input type="text" id="txt_EXOR_C43" style="width:70%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /> </td>
              <td><input type="text" id="txt_EXOR_C44" style="width:70%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /> </td>                
           </tr>  
            <tr class="estiloImput"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2">
				 <input type="text" id="txt_EXOR_C45" style="width:50%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/>
                <!-- <select size="1" id="txt_EXOR_C45" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="+1.00">+1.00</option>
                    <option value="+1.25">+1.25</option>
                    <option value="+1.50">+1.50</option>
                    <option value="+2.00">+2.00</option>
                    <option value="+2.25">+2.25</option>
                    <option value="+2.50">+2.50</option>
                    <option value="+3.00">+3.00</option>
                 </select>    -->            
              </td>
           </tr>               
            <tr class="estiloImput"> 
              <td>&nbsp;</td>                               
              <td colspan="2">&nbsp;</td>
           </tr>              
      </table> 
  </td>   
</tr>  
<tr>
<table width="100%">
<tr class="camposRepInp"> 


   <td >Punto Pr&oacute;ximo de Convergencia</td>
   <td >FP</td>
   <td >FN</td>
</tr>
<tr class="estiloImput">
	 <td>     
         Real:<input type="text"  value="" id="txt_EXOR_C46"  style="width:20%"   size="20" maxlength="20" onblur="guardarContenidoDocumento()"  >
         Luz:<input type="text"  value="" id="txt_EXOR_C47"  style="width:20%"   size="20"  maxlength="20" onblur="guardarContenidoDocumento()"  >         
         FR:<input type="text"  value="" id="txt_EXOR_C48"  style="width:20%"   size="20" maxlength="20" onblur="guardarContenidoDocumento()"  >                  
     </td> 
      <td>
        <input type="text" id="txt_EXOR_C49" style="width:40%" onblur="guardarContenidoDocumento()" />
      </td>
      <td>
        <input type="text" id="txt_EXOR_C50" style="width:40%" onblur="guardarContenidoDocumento()" />
      </td>
    </tr>  
  </table>  
</tr>
 <tr >
 <td width="50%" colspan="3">
      <table width="100%" align="center"> 
	       <tr class="estiloImput"> 
           	   <td >Reservas</td>               
               <td>Otros</td>               
           </tr>           
           <tr class="estiloImput"> 
              <td  >
					<textarea id="txt_EXOR_C51" style="width:90%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
              </td>             
               <td  >
					<textarea id="txt_EXOR_C52" style="width:90%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
              </td>
	      </tr>
		  <tr class="estiloImput"> 
           	   <td >Observaciones</td>               
               <td>Concepto</td>               
           </tr>           
           <tr class="estiloImput"> 
              <td  >
					<textarea id="txt_EXOR_C65" style="width:90%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
              </td>             
               <td  >
					<textarea id="txt_EXOR_C66" style="width:90%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>
              </td>
	      </tr>
		  
     </table>
   </td>
</tr> 

<tr class="estiloImput"> 
  <td colspan="3" bgcolor="#009999">&nbsp;</td>
</tr>


<tr class="estiloImput"> 
  <td width="100%" colspan="2">
   <TABLE width="100%">
      <tr class="titulos"> 
        <td colspan="2">Especificaciones del lente</td>
        <td>Filtro</td>  
        <td>Color</td>    
      </tr>
      <tr class="estiloImput"> 
        <td colspan="2"><input type="text" id="txt_EXOR_C53" maxlength="500" style="width:90%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>  
        <td><input type="text" id="txt_EXOR_C54"  maxlength="100" style="width:90%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>    
        <td><input type="text" id="txt_EXOR_C55"  maxlength="100" style="width:90%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>            
      </tr>
      <tr class="titulos"> 
        <td>Uso</td>  
        <td>control</td>    
        <td colspan="2">Recomendaciones</td>            
      </tr>
      <tr class="estiloImput"> 
        <td><input type="text" id="txt_EXOR_C56" style="width:90%" maxlength="100" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>  
        <td><input type="text" id="txt_EXOR_C57" style="width:90%" maxlength="100" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>    
        <td colspan="2"><input type="text" id="txt_EXOR_C58" maxlength="500" style="width:90%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/> </td>            
      </tr>      
       <tr>  
         <td colspan="4" align="right">
            <input id="btnProcedim_" type="button" class="small button blue" value="Impresion Formula" onClick="formulaPDFHC();"/>
          <!--  <input id="btnProcedimientoImprimir" title="btn_o57p" type="button" class="small button blue" value="Imprime formula opt&aacute;lmica" onclick="imprimirAnexoOptalmica()"  />-->
         </td>                    
       <tr>       
    </TABLE>    
   </td>
</tr>  
        
</table> 





