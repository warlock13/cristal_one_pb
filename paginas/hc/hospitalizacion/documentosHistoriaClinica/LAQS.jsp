
<table width="100%">
            <tr> 
              <td class="titulos1" width="25%"  align="right">ANALISIS</td>  
			  <td class="titulos1" width="25%"  align="right">RESULTADOS</td>	
			  <td class="titulos1" width="25%"  align="right">VALOR DE REFERENCIA</td>  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C1" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>70 - 105 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">COLESTEROL TOTAL:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C2" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>HASTA 200 mg/dl</label></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">TRIGLICERIDOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C3" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>HASTA 150 mg/dl</label></td>  
			</tr> 
			<tr class="estiloImput"> 
              <td align="right">COLESTEROL TOTAL HDL:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C4" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>RIESGO ELEVADO HASTA 35 mg/dl <br/> RIEGO BAJO MAYOR DE 60 mg/dl </label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">COLESTEROL TOTAL LDL:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C5" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>OPTIMO: HASTA 100 mg/dl <br/> CASI OPTIMO: 100 - 129 mg/dl <br/> MODERADO: 130 - 159 mg/dl <br/> ELEVADO: 160 - 189 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">COLESTEROL TOTAL VLDL:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C6" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>5 - 40 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">INDICE ARTERIAL:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C7" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>2.5 - 4.0</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CREATININA:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAQS_C8" size="100"  maxlength="40" style="width:15%"/>mg/dl</td>			  
              <td align="left"><label>MUJERES: MENOR DE 1.2 mg/dl <br/> HOMBRES: MENOR DE 1.4 mg/dl  </label></td>  
			</tr>
			
</table>  
 
 
  

<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACION</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_LAQS_C9"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr> 
  
           
      </table> 
  </td>
</tr>   
</table>  



 
