


<table width="100%"   align="center">
 <!-- <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">DIAGNOSTICOS:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C1" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()"/></td>                                                    
  </tr>    -->
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">Complicaciones:
    	<select size="1" id="txt_HQUI_C1" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"   >	  
			<option value="SI">SI</option>            
            <option value="NO">NO</option>
            <option value="NA">NA</option>                        
         </select>
    </td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C2" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"/></td>                                                    
  </tr> 
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">Evento Adverso:
    	<select size="1" id="txt_HQUI_C3" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"   >	  
            <option value="SI">SI</option> 
            <option value="NO">NO</option>           
            <option value="NA">NA</option>                        
         </select>
    </td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C4" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"/></td>                                                    
  </tr> 
<!--  
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">PRE-OPERATORIOS:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C5" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()"/></td>                                                    
  </tr>   
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">POS-OPERATORIOS:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C6" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()"/></td>                                                    
  <!-- </tr>   
    <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">HISTORICOS PREFERENTEMENTE:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C4" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()"/></td>                                                    
  </tr>  -->
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">Diagnosticos Pos-Operatorio:</td> 
    <!--<td width="20%" class="estiloImputDer">.</td>  -->
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C5" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"/>
   <!-- <input id="btnprocedimientoPos" title="btn_2d3d" type="button" class="small button blue" value="." onclick="traerProcedimientoPOP()"  /> -->
    </td>                                                    
  </tr>     
 <!-- <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">ESPECIFICAR:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C5" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()"/></td>                                                    
  </tr>  -->
   
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">MUESTRAS ENVIADAS A LABORATORIO:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C7" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"/></td>                                                    
  </tr>  
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer"> TEJIDOS ENVIADOS A PATOLOGIA:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C8" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"/></td>                                                    
  </tr>  
  <!--<tr class="estiloImput">
    <td width="20%" class="estiloImputDer">PATOLOGICA:</td> 
    <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C9" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()"/></td>                                                    
  </tr>   -->
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">TECNICA USADA PARA: </td> 
    <td width="80%" class="estiloImputIzq2">
    	<!--<input type="text" id="txt_HQUI_C6" maxlength="400" style="width:80%" onblur="guardarContenidoDocumento()"/> -->
        <select size="1" id="txt_HQUI_C6" style="width:90%;" title="32" onchange="cargarDescripcion(this.value);" onblur="guardarContenidoDocumento();"   >	  
			<option value='0'> </option>
            <option value="EXTRACCION EXTRACAPSULAR DE CRISTALINO POR FACOEMULSIFICACION CON IMPLANTE DE LIO SECUNDARIO">EXTRACCION EXTRACAPSULAR DE CRISTALINO POR FACOEMULSIFICACION CON IMPLANTE DE LIO SECUNDARIO</option>            
            <option value="RESECCION DE PTERIGION SIMPLE (NASAL O TEMPORAL) CON INJERTO">RESECCION DE PTERIGION SIMPLE (NASAL O TEMPORAL) CON INJERTO </option>
            <option value="RESECCION DE PTERIGION REPRODUCIDO SIMPLE (NASAL O TEMPORAL) CON PLASTIA LIBRE O CITOSTATICOS">RESECCION DE PTERIGION REPRODUCIDO SIMPLE (NASAL O TEMPORAL) CON PLASTIA LIBRE O CITOSTATICOS</option>                        
            <option value="RESECCION DE PTERIGION REPRODUCIDO (NASAL O TEMPORAL) CON PLASTIA LIBRE O CITOSTATICOS + REPARACION DE SIMLEFARON CON INJERTO LIBRE DE CONJUNTIVA">RESECCION DE PTERIGION REPRODUCIDO (NASAL O TEMPORAL) CON PLASTIA LIBRE O CITOSTATICOS + REPARACION DE SIMLEFARON CON INJERTO LIBRE DE CONJUNTIVA</option>
            <option value="RESECCION DE PTERIGION SIMPLE (NASAL O TEMPORAL) CON INJERTO + REPARACION DE SIMBLEFARON CON INJERTO LIBRE DE CONJUNTIVA">RESECCION DE PTERIGION SIMPLE (NASAL O TEMPORAL) CON INJERTO + REPARACION DE SIMBLEFARON CON INJERTO LIBRE DE CONJUNTIVA</option>
			<option value="RESECCION DE CHALAZION">RESECCION DE CHALAZION</option>
            <option value="RESECCION DE TUMOR BENIGNO O MALIGNO DE PARPADO ESPESOR PARCIAL UN TERCIO">RESECCION DE TUMOR BENIGNO O MALIGNO DE PARPADO ESPESOR PARCIAL UN TERCIO</option>
            <option value="RESECCION DE TUMOR BENIGNO O MALIGNO DE PARPADO ESPESOR PARCIAL DOS TERCIOS">RESECCION DE TUMOR BENIGNO O MALIGNO DE PARPADO ESPESOR PARCIAL DOS TERCIOS</option>
            <option value="RESECCION DE QUISTE O TUMOR BENIGNO DE CONJUNTIVA">RESECCION DE QUISTE O TUMOR BENIGNO DE CONJUNTIVA</option>
            <option value="INSERCION DE TAPONES (PLUGS) DE PUNTOS LAGRIMALES AO No 6">INSERCION DE TAPONES (PLUGS) DE PUNTOS LAGRIMALES AO No 6</option>
            <option value="INYECCION DE MATERIAL MIORELAJANTE (TOXINA BOTULINICA)">INYECCION DE MATERIAL MIORELAJANTE (TOXINA BOTULINICA)</option>
			<option value="INSERCION DE IMPLANTE PARA GLAUCOMA SOD">INSERCION DE IMPLANTE PARA GLAUCOMA SOD</option>
            <option value="IMPLANTE DE ANILLOS INTRAESTROMALES">IMPLANTE DE ANILLOS INTRAESTROMALES</option>
            <option value="IRIDOTOMIA CON LASER">IRIDOTOMIA CON LASER</option>
            <option value="PLASTIA DE CANALICULOS LAGRIMALES">PLASTIA DE CANALICULOS LAGRIMALES</option>
            <option value="BLEFAROPLASTIA SUPERIOR AO">BLEFAROPLASTIA SUPERIOR AO</option>
            <option value="CAPSULOTOMIA CON LASER">CAPSULOTOMIA CON LASER</option>               
            <option value="EVISCERACION DEL GLOBO OCULAR CON IMPLANTE SOD-NO INCLUYE PROTESIS">EVISCERACION DEL GLOBO OCULAR CON IMPLANTE SOD-NO INCLUYE PROTESIS</option>                                    
            <option value="TERAPIA ANTIANGIOGENICA">TERAPIA ANTIANGIOGENICA</option>
            <option value="FACO + VITRECTOMIA">FACO + VITRECTOMIA</option>
            <option value="CATARATA POR FACO">CATARATA POR FACO</option>
            <option value="VITRECTOMIA VIA POSTERIOR CON ENDOLASER.">VITRECTOMIA VIA POSTERIOR CON ENDOLASER</option>
            <option value="VITRECTOMIA VIA POSTERIOR CON INSERCION DE SILICON O GASES (136)">VITRECTOMIA VIA POSTERIOR CON INSERCION DE SILICON O GASES (136)</option>
            <option value="VITRECTOMIA VIA POSTERIOR + FACOEMULSIFICACION DE CATARATA  SOD">VITRECTOMIA VIA POSTERIOR + FACOEMULSIFICACION DE CATARATA  SOD</option>
            <option value="VITRECTOMIA VIA POSTERIOR + PELAMIENTO DE MEMBRANAS +  INSERCION DE  GASES (136)">VITRECTOMIA VIA POSTERIOR + PELAMIENTO DE MEMBRANAS +  INSERCION DE  GASES (136)</option>                       
            <option value="FACO + TRABECULECTOMIA">FACO + TRABECULECTOMIA</option>                                                             
            <option value="TRABECULECTOMIA">TRABECULECTOMIA</option>  
			<option value="INSERCION DE LENTE INTRAOCULAR FAQUICO">INSERCION DE LENTE INTRAOCULAR FAQUICO</option>                                                             			
         </select>
    </td>                                                    
  </tr>   
  <tr class="estiloImput">
    <td width="20%" colspan="2" class="titulosTodasMinuscu">
    DESCRIPCION DE LA INTERVENCION QUIRURGICA
    <p>
    Describa la operaci&oacute;n en el mismo orden en que se realiz&oacute; anotando hallazgos operatorios y procedimientos.
    
    </td> 
  </tr>   
  <tr class="estiloImput">
    <td width="80%" colspan="2">
	  <textarea type="text" rows="18" id="txt_HQUI_C9"  size="4000"  maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id);  guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>    
    </td>                                                    
  </tr>   
  
</table>  

 
 
<table width="100%">
           <tr class="estiloImput"> 
              <td colspan="1" align="CENTER">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76"  >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                  </select>              
              </td>                   
              <td colspan="1" align="CENTER">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"   >	
                          <option value="1">NINGUNA</option>                                                                                   
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>    
              <td colspan="1" align="CENTER">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO AL FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr>  
</table>




