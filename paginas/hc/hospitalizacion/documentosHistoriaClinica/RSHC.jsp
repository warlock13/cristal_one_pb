

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%" colspan="4">PRESCRIPCION DE HEMODIALISIS </td> 
  </tr>   
  <tr class="estiloImput">
     <td>     
		FILTRO        
		<select id="txt_RSHC_C1" style="width:30%" onblur="guardarContenidoDocumento();">
			<option value="120">120</option>
			<option value="150">150</option>
			<option value="160">160</option>
			<option value="180">180</option>
			<option value="210">210</option>
		</select> 
     </td>
	 <td>
		ANTIGENO
        <input type="text" id="txt_RSHC_C2" maxlength="500" style="width:10%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	 </td>
	 <td>     
		HEPARINA
     </td>
	 <td>        
		<select id="txt_RSHC_C3" style="width:30%" onblur="guardarContenidoDocumento();">
			<option value="500">500</option>
			<option value="1000">1000</option>
			<option value="2000">2000</option>
			<option value="3000">3000</option>
			<option value="4000">4000</option>
			<option value="5000">5000</option>
			<option value="7000">7000</option>
		</select> 
	 </td>
  </tr>
  <tr class="estiloImput">
     <td>     
		ACCESO VASCULAR		
     </td>
	 <td>	 
	    <table>
			<tr>
				<td>Fistula AV &nbsp; <input type="text" id="txt_RSHC_C4" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
				</td>
				<td>
					<table>
						<tr>
							<td rowspan="3">Cateter</td>
							<td>Yugular &nbsp; <input type="text" id="txt_RSHC_C16" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/> </td>
						</tr>
						<tr>							
							<td>Femoral &nbsp; <input type="text" id="txt_RSHC_C17" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>
						</tr>
						<tr>							
							<td>Subclave &nbsp;<input type="text" id="txt_RSHC_C18" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>
						</tr>
					</table>
				</td>
			<tr>
		</table>        
	 </td>
	 <td>     
		ULTRAFILTRACION
     </td>
	 <td>
        <input type="text" id="txt_RSHC_C5" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	 </td>
  </tr>
  <tr class="estiloImput">
     <td>     
		VELOCIDAD DE FLUJO
     </td>
	 <td>
        <input type="text" id="txt_RSHC_C6" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	 </td>
	 <td>     
		TIEMPO
     </td>
	 <td>
        <input type="text" id="txt_RSHC_C7" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	 </td>
  </tr>  
  <tr class="camposRepInp" >
     <td width="100%" colspan="4">PESO </td> 
  </tr> 
  <tr class="estiloImput">
     <td>INICIAL</td>
     <td>FINAL</td>
     <td>SECO</td>
     <td>&nbsp;</td>
  </tr>
  <tr class="estiloImput">
     <td><input type="text" id="txt_RSHC_C8" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>&nbsp;kg</td>
     <td><input type="text" id="txt_RSHC_C9" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/> &nbsp;kg</td>
     <td><input type="text" id="txt_RSHC_C10" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>
     <td>&nbsp;</td>
  </tr>
<tr class="estiloImput">
	<td colspan="2">
		Eritropoyetina  2000 unidades&nbsp;
		<select id="txt_RSHC_C12" style="width:15%" onblur="guardarContenidoDocumento();">
			<option value ="SI" >SI</option>
			<option value ="NO" >NO</option>
		</select>
		&nbsp;&nbsp;
		
	</td>
	<td colspan="2">
		HIERRO I.V. &nbsp;
		<select id="txt_RSHC_C14" style="width:15%" onblur="guardarContenidoDocumento();">
			<option value ="SI" >SI</option>
			<option value ="NO" >NO</option>
		</select>
		
	</td>
  </tr>
  <tr class="estiloImput">
	<td colspan="2"> MISCELA &nbsp;
		<input type="text" id="txt_RSHC_C13" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	</td>
	<td colspan ="2">
		<!--Observacion: &nbsp;&nbsp; -->
		<input type="hidden" id="txt_RSHC_C15" maxlength="500" style="width:50%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/>
	</td>
  </tr>
</table>  

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">Observación</td> 
  </tr>                     
  <tr class="estiloImput">
     <td>     
         <textarea type="text" rows="5" id="txt_RSHC_C11"  size="4000"  maxlength="4000" style="width:95%"    onblur="guardarContenidoDocumento()"> </textarea>     
     </td> 
  </tr>   
</table> 
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
	<tr class="estiloImput">
	  <td>TEMPERATURA</td>
      <td><input type="text" id="txt_RSHC_C19" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
	<tr class="estiloImput">
	  <td>CONDUCTIVIDAD</td>
      <td><input type="text" id="txt_RSHC_C20" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
	<tr class="estiloImput">
	  <td>VERIFICACION DE LA MAQUINA</td>
      <td><input type="text" id="txt_RSHC_C21" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
	<tr class="estiloImput">
	  <td>NUMERO DE SESION</td>
      <td><input type="text" id="txt_RSHC_C22" maxlength="500" style="width:20%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"/></td>     
	</tr>
</table> 