
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="60%" colspan="2" >
      <table width="95%" align="center">
	    <tr> <td width="80%">
		  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">	
      		<tr class="camposRepInp"> 
              <td colspan="5">
             <!-- 	<input id="btnProcedimiento" type="button" class="small button blue" value="COPIAR" title="BTN COPIAR" onClick="copiarContenidoAlArray()" disabled="disabled"/> 
                <input id="btnProcedimiento" type="button" class="small button blue" value="PEGAR" title="BTN PEGAR" onClick="traerContenidoDelArray()" disabled="disabled"/>  -->
              </td>                               
            </tr>            	      
            <tr class="camposRepInp"> 
              <td colspan="5">AGUDEZA VISUAL LEJANA</td>                               
            </tr>      
            <tr class="camposRepInp"> 
              <td width="8%"></td>                               
              <td width="40%">Sin Correccion</td>
              <td width="0%"  align="left">&nbsp;</td>              
              <td width="40%">Con correccion</td>   
   			  <td width="12%">Tipo</td>                           
            </tr>		
            <tr class="estiloImput"> 
              <td>Ojo derecho</td>                               
              <td>
                 <select size="1" id="txt_ECOF_C1" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>                 
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>                                        
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
					<option value="Otro">Otro</option> 		
                 </select>  
				 &nbsp;
				 <input type="text" id="txt_ECOF_C18" style="width:45%;"  size="3" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)" />     
              </td>              
              <td  align="left">                 
                 
			  </td>		
              <td>              
                 <select size="1" id="txt_ECOF_C3" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>                                        
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                <!--    <option value="Cuenta Dedos 20 cm">Cuenta Dedos 20 cm</option>                    
                    <option value="Cuenta Dedos 30 cm">Cuenta Dedos 30 cm</option>                                                            
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>
					<option value="Buena fijacion y seguimiento">Buena fijacion y seguimiento</option>					
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option> 
                    <option value="NO CORRIGE">NO CORRIGE</option>    -->
					<option value="Otro">Otro</option> 						
                 </select>  
				&nbsp;
				 <input type="text" id="txt_ECOF_C19" style="width:45%;"  size="3" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)" />
              </td>              
              <td>                 
                 <select size="1" id="txt_ECOF_C4" style="width:98%" title="32" onblur="guardarContenidoDocumento()" >	  
                    <option value=""></option>                                     
                    <option value="PINHOLE">PINHOLE</option>
                    <option value="LENTES">LENTES</option>                    
                 </select>                 
              </td>                
           </tr>                                                                            
            <tr class="estiloImput"> 
              <td>Ojo Izquierdo</td>                               
              <td>
                 <select size="1" id="txt_ECOF_C5" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>                 
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>                                        
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option> 
                   <!-- <option value="Cuenta Dedos 20 cm">Cuenta Dedos 20 cm</option>                    
                    <option value="Cuenta Dedos 30 cm">Cuenta Dedos 30 cm</option>                                                            
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>   
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option> 
					<option value="Buena fijacion y seguimiento">Buena fijacion y seguimiento</option>	
                    <option value="Fija sigue sostiene">Fija sigue sostiene</option>				
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>  
                    <option value="NO CORRIGE">NO CORRIGE</option>  -->
					<option value="Otro">Otro</option> 						
                 </select>  
				 &nbsp;
				 <input type="text" id="txt_ECOF_C20" style="width:45%;"  size="3" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"  />
              </td>              
              <td  align="left">                 
                 
              </td>
              <td>
                 <select size="1" id="txt_ECOF_C7" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>                 
                    <option value="20/20">20/20</option>
                    <option value="20/25">20/25</option>
                    <option value="20/30">20/30</option>
                    <option value="20/40">20/40</option>
                    <option value="20/50">20/50</option>
                    <option value="20/60">20/60</option>
                    <option value="20/70">20/70</option>
                    <option value="20/80">20/80</option>
                    <option value="20/100">20/100</option>
                    <option value="20/140">20/140</option>
                    <option value="20/150">20/150</option>                    
                    <option value="20/200">20/200</option>
                    <option value="20/300">20/300</option>
                    <option value="20/400">20/400</option>
                    <!-- <option value="Cuenta Dedos 20 cm">Cuenta Dedos 20 cm</option>                    
                    <option value="Cuenta Dedos 30 cm">Cuenta Dedos 30 cm</option>                                                            
                    <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                    <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                    <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                    <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                    <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                    <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                    <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                    <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                    <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                    <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                    <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                    <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option> 
					<option value="Buena fijacion y seguimiento">Buena fijacion y seguimiento</option>
                    <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                    <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                    <option value="NO CORRIGE">NO CORRIGE</option>    -->
					<option value="Otro">Otro</option> 	
                 </select>  
				&nbsp;
				 <input type="text" id="txt_ECOF_C21" style="width:45%;"  size="3" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)" />				 
              </td>              
              <td>                  
                 <select size="1" id="txt_ECOF_C8" style="width:98%" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>                                     
                    <option value="PINHOLE">PINHOLE</option>
                    <option value="LENTES">LENTES</option>                    
                 </select>              
               </td>                
           </tr>     
		   </table> 
		   </td>
			<td width="20%">
				<table  width="100%" border="1" cellpadding="0" cellspacing="7" align="center" >    						
					<tr class="camposRepInp"> 
						<td width="95%">AGUDEZA VISUAL CERCANA</td>                               
					</tr>						
					<tr class="estiloImput"> 						  
					  <td align="center" ><input type="text" id="txt_ECOF_C2" style="width:85%;"  size="3" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/></td>						  					 
					</tr>											
					<tr>
					  <td align="center" ><input type="text" id="txt_ECOF_C6" style="width:85%;" size="3" onblur="guardarContenidoDocumento()"   onkeypress="return validarKey(event,this.id)"/></td>
					</tr>
				</table>					
			</td></tr> 
	  </table> 
  </td>   
</tr>   
</table>  
<table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >    
  <tr class="camposRepInp" >
     <td width="25%">PUPILAS</td> 
  <tr> 
  <tr class="estiloImput"> 
     <td><input type="text" id="txt_ECOF_C9" maxlength="1000" style="width:70%" onblur="guardarContenidoDocumento()" /> </td>
    </tr>  
   <tr class="camposRepInp" >
   	<td>
      <input id="btnProcedimiento" type="button" class="small button blue" value="PRECONSULTA" title="B765" onClick="modificarCRUD('pacientePreconsulta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"/>                                               
  </td>
   </tr>
</table>
 
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="1000%" bgcolor="#c0d3c1" >.
  </td>
</tr>
</table>  

  


<table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >  
                                         
  <tr class="camposRepInp" >
	 <td width="25%">Presi&oacute;n intra Ocular Ojo Derecho</td> 
     <td width="25%">Presi&oacute;n intra Ocular Ojo Izquierdo</td>  
      
  </tr>                     
  <tr class="estiloImput">
     <td>
	     mmHg:<input type="text" id="txt_ECOF_C10" style="width:20%" size="20"  maxlength="5" onblur="guardarContenidoDocumento()" />&nbsp;&nbsp;
         Hrs:<input type="text" id="txt_ECOF_C11" style="width:20%" size="20"  maxlength="5" onblur="guardarContenidoDocumento()" />
     </td>          
     <td>     
	     mmHg:<input type="text" id="txt_ECOF_C12" style="width:20%" size="20"  maxlength="5" onblur="guardarContenidoDocumento()" />&nbsp;&nbsp;
         Hrs:<input type="text" id="txt_ECOF_C13" style="width:20%" size="20"  maxlength="5" onblur="guardarContenidoDocumento()" />
     </td> 
     
  </tr>  
   
  <tr class="camposRepInp" >
     
     <td width="25%">Subjetivo</td> 
     <td width="25%" >Objetivo</td>   
  </tr>                     
  <tr class="estiloImput">
     
     <td >     
         <textarea type="text" id="txt_ECOF_C14" placeholder="Motivo por el cual el paciente asiste a control..." size="4000" rows="4" maxlength="2000" style="width:95%"    onblur="v28(this.value,'txt_ECOF_C14'); guardarContenidoDocumento();" onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
     <td >     
         <textarea type="text" id="txt_ECOF_C15" placeholder="Examenes y demas pertientes, espacio para la evaluacion medica." size="4000" rows="4" maxlength="2000" style="width:95%"    onblur="v28(this.value,'txt_ECOF_C15');  guardarContenidoDocumento();"  onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
  </tr> 
  <tr class="camposRepInp" >    
     <td width="25%">Observaciones</td>   
     <td width="25%">Concepto</td>   
  </tr>  
  <tr class="estiloImput">     
     <td >     
         <textarea type="text" id="txt_ECOF_C16" placeholder="Observaciones..." size="4000" rows="4" maxlength="4000" style="width:95%"    onfocus="sugerencia(this.id)" onblur="v28(this.value,'txt_ECOF_C16');  guardarContenidoDocumento();"  onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
	 <td >     
         <textarea type="text" id="txt_ECOF_C17" placeholder="Concepto..." size="4000" rows="4" maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumento();"  onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
   </tr>
   
</table>  

<table width="100%">
           <tr class="estiloImput"> 
              <td colspan="1" align="CENTER">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76"  >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                  </select>              
              </td>                   
              <td colspan="1" align="CENTER">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"   >	
                          <option value=""></option>                                          
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>    
              <td colspan="1" align="CENTER">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO AL FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr>  
</table>


           




