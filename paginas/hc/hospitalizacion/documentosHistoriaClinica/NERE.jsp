<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">Nota de enfermeria</td> 
  <tr>                     
  <tr class="estiloImput">
     <td>     
         <textarea type="text" rows="10" id="txt_NERE_C1"  size="4000"  maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)"> </textarea>     
     </td> 
  </tr>   
  <tr>
    	<td> 
        	<table width="100%" align="center">                		
                <tr class="titulos">
                  <td width="20%">Frecuencia Cardiaca</td>                               
                  <td width="20%">Frecuencia Respiratoria</td>
                  <td width="20%">Temperatura</td>                
                  <td width="20%">Tension Arterial</td>
                  <td width="20%">Sat. Oxigeno</td>                 
                </tr>		
                <tr class="estiloImput"> 
                  <td>
                      <input type="text" id="txt_NERE_C2" maxlength="5" style="width:20%" onblur="guardarContenidoDocumento()"/>X MIN
                  </td>                
                  <td>
                      <input type="text" id="txt_NERE_C3" maxlength="5" style="width:20%" onblur="guardarContenidoDocumento()"/>X MIN
                  </td>                
                  <td>
                      <input type="text" id="txt_NERE_C4" maxlength="5" style="width:20%" onblur="guardarContenidoDocumento()"/>° Grados
                  </td>                
                  <td>
                      <input type="text" id="txt_NERE_C5" maxlength="5" style="width:20%" onblur="guardarContenidoDocumento()"/> mmHG
                  </td> 
                  <td>
                      <input type="text" id="txt_NERE_C6" maxlength="5" style="width:20%" onblur="guardarContenidoDocumento()"/>%
                  </td>   
                </tr>     
          </table> 
        </td>
    </tr>
</table>  
<table width="100%"   align="center">
    <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">Evento Adverso:
          <select size="1" id="txt_NERE_C7" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
              <option value="SI">SI</option> 
              <option value="NO">NO</option>           
              <option value="NA">NA</option>                        
           </select>
      </td>     
      <td width="80%" class="estiloImputIzq2">
      	<input type="text" id="txt_NERE_C8" maxlength="500" style="width:80%" onblur="v28(this.value,this.id);  guardarContenidoDocumento();"/>
      </td>                                                    
  </tr>
</table>  
 

<table width="100%">  
  <tr>  
     <td width="100%">    
       <input type="button" onclick="cerrarDocumentClinicoSinImp()" value="FINALIZAR SIN IMPRIMIR" title="btn587" class="small button blue" id="btnFinalizarDocumento_">        
     </td>                                                       
  </tr>   
</table>