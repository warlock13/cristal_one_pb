
<table width="100%"  align="center">
  <tr>
    <td >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="20%">EXAMEN CORRECTO</td>                               
              <td width="20%">SITIO DE EXAMEN CORRECTO</td>
              <td width="20%">DATOS DEL PACIENTE CORRECTOS</td>                
              <td width="20%">CANTIDAD DE EXAMENES CORRECTA</td>                
              <td width="20%">RIESGOS POR MEDICAMENTOS </td>                                                
            </tr>		
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_PENT_C1" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PENT_C2" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PENT_C3" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PENT_C4" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>   
              <td>
                 <select size="1" id="txt_PENT_C5" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="NA" selected="selected">NA</option>                    
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
            </tr>   
            
            <tr class="titulos"> 
              <td width="20%">PREPARACION PREVIA DEL PACIENTE</td>                               
              <td width="20%">DOCUMENTOS NECESARIOS</td>
              <td width="20%">CONSENTIMIENTO INFORMADO</td>                
              <td width="40%" colspan="2">OBSERVACIONES</td>                
            </tr>                                                                                     
  
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_PENT_C6" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PENT_C7" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PENT_C8" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td colspan="2"><input type="text" id="txt_PENT_C9" maxlength="500" style="width:96%" onblur="guardarContenidoDocumento()"  onkeypress="return validarKey(event,this.id)"/></td>
           </tr> 
           <tr class="estiloImput"> 
              <td colspan="2" align="right">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76"  >
                      <option value=""></option>         
                      <option value="2">Tomado</option>
                      <option value="7">Cancelado Finalizado</option>   
                      <option value="10">Reprogramado Finalizado</option>                                                                
                      <option value="8">Finalizado Urgente</option>  
                      <option value="9">Finalizado Alerta</option>                                                                                                                                                    
                  </select>              
              </td>    
              <td colspan="1">
              Motivo:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"   >	
                          <option value="1">NINGUNA</option>                                          
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>                              
              <td colspan="1" align="left">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO AL FOLIO" onclick="cambioEstadoFolio()">                                    
              </td>     
           </tr>  		   
      </table> 
    </td>   
  </tr>   
</table>  
