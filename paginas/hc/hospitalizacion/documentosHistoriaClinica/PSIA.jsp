  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >DEFINICION DEL PROBLEMA</td>
</tr>
</table>  
  
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%">EVOLUCION</td> 
                <td width="37%">CAUSAS</td>
				</tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C1"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_PSIA_C2"    rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>   
           <tr class="estiloImput"> 
                <td width="37%">ACCIONES REALIZADAS EN BUSCA DE SOLUCION</td> 
                <td width="37%">IMPLICACIONES(a nivel familiar, social, acad&eacute;mico, etc.)</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C3"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_PSIA_C4"    rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>            
      </table> 
  </td>
</tr>   
</table>

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >ESTRUCTURA Y FUNCIONAMIENTO FAMILIAR</td>
</tr>
</table>   

      <table border="1">
            <tr class="titulos1">
              <td align="center">MIEMBRO</td>
              <td align="center">PARENTESCO</td>
              <td align="center">EDAD</td>   
              <td align="center">ESCOLARIDAD</td>                
              <td align="center">OCUPACION</td> 
            </tr>		
            <tr class="estiloImput"> 
                <td align="center"><input type="text" id="txt_PSIA_C5" size="100"  maxlength="30" style="width:95%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C6" size="100"  maxlength="30" style="width:60%"/></td>
				<td align="center"><input type="number" id="txt_PSIA_C7"   min="0" max="200" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C8" size="100"  maxlength="30" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C9" size="100"  maxlength="30" style="width:65%"/></td>
           </tr>                                                                            
			<tr class="estiloImput"> 
                <td align="center"><input type="text" id="txt_PSIA_C10" size="100"  maxlength="30" style="width:95%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C11" size="100"  maxlength="30" style="width:60%"/></td>
				<td align="center"><input type="number" id="txt_PSIA_C12"   min="0" max="200" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C13" size="100"  maxlength="30" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C14" size="100"  maxlength="30" style="width:65%"/></td>
           </tr>   
		    <tr class="estiloImput"> 
                <td align="center"><input type="text" id="txt_PSIA_C15" size="100"  maxlength="30" style="width:95%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C16" size="100"  maxlength="30" style="width:60%"/></td>
				<td align="center"><input type="number" id="txt_PSIA_C17"   min="0" max="200" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C18" size="100"  maxlength="30" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C19" size="100"  maxlength="30" style="width:65%"/></td>
           </tr>   
		   <tr class="estiloImput"> 
                <td align="center"><input type="text" id="txt_PSIA_C20" size="100"  maxlength="30" style="width:95%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C21" size="100"  maxlength="30" style="width:60%"/></td>
				<td align="center"><input type="number" id="txt_PSIA_C22"   min="0" max="200" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C23" size="100"  maxlength="30" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C24" size="100"  maxlength="30" style="width:65%"/></td>
           </tr>   
		    <tr class="estiloImput"> 
                <td align="center"><input type="text" id="txt_PSIA_C25" size="100"  maxlength="30" style="width:95%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C26" size="100"  maxlength="30" style="width:60%"/></td>
				<td align="center"><input type="number" id="txt_PSIA_C27"   min="0" max="200" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C28" size="100"  maxlength="30" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C29" size="100"  maxlength="30" style="width:65%"/></td>
           </tr>   
		    <tr class="estiloImput"> 
                <td align="center"><input type="text" id="txt_PSIA_C30" size="100"  maxlength="30" style="width:95%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C31" size="100"  maxlength="30" style="width:60%"/></td>
				<td align="center"><input type="number" id="txt_PSIA_C32"   min="0" max="200" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C33" size="100"  maxlength="30" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C34" size="100"  maxlength="30" style="width:65%"/></td>
           </tr>   
			<tr class="estiloImput"> 
                <td align="center"><input type="text" id="txt_PSIA_C35" size="100"  maxlength="30" style="width:95%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C36" size="100"  maxlength="30" style="width:60%"/></td>
				<td align="center"><input type="number" id="txt_PSIA_C37"   min="0" max="200" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C38" size="100"  maxlength="30" style="width:65%"/></td>
                <td align="center"><input type="text" id="txt_PSIA_C39" size="100"  maxlength="30" style="width:65%"/></td>
           </tr>   
		<tr class="estiloImput"> 
                <td width="37%"  colspan="5">VINCULOS AFECTIVOS CONFLICTIVOS Y REDES DE COMUNICACION</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="5">         
                    <textarea type="text" id="txt_PSIA_C40"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>  		   
         
      </table> 
	  
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >HISTORIA PERSONAL</td>
</tr>
</table>  
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%">INFANCIA</td> 
                <td width="37%">ADOLESCENCIA</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C41"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_PSIA_C42"    rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>                
      </table> 
  </td>
</tr>   
</table>
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >HISTORIA ESCOLAR</td>
  <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C43"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>   
</tr>
</table> 
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >OBSERVACIONES (descripci&oacute;n f&iacute;sica, lenguaje no verbal, actitud, etc.)</td>
  <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C44"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>   
</tr>
</table> 

  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >DIMENSIONES</td>
</tr>
</table>  
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%">COMPORTAMENTAL</td> 
                <td width="37%">AFECTIVA</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C45"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_PSIA_C46"    rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>   
           <tr class="estiloImput"> 
                <td width="37%">SOMATICA</td> 
                <td width="37%">COGNITIVA</td>                  
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C47"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_PSIA_C48"    rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td> 
           </tr>  
			<tr class="estiloImput"> 
                <td width="37%">SOCIAL</td> 
                        
           </tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C49"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>		
      </table> 
  </td>
</tr>   
</table>
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >IMPRESION DIAGNOSTICA</td>
  <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C50"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>   
</tr>
</table> 
  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr class="camposRepInp">
  <td width="1000%" bgcolor="#c0d3c1" >TRATAMIENTO A SEGUIR</td>
  <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIA_C51"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr>   
</tr>
</table> 

 
