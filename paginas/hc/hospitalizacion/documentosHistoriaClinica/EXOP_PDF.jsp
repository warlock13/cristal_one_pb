


<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="70%" colspan="2" >
      <table width="50%" align="center">
            <tr class="inputExcel"> 
              <td width="20%">LENSOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="50%">Adici&oacute;n</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXOP_C1"/></label></td>
              <td><label id="lbl_EXOP_C2"/></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXOP_C3"/></label></td>
              <td><label id="lbl_EXOP_C4"/></label></td>                
           </tr>     
            <tr class="inputBlanco"> 
              <td>Observaciones</td>                               
              <td colspan="2"><label id="lbl_EXOP_C5"/></label></td>
           </tr>  
      </table> 
  </td>   
</tr>   
</table>  

 
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="80%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">Agudeza visual sin correcci&oacute;n</td>                               
              <td width="30%">Visi&oacute;n Lejana</td>
              <td width="30%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXOP_C6"/></label></td>
              <td><label id="lbl_EXOP_C7"/></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXOP_C8"/></label></td>
              <td><label id="lbl_EXOP_C9"/></label></td>                
           </tr> 
            <tr class="inputBlanco"> 
              <td>Pinhole Ojo derecho</td>                               
              <td colspan="2"><label id="lbl_EXOP_C10"/></label></td>
           </tr> 
           <tr class="inputBlanco" >
              <td>Pinhole Ojo Izquierdo</td>
              <td colspan="2"><label id="lbl_EXOP_C11"/></label></td>  
			</tr>
      </table>  
  </td>
  <td width="50%" >
      <table width="80%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">Agudeza visual con correcci&oacute;n</td>                               
              <td width="30%">Visi&oacute;n Lejana</td>
              <td width="30%">Visi&oacute;n pr&oacute;xima</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXOP_C12"/></label></td>
              <td><label id="lbl_EXOP_C13"/></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXOP_C14"/></label></td>
              <td><label id="lbl_EXOP_C15"/></label></td>                
           </tr>  
            <tr> 
              <td colspan="3">&nbsp;</td>                               
           </tr>  
            <tr> 
              <td colspan="3">&nbsp;</td>                               
            </tr>                     
      </table> 
  </td>   
</tr>   


</table> 
 
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
 <tr>
   <td width="50%" >
	<table  width="80%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
  <tr class="inputExcel" >
     <td width="50%" colspan="2">Examen externo Ojo Derecho</td> 
     <td width="50%" colspan="2">Examen externo Ojo Izquierdo</td> 
  </tr>                     
  <tr class="inputBlanco">
     <td colspan="2"><label id="lbl_EXOP_C16"/></label></td>          
     <td colspan="2"><label id="lbl_EXOP_C17"/></label></td> 
 
  </tr> 
  <tr class="inputExcel" >
     <td>Reflejos pupilares</td> 
     <td>Cover test</td>   
     <td>Punto Pr&oacute;ximo de Convergencia</td> 
     <td>Ducciones</td>
  </tr>                     
  <tr class="inputBlanco">
     <td><label id="lbl_EXOP_C18"/></label></td>          
     <td>     
         VL:&nbsp;<label id="lbl_EXOP_C19"/></label>
         VP:&nbsp;<label id="lbl_EXOP_C20"/></label>
     </td> 
     <td>     
         Real:&nbsp;<label id="lbl_EXOP_C21"/></label>
         Luz:&nbsp;<label id="lbl_EXOP_C22"/></label>
         FR:&nbsp;<label id="lbl_EXOP_C23"/></label>
     </td> 
     <td>     
         OD:&nbsp;<label id="lbl_EXOP_C24"/></label>
         OI:&nbsp;<label id="lbl_EXOP_C25"/></label>

     </td> 
  </tr> 
  <tr class="inputExcel" >
     <td colspan="2">Versiones</td> 
     <td>Oftalmoscopia Ojo Derecho</td> 
     <td>Oftalmoscopia Ojo Izquierdo</td> 
  </tr>	 
  <tr class="inputBlanco">
     <td colspan="2"><label id="lbl_EXOP_C26"/></label></td>
     <td><label id="lbl_EXOP_C27"/></label></td>
     <td><label id="lbl_EXOP_C28"/></label></td> 
   </tr>	
</table>  
   </td>
  </tr>
</table>  


<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
<tr>
  <td width="50%" >
      <table width="81%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">REFRACCION</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXOP_C29"/></label> </td>
              <td><label id="lbl_EXOP_C30"/></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXOP_C31"/></label> </td>
              <td><label id="lbl_EXOP_C32"/></label></td>                
           </tr>     
      </table>  
  </td>
  <td width="50%" >
      <table width="82%" align="center">
            <tr class="inputExcel"> 
              <td width="40%"> QUERATOMETRIA</td>                               
              <td width="30%">Valor</td>
              <td width="30%">&nbsp;</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXOP_C33"/></label></td>
              <td>&nbsp;</td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXOP_C34"/></label></td>
              <td>&nbsp;</td>                
           </tr>     
      </table> 
  </td>   
</tr> 

<tr>
  <td width="50%" >
      <table width="82%" align="center">
            <tr class="inputExcel"> 
              <td width="40%">SUBJETIVO</td>                               
              <td width="30%">Valor</td>
              <td width="30%">AV</td>                
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXOP_C35"/></label></td>
              <td><label id="lbl_EXOP_C36"/></label></td>                
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXOP_C37"/></label></td>
              <td><label id="lbl_EXOP_C38"/></label></td>                
           </tr> 
            <tr class="inputBlanco"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2"><label id="lbl_EXOP_C39"/></label></td>
           </tr>               
            <tr class="inputBlanco"> 
              <td>Distancia Pupilar</td>                               
              <td colspan="2"><label id="lbl_EXOP_C40"/></label></td>
           </tr>
      </table>  
  </td>
  <td width="50%" >
      <table width="83%" align="center">
            <tr class="inputExcel"> 
              <td width="25%">RX FINAL</td>                               
              <td width="25%">Esfera&nbsp;&nbsp;&nbsp;&nbsp;Cilindro&nbsp;&nbsp;&nbsp;&nbsp;Eje&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td width="25%">AV Lejana</td>                
              <td width="25%">AV Proxima</td>                              
            </tr>		
            <tr class="inputBlanco"> 
              <td>Ojo derecho</td>                               
              <td><label id="lbl_EXOP_C41"/></label>&nbsp;&nbsp;&nbsp;
                  <label id="lbl_EXOP_C42"/></label>&nbsp;&nbsp;&nbsp;&nbsp;
                  <label id="lbl_EXOP_C43"/></label>
              </td>
              <td><label id="lbl_EXOP_C44"/></label></td>
              <td><label id="lbl_EXOP_C45"/></label></td>              
           </tr>                                                                            
            <tr class="inputBlanco"> 
              <td>Ojo Izquierdo</td>                               
              <td><label id="lbl_EXOP_C46"/></label>&nbsp;&nbsp;&nbsp;
                  <label id="lbl_EXOP_C47"/></label>&nbsp;&nbsp;&nbsp;&nbsp;
                  <label id="lbl_EXOP_C48"/></label>
              </td>
              <td><label id="lbl_EXOP_C49"/></label></td>
              <td><label id="lbl_EXOP_C50"/></label></td>              
           </tr>  
            <tr class="inputBlanco"> 
              <td>Adici&oacute;n</td>                               
              <td colspan="2"><label id="lbl_EXOP_C51"/></label></td>
           </tr>               
            <tr class="estiloImput"> 
              <td>&nbsp;</td>                               
              <td colspan="2">&nbsp;</td>
           </tr>              
      </table> 
  </td>   
</tr>  
 
</table>      




