
<table width="100%">
            <tr> 
              <td class="titulos1" width="25%"  align="right">ANALISIS</td>  
			  <td class="titulos1" width="25%"  align="right">RESULTADOS</td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">COLOR:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAFV_C1" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">ASPECTO:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAFV_C2" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CANTIDAD:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAFV_C3" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">KOH:&nbsp;&nbsp;</td>  
			  <td align="left">
			   <select size="1" id="txt_LAFV_C4" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="Positivo">Positvo</option>
                    <option value="Negativo">Negativo</option>
                 </select> 	  
			  </td>			  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">TEST DE AMINAS:&nbsp;&nbsp;</td>  
			  <td align="left">
			   <select size="1" id="txt_LAFV_C5" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="Positivo">Positvo</option>
                    <option value="Negativo">Negativo</option>
                 </select> 	  
			  </td>		  
              <td></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">PH:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAFV_C6" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">TRICHOMONAS VAGINALES:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_LAFV_C7" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="Positivo">Positvo</option>
                    <option value="Negativo">Negativo</option>
                 </select> 	  
			  </td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">CELULAS GUIAS:&nbsp;&nbsp;</td>  
			  <td align="left">
			   <select size="1" id="txt_LAFV_C8" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="Positivo">Positvo</option>
                    <option value="Negativo">Negativo</option>
                 </select> 	  
			  </td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
				<td align="right">LEVADURAS:&nbsp;&nbsp;</td>  
			  	<td align="left">
				<select size="1" id="txt_LAFV_C9" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="+">+</option>
                    <option value="++">++</option>
                    <option value="++">+++</option>
                    <option value="++">No se observa</option>
                 </select> 	  
			  </td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">PSEUDOMICELIOS:&nbsp;&nbsp;</td>  
			  <td align="left">
				<select size="1" id="txt_LAFV_C10" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="+">+</option>
                    <option value="++">++</option>
                    <option value="++">+++</option>
                    <option value="++">No se observa</option>
                 </select> 	  
			  </td>				  
              <td></td>  
			</tr> 
			<tr class="camposRepInp">
				<td bgcolor="#c0d3c1" align="right" colspan"2">GRAM EXOCERVICAL</td>
				<td bgcolor="#c0d3c1"></td>
				<td></td>		
			</tr>
			<tr class="estiloImput"> 
              <td align="right">LEUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAFV_C11" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">FLORA NORMAL:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAFV_C12" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">FLORA PREDOMINANTE:&nbsp;&nbsp;</td>  
			  <td align="left">
                    <textarea type="text" id="txt_LAFV_C13"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
			  </td>			  
              <td></td>  
			</tr> 
			<tr class="camposRepInp">
				<td bgcolor="#c0d3c1" align="right" colspan"2">GRAM ENDOCERVICAL</td>
				<td bgcolor="#c0d3c1"></td>
				<td></td>		
			</tr>
			<tr class="estiloImput"> 
              <td align="right">LEUCOCITOS:&nbsp;&nbsp;</td>  
			  <td align="left"><input type="text" id="txt_LAFV_C14" size="100"  maxlength="40" style="width:50%"/></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">DIPLOCOCOS GRAM NEGATIVOS EXTRA CELULARES:&nbsp;&nbsp;</td>  
			  <td align="left">
			   <select size="1" id="txt_LAFV_C15" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="Positivo">Positvo</option>
                    <option value="Negativo">Negativo</option>
                 </select> 	  
			  </td>				  
              <td></td>  
			</tr>		
			<tr class="estiloImput"> 
              <td align="right">DIPLOCOCOS GRAM NEGATIVOS INTRA CELULARES:&nbsp;&nbsp;</td>  
			  <td align="left">
			   <select size="1" id="txt_LAFV_C16" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                    <option value=""></option>
                    <option value="Positivo">Positvo</option>
                    <option value="Negativo">Negativo</option>
                 </select> 	  
			  </td>			  
              <td></td>  
			</tr> 
			
</table>  
 
 
  

<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACION</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_LAFV_C17"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr> 
  
           
      </table> 
  </td>
</tr>   
</table>  



 
