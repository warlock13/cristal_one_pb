﻿var arrayOfRolloverClasses = new Array();
var arrayOfClickClasses = new Array();
var activeRow = false;
var activeRowClickArray = new Array();



function seleccionarFila(fila) {
           if (document.getElementById(fila).className == "tableRowClickEffect"){
                document.getElementById(fila).className = "overs";
            }  else {
               document.getElementById(fila).className = "tableRowClickEffect"
           }
}
function highlightTableRow()
{
	var tableObj = this.parentNode;
	if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;
	if(this!=activeRow){
		this.setAttribute('origCl',this.className);
		this.origCl = this.className;
	}
	this.className = arrayOfRolloverClasses[tableObj.id];
	activeRow = this;
}
function clickOnTableRow()
{
	var tableObj = this.parentNode;
	if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

	if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
		activeRowClickArray[tableObj.id].className='';
	}
	this.className = arrayOfClickClasses[tableObj.id];
	activeRowClickArray[tableObj.id] = this;

}
function resetRowStyle()
{
	var tableObj = this.parentNode;
	if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;
	if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
		this.className = arrayOfClickClasses[tableObj.id];
		return;
	}
	var origCl = this.getAttribute('origCl');
	if(!origCl)origCl = this.origCl;
	this.className=origCl;
}

function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
{
		//alert(777)
	arrayOfRolloverClasses[tableId] = whichClass;
	arrayOfClickClasses[tableId] = whichClassOnClick;
	var tableObj = document.getElementById(tableId);
	var tBody = tableObj.getElementsByTagName('TBODY');
	if(tBody){
		var rows = tBody[0].getElementsByTagName('TR');
	}else{
//		var rows = tableObj.getElementsByTagName('TR');
		var rows = tBody[0].getElementsByTagName('TR');
	}
	for(var no=0;no<rows.length;no++){
		rows[no].onmouseover = highlightTableRow;
		rows[no].onmouseout = resetRowStyle;

		if(whichClassOnClick){
			rows[no].onclick = clickOnTableRow;  
		}
	}
}

function marcarFilaTabla(){
	
	addTableRolloverEffect('listDocumentosHistoricos','overs','tableRowClickEffect');	
}
/* */


var vectorDocs = new Array();
var valorGuardado = new Array();



function ocultarAcordionesSegunTipoDocumento( tipoEvolucion){ 

  ocultar('divAcordionAntecedentes');
  ocultar('divAcordionAntecedentesFarmacologicos');
  ocultar('divAcordionRevisionPorSistemas');
  ocultar('divAcordionExamenFisico');
  ocultar('divAcordionProcedimientosCir');	 	/*para modulo cirugia pre pos*/  
  ocultar('divAcordionSignosVitales');    
  ocultar('divAcordionDiagnosticos');	 		/*conducta y tratamiento*/
  ocultar('divAcordionProcedimientos');  
  ocultar('divAcordionAdmMedicamentos');    
  ocultar('divAcordionHojaGastos');    
  ocultar('divAcordionIndicaciones'); 			
  ocultar('divAcordionTrabajos'); 			  
  mostrar('divAcordionContenidos');          
  mostrar('divAcordionAdjuntos');    
  mostrar('divMotivoEnfermedad');    
  
  switch(tipoEvolucion){ 
      case 'NENF':              
		    mostrar('divAcordionAntecedentes');
		    mostrar('divAcordionAdmMedicamentos');
		    mostrar('divAcordionHojaGastos');	
			mostrar('divAcordionSignosVitales');  
			ocultar('divMotivoEnfermedad');
	 break;
	 case 'AOCT':              
		    mostrar('divAcordionProcedimientos');
	 break;
	 case 'NCCI': 
            mostrar('divAcordionProcedimientosCir');		 
		    mostrar('divAcordionAdmMedicamentos');
		    mostrar('divAcordionHojaGastos');	
			mostrar('divAcordionSignosVitales');
			ocultar('divMotivoEnfermedad');
	 break;
	 case 'NERE': 
            mostrar('divAcordionProcedimientosCir');		 
		    mostrar('divAcordionAdmMedicamentos');
		    mostrar('divAcordionHojaGastos');	
			mostrar('divAcordionSignosVitales');
			ocultar('divMotivoEnfermedad');
	 break;	 
     case 'NEJE': 
            mostrar('divAcordionProcedimientosCir');		 
		    mostrar('divAcordionAdmMedicamentos');
		    mostrar('divAcordionHojaGastos');	
			mostrar('divAcordionSignosVitales');    													
		    mostrar('divAcordionProcedimientos');
			ocultar('divMotivoEnfermedad');
	 break;	 
     case 'EVME': 	 		 
		    mostrar('divAcordionRevisionPorSistemas');
		    mostrar('divAcordionExamenFisico');
		    mostrar('divAcordionProcedimientos');		
            mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionHojaGastos');		
	 break;	
	 case 'HQUI': 
            mostrar('divAcordionProcedimientosCir');	
            mostrar('divAcordionDiagnosticos');		
            mostrar('divAcordionProcedimientos'); 
			mostrar('divAcordionHojaGastos');			
	 break;	 
	 
	 case 'HQLA': 
            mostrar('divAcordionProcedimientosCir');	
            mostrar('divAcordionDiagnosticos');		
            mostrar('divAcordionProcedimientos'); 			
	 break;
	 case 'RPRE': 
            mostrar('divAcordionProcedimientosCir');		 
		    mostrar('divAcordionAdmMedicamentos');
		    mostrar('divAcordionHojaGastos');	
			mostrar('divAcordionSignosVitales');    													
	 break;	    
     case 'LICH': 
            mostrar('divAcordionProcedimientosCir');	
            mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionSignosVitales'); 	
			mostrar('divAcordionHojaGastos');			
	 break;	
	 case 'LICL': 
            mostrar('divAcordionProcedimientosCir');	
            mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionSignosVitales'); 	
			mostrar('divAcordionHojaGastos');			
	 break;		 
     case 'EXOF': 
			mostrar('divAcordionAntecedentes');
			mostrar('divAcordionRevisionPorSistemas');
			mostrar('divAcordionExamenFisico');
			ocultar('divAcordionSignosVitales');    
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionProcedimientos'); 
			mostrar('divAcordionAntecedentesFarmacologicos');
	 break;	
	case 'EVMI': 
			mostrar('divAcordionSignosVitales');       
	 break;		 
	 case 'ECOF': 
			mostrar('divAcordionAntecedentesFarmacologicos');
			mostrar('divAcordionAntecedentes');
			mostrar('divAcordionRevisionPorSistemas');
			mostrar('divAcordionExamenFisico');
			ocultar('divAcordionSignosVitales');    
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionProcedimientos');  
	 break;		 
	 case 'EXOP': 
			mostrar('divAcordionAntecedentesFarmacologicos');
			mostrar('divAcordionAntecedentes');
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionProcedimientos');  
			mostrar('divAcordionTrabajos');  						
	 break;	
	 case 'EVOP': 
			mostrar('divAcordionAntecedentesFarmacologicos');
			mostrar('divAcordionAntecedentes');
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionProcedimientos');  
			mostrar('divAcordionTrabajos');  									
	 break;	
	 case 'EVOF': 
			mostrar('divAcordionAntecedentes');
			mostrar('divAcordionRevisionPorSistemas');
			mostrar('divAcordionExamenFisico');
			ocultar('divAcordionSignosVitales');    
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionProcedimientos'); 				
	 break;	
	 case 'REOF': 
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos');  			
	 break;	
	 case 'EXBV': 
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos'); 			
	 break;	
	 case 'EXOR': 
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos');			
	 break;	
	 case 'EVOR': 
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos');			
	 break;
	  case 'TAOR': 
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos');			
	 break;
	 case 'ADXI': 
			//ocultar('divAcordionProcedimientosCir');
	 break;	
	 case 'ADAN': 
			mostrar('divAcordionDiagnosticos');	 	
            mostrar('divAcordionHojaGastos');   			
	 break;	
	  case 'BIOM': 
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionHojaGastos');   			
	 break;	
	 case 'FNYP': 			
			mostrar('divAcordionHojaGastos');   			
	 break;		 
	 case 'ECOG': 
			mostrar('divAcordionDiagnosticos');	 	
	 break;	 
	 case 'TEBV': 
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos');			
	 break;
	 case 'TEOR': 
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos');			
	 break;
	 case 'EVBV': 
			mostrar('divAcordionExamenFisico');	
			mostrar('divAcordionDiagnosticos');	
			mostrar('divAcordionProcedimientos');
	 break;	 	 
	 
	 /********************* HEMODIALISIS */
	 case 'CEXP': 
			mostrar('divAcordionAntecedentes');
			mostrar('divAcordionRevisionPorSistemas');
			mostrar('divAcordionExamenFisico');
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionProcedimientos'); 
			mostrar('divAcordionIndicaciones'); 
			mostrar('divCEXP');
			ocultar('divCEXC');			
	 break;	 	
	 case 'CEXC': 
			mostrar('divAcordionAntecedentes');
			mostrar('divAcordionRevisionPorSistemas');
			mostrar('divAcordionExamenFisico');
			mostrar('divAcordionDiagnosticos');	 	
			mostrar('divAcordionProcedimientos'); 
			mostrar('divAcordionIndicaciones'); 
			ocultar('divCEXP');
			mostrar('divCEXC');			
	 break;		
	 case 'NUTR': 
			mostrar('divAcordionAntecedentes');	 	
	 break;	   
	 case 'PSIP': 
			mostrar('divAcordionAntecedentes');	 	
	 break;		 
	 case 'PSIC': 
			mostrar('divAcordionAntecedentes');	 	
	 break;		
	 case 'TSOC': 
			mostrar('divAcordionAntecedentes');	 	
	 break;
	 case 'TSOP': 
			mostrar('divAcordionAntecedentes');	 	
	 break;
	 case 'RSHA': 
			mostrar('divAcordionSignosVitales');   
            mostrar('divAcordionAdmMedicamentos');    
            mostrar('divAcordionHojaGastos');   
	 break;	  	 
	 case 'RSHC': 
			mostrar('divAcordionSignosVitales');   
            mostrar('divAcordionAdmMedicamentos');    
            mostrar('divAcordionHojaGastos');   
	 break;	  	 
	 case 'EVNE': 
			mostrar('divAcordionSignosVitales');   
            mostrar('divAcordionAdmMedicamentos');    
            mostrar('divAcordionHojaGastos');   
	 break;	
	 case 'INTE': 
			mostrar('divAcordionSignosVitales');   
            mostrar('divAcordionProcedimientos');  
			mostrar('divAcordionDiagnosticos');	 				 
	 break;	  	
	 case 'ECOO': 
 
	 break;	 
	case 'HCOF': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos'); 
	break;	
	case 'HCOC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos'); 
	break;	
	case 'PSIA': 
 
	 break;	 
	 case 'PSIE': 
 
	 break;
	case 'LAFV': 
 
	 break;	 
	 case 'LAQH': 
 
	 break;	
	 case 'LAQS': 
 
	 break;
	 case 'LAPA': 
 
	 break;
	  case 'EXDP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos'); 
	  break;
		case 'EXDC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos'); 
	  break;
	  case 'EXIP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos'); 
	  break;
	  case 'EXIC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	  case 'EXAP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	  case 'EXAC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	  case 'EXGP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	  case 'EXGC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXNP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXNC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;	
	case 'EXTP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXTC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EOTP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EOTC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;	
	case 'EOTP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXPP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXPC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXMP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXMC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXSP': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;
	case 'EXSC': 
		mostrar('divAcordionAntecedentes');
		mostrar('divAcordionRevisionPorSistemas');
		mostrar('divAcordionExamenFisico');
		mostrar('divAcordionDiagnosticos');	 	
		mostrar('divAcordionProcedimientos');
	  break;	  
	 
	 /********************* HEMODIALISIS */	 
	 
	 /**************************LABORATORIO/*/
	 case 'HEMO': 
			mostrar('divAcordionHojaGastos');		
			ocultar('divMotivoEnfermedad'); 
	 break;	
	 case 'TIEM': 
			mostrar('divAcordionHojaGastos');		
			ocultar('divMotivoEnfermedad'); 
	 break;	
	 case 'UROC': 

	 
	 break;		 
	 case 'GLUC': 
			mostrar('divAcordionHojaGastos');		
			ocultar('divMotivoEnfermedad'); 
	 break;		 
  } 
  
 
}



function traerContenidoDelArray(){ 

  tipoDocumentDestino = '';
  tipoDocumentDestino= valorAtributo('lblTipoDocumento');
  
  
  if(valorAtributo('txtIdEsdadoDocumento')=='0'){ /*solo a los documentos que estan  abiertos*/

	  if(tipoDocumentDestino =='EXOF' || tipoDocumentDestino =='EVOF'  ){ //DESTINO 
	  		//alert('00003');
		 for( J = 0; J <= 49; J++){ 
			
		  // asignaAtributo(valorAtributo('txt_'+tipoDocumentDestino+'_C'+(J+1)), vectorDocs[J],0);	
		   document.getElementById('txt_'+tipoDocumentDestino+'_C'+(J+1)).value = vectorDocs[J];
		 }
	  }
  }else alert('! PARA PEGAR EL DOCUMENTO DESTINO DEBE ESTAR ABIERTO')
  
  
}

function copiarContenidoAlArray(){ 

   tipoDocumentDestino = '';
  tipoDocumentDestino= valorAtributo('lblTipoDocumento');
  if(valorAtributo('lblTipoDocumento')=='EXOF'){ //ORIGEN
     for( i = 0; i <= 49; i++){ 
		//alert('POS: '+i+ ' ' +valorAtributo('txt_EXOF_C'+(i+1)));				
	  // vectorDocs[i] = valorAtributo('txt_EXOF_C'+(i+1)) 
	  vectorDocs[i] =document.getElementById('txt_'+tipoDocumentDestino+'_C'+(i+1)).value;
     }
  }
}

function acordionArchivosAdjuntos(){ 
   tabActivo='divArchivosAdjuntos';
   cargarUnaPagina('/clinica/paginas/hc/hc/adjuntos.jsp', 'divParaArchivosAdjuntos');
   cerrarPopupArchivosPdf()
}




function traerContenidoDocumentoSeleccionado_(tipoDocumento, idDocumento, clase){  //calse =  a txt o lbl
	// limpiarDivEditarJuanDocumentos(tipoDocumento);

	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarHc_xml.jsp',true);
	 varajaxInit.onreadystatechange=function(){respuestatraerContenidoDocumentoSeleccionado(tipoDocumento, clase)};
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');			  			
	 valores_a_mandar="accion=contenidoDocumentoHC"; 
	 valores_a_mandar=valores_a_mandar+"&TipoDocumento="+tipoDocumento; 	 	 
	 valores_a_mandar=valores_a_mandar+"&idDocumento="+idDocumento; 

	 varajaxInit.send(valores_a_mandar);

}
function respuestatraerContenidoDocumentoSeleccionado_(tipoDocumento, clase){
	
	if(varajaxInit.readyState == 4 ){      
		  if(varajaxInit.status == 200){
			  raiz=varajaxInit.responseXML.documentElement;
			  if(raiz.getElementsByTagName('rows')!=null){ 
				   totalRegistros=raiz.getElementsByTagName('cell').length;  	
				   if(totalRegistros>0){   
					   for( i = 0; i < totalRegistros; i++){ 
					      asignaAtributo(clase+tipoDocumento+'_C'+(i+1), $.trim(raiz.getElementsByTagName('cell')[i].firstChild.data),0);						
					   }
					   if(POSICION_VECTOR > 0 && POSICION_VECTOR_MAX != 0)
					      recorrerImpresion()
				   }

			  }	                           
		   }else{
				  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		   }
		 }
	if(varajaxInit.readyState == 1){
		//abrirVentana(260, 100);
		//VentanaModal.setSombra(true);
	}
}



function imprimirPdfEncabezados(){
	   asignaAtributo('lbl_idElaboro', valorAtributo('txtIdProfesionalElaboro'), 0)
	   asignaAtributo('lbl_Historia', valorAtributo('lblIdHc'), 0)	    
	   asignaAtributo('lbl_idCliente', valorAtributo('lblIdPaciente'), 0)	    
	   asignaAtributo('lbl_nombresCompletos', valorAtributo('lblNombrePaciente'), 0) 	
	   asignaAtributo('lbl_idTipodocumento', valorAtributo('lblIdDocumento'), 0); 
	   
	   asignaAtributo('lblFirmaProfesional', '<img width="200" height="85" style="cursor:pointer" src="'+rutaFirmas+valorAtributo('txtIdProfesionalElaboro')+'.png ">',1);	
}
function imprimirPdfEncabezadosAnexos(){
	   asignaAtributo('lbl_idElaboro', valorAtributo('txtIdProfesionalElaboro'), 0)
	   asignaAtributo('lbl_Historia', valorAtributo('lblIdHc'), 0)	    
	   asignaAtributo('lbl_idCliente', valorAtributo('lblIdPaciente'), 0)	    
	   asignaAtributo('lbl_nombresCompletos', valorAtributo('lblNombrePaciente'), 0) 	
	   asignaAtributo('lbl_idTipodocumento', valorAtributo('lblIdDocumento'), 0)
	   asignaAtributo('lblFirmaProfesionalAnexos', '<img width="120" height="40" style="cursor:pointer" src="'+rutaNombres+valorAtributo('txtIdProfesionalElaboro')+'.png ">',1);	
}
function imprimirAnexos(){ 
  	
		imprimirPdfEncabezadosAnexos(); 
		if(verificarCamposGuardar('pdfAnexo3')){		
		asignaAtributo('lbl_JusficacionClinica', $('#drag'+ventanaActual.num).find('#txtJustificacion').val() )
		asignaAtributo('lblIdUsuario', valorAtributo('txtIdUsuario'),0 )	
		asignaAtributo('lblLogin', valorAtributo('txtLogin'),0 )		
		
			if(imprimirPdfProcedimientos()){
			   if(imprimirPdfDiagnosticosAnexos()){
				   imprimirCC('documentoProcedimientoPDF'); 
			   }
			}
		}
}
function botonImprimirAnexoDos(){
   buscarInformacionPaciente();
   
   setTimeout("imprimirAnexo2()",1000);

}

function imprimirAnexo2(){ 
  	
		
		imprimirPdfEncabezadosAnexos(); 
		if(verificarCamposGuardar('pdfAnexo2')){
			asignaAtributo('lbl_MotivoConsulta', $('#drag'+ventanaActual.num).find('#txt_VLUR_C19').val() )
			asignaAtributo('lblIdUsuario', valorAtributo('txtIdUsuario'),0 )	
			asignaAtributo('lblLogin', valorAtributo('txtLogin'),0 )		
			
			   if(imprimirPdfDiagnosticosAnexo2()){
				   imprimirCC('anexo2PDF'); 
			   }
		}

}

opcionSincronic = 0;


function imprimirSincronicoPdf(opcionSincronic){   // alert('AAAA='+opcionSincronic)

  if( valorAtributo('txtIdDocumentoVistaPrevia') =='' ){ 
	  TipoDocumento = valorAtributo('lblTipoDocumento')
      idDocumento   = valorAtributo('lblIdDocumento')
      idAdmision   = valorAtributo('lblIdAdmision')	  
  }
  else{ 	 
      TipoDocumento = valorAtributo('txtTipoDocumentoVistaPrevia')	   
      idDocumento   = valorAtributo('txtIdDocumentoVistaPrevia') 
      idAdmision   = valorAtributo('lblIdAdmisionVistaPrevia')	  	  
  }
 
  switch (opcionSincronic){	
    case 1: 
        llenarTablaDesdeUnReporte(113,idDocumento,1,'listFinalizarImprimirPDF')  
	break;		
    case 2: 
        llenarTablaDesdeUnReporte(95,idDocumento,5,'listPaciente1PDF')  						
	break;
    case 3: 
        llenarTablaDesdeUnReporte(96,idDocumento,8,'listPaciente2PDF')  						
	break;	
    case 4: 
        llenarTablaDesdeUnReporte(97,idDocumento,7,'listPaciente3PDF')  						
	break;
    case 5: // recorrerImpresion()	
		   llenarTablaDesdeUnReporte(149,idAdmision,3,'listMotivoEnfermedadPDF')  	 /*procedimientos cirugia*/					
	break;
    case 55: //  recorrerImpresion()	 
           llenarTablaDesdeUnReporte(105,idDocumento,3,'listMotivoEnfermedadPDF')  						
	break;	
    case 6: // alert(888889); //imprimirSincronicoPdf(8)
           llenarTablaDesdeUnReporte(103,idDocumento,5,'listAntecedentesPDF')  	
		  // recorrerImpresion()					
	break;
    case 7: 
	       llenarTablaDesdeUnReporte(102,idDocumento,3,'listSistemasPDF')  						
		  //		   recorrerImpresion()					
	break;	
    case 8: //  recorrerImpresion()	
        llenarTablaDesdeUnReporte(101,idDocumento,9,'listSignosVitalesPDF')  						
	break;
    case 9: 
		llenarTablaDesdeUnReporte(100,idDocumento,6,'listDiagnosticosPDF')  				
	break;
    case 10: 
        //llenarTablaDesdeUnReporte(104,idDocumento,6,'listPlanPDF')  		
		 llenarTablaDesdeUnReporte(150,idDocumento,6,'listPlanPDF')  

	break;	
    case 11: 
        llenarTablaDesdeUnReporte(128,idDocumento,6,'listMedicamentosPDF')  		
	break;	
    case 12: 
        llenarTablaDesdeUnReporte(106,idDocumento,2,'listProfesionalPDF')  		
	break;	
    case 13:  /* paraformato especialidad*/ // alert('BBBB')
		llenarTablaEspecialidad(  TipoDocumento,idDocumento);		
	break;
	case 14:  /* para administracion de medicamentos */ 
		llenarTablaDesdeUnReporte(141,idDocumento,8,'listAdmMedicamentosPDF') 	
	break;
	
	case 111:
	    
		recorrerImpresion();
	break;

  }  
  
}


/*******************para llenar table de examen especialidad*******************************************/
								
				
function llenarTablaEspecialidad(tipoEvolucion, idEvolucion){  
    ruta = '/clinica/paginas/hc/documentosHistoriaClinica/'
	pagina = tipoEvolucion+'_PDF.jsp';
	//idDivTitulo = 'divContenidos';
//	$('#drag'+ventanaActual.num).find('#'+idDivTitulo+idDivTitulo).html($.trim( nombreEvolucion ));  /*para el titulo del acordion*/
	
	cargarMenuTablaEspecialidad(ruta + pagina,  tipoEvolucion, idEvolucion)
}
function cargarMenuTablaEspecialidad(pagina,  tipoEvolucion, idEvolucion){   
	
		   varajaxMenu=crearAjax();
           valores_a_mandar="";
		   varajaxMenu.open("POST",pagina,true);
	   	   varajaxMenu.onreadystatechange=function(){llenarinfoPaginaEspecialidad(tipoEvolucion, idEvolucion)};	       
		   varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		   varajaxMenu.send(valores_a_mandar);   

 }	
function llenarinfoPaginaEspecialidad(tipoEvolucion, idEvolucion){ 
		if(varajaxMenu.readyState == 4 ){
			if(varajaxMenu.status == 200){
			    document.getElementById("divParaExamenEspecialidad").innerHTML = varajaxMenu.responseText;
				traerContenidoDocumentoSeleccionado(tipoEvolucion, idEvolucion, 'lbl_')
				
			}else{
				swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			}
		 } 
		if(varajaxMenu.readyState == 1){
		     swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
		 }
} 
function eliminarElemento(id){/*eliminar un elemento html*/
	imagen = document.getElementById(id);	
	if (!imagen){
		alert("El elemento selecionado no existe "+id);
	} else {
		padre = imagen.parentNode;
		padre.removeChild(imagen);
	}
} 
/*****************************   cargar tabla cirugia ******************************************/
function cargarTableListaEsperaCirugia(){
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/atusuario/cirugia/listaEsperaGestion.jsp',true); 
    varajaxMenu.onreadystatechange=llenarcargarTableListaEsperaCirugia;	
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
function llenarcargarTableListaEsperaCirugia(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			 document.getElementById("divParaPaginaListaEspera").innerHTML = varajaxMenu.responseText;

			 asignaAtributo('txtIdBusPaciente', valorAtributo('lblIdPaciente')+'-'+valorAtributo('lblIdentificacionPaciente')+' '+valorAtributo('lblNombrePaciente'),1); 
			 asignaAtributo('cmbIdSitioQuirurgicoLE', valorAtributo('lblIdSitio'),1); 	
			 asignaAtributo('txtIdProcedimientoLE',concatenarCodigoNombre(valorAtributo('lblIdProc'),valorAtributo('lblNombreProc')) ,1); 		 
			 
			 desHabilitar('btnCrearNuevoPaciente',1)
			 eliminarElemento('botoncitoLupita')
			 eliminarElemento('btn_limpiarLE')
			 eliminarElemento('btnCrearNuevoPaciente')	
			 eliminarElemento('btn_buscarLE')  
			 eliminarElemento('tituloForma2')
			 eliminarElemento('idProfesionalesTR')			 	
			 eliminarElemento('cmbIdProfesionales')			 				 
			 eliminarElemento('btn_GuardarLECirugia')
		     calendario('txtFechaVigencia',0); 							 			 
			 calendario('txtFechaNac',0)			 			 			 

			 setTimeout('buscarInformacionPacienteExistenteLE()',250)	; 
			 setTimeout("buscarAGENDA('listCitaCirugiaProcedimientoLEGestion')" , 1000); 			 
			 
			// setTimeout(cargarComboGRAL('cmbPadre','deptoDefecto','cmbIdEspecialidad',123),4500)			 
			  		 			 
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  
/***************************** fin   cargar tabla cirugia ******************************************/
 
function cargarTableListaEspera(){
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/atusuario/citas/listaEspera.jsp',true); 
    varajaxMenu.onreadystatechange=llenarcargarTableListaEspera;
	
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
 

 
function llenarcargarTableListaEspera(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaListaEspera").innerHTML = varajaxMenu.responseText;

			//  buscarInformacionNoPos(tipoFormato) 

			 if(valorAtributo('txtPrincipalHC')=='OK')
			     asignaAtributo('txtIdBusPaciente', valorAtributo('lblIdPaciente')+'-'+valorAtributo('lblIdentificacionPaciente')+' '+valorAtributo('lblNombrePaciente'),1); // para desde historia clinica
			 else 
			     asignaAtributo('txtIdBusPaciente', valorAtributo('lblIdPaciente')+'-'+valorAtributo('lblIdentificacion')+' '+valorAtributo('lblNombrePaciente'),1); 	//	para desde post consulta	 
			 
			 asignaAtributo('txtClaseEntrada','CONS')

			 setTimeout('buscarInformacionPacienteExistenteLE()',1000)
			 desHabilitar('btnCrearNuevoPaciente',1)
			 eliminarElemento('botoncitoLupita')
			 eliminarElemento('btn_limpiarLE')
			// eliminarElemento('btnCrearNuevoPaciente')	
			// eliminarElemento('btn_fosyga')				 		 			 
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  
/*****************************  cargar tabla le call center******************************************/

/*****************************   NO POS ******************************************/
function cargarTableVistaPreviaNoPos(tipoFormato){
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/hc/'+tipoFormato+'.jsp',true); 
    varajaxMenu.onreadystatechange=function(){llenarcargarTableVistaPreviaNoPos(tipoFormato)};	
	
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
function llenarcargarTableVistaPreviaNoPos(tipoFormato){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaNoPos").innerHTML = varajaxMenu.responseText;
			  buscarInformacionNoPos(tipoFormato) 
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  
/*****************************   FIN NO POS******************************************/

/******************** AnexoNoPos procedimientos ******************************************/

function imprimirAnexoNoPosProcedimientos(){ 

    cargarTableVistaPreviaAnexoNoPosProcedimientos()
    setTimeout("mostrar('divVentanitaPdf')",1000); 	
}
function cargarTableVistaPreviaAnexoNoPosProcedimientos(){ 
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/documentosHistoriaClinicaPDF/anexoNoPosProcedimientosPdf.jsp',true);
	varajaxMenu.onreadystatechange=llenarcargarTableVistaPreviaAnexoNoPosProcedimientos;
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
function llenarcargarTableVistaPreviaAnexoNoPosProcedimientos(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaVistaPrevia").innerHTML = varajaxMenu.responseText;
			  opcionSincronic = 0;		
			  imprimirSincronicoPdfAnexoNoPosProcedimientos(1) /*una vez exista el html procede a llenarlos anexo3*/
			  opcionSincronic = 0;		
     	
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  

function imprimirSincronicoPdfAnexoNoPosProcedimientos(opcionSincronic){ 

  switch (opcionSincronic){	
  
    case 1:
         llenarTablaDesdeUnReporteNoPosProcedimientos(120,valorAtributo('lblIdDocumento'),1,'listFinalizarImprimirPDF')  
	break;	
    case 2: 
        llenarTablaDesdeUnReporteNoPosProcedimientos(109,valorAtributo('lblIdDocumento'),2,'listAdministradoraPDF')  						
	break;
    case 3: 
        llenarTablaDesdeUnReporteNoPosProcedimientos(96,valorAtributo('lblIdDocumento'),8,'listPaciente2PDF')  						
	break;	
    case 4: 
        llenarTablaDesdeUnReporteNoPosProcedimientos(100,valorAtributo('lblIdDocumento'),6,'listDiagnosticosPDF')  				
	break;
	case 5: 
        //llenarTablaDesdeUnReporteNoPosProcedimientos(93,valorAtributo('lblId'),2,'listMedicamentoUtilizado1PDF')
		llenarTablaDesdeUnReporteNoPosProcedimientos(153,valorAtributo('lblId'),2,'listMedicamentoUtilizado1PDF')  		
	break;		
    case 6: 
        llenarTablaDesdeUnReporteNoPosProcedimientos(106,valorAtributo('lblIdDocumento'),1,'listProfesionalPDF')  		
	break;	
    case 7:
        llenarTablaDesdeUnReporteNoPosProcedimientos(94,valorAtributo('lblIdDocumento'),1,'listHoraFechaNoSolicitudPDF')  		
	break;	

  }
}
function llenarTablaDesdeUnReporteNoPosProcedimientos(idReporte,parametro1,cantColumnasQuery, tablaReportesDestino){ 
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesDestino;	 
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDesdeReporteNoPosProcedimientos;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
	 varajaxInit.send(valores_a_mandar); 
}

/*2- llamar */
function respuestallenarTablaDesdeReporteNoPosProcedimientos(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					   totalRegistros=raiz.getElementsByTagName('c1').length;  	
					 

					   if(totalRegistros){    
						  agregarDatosTablaDeReporteBlanco(raiz,totalRegistros);	
						  opcionSincronic++;
						  imprimirSincronicoPdfAnexoNoPosProcedimientos(opcionSincronic)						  
					   }
					   else{
						   alert("Sin datos en el reporte, cierre y vuelva a generarlo");
					   }				   

				 }	                           
			 }else{
					swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			 }
		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}

/******************  FIN AnexoNoPos procedimientos ********************************************/
/******************** inicio AnexoNoPos medicamentos******************************************/

function imprimirAnexoNoPos(){ 
    cargarTableVistaPreviaAnexoNoPos()
    setTimeout("mostrar('divVentanitaPdf')",1000); 	
}
function cargarTableVistaPreviaAnexoNoPos(){ 
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/documentosHistoriaClinicaPDF/anexoNoPosPdf.jsp',true);
	varajaxMenu.onreadystatechange=llenarcargarTableVistaPreviaAnexoNoPos;
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
function llenarcargarTableVistaPreviaAnexoNoPos(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaVistaPrevia").innerHTML = varajaxMenu.responseText;
			  opcionSincronic = 0;		
			  imprimirSincronicoPdfAnexoNoPos(1) /*una vez exista el html procede a llenarlos anexo3*/
			  opcionSincronic = 0;		
     	
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  

function imprimirSincronicoPdfAnexoNoPos(opcionSincronic){ 

  switch (opcionSincronic){	
  
    case 1:
         llenarTablaDesdeUnReporteNoPos(120,valorAtributo('lblIdDocumento'),1,'listFinalizarImprimirPDF')  
	break;	
    case 2: 
        llenarTablaDesdeUnReporteNoPos(109,valorAtributo('lblIdDocumento'),2,'listAdministradoraPDF')  						
	break;
    case 3: 
        llenarTablaDesdeUnReporteNoPos(96,valorAtributo('lblIdDocumento'),8,'listPaciente2PDF')  						
	break;	
    case 4: 
        llenarTablaDesdeUnReporteNoPos(100,valorAtributo('lblIdDocumento'),6,'listDiagnosticosPDF')  				
	break;
    case 5:  
        llenarTablaDesdeUnReporteNoPos(115,valorAtributo('lblId'),2,'listResumenEnferActualTratamientoPDF')  		
	break;		
    
	case 6: 
        llenarTablaDesdeUnReporteNoPos(116,valorAtributo('lblId'),3,'listMedicamentoUtilizado1PDF')  		
	break;		
	
    case 7: 
        llenarTablaDesdeUnReporteNoPos(117,valorAtributo('lblId'),3,'listMedicamentoUtilizado2PDF')  		
	break;	

    case 8: 
        llenarTablaDesdeUnReporteNoPos(118,valorAtributo('lblId'),5,'listMedicamentoNoPosSolicitadoPDF')  		
	break;	
    case 9: 
        llenarTablaDesdeUnReporteNoPos(119,valorAtributo('lblId'),2,'listPreguntasPDF')  		
	break;	
    case 10: 
        llenarTablaDesdeUnReporteNoPos(106,valorAtributo('lblIdDocumento'),1,'listProfesionalPDF')  		
	break;	
    case 11:
        llenarTablaDesdeUnReporteNoPos(121,valorAtributo('lblId'),1,'listHoraFechaNoSolicitudPDF')  		
	break;	

  }
}
function llenarTablaDesdeUnReporteNoPos(idReporte,parametro1,cantColumnasQuery, tablaReportesDestino){ 
   
   if(idReporte!='' & parametro1!=''  ){
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesDestino;	 
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDesdeReporteNoPos;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
	 varajaxInit.send(valores_a_mandar); 
   }
   else alert('Falta seleccionar el Elemnto no pos a Exportar')
}

/*2- llamar */
function respuestallenarTablaDesdeReporteNoPos(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					   totalRegistros=raiz.getElementsByTagName('c1').length;  	
					 

					   if(totalRegistros){    
						  agregarDatosTablaDeReporteBlanco(raiz,totalRegistros);	
						  opcionSincronic++;
						  imprimirSincronicoPdfAnexoNoPos(opcionSincronic)						  
					   }
					   else{
						   alert("Sin datos en el reporte, cierre y vuelva a generarlo");
					   }				   

				 }	                           
			 }else{
					swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			 }
		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}

/******************  FIN AnexoNoPos ********************************************/


/********************************************formula oftalmica****************/

function imprimirAnexoOptalmica(){ 

  if(valorAtributo('lblIdDocumento')!=''){
    bandFormOftal='SI'
    cargarTableVistaPreviaAnexoOptalmica()

	$('#drag'+ventanaActual.num).find("#idDivTransparencia2").css('top', 700 + 'px');

    setTimeout("mostrar('divVentanitaPdf')",500); 
  }
  else alert('SELECCIONE DOCUMENTO CLINICO') 
}
function cargarTableVistaPreviaAnexoOptalmica(){ 
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/documentosHistoriaClinicaPDF/anexoOftalmicaPdf.jsp',true);
	varajaxMenu.onreadystatechange=llenarcargarTableVistaPreviaAnexoOptalmica;
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
function llenarcargarTableVistaPreviaAnexoOptalmica(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaVistaPrevia").innerHTML = varajaxMenu.responseText;
			  opcionSincronic = 0;	
			  llenarArregloImpresion(); /* llama a la funcion del arreglo */
			  //imprimirSincronicoPdfAnexoOptalmica(0) /*una vez exista el html procede a llenarlos anexo3*/
			  opcionSincronic = 0;		
     	
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  

function imprimirSincronicoPdfAnexoOptalmica(opcionSincronic){

  switch (opcionSincronic){	
    case 1:
        llenarTablaDesdeUnReporteOptalmica(125,valorAtributo('lblIdDocumento'),2,'listAdministradoraPDF')  						
	break;
    case 2: 
        llenarTablaDesdeUnReporteOptalmica(96,valorAtributo('lblIdDocumento'),8,'listPaciente2PDF')  						
	break;	
    case 3: 
	    if(valorAtributo('lblIdTipoAdmision')=='P')	llenarTablaDesdeUnReporteOptalmica(123,valorAtributo('lblIdDocumento'),6,'listFormulaOptalmicaPDF') 
		
		else { llenarTablaDesdeUnReporteOptalmica(139,valorAtributo('lblIdDocumento'),6,'listFormulaOptalmicaPDF')  			   	
		}
	break;
    case 4: 
	    if(valorAtributo('lblIdTipoAdmision')=='P'){
			 llenarTablaDesdeUnReporteOptalmica(124,valorAtributo('lblIdDocumento'),3,'listFormulaOptalmicaTextoPDF')  				
		}
		else { llenarTablaDesdeUnReporteOptalmica(140,valorAtributo('lblIdDocumento'),3,'listFormulaOptalmicaTextoPDF')  				 	  
		}
	break;	
    case 5:
		llenarTablaDesdeUnReporteOptalmica(122,valorAtributo('lblIdDocumento'),1,'listFinalizarImprimirPDF')        		 	   
	break;	
    case 6: 	
        llenarTablaDesdeUnReporteOptalmica(106,valorAtributo('lblIdDocumento'),1,'listProfesionalPDF')  		
	break;	
	
	
  }
}
function llenarTablaDesdeUnReporteOptalmica(idReporte,parametro1,cantColumnasQuery, tablaReportesDestino){ 
   
   
   if(idReporte!='' & parametro1!=''  ){
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesDestino;	 
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDesdeReporteOptalmica;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
	 varajaxInit.send(valores_a_mandar); 
   }
   else alert('FALTA PARTE DEL REPORTE PDF REPORTE '+idReporte)
}

/*2- llamar */
function respuestallenarTablaDesdeReporteOptalmica(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					   totalRegistros=raiz.getElementsByTagName('c1').length;  	
					 

					   if(totalRegistros){    
						  agregarDatosTablaDeReporteBlanco(raiz,totalRegistros);	
						  opcionSincronic++;
						  imprimirSincronicoPdfAnexoOptalmica(opcionSincronic)						  
					   }
					   else{
						   alert("Sin datos en el reporte, cierre y vuelva a generarlo");
					   }				   

				 }	                           
			 }else{
					swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			 }
		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}

/********************************************fin formula oftalmica************/


/******************************************** inicio referencia****************/

function imprimirReferencia(){ 
    cargarTableVistaPreviaReferencia()
	$('#drag'+ventanaActual.num).find("#idDivTransparencia2").css('top', 700 + 'px');

    setTimeout("mostrar('divVentanitaPdf')",500); 	
}
function cargarTableVistaPreviaReferencia(){ 
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/documentosHistoriaClinicaPDF/referenciaPdf.jsp',true);
	varajaxMenu.onreadystatechange=llenarcargarTableVistaPreviaReferencia;
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
function llenarcargarTableVistaPreviaReferencia(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaVistaPrevia").innerHTML = varajaxMenu.responseText;
			  opcionSincronic = 0;		
			  imprimirSincronicoReferencia(1) /*una vez exista el html procede a llenarlos anexo3*/
			  opcionSincronic = 0;		
     	
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  

function imprimirSincronicoReferencia(opcionSincronic){ 

  switch (opcionSincronic){	
  
    case 1:
        llenarTablaDesdeUnReporteReferencia(122,valorAtributo('lblIdDocumento'),1,'listFinalizarImprimirPDF')		 	   
	break;	
    case 2: 
        llenarTablaDesdeUnReporteReferencia(125,valorAtributo('lblIdDocumento'),2,'listAdministradoraPDF')  						
	break;
    case 3: 
        llenarTablaDesdeUnReporteReferencia(96,valorAtributo('lblIdDocumento'),8,'listPaciente2PDF')  						
	break;	
    case 4: 
//		if(valorAtributo('lblIdTipoRemi')=='R')
        	llenarTablaDesdeUnReporteReferencia(134,valorAtributo('lblIdRemi'),2,'contenidoReferenciaPDF')  				
	//	else if(valorAtributo('lblIdTipoRemi')=='C')
        //	llenarTablaDesdeUnReporteReferencia(135,valorAtributo('lblIdRemi'),2,'contenidoReferenciaPDF')  				
			
	break;
    case 5: 
        llenarTablaDesdeUnReporteReferencia(100,valorAtributo('lblIdDocumento'),6,'listDiagnosticosPDF')  				
	break;	
    case 6:
        llenarTablaDesdeUnReporteReferencia(106,valorAtributo('lblIdDocumento'),1,'listProfesionalPDF')  		
	break;	
  }
}
function llenarTablaDesdeUnReporteReferencia(idReporte,parametro1,cantColumnasQuery, tablaReportesDestino){ 
   
   
   if(idReporte!='' & parametro1!=''  ){
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesDestino;	 
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDesdeReferencia;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
	 varajaxInit.send(valores_a_mandar); 
   }
   else alert('FALTA PARTE DEL REPORTE PDF REPORTE '+idReporte)
}


function respuestallenarTablaDesdeReferencia(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					 totalRegistros=raiz.getElementsByTagName('c1').length;  	
				   

					 if(totalRegistros){    
						agregarDatosTablaDeReporteBlanco(raiz,totalRegistros);	
						opcionSincronic++;
						imprimirSincronicoReferencia(opcionSincronic)						  
					 }
					 else{
						 alert("Sin datos en el reporte, cierre y vuelva a generarlo");
					 }				   

				 }	                           
			 }else{
					swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			 }

		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}

/********************************************fin referencia ************/



/******************************************** ini formula medicamento****************/

function etiqueta(){
	var espec="";
	if (valorAtributo('txt_MD_PRO') == 'Medicamentos' ){				
		espec ="FORMULA VALIDA POR TREINTA ( 30 ) DIAS";	
	}else{
		espec ="FORMULA VALIDA POR CIENTO OCHENTA ( 180 ) DIAS";			
	}	
	
	$("#lblEspecificacion").text(espec);
	$("#lblEspecificacion").show();
}


function imprimirFormulaMedica(){ 
    cargarTableVistaPreviaFormulaMedica()

	$('#drag'+ventanaActual.num).find("#idDivTransparencia2").css('top', 700 + 'px');

    setTimeout("mostrar('divVentanitaPdf')",300); 	
    setTimeout("etiqueta()",600); 	
		
}
function cargarTableVistaPreviaFormulaMedica(){ 
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/documentosHistoriaClinicaPDF/formulaMedicaPdf.jsp',true);
	varajaxMenu.onreadystatechange=llenarcargarTableVistaPreviaFormulaMedica;
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);  
    var espec ="FORMULA VALIDA POR OCHO ( 8 ) DIAS";	
	if (valorAtributo('txt_MD_PRO') == 'Medicamentos' ){		
		asignaAtributo('lblEspecificacion', espec,0);
	}	
 }
function llenarcargarTableVistaPreviaFormulaMedica(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaVistaPrevia").innerHTML = varajaxMenu.responseText;
			  opcionSincronic = 0;		
			  imprimirSincronicoFormulaMedica(1) /*una vez exista el html procede a llenarlos anexo3*/
			  opcionSincronic = 0;		
     	
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  

function imprimirSincronicoFormulaMedica(opcionSincronic){ 

  switch (opcionSincronic){	
  
    case 1:
        llenarTablaDesdeUnReporteFormulaMedica(122,valorAtributo('lblIdDocumento'),1,'listFinalizarImprimirPDF')		 	   
	break;	
    case 2: 
        llenarTablaDesdeUnReporteFormulaMedica(125,valorAtributo('lblIdDocumento'),2,'listAdministradoraPDF')  						
	break;
    case 3: 
        llenarTablaDesdeUnReporteFormulaMedica(96,valorAtributo('lblIdDocumento'),8,'listPaciente2PDF')  						
	break;	
    case 4: 
	
	  if(valorAtributo('txt_MD_PRO')=='Medicamentos'){
		  asignaAtributo('lblTituloOrdenFormula','FORMULA MEDICA')
		  if(valorAtributo('txt_ES_POS')=='')
			llenarTablaDesdeUnReporteFormulaMedica(128,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF')  				
		  else if(valorAtributo('txt_ES_POS')=='POS')
			llenarTablaDesdeUnReporteFormulaMedica(132,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF')  				
		  else if(valorAtributo('txt_ES_POS')=='NOPOS')
			llenarTablaDesdeUnReporteFormulaMedica(133,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF')  				
	  }
	  else{		  
		  asignaAtributo('lblTituloOrdenFormula','ORDEN MEDICA')
		  		  
		  if(valorAtributo('txt_ES_POS')==''){		  
			//llenarTablaDesdeUnReporteFormulaMedica(104,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF')  						  
			llenarTablaDesdeUnReporteFormulaMedica(150,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF') 			  		    
		  }
		  else if(valorAtributo('txt_ES_POS')=='POS'){				
			//llenarTablaDesdeUnReporteFormulaMedica(92,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF') 
			llenarTablaDesdeUnReporteFormulaMedica(152,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF')  						  		  					
		  }
		  else if(valorAtributo('txt_ES_POS')=='NOPOS')	
			//llenarTablaDesdeUnReporteFormulaMedica(91,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF')
			llenarTablaDesdeUnReporteFormulaMedica(151,valorAtributo('lblIdDocumento'),6,'listFormulaMedicaPDF')  						  		  					
	  }
	break;
    case 5:
	    if(valorAtributo('lblTipoDocumento')!='INTE'){
             llenarTablaDesdeUnReporteFormulaMedica(106,valorAtributo('lblIdDocumento'),1,'listProfesionalPDF'); }
		else imprimirSincronicoFormulaMedica(6)  	
	break;	
    case 6: 
        llenarTablaDesdeUnReporteAnexo3(100,valorAtributo('lblIdDocumento'),6,'listDiagnosticosPDF'); 
	break;
	
  }
}


function llenarTablaDesdeUnReporteFormulaMedica(idReporte,parametro1,cantColumnasQuery, tablaReportesDestino){    
   
   if(idReporte!='' & parametro1!=''  ){
	 //alert('id reporte: '+idReporte+' id_evo: '+parametro1)  
	 cantColumnasP = cantColumnasQuery; 
	 idTablaReportesExcel =  tablaReportesDestino;	 
	 varajaxInit=crearAjax();  
	 varajaxInit.open("POST",'/clinica/paginas/accionesXml/buscarReportes_xml.jsp',true);
	 varajaxInit.onreadystatechange=respuestallenarTablaDesdeFormulaMedica;
	 
	 varajaxInit.setRequestHeader('Content-Type','application/x-www-form-urlencoded');	
	 valores_a_mandar="accion=listadoReporteExcel"; 
	 valores_a_mandar=valores_a_mandar+"&id="+idReporte;
     valores_a_mandar=valores_a_mandar+"&parametro1="+parametro1;//+$('#drag'+ventanaActual.num).find('#txtBusFechaDesde').val();
	 varajaxInit.send(valores_a_mandar); 
   }
   else alert('FALTA PARTE DEL REPORTE PDF REPORTE '+idReporte)
}


function respuestallenarTablaDesdeFormulaMedica(){   
	
	if(varajaxInit.readyState == 4 ){      
	 //   VentanaModal.cerrar();
			if(varajaxInit.status == 200){   
			      raiz=varajaxInit.responseXML.documentElement; 

				  if(raiz.getElementsByTagName('rows')!=null){  
				  
					 totalRegistros=raiz.getElementsByTagName('c1').length;  					   					 
					 if(totalRegistros){    
						agregarDatosTablaDeReporteBlanco(raiz,totalRegistros);	
						opcionSincronic++;
						imprimirSincronicoFormulaMedica(opcionSincronic)						  
					 }
					 else{
						 alert("Sin datos en el reporte, cierre y vuelva a generarlo");
					 }				   

				 }	                           
			 }else{
					swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			 }

		 }
		if(varajaxInit.readyState == 1){
			//	abrirVentana(260, 100);
			  //  VentanaModal.setSombra(true);
		}
}

/********************************************fin formula medicamento************/



/******************** Anexo3 ******************************************/

function imprimirAnexo3Pdf(){ 
  if(valorAtributo('lblIdDocumento')!='' ){
	 cargarTableVistaPreviaAnexo3()
     setTimeout("mostrar('divVentanitaPdf')",1000); 	
 
  }else alert('DEBE DE SELECCIONAR EL ANEXO')
}
function cargarTableVistaPreviaAnexo3(){ 
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/documentosHistoriaClinicaPDF/anexo3Pdf.jsp',true);
	varajaxMenu.onreadystatechange=llenarcargarTableVistaPreviaAnexo3;
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }
function llenarcargarTableVistaPreviaAnexo3(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){
			  document.getElementById("divParaPaginaVistaPrevia").innerHTML = varajaxMenu.responseText;
			  opcionSincronic = 0;		
			  imprimirSincronicoPdfAnexo3(1) /*una vez exista el html procede a llenarlos anexo3*/
			  opcionSincronic = 0;		
	     	  asignaAtributo('lblTituloDocumento', valorAtributo('lblDescripcionTipoDocumento')  ,0);	/*tiene que existir el html*/		  		
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
}  

function imprimirSincronicoPdfAnexo3(opcionSincronic){ 

  switch (opcionSincronic){	
  
    case 1: 
         llenarTablaDesdeUnReporteAnexo3(114,valorAtributo('lblIdDocumento'),1,'listFinalizarImprimirPDF')  
	break;	
    case 2: 
        llenarTablaDesdeUnReporteAnexo3(109,valorAtributo('lblIdDocumento'),2,'listAdministradoraPDF')  						
	break;
    case 3: 
        llenarTablaDesdeUnReporteAnexo3(96,valorAtributo('lblIdDocumento'),8,'listPaciente2PDF')  						
	break;	
    case 4: 
        llenarTablaDesdeUnReporteAnexo3(97,valorAtributo('lblIdDocumento'),7,'listPaciente3PDF')  						
	break;
    case 5:	
        llenarTablaDesdeUnReporteAnexo3(108,valorAtributo('lblIdDocumento'),7,'listAtencionServiciosPDF')  // ENFERMEDAD GENERAL ETC 						
	break;
    case 6: 
        llenarTablaDesdeUnReporteAnexo3(100,valorAtributo('lblIdDocumento'),6,'listDiagnosticosPDF')  				
	break;
    case 7: 
        llenarTablaDesdeUnReporteAnexo3(111,valorAtributo('lblIdDocumento'),5,'listPlanPDF')  		
	break;	
    case 8: 
        llenarTablaDesdeUnReporteAnexo3(112,valorAtributo('lblIdDocumento'),1,'listJustificacionPDF')  		
	break;		
    case 9: 
	    if(valorAtributo('lblTipoDocumento')!='INTE')
             llenarTablaDesdeUnReporteAnexo3(106,valorAtributo('lblIdDocumento'),1,'listProfesionalPDF')  		
		else llenarTablaDesdeUnReporteAnexo3(10)  		        
	break;	
    case 10:
        llenarTablaDesdeUnReporteAnexo3(110,valorAtributo('lblIdDocumento'),2,'listHoraFechaNoSolicitudPDF')  		
	break;	

  }
}
/******************  FIN Anexo3 ********************************************/
var HC_com = 0;

function imprimirHCPdf(){ 
 if( valorAtributo('txtIdDocumentoVistaPrevia') =='' )
   	    TIPO_DOCUMENTO = valorAtributo('lblTipoDocumento') 
    else
	    TIPO_DOCUMENTO = tipo_evolucion
		
  if(TIPO_DOCUMENTO!=''){

	if (confirm("VER RESUMEN DE LA HISTORIA CLINICA ?\n Resumen(Aceptar) --- Completa (Cancelar) ")) {
		formatoPDFHC()
	} else {
		switch (TIPO_DOCUMENTO){
			case 'CEXP':
				HC_com=1;
				formatoPDFHC();
			break;
			/*case 'HQLA':
				HC_com=1;
				formatoPDFHC();
			break;*/
			case 'ECOF':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'LICL':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'EXOF':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'EXOP':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'EVMI':
				HC_com=1;
				formatoPDFHC();
			break;			
			case 'EVOP':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'CEXC':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'ADAN':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'NENF':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'HQUI':
				HC_com=1;
				formatoPDFHC();
			break;
			case 'LICH':
				HC_com=1;
				formatoPDFHC();
			break;
			default:
				cargarTableVistaPrevia()
				setTimeout("mostrar('divVentanitaPdf')",800); 	
			break;
		}		
	}
	HC_com=0;
  }
  else alert('SELECCIONE DOCUMENTO CLINICO')      
}
function imprimirHCPdf_paraFinalizar(){ 
 if( valorAtributo('txtIdDocumentoVistaPrevia') =='' )
   	    TIPO_DOCUMENTO = valorAtributo('lblTipoDocumento') 
    else
	    TIPO_DOCUMENTO = tipo_evolucion
		
  if(TIPO_DOCUMENTO!=''){

	//if (confirm("*VER RESUMEN DE LA HISTORIA CLINICA ?\n Resumen(Aceptar) --- Completa (Cancelar) ")) {
		formatoPDFHC_paraFinalizar()
//	}
	HC_com=0;
  }
  else alert('SELECCIONE DOCUMENTO CLINICO')      
}


function cargarTableVistaPrevia(){ 
//	document.getElementById("divParaPaginaVistaPrevia").innerHTML ='';
	varajaxMenu=crearAjax();
	valores_a_mandar="";
	varajaxMenu.open("POST",'/clinica/paginas/hc/documentosHistoriaClinicaPDF/documentosPdf.jsp',true);
	varajaxMenu.onreadystatechange=llenarcargarTableVistaPrevia;
	varajaxMenu.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);   
 }	
function llenarcargarTableVistaPrevia(){ 
	if(varajaxMenu.readyState == 4 ){
		if(varajaxMenu.status == 200){ 
			  document.getElementById("divParaPaginaVistaPrevia").innerHTML = varajaxMenu.responseText;
			  opcionSincronic = 0;	
			  	
			  llenarArregloImpresion(); /* llama a la funcion del arreglo */
			  opcionSincronic = 0;		
		     
	     	  if(valorAtributo('txtIdDocumentoVistaPrevia')=='')
			     asignaAtributo('lblTituloDocumento', valorAtributo('lblDescripcionTipoDocumento')  ,0);	/*tiene que existir el html*/		  	
			  else 
			     asignaAtributo('lblTituloDocumento', valorAtributo('lblTituloDocumentoVistaPrevia')  ,0);		  				  
			  	 	
			  
		}else{
			  swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	 } 
	if(varajaxMenu.readyState == 1){
		 swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	 }
} 



function imprimirPdfMedicamentos(){
    datosOK = true;
	borrarRegistrosTabla('listOrdenesMedicamentosPDF');
	var ids = jQuery("#listOrdenesMedicamentos").getDataIDs();
	if(ids.length > 0){	
		
		tablabody=document.getElementById('listOrdenesMedicamentosPDF').lastChild;	
			tr=document.createElement("TR");
			tr.className='titulos';					  		  
			th=document.createElement("Td");
			th.colSpan="5";
			th.innerHTML='ORDENES DE MEDICAMENTOS';
			tr.appendChild(th);
		 tablabody.appendChild(tr);	
		 
			tr=document.createElement("TR");
			  tr.className='letraTamano';					  		  
			  th=document.createElement("Td");
			  th.width='35%';		
			  th.innerHTML='ARTICULO';
			tr.appendChild(th);
	  
			  th=document.createElement("Td");
			  th.width='7%';		
			  th.innerHTML='VIA';			
			tr.appendChild(th);
	  
			  th=document.createElement("Td");
			  th.width='15%';		
			  th.innerHTML='DOSIS';								  
			tr.appendChild(th);

			  th=document.createElement("Td");
			  th.width='8%';		
			  th.innerHTML='DURACION';								  
			tr.appendChild(th);
			
			
			  th=document.createElement("Td");
			  th.width='40%';		
			  th.innerHTML='INDICACION'
			tr.appendChild(th);
			
		tablabody.appendChild(tr);	 
		
		for(var i=0;i<ids.length;i++){ 
			var c = ids[i];
			var datosDelRegistro = jQuery("#listOrdenesMedicamentos").getRowData(c);   
	

			  tr=document.createElement("TR");
				tr.className='letraTamano';					  		  
				th=document.createElement("Td");
				th.innerHTML=$.trim(datosDelRegistro.Articulo);
			  tr.appendChild(th);
		
				th=document.createElement("Td");
				th.innerHTML=$.trim(datosDelRegistro.nombre_via);			
			  tr.appendChild(th);
		
				th=document.createElement("Td");
				th.innerHTML=$.trim(datosDelRegistro.detalle_dosis_frecuencia);								  
			  tr.appendChild(th);
			  
				th=document.createElement("Td");
				th.innerHTML=$.trim(datosDelRegistro.nombre_repeticion_programada);	// DURACION							  
			  tr.appendChild(th);
			  
			  
				th=document.createElement("Td");
				th.innerHTML=$.trim(datosDelRegistro.observaciones);								  
			  tr.appendChild(th);
			  
			  tablabody.appendChild(tr);

		}	
	}
    else{  alert('FALTA MEDICACION \n  (El listado debe estar visible )'); datosOK = false; }
 return datosOK;
}



function imprimirPdfDiagnosticosAnexos(){
    datosOK = true;
	borrarRegistrosTabla('listDiagnosticosPDFAnexos');	
	
	var ids = jQuery("#listDx").getDataIDs();

	if(ids.length > 0){
		tablabody=document.getElementById('listDiagnosticosPDFAnexos').lastChild;	
		tablabody.width='100%';

			tr=document.createElement("TR");
			tr.className='titulos';					  		  
			th=document.createElement("Td");
			th.colSpan="4";
			th.innerHTML='IMPRESION DIAGNOSTICA';
			tr.appendChild(th);
		 tablabody.appendChild(tr);
		 
			tr=document.createElement("TR");
			  tr.className='letraTamano3';								  		  
			  th=document.createElement("Td");
			  th.width='30px';		
			  th.innerHTML='COD';
			tr.appendChild(th);
	  
			  th=document.createElement("Td");
			  th.width='55%';		
			  th.innerHTML='NOMBRE';			
			tr.appendChild(th);
	  
			  th=document.createElement("Td");
			  th.width='20%';		
			  th.innerHTML='TIPO';								  
			tr.appendChild(th);
			
			  th=document.createElement("Td");
			  th.width='20%';		
			  th.innerHTML='CLASE'
			tr.appendChild(th);
		tablabody.appendChild(tr);	 	
		
	
		for(var i=0;i<ids.length;i++){ 
			var c = ids[i];
			var datosDelRegistro = jQuery("#listDx").getRowData(c);  
			  
                if(i <4){ // para que salgan 4 registros primeros
               // if(datosDelRegistro.id_documento == valorAtributo('lblIdDocumento')){
					tr=document.createElement("TR");
					  tr.className='letraTamano3';					  
					  th=document.createElement("Td");
					  th.align='left';
					  th.innerHTML=$.trim(datosDelRegistro.id_cie);
					tr.appendChild(th);
			  
					  th=document.createElement("Td");
					  th.align='left';			
					  th.innerHTML=$.trim(datosDelRegistro.nombre);			
					tr.appendChild(th);
			  
					  th=document.createElement("Td");
					  th.align='left';			
					  th.innerHTML=$.trim(datosDelRegistro.tipo);								  
					tr.appendChild(th);
					
					  th=document.createElement("Td");
					  th.align='left';			
					  th.innerHTML=$.trim(datosDelRegistro.clase);								  
					tr.appendChild(th);
					
					tablabody.appendChild(tr);	
				}
		}	
  }//if ids
  else{  alert('FALTA DIAGNOSTICOS DE ESTA EVOLUCION \n  ( El listado debe estar visible )');  datosOK = false;  }
  return datosOK;
}


function imprimirPdfDiagnosticosAnexo2(){
    datosOK = true;
	borrarRegistrosTabla('listDiagnosticosPDFAnexo2');	
	
	var ids = jQuery("#listDx").getDataIDs();

	if(ids.length > 0){
		tablabody=document.getElementById('listDiagnosticosPDFAnexo2').lastChild;	
		tablabody.width='100%';

			tr=document.createElement("TR");
			tr.className='titulos';					  		  
			th=document.createElement("Td");
			th.colSpan="4";
			th.innerHTML='IMPRESION DIAGNOSTICA';
			tr.appendChild(th);
		 tablabody.appendChild(tr);
		 
			tr=document.createElement("TR");
			  tr.className='letraTamano3';								  		  
			  th=document.createElement("Td");
			  th.width='30px';		
			  th.innerHTML='COD';
			tr.appendChild(th);
	  
			  th=document.createElement("Td");
			  th.width='55%';		
			  th.innerHTML='NOMBRE';			
			tr.appendChild(th);
	  
			  th=document.createElement("Td");
			  th.width='20%';		
			  th.innerHTML='TIPO';								  
			tr.appendChild(th);
			
			  th=document.createElement("Td");
			  th.width='20%';		
			  th.innerHTML='CLASE'
			tr.appendChild(th);
		tablabody.appendChild(tr);	 	
		
	
		for(var i=0;i<ids.length;i++){ 
			var c = ids[i];
			var datosDelRegistro = jQuery("#listDx").getRowData(c);  
			  
                if(i <4){ // para que salgan 4 registros primeros
               // if(datosDelRegistro.id_documento == valorAtributo('lblIdDocumento')){
					tr=document.createElement("TR");
					  tr.className='letraTamano3';					  
					  th=document.createElement("Td");
					  th.align='left';
					  th.innerHTML=$.trim(datosDelRegistro.id_cie);
					tr.appendChild(th);
			  
					  th=document.createElement("Td");
					  th.align='left';			
					  th.innerHTML=$.trim(datosDelRegistro.nombre);			
					tr.appendChild(th);
			  
					  th=document.createElement("Td");
					  th.align='left';			
					  th.innerHTML=$.trim(datosDelRegistro.tipo);								  
					tr.appendChild(th);
					
					  th=document.createElement("Td");
					  th.align='left';			
					  th.innerHTML=$.trim(datosDelRegistro.clase);								  
					tr.appendChild(th);
					
					tablabody.appendChild(tr);	
				}
		}	
  }//if ids
  else{  alert('FALTA DIAGNOSTICOS DE ESTA EVOLUCION \n  ( El listado debe estar visible )');  datosOK = false;  }
  return datosOK;
}


function  limpiarCamposFormulario(dato){ 
	var inputs = document.dato.getElementsByTagName("input");
	for(var i=0;i<inputs.length;i++){
	   inputs[i].value = "";
	}
}


function ajaxModificarDocumento(){
   	varajax=crearAjax();
    varajax.open("POST",'/clinica/paginas/accionesXml/modificarDocumento_xml.jsp',true);
	varajax.onreadystatechange=respuestaModificarDocumento;
	varajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
	varajax.send(valores_a_mandar);
}
function respuestaModificarDocumento(){
	if(varajax.readyState == 4 ){

		   if(varajax.status == 200){
			raiz=varajax.responseXML.documentElement;

				if(raiz.getElementsByTagName('respuesta')[0].firstChild.data=='true'){ 

				   
				}else alert("ATENCION !!! Se desconecto la red, reanude sesión copiando el ultimo cambio ");
		   }else{
			   alert("ATENCION !!! Se desconecto la red, reanude sesión copiando el ultimo cambio ");
		   }
		}
		if(varajax.readyState == 1){
			//abrirVentana(260, 100);
			//VentanaModal.setSombra(true);
		}
}

function guardarContenidoDocumento(){ 	
	
 if(valorAtributo('lblIdDocumento')!=''){ 
 
   if( valorAtributo('txtIdEsdadoDocumento') == 0 || valorAtributo('txtIdEsdadoDocumento') == 2 || valorAtributo('txtIdEsdadoDocumento') == 3 || valorAtributo('txtIdEsdadoDocumento') == 4 || valorAtributo('txtIdEsdadoDocumento') == 5 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 12 ){ 
		arg = valorAtributo('lblTipoDocumento');
		valores_a_mandar='';
		valores_a_mandar=valores_a_mandar+"&TipoDocumento="+arg+"&parametros="; 

		if(arg =='EXOF') cantParametros = 58;
		if(arg =='EVOF') cantParametros = 50;	
		if(arg =='ECOF') cantParametros = 21;	
		if(arg =='REOF') cantParametros = 2;
		
		if(arg =='EXOP') cantParametros = 65; // 64
		if(arg =='EVOP') cantParametros = 65;					
	
		//formularios AYUDAS DX
		if(arg =='ADXI') cantParametros = 9;
		if(arg =='PENT') cantParametros = 9;	
		if(arg =='AOCT') cantParametros = 9;	
		if(arg =='BIOM') cantParametros = 10;	
		if(arg =='PAQU') cantParametros = 9;	
		if(arg =='PABI') cantParametros = 9;			
		if(arg =='ACAM') cantParametros = 9;					
		if(arg =='ECAM') cantParametros = 9;					
		if(arg =='FNYP') cantParametros = 9;					
		if(arg =='FNOP') cantParametros = 9;					
			
		if(arg =='ADAN') cantParametros = 22;	
		if(arg =='ECOG') cantParametros = 13;				
		if(arg =='ADXE') cantParametros = 9;		
			
		//formularios qx
		if(arg =='EPIC') cantParametros = 4;
		if(arg =='NENF') cantParametros = 3;
		if(arg =='LICH') cantParametros = 99;
		if(arg =='LICL') cantParametros = 41;
		if(arg =='HQLA') cantParametros = 8;
		if(arg =='HQUI') cantParametros = 9;

		if(arg =='RPRE') cantParametros = 30;
		if(arg =='NCCI') cantParametros = 8;
		if(arg =='NERE') cantParametros = 8;
		if(arg =='NEJE') cantParametros = 8;
		if(arg =='EVME') cantParametros = 7;						
		
		/*LABORATORIOS*/
		if(arg =='HEMO') cantParametros = 8;	
		if(arg =='TIEM') cantParametros = 8;
		if(arg =='UROC') cantParametros = 18;		
		if(arg =='GLUC') cantParametros = 8;			
		
		if(arg =='EXBV') cantParametros = 65;
     	if(arg =='EXOR') cantParametros = 67;	
		if(arg =='TAOR') cantParametros = 67;		
		if(arg =='EVOR') cantParametros = 67;	
	
		if(arg =='EVBV') cantParametros = 1;
		if(arg =='TEBV') cantParametros = 1;	
		if(arg =='TEOR') cantParametros = 1;	
		
		/*FORMATOS NUEVOS*/
		if(arg =='INTE') cantParametros = 1;
		if(arg =='EVNE') cantParametros = 1;
		if(arg =='TSOC') cantParametros = 2;
		if(arg =='TSOP') cantParametros = 6;
		if(arg =='PSIC') cantParametros = 3;	
		if(arg =='PSIP') cantParametros = 14;			
		if(arg =='NUTR') cantParametros = 7;	
		if(arg =='CEXT') cantParametros = 3;	
		if(arg =='CEXP') cantParametros = 6;	
		if(arg =='CEXC') cantParametros = 6;			
		if(arg =='RSHA') cantParametros = 29;	
		if(arg =='RSHC') cantParametros = 22;	
		if(arg =='ECOO') cantParametros = 5;	
		if(arg =='HCOF') cantParametros = 9;	
		if(arg =='HCOC') cantParametros = 9;
		if(arg =='PSIA') cantParametros = 51;	
		if(arg =='PSIE') cantParametros = 5;	
		if(arg =='LAFV') cantParametros = 17;
		if(arg =='LAQH') cantParametros = 10;	
		if(arg =='LAQS') cantParametros = 9;	
		if(arg =='LAPA') cantParametros = 13;	
		if(arg =='EXDP') cantParametros = 323;	
		if(arg =='EXIC') cantParametros = 4;	
		if(arg =='EXAC') cantParametros = 4;
		if(arg =='EXGC') cantParametros = 4;
		if(arg =='EXDC') cantParametros = 4;
		if(arg =='EOTC') cantParametros = 4;
		if(arg =='EXNC') cantParametros = 4;
		if(arg =='EXTC') cantParametros = 4;
		if(arg =='EXPC') cantParametros = 4;
		if(arg =='EXMC') cantParametros = 4;
		if(arg =='EXSC') cantParametros = 4;
		if(arg =='EXGP') cantParametros = 91;
		
	   for(i=1;i<=cantParametros;i++){	
		  add_valores_a_mandar(valorAtributo('txt_'+arg+'_C'+i));
		  console.log(arg+": "+i);	 
		  //alert(valorAtributo('txt_'+arg+'_C'+i));
	   }
	   add_valores_a_mandar(valorAtributo('lblIdDocumento'));	
	   ajaxModificarDocumento();
   }else alert('EL ESTADO DEL FOLIO NO PERMITE MODIFICAR')	 
	 
	 

 }else alert('SELECCIONE UN DOCUMENTO')
}













/*
function  valorAtributoDocumento(dato){ //saca el prefijo txt y pone el lbl	
	asignaAtributo('lbl_'+dato.substring(4,30), $('#drag'+ventanaActual.num).find('#'+dato).val() )

	return valorAtributo(dato);
}*/

function ocultarTodoTipoDocumentosDelAcordion(){
	ocultar('divAcordionOrdenesMedicas');
    ocultar('divAcordionDiagnosticos');	

	ocultar('OLAB');
	ocultar('ALAB');	
	ocultar('PCVI');
	
	
	ocultar('CDVQPDF');
	ocultar('ENVMPDF');
	ocultar('OLABPDF');
	ocultar('ALABPDF');		
	ocultar('PCVIPDF');			

	
}
function ocultarTodoTipoDocumentosYAcordiones(){ // solo del buscar de principalOrdenes.jsp buscar
    ocultar('divDocumentosHistoricos');ocultar('divOrdenesMedicas');ocultar('divDiagnosticos');ocultar('divProcedimientos');ocultar('divSignosVitales')	

	
	ocultar('OLAB');
	ocultar('ALAB');	
	ocultar('PCVI');
	
	ocultar('OLABPDF');	
	ocultar('ALABPDF');		
	ocultar('PCVIPDF');			
	
}



function mostrarTipoDocumentoSeleccionado(idTipodocumento){ /*por ahora es mas barato hacerlo en javascript sin ajax porque no se necesitaria ir a la DB*/
  ocultarTodoTipoDocumentosDelAcordion(); 
  
  mostrar('div'+idTipodocumento)
  mostrar(idTipodocumento+'PDF') 
   
  verOrdenes = 0, verDiagnosticos = 0, verProcedimientos = 0 ;

  switch(idTipodocumento){ 
     case 'VLIN': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;
	 break;   
     case 'CDVE': 
	   mostrar(idTipodocumento);
	   mostrar(idTipodocumento);	   	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'VLCX': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	
     case 'VLNX': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break; 	  
     case 'VLHS': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'VLIC': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'EPIS': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;		 
     case 'EPFC': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;	 
     case 'REMS': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'RMED': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'RCOD': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;	 
	 case 'REVM': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;
	 break;   
     case 'VCEM': 
	   mostrar(idTipodocumento);
	   mostrar(idTipodocumento);	   	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'VLHM': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'VLHR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'TOVR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'PCVR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;		 
     case 'ENVR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;	 
     case 'TSVR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'TSHS': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'TSVE': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'PCVE': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'VJMP': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;
	 break;   
     case 'PCIP': 
	   mostrar(idTipodocumento);
	   mostrar(idTipodocumento);	   	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'PCIF': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'PCPS': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'PCEG': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'VNTR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;	
     case 'VTRG': 
	   mostrar(idTipodocumento);	
       verOrdenes = 0;
	   verDiagnosticos = 1;
	   verProcedimientos = 0;	   
	 break;		 	 	 
     case 'FTVR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;	 
     case 'PCCR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'TSCR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'PCCE': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;
	 break;   
     case 'TOCR': 
	   mostrar(idTipodocumento);
	   mostrar(idTipodocumento);	   	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'FOVR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'VLRR': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'EPOU': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'VLLG': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;		 
     case 'VLPT': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;	 
     case 'VPPP': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'VLFM': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'VLPJ': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;
	 break;   
     case 'VLUR': 
	   mostrar(idTipodocumento);
	   mostrar(idTipodocumento);	   	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'VIHC': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'VIHN': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 	 
     case 'AZUL': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  
     case 'PCEI': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;  	 
     case 'VIHM': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;		 
     case 'PCEH': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'VMGP': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'TOVI':  
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	  
	   habilitar('txt_TOVI_C4',0); 
	   habilitar('txt_TOVI_C5',0); 
	   habilitar('txt_TOVI_C6',0); 	   	   
	   habilitar('txt_TOVI_C7',0); 
	   habilitar('txt_TOVI_C8',0); 
	   habilitar('txt_TOVI_C9',0); 	   	   
	   habilitar('txt_TOVI_C10',0); 
	   habilitar('txt_TOVI_C11',0); 
	   habilitar('txt_TOVI_C12',0); 	   	   
	   habilitar('txt_TOVI_C13',0);		    
	 break;	 
     case 'CDVQ': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'ENVM': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'OLAB': 
	   mostrar(idTipodocumento);	
       verOrdenes = 0;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 case 'ALAB': 
	   mostrar(idTipodocumento);	
       verOrdenes = 0;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;	 
	 case 'PCVI': 
	   mostrar(idTipodocumento);	
       verOrdenes = 1;
	   verDiagnosticos = 1;
	   verProcedimientos = 1;	   
	 break;
	 
}
	
 if(verOrdenes == 1)
	   mostrar('divAcordionOrdenesMedicas');	
 if(verDiagnosticos == 1)	   
	   mostrar('divAcordionDiagnosticos');	 
 if(verProcedimientos == 1)	   
	   mostrar('divAcordionProcedimientos');		       
	
}




/* 
	FUNCIONES:
	 CARGAR TEXTO: para cagar texto en el forumlario exof y evof de acuerdo a la opcion escogida en un select
	 validarKey: Para cambiar el texto abreviado a un texto completo.
	 copiarDatosAJustificacion: se acciona con el boton traer y lleva el texto a un text area segun el id del elemento
	 cargar descripcion: funcion para hoja quirurgica.
	AUTOR: DANIEL VALLEJO
	ULTIMA ACTUALIZACION: 29-10-2015
	FECHA-CREACION: 6-04-2015
 */

function mostrarEdicionDeAnexo(){
	mostrar('divCamposEdicion');	
}
  
 
function cargarDescripcion (opcion){
	//if(valorAtributoSU('txtMotivoConsulta') != ''){
	   var cadena1='';		
	   var idElemento= 'txt_HQUI_C9'; 
	   switch (opcion){
		    case'RESECCION DE PTERIGION SIMPLE (NASAL O TEMPORAL) CON INJERTO':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA\n3. COLOCACION DE BLEFAROSTATO.\n4. INFILTRACION DE ANESTESICO\n5. RESECCION DE PTERIGION CON TIJERAS.\n6. QUERATECTOMIA SUPERFICIAL.\n7. DISEÑO DE INJERTO EN CONJUNTIVA SUPERIOR. \n8. FIJACION DE INJERTO CON NYLON 9.0.\n9. LAVADO  CON SOLUCION SALINA Y OCLUSION  OCULAR CON GASA ESTERIL.\nBIOPSIA: ';	
			break;
			case 'RESECCION DE PTERIGION REPRODUCIDO (NASAL O TEMPORAL) CON PLASTIA LIBRE O CITOSTATICOS + REPARACION DE SIMLEFARON CON INJERTO LIBRE DE CONJUNTIVA':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA\n3. COLOCACION DE BLEFAROSTATO.\n4. INFILTRACION DE ANESTESICO\n5. RESECCION DE PTERIGION CON TIJERAS.\n6. CORRECCION DE FIBROSIS CON TIJERAS.\n7. QUERATECTOMIA SUPERFICIAL CON BISTURI.\n8. DISEÑO DE PLASTIA  EN CONJUNTIVA SUPERIOR.\n9. REPARACION DE SIMBLEFARON CON INJERTO\n10. FIJACION Y SUTURA DE INJERTO LIBRE Y PLASTIA DE CONJUNTIVA CON NYLON 9.0.\n11. LAVADO CON SOLUCION SALINA Y OCLUSION OCULAR CON GASA ESTERIL.\nBIOPSIA: \nREFERENCIA MEDICAMENTO CITOSTATICO: ';
			break;
			case 'RESECCION DE PTERIGION REPRODUCIDO SIMPLE (NASAL O TEMPORAL) CON PLASTIA LIBRE O CITOSTATICOS':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. COLOCACION DE BLEFAROSTATO.\n4. INFILTRACION DE ANESTESICO.\n5. RESECCION DE PTERIGION CON TIJERAS.\n6. CORRECCION DE SIMBLEFARON CON TIJERAS\n7. QUERATECTOMIA SUPERFICIAL CON BISTURI.\n8. DISEÑO DE PLASTIA EN CONJUNTIVA SUPERIOR.\n9. FIJACION Y SUTURA DE INJERTO LIBRE DE CONJUNTIVA  NYLON 9.0.\n10.LAVADO CON SOLUCION SALINA Y OCLUSION  OCULAR CON GASA ESTERIL.\nBIOPSIA: ';
			break;
			case 'RESECCION DE CHALAZION':
				cadena1='1. ASEPSIA Y ANTISEPSIA.\n2. INFILTRACION DE ANESTESICO SUBCUTANEO EN PARPADO :\n3. RESECCION DE CHALAZION VIA TRANSCONJUNTIVAL CON PINZA DE CHALAZION BISTURI\n4. CURETAJE DE TARSO CON CURETA DE CHALAZION\n5. LAVADO CON SOLUCION SALINA Y OCLUSION  OCULAR CON GASA ESTERIL.\nBIOPSIA: ';
			break;
			case 'RESECCION DE TUMOR BENIGNO O MALIGNO DE PARPADO ESPESOR PARCIAL UN TERCIO':
				cadena1='1. ASEPSIA Y ANTISEPSIA.\n2. INFILTRACION DE ANESTESICO SUBCUTANEO EN PARPADO :\n3. RESECCION TUMOR DE PARPADO CON BISTURI Y TIJERAS .\n4. HEMOSTASIA CON BIPOLAR.\n5. CIERRE DE INCISION POR PLANOS. \n6. LAVADO CON SOLUCION SALINA Y OCLUSION  OCULAR CON GASA ESTERIL.\nBIOPSIA: ';	
			break;
			case 'INSERCION DE TAPONES (PLUGS) DE PUNTOS LAGRIMALES AO No 6':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. DILATACION DE PUNTOS LAGRIMALES  INFERIOR Y SUPERIOR OJO DERECHO\n4. INSERCION DE PLUGS CON DISPENSADOR EN PUNTO LAGRIMAL INFERIOR Y LUEGO EN EL SUPERIROR.\n5. IGUAL PROCEDIMIENTO EN OJO IZQUIERDO\nREF NUMERO :';
			break;			
			case 'EXTRACCION EXTRACAPSULAR DE CRISTALINO POR FACOEMULSIFICACION CON IMPLANTE DE LIO SECUNDARIO':
				cadena1='1. ASEPSIA Y ANTISEPSIA.\n2. COLOCACION DE BLEFAROSTATO.\n3. YODOPOVIDONA.\n4. INSICION CORNEA CLARA 2.7 MM.\n5. 2 PUENTES LATERALES\n6. CAPSULOREXIS\n7. HIDODISECCION\n8. FACOEMULSIFICACION\n9. IRRIGACION-ASPIRACION DE MASA\n10. IMPLANTE DE LENTE INTRAOCULAR\n11. CIERRE HERMETICO\n12. ANTIBIOTICO\n13. PARCHE';
			break;
			case 'CATARATA POR FACO':			
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA, ANTISEPSIA Y COLOCACION DE BLEFAROSTATO.\n3. PERITOMIA.\n4. INCISION TAMAÑO: .\n5. VISCOLASTICO CAMARA ANTERIOR: .\n6. CAPSULOTOMIA: .\n7. EXTRACCION DE NUCLEO FACO: FACOEMULSIFICACION.\n8. IMPLANTE DE LENTE INTRAOCULAR SACO CAPSULAR .\n9. TIPO LENTE: \n10. CIERRE DE INCISION.\n11. MEDICACION: \n12. OTROS PROCEDIMIENTOS: \nSE REVISA BIOMETRIA Y SE ELIGE LENTE';
			break;
			case 'EVISCERACION DEL GLOBO OCULAR CON IMPLANTE SOD-NO INCLUYE PROTESIS':
				cadena1='1. PERITOMIA X 360 GRADOS.\n2. HEMOSTASIA.\n3. CAUTERIZACION DE CORNEA X 360 GRADOS.\n4. QUERATECTOMIA POR 360 GRADOS CON TIJERAS.\n5. DESINSERCION TOTAL DEL IRIS CON ESPATULA.\n6. LEGRADO INTRAOCULAR DE CONTENIDO Y LAVADO CON ALCOHOL ABSOLUTO.\n7. HEMOSTASIA.\n8. IMPLANTE DE PMMA  EN CAVIDAD.\n9. CIERRE DE ESCLERA CON VICRYL 6-0. \n10. CIERRE DE CONJUNTIVA CON VICRYL 7-0.\n11. CEFALOTINA MÁS DEXAMETASONA SUBCONJUNTIVAL.\n12. CONFORMADOR EN FORNIX.\n13. OCLUSOR ESTERIL.';
			break;	
			case 'INSERCION DE IMPLANTE PARA GLAUCOMA SOD':
				  cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. COLOCACION DE BLEFAROSTATO.\n4. PERITOMIA CONJUNTIVAL SUPERIOR.\n5. CONTROL DE HEMOSTASIA CON ELECTROCAUTERIO.\n6. IMPLANTE DE CUERPO DE VALVULA EN CONJUNTIVA SUPERIOR.\n7. DISENO DE TUNEL ESCLERO-CORENAL PARA TUBO.\n8. PASE DE TUBO POR TUNEL HACIA CAMARA ANTERIOR.\n9. SINTESIS DE CONJUNTIVA.\n10. ANTIBIOTICO TOPICO.\n11. OCLUSION OCULAR.';
			break;	
			case 'TERAPIA ANTIANGIOGENICA':
				cadena1='1. OFTALMOSCOPIA.\n2. ASEPSIA Y ANTISEPSIA.\n3. COLOCACION DE BLEFAROSTATO.\n4. ANESTESIA LOCAL.\n5. YODOPOVIDONA.\n6. APLICACION DE MEDICAMENTO AFLIBERCEPT A 3MM DEL LIMBO.\n7. SIN COMPLICACIONES.\n8. OFTALMOSCOPIA.\n9. ANTIBIOTICO.\n10. PARCHE.';
			break;	
			case 'FACO + VITRECTOMIA':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. COLOCACION DE BLEFAROSTATO.\n4. YODOPOVIDONA.\n5. INSICION CORNEA CLARA 2.7 MM.\n6. 2 PUENTES LATERALES.\n7. CAPSULOREXIS.\n8. HIDODISECCION.\n9. FACOMULSIFICACION.\n10. SE REALIZA VITRECTOMIA ANTERIOR. \n11. IRIGACION-ASPIRACION DE MASA.\n12. IMPLANTE DE LENTE INTRAOCULAR.\n13. CIERRE HERMETICO.\n14. ANTIBIOTICO.\n15. PARCHE.\nSE REVISA BIOMETRIA Y SE ELIGE LENTE ';
			break;
			case 'VITRECTOMIA VIA POSTERIOR CON ENDOLASER.':
				cadena1='1. ASEPSIA Y ANTISEPSIA.\n2. COLOCACION DE TROCARETS 23G  #3.\n3. VITRECTOMIA ANTERIOR.\n4. VITRECTOMIA POSTERIOR EXTENSA ENCONTRANDOSE HEMORRAGIA VITREA DENSA , RETINA APLICADA CON OCLUSION SEVERA DE VASOS Y ARCADAS TEMPORALES.\n5. ENDOLASER.\n6. PARACENTESIS.\n7. COLOCACION TRIAMCINOLOA EN CAMARA ANTERIOR.\n8. LAVADO DE CAMARA ANTERIOR.\n9. RETIRO DE TROCARETS.\n10.NO COMPLICACIONES.';
			break;
			case 'VITRECTOMIA VIA POSTERIOR CON INSERCION DE SILICON O GASES (136)':
				cadena1='1. ASEPSIA Y ANTISEPSIA.\n2. COLOCACION DE TROCARETS 23G #3. \n3. VITRECTOMIA ANTERIOR EXTENSA.\n4. VITRECTOMIA POSTERIOR EXTENSA ENCONTRANDOSE DESPRENDIMEINTO DE RETINA CON DESGARRO M2 Y COMPROMISO MACULAR.\n5. LIQUIDOS PESADOS, COLOCACION DE TRIAMCIOLONA, VITRECTOMIA EXTENSA.\n6. ENDOLASER.\n7. INTERCAMBIO LIQUIDO AIRE.\n8. INTERCAMBIO AIRE GAS C3F8. \n9. RETIRO DE TROCARETS. \n10. ANTIBIOTICOS SUBCONJUNTIVALES.\n11. NO COMPLICACIONES.';
			break;
			case 'VITRECTOMIA VIA POSTERIOR + FACOEMULSIFICACION DE CATARATA  SOD':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. COLOCACION DE TROCARETS 23G #1.\n4. INSICION EN CORNEA CLARA, PARASENTESIS, VISCOELASTICO.\n5. CAPSULOREXIS CIRCULAR CONTINUA, HIDRODISECCION.\n6. FACOEMULSIFICCACION DE CATARATA.\n7. SUTURA CORNEAL NYLON 10.0.\n8. COLOCACION DE TROCARETS 23G #2.\n9. VITRECTOMIA ANTERIOR EXTENSA.\n10. VITRECTOMIA POSTERIOR EXTENSA ENCONTRANDOSE DESPRENDIMEINTO DE RETINA TOTAL CON AGUJERO MACULAR EXTENSO CON BORDEW INVERTIDOS Y CAMBIOS HIPERPIGMENTADOS, ADEMAS CON DESGARRO TEMPORAL INFERIOR CON PVR C2 5.  LIQUIDOS PESADOS, COLOCACION DE TRIAMCIOLONA, VITRECTOMIA EXTENSA.\n11. INTERCAMBIO LIQUIDO AIRE.\n12. ENDOLASER.\n13. PERITOMIA LOCALIZADA, ESCLEROTOMIA.\n14. INTERCAMBIO AIRE SILICON 5000 CTS.\n15. SUTURA DE ESCLERA Y CONJUNTIVA CON VICRYL 7.0.\n16. RETIRO DE TROCARETS.\n17. ANTIBIOTICOS SUBCONJUNTIVALES.\n18. NO COMPLICACIONES.\nSE REVISA BIOMETRIA Y SE ELIGE LENTE';
			break;
			case 'VITRECTOMIA VIA POSTERIOR + PELAMIENTO DE MEMBRANAS +  INSERCION DE  GASES (136)':
				cadena1='1. ASEPSIA Y ANTISEPSIA.\n2. COLOCACION DE TROCARETS 23G #3. \n3. VITRECTOMIA ANTERIOR EXTENSA.\n4. VITRECTOMIA POSTERIOR EXTENSA ENCONTRANDOSE MEMBRANAN EPIRETINIANA CON TRACCION MACULAR.\n5. HIALOIDECTOMIA + TINCION CON AZUL BRILLANTE.\n6. PELAMIENTO DE MEMBRANA + DOBLE TINCION CON AZUL BRILLANTE.\n7. PELAMIENTO DE MEMBRANA LIMITANTE INTERNA CON EXITO.\n8. ENDOLASER.\n9. INTERCAMBIO LIQUIDO AIRE.\n10. INTERCAMBIO AIRE GAS.\n11. RETIRO DE TROCARETS.\n12. ANTIBIOTICOS SUBCONJUNTIVALES.\n13. NO COMPLICACIONES.';
			break;	
			case 'RESECCION DE QUISTE O TUMOR BENIGNO DE CONJUNTIVA':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. COLOCACION DE BLEFAROSTATO.\n4. INFILTRACION DE ANESTESICO.\n5. RESECCION COMPLETA DE QUISTE CONJUNTIVAL CON TIJERAS.\n6. HEMOSTASIA CON BIPOLAR.\n7. CIERRE DE CONJUNTIVA CON NYLON 9-O.\n8. LAVADO CON SOLUCION SALINA Y OCLUSION OCULAR CON GASA ESTERIL.\nBIOPSIA:';
			break;	
			case 'PLASTIA DE CANALICULOS LAGRIMALES':			
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ANESTESIA SUBCUTANEA EN CANTO MEDIO OJO DERECHO.\n3. PLASTIA DE CANALICULO Y PUNTO LAGRIMAL SUPERIOR E INFERIOR CON TIJERAS DE VANNAS\n4. HEMOSTASIA EN PUNTO LAGRIMAL SUPERIOR E INFERIOR CON BIPOLAR\n5. LAVADO CON SOLUCION SALINA.\n6. ANESTESIA SUBCUTANEA EN CANTO MEDIO IZQUIERDO\n7. IGUAL PROCEDIMIENTO EN OJO IZQUIERDO.';
			break;	
			case 'IMPLANTE DE ANILLOS INTRAESTROMALES':			
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. DEMARCACION DEL EJE VISUAL Y SURCO DE INCISION CON MARCADOR DE ANILLOS.\n4. INCISION A:   Y GRADOS:    MICRAS.\nINCISION A:   Y GRADOS:    MICRAS.\n5. DELAMINACION INICIAL DE SURCOS CON ESPATULA DE SUAREZ\nTEMPORAL    NASAL.\n6. DELAMINACION INICIAL DE SURCOS CON ESPATULA CURVA.\n7. INSERCION DE ANILLOS INTRAESTROMALES: .\n8. COLOCACION ANTIBIOTICO Y LENTE DE CONTACTO TERAPEUTICO .\n9. CONTROL EN: ';
			break;
			
			case 'FACO + TRABECULECTOMIA':			
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. PERITOMIA FORNIX .\n3.HEMOSTASIA .\n4. FLAP DE 3.2 X 2.7MM .\n5. MITOMICINA C 0.5 % X 3 MIN.\n6. LAVADO.\n7.DISECCION DE FLAP.\n8.2 PUERTASLATERALES .\n9.REXIS. \n10.HIDRODISECCION.\n11.FACO. \n12. IMPLANTE DE LIO. \n13.TRABECULECTOMIA PUNCH KELLY. \n14.IRIDECTOMIA. \n15.CIERRE FLAP 3 PUNTOS DE NYLON 10/0.\n16. CONJUNTIVA CAUTERIO BIPOLAR\n17.CIERRE HERMETICO. \n18.PARCHE.\nSE REVISA BIOMETRIA Y SE ELIGE LENTE';
			break;
			case 'TRABECULECTOMIA':			
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. PERITOMIA FORNIX .\n3. HEMOSTASIA .\n4. DISECCION DEL FLAP 2.7 *3.5 MM .\n5. MITOMICINA .05% * 3 MIN .\n6. LAVADO .\n7. ENTRADA A CAMARA ANTERIOR .\n8. PUERTO LATERAL .\n9. TRABECULECTOMIA PUNCH DE KELLY( 3 MORDIDDAS ) .\n10. IRIDECTOMIA .\n11. CIERRE DE FLAP .\n12. NYLON 10/0 3 PUNTOS .\n13.  CONJUNTIVA CAUTERIO BIPOLAR .\n14.  ANTIBIOTICO .\n15.  PARCHE.';
			break;
			case 'INYECCION DE MATERIAL MIORELAJANTE (TOXINA BOTULINICA)':			
				cadena1='1. ASEPSIA, ANTISEPSIA AMBOS OJOS.\n2. DILUCION DE LIOFILIZADO DE 100 UI DE TOXINA  BOTULINICA EN 5 ML DE  SOLUCION SALINA BALANCEADA.\n3. APLICACION DE DISOLUCION EN TRAYECTO DE FIBRAS DEL NERVIO FACIAL EN AREAS PERIOCULARES Y FRONTALES DE AMBOS OJOS \nCOMPLICACIONES: NO.';
			break;
			case 'BLEFAROPLASTIA SUPERIOR AO':
				cadena1='1. ASEPSIA Y ANTISEPSIA DE PARPADOS SUPERIORES AMBOS OJOS.\n2. MARCACION CON VIOLETINA DE LINEA INFERIOR DEL  PLIEGUE PALPEBRAL  SUPERIOR OJO DERECHO.\n3. MARCACION DE LINEA SUPERIOR  DEL PLIEGUE CON REDUNDANCIA DE PIEL.\n4. RESECCION DE PIEL REDUNDANTE CON BISTURI\n5. HEMOSTASIA BIPOLAR.\n6. CIERRE DEL PLANO CUTANEO CON PROLENE 6-0\n7. IGUAL PROCEDIMIENTO EN PARPADO SUPERIOR OJO IZQUIERDO.\nNO COMPLICACONES\nSE CUBRE CON APOSITO ESTERIL.';
			break;
			case 'RESECCION DE PTERIGION SIMPLE (NASAL O TEMPORAL) CON INJERTO + REPARACION DE SIMBLEFARON CON INJERTO LIBRE DE CONJUNTIVA':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA .\n3. COLOCACION DE BLEFAROSTATO .\n4. INFILTRACION DE ANESTESICO .\n5. RESECCION DE PTERIGION CON TIJERAS .\n6. CORRECCION DE FIBROSIS CON TIJERAS .\n7. QUERATECTOMIA SUPERFICIAL CON BISTURI .\n8. DISENO DE INJERTO EN CONJUNTIVA SUPERIOR .\n9. REPARACION DE SIMBLEFARON CON INJERTO .\n10. FIJACION Y SUTURA DE INJERTO LIBRE  DE CONJUNTIVA CON NYLON 9.0   .\n11. LAVADO CON SOLUCION SALINA Y OCLUSION OCULAR CON GASA ESTERIL .\nBIOPSIA: NO .\nREFERENCIA MEDICAMENTO CITOSTATICO:';
			break;
			case 'INSERCION DE LENTE INTRAOCULAR FAQUICO':
				cadena1='1. APLICACION DE PROFILAXIS YODOPOVIDONA AL 5% OFTALMICA.\n2. ASEPSIA Y ANTISEPSIA.\n3. COLOCACION DE BLEFAROSTATO.\n4. YODOPOVIDONA.\n5. PERITOMIA SUPERIOR.\n6. SURUCO CORNEO ESCLERAL.\n7. CONTRAPUNSIONES 10-12 HORARIA \n8. VISOELASTICO PESADO INTRACAMERAL\n9. INSICION DE 5.5 A 6 MM .\n10. IMPLANTE DE LENTE FAQUIQUICO TIPO ARTISAN.\n11. ENCLAVAMIENTO NASAL- TEMPORAL \n12. IRIDECTOMIA PERIFERICA\n13. SUTURA CON NYLON 10/0\n14. LAVADO DE CAMARA ANTERIOR \n15. ANTIBIOTICO SUBCONJUNTIVAL CEFALEXINA Y DEXAMETAZONA. \n16. PARCHE\nSE REVISA BIOMETRIA Y SE ELIGE LENTE ';
			break;
			
			default:
				cadena1='';
			break;
	   }
	   asignaAtributo(idElemento,  cadena1  ,0);	     
	//}
}

function cargarDescripcionLaboratorio(opcion){
	//if(valorAtributoSU('txtMotivoConsulta') != ''){
	   var cadena1='';		
	   var idElemento= 'txt_HEMO_C6'; 
	   switch (opcion){
			case 'Impedancia/Histogramas':
				cadena1='WMC  :       7.9       x10^3      3.5-10.5          LY%      26.0    %   20-50     \n'+
						'MXD% :        2.9         %            0 - 8               NE%        71.1     %     40-75\n'+     
						'LY#  :           2.1         %            0 - 8              NE%         71.1     %     40-75\n'+     
						'NE#  :           5.6         x10^3     3.5-10.5          LY%         26.0    %     20-50\n'+     
						'HGB#  :        14.8        G/DL       11.5-16           HCT%      43.1     %     35-48\n'+     
						'MCV#  :        82.1        G/DL       11.5-16           HCT%      43.1     %     35-48\n'+     
						'MHCV#  :      82.1        G/DL       11.5-16           HCT%      43.1     %     35-48\n'+     
						'RDW-SD#     42.6         G/DL      11.5-16            HCT%      43.1     %     35-48\n'+     
						'MPV#  :        8.6          G/DL       11.5-16           HCT%       43.1    %     35-48\n'+     
						'PCT#  :        0.20         %           11.5-16          HCT%       43.1     %     35-48\n\n'+
						'DIFERENCIA MANUAL:\n'+						
						'NEUTROFILOS:\n'+
						'LINFOCITOS:\n'+
						'EOSINOFILOS:\n'+						
						'MONOCITOS:'					
			break;			

			default:
				cadena1='';
			break;
	   }
	   asignaAtributo(idElemento,  cadena1  ,0);	     
	//}
}



function cargarTexto(valor, l)
	{
		var opcion = valor;
		var lugar =l;
		var texto ='';
		var cadenaText='';
		var textoDef='';
		
		var j = 0;
		pat= /AMBOS OJOS/
		
		if(valorAtributo('lblTipoDocumento')=='EXOF'){	
			var idElemento ='txt_EXOF_C';
		}
		if (valorAtributo('lblTipoDocumento')=='EVOF'){
			var idElemento ='txt_EVOF_C';
		}				
		
		if(lugar=='S'){ //SEGMENTO ANTERIOR
			switch(opcion)
			{
				case '1':
					texto ='OJO DERECHO ';
					for(i=29;i<=34;i++){
						cadenaText = ''+valorAtributoSU(idElemento+i); 	
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[0]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}
						j=j+1;							
					}
					j=0;
				break;	
				case '2':
					texto ='OJO DERECHO NO VALORABLE ';
					for(i=29;i<=34;i++){
						//asignaAtributo(idElemento+i,  texto  ,0);	
						cadenaText = ''+valorAtributoSU(idElemento+i); 
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[0]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}
					}
				break;
				case '3':
					texto =' OJO IZQUIERDO ';
					for(i=29;i<=34;i++){
						cadenaText = ''+valorAtributoSU(idElemento+i); 												
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[1]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}		
					}
				break;
				case '4':
					texto =' OJO IZQUIERDO NO VALORABLE';
					for(i=29;i<=34;i++){
						cadenaText = ''+valorAtributoSU(idElemento+i); 												
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[1]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}		
					}
				break;
				case '5':
					for(i=29;i<=34;i++){
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						asignaAtributo(idElemento+i,  texto  ,0);	
					}
				break;
				case '6':
					for(i=29;i<=34;i++){
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						switch(i){		
							case 30:
								texto='OJO DERECHO CLARA, OJO IZQUIERDO CLARA';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
							case 32:
								texto='OJO DERECHO FORMADA, OJO IZQUIERDO FORMADA';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
							case 34:
								texto='OJO DERECHO TRANSPARENTE, OJO IZQUIERDO TRANSPARENTE';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
							default:
								texto='OJO DERECHO NORMAL, OJO IZQUIERDO NORMAL';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
						}
					}
				break;
				case '7':
					texto ='OJO DERECHO NO VALORABLE, OJO IZQUIERDO NO VALORABLE';
					for(i=29;i<=34;i++){
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						asignaAtributo(idElemento+i,  texto  ,0);	
					}
				break;
				default: //racuperar valores guardados									
					for(i=29;i<=34;i++){
							//alert(valorGuardado[i] );
							asignaAtributo(idElemento+i,  valorGuardado[i]  ,0);
					 }			  
				break;
			}
		}//FIN SI
		if(lugar=='F'){ //FONDO DE OJO
			switch(opcion)
			{
				case '1':
					texto ='OJO DERECHO ';
					for(i=40;i<=44;i++){
						cadenaText = ''+valorAtributoSU(idElemento+i); 												
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[0]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}		
					}
				break;	
				case '2':
					texto ='OJO DERECHO NO VALORABLE ';
					for(i=40;i<=44;i++){
						cadenaText = ''+valorAtributoSU(idElemento+i); 												
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[0]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}		
					}
				break;
				case '3':
					texto =' OJO IZQUIERDO ';
					for(i=40;i<=44;i++){
						cadenaText = ''+valorAtributoSU(idElemento+i); 												
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[1]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}	
					}
				break;
				case '4':
					texto =' OJO IZQUIERDO NO VALORABLE';
					for(i=40;i<=44;i++){
						cadenaText = ''+valorAtributoSU(idElemento+i); 												
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						if(pat.test(cadenaText)){
							asignaAtributo(idElemento+i,  cadenaText  ,0);
						}
						
						else{
							var result = cadenaText.split(",");										
							result[1]=texto;
							textoDef=result[0]+' ,'+result[1];							
							asignaAtributo(idElemento+i,  textoDef  ,0);
						}	
					}
				break;
				case '5':
					for(i=40;i<=44;i++){
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						asignaAtributo(idElemento+i,  texto  ,0);	
					}
				break;
				case '6':
					for(i=40;i<=44;i++){
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						switch(i){		
							case 40:
								texto='OJO DERECHO CLARO, OJO IZQUIERDO CLARO';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
							case 41:
								texto='OJO DERECHO EXCAVACION , OJO IZQUIERDO EXCAVACION ';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
							case 44:
								texto='OJO DERECHO APLICADA, OJO IZQUIERDO APLICADA';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
							default:
								texto='OJO DERECHO NORMAL, OJO IZQUIERDO NORMAL';
								asignaAtributo(idElemento+i,  texto  ,0);	
							break;
						}
					}
				break;	
				case '7':
					texto ='OJO DERECHO NO VALORABLE, OJO IZQUIERDO NO VALORABLE';
					for(i=40;i<=44;i++){
						valorGuardado[i] =document.getElementById(idElemento+i).value;
						asignaAtributo(idElemento+i,  texto  ,0);	
					}
				break;
				default: //racuperar valores guardados									
					for(i=40;i<=44;i++){
							//alert(valorGuardado[i] );
							asignaAtributo(idElemento+i,  valorGuardado[i]  ,0);
					 }			  
				break;
			}
		}//FIN SI
			
	}//FIN FUNCION CARGAR TEXTO
 function v28(texto, elem) {
	texto = texto.toUpperCase(); 	
	
	//texto=texto.replace(/(À|Á|Â|Ã|Ä|Å|Æ)/gi,'A'); 
	//texto=texto.replace(/(È|É|Ê|Ë)/gi,'E'); 
	//texto=texto.replace(/(Ì|Í|Î|Ï)/gi,'I');
	//texto=texto.replace(/Ò|Ó|Ô|Ö/gi,'O');
	//texto=texto.replace(/(Ù|Ú|Û|Ü)/gi,'U');
	texto = texto.replace(/Á/gi,"A");
	texto = texto.replace(/É/gi,"E");
	texto = texto.replace(/Í/gi,"I");
	texto = texto.replace(/Ó/gi,"O");
	texto = texto.replace(/Ú/gi,"U");
	texto=texto.replace(/(Ñ)/gi,'N');  
	texto = texto.replace(/°/gi,"#");
	
	document.getElementById(elem).value = texto; 
	longitud = texto.length; 
	ptlin = new Array(); 
	for (i=0; i<longitud; i++) {
		ptlin[i]=texto.charAt(i); 
		 cod_tl=texto.charCodeAt(i); 		 
		 //if ((cod_tl < 48 || cod_tl > 57) && (cod_tl < 65 || cod_tl > 90) && (cod_tl < 96 ||  cod_tl > 122) && (cod_tl != 13) && (cod_tl != 44)&& (cod_tl != 45) && (cod_tl != 46) && (cod_tl != 47) && (cod_tl != 8)  && (cod_tl != 16) && (cod_tl != 32)) 
		//	{
				if((cod_tl == 165) || cod_tl == 209){
					ptlin[i]='N'; 
				} /*else{
					ptlin[i]=''; 
				}
			} 		*/
	}	
	var textof=''; 
	for (i=0;i<longitud;i++) {
		textof=textof+ptlin[i]; 
	}	
	document.getElementById(elem).value=textof; 
}
 
/* FUNCION TRADUCIR ABREVIACIONES AL SALIR NO FUNCIONA*/
function traducirAbr(idElemento){
	texto = valorAtributo(idElemento);
	resultado = texto.replace('OD','OJO DERECHO');
	asignaAtributo(idElemento,resultado,0)
}
 
/* variable de diccionario */ 
var pal_diccionario ='';

function busAbreviacion(palabra){
	
	ban = false;
	bn = false;
	//alert('pr RECIBIDA:'+palabra)	
	//alert('DICCIONARIO:'+diccionario)	
	if(palabra === 'EXOTROPIA.' ){
		bn= true;
	}
	if(palabra === 'ENDOTROPIA.' ){
		bn= true;
	}
		
		
	if ( !bn ){
		
		var dicc = diccionario.split('_-_');		
		pos = dicc.indexOf(palabra);
		
		//alert('pos ABREB:'+pos);
		
		if (dicc.indexOf(palabra)>=0){
			po = (parseInt(pos) + parseInt(1));
			//alert('pos PALABRE ENC: :'+po);
			//alert('PALABRE ENC: :'+dicc[po]);
			pal_diccionario = dicc[po];
			//alert('Pal enc en array. '+pal_diccionario );
			ban = true;
		}else{
			pal_diccionario=' ';
		}
		//alert(pal_diccionario);
	}
	return ban;
}



	var suggests = ["", "","",""];
//	var suggests = ["HELLO", "WORLD","","MYSQL"];	
function sugerencia(idElem){
	//alert(	$('#drag'+ventanaActual.num).find('#'+idElem).val()  );
	
	$('#drag'+ventanaActual.num).find('#'+idElem).asuggest(suggests);
}
 
function validarKey(evt,idElemento) {	
	var ob = document.getElementById(idElemento);
	var cursor = -1;
	var separadores = [' ','\n'];	
	cursor = ob.selectionStart;
	
	var code = (evt.which) ? evt.which : evt.keyCode;
	var cadenaJustifica='';
	var texto=' ';	
	cadenaJustifica = ''+valorAtributoSU(idElemento); 
	//alert('Cadena '+cadenaJustifica.length);
	//alert('Cadena '+cadenaJustifica);
	
	
	
	if (cursor >= cadenaJustifica.length){
	
		var cadena3 = 'BIOMICROSCOPIA: OJO DERECHO: CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO: TRANSPARENTE;\n\nOJO IZQUIERDO: CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO:TRANSPARENTE;\n\nFONDO DE OJO: OJO DERECHO: PAPILA: EXCAVACION: , MACULA: NORMAL, RETINA: APLICADA;\n\nOJO IZQUIERDO: PAPILA: EXCAVACION: , MACULA: NORMAL, RETINA: APLICADA;';
		var cadena4 = 'BIOMICROSCOPIA: OJO DERECHO: CONJUNTIVA NORMAL, CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO: TRANSPARENTE;\n\nOJO IZQUIERDO: CONJUNTIVA NORMAL, CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO:TRANSPARENTE;\n\nFONDO DE OJO: OJO DERECHO: VITREO: CLARO, PAPILA: EXCAVACION: , MACULA: NORMAL, RETINA: APLICADA;\n\nOJO IZQUIERDO:  VITREO: CLARO, PAPILA: EXCAVACION: , MACULA: NORMAL, RETINA: APLICADA;';
		var cadena5 = 'BIOMICROSCOPIA: OJO DERECHO: CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO: TRASPARENTE ,\n\nOJO IZQUIERDO:CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO:TRASPARENTE	\n\nFONDO DE OJO: OJO DERECHO: VITREO CLARO, EXCAVACION: 2/10, MACULA : NORMAL, RETINA APLICADA  \n\nOJO IZQUIERDO: VITREO CLARO, EXCAVACION: 2/10, MACULA : NORMAL, RETINA APLICADA';
		var cadena6 = 'BIOMICROSCOPIA: OJO DERECHO: CORNEA CLARA, CAMARA ANTERIOR FORMADA, CRISTALINO TRASPARENTE.\nOJO IZQUIERDO: CORNEA CLARA, CAMARA ANTERIOR FORMADA, CRISTALINO TRASPARENTE.\n\nFONDO DE OJO:  OJO DERECHO:  PAPILA EXCAVACION:   , VITREO CLARO, MACULA NORMAL, RETINA APLICADA.\nOJO IZQUIERDO: PAPILA EXCAVACION:  , VITREO CLARO, MACULA NORMAL ,RETINA APLICADA.';
		var cadena7 = 'BIOMICROSCOPIA: OJO DERECHO: CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO: TRASPARENTE ,\n\nOJO IZQUIERDO:CORNEA CLARA , CAMARA ANTERIOR FORMADA, CRISTALINO:TRASPARENTE	\n\nFONDO DE OJO: OJO DERECHO: RETINA APLICADA, EXCAVACION: , MACULA : NORMAL \n\nOJO IZQUIERDO:  RETINA APLICADA, EXCAVACION: , MACULA : NORMAL';
		var bandera=false;
		
		var i=0;
		if (code == 13){
			//asignaAtributo (idElemento,valorAtributoSU(idElemento)+' \n . \ ',0)
		}
        if(code==32 )  
        {            
			
			var result = cadenaJustifica.split(" ");			
			//var result = cadenaJustifica.split(new RegExp(separadores.join('|'),'g'));			
			
			for ( i=0; i<=result.length-1;i++ ){							
				//alert(result[i]);
			
				if (busAbreviacion(result[i]) ){
					bandera=true;
					if (pal_diccionario === undefined){
						pal_diccionario = '';
					}
					result[i]=pal_diccionario;

				}
				
						
				if(result[i]=='OSCARA'){
					bandera=true;
					result[i]=cadena4;
				}
				if(result[i]=='LORENAE'){
					bandera=true;
					result[i]=cadena4;
				}
				if(result[i]=='ANITA'){
					bandera=true;
					result[i]=cadena5;
				}
				if(result[i]=='JENNIFER'){
					bandera=true;
					result[i]=cadena6;
				}
				
				if (result[i]=== undefined ){
					result[i]= '';
				}
				
				texto=texto+' '+result[i];
			}//fin for	
						
			if(bandera){
				asignaAtributo(idElemento,  texto  ,0);							
			}			
        }//fin si      
		 /**/
    }	
}
/**/

function copiarDatosAJustificacion(idElemento){ 
  var cadenaJustifica ='';
  var cadena1 = 'OJO DERECHO NORMAL, OJO IZQUIERDO NORMAL';
  var cadena2 = 'OJO DERECHO CLARA, OJO IZQUIERDO CLARA';
  var cadena3 = 'OJO DERECHO FORMADA, OJO IZQUIERDO FORMADA';
  var cadena4 = 'OJO DERECHO TRANSPARENTE, OJO IZQUIERDO TRANSPARENTE';
  var cadena5 = 'OJO DERECHO CLARO, OJO IZQUIERDO CLARO';
  var cadena6 = 'OJO DERECHO EXCAVACION, OJO IZQUIERDO EXCAVACION';
  var cadena7 = 'OJO DERECHO APLICADA, OJO IZQUIERDO APLICADA';
  
  if(valorAtributo('lblTipoDocumento')=='EXOF' || valorAtributo('lblTipoDocumento')=='EVOF' || valorAtributo('lblTipoDocumento')=='EVME'){	

	if(valorAtributoSU('txtMotivoConsulta') != ''){

	   cadenaJustifica = ''+valorAtributoSU('txtMotivoConsulta')+'.\n'; 
	        
	}
	if(valorAtributoSU('txtEnfermedadActual') != ''){

	   cadenaJustifica = cadenaJustifica + ''+valorAtributoSU('txtEnfermedadActual')+'.\n'; 
	        
	}
  }//FIN SI
if(valorAtributo('lblTipoDocumento')=='EXOF'){		
	
	if(valorAtributoSU('txt_EXOF_C26') != cadena1){
	   cadenaJustifica = cadenaJustifica+'PUPILAS: '+valorAtributoSU('txt_EXOF_C26')+'.\n'; 	   	        
	}
	if(valorAtributoSU('txt_EXOF_C27') != cadena1){
	   cadenaJustifica = cadenaJustifica+'PARPADOS: '+valorAtributoSU('txt_EXOF_C27')+'.\n';	        
	}
	if(valorAtributoSU('txt_EXOF_C28') != cadena1){
		cadenaJustifica = cadenaJustifica+'APARATO LAGRIMAL: '+valorAtributoSU('txt_EXOF_C28')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C29') != cadena1){
		cadenaJustifica = cadenaJustifica+'CONJUNTIVA: '+valorAtributoSU('txt_EXOF_C29')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C30') != cadena2){
		cadenaJustifica = cadenaJustifica+'CORNEA: '+valorAtributoSU('txt_EXOF_C30')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C31') != cadena1){
		cadenaJustifica = cadenaJustifica+'ESCLEROTICA: '+valorAtributoSU('txt_EXOF_C31')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C32') != cadena3){
		cadenaJustifica = cadenaJustifica+'CAMARA ANTERIOR: '+valorAtributoSU('txt_EXOF_C32')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C33') != cadena1){
		cadenaJustifica = cadenaJustifica+'IRIS: '+valorAtributoSU('txt_EXOF_C33')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C34') != cadena4){
		cadenaJustifica = cadenaJustifica+'CRISTALINO: '+valorAtributoSU('txt_EXOF_C34')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C35') != cadena1){
		cadenaJustifica = cadenaJustifica+'GONIOSCOPIA: '+valorAtributoSU('txt_EXOF_C35')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C40') != cadena5){
		cadenaJustifica = cadenaJustifica+'VITREO: '+valorAtributoSU('txt_EXOF_C40')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C41') != cadena6){
		cadenaJustifica = cadenaJustifica+'PAPILA (NERVIO OPTICO): '+valorAtributoSU('txt_EXOF_C41')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C42') != cadena1){
		cadenaJustifica = cadenaJustifica+'MACULA: '+valorAtributoSU('txt_EXOF_C42')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C43') != cadena1){
		cadenaJustifica = cadenaJustifica+'VASOS: '+valorAtributoSU('txt_EXOF_C43')+'.\n'; 
	}
	if(valorAtributoSU('txt_EXOF_C44') != cadena7){
		cadenaJustifica = cadenaJustifica+'RETINA: '+valorAtributoSU('txt_EXOF_C44')+'.\n'; 
	}
  }
  if(valorAtributo('lblTipoDocumento')=='ECOF'){
	if(valorAtributoSU('txt_ECOF_C14') != ''){
	   cadenaJustifica = cadenaJustifica+'SUBJETIVO: '+valorAtributoSU('txt_ECOF_C14')+'.\n'; 	   	        
	}
	if(valorAtributoSU('txt_ECOF_C15') != ''){
	   cadenaJustifica = cadenaJustifica+'OBJETIVO: '+valorAtributoSU('txt_ECOF_C15')+'.\n'; 	   	        
	}
  }
  
  if(valorAtributo('lblTipoDocumento')=='REOF'){
	if(valorAtributoSU('txt_REOF_C1') != ''){
	   cadenaJustifica = cadenaJustifica+'SUBJETIVO: '+valorAtributoSU('txt_REOF_C1')+'.\n'; 	   	        
	}
	if(valorAtributoSU('txt_REOF_C2') != ''){
	   cadenaJustifica = cadenaJustifica+'OBJETIVO: '+valorAtributoSU('txt_REOF_C2')+'.\n'; 	   	        
	}
  }
  
  if(valorAtributo('lblTipoDocumento')=='EVME'){
  	if(valorAtributoSU('txt_EVME_C1') != ''){
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_EVME_C1');		
	}
  }
  else if(valorAtributo('lblTipoDocumento')=='EVOF'){			
	if(valorAtributoSU('txt_EVOF_C26') != cadena1){
	   cadenaJustifica = cadenaJustifica+'PUPILAS: '+valorAtributoSU('txt_EVOF_C26')+'.\n'; 	   	        
	}
	if(valorAtributoSU('txt_EVOF_C27') != cadena1){
	   cadenaJustifica = cadenaJustifica+'PARPADOS: '+valorAtributoSU('txt_EVOF_C27')+'.\n';	        
	}
	if(valorAtributoSU('txt_EVOF_C28') != cadena1){
		cadenaJustifica = cadenaJustifica+'APARATO LAGRIMAL: '+valorAtributoSU('txt_EVOF_C28')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C29') != cadena1){
		cadenaJustifica = cadenaJustifica+'CONJUNTIVA: '+valorAtributoSU('txt_EVOF_C29')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C30') != cadena2){
		cadenaJustifica = cadenaJustifica+'CORNEA: '+valorAtributoSU('txt_EVOF_C30')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C31') != cadena1){
		cadenaJustifica = cadenaJustifica+'ESCLEROTICA: '+valorAtributoSU('txt_EVOF_C31')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C32') != cadena3){
		cadenaJustifica = cadenaJustifica+'CAMARA ANTERIOR: '+valorAtributoSU('txt_EVOF_C32')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C33') != cadena1){
		cadenaJustifica = cadenaJustifica+'IRIS: '+valorAtributoSU('txt_EVOF_C33')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C34') != cadena4){
		cadenaJustifica = cadenaJustifica+'CRISTALINO: '+valorAtributoSU('txt_EVOF_C34')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C35') != cadena1){
		cadenaJustifica = cadenaJustifica+'GONIOSCOPIA: '+valorAtributoSU('txt_EVOF_C35')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C40') != cadena5){
		cadenaJustifica = cadenaJustifica+'VITREO: '+valorAtributoSU('txt_EVOF_C40')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C41') != cadena6){
		cadenaJustifica = cadenaJustifica+'PAPILA (NERVIO OPTICO): '+valorAtributoSU('txt_EVOF_C41')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C42') != cadena1){
		cadenaJustifica = cadenaJustifica+'MACULA: '+valorAtributoSU('txt_EVOF_C42')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C43') != cadena1){
		cadenaJustifica = cadenaJustifica+'VASOS: '+valorAtributoSU('txt_EVOF_C43')+'.\n'; 
	}
	if(valorAtributoSU('txt_EVOF_C44') != cadena7){
		cadenaJustifica = cadenaJustifica+'RETINA: '+valorAtributoSU('txt_EVOF_C44')+'.\n'; 
	}
  }  
	
  if(valorAtributo('lblTipoDocumento')=='EXOP' || valorAtributo('lblTipoDocumento')=='EVOP' || valorAtributo('lblTipoDocumento')=='EXBV'){	
  	if(valorAtributoSU('txtMotivoConsulta') != ''){

	   cadenaJustifica = ''+valorAtributoSU('txtMotivoConsulta')+'.\n'; 
	        
	}
	if(valorAtributoSU('txtEnfermedadActual') != ''){

	   cadenaJustifica = cadenaJustifica + ''+valorAtributoSU('txtEnfermedadActual')+'.\n'; 
	        
	}
  }
  if(valorAtributo('lblTipoDocumento')=='HQLA'){  	
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_HQLA_C1')+'.\n'; 	
		cadenaJustifica = cadenaJustifica + 'PODER: OJO DERECHO: ';
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_HQLA_C2')+'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     OJO IZQUIERDO: '; 		
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_HQLA_C3')+'.\n'; 
		cadenaJustifica = cadenaJustifica + 'TIEMPO: OJO DERECHO: ';			
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_HQLA_C4')+'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      OJO IZQUIERDO: '; 	
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_HQLA_C5')+'.\n'; 
		cadenaJustifica = cadenaJustifica + 'NUMERO DE DISPAROS: OJO DERECHO: ';			
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_HQLA_C6')+'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      OJO IZQUIERDO: '; 	
		cadenaJustifica = cadenaJustifica + valorAtributoSU('txt_HQLA_C7')+'.     \n'; 	
		cadenaJustifica = cadenaJustifica + 'OBSERVACION: ' +valorAtributoSU('txt_HQLA_C8')+'.\n'; 													
  }
  asignaAtributo(idElemento,  cadenaJustifica  ,0);	

}

function ponerJustificacion(){	
	var t1 = document.getElementById("lbl_JUST_NOPOS").innerHTML;
//	alert('t11111='+t1)
//	asignaAtributo("txt_JUST",t1,0);	
}


/*
	FIN DE FUNCIONES 
	AUTOR:DANIEL VALLEJO.
*/
