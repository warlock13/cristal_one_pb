<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.*, java.text.*, java.sql.*" %>
<%@ page import="Sgh.Utilidades.*" %>
<%@ page import="Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import="Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<%
    beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1050px" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- TÍTULO -->
            <div align="center" id="tituloForma">
                <jsp:include page="../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="FORMATOS DE IMPRESIÓN" />
                </jsp:include>
            </div>
            <!-- FIN DEL TÍTULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <!-- Tabla con los datos de búsqueda -->
                        <div id="divBuscar" style="display:block; min-height:400px; margin-left: 1px;">
                            <table width="99.7%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosListaEspera" align="center">
                                    <td width="10%">TIPO ID</td>
                                    <td width="10%">DOCUMENTO</td>
                                    <td width="20%">PACIENTE</td>
                                    <td width="5%"></td>
                                </tr>
                                <tr class="estiloImputListaEspera">
                                    <td>
                                        <select size="1" id="cmbTipoId" style="width:90%" >
                                            <option value=""></option>
                                            <% resultaux.clear();
                                                resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                                                ComboVO cmb105;
                                                for (int k = 0; k < resultaux.size(); k++) {
                                                    cmb105 = (ComboVO) resultaux.get(k);%>
                                            <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                                <%= cmb105.getId() + "  " + cmb105.getDescripcion()%>
                                            </option>
                                            <%}%>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtIdentificacion" style="width: 90%;"
                                               onkeypress="javascript:return soloNumeroIdentificacionHc(event);" />
                                    </td>
                                    <td>
                                        <input type="text" size="70" maxlength="70" style="width:90%" id="txtIdBusPaciente" />
                                        <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT"
                                             onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')"
                                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                    </td>
                                    <td>
                                        <input id="imgBuscarPaciente" type="button" class="small button blue" value="B U S C A R" title="BT86R"
                                               onclick='buscarHistoria("FoliosFormatoImpresion")'  />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr class="titulos" align="center">
                                    <td colspan="7">
                                        <table id="divParaContenedorFoliosPaciente"></table>
                                    </td>
                                </tr>
                            </table>
                            <table class="options-table" width="100%" style="float:right;">
                                <tbody>
                                    <tr class="estiloImputListaEspera">
                                        <td style="text-align:left;">
                                            <dl>
                                                <dt><input type="checkbox" id="chkEstadoLE" name="chkEstadoLE" value="" onchange="toggleAllCheckboxes()">[TODO]</dt>
                                                <dt><input type="checkbox" id="chkEstadoLE_1" name="chkEstadoLE_hc" value="1" onchange="updateTodoCheckbox()">Folio Historia Clinica </dt>
                                                <dt><input type="checkbox" id="chkEstadoLE_3" name="chkEstadoLE_anexo3" value="3" onchange="updateTodoCheckbox()">Anexo 3 de procedimientos</dt>
                                            </dl>
                                        </td>
                                        <td style="text-align:left;">
                                            <dl>
                                                <dt><input type="checkbox" id="chkEstadoLE_remisiones" name="chkEstadoLE_remisiones" value="" onchange="updateTodoCheckbox()">Remisiones</dt>
                                                <dt><input type="checkbox" id="chkEstadoLE_incapacidad" name="chkEstadoLE_incapacidad" value="" onchange="updateTodoCheckbox()">Incapacidad</dt>
                                                <!--<dt><input type="checkbox" id="chkEstadoLE_recomendaciones" name="chkEstadoLE_recomendaciones" value="" onchange="updateTodoCheckbox()">Recomendaciones</dt>-->
                                                <!--<dt><input type="checkbox" id="chkEstadoLE_indicaciones" name="chkEstadoLE_indicaciones" value="" onchange="updateTodoCheckbox()">Indicaciones</dt>-->
                                                <dt><input type="checkbox" id="chkEstadoLE_consentimiento_disentimiento" name="chkEstadoLE_consentimiento_disentimiento" value="" onchange="updateTodoCheckbox()">&nbsp;Consentimiento y disentimiento</dt>
                                            </dl>
                                        </td>
                                        <td style="vertical-align: bottom;">
                                            <dl style="margin-bottom: 5px;">
                                                <dt>
                                                    <button class="btn btn-primario" onclick="imprimirFormatosImpresion()">
                                                        Imprimir <i class="fas fa-print"></i>
                                                    </button>
                                                </dt>
                                            </dl>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table id="tableProcedimientosMedicamentos" width="100%">
                                <tr>
                                    <td>
                                        <table id="listaProcedimientoConductaTratamiento" class="scroll" width="100%"></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="listMedicacion" class="scroll" width="100%"></table>
                                    </td>
                                </tr>
                            </table>                                     
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<label id="lblIdDocumento"></label>
<label id="lblIdAdmision"></label>
<div id="divParaVentanita"></div>
<div id="divParaVentanita" style="display: none;">
    <label id="lblIdCitaAdjuntos"></label>
    <label id="lblIdAdmisionAdjuntos"></label>
    <label id="lblIdFacturaAdjuntos"></label>
    <label id="lblIdEvolucionAdjuntos"></label>
</div>
