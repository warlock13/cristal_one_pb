<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%  

beanAdmin.setCn(beanSession.getCn()); 
ArrayList resulCombo = new ArrayList();
ComboVO cmb;

String identificacion = request.getParameter("identificacion");
String identificacion_paciente = "";
String tipo_identificacion = "";
String fecha_ingreso_programa = "";
String fecha_ultima_atencion = "";
String numero_controles = "";
String codigo_municipio = "";
String zona = "";
String escolaridad = "";
String codigo_eps = "";
String codigo_pertenencia_etnia = "";
String grupo_poblacional = "";
String fecha_ingreso_nefroproteccion = "";
String diagnostico_hta = "";
String fecha_dx_hta = "";
String diagnostico_diabetes = "";
String fecha_dx_diabetes = "";
String etiologia = "";
String peso = "";
String talla = "";
String tension_sitolica = "";
String tension_diastolica = "";
String creatinina = "";
String fecha_creatinina = "";
String hemoglobina_a1c = "";
String fecha_hemoglobina_a1c = "";
String albuminuria = "";
String fecha_albuminuria = "";
String cociente_albuminuria_creatinuria = "";
String fecha_cociente_ac = "";
String historico_cocienteac1 = "";
String fecha_historico_cociente1 = "";
String historico_cocienteac2 = "";
String fecha_historico_cociente2 = "";
String historico_cocienteac3 = "";
String fecha_historico_cociente3 = "";
String albuminuria_confirmada = "";
String proteinas_uroanalsis = "";
String uroanalisis = "";
String fecha_uroanalisis = "";
String colesterol_total = "";
String fecha_colesterol = "";
String colesterol_hdl = "";
String fecha_hdl = "";
String colesterol_ldl = "";
String fecha_ldl = "";
String paratorhomona = "";
String fecha_pth = "";
String tfg_actual = "";
String recibe_ieca = "";
String recibe_araii = "";
String diagnostico_erc = "";
String envejecimiento_renal = "";
String estadio_erc = "";
String fecha_dx_erc = "";
String programa_erc = "";
String tfg_iniciar_trr = "";
String modo_inicio_trr = "";
String fecha_inicio_trr = "";
String fecha_ingreso_unidad_renal = "";
String hemodialisis = "";
String dosis_dialisis = "";
String dialisis_peritoneal = "";
String dosis_dialisis_peritoneal = "";
String horas_hemodialisis = "";
String peritonitis = "";
String vacuna_hepatitisb = "";
String fecha_dx_hepatitisb = "";
String fecha_dx_hepatitisc = "";
String terapia_no_dialitica_erc5 = "";
String hemoglobina = "";
String fecha_hemoglobina = "";
String albumina = "";
String fecha_albumina = "";
String fosforo = "";
String fecha_fosforo = "";
String estudio_posible_trasplante_renal = "";
String contraindicacion_trasplante_por_cancer = "";
String contraindicacion_trasplante_por_infeccion = "";
String contraindicacion_trasplante_no_deseo_paciente = "";
String contraindicacion_trasplante_esperanza_vida_menor_seismeses = "";
String contraindicacion_trasplante_limitacion_autocuidado = "";
String contraindicacion_trasplante_enfermedad_ccv = "";
String contraindicacion_trasplante_vih = "";
String contraindicacion_trasplante_hepatitisc = "";
String contraindicacion_enfermedad_inmunologica_activa = "";
String contraindicacion_trasplante_enfermedad_pulmonar_cronica = "";
String contraindicacion_trasplante_enfermedades_cronicas = "";
String fecha_ingreso_lista_espera_trasplante = "";
String ips_lista_espera = "";
String recibio_trasplante_renal = "";
String codigo_eps_realizo_ultimo_transplante = "";
String codigo_ips_realizo_ultimo_transplante = "";
String tipo_donante = "";
String complicacion_trasplante_renal = "";
String fecha_infeccion_cmv = "";
String fecha_infeccion_hongos = "";
String fecha_infeccion_tbc = "";
String fecha_complicacion_vascular = "";
String fecha_complicacion_urologica = "";
String fecha_complicacion_herida_qx = "";
String fecha_dx_cancer = "";
String medicamentos_inmunosupresores = "";
String metilprednisolona = "";
String azatioprina = "";
String ciclosporina = "";
String micofenolato = "";
String tracolimus = "";
String prednisona = "";
String medicamento_no_pos1 = "";
String medicamento_no_pos2 = "";
String medicamento_no_pos3 = "";
String episodios_rechazo_agudo = "";
String fecha_primer_rechazo_agudo = "";
String fecha_retorno_dialisis = "";
String numero_trasplantes_renales = "";
String costo_terapia_postrasplante = "";
String tiempo_prestacion_servicios = "";
String novedades = "";
String causa_muerte = "";
String fecha_muerte = "";
String categoria_tension_arterial = "";
String compensacion_hta = "";
String clasificacion_diabetes = "";
String compensacion_diabetes_mellitus = "";
String diagnostico_dislipidemias = "";
String fecha_dx_dislipidemias = "";
String fecha_dx_erc2 = "";
String creatinina_anterior = "";
String fecha_toma_creatinina = "";
String creatinuria = "";
String fecha_toma_creatinuria = "";
String glicemia_ayuno = "";
String fecha_toma_glicemia_ayuno = "";
String glicemia_posprandial = "";
String fecha_toma = "";
String trigliceridos = "";
String fecha_toma_trigliceridos = "";
String proteinuria = "";
String fecha_toma_proteinuria = "";
String potasio = "";
String fecha_toma_potasio = "";
String perimetro_abdominal = "";
String imc = "";
String clasificacion_imc = "";
String farmacos_antihipertensivos = "";
String antidiabeticos = "";
String estatina = "";
String adherencia_tratamiento = "";
String fumador_activo = "";
String exposicion_humo_lena = "";
String consumo_alcohol = "";
String actividad_fisica = "";
String antecedente_familiar_enfermedad_cardiovascular = "";
String tamizado_encuesta_rcv = "";
String complicacion_cardiaca = "";
String complicacion_cerebral = "";
String complicacion_retinianas = "";
String complicacion_vascular = "";
String complicacion_renales = "";
String educacion_patologia_equipo_interdisciplinario = "";
String valoracion_oftalmologica_medico_general = "";
String valoracion_podologica = "";
String fecha_atencion_medicina_interna = "";
String fecha_atencion_endocrinologia = "";
String fecha_atencion_cardiologia = "";
String fecha_atencion_oftalmologia = "";
String fecha_atencion_mefrologia = "";
String fecha_atencion_psicologia = "";
String fecha_atencion_nutricion = "";
String fecha_atencion_trabajo_social = "";
String fecha_toma_electrocardiograma = "";
String tfg_2016 = "";
String estadio_2016 = "";
String indice_progresion_erc = "";
String riesgo_framingham = "";
String clasificacion_riesgo_framingham = "";
String riesgo_cardiovascular_global = "";
String codigo_unico_identificacion = "";
String fecha_proximo_control = "";
String observaciones = "";
String medico = "";
String nombre_paciente = "";
String nombre_municipio = "";
Connection conexion = beanSession.cn.getConexion();
try{

Statement stmt = conexion.createStatement();
String SQL = " ";
SQL =  "select "+  
"coalesce(concat(p.nombre1,' ', p.nombre2,' ', p.apellido1,' ', p.apellido2), '') as nombre_paciente, "+
"coalesce (m.nombre, '') as nombre_municipio, "+
"coalesce(identificacion_paciente,  '') as identificacion_paciente, "+
"coalesce(tipo_identificacion,  '') as tipo_identificacion, "+
"coalesce(fecha_ingreso_programa,   '') as fecha_ingreso_programa, "+
"coalesce(fecha_ultima_atencion,    '') as fecha_ultima_atencion, "+
"coalesce(numero_controles,     '') as numero_controles, "+
"coalesce(codigo_municipio,     '') as codigo_municipio, "+
"coalesce(zona,     '') as zona, "+
"coalesce(escolaridad,  '') as escolaridad, "+
"coalesce(codigo_eps,   '') as codigo_eps, "+
"coalesce(codigo_pertenencia_etnia,     '') as codigo_pertenencia_etnia, "+
"coalesce(grupo_poblacional,    '') as grupo_poblacional, "+
"coalesce(fecha_ingreso_nefroproteccion,    '') as fecha_ingreso_nefroproteccion, "+
"coalesce(dha.nombre,  '') as diagnostico_hta, "+
"coalesce(fecha_dx_hta,     '') as fecha_dx_hta, "+
"coalesce(ddma.nombre ,     '') as diagnostico_diabetes, "+
"coalesce(fecha_dx_diabetes,    '') as fecha_dx_diabetes, "+
"coalesce(ee.nombre,    '') as etiologia, "+
"coalesce(bn.peso,     '') as peso, "+
"coalesce(bn.talla,    '') as talla, "+
"coalesce(tension_sitolica,     '') as tension_sitolica, "+
"coalesce(tension_diastolica,   '') as tension_diastolica, "+
"coalesce(creatinina,   '') as creatinina, "+
"coalesce(fecha_creatinina,     '') as fecha_creatinina, "+
"coalesce(hemoglobina_a1c,  '') as hemoglobina_a1c, "+
"coalesce(fecha_hemoglobina_a1c,    '') as fecha_hemoglobina_a1c, "+
"coalesce(albuminuria,  '') as albuminuria, "+
"coalesce(fecha_albuminuria,    '') as fecha_albuminuria, "+
"coalesce(cociente_albuminuria_creatinuria,     '') as cociente_albuminuria_creatinuria, "+
"coalesce(fecha_cociente_ac,    '') as fecha_cociente_ac, "+
"coalesce(historico_cocienteac1,    '') as historico_cocienteac1, "+
"coalesce(fecha_historico_cociente1,    '') as fecha_historico_cociente1, "+
"coalesce(historico_cocienteac2,    '') as historico_cocienteac2, "+
"coalesce(fecha_historico_cociente2,    '') as fecha_historico_cociente2, "+
"coalesce(historico_cocienteac3,    '') as historico_cocienteac3, "+
"coalesce(fecha_historico_cociente3,    '') as fecha_historico_cociente3, "+
"coalesce(albuminuria_confirmada,   '') as albuminuria_confirmada, "+
"coalesce(pua.nombre,     '') as proteinas_uroanalsis, "+
"coalesce(uroanalisis,  '') as uroanalisis, "+
"coalesce(fecha_uroanalisis,    '') as fecha_uroanalisis, "+
"coalesce(colesterol_total,     '') as colesterol_total, "+
"coalesce(fecha_colesterol,     '') as fecha_colesterol, "+
"coalesce(colesterol_hdl,   '') as colesterol_hdl, "+
"coalesce(fecha_hdl,    '') as fecha_hdl, "+
"coalesce(colesterol_ldl,   '') as colesterol_ldl, "+
"coalesce(fecha_ldl,    '') as fecha_ldl, "+
"coalesce(paratorhomona,    '') as paratorhomona, "+
"coalesce(fecha_pth,    '') as fecha_pth, "+
"coalesce(tfg_actual,   '') as tfg_actual, "+
"coalesce(recibe_ieca,  '') as recibe_ieca, "+
"coalesce(recibe_araii,     '') as recibe_araii, "+
"coalesce(dea.nombre,  '') as diagnostico_erc, "+
"coalesce(envejecimiento_renal,     '') as envejecimiento_renal, "+
"coalesce(eea.nombre,  '') as estadio_erc, "+
"coalesce(fecha_dx_erc,     '') as fecha_dx_erc, "+
"coalesce(programa_erc,     '') as programa_erc, "+
"coalesce(tfg_iniciar_trr,  '') as tfg_iniciar_trr, "+
"coalesce(modo_inicio_trr,  '') as modo_inicio_trr, "+
"coalesce(fecha_inicio_trr,     '') as fecha_inicio_trr, "+
"coalesce(fecha_ingreso_unidad_renal,   '') as fecha_ingreso_unidad_renal, "+
"coalesce(hemodialisis,     '') as hemodialisis, "+
"coalesce(dosis_dialisis,   '') as dosis_dialisis, "+
"coalesce(dialisis_peritoneal,  '') as dialisis_peritoneal, "+
"coalesce(dosis_dialisis_peritoneal,    '') as dosis_dialisis_peritoneal, "+
"coalesce(horas_hemodialisis,   '') as horas_hemodialisis, "+
"coalesce(peritonitis,  '') as peritonitis, "+
"coalesce(vacuna_hepatitisb,    '') as vacuna_hepatitisb, "+
"coalesce(fecha_dx_hepatitisb,  '') as fecha_dx_hepatitisb, "+
"coalesce(fecha_dx_hepatitisc,  '') as fecha_dx_hepatitisc, "+
"coalesce(terapia_no_dialitica_erc5,    '') as terapia_no_dialitica_erc5, "+
"coalesce(hemoglobina,  '') as hemoglobina, "+
"coalesce(fecha_hemoglobina,    '') as fecha_hemoglobina, "+
"coalesce(albumina,     '') as albumina, "+
"coalesce(fecha_albumina,   '') as fecha_albumina, "+
"coalesce(fosforo,  '') as fosforo, "+
"coalesce(fecha_fosforo,    '') as fecha_fosforo, "+
"coalesce(estudio_posible_trasplante_renal,     '') as estudio_posible_trasplante_renal, "+
"coalesce(contraindicacion_trasplante_por_cancer,   '') as contraindicacion_trasplante_por_cancer, "+
"coalesce(contraindicacion_trasplante_por_infeccion,    '') as contraindicacion_trasplante_por_infeccion, "+
"coalesce(contraindicacion_trasplante_no_deseo_paciente,    '') as contraindicacion_trasplante_no_deseo_paciente, "+
"coalesce(contraindicacion_trasplante_esperanza_vida_menor_seismeses,   '') as contraindicacion_trasplante_esperanza_vida_menor_seismeses, "+
"coalesce(contraindicacion_trasplante_limitacion_autocuidado,   '') as contraindicacion_trasplante_limitacion_autocuidado, "+
"coalesce(contraindicacion_trasplante_enfermedad_ccv,   '') as contraindicacion_trasplante_enfermedad_ccv, "+
"coalesce(contraindicacion_trasplante_vih,  '') as contraindicacion_trasplante_vih, "+
"coalesce(contraindicacion_trasplante_hepatitisc,   '') as contraindicacion_trasplante_hepatitisc, "+
"coalesce(contraindicacion_enfermedad_inmunologica_activa,  '') as contraindicacion_enfermedad_inmunologica_activa, "+
"coalesce(contraindicacion_trasplante_enfermedad_pulmonar_cronica,  '') as contraindicacion_trasplante_enfermedad_pulmonar_cronica, "+
"coalesce(contraindicacion_trasplante_enfermedades_cronicas,    '') as contraindicacion_trasplante_enfermedades_cronicas, "+
"coalesce(fecha_ingreso_lista_espera_trasplante,    '') as fecha_ingreso_lista_espera_trasplante, "+
"coalesce(ips_lista_espera,     '') as ips_lista_espera, "+
"coalesce(recibio_trasplante_renal,     '') as recibio_trasplante_renal, "+
"coalesce(codigo_eps_realizo_ultimo_transplante,    '') as codigo_eps_realizo_ultimo_transplante, "+
"coalesce(codigo_ips_realizo_ultimo_transplante,    '') as codigo_ips_realizo_ultimo_transplante, "+
"coalesce(tipo_donante,     '') as tipo_donante, "+
"coalesce(complicacion_trasplante_renal,    '') as complicacion_trasplante_renal, "+
"coalesce(fecha_infeccion_cmv,  '') as fecha_infeccion_cmv, "+
"coalesce(fecha_infeccion_hongos,   '') as fecha_infeccion_hongos, "+
"coalesce(fecha_infeccion_tbc,  '') as fecha_infeccion_tbc, "+
"coalesce(fecha_complicacion_vascular,  '') as fecha_complicacion_vascular, "+
"coalesce(fecha_complicacion_urologica,     '') as fecha_complicacion_urologica, "+
"coalesce(fecha_complicacion_herida_qx,     '') as fecha_complicacion_herida_qx, "+
"coalesce(fecha_dx_cancer,  '') as fecha_dx_cancer, "+
"coalesce(medicamentos_inmunosupresores,    '') as medicamentos_inmunosupresores, "+
"coalesce(mm.nombre,    '') as metilprednisolona, "+
"coalesce(am.nombre,  '') as azatioprina, "+
"coalesce(cm.nombre,     '') as ciclosporina, "+
"coalesce(mm2.nombre,     '') as micofenolato, "+
"coalesce(tm.nombre,   '') as tracolimus, "+
"coalesce(pre.nombre,   '') as prednisona, "+
"coalesce(medicamento_no_pos1,  '') as medicamento_no_pos1, "+
"coalesce(medicamento_no_pos2,  '') as medicamento_no_pos2, "+
"coalesce(medicamento_no_pos3,  '') as medicamento_no_pos3, "+
"coalesce(episodios_rechazo_agudo,  '') as episodios_rechazo_agudo, "+
"coalesce(fecha_primer_rechazo_agudo,   '') as fecha_primer_rechazo_agudo, "+
"coalesce(fecha_retorno_dialisis,   '') as fecha_retorno_dialisis, "+
"coalesce(numero_trasplantes_renales,   '') as numero_trasplantes_renales, "+
"coalesce(costo_terapia_postrasplante,  '') as costo_terapia_postrasplante, "+
"coalesce(tiempo_prestacion_servicios,  '') as tiempo_prestacion_servicios, "+
"coalesce(novedades,    '') as novedades, "+
"coalesce(causa_muerte,     '') as causa_muerte, "+
"coalesce(fecha_muerte,     '') as fecha_muerte, "+
"coalesce(cha.nombre,   '') as categoria_tension_arterial, "+
"coalesce(compensacion_hta,     '') as compensacion_hta, "+
"coalesce(cdm.nombre,   '') as clasificacion_diabetes, "+
"coalesce(cda.nombre,   '') as compensacion_diabetes_mellitus, "+
"coalesce(ddc.nombre,    '') as diagnostico_dislipidemias, "+
"coalesce(fecha_dx_dislipidemias,   '') as fecha_dx_dislipidemias, "+
"coalesce(fecha_dx_erc2,    '') as fecha_dx_erc2, "+
"coalesce(creatinina_anterior,  '') as creatinina_anterior, "+
"coalesce(fecha_toma_creatinina,    '') as fecha_toma_creatinina, "+
"coalesce(creatinuria,  '') as creatinuria, "+
"coalesce(fecha_toma_creatinuria,   '') as fecha_toma_creatinuria, "+
"coalesce(glicemia_ayuno,   '') as glicemia_ayuno, "+
"coalesce(fecha_toma_glicemia_ayuno,    '') as fecha_toma_glicemia_ayuno, "+
"coalesce(glicemia_posprandial,     '') as glicemia_posprandial, "+
"coalesce(fecha_toma,   '') as fecha_toma, "+
"coalesce(trigliceridos,    '') as trigliceridos, "+
"coalesce(fecha_toma_trigliceridos,     '') as fecha_toma_trigliceridos, "+
"coalesce(proteinuria,  '') as proteinuria, "+
"coalesce(fecha_toma_proteinuria,   '') as fecha_toma_proteinuria, "+
"coalesce(potasio,  '') as potasio, "+
"coalesce(fecha_toma_potasio,   '') as fecha_toma_potasio, "+
"coalesce(perimetro_abdominal,  '') as perimetro_abdominal, "+
"coalesce(imc,  '') as imc, "+
"coalesce(clasificacion_imc,    '') as clasificacion_imc, "+
"coalesce(fa.nombre,   '') as farmacos_antihipertensivos, "+
"coalesce(antd.nombre,   '') as antidiabeticos, "+
"coalesce(meac.nombre,     '') as estatina, "+
"coalesce(adherencia_tratamiento,   '') as adherencia_tratamiento, "+
"coalesce(fumador_activo,   '') as fumador_activo, "+
"coalesce(exposicion_humo_lena,     '') as exposicion_humo_lena, "+
"coalesce(consumo_alcohol,  '') as consumo_alcohol, "+
"coalesce(actividad_fisica,     '') as actividad_fisica, "+
"coalesce(antecedente_familiar_enfermedad_cardiovascular,   '') as antecedente_familiar_enfermedad_cardiovascular, "+
"coalesce(tamizado_encuesta_rcv,    '') as tamizado_encuesta_rcv, "+
"coalesce(complicacion_cardiaca,    '') as complicacion_cardiaca, "+
"coalesce(complicacion_cerebral,    '') as complicacion_cerebral, "+
"coalesce(complicacion_retinianas,  '') as complicacion_retinianas, "+
"coalesce(complicacion_vascular,    '') as complicacion_vascular, "+
"coalesce(complicacion_renales,     '') as complicacion_renales, "+
"coalesce(educacion_patologia_equipo_interdisciplinario,    '') as educacion_patologia_equipo_interdisciplinario, "+
"coalesce(valoracion_oftalmologica_medico_general,  '') as valoracion_oftalmologica_medico_general, "+
"coalesce(valoracion_podologica,    '') as valoracion_podologica, "+
"coalesce(fecha_atencion_medicina_interna,  '') as fecha_atencion_medicina_interna, "+
"coalesce(fecha_atencion_endocrinologia,    '') as fecha_atencion_endocrinologia, "+
"coalesce(fecha_atencion_cardiologia,   '') as fecha_atencion_cardiologia, "+
"coalesce(fecha_atencion_oftalmologia,  '') as fecha_atencion_oftalmologia, "+
"coalesce(fecha_atencion_mefrologia,    '') as fecha_atencion_mefrologia, "+
"coalesce(fecha_atencion_psicologia,    '') as fecha_atencion_psicologia, "+
"coalesce(fecha_atencion_nutricion,     '') as fecha_atencion_nutricion, "+
"coalesce(fecha_atencion_trabajo_social,    '') as fecha_atencion_trabajo_social, "+
"coalesce(fecha_toma_electrocardiograma,    '') as fecha_toma_electrocardiograma, "+
"coalesce(tfg_2016,     '') as tfg_2016, "+
"coalesce(estadio_2016,     '') as estadio_2016, "+
"coalesce(indice_progresion_erc,    '') as indice_progresion_erc, "+
"coalesce(riesgo_framingham,    '') as riesgo_framingham, "+
"coalesce(clasificacion_riesgo_framingham,  '') as clasificacion_riesgo_framingham, "+
"coalesce(riesgo_cardiovascular_global,     '') as riesgo_cardiovascular_global, "+
"coalesce(codigo_unico_identificacion,  '') as codigo_unico_identificacion, "+
"coalesce(fecha_proximo_control,    '') as fecha_proximo_control, "+
"coalesce(observaciones,    '') as observaciones, "+
"coalesce(medico,   '') as medico "+

"from migraciones.base_nominal_visor bn "+
"inner join hc.paciente p on p.identificacion = bn.identificacion_paciente "+
"inner join adm.municipio m on m.id = bn.codigo_municipio "+
"left join migraciones.diagnostico_hta_ac dha on  bn.diagnostico_hta = dha.id::VARCHAR "+
"left join migraciones.categoria_hta_ac cha on bn.categoria_tension_arterial = cha.id::VARCHAR "+
"left join migraciones.diagnostico_diabetes_miellitus_ac ddma on bn.diagnostico_diabetes = ddma.id::varchar "+
"left join migraciones.clasificacion_diabetes_mielitus cdm on  bn.clasificacion_diabetes = cdm.id ::varchar "+
"left join migraciones.compensacion_diabetes_ac cda on bn.compensacion_diabetes_mellitus = cda.id::varchar "+
"left join migraciones.etiologia_erc ee on bn.etiologia = ee.id::varchar "+
"left join migraciones.diagnostico_erc_ac dea on bn.diagnostico_erc = dea.id::varchar "+
"left join migraciones.estadio_erc_ac eea on bn.estadio_erc = eea.id::varchar "+
"left join migraciones.diagnostico_displidemias_ac ddc on bn.diagnostico_dislipidemias = ddc.id::varchar "+
"left join migraciones.proteina_uroanalisis_ac pua on bn.proteinas_uroanalsis = pua.id::varchar "+
"left join migraciones.meltipredinisolona_medac mm on bn.metilprednisolona = mm.id ::varchar "+
"left join migraciones.azatioprina_medac am on bn.azatioprina = am.id::varchar "+
"left join migraciones.ciclosporina_medac cm on bn.ciclosporina = cm.id::varchar "+
"left join migraciones.micofenolato_medac mm2 on bn.micofenolato = mm2.id::varchar "+
"left join migraciones.tracolimus_medac tm on bn.tracolimus = tm.id::varchar "+
"left join migraciones.meltipredinisolona_medac pre on bn.prednisona = pre.id::varchar "+
"left join migraciones.farmacos_antihipertensivosac fa on bn.farmacos_antihipertensivos = fa.id::varchar "+
"left join migraciones.antidiabeticos_ac antd on bn.antidiabeticos = antd.id::varchar "+
"left join migraciones.estatina_ac meac on bn.estatina = meac.id::varchar "+
"where identificacion_paciente ='" +identificacion + "'";

    
System.out.println(SQL);

  ResultSet rs = stmt.executeQuery(SQL);
  while(rs.next()){

    nombre_paciente = rs.getString("nombre_paciente");
    nombre_municipio = rs.getString("nombre_municipio");
    identificacion_paciente = rs.getString("identificacion_paciente");
    tipo_identificacion = rs.getString("tipo_identificacion");
    fecha_ingreso_programa = rs.getString("fecha_ingreso_programa");
    fecha_ultima_atencion = rs.getString("fecha_ultima_atencion");
    numero_controles = rs.getString("numero_controles");
    codigo_municipio = rs.getString("codigo_municipio");
    zona = rs.getString("zona");
    escolaridad = rs.getString("escolaridad");
    codigo_eps = rs.getString("codigo_eps");
    codigo_pertenencia_etnia = rs.getString("codigo_pertenencia_etnia");
    grupo_poblacional = rs.getString("grupo_poblacional");
    fecha_ingreso_nefroproteccion = rs.getString("fecha_ingreso_nefroproteccion");
    diagnostico_hta = rs.getString("diagnostico_hta");
    fecha_dx_hta = rs.getString("fecha_dx_hta");
    diagnostico_diabetes = rs.getString("diagnostico_diabetes");
    fecha_dx_diabetes = rs.getString("fecha_dx_diabetes");
    etiologia = rs.getString("etiologia");
    peso = rs.getString("peso");
    talla = rs.getString("talla");
    tension_sitolica = rs.getString("tension_sitolica");
    tension_diastolica = rs.getString("tension_diastolica");
    creatinina = rs.getString("creatinina");
    fecha_creatinina = rs.getString("fecha_creatinina");
    hemoglobina_a1c = rs.getString("hemoglobina_a1c");
    fecha_hemoglobina_a1c = rs.getString("fecha_hemoglobina_a1c");
    albuminuria = rs.getString("albuminuria");
    fecha_albuminuria = rs.getString("fecha_albuminuria");
    cociente_albuminuria_creatinuria = rs.getString("cociente_albuminuria_creatinuria");
    fecha_cociente_ac = rs.getString("fecha_cociente_ac");
    historico_cocienteac1 = rs.getString("historico_cocienteac1");
    fecha_historico_cociente1 = rs.getString("fecha_historico_cociente1");
    historico_cocienteac2 = rs.getString("historico_cocienteac2");
    fecha_historico_cociente2 = rs.getString("fecha_historico_cociente2");
    historico_cocienteac3 = rs.getString("historico_cocienteac3");
    fecha_historico_cociente3 = rs.getString("fecha_historico_cociente3");
    albuminuria_confirmada = rs.getString("albuminuria_confirmada");
    proteinas_uroanalsis = rs.getString("proteinas_uroanalsis");
    uroanalisis = rs.getString("uroanalisis");
    fecha_uroanalisis = rs.getString("fecha_uroanalisis");
    colesterol_total = rs.getString("colesterol_total");
    fecha_colesterol = rs.getString("fecha_colesterol");
    colesterol_hdl = rs.getString("colesterol_hdl");
    fecha_hdl = rs.getString("fecha_hdl");
    colesterol_ldl = rs.getString("colesterol_ldl");
    fecha_ldl = rs.getString("fecha_ldl");
    paratorhomona = rs.getString("paratorhomona");
    fecha_pth = rs.getString("fecha_pth");
    tfg_actual = rs.getString("tfg_actual");
    recibe_ieca = rs.getString("recibe_ieca");
    recibe_araii = rs.getString("recibe_araii");
    diagnostico_erc = rs.getString("diagnostico_erc");
    envejecimiento_renal = rs.getString("envejecimiento_renal");
    estadio_erc = rs.getString("estadio_erc");
    fecha_dx_erc = rs.getString("fecha_dx_erc");
    programa_erc = rs.getString("programa_erc");
    tfg_iniciar_trr = rs.getString("tfg_iniciar_trr");
    modo_inicio_trr = rs.getString("modo_inicio_trr");
    fecha_inicio_trr = rs.getString("fecha_inicio_trr");
    fecha_ingreso_unidad_renal = rs.getString("fecha_ingreso_unidad_renal");
    hemodialisis = rs.getString("hemodialisis");
    dosis_dialisis = rs.getString("dosis_dialisis");
    dialisis_peritoneal = rs.getString("dialisis_peritoneal");
    dosis_dialisis_peritoneal = rs.getString("dosis_dialisis_peritoneal");
    horas_hemodialisis = rs.getString("horas_hemodialisis");
    peritonitis = rs.getString("peritonitis");
    vacuna_hepatitisb = rs.getString("vacuna_hepatitisb");
    fecha_dx_hepatitisb = rs.getString("fecha_dx_hepatitisb");
    fecha_dx_hepatitisc = rs.getString("fecha_dx_hepatitisc");
    terapia_no_dialitica_erc5 = rs.getString("terapia_no_dialitica_erc5");
    hemoglobina = rs.getString("hemoglobina");
    fecha_hemoglobina = rs.getString("fecha_hemoglobina");
    albumina = rs.getString("albumina");
    fecha_albumina = rs.getString("fecha_albumina");
    fosforo = rs.getString("fosforo");
    fecha_fosforo = rs.getString("fecha_fosforo");
    estudio_posible_trasplante_renal = rs.getString("estudio_posible_trasplante_renal");
    contraindicacion_trasplante_por_cancer = rs.getString("contraindicacion_trasplante_por_cancer");
    contraindicacion_trasplante_por_infeccion = rs.getString("contraindicacion_trasplante_por_infeccion");
    contraindicacion_trasplante_no_deseo_paciente = rs.getString("contraindicacion_trasplante_no_deseo_paciente");
    contraindicacion_trasplante_esperanza_vida_menor_seismeses = rs.getString("contraindicacion_trasplante_esperanza_vida_menor_seismeses");
    contraindicacion_trasplante_limitacion_autocuidado = rs.getString("contraindicacion_trasplante_limitacion_autocuidado");
    contraindicacion_trasplante_enfermedad_ccv = rs.getString("contraindicacion_trasplante_enfermedad_ccv");
    contraindicacion_trasplante_vih = rs.getString("contraindicacion_trasplante_vih");
    contraindicacion_trasplante_hepatitisc = rs.getString("contraindicacion_trasplante_hepatitisc");
    contraindicacion_enfermedad_inmunologica_activa = rs.getString("contraindicacion_enfermedad_inmunologica_activa");
    contraindicacion_trasplante_enfermedad_pulmonar_cronica = rs.getString("contraindicacion_trasplante_enfermedad_pulmonar_cronica");
    contraindicacion_trasplante_enfermedades_cronicas = rs.getString("contraindicacion_trasplante_enfermedades_cronicas");
    fecha_ingreso_lista_espera_trasplante = rs.getString("fecha_ingreso_lista_espera_trasplante");
    ips_lista_espera = rs.getString("ips_lista_espera");
    recibio_trasplante_renal = rs.getString("recibio_trasplante_renal");
    codigo_eps_realizo_ultimo_transplante = rs.getString("codigo_eps_realizo_ultimo_transplante");
    codigo_ips_realizo_ultimo_transplante = rs.getString("codigo_ips_realizo_ultimo_transplante");
    tipo_donante = rs.getString("tipo_donante");
    complicacion_trasplante_renal = rs.getString("complicacion_trasplante_renal");
    fecha_infeccion_cmv = rs.getString("fecha_infeccion_cmv");
    fecha_infeccion_hongos = rs.getString("fecha_infeccion_hongos");
    fecha_infeccion_tbc = rs.getString("fecha_infeccion_tbc");
    fecha_complicacion_vascular = rs.getString("fecha_complicacion_vascular");
    fecha_complicacion_urologica = rs.getString("fecha_complicacion_urologica");
    fecha_complicacion_herida_qx = rs.getString("fecha_complicacion_herida_qx");
    fecha_dx_cancer = rs.getString("fecha_dx_cancer");
    medicamentos_inmunosupresores = rs.getString("medicamentos_inmunosupresores");
    metilprednisolona = rs.getString("metilprednisolona");
    azatioprina = rs.getString("azatioprina");
    ciclosporina = rs.getString("ciclosporina");
    micofenolato = rs.getString("micofenolato");
    tracolimus = rs.getString("tracolimus");
    prednisona = rs.getString("prednisona");
    medicamento_no_pos1 = rs.getString("medicamento_no_pos1");
    medicamento_no_pos2 = rs.getString("medicamento_no_pos2");
    medicamento_no_pos3 = rs.getString("medicamento_no_pos3");
    episodios_rechazo_agudo = rs.getString("episodios_rechazo_agudo");
    fecha_primer_rechazo_agudo = rs.getString("fecha_primer_rechazo_agudo");
    fecha_retorno_dialisis = rs.getString("fecha_retorno_dialisis");
    numero_trasplantes_renales = rs.getString("numero_trasplantes_renales");
    costo_terapia_postrasplante = rs.getString("costo_terapia_postrasplante");
    tiempo_prestacion_servicios = rs.getString("tiempo_prestacion_servicios");
    novedades = rs.getString("novedades");
    causa_muerte = rs.getString("causa_muerte");
    fecha_muerte = rs.getString("fecha_muerte");
    categoria_tension_arterial = rs.getString("categoria_tension_arterial");
    compensacion_hta = rs.getString("compensacion_hta");
    clasificacion_diabetes = rs.getString("clasificacion_diabetes");
    compensacion_diabetes_mellitus = rs.getString("compensacion_diabetes_mellitus");
    diagnostico_dislipidemias = rs.getString("diagnostico_dislipidemias");
    fecha_dx_dislipidemias = rs.getString("fecha_dx_dislipidemias");
    fecha_dx_erc2 = rs.getString("fecha_dx_erc2");
    creatinina_anterior = rs.getString("creatinina_anterior");
    fecha_toma_creatinina = rs.getString("fecha_toma_creatinina");
    creatinuria = rs.getString("creatinuria");
    fecha_toma_creatinuria = rs.getString("fecha_toma_creatinuria");
    glicemia_ayuno = rs.getString("glicemia_ayuno");
    fecha_toma_glicemia_ayuno = rs.getString("fecha_toma_glicemia_ayuno");
    glicemia_posprandial = rs.getString("glicemia_posprandial");
    fecha_toma = rs.getString("fecha_toma");
    trigliceridos = rs.getString("trigliceridos");
    fecha_toma_trigliceridos = rs.getString("fecha_toma_trigliceridos");
    proteinuria = rs.getString("proteinuria");
    fecha_toma_proteinuria = rs.getString("fecha_toma_proteinuria");
    potasio = rs.getString("potasio");
    fecha_toma_potasio = rs.getString("fecha_toma_potasio");
    perimetro_abdominal = rs.getString("perimetro_abdominal");
    imc = rs.getString("imc");
    clasificacion_imc = rs.getString("clasificacion_imc");
    farmacos_antihipertensivos = rs.getString("farmacos_antihipertensivos");
    antidiabeticos = rs.getString("antidiabeticos");
    estatina = rs.getString("estatina");
    adherencia_tratamiento = rs.getString("adherencia_tratamiento");
    fumador_activo = rs.getString("fumador_activo");
    exposicion_humo_lena = rs.getString("exposicion_humo_lena");
    consumo_alcohol = rs.getString("consumo_alcohol");
    actividad_fisica = rs.getString("actividad_fisica");
    antecedente_familiar_enfermedad_cardiovascular = rs.getString("antecedente_familiar_enfermedad_cardiovascular");
    tamizado_encuesta_rcv = rs.getString("tamizado_encuesta_rcv");
    complicacion_cardiaca = rs.getString("complicacion_cardiaca");
    complicacion_cerebral = rs.getString("complicacion_cerebral");
    complicacion_retinianas = rs.getString("complicacion_retinianas");
    complicacion_vascular = rs.getString("complicacion_vascular");
    complicacion_renales = rs.getString("complicacion_renales");
    educacion_patologia_equipo_interdisciplinario = rs.getString("educacion_patologia_equipo_interdisciplinario");
    valoracion_oftalmologica_medico_general = rs.getString("valoracion_oftalmologica_medico_general");
    valoracion_podologica = rs.getString("valoracion_podologica");
    fecha_atencion_medicina_interna = rs.getString("fecha_atencion_medicina_interna");
    fecha_atencion_endocrinologia = rs.getString("fecha_atencion_endocrinologia");
    fecha_atencion_cardiologia = rs.getString("fecha_atencion_cardiologia");
    fecha_atencion_oftalmologia = rs.getString("fecha_atencion_oftalmologia");
    fecha_atencion_mefrologia = rs.getString("fecha_atencion_mefrologia");
    fecha_atencion_psicologia = rs.getString("fecha_atencion_psicologia");
    fecha_atencion_nutricion = rs.getString("fecha_atencion_nutricion");
    fecha_atencion_trabajo_social = rs.getString("fecha_atencion_trabajo_social");
    fecha_toma_electrocardiograma = rs.getString("fecha_toma_electrocardiograma");
    tfg_2016 = rs.getString("tfg_2016");
    estadio_2016 = rs.getString("estadio_2016");
    indice_progresion_erc = rs.getString("indice_progresion_erc");
    riesgo_framingham = rs.getString("riesgo_framingham");
    clasificacion_riesgo_framingham = rs.getString("clasificacion_riesgo_framingham");
    riesgo_cardiovascular_global = rs.getString("riesgo_cardiovascular_global");
    codigo_unico_identificacion = rs.getString("codigo_unico_identificacion");
    fecha_proximo_control = rs.getString("fecha_proximo_control");
    observaciones = rs.getString("observaciones");
    medico = rs.getString("medico");
  }
}
catch ( Exception e ){
  System.out.println(e.getMessage());
} %>   



<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
  <!-- <div style="background-color: white; height: 300px; width: 100%; overflow: scroll;"> -->
      <div class="card">
          <div class="card-header alert alert-success">
              Antecedentes e historicos del paciente
          </div>
          <div class="card-body">
            <h5 class="card-title"> Informacion del paciente </h5>
            <div>
                
                <input class="form-control" type="text" style="border: none;" value="<%=nombre_paciente%>" readonly>  </p>
                
                <p class="card-text" >
                    Municipio
                    <input  type="text" style="border: none;" value="<%=nombre_municipio%>" readonly> 
                    Identificacion 
                    <input  type="text" style="border: none;" value="<%=identificacion_paciente%>" readonly> 
                
                </p>
                <p class="card-text" >
                    Estadio
                    <input style="border: none;" class="col-md-12" type="text" readonly value="<%=estadio_erc%>" >
                </p>
                
                
            </div>
          </div>
        </div>
  <!-- </div> -->
  <div class="accordion" id="accordionExample">
      <div class="card">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Historico de Atenciones 
            </button>
          </h2>
        </div>
    
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
              <div >
                  Fecha de ingreso al programa 
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_ingreso_programa%>" >
                  <hr>
              </div> 
              <div>
                  Fecha de ultima atencion 
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_ultima_atencion%>" >
                  <hr>
              </div> 
              <div>
                  Controles registrados hasta la ultima fecha de atencion
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=numero_controles%>" >
                  <hr>
              </div> 
              <!-- <code style="display: none;">.show</code> -->
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header" id="headingTwo">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Diagnosticos

            </button>
          </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
          <div class="card-body">
            <div>
                Diagnostico Hipertension arterial y fecha diagnostico
            </div>
            <div>
                <input id="diagnostico_hta" style="border: none;" class="col-md-4" type="text" readonly value="<%=diagnostico_hta%>" >
                <input id="fecha_dx_hta" style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_dx_hta%>" >
                <hr>
            </div>
            <div>
                Categoria tension arterial
            </div>
            <div>
                <input style="border: none;" class="col-md-4" type="text" readonly value="<%=categoria_tension_arterial%>" >
                <!-- Agregar datos -->
                <hr>
            </div>
            <div>
                Diagnostico Diabetes miellitus, clasificacion  y fecha diagnostico
            </div>
            <div>
                <!-- Agregar datos -->
                  <input style="border: none;" class="col-md-2" type="text" readonly value="<%=diagnostico_diabetes%>" >
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=clasificacion_diabetes%>" >
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_dx_diabetes%>" >
                  <hr>
            </div>
            <div>
                Compensacion Diabetes miellitus
            </div>
            <div>
                <!-- Agregar datos -->
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=compensacion_diabetes_mellitus%>" >
                  <hr>
            </div>
                <div>
                    Etiologia de la Enfermedad Renal cronica
                </div>
                <div>
                  <input style="border: none;" class="col-md-5" type="text" readonly value="<%=etiologia%>" >
                  <!-- Agregar datos -->
                  <hr>
                </div>
                
                <div>
                    Diagnostico ERC 
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=diagnostico_erc%>" >
                  <hr>
                </div>
                <div>
                    Estadio Enfermedad Renal cronica (ERC)
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-12" type="text" readonly value="<%=estadio_erc%>" >
                    <p>Valor TFG (988-999 no aplica) :</p><input  style="border: none;" class="col-md-6" type="text" readonly value="<%=tfg_actual%>" >
                    <hr>
                </div>
                <div>
                    Diagnostico Dislipidemias, fecha
                </div>
                <div>
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=diagnostico_dislipidemias%>" >
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_dx_dislipidemias%>" >
                  <!-- Agregar datos -->
                  <hr>
                </div>
                <div>
                    Riesgo cardio vascular global
                </div>
                <div>
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=riesgo_cardiovascular_global%>" >
                  <!-- Agregar datos -->
                  <hr>
                </div>
                
                
            </div>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header" id="headingThree">
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#colapsaTres" aria-expanded="false" aria-controls="colapsaTres">
                    Registros de medidas
                </button>
            </h2>
        </div>
        <div id="colapsaTres" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                Peso
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-2" type="text" readonly value="<%=peso%>" > kg
                    <hr>
                <div >
                    Talla
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-2" type="text" readonly value="<%=talla%>" > cm
                    <hr>
                </div>
                <div >
                    Tension Diastolica
                    <input style="border: none;" class="col-md-2" type="text" readonly value="<%=tension_diastolica%>" >mm de Hg
                    <hr>
                </div>
                
                <div >
                    Tension Sistolica
                    <input style="border: none;" class="col-md-2" type="text" readonly value="<%=tension_sitolica%>" > mm de Hg
                    <hr>
                </div>
                
                <div >
                    Perimetro Abdominal
                    <input style="border: none;" class="col-md-2" type="text" readonly value="<%=perimetro_abdominal%>" >cm
                    <hr>
                </div>
                
                <div >
                    Imc - clasificacion
                    <input style="border: none;" class="col-md-2" type="text" readonly value="<%=imc%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=clasificacion_imc%>" >
                    <hr>
                </div>
                
            </div>
            
        </div>
    </div>
    
    <div class="card">
        <div class="card-header" id="headingThree">
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapsaCuatro" aria-expanded="false" aria-controls="collapsaCuatro">
                    Resultados a examenes anteriores
                </button>
            </h2>
        </div>
        <div id="collapsaCuatro" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                Creatinina, fecha examen
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=creatinina%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_creatinina%>" >
                    <hr>
                </div>
                <div >
                    Creatinina anterior
                </div>
                <div>
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=creatinina_anterior%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_toma_creatinina%>" >
                    <!-- Agregar datos -->
                    <hr>
                </div>
                <div >
                    Creatinuria mg/dL, Fecha
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=creatinuria%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_toma_creatinuria%>" >
                    <hr>
                </div>
                <div>
                    <div >
                        Hemoglobina A1C, fecha examen
                    </div>
                    <div>
                        <!-- Agregar datos -->
                        <input style="border: none;" class="col-md-4" type="text" readonly value="<%=hemoglobina_a1c%>" >
                        <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_hemoglobina_a1c%>" >
                        <hr>
                    </div>
                    <div >
                        Albuminuria, Fecha_albuminuria
                    </div>
                    <div>
                        <!-- Agregar datos -->
                        <input style="border: none;" class="col-md-4" type="text" readonly value="<%=albuminuria%>" >
                        <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_albuminuria%>" >
                        <hr>
                    </div>
                    <div >
                        Cociente albuminuria creatinuria, fecha
                    </div>
                    <div>
                        <!-- Agregar datos -->
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=cociente_albuminuria_creatinuria%>" >
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_cociente_ac%>" >
                  <hr>
                </div>
                <div >
                    Historico 1 cociente Creatinuria/Albuminuria , fecha
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=historico_cocienteac1%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_historico_cociente1%>" >
                    <hr>
                </div>
                <div >
                    Historico 2 cociente Creatinuria/Albuminuria , fecha
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=historico_cocienteac2%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_historico_cociente2%>" >
                    <hr>
                </div>
                <div >
                    Historico 3 cociente Creatinuria/Albuminuria , fecha
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=historico_cocienteac3%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_historico_cociente3%>" >
                    <hr>
                </div>
                <div >
                    Uroanalisis, Fecha
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=uroanalisis%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_uroanalisis%>" >
                    <hr>
                </div>
                <div >
                    Proteinas en Uroanalisis
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=proteinas_uroanalsis%>" >
                    <hr>
                </div>
                <div >
                    Colesterol total, fecha colesterol
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=colesterol_total%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_colesterol%>" >
                    <hr>
                </div>
                <div >
                    Colesterol HDL, fecha
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=colesterol_hdl%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_hdl%>" >
                    <hr>
                </div>
                <div >
                    Colesterol LDL, fecha
                </div>
                <div>
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=colesterol_ldl%>" >
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_ldl%>" >
                  <!-- Agregar datos -->
                  <hr>
                </div>
                <div >
                    paratorhomona, fecha
                </div>
                <div>
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=paratorhomona%>" >
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_pth%>" >
                  <!-- Agregar datos -->
                  <hr>
                </div>
                <div >
                    Tasa de filtracion glomerural TFG
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=tfg_actual%>" >
                    <hr>
                </div>
                
                <div >
                    Glicemia de Ayuno, Fecha
                </div>
                <div>
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=glicemia_ayuno%>" >
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_toma_glicemia_ayuno%>" >
                  <!-- Agregar datos -->
                  <hr>
                </div>
                <div >
                    Glicemia Posprandial (valores 998 No aplica y 999 no existe dato en la historia clinica)
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=glicemia_posprandial%>" >
                    <hr>
                </div>
                <div >
                    Trigliceridos , Fecha
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=trigliceridos%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_toma_trigliceridos%>" >
                    <hr>
                </div>
                <div >
                    Proteinuria, Fecha toma
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=proteinuria%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_toma_proteinuria%>" >
                    <hr>
                </div>
                <div >
                    Potasio, Fecha toma 
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=potasio%>" >
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=fecha_toma_potasio%>" >
                  <hr>
              </div>
            </div>
          </div>
      </div>

      <div class="card">
          <div class="card-header" id="headingThree">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#colapsaCinco" aria-expanded="false" aria-controls="colapsaCinco">
                  Medicamentos
              </button>
            </h2>
          </div>
          <div id="colapsaCinco" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
              <div >
                  medicamentos Inmunosupresores

              </div>
              <div >
                  <hr>
                  Metilprednisolona
                  <input style="border: none;" class="col-md-4" type="text" readonly value="<%=metilprednisolona%>" >
                  <hr>
                </div>
                <div >
                    Azatioprina
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=azatioprina%>" >
                    <hr>
                </div>
                <div >
                    Ciclosporina
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=ciclosporina%>" >
                    <hr>
                </div>
                <div >
                    Micofenolato
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=micofenolato%>" >
                    <hr>
                </div>
                <div >
                    Tracolimus
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=tracolimus%>" >
                    <hr>
                </div>
                <div >
                    Prednisona 
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=prednisona%>" >
                    <hr>
                </div>
               
                <div >
                    Farmacos Antihipertensivos 
                </div>
                <div>
                    <!-- Agregar datos -->
                    <input style="border: none; font-size: small;" class="col-md-12" type="text" readonly value="<%=farmacos_antihipertensivos%>" >
                  <hr>
              </div>
              <div >
                 Antidiabeticos 
            </div>
            <div>
                <!-- Agregar datos -->
                <input style="border: none; font-size: small;" class="col-md-12" type="text" readonly value="<%=antidiabeticos%>" >
              <hr>
          </div>
               
                <div >
                    Estatina (lovastatina pravastatina rosuvastatina simvastatina atorvastatina)
                    <input style="border: none;" class="col-md-4" type="text" readonly value="<%=estatina%>" >
                    <hr>
              </div>
            </div>
          </div>
       </div>

  </div>

</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</html>
