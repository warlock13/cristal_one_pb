 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->
    

<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();

	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy HH:MM");
    SimpleDateFormat fdia = new SimpleDateFormat("dd");
    SimpleDateFormat fmes = new SimpleDateFormat("MM");
    SimpleDateFormat fanio = new SimpleDateFormat("yyyy");	
    SimpleDateFormat fhora = new SimpleDateFormat("HH:MM");		
	
	String dia=fdia.format((java.util.Calendar.getInstance(Locale.US)).getTime());
	String mes=fmes.format((java.util.Calendar.getInstance(Locale.US)).getTime());
    String anio=fanio.format((java.util.Calendar.getInstance(Locale.US)).getTime());
    String hora=fhora.format((java.util.Calendar.getInstance(Locale.US)).getTime());	
    String fechaHoy=anio+"-"+mes+"-"+dia+" Hora "+hora ;

%>
<style>
.letraTamano2 {
	font-size:10px;
	height:85px;
}
</style>  
<table id="documentoProcedimientoPDF" width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
  <tr class="camposRepInp">
    <td>

<table  width="100%" border="0" cellpadding="1" cellspacing="1" align="center" class="letraTamano2" >
  <tr>
    <td colspan="9" align="center"><b>MINISTERIO DE LA PROTECCION SOCIAL </b></td>
  </tr>
  <tr>
    <td colspan="9" align="center"><b>SOLICITUD DE AUTORIZACION DE SERVICIOS DE SALUD</b></td>    
  </tr>
  <tr>
    <td colspan="9" align="center">Numero de solicitud:<label id="lblIdDocumento"></label>&nbsp;&nbsp;&nbsp;&nbsp;Fecha:<%=fechaHoy%></td>    
  </tr>
  
</table>  

<table  width="100%" border="0" cellpadding="1" cellspacing="1" align="rigth" class="letraTamano2" >
  <tr>
    <td colspan="9" align="rigth"><font size="1"> Informaci&oacute;n del Prestador</font></td>
  </tr>
  <tr style="font-size:10px">
    <td width="67" align="" >Nombre:&nbsp;</td>
    <td colspan="6">Hermanas Hospitalarias del Sagrado Coraz&oacute;n :: Hospital Mental Nuestra Se&ntilde;ora del Perpetuo Socorro&nbsp;</td>
    <td width="76" colspan="2">Nit&nbsp;&nbsp;860007760-1&nbsp;</td>
  </tr>
  <tr style="font-size:10px">
    <td align="">C&oacute;digo&nbsp;</td>
    <td width="89">520010111401</td>
    <td colspan="1" width="105" align="right">Direcci&oacute;n Prestador:&nbsp;</td>
    <td colspan="6">Carrera 33 No 5 oeste 104 Barrio el Bosque &nbsp;</td>
    
  </tr>
  <tr style="font-size:10px">
    <td align="">Tel&eacute;fono&nbsp;</td>
    <td>7227094</td>
    <td align="right" colspan="1"  >Departamento:&nbsp;</td>
    <td colspan="1" width="158">Nari&ntilde;o&nbsp;</td>
    <td width="89" align="right">Municipio:&nbsp;</td>
    <td colspan="3">San Juan de Pasto</td>
  </tr>
  <tr>
    <td colspan="8">
      <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="left" class="letraTamano2" >                
               <tr class="titulos">
                   <td colspan="3"><b> ENTIDAD A LA QUE SE LE SOLICITA(PAGADOR):</b></td>
                   <td colspan="3"><label id="lblNombreAdmin"></label></td>
               </tr>      
                <tr class="titulos">
                   <td width="100%" colspan="6">DATOS DEL PACIENTE</b></td>
               </tr>
               <tr class="titulos">
                   <td width="16%" align="center" >Tipo Documento</td>               
                   <td width="16%" align="center" >Numero Documento</td>
                   <td width="16%" align="center">1er Apellido</td>  
                   <td width="16%" align="center">2do Apellido</td>                 
                   <td width="16%" align="center">1er Nombre</td>           
                   <td width="16%" align="center" colspan="2">2do Nombre</td>	                            
               </tr> 
               <tr style="font-size:11px"> 
                   <td align="center" ><font size="2"><label id="lblTipoIdPaciente"></label></b></font></td>                  
                   <td align="center" ><font size="2"><label id="lblIdentificacionPaciente"></label></b></font></td>   
                   <td align="center"><font size="2"><label id="lblApellido1"></label></b></font></td>
                   <td align="center"><font size="2"><label id="lblApellido2"></label></b></font></td>  
                   <td align="center"><font size="2"><label id="lblNombre1"></label></b></font></td>
                   <td align="center" colspan="2"><font size="2"><label id="lblNombre2" ></label></b></font></td>
               </tr>            
      </table> 
     </td>
    </tr>
  <tr>
    <td colspan="8">    
        
      <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="left" class="letraTamano2" >                
                <tr class="titulos">
                   <td width="5%" align="center">Cama</td>  
                   <td width="10%" align="center">Tipo Usuario</td>                 
                   <td width="15%" align="center">Fecha Nacimiento</td>           
                   <td width="25%" align="center">Direcci&oacute;n</td>
                   <td width="20%" align="center">Municipio Departamento</td> 
                   <td width="25%" align="center">Tel&eacute;fonos</td>		                            
               <tr> 
               <tr style="font-size:10px">
                   <td align="center"><label id="lblTipoServicio"></label></td>  
                   <td align="center"><label id="lblTipoUsuario"></label></td>                 
                   <td align="center"><label id="lblFechaNacimiento"></label></td>           
                   <td align="center"><label id="lblDireccion"></label></td>
                   <td align="center"><label id="lblMunicipio"></label></td> 
                   <td align="center"><label id="lblTelefono"></label></td>  				 		                                        
               <tr>  
      </table>        
    </td>
  </table>
           
           
    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >   
      <tr style="font-size:10px">
        <td colspan="4" align="center"><b>INFORMACION DE LA ATENCION Y SERVICIOS SOLICITADOS</b>
        </td>
      </tr>
      <tr style="font-size:10px">
         <td><b>ORIGEN ATENCION</td>
         <td><b>TIPO SERVICIO SOLICITADO</td>
         <td><b>PRIORIDAD DE LA ATENCION</td>
         <td><b>MANEJO INTEGRAL SEGUN GUIA</td>          
      </tr>  
      <tr style="font-size:10px">
         <td><label id="lblOrigenAtencionPdf" /></label></td>
         <td><label id="lblTipoServiciosSolicitadosPdf" /></label></td>
         <td><label id="lblPrioridadAtencionPdf" /></label></td>
         <td><label id="lblGuiaPdf" /></label></td>     
      </tr>
    </table>


           
<!--****************************   INICIO ACORDIONES             **********************-->
           <jsp:include page="divsSinAcordeon.jsp" flush="FALSE" >
                  <jsp:param name="idDiv" value="ordenProcedimientosPDF" />           
                  <jsp:param name="pagina" value="../documentosHistoriaClinicaPDF/ordenProcedimientosPDF.jsp" />                
                  <jsp:param name="display" value="block" />                
           </jsp:include>           
    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >  
        <tr class="titulos">
          <td colspan="4" align="center">JUSTIFICACION CLINICA
          </td>
        </tr>
        <tr style="font-size:10px">
         <td colspan="4"><label id="lbl_JusficacionClinica" /></label></td>                           
        </tr>                        
        <tr>
          <td width="100%">                                    
                <table id="listDiagnosticosPDFAnexos" class="scroll" cellpadding="0" cellspacing="0" align="center">
                <tr><th>
                
                </th></th></tr>  
                </table>
          </td>
        </tr>
    </table>   
<!--****************************   FIN    ACORDIONES             **********************-->                
  <table border="0" width="100%" cellpadding="1" cellspacing="1" align="center" bgcolor="#FF3333">                          
    <tr style="font-size:10px">
       <td align="center" colspan="2"><b>INFORMACION DE LA PERSONA QUE SOLICITA</td>
    </tr>
    <tr style="font-size:10px">
       <td><label id="lblFirmaProfesionalAnexos"></td>
       <td>TELEFONO:&nbsp;&nbsp;7235684</td>
    </tr>
  </table>
              
    </td>
  </tr>
</table>           

