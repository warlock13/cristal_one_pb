


<table id="documentoPDF" width="100%" border="1" cellpadding="1" cellspacing="1" align="center">                          
  <tr bgcolor="#FFFFFF">
    <td>
      <table  width="100%"  cellpadding="1" cellspacing="1" align="center" class="letraTamano2" >
        <tr>
          <td align="center"><b>SOLICITUD Y JUSTIFICACION MEDICA PARA PROCEDIMIENTOS E INSUMOS NO POS </b></td>
        </tr>
        <tr>
          <td align="center"><b>ESTE DOCUMENTO DEBE SER DILIGENCIADO EN FORMA COMPLETA, UNICAMENTE POR PARTE DEL MEDICO ESPECIALISTA</b></td>    
        </tr>
        <tr>
          <td align="center">
              <table width="50%" id="listHoraFechaNoSolicitudPDF" cellpadding="1" cellspacing="0" align="CENTER">
               <tr><th></th></tr>  
              </table>           
          </td>    
        </tr>  
      </table> 
    </td>
  </tr>
  <tr>
    <td>  
       <TABLE width="100%" class="fondoTablaPdf">    
          <tr>
            <td width="25%" class="tituloLineaInferior">NOMBRE DE LA IPS RECEPTORA</td>
            <td width="75%" rowspan="2">
              <table width="100%" id="listAdministradoraPDF" cellpadding="1" cellspacing="0" align="CENTER">
               <tr><th></th></tr>  
              </table>               
            </td>                  
          </tr> 
          <tr>
            <td class="tituloLineaInferior">Fundaci&oacute;n Oftalmol&oacute;gica de Nari&ntilde;o</td>
          </tr>  
       </TABLE> 
    </td>   
  </tr>  
  <tr bgcolor="#FFFFFF">
    <td>  
       <TABLE width="100%" class="fondoTablaPdf">    
          <tr>
            <td class="tituloLineaInferior">DATOS DEL PACIENTE
            </td>
          </tr>   
          <tr>
            <td width="100%">
              <table width="100%" id="listPaciente2PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr>  
       </TABLE> 
      </td>
  </tr>  
  
  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
             <td class="tituloLineaInferior">DIAGNOSTICO
             </td>
          </tr>
          <tr>
            <td width="100%">
              <table width="100%" id="listDiagnosticosPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr> 
  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="100%">
              <table width="100%" id="listResumenEnferActualTratamientoPDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr>   

  <tr bgcolor="#FFFFFF">
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="100%">
              <table width="100%" id="listMedicamentoUtilizado1PDF" cellpadding="1" cellspacing="1" align="CENTER">
               <tr><th></th></tr>  
              </table>
            </td>
          </tr> 
       </TABLE> 
      </td>  
  </tr> 
 
  <tr bgcolor="#FFFFFF">
    <td width="100%">
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>  
             <td class="tituloLineaInferior">EL COMITÉ VERIFICARA SI COINCIDE LA PRESCRIPCION CON LAS INDICACIONES TERAPEUTICAS APROBADAS POR EL INVIMA EN EL REGISTRO SANITARIO. NO APROBARA TRATAMIENTOS EXPERIMENTALES NI MEDICAMENTOS PRESCRITOS PARA TRATAMIENTOS EXPRESAMENTE EXCLUIDOS DEL POS </td>  
          </tr> 
       </TABLE>              
     </td>   
  </tr>  

  
  <tr bgcolor="#FFFFFF">&nbsp;
     <td>  
       <TABLE width="100%" class="fondoTablaPdf">
          <tr>
            <td width="60%">
              <table width="100%" id="listProfesionalPDF" cellpadding="1"  cellspacing="1">
               <tr><th></th></tr>  
              </table>
            </td>
            <td width="40%">&nbsp;
            </td>            
          </tr> 
       </TABLE> 
      </td>  
  </tr>      
</table>           

     <TABLE width="10%" class="fondoTablaPdf" align="center">
       <tr>
         <td width="100%" align="center">
           <table width="100%" id="listFinalizarImprimirPDF">
            <tr><th></th></tr>  
           </table>
         </td>
       </tr>             
     </TABLE>

