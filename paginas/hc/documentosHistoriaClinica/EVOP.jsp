<div id="plantilla_EVOP">

  <table width="100%" align="center">
    <tr>
      <td width="50%">&nbsp;

      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="20%">LENSOMETRIA</td>
            <td width="30%">Valor</td>
            <td width="50%">Adici&oacute;n</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EVOP_C1" name="lensom_od_valor" size="100" maxlength="25"
                style="width:90%" /></td>
            <td>
              <input type="text" id="txt_EVOP_C2" name="lensom_od_adicion" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()" />
              <!-- <select size="1" id="txt_EVOP_C2" name="" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                          <option value=""></option>
                          <option value="+1.00">+1.00</option>
                          <option value="+1.25">+1.25</option>
                          <option value="+1.50">+1.50</option>
                          <option value="+1.75">+1.75</option>
                          <option value="+2.00">+2.00</option>
                          <option value="+2.25">+2.25</option>
                          <option value="+2.50">+2.50</option>
                          <option value="+2.75">+2.75</option>                    
                          <option value="+3.00">+3.00</option>
                       </select>      -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EVOP_C3" name="lensom_oi_valor" size="100" maxlength="25" style="width:90%"
                onblur="guardarContenidoDocumento()" /></td>
            <td>
              <input type="text" id="txt_EVOP_C4" name="lensom_oi_adicion" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()" />
              <!-- <select size="1" id="txt_EVOP_C4" name="" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                          <option value=""></option>
                          <option value="+1.00">+1.00</option>
                          <option value="+1.25">+1.25</option>
                          <option value="+1.50">+1.50</option>
                          <option value="+1.75">+1.75</option>                    
                          <option value="+2.00">+2.00</option>
                          <option value="+2.25">+2.25</option>
                          <option value="+2.50">+2.50</option>
                          <option value="+2.75">+2.75</option>                    
                          <option value="+3.00">+3.00</option>
                       </select>    -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Observaciones</td>
            <td colspan="2"><input type="text" id="txt_EVOP_C5" name="lensom_observaciones" size="100" maxlength="200"
                style="width:96%" onblur="guardarContenidoDocumento()" /></td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td width="25%">AGUDEZA VISUAL Sin Correcci&oacute;n</td>
            <td width="37%">Visi&oacute;n Lejana</td>
            <td width="37%">Visi&oacute;n pr&oacute;xima</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td>
              <select size="1" id="txt_EVOP_C6" name="avsc_od_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>   -->
                <option value="Otro">Otro</option>
              </select>
              <input type="text" value="" id="txt_EVOP_C58" name="avsc_od_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <select size="1" id="txt_EVOP_C7" name="avsc_od_vision_proxima" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <!--<option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>  -->
                <option value="Otro">Otro</option>
              </select>
              <input type="text" value="" id="txt_EVOP_C59" name="avsc_od_proxima_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td>
              <select size="1" id="txt_EVOP_C8" name="avsc_oi_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>   -->
                <option value="Otro">Otro</option>
              </select>
              <input type="text" value="" id="txt_EVOP_C60" name="avsc_oi_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <select size="1" id="txt_EVOP_C9" name="avsc_oi_vision_proxima" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>    -->
                <option value="Otro">Otro</option>
              </select>
              <input type="text" value="" id="txt_EVOP_C61" name="avsc_oi_proxima_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Pinhole Ojo derecho</td>
            <td colspan="2">
              <input type="text" id="txt_EVOP_C10" name="pinhole_od" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()"  />
              <!--  <select size="1" id="txt_EVOP_C10" name="" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                          <option value=""></option>                    
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/150">20/150</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                       </select> -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Pinhole Ojo Izquierdo</td>
            <td colspan="2">
              <input type="text" id="txt_EVOP_C11" name="pinhole_oi" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()"  />
              <!--  <select size="1" id="txt_EVOP_C11" name="" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                          <option value=""></option>                   
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/150">20/150</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                       </select>    -->
            </td>
        </table>
      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="25%">AGUDEZA VISUAL Con Correcci&oacute;n</td>
            <td width="37%">Visi&oacute;n Lejana</td>
            <td width="37%">Visi&oacute;n pr&oacute;xima</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td>
              <select size="1" id="txt_EVOP_C12" name="avcc_od_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>    -->
                <option value="Otro">Otro</option>
              </select>
              <input type="text" value="" id="txt_EVOP_C62" name="avcc_od_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <input type="text" value="" id="txt_EVOP_C13" name="avcc_od_vision_proxima" style="width:80%" size="20"
                maxlength="20" onblur="guardarContenidoDocumento()" >
              <!--	<select size="1" id="txt_EVOP_C13" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                          <option value=""></option>                    
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/150">20/150</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>  
                     <option value="Otro">Otro</option>					
                       </select>  
                   -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td>
              <select size="1" id="txt_EVOP_C14" name="avcc_oi_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <!-- <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>    -->
                <option value="Otro">Otro</option>
              </select>
              <input type="text" value="" id="txt_EVOP_C63" name="avcc_oi_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <input type="text" value="" id="txt_EVOP_C15" name="avcc_oi_vision_proxima" style="width:80%" size="20"
                maxlength="20" onblur="guardarContenidoDocumento()" >
              <!-- 	<select size="1" id="txt_EVOP_C15" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                          <option value=""></option>                   
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/150">20/150</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                       </select>  -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>...</td>
            <td colspan="2">
              ...
            </td>
          </tr>
          <tr class="estiloImput">
            <td>..</td>
            <td colspan="2">
              ..
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>


  <table width="100%" border="1" cellpadding="1" cellspacing="1" align="center">
    <!-- <tr class="camposRepInp">
       <td width="50%" colspan="2">Examen externo Ojo Derecho</td>
       <td width="50%" colspan="2">Examen externo Ojo Izquierdo</td>
     <tr> -->
    <!-- <tr class="estiloImput">
       <td colspan="2">
         <textarea type="text" id="txt_EVOP_C16" name="examen_externo_od" size="1000" maxlength="4000" style="width:95%" 
           onblur="guardarContenidoDocumento()" onkeyup="this.value=this.value.toUpperCase()"> </textarea>
       </td>
       <td colspan="2">
         <textarea type="text" id="txt_EVOP_C17" name="examen_externo_oi" size="4000" maxlength="4000" style="width:95%" 
           onblur="guardarContenidoDocumento()" onkeyup="this.value=this.value.toUpperCase()"> </textarea>
       </td>
 
     </tr> -->
    <tr class="camposRepInp">
      <td style="width: 20%;">Reflejos pupilares</td>
      <td style="width: 25%;">Cover test</td>
      <td style="width: 25%;">Punto Pr&oacute;ximo de Convergencia</td>
      <td style="width: 30%;">Ducciones</td>
    <tr>
    <tr class="estiloImput">
      <td>
        <input type="text" value="" id="txt_EVOP_C18" name="reflejos_pupilares" style="width:98%" size="20"
          maxlength="1000" onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        VL:<input type="text" value="" id="txt_EVOP_C19" name="cover_test_vl" style="width:40%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
        VP:<input type="text" value="" id="txt_EVOP_C20" name="cover_test_vp" style="width:40%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        Real: <input type="text" value="" id="txt_EVOP_C21" name="ppc_real" style="width:20%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
        Luz: <input type="text" value="" id="txt_EVOP_C22" name="ppc_luz" style="width:20%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
        FR: <input type="text" value="" id="txt_EVOP_C23" name="ppc_fr" style="width:20%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        OD: <textarea type="text" id="txt_EVOP_C24" name="duccion_od" size="100" maxlength="100" style="width:96%"
           onblur="guardarContenidoDocumento()"> </textarea>
        OI: <textarea type="text" id="txt_EVOP_C25" name="duccion_oi" size="100" maxlength="100" style="width:96%"
           onblur="guardarContenidoDocumento()"> </textarea>
      </td>
    </tr>
    <tr class="camposRepInp">
      <td colspan="4">Versiones</td>
      <!-- <td>Oftalmoscopia Ojo Derecho</td>
      <td>Oftalmoscopia Ojo Izquierdo</td> -->
    <tr class="estiloImput">
      <td colspan="4">
        <input type="text" value="" id="txt_EVOP_C26" name="versiones" style="width:80%" size="1000" maxlength="50"
          onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        <!-- <textarea type="text" id="txt_EVOP_C66" name="oftalmoscopia_od" size="1000" maxlength="2000" style="width:90%"
           onblur="guardarContenidoDocumento()"> </textarea> -->
      </td>
      <td>
        <!-- <textarea type="text" id="txt_EVOP_C67" name="oftalmoscopia_oi" size="1000" maxlength="2000" style="width:95%"
           onblur="guardarContenidoDocumento()"> </textarea> -->
      </td>
  </table>
  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td width="40%">REFRACCION</td>
            <td width="30%">Valor</td>
            <td width="30%">AV</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EVOP_C68" name="refraccion_od_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <select size="1" id="txt_EVOP_C69" name="refraccion_od_av" style="width:99%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>
                <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>
                <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
              </select>
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EVOP_C70" name="refraccion_oi_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <select size="1" id="txt_EVOP_C71" name="refraccion_oi_av" style="width:99%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/140">20/140</option>
                <option value="20/200">20/200</option>
                <option value="20/300">20/300</option>
                <option value="20/400">20/400</option>
                <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>
                <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>
                <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="40%"> QUERATOMETRIA</td>
            <td width="30%">Valor</td>
            <td width="30%">&nbsp;</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EVOP_C72" name="queratometria_od_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>&nbsp;</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EVOP_C73" name="queratometria_oi_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td width="40%">SUBJETIVO</td>
            <td width="30%">Valor</td>
            <td width="30%">AV</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EVOP_C74" name="subjetivo_od_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <input type="text" id="txt_EVOP_C36" name="subjetivo_od_av" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
              <!-- <select size="1" id="txt_EVOP_C36" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                          <option value=""></option>                   
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                       </select>   -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EVOP_C37" name="subjetivo_oi_valor" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <input type="text" id="txt_EVOP_C38" name="subjetivo_oi_av" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
              <!-- <select size="1" id="txt_EVOP_C38" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                          <option value=""></option>                   
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>                                                                                                                                                                                                                                                                    
                       </select>        -->

            </td>
          </tr>
          <tr class="estiloImput">
            <td>Agudeza visual binocular</td>
            <td colspan="2">
              <input type="text" id="txt_EVOP_C65" name="av_binocular" size="50" maxlength="100" style="width:90%"
                onblur="guardarContenidoDocumento()" />
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Adici&oacute;n</td>
            <td colspan="2">
              <input type="text" value="" id="txt_EVOP_C39" name="subjetivo_adicion" style="width:50%" size="20"
                onblur="guardarContenidoDocumento()" >
              <!-- <select size="1" id="txt_EVOP_C39" name="" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                          <option value=""></option>
                          <option value="+1.00">+1.00</option>
                          <option value="+1.25">+1.25</option>
                          <option value="+1.50">+1.50</option>
                          <option value="+1.75">+1.75</option>
                          <option value="+2.00">+2.00</option>
                          <option value="+2.25">+2.25</option>
                          <option value="+2.50">+2.50</option>
                          <option value="+2.75">+2.75</option>                    
                          <option value="+3.00">+3.00</option>
                       </select>   -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Distancia Pupilar</td>
            <td colspan="2"><input type="text" id="txt_EVOP_C40" name="subjetivo_distancia_pupilar" maxlength="40"
                style="width:40%" onblur="guardarContenidoDocumento()" /> </td>
          </tr>
        </table>
      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="16%">RX FINAL</td>
            <td width="30%">Esfera&nbsp;&nbsp;&nbsp;&nbsp;Cilindro&nbsp;&nbsp;&nbsp;&nbsp;Eje&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td width="27%">Agudeza Visual Lejana</td>
            <td width="27%">Agudeza Visual Pr&oacute;xima</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td>
              <input type="text" id="txt_EVOP_C41" name="rx_od_esfera" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EVOP_C42" name="rx_od_cilindro" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EVOP_C43" name="rx_od_eje" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
            </td>
            <td>
              <input type="text" id="txt_EVOP_C44" name="rx_od_avl" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
              <!-- <select size="1" id="txt_EVOP_C44" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                     <option value=""></option>                     
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                          <option value="NO CORRIGE">NO CORRIGE</option>                                                                  
                       </select>  -->
            </td>
            <td>
              <input type="text" id="txt_EVOP_C45" name="rx_od_avp" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
              <!-- <select size="1" id="txt_EVOP_C45" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                          <option value=""></option>  
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                     <option value="NO CORRIGE">NO CORRIGE</option>
      
                       </select>  -->
            </td>
          </tr>

          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td>
              <input type="text" id="txt_EVOP_C46" name="rx_oi_esfera" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EVOP_C47" name="rx_oi_cilindro" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EVOP_C48" name="rx_oi_eje" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
            </td>
            <td>
              <input type="text" id="txt_EVOP_C49" name="rx_oi_avl" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
              <!-- <select size="1" id="txt_EVOP_C49" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                          <option value=""></option>  
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                          <option value="NO CORRIGE">NO CORRIGE</option>                                                      
                       </select>   -->
            </td>
            <td>
              <input type="text" id="txt_EVOP_C50" name="rx_oi_avp" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
              <!--  <select size="1" id="txt_EVOP_C50" name="" style="width:99%;" title="32" onblur="guardarContenidoDocumento()" >	  
                          <option value=""></option>  
                          <option value="20/20">20/20</option>
                          <option value="20/25">20/25</option>
                          <option value="20/30">20/30</option>
                          <option value="20/40">20/40</option>
                          <option value="20/50">20/50</option>
                          <option value="20/60">20/60</option>
                          <option value="20/70">20/70</option>
                          <option value="20/80">20/80</option>
                          <option value="20/100">20/100</option>
                          <option value="20/140">20/140</option>
                          <option value="20/200">20/200</option>
                          <option value="20/300">20/300</option>
                          <option value="20/400">20/400</option>
                          <option value="Cuenta Dedos 50 cm">Cuenta Dedos 50 cm</option>
                          <option value="Cuenta Dedos 1 mt">Cuenta Dedos 1 mt</option>
                          <option value="Cuenta Dedos 2 mt">Cuenta Dedos 2 mt</option>
                          <option value="Cuenta Dedos 3 mt">Cuenta Dedos 3 mt</option>
                          <option value="Cuenta Dedos 4 mt">Cuenta Dedos 4 mt</option>
                          <option value="Cuenta Dedos 5 mt">Cuenta Dedos 5 mt</option>  
                          <option value="Movimiento de manos 50 cm">Movimiento de manos 50 cm</option>
                          <option value="Movimiento de manos 1 mt">Movimiento de manos 1 mt</option>
                          <option value="Movimiento de manos 2 mt">Movimiento de manos 2 mt</option>
                          <option value="Movimiento de manos 3 mt">Movimiento de manos 3 mt</option>
                          <option value="Movimiento de manos 4 mt">Movimiento de manos 4 mt</option>
                          <option value="Movimiento de manos 5 mt">Movimiento de manos 5 mt</option>                                                                                
                          <option value="PERCEPCION DE LUZ">PERCEPCION DE LUZ</option>
                          <option value="NO PERCEPCION DE LUZ">NO PERCEPCION DE LUZ</option>
                          <option value="NO CORRIGE">NO CORRIGE</option>                                                       
                       </select>     -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Adici&oacute;n</td>
            <td colspan="2">
              <input type="text" value="" id="txt_EVOP_C51" name="rx_adicion" style="width:50%" size="20"
                onblur="guardarContenidoDocumento()" >
              <!-- <select size="1" id="txt_EVOP_C51" name="" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"  >	  
                          <option value=""></option>
                          <option value="+1.00">+1.00</option>
                          <option value="+1.25">+1.25</option>
                          <option value="+1.50">+1.50</option>
                          <option value="+1.75">+1.75</option>
                          <option value="+2.00">+2.00</option>
                          <option value="+2.25">+2.25</option>
                          <option value="+2.50">+2.50</option>
                          <option value="+2.75">+2.75</option>
                          <option value="+3.00">+3.00</option>
                       </select>   -->
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Observaciones:</td>
            <td colspan="2">
              <input type="text" value="" id="txt_EVOP_C64" name="observacion" style="width:99%" size="20"
                maxlength="800" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
        </table>
      </td>
    </tr>

   

    <tr>
      <td>
        <table class="tbPersonalizada" align="center" style="border-collapse: collapse; width: 100%; background:#fff;">
          <tr class="camposRepInp">
            <td colspan="4" style="text-align: center;">BIOMICROSCOPIA</td>
          </tr>
          <tr class="camposRepInp">
            <td align="right">SEGMENTO ANTERIOR</td>
            <td colspan="2" align="left">
              &nbsp;<select onchange="textoFondoOjo(this.value,'S','evop');" onblur="guardarContenidoDocumento()">
                <option value=""></option>
                <option value="1">OJO DERECHO - BLANCO</option>
                <option value="2">OJO DERECHO - NO VALORABLE</option>
                <option value="3">OJO IZQUIERDO - BLANCO</option>
                <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
                <option value="7">AMBOS OJOS - NO VALORABLES</option>
                <option value="5">LIMPIAR</option>
                <option value="6">VALORES NORMALES</option>
              </select>
            </td>
          </tr>
          <tr>
            <th></th>
            <th>OJO DERECHO</th>
            <th>OJO IZQUIERDO</th>
          </tr>
          <tr>
            <td>Parpados</td>
            <td>
              <textarea type="text" id="txt_EVOP_C27" name="parpados" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C84" name="parpados_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Aparato lagrimal</td>
            <td>
              <textarea type="text" id="txt_EVOP_C28" name="aparato_lagrimal" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C85" name="aparato_lagrimal_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Conjuntiva</td>
            <td>
              <textarea type="text" id="txt_EVOP_C29" name="conjuntiva" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C86" name="conjuntiva_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Cornea</td>
            <td>
              <textarea type="text" id="txt_EVOP_C30" name="cornea" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C87" name="cornea_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Esclerotica</td>
            <td>
              <textarea type="text" id="txt_EVOP_C31" name="esclerotica" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C88" name="esclerotica_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Camara Anterior</td>
            <td>
              <textarea type="text" id="txt_EVOP_C32" name="camara_anterior" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C89" name="camara_anterior_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>iris</td>
            <td>
              <textarea type="text" id="txt_EVOP_C33" name="iris" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C90" name="iris_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Cristalino</td>
            <td>
              <textarea type="text" id="txt_EVOP_C34" name="cristalino" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C91" name="cristalino_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Gonioscopia</td>
            <td>
              <textarea type="text" id="txt_EVOP_C35" name="gonioscopia" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C92" name="gonioscopia_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Schimmer</td>
            <td>
              <textarea type="text" id="txt_EVOP_C93" name="schimmer_d" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C94" name="schimmer_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>BUT</td>
            <td>
              <textarea type="text" id="txt_EVOP_C95" name="but_d" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C96" name="but_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>RB</td>
            <td>
              <textarea type="text" id="txt_EVOP_C97" name="rb_d" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C98" name="rb_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
        </table>
      </td>

      <td style="vertical-align: top;">

        <table id="idtboftalmoscopia" class="tbPersonalizada" align="center"
          style="border-collapse: collapse; width: 100%; background:#fff;">
          <tr class="camposRepInp">
            <td colspan="4" style="text-align: center;">OFTALMOSCOP&Iacute;A</td>
          </tr>
          <td align="left" style="height: 45px;" colspan="3">
            <!-- &nbsp;<select onchange="textoFondoOjo(this.value,'S','exop');" onblur="guardarContenidoDocumento()">
                    <option value=""></option>
                    <option value="1">OJO DERECHO - BLANCO</option>
                    <option value="2">OJO DERECHO - NO VALORABLE</option>
                    <option value="3">OJO IZQUIERDO - BLANCO</option>
                    <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
                    <option value="7">AMBOS OJOS - NO VALORABLES</option>
                    <option value="5">LIMPIAR</option>
                    <option value="6">VALORES NORMALES</option>
                  </select> -->
          </td>
          <tr>
            <th></th>
            <th>OJO DERECHO</th>
            <th>OJO IZQUIERDO</th>
          </tr>
          <tr>
            <td>Parpados</td>
            <td>
              <textarea type="text" id="txt_EVOP_C109" name="parpados_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C110" name="parpados_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Aparato lagrimal</td>
            <td>
              <textarea type="text" id="txt_EVOP_C111" name="aparato_lagrimal_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C112" name="aparato_lagrimal_i_of" size="1000" rows="4"
                maxlength="1000" style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Conjuntiva</td>
            <td>
              <textarea type="text" id="txt_EVOP_C113" name="conjuntiva_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C114" name="conjuntiva_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Cornea</td>
            <td>
              <textarea type="text" id="txt_EVOP_C115" name="cornea_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C116" name="cornea_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Esclerotica</td>
            <td>
              <textarea type="text" id="txt_EVOP_C117" name="esclerotica_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C118" name="esclerotica_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Camara Anterior</td>
            <td>
              <textarea type="text" id="txt_EVOP_C119" name="camara_anterior_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C120" name="camara_anterior_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>iris</td>
            <td>
              <textarea type="text" id="txt_EVOP_C121" name="iris_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C122" name="iris_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Cristalino</td>
            <td>
              <textarea type="text" id="txt_EVOP_C123" name="cristalino_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C124" name="cristalino_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Gonioscopia</td>
            <td>
              <textarea type="text" id="txt_EVOP_C125" name="gonioscopia_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C126" name="gonioscopia_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Schimmer</td>
            <td>
              <textarea type="text" id="txt_EVOP_C127" name="schimmer_d_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C128" name="schimmer_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>BUT</td>
            <td>
              <textarea type="text" id="txt_EVOP_C129" name="but_d_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C130" name="but_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>RB</td>
            <td>
              <textarea type="text" id="txt_EVOP_C131" name="rb_d_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EVOP_C132" name="rb_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
              </textarea>
            </td>
          </tr>
        </table>

      </td>

    </tr>
    <tr class="camposRepInp">
      <td width="50%" align="right">FONDO DE OJO &nbsp;</td>
      <td width="50%" align="left">
        &nbsp;<select onchange="textoFondoOjo(this.value,'F','evop')" onblur="guardarContenidoDocumento()">
          <option value=""></option>
          <option value="1">OJO DERECHO - BLANCO</option>
          <option value="2">OJO DERECHO - NO VALORABLE</option>
          <option value="3">OJO IZQUIERDO - BLANCO</option>
          <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
          <option value="7">AMBOS OJOS - NO VALORABLES</option>
          <option value="5">LIMPIAR</option>
          <option value="6">VALORES NORMALES</option>
        </select>
      </td>
    </tr>
    <tr>
      <table class="tbPersonalizada" align="center" style="border-collapse: collapse; width: 80%; background:#fff;">
        <tr>
          <th></th>
          <th>OJO DERECHO</th>
          <th>OJO IZQUIERDO</th>
        </tr>
        <tr>
          <td>Vitreo</td>
          <td>
            <textarea type="text" id="txt_EVOP_C99" name="vitreo" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EVOP_C100" name="vitreo_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
        </tr>
        <tr>
          <td>Papila (nervio &oacute;ptico)</td>
          <td>
            <textarea type="text" id="txt_EVOP_C101" name="papila" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EVOP_C102" name="papila_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
        </tr>
        <tr>
          <td>Macula</td>
          <td>
            <textarea type="text" id="txt_EVOP_C103" name="macula" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EVOP_C104" name="macula_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
        </tr>
        <tr>
          <td>Vasos</td>
          <td>
            <textarea type="text" id="txt_EVOP_C105" name="vasos" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EVOP_C106" name="vasos_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
        </tr>
        <tr>
          <td>Retina</td>
          <td>
            <textarea type="text" id="txt_EVOP_C107" name="retina" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EVOP_C108" name="retina_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
          </textarea>
          </td>
        </tr>
      </table>
    </tr>


    <tr class="estiloImput">
      <td width="100%" colspan="2">
        <TABLE width="100%">
          <tr class="estiloImput">
            <td colspan="4" bgcolor="#009999">&nbsp;</td>
          </tr>
          <tr class="titulos">
            <td colspan="2">Especificaciones del lente</td>
            <td>Filtro</td>
            <td>Color</td>
          </tr>
          <tr class="estiloImput">
            <td colspan="2"><input type="text" id="txt_EVOP_C52" name="especificaciones_lente" maxlength="500"
                style="width:90%" onblur="guardarContenidoDocumento()" /> </td>
            <td><input type="text" id="txt_EVOP_C53" name="filtro" maxlength="100" style="width:90%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td><input type="text" id="txt_EVOP_C54" name="color" maxlength="100" style="width:90%"
                onblur="guardarContenidoDocumento()" /> </td>
          </tr>
          <tr class="titulos">
            <td>Uso</td>
            <td>control</td>
            <td colspan="2">Recomendaciones</td>
          </tr>
          <tr class="estiloImput">
            <td><input type="text" id="txt_EVOP_C55" name="uso" style="width:90%" maxlength="100"
                onblur="guardarContenidoDocumento()" /> </td>
            <td><input type="text" id="txt_EVOP_C56" name="control_opt" style="width:90%" maxlength="100"
                onblur="guardarContenidoDocumento()" /> </td>
            <td colspan="2"><input type="text" id="txt_EVOP_C57" name="recomendaciones" maxlength="500"
                style="width:90%" onblur="guardarContenidoDocumento()" /> </td>
          </tr>
          <!-- <tr>  
               <td colspan="4" align="right">
                  <input id="btnProcedim_" type="button" class="small button blue" value="Impresion Ireport" onClick="formulaPDFHC();" />
                  <input id="btnProcedimientoImprimir" title="btn_o57p" type="button" class="small button blue" value="Imprime formula opt&aacute;lmica" onclick="imprimirAnexoOptalmica()"   />
               </td>                    
             <tr>  -->
        </TABLE>
      </td>
    </tr>

  </table>







</div>
<% /* <TABLE width="100%">
  <tr class="estiloImput">
    <td width="25%" align="center">ESTADO:
      <select size="1" id="cmbIdEstadoFolioEdit" style="width:60%" title="76" 
        onfocus="limpiarDivEditarJuan('limpiarMotivoFolioEdit');">
        <option value=""></option>
        <option value="7">Cancelado Finalizado</option>
        <option value="10">Reprogramado Finalizado</option>
      </select>
    </td>
    <td width="25%" align="center">
      MOTIVO:
      <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26" 
        onfocus="limpiaAtributo('cmbMotivoClaseFolioEdit',0)">
        <option value=""></option>
        <option value="26">ATRIBUIBLE AL PACIENTE</option>
        <option value="27">ATRIBUIBLE A LA INSTITUCION</option>
        <option value="20">ORDEN MEDICA</option>
      </select>
    </td>


    <td width="25%" align="center">
      CLASIFICACION:
      <select id="cmbMotivoClaseFolioEdit" style="width:60%" 
        onfocus="comboDependienteDosCondiciones('cmbMotivoClaseFolioEdit','cmbIdMotivoEstadoEdit','lblIdDocumento','565')">
        <option value=""></option>
      </select>
    </td>

    <td width="25%" align="center">
      <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO"
        onclick="cambioEstadoFolio()">

    </td>
  </tr>
  </TABLE>
  */
  %>
