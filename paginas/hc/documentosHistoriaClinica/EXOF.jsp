
<div id="plantilla_EXOF">
  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="60%" colspan="2">
        <table width="95%" align="center">
          <tr>
            <td width="80%">
              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="camposRepInp">
                  <td colspan="5">
                  </td>
                </tr>
                <tr class="camposRepInp">
                  <td colspan="5">AGUDEZA VISUAL LEJANA</td>
                </tr>
                <tr class="camposRepInp">
                  <td width="8%"></td>
                  <td width="40%">Sin Correccion</td>
                  <td width="0%" align="left">&nbsp;</td>
                  <td width="40%">Con correccion</td>
                  <td width="12%">Tipo</td>
                </tr>
                <tr class="estiloImput">
                  <td>Ojo derecho</td>
                  <td>
                    <select size="1" id="txt_EXOF_C1" name="agudeza_visual_od_sin" style="width:30%;" title="32"
                      onblur="guardarContenidoDocumento()" >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>
                    </select>
                    &nbsp;
                    <input type="text" id="txt_EXOF_C55" name="agudeza_visual_od_sin_obs" style="width:60%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" />

                  </td>
                  <td align="left"> . </td>
                  <td>
                    <select size="1" id="txt_EXOF_C3" name="agudeza_visual_od_con" style="width:30%;" title="32"
                      onblur="guardarContenidoDocumento()" >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>
                    </select>
                    &nbsp;
                    <input type="text" id="txt_EXOF_C56" name="agudeza_visual_od_con_obs" style="width:60%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" />

                  </td>
                  <td>
                    <select size="1" id="txt_EXOF_C4" name="agudeza_visual_od_con_tipo" style="width:98%" title="32"
                      onblur="guardarContenidoDocumento()" >
                      <option value=""></option>
                      <option value="PINHOLE">PINHOLE</option>
                      <option value="LENTES">LENTES</option>
                    </select>
                  </td>
                </tr>
                <tr class="estiloImput">
                  <td>Ojo Izquierdo</td>
                  <td>
                    <select size="1" id="txt_EXOF_C5" name="agudeza_visual_oi_sin" style="width:30%;" title="32"
                      onblur="guardarContenidoDocumento()" >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>
                    </select>
                    &nbsp;
                    <input type="text" id="txt_EXOF_C57" name="agudeza_visual_oi_sin_obs" style="width:60%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" />

                  </td>
                  <td align="left">

                  </td>
                  <td>
                    <select size="1" id="txt_EXOF_C7" name="agudeza_visual_oi_con" style="width:30%;" title="32"
                      onblur="guardarContenidoDocumento()" >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>
                    </select>
                    &nbsp;
                    <input type="text" id="txt_EXOF_C58" name="agudeza_visual_oi_con_obs" style="width:60%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" />

                  </td>
                  <td>
                    <select size="1" id="txt_EXOF_C8" name="agudeza_visual_oi_con_tipo" style="width:98%" title="32"
                      onblur="guardarContenidoDocumento()" >
                      <option value=""></option>
                      <option value="PINHOLE">PINHOLE</option>
                      <option value="LENTES">LENTES</option>
                    </select>
                  </td>
                </tr>
              </table>
            </td>
            <td width="20%">
              <table width="100%" border="1" cellpadding="0" cellspacing="7" align="center">
                <tr class="camposRepInp">
                  <td width="95%">AGUDEZA VISUAL CERCANA</td>
                </tr>
                <tr class="estiloImput">
                  <td align="center"><input type="text" id="txt_EXOF_C2" name="agudeza_visual_od_factor"
                      style="width:85%;" size="3" onblur="guardarContenidoDocumento()"
                      onkeypress="return validarkey_(event,this.id)" /></td>
                </tr>
                <tr class="estiloImput">
                  <td align="center"><input type="text" id="txt_EXOF_C6" name="agudeza_visual_oi_factor"
                      style="width:85%;" size="3" onblur="guardarContenidoDocumento()"
                      onkeypress="return validarkey_(event,this.id)" /></td>
                </tr>
              </table>
            </td>
          </tr> <!-- fila 1 -->
        </table>
      </td>
    </tr>
  </table>

  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="100%">
        <table width="50%" align="center">
          <tr class="titulos1">
            <td colspan="8" width="50%">REFRACCION</td>
          </tr>
          <tr class="titulos2">
            <td colspan="4" width="25%">Cerca</td>
            <td colspan="4" width="25%">Lejos</td>
          </tr>
          <tr class="estiloImput">
            <td></td>
            <td>Esf&eacute;rico</td>
            <td>Cilindro</td>
            <td>Eje</td>
            <td></td>
            <td>Esf&eacute;rico</td>
            <td>Cilindro</td>
            <td>Eje</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOF_C59" name="ref_cer_esf_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C60" name="ref_cer_cil_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C61" name="ref_cer_eje_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOF_C62" name="ref_lej_esf_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C63" name="ref_lej_cil_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C64" name="ref_lej_eje_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOF_C65" name="ref_cer_esf_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C66" name="ref_cer_cil_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C67" name="ref_cer_eje_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOF_C68" name="ref_lej_esf_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C69" name="ref_lej_cil_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C70" name="ref_lej_eje_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
          </tr>
        </table>
      </td>
      <td width="100%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td colspan="5" width="100%">QUERATOMETRIA</td>
          </tr>
          <tr class="estiloImput">
            <td></td>
            <td width="40%">K1</td>
            <td width="30%">K2</td>
            <td width="30%">Eje</td>
            <td width="30%">Dominancia</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOF_C71" name="que_k1_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C72" name="que_k2_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C73" name="que_eje_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td width="30%">
              <select size="1" id="txt_EXOF_C25" name="foria_dominancia" style="width:100px" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="OJO IZQUIERDO">OJO IZQUIERDO</option>
                <option value="OJO DERECHO">OJO DERECHO</option>
                <option value="ALTERNANTE">ALTERNANTE</option>
              </select>
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOF_C74" name="que_k1_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C75" name="que_k2_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C76" name="que_eje_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="100%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td colspan="5" width="100%">REFRACT&Oacute;METRO</td>
          </tr>
          <tr class="estiloImput">
            <td></td>
            <td>Esf&eacute;rico</td>
            <td>Cilindro</td>
            <td>Eje</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOF_C77" name="rec_esf_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C78" name="rec_cil_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C79" name="rec_eje_od" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOF_C80" name="rec_esf_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C81" name="rec_cil_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td><input type="text" id="txt_EXOF_C82" name="rec_eje_oi" maxlength="25" style="width:35px"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
      <td>

      </td>
      <!-- <td>
        <input id="btnProcedimiento" type="button" class="small button blue" value="PRECONSULTA"
          title="ATENDIDO DOCUMENTO BTN9765"
          onClick="modificarCRUD('pacientePreconsulta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')" />
      </td> -->
    </tr>
    <tr class="estiloImput">
      <td colspan="3">
        Observaciones:<input type="text" id="txt_EXOF_C50" name="observacion_refraccion" maxlength="3000"
          style="width:80%" onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" />
      </td>
    </tr>


    </tr>
  </table>

  <table width="100%" border="1" cellpadding="1" cellspacing="1" align="center">
    <tr class="camposRepInp">
      <td width="25%">PUPILAS</td>
    <tr>
    <tr class="estiloImput">
      <td><input type="text" id="txt_EXOF_C26" name="pupilas" maxlength="2000" style="width:70%"
          onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
    </tr>
  </table>

  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="1000%" bgcolor="#c0d3c1">.
      </td>
    </tr>
  </table>

  <table width="100%" border="1" cellpadding="1" cellspacing="1" align="center">
    <tr class="camposRepInp">
      <td colspan="4">BALANCE MUSCULAR</td>
    <tr>
    <tr class="camposRepInp">
      <td width="25%">Ojo derecho</td>
      <td width="25%">Ojo izquierdo</td>
      <td width="50%" colspan="2">

      </td>
    <tr>
    <tr>
      <td align="center">
        <table width="100" height="100" cellpadding="2" border="4"
          background="/clinica/utilidades/imagenes/acciones/balanceMuscular.png">
          <tr>
            <td valign="top" align="left"><input type="text" id="txt_EXOF_C9" name="balance_od1"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td valign="top" align="right"><input type="text" id="txt_EXOF_C10" name="balance_od2"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
          </tr>
          <tr>
            <td rowspan="2" align="left"><input type="text" id="txt_EXOF_C11" name="balance_od3"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td rowspan="2" align="right"><input type="text" id="txt_EXOF_C12" name="balance_od4"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td valign="bottom"><input type="text" id="txt_EXOF_C13" name="balance_od5"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td valign="bottom" align="right"><input type="text" id="txt_EXOF_C14" name="balance_od6"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
          </tr>
        </table>
      </td>
      <td align="center">
        <table width="100" height="100" cellpadding="2" border="4"
          background="/clinica/utilidades/imagenes/acciones/balanceMuscular.png">
          <tr>
            <td valign="top" align="left"><input type="text" id="txt_EXOF_C15" name="balance_oi1"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td valign="top" align="right"><input type="text" id="txt_EXOF_C16" name="balance_oi2"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
          </tr>
          <tr>
            <td rowspan="2" align="left"><input type="text" id="txt_EXOF_C17" name="balance_oi3"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td rowspan="2" align="right"><input type="text" id="txt_EXOF_C18" name="balance_oi4"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td valign="bottom"><input type="text" id="txt_EXOF_C19" name="balance_oi5"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td valign="bottom" align="right"><input type="text" id="txt_EXOF_C20" name="balance_oi6"
                style="width:120%; padding: 0px; text-align: center;" size="3" onblur="guardarContenidoDocumento()"
                maxlength="3" /></td>
          </tr>
        </table>
      </td>
      <td align="center" valign="top" colspan="2">
        <table width="100%" border="1" cellpadding="1" cellspacing="1" align="center">
          <tr class="camposRepInp">
            <td width="33%">Tropias</td>
            <td width="33%">Prismas</td>
            <td width="33%">Observaciones</td>
          <tr>
          <tr class="camposRepInp">
            <td width="33%">
              <select size="1" id="txt_EXOF_C21" name="tropias" style="width:70%" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="ORTOTROPIA">ORTOTROPIA</option>
                <option value="ENDOTROPIA">ENDOTROPIA</option>
                <option value="MICROTROPIA">MICROTROPIA</option>
                <option value="EXOTROPIA IZQUIERDA">EXOTROPIA IZQUIERDA</option>
                <option value="EXOTROPIA DERECHA">EXOTROPIA DERECHA</option>
                <option value="EXOTROPIA ALTERNANTE">EXOTROPIA ALTERNANTE</option>
                <option value="ENDOTROPIA IZQUIERDA">ENDOTROPIA IZQUIERDA</option>
                <option value="ENDOTROPI ADERECHA">ENDOTROPIA DERECHA</option>
                <option value="ENDOTROPIA ALTERNANTE">ENDOTROPIA ALTERNANTE</option>
                <option value="HIPERTROPIA IZQUIERDA">HIPERTROPIA IZQUIERDA</option>
                <option value="HIPERTROPIA DERECHA">HIPERTROPIA DERECHA</option>
                <option value="HIPOTROPIA IZQUIERDA">HIPOTROPIA IZQUIERDA</option>
                <option value="HIPOTROPIA DERECHA">HIPOTROPIA DERECHA</option>
                <option value="ESTRABISMO MIXTO">ESTRABISMO MIXTO</option>
              </select>
            </td>
            <td width="33%">
              <input type="text" id="txt_EXOF_C22" name="tropias_prismas" style="width:20%" size="20" maxlength="20"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" />
            </td>
            <td rowspan="6" width="33%">
              <textarea id="txt_EXOF_C51" name="observacion_balance"
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)"> </textarea>
            </td>
          <tr>
            <p>
          <tr class="camposRepInp">
            <td width="33%">Foria</td>
            <td width="33%">Prismas</td>
            <td>&nbsp;</td>
          <tr>
          <tr class="camposRepInp">
            <td width="33%">
              <select size="1" id="txt_EXOF_C23" name="foria" style="width:70%" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="ORTOFORIA">ORTOFORIA</option>
                <option value="EXOFORIA">EXOFORIA</option>
                <option value="ENDOFORIA">ENDOFORIA</option>
              </select>
            </td>
            <td width="33%">
              <input type="text" id="txt_EXOF_C24" name="foria_prismas" style="width:20%" size="20" maxlength="20"
                onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" />
            </td>
            <td>&nbsp;</td>
          <tr>

        </table>



      </td>
    </tr>
    <tr class="camposRepInp">
      <td colspan="4">BIOMICROSCOPIA</td>
    <tr class="camposRepInp">
      <td colspan="2" align="right">SEGMENTO ANTERIOR &nbsp;</td>
      <td colspan="2" align="left">
        &nbsp;<select onchange="textoFondoOjo(this.value,'S','exof');" onblur="guardarContenidoDocumento()">
          <option value=""></option>
          <option value="1">OJO DERECHO - BLANCO</option>
          <option value="2">OJO DERECHO - NO VALORABLE</option>
          <option value="3">OJO IZQUIERDO - BLANCO</option>
          <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
          <option value="7">AMBOS OJOS - NO VALORABLES</option>
          <option value="5">LIMPIAR</option>
          <option value="6">VALORES NORMALES</option>
        </select>
      </td>
    </tr>
    <tr>

      <table class="tbPersonalizada" align="center" style="border-collapse: collapse; width: 80%; background:#fff;">
        <tr>
          <th></th>
          <th>OJO DERECHO</th>
          <th>OJO IZQUIERDO</th>
        </tr>
        <tr>
          <td>Parpados</td>
          <td>
            <textarea type="text" id="txt_EXOF_C27" name="parpados" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C84" name="parpados_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Aparato lagrimal</td>
          <td>
            <textarea type="text" id="txt_EXOF_C28" name="aparato_lagrimal" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C85" name="aparato_lagrimal_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Conjuntiva</td>
          <td>
            <textarea type="text" id="txt_EXOF_C29" name="conjuntiva" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C86" name="conjuntiva_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Cornea</td>
          <td>
            <textarea type="text" id="txt_EXOF_C30" name="cornea" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C87" name="cornea_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Esclerotica</td>
          <td>
            <textarea type="text" id="txt_EXOF_C31" name="esclerotica" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C88" name="esclerotica_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Camara Anterior</td>
          <td>
            <textarea type="text" id="txt_EXOF_C32" name="camara_anterior" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C89" name="camara_anterior_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>iris</td>
          <td>
            <textarea type="text" id="txt_EXOF_C33" name="iris" size="1000" rows="4" maxlength="1000" style="width:95%"
               onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarKey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C90" name="iris_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Cristalino</td>
          <td>
            <textarea type="text" id="txt_EXOF_C34" name="cristalino" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C91" name="cristalino_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Gonioscopia</td>
          <td>
            <textarea type="text" id="txt_EXOF_C35" name="gonioscopia" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C92" name="gonioscopia_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>Schimmer</td>
          <td>
            <textarea type="text" id="txt_EXOF_C93" name="schimmer_d" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C94" name="schimmer_i" size="1000" rows="4" maxlength="1000"
              style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>BUT</td>
          <td>
            <textarea type="text" id="txt_EXOF_C95" name="but_d" size="1000" rows="4" maxlength="1000" style="width:95%"
               onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarKey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C96" name="but_i" size="1000" rows="4" maxlength="1000" style="width:95%"
               onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarKey_(event,this.id)">
            </textarea>
          </td>
        </tr>
        <tr>
          <td>RB</td>
          <td>
            <textarea type="text" id="txt_EXOF_C97" name="rb_d" size="1000" rows="4" maxlength="1000" style="width:95%"
               onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarKey_(event,this.id)">
            </textarea>
          </td>
          <td>
            <textarea type="text" id="txt_EXOF_C98" name="rb_i" size="1000" rows="4" maxlength="1000" style="width:95%"
               onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarKey_(event,this.id)">
            </textarea>
          </td>
        </tr>
      </table>

    </tr>

    <tr>
      <table style="width: 100%;">
        <tr class="estiloImput">
          <td width="50%">
            <table width="100%" align="center">
              <tr class="titulos1">
                <td colspan="5" width="100%">Presi&oacute;n intra Ocular</td>
              </tr>
              <tr class="estiloImput">
                <td>Hora</td>
                <td>Ojo Derecho</td>
                <td>Ojo Izquierdo</td>
              </tr>
              <tr class="estiloImput">
                <td><input type="time" id="txt_EXOF_C83" name="presion_intra_hora" maxlength="25" style="width:100px"
                    onblur="guardarContenidoDocumento()" onkeypress="return validarkey_(event,this.id)" /> </td>
                <td>mmHg:<input type="text" id="txt_EXOF_C36" name="presion_intra_ocula_od" maxlength="25"
                    style="width:100px" onblur="guardarContenidoDocumento()"
                    onkeypress="return validarkey_(event,this.id)" /> </td>
                <td>mmHg:<input type="text" id="txt_EXOF_C38" name="presion_intra_ocula_oi" maxlength="25"
                    style="width:100px" onblur="guardarContenidoDocumento()"
                    onkeypress="return validarkey_(event,this.id)" /> </td>
            </table>
          </td>
          <td width="50%">
            <table width="100%" align="center">
              <tr class="camposRepInp">
                <td colspan="4" align="center" width="100%">Observaciones </td>
              </tr>
              <tr>
                <td colspan="4" width="100%"><textarea id="txt_EXOF_C52" name="observacion_pio" style="width:95%"
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"></textarea></td>
              </tr>
            </table>
          </td>
        </tr>

      </table>

      <table style="width: 100%;">

        <tr class="camposRepInp">

          <td width="50%" align="right">FONDO DE OJO &nbsp;</td>
          <td width="50%" align="left">
            &nbsp;<select onchange="textoFondoOjo(this.value,'F','exof')" onblur="guardarContenidoDocumento()">
              <option value=""></option>
              <option value="1">OJO DERECHO - BLANCO</option>
              <option value="2">OJO DERECHO - NO VALORABLE</option>
              <option value="3">OJO IZQUIERDO - BLANCO</option>
              <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
              <option value="7">AMBOS OJOS - NO VALORABLES</option>
              <option value="5">LIMPIAR</option>
              <option value="6">VALORES NORMALES</option>
            </select>
          </td>
        </tr>
        <tr class="estiloImput">
          <td colspan="2">

            <table class="tbPersonalizada" align="center" style="border-collapse: collapse; width: 80%; background:#fff;">
              <tr>
                <th></th>
                <th>OJO DERECHO</th>
                <th>OJO IZQUIERDO</th>
              </tr>
              <tr>
                <td>Vitreo</td>
                <td>
                  <textarea type="text" id="txt_EXOF_C99" name="vitreo" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
                <td>
                  <textarea type="text" id="txt_EXOF_C100" name="vitreo_i" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
              </tr>
              <tr>
                <td>Papila (nervio &oacute;ptico)</td>
                <td>
                  <textarea type="text" id="txt_EXOF_C101" name="papila" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
                <td>
                  <textarea type="text" id="txt_EXOF_C102" name="papila_i" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
              </tr>
              <tr>
                <td>Macula</td>
                <td>
                  <textarea type="text" id="txt_EXOF_C103" name="macula" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
                <td>
                  <textarea type="text" id="txt_EXOF_C104" name="macula_i" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
              </tr>
              <tr>
                <td>Vasos</td>
                <td>
                  <textarea type="text" id="txt_EXOF_C105" name="vasos" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
                <td>
                  <textarea type="text" id="txt_EXOF_C106" name="vasos_i" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
              </tr>
              <tr>
                <td>Retina</td>
                <td>
                  <textarea type="text" id="txt_EXOF_C107" name="retina" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
                <td>
                  <textarea type="text" id="txt_EXOF_C108" name="retina_i" size="1000" rows="4" maxlength="1000"
                    style="width:95%" 
                    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                    onkeypress="return validarkey_(event,this.id)">
                  </textarea>
                </td>
              </tr>
            </table>
          </td>
        <tr class="camposRepInp">
          <td width="25%" colspan="2">Observaciones</td>
        </tr>
        <tr class="camposRepInp">
          <td colspan="2">
            <textarea type="text" id="txt_EXOF_C53" name="observacion_seg_posterior" size="4000" rows="4"
              maxlength="1000" style="width:95%" 
              onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
              onkeypress="return validarkey_(event,this.id)"> </textarea>
          </td>
        </tr>
    </tr>
    <tr class="camposRepInp">
      <td width="25%">Examenes complementarios</td>
      <td width="25%">Concepto</td>
    </tr>
    <tr class="estiloImput">

      <td>
        <textarea type="text" id="txt_EXOF_C45" name="consul_exa_suplem" size="4000" rows="4" maxlength="1000"
          style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
          onkeypress="return validarKey_(event,this.id)"> </textarea>
      </td>
      <td>
        <textarea type="text" id="txt_EXOF_C54" name="concepto" size="4000" rows="4" maxlength="8000" style="width:95%"
           onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
          onkeypress="return validarKey_(event,this.id)"> </textarea>
      </td>
    </tr>
  </table>
  </tr>



  </table>



</div>