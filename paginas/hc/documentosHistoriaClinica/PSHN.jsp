<table width="100%"  cellpadding="0" cellspacing="0"  align="center">

    <tr class="camposRepInp">
        <td  bgcolor="#c0d3c1" >DATOS DE IDENTIFICACI&Oacute;N DEL PACIENTE</td> 
    </tr>

    <tr class="estiloImput">

        <td >         
<textarea type="text" id="txt_PSHN_C1" rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()" 
placeholder="Orientaci&oacute;n sexual:
Estado Civil:
Religi&oacute;n:
Grupo Poblacional:"></textarea>         
        </td>  

    </tr>
    <tr class="camposRepInp"> 
        <td bgcolor="#c0d3c1" >1. EVALUACI&Oacute;N CLINICA PSICOLOGICA</td>
    </tr>
    <tr>
        <td width="100%" >
            <table width="100%" align="center">

                <tr class="camposRepInp"> 
                    <td width="37%">FACTORES DESENCADENANTES</td> 
                    <td width="37%">MANTENEDORES DEL PROBLEMA</td>
                </tr>     
                <tr class="estiloImput"> 
                    <td>         
                        <textarea type="text" id="txt_PSHN_C2"  placeholder="Hace referencia a los hechos que produjeron el problema, tiempo en el que aparecieron, por que no ha podido solucionar su problema." rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()" ></textarea>         
                    </td>  
                    <td>
                        <textarea type="text" id="txt_PSHN_C3"  placeholder="Que personas, situaciones o cosas empeoran o mantienen las dificultades."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>         
                    </td> 
                </tr>  
            </table>
        </td>


    <tr class="camposRepInp"> 
        <td >ANTECEDENTES PERSONALES Y FAMILIARES</td>                   

    </tr>     
    <tr class="estiloImput"> 
        <td >         
            <textarea type="text" id="txt_PSHN_C4"  placeholder="Estados, situaciones o vivencias que la persona o sus familiares han padecido y pueden estar influyendo en la sintomatologia actual. Tener en cuenta la manifestacion de tipos de violencias en la vida familiar, de pareja, laboral o social. Asi mismo, la manifestacion de conducta suicida en la familia, personas significativas o en el mismo paciente."  rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>         
        </td> 
    </tr>  


    <tr class="camposRepInp"> 
        <td  bgcolor="#c0d3c1" >2.  EXTRUCTURA Y FUNCIONAMIENTO FAMIIAR</td>
    </tr>


    <tr>
        <td width="100%">

            <table width="100%"   align="center">
                <tr class="estiloImput">
                    <td><b>MIEMBRO</b></td>
                    <td><b>PARENTESCO</b></td>
                    <td><b>EDAD</b></td>
                    <td><b>ESCOLARIDAD</b></td>
                    <td><b>OCUPACI&Oacute;N</b></td>

                </tr>
                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHN_C5" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C6" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C7" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C8" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C9" style="width:80%"/></td>
                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHN_C10" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C11" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C12" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C13" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C14" style="width:80%"/></td>
                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHN_C15" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C16" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C17" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C18" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C19" style="width:80%"/></td>
                </tr>


            </table>
        </td>
    </tr>

    <tr class="camposRepInp">
        <td  bgcolor="#c0d3c1" >3. ANTECEDENTES DEL PACIENTE </td>
    </tr>     
       

    <tr>
        <td >

            <table width="100%"   align="center">

                <tr class="camposRepInp">
        <td>1. GESTACION:COMO FUE </td>
        <td>2. PARTO</td>
        <td>3. RECIEN NACIDO </td>
    </tr>
                
                <tr class="estiloImput"> 

<td>
<textarea type="text" id="txt_PSHN_C20"  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>  
</td>  
<td>
<textarea type="text" id="txt_PSHN_C21"   rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>  
</td> 
<td>
<textarea type="text" id="txt_PSHN_C22"  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>  
</td> 
                </tr>


            </table>

        </td>

    </tr>

    <tr class="camposRepInp">
        <td > INFANCIA </td>
    </tr>  

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="estiloImput">
                    <td ><b>CONDUCTA </b></td>
                    <td ><b>EDAD</b></td>
                    <td ><b>CONDUCTA </b></td>
                    <td ><b>EDAD</b></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>SOSTIENEN LA CABEZA</u></td>
                    <td><input type="text" id="txt_PSHN_C23" style="width:30%"/></td>
                    <td><u>DECIR VARIAS PALABRAS JUNTAS </u></td>
                    <td><input type="text" id="txt_PSHN_C24" style="width:30%"/></td>
                </tr>


                <tr class="estiloImput"> 
                    <td><u>BALBUCEO</u></td>
                    <td><input type="text" id="txt_PSHN_C25" style="width:30%"/></td>
                    <td><u>CONTROL DE ESFINTERES </u></td>
                    <td><input type="text" id="txt_PSHN_C26" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>SENTARSE SOLO(A)</u></td>
                    <td><input type="text" id="txt_PSHN_C27" style="width:30%"/></td>
                    <td><u>ALIMENTARCE SOLO</u></td>
                    <td><input type="text" id="txt_PSHN_C28" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>GATEAR </u></td>
                    <td><input type="text" id="txt_PSHN_C29" style="width:30%"/></td>
                    <td><u>VERSTIRSE SOLO</u></td>
                    <td><input type="text" id="txt_PSHN_C30" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>CAMINAR SOLO (A)</u></td>
                    <td><input type="text" id="txt_PSHN_C31" style="width:30%"/></td>
                    <td><u>BA&Ntilde;ARSE SOLO (A)</u></td>
                    <td><input type="text" id="txt_PSHN_C32" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>DECIR SU PRIMERA PALABRA </u></td>
                    <td><input type="text" id="txt_PSHN_C33" style="width:30%"/></td>
                                    </tr>

            </table>
        </td>

    </tr>

   

    <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" >4. EVALUACION DE AREAS DE DESEMPE&Ntilde;O</td>
    </tr>

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="camposRepInp">
                    <td>1. AREA PERSONAL</td>
                    <td>2. AREA ACADEMICA</td>
                    <td>3. AREA SOCIAL</td>
                  
                </tr>


                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHN_C34"  placeholder="Descripcion personal de su forma de ser, valoracion, cualidades y defectos."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C35"  placeholder="Institucion donde estudia, grado, desempeno, adaptacion al ambiente, a la metodologia de ensenanza y a las normas, proceso de aprendizaje, identificar si ha requerido de apoyos educativos o centros de Educacion Especial, referir impresión de CI (baja, promedio, alta, etc.)"  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C36"  placeholder="Preferencias de grupos de amigos y actividades compartidas. Satisfaccion y desempeno. Manifestacion de algun tipo de violencias"  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    
                </tr>



                <tr class="camposRepInp">
                      <td>4. AREA FAMILIAR</td>
                    <td>5. AREA AFECTIVA </td>
                    <td>6. JUEGO Y RECREACI&Oacute;N</td>

                </tr>

                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHN_C37"  placeholder="Composicion familiar, adaptacion, comunicacion."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C38"  placeholder="Nivel y manifestacion de emociones y afectos, caracteristicas generales."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C39"  placeholder="Tendencias y preferencias."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    
                </tr>


            </table>

        </td>
    </tr>



    <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" >5. TECNICAS DISCIPLINARIAS</td>
    </tr>



 <tr>
        <td >
            <table width="100%" align="center">
                <tr class="estiloImput">
                    <td ><b>TECNICA  </b></td>
                    <td ><b>MARCA </b></td>
                    <td ><b>TECNICA  </b></td>
                    <td ><b>MARCA </b></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>IGNORAR CONDUCTA PROBLEMA </u></td>
                    <td><input type="text" id="txt_PSHN_C40" style="width:30%"/></td>
                    <td><u>DECIRLE QUE SE SIENTE  </u></td>
                    <td><input type="text" id="txt_PSHN_C41" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>REGA&Ntilde;AR</u></td>
                    <td><input type="text" id="txt_PSHN_C42" style="width:30%"/></td>
                    <td><u>MANDAR AL CUARTO </u></td>
                    <td><input type="text" id="txt_PSHN_C43" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>GOLPEAR</u></td>
                    <td><input type="text" id="txt_PSHN_C44" style="width:30%"/></td>
                    <td><u>RETIRAR ALGUNAS ACTIVIDADES DE AGRADO PARA EL NI&Ntilde;O (A)</u></td>
                    <td><input type="text" id="txt_PSHN_C45" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>NALGUEAR </u></td>
                    <td><input type="text" id="txt_PSHN_C46" style="width:30%"/></td>
                    <td><u>GRITAR </u></td>
                    <td><input type="text" id="txt_PSHN_C47" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>AMENAZAR </u></td>
                    <td><input type="text" id="txt_PSHN_C48" style="width:30%"/></td>
                    <td><u>NINGUNA TECNICA </u></td>
                    <td><input type="text" id="txt_PSHN_C49" style="width:30%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>RAZONAR CON EL NI&Ntilde;O(A)</u></td>
                    <td><input type="text" id="txt_PSHN_C50" style="width:30%"/></td>
                    <td> <u>OTRA TECNICA</u></td>
                    <td><input type="text" id="txt_PSHN_C51" style="width:80%"/></td>
                </tr>

                <tr class="estiloImput"> 
                    <td><u>REDIRIGIR EL INTERES DEL NI&Ntilde;O(A)</u></td>
                    <td><input type="text" id="txt_PSHN_C52" style="width:30%"/></td>
                </tr>

              
        </table>
    </td>

</tr>






    <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" >6. RED DE APOYO</td>
    </tr>


    <tr>
        <td width="100%">

            <table width="100%"   align="center">
                <tr class="estiloImput">
                    <td><b>MIEMBRO</b></td>
                    <td><b>PARENTESCO</b></td>
                    <td><b>EDAD</b></td>
                    <td><b>ESCOLARIDAD</b></td>
                    <td><b>OCUPACI&Oacute;N</b></td>                    
                    <td><b>TELEFONO</b></td>
                    <td><b>DIRECCI&Oacute;N</b></td>


                </tr>
                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHN_C53" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C54" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C55" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C56" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C57" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C58" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C59" style="width:80%"/></td>

                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHN_C60" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C61" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C62" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C63" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C64" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C65" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C66" style="width:80%"/></td>
                </tr>

                <tr class="estiloImput">
                    <td><input type="text" id="txt_PSHN_C67" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C68" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C69" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C70" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C71" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C72" style="width:80%"/></td>
                    <td><input type="text" id="txt_PSHN_C73" style="width:80%"/></td>
                </tr>


            </table>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            <table width="100%" align="center">

                <tr class="camposRepInp">
                    <td bgcolor="#c0d3c1" >7. EXPECTATIVAS FRENTE AL PROCESO DE INTERVENCI&Oacute;N</td>
                    <td bgcolor="#c0d3c1" >8. IMPRESI&Oacute;N DIAGNOSTICA</td>
                </tr>
                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHN_C74"  placeholder="Que espera del presente proceso."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C75"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                </tr>

            </table>

        </td>

    </tr>

    <tr class="camposRepInp">
        <td bgcolor="#c0d3c1" >9. PLAN DE MANEJO </td>
    </tr>

    <tr>
        <td >
            <table width="100%" align="center">
                <tr class="camposRepInp">
                    <td>ENFOQUE TERAPEUTICO</td>
                    <td>PLAN DE SEGUIMIENTO</td>
                    <td>TECNICA PSICOTERAPEUTICA A APLICAR </td>
                    <td>PRUEBAS PSICOLOGICAS ADICIONALES </td>
                </tr>


                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHN_C76"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C77"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C78"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C79"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>

                </tr>

                <tr class="camposRepInp">
                    <td>IDENTIFICAR NIVEL DE RIESGO </td>
                    <td>PSICOEDUCACION Y RECOMENDACIONES</td>
                    <td>REMISION A II NIVEL DE ATENCION U OTROS SERVICIOS </td>
                    <td>ACTIVACION DE RUTAS DE ATENCION ESPECIFICAS </td>
                </tr>


                <tr class="estiloImput">
                    <td><textarea type="text" id="txt_PSHN_C80"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C81"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C82"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                    <td><textarea type="text" id="txt_PSHN_C83"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>

                </tr>


            </table> 
        </td>
    </tr>
</table>

