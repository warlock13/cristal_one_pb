
<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" pageEncoding="UTF-8" %>

<table width="100%"  align="center">
<tr>
 <td >
  <table width="100%" align="center">

        <tr class="estiloImput">
          <td width="100%" colspan="4">PACIENTE REINGRESA A CIRUGÍA EN UN PERIODO MENOR A 3 MESES POR LA MISMA CAUSA:
             <select size="1" id="txt_LICC_C1" style="width:5%;" title="32"    >	  
                <option value="SI" selected="selected">SI</option>
                <option value="NO">NO</option>
             </select>
          </td>                                           
        </tr>		
        <tr class="titulos">
          <td width="20%">TIEMPO DE ADMISIÓN</td>                               
          <td width="20%">TIEMPO DE ANESTESIA</td>
          <td width="20%">TIEMPO DE PROCEDIMIENTO</td>                
          <td width="20%">TIEMPO DE RECUPERACION</td>                
        </tr>		

        <tr class="estiloImput"> 
          <td>
              <input type="text" id="txt_LICC_C2" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICC_C3" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICC_C4" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICC_C5" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>   
        </tr>     
  </table> 

  <table width="100%" align="center">
    <tr class="titulos2">
      <td>ÍTEM</td>
      <td colspan="2" class="tituloLicc">1 - ADMISIÓN A CIRUGÍA AMBULATORIA (PERSONAL DE ENFERMERIA VERIFICA):
      </td>
	  <td colspan="2">OBSERVACIÓN
      </td>	  
    </tr> 
    <tr class="estiloImput">
      <td width="10%" class="estiloImputIzq2">AUXILIAR</td>
      <td width="75%" class="estiloImputDer">PACIENTE LLEGA 30 MINUTOS ANTES DE LA HORA DE CIRUGÍA:</td> 
      <td width="5%" class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C6" style="width:90%;" title="32"   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
            <option value=""></option>
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C7" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>	 

    </tr>  

    <tr class="estiloImput">
       <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">SALUDA AL PACIENTE, LE DA LA BIENVENIDA Y REALIZA LA PRESENTACIÓN CORRESPONDIENTE:</td> 
      <td class="estiloImputIzq2">
         <select id="txt_LICC_C8" style="width:90%;" title="32"   >    
            <option value="SI" selected>SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option><option value=""></option>
         </select>
      </td>
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C9" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>  
    </tr> 


    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL NOMBRE Y APELLIDO DEL PACIENTE CORRESPONDE CON LA PROGRAMACIÓN QUIRÚRGICA Y CON EL PACIENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C10" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""> </option>
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C11" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL NÚMERO DE IDENTIFICACIÓN DEL PACIENTE CORRESPONDE CON LA PROGRAMACIÓN QUIRÚRGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C12" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""> </option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C13" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>		
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL NOMBRE DEL PROCEDIMIENTO A REALIZAR CORRESPONDE CON LA PROGRAMACIÓN QUIRÚRGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C14" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
      <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C15" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td> 	  
    </tr>    

    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL SITIO QUIRÚRGICO CORRESPONDE CON LA PROGRAMACIÓN QUIRÚRGICA Y A LA HISTORIA CLINICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C16" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>      
         </select>
      </td>
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C17" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr>  

    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL SITIO QUIRÚRGICO SE MARCA DE ACUERDO CON LA PROGRAMACIÓN QUIRÚRGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C18" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>			
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C19" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>    

    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">HISTORIA CLINICA COMPLETA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C20" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>      
         </select>
      </td>
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C21" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr>  


    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">CONFIRMAR AYUNO (CUANDO CORRESPONDA SEGÚN GUÍA DE RECOMENDACIONES PREQUIRÚRGICAS) ESCRIBA LA ULTIMA HORA DE INGESTA DE ALIMENTO O BEBIDA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C22" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>       
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C23" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>      
    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL PACIENTE SE PRESENTA CON OJOS SIN MAQUILLAJE (VERIFICAR AUSENCIA DE PESTAÑINA):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C24" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td  class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C25" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL PACIENTE SE PRESENTA CON UÑAS CORTAS, LIMPIAS Y SIN ESMALTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C26" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td  class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C27" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>     
    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL PACIENTE SE PRESENTA SIN OBJETOS DE VANIDAD, ARETES, ANILLOS, ETC:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C28" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C29" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 

    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL PACIENTE TIENE DESEO DE HACER DEPOSICIÓN:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C30" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>
    <td  class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C31" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr> 


    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImput">NOMBRE DE LOS MEDICAMENTOS QUE ESTÁ INGIRIENDO EL PACIENTE (INDAGAR SOBRE MEDICAMENTOS RETROVIRALES):

        
      </td> 

      <td colspan="2">
          <textarea type="text" id="txt_LICC_C32"  size="4000"  maxlength="4000" style="width:90%"    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();" onkeypress="return validarKey(event,this.id)" > </textarea>
          </td>
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL PACIENTE HA INGERIDO MEDICAMENTOS ESTIPULADOS EN LA GUÍA DE RECOMENDACIONES PRE QUIRÚRGICAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C33" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C34" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">EL PACIENTE ES CONSUMIDOR CRÓNICO DE ALCOHOL, CIGARRILLO O SUSTANCIAS PSICOACTIVAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C35" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C36" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>	
    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">MANILLA DE IDENTIFICACIÓN CON NOMBRE Y APELLIDOS COMPLETOS, DOCUMENTO DE IDENTIFICACIÓN Y NOMBRE DEL PROCEDIMIENTO, SITIO QUIRÚRGICO, SE INDAGA E IDENTIFICAN LOS RIESGOS DEL PACIENTE (ALERGIA, POLIMEDICACIÓN Y/O CAIDAS):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C37" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td  class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C38" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  

    <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">BRINDA APOYO PSICOLÓGICO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C39" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>   
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C40" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr> 


     <tr class="estiloImput">
      <td class="estiloImputIzq2">AUXILIAR</td>
      <td class="estiloImputDer">PACIENTE PRESENTA GRIPA O TOS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C120" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>   
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C121" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr> 


    <tr class="titulos2">
      <td>ÍTEM</td>
      <td colspan="2" class="tituloLicc">2 - INGRESO A CIRUGÍA AMBULATORIA (CIRCULANTE VERIFICA):
      </td>  
	<td colspan="2">OBSERVACIÓN
      </td>		  
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">LA VESTIMENTA DEL PACIENTE ES ADECUADA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C41" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C42" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>          
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">VERIFICA MANILLA DE IDENTIFICACIÓN DEL PACIENTE E INFORMA AL EQUIPO QUIRÚRGICO LAS ALERTAS Y RIESGOS IDENTIFICADOS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C43" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C44" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">CONFIRMACION DEL NOMBRE E IDENTIFICACIÓN DEL PACIENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C45" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C46" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">PROCEDIMIENTO QUIRÚRGICO CONFIRMADO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C47" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C48" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>   
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">CONSENTIMIENTO INFORMADO FIRMADO POR EL PACIENTE Y MÉDICO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C49" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C50" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>   
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">MARCACIÓN DE SITIO OPERATORIO CORRECTO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C51" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C52" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
          
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">CAMILLAS CON BARANDA ARRIBA Y CON FRENO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C53" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C54" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">ACCESO VENOSO PERMEABLE (CUANDO CORRESPONDA):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C55" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C56" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">TOMA Y REGISTROS DE SIGNOS VITALES:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C57" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C58" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>    
<tr class="estiloImput">
  <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">CUENTA CON EXÁMENES PREQUIRÚRGICOS E IMÁGENES O AYUDAS DIAGNOSTICAS ESENCIALES:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C59" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>
    <td  class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C60" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
    </td>
    </tr>   
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">SE REALIZÓ ADMINISTRACIÓN DE PROFILAXIS ANTIBIÓTICA EN LOS ÚLTIMOS 60 MINUTOS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C61" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C62" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
    </td>
    </tr>   
   <tr class="titulos2">
    <td>ÍTEM</td>
      <td colspan="2" class="tituloLicc">3 - ANTES DE LA INDUCCIÓN DE LA ANESTESIA O BLOQUEO ANESTÉSICO (ANESTESIOLOGO, CIRUJANO, CIRCULANTE VERIFICAN):</td>   
	<td colspan="2">OBSERVACIÓN
      </td>		  
    </tr>                         
        
<tr class="titulos2">
  <td></td>
      <td colspan="2">CIRCULANTE CORROBORA EN VOZ ALTA Y CLARA CON ANESTESIOLOGO:  
      </td>   
  <td colspan="2"></td>     
    </tr>   

   <tr class="estiloImput">
      <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">SE HA COMPLETADO LA COMPROBACIÓN DE LOS APARATOS DE ANESTESIA Y LA MEDICACIÓN ANESTÉSICA?:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C63" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>  
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C64" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr> 
         
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">CIRCUITOS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C65" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C66" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">MEDICAMENTOS DE LOS PACIENTES COMPLETOS Y DISPONIBLES EN LA SALA DE CIRURGIA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C67" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C68" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">REGISTRO ANESTÉSICO DEL PACIENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C69" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C70" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">EQUIPO DE INTUBACIÓN:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C71" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C72" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">EQUIPO PARA ASPIRACIÓN DE VÍA AÉREA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C73" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option> 
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C74" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">SISTEMA DE VENTILACIÓN:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C75" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C76" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">PREMEDICACIÓN ANESTÉSICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C77" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>     
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C78" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">EQUIPO DE SUCCIÓN:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C79" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>    
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C80" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">SE HA COLOCADO EL PULSOXIMETRO AL PACIENTE Y FUNCIONA?:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C81" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>     
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C82" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRUJANO</td>
      <td class="estiloImputDer">ALERGIAS CONOCIDAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C83" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>	
			<option value=""></option>	
         </select>
      </td>    
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C84" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO - CIRCULANTE</td>
      <td class="estiloImputDer">VÍA AÉREA DIFÍCIL / RIESGO DE ASPIRACIÓN:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C85" style="width:90%;" title="32" onblur="guardarContenidoDocumento()"  >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C86" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();" />
	  </td>
    </tr>
	<tr class="titulos2">
    <td>ÍTEM</td>
      <td colspan="2" class="tituloLicc">4 - ANTES DE LA INCISIÓN
      </td>  
	<td colspan="2">OBSERVACIÓN
      </td>		  
    </tr>                         
    <tr class="estiloImput">
      <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR - CIRUJANO - ANESTESIÓLOGO</td>
      <td class="estiloImputDer">TODOS LOS MIEMBROS DEL EQUIPO ESTÁN PRESENTES:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C87" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C88" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR - CIRUJANO - ANESTESIÓLOGO</td>
      <td class="estiloImputDer">CIRUJANO, INSTRUMENTADOR, ANESTESIÓLOGO Y CIRCULANTE SE PRESENTAN AL PACIENTE CON NOMBRES, APELLIDOS Y CARGOS: 
      </td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C89" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>                                                    
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C90" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>


<tr class="estiloImput">
  <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR - CIRUJANO - ANESTESIÓLOGO</td>
      <td class="estiloImputDer">CIRUJANO, INSTRUMENTADOR, ANESTESIÓLOGO Y CIRCULANTE CONFIRMAN CON EL PACIENTE: SU NOMBRE, SITIO
OPERATORIO DEMARCADO, PROCEDIMIENTO QUIRÚRGICO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C91" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>                                                    
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C92" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr>

<tr class="estiloImput">
  <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR</td>
      <td class="estiloImputDer">CIRCULANTE CORROBORA EN VOZ ALTA Y CLARA CON INSTRUMENTADOR: EL INSTRUMENTADOR VERIFICA QUE TODO EL INSTRUMENTAL Y LOS EQUIPOS A UTILIZAR SE ENCUENTREN FUNCIONANDO CORRECTAMENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C93" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>                                                    
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C94" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
    </td>
    </tr>




	<tr class="titulos2">
    <td></td>
      <td colspan="2">PREVISION DE EVENTOS CRITICOS
      </td> 
	  <td colspan="2">OBSERVACIÓN
      </td>		  
    </tr> 

	<tr class="estiloImput">
     <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR - CIRUJANO - ANESTESIÓLOGO</td>
      <td class="estiloImputDer">EL EQUIPO CONOCE LAS PROBABLES COMPLICACIONES INTRAOPERATORIAS DEL PROCEDIMIENTO?:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C95" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C96" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRUJANO</td>
      <td class="estiloImputDer">EL CIRUJANO REVISA: LOS PASOS CRÍTICOS O IMPREVISTOS, LA DURACIÓN DE LA OPERACIÓN Y LA PÉRDIDA DE SANGRE PREVISTA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C97" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C98" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 

<tr class="estiloImput">
    <td class="estiloImputIzq2">ANESTESIÓLOGO</td>
      <td class="estiloImputDer">EL EQUIPO DE ANESTESIA REVISA: SI EL PACIENTE PRESENTA ALGÚN PROBLEMA ESPECÍFICO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C99" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>  
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C100" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
    </td>
    </tr> 



<tr class="titulos2">
  <td></td>
      <td colspan="2">EQUIPO DE ENFERMERÍA E INSTRUMENTADORA
      </td>   
    <td colspan="2">OBSERVACIÓN
      </td>     
    </tr> 


  <tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR</td>
      <td class="estiloImputDer">CIRCULANTE CORROBORA EN VOZ ALTA Y CLARA CON INSTRUMENTADOR: EL INSTRUMENTADOR CONFIRMA EL ESTADO DE ESTERILIZACIÓN DE PAQUETES QUIRÚRGICOS Y DISPOSITIVOS MÉDICOS (CON RESULTADOS DE LOS INDICADORES):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C101" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td>   
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C102" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
    </td>
    </tr>


<tr class="estiloImput">
  <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR</td>
      <td class="estiloImputDer">SE RESOLVIERON LAS INQUIETUDES REFERENTES AL INSTRUMENTAL O INSUMOS A UTILIZAR EN EL PROCEDIMIENTO?:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C103" style="width:90%;" title="32"   >   
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
      <option value=""></option>  
         </select>
      </td> 
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C104" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
    </td>
    </tr> 



	<tr class="titulos2">
    <td>ÍTEM</td>
      <td colspan="2" class="tituloLicc">5 - DURANTE EL PROCEDIMIENTO
      </td>      
	  <td colspan="2">OBSERVACIÓN
      </td>	
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">EL CIRCULANTE RECIBE, REALIZA EMBALAJE DE MUESTRAS PARA PATOLOGÍA (CUANDO CORRESPONDA):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C105" style="width:90%;" title="32"    >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C106" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE</td>
      <td class="estiloImputDer">ETIQUETADO DE LAS MUESTRAS (LECTURA DE LA ETIQUETA EN VOZ ALTA, INCLUIDO EL NOMBRE DEL PACIENTE):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C107" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C108" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();"/>
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR</td>
      <td class="estiloImputDer">EL INSTRUMENTADOR CONFIRMA VERBALMENTE RECUENTO DE MATERIALES E INSTRUMENTAL:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C109" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C110" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();" />
	  </td>
    </tr>

	<tr class="titulos2">
    <td>ÍTEM</td>
      <td colspan="2" class="tituloLicc">6 - ANTES DE QUE EL PACIENTE SALGA A RECUPERACION
      </td>
	  <td colspan="2">OBSERVACIÓN
      </td>	  
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE - INSTRUMENTADOR - CIRUJANO - ANESTESIÓLOGO</td>
      <td class="estiloImputDer">ANTES DE QUE EL PACIENTE SALGA DE LA SALA DE CIRUGÍA, CIRUJANO, INSTRUMENTADOR, ANESTESIÓLOGO Y CIRCULANTE CONFIRMAN PROCEDIMIENTO REALIZADO Y SITIO QUIRÚRGICO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C111" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C112" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); " />
	  </td>
    </tr> 
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE - ANESTESIÓLOGO - CIRUJANO</td>
      <td class="estiloImputDer">SE IMPARTIÓ RECOMENDACIONES PARA EL PERIODO DE RECUPERACIÓN Y/O TRATAMIENTO DEL PACIENTE POR PARTE DE ANESTESIA O CIRUGÍA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C113" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option> 
			<option value=""></option>	
         </select>
      </td>
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C114" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); "/>
	  </td>
    </tr>
	<tr class="estiloImput">
    <td class="estiloImputIzq2">CIRCULANTE - ANESTESIÓLOGO - CIRUJANO</td>
      <td class="estiloImputDer">SE VERIFICA QUE LOS REGISTROS DE ANESTESIA Y DESCRIPCIÓN QUIRÚRGICA ESTEN DILIGENCIADOS EN SU TOTALIDAD:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C115" style="width:90%;" title="32"   >	  
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td class="estiloImputIzq2">
			<input type="text" id="txt_LICC_C116" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();"/>
	  </td>
    </tr> 

<tr class="estiloImput">
  <td class="estiloImputIzq2">INSTRUMENTADOR</td>
      <td class="estiloImputDer">SE RESUELVE PROBLEMAS RELACIONADOS CON INSTRUMENTAL Y EQUIPOS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICC_C117" style="width:90%;" title="32"   >    
            <option value="SI" selected="selected">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
            <option value=""> </option>  
         </select>
      </td> 
    <td class="estiloImputIzq2">
      <input type="text" id="txt_LICC_C118" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();"/>
    </td>
    </tr> 

  </table>  
   
 </td>   
</tr>   
	<tr class="titulos2" colspan="3">
      <td width="100%">Enfermeria Observacion:</td>	  
    </tr> 
    <tr class="estiloImput" colspan="3">
      <td class="estiloImput" width="100%">
        <textarea type="text" id="txt_LICC_C119"   maxlength="4000" style="width:50%"    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"  onkeypress="return validarKey(event,this.id)"> </textarea>
      </td> 
    </tr>   
  
    <tr>
    	<td colspan="5" align="right">
        	<input type="button" onClick="guardarContenidoDocumento()" value="Guardar" title="btn544" class="small button blue" id="btnGuardarDocumento">                           
        </td>
    </tr>
</table>
<table width="100%">  
  <tr>  
     <td width="100%">    
       <input type="button" onclick="cerrarDocumentClinicoSinImp()" value="FINALIZAR SIN IMPRIMIR" title="btn587" class="small button blue" id="btnFinalizarDocumento_">        
     </td>                                                       
  </tr>   
</table>   
<!--
<table width="100%">
           <tr class="estiloImput"> 
              <td colspan="1" align="CENTER">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76"  >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                  </select>              
              </td>                   
              <td colspan="1" align="CENTER">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"   >	
                          <option value="1">NINGUNA</option>
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>    
              <td colspan="1" align="CENTER">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO AL FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr>  
</table>
-->