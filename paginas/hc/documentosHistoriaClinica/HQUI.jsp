
<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<% 	
   ArrayList resultaux = new ArrayList();

   ControlAdmin beanAdmin = new ControlAdmin();
       ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
       Conexion cn = new Conexion(iConnection.getConnection());
       beanAdmin.setCn(cn);
%>
<div id="plantilla_HQUI">
  <table width="100%" align="center">
    <tr class="estiloImput">
      <td width="20%" class="estiloImputDer">Complicaciones:
        <select size="1" id="txt_HQUI_C1" name="complicacion_preg" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"
          >
          <option value="SI">SI</option>
          <option value="NO">NO</option>
          <option value="NA">NA</option>
        </select>
      </td>
      <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C2" name="complicacion_observ" maxlength="400" style="width:80%"
          onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /></td>
    </tr>
    <tr class="estiloImput">
      <td width="20%" class="estiloImputDer">Evento Adverso:
        <select size="1" id="txt_HQUI_C3" name="evento_preg" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"
          >
          <option value="SI">SI</option>
          <option value="NO">NO</option>
          <option value="NA">NA</option>
        </select>
      </td>
      <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C4" name="evento_observ" maxlength="400" style="width:80%"
          onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /></td>
    </tr>
  
    <tr class="estiloImput">
      <td width="20%" class="estiloImputDer">Diagnosticos Pos-Operatorio:</td>
      <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C5" name="pos_ope" maxlength="400" style="width:80%"
          onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" />
  
      </td>
    </tr>
  
  
    <tr class="estiloImput">
      <td width="20%" class="estiloImputDer">MUESTRAS ENVIADAS A LABORATORIO:</td>
      <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C7" name="tejidos_enviados" maxlength="400" style="width:80%"
          onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /></td>
    </tr>
    <tr class="estiloImput">
      <td width="20%" class="estiloImputDer"> TEJIDOS ENVIADOS A PATOLOGIA:</td>
      <td width="80%" class="estiloImputIzq2"><input type="text" id="txt_HQUI_C8" name="patologia" maxlength="400" style="width:80%"
          onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /></td>
    </tr>
  
    <tr class="estiloImput">
      <td width="20%" class="estiloImputDer">TECNICA USADA PARA: </td>
      <td width="80%" class="estiloImputIzq2">
        <select size="1" id="txt_HQUI_C6" name="tec_usada" style="width:90%;"
          onchange="cargarDescripcionHQUI(this.value)" onblur="guardarContenidoDocumento();" >
          <option value='0'> </option>
          <% resultaux.clear(); resultaux=(ArrayList)beanAdmin.combo.cargar(578); ComboVO cmbBod; for(int
            k=0;k<resultaux.size();k++){ cmbBod=(ComboVO)resultaux.get(k); %>
            <option id='cmbCom' value="<%= cmbBod.getId()%>" title="<%= cmbBod.getTitle()%>">
              <%=cmbBod.getDescripcion()%>
            </option>
            <%} %>
        </select>
  
      </td>
    </tr>
    <tr class="estiloImput">
      <td width="20%" colspan="2" class="titulosTodasMinuscu">
        DESCRIPCION DE LA INTERVENCION QUIRURGICA
        <p>
          Describa la operaci&oacute;n en el mismo orden en que se realiz&oacute; anotando hallazgos operatorios y
          procedimientos.
  
      </td>
    </tr>
    <tr class="estiloImput">
      <td width="80%" colspan="2">
        <textarea type="text" rows="18" id="txt_HQUI_C9" name="desc_intervencion_qx" size="4000" maxlength="4000" style="width:95%; height: 1%;"
           onblur="v28(this.value,this.id);  guardarContenidoDocumento()"
          onkeypress="return validarKey(event,this.id)"> </textarea>
      </td>
    </tr>
  
  </table>
</div>