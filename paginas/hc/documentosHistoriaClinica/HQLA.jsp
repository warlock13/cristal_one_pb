<div id="plantilla_HQLA">
  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr class="camposRepInp">
      <td width="100%" colspan="2">DESCRIPCION DE PROCEDIMIENTO</td>
    </tr>
    <tr class="estiloImputIzq2">
      <td width="10%" style="padding-left:10px">A.</td>
      <td width="90%">
        <select id="txt_HQLA_C1" name="desc_procedimiento" style="width:30%"  onblur="guardarContenidoDocumento()">
          <option value="LASER">LASER</option>
          <option value="YAG">YAG</option>
          <option value="DIODO ARGOM">DIODO ARGOM</option>
        </select>
      </td>
    </tr>
    <tr class="estiloImputIzq2">
      <td width="10%" style="padding-left:10px">B. Poder: </td>
      <td width="90%">
        O.D.<input type="text" id="txt_HQLA_C2" name="poder_od" size="" style="width:10%" value="" 
          onblur="guardarContenidoDocumento()" />
        O.I.<input type="text" id="txt_HQLA_C3" name="poder_oi" size="" style="width:10%" value="" 
          onblur="guardarContenidoDocumento()" />
      </td>
    </tr>
    <tr class="estiloImputIzq2">
      <td width="10%" style="padding-left:10px">C. Tiempo: </td>
      <td width="90%">
        O.D.<input type="text" id="txt_HQLA_C4" name="tiempo_od" style="width:10%" value="" 
          onblur="guardarContenidoDocumento()" />
        O.I.<input type="text" id="txt_HQLA_C5" name="tiempo_oi" style="width:10%" value="" 
          onblur="guardarContenidoDocumento()" />
      </td>
    </tr>
    <tr class="estiloImputIzq2">
      <td width="15%" style="padding-left:10px">D. Numero de disparos: </td>
      <td width="90%">
        O.D.<input type="text" id="txt_HQLA_C6" name="n_disparos_od" style="width:10%" value="" 
          onblur="guardarContenidoDocumento()" />
        O.I.<input type="text" id="txt_HQLA_C7" name="n_disparos_oi" style="width:10%" value="" 
          onblur="guardarContenidoDocumento()" />
      </td>
    </tr>
    <tr class="estiloImputIzq2">
      <td width="10%" style="padding-left:10px">Observaciones: </td>
      <td width="90%">
        <textarea rows="5" id="txt_HQLA_C8" name="observacion" size="4000" maxlength="4000" style="width:95%; border:#06C solid 1px"
           onblur="v28(this.value,this.id); guardarContenidoDocumento()"
          onkeypress="return validarKey(event,this.id)"> </textarea>
      </td>
    </tr>
  </table>
</div>