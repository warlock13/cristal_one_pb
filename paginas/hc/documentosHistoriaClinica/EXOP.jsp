<div id="plantilla_EXOP">
  <table width="100%" align="center">
    <tr>
      <td width="50%">&nbsp;

      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="20%">LENSOMETRIA</td>
            <td width="30%">Valor</td>
            <td width="50%">Adici&oacute;n</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOP_C1" name="lensom_od_valor" size="100" maxlength="25"
                style="width:90%" /></td>
            <td>
              <input type="text" id="txt_EXOP_C2" name="lensom_od_adicion" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()" />
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOP_C3" name="lensom_oi_valor" size="100" maxlength="25" style="width:90%"
                onblur="guardarContenidoDocumento()" /></td>
            <td>
              <input type="text" id="txt_EXOP_C4" name="lensom_oi_adicion" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()" />
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Observaciones</td>
            <td colspan="2"><input type="text" id="txt_EXOP_C5" name="lensom_observaciones" size="100" maxlength="10000"
                style="width:96%" onblur="guardarContenidoDocumento()" /></td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td width="25%">AGUDEZA VISUAL Sin Correcci&oacute;n</td>
            <td width="37%">Visi&oacute;n Lejana</td>
            <td width="37%">Visi&oacute;n pr&oacute;xima</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td>
              <select size="1" id="txt_EXOP_C6" name="avsc_od_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/125">20/125</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/160">20/160</option>
                <option value="20/200">20/200</option>
                <option value="20/250">20/250</option>
                <option value="20/300">20/300</option>
                <option value="20/320">20/320</option>
                <option value="20/400">20/400</option>
                <option value="20/500">20/500</option>
                <option value="20/640">20/640</option>
                <option value="20/800">20/800</option>
                <option value="20/1000">20/1000</option>
                <option value="20/1280">20/1280</option>
                <option value="20/1600">20/1600</option>
                <option value="20/2000">20/2000</option>
                <option value="20/2480">20/2480</option>
                <option value="20/2560">20/2560</option>
                <option value="20/3200">20/3200</option>
                <option value="Cuenta dedos">Cuenta dedos</option>
                <option value="Movimiento de manos">Movimiento de manos</option>
                <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                <option value="No percibe luz">No percibe luz</option>
                <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
              <input type="text" value="" id="txt_EXOP_C58" name="avsc_od_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <select size="1" id="txt_EXOP_C7" name="avsc_od_vision_proxima" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/125">20/125</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/160">20/160</option>
                <option value="20/200">20/200</option>
                <option value="20/250">20/250</option>
                <option value="20/300">20/300</option>
                <option value="20/320">20/320</option>
                <option value="20/400">20/400</option>
                <option value="20/500">20/500</option>
                <option value="20/640">20/640</option>
                <option value="20/800">20/800</option>
                <option value="20/1000">20/1000</option>
                <option value="20/1280">20/1280</option>
                <option value="20/1600">20/1600</option>
                <option value="20/2000">20/2000</option>
                <option value="20/2480">20/2480</option>
                <option value="20/2560">20/2560</option>
                <option value="20/3200">20/3200</option>
                <option value="Cuenta dedos">Cuenta dedos</option>
                <option value="Movimiento de manos">Movimiento de manos</option>
                <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                <option value="No percibe luz">No percibe luz</option>
                <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
              <input type="text" value="" id="txt_EXOP_C59" name="avsc_od_proxima_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td>
              <select size="1" id="txt_EXOP_C8" name="avsc_oi_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <<option value="">
                  </option>
                  <option value="20/20">20/20</option>
                  <option value="20/25">20/25</option>
                  <option value="20/30">20/30</option>
                  <option value="20/40">20/40</option>
                  <option value="20/50">20/50</option>
                  <option value="20/60">20/60</option>
                  <option value="20/70">20/70</option>
                  <option value="20/80">20/80</option>
                  <option value="20/100">20/100</option>
                  <option value="20/125">20/125</option>
                  <option value="20/140">20/140</option>
                  <option value="20/150">20/150</option>
                  <option value="20/160">20/160</option>
                  <option value="20/200">20/200</option>
                  <option value="20/250">20/250</option>
                  <option value="20/300">20/300</option>
                  <option value="20/320">20/320</option>
                  <option value="20/400">20/400</option>
                  <option value="20/500">20/500</option>
                  <option value="20/640">20/640</option>
                  <option value="20/800">20/800</option>
                  <option value="20/1000">20/1000</option>
                  <option value="20/1280">20/1280</option>
                  <option value="20/1600">20/1600</option>
                  <option value="20/2000">20/2000</option>
                  <option value="20/2480">20/2480</option>
                  <option value="20/2560">20/2560</option>
                  <option value="20/3200">20/3200</option>
                  <option value="Cuenta dedos">Cuenta dedos</option>
                  <option value="Movimiento de manos">Movimiento de manos</option>
                  <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                  <option value="No percibe luz">No percibe luz</option>
                  <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                  <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
              <input type="text" value="" id="txt_EXOP_C60" name="avsc_oi_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <select size="1" id="txt_EXOP_C9" name="avsc_oi_vision_proxima" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/125">20/125</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/160">20/160</option>
                <option value="20/200">20/200</option>
                <option value="20/250">20/250</option>
                <option value="20/300">20/300</option>
                <option value="20/320">20/320</option>
                <option value="20/400">20/400</option>
                <option value="20/500">20/500</option>
                <option value="20/640">20/640</option>
                <option value="20/800">20/800</option>
                <option value="20/1000">20/1000</option>
                <option value="20/1280">20/1280</option>
                <option value="20/1600">20/1600</option>
                <option value="20/2000">20/2000</option>
                <option value="20/2480">20/2480</option>
                <option value="20/2560">20/2560</option>
                <option value="20/3200">20/3200</option>
                <option value="Cuenta dedos">Cuenta dedos</option>
                <option value="Movimiento de manos">Movimiento de manos</option>
                <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                <option value="No percibe luz">No percibe luz</option>
                <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
              <input type="text" value="" id="txt_EXOP_C61" name="avsc_oi_proxima_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Pinhole Ojo derecho</td>
            <td colspan="2">
              <input type="text" id="txt_EXOP_C10" name="pinhole_od" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()"  />
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Pinhole Ojo Izquierdo</td>
            <td colspan="2">
              <input type="text" id="txt_EXOP_C11" name="pinhole_oi" size="100" style="width:50%"
                onblur="guardarContenidoDocumento()"  />

            </td>
        </table>
      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="25%">AGUDEZA VISUAL Con Correcci&oacute;n</td>
            <td width="37%">Visi&oacute;n Lejana</td>
            <td width="37%">Visi&oacute;n pr&oacute;xima</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td>
              <select size="1" id="txt_EXOP_C12" name="avcc_od_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/125">20/125</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/160">20/160</option>
                <option value="20/200">20/200</option>
                <option value="20/250">20/250</option>
                <option value="20/300">20/300</option>
                <option value="20/320">20/320</option>
                <option value="20/400">20/400</option>
                <option value="20/500">20/500</option>
                <option value="20/640">20/640</option>
                <option value="20/800">20/800</option>
                <option value="20/1000">20/1000</option>
                <option value="20/1280">20/1280</option>
                <option value="20/1600">20/1600</option>
                <option value="20/2000">20/2000</option>
                <option value="20/2480">20/2480</option>
                <option value="20/2560">20/2560</option>
                <option value="20/3200">20/3200</option>
                <option value="Cuenta dedos">Cuenta dedos</option>
                <option value="Movimiento de manos">Movimiento de manos</option>
                <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                <option value="No percibe luz">No percibe luz</option>
                <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
              <input type="text" value="" id="txt_EXOP_C62" name="avcc_od_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <input type="text" value="" id="txt_EXOP_C13" name="avcc_od_vision_proxima" style="width:80%" size="20"
                maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td>
              <select size="1" id="txt_EXOP_C14" name="avcc_oi_vision_lejana" style="width:40%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/125">20/125</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/160">20/160</option>
                <option value="20/200">20/200</option>
                <option value="20/250">20/250</option>
                <option value="20/300">20/300</option>
                <option value="20/320">20/320</option>
                <option value="20/400">20/400</option>
                <option value="20/500">20/500</option>
                <option value="20/640">20/640</option>
                <option value="20/800">20/800</option>
                <option value="20/1000">20/1000</option>
                <option value="20/1280">20/1280</option>
                <option value="20/1600">20/1600</option>
                <option value="20/2000">20/2000</option>
                <option value="20/2480">20/2480</option>
                <option value="20/2560">20/2560</option>
                <option value="20/3200">20/3200</option>
                <option value="Cuenta dedos">Cuenta dedos</option>
                <option value="Movimiento de manos">Movimiento de manos</option>
                <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                <option value="No percibe luz">No percibe luz</option>
                <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
              <input type="text" value="" id="txt_EXOP_C63" name="avcc_oi_lejana_observacion" style="width:50%"
                size="20" maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
            <td>
              <input type="text" value="" id="txt_EXOP_C15" name="avcc_oi_vision_proxima" style="width:80%" size="20"
                maxlength="20" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
          <tr class="estiloImput">
            <td>...</td>
            <td colspan="2">
              ...
            </td>
          </tr>
          <tr class="estiloImput">
            <td>..</td>
            <td colspan="2">
              ..
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <table width="100%" border="1" cellpadding="1" cellspacing="1" align="center">
    <!-- <tr class="camposRepInp">
      <td width="50%" colspan="2">Examen externo Ojo Derecho</td>
      <td width="50%" colspan="2">Examen externo Ojo Izquierdo</td>
    <tr> -->
    <!-- <tr class="estiloImput">
      <td colspan="2">
        <textarea type="text" id="txt_EXOP_C16" name="examen_externo_od" size="1000" maxlength="4000" style="width:95%" 
          onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,'txt_EXOP_C16')"
          onkeyup="this.value=this.value.toUpperCase()"> </textarea>
      </td>
      <td colspan="2">
        <textarea type="text" id="txt_EXOP_C17" name="examen_externo_oi" size="4000" maxlength="4000" style="width:95%" 
          onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,'txt_EXOP_C17')"
          onkeyup="this.value=this.value.toUpperCase()"> </textarea>
      </td>
  
    </tr> -->
    <tr class="camposRepInp">
      <td width="20%">Reflejos pupilares</td>
      <td width="25%">Cover test</td>
      <td width="25%">Punto Pr&oacute;ximo de Convergencia</td>
      <td width="30%">Ducciones</td>
    <tr>
    <tr class="estiloImput">
      <td>
        <input type="text" value="" id="txt_EXOP_C18" name="reflejos_pupilares" style="width:98%" size="20"
          maxlength="1000" onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        VL:<input type="text" value="" id="txt_EXOP_C19" name="cover_test_vl" style="width:40%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
        VP:<input type="text" value="" id="txt_EXOP_C20" name="cover_test_vp" style="width:40%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        Real:<input type="text" value="" id="txt_EXOP_C21" name="ppc_real" style="width:20%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
        Luz:<input type="text" value="" id="txt_EXOP_C22" name="ppc_luz" style="width:20%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
        FR:<input type="text" value="" id="txt_EXOP_C23" name="ppc_fr" style="width:20%" size="20" maxlength="20"
          onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        OD:<textarea type="text" id="txt_EXOP_C24" name="duccion_od" size="100" maxlength="100" style="width:97%"
           onblur="guardarContenidoDocumento()"> </textarea>
        OI:<textarea type="text" id="txt_EXOP_C25" name="duccion_oi" size="100" maxlength="100" style="width:97%"
           onblur="guardarContenidoDocumento()"> </textarea>
      </td>
    </tr>
    <tr class="camposRepInp">
      <td colspan="4">Versiones</td>
      <!-- <td>Oftalmoscopia Ojo Derecho</td>
      <td>Oftalmoscopia Ojo Izquierdo</td> -->
    <tr class="estiloImput">
      <td colspan="4">
        <input type="text" value="" id="txt_EXOP_C26" name="versiones" style="width:80%" size="1000" maxlength="50"
          onblur="guardarContenidoDocumento()" >
      </td>
      <td>
        <!-- <textarea type="text" id="txt_EXOP_C66" name="oftalmoscopia_od" size="1000" maxlength="2000" style="width:90%"
           onblur="guardarContenidoDocumento()"> </textarea> -->
      </td>
      <td>
        <!-- <textarea type="text" id="txt_EXOP_C67" name="oftalmoscopia_oi" size="1000" maxlength="2000" style="width:95%"
           onblur="guardarContenidoDocumento()"> </textarea> -->
      </td>
  </table>
  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td width="40%">REFRACCI&Oacute;N</td>
            <td width="30%">Valor</td>
            <td width="30%">AV</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOP_C68" name="refraccion_od_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <select size="1" id="txt_EXOP_C69" name="refraccion_od_av" style="width:99%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/125">20/125</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/160">20/160</option>
                <option value="20/200">20/200</option>
                <option value="20/250">20/250</option>
                <option value="20/300">20/300</option>
                <option value="20/320">20/320</option>
                <option value="20/400">20/400</option>
                <option value="20/500">20/500</option>
                <option value="20/640">20/640</option>
                <option value="20/800">20/800</option>
                <option value="20/1000">20/1000</option>
                <option value="20/1280">20/1280</option>
                <option value="20/1600">20/1600</option>
                <option value="20/2000">20/2000</option>
                <option value="20/2480">20/2480</option>
                <option value="20/2560">20/2560</option>
                <option value="20/3200">20/3200</option>
                <option value="Cuenta dedos">Cuenta dedos</option>
                <option value="Movimiento de manos">Movimiento de manos</option>
                <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                <option value="No percibe luz">No percibe luz</option>
                <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOP_C70" name="refraccion_oi_valor" maxlength="25" maxlength="25"
                style="width:70%" onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <select size="1" id="txt_EXOP_C71" name="refraccion_oi_av" style="width:99%;" title="32"
                onblur="guardarContenidoDocumento()" >
                <option value=""></option>
                <option value="20/20">20/20</option>
                <option value="20/25">20/25</option>
                <option value="20/30">20/30</option>
                <option value="20/40">20/40</option>
                <option value="20/50">20/50</option>
                <option value="20/60">20/60</option>
                <option value="20/70">20/70</option>
                <option value="20/80">20/80</option>
                <option value="20/100">20/100</option>
                <option value="20/125">20/125</option>
                <option value="20/140">20/140</option>
                <option value="20/150">20/150</option>
                <option value="20/160">20/160</option>
                <option value="20/200">20/200</option>
                <option value="20/250">20/250</option>
                <option value="20/300">20/300</option>
                <option value="20/320">20/320</option>
                <option value="20/400">20/400</option>
                <option value="20/500">20/500</option>
                <option value="20/640">20/640</option>
                <option value="20/800">20/800</option>
                <option value="20/1000">20/1000</option>
                <option value="20/1280">20/1280</option>
                <option value="20/1600">20/1600</option>
                <option value="20/2000">20/2000</option>
                <option value="20/2480">20/2480</option>
                <option value="20/2560">20/2560</option>
                <option value="20/3200">20/3200</option>
                <option value="Cuenta dedos">Cuenta dedos</option>
                <option value="Movimiento de manos">Movimiento de manos</option>
                <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz</option>
                <option value="No percibe luz">No percibe luz</option>
                <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                <option value="Riesgo no evaluado">Riesgo no evaluado</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="40%"> QUERATOMETRIA</td>
            <td width="30%">Valor</td>
            <td width="30%">&nbsp;</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOP_C72" name="queratometria_od_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>&nbsp;</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOP_C73" name="queratometria_oi_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos1">
            <td width="40%">SUBJETIVO</td>
            <td width="30%">Valor</td>
            <td width="30%">AV</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td><input type="text" id="txt_EXOP_C74" name="subjetivo_od_valor" maxlength="25" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <input type="text" id="txt_EXOP_C36" name="subjetivo_od_av" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />

            </td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td><input type="text" id="txt_EXOP_C37" name="subjetivo_oi_valor" style="width:70%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td>
              <input type="text" id="txt_EXOP_C38" name="subjetivo_oi_av" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Agudeza visual binocular</td>
            <td colspan="2">
              <input type="text" id="txt_EXOP_C65" name="av_binocular" size="50" maxlength="100" style="width:90%"
                onblur="guardarContenidoDocumento()" />
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Adici&oacute;n</td>
            <td colspan="2">
              <input type="text" value="" id="txt_EXOP_C39" name="subjetivo_adicion" style="width:50%" size="20"
                onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
          <tr class="estiloImput">
            <td>Distancia Pupilar</td>
            <td colspan="2"><input type="text" id="txt_EXOP_C40" name="subjetivo_distancia_pupilar" maxlength="40"
                style="width:40%" onblur="guardarContenidoDocumento()" /> </td>
          </tr>

        </table>
      </td>
      <td width="50%">
        <table width="100%" align="center">
          <tr class="titulos2">
            <td width="16%">RX FINAL</td>
            <td width="30%">Esfera&nbsp;&nbsp;&nbsp;&nbsp;Cilindro&nbsp;&nbsp;&nbsp;&nbsp;Eje&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td width="27%">Agudeza Visual Lejana</td>
            <td width="27%">Agudeza Visual Pr&oacute;xima</td>
          </tr>
          <tr class="estiloImput">
            <td>Ojo derecho</td>
            <td>
              <input type="text" id="txt_EXOP_C41" name="rx_od_esfera" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EXOP_C42" name="rx_od_cilindro" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EXOP_C43" name="rx_od_eje" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
            </td>
            <td>
              <input type="text" id="txt_EXOP_C44" name="rx_od_avl" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
            </td>
            <td>
              <input type="text" id="txt_EXOP_C45" name="rx_od_avp" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />
            </td>
          </tr>

          <tr class="estiloImput">
            <td>Ojo Izquierdo</td>
            <td>
              <input type="text" id="txt_EXOP_C46" name="rx_oi_esfera" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EXOP_C47" name="rx_oi_cilindro" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
              <input type="text" id="txt_EXOP_C48" name="rx_oi_eje" maxlength="25" style="width:25%"
                onblur="guardarContenidoDocumento()" /> &nbsp;&nbsp;
            </td>
            <td>
              <input type="text" id="txt_EXOP_C49" name="rx_oi_avl" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />

            </td>
            <td>
              <input type="text" id="txt_EXOP_C50" name="rx_oi_avp" size="100" style="width:99%"
                onblur="guardarContenidoDocumento()"  />

            </td>
          </tr>
          <tr class="estiloImput">
            <td>Adici&oacute;n</td>
            <td colspan="2">
              <input type="text" value="" id="txt_EXOP_C51" name="rx_adicion" style="width:50%" size="20"
                onblur="guardarContenidoDocumento()" >

            </td>
          </tr>
          <tr class="estiloImput">
            <td>Observaciones:</td>
            <td colspan="2">
              <input type="text" value="" id="txt_EXOP_C64" name="observacion" style="width:99%" size="20"
                maxlength="2000" onblur="guardarContenidoDocumento()" >
            </td>
          </tr>
        </table>
      </td>
    </tr>


    <tr>
      <td>
        <table class="tbPersonalizada" align="center" style="border-collapse: collapse; width: 100%; background:#fff;">
          <tr class="camposRepInp">
            <td colspan="4" style="text-align: center;">BIOMICROSCOPIA</td>
          </tr>
          <tr class="camposRepInp">
            <td align="right">SEGMENTO ANTERIOR</td>
            <td colspan="2" align="left">
              &nbsp;<select onchange="textoFondoOjo(this.value,'S','exop');" onblur="guardarContenidoDocumento()">
                <option value=""></option>
                <option value="1">OJO DERECHO - BLANCO</option>
                <option value="2">OJO DERECHO - NO VALORABLE</option>
                <option value="3">OJO IZQUIERDO - BLANCO</option>
                <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
                <option value="7">AMBOS OJOS - NO VALORABLES</option>
                <option value="5">LIMPIAR</option>
                <option value="6">VALORES NORMALES</option>
              </select>
            </td>
          </tr>
          <tr>
            <th></th>
            <th>OJO DERECHO</th>
            <th>OJO IZQUIERDO</th>
          </tr>
          <tr>
            <td>Parpados</td>
            <td>
              <textarea type="text" id="txt_EXOP_C27" name="parpados" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C84" name="parpados_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Aparato lagrimal</td>
            <td>
              <textarea type="text" id="txt_EXOP_C28" name="aparato_lagrimal" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C85" name="aparato_lagrimal_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Conjuntiva</td>
            <td>
              <textarea type="text" id="txt_EXOP_C29" name="conjuntiva" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C86" name="conjuntiva_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Cornea</td>
            <td>
              <textarea type="text" id="txt_EXOP_C30" name="cornea" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C87" name="cornea_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Esclerotica</td>
            <td>
              <textarea type="text" id="txt_EXOP_C31" name="esclerotica" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C88" name="esclerotica_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Camara Anterior</td>
            <td>
              <textarea type="text" id="txt_EXOP_C32" name="camara_anterior" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C89" name="camara_anterior_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>iris</td>
            <td>
              <textarea type="text" id="txt_EXOP_C33" name="iris" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C90" name="iris_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Cristalino</td>
            <td>
              <textarea type="text" id="txt_EXOP_C34" name="cristalino" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C91" name="cristalino_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Gonioscopia</td>
            <td>
              <textarea type="text" id="txt_EXOP_C35" name="gonioscopia" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C92" name="gonioscopia_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Schimmer</td>
            <td>
              <textarea type="text" id="txt_EXOP_C93" name="schimmer_d" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C94" name="schimmer_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>BUT</td>
            <td>
              <textarea type="text" id="txt_EXOP_C95" name="but_d" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C96" name="but_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>RB</td>
            <td>
              <textarea type="text" id="txt_EXOP_C97" name="rb_d" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C98" name="rb_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
        </table>
      </td>
      <td style="vertical-align: top;">

        <table id="idtboftalmoscopia" class="tbPersonalizada" align="center"
          style="border-collapse: collapse; width: 100%; background:#fff;">
          <tr class="camposRepInp">
            <td colspan="4" style="text-align: center;">OFTALMOSCOP&Iacute;A</td>
          </tr>
          <td align="left" style="height: 45px;" colspan="3">
            <!-- &nbsp;<select onchange="textoFondoOjo(this.value,'S','exop');" onblur="guardarContenidoDocumento()">
                    <option value=""></option>
                    <option value="1">OJO DERECHO - BLANCO</option>
                    <option value="2">OJO DERECHO - NO VALORABLE</option>
                    <option value="3">OJO IZQUIERDO - BLANCO</option>
                    <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
                    <option value="7">AMBOS OJOS - NO VALORABLES</option>
                    <option value="5">LIMPIAR</option>
                    <option value="6">VALORES NORMALES</option>
                  </select> -->
          </td>
          <tr>
            <th></th>
            <th>OJO DERECHO</th>
            <th>OJO IZQUIERDO</th>
          </tr>
          <tr>
            <td>Parpados</td>
            <td>
              <textarea type="text" id="txt_EXOP_C109" name="parpados_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C110" name="parpados_i_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Aparato lagrimal</td>
            <td>
              <textarea type="text" id="txt_EXOP_C111" name="aparato_lagrimal_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C112" name="aparato_lagrimal_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Conjuntiva</td>
            <td>
              <textarea type="text" id="txt_EXOP_C113" name="conjuntiva_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C114" name="conjuntiva_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Cornea</td>
            <td>
              <textarea type="text" id="txt_EXOP_C115" name="cornea_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C116" name="cornea_i_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Esclerotica</td>
            <td>
              <textarea type="text" id="txt_EXOP_C117" name="esclerotica_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C118" name="esclerotica_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Camara Anterior</td>
            <td>
              <textarea type="text" id="txt_EXOP_C119" name="camara_anterior_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C120" name="camara_anterior_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>iris</td>
            <td>
              <textarea type="text" id="txt_EXOP_C121" name="iris_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C122" name="iris_i_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Cristalino</td>
            <td>
              <textarea type="text" id="txt_EXOP_C123" name="cristalino_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C124" name="cristalino_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Gonioscopia</td>
            <td>
              <textarea type="text" id="txt_EXOP_C125" name="gonioscopia_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C126" name="gonioscopia_i_of" size="1000" rows="4" maxlength="1000"
                style="width:95%"  onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>Schimmer</td>
            <td>
              <textarea type="text" id="txt_EXOP_C127" name="schimmer_d_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C128" name="schimmer_i_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>BUT</td>
            <td>
              <textarea type="text" id="txt_EXOP_C129" name="but_d_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C130" name="but_i_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
          <tr>
            <td>RB</td>
            <td>
              <textarea type="text" id="txt_EXOP_C131" name="rb_d_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C132" name="rb_i_of" size="1000" rows="4" maxlength="1000" style="width:95%"
                 onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarKey_(event,this.id)">
                    </textarea>
            </td>
          </tr>
        </table>

      </td>
    </tr>
    <tr class="camposRepInp">
      <td width="50%" align="right">FONDO DE OJO &nbsp;</td>
      <td width="50%" align="left">
        &nbsp;<select onchange="textoFondoOjo(this.value,'F','exop')" onblur="guardarContenidoDocumento()">
          <option value=""></option>
          <option value="1">OJO DERECHO - BLANCO</option>
          <option value="2">OJO DERECHO - NO VALORABLE</option>
          <option value="3">OJO IZQUIERDO - BLANCO</option>
          <option value="4">OJO IZQUIERDO - NO VALORABLE</option>
          <option value="7">AMBOS OJOS - NO VALORABLES</option>
          <option value="5">LIMPIAR</option>
          <option value="6">VALORES NORMALES</option>
        </select>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table class="tbPersonalizada" align="center" style="border-collapse: collapse; width: 80%; background:#fff;">
          <tr>
            <th></th>
            <th>OJO DERECHO</th>
            <th>OJO IZQUIERDO</th>
          </tr>
          <tr>
            <td>Vitreo</td>
            <td>
              <textarea type="text" id="txt_EXOP_C99" name="vitreo" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C100" name="vitreo_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Papila (nervio &oacute;ptico)</td>
            <td>
              <textarea type="text" id="txt_EXOP_C101" name="papila" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C102" name="papila_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Macula</td>
            <td>
              <textarea type="text" id="txt_EXOP_C103" name="macula" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C104" name="macula_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Vasos</td>
            <td>
              <textarea type="text" id="txt_EXOP_C105" name="vasos" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C106" name="vasos_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
          <tr>
            <td>Retina</td>
            <td>
              <textarea type="text" id="txt_EXOP_C107" name="retina" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
            <td>
              <textarea type="text" id="txt_EXOP_C108" name="retina_i" size="1000" rows="4" maxlength="1000"
                style="width:95%" 
                onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"
                onkeypress="return validarkey_(event,this.id)">
              </textarea>
            </td>
          </tr>
        </table>
      </td>
    </tr>




    <tr class="estiloImput">
      <td width="100%" colspan="2">
        <TABLE width="100%">
          <tr class="estiloImput">
            <td colspan="5" bgcolor="#009999">&nbsp;</td>
          </tr>
          <tr class="titulos">
            <td colspan="2">Especificaciones del lente</td>
            <td>Filtro</td>
            <td>Color</td>
          </tr>
          <tr class="estiloImput">
            <td colspan="2"><input type="text" id="txt_EXOP_C52" name="especificaciones_lente" maxlength="500"
                style="width:90%" onblur="guardarContenidoDocumento()" /> </td>
            <td><input type="text" id="txt_EXOP_C53" name="filtro" maxlength="100" style="width:90%"
                onblur="guardarContenidoDocumento()" /> </td>
            <td><input type="text" id="txt_EXOP_C54" name="color" maxlength="100" style="width:90%"
                onblur="guardarContenidoDocumento()" /> </td>
          </tr>
          <tr class="titulos">
            <td>Uso</td>
            <td>control</td>
            <td colspan="2">Recomendaciones</td>
          </tr>
          <tr class="estiloImput">
            <td><input type="text" id="txt_EXOP_C55" name="uso" style="width:90%" maxlength="100"
                onblur="guardarContenidoDocumento()" /> </td>
            <td><input type="text" id="txt_EXOP_C56" name="control_opt" style="width:90%" maxlength="100"
                onblur="guardarContenidoDocumento()" /> </td>
            <td colspan="2"><input type="text" id="txt_EXOP_C57" name="recomendaciones" maxlength="500"
                style="width:90%" onblur="guardarContenidoDocumento()" /> </td>
          </tr>
          <tr>

          <tr>
        </TABLE>
      </td>
    </tr>

  </table>

</div>
