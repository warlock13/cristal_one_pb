
 <%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" pageEncoding="UTF-8" %>
 <%@ page import = "java.util.*,java.text.*,java.sql.*"%>
 <%@ page import = "Sgh.Utilidades.*" %>
 <%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
 <%@ page import = "Clinica.Presentacion.*" %>
 
<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
%>

<table width="100%"  align="center">
  <tr>
    <td >
      <table width="100%" align="center">
            <tr class="titulos"> 
              <td width="20%">EXAMEN CORRECTO</td>                               
              <td width="20%">SITIO DE EXAMEN CORRECTO</td>
              <td width="20%">DATOS DEL PACIENTE CORRECTOS</td>                
              <td width="20%">CANTIDAD DE EXAMENES CORRECTA</td>                
              <td width="20%">RIESGOS POR MEDICAMENTOS </td>                                                
            </tr>		
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_BIOM_C1" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_BIOM_C2" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_BIOM_C3" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_BIOM_C4" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>   
              <td>
                 <select size="1" id="txt_BIOM_C5" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="NA" selected="selected">NA</option>                    
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
            </tr>   
            
            <tr class="titulos"> 
              <td width="20%">PREPARACION PREVIA DEL PACIENTE</td>                               
              <td width="20%">DOCUMENTOS NECESARIOS</td>
              <td width="20%">CONSENTIMIENTO INFORMADO</td>                
              <td width="15%">No LENTE</td>                
              <td width="25%">OBSERVACIONES</td>                
            </tr>                                                                                     
  
            <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_BIOM_C6" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="SI">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_BIOM_C7" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_BIOM_C8" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   >	  
                    <option value="NA">NA</option>                    
                    <option value="SI" selected="selected">SI</option>
                    <option value="NO">NO</option>
                 </select>               
              </td>                
              <td ><input type="text" id="txt_BIOM_C10" maxlength="500" style="width:40%" onblur="guardarContenidoDocumento()" />
				<img src="/clinica/utilidades/imagenes/botones-estandar/Guardar.gif" title="Guardar Numero de Lente" width="20px" height="20px" onclick="modificarCRUD('guardarNoLente', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">
			  </td>
              <td >
				<!-- <input type="text" id="txt_BIOM_C9" maxlength="2000" style="width:96%" onblur="guardarContenidoDocumento()" /> -->
				<textarea id="txt_BIOM_C9" onblur="guardarContenidoDocumento()" rows="1">
				</textarea>
			  </td>
           </tr>  
           
           
		   <tr>    
              <td colspan="5">
                
                <TABLE width="100%">
                        <tr class="estiloImput"> 
              <td width="25%" align="center">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:60%" title="76"  onfocus="limpiarDivEditarJuan('limpiarMotivoFolioEdit');" >
                      <option value=""></option>                      
                          <option value="7">Cancelado Finalizado</option>    
                          <option value="10">Reprogramado Finalizado</option>
                          <option value="8">Finalizado Urgente</option>  
                          <option value="9">Finalizado Alerta</option>                                                     
                  </select>              
              </td>                   
              <td  width="25%" align="center">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"   onfocus="limpiaAtributo('cmbMotivoClaseFolioEdit',0)">  
                          <option value=""></option> 
                          <option value="26">ATRIBUIBLE AL PACIENTE</option>  
                          <option value="27">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>    


              <td width="25%" align="center">
                CLASIFICACION:
                    <select id="cmbMotivoClaseFolioEdit" style="width:60%"  onfocus="comboDependienteDosCondiciones('cmbMotivoClaseFolioEdit','cmbIdMotivoEstadoEdit','lblIdDocumento','565')">
                    <option value=""></option>                      
                </select> 
                </td>

              <td width="25%" align="center">
             <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">                                    
        
              </td> 
           </tr>  
                </TABLE>
      </td>
</tr>
           	
           
           <tr class="titulosCentrados"> 
              <td colspan="5" align="center">Espacio para Nota adicional
              </td>
           </tr>    
           
           <tr class="estiloImput"> 
              <td>
                 <input id="btn_limpia" title="BTUU8" class="small button blue" type="button" value="Traer"  onclick="traerTextoPredefinidoBiometria()">              
              </td>
              <td colspan="3" align="right">
                 <textarea type="text" id="txt_gestionarPaciente" rows="11"   maxlength="4000" style="width:95%"    onkeypress="return validarKey(event,this.id)" > </textarea>                                                
              </td>                   
              <td colspan="1" align="left">
	           <input id="btnGestionPaciente" class="small button blue" type="button" title="bt487" value="GESTIONAR" onclick="modificarCRUD('gestionPacienteFolio', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                                    
              </td>    
            </tr>              
            
      </table> 
    </td>   
  </tr>   
</table>  
