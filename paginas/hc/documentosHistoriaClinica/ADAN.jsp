
<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" pageEncoding="UTF-8" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->
<% 	
   ArrayList resultaux = new ArrayList();

   ControlAdmin beanAdmin = new ControlAdmin();
       ConnectionClinica iConnection = (ConnectionClinica) request.getAttribute("iConnection");
       Conexion cn = new Conexion(iConnection.getConnection());
       beanAdmin.setCn(cn);
%>

<div id="plantilla_ADAN">
<table width="100%"  align="center">
 <tr>
   <td >
     <table width="100%" align="center">
           <tr class="titulos"> 
             <td width="20%">EXAMEN CORRECTO</td>                               
             <td width="20%">SITIO DE EXAMEN CORRECTO</td>
             <td width="20%">DATOS DEL PACIENTE CORRECTOS</td>                
             <td width="20%">CANTIDAD DE EXAMENES CORRECTA</td>                
             <td width="20%">RIESGOS POR MEDICAMENTOS </td>                                                
           </tr>		
           <tr class="estiloImput"> 
             <td>
                <select size="1" id="txt_ADAN_C1" name="examen_correcto" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="SI">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C2" name="sitio_correcto" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="SI">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C3" name="datos_paciente_correcto" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="SI">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C4" name="cantidad_correcta" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="SI">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>   
             <td>
                <select size="1" id="txt_ADAN_C5" name="riesgo_medicamentos" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="NA" selected="selected">NA</option>                    
                   <option value="SI">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
           </tr>   
           
           <tr class="titulos"> 
             <td width="20%">PREPARACION PREVIA DEL PACIENTE</td>                               
             <td width="20%">DOCUMENTOS NECESARIOS</td>
             <td width="20%">CONSENTIMIENTO INFORMADO</td>                
             <td width="20%" colspan="2">FECHA ORDENADO EXAMEN</td>                

           </tr>                                                                                     
 
           <tr class="estiloImput"> 
             <td>
                <select size="1" id="txt_ADAN_C6"  name="preparacion_previa" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="SI">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C7"  name="documentos_necesarios" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="NA">NA</option>                    
                   <option value="SI" selected="selected">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C8"  name="consentimiento_informado" style="width:20%;"  title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="NA">NA</option>                    
                   <option value="SI" selected="selected">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td colspan="2">
                <input id="txt_ADAN_C18"   name="fecha_ordenado" type="text"  size="10"  maxlength="10"  onblur="guardarContenidoDocumentoAYD()"  />
             </td>
          </tr>   

     <!-- nuevos campos -->
     <tr class="titulos"> 
             <td width="20%">SE REVISÓ EL EQUIPO Y FUNCIONA CORRECTAMENTE</td>                               
             <td width="20%">PACIENTE DIABETICO ¿SUSPENDIÓ LA ADMINISTRACIÓN DEL MEDICAMENTO?</td>
             <td width="20%">PACIENTE HIPERTENSO ¿ADMINISTRA EL MEDICAMENTO NORMALMENTE?</td>                
             <td width="20%" >PACIENTE ASISTE ACOMPAÑADO</td>                
             <td width="20%" >&nbsp;</td>                

           </tr> 
        
        <tr class="estiloImput"> 
             <td>
                <select size="1" id="txt_ADAN_C19" name="equipo_funciona" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="SI">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C20"  name="paciente_diabetico" style="width:20%;" title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="NA">NA</option>                    
                   <option value="SI" >SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C21"   name="paciente_hipertenso" style="width:20%;"  title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="NA">NA</option>                    
                   <option value="SI" >SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td>                
             <td>
                <select size="1" id="txt_ADAN_C22"   name="paciente_acompanado" style="width:20%;"  title="32" onblur="guardarContenidoDocumentoAYD()"   >	  
                   <option value="NA">NA</option>                    
                   <option value="SI" selected="selected">SI</option>
                   <option value="NO">NO</option>
                </select>               
             </td> 
          <td>&nbsp;</td>
          </tr> 
        
        <!-- fin  -->
          
      <tr class="titulos">            
       <td colspan="5" width="100%">
        <TABLE width="100%">
          <tr>         
           <td colspan="3">OBSERVACIONES</td>                              
          </tr>
          <tr>         
           <td colspan="3">
              <textarea type="text" id="txt_ADAN_C9"   name="observaciones" rows="2"   maxlength="4000" style="width:70%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                  
           </td>                              
          </tr>           
          <tr class="titulos"> 
             <td width="33%" id="10" onclick="traerTextoPredefinidoAngio(this.id)">FOTOGRAFIA A COLOR OJO DERECHO</td>
             <td width="33%" id="11" onclick="traerTextoPredefinidoAngio(this.id)">FOTOGRAFIA ANERITRA OJO DERECHO</td>
             <td width="33%" id="12" onclick="traerTextoPredefinidoAngio(this.id)">TRANSITO ANGIOGRAFICO OJO DERECHO</td>     
          </tr>  
          <tr class="estiloImput"> 
             <td>
           <textarea type="text" id="txt_ADAN_C10"  name="foto_color_od" rows="7"  maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                                
             </td>
             <td>
           <textarea type="text" id="txt_ADAN_C11" name="foto_anerita_od"  rows="7"   maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                                
             </td>
             <td>
           <textarea type="text" id="txt_ADAN_C12"  name="transito_od"  rows="7"   maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                                
             </td>
          </tr>       
          <tr class="titulos"> 
             <td width="33%" id="13" onclick="traerTextoPredefinidoAngio(this.id)">FOTOGRAFIA A COLOR OJO IZQUIERDO</td>
             <td width="33%" id="14" onclick="traerTextoPredefinidoAngio(this.id)">FOTOGRAFIA ANERITRA OJO IZQUIERDO</td>
             <td width="33%" id="15" onclick="traerTextoPredefinidoAngio(this.id)">TRANSITO ANGIOGRAFICO OJO IZQUIERDO</td>     
          </tr>  
          <tr class="estiloImput"> 
             <td>
           <textarea type="text" S id="txt_ADAN_C13"  name="foto_color_oi" rows="7"  maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                                
             </td>
             <td>
           <textarea type="text" id="txt_ADAN_C14"  name="foto_anerita_oi"  rows="7"   maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                                
             </td>
             <td>
           <textarea type="text" id="txt_ADAN_C15"  name="transito_oi" rows="7"    maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                                
             </td>
          </tr> 
          <tr>
             <td colspan="3" id="16"   onclick="traerTextoPredefinidoAngio(this.id)">COMENTARIO</td>                                 
          </tr>     
          <tr>
             <td colspan="3">
             <textarea type="text" id="txt_ADAN_C16"  name="comentario" rows="2"   maxlength="4000" style="width:95%"    onblur="v28(this.value,this.id); guardarContenidoDocumentoAYD()"  onkeypress="return validarKey(event,this.id)"> </textarea>                                
             </td >  
          </tr>                                       
         <% 
         /*
         <!-- <tr class="titulosCentrados"> 
            <td colspan="3" align="center">Espacio para Seguimiento
            </td>
         </tr>    
         
         <tr class="estiloImput"> 
            <td colspan="2" align="right">
               <textarea type="text" id="txt_gestionarPaciente" rows="2"   maxlength="4000" style="width:95%"     onkeypress="return validarKey(event,this.id)"> </textarea>                                                
            </td>                   
            <td colspan="1" align="left">
            <input id="btnGestionPaciente" class="small button blue" type="button" title="bt487" value="GESTIONAR CON PACIENTE" onclick="modificarCRUD('gestionPacienteFolio', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                                    
            </td>    
          </tr>       -->     

         */
         %>
         <TABLE>
        </td>                                         
      </tr>             
         
         
          
          
                           
     </table> 
   </td>   
 </tr>   
</table>  

</div>
 



 

  


