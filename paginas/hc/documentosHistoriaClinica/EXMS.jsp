<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp">
     <td width="100%">CARACTERISTICAS DE LA LESION</td> 
  </tr>
   <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="titulos1">
                  <td width="16.6%">FECHA DE INICIO DEL DOLOR O LESION</td>
                  <td width="16.6%">TIPO DE LESION</td>
                  <td width="16.6%">ATENCION RECIBIDA</td>
                  <td width="16.6%">AYUDAS ORTESICAS</td>
                  <td width="16.6%">DOLOR</td>
                  <td width="16.6%">HACE CUANTO TIEMPO</td>
                </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXMS_C1" maxlength="10" style="width:35%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXMS_C2"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXMS_C3"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td>                
                   <td>
                  <textarea type="text" id="txt_EXMS_C4"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                    <td>
                     <select size="1" id="txt_EXMS_C5" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>    
                  </td>
                    <td>
              <textarea type="text" id="txt_EXMS_C6"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>    
                  </td>                 
              </tr> 
          </table> 
        </td>
    </tr>   
    <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="titulos1">
                  <td width="16.6%">DONDE DUELE</td>
                  <td width="16.6%">COMO DUELE</td>
                  <td width="16.6%">INTENSIDAD (DE 1 A 10)</td>
                  <td width="16.6%">FACTOR DESENCADENANTE</td>
                  <td width="16.6%">LIMITE DE LA MOVILIDAD</td>
                  <td width="16.6%">POSTURA ANTALGICA</td>
                </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                  <textarea type="text" id="txt_EXMS_C7"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXMS_C8"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXMS_C9"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td>                
                   <td>
                  <textarea type="text" id="txt_EXMS_C10"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                    <td>
              <textarea type="text" id="txt_EXMS_C11"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>    
                  </td> 
                   <td>
                     <select size="1" id="txt_EXMS_C12" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>    
                  </td>                
              </tr> 
          </table> 
        </td>
    </tr> 
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="titulos1">
                  <td width="50%">CARACTERISTICAS</td>
                  <td width="50%">DOMINIO</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                     <textarea type="text" id="txt_EXMS_C13"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                  <td>
                      DERECHO:
                      <input type="text" id="txt_EXMS_C14" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                      <br><br>
                      IZQUIERDO:
                      <input type="text" id="txt_EXMS_C15" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>  
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp">
     <td width="100%">EXAMEN ARTICULAR</td> 
  </tr> 
   <tr class="camposRepInp">
     <td width="100%">CUELLO</td> 
  </tr> 
   <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="titulos1">
                  <td width="16.6%">FLEXION</td>
                  <td width="16.6%">EXTENSION</td>
                  <td width="16.6%">INCLINACION LATERAL DERECHA</td>
                  <td width="16.6%">INCLINACION LATERAL IZQUIERDA</td>
                  <td width="16.6%">ROTACION A LA DERECHA</td>
                  <td width="16.6%">ROTACION A LA IZQUIERDA</td>
                </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXMS_C16" maxlength="25" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C17" maxlength="25" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C18" maxlength="25" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXMS_C19" maxlength="25" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C20" maxlength="25" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C21" maxlength="25" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                   
               </tr>  
          </table> 
        </td>
    </tr>
   <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="camposRepInp">
                  <td width="50%" colspan="3">HOMBRO </td>
                  <td width="50%" colspan="3">CODO</td>
               </tr>
               <tr class="camposRepInp">
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                </tr>                 
            <tr class="estiloImput"> 
                  <td>
                     FLEXION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C22" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C23" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      FLEXION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C24" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C25" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>
               <tr class="estiloImput"> 
                  <td>
                     EXTENSION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C26" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C27" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      EXTENSION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C28" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C29" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>  
                   <tr class="estiloImput"> 
                  <td>
                     ABDUCCION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C30" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C31" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      SUPINACION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C32" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C33" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr> 
                   </tr>  
                   <tr class="estiloImput"> 
                  <td>
                     ROTACION EXTERNA
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C34" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C35" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      PRONACION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C36" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C37" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr> 
                     </tr>  
                   <tr class="estiloImput"> 
                  <td>
                    ROTACION INTERNA
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C38" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C39" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                  </td>
                    <td>
                  </td> 
                    <td>
                  </td>                 
               </tr>   
          </table> 
        </td>
    </tr> 
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="camposRepInp">
                  <td width="50%" colspan="3">MU&Ntilde;ECA </td>
                  <td width="50%" colspan="3">DEDO I</td>
               </tr>
               <tr class="camposRepInp">
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                </tr>                 
            <tr class="estiloImput"> 
                  <td>
                     FLEXION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C40" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C41" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      FLEXION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C42" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C43" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>
               <tr class="estiloImput"> 
                  <td>
                     EXTENSION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C44" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C45" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      EXTENSION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C46" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C47" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>  
                   <tr class="estiloImput"> 
                  <td>
                     DESVIACION CUBITAL
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C48" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C49" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      ABDUCCION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C50" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C51" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr> 
                   </tr>  
                   <tr class="estiloImput"> 
                  <td>
                  DESVIACION RADIAL
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C52" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C53" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      ADUCCION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C54" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C55" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>                  
          </table> 
        </td>
    </tr> 
        <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="camposRepInp">
                  <td width="50%" colspan="3">DEDO II</td>
                  <td width="50%" colspan="3">DEDO III</td>
               </tr>
               <tr class="camposRepInp">
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                </tr>                 
            <tr class="estiloImput"> 
                  <td>
                     FLEXION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C56" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C57" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      FLEXION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C58" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C59" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>
               <tr class="estiloImput"> 
                  <td>
                     EXTENSION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C60" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C61" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      EXTENSION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C62" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C63" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>  
                   <tr class="estiloImput"> 
                  <td>
                    ABDUCCION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C64" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C65" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      ABDUCCION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C66" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C67" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr> 
                   </tr>  
                   <tr class="estiloImput"> 
                  <td>
                  ADUCCION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C68" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C69" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      ADUCCION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C70" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C71" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>                  
          </table> 
        </td>
    </tr>  
            <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="camposRepInp">
                  <td width="50%" colspan="3">DEDO IV</td>
                  <td width="50%" colspan="3">DEDO V</td>
               </tr>
               <tr class="camposRepInp">
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                  <td width="16.6%">MOVIMIENTO</td>
                  <td width="16.6%">DERECHO</td>
                  <td width="16.6%">IZQUIERDO</td>
                </tr>                 
            <tr class="estiloImput"> 
                  <td>
                     FLEXION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C72" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C73" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      FLEXION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C74" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C75" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>
               <tr class="estiloImput"> 
                  <td>
                     EXTENSION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C76" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C77" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      EXTENSION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C78" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C79" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>  
                   <tr class="estiloImput"> 
                  <td>
                    ABDUCCION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C80" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C81" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      ABDUCCION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C82" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C83" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr> 
                   </tr>  
                   <tr class="estiloImput"> 
                  <td>
                  ADUCCION
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C84" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C85" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      ADUCCION
                  </td>
                    <td>
                      <input type="text" id="txt_EXMS_C86" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                    <td>
                      <input type="text" id="txt_EXMS_C87" maxlength="20" style="width:80%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
               </tr>                  
          </table> 
        </td>
    </tr>
    <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="50%">PRESENTA LESION NERVIOSA?  </td>
            <td width="50%">DEFORMIDAD ARTICULAR</td>
          </tr>   
          <tr class="estiloImput"> 
            <td align="center">
                     <select size="1" id="txt_EXMS_C88" style="width:7%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select><br><br>
              TIPO DE LESION
             <textarea type="text" id="txt_EXMS_C89"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>      
            <td align="center">
                     <select size="1" id="txt_EXMS_C90" style="width:7%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                    </select><br><br>
            CARACTERISTICAS
             <textarea type="text" id="txt_EXMS_C91"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>                      
        </table> 
      </td> 
  </tr>
    <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">SENSIBILIDAD</td> 
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="titulos1">
                  <td width="25%">NORMAL</td>
                  <td width="25%">HIPOESTESIA</td>
                  <td width="25%">HIPERESTESIA</td>
                  <td width="25%">ANESTESIA</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXMS_C92" maxlength="8" style="width:20%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXMS_C93" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXMS_C94" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXMS_C95" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
      <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="33.3%">AREA</td>
            <td width="33.3%">CARACTERISTICAS</td>
            <td width="33.3%">FUERZA MUSCULAR</td>
         </tr>                 
         <tr class="camposRepInp"> 
              <td>
             <textarea type="text" id="txt_EXMS_C96"    rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
             <td>
             <textarea type="text" id="txt_EXMS_C97"    rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
             <td>
             <textarea type="text" id="txt_EXMS_C98"    rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr>  
      <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">PALPACION</td> 
  </tr>
  <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="33.3%">TEJIDOS BLANDOS</td>
            <td width="33.3%">TEJIDOS OSEOS</td>
            <td width="33.3%" rowspan="2">AYUDAS DIAGNOSTICAS</td>
         </tr>                 
          <tr class="titulos1">
            <td width="33.3%">HALLAZGOS</td>
            <td width="33.3%">HALLAZGOS</td>
         </tr>    
      <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXMS_C99"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXMS_C100"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
            <td>
             <textarea type="text" id="txt_EXMS_C101"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr> 
</table>


 
 





