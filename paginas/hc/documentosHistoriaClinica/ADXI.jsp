<div id="plantilla_ADXI">
  <table width="100%" align="center">
    <tr>
      <td>
        <table width="100%" align="center">
          <tr class="titulos">
            <td width="20%">EXAMEN CORRECTO</td>
            <td width="20%">SITIO DE EXAMEN CORRECTO</td>
            <td width="20%">DATOS DEL PACIENTE CORRECTOS</td>
            <td width="20%">CANTIDAD DE EXAMENES CORRECTA</td>
            <td width="20%">RIESGOS POR MEDICAMENTOS </td>
          </tr>
          <tr class="estiloImput">
            <td>
              <select size="1" id="txt_ADXI_C1" name="examen_correcto" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
            <td>
              <select size="1" id="txt_ADXI_C2" name="sitio_correcto" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
            <td>
              <select size="1" id="txt_ADXI_C3" name="datos_paciente_correcto" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
            <td>
              <select size="1" id="txt_ADXI_C4" name="cantidad_correcta" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
            <td>
              <select size="1" id="txt_ADXI_C5" name="riesgo_medicamentos" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="NA" selected="selected">NA</option>
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
          </tr>

          <tr class="titulos">
            <td width="20%">PREPARACION PREVIA DEL PACIENTE</td>
            <td width="20%">DOCUMENTOS NECESARIOS</td>
            <td width="20%">CONSENTIMIENTO INFORMADO</td>
            <td width="40%" colspan="2">OBSERVACIONES</td>  
          </tr>

          <tr class="estiloImput">
            <td>
              <select size="1" id="txt_ADXI_C6" name="preparacion_previa" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
            <td>
              <select size="1" id="txt_ADXI_C7" name="documentos_necesarios" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="NA">NA</option>
                <option value="SI" selected="selected">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
            <td>
              <select size="1" id="txt_ADXI_C8" name="consentimiento_informado" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
                >
                <option value="NA">NA</option>
                <option value="SI" selected="selected">SI</option>
                <option value="NO">NO</option>
              </select>
            </td>
            <td colspan="2"><input type="text" id="txt_ADXI_C9" name="observaciones" maxlength="500" style="width:96%"
                onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</div>