<div id="plantilla_VRCA">
     <table BORDER="2" width="100%" cellpadding="0" cellspacing="0" align="right">
       <tr>
         <td width="1000%" bgcolor="#c0d3c1">.</td>
       </tr>
       <tr class="camposRepInp">
         <td colspan="8">ESCALA DE RIESGO DE CAIDAS J.H.DOWNTON</td>
       </tr>
       <table BORDER="2" align="center">
         <tr>
           <td class="camposRepInp" colspan="2">CAIDAS PREVIAS</td>
           <td class="camposRepInp"></td>
           <td class="camposRepInp">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C1" name="caidas_previas" style="width:34%;" title="32" 
               onclick="total();imprimirTotalcombo();guardarContenidoDocumento();">
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C2" name="caidas_previas1" style="width:15%"  />
           </td>
         </tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <tr>
           <td ROWSPAN=7 class="camposRepInp" colspan="2">MEDICAMENTOS</td>
           <td class="estiloImput" align="right">NINGUNO:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C3" name="medicamentos_ninguno1" style="width:34%;" title="32" 
               onclick="total();imprimirTotalcombo();guardarContenidoDocumento();" onchange="deshabilitarVRCA()">
               <option value="1">SI</option>
               <option value="0">NO</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C4" name="medicamentos_ninguno2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">TRANQUILIZANTES-SEDANTES:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C5" name="medicamentos_tranquilizantes1" style="width:34%;" title="32"
               onclick="total();imprimirTotalcombo();guardarContenidoDocumento();" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" type="text" id="txt_VRCA_C6" name="medicamentos_tranquilizantes2" disabled="disabled" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">DIURETICOS:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C7" name="medicamentos_diureticos1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" type="text" id="txt_VRCA_C8" name="medicamentos_diureticos2" disabled="disabled" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">HIPOTENSORES (NO DIURETICOS):</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C9" name="medicamentos_hipotensores1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C10" name="medicamentos_hipotensores2" style="width:15%"  />
           </td>
         </tr>
   
         <tr>
           <td class="estiloImput" align="right">ANTIPARKINSONIANOS:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C11" name="medicamentos_antiparkinsonianos1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C12" name="medicamentos_antiparkinsonianos2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">ANTIDEPRESIVOS:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C13" name="medicamentos_antidepresivos1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C14" name="medicamentos_antidepresivos2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">OTROS MEDICAMENTOS:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C15" name="medicamentos_otros1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C16" name="medicamentos_otros2" style="width:15%"  />
           </td>
         </tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <tr>
           <td ROWSPAN=4 class="camposRepInp" colspan="2">DEFICITS SENSITIVO-MOTORES</td>
           <td class="estiloImput" align="right">NINGUNO:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C17" name="deficits_ninguno1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32"  onchange="deshabilitarVRCA1()">
               <option value="1">SI</option>
               <option value="0">NO</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C18" name="deficits_ninguno2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">ALTERACIONES VISUALES:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C19" name="deficits_visuales1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento()"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C20" name="deficits_visuales2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">ALTERACIONES AUDITIVAS:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C21" name="deficits_auditivas1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C22" name="deficits_auditivas2" style="width:15%"  />
           </td>
         </tr>
   
         <tr>
           <td class="estiloImput" align="right">EXTREMIDADES (Ictus...):</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C23" name="deficits_extremidades1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C24" name="deficits_extremidades2" style="width:15%"  />
           </td>
         </tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <tr>
           <td ROWSPAN=2 class="camposRepInp" colspan="2">ESTADO MENTAL</td>
           <td class="estiloImput" align="right">ORIENTADO:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C25" name="estado_orientado1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               onchange="cambiarCombo()" style="width:34%;" title="32" >
               <option value="1">SI</option>
               <option value="0">NO</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C26" name="estado_orientado2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">CONFUSO:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C27" name="estado_confuso1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               onchange="cambiarCombo1()" style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C28" name="estado_confuso2" style="width:15%"  />
           </td>
         </tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <tr></tr>
         <td ROWSPAN=5 class="camposRepInp" colspan="2">DEAMBULACION</td>
         <td class="estiloImput" align="right">NORMAL:</td>
         <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
           <select size="1" id="txt_VRCA_C29" name="deambulacion_normal1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
             style="width:34%;" title="32" >
             <option value="1">SI</option>
             <option value="0">NO</option>
           </select>
           <input value="0" disabled="disabled" type="text" id="txt_VRCA_C30" name="deambulacion_normal2" style="width:15%"  />
         </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">SEGURA:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C31" name="deambulacion_segura1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               style="width:34%;" title="32" >
               <option value="1">SI</option>
               <option value="0">NO</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C32" name="deambulacion_segura2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">SEGURA CON AYUDA:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C33" name="deambulacion_segura_ayuda1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               onchange="cambiarComboDeambulacion()" style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C34" name="deambulacion_segura_ayuda2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">INSEGURA CON AYUDA/SIN AYUDA:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C35" name="deambulacion_inseguraayuda1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               onchange="cambiarComboDeambulacion()" style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C36" name="deambulacion_insegura2" style="width:15%"  />
           </td>
         </tr>
         <tr>
           <td class="estiloImput" align="right">IMPOSIBLE:</td>
           <td class="camposRepInp" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
             <select size="1" id="txt_VRCA_C37" name="deambulacion_imposible1" onclick="total();imprimirTotalcombo();guardarContenidoDocumento();"
               onchange="cambiarComboDeambulacion()" style="width:34%;" title="32" >
               <option value="0">NO</option>
               <option value="1">SI</option>
             </select>
             <input value="0" disabled="disabled" type="text" id="txt_VRCA_C38" name="deambulacion_imposible2" style="width:15%"  />
           </td>
           <table align="center">
             <tr>
               <td class="estiloImput"><strong>VALORACION TOTAL:</strong></td>
               <td><strong>&nbsp;<input align="center" type="text" id="txt_VRCA_C39" name="total" disabled="disabled"
                     style="WIDTH: 30px; HEIGHT: 20px" /></strong>
   
               </td>
   
               <td><input value="0" disabled="disabled" type="text" id="txt_VRCA_C40" name="mensaje" style="WIDTH: 200px; HEIGHT: 15px"
                    /></td>
             </tr>
           </table>
         </tr>
   
       </table>
       </tr>
     </table>
   </div>