<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">RESUMEN DE HISTORIA CLINICA</td> 
  </tr>
  <tr class="camposRepInp">               
    <td>
         <textarea type="text" id="txt_EXNU_C1"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
    </td>                  
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp" >
     <td width="100%">LENGUAJE COMPRENSIVO</td> 
  </tr>
   <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="25%">ORIENTACION</td>
                  <td width="25%">COMPRENSION DE ORDENES</td>
                  <td width="25%">IDENTIFICACION</td>
                  <td width="25%">RESPONDE A PREGUNTAS SIMPLES</td>
                </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                    <select size="1" id="txt_EXNU_C2" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="TIEMPO">TIEMPO</option> 
                        <option value="LUGAR">LUGAR</option>   
                        <option value="PERSONA">PERSONA</option>           
                    </select>
                  </td>
                  <td>
                    <select size="1" id="txt_EXNU_C3" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SIMPLES">SIMPLES</option> 
                        <option value="COMPLEJAS">COMPLEJAS</option>           
                      </select>
                  </td>                
                  <td>
                   <select size="1" id="txt_EXNU_C4" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SENALANDO">SE&Ntilde;ALANDO</option> 
                        <option value="NOMINANDO">NOMINANDO</option>           
                      </select>    
                  </td>                
                   <td>
                     <select size="1" id="txt_EXNU_C5" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>    
                  </td>                  
              </tr> 
          </table> 
        </td>
    </tr>   
      <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp" >
     <td width="100%">LENGUAJE EXPRESIVO</td> 
  </tr> 
  <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="25%">HABLA</td>
                  <td width="25%">LENGUAJE</td>
                  <td width="25%">DISCURSO</td>
                  <td width="25%">DEFORMACIONES DEL LENGUAJE</td>
                </tr>                 
            <tr class="camposRepInp"> 
                   <td>
                     <select size="1" id="txt_EXNU_C6" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>    
                  </td> 
                  <td>
                    <select size="1" id="txt_EXNU_C7" style="width:35%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="INTELIGIBLE">INTELIGIBLE</option> 
                        <option value="ININTELIGIBLE">ININTELIGIBLE</option>           
                      </select>
                  </td>                
                  <td>
                   <select size="1" id="txt_EXNU_C8" style="width:35%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="FLUIDO">FLUIDO</option> 
                        <option value="NO FLUIDO">NO FLUIDO</option>      
                        <option value="COHERENTE">COHERENTE</option>           
                        <option value="INCOHERENTE">INCOHERENTE</option>           
     
                      </select>    
                  </td>                
                   <td>
                     <textarea type="text" id="txt_EXNU_C9"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>    
                  </td>                  
              </tr> 
          </table> 
        </td>
    </tr>   
      <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="25%">LENGUAJE AUTOMATICO</td>
                  <td width="25%">REPETICION</td>
                  <td width="25%">DENOMINACION</td>
                  <td width="25%">PERSEVERACIONES</td>
                </tr>                 
            <tr class="camposRepInp"> 
                   <td>
                     <select size="1" id="txt_EXNU_C10" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="DIAS DE LA SEMANA ">DIAS DE LA SEMANA </option> 
                        <option value="NUMEROS">NUMEROS</option> 
                        <option value="MESES DEL ANO">MESES DEL A&Ntilde;O</option>           
                      </select>    
                  </td> 
                  <td>
                    <select size="1" id="txt_EXNU_C11" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SILABAS">SILABAS</option> 
                        <option value="PALABRAS">PALABRAS</option>           
                        <option value="FRASES">FRASES</option>           
                      </select>
                  </td>                
                  <td>
                    COMO SE LLAMA...? 
                   <textarea type="text" id="txt_EXNU_C12"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </select>    
                  </td>                
                   <td>
                     <textarea type="text" id="txt_EXNU_C13"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>    
                  </td>                  
              </tr> 
          </table> 
        </td>
    </tr>
    <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">HABLA</td> 
    </tr> 
    <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">EXAMEN POSTURAL</td> 
    </tr> 
      <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="20%">CORPORAL</td>
                  <td width="20%">CINTURA ESCAPULAR/CINTURA PELVICA</td>
                  <td width="20%">ALINEACION</td>
                  <td width="20%">FACIAL - PROPORCION DE TERCIOS</td>
                </tr>                 
            <tr class="camposRepInp"> 
                   <td>
                     <select size="1" id="txt_EXNU_C14" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="DECUBITO DORSAL">DECUBITO DORSAL</option> 
                        <option value="SEDENTE">SEDENTE</option> 
                      </select>    
                  </td> 
                  <td>
                    <select size="1" id="txt_EXNU_C15" style="width:39%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="ANTEROVERSION">ANTEROVERSION</option> 
                        <option value="RETROVERSION">RETROVERSION</option>           
                      </select>
                  </td>                
                   <td>
                    <select size="1" id="txt_EXNU_C16" style="width:30%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="ASCENSO">ASCENSO</option> 
                        <option value="DESCENSO">DESCENSO</option>           
                      </select>
                  </td>                
                    <td>
                      <textarea type="text" id="txt_EXNU_C17"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>    
                  </td>                   
              </tr> 
          </table> 
        </td>
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                <tr class="estiloImput">
                  <td width="100%" colspan="6">FACIAL - SIMETRIA/ASIMETRIA EN:</td>
                </tr>
                 <tr class="estiloImput">
                  <td width="16.6%">OJOS</td>
                  <td width="16.6%">NARINAS</td>
                  <td width="16.6%">COMISURAS</td>
                  <td width="16.6%">CUELLO</td>
                  <td width="16.6%">CABEZA</td>
                  <td width="16.6%">ANGULOSGONIACOS</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C18" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C19" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C20" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C21" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C22" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C23" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">EXAMEN OFA</td> 
    </tr> 
    <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">EXOBUCAL</td> 
    </tr>
     <tr class="estiloImput" >
      <td width="100%">ESTATICO</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="16.6%">COLOR</td>
                  <td width="16.6%">FORMA</td>
                  <td width="16.6%">TAMA&Ntilde;O</td>
                  <td width="16.6%">GRUESOS</td>
                  <td width="16.6%">DELGADOS</td>
                  <td width="16.6%">BLANQUECINOS</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C24" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C25" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C26" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C27" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C28" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C29" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>   
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="25%">RESECOS</td>
                  <td width="25%">IRRITACION COMISURAL</td>
                  <td width="25%">INVERTIDOS</td>
                  <td width="25%">EVERTIDOS</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C30" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C31" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C32" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C33" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>    
    <tr class="estiloImput" >
      <td width="100%">DINAMICO</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="25%">SELLE LABIAL</td>
                  <td width="25%">PRAXIAS (COORDINACION, FUERZA, VELOCIDAD) LABIALES</td>
                  <td width="25%">MANDIBULARES</td>
                  <td width="25%">BUCAS</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C34" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C35" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C36" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C37" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>   
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="50%">MENTON</td> 
                  <td width="50%">TONO MUSCULAR</td> 
                </tr>
                <tr class="camposRepInp"> 
                   <td>
                    <select size="1" id="txt_EXNU_C38" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="TONO">TONO</option> 
                        <option value="PROTRUIDO">PROTRUIDO</option>           
                        <option value="RETRAIDO">RETRAIDO</option>           
                      </select>
                  </td> 
                  <td>
                    <select size="1" id="txt_EXNU_C39" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="MASETERO">MASETERO</option> 
                        <option value="ORBICULAR DE LABIOS">ORBICULAR DE LABIOS</option>           
                        <option value="BUCCINADOR">BUCCINADOR</option>           
                      </select>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>   
     <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">ENDOBUCAL</td> 
    </tr>
    </tr>    
    <tr class="estiloImput" >
      <td width="100%">VESTIBULO</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="20%">MUCOSA</td>
                  <td width="20%">SECRESIONES</td>
                  <td width="20%">ROSADAS</td>
                  <td width="20%">LASERASIONES</td>
                  <td width="20%">AFTAS</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C40" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C41" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C42" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C43" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C44" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                    <td width="33.3%">FRENILLOS</td>
                    <td width="33.3%">ENCIAS</td> 
                    <td width="33.3%">DIENTES</td> 
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <select size="1" id="txt_EXNU_C45" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="CORTOS ">CORTOS </option> 
                        <option value="NORMALES">NORMALES</option>           
                      </select>
                  </td>                
                 <td>
                      <select size="1" id="txt_EXNU_C46" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="NORMALES">NORMALES</option> 
                        <option value="DESGASTADAS">DESGASTADAS</option>           
                      </select>
                 </td>                 
                  <td>
                      <select size="1" id="txt_EXNU_C47" style="width:50%;" title="40" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="EDENTULO">EDENTULO</option> 
                        <option value="AUSENCIA DE PIEZAS DENTARIAS">AUSENCIA DE PIEZAS DENTARIAS</option>           
                      </select>
                  </td> 
               </tr>     
          </table> 
        </td>
    </tr>   
    <tr class="estiloImput" >
      <td width="100%">LENGUA</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="25%">COLOR</td>
                  <td width="25%">FORMA</td>
                  <td width="25%">TAMA&Ntilde;O</td>
                  <td width="25%">POSICION</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C48" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C49" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C50" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C51" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
    <tr class="estiloImput" >
      <td width="100%">PALADAR</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="14.28%">ALTO</td>
                  <td width="14.28%">ANCHO</td>
                  <td width="14.28%">PLANO</td>
                  <td width="14.28%">OJIVAL</td>
                  <td width="14.28%">F</td>
                  <td width="14.28%">C</td>
                  <td width="14.28%">T</td>

                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C52" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C53" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C54" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C55" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C56" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C57" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C58" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
    <tr class="estiloImput" >
      <td width="100%">VELO DEL PALADAR</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="25%">UVULA DESVIADA </td>
                  <td width="25%">CORTA</td>
                  <td width="25%">LARGA</td>
                  <td width="25%">NORMAL</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C59" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C60" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C61" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C62" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
    <tr class="estiloImput" >
      <td width="100%">DINAMICO</td> 
    </tr>
    <tr>
          <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="100%">PRAXIAS LINGUALES</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                       <textarea type="text" id="txt_EXNU_C63"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
               </tr>     
          </table> 
        </td>
    </tr> 
     <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">REFLEJOS</td> 
    </tr> 
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="25%">TUSIGENO VOLUNTARIO</td>
                  <td width="25%">NAUSEOSO</td>
                  <td width="25%">PALATAL</td>
                  <td width="25%">DEGLUTORIO</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <select size="1" id="txt_EXNU_C64" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>  
                  </td>                
                 <td>
                      <select size="1" id="txt_EXNU_C65" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>  
                  </td>                 
                  <td>
                        <select size="1" id="txt_EXNU_C66" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>  
                  </td> 
                  <td>
                   <select size="1" id="txt_EXNU_C67" style="width:15%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>  
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">FUNCIONES DE ALIMENTACION</td> 
    </tr>
    <tr class="camposRepInp">
      <td width="100%">
        <p>TECNICA DE LOS 5 MINUTOS (3), TECNICA DE LOS 5 DEDOS, DEGLUCION EN SECO, GOTEO, HUMEDECIMIENTO 1CC-5CC, AUSCULTACION ANTES DURANTE Y DESPUES.</p>
        </td> 
    </tr>
    <tr class="estiloImput" >
      <td width="100%">CONSISTENCIAS</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="14.28%">FLUIDOS PROPIOS</td>
                  <td width="14.28%">CLARO</td>
                  <td width="14.28%">ESPESO</td>
                  <td width="14.28%">LIQUIDO</td>
                  <td width="14.28%">SEMILIQUIDO</td>
                  <td width="14.28%">SOLIDO</td>
                  <td width="14.28%">SEMISOLIDO</td>

                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C68" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C69" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C70" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C71" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C72" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C73" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C74" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
    <tr class="estiloImput" >
      <td width="100%">CANTIDAD</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="25%">1CC</td>
                  <td width="25%">3CC</td>
                  <td width="25%">5CC</td>
                  <td width="25%">OTRO</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C75" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>

                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C76" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C77" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                       <textarea type="text" id="txt_EXNU_C78"   rows="2" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
       <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">FASE PREPARATORIA</td> 
    </tr>
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="33.3%">ANTICIPACION AL ALIMENTO</td>
                  <td width="33.3%">APERTURA ORAL</td>
                  <td width="33.3%">SELLE LABIAL </td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                 <textarea type="text" id="txt_EXNU_C79"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                 <td>
                   <textarea type="text" id="txt_EXNU_C80"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                 
                  <td>
                       <textarea type="text" id="txt_EXNU_C81"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
    <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">FASE ORAL</td> 
    </tr> 
     <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="14.28%">SELLE LABIAL</td>
                  <td width="14.28%">DERRAME</td>
                  <td width="14.28%">TRASTORNOS SENSORIALES</td>
                  <td width="14.28%">MASTICACION</td>
                  <td width="14.28%">SALIVACION</td>
                  <td width="14.28%">FORMACION DEL BOLO</td>
                  <td width="14.28%">PROTRUSION LINGUAL</td>

                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C82" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C83" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C84" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C85" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C86" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C87" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C88" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
        <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="33.3%">DIFICULTAD PARA MOVILIZAR EL BOLO HACIA ATRAS</td>
                  <td width="33.3%">TRANSITO ORAL</td>
                  <td width="33.3%">RESIDUOS EN LA CAVIDAD ORAL</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C89" maxlength="20" style="width:26%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                    <select size="1" id="txt_EXNU_C90" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>           
                        <option value="DISMINUIDO">DISMINUIDO</option>           
                      </select>  
                  </td>                 
                  <td>
                       <textarea type="text" id="txt_EXNU_C91"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
        <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp" >
      <td width="100%">FASE FARINGEA</td> 
    </tr> 
         <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="20%">REFLEJO DEGLUTORIO</td>
                  <td width="20%">DEGLUCIONES MULTIPLES</td>
                  <td width="20%">DEGLUCIONES AUDIBLES</td>
                  <td width="20%">TOS</td>
                  <td width="20%">REGURGITACION</td>

                </tr>
                <tr class="camposRepInp"> 
                  <td>
                     <select size="1" id="txt_EXNU_C92" style="width:55%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="RETRASADO">RETRASADO</option> 
                        <option value="AUSENCIA">AUSENCIA</option>           
                      </select>  
                  </td>                
                  <td>
                      <input type="text" id="txt_EXNU_C93" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C94" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C95" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C96" maxlength="15" style="width:65%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
            <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="25%">ATRAGANTAMIENTO </td>
                  <td width="25%">PENETRACION</td>
                  <td width="25%">ASPIRACION</td>
                 <td width="25%">CALIDAD VOCAL ALTERADA (VOZ HUMEDA) </td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C97" maxlength="15" style="width:35%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C98" maxlength="15" style="width:35%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C99" maxlength="15" style="width:35%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C100" maxlength="15" style="width:35%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
    <tr class="camposRepInp" >
      <td width="100%">FASE ESOFAGICA</td> 
    </tr> 
     <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="16.6%">RGE</td>
                  <td width="16.6%">ERUCTOS</td>
                  <td width="16.6%">NAUSEAS</td>
                  <td width="16.6%">VOMITO</td>
                  <td width="16.6%">SENSACION DE LLENURA</td>
                  <td width="16.6%">DOLOR ESTERNAL</td>

                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C101" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C102" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C103" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C104" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C105" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C106" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
      <tr>
      <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp">
      <td width="100%">RESPIRACION</td> 
    </tr> 
     <tr>
      <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="16.6%">MODO</td>
                  <td width="16.6%">TIPO</td>
                  <td width="16.6%">FRECUENCIA RESPIRATORIA</td>
                  <td width="16.6%">APOYO O2</td>
                  <td width="16.6%">MASCARA</td>
                  <td width="16.6%">CANULA NASAL</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C107" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C108" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C109" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C110" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C111" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C112" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
     <td width="1000%" bgcolor="#c0d3c1">.</td>
    </tr> 
    <tr class="camposRepInp">
      <td width="100%">VOZ</td> 
    </tr> 
    <tr class="camposRepInp">
      <td width="100%">CARACTERISTICAS ACUSTICO PERCEPTUALES DE LA VOZ.</td> 
    </tr> 
    <tr class="estiloImput">
      <td width="100%">TIMBRE</td> 
    </tr> 
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="16.6%">RONCO</td>
                  <td width="16.6%">NASAL</td>
                  <td width="16.6%">CARRASPOSO</td>
                  <td width="16.6%">SOPLADO</td>
                  <td width="16.6%">DISFONICO</td>
                  <td width="16.6%">ESTRIDENTE</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXNU_C113" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXNU_C114" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXNU_C115" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXNU_C116" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_EXNU_C117" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
                   <td>
                      <input type="text" id="txt_EXNU_C118" maxlength="20" style="width:70%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
      <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="33.3%">TONO</td>
                  <td width="33.3%">INTENSIDAD</td>
                  <td width="33.3%">DURACION</td>
               </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <select size="1" id="txt_EXNU_C119" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AGUDO">AGUDO</option>           
                        <option value="GRAVE">GRAVE</option>           
                      </select>  
                  </td>                
                 <td>
                    <select size="1" id="txt_EXNU_C120" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AUMENTADO">AUMENTADO</option>           
                        <option value="DISMINUIDA">DISMINUIDA</option>           
                      </select>  
                  </td>                 
                  <td>
                       <select size="1" id="txt_EXNU_C121" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="CORTA">CORTA</option> 
                        <option value="PROLONGADO">PROLONGADO</option>           
                      </select>  
                  </td>
               </tr>     
          </table> 
        </td>
    </tr>
    <tr class="estiloImput">
      <td width="100%">CARACTERISTICAS DEL HABLA.</td> 
    </tr> 
    <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="estiloImput">
                  <td width="16.6%">GRADO DE APERTURA ORAL (ADICION)</td>
                  <td width="16.6%">ARTICULACION</td>
                  <td width="16.6%">PROSODIA</td>
                  <td width="16.6%">FLUIDEZ</td>
                  <td width="16.6%">VELOCIDAD</td>
                  <td width="16.6%">RITMO / PAUSAS</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                        <select size="1" id="txt_EXNU_C122" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="AFECTADA">AFECTADA</option>           
                      </select>  
                  </td>                
               <td>
                        <select size="1" id="txt_EXNU_C123" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="CORRECTA">CORRECTA</option> 
                        <option value="ALTERADA">ALTERADA</option>           
                      </select>  
                  </td>   
                  <td>
                        <select size="1" id="txt_EXNU_C124" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="CORRECTA">CORRECTA</option> 
                        <option value="ALTERADA">ALTERADA</option>           
                      </select>  
                  </td>   
                  <td>
                        <select size="1" id="txt_EXNU_C125" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="NORMAL">NORMAL</option> 
                        <option value="DEFICIENTE">DEFICIENTE</option>           
                      </select>  
                  </td>   
                  <td>
                        <select size="1" id="txt_EXNU_C126" style="width:45%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="NORMAL ">NORMAL </option> 
                        <option value="TAQUILALIA">TAQUILALIA</option>      
                        <option value="BRADIALIA">BRADIALIA</option>           
                        </select>  
                  </td>   
                  <td>
                        <select size="1" id="txt_EXNU_C127" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"   > <option value=""></option> 
                        <option value="SI">SI</option> 
                        <option value="NO">NO</option>           
                      </select>  
                  </td>   
               </tr>     
          </table> 
        </td>
    </tr>
      <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="estiloImput">
            <td width="100%">CONDUCTA HA SEGUIR</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXNU_C128"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
        </table> 
      </td> 
  </tr>
</table>


 
 





