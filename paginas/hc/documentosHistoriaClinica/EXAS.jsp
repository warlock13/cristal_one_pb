<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="camposRepInp">
                  <td width="100%">ASPECTO DEL SENO</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                    <textarea type="text" id="txt_EXAS_C1"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                     
              </tr> 
          </table> 
        </td>
    </tr>  
     <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp">
     <td width="100%">EXAMEN POSTURAL</td> 
  </tr>  
    <tr class="camposRepInp">
     <td width="100%">CABEZA</td> 
  </tr> 
  <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="33.3%">VISTA ANTERIOR</td>
                  <td width="33.3%">VISTA LATERAL</td>
                  <td width="33.3%">VISTA POSTERIOR</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <textarea type="text" id="txt_EXAS_C2"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXAS_C3"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXAS_C4"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
          </tr> 
          </table> 
        </td>
    </tr>
    <tr class="camposRepInp">
     <td width="100%">HOMBROS</td> 
  </tr>  
    <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="33.3%">VISTA ANTERIOR</td>
                  <td width="33.3%">VISTA LATERAL</td>
                  <td width="33.3%">VISTA POSTERIOR</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                    <textarea type="text" id="txt_EXAS_C5"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXAS_C6"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXAS_C7"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
          </tr> 
          </table> 
        </td>
    </tr>    
     <tr class="camposRepInp">
     <td width="100%">COLUMNA CERVICAL</td> 
  </tr>  
    <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="33.3%">VISTA ANTERIOR</td>
                  <td width="33.3%">VISTA LATERAL</td>
                  <td width="33.3%">VISTA POSTERIOR</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                    <textarea type="text" id="txt_EXAS_C8"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXAS_C9"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXAS_C10"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
          </tr> 
          </table> 
        </td>
    </tr>    
    <tr class="camposRepInp">
     <td width="100%">COLUMNA DORSAL</td> 
  </tr>  
    <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="33.3%">VISTA ANTERIOR</td>
                  <td width="33.3%">VISTA LATERAL</td>
                  <td width="33.3%">VISTA POSTERIOR</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                    <textarea type="text" id="txt_EXAS_C11"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXAS_C12"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXAS_C13"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
          </tr> 
          </table> 
        </td>
    </tr> 
    <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
     <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="25%">RETRACIONES MUSCULARES EN</td>
                  <td width="25%">DEBILIDAD MUSCULAR EN</td>
                  <td width="25%">AYUDAS DIAGNOSTICAS</td>
                  <td width="25%">TEGIDOS BLANDOS</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <textarea type="text" id="txt_EXAS_C14"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_EXAS_C15"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
                  <td>
                     <textarea type="text" id="txt_EXAS_C16"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
                  <td>
                     <textarea type="text" id="txt_EXAS_C17"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>   
                  </td> 
          </tr> 
          </table> 
        </td>
    </tr>   
</table>


 
 





