<div id="plantilla_LICH">
<table width="100%"  align="center">
<tr>
 <td >
  <table width="100%" align="center">
        <tr class="estiloImput">
          <td width="100%" colspan="4">USUARIO REINGRESA A CIRUGIA EN UN PERIODO MENOR A 3 MESES POR LA MISMA CAUSA:
             <select size="1" id="txt_LICH_C1"  name="p1" style="width:5%;" title="32"  onblur=" "   >	  
                <option value="SI">SI</option>
                <option value="NO">NO</option>
             </select>
          </td>                                           
        </tr>		
        <tr class="titulos">
          <td width="20%">TIEMPO DE ADMISION</td>                               
          <td width="20%">TIEMPO DE ANESTESIA</td>
          <td width="20%">TIEMPO DE PROCEDIMIENTO</td>                
          <td width="20%">TIEMPO DE RECUPERACION</td>                
        </tr>		
        <tr class="estiloImput"> 
          <td>
              <input type="text" id="txt_LICH_C2"  name="tiempo_admision" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICH_C3"  name="tiempo_anestesia" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICH_C4" name="tiempo_proc" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>                
          <td>
              <input type="text" id="txt_LICH_C5"  name="tiempo_rec" maxlength="5" style="width:20%" onblur=" "/> MIN
          </td>   
        </tr>     
  </table> 

  <table width="100%" align="center">
    <tr class="titulos2">
      <td width="90%" colspan="2">1-ADMISION A CIRUGIA AMBULATORIA
      </td>
	  <td width="10%" colspan="2">OBSERVACION
      </td>	  
    </tr> 
    <tr class="estiloImput">
      <td width="80%" class="estiloImputDer">USUARIO LLEGA 30 MINUTOS ANTES DE LA HORA DE CIRUGIA:</td> 
      <td width="10%" class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C6"  name="p2" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI" >SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
            <option value=""></option>
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C7"  name="o2" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>	 
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">EL NOMBRE Y APELLIDO DE USUARIO CORRESPONDE CON LA PROGRAMACION QUIRURGICA Y CON EL PACIENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C8" name="p3" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C9" name="o3" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
    <tr class="estiloImput">
      <td class="estiloImputDer">EL NUMERO DE IDENTIFICACION DEL USUARIO CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C10"  name="p4" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C11" name="o4" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>		
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">EL NOMBRE DEL PROCEDIMIENTO A REALIZAR CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C12"  name="p5" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
      <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C13"  name="o5" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td> 	  
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">EL SITIO QUIRURGICO CORRESPONDE CON LA PROGRAMACION QUIRURGICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C14"  name="p6" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>			
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C15"  name="o6" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">CONFIRMAR AYUNO (CUANDO CORRESPONDA SUGUN GUIA DE RECOMENDACIONES PREQUIRURGICAS) ESCRIBA LA ULTIMA HORA DE INGESTA DE ALIMENTO O BEBIDA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C16"  name="p7" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>       
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C17"    name="o7" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>      
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO SE PRESENTA CON OJOS SIN MAQUILLAJE (VERIFICAR AUSENCIA DE PESTAÑINA):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C18"   name="p8" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C19" name="o8" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO SE PRESENTA CON UÑAS CORTAS, LIMPIAS Y SIN ESMALTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C20" name="p9" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C21"  name="o9" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>     
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO SE PRESENTA SIN OBJETOS DE VANIDAD, ARETES, ANILLOS ETC:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C22"  name="p10" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C23" name="o10" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImput">NOMBRE DE LOS MEDICAMENTOS QUE ESTA INGIRIENDO EL USUARIO:
        <textarea type="text" id="txt_LICH_C24" name="med_ingiriendo"  size="4000"  maxlength="4000" style="width:50%"    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();" onkeypress="return validarKey(event,this.id)" > </textarea>
      </td> 
    </tr>    
    <tr class="estiloImput">
      <td class="estiloImputDer">USUARIO HA INGERIDO MEDICAMENTOS ESTIPULADOS EN LA GUIA DE RECOMENDACIONES PRE QUIRURGICAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C25"  name="p11" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C26" name="o11"  maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">USUARIO ES CONSUMIDOR CRONICO DE ALCOHOL, CIGARRILLO O SUSTANCIAS PSICOACTIVAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C98"   name="p47" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C99" name="o47"  maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>	
    <tr class="estiloImput">
      <td class="estiloImputDer">MANILLA DE IDENTIFICACION CON NOMRE Y APELLIDOS COMPLETOS, DOCUMENTO DE IDENTIFICACION Y NOMBRE DEL PROCEDIMIENTO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C27"  name="p12"  style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C28"  name="o12"  maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="titulos2">
      <td width="90%" colspan="2">INGRESO A CIRUGIA AMBULATORIA (Circulante)
      </td>  
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">VESTIMENTA DEL USUARIO ADECUADA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C29" name="p13" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C30"  name="o13"maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>          
    <tr class="estiloImput">
      <td class="estiloImputDer">MANILLA DE IDENTIFICACION DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C31" name="p14" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C32" name="o14" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">CONFIRMACION DEL NOMBRE E IDENTIFICACION DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C33" name="p15" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C34"  name="o15"maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">PROCEDIMIENTO QUIRURGICO CONFIRMADO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C35"  name="p16" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C36"  name="o16"maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>   
    <tr class="estiloImput">
      <td class="estiloImputDer">CONSENTIMIENTO INFORMADO FIRMADO POR EL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C37"  name="p17" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C38" name="o17" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>   
    <tr class="estiloImput">
      <td class="estiloImputDer">MARCACION DE SITIO OPERATORIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C39"  name="p18" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C40"  name="o18" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
    <tr class="estiloImput">
      <td class="estiloImputDer">MANILLA ROJA DE ALERTA DE ALERGIAS MEDICAMNETOSAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C41" name="p19"  style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C42" name="o19" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>         
    <tr class="estiloImput">
      <td class="estiloImputDer">CAMILLAS CON BARANDA ARRIBA Y CON FRENO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C43"  name="p20" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C44" name="o20" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">ACCESO  VENOSO PERMEABLE (CUANDO CORRESPONDA):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C45"  name="p21" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C46" name="o21" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>  
    <tr class="estiloImput">
      <td class="estiloImputDer">TOMA Y REGISTROS DE SIGNOS VITALES:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C47"  name="p22"style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C48" name="o22" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>    
    <tr class="titulos2">
      <td width="90%" colspan="2">2-ANTES DE LA INDUCCION DE LA ANESTESIA O BLOQUEO ANESTESICO (ANESTESIOLOGO, CIRUJANO, CIRCULANTE)
      </td>   
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr>                         
    <tr class="estiloImput">
      <td class="estiloImputDer">SE HA COMPLETADO LA COMPROBACION DE LOS APARATOS DE ANESTESIA Y LA MEDICACION ANESTESICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C49"  name="p23" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C50" name="o23" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>            
	<tr class="estiloImput">
      <td class="estiloImputDer">CIRCUITOS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C51"  name="p24"style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C52" name="o24" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">MEDICACION DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C53" name="p25" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C54" name="o25" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">REGISTRO ANESTESICO DEL USUARIO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C55"  name="p26" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C56" name="o26" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EQUIPO DE INTUBACION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C57" name="p27" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C58" name="o27" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EQUIPO PARA ASPIRACION DE VIA AEREA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C59" name="p28"style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option> 
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C60" name="o28" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">SISTEMA DE VENTILACION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C61"  name="p29" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C62" name="o29" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">PREMEDICACION ANESTESICA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C63" name="p30" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>     
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C64"  name="o30" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">EQUIPO DE SUCCION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C65" name="p31"  style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>    
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C66" name="o31" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">SE HA COLOCADO EL PULSOXIMETRO AL PACIENTE Y FUNCIONA ?:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C67" name="p32" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>     
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C68"  name="o32"maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">ALERGIAS CONOCIDAS:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C69"  name="p33" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>	
			<option value=""></option>	
         </select>
      </td>    
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C70" name="o33" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">VIA AÉREA DIFICIL / RIESGO DE ASPIRACION:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C71" name="p34" style="width:40%;" title="32" onblur="guardarContenidoDocumento()"  >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C72"  name="o34"maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();" />
	  </td>
    </tr>
	<tr class="titulos2">
      <td width="90%" colspan="2">3-ANTES DE LA INCISION
      </td>  
	<td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr>                         
    <tr class="estiloImput">
      <td class="estiloImputDer">TODOS LOS MIEMBROS DEL EQUIPO ESTAN PRESENTES:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C73"  name="p35" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C74"  name="o35" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">CIRUJANO, INSTRUMENTADOR Y CIRCULANTE CONFIRMAN: SU NOMBRE, FUNCION E IDENTIDAD DEL PACIENTE, SITIO OPERATORIO DEMARCADO, PROCEDIMIENTO QUIRURGICO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C75" name="p36"  style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>                                                    
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C76" name="o36" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  " />
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">EL INSTRUMENTADOR VERIFICA QUE TODO EL INSTRUMENTAL Y LOS EQUIPOS A UTILIZAR SE ENCUENTREN FUNCIONANDO CORRECTAMENTE:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C77"  name="p37" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>   
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C78"  name="o37"maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr>
	<tr class="titulos2">
      <td width="90%" colspan="2">PREVISION DE EVENTOS CRITICOS
      </td> 
	  <td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EL EQUIPO CONOCE LAS PROBABLES COMPLICACIONES INTRAOPERATORIAS DEL PROCEDIMIENTO :</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C79"  name="p38" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C80" name="o38" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="titulos2">
      <td width="90%" colspan="2">EQUIPO DE ENFERMERIA E INSTRUMENTADORA
      </td>   
	  <td width="10%" colspan="2">OBSERVACION
      </td>		  
    </tr> 
	 <tr class="estiloImput">
      <td class="estiloImputDer">EL INSTRUMENTADOR CONFIRMA EL ESTADO DE ESTERILIZACION DE PAQUETES QUIRURGICOS Y DISPOSITIVOS MEDICOS (CON RESULTADOS DE LOS INDICADORES) :</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C81"  name="p39" style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C82" name="o39" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">SE RESOLVIERON LAS INQUIETUDES REFERENTES AL INSTRUMENTAL O INSUMOS A UTILIZAR EN EL PROCEDIMIENTO:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C83"  name="p40"style="width:40%;" title="32" onblur=" "   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C84" name="o40" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="titulos2">
      <td width="90%" colspan="2">4- DURANTE EL PROCEDIMIENTO
      </td>      
	  <td width="10%" colspan="2">OBSERVACION
      </td>	
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">EL CIRCULANTE RECIBE, REALIZA EMBALAJE DE MUESTRAS PARA PATOLOGIA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C85" name="p41" style="width:40%;" title="32"    >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C86" name="o41" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();  "/>
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">ETIQUETADO DE LAS MUESTRAS (LECTURA DE LA ETIQUETA EN VOZ ALTA, INCLUIDO EL NOMBRE DEL PACIENTE):</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C87" name="p42" style="width:40%;" title="32"   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C88"  name="o42" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();"/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">EL INSTRUMENTADOR CONFIRMA VERBALMENTE RECUENTO DE MATERIALES E INSTRUMENTAL :</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C89" name="p43" style="width:40%;" title="32"   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C90"  name="o43"maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();" />
	  </td>
    </tr>
	<tr class="titulos2">
      <td width="90%" colspan="2">5- ANTES DE QUE EL PACIENTE SALGA A RECUPERACION
      </td>
	  <td width="10%" colspan="2">OBSERVACION
      </td>	  
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">ANTES DE QUE EL PACIENTE SALGA DE LA SALA DE CIRUGIA, CIRUJANO Y AUXILIAR DE ENFERMERIA CONFIRMAN PROCEDIMIENTO REALIZADO::</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C91"  name="p44" style="width:40%;" title="32"   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td>  
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C92"  name="o44" maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); " />
	  </td>
    </tr> 
	<tr class="estiloImput">
      <td class="estiloImputDer">SE IMPARTIO RECOMENDACIONES PARA EL PERIODO DE RECUPERACION Y/O TRATAMIENTO DEL PACIENTE POR PARTE DE ANESTESIA O CIRUGIA:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C93"  name="p45" style="width:40%;" title="32"   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option> 
			<option value=""></option>	
         </select>
      </td>
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C94" name="o45"  maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase(); "/>
	  </td>
    </tr>
	<tr class="estiloImput">
      <td class="estiloImputDer">SE VERIFICA QUE LOS REGISTROS DE ANESTESIA Y DESCRIPCION QUIRURGICA ESTEN DILIGENCIADOS EN SU TOTALIDAD:</td> 
      <td class="estiloImputIzq2">
         <select size="1" id="txt_LICH_C95"  name="p46"  style="width:40%;" title="32"   >	  
            <option value="SI">SI</option>
            <option value="NO">NO</option>
            <option value="NA">NA</option>
			<option value=""></option>	
         </select>
      </td> 
	  <td width="10%" class="estiloImputIzq2">
			<input type="text" id="txt_LICH_C96" name="o46"  maxlength="50" style="width:90%" onblur="this.value=this.value.toUpperCase();"/>
	  </td>
    </tr> 
  </table>  
   
 </td>   
</tr>   
	<tr class="titulos2" colspan="3">
      <td width="100%">Enfermeria Observacion:</td>	  
    </tr> 
    <tr class="estiloImput" colspan="3">
      <td class="estiloImput" width="100%">
        <textarea type="text" id="txt_LICH_C97"  name="nota_enfermeria"   maxlength="4000" style="width:50%"    onblur="this.value=this.value.toUpperCase(); guardarContenidoDocumento();"  onkeypress="return validarKey(event,this.id)"> </textarea>
      </td> 
    </tr>   
   <%/*
    <!-- <tr>
    	<td colspan="5" align="right">
        	<input type="button" onClick="guardarContenidoDocumento()" value="Guardar" title="btn544" class="small button blue" id="btnGuardarDocumento">                           
        </td>
      </tr> -->
   */ %>
</table>
<%/*
<!-- <table width="100%">  
  <tr>  
     <td width="100%">    
       <input type="button" onclick="cerrarDocumentClinicoSinImp()" value="FINALIZAR SIN IMPRIMIR" title="btn587" class="small button blue" id="btnFinalizarDocumento_">        
     </td>                                                       
  </tr>   
</table>   -->
*/ %>

</div>
<!-- 
<table width="100%">
           <tr class="estiloImput"> 
            
              <td colspan="1" align="CENTER">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76"  >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                  </select>              
              </td>                   
              <td colspan="1" align="CENTER">
              MOTIVO:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:27%" title="26"   >	
                          <option value="1">NINGUNA</option>
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>   
              <td colspan="1" align="CENTER">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO AL FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr>  
</table>-->
