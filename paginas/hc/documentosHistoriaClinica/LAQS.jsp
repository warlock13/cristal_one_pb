
<table width="100%">
            <tr> 
              <td class="titulos1" width="28%"  align="right">EXAMEN</td>  
			  <td class="titulos1" width="25%"  align="right">RESULTADOS</td>	
			  <td class="titulos1" width="25%"  align="right">VALOR DE REFERENCIA</td>  
              <td></td>  
			</tr>
			<tr class="camposRepInp"> 
              <td align="right">CURVA DE TOLERANCIA A LA GLUCOSA 2 HORAS</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">EL PACIENTE RECIBIO UNA CARGA DE GLUCOSA:&nbsp;&nbsp;</td>  
			  <td align="center">
			  <input type="text" id="txt_LAQS_C1" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>
				gr&nbsp;&nbsp;&nbsp;&nbsp;</td>			  
              <td align="left"><label></label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA BASAL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C2" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>70 - 105 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA BASAL UNA (1) HORAS POST CARGA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C3" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>90 - 150 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA BASAL DOS (2) HORAS POST CARGA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C4" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>80 - 115 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA BASAL TRES (3) HORAS POST CARGA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C5" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>70 - 110 mg/dl</label></td>  
			</tr>
			<tr class="camposRepInp"> 
              <td align="right">GLUCOSA PRE Y POST PRANDIAL</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>
	     	<tr class="estiloImput"> 
              <td align="right">GLICEMIA BASAL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C6" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>70 - 105 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA DOS (2) HORAS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C7" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>MENOR DE 140 mg/dl</label></td>  
			</tr>
			<tr class="camposRepInp"> 
              <td align="right">TEST DE O'SULLIVAN TAMIZAJE DE  DIABETES GESTACIONAL</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">EL PACIENTE RECIBIO UNA CARGA DE GLUCOSA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C8" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>
				gr&nbsp;&nbsp;&nbsp;&nbsp;</td>			  
              <td align="left"><label></label></td>  
			</tr>	
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA BASAL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C9" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>70 - 105 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">GLICEMIA UNA (1)  POST CARGA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C10" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>MENOR DE 140  mg/dl</label></td>  
			</tr>
			<tr class="camposRepInp"> 
              <td align="right">OTROS ANALITOS</td>  
			  <td colspan="2"></td>			  
              <td></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">COLESTEROL TOTAL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C11" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HASTA 200 mg/dl</label></td>  
			</tr> 			
			<tr class="estiloImput"> 
              <td align="right">TRIGLICERIDOS:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C12" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HASTA 150 mg/dl</label></td>  
			</tr> 
			<tr class="estiloImput"> 
              <td align="right">COLESTEROL - HDL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C13" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HOMBRES: 40 - 70 mg/dl<br/> MUJERES: 45 - 70 mg/dl </label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">COLESTEROL - LDL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C14" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>MENOR DE 130 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">LIPOPROTEINAS VLDL :&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C15" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>MENOR DE 30 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">INDICE ARTERIAL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C16" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>2.5 - 4.0</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">CREATININA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C17" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HOMBRES: 0.7 - 1.2 mg/dl<br/> MUJERES: 0.6 - 1.1 mg/dl </label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">NITROGENO UREICO:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C18" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>9 - 21 mg/dl</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">ACIDO URICO:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C19" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HOMBRES: 3.5 - 7.2 mg/dl<br/> MUJERES: 2.6 - 6.0 mg/dl </label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">UREA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C20" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>25 - 40 mg/dll</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">AMILASA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C21" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>28 - 100 mg/dll</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">TRANSAMINASAS OXALOCETICAS GOT(AST):&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C22" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HASTA 40 U/L</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">TRANSAMINASAS PIRUVICA GPT(ALT):&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C23" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HASTA 40 U/L</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">FOSFATASA ALCALINA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C24" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>ADULTOS:  68 - 270  U/L<br/> NI&Ntilde;OS: 100 - 800 U/L </label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">BILIRRUBINA TOTAL:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C25" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HASTA 1.0 U/L</label></td>  
			</tr>
			<tr class="estiloImput"> 
              <td align="right">BILIRRUBINA DIRECTA:&nbsp;&nbsp;</td>  
			  <td align="center"><input type="text" id="txt_LAQS_C26" size="100"  maxlength="10" style="width:25%" onblur="guardarContenidoDocumento()"/>mg/dl</td>			  
              <td align="left"><label>HASTA 0.2 U/L</label></td>  
			</tr>
			
</table>  
 
 
  

<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">
           <tr class="estiloImput"> 
                <td width="37%" colspan="2" class="titulosTodasMinuscu">OBSERVACION</td>                        
           </tr>     
           <tr class="estiloImput"> 
                <td  colspan="2">         
                    <textarea type="text" id="txt_LAQS_C27"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>         
                </td>  
           </tr> 
  
           
      </table> 
  </td>
</tr>   
</table>  



 
<table width="100%"  align="center">
 <tr class="estiloImput"> 
              <td colspan="1" align="right">ESTADO:
                  <select size="1" id="cmbIdEstadoFolioEdit" style="width:40%" title="76"  >
                      <option value=""></option>         
                      <option value="7">Cancelado Finalizado</option>
                      <option value="10">Reprogramado Finalizado</option>                                                                                       
                      <option value="2">Tomado</option>
					  <option value="11">Para direccion medica</option>                         
                      <option value="3">Enviado</option>      
                      <option value="5">Enviado urgente</option>                                                
                    <!--  <option value="4">Interpretado</option>  -->
                    <!--  <option value="6">Interpretado Urgente</option>  -->
                      <option value="8">Finalizado Urgente</option>                                                              
                  </select>              
              </td>                   
              <td colspan="1" align="left">
              Motivo:
                        <select size="1" id="cmbIdMotivoEstadoEdit" style="width:60%" title="26"   >	
                          <option value="1">NINGUNA</option>                                                                                   
                          <option value="3">ATRIBUIBLE AL PACIENTE</option>  
 						  <option value="4">ATRIBUIBLE A LA INSTITUCION</option>  
                          <option value="20">ORDEN MEDICA</option>                                                  
                        </select>  
              </td>                                         
              <td colspan="1" align="left">
	           <input id="btnFinalizarDocumen" class="small button blue" type="button" title="bt487" value="CAMBIAR ESTADO FOLIO" onclick="cambioEstadoFolio()">                                    
			  </td> 
           </tr> 
</table> 