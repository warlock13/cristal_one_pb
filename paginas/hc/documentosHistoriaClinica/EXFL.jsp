<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr class="camposRepInp" >
     <td width="100%">EXAMEN POSTURAL</td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">VISTA ANTERIOR</td> 
  </tr>
    <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="titulos1">
            <td width="16.6%">CABEZA</td>
            <td width="16.6%">HOMBROS</td>
            <td width="16.6%">ESPINAS ILIACAS ANTERIORES</td>
            <td width="16.6%">RODILLAS</td>
            <td width="16.6%">TOBILLOS</td>
            <td width="16.6%">PIES</td>
          </tr>   
          <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXFL_C1"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXFL_C2"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
            <td>
             <textarea type="text" id="txt_EXFL_C3"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>   
              <td>
             <textarea type="text" id="txt_EXFL_C4"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td> 
              <td>
             <textarea type="text" id="txt_EXFL_C5"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td> 
              <td>
             <textarea type="text" id="txt_EXFL_C6"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>             
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">VISTA LATERAL</td> 
  </tr>
      <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="titulos1">
            <td width="20%">CABEZA</td>
            <td width="20%">HOMBROS</td>
            <td width="20%">CIFOSIS DORSAL</td>
            <td width="20%">LORDOSIS LUMBAR</td>
            <td width="20%">PELVIS</td>
      </tr>    
      <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXFL_C7"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXFL_C8"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
            <td>
             <textarea type="text" id="txt_EXFL_C9"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
              <td>
             <textarea type="text" id="txt_EXFL_C10"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td> 
              <td>
             <textarea type="text" id="txt_EXFL_C11"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>            
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">VISTA POSTERIOR</td> 
  </tr>
      <tr>
      <td>
        <table width="100%" align="center">                   
          <tr class="titulos1">
            <td width="12.5%">CABEZA</td>
            <td width="12.5%">HOMBROS</td>
            <td width="12.5%">ESCAPULAS</td>
            <td width="12.5%">ESPINAS ILIACAS POSTERIORES SUPERIORES</td>
            <td width="12.5%">RODILLAS</td>
            <td width="12.5%">TOBILLOS</td>
            <td width="12.5%">RETRACCIONES MUSCULARES EN</td>
            <td width="12.5%">DEBILIDAD MUSCULAR EN</td>
      </tr>    
      <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXFL_C12"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXFL_C13"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
            <td>
             <textarea type="text" id="txt_EXFL_C14"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
              <td>
             <textarea type="text" id="txt_EXFL_C15"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td> 
              <td>
             <textarea type="text" id="txt_EXFL_C16"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>   
             <td>
             <textarea type="text" id="txt_EXFL_C17"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td> 
             <td>
             <textarea type="text" id="txt_EXFL_C18"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXFL_C19"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>          
        </table> 
      </td> 
  </tr>
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">SENSIBILIDAD</td> 
  </tr>
  <tr>
      <td> 
          <table width="100%" align="center">                   
                 <tr class="titulos1">
                  <td width="25%">NORMAL</td>
                  <td width="25%">HIPOESTESIA</td>
                  <td width="25%">HIPERESTESIA</td>
                  <td width="25%">ANESTESIA</td>
                </tr>
                <tr class="camposRepInp"> 
                  <td>
                      <input type="text" id="txt_EXFL_C20" maxlength="8" style="width:20%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_EXFL_C21" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_EXFL_C22" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td>
                      <input type="text" id="txt_EXFL_C23" maxlength="8" style="width:20%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
          </table> 
        </td>
    </tr> 
        <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="100%">AREA</td>
         </tr>                 
         <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXFL_C24"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr>
    <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr>
   <tr class="camposRepInp" >
     <td width="100%">PALPACION</td> 
  </tr>
    <tr>
      <td>
        <table width="100%" align="center">   
         <tr class="titulos1">
            <td width="33.3%">TEJIDOS BLANDOS</td>
            <td width="33.3%">TEJIDOS OSEOS</td>
            <td width="33.3%" rowspan="2">AYUDAS DIAGNOSTICAS</td>
         </tr>                 
          <tr class="titulos1">
            <td width="33.3%">HALLAZGOS</td>
            <td width="33.3%">HALLAZGOS</td>
         </tr>    
      <tr class="camposRepInp"> 
            <td>
             <textarea type="text" id="txt_EXFL_C25"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>
            <td>
             <textarea type="text" id="txt_EXFL_C26"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>  
            <td>
             <textarea type="text" id="txt_EXFL_C27"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>      
            </td>    
          </tr>            
        </table> 
      </td> 
  </tr>
</table>


 
 





