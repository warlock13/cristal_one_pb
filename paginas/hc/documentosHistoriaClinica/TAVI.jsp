<table cellpadding="10" width="100%" align="center">
   <tr class="camposRepInp">
      <td>
         Diagnostico de remision
      </td>
   </tr>
   <tr>
      <td>
         <input type="text" id="txt_TAVI_C1" onfocus="llamarAutocomIdDescripcionConDato('txt_TAVI_C1',57)"
            style="width:90%" maxlength="50"  onblur="guardarContenidoDocumento();">
         <img width="18px" height="18px" align="middle" title="VER52" id="idLupitaVentanitaMuni"
            onclick="traerVentanita(this.id,'','divParaVentanita','txt_TAVI_C1','16')"
            src="/clinica/utilidades/imagenes/acciones/buscar.png">
      </td>
   </tr>
   <td colspan="2">
      <hr>
   </td>
   <tr>
      <td class="camposRepInp">
         AGUDEZA VISUAL OJO IZQUIERDO
      </td>
      <td class="camposRepInp">
         AGUDEZA VISUAL OJO DERECHO
      </td>
   </tr>
   <tr>
      <td>
         <table width="100%">
            <tr>
               <td align="CENTER">
                  <input type="text" id="txt_TAVI_C2" style="width:90%" maxlength="50"  onblur="guardarContenidoDocumento();"/>
               </td>
               <td>
                  <select id="txt_TAVI_C3" style="width:90%" onblur="guardarContenidoDocumento();">
                     <option value=""></option>
                     <option value="NORMAL">NORMAL</option>
                     <option value="ANORMAL">ANORMAL</option>
                  </select>
               </td>
               <td>
                  <textarea type="text" id="txt_TAVI_C4" placeholder="Observaciones..." rows="6" style="width:95%"
                     onblur="v28(this.value,'txt_TAVI_C4'); guardarContenidoDocumento();"
                     onkeypress="return validarKey(event,this.id)"></textarea>
               </td>
            </tr>
         </table>
      </td>
      <td>
         <table width="100%">
            <tr>
               <td align="CENTER">
                  <input type="text" id="txt_TAVI_C5" style="width:90%" maxlength="50"  onblur="guardarContenidoDocumento();"/>
               </td>
               <td>
                  <select id="txt_TAVI_C6" style="width:90%" onblur="guardarContenidoDocumento();">
                     <option value=""></option>
                     <option value="NORMAL">NORMAL</option>
                     <option value="ANORMAL">ANORMAL</option>
                  </select>
               </td>
               <td>
                  <textarea type="text" id="txt_TAVI_C7" placeholder="Observaciones..." rows="6" style="width:95%"
                     onblur="v28(this.value,'txt_TAVI_C7'); guardarContenidoDocumento();"
                     onkeypress="return validarKey(event,this.id)"></textarea>
               </td>
            </tr>
         </table>
      </td>
   </tr>
   <td colspan="2">
      <hr>
   </td>
   <tr>
      <td colspan="2" class="camposRepInp">
         EXAMEN DE PUPILAS
      </td>
   </tr>
   <tr>
      <td>
         <textarea type="text" id="txt_TAVI_C8" placeholder="Observaciones..." rows="4"
            style="width:95%" onblur="v28(this.value,'txt_TAVI_C8'); guardarContenidoDocumento();"
            onkeypress="return validarKey(event,this.id)"></textarea>
      </td>
      <td>
         <select id="txt_TAVI_C9" style="width:30%" onblur="guardarContenidoDocumento();">
            <option value=""></option>
            <option value="NORMAL">NORMAL</option>
            <option value="ANORMAL">ANORMAL</option>
         </select>
      </td>
   </tr>
   <tr>
      <td colspan="2">
         <hr>
      </td>
   </tr>
   <tr>
      <td colspan="2" class="camposRepInp">
         EXAMEN EXTERNO (PARPADOS)
      </td>
   </tr>
   <tr>
      <td>
         <textarea type="text" id="txt_TAVI_C10" placeholder="Observaciones..." rows="4"
            style="width:95%" onblur="v28(this.value,'txt_TAVI_C10'); guardarContenidoDocumento();"
            onkeypress="return validarKey(event,this.id)"></textarea>
      </td>
      <td>
         <select id="txt_TAVI_C11" style="width:30%" onblur="guardarContenidoDocumento();">
            <option value=""></option>
            <option value="NORMAL">NORMAL</option>
            <option value="ANORMAL">ANORMAL</option>
         </select>
      </td>
   </tr>
   <tr>
      <td colspan="2">
         <hr>
      </td>
   </tr>
   <tr>
      <td colspan="2" class="camposRepInp">
         EXAMEN EXTERNO (APARATO LAGRIMAL)
      </td>
   </tr>
   <tr>
      <td>
         <textarea type="text" id="txt_TAVI_C12" placeholder="Observaciones..." rows="4"
            style="width:95%" onblur="v28(this.value,'txt_TAVI_C12'); guardarContenidoDocumento();"
            onkeypress="return validarKey(event,this.id)"></textarea>
      </td>
      <td>
         <select id="txt_TAVI_C13" style="width:30%" onblur="guardarContenidoDocumento();">
            <option value=""></option>
            <option value="NORMAL">NORMAL</option>
            <option value="ANORMAL">ANORMAL</option>
         </select>
      </td>
   </tr>
   <tr>
      <td colspan="2">
         <hr>
      </td>
   </tr>
</table>