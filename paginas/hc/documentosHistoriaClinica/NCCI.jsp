
<div id="plantilla_NCCI">
<table width="100%" cellpadding="0" cellspacing="0" align="center">
  <tr class="camposRepInp">
    <td width="100%">Nota de enfermeria</td>
  <tr>
  <tr class="estiloImput">
    <td>
      <textarea type="text" rows="10" id="txt_NCCI_C1"  name="nota_enfermeria" style="width:95%" 
        onblur="v28(this.value,this.id); guardarContenidoDocumento()"> </textarea>
    </td>
  </tr>
  <tr>
    <td>
      <div style="display:none">
        <table width="100%" align="center">
          <tr class="titulos">
            <td width="20%">Frecuencia Cardiaca</td>
            <td width="20%">Frecuencia Respiratoria</td>
            <td width="20%">Temperatura</td>
            <td width="20%">Tension Arterial</td>
            <td width="20%">Sat. Oxigeno</td>
          </tr>
          <tr class="estiloImput">
            <td>
              <input type="text" id="txt_NCCI_C2" name="frec_cardiaca"  maxlength="5" style="width:20%" disabled="disabled"
                onblur="guardarContenidoDocumento()" /> X MIN
            </td>
            <td>
              <input type="text" id="txt_NCCI_C3"  name="frec_respiratoria"  maxlength="5" style="width:20%" disabled="disabled"
                onblur="guardarContenidoDocumento()" /> X MIN
            </td>
            <td>
              <input type="text" id="txt_NCCI_C4"  name="temperatura"  maxlength="5" style="width:20%" disabled="disabled"
                onblur="guardarContenidoDocumento()" />° Grados
            </td>
            <td>
              <input type="text" id="txt_NCCI_C5" name="tension_art"  maxlength="5" style="width:20%" disabled="disabled"
                onblur="guardarContenidoDocumento()" /> mmHG
            </td>
            <td>
              <input type="text" id="txt_NCCI_C6" name="sat_oxigeno"   maxlength="5" style="width:20%" disabled="disabled"
                onblur="guardarContenidoDocumento()" />%
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<table width="100%" align="center">
  <tr class="estiloImput">
    <td width="20%" class="estiloImputDer">Evento Adverso:
      <select size="1" id="txt_NCCI_C7"  name="evento_preg" style="width:20%;" title="32" onblur="guardarContenidoDocumento()"
        >
        <option value="SI">SI</option>
        <option value="NO">NO</option>
        <option value="NA">NA</option>
      </select>
    </td>
    <td width="80%" class="estiloImputIzq2">
      <input type="text" id="txt_NCCI_C8"  name="evento_observ" maxlength="500" style="width:80%"
        onblur="v28(this.value,this.id);  guardarContenidoDocumento();" />
    </td>
  </tr>
</table>

</div>
<!-- <table width="100%">  
  <tr>  
     <td width="100%">    
       <input type="button" onclick="cerrarDocumentClinicoSinImp()" value="FINALIZAR SIN IMPRIMIR" title="btn587" class="small button blue" id="btnFinalizarDocumento_">        
     </td>                                                       
  </tr>   
</table> 
  -->