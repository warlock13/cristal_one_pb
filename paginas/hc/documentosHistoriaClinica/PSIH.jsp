  <table width="100%"  cellpadding="0" cellspacing="0"  align="center">


    <tr class="camposRepInp">
             <td  bgcolor="#c0d3c1" >DATOS DE IDENTIFICACI&Oacute;N DEL PACIENTE</td> 
          </tr>

          <tr class="estiloImput">

            <td >         
                    <textarea type="text" id="txt_PSIH_C63" rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()" ></textarea>         
                </td>  

          </tr>



<tr class="camposRepInp">
 
</tr>
</table>  
  
<table width="100%"  align="center">
<tr>
  <td width="100%" >
  <table width="100%" align="center">

       <tr class="camposRepInp"> 
        <td colspan="2" bgcolor="#c0d3c1" >EVALUACI&Oacute;N CLINICA PSICOLOGICA</td>
      </tr>

           <tr class="camposRepInp"> 
                <td width="37%">FACTORES DESENCADENANTES</td> 
                <td width="37%">MANTENEDORES DEL PROBLEMA</td>
				</tr>     
           <tr class="estiloImput"> 
                <td>         
                    <textarea type="text" id="txt_PSIH_C1"  placeholder="Hace referencia a los hechos que produjeron el problema, tiempo en el que aparecieron, por que no ha podido solucionar su problema." rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()" ></textarea>         
                </td>  
                <td>
                    <textarea type="text" id="txt_PSIH_C2"  placeholder="Que personas, situaciones o cosas empeoran o mantienen las dificultades."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>         
                </td> 
           </tr>   
           <tr class="camposRepInp"> 
                <td colspan="2">ANTECEDENTES PERSONALES Y FAMILIARES</td>                   
                        
           </tr>     
           <tr class="estiloImput"> 
                <td colspan="2">         
                    <textarea type="text" id="txt_PSIH_C3"  placeholder="Estados, situaciones o vivencias que la persona o sus familiares han padecido y pueden estar influyendo en la sintomatologia actual. Tener en cuenta la manifestacion de tipos de violencias en la vida familiar, de pareja, laboral o social. Asi mismo, la manifestacion de conducta suicida en la familia, personas significativas o en el mismo paciente."  rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>         
                </td> 
           </tr>  
           <tr class="camposRepInp">
  <td colspan="2"  bgcolor="#c0d3c1" >EXAMEN MENTAL</td>
          </tr>     
 <tr class="camposRepInp">
  <td colspan="2"  >APARIENCIA PERSONAL</td>
          </tr>   

          <tr>
            <td colspan="2">

              <table width="100%"   align="center">
                <tr class="estiloImput">
                  <td><b>APARIENCIA PERSONAL</b></td>
                  <td><b>EXPRESI&Oacute;N FACIAL<b></td>
                  <td><b>CONTACTO VISUAL</b></td>
                  <td><b>GESTOS PARTICULARES</b></td>

                </tr>

                <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_PSIH_C4" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="ADECUADA">ADECUADA</option>
                    <option value="INADECUADA">INADECUADA</option>
                 </select>               
              </td>                
              <td>
                 <select size="1" id="txt_PSIH_C5" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="ALERTA">ALERTA</option>
                    <option value="VIGILANTE">VIGILANTE</option>
                    <option value="DEPRESIVA">DEPRESIVA</option>
                 </select>               
              </td>  
              <td>
                 <select size="1" id="txt_PSIH_C6" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="DIRECTO">DIRECTO</option>
                    <option value="ESQUIVO">ESQUIVO</option>
                 </select>               
              </td>
              <td>
                 <select size="1" id="txt_PSIH_C7" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="TICS">TICS</option>
                    <option value="CICATRICES">CICATRICES</option>
                    <option value="OTROS">OTROS</option>
                 </select>               
              </td>
            </tr>
                

              </table>

            </td>

          </tr>

          <tr class="camposRepInp">
  <td colspan="2"  >FUNCIONES COGNITIVAS</td>
          </tr>  

          <tr>

            <td colspan="2">
               <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                <tr class="estiloImput">
                  <td width="25%"><b>ATENCI&Oacute;N</b></td>
                  <td width="25%"><b>CONCIENCIA<b></td>
                  <td colspan="2"><b>ORIENTACI&Oacute;N</b></td>

                </tr>

                <tr class="estiloImput"> 
              <td>
                 <select size="1" id="txt_PSIH_C8" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="NORMAL">NORMAL</option>
                    <option value="AUMENTADA">AUMENTADA</option>
                    <option value="DISMINUIDA">DISMINUIDA</option>
                 </select>               
              </td>

              <td>
                 <select size="1" id="txt_PSIH_C9" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="VIGILIA">VIGILIA</option>
                    <option value="HIPERVIGILIA">HIPERVIGILIA</option>
                    <option value="SOMNOLENCIA">SOMNOLENCIA</option>
                    <option value="ESTUPOR">ESTUPOR</option>
                 </select>               
              </td>  

              <td colspan="2">
                    <textarea type="text" id="txt_PSIH_C10"  placeholder="En tiempo, espacio y persona
- Auto psiquico: nombre y apellido, edad, etc.
- Alo psiquica: ciudad, sitio , fecha."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>         
                </td> 

                </tr>

                 <tr class="estiloImput">
                  <td ><b>MEMORIA</b></td>
                  <td ><b>PENSAMIENTO<b></td>
                  <td width="25%"><b>INTELIGENCIA</b></td>
                  <td width="25%"><b>JUICIO Y RACIOCINIO</b></td>

                </tr>

                <tr class="estiloImput">
                  <td> <u>REMOTA</u>
                    <select size="1" id="txt_PSIH_C11" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="ADECUADA">ADECUADA</option>
                      <option value="INADECUADA">INADECUADA</option>
                    </select>
                      <br><br>
                     <u>RECIENTE</u>
                    <select size="1" id="txt_PSIH_C12" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="ADECUADA">ADECUADA</option>
                      <option value="INADECUADA">INADECUADA</option>
                    </select>
                    <br><br>
                    <u>INMEDIATA</u>
                    <select size="1" id="txt_PSIH_C13" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="ADECUADA">ADECUADA</option>
                      <option value="INADECUADA">INADECUADA</option>
                    </select>
                  </td>

                   <td> <u>PRODUCCI&Oacute;N</u>
                    <select size="1" id="txt_PSIH_C14" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="ACELERADO">ACELERADO</option>
                      <option value="ENLENTECIDO">ENLENTECIDO</option>
                      <option value="RETARDADO">RETARDADO</option>
                    </select>
                      <br><br>
                     <u>CONTINUIDAD</u>
                    <select size="1" id="txt_PSIH_C15" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="COHERENTE">COHERENTE</option>
                      <option value="BLOQUEADO">BLOQUEADO</option>
                      <option value="PERSEVERANTE">PERSEVERANTE</option>
                    </select>
                    <br><br>
                    <u>CONTENIDO</u>
                    <select size="1" id="txt_PSIH_C16" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="OBSESIVO">OBSESIVO</option>
                      <option value="FANTASIOSO">FANTASIOSO</option>
                      <option value="REALISTA">REALISTA</option>
                    </select>
                  </td>

                  <td>

                    <u>ABSTRACCI&Oacute;N</u><br>
                    <b>Ej:</b> Hijo de tigre sale pintado<br> vale pajaro en mano que 100 volando...<br>
                    <select size="1" id="txt_PSIH_C17" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="SI">SI</option>
                      <option value="NO">NO</option>
                    </select>

                    <br><br>
                    <u>CAPACIDAD DE CALCULO</u><br>
                     <b>Ej:</b> 100-3=, 45+12=<br>Mas de 7 errores indican problemas de calculo.<br>
                     <select size="1" id="txt_PSIH_C18" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="SI">SI</option>
                      <option value="NO">NO</option>
                    </select>
                  </td>

                  <td>
                    <u>REALIDAD INTERNA (INTROSPECCI&Oacute;N)</u><br>
                    Habla sobre su enfermedad y limitaciones<br>
                    <select size="1" id="txt_PSIH_C19" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="SI">SI</option>
                      <option value="NO">NO</option>
                    </select>
                    <br><br>
                    <u>REALIDAD EXTERNA</u><br>
                     Desconfianza y culpabilidad hacia el exterior<br>
                     <select size="1" id="txt_PSIH_C20" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="SI">SI</option>
                      <option value="NO">NO</option>
                    </select>

                     <br><br>
                    <u>PROSPECCI&Oacute;N</u><br>
                     Tiene vision de objetivos y metas futuras<br>
                      En esta area es importante y necesaria la identificacion<br> 
                      de ideacion o intento suicida en el presente.<br>
                     <select size="1" id="txt_PSIH_C21" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="SI">SI</option>
                      <option value="NO">NO</option>
                    </select><br>
                  </td>

                </tr>

                <tr class="camposRepInp">
                  <td >EVALUACI&Oacute;N EMOCIONAL</td>
                  <td >MOTRICIDAD</td>
                  <td colspan="2">LENGUAJE</td>
              </tr>

              <tr class="estiloImput">
                  <td ><b>EVALUACI&Oacute;N EMOCIONAL</b></td>
                  <td ><b>MOTRICIDAD</b></td>
                  <td ><b>COMUNICACI&Oacute;N VERBAL</b></td>
                  <td ><b>COMUNICACI&Oacute;N NO VERBAL</b></td>
              </tr>



               <tr class="estiloImput">
                  <td>
                <br>
                    
                 <select size="1" id="txt_PSIH_C22" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="ALEGRIA">ALEGRIA</option>
                    <option value="EUFORIA">EUFORIA</option>
                    <option value="LABILIDAD">LABILIDAD</option>
                    <option value="MIEDO">MIEDO</option>
                    <option value="IRRITABILIDAD">IRRITABILIDAD</option>
                    <option value="DEPRESION">DEPRESION</option>
                    <option value="ANSIEDAD">ANSIEDAD</option>
                    <option value="AGRESION">AGRESION</option>
                 </select>               
              </td>  

               <td><br>
                 <select size="1" id="txt_PSIH_C23" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="NORMAL">NORMAL</option>
                    <option value="DISMINUIDA">DISMINUIDA</option>
                    <option value="AUMENTADA">AUMENTADA</option>
                 </select>               
              </td>  


               <td><br>
                 
                    <u>CONTENIDO DEL LENGUAJE</u>
                    <select size="1" id="txt_PSIH_C24" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="RICO">RICO</option>
                      <option value="POBRE">POBRE</option>
                      <option value="COORDINADO">COORDINADO</option>
                      <option value="LENTO">LENTO</option>
                      <option value="RAPIDO">RAPIDO</option>
                    </select>
                      <br><br>
                     <u>TONO DE VOZ</u>
                    <select size="1" id="txt_PSIH_C25" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="AUDIBLE">AUDIBLE</option>
                      <option value="NO_AUDIBLE">NO AUDIBLE</option>
                      <option value="SUAVE">SUAVE</option>
                      <option value="FUERTE">FUERTE</option>
                      <option value="VARIABLE">VARIABLE</option>
                      <option value="AGRESIVO">AGRESIVO</option>
                      <option value="ESTEREOTIPO">ESTEREOTIPO</option>
                      <option value="MONOTONO">MONOTONO</option>
                    </select>


              </td>  

               <td><br>
                 
                    <select size="1" id="txt_PSIH_C26" style="width:70%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="ALTERACIONES_EMOCIONALES">ALTERACIONES EMOCIONALES</option>
                      <option value="ALTERACIONES_ORGANICAS">ALTERACIONES ORGANICAS</option>
                      <option value="GESTOS">GESTOS</option>
                      <option value="REACCIONES">REACCIONES</option>
                    </select>


              </td>
               <br><br>

            </tr>


           

             <tr class="camposRepInp">
                  <td colspan="4" >EVALUACI&Oacute;N DE SENSOPERCEPCI&Oacute;N</td>
              </tr>

              <tr  class="estiloImput">
                  <td><br><b>ILUSIONES</b>

                    <br><br>
                    <u>VISUALES</u>
                     <select size="1" id="txt_PSIH_C27" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>AUDITIVAS</u>
                     <select size="1" id="txt_PSIH_C28" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>TACTILES</u>
                     <select size="1" id="txt_PSIH_C29" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>GUSTATIVAS</u>
                     <select size="1" id="txt_PSIH_C30" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>OLFATIVAS</u>
                     <select size="1" id="txt_PSIH_C31" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                  </td>
                  <td>

                    <br><b>ALUCINACIONES</b>

                    <br><br>
                    <u>VISUALES</u>
                     <select size="1" id="txt_PSIH_C32" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>AUDITIVAS</u>
                     <select size="1" id="txt_PSIH_C33" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>TACTILES</u>
                     <select size="1" id="txt_PSIH_C34" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>GUSTATIVAS</u>
                     <select size="1" id="txt_PSIH_C35" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                     <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>
                    <u>OLFATIVAS</u>
                     <select size="1" id="txt_PSIH_C36" style="width:25%;" title="32" onblur="guardarContenidoDocumento()"   >    
                    <option value="NO">NO</option>
                        <option value="SI">SI</option>
                    </select><br><br>


                    


                  </td>

                  <td colspan="2">

                    <br><b>DISTORSION DEL ESQUEMA PERSONAL</b>
                    <br><br>
                    <textarea type="text" id="txt_PSIH_C37"  placeholder="Despersonalizacion"  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>

                  </td>

              </tr>


             <tr class="camposRepInp">
                  <td colspan="2">FUNCIONES SOMATICAS</td>
                  <td colspan="2">INTEGRIDAD COMPORTAMENTAL</td>
              </tr>

              <tr class="estiloImput">

                  <td><br><b>SUE&Ntilde;O</b>
                    <br><br>
                 
                    <select size="1" id="txt_PSIH_C38" style="width:70%;" title="32" onblur="guardarContenidoDocumento()"   >    
                      <option value="NORMAL">NORMAL</option>
                      <option value="DIFICULTAD_PARA_CONCILIAR">DIFICULTAD PARA CONCILIAR</option>
                      <option value="DIFICULTAD_PARA_RECONCILIAR">DIFICULTAD PARA RECONCILIAR</option>
                      <option value="DESPERTAR_TEMPRANO">DESPERTAR TEMPRANO</option>
                      <option value="PESADILLAS">PESADILLAS</option>
                    </select>


              </td>

               <td><br><b>APETITO</b>
                    <br><br>
                 
                    <select size="1" id="txt_PSIH_C39" style="width:70%;" title="32" onblur="guardarContenidoDocumento()"   > 
                      <option value="NORMAL">NORMAL</option>
                      <option value="DISMINUIDO">DISMINUIDO</option>
                      <option value="AUMENTADO">AUMENTADO</option>
                    </select>


              </td>

              <td colspan="2">
                 <textarea type="text" id="txt_PSIH_C40"  placeholder="- Descuido en el auto cuidado
- Olvido frecuente de intensiones
- Desinhibicion motora
- Ruptura de normas sociales
- Irresponsabilidad con el dinero
- Desconfianza exagerada
- Autoagresion
- Disminucion de la sociabilidad
- Ausencia de sentimiento de enfermedad o rechazo al tratamiento."  rows="10" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea>
              </td>


              </tr>

              <tr class="camposRepInp">
                <td colspan="4"  bgcolor="#c0d3c1" >EVALUACION DE AREAS DE DESEMPE&Ntilde;O</td>
              </tr>
              <tr class="camposRepInp">
                  <td>AREA PERSONAL</td>
                  <td>AREA ACADEMICA</td>
                  <td>AREA LABORAL</td>
                  <td>AREA FAMILIAR</td>
              </tr>


              <tr class="estiloImput">
                <td><textarea type="text" id="txt_PSIH_C41"  placeholder="autovaloracion y actitudes frente a la vida (sentido, calidad y proyecto de vida) (Como me veo y me siento)."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 <td><textarea type="text" id="txt_PSIH_C42"  placeholder="Institucion Educativa, grado, (adaptacion al ambiente escolar, proceso de aprendizaje, satisfaccion y rendimiento),"  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C43"  placeholder="Sitio de Trabajo, ocupacion, tiempo de permanencia, (Adaptacion, satisfaccion y rendimiento)."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C44"  placeholder="tipologia y composicion familiar, Identificacion de la dinamica y funcionalidad de relaciones intrafamiliares."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
              </tr>



              <tr class="camposRepInp">
                  <td>AREA SOCIAL</td>
                  <td>AREA DE PAREJA</td>
                  <td>AREA SEXUAL</td>
                  <td>AREA ECONOMICA</td>
                  
              </tr>

               <tr class="estiloImput">
                <td><textarea type="text" id="txt_PSIH_C45"  placeholder="caracteristicas de sociabilidad, grupo(s) al que pertenece, tipo de actividades sociales, habilidades sociales, satisfaccion y relaciones interpersonales."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 <td><textarea type="text" id="txt_PSIH_C46"  placeholder="tipo de relacion, dinamica de interaccion con la pareja informacion sobre companero(a)."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C47"  placeholder="Desarrollo psicosexual, desempeno y vivencia de su sexualidad. (puede incluirse en el area de pareja)."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C48"  placeholder="ingresos economicos y fuente de ingresos, aspiraciones nivel de vida."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
              </tr>

              <tr class="camposRepInp">
                <td>AREA RELIGIOSA</td>
                  <td>AREA AFECTIVA (SOLO PARA NI&Ntilde;OS)</td>
                  <td>JUEGO Y RECREACION (SOLO PARA NI&Ntilde;OS)</td>
                  <td></td>
              </tr>

               <tr class="estiloImput">
                 <td><textarea type="text" id="txt_PSIH_C49"  placeholder="tipo de Religion, practicas asociadas, creencias y valores."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C50"  placeholder="Nivel y manifestacion de emociones y afectos, caracteristicas generales."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C51"  placeholder="Tendencias y preferencias."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
              </tr>





                </table>

            </td>


          </tr>

          <tr>
            <td colspan="2">
                <table width="100%" align="center">


               <tr class="camposRepInp">
                <td bgcolor="#c0d3c1" >RED DE APOYO</td>
                <td bgcolor="#c0d3c1" >EXPECTATIVAS FRENTE AL PROCESO DE INTERVENCI&Oacute;N</td>
                <td bgcolor="#c0d3c1" >PLAN DE MANEJO</td>
              </tr>

              <tr class="estiloImput">
                 <td><textarea type="text" id="txt_PSIH_C52"  placeholder="A que personas, instituciones, organizaciones han sido apoyo o se ha pedido ayuda en relacion al problema."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C53"  placeholder="Que espera del presente proceso."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C54"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
              </tr>

              <tr class="camposRepInp">
                <td colspan="3" bgcolor="#c0d3c1" >PARA NI&Ntilde;OS</td>
              </tr>

               <tr class="camposRepInp">
                <td  >GESTACI&Oacute;N</td>
                <td >PARTO</td>
                <td  >RECIEN NACIDO</td>
              </tr>

               <tr class="estiloImput">
                 <td><textarea type="text" id="txt_PSIH_C55"  placeholder="Descripcion detallada de aspectos relevantes durante la gestacion consumo de SPA, alteraciones emocionales o fisicas de la madre o el feto. Nivel de aceptacion del embarazo (en los dos padres)."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C56"  placeholder="Descripcion del momento y tipo de atencion en el parto, caracteristicas generales. Edad gestacional, posibles complicaciones del parto, condiciones del nino al nacer etc."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C57"  placeholder="caracteristicas de habitos alimenticios, de sueno, conducta motora, estado emocional, lactante o no, cuidado afectivo y sensitivo, etc."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
              </tr>

              <tr class="camposRepInp">
                <td  >INFANCIA</td>
                <td >TECNICAS DISCIPLINARIAS</td>
              </tr>

               <tr class="estiloImput">
                 <td><textarea type="text" id="txt_PSIH_C58"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C59"  placeholder=""  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
               
              </tr>

              <tr class="camposRepInp">
                <td colspan="3" bgcolor="#c0d3c1" >PARA ADOLESCENTES</td>
              </tr>

               <tr class="camposRepInp">
                <td  >CONSUMO DE SPA</td>
                <td >IDENTIFICACI&Oacute;N, MANEJO Y SEGUIMIENTO DE NORMAS Y LIMITES</td>
                <td  >IDEACI&Oacute;N SUICIDA Y ACTITUDES FRENTE A LA VIDA</td>
              </tr>
              
              <tr class="estiloImput">
                 <td><textarea type="text" id="txt_PSIH_C60"  placeholder="Tipo de sustancias, evolucion, frecuencia, severidad y afectacion."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C61"  placeholder="Caracteristicas del o la joven y actitud frente a las normas e imágenes de autoridad."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
                 </td>
                 <td><textarea type="text" id="txt_PSIH_C62"  placeholder="Hay episodios de tristeza, pensamientos e ideas de quitarse la vida, encuentra sentido de vida."  rows="5" cols="50"  maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"></textarea></td>
              </tr>


            </table>


            </td>

          </tr>


      </table> 
  </td>
</tr>   
</table>

 