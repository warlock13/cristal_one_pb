<div id="plantilla_ECOF">
  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="60%" colspan="2">
        <table width="95%" align="center">
          <tr>
            <td width="80%">
              <table width="100%" cellpadding="0" cellspacing="0" align="center">
                <tr class="camposRepInp">
                  <td colspan="5">
                  </td>
                </tr>
                <tr class="camposRepInp">
                  <td colspan="5">AGUDEZA VISUAL LEJANA</td>
                </tr>
                <tr class="camposRepInp">
                  <td width="8%"></td>
                  <td width="40%">Sin Correccion</td>
                  <td width="0%" align="left">&nbsp;</td>
                  <td width="40%">Con correccion</td>
                  <td width="12%">Tipo</td>
                </tr>
                <tr class="estiloImput">
                  <td>Ojo derecho</td>
                  <td>
                    <select size="1" id="txt_ECOF_C1" name="agudeza_visual_od_sin" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"
                      >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>
                    </select>
                    &nbsp;
                    <input type="text" id="txt_ECOF_C18" name="agudeza_visual_od_sin_obs" style="width:45%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" />
                  </td>
                  <td align="left">

                  </td>
                  <td>
                    <select size="1" id="txt_ECOF_C3" name="agudeza_visual_od_con" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"
                      >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>

                    </select>
                    &nbsp;
                    <input type="text" id="txt_ECOF_C19" name="agudeza_visual_od_con_obs" style="width:45%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" />
                  </td>
                  <td>
                    <select size="1" id="txt_ECOF_C4" name="agudeza_visual_od_con_tipo" style="width:98%" title="32" onblur="guardarContenidoDocumento()"
                      >
                      <option value=""></option>
                      <option value="PINHOLE">PINHOLE</option>
                      <option value="LENTES">LENTES</option>
                    </select>
                  </td>
                </tr>
                <tr class="estiloImput">
                  <td>Ojo Izquierdo</td>
                  <td>
                    <select size="1" id="txt_ECOF_C5" name="agudeza_visual_oi_sin" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"
                      >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>
                    </select>
                    &nbsp;
                    <input type="text" id="txt_ECOF_C20" name="agudeza_visual_oi_sin_obs" style="width:45%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" />
                  </td>
                  <td align="left">

                  </td>
                  <td>
                    <select size="1" id="txt_ECOF_C7" name="agudeza_visual_oi_con" style="width:50%;" title="32" onblur="guardarContenidoDocumento()"
                      >
                      <option value=""></option>
                      <option value="20/20">20/20</option>
                      <option value="20/25">20/25</option>
                      <option value="20/30">20/30</option>
                      <option value="20/40">20/40</option>
                      <option value="20/50">20/50</option>
                      <option value="20/60">20/60</option>
                      <option value="20/70">20/70</option>
                      <option value="20/80">20/80</option>
                      <option value="20/100">20/100</option>
                      <option value="20/125">20/125</option>
                      <option value="20/140">20/140</option>
                      <option value="20/150">20/150</option>
                      <option value="20/160">20/160</option>
                      <option value="20/200">20/200</option>
                      <option value="20/250">20/250</option>
                      <option value="20/300">20/300</option>
                      <option value="20/320">20/320</option>
                      <option value="20/400">20/400</option>
                      <option value="20/500">20/500</option>
                      <option value="20/640">20/640</option>
                      <option value="20/800">20/800</option>
                      <option value="20/1000">20/1000</option>
                      <option value="20/1280">20/1280</option>
                      <option value="20/1600">20/1600</option>
                      <option value="20/2000">20/2000</option>
                      <option value="20/2480">20/2480</option>
                      <option value="20/2560">20/2560</option>
                      <option value="20/3200">20/3200</option>
                      <option value="Cuenta dedos">Cuenta dedos</option>
                      <option value="Movimiento de manos">Movimiento de manos</option>
                      <option value="Proyeccion y percepcion de luz">Proyecci&oacute;n y percepci&oacute;n de luz
                      </option>
                      <option value="No percibe luz">No percibe luz</option>
                      <option value="Percepcion de bultos">Percepci&oacute;n de bultos</option>
                      <option value="Riesgo no evaluado">Riesgo no evaluado</option>
                    </select>
                    &nbsp;
                    <input type="text" id="txt_ECOF_C21" name="agudeza_visual_oi_con_obs" style="width:45%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" />
                  </td>
                  <td>
                    <select size="1" id="txt_ECOF_C8" name="agudeza_visual_oi_con_tipo" style="width:98%" title="32" onblur="guardarContenidoDocumento()"
                      >
                      <option value=""></option>
                      <option value="PINHOLE">PINHOLE</option>
                      <option value="LENTES">LENTES</option>
                    </select>
                  </td>
                </tr>
              </table>
            </td>
            <td width="20%">
              <table width="100%" border="1" cellpadding="0" cellspacing="7" align="center">
                <tr class="camposRepInp">
                  <td width="95%">AGUDEZA VISUAL CERCANA</td>
                </tr>
                <tr class="estiloImput">
                  <td align="center"><input type="text" id="txt_ECOF_C2" name="agudeza_visual_od_factor" style="width:85%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /></td>
                </tr>
                <tr>
                  <td align="center"><input type="text" id="txt_ECOF_C6" name="agudeza_visual_oi_factor" style="width:85%;" size="3"
                      onblur="guardarContenidoDocumento()" onkeypress="return validarKey(event,this.id)" /></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="100%" border="1" cellpadding="1" cellspacing="1" align="center">
    <tr class="camposRepInp">
      <td width="25%">PUPILAS</td>
    <tr>
    <tr class="estiloImput">
      <td><input type="text" id="txt_ECOF_C9" name="pupilas" maxlength="1000" style="width:70%" onblur="guardarContenidoDocumento()" />
      </td>
    </tr>
    <tr class="camposRepInp">
      <td>
        <input id="btnProcedimiento" type="button" class="small button blue" value="PRECONSULTA" title="B765"
          onClick="modificarCRUD('pacientePreconsulta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')" />
      </td>
    </tr>
  </table>

  <table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="1000%" class='classTitulo'>.
      </td>
    </tr>
  </table>

  <table width="100%" border="1" cellpadding="1" cellspacing="1" align="center">
    <tr class="camposRepInp">
      <td width="25%">Presi&oacute;n intra Ocular Ojo Derecho</td>
      <td width="25%">Presi&oacute;n intra Ocular Ojo Izquierdo</td>
    </tr>
    <tr class="estiloImput">
      <td>
        mmHg:<input type="text" id="txt_ECOF_C10" name="presion_intra_ocula_od" style="width:20%" size="20" maxlength="5"
          onblur="guardarContenidoDocumento()" />&nbsp;&nbsp;
        Hrs:<input type="text" id="txt_ECOF_C11" name="presion_intra_ocula_od_hr" style="width:20%" size="20" maxlength="5"
          onblur="guardarContenidoDocumento()" />
      </td>
      <td>
        mmHg:<input type="text" id="txt_ECOF_C12" name="presion_intra_ocula_oi" style="width:20%" size="20" maxlength="5"
          onblur="guardarContenidoDocumento()" />&nbsp;&nbsp;
        Hrs:<input type="text" id="txt_ECOF_C13" name="presion_intra_ocula_oi_hr" style="width:20%" size="20" maxlength="5"
          onblur="guardarContenidoDocumento()" />
      </td>
    </tr>
    <tr class="camposRepInp">
      <td width="25%">Subjetivo</td>
      <td width="25%">Objetivo</td>
    </tr>
    <tr class="estiloImput">
      <td>
        <textarea type="text" id="txt_ECOF_C14" name="subjetivo" placeholder="Motivo por el cual el paciente asiste a control..."
          size="4000" rows="4" maxlength="2000" style="width:95%" 
          onblur="v28(this.value,'txt_ECOF_C14'); guardarContenidoDocumento();"
          onkeypress="return validarKey(event,this.id)"> </textarea>
      </td>
      <td>
        <textarea type="text" id="txt_ECOF_C15" name="objetivo"
          placeholder="Examenes y demas pertientes, espacio para la evaluacion medica." size="4000" rows="4"
          maxlength="2000" style="width:95%" 
          onblur="v28(this.value,'txt_ECOF_C15');  guardarContenidoDocumento();"
          onkeypress="return validarKey(event,this.id)"> </textarea>
      </td>
    </tr>
    <tr class="camposRepInp">
      <td width="25%">Observaciones</td>
      <td width="25%">Concepto</td>
    </tr>
    <tr class="estiloImput">
      <td>
        <textarea type="text" id="txt_ECOF_C16" name="observacion" placeholder="Observaciones..." size="4000" rows="4" maxlength="4000"
          style="width:95%"  onfocus="sugerencia(this.id)"
          onblur="v28(this.value,'txt_ECOF_C16');  guardarContenidoDocumento();"
          onkeypress="return validarKey(event,this.id)"> </textarea>
      </td>
      <td>
        <textarea type="text" id="txt_ECOF_C17" name="concepto" placeholder="Concepto..." size="4000" rows="4" maxlength="4000"
          style="width:95%"  onblur="v28(this.value,this.id); guardarContenidoDocumento();"
          onkeypress="return validarKey(event,this.id)"> </textarea>
      </td>
    </tr>
  </table>
</div>