<table width="100%"  cellpadding="0" cellspacing="0"  align="left">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="40%" align="left">
          <tr class="camposRepInp">
            <td colspan="2">TONO MUSCULAR</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="20%">SEGMENTO CORPORAL</td>
            <td width="20%"></td>
          </tr>  
          <tr class="estiloImput">
            <td align="right">CUELLO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_ETOP_C1" style="width:37%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr>  
           <tr class="estiloImput">
            <td align="right">TRONCO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_ETOP_C2" style="width:37%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">MIEMBRO SUPERIOR DERECHO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_ETOP_C3" style="width:37%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr>
          <tr class="estiloImput">
            <td align="right">MIEMBRO SUPERIOR IZQUIERDO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_ETOP_C4" style="width:37%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr>
           <tr class="estiloImput">
            <td align="right">MIEMBRO INFERIOR DERECHO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                   <select size="1" id="txt_ETOP_C5" style="width:37%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
          <tr class="estiloImput">
            <td align="right">MIEMBRO INFERIOR IZQUIERDO:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <select size="1" id="txt_ETOP_C6" style="width:37%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
           <tr class="estiloImput">
            <td align="right">FUERZA MUSCULAR:</td>
             <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                  <select size="1" id="txt_ETOP_C7" style="width:37%;" title="32" onblur="guardarContenidoDocumento()"   > 
                        <option value=""></option> 
                        <option value="EUTONO">EUTONO</option> 
                        <option value="HIPERTONO">HIPERTONO</option>           
                        <option value="HIPOTONO">HIPOTONO</option>           
                    </select>
               </td>            
          </tr> 
       </table>  
      </td>             
  </tr>
</table>
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">ANTECEDENTES DE DESARROLLO PSICOMOTOR</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="12.5%">ITEM</td>
            <td width="12.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="13.5%">ITEM</td>
            <td width="11.5">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
          <tr class="estiloImput">
                  <td align="right">
                    CONTROL CEFALICO 3M:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C8" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C9" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C10" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                      RODILLAS 10 M:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C11" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C12" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C13" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    GIROS 4 M:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C14" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C15" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C16" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    BIPEDO CON APOYO 10M:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C17" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C18" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C19" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                  ARRASTRE 5M:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C20" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C21" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C22" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  BIPEDO SIN APOYO 11M:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C23" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C24" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C25" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    SEDENTE 7M:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C26" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C27" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C28" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    CAMINA 12 A 24 M:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C29" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C30" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C31" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      GATEO 8M:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C32" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C33" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C34" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SUBE ESCALERAS 18 A 24 M:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C35" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C36" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C37" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
        </table>   
       </td>        
  </tr>
</table>





<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">PATRONES FUNCIONALES DE MOVIMIENTO</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="13.5%">PATRONES</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="13.5%">PATRONES</td>
            <td width="11.5">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
          <tr class="estiloImput">
                  <td align="right">
                   MANO CABEZA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C38" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C39" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C40" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGARRE A MANO LLENA:
                 </td>
                  <td>
                      <input type="text" id="txt_ETOP_C41" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C42" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C43" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    MANO BOCA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C44" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C45" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C46" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGARRE CILINDRICO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C47" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C48" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C49" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                  MANO CUELLO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C50" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C51" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C52" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  ENGANCHE:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C53" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C54" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C55" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    MANO HOMBRO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C56" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C57" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C58" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PINZA FINA:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C59" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C60" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C61" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      MANO ESPALDA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C62" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C63" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C64" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PINZA TRIPODE:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C65" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C66" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C67" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      MANO PIERNA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C68" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C69" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C70" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PINZA LATERAL:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C71" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C72" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C73" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      MANO PIE:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C74" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C75" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C76" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    OPOSICION:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C77" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C78" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C79" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
                  <td class="titulos1" colspan="4"> 
                  PATRONES INTEGRALES DE MOVIMIENTO
                  </td>              
                  <td style="text-align: right;" class="estiloImput">
                   ATRAPAR:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C80" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C81" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C82" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                 <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO SUPERIOR:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C83" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C84" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C85" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SOLTAR VOLUNTARIO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C86" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C87" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C88" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO INFERIOR:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C89" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C90" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C91" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SOLTAR INVOLUNTARIO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C92" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C93" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C94" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO LATERAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C95" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C96" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_97" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PATEAR:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C98" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C99" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C100" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
              <tr class="estiloImput">
                  <td align="right"> 
                      ALCANCE PLANO DIAGONAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C101" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C102" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C103" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    LANZAR:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C104" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C105" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C106" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
        </table>   
       </td>        
  </tr>

<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">SISTEMA SENSORIAL</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="16.5%">SENSIBILIDAD SUPERFICIAL</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="11.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="16.5%">AUDITIVO</td>
            <td width="11.5">FUNCIONAL (F)</td>
            <td width="11.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
          <tr class="estiloImput">
                  <td align="right">
                   CALOR:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C107" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C108" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C109" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGIDENTIFICACION DEL SONIDO:
                 </td>
                  <td>
                      <input type="text" id="txt_ETOP_C110" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C111" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C112" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    FRIO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C113" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C114" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C115" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  DISCRIMINACION DEL SONIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C116" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C117" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C118" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                 TOQUE LIGERO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C119" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C120" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C121" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  LOCALIZACION DEL SONIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C122" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C123" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C124" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
                <td style="text-align: right;" class="estiloImput">
                   ROMO O AGUDO:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C125" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C126" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C127" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td colspan="4" class="titulos1">
                    OLFATIVO
                  </td>                 
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                     DOLOR AL TACTO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C128" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C129" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C130" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    AGRADABLE:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C131" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C132" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C133" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
                  <td class="titulos1" colspan="4"> 
                      SENSIBILIDAD PROFUNDA
                  </td> 
                  <td style="text-align: right;" class="estiloImput">
                    DESAGRADABLE:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C134" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C135" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C136" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr>
               <td style="text-align: right;" class="estiloImput">
                      BAROGNOSIA:
                 </td>
                  <td  class="estiloImput">
                      <input type="text" id="txt_ETOP_C137" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td  class="estiloImput">
                      <input type="text" id="txt_ETOP_C138" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td  class="estiloImput">
                      <input type="text" id="txt_ETOP_C139" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td class="titulos1" colspan="4">
                    GUSTATIVO
                  </td>                  
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                     ESTEROGNOSIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C140" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C141" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C142" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DULCE:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C143" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C144" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C145" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                      GRAFESTESIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C146" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C147" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C148" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SALADO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C149" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C150" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C151" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      DIFERENCIACION DE TEXTURAS:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C152" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C153" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C154" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    ACIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C155" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C156" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C157" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
          </table>   
       </td>        
  </tr>
</table>




<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">PERCEPCION VISUAL</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="16.5%">ITEM</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="11.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="50%" colspan="4">FIJACION VISUAL</td>
          </tr>
          <tr>
              <td style="text-align: right;" class="estiloImput">
                   SEGUIMIENTO VISUAL:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C158" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C159" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C160" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                <td class="camposRepInp">ITEM</td>
                <td class="camposRepInp">COLOR</td>
                <td class="camposRepInp">FORMA</td>
                <td class="camposRepInp">TAMA&Ntilde;O</td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    HORIZONTAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C161" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C162" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C163" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  PAREA:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C164" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C165" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C166" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                 VERTICAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C167" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C168" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C169" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  AGRUPAR:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C170" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C171" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C172" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                   DIAGONAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C173" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C174" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C175" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   IDENTIFICA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C176" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C177" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C178" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                     CIRCULAR:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C179" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C180" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C181" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    NOMINA:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C182" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C183" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C184" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
       <tr class="estiloImput">
                  <td align="right"> 
                     ATENCION VISUAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C185" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C186" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C187" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    FIGURA FONDO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C188" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C189" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C190" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right"> 
                     DISCRIMINACION VISUAL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C191" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C192" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C193" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    COPIA DE DISE&Ntilde;O:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C194" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C195" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C196" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>            
          </table>   
       </td>        
  </tr>
</table>

 
 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">PERCEPCION TEMPORO ESPACIAL</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="12.5%">ITEM</td>
            <td width="12.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="13.5%">ITEM</td>
            <td width="11.5">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
          <tr>
                  <td class="camposRepInp" colspan="4">
                      ESPACIO
                  </td>
                  <td style="text-align: right;" class="estiloImput">
                      NOCHE:
                 </td> 
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C197" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C198" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C199" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    ARRIBA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C200" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C201" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C202" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    MA&Ntilde;ANA:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C203" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C204" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C205" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                  ABAJO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C206" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C207" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C208" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  TARDE:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C209" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C210" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C211" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    ADELANTE:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C212" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C213" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C214" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    HOY:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C215" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C216" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C217" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      ATRAS:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C218" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C219" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C220" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    AYER:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C221" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C222" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C223" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      AFUERA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C224" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C225" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C226" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    MES:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C227" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C228" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C229" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      DENTRO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C230" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C231" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C232" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DIA:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C233" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C234" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C235" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr>
                  <td class="titulos1" colspan="4"> 
                      TIEMPO
                  </td> 
                  <td style="text-align: right;" class="estiloImput">
                    A&Ntilde;O:
                 </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C236" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C237" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td class="estiloImput">
                      <input type="text" id="txt_ETOP_C238" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
              <tr class="estiloImput">
                  <td align="right"> 
                    DIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C239" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C240" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C241" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    FECHA:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C242" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C243" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C244" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
        </table>   
       </td>        
  </tr>


 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">ESQUEMA CORPORAL / LATERALIDAD</td>
          </tr>                    
          <tr class="camposRepInp">
            <td width="12.5%">ITEM</td>
            <td width="12.5%">EN SI MISMO</td>
            <td width="12.5%">EN OTRA PERSONA</td>
            <td width="12.5%">EN RELACION A OBJETOS</td>
            <td width="12.5%">ITEM</td>
            <td width="12.5%">EN SI MISMO</td>
            <td width="12.5%">EN OTRA PERSONA</td>
            <td width="12.5%">EN RELACION A OBJETOS</td>
          </tr>
          <tr class="camposRepInp" >
                  <td colspan="4">
                      LATERALIDAD
                  </td> 
                  <td colspan="4">
                      ESQUEMA CORPORAL:
                  </td>
            </tr> 
            <tr class="estiloImput">
                  <td align="right">
                    MANO DERECHA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C245" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C246" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C247" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    PARTES FINAS:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C248" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C249" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C250" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                  MANO IZQUIERDA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C251" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C252" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C253" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  PARTES GRUESAS:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C254" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C255" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C256" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    OJO DERECHO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C257" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C258" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C259" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    ARTICULACIONES:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C260" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C261" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C262" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      OJO IZQUIERDO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C263" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C264" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C265" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DETALLES:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C266" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C267" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C268" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      PIE DERECHO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C269" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C270" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C271" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    IMAGEN CORPORAL:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C272" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C273" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C274" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      PIE IZQUIERDO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C275" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C276" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C277" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  
            </tr>               
        </table>   
       </td>        
  </tr>
</table>




 <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
  <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr>
      <td>
        <table width="100%" align="left">
          <tr class="camposRepInp">
            <td colspan="8">MOTRICIDAD</td>
          </tr>                    
        <tr class="camposRepInp">
            <td width="12.5%">MOTRICIDAD FINA</td>
            <td width="12.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
            <td width="13.5%">MOTRICIDAD GRUESA</td>
            <td width="11.5%">FUNCIONAL (F)</td>
            <td width="12.5%">SEMIFUNCIONAL (SF)</td>
            <td width="12.5%">NO FUNCIONAL( NF)</td>
          </tr>
            <tr class="estiloImput">
                  <td align="right">
                    PICADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C278" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C279" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C280" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    DESPLAZAMIENTO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C281" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C282" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C283" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right">
                 RASGADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C284" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C285" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C286" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                  EQUILIBRIO ESTATICO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C287" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C288" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C289" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
            <tr class="estiloImput">
                  <td align="right">
                    BORDEADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C290" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C291" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C292" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    EQUILIBRIO DINAMICO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C293" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C294" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C295" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
             <tr class="estiloImput">
                  <td align="right"> 
                      RECORTADO CON TIJERAS:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C296" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C297" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C298" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    POSICION SEDENTE
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C299" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C300" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C301" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                  <tr class="estiloImput">
                  <td align="right"> 
                      ENCAJADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C302" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C303" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C304" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    POSICION BIPEDA:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C305" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C306" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C307" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                <tr class="estiloImput">
                  <td align="right"> 
                      ENSARTADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C308" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C309" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C310" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   POSICION DE RODILLAS:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C311" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C312" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C313" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                           <tr class="estiloImput">
                  <td align="right"> 
                      BORDADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C314" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C315" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C316" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                    SALTO UNILATERAL:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C317" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C318" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C319" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
                           <tr class="estiloImput">
                  <td align="right"> 
                      CALCADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C320" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C321" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C322" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   SALTO BILATERAL:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C323" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C324" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C325" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
              <tr class="estiloImput">
                  <td align="right"> 
                      DIBUJADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C326" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C327" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C328" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                      CORRER:
                    </td>
                  <td>
                      <input type="text" id="txt_ETOP_C329" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C330" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C331" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
              <tr class="estiloImput">
                  <td align="right"> 
                      COLOREADO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C332" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C333" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C334" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   TRANSPORTE DE PESO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C335" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C336" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C337" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>      
            <tr class="estiloImput">
                  <td align="right"> 
                      UTILIZACION DE PINCEL:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C338" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C339" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C340" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   ALCANZAR:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C341" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C342" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C343" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>      
            <tr class="estiloImput">
                  <td align="right"> 
                  </td> 
                  <td>
                  </td>                
                 <td>
                  </td>                 
                  <td>
                  </td> 
                  <td align="right">
                    EMPUJAR:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C344" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C345" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C346" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr>
              <tr class="camposRepInp">
                <td colspan="4">PROCESOS COGNITIVOS</td>
                <td colspan="4">ACTIVIDADES DE LA VIDA DIARIA</td>
             </tr>
            <tr class="camposRepInp">
                <td width="12.5%">ITEM</td>
                <td width="12.5%">FUNCIONAL (F)</td>
                <td width="12.5%">SEMIFUNCIONAL (SF)</td>
                <td width="12.5%">NO FUNCIONAL( NF)</td>
                <td width="12.5%">ITEM</td>
                <td width="12.5%">FUNCIONAL (F)</td>
                <td width="12.5%">SEMIFUNCIONAL (SF)</td>
                <td width="12.5%">NO FUNCIONAL( NF)</td>
           </tr>     
             <tr class="estiloImput">
                  <td align="right"> 
                      PERCEPCION:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C347" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C348" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C349" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   VESTIDO:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C350" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C351" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C352" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
            </tr> 
             <tr class="estiloImput">
                  <td align="right"> 
                      JUICIO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C353" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C354" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C355" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   ALIMENTACION:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C356" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C357" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C358" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>  
               <tr class="estiloImput">
                  <td align="right"> 
                      RACIOCINIO:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C359" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C360" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C361" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   HIGIENE MAYOR:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C362" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C363" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C364" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>     
               <tr class="estiloImput">
                  <td align="right"> 
                      ATENCION:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C365" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C366" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C367" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td> 
                  <td align="right">
                   HIGIENE MENOR:
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C368" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C369" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
                  <td>
                      <input type="text" id="txt_ETOP_C370" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>
               </tr>  
               <tr class="estiloImput">
                  <td align="right"> 
                      MEMORIA:
                  </td> 
                  <td>
                      <input type="text" id="txt_ETOP_C371" maxlength="20" style="width:85%" onblur="guarhdarContenidoDocumento()"/>
                  </td>                
                 <td>
                      <input type="text" id="txt_ETOP_C372" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                 
                  <td>
                      <input type="text" id="txt_ETOP_C373" maxlength="20" style="width:85%" onblur="guardarContenidoDocumento()"/>
                  </td>                   
               </tr>    
        </table>   
       </td>        
  </tr>
   <tr>
    <td width="1000%" bgcolor="#c0d3c1">.</td>
  </tr> 
  <tr class="camposRepInp">
     <td width="100%">USO DE AYUDAS TECNICAS:</td> 
  </tr>  
  <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="50%">DESEMPE&Ntilde;O ESCOLAR</td>
                  <td width="50%">ACTIVIDADES PREVOCACIONALES</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <textarea type="text" id="txt_ETOP_C374"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_ETOP_C375"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
          </tr> 
          </table> 
        </td>
    </tr>
      <tr>
      <td> 
          <table width="100%" align="center">    
            <tr class="estiloImput">
                  <td width="50%">EXPERIENCIA LABORAL</td>
                  <td width="50%">ACTIVIDADES DE OCIO Y TIEMPO LIBRE</td>
            </tr>                 
            <tr class="camposRepInp"> 
                  <td>
                      <textarea type="text" id="txt_ETOP_C376"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>
                  <td>
                    <textarea type="text" id="txt_ETOP_C377"   rows="5" cols="50" maxlength="4000" style="width:95%"  onblur="guardarContenidoDocumento()"> </textarea>  
                  </td>                
          </tr> 
          </table> 
        </td>
    </tr>
</table>






 
 





