<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	
    beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="Plantillas Folio" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>

                        <div style="overflow:auto;height:350px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                    <tr class="titulos" align="center">
                                        <td width="10%">ID FOLIO</td>
                                        <td width="60%">NOMBRE</td>
                                        <td width="20%">VIGENTE</td>
                                        <td width="10%">&nbsp;</td>
                                    </tr>
                                    <tr class="estiloImput">
                                        <td colspan="1"><input type="text" id="txtId" style="width:90%"  onkeypress="javascript:return teclearsoloAlfabeto(event);" onkeyup="javascript: this.value= this.value.toUpperCase();" />
                                        </td>
                                        <td colspan="1"><input type="text" id="txtNombre" style="width:90%"
                                            onkeypress="javascript:return teclearsoloAlfabeto(event);"   />
                                        </td>
                                        <td><select size="1" id="cmbEstado" style="width:80%" >
                                                <option value="true">SI</option>
                                                <option value="flase">NO</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input name="btn_MODIFICAR" title="btn_pl22" type="button"
                                                class="small button blue" value="BUSCAR"
                                                onclick="buscarHistoria('listGrillaFolios')" /> 
                                        </td>
                                    </tr>
                                </table>
                                <table id="listGrillaFolios" class="scroll"></table>
                            </div>
                        </div><!-- div contenido-->

                        <div id="divEditar" style="display:block; width:100%">

                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3"> AGREGAR PLANTILLA AL FOLIO
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td class="estiloImputDer" width="10%">ID: </td>
                                                <td class="estiloImputIzq2" ><label id="lblId"></label></td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputDer">NOMBRE: </td>
                                                <td class="estiloImputIzq2" ><label id="lblNombre"></label></td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputDer">PLANTILLA:</td>
                                                <td>
                                                    <select style="width:50%" id="cmbPlantilla">
                                                        <option value="0"></option>
                                                        <%
                                                            resultaux.clear();
                                                            resultaux=(ArrayList)beanAdmin.combo.cargar(588);//585estaba antes
                                                            ComboVO cmbPL; 	
                                                            for(int k=0;k<resultaux.size();k++){ 
                                                                cmbPL=(ComboVO)resultaux.get(k);
                                                            %>
                                                        <option value="<%= cmbPL.getId()%>"
                                                            title="<%= cmbPL.getTitle()%>">
                                                            <%= cmbPL.getId() + " " +cmbPL.getDescripcion()%></option>
                                                        <%}%>			   
                                                    </select> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputDer">ORDEN:</td>
                                                <td>
                                                    <select id="cmbOrden">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>                                                        
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%" style="cursor:pointer">
                                <tr>
                                    <td width="99%" class="titulos">
                                        <input name="btn_MODIFICAR" title="btn_cP78" type="button"
                                            class="small button blue" value="AGREGAR PLANTILLA"
                                            onclick="modificarCRUD('agregarPlantillaFolio', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>

                <tr class="titulosCentrados">
                    <td> LISTA DE PLANTILLAS DEL FOLIO
                    </td>
                </tr>

                <tr>
                    <td>
                        <table id="listGrillaPlantillasFolio" class="scroll"></table>
                    </td>
                </tr> 
            </table>
            </div>



        </td>
    </tr>


</table>
</td>



</table>