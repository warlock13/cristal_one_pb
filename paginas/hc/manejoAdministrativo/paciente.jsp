<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();
%>

<table width="1400px" align="center"   border="0" cellspacing="0" cellpadding="1"  onload="buscarPaciente('paciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');" >
    <tr>
        <td style="padding: 0;">
            <!-- AQUI COMIENZA EL TITULO -->	
            <div id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../tituloPaciente.jsp" flush="true">
                    <jsp:param name="titulo" value="Crear o Editar Paciente" />
                </jsp:include>
            </div>	
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <table width="1400px" style="background-color: #fbfbfb;">
        <tr></tr>
                            <tr class="titulos"> 
                                <td width="20%" >Tipo Identificacion</td>	
                                <td width="20%" >Identificacion</td>                                                                  
                                <!-- <td width="60%" >Nombres</td> -->                                                               
                            </tr>
                            <tr class="estiloImput"> 
                                <td>
                                    <select size="1" style="width: 150px;"  id="cmbTipoId" style="width:55%"  >
                                        <option value=""></option>
                                        <%     resultaux.clear();
                                               resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                               ComboVO cmb1051; 
                                               for(int k=0;k<resultaux.size();k++){ 
                                                     cmb1051=(ComboVO)resultaux.get(k);
                                        %>
                                        <option value="<%= cmb1051.getId()%>" title="<%= cmb1051.getTitle()%>"><%= cmb1051.getId()+"  "+cmb1051.getDescripcion()%></option>
                                        <%}%>						
                                    </select>
                                </td> 
                                <td  >
                                <input  type="text" id="txtIdentificacion"  style="width:70%"   size="20" maxlength="20" onKeyPress="pacienteSearch(event);"   />
                                        <!--<input name="btn_paciente" type="button" value="Consultar" onclick="guardarYtraerDatoAlListado('buscarIdentificacionPaciente')"  /> -->                          
                                </td>  	 
                                <!-- <td>
                                    <input type="text" value=""  size="50"  maxlength="70"  id="txtNombrePaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtBusIdPaciente', 307)"   /> 
                                    <input name="btn_paciente" type="button" value="BUSCAR" onclick="buscarPaciente('paciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"  />
                                </td>  -->
                            </tr>
</table> 
    <table style="background-color: #fbfbfb;" width="1400px">
        <tr >
            <td>
                <div>
                    <!-- <br> -->
                    <table class="table" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <span class="subtitulo">Información Principal</span>
                                </td>
                              </tr>
                          <tr>
                            <td><label class="label-field">Tipo ID <span style="color: red;">*</span></label></td>                                                            
                            <td><label class="label-field">Identificación <span style="color: red;">*</span></label></td>      
                            <td><label class="label-field">Nombre 1 <span style="color: red;">*</span></label></td>                                                            
                            <td><label class="label-field">Nombre 2</label></td>      
                            <td><label class="label-field">Apellido 1 <span style="color: red;">*</span></label></td>
                            <td><label class="label-field">Apellido 2</label></td>                                                    
                          </tr>
                          <tr>
                            <td>
                                <select class="select2-custom" style="width: 180px;" id="cmbTipoIdPac">
                                    <option value="" >[Seleccione]</option>
                                    <%      resultaux.clear();
                                            resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                            ComboVO cmbTI; 
                                            for(int k=0;k<resultaux.size();k++){ 
                                                cmbTI=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbTI.getId()%>" title="<%= cmbTI.getTitle()%>"><%= cmbTI.getDescripcion()%></option>
                                            <%}%>                                                                      
                                </select>
                            </td> 
                            <td> 
                                <input class="input-field" id="txtIdentificacionPac" type="text" onkeypress="javascript:return excluirEspecialesv2(event);">
                            </td> 
                            <td> 
                                <input class="input-field" type="text" id="txtNombre1" oninput="validarTextoOnInput(this)" onpaste="validarTexto(event, this.id)">
                            </td>  
                            <td>
                                <input class="input-field" type="text" id="txtNombre2" oninput="validarTextoOnInput(this)" onpaste="validarTexto(event, this.id)">
                            </td> 
                            <td> 
                                <input class="input-field" type="text" id="txtApellido1" oninput="validarTextoOnInput(this)" onpaste="validarTexto(event, this.id)">
                            </td>
                            <td> 
                                <input class="input-field" type="text" id="txtApellido2" oninput="validarTextoOnInput(this)" onpaste="validarTexto(event, this.id)">
                            </td>                                                                                       
                          </tr>
                        
                          <tr>
                            <td><label class="label-field">Fecha de Nacimiento <span style="color: red;">*</span></label></td>
                            <td><label class="label-field">Lugar de Nacimiento <span style="color: red;">*</span></label></td>      
                            <td><label class="label-field">Etnia <span style="color: red;">*</span></label></td>
                            <td><label for="" class="label-field campoRequerido">Grupo Sanguíneo </label></td>
                            <td><label class="label-field">Sexo <span style="color: red;">*</span></label></td>                           
                            <td><label class="label-field">Estado Civil <span style="color: red;">*</span></label></td>                                                                
                          </tr>
                          <tr>
                            <td> 
                                <input class="input-field" type="date" id="txtFechaNac" onblur="validarFechaPatron(this.value, this.id, 1900)">
                            </td>  
                              
                            <td>
                                <select  class="select2-custom" style="width: 180px"  id="txtNacimiento">
                                    <option value="">[Seleccione]</option>
                                    <%      resultaux.clear();
                                            resultaux=(ArrayList)beanAdmin.combo.cargar(10390);	
                                            ComboVO cmbNM; 
                                            for(int k=0;k<resultaux.size();k++){ 
                                                cmbNM=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbNM.getId()%>" title="<%= cmbNM.getTitle()%>"><%= cmbNM.getDescripcion()%></option>
                                            <%}%>                                                                      
                                </select>
                                <!-- <input type="text" id="txtNacimiento" style="width:110px" disabled>
                                <img title="VEN52" id="idLupitaVentanitaNacimiento" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtNacimiento', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle"> -->
                            </td> 
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbEtnia">
                                    <option value="6">NINGUNA DE LAS ANTERIORES</option>
                                    <option value="1">INDIGENA</option>
                                    <option value="2">GITANO</option>
                                    <option value="3">RAIZAL</option>
                                    <option value="4">PALENQUERO</option>
                                    <option value="5">AFROCOLOMBIANO</option>
                                </select>
                            </td>
                            <td>
                                <select class="select2-custom" style="width: 180px;"  id="cmbGruposSanguineo" onchange="">
                                    <option value="">[Seleccione]</option>
                                    <option value="0">A +</option>
                                    <option value="1">A -</option>
                                    <option value="2">B +</option>
                                    <option value="3">B -</option>
                                    <option value="4">AB +</option>
                                    <option value="5">O +</option>
                                    <option value="6">O -</option>
                                    <option value="7">AB -</option>
                                    <option value="8">No Sabe</option>
                                </select>
    
                            </td>
    
                            <td>
                                <select class="select2-custom" style="width: 180px;"  id="cmbSexo" onchange="validacionGestante()">
                                    <option value="">[Seleccione]</option>
                                    <option value="F">Femenino</option>
                                    <option value="M">Masculino</option>
                                    <option value="O">Otro</option>
                                </select>
                            </td>   
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbEstadoCivil">
                                    <option value="">[Seleccione]</option>
                                    <option value="0">SOLTERO</option>
                                    <option value="1">CASADO</option>
                                    <option value="2">SOLTERO</option>
                                    <option value="3">UNION LIBRE</option>
                                    <option value="4">VIUDO</option>
                                    <option value="5">OTRO</option>
                                </select>
                            </td>                                                   
                          </tr>
                        <tr>
                            <td><label class="label-field">Nacionalidad <span style="color: red;">*</span></label></td>                                                              
                            <td><label class="label-field">Municipio <span style="color: red;">*</span></label></td>  
                            <td><label class="label-field">Dirección <span style="color: red;">*</span></label></td>                                                      
                            <td><label class="label-field">Barrio</label></td>   
                            <td><label class="label-field">Estrato <span style="color: red;">*</span></label></td>    
                            <td><label class="label-field">Nivel Sisben <span style="color: red;">*</span></label></td>
                        </tr>
                          <tr>
                            
                            <td>
                                <select class="select2-custom" style="width: 180px;"  id="txtNacionalidad">
                                    <option value="">[Seleccione]</option>
                                    <%      resultaux.clear();
                                            resultaux=(ArrayList)beanAdmin.combo.cargar(10389);	
                                            ComboVO cmbNA; 
                                            for(int k=0;k<resultaux.size();k++){ 
                                                cmbNA=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbNA.getId()%>" title="<%= cmbNA.getTitle()%>"><%= cmbNA.getDescripcion()%></option>
                                            <%}%>                                                                      
                                </select>
                                <!-- <input type="text" id="txtNacionalidad" value  = "CO-COLOMBIA" style="width:110px"  onkeydown="return false">
                                <img title="VEN52" id="idLupitaVentanitaNacionalidad" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtNacionalidad', '60')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle"> -->
                            </td> 
                            <td>
                                <select class="select2-custom" style="width: 180px;" onchange="cargarComboBarrio();" id="txtMunicipio">
                                    <option value="">[Seleccione]</option>
                                    <%      resultaux.clear();
                                            resultaux=(ArrayList)beanAdmin.combo.cargar(10390);	
                                            ComboVO cmbMU; 
                                            for(int k=0;k<resultaux.size();k++){ 
                                                cmbMU=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbMU.getId()%>" title="<%= cmbMU.getTitle()%>"><%= cmbMU.getDescripcion()%></option>
                                            <%}%>                                                                      
                                </select>
                                <!-- <input type="text" id="txtMunicipio" style="width:110px" disabled>
                                <img title="VEN52" id="idLupitaVentanitaMunicipio" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle"> -->
                            </td>
                            <td> 
                                <input class="input-field" type="text" id="txtDireccion">
                            </td> 
                            <td> 
                                <input type="text" id="txtBarrio" placeholder="Seleccione el barrio" style="width:180px">
                                <!-- <img title="VEN52" id="idLupitaVentanitaBarrio" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtBarrio', '53')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle"> -->
                            </td>  
                            <td hidden>
                                <input type="text" id="txtLocalidad" style="width:110px" hidden>
                                <img title="VEN52" id="idLupitaVentanitaLocalidad" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtLocalidad', '57')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle">
                            </td>
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbEstrato">
                                    <option value="">[Seleccione]</option>
                                    <option value="0">0 - Bajo-Bajo</option>
                                    <option value="1">1 - Bajo</option>
                                    <option value="2">2 - Medio-Bajo</option>
                                    <option value="3">3 - Medio</option>
                                    <option value="4">4 - Medio-Alto</option>
                                    <option value="5">5 - Alto</option>
                                    <option value="6">6 - Sin Dato</option>
                                    <option value="7">7 - No Aplica</option>
                                </select>
                            </td> 
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbSisben">
                                    <option value="">[Seleccione]</option>
                                    <option value="N">NO APLICA</option>
                                    <option value="1">NIVEL 1</option>
                                    <option value="2">NIVEL 2</option>
                                    <option value="3">NIVEL 3</option>
                                </select>
                            </td>                                                              
                          </tr>
                          
                          <tr >
                            <td><label class="label-field">Ocupación <span style="color: red;">*</span></label></td>    
                            <td><label class="label-field">Discapacidad <span style="color: red;">*</span></label></td>     
                            <td><label class="label-field">Nivel Escolar <span style="color: red;">*</span></label></td>                                                            
                            <td><label class="label-field">Grupo Poblacional <span style="color: red;">*</span></label></td>      
                            <td><label class="label-field">Condición de Vulnerabilidad <span style="color: red;">*</span></label></td>                            
                            <td><label class="label-field">Hechos Victimizantes <span style="color: red;">*</span></label></td>  
                          </tr>
                          <tr >
                            
                            <td>
                                <select class="select2-custom" style="width: 180px;"  id="txtOcupacion">
                                    <option value="">[Seleccione]</option>
                                    <%      resultaux.clear();
                                            resultaux=(ArrayList)beanAdmin.combo.cargar(593);	
                                            ComboVO cmbOC; 
                                            for(int k=0;k<resultaux.size();k++){ 
                                                cmbOC=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbOC.getId()%>" title="<%= cmbOC.getTitle()%>"><%= cmbOC.getDescripcion()%></option>
                                            <%}%>                                                                      
                                </select>
                                <!-- <input type="text" id="txtOcupacion" style="width:110px" disabled>
                                <img title="VEN52" id="idLupitaVentanitaOcupacion" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtOcupacion', '56')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle"> -->
                            </td>
                            
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbDiscapacidad">
                                    <option value="3">NO APLICA</option>
                                    <option value="1">FISICA</option>
                                    <option value="2">PSIQUICA</option>
                                    <option value="4">SENSORIAL</option>
                                </select>
                            </td>  
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbEscolaridad">
                                    <option value="">[Seleccione]</option>
                                        <%      resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(4);	
                                                ComboVO cmbEs; 
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbEs=(ComboVO)resultaux.get(k);
                                        %>
                                    <option value="<%= cmbEs.getId()%>" title="<%= cmbEs.getTitle()%>"><%= cmbEs.getDescripcion()%></option>
                                        <%}%> 
                                </select>
                            </td>  
                            <td>
                                <select class="select2-custom" style="width: 180px;"  id="cmbGrupoPoblacional">
                                        <%      resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(955);	
                                                ComboVO cmbGp; 
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbGp=(ComboVO)resultaux.get(k);
                                        %>
                                    <option value="<%= cmbGp.getId()%>" title="<%= cmbGp.getTitle()%>"><%= cmbGp.getDescripcion()%></option>
                                        <%}%> 
                                </select>
                            </td> 
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbVulnerabilidad">
                                        <%      resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(959);	
                                                ComboVO cmbVu; 
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbVu=(ComboVO)resultaux.get(k);
                                        %>
                                    <option value="<%= cmbVu.getId()%>" title="<%= cmbVu.getTitle()%>"><%= cmbVu.getDescripcion()%></option>
                                        <%}%> 
                                </select>
                            </td>
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbHechosVicti">
                                        <%      resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(954);	
                                                ComboVO cmbHv; 
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbHv=(ComboVO)resultaux.get(k);
                                        %>
                                    <option value="<%= cmbHv.getId()%>" title="<%= cmbHv.getTitle()%>"><%= cmbHv.getDescripcion()%></option>
                                        <%}%> 
                                </select>
                            </td>                                                        
                          </tr>
                          <tr >                            
                            <td><label class="label-field">Intersexual <span style="color: red;">*</span></label></td>   
                            <td><label class="label-field">Orientación Sexual </label></td>   
                            <td><label class="label-field">E-Mail</label></td>   
                            <td><label class="label-field">Teléfono</label></td>                                                            
                            <td><label class="label-field">Celular 1 <span style="color: red;">*</span></label></td>     
                            <td><label class="label-field">Celular 2</label></td>
                          </tr>
                          
                          <tr >
                            <td>
                                <select class="input-field" id="cmbIntersexual" style="width: 180px;">
                                    <option value="2">No</option>
                                    <option value="1">Si</option>
                                </select>
                            </td>
                            <td>
                                <select class="select2-custom" id="cmbOrientacion" style="width: 180px;">
                                    <option value="0">[Seleccione]</option>
                                    <option value="1">HETEROSEXUAL</option>
                                    <option value="3">HOMOSEXUAL</option>
                                    <option value="4">BISEXUAL</option>
                                    <option value="5">TRANSEXUAL</option>
                                </select>
                            </td>
                            <td> 
                                <input class="input-field" type="text" id="txtEmail">
                            </td> 
                            <td> 
                                <input class="input-field" type="number" id="txtTelefono" oninput="limitarCaracteresNumericos(this, 10)" onkeypress="javascript:return soloNumeroIdentificacionHc(event);">
                            </td>  
                            <td>
                                <input class="input-field" type="number" id="txtCelular1" oninput="limitarCaracteresNumericos(this, 10)" onkeypress="javascript:return soloNumeroIdentificacionHc(event);">
                            </td> 
                            <td> 
                                <input class="input-field" type="number" id="txtCelular2" oninput="limitarCaracteresNumericos(this, 10)" onkeypress="javascript:return soloNumeroIdentificacionHc(event);">
                            </td>
                                                                                          
                          </tr>
                          <tr>
                            <td colspan="6">
                                <div class="linea-de-division"></div>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">
                                <span class="subtitulo">Información Administradora</span>
                            </td>
                          </tr>
                          <tr>     
                            <td><label class="label-field">Administradora <span style="color: red;">*</span></label></td>                                                            
                            <td><label class="label-field">IPS Primaria <span style="color: red;">*</span></label></td>      
                            <td><label class="label-field">Régimen <span style="color: red;">*</span></label></td>                            
                            <td><label class="label-field">Tipo Afiliado <span style="color: red;">*</span></label></td>                                                     
                            <td><label class="label-field">Código Afiliación</label></td>  
                            <td><label class="label-field">Programa Social <span style="color: red;">*</span></label></td>
                          </tr>
                          <tr >
                            
                            <td>
                                <select class="select2-custom" style="width: 180px;"  id="txtAdministradora">
                                    <option value="">[Seleccione]</option>
                                    <%      resultaux.clear();
                                            resultaux=(ArrayList)beanAdmin.combo.cargar(140);	
                                            ComboVO cmbAD; 
                                            for(int k=0;k<resultaux.size();k++){ 
                                                cmbAD=(ComboVO)resultaux.get(k);
                                    %>
                                    <option value="<%= cmbAD.getId()%>" title="<%= cmbAD.getTitle()%>"><%= cmbAD.getDescripcion()%></option>
                                            <%}%>                                                                      
                                </select>
                                <!-- <input type="text" id="txtAdministradora" style="width:110px" disabled>
                                <img title="VEN52" id="idLupitaVentanitaAdministradora" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtAdministradora', '54')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle"> -->
                            </td>  
                            <td>
                                <input class="input-field" type="text" id="txtIpsPrimaria" style="width:180px" disabled value="190010918001-BIOS MEDICAL CENTER SAS SEDE 1::POPAYAN">
                                <!-- <img title="VEN52" id="idLupitaVentanitaIpsPrimaria" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIpsPrimaria', '55')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle"> -->
                            </td> 
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbRegimen">
                                    <option value="">[Seleccione]</option>
                                    <option value="C">CONTRIBUTIVO</option>
                                    <option value="S">SUBSIDIADO</option>
                                    <option value="A">PARTICULAR</option>
                                </select>
                            </td>
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbTipoAfiliado">
                                    <option value="">[Seleccione]</option>
                                    <option value="0">OTRO</option>
                                    <option value="1">COTIZANTE</option>
                                    <option value="2">BENEFICIARIO</option>
                                    <option value="3">ADICIONAL</option>
                                    <option value="4">EMPLEADO</option>
                                    <option value="5">AFILIADO</option>
                                    <option value="6">PARTICULAR</option>
                                    <option value="8">SUBSIDIADOS-COOMEVA</option>
                                    <option value="9">MEDICINA PREPAGADA</option>
                                </select>
                            </td>        
                            <td> 
                                <input class="input-field" id="txtAfiliacion" type="text" onkeypress="javascript:return soloNumeroIdentificacionHc(event);">
                            </td>
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbProgramaSocial">
                                    <option value="">[Seleccione]</option>
                                    <option value="0">NINGUNO</option>
                                    <option value="1">ICBF</option>
                                    <option value="2">FAMILIAS EN ACCION</option>
                                    <option value="3">CDI</option>
                                    <option value="4">JOVENES EN ACCION</option>
                                    <option value="5">ADULTO MAYOR</option>
                                    <option value="6">OTRO</option>
                                </select>
                            </td>                                                  
                          </tr>
                          <tr >                                                        
                            <td><label class="label-field">Fecha de Afiliación</label></td>
                          </tr>
                          <tr >                                  
                            
                            <td>
                                <input class="input-field" type="date" id="txtFechaAfi" onblur="validarFechaPatron(this.value, this.id, 1900)">
                            </td>                                                                                         
                          </tr>
                          <tr>
                            <td colspan="6">
                                <div class="linea-de-division"></div>
                            </td>
                          </tr>
                          <tr>
                            <td>
                                <span class="subtitulo">Información Acudiente</span>
                            </td>
                          </tr>
                          <tr >
                            <td><label class="label-field">Acudiente</label></td>                                                            
                            <td><label class="label-field">Parentesco</label></td>      
                            <td><label class="label-field">Tipo ID Acudiente</label></td>
                            <td><label class="label-field">Identificación Acudiente</label></td>   
                            <td><label class="label-field">Dirección Acudiente</label></td>                                                            
                            <td><label class="label-field">Teléfono Acudiente</label></td>  
                          </tr>
                          <tr >
                             
                            <td> 
                                <input type="text" id="txtAcudiente" class="input-field" oninput="validarTextoOnInput(this)" onpaste="validarTexto(event, this.id)">
                            </td>  
                            <td>
                                <select class="select2-custom" style="width: 180px;"  id="cmbParentesco">
                                    <option value="">[Seleccione]</option>
                                        <%      resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(957);	
                                                ComboVO cmbPa; 
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbPa=(ComboVO)resultaux.get(k);
                                        %>
                                    <option value="<%= cmbPa.getId()%>" title="<%= cmbPa.getTitle()%>"><%= cmbPa.getDescripcion()%></option>
                                        <%}%> 
                                </select>
                            </td> 
                            <td> 
                                <select class="select2-custom" style="width: 180px;"  id="cmbTipoIdAcudiente">
                                    <option value="">[Seleccione]</option>
                                        <%      resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                                ComboVO cmbTa; 
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbTa=(ComboVO)resultaux.get(k);
                                        %>
                                    <option value="<%= cmbTa.getId()%>" title="<%= cmbTa.getTitle()%>"><%= cmbTa.getDescripcion()%></option>
                                        <%}%> 
                                </select>
                            </td>
                            <td> 
                                <input class="input-field" type="number" id="txtIdentificacionAcudiente" onkeypress="javascript:return soloNumeroIdentificacionHc(event);">
                            </td>   
                            <td> 
                                <input class="input-field" type="text" id="txtDireccionAcudiente">
                            </td>  
                            <td>
                                <input class="input-field" type="number" id="txtTelefonoAcudiente" onkeypress="javascript:return soloNumeroIdentificacionHc(event);">
                            </td>                                                        
                          </tr>
                          <tr >
                            <td><label class="label-field">E-Mail Acudiente</label></td>
                          </tr>
                          <tr >
                            
                            <td> 
                                <input class="input-field" type="text" id="txtEmailAcudiente">
                            </td>
                                                                                        
                          </tr>
                          <tr >
                        </tr>
                        <tr >
                            <tr>
                                <td colspan="6">
                                    <div class="linea-de-division"></div>
                                </td>
                              </tr>                                           
                        </tr>
                          <tr>    
                            <td><label class="label-field">Es un Paciente de Prueba <span style="color: red;">*</span></label></td>
                            <td><label class="label-field">Gestante</label></td>
                          </tr>
                          <tr >
                            
                            <td>
                                <select class="input-field" id="cmbPrueba" >
                                    <option value="N">No</option>
                                    <option value="S">Si</option>
                                </select>
                            </td>
                            <td>
                                <select class="input-field" id="cmbGestante" >
                                    <option value="NO">No</option>
                                    <option value="SI">Si</option>
                                </select>
                            </td>
                            
                           
                          </tr>
                          <tr >              
                            <!-- <td>CODIGO AFILIACION </td> -->                   
                          </tr>
                          <tr  >
                            
                            <!-- <td> 
                                <input id="txtAfiliacion" type="text" onkeypress="javascript:return soloNumeroIdentificacionHc(event);">
                            </td> --> 
                          </tr>
                          <tr>
                            <td colspan="6">
                                <div class="linea-de-division"></div>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" >
                                <div style="display: flex; justify-content: end;">
                                    <input class="newbuttonpac" type="button" id="crearPac" value="Crear Paciente" onclick="validarDatosAntesDeGuardarPaciente(this.id)">
                                </div>
                            </td>
                            <td colspan="2">
                                <div style="display: flex; justify-content: center;">
                                    <input class="newbuttonpac" type="button" id="editarPac" value="Modificar Paciente" onclick="validarDatosAntesDeGuardarPaciente(this.id)">
                                </div>
                            </td>
                            <td colspan="2">
                                <div style="display: flex; justify-content: start;">
                                    <input class="newbuttonpac" type="button" value="Limpiar Campos" onclick="limpiarVentanaPaciente()">
                                </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                </div>
               
            </td> 
        </tr>
        <tr>
            <td colspan="6">
                <div class="linea-de-division"></div>
            </td>
          </tr>
        <tr>
            <td>
                <table  width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                    <tr>
                        <td>
                            <div id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                                <!--******************************** inicio de la tabla con los datos de busqueda ***************************-->	
                                <div id="divParaVentanita"></div>
                                <div id="divBuscar"  style="display:block"   >
                                    <!-- <table width="100%"   align="center">
                                        <tr class="titulos"> 
                                            <td width="20%" >Tipo Identificacion</td>	
                                            <td width="20%" >Identificacion</td>                                                                  
                                            <td width="60%" >Nombres</td>                                                               
                                        </tr>
                                        <tr class="estiloImput"> 
                                            <td>
                                                <select size="1" style="width: 150px;"  id="cmbTipoId" style="width:55%"  >
                                                    <option value=""></option>
                                                    <%     resultaux.clear();
                                                           resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                                           ComboVO cmb105; 
                                                           for(int k=0;k<resultaux.size();k++){ 
                                                                 cmb105=(ComboVO)resultaux.get(k);
                                                    %>
                                                    <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>"><%= cmb105.getId()+"  "+cmb105.getDescripcion()%></option>
                                                    <%}%>						
                                                </select>
                                            </td> 
                                            <td  >
                                            <input  type="text" id="txtIdentificacion"  style="width:70%"   size="20" maxlength="20" onKeyPress="pacienteSearch(event);"   />
                                                    
                                            </td>  	 
                                            <td>
                                                <input type="text" value=""  size="50"  maxlength="70"  id="txtNombrePaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtBusIdPaciente', 307)"   /> 
                                                <input name="btn_paciente" type="button" value="BUSCAR" onclick="buscarHC('listGrillaHomologacionIdentificaPaciente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');"  />
                                            </td> 
                                        </tr>                           
                                    </table> -->
                                    
                                    <div id="divListado" style="display: none;">
                                        <div>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <table id="listGrilla"  ></table>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                          </div>  
                                        <div id="pagerGrilla"  class="scroll" style="text-align:center;"></div>
                                        
                                    </div>
                                    
                                </div>
                                <tr>
                                    <td>
                                        <input name="btn_crear_admsion" type="button" class="small button blue" value="INCONSISTENCIA EN DATOS" onclick="abrirVentanaAnexo1()"/>              
                                    </td>
                                </tr>
                                 <!--********************** fin de la tabla con los datos del formulario ***********************************************-->
                            </div><!-- div contenido-->
                            <!--
                            <div id="divEditar" style="display:block; width:800px" >  
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer" >
                                    <tr><td width="99%" class="titulosCentrados">EDITAR
                                            <input type="button" title="BT87" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('paciente')" />                      
                                            <input type="button" title="BTR7" class="small button blue" value="CREAR" onclick="modificarCRUD('crearPacienteNuevo', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />                                          
                                            <input type="button" title="BTW7" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('paciente', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                                            <input type="button" title="BTY7" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarPaciente', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">                                                              
                                        </td></tr>
                                </table> 
                                <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
                                    <tr>
                                        <td colspan="4">
                                            <table width="100%">
                                                <tr class="titulos"> 
                                                    <td width="10%" >Tipo Ide</td>	
                                                    <td width="10%" >Identificacion</td>                                                                  
                                                    <td width="10%" >Nombre 1</td> 
                                                    <td width="10%" >Nombre 2</td>                                 
                                                    <td width="10%" >Apellido 1</td>                                 
                                                    <td width="10%" >Apellido 2</td>                                                                 
                                                </tr>		
                                                <tr class="estiloImput"> 
                                                    <td><select style="width: 150px;"  id="cmbTipoIdEdit" style="width:80%" title="18"  disabled="disabled" >	                                        
                                                            <option value=""></option>
                                                            <%     resultaux.clear();
                                                                   resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                                                   ComboVO cmb0;
                                                                   for(int k=0;k<resultaux.size();k++){ 
                                                                         cmb0=(ComboVO)resultaux.get(k);
                                                            %>
                                                            <option value="<%= cmb0.getId()%>" title="<%= cmb0.getTitle()%>"><%= cmb0.getDescripcion()%></option>
                                                            <%}%>						
                                                        </select>	                   
                                                    </td>     
                                                    <td><input type="text"  value="" id="txtIdPaciente" style="width:90%"  size="20"  maxlength="20"   disabled="disabled"></td>  
                                                    <td><input type="text"  value="" id="txtNombre1" style="width:90%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" ></td>      
                                                    <td><input type="text"  value="" id="txtNombre2" style="width:90%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" ></td>                            
                                                    <td><input type="text"  value="" id="txtApellido1" style="width:90%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" ></td>                            
                                                    <td><input type="text"  value="" id="txtApellido2" style="width:90%"  size="30"  maxlength="30" onblur="v28(this.value, this.id);" ></td>                                                                                                                                
                                                </tr>	
                                                <tr class="titulos"> 
                                                    <td width="10%" >Sexo</td>	
                                                    <td width="10%" >Fecha Nacimiento</td>    
                                                    <td width="10%"  colspan="2">Telefonos</td>  
                                                    <td colspan="2" >Administradora EPS</td>                                                                                                                                                                     
    
                                                </tr>		
                                                <tr class="estiloImput"> 
                                                    <td><select size="1" style="width: 150px;"  id="cmbSexo" style="width:70%"    >	                                        
                                                            <option value=""> </option>  
                                                            <option value="F">Mujer</option> 
                                                            <option value="M">Hombre</option>                              
                                                            <option value="X">No disponible</option>                                                          
                                                        </select>	                   
                                                    </td>     
                                                    <td><input type="text" value=""  size="10"  maxlength="10"  id="txtFechaNacimiento"  /></td> 
                                                    <td colspan="2">
                                                        <input type="text"  id="txtTelefono" style="width:30%"  size="50"  maxlength="50"  >
                                                        <input type="text"  id="txtCelular1" style="width:30%"  size="50"  maxlength="50"  >
                                                        <input type="text"  id="txtCelular2" style="width:30%"  size="50"  maxlength="50"  >
                                                    </td>                                                             
                                                    <td colspan="2" ><input type="text"  id="txtAdministradora1"  size="60"  maxlength="60"   onkeypress="llamarAutocomIdDescripcionConDato('txtAdministradora1', 308)" style="width:98%"   />                                  	 </td>                                                                                                                                                                
                                                </tr>  
                                                <tr class="titulos"> 
                                                    <td colspan="2" >Municipio</td> 
                                                    <td colspan="2" >Direccion</td>   
    
                                                    <td colspan="2" >Correo</td>
    
                                                </tr>		
                                                <tr class="estiloImput"> 
                                                    <td colspan="2" ><input type="text" id="txtMunicipioResi"  style="width:90%"  size="20"  maxlength="20"  >
                                                        <img width="18px" height="18px" align="middle" title="VEN1" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipioResi', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
                                                        <div id="divParaVentanita"></div>                                                    
                                                    </td>      
                                                    <td colspan="2" ><input type="text" id="txtDireccion" style="width:90%"  size="200"  maxlength="200" onblur="v28(this.value, this.id);" ></td>                            
                                                    <td colspan="2">
                                                        <input type="text" id="txtEmail" size="60"  maxlength="60"  style="width:90%"  /> 
                                                    </td>
    
                                                </tr>  
                                                <tr class="titulos">
                                                    <td width="10%" >Id</td>                                 
                                                    <td width="10%" >Nueva identificacion</td>  
                                                </tr>
                                                <tr class="estiloImput">
                                                    <td><label id="lblId"></label></td>      
                                                    <td><input id="chk_nuevaIdPaciente" type="checkbox" value="" onchange="nuevaIdentificacion(this.id)" /></td>                                                                                                                                
                                                </tr>
    
                                            </table> 
                                        </td>   
                                    </tr>   
                                </table>
    
                                <div id="divEditarIdNuevaPaciente" style="display:none; width:100%;" align="center" >  
                                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" style="cursor:pointer" >
                                        <tr  class="titulos"><td width="50%">Id Anteriores</td><td width="20%">Tipo Id Nueva</td> <td width="30%"> Identificación Nueva</td><td>&nbsp;</td>
                                        </tr>
                                        <tr class="estiloImput"> 
                                            <td><label id="lblIdAnteriores"></label></td>
                                            <td> <input name="btn_nueva_Id" type="button" value="Correccion de identificación" onclick="modificarCRUD('correccion_id_historico');" /></td>   
                                            <td><select size="1" style="width: 150px;"  id="cmbTipoIdNueva" style="width:80%" title="18"  >	                                        
                                                    <option value="">[Todos ]</option>
                                                    <%     resultaux.clear();
                                                           resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                                           ComboVO cmb2;
                                                           for(int k=0;k<resultaux.size();k++){ 
                                                                 cmb2=(ComboVO)resultaux.get(k);
                                                    %>
                                                    <option value="<%= cmb2.getId()%>" title="<%= cmb2.getTitle()%>"><%= cmb2.getDescripcion()%></option>
                                                    <%}%>						
                                                </select>	                   
                                            </td> 
                                            <td><input type="text"  value="" id="txtIdentificacionNueva" style="width:90%"  size="20"  maxlength="20"   ></td>  
                                            <td> <input name="btn_nueva_Id" type="button" value="Guardar identificación homologacion" onclick="modificarCRUD('btn_nueva_Id', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" /></td>   
                                        </tr>  
                                    </table> 
                                </div>
    
                            </div>--><!-- divEditar--> 
    
    
                            
    
                        </td>
                    </tr>
                    
                </table>
                <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>"/>    
                <!--      <input type="text" id="IdTabActivo" value="1"/>          -->
            </td>
        </tr> 
        <tr class="estiloImput" align="left"> 
            <td>HOMOLOGACIONES DE IDENTIFICACION DEL PACIENTE
            </td>
        </tr>             
        <tr>
            <td width="80%">
                <table id="listGrillaHomologacionIdentifica" class="scroll"></table>  
            </td>
        </tr> 
        
    </table>

</table>

<div class="bubble" id="bubble" onclick="maximizarVentanita('divInfoPacienteVentana')">
    <i class="fa-solid fa-address-card"></i>
</div>


<div id="divInfoPacienteVentana" style="display: none; top: 75px; right: 3%; width: 170px; position: fixed; white-space: normal;" onclick="moverTarjeta()">
    <div class="card-container">
  
        <div class="interno-card" id="miTarjeta">
          <div class="class-card barbarian">
            <div class="class-card__image class-card__image--barbarian">
                <!-- <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divInfoPacienteVentana')" /> -->
                <label onclick="ocultar('divInfoPacienteVentana')" class="cerrarInfoPaciente">
                    <i class="fas fa-times-circle"></i>
                </label>
                <label onclick="minimizarVentanita('divInfoPacienteVentana')" class="minimizarInfoPaciente">
                    <i aria-hidden="true" class="fas fa-chevron-circle-down" ></i>
                </label>
              <i class="fas fa-hospital-user fasCard"></i>
            </div>
            <div class="class-card__level class-card__level--barbarian">NOMBRE PACIENTE:</div>
            <div class="class-card__unit-name"><label id="lblNombrePacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label></div>
            <div class="grRh">
                <label for="">RH:</label>
                <label for="" id="lblRH"></label>
            </div>
            <div class="class-card__unit-description">
                <label id="lblEdadPacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label><label class="" style="color:white;"></label>
            </div>

            
            <div class="parentCard">
                <div id="divDatosGestantes" style="display: flex; padding-top: 5%; margin-left: 13%; gap:5px;">
                    <label style="color:rgb(231, 58, 58);"> <b>Gestante</b> </label>

                    <div class="onoffswitch2" >
                        <input type="checkbox" name="chkgestante" class="onoffswitch-checkbox2" id="chkgestante"  onclick="valorSwitchsn(this.id, this.checked);guardarGestanteVentanita(document.getElementById('txtIdPac').value , this.value,'ventana paciente'); ">  
                        <label class="onoffswitch-label2" for="chkgestante">
                            <span  class="onoffswitch-inner2" id="chkgestante-span1"></span>
                            <span  class="onoffswitch-switch2" id="chkgestante-span2"></span>
                        </label>
                    </div>

                </div>
                <div>
                    <!-- <h5>Creatinina</h5> -->
                    <p><label id="lblCreatininaVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
                </div>
                <div>
                    <!-- <h5>TFG Actual</h5> -->
                    <p><label id="lblTFGVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
                </div>
                <div>
                    <!-- <h5>GRADO</h5> -->
                    <p><label id="lblEstadioVentanita" class="" style="color:rgb(1, 1, 1);"></label></p>
                </div>
            </div>      
          </div> <!-- end class-card barbarian-->
        </div> <!-- end wrapper -->
      </div> <!-- end container -->
</div>




<input id="txtIdPac" type="hidden"/>
<input style="width: 150px;"  id="cmbTipoId" type="hidden"/>
<input id="txtIdentificacion" type="hidden"/>
<input id="txtNombre1" type="hidden"/>
<input id="txtNombre2" type="hidden"/>
<input id="txtApellido1" type="hidden"/>
<input id="txtApellido2" type="hidden"/>
<input id="txtFechaNac" type="hidden"/>
