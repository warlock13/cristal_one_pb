<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center"   border="0" cellspacing="0" cellpadding="1"   >
<tr>
  <td>
     <!-- AQUI COMIENZA EL TITULO -->	
        <div align="center" id="tituloForma" ><!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
            <jsp:include page="../../titulo.jsp" flush="true">
                   <jsp:param name="titulo" value="DIAGNOSTICO" />
            </jsp:include>
        </div>	
       <!-- AQUI TERMINA EL TITULO -->
  </td>
</tr> 
<tr>
 <td>
    <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr>
       <td>

  <div style="overflow:auto;height:350px; width:100%"   id="divContenido" >   <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
     <div id="divBuscar"  style="display:block"   >
     <table width="100%"   cellpadding="0" cellspacing="0"  align="center">
          <tr class="titulos" align="center">
             <td width="10%">CODIGO</td>                            
             <td width="60%">NOMBRE</td>
             <td width="20%">VIGENTE</td>
             <td width="10%">&nbsp;</td>                            
          </tr>
          <tr class="estiloImput"> 
             <td colspan="1"><input type="text" id="txtCodDiagnosticoBus"  style="width:90%"   /> 
             </td> 
             <td colspan="1"><input type="text" id="txtNomDiagnosticoBus"  style="width:90%"   /> 
             </td> 
             <td><select size="1" id="cmbVigenteBus" style="width:80%"  >	           
                 <option value="">TODOS</option>                             
                 <option value="S">SI</option>
                 <option value="N">NO</option>                  
                </select>	                   
             </td>     
             <td>
                 <input name="btn_MODIFICAR" title="bw22" type="button" class="small button blue" value="BUSCAR"  onclick="buscarHistoria('listGrillaDiagnostico')"   />                    
             </td>         
         </tr>	                          
     </table>
     <table id="listGrillaDiagnostico" class="scroll"></table>  
   </div>              
 </div><!-- div contenido-->

<div id="divEditar" style="display:block; width:100%" >  
    <table width="100%" cellpadding="0" cellspacing="0" align="center" id="infoPaciente" style="font-size: 9px;">
        <tr class="titulosCentrados">
          <td colspan="3">INFORMACION DEL DIAGNOSTICO
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" cellpadding="0" cellspacing="0" align="center">
              <tr>
                <td>
                  <table width="100%">
                    <tr class="estiloImputIzq2">
                      <td width="30%">CODIGO</td>
                      <td width="50%" colspan="2"><input type="text" id="txtIdCodigo" style="width:80%;height: 15px;" readonly  />
                      </td>
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="30%">NOMBRE</td>
                      <td width="50%" colspan="2"><input type="text" id="txtNombre" style="width:80%;height: 15px;" readonly  />
                      </td>
                    </tr>
                    <tr class="estiloImputIzq2">
                      <td width="30%">INDICACION</td>
                      <td width="50%" colspan="2"><input type="text" id="txtIndicacion" style="width:80%;height: 15px;" onkeyup="javascript: this.value= this.value.toUpperCase();" />
                      </td>
                    </tr>                                              
                    <tr   class="estiloImputIzq2">                         
                      <td width="30%">SEXO</td>
                      <td width="50%" colspan="2">
                        <select size="1" id="cmbSexo" style="width:80%"  >	                                        
                            <option value=""></option>    
                            <option value="A">AMBOS</option>
                            <option value="M">MASCULINO</option>
                            <option value="F">FEMENINO</option>
                            <option value="O">OTRO</option>                  
                      </td>                                             
                    </tr>  
                    <tr   class="estiloImputIzq2">                         
                      <td width="30%">EDAD INICIAL (MESES)</td>
                      <td width="50%" colspan="2"><input type="text" id="txtEdadIni" style="width:80%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                      </td>                                             
                    </tr> 
                    <tr   class="estiloImputIzq2">                         
                      <td width="30%">EDAD FINAL (MESES)</td>
                      <td width="50%" colspan="2"><input type="text" id="txtEdadFin" style="width:80%;height: 15px;" onkeypress="javascript:return soloTelefono(event);" />
                      </td>                                             
                    </tr>             
                    <tr class="estiloImputIzq2">
                      <td width="30%">VIGENTE</td><td width="50%" colspan="2">
                        <select size="1" id="cmbVigente" style="width:80%"  >	 
                            <option value=""></option>                                       
                            <option value="S">SI</option>
                            <option value="N">NO</option>                  
                           </select>                       
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" style="cursor:pointer">
                <tr>
                    <td width="99%" class="titulos">
                        <input name="btn_MODIFICAR" title="b668" type="button" class="small button blue" value="MODIFICAR DIAGNOSTICO" onclick="modificarCRUD('activarCIE','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                    </td>
                </tr>
            </table>
          </td>
        </tr>
    </table> 
</div><!-- divEditar--> 
     

       </td>
      </tr>
     </table>

  </td>
</tr> 

</table>

