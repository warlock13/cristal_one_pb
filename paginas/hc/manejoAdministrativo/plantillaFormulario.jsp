<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	
    beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="Administracion de Plantillas" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla"> 
                <tr>
                    <td>

                        <div style="overflow:auto;height:350px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center" style="margin-top: 10px;">
                                    <tr class="titulos" align="center" >
                                        <td width="10%">ID PLANTILLA</td>
                                        <td width="60%">NOMBRE</td>
                                        <!--<td width="10%">VIGENTE</td>-->
                                        <td width="20%">TIPO</td>
                                        <td width="10%">&nbsp;</td>
                                    </tr>
                                    <tr class="estiloImput">
                                        <td colspan="1"><input type="text" id="txtCodPlantilla" style="width:90%"
                                                 onKeyPress="javascript:return teclearsoloDigitos(event);" />
                                        </td>
                                        <td colspan="1"><input type="text" id="txtNomPlantilla" style="width:90%"
                                                 onkeyup="javascript: this.value= this.value.toUpperCase();" 
                                                onkeypress="javascript:return teclearsoloAlfabeto(event);"/> 
                                        </td>
                                        <!--<td><select size="1" id="cmbVigenteBus" style="width:80%" >
                                                <option value="1">1-SI</option>
                                                <option value="0">0-NO</option>
                                            </select>
                                        </td>-->
                                        <td>
                                            <select size="1" id="cmbTipoBus" style="width:90%" >
                                                <option value=""> </option>
                                                        
                                                <option value="1">EXAMEN LABORATORIO
                                                </option>
                                                
                                                <option value="2">ENFERMERIA
                                                </option>
                                                
                                                <option value="3">FOLIO
                                                </option>
                                                
                                                <option value="4">ENCUESTA
                                                </option>
                                                <!--<option value="5">FOLIO ESPECIALIDAD ANTECED
                                                </option>-->
                                                <option value="5">FOLIO ANTECEDENTES
                                                </option>
                                                <option value="6">ADMINISTRATIVO
                                                </option>
                                              
                                            </select>
                                        </td>
                                        <td>
                                            <input name="btn_MODIFICAR" title="btn_pl22" type="button"
                                                class="small button blue" value="BUSCAR"
                                                onclick="buscarHistoria('listGrillaPlantilla')" />
                                        </td>
                                    </tr>
                                </table>
                                <table id="listGrillaPlantilla" class="scroll"  ></table>
                            </div>
                        </div><!-- div contenido-->

                        <div id="divEditar" style="display:block; width:100%">

                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3"> ELEMENTO A EDITAR
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td class="estiloImputIzq2"  width="10%">CODIGO: </td>
                                                <td><label id="lblCodigo" style="height: 20px;"></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td class="estiloImputIzq2" >NOMBRE: </td>
                                                <td><input class="estImputAdminLargo" id="txtNombre"  onkeyup="javascript: this.value= this.value.toUpperCase();"/></td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputIzq2"  >CONTENIDO: </td>
                                                <td>
                                                    <input class="estImputAdminLargo"  id="txtContenido" onkeyup="javascript: this.value= this.value.toUpperCase();" 
                                                     />    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputIzq2" >TIPO: </td>
                                                <td><select class="estImputAdmincomboPequeño" size="1" id="cmbTipo" >
                                                        <option value=""></option>
                                                        <%     resultaux.clear();
                                                                           resultaux=(ArrayList)beanAdmin.combo.cargar(306);	
                                                                           ComboVO cmbPL; 
                                                                           for(int k=0;k<resultaux.size();k++){ 
                                                                                         cmbPL=(ComboVO)resultaux.get(k);
                                                                                          if( ! cmbPL.getId().equals("0")){ 
                                                        %>
                                                        <option value="<%= cmbPL.getId()%>"><%= cmbPL.getDescripcion()%> 
                                                        </option>
                                                        <%}}%>
                                                    </select> </td>
                                            </tr>

                                            <tr>
                                                <td class="estiloImputIzq2" >ANCHO COLUMNA 1 (%)</td>
                                                <td>
                                                    <input class="estImputAdminPequeño" type="text" id="txtAnCol1" onchange="calculoAncho();" 
                                                     onKeyPress="javascript:return teclearsoloDigitos(event);" />  
                                                    
                                                </td>
                                            </tr>
                                            <tr >
                                                <td class="estiloImputIzq2" >ANCHO COLUMNA 2 (%)</td> 
                                                <td>
                                                    <input class="estImputAdminPequeño" type="text" id="txtAnCol2"    readonly 
                                                     />  
                                                </td>
                                            </tr>

                                            <tr >
                                                <td class="estiloImputIzq2" >TABLA </td> 
                                                <td>
                                                    <input class="estImputAdminPequeño" type="text" id="txtTabla"  
                                                     />  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputIzq2" >SEXO: </td>
                                                <td><select class="estImputAdmincomboPequeño" size="1" id="cmbSexo" >
                                                        <option value="A">AMBOS</option>
                                                        <option value="F">FEMENINO</option>
                                                        <option value="M">MASCULINO</option>                                                       
                                                    </select> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputIzq2" >RANGO EDAD (CURSOS DE VIDA): </td>
                                                <td><select class="estImputAdmincomboPequeño" size="1" id="cmbRangoEdad" >                                                    
                                                    <%
                                                        resultaux.clear();
                                                        resultaux=(ArrayList)beanAdmin.combo.cargar(116);	
                                                        for(int k=0;k<resultaux.size();k++){ 
                                                            cmbPL=(ComboVO)resultaux.get(k);
                                                        %>
                                                    <option value="<%= cmbPL.getId()%>"
                                                        title="<%= cmbPL.getTitle()%>">
                                                        <%= cmbPL.getId() + " " +cmbPL.getDescripcion()%></option>
                                                    <%}%>                                                      
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputIzq2" >EDAD INICIAL MESES: </td>
                                                <td> <input class="estImputAdminPequeño" type="text" id="txtEdadInicial"  
                                                     />  
                                                 </td>
                                            </tr>
                                            <tr>
                                                <td class="estiloImputIzq2" >EDAD FINAL MESES: </td>
                                                <td><input class="estImputAdminPequeño" type="text" id="txtEdadFinal"  
                                                     /> </td>
                                            </tr>


                                           

                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%" style="cursor:pointer">
                                <tr>
                                    <td width="99%" class="titulos">
                                        <input name="btn_MODIFICAR" title="btn_cP78" type="button"
                                            class="small button blue" value="CREAR"
                                            onclick="modificarCRUD('crearPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                        <input name="btn_MODIFICAR" title="btn_cP41" type="button" 
                                            class="small button blue" value="MODIFICAR"
                                            onclick="modificarCRUD('modificarPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                        <input name="btn_MODIFICAR" title="btn_L63" type="button"
                                            class="small button blue" value="LIMPIAR"
                                            onclick="limpiarDivEditarJuan('plantillaFormulario')" />
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>

                <tr class="titulosCentrados">
                    <td> ELEMENTOS DE LA PLANTILLA
                    </td>
                </tr>

                <tr>
                    <td>
                        <table id="listGrillaPlantillaElemento" class="scroll"></table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div id="divEditar2" style="display:block; width:100%">

                        <table width="100%" cellpadding="0" cellspacing="0" align="center"  >
                                <tr class="titulosCentrados">
                                    <td colspan="3"> ADICIONAR ELEMENTOS A LA PLANTILLA
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" onclick="valTipoCampo(); validacionTipoEntrada(); valBotonFormula()" >
                                            <td class="estiloImputIzq2" width="10%">DESCRIPCION: </td>
                                            <td><input class="estImputAdminLargo" id="txtDescripcion"  
                                                onkeyup="javascript: this.value= this.value.toUpperCase();"/></td>
                                </tr>
                                <tr>
                                    <td class="estiloImputIzq2">ETIQUETA: </td>
                                    <td><input class="estImputAdminLargo" id="txtEtiqueta"  
                                        onkeyup="javascript: this.value= this.value.toUpperCase();"/></td>
                                </tr>
                               
                                <tr>
                                    <td class="estiloImputIzq2">ID PADRE ORDEN: </td>
                                    <td><input class="estImputAdminPequeño" id="txtOrdenPadre"   
                                        onKeyPress="javascript:return teclearsoloDigitos(event);"/>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="estiloImputIzq2">JERARQUIA: </td>
                                    <td>
                                        <select class="estImputAdmincomboPequeño" name="" id="cmbJerarquia" onchange="valTipoCampo()">
                                            <option value=""></option>
                                            <option value="PA">PADRE</option>
                                            <option value="HI">HIJO</option>
                                            <option value="TI">TITULO</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>

                                    
                                    <td class="estiloImputIzq2" >TIPO DE ENTRADA:</td>
                                         <td >
                                                <select class="estImputAdmincomboPequeño" name="" id="cmbTipoEntrada" onchange="validacionTipoEntrada()">
                                                    <option value=""></option>
                                                    <option value="input">Texto</option>
                                                    <option value="inputl">Texto Lupita</option>
                                                    <option value="inputFecha">Texto Fecha</option>                                                    
                                                    <option value="numeros">Texto Numerico</option>
                                                    <option value="decimal">Decimales</option>
                                                    <option value="hora">Hora</option>
                                                    <option value="texta">Area de texto</option>
                                                    <option value="textadc">Area de texto con Diccionario</option>
                                                    <option value="combo">Combo</option>
                                                    <option value="switchsn">Switch(Si/No)</option>
                                                    <option value="switch10">Switch(1 / 0)</option>
                                                    <option value="switchobs">Switch OBS</option>
                                                </select>

                                            </td>
                                        
                                </tr>

                                <tr>
                                    <td class="estiloImputIzq2">REFERENCIA: </td>
                                    <td><input class="estImputAdminPequeño" id="txtReferencia"  /></td>
                                    <td><input id="txtPlantilla" type='hidden' /></td>
                                    <td><input id="txtOrden" type='hidden' /></td>
                                </tr>

                                <tr>
                                    <td class="estiloImputIzq2">CONSULTA PARA COMBO:</td>
                                    <td >
                                        <select class="estImputAdminLargo" id="cmbConsultaCombo">
                                            <option value="0"></option>
                                            <%
                                                resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(585);	
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbPL=(ComboVO)resultaux.get(k);
                                                %>
                                            <option value="<%= cmbPL.getId()%>"
                                                title="<%= cmbPL.getTitle()%>">
                                                <%= cmbPL.getId() + " " +cmbPL.getDescripcion()%></option>
                                            <%}%>			   
                                            </select>
                                        </td>
                                </tr>

                                <tr>
                                    <td class="estiloImputIzq2">CONSULTA PARA VENTANITA:</td>
                                    <td >
                                        <select class="estImputAdminLargo" id="cmbConsultaVentanita">
                                            <option value="0"></option>
                                            <%
                                                resultaux.clear();
                                                resultaux=(ArrayList)beanAdmin.combo.cargar(1250);	
                                                for(int k=0;k<resultaux.size();k++){ 
                                                    cmbPL=(ComboVO)resultaux.get(k);
                                                %>
                                            <option value="<%= cmbPL.getId()%>"
                                                title="<%= cmbPL.getTitle()%>">
                                                <%= cmbPL.getId() + " " +cmbPL.getDescripcion()%></option>
                                            <%}%>			   
                                            </select>
                                        </td>
                                </tr>
                                <tr>
                                    <td class="estiloImputIzq2">CANTIDAD DE DECIMALES:</td>
                                    <td >
                                        <select class="estImputAdminLargo" id="cmbDecimales">
                                                    <option value=""></option>
                                                    <option value="0.1">1 Decimal</option>
                                                    <option value="0.01">2 Decimales</option>
                                                    <option value="0.001">3 Decimales</option>
                                                    <option value="0.0001">4 Decimales</option>                                                    
                                            </select>
                                        </td>
                                </tr>

                                <tr>
                                    <td class="estiloImputIzq2">SEXO:</td>
                                    <td >
                                        <select class="estImputAdminLargo" id="cmbSexoPD">
                                                    <option value="A">AMBOS</option>
                                                    <option value="F">FEMENINO</option>
                                                    <option value="M">MASCULINO</option>                                                                                                   
                                            </select>
                                        </td>
                                </tr>

                                <tr>                                    
                                    <td class="estiloImputIzq2" >RANGO EDAD (CURSOS DE VIDA): </td>
                                                <td><select class="estImputAdminLargo" id="cmbRangoEdadPD" >                                                    
                                                    <%
                                                        resultaux.clear();
                                                        resultaux=(ArrayList)beanAdmin.combo.cargar(116);	
                                                        for(int k=0;k<resultaux.size();k++){ 
                                                            cmbPL=(ComboVO)resultaux.get(k);
                                                        %>
                                                    <option value="<%= cmbPL.getId()%>"
                                                        title="<%= cmbPL.getTitle()%>">
                                                        <%= cmbPL.getId() + " " +cmbPL.getDescripcion()%></option>
                                                    <%}%>                                                      
                                                    </select> </td>
                                </tr>
                                <tr>
                                    <td class="estiloImputIzq2" >EDAD INICIAL MESES: </td>
                                    <td> <input class="estImputAdminPequeño" type="text" id="txtEdadInicialD"  
                                         />  
                                     </td>
                                </tr>
                                <tr>
                                    <td class="estiloImputIzq2" >EDAD FINAL MESES: </td>
                                    <td><input class="estImputAdminPequeño" type="text" id="txtEdadFinalD"  
                                         /> </td>
                                </tr>
                                <tr>
                                    <td class="estiloImputIzq2" >FOLIO: </td>
                                    <td><input class="estImputAdminPequeño" type="text" id="txtFolioD"  
                                         /> </td>
                                </tr>

                                    <tr>
                                        <td class="estiloImputIzq2">ANCHO DEL CAMPO(%): </td>
                                        <td>
                                            <input class="estImputAdminPequeño" type="text" onchange="anchoCampo()" id="txtAncho"
                                            onKeyPress="javascript:return teclearsoloDigitos(event);">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="estiloImputIzq2">ALTURA TEXTAREA(px): </td>
                                        <td>
                                            <input class="estImputAdminPequeño" type="text" id="txtAlto"
                                            onKeyPress="javascript:return teclearsoloDigitos(event);">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="estiloImputIzq2">MAXLENGHT: </td>
                                        <td ><input class="estImputAdminPequeño" type="text" id="txtMaxlenght"   
                                            onKeyPress="javascript:return teclearsoloDigitos(event);"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="estiloImputIzq2">VALOR POR DEFECTO: </td>
                                        <td ><input class="estImputAdminPequeño" id="txtValorDefecto"   /></td>
                                    </tr>

                                    <tr>
                                        <input type="text" id="txtOcultoFormulaEditar" hidden >
                                        <input type="text" id="txtOcultoCampoResultado" hidden >
                                        <input type="text" id="txtOcultoCamposFormula" hidden >
                                    </tr>

                                    <tr>
                                        <td class="estiloImputIzq2">ID FORMULA: </td>
                                        <td > <input class="estImputAdminPequeño" id="txtidFormula"  onchange="valBotonFormula()"   readonly/> 
                            
                                            <input type="button" id="btnFormula"  hidden> 
                                            <input type="button" id="btnGuardarFormula" hidden>
                                    
                                        </td>

                                    </tr>
                                    <tr id="separadorFormula" hidden class="titulosCentrados">
                                        <td style="color: #06C;" > - </td>
                                        <td >  FORMULA </td>
                                    </tr>
                                    <tr id="campoResultadoFormula" hidden > 
                                        <td class="estiloImputIzq2"> CAMPO RESULTADO </td>
                                        <td> <input class="estImputAdminPequeño" type="text" id="txtCampoResultado" readonly > </td>
                                    </tr>
                                    <tr id="agregarValorFormula" hidden>
                                        <td class="estiloImputIzq2"> AGREGAR VALOR DIFERENTE</td>
                                        <td> 
                                            <input type="text" class="estImputAdminPequeño" id="valorAdicional">
                                            <label > 
                                                <input type="button" onclick="adicionarValorFormula()" class="small button blue" value="ADICIONAR" > 
                                                <input type="button" class="small button blue" onclick="cambiarValor()" value="CAMBIAR">
                                            </label>                                            
                                        </td> 
                                    </tr>

                                    <tr id="agregarFormula1" hidden>

                                        <td class="estiloImputIzq2"> FILTROS: </td>
                                        <td>
                                            <select class="estImputAdminLargo" id="cmbEsquema" style="width:40%" onchange="validacionTipoEntrada()">
                                                <option value="">Seleccione Esquema</option>
                                                    <%
                                                    resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(899);	
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmbPL=(ComboVO)resultaux.get(k);
                                                    %>
                                                        <option value="<%= cmbPL.getId()%>" title="<%= cmbPL.getTitle()%>">
                                                            <%=cmbPL.getDescripcion()%>
                                                        </option>
                                                    <%}%>			   
                                            </select>
                                            <select id="cmbTabla" class="estImputAdminLargo" style="width:40%" onfocus="cargarTablasEsquema('cmbEsquema', 'cmbTabla')" onchange="cargarCamposDesdeEsquemaTabla('cmbEsquema', 'cmbTabla', 'cmbRefCampo')">	                                        
                                               <option value="">Seleccione Tabla</option>
                                            </select>
                                            <div style="width:10%">
                                                <input type="checkbox" id="chkIdentificador" class="estImputAdminPequeño" onchange="cambiarIdentificador()" checked> Identificador (IdEvolucion)  
                                            </div>
                                            <select id="cmbIdentificador" class="estImputAdminLargo" onfocus="cargarCamposDesdeEsquemaTabla('cmbEsquema', 'cmbTabla', 'cmbIdentificador')"  style="width:40%;" hidden>
                                                <option value="">Seleccione Identificador</option>
                                            </select>
                                            <input type="text" class="estImputAdminPequeño" id="txtIdentificador" style="width:40%" hidden>
                                        </td> 
                                    </tr>

                                    <tr id="agregarFormula3" hidden>

                                        <td class="estiloImputIzq2"> LABORATORIO: </td>
                                        <td>
                                            <select class="estImputAdminLargo" id="cmbLaboratorio" style="width:40%" onchange="validacionTipoEntrada()">
                                                <option value="">Seleccione Procedimiento</option>
                                                    <%
                                                    resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(1251);	
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmbPL=(ComboVO)resultaux.get(k);
                                                    %>
                                                        <option value="<%= cmbPL.getId()%>" title="<%= cmbPL.getTitle()%>">
                                                            <%=cmbPL.getDescripcion()%>
                                                        </option>
                                                    <%}%>			   
                                            </select>
                                            <select id="cmbLaboratorioDetalle" class="estImputAdminLargo" style="width:40%" onfocus="cargarLaboratorioDetalle('cmbLaboratorio', 'cmbLaboratorioDetalle')">	                                        
                                               <option value="">Seleccione Analito</option>
                                            </select>
                                            <label >
                                                <input type="button" class="small button blue " onclick="pasarLaboratorioFormula()" value="ADICIONAR">
                                                <br>
                                                <br>
                                                <input type="button" class="small button blue" onclick="cambiarLaboratorio()" value="CAMBIAR">
                                            </label>
                                        </td> 
                                    </tr>

                                    <tr id="agregarFormula2" hidden>
                                        <td class="estiloImputIzq2"> CAMPO: </td>

                                        <td style="display: flex;align-items: center;" class="scrollHorizontalFormula" >
                                            <select style="height: 150px; width: 215px; font-size: 12px; " multiple="multiple"  class="estImputAdminPequeño" id="cmbRefCampo">                                                
                                            </select>

                                            <label >
                                                <input type="button" class="small button blue" onclick="pasarOperando()" value="ADICIONAR">
                                                <br>
                                                <br>
                                                <input type="button" class="small button blue" onclick="cambiarOperando()" value="CAMBIAR">
                                            </label>
                                            <label  class="estiloImputIzq2" style="padding-left: 20px;align-items: center;display: flex;" > OPERADOR 
                                                
                                                <select  style="height: 150px; width: 90px; font-size: 14px; text-align: center; "  multiple="multiple" class="estImputAdminPequeño" name="cmbOperador[]" id="cmbOperador">
                                                    <option style=" padding-top: 1.5px;" value="*" > * </option>
                                                    <option class="optionOperador" value="/" > / </option>
                                                    <option class="optionOperador" value="+" > + </option>
                                                    <option class="optionOperador" value="-" > - </option>
                                                    <option class="optionOperador" value="^" > ^ </option>
                                                    <option class="optionOperador" value="==" > == </option> 
                                                    <option class="optionOperador" value="!=" > != </option> 
                                                    <option class="optionOperador" value="<" > < </option>
                                                    <option class="optionOperador" value=">" > > </option>
                                                    <option class="optionOperador" value="<=" > <= </option>
                                                    <option class="optionOperador" value=">=" > >= </option>
                                                    <option class="optionOperador" value="(" > ( </option>
                                                    <option class="optionOperador" value=")" > ) </option>
                                                    <option class="optionOperador" value="&&" > && </option>
                                                    <option class="optionOperador" value="||" > || </option>
                                                    <option class="optionOperador" value="!" > ! </option>           
                                                    <option class="optionOperador" value="IF" > IF </option>
                                                    <option class="optionOperador" value="THEN" > THEN </option>
                                                    <option class="optionOperador" value="ELSE" > ELSE </option>
                                                    <option class="optionOperador" value="ENDIF" > ENDIF </option>
                                                </select> 
                                            </label>
                                            <label >
                                                <input type="button" class="small button blue " onclick="pasarOperadorFormula()" value="ADICIONAR">
                                                <br>
                                                <br>
                                                <input type="button" class="small button blue" onclick="cambiarOperador()" value="CAMBIAR">
                                            </label>
                                        </td>
                                    </tr>

                                    <tr id="construirFormula" style="box-sizing: border-box;" hidden > 
                                        <td class="estiloImputIzq2">CONSTRUIR FORMULA </td>                                        
                                        <td id="datafo" >                                        
                                            <select class="selectFormula" id="construFormula" name="construFormula[]" multiple="multiple" size="1"></select>
                                            <label > <input type="button" class="small button blue" onclick="eliminarCampo()" value="ELIMINAR"> </label>
                                        </td> 
                                    </tr>

                                    <tr id="construirFormula2" hidden > 
                                        <td class="estiloImputIzq2" > FORMULA </td>  
                                        <td >
                                            <input type="text" class="estImputAdminLargo" id="txtFormulaCompleta"   readonly/>
                                            <input type="text" class="estImputAdminLargo" id="txtCamposFormula"   readonly />
                                            <input type="text" id="txtFormulaConvertida" hidden/>
                                        </td>
                                    </tr>
                        </table> 
                    </td>   
                </tr>   

                <tr>
                    <td width="99%" class="titulos">
                        <input name="btn_MODIFICAR" title="btn_PE01" type="button" class="small button blue" value="CREAR" onclick="modificarCRUD('crearPlantillaElemento', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                        <input name="btn_MODIFICAR" title="btn_cP41" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarPlantillaElemento', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />  
                        <input name="btn_MODIFICAR" title="btn_L63" type="button" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('plantillaElemento')"  />  
                    </td>
                </tr>
            </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="divEditar3" style="display:block; width:100%">
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                    <tr class="titulosCentrados">
                        <td colspan="3"> VALIDACIONES PLANTILLA </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="listGrillaValidacionesPlantilla" class="scroll"></table>
                        </td>
                    </tr>                    
                </table>
                <div id="divEditarValidacion">
                    <table width="100%">
                        <tr>
                            <td class="estiloImputIzq2"  width="10%">CODIGO: </td>
                            <td><input class="estImputAdminLargo" id="txtValCodigo"  onkeyup="javascript: this.value= this.value.toUpperCase();"/></td>
                            
                        </tr>
                        <tr>
                            <td class="estiloImputIzq2" >DESCRIPCION: </td>
                            <td><input class="estImputAdminLargo" id="txtValDescripcion"  /></td>
                        </tr>
                        <tr><td></td><td><input id="txtValTipo" value="1" hidden></td></tr>
                    </table>
                </div>                
                <table width="100%" style="cursor:pointer">
                    <tr>
                        <td width="99%" class="titulos">
                            <input name="btn_MODIFICAR" title="btn_cP78" type="button" class="small button blue" value="CREAR" onclick="modificarCRUD('crearValidacionPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                            <input name="btn_MODIFICAR" title="btn_cP41" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarValidacionPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                            <input name="btn_MODIFICAR" title="btn_L63" type="button" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('divEditarValidacion')" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="divEditar4" style="display:block; width:100%">
                <table width="100%">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3"> CONDICIONES </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="listGrillaValidacionesCondiciones" class="scroll"></table>
                                    </td>
                                </tr>                    
                            </table>
                            <div id="divEditarCondicion">
                                <table width="100%">
                                    <tr>
                                        <td class="estiloImputIzq2"> CAMPO: </td>
                                        <td>
                                            <input id="txtIdCondicion" hidden/>
                                            <select class="estImputAdminLargo" id="cmbCondEsquemaCampo" style="width:40%" onchange="validacionTipoEntrada()">
                                                <option value="">Seleccione Esquema</option>
                                                    <%
                                                    resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(899);	
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmbPL=(ComboVO)resultaux.get(k);
                                                    %>
                                                        <option value="<%= cmbPL.getId()%>" title="<%= cmbPL.getTitle()%>">
                                                            <%=cmbPL.getDescripcion()%>
                                                        </option>
                                                    <%}%>			   
                                            </select>
                                            <select id="cmbCondTablaCampo" class="estImputAdminLargo" style="width:40%" onfocus="cargarTablasEsquema('cmbCondEsquemaCampo', 'cmbCondTablaCampo')" onchange="cargarCamposDesdeEsquemaTabla('cmbCondEsquemaCampo', 'cmbCondTablaCampo', 'cmbCondRefCampo')">	                                        
                                                <option value="">Seleccione Tabla</option>
                                            </select>
                                            <select id="cmbCondRefCampo" style="width:40%" class="estImputAdminPequeño" onchange="cargarCampoValidacion('cmbCondEsquemaCampo', 'cmbCondTablaCampo', this.id, 'txtCondCampo')" >                                                
                                            </select>
                                            <input class="estImputAdminLargo" id="txtCondCampo"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="estiloImputIzq2"> OPERADOR: </td>
                                        <td>
                                            <select  style="width:40%"  class="estImputAdminPequeño" name="cmbOperadorCondicion[]" id="cmbOperadorCondicion">
                                                <option class="optionOperador" value="==" > == </option> 
                                                <option class="optionOperador" value="!=" > != </option> 
                                                <option class="optionOperador" value="<" > < </option>
                                                <option class="optionOperador" value=">" > > </option>
                                                <option class="optionOperador" value="<=" > <= </option>
                                                <option class="optionOperador" value=">=" > >= </option>
                                                <option class="optionOperador" value="!" > ! </option>           
                                            </select> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="estiloImputIzq2" >VALOR: </td>
                                        <td><input class="estImputAdminLargo" id="txtCondValor"  /></td>
                                    </tr>
                                    <tr>
                                        <td class="estiloImputIzq2"> CAMPO COMPARA: </td>
                                        <td>
                                            <select class="estImputAdminLargo" id="cmbCondEsquemaComp" style="width:40%" onchange="validacionTipoEntrada()">
                                                <option value="">Seleccione Esquema</option>
                                                    <%
                                                    resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(899);	
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmbPL=(ComboVO)resultaux.get(k);
                                                    %>
                                                        <option value="<%= cmbPL.getId()%>" title="<%= cmbPL.getTitle()%>">
                                                            <%=cmbPL.getDescripcion()%>
                                                        </option>
                                                    <%}%>			   
                                            </select>
                                            <select id="cmbCondTablaComp" class="estImputAdminLargo" style="width:40%" onfocus="cargarTablasEsquema('cmbCondEsquemaComp', 'cmbCondTablaComp')" onchange="cargarCamposDesdeEsquemaTabla('cmbCondEsquemaComp', 'cmbCondTablaComp', 'cmbCondRefComp')">	                                        
                                                <option value="">Seleccione Tabla</option>
                                            </select>
                                            <select id="cmbCondRefComp" style="width:40%" class="estImputAdminPequeño" onchange="cargarCampoValidacion('cmbCondEsquemaComp', 'cmbCondTablaComp', this.id, 'txtCondComp')" >                                                
                                            </select>
                                            <input class="estImputAdminLargo" id="txtCondComp"  />
                                        </td>
                                    </tr>                                    
                                    <tr><td></td><td><input id="txtValTipo" value="1" hidden></td></tr>
                                </table>
                            </div>                
                            <table width="100%" style="cursor:pointer">
                                <tr>
                                    <td width="99%" class="titulos">
                                        <input name="btn_MODIFICAR" title="btn_cP78" type="button" class="small button blue" value="CREAR" onclick="modificarCRUD('crearCondicionPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                        <input name="btn_MODIFICAR" title="btn_cP41" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarCondicionPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                        <input name="btn_MODIFICAR" title="btn_L63" type="button" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('divEditarCondicion')" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulosCentrados">
                                    <td colspan="3"> RESULTADOS </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="listGrillaValidacionesResultados" class="scroll"></table>
                                    </td>
                                </tr>                    
                            </table>
                            <div id="divEditarResultado">
                                <table width="100%">
                                    <tr>
                                        <td class="estiloImputIzq2"> CAMPO: </td>
                                        <td>
                                            <input id="txtIdResultado" hidden/>
                                            <input id="txtResultadoEsquema" value="formulario">
                                            <select id="cmbRefCampoResultado" style="width:40%" class="estImputAdminPequeño" onfocus="cargarCampos('formulario','txtTabla','cmbRefCampoResultado')" onchange="cargarCampoValidacion('txtResultadoEsquema', 'txtTabla', this.id, 'txtResultadoCampo')" >                                                
                                            </select>
                                            <input class="estImputAdminLargo" id="txtResultadoCampo"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="estiloImputIzq2"> OPERADOR: </td>
                                        <td>
                                            <select  style="width:40%"  class="estImputAdminPequeño" name="cmbOperadorResultado[]" id="cmbOperadorResultado">
                                                <option class="optionOperador" value="=" > = </option> 
                                                <option class="optionOperador" value="V" > V </option>       
                                            </select> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="estiloImputIzq2" >VALOR: </td>
                                        <td><input class="estImputAdminLargo" id="txtValorResultado"  /></td>
                                    </tr>
                                    <tr><td></td><td><input id="txtTipoResultado" value="1" hidden></td></tr>
                                    <tr>
                                        <td class="estiloImputIzq2"> CAMPO IGUALA: </td>
                                        <td>
                                            <select class="estImputAdminLargo" id="cmbResEsquemaComp" style="width:40%" onchange="validacionTipoEntrada()">
                                                <option value="">Seleccione Esquema</option>
                                                    <%
                                                    resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(899);	
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmbPL=(ComboVO)resultaux.get(k);
                                                    %>
                                                        <option value="<%= cmbPL.getId()%>" title="<%= cmbPL.getTitle()%>">
                                                            <%=cmbPL.getDescripcion()%>
                                                        </option>
                                                    <%}%>			   
                                            </select>
                                            <select id="cmbResTablaComp" class="estImputAdminLargo" style="width:40%" onfocus="cargarTablasEsquema('cmbResEsquemaComp', 'cmbResTablaComp')" onchange="cargarCamposDesdeEsquemaTabla('cmbResEsquemaComp', 'cmbResTablaComp', 'cmbResRefComp')">	                                        
                                                <option value="">Seleccione Tabla</option>
                                            </select>
                                            <select id="cmbResRefComp" style="width:40%" class="estImputAdminPequeño" onchange="cargarCampoValidacion('cmbResEsquemaComp', 'cmbResTablaComp', this.id, 'txtResComp')">                                                
                                            </select>
                                            <input class="estImputAdminLargo" id="txtResComp"  />
                                        </td>
                                    </tr>
                                </table>
                            </div>                
                            <table width="100%" style="cursor:pointer">
                                <tr>
                                    <td width="99%" class="titulos">
                                        <input name="btn_MODIFICAR" title="btn_cP78" type="button" class="small button blue" value="CREAR" onclick="modificarCRUD('crearResultadoPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                        <input name="btn_MODIFICAR" title="btn_cP41" type="button" class="small button blue" value="MODIFICAR" onclick="modificarCRUD('modificarResultadoPlantilla', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                                        <input name="btn_MODIFICAR" title="btn_L63" type="button" class="small button blue" value="LIMPIAR" onclick="limpiarDivEditarJuan('divEditarResultado')" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                
            </div>
        </td>
    </tr>
    <tr>
        <td>

        </td>
    </tr>
</table>
</td>



</table>