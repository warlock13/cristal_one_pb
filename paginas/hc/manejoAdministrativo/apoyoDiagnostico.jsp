<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1200px" align="center" border="0" cellspacing="0" cellpadding="1">
   <tr>
      <td>
         <div align="center" id="tituloForma">
            <jsp:include page="../../titulo.jsp" flush="true">
               <jsp:param name="titulo" value="PANEL DE APOYO DIAGNOSTICO" />
            </jsp:include>
         </div>
      </td>
   </tr>
   <tr>
      <td>
         <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr>
               <td>
                  <div style="overflow:auto;height:700px; width:1200px" id="divContenido">
                     <div id="divBuscar" style="display:block">
                        <table width="100%" cellpadding="0" cellspacing="0" align="center">
                           <tr class="titulosListaEspera">
                              <td width="15%">CLASE</td>
                              <td width="25%">PROCEDIMIENTO</td>
                              <td width="10%">TIPO</td>
                              <td width="10%">GRADO DE NECESIDAD</td>
                              <td width="10%">HOMOLOGADO</td> 
                              <td width="20%">ESTADO</td>
                           </tr>
                           <tr class="estiloImput">
                              <td>
                                 <select id="cmbClase" style="width:95%" >
                                    <option value="5">Laboratorios</option>
                                    <option value="6">Banco de Sangre</option>
                                    <option value="4">Diagnosticos</option>
                                 </select>
                              </td>
                              <td>
                                 <input type="text" id="txtBusIdProcedimientoBus" style="width:90%" />
                              </td>
                              <td><select size="1" id="cmbTipo" style="width:80%" >
                                    <option value=""></option>
                                    <option value="POS">POS</option>
                                    <option value="NOPOS">NOPOS</option>
                                 </select>
                              </td>
                              
             <td>
               <select id="cmbNecesidad" size="1" style="width:90%" >
                  <option value=""></option>
                  <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(113);  
                            ComboVO cmbG; 
                            for(int k=0;k<resultaux.size();k++){ 
                                  cmbG=(ComboVO)resultaux.get(k);
                     %>
                                    <option value="<%= cmbG.getId()%>" title="<%= cmbG.getTitle()%>">
                                       <%=cmbG.getDescripcion()%></option>
                                    <%}%>           
                </select> 
             </td>     
             <td>
               <select id="cmbHomologado" style="width:95%" >
                  <option value="S">HOMOLOGADO</option>
                  <option value="N">NO HOMOLOGADO</option>
               </select>
            </td>       
            <td><select size="1" id="cmbEstado" style="width:90%" >
               <option value=""></option>
                  <%     resultaux.clear();
        resultaux=(ArrayList)beanAdmin.combo.cargar(1);	
        ComboVO cmbES; 
        for(int k=0;k<resultaux.size();k++){ 
              cmbES=(ComboVO)resultaux.get(k);                   %>
                  <option value="<%= cmbES.getId()%>"><%=cmbES.getId()+" "+cmbES.getDescripcion()%>  </option>  <%}%>						
</select>	                   
</td> 
         </tr>	                          
     
          <tr class="titulosListaEspera">
             <td>EMPRESAS</td> 
             <td>PACIENTE</td>
             <td>FECHA DESDE</td>    
             <td>FECHA HASTA</td>  
             <td>PROGRAMA</td>
             <td></td>
          </tr>
          <tr class="estiloImput"> 
            <td >
               <select id="cmbEmpresa" size="1" style="width:90%"  onfocus="comboDependienteEmpresa('cmbEmpresa',91)">
                  <option value=""></option>
                            
                </select>
            
            
            </td>           
             <td>
                <input type="text" id="txtIdPaciente" onkeypress="llamarAutocomIdDescripcionConDato('txtIdPaciente',307)" style="width:80%"/> 
                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaPaciente"
                onclick="traerVentanita(this.id,'','divParaVentanita','txtIdPaciente','26')"
                src="/clinica/utilidades/imagenes/acciones/buscar.png">
                <div id="divParaVentanita"></div>
             </td> 
             <td><input type="text" style="width:85%"  id="txtFechaDesdeP"/></td>    
             <td><input type="text" style="width:90%"  id="txtFechaHastaP"/></td>  
             <td>

               <select id="cmbProgramas" size="1" style="width:95%">
                  <option value=""></option>
                  <%     resultaux.clear();
                            resultaux=(ArrayList)beanAdmin.combo.cargar(1002);  
                            ComboVO cmbPr; 
                            for(int k=0;k<resultaux.size();k++){ 
                              cmbPr=(ComboVO)resultaux.get(k);
                     %>
                                    <option value="<%= cmbPr.getId()%>" title="<%= cmbPr.getTitle()%>">
                                       <%=cmbPr.getDescripcion()%></option>
                                    <%}%>           
                </select> 

             </td>
             <td>
               <input type="button" class="small button blue" title="BTL4" value="BUSCAR"  onclick="buscarHC('listGrillaApoyoDiagnotico', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')"   />                    
               <input type="button" class="small button blue" title="BTL3" value="LIMPIAR"  onclick="limpiaAtributo('txtBusIdProcedimientoBus',0);limpiaAtributo('cmbTipoId',0);limpiaAtributo('txtIdentificacion',0);limpiaAtributo('txtIdPaciente',0)"   />                                                    
               <input type="button" class="small button blue" title="BTL3" value="REENVIAR ORDEN"  onclick="reenviarOrden()"   />                                                    
             </td>                            
         </tr>   
         
     </table>
     <table id="listGrillaApoyoDiagnotico" class="scroll"></table>  
   </div>              
 </div>

       </td>
      </tr>
     </table>

  </td>
</tr> 

</table>


<div id="divVentanitaSubirPdf"  style="display:none; z-index:1007; top:1px; left:211px;">
   <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
      </div>   
    <div  id="ventanitaHija" style="z-index:1009; position:fixed; top:200px; left:160px; width:70%">  
        <table width="1000" height="90" class="fondoTabla">
         <tr class="estiloImput" >
             <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaSubirPdf')" /></td>  
             <td>&nbsp;</td>                   
             <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaSubirPdf')" /></td>  
         </tr> 
            <tr>
               <td colspan="3">
                  <div id="tabsSubirDocumentos" style="width:98%; height:auto">
                     <ul>
                        <li> <a href="#divSubirArchivo" onclick="buscarHistoria('listLaboratoriosPdf');tabActivo='divSubirArchivo'""><center>SUBIR ARCHIVO</center></a> </li>
                        <li> <a href="#divPlantilla" onclick="buscarHC('listLaboratoriosPlantilla', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');tabActivo='divPlantilla'""><center>REGISTRAR PLANTILLA</center></a> </li>
                     </ul>
                     <div id="divSubirArchivo" style="width:99%"> 
                        <form method="post" enctype="multipart/form-data" action="" target="iframeUpload" id="formUpc" name="formUpc">   
                            <table style="background-color: #E8E8E8;">
                                <tr class="titulosListaEspera"> 
                                    <td>Escoja el archivo</td>
                                    <td>Observacion</td>
                                    <td> Enviar archivo</td>              
                                </tr>        
                                <tr class="estiloImput">          				
                                    <td align="center">
                                        <label>
                                            <input id="fileUpc" name="fileUpc" type="file"   size="99%"/>  
                                        </label>
                                    </td>
                                   
                                    <td>
                                        <textarea id="txtObservacionAdjunto"  style="width:90%"  maxlength="80"  ></textarea>
                                    </td>
                                    <td align="center"><input align="center" type="button" class="small button blue" value="S U B I R    A R C H I V O" onclick="subirArchivoOrderProgramacion()" /></td>
                                </tr>
                                <tr>
                                 <td class="titulos" colspan="3"  >
                                    <table id="listLaboratoriosPdf" ></table>
                                    
                                 </td>
                              </tr>
                            </table> 
                        </form>
                     </div>  
                     <div id="divPlantilla" style="width:99%"> 
                        <table width="100%" style="background-color: #E8E8E8;">
                           <tr>
                              <td class="titulos">
                                 <table id="listLaboratoriosPlantilla" ></table>
                                 <div id="pagerPlantilla" ></div>
                              </td>
                           </tr>
                        
                        </table>
                     </div>
                     <input align="center" style="height: 13px; width: 10px;" id="btnSubirArchivoOrdenProgramacion" type="button" value="*" onclick="comprobarYSubirArchivos('orden_programacion')"/>
                  </div>
               </td>
            </tr>
      </table>  
    </div>  
</div>

<!--<div id="divVentanitaSubirPdf"  style="display:none; z-index:1007; top:1px; left:211px;">
   <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
      </div>   
    <div  id="ventanitaHija" style="z-index:1009; position:fixed; top:200px; left:160px; width:70%">  
        <table width="1000" height="90" class="fondoTabla">
         <tr class="estiloImput" >
             <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaSubirPdf')" /></td>  
             <td>&nbsp;</td>                   
             <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaSubirPdf')" /></td>  
         </tr> 
         
            <tr>
               <td colspan="3">
                  <div id="tabsSubirDocumentos" style="width:98%; height:auto">
                     <ul>
                        <li> <a href="#divSubirArchivo" onclick="traerFormularioArchivo();tabActivo='divSubirArchivo'""><center>SUBIR ARCHIVO</center></a> </li>
                        <li> <a href="#divPlantilla" onclick="buscarHC('listLaboratoriosPlantilla', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');tabActivo='divPlantilla'""><center>REGISTRAR PLANTILLA</center></a> </li>
                     </ul>
                     <div id="divSubirArchivo" style="width:99%"> 
                     </div>  
                     <div id="divPlantilla" style="width:99%"> 
                        <table width="100%" style="background-color: #E8E8E8;">
                           <tr>
                              <td class="titulos">
                                 <table id="listLaboratoriosPlantilla" ></table>
                                 <div id="pagerPlantilla" ></div>
                              </td>
                           </tr>
                        
                        </table>
                     </div>
                  </div>

               </td>

            </tr>
                 
      </table>  
    </div>  
</div>      -->          


<div id="divVentanitaArchivosAdjuntos"  style="position:absolute; display:none; top:10px; left:1px; width:600px; height:90px; z-index:2002">
   <div class="transParencia" style="z-index:2003; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
   </div> 
   <div   style="z-index:2004; position:absolute; top:100px; left:150px; width:95%">          
   <table id="idSubVentanita" width="100%" class="fondoTabla">
   <tr class="estiloImput">
       <td colspan="2">Usuario elaboro: <label id="lblUsuarioElaboro"></label></td>
       <td colspan="2" align="right">
           <input name="btn_cerrar" type="button" value="CERRAR" onclick="cerrarVentanitaArchivosAdjuntos()" /></td>  
   
   <tr>     
   <tr class="titulos">
       <td width="5%">ID</td>  
       <td width="65%">NOMBRE ARCHIVO</td>
       <td width="30%">&nbsp;</td>                      
   <tr>           
   <tr class="estiloImput" >
   <form method="post" enctype="multipart/form-data" action="" target="iframeUpload" id="formUpcElimina" name="formUpcElimina">   
       <td><label id="lblId"></label></td>            
       <td align="CENTER" onClick="abrirPopupArchivoAdjunto('/docPDF/', 1200, 1200)"><label id="lblNombreElemento"></label><label id="lblImagen"></label>
           &nbsp;&nbsp;&nbsp;&nbsp;<a><u>Descargar</u></a>
       </td>
       <td align="CENTER">&nbsp;
           <% if(beanSession.usuario.preguntarMenu("111") ){%>         
           <input type="button" title="BE08" onclick="javascript:eliminarArchivoAdjunto()" value="ELIMINA" class="small button blue" id="btnProcedimiento">           
           <!--input type="button" title="BE09" onclick="javascript:limpiarArchivoAdjunto()"  value="LIMPIA"  class="small button blue" id="btnProcedimiento"-->                          
           <%}%>
       </td>            
       <tr> 
           </table>  
           </div>            
           </div> 

 
 
<input type="hidden" id="txt_banderaOpcionCirugiaGestion" value="OKGESTIONPROC" />         
<input type="hidden" id="txt_bandejaClase" value="gestionAyudaDx" />                   
<input type="hidden" id="txtIdBusPaciente" />                   
<input type="hidden" id="txtIdPlanEvolucion" />                   
<input type="hidden" id="txtIdDocumento" /> 
<input type="hidden" id="cmbTipoArchivoSubir" value="orden_programacion" /> 
<input type="hidden" id="txtIdExt" value=".pdf">
<input type="hidden" id="txtIdOrdenes" /> 


