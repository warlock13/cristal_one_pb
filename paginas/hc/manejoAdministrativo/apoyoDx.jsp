<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />

<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1000px" align="center" border="0" cellspacing="0" cellpadding="1">
   <tr>
      <td>
         <div align="center" id="tituloForma">
            <jsp:include page="../../titulo.jsp" flush="true">
               <jsp:param name="titulo" value="PANEL DE APOYO DIAGNOSTICO" />
            </jsp:include>
         </div>
      </td>
   </tr>
   <tr>
      <td>
         <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr>
               <td>
                  <table width="100%">
                     <tr class="titulosListaEspera">
                        <td width="10%">ID FOLIO</td>
                        <td></td>
                     </tr>
                     <tr class="estiloImputListaEspera">
                        <td><input type="text" id="txtIdFolio"></td>
                        <td align="LEFT"><input type="button" class="small button blue" value="BUSCAR"
                              onclick="buscarFolioAyudasDx()"></td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr>
               <td>
                  <div id="divTablaOrdenesApoyoDx" width="100%">
                     <table id="listaOrdenesApoyoDx" width="100%"></table>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div id="tabsSubirDocumentos" style="width:98%; height:auto" hidden>
                     <ul>
                        <li> <a href="#divSubirArchivo" onclick="buscarHistoria('listArchivosAdjuntosVarios')">
                              <center>SUBIR ARCHIVO</center>
                           </a> </li>
                     </ul>
                     <div id="divSubirArchivo" style="width:99%">
                        <form method="post" enctype="multipart/form-data" action="" target="iframeUpload" id="formUpc"
                           name="formUpc">
                           <table width="98%">
                              <tr class="titulos">
                                 <td width="20%">Paciente</td>
                                 <td width="20%">Id de Cita</td>
                                 <td width="20%">Id Admision</td>
                                 <td width="20%">Id Factura</td>
                                 <td width="20%">Id Evolucion</td>
                              </tr>
                              <tr class="estiloImput">
                                    <td>
                                       <strong><label id="lblPacienteAdjuntos" style="color: red;"></label></strong>
                                    </td>
                                    <td>
                                       <strong><label id="lblIdCitaAdjuntos" style="color: red;"></label></strong>
                                    </td>
                                    <td>
                                       <strong><label id="lblIdAdmisionAdjuntos" style="color: red;"></label></strong>
                                    </td>
                                    <td>
                                       <strong><label id="lblIdFacturaAdjuntos" style="color: red;"></label></strong>
                                    </td>
                                    <td>
                                       <strong><label id="lblIdEvolucionAdjuntos" style="color: red;"></label></strong>
                                    </td>
                              </tr>
                           </table>
                           <table width="98%" style="background-color: #E8E8E8;">
                              <tr class="titulosListaEspera">
                                 <td>Escoja el archivo</td>
                                 <td>Observacion</td>
                                 <td>Enviar archivo</td>
                              </tr>
                              <tr class="estiloImput">
                                 <td align="center">
                                    <label>
                                       <input id="fileUpc" name="fileUpc" type="file"  size="99%"/>
                                    </label>
                                    <select id="cmbIdTipoArchivosVarios" hidden>
                                       <option value="8"></option>
                                    </select>
                                 </td>

                                 <td>
                                    <textarea id="txtObservacionAdjuntosVarios" style="width:90%" maxlength="80"
                                       ></textarea>
                                 </td>
                                 <td align="center"><input align="center" type="button" id="btnSubirAdjuntos" class="small button blue"
                                       value="SUBIR ARCHIVO" onclick="subirAdjuntos()" />
                                 </td>
                              </tr>
                           </table>
                           <table width="98%">
                              <tr class="titulos">
                                 <td>
                                    <div id="divContenedorListaArchivos">
                                       <table id="listArchivosAdjuntosVarios"></table>
                                    </div>
                                 </td>
                              </tr>
                           </table>
                        </form>
                     </div>
                  </div>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>

<input type="hidden" id="txtIdExt" value=".pdf">