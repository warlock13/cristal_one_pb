<table width="100%" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td align="right" width="61%" height="7px" style="position:absolute;">
      <div id="contenedorMuelaGrande"
        style=" display:block; top: 15px; left:10px; width: 100px; height: 120px; position:relative; z-index:3;">
        <br>
        <img src="/clinica/paginas/hc/saludOral/diente/dentaduraHueco.png" id="imagen" alt="mueita" usemap="#as"
          style="position:relative;  top:-30px; left:0px; width: 100px; height: 125px; z-index:22;" />
        <map name="as" id="as">
          <area shape="poly" coords="16,17,50,2,83,16,79,21,50,10,21,21" alt="6" href="#" id="BIS"
            onclick="cambiarSeccionDienteGrande(this.id);" />
          <area shape="poly" coords="21,30,50,20,77,30,65,44,50,40,35,44" alt="1" href="#" id="V"
            onclick="cambiarSeccionDienteGrande(this.id);" />
          <area shape="poly" coords="69,50,85,36,94,63,84,91,69,77,72,63" alt="2" href="#" id="M"
            onclick="cambiarSeccionDienteGrande(this.id);" />
          <area shape="poly" coords="65,82,79,95,50,106,21,97,35,82,50,87" alt="3" href="#" id="L"
            onclick="cambiarSeccionDienteGrande(this.id);" />
          <area shape="poly" coords="31,48,17,35,5,63,16,91,32,77,26,63" alt="4" href="#" id="D"
            onclick="cambiarSeccionDienteGrande(this.id);" />
          <area shape="circle" coords="50,63,13" alt="5" href="#" id="O"
            onclick="cambiarSeccionDienteGrande(this.id);" />
          <area shape="poly" coords="16,109,50,121,83,109,80,105,50,116,22,105" alt="7" href="#" id="BII"
            onclick="cambiarSeccionDienteGrande(this.id);" />
        </map>
        <table cellpadding="0" cellspacing="0">
          <tr class="subtitulosRep">
            <td width="271">
              <input type="button" class="small button blue" onclick="modificarCRUDOdontologia('limpiarDiente')"
                value="LIMPIAR" />
            </td>
          </tr>
          <tr class="subtitulosRep">
            <td width="271">
              <div  style="overflow: auto; max-height: 200px;">
                <font color="#333399" size="+3.5"><label id="idDienteElegido">0</label></font>
              </div>
            </td>
          </tr>
          <tr class="subtitulosRep">
            <td width="265">
              <input type="button" style="left: -10px;" class="small button blue" onclick="limpiarSeleccion()"
                value="QUITAR SELECCION" />
            </td>
          </tr>
          
          <!-- 
          <tr class="estiloImput">
            <td width="271">
              cariados = <label id="lblCantCariados"></label>
            </td>
          </tr>
          <tr class="estiloImput">
            <td width="271">
              Obturados= <label id="lblCantObturados"></label>
            </td>
          </tr>
          <tr class="estiloImput">
            <td width="271">
              Ausentes = <label id="lblCantAusentes"></label>
            </td>
          </tr> -->
        </table>
        <!-- PARTE GRAFICA DE LAS 5 CARAS -->
        <!-- PARTE GRAFICA DE LAS 5 CARAS -->
        <div id="container2">
          <DIV class="grande G-V-0" id="G-V" STYLE="position:absolute; top:0px; left:0px; z-index:3; "> </DIV>
          <DIV class="grande G-M-0" id="G-M" STYLE="position:absolute; top:0px; left:0px; z-index:4; "> </DIV>
          <DIV class="grande G-L-0" id="G-L" STYLE="position:absolute; top:0px; left:0px; z-index:5; "> </DIV>
          <DIV class="grande G-D-0" id="G-D" STYLE="position:absolute; top:0px; left:0px; z-index:6; "> </DIV>
          <DIV class="grande G-O-0" id="G-O" STYLE="position:absolute; top:0px; left:0px; z-index:7; "> </DIV>
          <!-- Modificación-->
          <DIV class="grande G-BIS-0" id="G-BIS" STYLE="position:absolute; top:-11px; left:-1px; z-index:1; "> </DIV>
          <DIV class="grande G-BII-0" id="G-BII" STYLE="position:absolute; top:9px; left:0px; z-index:2; "> </DIV>
          <DIV id="G-sellante" STYLE="position:absolute; top:0px; left:0px; z-index:8; "> </DIV>
          <DIV id="G-sellanteXhacer" STYLE="position:absolute; top:0px; left:0px; z-index:19; "> </DIV>
          <DIV id="G-sellanteRealizado" STYLE="position:absolute; top:0px; left:0px; z-index:30; "> </DIV>
          <DIV id="G-protesis" STYLE="position:absolute; top:30px; left:0px; z-index:9; "> </DIV>
          <DIV id="G-protesisXhacer" STYLE="position:absolute; top:30px; left:0px; z-index:10; "> </DIV>
          <DIV id="G-protesisRemovible" STYLE="position:absolute; top:40px; left:0px; z-index:20; "> </DIV>
          <DIV id="G-protesisRemovibleX" STYLE="position:absolute; top:40px; left:0px; z-index:24; "> </DIV>
          <DIV id="G-corona" STYLE="position:absolute; top:0px; left:0px; z-index:11; "> </DIV>
          <DIV id="G-coronaXhacer" STYLE="position:absolute; top:0px; left:0px; z-index:12; "> </DIV>
          <DIV id="G-nucleo" STYLE="position:absolute; top:0px; left:0px; z-index:21; "> </DIV>
          <DIV id="G-nucleoXhacer" STYLE="position:absolute; top:0px; left:0px; z-index:22; "> </DIV>


          <DIV id="G-endodoncia" STYLE="position:absolute; top:0px; left:0px; z-index:13; "> </DIV>
          <DIV id="G-endodonciaXhacer" STYLE="position:absolute; top:0px; left:0px; z-index:14; "> </DIV>
          <DIV id="G-exodonciaIndi" STYLE="position:absolute; top:0px; left:0px; z-index:15; "> </DIV>
          <DIV id="G-exodonciaPre" STYLE="position:absolute; top:0px; left:0px; z-index:25; "> </DIV>
          <DIV id="G-extraido" STYLE="position:absolute; top:0px; left:0px; z-index:16; "> </DIV>
          <DIV id="G-extraidoXcaries" STYLE="position:absolute; top:0px; left:0px; z-index:26; "> </DIV>
          <DIV id="G-incluido" STYLE="position:absolute; top:0px; left:0px; z-index:23; "> </DIV>
          <DIV id="G-sinErupcionar" STYLE="position:absolute; top:33px; left:0px; z-index:17; "> </DIV>
          <DIV id="G-erupcion" STYLE="position:absolute; top:0px; left:0px; z-index:18; "> </DIV>
          <DIV id="G-sano" STYLE="position:absolute; top:0px; left:0px; z-index:27; "> </DIV>
        </div>
      </div>
    </td>

    <td width="39%" align="CENTER" class="estiloImput">TIPO TAREA=
      <select size="1"  id="cmbIdTipoTratamiento" style="width:30%;" onchange="cambiarSeleccionOdonto()">
        <option value="S" title="S">SEGUIMIENTO</option>
        <option value="P" title="P">PLACA</option>
      </select>
      <label id="lblIdTipoTratamiento">-</label>


      <select size="1"  id="cmb_num_trata_historico" style="width:40px;">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
      </select>


    </td>



  </tr>

  <tr>
    <td colspan="2">
      <div id="divParaOdontograma"></div>
    </td>
  </tr>
</table>