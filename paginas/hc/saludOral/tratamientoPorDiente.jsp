<table width="1124px">
  <tr class="titulos"> 
    <td width="10%">Transacci&oacute;n</td>                                 
    <td width="50%">Descripci&oacute;n del Tratamiento</td>                           
    <td width="35%" >Procedimiento Realizado(CUPS)</td>
    <td width="5%" >Cantidad</td>      
  </tr>		
  <tr class="estiloImput"> 
    <td><label id="lblIdTnsDienteTratamien"></label></td>                              
    <td>
      <textarea type="text" id="txtDesTratamientoPorDiente"  onkeypress="return validarKey(event,this.id)"  rows="3" cols="50" style="width:95%" > </textarea>         
    </td>
    <td>
      <input id="txtIdProcedimientoEjecutadoOdontograma"
            oninput="llenarElementosAutoCompletarKey(this.id, 204, this.value)"
            style="width:90%">
      <input id="txtIdProcedimientoEjecutado" style="display: none;">
      <input id="txtIdEvolucionTratamiento" style="display: none;">
    </td>                     
    <td>
      <input type="text" id="txtCantidadOdontograma" style="width:90%" onKeyPress="javascript:return teclearsoloDigitos(event);"  maxlength="3" >
    </td>     
 </tr>                           
 <tr class="estiloImput">
  <td colspan="4" align="center"><input type="button" class="small button blue" value="Adicionar" onclick="modificarCRUDOdontologia('tratamientoPorDiente');"  />
  <input type="button" class="small button blue" value="Modificar" onclick="modificarCRUDOdontologia('modificarTratamientoPorDiente');"  />
  <input type="button" class="small button blue" value="Eliminar" onclick="modificarCRUDOdontologia('eliminarTratamientoPorDiente');"  /></td> 
 </tr>                                                 
</table> 
<div id="divControl" style="width:99%; height:180px"> 
  <table  width="100%" align="center" id="idTablaTratamEjec" >                          
    <tr class="titulos">
      <td>			  
          <table  id="listTratamientoPorDiente" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>		     
      </td>
    </tr>                          
  </table> 
</div>