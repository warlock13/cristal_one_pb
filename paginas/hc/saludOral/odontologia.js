


function envioObservacionesSegunNumero(textAreaDestino, texto) {

	//   cad = document.getElementById(textAreaDestino).value + '\n' + texto
	//   cad = cad.substring(0,cad.length-1);

	asignaAtributo(textAreaDestino, document.getElementById(textAreaDestino).value + '\n' + texto, 0)
	document.getElementById(textAreaDestino).focus();


}

function cargarAcordionesOdontograma() {
	ocultar('divContenidos')
	varajaxMenu = crearAjax();
	valores_a_mandar = "";
	varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/contenidoTodoOdontograma.jsp', true);
	varajaxMenu.onreadystatechange = llenarcargarAcordionesOdontograma;
	varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);
}

function llenarcargarAcordionesOdontograma() {
	if (varajaxMenu.readyState == 4) {
		if (varajaxMenu.status == 200) {
			document.getElementById("divParaAcordionesOdontograma").innerHTML = varajaxMenu.responseText;
			// buscarHCOdontologia('listTratamientoPorDiente','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxMenu.readyState == 1) {
		swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	}
}

function cargarTratamientoPorDiente() {
	varajaxMenu = crearAjax();
	valores_a_mandar = "";
	varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/tratamientoPorDiente.jsp', true);
	varajaxMenu.onreadystatechange = llenarcargarTratamientoPorDiente;
	varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);
}

function llenarcargarTratamientoPorDiente() {
	if (varajaxMenu.readyState == 4) {
		if (varajaxMenu.status == 200) {
			document.getElementById("divParaTratamientoPorDiente").innerHTML = varajaxMenu.responseText;
			buscarHCOdontologia('listTratamientoPorDiente', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxMenu.readyState == 1) {
		swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	}
}


function cambiarSeleccionOdonto() {
	console.log('si se ejecuta cambiarSeleccionOdonto')
	if (valorAtributo('cmbIdTipoTratamiento') == 'S') {
		lista = []
		$('#drag' + ventanaActual.num).find('#4').attr('checked', 'checked') // debe quedar por defecto en odontologia la caries solo al camio de combo
		gestionTratam(4, 'caries');
		$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
		$('#drag' + ventanaActual.num).find('#idDienteElegido').append('0');
		document.getElementById('G-V').className = "grande G-V-0";
		document.getElementById('G-M').className = "grande G-M-0";
		document.getElementById('G-L').className = "grande G-L-0";
		document.getElementById('G-D').className = "grande G-D-0";
		document.getElementById('G-O').className = "grande G-O-0";
		document.getElementById('G-BIS').className = "grande G-BIS-0";
		document.getElementById('G-BII').className = "grande G-BII-0";
		document.getElementById('G-sellante').className = "";
		document.getElementById('G-sellanteXhacer').className = "";
		document.getElementById('G-sellanteRealizado').className = "";
		document.getElementById('G-protesis').className = "";
		document.getElementById('G-protesisXhacer').className = "";
		document.getElementById('G-protesisRemovible').className = "";
		document.getElementById('G-protesisRemovibleX').className = "";
		document.getElementById('G-corona').className = "";
		document.getElementById('G-coronaXhacer').className = "";
		document.getElementById('G-nucleo').className = "";
		document.getElementById('G-nucleoXhacer').className = "";
		document.getElementById('G-endodoncia').className = "";
		document.getElementById('G-endodonciaXhacer').className = "";
		document.getElementById('G-exodonciaIndi').className = "";
		document.getElementById('G-exodonciaPre').className = "";
		document.getElementById('G-extraido').className = "";
		document.getElementById('G-extraidoXcaries').className = "";
		document.getElementById('G-incluido').className = "";
		document.getElementById('G-sinErupcionar').className = "";
		document.getElementById('G-erupcion').className = "";
		document.getElementById('G-sano').className = "";

	}
	else {
		lista = []
		$('#drag' + ventanaActual.num).find('#8').attr('checked', 'checked') // debe quedar por defecto en odontologia la placa solo al camio de combo
		gestionTratam(8, 'placa')
		$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
		$('#drag' + ventanaActual.num).find('#idDienteElegido').append('0');
		document.getElementById('G-V').className = "grande G-V-0";
		document.getElementById('G-M').className = "grande G-M-0";
		document.getElementById('G-L').className = "grande G-L-0";
		document.getElementById('G-D').className = "grande G-D-0";
		document.getElementById('G-O').className = "grande G-O-0";
		document.getElementById('G-BIS').className = "";
		document.getElementById('G-BII').className = "";
		document.getElementById('G-sellante').className = "";
		document.getElementById('G-sellanteXhacer').className = "";
		document.getElementById('G-sellanteRealizado').className = "";
		document.getElementById('G-protesis').className = "";
		document.getElementById('G-protesisXhacer').className = "";
		document.getElementById('G-protesisRemovible').className = "";
		document.getElementById('G-protesisRemovibleX').className = "";
		document.getElementById('G-corona').className = "";
		document.getElementById('G-coronaXhacer').className = "";
		document.getElementById('G-nucleo').className = "";
		document.getElementById('G-nucleoXhacer').className = "";
		document.getElementById('G-endodoncia').className = "";
		document.getElementById('G-endodonciaXhacer').className = "";
		document.getElementById('G-exodonciaIndi').className = "";
		document.getElementById('G-exodonciaPre').className = "";
		document.getElementById('G-extraido').className = "";
		document.getElementById('G-extraidoXcaries').className = "";
		document.getElementById('G-incluido').className = "";
		document.getElementById('G-sinErupcionar').className = "";
		document.getElementById('G-erupcion').className = "";
		document.getElementById('G-sano').className = "";
	}
	cargarOdontograma()
}

function cargarOdontograma() { //alert('va a limpiaaaar')
	varajaxMenu = crearAjax();
	valores_a_mandar = "";
	varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/odontograma.jsp', true);
	varajaxMenu.onreadystatechange = llenarcargarOdontograma;
	varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);
}

function llenarcargarOdontograma() {
	if (varajaxMenu.readyState == 4) {
		if (varajaxMenu.status == 200) {
			document.getElementById("divParaOdontograma").innerHTML = varajaxMenu.responseText;
			asignaAtributo('lblIdTipoTratamiento', 'S', 0)


			if (valorAtributo('cmbIdTipoTratamiento') == 'S') {
				asignaAtributo('lblIdTipoTratamiento', 'S', 0)
				mostrar('G-O')
				mostrar('O')
				mostrar('G-BIS')
				mostrar('BIS')
				mostrar('G-BII')
				mostrar('BII')
			}
			else {
				asignaAtributo('lblIdTipoTratamiento', 'P', 0)
				if (valorAtributo('cmb_superficiesPorDiente') == 4) {	/*SUPERFICIES POR DIENTE: COMBO*/
					ocultar('G-O')/*oclusal no hay para placa*/
					ocultar('O')/*oclusal no hay para placa*/
				}
				ocultar('G-BIS')
				ocultar('BIS')
				ocultar('G-BII')
				ocultar('BII')
				ocultar('txt_cariados')
				ocultar('td_cariados')
				ocultar('txt_dpresentes')
				ocultar('td_dpresentes')
				ocultar('txt_dcavitacional')
				ocultar('td_dcavitacional')
				ocultar('txt_obturados')
				ocultar('td_obturados')
				ocultar('txt_perdidos')
				ocultar('td_perdidos')
				ocultar('txt_sanos')
				ocultar('td_sanos')
				ocultar('txt_perdidosXCaries')
				ocultar('td_perdidosXCaries')
				ocultar('txt_observacionCops')
				ocultar('td_observacionCops')
				ocultar('creartratamientoini')
				ocultar('eliminartratamientoini')
				ocultar('guardarcops')

			}
			if (valorAtributo('cmbIdTipoTratamiento') == 'S') {
				ocultar('idTablaPlaca')
				mostrar('idTablaTratamientos')
			}
			else {
				ocultar('idTablaTratamientos')
				mostrar('idTablaPlaca')
			}
			
			iraOdontograma()
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxMenu.readyState == 1) {
		swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	}
}


function cargarOdontogramaInicial() {
	varajaxMenu = crearAjax();
	valores_a_mandar = "";
	varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/odontogramaInicial.jsp', true);
	varajaxMenu.onreadystatechange = llenarcargarOdontogramaInicial;
	varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);
}

function llenarcargarOdontogramaInicial() {
	if (varajaxMenu.readyState == 4) {
		if (varajaxMenu.status == 200) {
			document.getElementById("divParaOdontogramaInicial").innerHTML = varajaxMenu.responseText;
			if (valorAtributo('cmbIdTipoTratamiento') == 'S') {
				ocultar('divTratamientoSeguimiento');
				asignaAtributo('lblIdTipoTratamiento', 'I', 0);
				consultarOdontogramaBD();
			}
			else {

				ocultar('divTratamientoSeguimiento');
				asignaAtributo('cmbIdTipoTratamiento', 'S', 0)
				asignaAtributo('lblIdTipoTratamiento', 'I', 0);
				consultarOdontogramaBD();
			}
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxMenu.readyState == 1) {
		swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
	}
}


function cargarControlDePlaca() {

	ocultar('divTratamientoSeguimiento')	// SOLO SE VE EL ACORDION DE PLACA OCULTANDO EL SEGUIMIENTO
	asignaAtributo('cmbIdTipoTratamiento', 'S', 0)

	varajaxMenu = crearAjax();
	valores_a_mandar = "";
	varajaxMenu.open("POST", '/clinica/paginas/hc/saludOral/controlDePlaca.jsp', true);
	varajaxMenu.onreadystatechange = llenarcargarControlDePlaca;
	varajaxMenu.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	varajaxMenu.send(valores_a_mandar);
}

function llenarcargarControlDePlaca() {


	if (valorAtributo('cmbIdTipoTratamiento') == 'S') {
		if (varajaxMenu.readyState == 4) {
			if (varajaxMenu.status == 200) {
				document.getElementById("divParaControlDePlaca").innerHTML = varajaxMenu.responseText;
				asignaAtributo('lblIdTipoTratamiento', 'P', 0)
				iraOdontograma()
			} else {
				swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
			}
		}
		if (varajaxMenu.readyState == 1) {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE')
		}
	}
	else {
		ocultar('divControlDePlaca')
		alert('PARA VER HISTORICO DE PLACA, EL TIPO DE TAREA DEBE SER SEGUIMIENTO')
	}


}

//----------------------------------------------------------------------------------------odontograma--------------------------------------------
var trat = 0;
var idDien = 0;

var diente = new Array();
var dienteIni = new Array();

function caras() {
	this.V;
	this.M;
	this.L;
	this.D;
	this.O;
	this.BIS; // BORDE INCISAL SUPERIOR
	this.BII; // BORDE INCISAL INFERIOR 
	this.sellante;
	this.sellanteXhacer;
	this.sellanteRealizado;
	this.protesis;
	this.protesisXhacer;
	this.protesisRemovible;
	this.protesisRemovibleX; //PROTESIS REMOVIBLE EN MAL ESTADO	   	   
	this.corona;
	this.coronaXhacer;
	this.nucleo;
	this.nucleoXhacer;
	this.endodoncia;
	this.endodonciaXhacer;
	this.exodonciaIndi;
	this.exodonciaPre; //EXODONCIA PRESENTE
	this.extraido;
	this.extraidoXcaries; // EXTRAIDO POR CARIES	
	this.incluido;
	//this.nucleo;  
	this.radiografia;
	this.sinErupcionar;
	this.erupcion;
	this.sano; //DIENTE SANO		   
}


function iraOdontograma() {
	
	console.log('entro a ira')
	$('#drag' + ventanaActual.num).find('#idTratamientoDient').empty(); // limpiar
	$('#drag' + ventanaActual.num).find('#desTratamientoDient').empty(); // limpiar
	//$('#drag'+ventanaActual.num).find('#idDienteElegido').empty();// limpiar al pasar del formulario al odontograma para que no desaparesca el diente
	//$('#drag'+ventanaActual.num).find('#idDienteElegido').append(''); 
	if (valorAtributo('lblIdTipoTratamiento') != 'P') {
		console.log('entro a ira if')
		guardarYtraerDatoAlListadoOdontologia2('datosOdontologiacops');
		//consultarOdontogramaBD();
	}
	else {
		guardarYtraerDatoAlListadoOdontologia('datosOdontologiaPlaca');
		//guardarYtraerDatoAlListadoOdontologia2('datosOdontologiacops');
	}
	diente[100] = new caras();
}

function guardarYtraerDatoAlListadoOdontologia(opcion) {

	varajaxInit = crearAjax();
	varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
	varajaxInit.onreadystatechange = function () { respuestaguardaListadoOdonto(opcion) };
	varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	valores_a_mandar = "accion=" + opcion;

	switch (opcion) {
		case 'datosOdontologiaPlaca':
			if (valorAtributo('lblIdDocumento') != '') {
				valores_a_mandar = valores_a_mandar + "&id_evolucion=" + valorAtributo('lblIdDocumento');
			} else {
				return;
			}
			break;
	}

	varajaxInit.send(valores_a_mandar);
}

function guardarYtraerDatoAlListadoOdontologia2(opcion) {
	varajaxInit = crearAjax();
	varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
	varajaxInit.onreadystatechange = function () { respuestaguardaListadoOdonto2(opcion) };
	varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	valores_a_mandar = "accion=" + opcion;

	switch (opcion) {
		case 'datosOdontologiacops':
			if (valorAtributo('lblIdDocumento') != '') {
				valores_a_mandar = valores_a_mandar + "&id_evolucion=" + valorAtributo('lblIdDocumento');
			} else {
				return;
			}
			break;
	}

	varajaxInit.send(valores_a_mandar);
}


function respuestaguardaListadoOdonto(opcion) {
	if (varajaxInit.readyState == 4) {
		if (varajaxInit.status == 200) {
			raiz = varajaxInit.responseXML.documentElement;
			if (raiz.getElementsByTagName('raiz') != null) {


				if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
					switch (opcion) {
						case 'datosOdontologiaPlaca':
							asignaAtributo('txt_cantDientesPlaca', raiz.getElementsByTagName('cantDientesPlaca')[0].firstChild.data, 0);
							asignaAtributo('cmb_superficiesPorDiente', raiz.getElementsByTagName('superficiesPorDiente')[0].firstChild.data, 0);
							asignaAtributo('txt_observacionPlaca', raiz.getElementsByTagName('observacionPlaca')[0].firstChild.data, 0);
							consultarOdontogramaBD();
							break;
					}
				}
				else {
					alert('PARA ESTE FOLIO NO EXISTE VALORACION DE PLACA, \nSI DESEA INICIAR, DEBE PRIMERO GUARDAR EL CONTROL DE PLACA');
					asignaAtributo('txt_cantDientesPlaca', 0, 0);
					//consultarOdontogramaBD();
				}

			}
			guardarYtraerDatoAlListadoOdontologia2('datosOdontologiacops');
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxInit.readyState == 1) {
		//abrirVentana(260, 100);
		//VentanaModal.setSombra(true);
	}
}


function validarcantdientes(ele) {
	if (parseInt(ele.value.trim()) > 35) {
		alert('SOLO SE ADMITE UN N\xdaMERO MAXIMO DE 35 DIENTES');
		iraOdontograma();
	}
	else if (parseInt(ele.value.trim()) < 0) {
		alert('EL N\xdaMERO DE DIENTES DEBE SER MAYOR A 0');
		iraOdontograma();
	}
}

function validarcantdientes2(ele) {
	num_dientes_ausentes = valorAtributo('lblCantAusentes');
	num_dientes_evaluados = 52 - num_dientes_ausentes;
	if (parseInt(ele.value.trim()) > num_dientes_evaluados) {
		alert('SOLO SE ADMITE UN N\xdaMERO MAXIMO DE ' + num_dientes_evaluados + ' DIENTES');
		iraOdontograma();
	}
	else if (parseInt(ele.value.trim()) <= 0) {
		alert('EL N\xdaMERO DE DIENTES DEBE SER MAYOR A 0');
		iraOdontograma();
	}
}

function respuestaguardaListadoOdonto2(opcion) {
	if (varajaxInit.readyState == 4) {
		if (varajaxInit.status == 200) {
			raiz = varajaxInit.responseXML.documentElement;
			if (raiz.getElementsByTagName('raiz') != null) {


				if (raiz.getElementsByTagName('diligenciado')[0].firstChild.data == 'true') {
					switch (opcion) {
						case 'datosOdontologiacops':
							asignaAtributo('txt_cariados', raiz.getElementsByTagName('cariados')[0].firstChild.data, 0);
							asignaAtributo('txt_obturados', raiz.getElementsByTagName('obturados')[0].firstChild.data, 0);
							asignaAtributo('txt_perdidos', raiz.getElementsByTagName('perdidos')[0].firstChild.data, 0);
							asignaAtributo('txt_sanos', raiz.getElementsByTagName('sanos')[0].firstChild.data, 0);
							asignaAtributo('txt_perdidosXCaries', raiz.getElementsByTagName('perdidosXCaries')[0].firstChild.data, 0);
							asignaAtributo('txt_observacionCops', raiz.getElementsByTagName('observacionCops')[0].firstChild.data, 0);
							asignaAtributo('txt_dpresentes', raiz.getElementsByTagName('presentes')[0].firstChild.data, 0);
							asignaAtributo('txt_dcavitacional', raiz.getElementsByTagName('cavitacional')[0].firstChild.data, 0);
							consultarOdontogramaBD();
							break;
					}
				}
				else {
					alert('PARA ESTE FOLIO NO EXISTE REGISTRO DE COPS\nSI DESEA, PUEDE REGISTRAR Y GUARDAR LAS CANTIDADES EN COPS');
					asignaAtributo('txt_cariados', 0, 0);
					asignaAtributo('txt_obturados', 0, 0);
					asignaAtributo('txt_perdidos', 0, 0);
					asignaAtributo('txt_sanos', 0, 0);
					asignaAtributo('txt_perdidosXCaries', 0, 0);
					asignaAtributo('txt_dpresentes', 0, 0);
					asignaAtributo('txt_dcavitacional', 0, 0);
					consultarOdontogramaBD();
				}

			}
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
}


function consultarOdontogramaBD() {

	if (valorAtributo('lblIdTipoTratamiento') == 'S') {		/*PARA PLACA SE NECESITA SABER EL ID FOLIO*/
		datos_a_mandar = "accion=consultaOdontograma";
		datos_a_mandar = datos_a_mandar + "&idPaciente=" + valorAtributo('lblIdPaciente');
		datos_a_mandar = datos_a_mandar + "&num_trata_historico=" + valorAtributo('cmb_num_trata_historico');
		datos_a_mandar = datos_a_mandar + "&id_tipo_tratamiento=" + valorAtributo('lblIdTipoTratamiento');
		//datos_a_mandar = datos_a_mandar+"&id_evolucion="+valorAtributo('lblIdDocumento');	
	}
	else if (valorAtributo('lblIdTipoTratamiento') == 'I') {
		datos_a_mandar = "accion=consultaOdontograma";
		datos_a_mandar = datos_a_mandar + "&idPaciente=" + valorAtributo('lblIdPaciente');
		datos_a_mandar = datos_a_mandar + "&num_trata_historico=" + valorAtributo('cmb_num_trata_historico');
		datos_a_mandar = datos_a_mandar + "&id_tipo_tratamiento=" + valorAtributo('lblIdTipoTratamiento');
	}
	else {
		datos_a_mandar = "accion=consultaOdontogramaPlaca";
		datos_a_mandar = datos_a_mandar + "&idPaciente=" + valorAtributo('lblIdPaciente');
		datos_a_mandar = datos_a_mandar + "&num_trata_historico=" + valorAtributo('cmb_num_trata_historico');
		datos_a_mandar = datos_a_mandar + "&id_tipo_tratamiento=" + valorAtributo('lblIdTipoTratamiento');

		if (valorAtributo('cmbIdTipoTratamiento') == 'P') /*si estan trabajando en la edicion de la placa*/
			datos_a_mandar = datos_a_mandar + "&id_evolucion=" + valorAtributo('lblIdDocumento');
		else datos_a_mandar = datos_a_mandar + "&id_evolucion=" + valorAtributo('lblIdDocumentoPlaca');
	}


	varajaxInit = crearAjax();
	varajaxInit.open("POST", '/clinica/paginas/accionesXml/buscarHc_xml.jsp', true);
	varajaxInit.onreadystatechange = respuestaOdontogramaBD;
	varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	varajaxInit.send(datos_a_mandar);
}

function respuestaOdontogramaBD() {
	if (varajaxInit.readyState == 4) {
		if (varajaxInit.status == 200) {
			raiz = varajaxInit.responseXML.documentElement;

			if (raiz.getElementsByTagName('diente') != null) {
				var idDiente = raiz.getElementsByTagName('idDiente');
				var v = raiz.getElementsByTagName('v');
				var m = raiz.getElementsByTagName('m');
				var l = raiz.getElementsByTagName('l');
				var d = raiz.getElementsByTagName('d');
				var o = raiz.getElementsByTagName('o');
				var bis = raiz.getElementsByTagName('bis');
				var bii = raiz.getElementsByTagName('bii');
				var idTratam = raiz.getElementsByTagName('idTratam');
				totalRegistros = idDiente.length;
				contCaries = 0;
				contObturad = 0;
				contAusentes = 0;
				contSanos = 0;
				contPerdidosXCaries = 0;
				contpresentes = 0;
				contcavitacional = 0;
				van = false
				contDient = 0;
				contAux1 = 0;
				totPiezasPlaca = valorAtributo('txt_cantDientesPlaca');  //32
				edadPaciente = valorAtributo('lblEdadPaciente');
				totSuperficiesPlaca = 0;
				lblCalculoPlaca = 0;

				for (j = 11; j <= 85; j++) {	  //85
					if ((j >= 11 && j <= 18) || (j >= 41 && j <= 48) || (j >= 21 && j <= 28) || (j >= 31 && j <= 38) || (j >= 51 && j <= 55) || (j >= 81 && j <= 85) || (j >= 61 && j <= 65) || (j >= 71 && j <= 75)) {
						van = false

						if (totalRegistros > 0) {
							for (i = 0; i < totalRegistros; i++) {
								if (j == idDiente[i].firstChild.data) {  /*encontro diligenciado en esta posision*/
									contDient = i;
									van = true;
									break;
								}
								else van = false;
							}
						} else van = false;

						if (van) {
							diente[j] = new caras();	 // se carga por primera vez el array																			
							diente[j].V = v[contDient].firstChild.data;
							diente[j].M = m[contDient].firstChild.data;
							diente[j].L = l[contDient].firstChild.data;
							diente[j].D = d[contDient].firstChild.data;
							diente[j].O = o[contDient].firstChild.data;
							diente[j].BIS = bis[contDient].firstChild.data;
							diente[j].BII = bii[contDient].firstChild.data;
							diente[j].sellante = '';
							diente[j].sellanteXhacer = '';
							diente[j].sellanteRealizado = '';
							diente[j].protesis = '';
							diente[j].protesisXhacer = '';
							diente[j].protesisRemovible = '';
							diente[j].protesisRemovibleX = '';
							diente[j].corona = '';
							diente[j].coronaXhacer = '';
							diente[j].nucleo = '';
							diente[j].nucleoXhacer = '';
							diente[j].endodoncia = '';
							diente[j].endodonciaXhacer = '';
							diente[j].exodonciaIndi = '';
							diente[j].exodonciaPre = '';
							diente[j].extraido = '';
							diente[j].extraidoXcaries = '';
							diente[j].incluido = '';
							diente[j].sinErupcionar = '';
							diente[j].erupcion = '';
							diente[j].sano = '';

							if (diente[j].V == 4 || diente[j].M == 4 || diente[j].L == 4 || diente[j].D == 4 || diente[j].O == 4
								|| diente[j].V == 5 || diente[j].M == 5 || diente[j].L == 5 || diente[j].D == 5 || diente[j].O == 5
								|| diente[j].V == 6 || diente[j].M == 6 || diente[j].L == 6 || diente[j].D == 6 || diente[j].O == 6
								|| diente[j].V == 7 || diente[j].M == 7 || diente[j].L == 7 || diente[j].D == 7 || diente[j].O == 7
								|| diente[j].V == 14 || diente[j].M == 14 || diente[j].L == 14 || diente[j].D == 14 || diente[j].O == 14) { //cuantos dientes tienen caries	
								contCaries = contCaries + 1;
							}
							else if (diente[j].V == 1 || diente[j].M == 1 || diente[j].L == 1 || diente[j].D == 1 || diente[j].O == 1
								|| diente[j].V == 2 || diente[j].M == 2 || diente[j].L == 2 || diente[j].D == 2 || diente[j].O == 2
								|| diente[j].V == 3 || diente[j].M == 3 || diente[j].L == 3 || diente[j].D == 3 || diente[j].O == 3
								|| diente[j].V == 13 || diente[j].M == 13 || diente[j].L == 13 || diente[j].D == 13 || diente[j].O == 13) {
								contObturad = contObturad + 1;
							}
							else if (diente[j].V == 15 || diente[j].M == 15 || diente[j].L == 15 || diente[j].D == 15 || diente[j].O == 15) {
								contcavitacional = contcavitacional + 1;
							}


							if (diente[j].V == 8)
								totSuperficiesPlaca = totSuperficiesPlaca + 1;
							if (diente[j].M == 8)
								totSuperficiesPlaca = totSuperficiesPlaca + 1;
							if (diente[j].L == 8)
								totSuperficiesPlaca = totSuperficiesPlaca + 1;
							if (diente[j].D == 8)
								totSuperficiesPlaca = totSuperficiesPlaca + 1;
							if (diente[j].O == 8)
								totSuperficiesPlaca = totSuperficiesPlaca + 1;



							if (idTratam[contDient].firstChild.data == 1) { diente[j].sellante = 'sellante'; contSanos = contSanos + 1; }
							if (idTratam[contDient].firstChild.data == 12) { diente[j].sellanteXhacer = 'sellanteXhacer'; contSanos = contSanos + 1; }
							if (idTratam[contDient].firstChild.data == 21) { diente[j].sellanteRealizado = 'sellanteRealizado'; contSanos = contSanos + 1; }
							if (idTratam[contDient].firstChild.data == 2) { diente[j].protesis = 'protesis'; contAusentes = contAusentes + 1; }
							if (idTratam[contDient].firstChild.data == 3) {
								diente[j].protesisXhacer = 'protesisXhacer';
								contAusentes = contAusentes + 1;
							}
							if (idTratam[contDient].firstChild.data == 13) {
								diente[j].protesisRemovible = 'protesisRemovible';
								contAusentes = contAusentes + 1;
							}
							if (idTratam[contDient].firstChild.data == 17) {
								diente[j].protesisRemovibleX = 'protesisRemovibleX';
								contAusentes = contAusentes + 1;
							}
							if (idTratam[contDient].firstChild.data == 4) { diente[j].corona = 'corona'; }
							if (idTratam[contDient].firstChild.data == 5) { diente[j].coronaXhacer = 'coronaXhacer'; }
							if (idTratam[contDient].firstChild.data == 15) { diente[j].nucleo = 'nucleo'; }
							if (idTratam[contDient].firstChild.data == 16) { diente[j].nucleoXhacer = 'nucleoXhacer'; }
							if (idTratam[contDient].firstChild.data == 6) { diente[j].endodoncia = 'endodoncia'; }
							if (idTratam[contDient].firstChild.data == 7) { diente[j].endodonciaXhacer = 'endodonciaXhacer'; }
							if (idTratam[contDient].firstChild.data == 8) { diente[j].exodonciaIndi = 'exodonciaIndi'; }
							if (idTratam[contDient].firstChild.data == 18) { diente[j].exodonciaPre = 'exodonciaPre'; }
							if (idTratam[contDient].firstChild.data == 9 && edadPaciente) {
								diente[j].extraido = 'extraido';
								contAusentes = contAusentes + 1;
							}
							if (idTratam[contDient].firstChild.data == 19 && edadPaciente) {
								diente[j].extraidoXcaries = 'extraidoXcaries';
								contPerdidosXCaries = contPerdidosXCaries + 1;
							}
							if (idTratam[contDient].firstChild.data == 14) { diente[j].incluido = 'incluido'; }
							if (idTratam[contDient].firstChild.data == 10) { diente[j].sinErupcionar = 'sinErupcionar'; }
							if (idTratam[contDient].firstChild.data == 11) { diente[j].erupcion = 'erupcion'; }
							if (idTratam[contDient].firstChild.data == 20) { diente[j].sano = 'sano'; contSanos = contSanos + 1; }
							contAux1 = contAusentes + contPerdidosXCaries;
							if (edadPaciente > 5) {
								contpresentes = 32 - contAux1;
							}
							if (edadPaciente < 5) {
								contpresentes = 20 - contAux1;
							}

						}
						else {
							diente[j] = new caras();	 // se carga por primera vez el array																			
							diente[j].V = 0;
							diente[j].M = 0;
							diente[j].L = 0;
							diente[j].D = 0;
							diente[j].O = 0;
							diente[j].BIS = 0;
							diente[j].BII = 0;
							diente[j].sellante = '';
							diente[j].sellanteXhacer = '';
							diente[j].sellanteRealizado = '';
							diente[j].protesis = '';
							diente[j].protesisXhacer = '';
							diente[j].protesisRemovible = '';
							diente[j].protesisRemovibleX = '';
							diente[j].corona = '';
							diente[j].coronaXhacer = '';
							diente[j].nucleo = '';
							diente[j].nucleoXhacer = '';
							diente[j].endodoncia = '';
							diente[j].endodonciaXhacer = '';
							diente[j].exodonciaIndi = '';
							diente[j].exodonciaPre = '';
							diente[j].extraido = '';
							diente[j].extraidoXcaries = '';
							diente[j].incluido = '';
							diente[j].sinErupcionar = '';
							diente[j].erupcion = '';
							diente[j].sano = '';
						} // if van
					}  // if
				}// for 85
				console.log('Esta es la edad:', edadPaciente)
				/* $('#drag' + ventanaActual.num).find('#lblCantCariados').empty();
				$('#drag' + ventanaActual.num).find('#lblCantCariados').append(contCaries);
				-------------------------------------------
				INICIO CONTADORES GUARDADO AUTOMTICO.
				--------------------------------------
				*/


				if (valorAtributo('txt_cariados') == 0) {
					asignaAtributo('txt_cariados', contCaries, 0);
				}
				/* $('#drag' + ventanaActual.num).find('#lblCantObturados').empty();
				$('#drag' + ventanaActual.num).find('#lblCantObturados').append(contObturad); */
				if (valorAtributo('txt_obturados') == 0) {
					asignaAtributo('txt_obturados', contObturad, 0);
				}
				if (valorAtributo('txt_perdidos') == 0) {
					asignaAtributo('txt_perdidos', contAusentes, 0);
				}
				if (valorAtributo('txt_sanos') == 0) {
					asignaAtributo('txt_sanos', contSanos, 0);
				}
				if (valorAtributo('txt_perdidosXCaries') == 0) {
					asignaAtributo('txt_perdidosXCaries', contPerdidosXCaries, 0);
				}
				if (valorAtributo('txt_dcavitacional') == 0) {
					asignaAtributo('txt_dcavitacional', contcavitacional, 0);
				}
				if (valorAtributo('txt_dpresentes') == 0) {
					asignaAtributo('txt_dpresentes', contpresentes, 0);
				}
				$('#drag' + ventanaActual.num).find('#lblCantAusentes').empty();
				$('#drag' + ventanaActual.num).find('#lblCantAusentes').append(contAusentes);
				if (valorAtributo('txt_cantDientesPlaca') == 0) {
					$('#drag' + ventanaActual.num).find('#lblCalculoPlaca').empty();
					$('#drag' + ventanaActual.num).find('#lblCalculoPlaca').append(('0'));
					$('#drag' + ventanaActual.num).find('#lblTotalPiezas').empty();
					$('#drag' + ventanaActual.num).find('#lblTotalPiezas').append(('0'));
				}
				else {
					piezas_evaluadas = totPiezasPlaca;
					$('#drag' + ventanaActual.num).find('#lblTotalPiezas').empty();
					$('#drag' + ventanaActual.num).find('#lblTotalPiezas').append((piezas_evaluadas));
					var numero = (totSuperficiesPlaca * 100) / ((piezas_evaluadas) * valorAtributo('cmb_superficiesPorDiente'));  // es 5 tomando sumando a OCLUSAL, por el contrario es 4
					$('#drag' + ventanaActual.num).find('#lblCalculoPlaca').empty();
					$('#drag' + ventanaActual.num).find('#lblCalculoPlaca').append((numero.toFixed(0)));
				}
				$('#drag' + ventanaActual.num).find('#lblSuperficiesPlaca').empty();
				$('#drag' + ventanaActual.num).find('#lblSuperficiesPlaca').append((totSuperficiesPlaca));
				dibujarOdontoUsuario();
			}
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}

	//seleccionarDientePequnoConArea($('#drag' + ventanaActual.num).find('#idDienteElegido').html())
	seleccionarDientePequnoConArea($('#drag' + ventanaActual.num).find(lista[lista.length - 1]).html())
	limpiarSeleccion()

}
var leftX = 8; topY = 40;


function dibujarOdontoUsuario() {		// es llamado desde cargar el odontograma en funcionesMenu.js   //fabrica los divs de cada diente con parametros con:  No diente, posX , posY */
	leftX = 0
	topY = 0
	for (d = 18; d >= 11; d--) {
		crearDientesDOM(d, leftX, topY + 21);	      //del 18 - 11 
		crearDientesDOM(30 + d, leftX, topY + 244);	  //del 48 - 41					
		if (d <= 15) {
			crearDientesDOM(40 + d, leftX, topY + 96);	  //del 55 - 51
			crearDientesDOM(70 + d, leftX, topY + 170);  //del 85 - 81					   
		}
		leftX = leftX + 39;
	}
	for (d = 21; d <= 28; d++) {
		crearDientesDOM(d, leftX, topY + 21);	      //del 21 - 28
		crearDientesDOM(10 + d, leftX, topY + 244);	  //del 31 - 38				

		if (d <= 25) {
			crearDientesDOM(40 + d, leftX, topY + 96);	  //del 61 - 65
			crearDientesDOM(50 + d, leftX, topY + 170);  //del 71 - 75			        
		}
		leftX = leftX + 39;
	}
}




function crearDientesDOM(dientConten, posX, posY) {  //alert(dientConten +'::'+ posX+'::'+posY)
	posY = posY + 7;
	contIndex = 0;

	if (valorAtributo('cmbIdTipoTratamiento') == 'S') {
		if (valorAtributo('lblIdTipoTratamiento') == 'S')
			contenedorDiv = document.getElementById('contenedorOdonto');
		else if (valorAtributo('lblIdTipoTratamiento') == 'I')
			contenedorDiv = document.getElementById('contenedorOdontoIni');
		else if (valorAtributo('lblIdTipoTratamiento') == 'P')
			contenedorDiv = document.getElementById('contenedorOdontoPlaca');
	}
	else contenedorDiv = document.getElementById('contenedorOdonto');



	cuerpoDiv = document.createElement('DIV');  // se crean los div hijos pa cada diente
	cuerpoDiv.id = 'container' + dientConten;



	Nregistro = document.createElement("DIV");   //   div V
	Nregistro.className = 'pequeno M-V-' + diente[dientConten].V;
	Nregistro.id = dientConten + '-M-V';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");    //   div M
	Nregistro.className = 'pequeno M-M-' + diente[dientConten].M;
	Nregistro.id = dientConten + '-M-M';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	//   div L
	Nregistro.className = 'pequeno M-L-' + diente[dientConten].L;
	Nregistro.id = dientConten + '-M-L';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");    //   div D
	Nregistro.className = 'pequeno M-D-' + diente[dientConten].D;
	Nregistro.id = dientConten + '-M-D';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	if (valorAtributo('lblIdTipoTratamiento') == 'P') {
		if (valorAtributo('cmb_superficiesPorDiente') == 5) {
			Nregistro = document.createElement("DIV");	//   div Oclusal en placa no se presenta jjj
			Nregistro.className = 'pequeno M-O-' + diente[dientConten].O;
			Nregistro.id = dientConten + '-M-O';
			Nregistro.style.position = 'absolute';
			Nregistro.style.top = posY + 'px';
			Nregistro.style.left = posX + 'px';
			Nregistro.style.zIndex = contIndex++;
			cuerpoDiv.appendChild(Nregistro);
			mostrar('G-O')
			mostrar('O')
		}

	}
	else {
		Nregistro = document.createElement("DIV");   //   div BIS
		Nregistro.className = 'pequeno M-BIS-' + diente[dientConten].BIS;
		Nregistro.id = dientConten + '-M-BIS';
		Nregistro.style.position = 'absolute';
		Nregistro.style.top = (posY - 7) + 'px';
		Nregistro.style.left = (posX + 2) + 'px';
		Nregistro.style.zIndex = contIndex++;
		cuerpoDiv.appendChild(Nregistro);

		Nregistro = document.createElement("DIV");	//   div Oclusal en placa no se presenta jjj
		Nregistro.className = 'pequeno M-O-' + diente[dientConten].O;
		Nregistro.id = dientConten + '-M-O';
		Nregistro.style.position = 'absolute';
		Nregistro.style.top = posY + 'px';
		Nregistro.style.left = posX + 'px';
		Nregistro.style.zIndex = contIndex++;
		cuerpoDiv.appendChild(Nregistro);

		Nregistro = document.createElement("DIV");   //   div BII
		Nregistro.className = 'pequeno M-BII-' + diente[dientConten].BII;
		Nregistro.id = dientConten + '-M-BII';
		Nregistro.style.position = 'absolute';
		Nregistro.style.top = (posY + 7) + 'px';
		Nregistro.style.left = (posX) + 'px';
		Nregistro.style.zIndex = contIndex++;
		cuerpoDiv.appendChild(Nregistro);
	}


	Nregistro = document.createElement("DIV");	//   div Sellante  1    
	if (diente[dientConten].sellante != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].sellante + 'P';
	Nregistro.id = dientConten + '-M-sellante';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	

	Nregistro = document.createElement("DIV");	//   div SellanteXhacer  12
	if (diente[dientConten].sellanteXhacer != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].sellanteXhacer + 'P';
	Nregistro.id = dientConten + '-M-sellanteXhacer';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	
	Nregistro = document.createElement("DIV");
	if (diente[dientConten].sellanteRealizado != '')
		Nregistro.className = 'pequeno3 ' + diente[dientConten].sellanteRealizado + 'P';
	Nregistro.id = dientConten + '-M-sellanteRealizado';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	Nregistro = document.createElement("DIV");
	if (diente[dientConten].protesis != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].protesis + 'P';
	Nregistro.id = dientConten + '-M-protesis';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	//   div protesisXhacer  3    mmmm
	if (diente[dientConten].protesisXhacer != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].protesisXhacer + 'P';
	Nregistro.id = dientConten + '-M-protesisXhacer';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	//   div protesisRemovible  3    mmmm
	if (diente[dientConten].protesisRemovible != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].protesisRemovible + 'P';
	Nregistro.id = dientConten + '-M-protesisRemovible';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	//   div protesisRemovibleX  mal estado 
	if (diente[dientConten].protesisRemovibleX != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].protesisRemovibleX + 'P';
	Nregistro.id = dientConten + '-M-protesisRemovibleX';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	Nregistro = document.createElement("DIV");   // div corona  4
	if (diente[dientConten].corona != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].corona + 'P';
	Nregistro.id = dientConten + '-M-corona';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = (posY + 1) + 'px';
	Nregistro.style.left = (posX + 1) + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");   // div coronaXhacer   5
	if (diente[dientConten].coronaXhacer != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].coronaXhacer + 'P';
	Nregistro.id = dientConten + '-M-coronaXhacer';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = (posY + 1) + 'px';
	Nregistro.style.left = (posX + 1) + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	Nregistro = document.createElement("DIV");   // div nucleo  4
	if (diente[dientConten].nucleo != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].nucleo + 'P';
	Nregistro.id = dientConten + '-M-nucleo';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");   // div nucleoXhacer   5
	if (diente[dientConten].nucleoXhacer != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].nucleoXhacer + 'P';
	Nregistro.id = dientConten + '-M-nucleoXhacer';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	Nregistro = document.createElement("DIV");	// div endodoncia  6
	if (diente[dientConten].endodoncia != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].endodoncia + 'P';
	Nregistro.id = dientConten + '-M-endodoncia';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);


	Nregistro = document.createElement("DIV");		// div endodonciaXhacer
	if (diente[dientConten].endodonciaXhacer != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].endodonciaXhacer + 'P';
	Nregistro.id = dientConten + '-M-endodonciaXhacer';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	// div exodonciaIndi
	if (diente[dientConten].exodonciaIndi != '')
		Nregistro.className = 'pequeno2 ' + diente[dientConten].exodonciaIndi + 'P';
	Nregistro.id = dientConten + '-M-exodonciaIndi';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	// div exodonciaPre
	if (diente[dientConten].exodonciaPre != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].exodonciaPre + 'P';
	Nregistro.id = dientConten + '-M-exodonciaPre';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	// div extraido
	if (diente[dientConten].extraido != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].extraido + 'P';
	Nregistro.id = dientConten + '-M-extraido';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	// div extraidoXcaries
	if (diente[dientConten].extraidoXcaries != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].extraidoXcaries + 'P';
	Nregistro.id = dientConten + '-M-extraidoXcaries';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	// div incluido
	if (diente[dientConten].incluido != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].incluido + 'P';
	Nregistro.id = dientConten + '-M-incluido';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");	// div sinErupcionar	
	if (diente[dientConten].sinErupcionar != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].sinErupcionar + 'P';
	Nregistro.id = dientConten + '-M-sinErupcionar';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");		// div erupcion  
	if (diente[dientConten].erupcion != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].erupcion + 'P';
	Nregistro.id = dientConten + '-M-erupcion';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;
	cuerpoDiv.appendChild(Nregistro);

	Nregistro = document.createElement("DIV");		// div sano  
	if (diente[dientConten].sano != '')
		Nregistro.className = 'pequeno ' + diente[dientConten].sano + 'P';
	Nregistro.id = dientConten + '-M-sano';
	Nregistro.style.position = 'absolute';
	Nregistro.style.top = posY + 'px';
	Nregistro.style.left = posX + 'px';
	Nregistro.style.zIndex = contIndex++;



	cuerpoDiv.appendChild(Nregistro);
	contenedorDiv.appendChild(cuerpoDiv);

	// fabricar el map de cada diente
	centromapX = posX + 17;
	centromapY = posY + 17;	 // 37 diametro /2    asi funcia:posY-18


	cuerpoMap = document.getElementById('mapAreaOdontograma');
	Nregistro = document.createElement("area");
	Nregistro.id = dientConten;
	Nregistro.shape = 'circle';
	Nregistro.coords = centromapX + ',' + centromapY + ',' + '18';
	Nregistro.href = '#';
	Nregistro.setAttribute('onclick', 'seleccionarDientePequnoConArea(' + dientConten + ')');	 // mejoraaaaaaaaaaa


	cuerpoMap.appendChild(Nregistro);

}


var lista = []

function limpiarSeleccion() {
	$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
	limpiarDienteGrande()
	lista = []

}

function limpiarDienteGrande() {
	console.log("dentro de la funcion")
	document.getElementById('G-V').className = "grande G-V-" + '0';	  // DIBUJAR DIENTE GRANDE A PARTIR DEL DIENTE PEQUEÑO ESCOIDO
	document.getElementById('G-M').className = "grande G-M-" + '0';
	document.getElementById('G-L').className = "grande G-L-" + '0';
	document.getElementById('G-D').className = "grande G-D-" + '0';
	document.getElementById('G-O').className = "grande G-O-" + '0';

	document.getElementById('G-BIS').className = "grande G-BIS-" + '0';
	document.getElementById('G-BII').className = "grande G-BII-" + '0';

	document.getElementById('G-sellante').className = "0";
	document.getElementById('G-sellanteXhacer').className = "0";
	document.getElementById('G-sellanteRealizado').className = "0";
	document.getElementById('G-protesis').className = "0";
	document.getElementById('G-protesisXhacer').className = "0";
	document.getElementById('G-protesisRemovible').className = "0";
	document.getElementById('G-protesisRemovibleX').className = "0";
	document.getElementById('G-corona').className = "0";
	document.getElementById('G-coronaXhacer').className = "0";
	document.getElementById('G-nucleo').className = "0";
	document.getElementById('G-nucleoXhacer').className = "0";
	document.getElementById('G-endodoncia').className = "0";
	document.getElementById('G-endodonciaXhacer').className = "0";
	document.getElementById('G-exodonciaIndi').className = "0";
	document.getElementById('G-exodonciaPre').className = "0";
	document.getElementById('G-extraido').className = "0";
	document.getElementById('G-extraidoXcaries').className = "0";
	document.getElementById('G-incluido').className = "0";
	document.getElementById('G-sinErupcionar').className = "0";
	document.getElementById('G-erupcion').className = "0";
	document.getElementById('G-sano').className = "0";
}

function dibujarDienteGrande(lista){

	console.log('dibujar diante grandee')
	console.log('lista dibujar', lista, '´++', lista[lista.length - 1].ele)
	limpiarDienteGrande()

	document.getElementById('G-V').className = "grande G-V-" + diente[lista[lista.length - 1].ele].V;	  // DIBUJAR DIENTE GRANDE A PARTIR DEL DIENTE PEQUEÑO ESCOIDO
		document.getElementById('G-M').className = "grande G-M-" + diente[lista[lista.length - 1].ele].M;
		document.getElementById('G-L').className = "grande G-L-" + diente[lista[lista.length - 1].ele].L;
		document.getElementById('G-D').className = "grande G-D-" + diente[lista[lista.length - 1].ele].D;
		// if(valorAtributo('cmbIdTipoTratamiento')!='P'){ // para control de placa este no se debe presentar
		document.getElementById('G-O').className = "grande G-O-" + diente[lista[lista.length - 1].ele].O;
		// }
		document.getElementById('G-BIS').className = "grande G-BIS-" + diente[lista[lista.length - 1].ele].BIS;
		document.getElementById('G-BII').className = "grande G-BII-" + diente[lista[lista.length - 1].ele].BII;

		if (diente[lista[lista.length - 1].ele].sellante != '') document.getElementById('G-sellante').className = "grande sellanteG";
		if (diente[lista[lista.length - 1].ele].sellanteXhacer != '') document.getElementById('G-sellanteXhacer').className = "grande sellanteXhacerG";
		if (diente[lista[lista.length - 1].ele].protesis != '') document.getElementById('G-protesis').className = "marcos protesisG";
		if (diente[lista[lista.length - 1].ele].protesisXhacer != '') document.getElementById('G-protesisXhacer').className = "marcos protesisXhacer";
		if (diente[lista[lista.length - 1].ele].protesisRemovible != '') document.getElementById('G-protesisRemovible').className = "marcos protesisRemovible";
		if (diente[lista[lista.length - 1].ele].protesisRemovibleX != '') document.getElementById('G-protesisRemovibleX').className = "marcos protesisRemovibleX";
		if (diente[lista[lista.length - 1].ele].corona != '') document.getElementById('G-corona').className = "grande3 coronaG";
		if (diente[lista[lista.length - 1].ele].coronaXhacer != '') document.getElementById('G-coronaXhacer').className = "grande3 coronaXhacerG";
		if (diente[lista[lista.length - 1].ele].nucleo != '') document.getElementById('G-nucleo').className = "grande nucleoG";
		if (diente[lista[lista.length - 1].ele].nucleoXhacer != '') document.getElementById('G-nucleoXhacer').className = "grande nucleoXhacerG";
		if (diente[lista[lista.length - 1].ele].endodoncia != '') document.getElementById('G-endodoncia').className = "grande endodonciaG";
		if (diente[lista[lista.length - 1].ele].endodonciaXhacer != '') document.getElementById('G-endodonciaXhacer').className = "grande endodonciaXhacer";
		if (diente[lista[lista.length - 1].ele].exodonciaIndi != '') document.getElementById('G-exodonciaIndi').className = "grande exodonciaIndiG";
		if (diente[lista[lista.length - 1].ele].exodonciaPre != '') document.getElementById('G-exodonciaPre').className = "grande exodonciaPreG";
		if (diente[lista[lista.length - 1].ele].extraido != '') {
			if (valorAtributo('cmbIdTipoTratamiento') == 'P' && valorAtributo('lblIdTipoTratamiento') == 'P') {
				alert('NO SE PUEDE SELECCIONAR ESTE DIENTE PORQUE FUE EXTRAIDO');
				$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
				$('#drag' + ventanaActual.num).find('#idDienteElegido').append('0');
				document.getElementById('G-V').className = "grande G-V-0";
				document.getElementById('G-M').className = "grande G-M-0";
				document.getElementById('G-L').className = "grande G-L-0";
				document.getElementById('G-D').className = "grande G-D-0";
				document.getElementById('G-O').className = "grande G-O-0";
				document.getElementById('G-extraido').className = "";
			}
			else {
				document.getElementById('G-extraido').className = "grande2 extraidoG";
			}
		}
		if (diente[lista[lista.length - 1].ele].extraidoXcaries != '') {
			if (valorAtributo('cmbIdTipoTratamiento') == 'P' && valorAtributo('lblIdTipoTratamiento') == 'P') {
				alert('NO SE PUEDE SELECCIONAR ESTE DIENTE PORQUE FUE EXTRAIDO POR CARIES');
				$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
				$('#drag' + ventanaActual.num).find('#idDienteElegido').append('0');
				document.getElementById('G-V').className = "grande G-V-0";
				document.getElementById('G-M').className = "grande G-M-0";
				document.getElementById('G-L').className = "grande G-L-0";
				document.getElementById('G-D').className = "grande G-D-0";
				document.getElementById('G-O').className = "grande G-O-0";
				document.getElementById('G-extraidoXcaries').className = "";
			}
			else {
				document.getElementById('G-extraidoXcaries').className = "grande extraidoXcariesG";
			}
		}
		if (diente[lista[lista.length - 1].ele].incluido != '') document.getElementById('G-incluido').className = "grande incluidoG";
		if (diente[lista[lista.length - 1].ele].sinErupcionar != '') document.getElementById('G-sinErupcionar').className = "marcos sinErupcionarG";
		if (diente[lista[lista.length - 1].ele].erupcion != '') document.getElementById('G-erupcion').className = "grande erupcionG";
		if (diente[lista[lista.length - 1].ele].sano != '') document.getElementById('G-sano').className = "grande dsanoG";

}

function seleccionarDientePequnoConArea(ele) {
	

	try {
		objeto_diente = {
			ele: ele,
			v: diente[ele].V,
			m: diente[ele].M,
			l: diente[ele].L,
			d: diente[ele].D,
			o: diente[ele].O,
			bis: diente[ele].BIS,
			bii: diente[ele].BII,
		}	
		
	} catch (error) {
		//----
	}

	var id_diente_repetido = lista.find(function (objeto) {
		return objeto.ele === ele;
	});

	if (id_diente_repetido) {
		Swal.fire({
			text: 'El diente ya fue seleccionado, quiere eliminarlo de la seleccion? ',
			icon: 'info',
			showCancelButton: true,
			showConfirmButton: true,
			confirmButtonColor: '#3085D6',
			cancelButtonColor: '#999999',
			confirmButtonText: "SI",
			cancelButtonText: "NO"
		}).then((result) => {
			if (result.isConfirmed) {
				var indice = lista.findIndex(function (objeto) {
					return objeto.ele === ele;
				});
				lista.splice(indice, 1)
				$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
				lista.forEach(function (elemento) {
					$('#drag' + ventanaActual.num).find('#idDienteElegido').append(elemento.ele);
					$('#drag' + ventanaActual.num).find('#idDienteElegido').append('<br>');
				});
			}
		})
	} else {
		try {
			lista.push(objeto_diente);
		} catch (error) {
			
		}
		

		$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();   // ele = diente pequeño elegido en el odontograma pa trabajarlo
		try {
			lista.forEach(function (elemento) {
				$('#drag' + ventanaActual.num).find('#idDienteElegido').append(elemento.ele);
				$('#drag' + ventanaActual.num).find('#idDienteElegido').append('<br>');
			});
		} catch (error) {
			console.log('no se puede agregar:  ', error)
		}
	}


	diente[100].V = '0';	   //limpia el array de diente auxiliar
	diente[100].M = '0';
	diente[100].L = '0';
	diente[100].D = '0';
	diente[100].O = '0';
	diente[100].BIS = '0';
	diente[100].BII = '0';
	diente[100].sellante = '';
	diente[100].sellanteXhacer = '';
	diente[100].sellanteRealizado = '';
	diente[100].protesis = '';
	diente[100].protesisXhacer = '';
	diente[100].protesisRemovible = '';
	diente[100].protesisRemovibleX = '';
	diente[100].corona = '';
	diente[100].coronaXhacer = '';
	diente[100].nucleo = '';
	diente[100].nucleoXhacer = '';
	diente[100].endodoncia = '';
	diente[100].endodonciaXhacer = '';
	diente[100].exodonciaIndi = '';
	diente[100].exodonciaPre = '';
	diente[100].extraido = '';
	diente[100].extraidoXcaries = '';
	diente[100].incluido = '';
	diente[100].sinErupcionar = '';
	diente[100].erupcion = '';
	diente[100].sano = '';

	try {
		diente[100].V = diente[lista[lista.length - 1].ele].V;	 //LLENA EN EL VECTOR AUXILIAR
		diente[100].M = diente[lista[lista.length - 1].ele].M;
		diente[100].L = diente[lista[lista.length - 1].ele].L;
		diente[100].D = diente[lista[lista.length - 1].ele].D;
		diente[100].O = diente[lista[lista.length - 1].ele].O;
		diente[100].BIS = diente[lista[lista.length - 1].ele].BIS;
		diente[100].BII = diente[lista[lista.length - 1].ele].BII;
		diente[100].sellante = diente[lista[lista.length - 1].ele].sellante;
		diente[100].sellanteXhacer = diente[lista[lista.length - 1].ele].sellanteXhacer;
		diente[100].sellanteRealizado = diente[lista[lista.length - 1].ele].sellanteRealizado;
		diente[100].protesis = diente[lista[lista.length - 1].ele].protesis;
		diente[100].protesisXhacer = diente[lista[lista.length - 1].ele].protesisXhacer;
		diente[100].protesisRemovible = diente[lista[lista.length - 1].ele].protesisRemovible;
		diente[100].protesisRemovibleX = diente[lista[lista.length - 1].ele].protesisRemovibleX;
		diente[100].corona = diente[lista[lista.length - 1].ele].corona;
		diente[100].coronaXhacer = diente[lista[lista.length - 1].ele].coronaXhacer;
		diente[100].nucleo = diente[lista[lista.length - 1].ele].nucleo;
		diente[100].nucleoXhacer = diente[lista[lista.length - 1].ele].nucleoXhacer;
		diente[100].endodoncia = diente[lista[lista.length - 1].ele].endodoncia;
		diente[100].endodonciaXhacer = diente[lista[lista.length - 1].ele].endodonciaXhacer
		diente[100].exodonciaIndi = diente[lista[lista.length - 1].ele].exodonciaIndi;
		diente[100].exodonciaPre = diente[lista[lista.length - 1].ele].exodonciaPre;
		diente[100].extraido = diente[lista[lista.length - 1].ele].extraido;
		diente[100].extraidoXcaries = diente[lista[lista.length - 1].ele].extraidoXcaries;
		diente[100].incluido = diente[lista[lista.length - 1].ele].incluido;
		diente[100].sinErupcionar = diente[lista[lista.length - 1].ele].sinErupcionar;
		diente[100].erupcion = diente[lista[lista.length - 1].ele].erupcion;
		diente[100].sano = diente[lista[lista.length - 1].ele].sano;
	} catch (error) {
		//console.log(error)
	}

	console.log("fuera de la funcion")
	limpiarDienteGrande()


	try {
		document.getElementById('G-V').className = "grande G-V-" + diente[lista[lista.length - 1].ele].V;	  // DIBUJAR DIENTE GRANDE A PARTIR DEL DIENTE PEQUEÑO ESCOIDO
		document.getElementById('G-M').className = "grande G-M-" + diente[lista[lista.length - 1].ele].M;
		document.getElementById('G-L').className = "grande G-L-" + diente[lista[lista.length - 1].ele].L;
		document.getElementById('G-D').className = "grande G-D-" + diente[lista[lista.length - 1].ele].D;
		// if(valorAtributo('cmbIdTipoTratamiento')!='P'){ // para control de placa este no se debe presentar
		document.getElementById('G-O').className = "grande G-O-" + diente[lista[lista.length - 1].ele].O;
		// }
		document.getElementById('G-BIS').className = "grande G-BIS-" + diente[lista[lista.length - 1].ele].BIS;
		document.getElementById('G-BII').className = "grande G-BII-" + diente[lista[lista.length - 1].ele].BII;

		if (diente[lista[lista.length - 1].ele].sellante != '') document.getElementById('G-sellante').className = "grande sellanteG";
		if (diente[lista[lista.length - 1].ele].sellanteXhacer != '') document.getElementById('G-sellanteXhacer').className = "grande sellanteXhacerG";
		if (diente[lista[lista.length - 1].ele].sellanteRealizado != '') document.getElementById('G-sellanteRealizado').className = "grande4 sellanteRealizadoG";
		if (diente[lista[lista.length - 1].ele].protesis != '') document.getElementById('G-protesis').className = "marcos protesisG";
		if (diente[lista[lista.length - 1].ele].protesisXhacer != '') document.getElementById('G-protesisXhacer').className = "marcos protesisXhacer";
		if (diente[lista[lista.length - 1].ele].protesisRemovible != '') document.getElementById('G-protesisRemovible').className = "marcos protesisRemovible";
		if (diente[lista[lista.length - 1].ele].protesisRemovibleX != '') document.getElementById('G-protesisRemovibleX').className = "marcos protesisRemovibleX";
		if (diente[lista[lista.length - 1].ele].corona != '') document.getElementById('G-corona').className = "grande3 coronaG";
		if (diente[lista[lista.length - 1].ele].coronaXhacer != '') document.getElementById('G-coronaXhacer').className = "grande3 coronaXhacerG";
		if (diente[lista[lista.length - 1].ele].nucleo != '') document.getElementById('G-nucleo').className = "grande nucleoG";
		if (diente[lista[lista.length - 1].ele].nucleoXhacer != '') document.getElementById('G-nucleoXhacer').className = "grande nucleoXhacerG";
		if (diente[lista[lista.length - 1].ele].endodoncia != '') document.getElementById('G-endodoncia').className = "grande endodonciaG";
		if (diente[lista[lista.length - 1].ele].endodonciaXhacer != '') document.getElementById('G-endodonciaXhacer').className = "grande endodonciaXhacer";
		if (diente[lista[lista.length - 1].ele].exodonciaIndi != '') document.getElementById('G-exodonciaIndi').className = "grande exodonciaIndiG";
		if (diente[lista[lista.length - 1].ele].exodonciaPre != '') document.getElementById('G-exodonciaPre').className = "grande exodonciaPreG";
		if (diente[lista[lista.length - 1].ele].extraido != '') {
			if (valorAtributo('cmbIdTipoTratamiento') == 'P' && valorAtributo('lblIdTipoTratamiento') == 'P') {
				alert('NO SE PUEDE SELECCIONAR ESTE DIENTE PORQUE FUE EXTRAIDO');
				$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
				$('#drag' + ventanaActual.num).find('#idDienteElegido').append('0');
				document.getElementById('G-V').className = "grande G-V-0";
				document.getElementById('G-M').className = "grande G-M-0";
				document.getElementById('G-L').className = "grande G-L-0";
				document.getElementById('G-D').className = "grande G-D-0";
				document.getElementById('G-O').className = "grande G-O-0";
				document.getElementById('G-extraido').className = "";
			}
			else {
				document.getElementById('G-extraido').className = "grande2 extraidoG";
			}
		}
		if (diente[lista[lista.length - 1].ele].extraidoXcaries != '') {
			if (valorAtributo('cmbIdTipoTratamiento') == 'P' && valorAtributo('lblIdTipoTratamiento') == 'P') {
				alert('NO SE PUEDE SELECCIONAR ESTE DIENTE PORQUE FUE EXTRAIDO POR CARIES');
				$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
				$('#drag' + ventanaActual.num).find('#idDienteElegido').append('0');
				document.getElementById('G-V').className = "grande G-V-0";
				document.getElementById('G-M').className = "grande G-M-0";
				document.getElementById('G-L').className = "grande G-L-0";
				document.getElementById('G-D').className = "grande G-D-0";
				document.getElementById('G-O').className = "grande G-O-0";
				document.getElementById('G-extraidoXcaries').className = "";
			}
			else {
				document.getElementById('G-extraidoXcaries').className = "grande extraidoXcariesG";
			}
		}
		if (diente[lista[lista.length - 1].ele].incluido != '') document.getElementById('G-incluido').className = "grande incluidoG";
		if (diente[lista[lista.length - 1].ele].sinErupcionar != '') document.getElementById('G-sinErupcionar').className = "marcos sinErupcionarG";
		if (diente[lista[lista.length - 1].ele].erupcion != '') document.getElementById('G-erupcion').className = "grande erupcionG";
		if (diente[lista[lista.length - 1].ele].sano != '') document.getElementById('G-sano').className = "grande dsanoG";

	} catch (error) {
		//console.log(error)
	}
}





function cambiarSeccionDienteGrande(cara) {   //alert(cara); //arg=historiaOdontologia
	console.log('cara', cara)
	if ($('#drag' + ventanaActual.num).find('#idDienteElegido').html() == "0") {
		alert('POR FAVOR SELECCIONE UN DIENTE DEL ODONTOGRAMA');
	}
	else {
		if (valorAtributo('cmb_superficiesPorDiente') == 4 && cara == 'O' && valorAtributo('lblIdTipoTratamiento') == 'P') {
			alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PORQUE LAS SUPERFICIES POR DIENTE ES DE 4, NO INCLUYE OCLUSAL')
		}
		else {
			if (trat != 0) {
				if ($('#drag' + ventanaActual.num).find('#idDienteElegido').html() != "") {
					if (trat == '1' || trat == '2' || trat == '3' || trat == '4' || trat == '5' || trat == '6' || trat == '7' || trat == '8'
						|| trat == '9' || trat == '13' || trat == '14' || trat == '15') {


						if (cara == 'V' || cara == 'M' || cara == 'L' || cara == 'D' || cara == 'O') {
							document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + trat;
						}
						switch (cara) {
							case 'V':
								diente[100].V = trat;
								break;
							case 'M':
								diente[100].M = trat;
								break;
							case 'L':
								diente[100].L = trat;
								break;
							case 'D':
								diente[100].D = trat;
								break;
							case 'O':
								diente[100].O = trat;
								break;
						}
					}
					else if (trat == '10' || trat == "11" || trat == "12") {

						if (cara == 'BII' || cara == 'BIS') {
							document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + trat;
						}

						//document.getElementById('G-M').className = "grande G-M-10";
						switch (cara) {
							case 'BIS':
								diente[100].BIS = trat;
								break;
							case 'BII':
								diente[100].BII = trat;
								break;
						}
					}
					else {
						switch (trat) {
							case 'sellante':
								if (diente[100].sellante != trat) {
									diente[100].sellante = trat;
									document.getElementById('G-sellante').className = "grande sellanteG";
								}
								else {
									diente[100].sellante = "";
									document.getElementById('G-sellante').className = "";
								}
								break;
							case 'sellanteXhacer':
								if (diente[100].sellanteXhacer != trat) {
									diente[100].sellanteXhacer = trat;
									document.getElementById('G-sellanteXhacer').className = "grande sellanteXhacerG";
								}
								else {
									diente[100].sellanteXhacer = "";
									document.getElementById('G-sellanteXhacer').className = "";
								}
								break;

								case 'sellanteRealizado':
									if (diente[100].sellanteRealizado != trat) {
										diente[100].sellanteRealizado = trat;
										document.getElementById('G-sellanteRealizado').className = "grande4 sellanteRealizadoG";
									}
									else {
										diente[100].sellanteRealizado = "";
										document.getElementById('G-sellanteRealizado').className = "";
									}
									break;	

							case 'protesis':
								if (diente[100].protesis != trat) {
									diente[100].protesis = trat;
									document.getElementById('G-protesis').className = "marcos protesisG";
								}
								else {
									diente[100].protesis = "";
									document.getElementById('G-protesis').className = "";
								}
								break;
							case 'protesisXhacer':
								if (diente[100].protesisXhacer != trat) {
									diente[100].protesisXhacer = trat;
									document.getElementById('G-protesisXhacer').className = "marcos protesisXhacer";
								}
								else {
									diente[100].protesisXhacer = "";
									document.getElementById('G-protesisXhacer').className = "";
								}
								break;
							case 'protesisRemovible':
								if (diente[100].protesisRemovible != trat) {
									diente[100].protesisRemovible = trat;
									document.getElementById('G-protesisRemovible').className = "marcos protesisRemovible";
								}
								else {
									diente[100].protesisRemovible = "";
									document.getElementById('G-protesisRemovible').className = "";
								}
								break;
							case 'protesisRemovibleX':
								if (diente[100].protesisRemovibleX != trat) {
									diente[100].protesisRemovibleX = trat;
									document.getElementById('G-protesisRemovibleX').className = "marcos protesisRemovibleX";
								}
								else {
									diente[100].protesisRemovibleX = "";
									document.getElementById('G-protesisRemovibleX').className = "";
								}
								break;

							case 'corona':
								if (diente[100].corona != trat) {
									diente[100].corona = trat;
									document.getElementById('G-corona').className = "grande3 coronaG";
								}
								else {
									diente[100].corona = "";
									document.getElementById('G-corona').className = "";
								}
								break;
							case 'coronaXhacer':
								if (diente[100].coronaXhacer != trat) {
									diente[100].coronaXhacer = trat;
									document.getElementById('G-coronaXhacer').className = "grande3 coronaXhacerG";
								}
								else {
									diente[100].coronaXhacer = "";
									document.getElementById('G-coronaXhacer').className = "";
								}
								break;
							case 'nucleo':
								if (diente[100].nucleo != trat) {
									diente[100].nucleo = trat;
									document.getElementById('G-nucleo').className = "grande nucleoG";
								}
								else {
									diente[100].nucleo = "";
									document.getElementById('G-nucleo').className = "";
								}
								break;
							case 'nucleoXhacer':
								if (diente[100].nucleoXhacer != trat) {
									diente[100].nucleoXhacer = trat;
									document.getElementById('G-nucleoXhacer').className = "grande nucleoXhacerG";
								}
								else {
									diente[100].nucleoXhacer = "";
									document.getElementById('G-nucleoXhacer').className = "";
								}
								break;

							case 'endodoncia':
								if (diente[100].endodoncia != trat) {
									diente[100].endodoncia = trat;
									document.getElementById('G-endodoncia').className = "grande endodonciaG";
								}
								else {
									diente[100].endodoncia = "";
									document.getElementById('G-endodoncia').className = "";
								}
								break;
							case 'endodonciaXhacer':
								if (diente[100].endodonciaXhacer != trat) {
									diente[100].endodonciaXhacer = trat;
									document.getElementById('G-endodonciaXhacer').className = "grande endodonciaXhacer";
								}
								else {
									diente[100].endodonciaXhacer = "";
									document.getElementById('G-endodonciaXhacer').className = "";
								}
								break;
							case 'exodonciaIndi':
								if (diente[100].exodonciaIndi != trat) {
									diente[100].exodonciaIndi = trat;
									document.getElementById('G-exodonciaIndi').className = "grande exodonciaIndiG";
								}
								else {
									diente[100].exodonciaIndi = "";
									document.getElementById('G-exodonciaIndi').className = "";
								}
								break;
							case 'exodonciaPre':
								if (diente[100].exodonciaPre != trat) {
									diente[100].exodonciaPre = trat;
									document.getElementById('G-exodonciaPre').className = "grande exodonciaPreG";
								}
								else {
									diente[100].exodonciaPre = "";
									document.getElementById('G-exodonciaPre').className = "";
								}
								break;
							case 'extraido':
								if (diente[100].extraido != trat) {
									diente[100].extraido = trat;
									document.getElementById('G-extraido').className = "grande2 extraidoG";
								}
								else {
									diente[100].extraido = "";
									document.getElementById('G-extraido').className = "";
								}
								break;
							case 'extraidoXcaries':
								if (diente[100].extraidoXcaries != trat) {
									diente[100].extraidoXcaries = trat;
									document.getElementById('G-extraidoXcaries').className = "grande extraidoXcariesG";
								}
								else {
									diente[100].extraidoXcaries = "";
									document.getElementById('G-extraidoXcaries').className = "";
								}
								break;
							case 'incluido':
								if (diente[100].incluido != trat) {
									diente[100].incluido = trat;
									document.getElementById('G-incluido').className = "grande incluidoG";
								}
								else {
									diente[100].incluido = "";
									document.getElementById('G-incluido').className = "";
								}
								break;

							case 'sinErupcionar':
								if (diente[100].sinErupcionar != trat) {
									diente[100].sinErupcionar = trat;
									document.getElementById('G-sinErupcionar').className = "marcos sinErupcionarG";
								}
								else {
									diente[100].sinErupcionar = "";
									document.getElementById('G-sinErupcionar').className = "";
								}
								break;
							case 'erupcion':
								if (diente[100].erupcion != trat) {
									diente[100].erupcion = trat;
									document.getElementById('G-erupcion').className = "grande erupcionG";
								}
								else {
									diente[100].erupcion = "";
									document.getElementById('G-erupcion').className = "";
								}
								break;
							case 'sano':
								if (diente[100].sano != trat) {
									diente[100].sano = trat;
									document.getElementById('G-sano').className = "grande sano";
								}
								else {
									diente[100].sano = "";
									document.getElementById('G-sano').className = "";
								}
								break;

						}
					}

					if (trat == 'limpiaProcedCara') {
						idDienteLimpiar = $('#drag' + ventanaActual.num).find('#idDienteElegido').html();

						switch (cara) {
							case 'V':
								diente[100].V = diente[idDienteLimpiar].V;
								document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + diente[idDienteLimpiar].V;
								break;
							case 'M':
								diente[100].M = diente[idDienteLimpiar].M;
								document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + diente[idDienteLimpiar].M;
								break;
							case 'L':
								diente[100].L = diente[idDienteLimpiar].L;
								document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + diente[idDienteLimpiar].L;
								break;
							case 'D':
								diente[100].D = diente[idDienteLimpiar].D;
								document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + diente[idDienteLimpiar].D;
								break;
							case 'O':
								diente[100].O = diente[idDienteLimpiar].O;
								document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + diente[idDienteLimpiar].O;

								break;
							case 'BIS':
								diente[100].BIS = diente[idDienteLimpiar].BIS;
								document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + diente[idDienteLimpiar].BIS;

								break;
							case 'BII':
								diente[100].O = diente[idDienteLimpiar].BII;
								document.getElementById('G-' + cara).className = "grande G-" + cara + "-" + diente[idDienteLimpiar].BII;

								break;
						}
					}
					if (trat == 'limpiaTodoProced') {
						seleccionarDientePequnoConArea($('#drag' + ventanaActual.num).find('#idDienteElegido').html());
					}
					guardarDienteYTratamiento(cara)
					
				}
				else alert("Seleccione el diente a Tratar");

			}
			else alert("Seleccione un tratamiento");

			console.log('trat antes guiardar', trat)
			//guardarDienteYTratamiento(cara)
			console.log('trat despues de guardar', trat)

	
		}
		if (trat == '1' || trat == '2' || trat == '3' || trat == '4' || trat == '5' || trat == '6' || trat == '7' || trat == '8' || trat == '9'
			|| trat == '13' || trat == '14' || trat == '15') {
			switch (cara) {
				case 'BIS':
					alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PARA ESTA CONVENCI\xd3N');
					break;
				case 'BII':
					alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PARA ESTA CONVENCI\xd3N');
					break;
			}
		}
		console.log('trat despues de if', trat)
		if (trat == '10' || trat == '11' || trat == '12') {
			console.log('mensaje trat')
			switch (cara) {
				case 'V':
					alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PARA ESTA CONVENCI\xd3N');
					break;
				case 'M':
					alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PARA ESTA CONVENCI\xd3N');
					break;
				case 'L':
					alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PARA ESTA CONVENCI\xd3N');
					break;
				case 'D':
					alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PARA ESTA CONVENCI\xd3N');
					break;
				case 'O':
					alert('NO SE PUEDE SELECCIONAR ESTA SUPERFICIE PARA ESTA CONVENCI\xd3N');
					break;
			}
		}

	}
}





function gestionTratam(elemn, nom_tratamiento) {// alert('987987 elemn='+elemn+'::'+ 'nom_tratamiento='+nom_tratamiento)
	trat = elemn;
	console.log('gestionTratam elem', elemn)
	console.log('gestionTratam nom_tratamiento', nom_tratamiento)
	$('#drag' + ventanaActual.num).find('#idTratamientoDient').empty();

	if (trat == '4') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('01'); //caries
	if (trat == '1') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('02'); //Obturacion en amalgama buen estado 
	if (trat == '5') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('03'); //Obturacion en amalgama en MAL estado	
	if (trat == '2') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('04'); //Obturacion en resina buen estado
	if (trat == '6') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('05'); //Obturacion en resina MAL estado
	if (trat == '3') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('06'); //Obturacion temporal buen estado
	if (trat == '7') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('07'); //Obturacion temporal MAL estado	
	if (trat == '8') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('08'); //placa
	if (trat == '9') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('09'); //fractura
	if (trat == '10') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('010'); //abrasión
	if (trat == '11') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('011'); //recesión buen estado
	if (trat == '12') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('012'); //recesión mal estado
	if (trat == '13') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('013'); //Obturacion en tratamiento buen estado
	if (trat == '14') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('014'); //Obturacion tratamiento buen estado
	if (trat == '15') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('015'); //caries cavitacional
	if (trat == 'sellante') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('1');
	if (trat == 'sellanteXhacer') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('12');
	if (trat == 'sellanteRealizado') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('21');
	if (trat == 'protesis') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('2');
	if (trat == 'protesisXhacer') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('3');
	if (trat == 'protesisRemovible') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('13');
	if (trat == 'protesisRemovibleX') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('17');
	if (trat == 'corona') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('4');
	if (trat == 'coronaXhacer') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('5');
	if (trat == 'nucleo') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('15');
	if (trat == 'nucleoXhacer') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('16');
	if (trat == 'endodoncia') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('6');
	if (trat == 'endodonciaXhacer') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('7');
	if (trat == 'exodonciaIndi') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('8');
	if (trat == 'exodonciaPre') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('18');
	if (trat == 'extraido') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('9');
	if (trat == 'extraidoXcaries') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('19');
	if (trat == 'incluido') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('14');
	if (trat == 'sinErupcionar') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('10');
	if (trat == 'erupcion') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('11');
	if (trat == 'sano') $('#drag' + ventanaActual.num).find('#idTratamientoDient').append('20');

	$('#drag' + ventanaActual.num).find('#desTratamientoDient').empty();
	$('#drag' + ventanaActual.num).find('#desTratamientoDient').append(nom_tratamiento);
}



function guardarDienteYTratamiento(cara) {

	modificarCRUDOdontologia('guardarDienteYTratamiento', 'undefined', cara);
	setTimeout(function() {
		dibujarDienteGrande(lista);
	  }, 2000);

}

function modificarCRUDOdontologia(arg, pag, cara) {

	if (pag == 'undefined')
		pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

	pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
	paginaActual = arg;

	if (valorAtributo('txtIdEsdadoDocumento') == 1 || valorAtributo('txtIdEsdadoDocumento') == 6 || valorAtributo('txtIdEsdadoDocumento') == 7 || valorAtributo('txtIdEsdadoDocumento') == 8 || valorAtributo('txtIdEsdadoDocumento') == 9 || valorAtributo('txtIdEsdadoDocumento') == 10) {
		alert('EL ESTADO FINALIZADO DEL FOLIO, NO PERMITE MODIFICAR');
		arg = '';
	}


	switch (arg) {

		case 'guardarPlaca':
			if (valorAtributo('txt_cantDientesPlaca') != '') {
				if (valorAtributo('txt_cantDientesPlaca') > 0) {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&idQuery=850&parametros=";
					add_valores_a_mandar(valorAtributo('lblIdDocumento'));
					add_valores_a_mandar(valorAtributo('lblIdDocumento'));

					add_valores_a_mandar(valorAtributo('txt_cantDientesPlaca'));
					add_valores_a_mandar(valorAtributo('cmb_superficiesPorDiente'));
					add_valores_a_mandar(valorAtributo('txt_observacionPlaca'));
					ajaxModificar();
				}
				else {
					alert('LA CANTIDAD DE DIENTES DEBE SER MAYOR A 0');
				}
			} else alert('FALTAN CANTIDAD DE DIENTES O SUPERFICIE DE DIENTES POR DILIGENCIAR')
			break;

		case 'eliminarTratamientoPorDiente':
			if (valorAtributo("txtIdProcedimientoEjecutado") != '') {
				if (verificarCamposGuardar(arg)) {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&idQuery=2749&parametros=";
					add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
					add_valores_a_mandar(valorAtributo('txtIdProcedimientoEjecutado'));
					ajaxModificar();
				}
			}
			else {
				if (verificarCamposGuardar(arg)) {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&idQuery=702&parametros=";
					add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
					ajaxModificar();
				}
			}
			break;
		case 'modificarTratamientoPorDiente':
			if (valorAtributo("txtIdProcedimientoEjecutado") != '') {
				if (verificarCamposGuardar(arg)) {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&idQuery=2748&parametros=";
					add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
					add_valores_a_mandar(IdSesion());
					add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
					add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdProcedimientoEjecutadoOdontograma'));
					add_valores_a_mandar(valorAtributo('txtCantidadOdontograma'));
					add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
					add_valores_a_mandar(valorAtributo('txtIdProcedimientoEjecutado'));
					ajaxModificar();
				}
			}
			else {
				if (valorAtributo("txtIdProcedimientoEjecutadoOdontograma") != '') {
					if (verificarCamposGuardar(arg)) {
						valores_a_mandar = 'accion=' + arg;
						valores_a_mandar = valores_a_mandar + "&idQuery=2750&parametros=";
						add_valores_a_mandar(valorAtributo("lblIdAdmision"));
						add_valores_a_mandar(valorAtributo("lblIdDocumento"));
						add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdProcedimientoEjecutadoOdontograma"));
						add_valores_a_mandar(valorAtributo("txtCantidadOdontograma"));
						add_valores_a_mandar(valorAtributo("txtDesTratamientoPorDiente"));
						add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
						add_valores_a_mandar(IdSesion());
						add_valores_a_mandar(valorAtributo("lblIdDocumento"));
						add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
						ajaxModificar();
					}
				}
				else {
					if (verificarCamposGuardar(arg)) {
						valores_a_mandar = 'accion=' + arg;
						valores_a_mandar = valores_a_mandar + "&idQuery=847&parametros=";
						add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
						add_valores_a_mandar(IdSesion());
						add_valores_a_mandar(valorAtributo('lblIdTnsDienteTratamien'));
						ajaxModificar();
					}
				}
			}
			break;
		case 'tratamientoPorDiente':
			if (valorAtributo('txtIdProcedimientoEjecutadoOdontograma') != '') {
				if (verificarCamposGuardar(arg)) {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&idQuery=2745&parametros=";
					add_valores_a_mandar(valorAtributo("lblIdAdmision"));
					add_valores_a_mandar(valorAtributo("lblIdDocumento"));
					add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdProcedimientoEjecutadoOdontograma"));
					add_valores_a_mandar(valorAtributo("txtCantidadOdontograma"));
					add_valores_a_mandar(valorAtributo("txtDesTratamientoPorDiente"));
					add_valores_a_mandar(valorAtributo('lblIdDocumento'));
					add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
					add_valores_a_mandar(IdSesion());
					add_valores_a_mandar(valorAtributo('lblIdDocumento'));
					ajaxModificar();
				}
			}
			else {
				if (verificarCamposGuardar(arg)) {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&idQuery=700&parametros=";
					add_valores_a_mandar(valorAtributo('lblIdDocumento'));
					add_valores_a_mandar(valorAtributo('txtDesTratamientoPorDiente'));
					add_valores_a_mandar(IdSesion());
					ajaxModificar();
				}
			}
			break;


		case 'guardarDienteYTratamiento':
			
			if (verificarCamposGuardar(arg)) {
				var id_trataDiente = 0

				if (diente[100].sellante == 'sellante')
					id_trataDiente = 1
				else if (diente[100].sellanteXhacer == 'sellanteXhacer')
					id_trataDiente = 12
				else if (diente[100].sellanteRealizado == 'sellanteRealizado')
					id_trataDiente = 21
				else if (diente[100].protesis == 'protesis')
					id_trataDiente = 2
				else if (diente[100].protesisXhacer == 'protesisXhacer')
					id_trataDiente = 3
				else if (diente[100].protesisRemovible == 'protesisRemovible')
					id_trataDiente = 13
				else if (diente[100].protesisRemovibleX == 'protesisRemovibleX')
					id_trataDiente = 17

				if (diente[100].corona == 'corona')
					id_trataDiente = 4
				else if (diente[100].coronaXhacer == 'coronaXhacer')
					id_trataDiente = 5
				if (diente[100].nucleo == 'nucleo')
					id_trataDiente = 15
				else if (diente[100].nucleoXhacer == 'nucleoXhacer')
					id_trataDiente = 16
				else if (diente[100].endodoncia == 'endodoncia')
					id_trataDiente = 6
				else if (diente[100].endodonciaXhacer == 'endodonciaXhacer')
					id_trataDiente = 7
				else if (diente[100].exodonciaIndi == 'exodonciaIndi')
					id_trataDiente = 8
				else if (diente[100].exodonciaPre == 'exodonciaPre')
					id_trataDiente = 18
				else if (diente[100].extraido == 'extraido')
					id_trataDiente = 9
				else if (diente[100].extraidoXcaries == 'extraidoXcaries')
					id_trataDiente = 19
				else if (diente[100].incluido == 'incluido')
					id_trataDiente = 14
				else if (diente[100].sinErupcionar == 'sinErupcionar')
					id_trataDiente = 10
				else if (diente[100].erupcion == 'erupcion')
					id_trataDiente = 11
				else if (diente[100].sano == 'sano')
					id_trataDiente = 20

				let arr = []


				switch (cara) {
					case 'M':
						if (diente[100].M != undefined) {
							lista.forEach((data) => {
								data.m = diente[100].M;
							})
						}
						break;
					case 'V':
						if (diente[100].V != undefined) {
							lista.forEach((data) => {
								data.v = diente[100].V;
							})
						}
						break;
					case 'L':
						if (diente[100].L != undefined) {
							lista.forEach((data) => {
								data.l = diente[100].L;
							})
						}
						break;
					case 'D':
						if (diente[100].D != undefined) {
							lista.forEach((data) => {
								data.d = diente[100].D;
							})
						}
						break;
					case 'O':
						if (diente[100].O != undefined) {
							lista.forEach((data) => {
								data.o = diente[100].O;
							})
						}
						break;
					case 'BIS':
						if (diente[100].BIS != undefined) {
							lista.forEach((data) => {
								data.bis = diente[100].BIS;
							})
						}
						break;
					case 'BII':
						if (diente[100].BII != undefined) {
							lista.forEach((data) => {
								data.bii = diente[100].BII;
							})
						}
						break;
				}

				lista.forEach((data) => {
					//console.log('diente[100].M,',diente[100].M)
					objeto = {
						id_paciente: valorAtributo('lblIdPaciente'),
						num_trata_historico: valorAtributo('cmb_num_trata_historico'),
						id_tipo_tratamiento: valorAtributo('cmbIdTipoTratamiento'),
						id_diente: data.ele,
						id_evolucion: valorAtributo('lblIdDocumento'),
						v: data.v,
						m: data.m,
						l: data.l,
						d: data.d,
						o: data.o,
						bis: data.bis,
						bii: data.bii,
						id_trata_diente: id_trataDiente
					}
					arr.push(objeto)
				})

				valores_a_mandar = 'accion=' + arg;
				valores_a_mandar = valores_a_mandar + "&idQuery=698&parametros=";
				add_valores_a_mandar(JSON.stringify(arr));
				add_valores_a_mandar(JSON.stringify(arr));
				console.log('datos ', arr)
				ajaxModificar();
			}
			
			break;

		case 'limpiarDiente':
			if (verificarCamposGuardar(arg)) {
				var id_trataDiente = 0

				let arr = []
				lista.forEach((data) => {
					objeto = {
						id_paciente: valorAtributo('lblIdPaciente'),
						num_trata_historico: valorAtributo('cmb_num_trata_historico'),
						id_tipo_tratamiento: valorAtributo('cmbIdTipoTratamiento'),
						id_diente: data.ele,
						id_evolucion: valorAtributo('lblIdDocumento'),
						v: '0',
						m: '0',
						l: '0',
						d: '0',
						o: '0',
						bis: '0',
						bii: '0',
						id_trata_diente: id_trataDiente
					}
					arr.push(objeto)
				})
				valores_a_mandar = 'accion=' + arg;
				valores_a_mandar = valores_a_mandar + "&idQuery=698&parametros=";
				add_valores_a_mandar(JSON.stringify(arr));
				add_valores_a_mandar(JSON.stringify(arr));
				ajaxModificar();
			}
			trat = 0;
			lista = [];
			$('#drag' + ventanaActual.num).find('#idDienteElegido').empty();
			limpiarDienteGrande()
			break;

		case 'eliminarTratamientoInicial':
			if (verificarCamposGuardar(arg)) {
				valores_a_mandar = 'accion=' + arg;
				valores_a_mandar = valores_a_mandar + "&idQuery=710&parametros=";
				add_valores_a_mandar(valorAtributo('lblIdPaciente'));
				add_valores_a_mandar(valorAtributo('lblIdDocumento'));
				add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
				ajaxModificar();
			}
			break;
		case 'dejarTratamientoInicial':
			if (verificarCamposGuardar(arg)) {
				valores_a_mandar = 'accion=' + arg;
				valores_a_mandar = valores_a_mandar + "&idQuery=699&parametros=";
				add_valores_a_mandar(valorAtributo('lblIdPaciente'));
				add_valores_a_mandar(valorAtributo('lblIdDocumento'));
				add_valores_a_mandar(valorAtributo('cmb_num_trata_historico'));
				ajaxModificar();
			}
			break;
		case 'guardar_cops':
			if (verificarCamposGuardar(arg)) {
				valores_a_mandar = 'accion=' + arg;
				valores_a_mandar = valores_a_mandar + "&idQuery=1055&parametros=";
				add_valores_a_mandar(valorAtributo('lblIdDocumento'));

				if (valorAtributo('txt_cariados') == '') {
					cariados = 0;
				}
				else {
					cariados = valorAtributo('txt_cariados');
				}
				if (valorAtributo('txt_obturados') == '') {
					obturados = 0;
				}
				else {
					obturados = valorAtributo('txt_obturados');
				}
				if (valorAtributo('txt_perdidos') == '') {
					perdidos = 0;
				}
				else {
					perdidos = valorAtributo('txt_perdidos');
				}
				if (valorAtributo('txt_sanos') == '') {
					sanos = 0;
				}
				else {
					sanos = valorAtributo('txt_sanos');
				}
				if (valorAtributo('txt_dpresentes') == '') {
					presentes = 0;
				}
				else {
					presentes = valorAtributo('txt_dpresentes');
				}
				if (valorAtributo('txt_perdidosXCaries') == '') {
					perdidosXCaries = 0;
				}
				else {
					perdidosXCaries = valorAtributo('txt_perdidosXCaries');
				}
				if (valorAtributo('txt_dcavitacional') == '') {
					cavitacional = 0;
				}
				else {
					cavitacional = valorAtributo('txt_dcavitacional');
				}
				add_valores_a_mandar(valorAtributo('lblIdDocumento'));
				add_valores_a_mandar(cariados);
				add_valores_a_mandar(obturados);
				add_valores_a_mandar(perdidos);
				add_valores_a_mandar(sanos);
				add_valores_a_mandar(perdidosXCaries);
				add_valores_a_mandar(valorAtributo('txt_observacionCops'));
				add_valores_a_mandar(presentes);
				add_valores_a_mandar(cavitacional);
				ajaxModificar();
			} else alert('COMPLETE TODOS LOS CAMPOS PARA COPS')
			break;

	}
}

function respuestaModificarCRUDOdontologia(arg, xmlraiz) {
	if (xmlraiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
		switch (arg) {
			case 'guardarPlaca':
				alert('GUARDADO EXITOSAMENTE')
				cargarOdontograma()
				break;
			case 'eliminarTratamientoPorDiente':
				cargarTratamientoPorDiente()
				break;
			case 'modificarTratamientoPorDiente':
				cargarTratamientoPorDiente()
				break;
			case 'tratamientoPorDiente':
				cargarTratamientoPorDiente()
				break;
			case 'guardarDienteYTratamiento':
				cargarOdontograma()
				break;
			case 'limpiarDiente':
				cargarOdontograma()
				break;
			case 'eliminarTratamientoInicial':
				cargarOdontogramaInicial()
				break;
			case 'dejarTratamientoInicial':
				cargarOdontogramaInicial()
				break;
			case 'guardar_cops':
				alert('GUARDADO EXITOSAMENTE')
				cargarOdontograma()
				break;
		}
	}
}








function respuestaGuardarOdontogramaBDconAjax() {
	if (varajaxInit.readyState == 4) {
		if (varajaxInit.status == 200) {
			raiz = varajaxInit.responseXML.documentElement;
			if (raiz.getElementsByTagName('respuesta')[0].firstChild.data == 'true') {
				alert("Se guardo el odontograma de este paciente");

			} else {
				alert("No puedo guardar los cambios al odontograma de este paciente");
			}
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxInit.readyState == 1) {
		//abrirVentana(260, 100);
		//VentanaModal.setSombra(true);
	}
}





function buscarHCOdontologia(arg, pag) {
	pagina = pag;
	ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 100;
	//    alto= ($('#drag'+ventanaActual.num).find("#divContenido").width())*92/100;  
	switch (arg) {

		case 'listTratamientoPorDiente':
			ancho = ($('#drag' + ventanaActual.num).find("#idTablaTratamEjec").width()) - 5;
			limpiarDivEditarJuan(arg);

			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=701&parametros="; //SUSTITUYE 16
			add_valores_a_mandar(valorAtributo('lblIdDocumento'));

			$('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'POST',
				colNames: ['contador', 'Id', 'Descripcion del Tratamiento', 'Profesional', 'Fecha elaboro', 'Folio', 'id_Estado_folio', 'Estado_folio', 'Id Procedimiento Ejecutado', 'Id Procedimiento', 'Procedimiento Ejecutado', 'Cantidad'],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },
					{ name: 'id', index: 'id', hidden: true },
					{ name: 'descripcion', index: 'descripcion', width: anchoP(ancho, 50) },
					{ name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 10) },
					{ name: 'Fecha', index: 'Fecha', width: anchoP(ancho, 10) },
					{ name: 'folio', index: 'folio', width: anchoP(ancho, 5) },
					{ name: 'id_estado_folio', index: 'id_estado_folio', hidden: true },
					{ name: 'estado_folio', index: 'estado_folio', width: anchoP(ancho, 5) },
					{ name: 'id_procedimiento_ejecutado', index: 'id_procedimiento_ejecutado', hidden: true },
					{ name: 'id_procedimiento', index: 'id_procedimiento', hidden: true },
					{ name: 'procedimiento', index: 'procedimiento', width: anchoP(ancho, 20) },
					{ name: 'cantidad', index: 'cantidad', hidden: true },
				],
				onSelectRow: function (rowid) {
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

					if (datosRow.id_estado_folio != '1') {
						asignaAtributo('lblIdTnsDienteTratamien', datosRow.id, 0)
						asignaAtributo('txtDesTratamientoPorDiente', datosRow.descripcion, 0)
						console.log(datosRow.id_procedimiento)
						asignaAtributo('txtIdProcedimientoEjecutadoOdontograma', concatenarCodigoNombre(datosRow.id_procedimiento, datosRow.procedimiento), 0)
						asignaAtributo('txtIdProcedimientoEjecutado', datosRow.id_procedimiento_ejecutado, 0)
						asignaAtributo('txtCantidadOdontograma', datosRow.cantidad, 0)
						asignaAtributo('txtIdEvolucionTratamiento', datosRow.folio, 0)
					} else {
						alert('PARA PODER EDITAR, EL ESTADO DEL FOLIO DEBE ESTAR ABIERTO')
					}
				},
				height: 140,
				width: ancho,
			});
			$('#drag' + ventanaActual.num).find("#listTratamientoPorDiente").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;
		case 'listPlaca':
			ancho = 1100;
			limpiarDivEditarJuan(arg);

			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=848&parametros=";
			add_valores_a_mandar(valorAtributo('lblIdPaciente'));


			$('#drag' + ventanaActual.num).find("#listPlaca").jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'POST',
				colNames: ['contador', 'Cantidad dientes', 'Superficies por diente', 'Observaciones', 'Piezas evaluadas', 'Superficies', 'Ausentes', 'Calculo', 'Profesional', 'Fecha elaboro', 'Folio', 'id_Estado_folio', 'Estado folio'],
				colModel: [
					{ name: 'contador', index: 'contador', hidden: true },

					{ name: 'cantidad_dientes', index: 'cantidad_dientes', width: anchoP(ancho, 10) },
					{ name: 'superficies_por_diente', index: 'superficies_por_diente', width: anchoP(ancho, 10) },
					{ name: 'observaciones', index: 'observaciones', width: anchoP(ancho, 12) },

					{ name: 'piezas', index: 'piezas', width: anchoP(ancho, 10) },
					{ name: 'superficies', index: 'superficies', width: anchoP(ancho, 10) },
					{ name: 'ausentes', index: 'ausentes', width: anchoP(ancho, 10) },
					{ name: 'calculo', index: 'calculo', width: anchoP(ancho, 10) },

					{ name: 'Profesional', index: 'Profesional', width: anchoP(ancho, 10) },
					{ name: 'Fecha', index: 'Fecha', width: anchoP(ancho, 9) },
					{ name: 'folio', index: 'folio', width: anchoP(ancho, 4) },
					{ name: 'id_estado_folio', index: 'id_estado_folio', hidden: true },
					{ name: 'estado_folio', index: 'estado_folio', width: anchoP(ancho, 5) },
				],
				onSelectRow: function (rowid) {
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);

					//   if (datosRow.id_estado_folio != '1') {
					asignaAtributo('lblIdDocumentoPlaca', datosRow.folio, 0)


					if (valorAtributo('cmbIdTipoTratamiento') == 'S') {
						cargarControlDePlaca()

					} else alert('EL TIPO DE TAREA DEBE ESTAR EN SEGUIMIENTO')
					/*  } else {
						  alert('PARA PODER EDITAR, EL ESTADO DEL FOLIO DEBE ESTAR ABIERTO')
					  }*/
				},

				height: 100,
				width: ancho,
			});
			$('#drag' + ventanaActual.num).find("#listPlaca").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			$('#drag' + ventanaActual.num).find("#listPlaca").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;
	}
}        