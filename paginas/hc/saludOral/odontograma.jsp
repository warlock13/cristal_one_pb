<div id="divOdontograma">
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td width="50%" valign="top">
        <table>
          <tr>
            <td width="630" height="300" valign="top">
              <div id="contenedorOdonto"
                style=" background:url(/clinica/paginas/hc/saludOral/diente/NUEVO_fondoOdontograma.png); display:block; top:0px; left:0px; width: 630px; height: 300px; position:relative; z-index:99;">
                <img id="imagenAreaOdontograma" usemap="#odontogramaHueco"
                  src="/clinica/paginas/hc/saludOral/diente/nueva_dentaduraHueco.png"
                  style="top:0px; left:0px; width: 630px; height: 300px; position:relative; z-index:100;"
                  alt="mueita" />
                <map name="odontogramaHueco" id="mapAreaOdontograma"></map>
              </div>
            </td>
          </tr>
          <tr height="15px">
            <td> </td>
          </tr>
          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_cariados">CARIES CAVITACIONAL: </td>
                  <td align="left"><input type="text" id="txt_cariados" oninput="validarcantdientes(this);"
                      onkeypress="javascript:return soloNumeros(event);" style="width:30%; text-align: center;"
                      maxlength="2"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_obturados">OBTURADOS: </td>
                  <td align="left"><input type="text" id="txt_obturados" oninput="validarcantdientes(this);"
                      onkeypress="javascript:return soloNumeros(event)" style="width:30%; text-align: center;"
                      maxlength="2"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_perdidos">PERDIDOS: </td>
                  <td align="left"><input type="text" id="txt_perdidos" oninput="validarcantdientes(this);"
                      onkeypress="javascript:return soloNumeros(event)" style="width:30%; text-align: center;"
                      maxlength="2"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_sanos">SANOS: </td>
                  <td align="left"><input type="text" id="txt_sanos" oninput="validarcantdientes(this);"
                      onkeypress="javascript:return soloNumeros(event)" style="width:30%; text-align: center;"
                      maxlength="2"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_dpresentes">DIENTES PRESENTES BOCA: </td>
                  <td align="left"><input type="text" id="txt_dpresentes" oninput="validarcantdientes(this);"
                      onkeypress="javascript:return soloNumeros(event)" style="width:30%; text-align: center;"
                      maxlength="2"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_perdidosXCaries">EXTRAIDO POR CARIES: </td>
                  <td align="left"><input type="text" id="txt_perdidosXCaries" oninput="validarcantdientes(this);"
                      onkeypress="javascript:return soloNumeros(event)" style="width:30%; text-align: center;"
                      maxlength="2"></td>
                </tr>
              </table>
            </td>
          </tr>

          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_dcavitacional">DIENTES CARIES NO CAVITACIONAL: </td>
                  <td align="left"><input type="text" id="txt_dcavitacional" oninput="validarcantdientes(this);"
                      onkeypress="javascript:return soloNumeros(event)" style="width:30%; text-align: center;"
                      maxlength="2"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="camposRep">
            <td align="center" width="50%">
              <table>
                <tr>
                  <td align="right" width="50%" id="td_observacionCops">OBSERVACIONES: </td>
                  <td align="left"> </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="camposRep">
            <td align="right" width="50%">
              <table>
                <tr>
                  <td align="right" width="84%"><textarea type="text" id="txt_observacionCops" rows="4" cols="50"
                      maxlength="4000" style="width:90%"  onblur=""> </textarea></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr height="15px">
            <td> </td>
          </tr>
          <tr>
            <td align="center">
              <input type="button" id="creartratamientoini" class="small button blue"
                onclick="modificarCRUDOdontologia('dejarTratamientoInicial')" value=" CREAR TRATAMIENTO INICIAL" />
              <input type="button" id="eliminartratamientoini" class="small button blue"
                onclick="modificarCRUDOdontologia('eliminarTratamientoInicial')" value="ELIMINAR TRATAMIENTO INICIAL" />
              <input type="button" id="guardarcops" class="small button blue"
                onclick="modificarCRUDOdontologia('guardar_cops')" value="GUARDAR COPS" />
              <input type="button" id="reiniciar_contadores" class="small button blue"
                onclick="asignaAtributo('txt_cariados', 0, 0);asignaAtributo('txt_obturados', 0, 0);asignaAtributo('txt_perdidos', 0, 0);
                asignaAtributo('txt_sanos', 0, 0);asignaAtributo('txt_dpresentes', 0, 0);asignaAtributo('txt_perdidosXCaries', 0, 0);
                asignaAtributo('txt_dcavitacional', 0, 0);" value="REINICIAR CONTADORES" />
            </td>
          </tr>
          <tr height="15px">
            <td> </td>
          </tr>
        </table>
      </td>
      <td width="50%" valign="top">
        <table width="100%" height="250">
          <tr class="subtitulosRep" height="150px">
            <td width="35%" height="200" valign="top">

            </td>
            <td width="72%" valign="top">
              <!--
               
-->


              <table width="100%" height="200" id="idTablaTratamientos">
                <tr>
                  <td height="200" valign="top" id="tdTratamien">
                    <table width="421" border="0" height="200px" cellpadding="0" cellspacing="0">
                      <tr class="camposRep">
                        <td width="10%" class="puntos caries"></td>
                        <td width="90%" cellpadding="0" cellspacing="0"> <input type="radio" id="4" name="1"
                            onclick="gestionTratam(this.id, 'caries');" />Caries Cavitacional
                        </td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos puntiAzul"> </td>
                        <td>
                          <input type="radio" id="1" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion en amalgama buen estado');" />Obturacion en
                          amalgama buen estado
                        </td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos puntiAzulMalEstado"> </td>
                        <td>
                          <input type="radio" id="5" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion en amalgama en MAL estado');" />
                          Obturacion en amalgama en mal estado</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos puntiAmar"> </td>
                        <td>
                          <input type="radio" id="2" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion en resina buen estado');" />Obturaci&oacute;n en
                          resina buen estado</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos puntiAmarMalEstado"> </td>
                        <td>
                          <input type="radio" id="6" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion en resina MAL estado');" />Obturaci&oacute;n en
                          resina MAL estado</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos puntiVerd"> </td>
                        <td>
                          <input type="radio" id="3" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion temporal buen estado');" />Obturaci&oacute;n
                          temporal buen estado</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos puntiVerdMalEstado"> </td>
                        <td>
                          <input type="radio" id="7" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion temporal MAL estado');" />Obturaci&oacute;n
                          temporal MAL estado</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos traumaBuenEstado"> </td>
                        <td>
                          <input type="radio" id="13" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion trauma buen estado');" />Obturaci&oacute;n por
                          trauma
                          buen estado</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos traumaMalEstado"> </td>
                        <td>
                          <input type="radio" id="14" name="1"
                            onclick="gestionTratam(this.id, 'Obturacion trauma mal estado');" />Obturaci&oacute;n por
                          trauma mal estado</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos ccavitacional"> </td>
                        <td>
                          <input type="radio" id="15" name="1"
                            onclick="gestionTratam(this.id, 'caries cavitacional');" />Caries no cavitacional</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos fractura"> </td>
                        <td>
                          <input type="radio" id="9" name="1" onclick="gestionTratam(this.id, 'Fractura');" />Fractura
                        </td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos abrasion"> </td>
                        <td>
                          <input type="radio" id="10" name="1"
                            onclick="gestionTratam(this.id, 'Abrasi\F3n');" />Abrasi&oacute;n</td>
                      </tr>
                      <tr class="camposRep">
                        <td class="puntos puntiCian"> </td>
                        <td>
                          <input type="radio" id="11" name="1"
                            onclick="gestionTratam(this.id, 'Recesion buen estado');" />Recesi&oacute;n</td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          <TABLE width="100%" border="1">

                            <tr class="camposRep">
                              <td class="puntos lasS"> </td>
                              <td>
                                <input type="radio" id="sellante" name="1"
                                  onclick="gestionTratam(this.id, 'Sellante se hizo o no tiene');" />Sellante presente
                              </td>
                              <td class="puntos lasSRoja"> </td>
                              <td>
                                <input type="radio" id="sellanteXhacer" name="1"
                                  onclick="gestionTratam(this.id, 'Sellante por hacer');" />Sellante indicado</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos3 lasSRealizado"> </td>
                              <td>
                                <input type="radio" id="sellanteRealizado" name="1"
                                  onclick="gestionTratam(this.id, 'Sellante realizado');" />Sellante realizado
                              </td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos elIgual"> </td>
                              <td>
                                <input type="radio" id="protesis" name="1"
                                  onclick="gestionTratam(this.id, 'Pr\F3tesis se hizo o  tiene');" />Pr&oacute;tesis
                                adaptada</td>
                              <td class="puntos elIgualRoja"> </td>
                              <td>
                                <input type="radio" id="protesisXhacer" name="1"
                                  onclick="gestionTratam(this.id, 'Pr\F3tesis por hacer');" />Pr&oacute;tesis
                                desadaptada</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos cruzCaida"> </td>
                              <td>
                                <input type="radio" id="protesisRemovible" name="1"
                                  onclick="gestionTratam(this.id, 'Pr\F3tesis removible');" />Pr&oacute;tesis removible
                                buen estado</td>
                              <td class="puntos cruzCaidaroja"> </td>
                              <td>
                                <input type="radio" id="protesisRemovibleX" name="1"
                                  onclick="gestionTratam(this.id, 'Pr\F3tesis removible mal estado');" />Pr&oacute;tesis
                                removible mal estado</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos elAritoAzul"></td>
                              <td><input type="radio" id="corona" name="1"
                                  onclick="gestionTratam(this.id, 'Corona se hizo o tiene');" />Corona adaptada</td>
                              <td class="puntos elAritoRoja"></td>
                              <td><input type="radio" id="coronaXhacer" name="1"
                                  onclick="gestionTratam(this.id, 'Corona por hacer');" />Corona desadaptada</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos triangulito"></td>
                              <td><input type="radio" id="endodoncia" name="1"
                                  onclick="gestionTratam(this.id, 'Nucleo realizado');" />N&uacute;cleo realizado</td>
                              <td class="puntos triangulitoRoja"></td>
                              <td><input type="radio" id="endodonciaXhacer" name="1"
                                  onclick="gestionTratam(this.id, 'Nucleo por hacer');" />N&uacute;cleo requerido</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos triangulitoAroAzul"> </td>
                              <td><input type="radio" id="nucleo" name="1"
                                  onclick="gestionTratam(this.id, 'Endodoncia se hizo o tiene');" />Endodoncia presente
                              </td>
                              <td class="puntos triangulitoAroRojo"> </td>
                              <td><input type="radio" id="nucleoXhacer" name="1"
                                  onclick="gestionTratam(this.id, 'Endodoncia por hacer');" />Endodoncia indicada</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos palito"> </td>
                              <td><input type="radio" id="extraido" name="1"
                                  onclick="gestionTratam(this.id, 'Extraido');" />Extraido o ausente</td>
                              <td class="puntos extraccionXCaries"> </td>
                              <td><input type="radio" id="extraidoXcaries" name="1"
                                  onclick="gestionTratam(this.id, 'Extraccion por caries');" />Extraido o ausente por
                                caries</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos exodonciaPresente"> </td>
                              <td><input type="radio" id="exodonciaPre" name="1"
                                  onclick="gestionTratam(this.id, 'Exodoncia presente');" />Exodoncia realizada</td>
                              <td class="puntos exodonciaIndicada"> </td>
                              <td><input type="radio" id="exodonciaIndi" name="1"
                                  onclick="gestionTratam(this.id, 'Exodoncia indicada');" />Exodoncia indicada</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos erupcionPunto"> </td>
                              <td><input type="radio" id="erupcion" name="1"
                                  onclick="gestionTratam(this.id, 'En erupci\F3n');" />En erupci&oacute;n</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos incluido"> </td>
                              <td><input type="radio" id="incluido" name="1"
                                  onclick="gestionTratam(this.id, 'incluido');" />Incluido o semiincluido</td>
                            </tr>
                            <tr class="camposRep">
                              <td height="26" class="puntos ausent"></td>
                              <td><input type="radio" id="sinErupcionar" name="1"
                                  onclick="gestionTratam(this.id, 'sinErupcionar');" />sin Erupcionar</td>
                            </tr>
                            <tr class="camposRep">
                              <td class="puntos puntisano"></td>
                              <td><input type="radio" id="sano" name="1"
                                  onclick="gestionTratam(this.id, 'sano');" />Diente Sano</td>
                            </tr>
                          </TABLE>

                        </td>
                      </tr>

                    </table>
                  </td>
                </tr>
              </table>


              <TABLE width="99%" style="display:block;" id="idTablaPlaca">
                <tr>
                  <td class="estiloImput" width="99%" colspan="4">
                    <table width="100%">
                      <tr class="camposRep" valign="top">
                        <td align="center" width="50%">
                          <table align="left">
                            <tr>
                              <td align="center" class="puntos placa"> </td>
                              <td align="left"><input type="radio" id="8" name="1"
                                  onclick="gestionTratam(this.id, 'placa');" />Placa</td>
                              <td width="30px"> </td>
                              <td align="right" class="puntos2 palitoAzul"> </td>
                              <td align="left" width="65%"><input type="radio" id="extraido" name="1"
                                  onclick="gestionTratam(this.id, 'Extraido');" />Extraido o ausente</td>
                            </tr>
                          </table>
                        </td>
                        <!-- <td width="65%"><input type="radio" id="extraido" name="1" onclick="gestionTratam(this.id, 'Extraido');" />Extraido o ausente</td> -->
                      </tr>
                      <tr class="camposRep" valign="top">
                        <td class="estiloImput">
                        </td>
                      </tr>
                    </table>

                    <table width="100%">
                      <tr class="camposRep">
                        <td width="50%">CANTIDAD DE DIENTES:</td>
                        <td width="50%" align="left"><input type="text" id="txt_cantDientesPlaca"
                            oninput="validarcantdientes2(this);" onkeypress="javascript:return soloNumeros(event)"
                            style="width:40%; text-align: center;" maxlength="2"></td>
                      </tr>
                      <tr class="camposRep">
                        <td>SUPERFICIES POR DIENTE:</td>
                        <td align="left">
                          <select size="1" id="cmb_superficiesPorDiente" title="3" style="width:40%"
                            onblur="alert('RECUERDE GUARDAR CAMBIOS PARA ESTE CONTROL DE PLACA')" >
                            <option value="4" selected>4</option>
                            <option value="5">5</option>
                          </select> </td>
                      </tr>
                      <tr class="camposRep">
                        <td colspan="2">OBSERVACIONES:</td>
                      </tr>
                      <tr class="camposRep">
                        <td colspan="3">
                          <textarea type="text" id="txt_observacionPlaca" rows="3" cols="50" maxlength="4000"
                            style="width:95%"  onblur=""> </textarea>
                        </td>
                      </tr>
                      <tr class="camposRep">
                        <td colspan="3" align="center">
                          <input id="btnguardarPlaca" title="BPLACA" type="button" class="small button blue"
                            value="GUARDAR CONTROL DE PLACA" onclick="modificarCRUDOdontologia('guardarPlaca')"
                             />
                        </td>
                      </tr>
                    </table>


                    <table width="100%">
                      <tr class="camposRep">
                        <td>AUSENTES:</td>
                        <td><label id="lblCantAusentes">-</label></td>
                      </tr>
                      <tr class="camposRep">
                        <td width="50%">DIENTES EVALUADOS:</td>
                        <td width="50%" align="left"><label id="lblTotalPiezas">0</label></td>
                      </tr>
                      <tr class="camposRep">
                        <td>SUPERFICIES CON PLACA:</td>
                        <td align="left"><label id="lblSuperficiesPlaca">-</label></td>
                      </tr>
                      <tr class="camposRep">
                        <td>INDICE DE O'LEARY:</td>
                        <td colspan="3"><label id="lblCalculoPlaca">-</label> %
                        </td>
                      </tr>
                    </table>

                  </td>
                </tr>
              </TABLE>


            </td>
          </tr>

        </table>
      </td>
    </tr>

  </table>
  </td>
  </tr>
  </table>
</div>