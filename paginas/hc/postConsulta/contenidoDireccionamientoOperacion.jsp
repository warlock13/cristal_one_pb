<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table id="idTablaVentanitaIntercambio" width="100%" height="120px">
  <tr class="estiloImput">
    <td colspan="2">&nbsp;
    </td>
    <td colspan="1">
      <input type="button"
        onClick="buscarHC('listProcedimientosOrdenes','/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')"
        value="BUSCAR" title="BT98">
    </td>
    <td colspan="1">&nbsp;
    </td>
  </tr>
  <tr class="estiloImput">
    <td colspan="4" valign="top" align="center">
      <table id="listProcedimientosOrdenesIntercambio" class="scroll"></table>
    </td>
  </tr>
  <tr class="estiloImput">
    <td colspan="4">
      <table width="100%">
        
      </table>

      <table width="100%">
        <tr class="estiloImput" style="display:none">
          <td colspan="2"><label id="lblIdProcedimientoAutoriza"></label> <label
              id="lblNomProcedimientoAutoriza"></label>
          </td>
        </tr>        
        </table>
    </td>
  </tr>
  <tr>
    <td colspan="4">
      <div id="tabsPrincipalDireccionamiento" style="width:98%; height:auto">
        <ul>
          <li>
            <a href="#divSolicitudes" onclick="tabActivoDireccionamiento('divSolicitudes')">
              <center>SOLICITUDES</center>
            </a>
          </li>
          <li>
            <a href="#divListaEspera" onclick="tabActivoDireccionamiento('divListaEspera')">
              <center>LISTA ESPERA INDIVIDUAL</center>
            </a>
          </li>
          <li>
            <a href="#divLaboratorio" onclick="TabActivoConductaTratamiento('divLaboratorio')">
              <center>DIRECCIONAMIENTO LABORATORIO</center>
            </a>
          </li>
          <li>
            <a href="#divProgramacionTerapias" onclick="TabActivoConductaTratamiento('divProgramacionTerapias');">
              <center>PROGRAMACION TERAPIAS</center>
            </a>
          </li>

        </ul>

        <div id="divSolicitudes">

          <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr class="titulos">
              <td align="right">Observaci&oacute;n:</td>
              <td colspan="6">
                <textarea type="text" id="txt_observacionGestion" rows="2" maxlength="4000" style="width:80%"
                  > </textarea>
              </td>
              <td width="50%">
              </td>
            </tr>
            <tr class="titulos">
              <td align="right">Justificaci&oacute;n</td>
              <td colspan="6">
                <textarea type="text" id="txtJustificacionFolio" rows="2" maxlength="4000" style="width:80%"
                  > </textarea>
              </td>
              <td></td>
            </tr>
            <tr class="titulos">
              <td width="3%"></td>
              <td width="37%">Estado de la orden</td>
              <td width="15%">Solicitud:<label id="lblIdSolicitud"></label></td>
              <td width="10%"><label id="lblIdAutorizacion"></label></td>
              <td width="10%">Esperar Dias</td>
              <td width="5%">Gestionado</td>
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
            </tr>
            <tr class="estiloImput">
              <td>
                <input id="idBtnSolicitAutoriza" title="AUT34" type="button" class="small button blue"
                  value="Guardar Justificaci&oacute;n" onClick="modificarCRUD('modificaJustificacionConcepto')" />
              </td>
              <td>
                Estado:<select id="cmbEstadoEditProc" style="width:40%" >
                  <option value=""></option>
                  <option value="1">1-Ordenado</option>
                  <option value="3">3-Pendiente</option>
                  <option value="7">7-Gestionado con cotizacion</option>
                </select>
                &nbsp;&nbsp;
                <input id="idBtnSolicitAutoriza" title="LOCA17" type="button" class="small button blue" value="Modificar"
                  onclick="modificarCRUD('procedimientosGestionEdit_2');" />
              </td>
              <td align="center">
                <input type="button" class="small button blue" title="BPQ4" value="Ver"
                  onclick="consultaSolicitudAutorizacion('solicitud')" />
                <input type="button" class="small button blue" value="Limpiar" title="BL77"
                  onClick="modificarCRUD('limpiaSolicitudIntercambio')" />
              </td>
              <td align="center"> <input type="button" class="small button blue" title="BPQ5" value="Autorizaci&oacute;n"
                  onclick="consultaSolicitudAutorizacion('autorizacion')" /></td>
              <td>
                <select id="cmbEsperarProcAutoriza" size="1" style="width:60%" >
                  <option value="0">0 Dias</option>
                  <option value="10">10 dias</option>
                  <option value="20">20 dias</option>
                  <option value="30">30 dias (1 mes)</option>
                  <option value="60">60 dias (2 meses)</option>
                  <option value="90">90 dias (3 meses)</option>
                  <option value="120">120 dias(4 meses)</option>
                  <option value="150">150 dias(5 meses)</option>
                  <option value="180">180 dias(6 meses)</option>
                  <option value="210">210 dias(7 meses)</option>
                  <option value="240">240 dias(8 meses)</option>
                  <option value="270">270 dias(9 meses)</option>
                  <option value="300">300 dias(10 meses)</option>
                  <option value="330">330 dias(11 meses)</option>
                  <option value="365">365 dias(12 meses)</option>
                </select>
              </td>
              <td>
                <select id="cmbIdEnviado" style="width:70%" title="32">
                  <option value="SI">SI</option>
                  <option value="NO">NO</option>
                </select>
              </td>
              <td><input id="idBtnSolicitAutoriza" type="button" class="small button blue" value="Gestionar Procedimiento"
                  onClick="modificarCRUD('enviaAutorizaIntercambio')" /></td>
              <td><input id="idBtnSolicitAutoriza" type="button" class="small button blue" title="BL231"
                  value="Enviar solicitud" onClick="formatoReportPDFIntercambio()" /></td>
            </tr>

            </table>


        </div>

        <div id="divListaEspera" style="width:99%">
          <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr class="titulosListaEspera">
              <td width="20%">Sede</td>
              <td width="20%">Especialidad</td>
              <td width="20%">Tipo Cita</td>
              <td width="20%">Profesional</td>
            </tr>
            <tr class="estiloImput">
              <td>
                <select size="1" id="cmbSede" style="width:95%" title="GD48"
                  onfocus="comboDependienteEmpresa('cmbSede','557')"
                  onchange="asignaAtributo('cmbIdEspecialidad', '', 0); asignaAtributo('cmbProfesionales', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCita', 'cmbIdEspecialidad')">
                </select>
              </td>
              <td>
                <select size="1" id="cmbIdEspecialidad" style="width:99%" onfocus="cargarEspecialidadDesdeSede('cmbSede', 'cmbIdEspecialidad')" onchange="buscarAGENDA('listDias', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');">	                                        
                  <option value=""></option> 
                </select>
              </td>
              <td>
                <select id="cmbTipoCita"  onfocus="cargarTipoCitaSegunEspecialidad(this.id)" onchange="buscarAGENDA('listEsperaCEXProcedimientoTraer')" style="width:90%"  >
                  <option value=""></option>
                </select>  
              </td>
              <td>
                <select id="cmbProfesionales" style="width:80%" onfocus="cargarProfesionalesDesdeEspecialidad(this.id)" >
                  <option value="" selected="">[ Seleccione ]</option>
                </select>
              </td>
            </tr>
            <tr class="camposRepInp">
              <td colspan="4">INFOMACION DEL PROCEDIMIENTO</td>
            </tr>
            <tr>
              <td colspan="4">
                <table width="100%">
                  <tr class="titulosListaEspera">
                    <td style="width: 10%;">Sitio</td>
                    <td style="width: 10%;">Codigo Procedimiento</td>
                    <td style="width: 40%;">Nombre</td>
                    <td>Observaciones</td>
                  </tr>
                  <tr class="estiloImput">
                    <td><input type="hidden" id="txtIdSitioLE"><strong><label id="lblSitioLE"></label></strong></td>
                    <td><strong><label id="lblIdProcedimientoLE"></label></strong></td>
                    <td><strong><label id="lblNombreProcedimientoLE"></label></strong></td>
                    <td><strong><label id="lblObservacionesLE"></label></strong></td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr class="camposRepInp">
              <td colspan="2">LISTA DE ESPERA</td>
              <td colspan="2">CITA</td>
            </tr>

            
            <tr>
              <td colspan="2">
                <table width="100%">
                  <tr class="titulosListaEspera">
                    <td width="25%">Esperar dias</td>
                    <td>Discapacidad F&iacute;sica</td>
                    <td>Embarazo</td>
                    <td width="25%">Grado Necesidad</td>
                    <td></td>
                  </tr>
                  <tr class="estiloImput">
                    <td align="CENTER">10</td>
                    <td>
                      <select id="cmbDiscapaciFisica"  size="1" style="width:80%"  >
                        <option value=""></option>                       
                        <option value="N" selected="selected">No</option>                                      
                        <option value="S">Si</option>
                    </select>                                  	 
                    </td>                       
                    <td>
                      <select id="cmbEmbarazo" size="1"  style="width:70%"  >
                        <option value=""></option>                       
                        <option value="N" selected="selected">No</option>                                      
                        <option value="S">Si</option>
                      </select>                                  	 
                    </td>        
                    <td><select id="cmbNecesidad" size="1" style="width:90%" >
                      <option value=""></option>
                      <%     
                      resultaux.clear();
                      resultaux=(ArrayList)beanAdmin.combo.cargar(113);  
                      ComboVO cmbG; 
                      for(int k=0;k<resultaux.size();k++){ 
                            cmbG=(ComboVO)resultaux.get(k);
                        %>
                      <option value="<%= cmbG.getId()%>" title="<%= cmbG.getTitle()%>"><%=cmbG.getDescripcion()%></option>
                      <%}%> 
                      </select> 
                   </td> 
                    <td align="left">
                      <input id="idBtnSolicitAutoriza" type="button" class="small button blue" title="BL231"
                      value="Crear lista espera" onclick="modificarCRUD('crearListaEsperaProgramacion')"/>
                    </td>
                  </tr>
                </table>
              </td>
              <td colspan="2">
                <table width="100%">
                  <tr class="titulosListaEspera">
                    <td>Fecha Cita</td>
                    <td>Hora</td>
                    <td width="50%">Observaciones</td>
                    <td></td>
                  </tr>
                  <tr class="estiloImput">
                    <td>
                      <input type="text" id="txtFechaCita" style="width: 95%;">
                    </td>
                    <td>
                      <select id="cmbHoraCita" style="width: 30%;">
                        <%
                                            for (int i = 101; i < 113; i++) {
                                                    String[] value = Integer.toString(i).split("");
                                                    String hour = value[1] + value[2];
        
                                        %>
                        <option value="<%=hour%>"><%=hour%></option>
                        <%}
                                        %>
                        </select>
                        <select id="cmbMinutoCita" style="width: 30%;">
                        <%
                                            for (int i = 100; i < 160; i++) {
                                                String[] value = Integer.toString(i).split("");
                                                String minute = value[1] + value[2];
                                        %>
                        <option value="<%=minute%>"><%=minute%></option>
                        <%}
                                        %>
                        </select>
                        <select id="cmbPeriodoCita" style="width: 30%;">
                            <option value="AM">AM</option>
                            <option value="PM">PM</option>
                        </select>
                    </td>
                    <td>
                      <textarea id="txtObservacionesLE" rows="1" style="width: 95%;"></textarea>
                    </td>
                    <td align="left">
                      <input type="button" class="small button blue" title="BL231"
                      value="Crear cita" onClick="modificarCRUD('crearCitaProgramacion')" />
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr class="titulosCentrados" >   
              <td colspan="2">LISTA ESPERA</td>
              <td colspan="2">CITA</td>                
            </tr> 
            <tr class="titulos">
              <td colspan="2">
                <table id="listaEsperaProgramacion" class="scroll" width="50%"></table> 
              </td>
              <td colspan="2">
                <table id="citaProgramacion" class="scroll" width="50%"></table>
              </td>
            </tr>
          </table>
        </div>

        <div id="divLaboratorio">
          <table width="100%" style="background-color: #E8E8E8;" align="center">

              <tr class="titulosListaEspera">
                <td style="width: 5%;">Cups</td>
                <td style="width: 10%;">Clase</td>
                <td style="width: 30%;">Procedimiento</td>
                <td style="width: 5%;">Cantidad</td>
                <td style="width: 10%;">Grado Necesidad</td>
                <td style="width: 10%;">Estado</td>
                <td style="width: 25%;">Indicaciones</td>
                <td style="width: 5%;">Diagnostico</td>
              </tr>

              <tr class="estiloImput">
                <td><label id="lblCupsLab"></label></td>
                <td><label id="lblIdClaseLab"></label>-<label id="lblClaseLab"></label></td>
                <td><label id="lblNombreProcedimientoLab"></label></td>
                <td><label id="lblCantidadLab"></label></td>
                <td><label id="lblGradoNecisidadLab"></label></td>
                <td><label id="lblIdEstadoLab"></label>-<label id="lblEstadoLab"></label></td>
                <td><label id="lblIndicacionesLab"></label></td>
                <td><label id="lblDiagnosticoLab"></label></td>
              </tr>

              <tr class="titulosListaEspera">
                <td colspan="3">Laboratorio Direccionamiento</td>
                <td colspan="4">Observaciones</td>
                <td></td>
              </tr>
              <tr class="estiloImput">
                <td colspan="3">
                <input type="text" id="txtIdLaboratorioEmpresa" style="width:98%" onkeypress="llamarAutocomIdDescripcionConDato('txtIdLaboratorioEmpresa', 259)"/>
              </td>
              <td colspan="4">
                <textarea id="txtObservacionesLab" rows="1" style="width: 90%;"></textarea>
              </td>
              <td>
                <input type="button" class="small button blue" value="GUARDAR" onClick="modificarCRUD('direccionarLaboratorio')" />
              </td>
              </tr>
          </table>
        </div>

        
        <div id="divProgramacionTerapias" style="width:99%">
          <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr class="titulosListaEspera">
              <td width="15%">Sede</td>
              <td width="15%">Especialidad</td>
              <td width="15%">Tipo Cita</td>
              <td width="15%">Profesional</td>
              <td width="10%">Fecha Inicio</td>
              <td width="10%">Cantidad citas</td>
              <td width="10%">Frecuencia</td>
              <td width="10%"></td>
            </tr>
            <tr class="estiloImput">
              <td>
                <select size="1" id="cmbSedeTerapia" style="width:95%" title="GD48"
                  onfocus="comboDependienteSede('cmbSedeTerapia','1127')"
                  onchange="asignaAtributo('cmbIdEspecialidadTerapia', '', 0); asignaAtributo('cmbProfesionalesTerapia', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCitaTerapia', 'cmbIdEspecialidad')">
                </select>
              </td>
              <td>
                <select size="1" id="cmbIdEspecialidadTerapia" style="width:99%" onfocus="cargarEspecialidadDesdeSede('cmbSedeTerapia', 'cmbIdEspecialidadTerapia')"
                onchange="asignaAtributo('cmbTipoCitaTerapia', '', 0)">	                                        
                  <option value=""></option> 
                </select>
              </td>
              <td>
                <select id="cmbTipoCitaTerapia"  onfocus="cargarCitaSegunEspecialidad(this.id,'cmbIdEspecialidadTerapia')"  style="width:90%"  >
                  <option value=""></option>
                </select>  
              </td>
              <td>
                <select id="cmbProfesionalesTerapia" style="width:80%" onfocus="cargarProfesionalDesdeEspecialidad('cmbIdEspecialidadTerapia',this.id)" >
                  <option value="" selected="">[ Seleccione ]</option>
                </select>
              </td>


                <td ><input id="txtFechaInicioTerapia" type="text"  /></td> 

              
           
              
              <td>

                <select id="cmbCantidadCitasTerapia"  style="width:80%" >	                                        
                  <option value=""></option>	
                  <option value="1">1</option>						
                  <option value="2">2</option>	
                  <option value="3">3</option>						
                  <option value="4">4</option>						                                                                  					
                  <option value="5">5</option>						
                  <option value="6">6</option>	
                  <option value="7">7</option>						
                  <option value="8">8</option>	
                  <option value="9">9</option>	
                  <option value="10">10</option>						
                  <option value="11">11</option> 
                  <option value="12">12</option>	
                  <option value="13">13</option>						
                  <option value="14">14</option>						                                                                  					
                  <option value="15">15</option>						
                  <option value="16">16</option>	
                  <option value="17">17</option>						
                  <option value="18">18</option>	
                  <option value="19">19</option>	
                  <option value="20">20</option>	 
                  <option value="21">21</option> 
                  <option value="22">22</option>	
                  <option value="23">23</option>						
                  <option value="24">24</option>						                                                                  					
                  <option value="25">25</option>						
                  <option value="26">26</option>	
                  <option value="27">27</option>						
                  <option value="28">28</option>	
                  <option value="29">29</option>	
                  <option value="30">30</option> 								                                                                     
                              </select>  

              </td>
   <td>

    <select id="cmbFrecuenciaTerapia"  style="width:80%">
      <option value=""></option>	
      <option value="1">1 dia</option>
      <option value="2">2 dias</option>
      <option value="3">3 dias</option>
      <option value="4">4 dias</option>
      <option value="5">5 dias</option>
      <option value="6">6 dias</option>
      <option value="7">7 dias</option>
      <option value="8">8 dias</option>
      <option value="9">9 dias</option>
      <option value="10">10 dias</option>
      <option value="11">11 dias</option>
      <option value="12">12 dias</option>
      <option value="13">13 dias</option>
      <option value="14">14 dias</option>
      <option value="15">15 dias</option>
    </select>

   </td>

              <td ><input type="button" class="small button blue" title="BL231"
                value="Programar" onClick="modificarCRUD('crearCitasTerapias')" /></td>     
            </tr> 
            
            <tr>
              <td class="titulos" colspan="8">
                <table id="listaProgramacionTerapias"></table> 
              </td>
            </tr>
          </table>
     
        </div>
      </div>
    </td>
  </tr>
</table>


<div id="divVentanitaEditarTerapia" style="display:none; z-index:2000; top:400px; left:50px; ">

  <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
  </div>
  <div style="z-index:2004; position:fixed; top:300px;left:400px; width:90%">
      <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
          <tr class="estiloImput">
              <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                      onclick="ocultar('divVentanitaEditarTerapia')" /></td>
              <td colspan="6">&nbsp;</td>
              <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                       onclick="ocultar('divVentanitaEditarTerapia')"/></td>
          </tr>
          <tr class="titulosListaEspera">
              <td width="10%">ID</td>
              <td width="20%">SEDE</td>
              <td width="20%">ESPECIALIDAD</td>
              <td width="20%">TIPO CITA</td>
              <td width="20%">PROFESIONAL</td>
              <td width="10%"> FECHA INICIO</td>
          </tr>
          <tr class="estiloImput">
            <td><label id="lblIdProgramacionTerapiaEditar"></label></td>

            <td>
              <select size="1" id="cmbSedeTerapiaEditar" style="width:95%" title="GD48"
                onfocus="comboDependienteSede('cmbSedeTerapiaEditar','1127')"
                onchange="asignaAtributo('cmbIdEspecialidadTerapiaEditar', '', 0); asignaAtributo('cmbProfesionalesTerapiaEditar', '', 0);cargarTipoCitaSegunEspecialidad('cmbTipoCitaTerapiaEditar', 'cmbIdEspecialidadEditar')">
              </select>
            </td>
            <td>
              <select size="1" id="cmbIdEspecialidadTerapiaEditar" style="width:99%" onfocus="cargarEspecialidadDesdeSede('cmbSedeTerapiaEditar', 'cmbIdEspecialidadTerapiaEditar')"
              onchange="asignaAtributo('cmbTipoCitaTerapiaEditar', '', 0)">	                                        
                <option value=""></option> 
              </select>
            </td>
            <td>
              <select id="cmbTipoCitaTerapiaEditar"  onfocus="cargarCitaSegunEspecialidad(this.id,'cmbIdEspecialidadTerapiaEditar')"  style="width:90%"  >
                <option value=""></option>
              </select>  
            </td>
            <td>
              <select id="cmbProfesionalesTerapiaEditar" style="width:80%" onfocus="cargarProfesionalDesdeEspecialidad('cmbIdEspecialidadTerapiaEditar',this.id)" >
                <option value="" selected="">[ Seleccione ]</option>
              </select>
            </td>


              <td ><input id="txtFechaInicioTerapiaEditar" type="text" /></td> 

             
          </tr>
          <tr class="estiloImputListaEspera">
              <td colspan="9">
                  <input id="BTN_MO" title="BI101" type="button" class="small button blue"
                         value="MODIFICAR"
                         onclick="modificarCRUD('modificarProgramacionTerapia')"
                          />
                  <input id="BTN_MO" title="BI102" type="button" class="small button blue"
                         value="ELIMINAR"
                         onclick="modificarCRUD('eliminarProgramacionTerapia')"
                          />
              </td>
          </tr>
      </table>
  </div>
</div>