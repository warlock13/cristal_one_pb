<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="ARCHIVOS ADJUNTOS" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <form method="post" enctype="multipart/form-data" action="" target="iframeUpload" id="formUpc"
                  name="formUpc">
                <table width="100%">
                    <tr class="titulos">
                        <td width="25%">Paciente</td>
                        <td width="20%">Tipo</td>
                        <td width="20%">Escoja el archivo</td>
                        <td width="35%">Observacion</td>
                    </tr>
                    <tr class="estiloImput">
                        <td>
                            <input type="text" size="70"  maxlength="70" style="width:90%"  id="txtIdBusPaciente"  />                 
                            <img  width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                        </td>
                        <td>
                            <select id="cmbIdTipoArchivosVarios" style="width:95%" >
                                <option value=""></option>
                                <%     resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(514);
                                    ComboVO cmbAV;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbAV = (ComboVO) resultaux.get(k);
                                %>
                                <option value="<%= cmbAV.getId()%>" title="<%= cmbAV.getTitle()%>">
                                    <%= cmbAV.getDescripcion()%></option>
                                    <%}%>                                       
                            </select>
                        </td>
                        <td>
                            <label>
                                <input id="fileUpc" name="fileUpc" type="file"  size="99%" />
                            </label>
                        </td>
                        <td>
                            <textarea id="txtObservacionAdjuntosVarios" style="width:90%"  maxlength="80"  ></textarea>
                        </td>
                    </tr>
                    <tr >
                        <td align="center" colspan="4">
                            <input id="btnSubirAdjuntos" align="center" type="button" title="BT84P." class="small button blue" value="SUBIR DOCUMENTOS" onclick="subirAdjuntos()"/>
                            <input align="center" type="button" class="small button blue" value="REFRESCAR LISTADO" onclick="buscarHistoria('listArchivosAdjuntosVarios')" /></td>
                      </tr>
                </table> 
            </form>
          </td>
        </tr> 
        <tr>
          <td>
            <table  width="100%" align="center">
                                         
                <tr class="titulos">
                    <td colspan="">                                    
                        <div id="divContenedorListaArchivos">
                            <table id="listArchivosAdjuntosVarios"></table>
                         </div>
                    </td>
                </tr>
            </table>	 
          </td>
        </tr>  
      </table>
    </td>
  </tr>      
</table>
<div id="divParaVentanita"></div>
<div id="divParaVentanita" style="display: none;">
    <label id="lblIdCitaAdjuntos"></label>
    <label id="lblIdAdmisionAdjuntos"></label>
    <label id="lblIdFacturaAdjuntos"></label>
    <label id="lblIdEvolucionAdjuntos"></label>
</div>
