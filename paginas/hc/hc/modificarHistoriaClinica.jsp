<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" pageEncoding="UTF-8" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<div id="loader" hidden>
  <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="20px" height="20px"> <label style="font-size: small; font-style: italic;">Cargando elementos</label>
</div>
<table width="1200px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma"> 
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="MODIFICAR HISTORIA CLINICA" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
            <div style="overflow:auto;height:3px; width:100%" id="divContenido">
              <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
            </div><!-- div contenido-->

            <div id="divEditar" style="display:block; width:100%">
              <div id="divBuscar">
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                  <tr class="titulosListaEspera" align="center">                      
                      <td width="10%">TIPO ID</td>
                      <td width="10%">DOCUMENTO</td>
                      <td width="20%">PACIENTE</td>                      
                      <td width="5%"></td>
                  </tr>
                  <tr class="estiloImputListaEspera">                                
                      <td >
                          <select size="1" id="cmbTipoId" style="width:90%"  >                              
                              <%     resultaux.clear();
                                  resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                                  ComboVO cmb105;
                                  for (int k = 0; k < resultaux.size(); k++) {
                                      cmb105 = (ComboVO) resultaux.get(k);
                              %>
                              <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                                  <%= cmb105.getId() + "  " + cmb105.getDescripcion()%></option>
                                  <%}%>						
                          </select>
                      </td> 
                      <td  >
                          <input  type="text" id="txtIdentificacion" style="width: 90%;" onKeyPress="javascript:checkKey2(event);javascript:return soloTelefono(event);"/>	         
                      </td>
                      <td> 
                          <input type="text" size="70"  maxlength="70" style="width:90%"  id="txtIdBusPaciente"  />   
                          <input id="btnActCie" type="hidden" class="small button blue" value="a" title="BNT_ACT_CIE" onClick="modificarCRUD('activarCIE', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"/>                                             
                          <input id="btnDesactCie" type="hidden" class="small button blue" value="d" title="BNT_DSCT_CIE" onClick="modificarCRUD('desactivarCIE', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')" disabled="disabled"/>                                             
                          <!-- <img  width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="//traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27'); $('#divParaVentanita').html('');" src="/clinica/utilidades/imagenes/acciones/buscar.png">  -->
                      </td>                      
                      <td>                     
                          <input id="imgBuscarPaciente"  type="button" class="small button blue"  value="B U S C A R" title="BT86R" onclick="contenedorBuscarPaciente()"  />
                      </td>
                  </tr>
              </table>
              </div>                            
              <body>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer">
                 

                  <tr class="titulos">                    
                    <td width="5%">ID</td>
                    <td width="30%">PACIENTE</td>
                    <td width="10%">EDAD</td>                    
                    <td width="10%">ESTADIO</td>                    
                  </tr>
                  <tr class="subtitulosRep overs">
                    <input type="hidden" id="txtSexoPaciente">                    
                    <td>
                      <label id="lblIdentificacionPaciente" style="size:1PX"></label></td>
                    <td><label id="lblNombrePaciente"></label></td>
                    <td><label id="lblEdadPaciente"></label>&nbsp;Años                      
                    </td>                    
                    <td><label style="font-size:11px; color:#F00" id="lblEstadio"></label></td>                                        
                  </tr>
                </table>
                <table class="titulosCentrados" style="width:100%">
                  <tr>
                    <td>
                      HISTORICOS DE ATENCION
                    </td>
                  </tr>
                </table>
                
                <table width="100%">
                  <tr class="titulosListaEspera overs">		
                    <td width="10%">
                      Cita Actual: <label style="color: red;" id="lblIdCita"></label>
                    </td>
                    <td width="15%">
                      Admisi&oacute;n Actual: <label style="color: red;" id="lblIdAdmisionAgen"></label>
                    </td>
                    <td width="20%">N&uacute;mero de evoluci&oacute;n seleccionada: <label id="lblIdDocumento"></label></td>
                    <td width="15%">
                      Admisi&oacute;n Folio: <label id="lblIdAdmision"></label>
                    </td>
                    <td width="20%">
                      Cita Folio: <label id="lblIdCitaFolio"></label>
                    </td>
                    <td width="20%" align="LEFT">Estado de evoluci&oacute;n: <label id="lblNomEstadoDocumento" style="size:1PX"></label>
                      <label class="parpadea text"  style="color:#F00;font-weight:bold" id="lblSemeforoRiesgo"></label>
                    </td>			
                  </tr>
                </table>
                <br>
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                  <tr class="titulosListaEspera">
                    <td style="width: 33%;">
                      Especialidad
                    </td>
                    <td style="width: 33%;">
                      Profesional
                    </td>
                    <td style="width: 34%;"> 
                      Tipo Folio
                    </td>
                  </tr>
                  <tr class="estiloImputListaEspera">
                    <td>
                      <input type="text" id="txtEspecialidadHistoricos" style="width:90%" onblur="buscarHC('listDocumentosHistoricos')" onkeypress="llamarAutocomIdDescripcionConDato('txtEspecialidadHistoricos', 1417); checkKeyHistoricos(event);" >
                      <img onclick="limpiaAtributo('txtEspecialidadHistoricos'); buscarHC('listDocumentosHistoricos')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">
                    </td>	
                    <td>
                      <input type="text" id="txtIdProfesionalHistoricos" style="width:90%" onblur="buscarHC('listDocumentosHistoricos')" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProfesionalHistoricos', 1416); checkKeyHistoricos(event);" >    
                      <img onclick="limpiaAtributo('txtIdProfesionalHistoricos'); buscarHC('listDocumentosHistoricos')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">            
                    </td>	
                    <td >
                      <select id="cmbTipoFolioFiltro" style="width:90%" 
                                    onfocus="cargarComboGRALCondicion1('cmbPadre', '1', 'cmbTipoFolioFiltro', 3225, valorAtributo('lblIdPaciente'));" onchange="buscarHC('listDocumentosHistoricos')">	                                        
                                    <option value=''>[ TODOS ]</option>
                            </select>
                    </td> 
                  </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                  <tr class="titulosListaEspera">
                    <td style="width: 10%;">Cita</td>
                    <td style="width: 10%;"> Admision</td>                    
                    <td style="width: 10%;">Fecha Desde</td>
                    <td style="width: 15%;">Fecha Hasta</td>
                    <td style="width: 15%;">Estado</td>
                    <td style="width: 15%;">Numero de evolucion</td>
                    <td style="width: 10%;"></td>
                  </tr>
                  <tr class="estiloImputListaEspera">	
                    <td>
                      <select id="cmbCitaHistoricos" style="width:90%" onchange="buscarHC('listDocumentosHistoricos')">
                        <option value="">[ TODO ]</option>
                        <option value="1">Cita Actual</option>
                      </select>
                    </td>
                    <td >
                      <select id="cmbAdmisionHistoricos" style="width:90%" 
                                    onfocus="cargarComboGRALCondicion1('cmbPadre', '1', 'cmbAdmisionHistoricos', 3224, valorAtributo('lblIdPaciente'));" onchange="buscarHC('listDocumentosHistoricos')">                                       
                            </select>	
                    </td>                    
                    <td>
                      <input id="txtFechaDesdeHistoricos" type="date" onchange="buscarHC('listDocumentosHistoricos')" style="width: 90%;">
                    </td>
                    <td>
                      <input id="txtFechaHastaHistoricos" type="date" onchange="buscarHC('listDocumentosHistoricos')" style="width: 90%;">
                    </td>
                    <td > 
                      <select id="cmbEstadoFolioHistoricos" style="width:90%" onchange="buscarHC('listDocumentosHistoricos')">
                        <option value="">[ TODO ]</option>
                        <option value="0">Borrador</option>
                        <option value="1">Firmado</option>
                      </select>
                    </td>
                    <td>
                      <input type="number" id="txtIdNumeroEvolucion" style="width:80%" onblur="buscarHC('listDocumentosHistoricos')" onkeypress="checkKeyHistoricos(event);" >    
                      <img onclick="limpiaAtributo('txtIdNumeroEvolucion'); buscarHC('listDocumentosHistoricos')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">            
                    </td>	
                    <td>
                            <input type="button" style ="width:90%" class="small button blue" value="LIMPIAR" onclick="limpiaAtributo('txtEspecialidadHistoricos'); limpiaAtributo('txtIdProfesionalHistoricos');
                      limpiaAtributo('cmbTipoFolioFiltro'); limpiaAtributo('cmbCitaHistoricos'); limpiaAtributo('cmbAdmisionHistoricos'); limpiaAtributo('txtIdNumeroEvolucion');
                      limpiaAtributo('txtIdNumeroAdmision'); limpiaAtributo('txtFechaDesdeHistoricos'); limpiaAtributo('txtFechaHastaHistoricos'); 
                      limpiaAtributo('cmbEstadoFolioHistoricos'); buscarHC('listDocumentosHistoricos') "/>                                     
                        </td> 
                  </tr>                                    
                  <tr>
                    <td colspan="7">
                      <table width="100%" border="1" cellpadding="0" cellspacing="0" align="center" >
                        <tr class="titulos">
                          <td style="width:99%">
                            <div style="width: 100%; height: 150px; overflow-y: scroll;">							
                                <table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0" align="center">                                  
                                </table>																									
                            </div>													
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                
                </table>
                
                <input id="txtHistoricos" hidden value="HC"/>

                <div id="divMedidasSignosVitales" style="display: none;">
                  <table width="100%">
                    <tr>
                      <td class="titulos" colspan="11">
                        MEDIDAS Y SIGNOS VITALES
                      </td>
                    </tr>
                    <tr class="titulosListaEspera">
                      <td width="9%">PESO(KG) <label style="color: red;">*</label></td>
                      <td width="9%">TALLA(CM) <label style="color: red;">*</label></td>
                      <td width="9%">IMC</td>                    
                      <td width="9%">PRESION CAPILAR</td> 
                      <td width="9%">FRECUENCIA RESPIRATORIA</td>    
                      <td width="9%">TENSION ARTERIAL SISTOLICA <label style="color: red;">*</label></td>
                      <td width="9%">TENSION ARTERIAL DIASTOLICA <label style="color: red;">*</label></td>
                      <td width="9%">TENSION ARTERIAL MEDIA</td>
                      <td width="9%">TEMPERATURA</td>
                      <td width="9%">SATURACION OXIGENO</td>
                      <td width="9%">PULSO</td>                                                                     
                    </tr>
                    <tr class="estiloImput">
                      <td width="9%"><input id="txtc1" style="width: 50%;" type="number" step="0.01" min="0" max="200" onkeyup="calcularIMC()" onblur="guardarDatosPlantilla('daan', 'c1',  this.value);"></td>
                      <td width="9%"><input id="txtc2" style="width: 50%;" type="number" min="0" max="250" onkeyup="calcularIMC()" onblur="guardarDatosPlantilla('daan', 'c2',  this.value);"></td>
                      <td width="9%"><input id="txtc3" style="width: 50%;" type="number" step="0.01" min="0" max="200" disabled></td>                    
                      <td width="9%"><input id="txtc8" style="width: 50%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c8',  this.value);"></td> 
                      <td width="9%"><input id="txtc11" style="width: 50%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c11',  this.value);"></td>
                      <td width="9%"><input id="txtc4" style="width: 50%;" type="number" min="0" max="300" onkeyup="calcularTAM()" onblur="guardarDatosPlantilla('daan', 'c4',  this.value);"></td>
                      <td width="9%"><input id="txtc5" style="width: 50%;" type="number" min="0" max="300" onkeyup="calcularTAM()" onblur="guardarDatosPlantilla('daan', 'c5',  this.value);"></td>
                      <td width="9%"><input id="txtc14" style="width: 50%;" type="number" step="0.01" min="0" max="200" disabled></td>
                      <td width="9%"><input id="txtc7" style="width: 50%;" type="number" step="0.01" min="0" max="40" onblur="guardarDatosPlantilla('daan', 'c7',  this.value);"></td>
                      <td width="9%"><input id="txtc13" style="width: 50%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c13',  this.value);"></td>
                      <td width="9%"><input id="txtc6" style="width: 50%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c6',  this.value);"></td>
                    </tr>                
                  </table> 
                </div>
                
                <div id="divPadreDeFolios" style="display:BLOCK">
                  
                  <div id="divFormularioDatosGeneraleshc"  title="divFormularioDatosGeneraleshc"  style="display:none">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="DATOS GENERALES HISTORIA CLINICA" />
                      <jsp:param name="idDiv" value="divDatosGeneraleshc" />
                      <jsp:param name="pagina" value="contenidoFormularioDatosGeneraleshc.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="traerInfoGeneralHc()" />
                    </jsp:include>
                  </div>

                  <div id="divFormularioDatosGeneralesTerapia"  title="divFormularioDatosGeneralesTerapia"  style="display:none">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="DATOS GENERALES HISTORIA CLINICA" />
                      <jsp:param name="idDiv" value="divDatosGeneralesTerapia" />
                      <jsp:param name="pagina" value="contenidoFormularioDatosGeneralesTerapia.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="traerInfoGeneralHcTerapia()" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionAntecedentes" title="divAcordionAntecedentes"  style="display:none"> 
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Antecedentes" />
                      <jsp:param name="idDiv" value="divAntecedentes" />
                      <jsp:param name="pagina" value="antecedentes.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones"
                        value="buscarHC('listAntecedentes' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionFolioEspecialidad" style="display:BLOCK" 
                  onclick="validarTabsGeneroEspecialidad(divAcordionFolioEspecialidad);">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="OTROS ANTECEDENTES" />
                      <jsp:param name="idDiv" value="divFolioEspecialidad" />
                      <jsp:param name="pagina" value="../../encuesta/correccionHC/contenedorFolioEspecialidad.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="tabsContenidoEncuesta('folioEspecialidad')" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionRevisionPorSistemas" style="display:none">
                      <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                        <jsp:param name="titulo" value="REVISION POR SISTEMAS"/>
                        <jsp:param name="idDiv" value="divFolioPlantillaR"/>
                        <jsp:param name="pagina" value="../../encuesta/contenedorFolioPlantillaR.jsp" />
                        <jsp:param name="display" value="NONE" />
                        <jsp:param name="funciones" value="tabsContenidoEncuesta('revisionSistemas')" />
                      </jsp:include>
                    </div>

                  <div id="divAcordionExamenFisico" style="display:none;">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Mediciones y signos vitales" />
                      <jsp:param name="idDiv" value="divExamenFisico" />
                      <jsp:param name="pagina" value="examenFisico.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones"
                        value="buscarHC('listSignosVitales' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                    </jsp:include>
                  </div>                  

                  <div id="divEnfActual" style="display:none;">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="TAMIZAJES Y ENCUESTAS" />
                      <jsp:param name="idDiv" value="divEncuesta" />
                      <jsp:param name="pagina" value="../../encuesta/contenedorEncuesta.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="contenedorEncuesta()"/>  
                    </jsp:include>
                  </div>

                  <div id="divAcordionOrdenes" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Resultados a procedimientos" />
                      <jsp:param name="idDiv" value="divOrdenes" />
                      <jsp:param name="pagina" value="contenidoOrdenes.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarTableOrdenes()" />
                    </jsp:include>
                  </div>
                  
                  <div id="divAcordionLabNefro" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Laboratorios Nefroproteccion" />
                      <jsp:param name="idDiv" value="divLabNefro" />
                      <jsp:param name="pagina" value="contenidoLaboratoriosNefro.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="buscarHC('listLabActual'); setTimeout(() => {buscarHC('listLabAnteriores')}, 300);" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionFolioPlantilla" style="display:none"
                  onclick="validarTabsEvaluacionGeneral();">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="EVALUACION GENERAL"/>
                      <jsp:param name="idDiv" value="divFolioPlantilla"/>
                      <jsp:param name="pagina" value="../../encuesta/contenedorFolioPlantilla.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="tabsContenidoEncuesta('desdeFolio')" />
                    </jsp:include>
                  </div>                           

                  <div id="divAcordionEncuesta" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="ENCUESTAS Y TAMIZAJES" />
                      <jsp:param name="idDiv" value="divEncuesta" />
                      <jsp:param name="pagina" value="../../encuesta/contenedorEncuesta.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="contenedorEncuesta()" /> 
                    </jsp:include>
                  </div>

                   

                  <div id="divAcordionDiagnosticos" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Diagn&oacute;sticos"/>
                      <jsp:param name="idDiv" value="divDiagnosticos"/>
                      <jsp:param name="pagina" value="diagnosticos.jsp"/>
                      <jsp:param name="display" value="NONE"/>
                      <jsp:param name="funciones"
                        value="buscarHC('listDiagnosticos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionAnalisisPlan" style="display:none">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="ANALISIS Y PLAN"/>
                      <jsp:param name="idDiv" value="divFolioPlantillaAyP"/>
                      <jsp:param name="pagina" value="../../encuesta/contenedorFolioPlantillaAyP.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="tabsContenidoEncuesta('analisisyplan')" />
                    </jsp:include>
                  </div>
 
                  <div id="divAcordionProcedimientos" onclick="validacionTabsFolios();" style="display:BLOCK" >
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Conducta y tratamiento" />
                      <jsp:param name="idDiv" value="divProcedimientos" />
                      <jsp:param name="pagina" value="contenidoPlanTratamiento.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarTablePlanTratamiento();" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionPlanT" onclick="validacionTabsFolios();" style="display:BLOCK" >
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Plan de Tratamiento" />
                      <jsp:param name="idDiv" value="divPlanTratamiento" />
                      <jsp:param name="pagina" value="contenidoPlanTratamientoT.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarTablePlanTratamientoT();" />
                    </jsp:include>
                  </div>

                  <div id="divAdministrativa" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Administrativa" />
                      <jsp:param name="idDiv" value="divAdministrativos" />
                      <jsp:param name="pagina" value="contenidoAdministrativa.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarAdministrativo()" />
                    </jsp:include>
                  </div>

                  <div id="divFactoresDeRiesgo" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="AGENDAMIENTO" />
                      <jsp:param name="idDiv" value="divFactoresRiesgo" />
                      <jsp:param name="pagina" value="contenidoFactoresDeRiesgo.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value=" " />
                    </jsp:include>
                  </div>

                  <div id="divAcordionHojaRuta" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="HOJA DE RUTA" />
                      <jsp:param name="idDiv" value="divHojaRuta" />
                      <jsp:param name="pagina" value="../../planAtencion/contenedorHojaRuta.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="contenedorRutaPacienteHC()" />
                    </jsp:include>
                  </div>                  
                
                  <div id="divAcordionAgendamientoHC" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="AGENDAMIENTO" />
                      <jsp:param name="idDiv" value="divAgendamiento" />
                      <jsp:param name="pagina" value="agendamientohc.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="" />
                    </jsp:include> 
                  </div>

                  <!-- adm de medicamentos -->
                  <div id="divAcordionAdmMedicamentos" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Administracion de Medicamentos" />
                      <jsp:param name="idDiv" value="divAdmMedicamentos" />
                      <jsp:param name="pagina" value="administracionMedicamentos.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones"
                        value="buscarHC('listAdministracionMedicacion' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                    </jsp:include>
                  </div>

                  <!-- HOJA DE GASTOS -->
                  <div id="divAcordionHojaGastos" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Hoja de Gastos" />
                      <jsp:param name="idDiv" value="divHojaGastos" />
                      <jsp:param name="pagina" value="contenidoHojaDeGastos.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarTableHojaDeGastos()" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionAdjuntos" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Archivos adjuntos" />
                      <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                      <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
                    </jsp:include>
                  </div>
              
                  <div id="divAcordionContenidos" style="display:BLOCK">
                    <jsp:include page="divsAcordeonFormulario.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="" />
                      <jsp:param name="idDiv" value="divContenidos" />
                      <jsp:param name="pagina" value="../../contenidos.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="contenidoDoc()" />
                    </jsp:include>
                  </div>                  

                  <div id="divAcordionAntecedentesFarmacologicos" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Antecedentes Farmacologicos" />
                      <jsp:param name="idDiv" value="divAntecedentesFarmacologicos" />
                      <jsp:param name="pagina" value="antecedentesFarmacologicos.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones"
                        value="buscarHC('listAntecedentesFarmacologicos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionExamenFisicoDescripcion" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Examen Fisico" />
                      <jsp:param name="idDiv" value="divExamenFisicoDescripcion" />
                      <jsp:param name="pagina" value="examenFisicoDescripcion.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="guardarYtraerDatoAlListado('consultaExamenFisico')" />

                    </jsp:include>
                  </div>

                  <div id="divAcordionProcedimientosCir" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Procedimientos Programados" />
                      <jsp:param name="idDiv" value="divProcedimientosCir" />
                      <jsp:param name="pagina" value="contenidoProcedimientosProgramacion.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarTableProcedimientosProgramados()" />
                    </jsp:include>
                  </div>
                  
                  <!-- MONITORIZACION HEMODINAMICA DE HEMODIALISIS AGUDA -->
                  <div id="divAcordionSignosVitales" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Signos Vitales" />
                      <jsp:param name="idDiv" value="divSignosVitales" />
                      <jsp:param name="pagina" value="signosVitales.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones"
                        value="buscarHC('listMonitorizacion' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                    </jsp:include>
                  </div>
                  <!-- FIN  -->

                  <div id="divAcordionOdonto" style="display:BLOCK">
                    <jsp:include page="divsAcordeonFormulario.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="ODONTOGRAMA Y SEGUIMIENTO" />
                      <jsp:param name="idDiv" value="divOdonto" />
                      <jsp:param name="pagina" value="../saludOral/contenidoAcordionesOdontograma.jsp" />
                      <jsp:param name="display" value="NONE" />
                      <jsp:param name="funciones" value="cargarAcordionesOdontograma()" />
                    </jsp:include>
                  </div>

                  <div id="divAcordionIndicaciones" style="display:BLOCK">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Otras Indicaciones " />
                      <jsp:param name="idDiv" value="divIndicaciones" />
                      <jsp:param name="pagina" value="indicaciones.jsp" />
                      <jsp:param name="display" value="NONE" />
                    </jsp:include>
                  </div>

                  <div id="divVideollamada" style="display:NONE">
                    <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                      <jsp:param name="titulo" value="Videollamada " />
                      <jsp:param name="idDiv" value="divVideo" />
                      <jsp:param name="pagina" value="videollamada.jsp" />
                      <jsp:param name="display" value="NONE" />
                    </jsp:include>
                  </div>

                </div><!-- divPadreDeFolios-->
            </div><!-- divEditar-->
            </div>
          </td>
        </tr>
        <tr> 
          <td align="center" colspan="4">
            <!-- <script src='https://code.jquery.com/jquery-1.12.4.min.js'></script>
            <iframe  sandbox="allow-scripts allow-popups" style='display:none' id='frame' width='1000' height='700' frameborder='1'></iframe>
            <br> 
            <input type="button" class="small button blue" title="BVideollamada" value="vIDEOLLAMADA"
              onClick="teleconsulta()" />-->
            <input type="button" class="small button blue" title="BT7QR" value="VISTA PREVIA"
              onClick="imprimirHCPdf()" />
            <% if(beanSession.usuario.preguntarMenu("BTFINF")){%>
              <!--<input class="small button blue" type="button" id="btnFirmarFolio"
                onClick="firmarFolio()" value="FIRMAR">--!>
              <input class="small button blue" type="button" id="btnFirmarFolio"
                onClick="auditarFolioModificar()" value="AUDITAR FOLIO"/>          
            <%}%>      
              
         </td>  
       </tr>	
      
	   <% if(beanSession.usuario.preguntarMenu("b611D") || beanSession.usuario.preguntarMenu("HC")){%>
        <tr>
          <td align="center" colspan="4">&nbsp;
          </td>
        </tr>
        <tr>
          <td align="center" colspan="4">
            <TABLE width="100%">
              <TR class="titulos">
                <TD width="20%">ACCION AL FOLIO</TD>
                <td style="display: none;">MOTIVOS APERTURA</td>
                <TD width="60%" align="center" colspan="2">JUSTIFICACION DE LA ACCION</TD>
                <TD width="20%" align="center" colspan="2"></TD>
              </TR>
              <TR class="estiloImput">
                <TD width="20%">
                  <select id="cmbAccionFolio" style="width:70%" title="32" onchange="mostrarMotivosAbrir()">                    
                    <!--<option value="0">ABRIR</option>-->                    
                    <option value="3">CREAR NOTA ACLARATORIA</option>
                    <option value="4">CREAR EVOLUCION NOTA DE ENFERMERIA</option>
                    <option value="11">CREAR NOTA EVOLUCION NEFROPROTECCION</option>
                    <option value="8">CREAR FORMATO DE ENMIENDA</option>
                    <option value="9">CREAR INFORME FINAL DE TERAPIAS</option>
                    <option value="2" title="PERMITE CLONAR LA HISTORIA CLINICA">CLONAR HISTORIA CLINICA</option>
                    <option value="1">FIRMAR NOTA ACLARATORIA/FORMATO DE ENMIENDA</option>
                    <option value="5">ELIMINAR FOLIO EN ESTADO BORRADOR</option>
                    <!--<option value="7">ANULAR</option>-->
                  </select>
                </TD>
                <td width="20%" style="display: none;">
                  <select style="width:70%" id="cmbMotivosAbrir">
                  </select>
                </td>
                <TD width="60%" align="center" colspan="2">
                  <input type="text" id="txtMotivoAccion" size="100" maxlength="100" style="width:90%" />
                </TD>
                <TD width="20%" align="center" colspan="1">
                  <button id="btnVideo" onclick="teleconsulta()">.</button>
                  <input id="btnabrirDireMedica" type="button" class="small button blue"
                    value="EJECUTAR ACCION" title="BT00I- DESDE DIRECCION MEDICA"
                    onClick="verificarNotificacionAntesDeGuardar('accionesAlEstadoFolio')" />
                </TD>
                <TD>.
                  <!--  <input id="btnabrirDireMedica"  type="button" class="small button blue" value="CLONAR" title="BT00I" onClick="modificarCRUD('clonarFolio','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"/> 
                 -->
                </TD>
              </TR>
            </TABLE>
          </td>
        </tr>
        <%} %>

      </table>

    </td>
  </tr>
  <tr>
    <td>
      <label style="font-size:10px" id="lblIdEspecialidad"></label>
      <label style="font-size:10px" id="lblNombreEspecialidad"></label>&nbsp;&nbsp;&nbsp;
      <label style="font-size:10px" id="lblTipoDocumento"></label>
      <label style="font-size:10px" id="lblDescripcionTipoDocumento"></label>&nbsp;&nbsp;&nbsp;
      <label style="font-size:10px" id="lblGeneroPaciente" hidden></label>
      <label style="font-size:10px" id="lblEdadPaciente" hidden></label>
      <label style="font-size:10px" id="lblMesesEdadPaciente" ></label>
      <!-- <img src="" alt=""> -->
      <H6 align="right">
        Id Auxiliar=<label style="font-size:9px" id="lblIdAuxiliar">98400407</label>
        &nbsp;&nbsp;&nbsp;&nbsp;
      </H6>
    </td>
  </tr>

</table>

<!-- <div id="divValidaciones" hidden> -->
<div id="divValidaciones" hidden>
  <table id="listGrillaFormulas" class="scroll"></table>
  <table id="listGrillaFormulasE" class="scroll"></table>
  <table id="listGrillaValoresValidaciones" class="scroll"></table>
  <table id="listGrillaValoresValidacionesE" class="scroll"></table>
  <table id="listGrillaValidaciones" class="scroll"></table>
  <table id="listGrillaValidacionesE" class="scroll"></table>
</div>

<div id="divFichaPaciente"
  style="position:absolute; display:none; background-color:#E2E1A5; top:210px; left:200px; width:800px; height:100px; z-index:999">
  <table width="100%" border="1" class="fondoTabla">
    <tr class="estiloImput">
      <td colspan="3" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
          onClick="ocultar('divFichaPaciente')" /></td>
    <tr>
    <tr class="titulos">
      <td width="10%" colspan="3">
        <jsp:include page="divsSinAcordeon.jsp" flush="FALSE">
          <jsp:param name="titulo" value="Informacion Administrativa Paciente" />
          <jsp:param name="pagina" value="../fichaPaciente.jsp" />
          <jsp:param name="display" value="block" />
        </jsp:include>
      </td>
    </tr>
  </table>
  <H6 align="right">
    <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>" />
    <input type="hidden" id="txtLogin" value="<%=beanSession.usuario.getLogin()%>" />
    <input type="hidden" id="txtPrincipalHC" value="OK" />
    IdPaciente=<label id="lblIdPaciente" style="size:1PX"></label>
  </H6>
</div>

</body>


<div id="divVentanitaPdf" style="display:none; z-index:2050; top:1px; left:190px;">
  <div id="idDivTransparencia" class="transParencia"
    style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div id="idDivTransparencia2" style="z-index:2055; position:absolute; top:5px; left:15px; width:100%">
    <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaPdf')" /></td>
        <td>&nbsp;</td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaPdf')" /></td>
      </tr>
      <tr class="estiloImput">
        <td colspan="3">
          <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td>
                <div id="divParaPaginaVistaPrevia"></div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>

<input type="hidden" id="txtIdBusPaciente" value="" />
<input type="hidden" id="txtIdProfesionalElaboro" value="" />


<div id="divAuxiliar" style="display:none; z-index:2000; top:111px; left:-211px;">
  <div class="transParencia" style="z-index:2001; background:#999; filter:alpha(opacity=5);float:left;">
  </div>
  <div style="z-index:2002; position:absolute; top:200px; left:60px; width:95%">
    <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" disabled="disabled"
            onClick="ocultar('divAuxiliar')" /></td>
        <td>&nbsp</td>
        <td>&nbsp</td>
        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" disabled="disabled"
            onClick="ocultar('divAuxiliar')" /></td>
      <tr>
      <tr>
        <td class="titulos" colspan="4">DIGITE CONTRASEÑA DEL AUXILIAR DE CONSULTORIO</td>
      </tr>
      <tr>
        <td colspan="4">
          <center>
            <input id="txtContrasenaAdm" value="CRISTAL" type="password" size="32" maxlength="30"
              onChange="verificarContrasenaAxiliar(this.value)"  />
          </center>
        </td>
      </tr>
    </table>
  </div>
</div>


<div id="divParaVentanita"></div>
<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />




<div id="divVentanitaODONTOGRAMA" style="display:none; z-index:9997; top:-51px; left:50px; width: 1200px">

  <div id="ventanitaHija" style="z-index:9999; position:absolute; top:0px; left:60px; width:1200px;">
    <table width="1200px" align="center" class="fondoTablaAmarillo">
      <tr>
        <td width="100%" align="center" colspan="2">

          <div id="divAcordionTratamientoPorDiente" style="display:BLOCK">
            <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
              <jsp:param name="titulo" value="Descripcion del Tratamiento" />
              <jsp:param name="idDiv" value="divTratamientoPorDiente" />
              <jsp:param name="pagina" value="../saludOral/contenidoTratamientoPorDiente.jsp" />
              <jsp:param name="display" value="BLOCK" />
              <jsp:param name="funciones" value="cargarTratamientoPorDiente()" />
            </jsp:include>
          </div>
          <div id="divAcordionTratamientoSeguimiento" style="display:BLOCK">
            <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
              <jsp:param name="titulo" value="Seguimiento" />
              <jsp:param name="idDiv" value="divTratamientoSeguimiento" />
              <jsp:param name="pagina" value="../saludOral/contenidoOdontograma.jsp" />
              <jsp:param name="display" value="NONE" />
              <jsp:param name="funciones" value="ocultar('divControlDePlaca');cargarOdontograma()" />
            </jsp:include>
          </div>
          <div id="divAcordionTratamientoInicial" style="display:BLOCK">
            <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
              <jsp:param name="titulo" value="Odontograma inicial" />
              <jsp:param name="idDiv" value="divTratamientoInicial" />
              <jsp:param name="pagina" value="../saludOral/contenidoOdontogramaInicial.jsp" />
              <jsp:param name="display" value="NONE" />
              <jsp:param name="funciones" value="cargarOdontogramaInicial()" />
            </jsp:include>
          </div>
          <div id="divAcordionIndiceDePlaca" style="display:BLOCK">
            <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
              <jsp:param name="titulo" value="Indice De Placa" />
              <jsp:param name="idDiv" value="divControlDePlaca" />
              <jsp:param name="pagina" value="../saludOral/contenidoControlDePlaca.jsp" />
              <jsp:param name="display" value="NONE" />
              <jsp:param name="funciones" value="cargarControlDePlaca()" />
            </jsp:include>
          </div>

        </td>
        <td align="right" valign="top">
          <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" title="Cerrar"
            onclick="javascript:ocultar('divVentanitaODONTOGRAMA')" width="15" height="15">
        </td>
      </tr>
    </table>

  </div> 
</div>

<div  id="divNotificacionAlertFolioPlantilla" align="center" title="MENSAJE" style="z-index:3000; top:0px; left:0px; position:fixed; display:none;" >
  <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
  </div> 
  <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%;">     
      <table bgcolor="#FFFFFF" align="center"  width="300px" height="100px" id="tablaNotificacionesAlert" style="border: 1px solid #000;" >
          <tr>
              <td bgcolor="#7CD9FC" align="center" background="/clinica/utilidades/imagenes/menu/six_0_.gif" >
                  <img align="left" height="20" width="20" src="/clinica/utilidades/imagenes/acciones/alertaIntermitente.gif"/>
                  <b>ALERTA</b>
              </td>      
          </tr>	   
          <tr>
              <td>
                  <i><label style="font-family: Helvetica;" id="lblTotNotifVentanillaAlert">HA OCURRIDO UN PROBLEMA AL INTENTAR GUARDAR LA INFORMACI&Oacute;N.<br><br>POR FAVOR VERIFIQUE SU CONEXI&Oacute;N A INTERNET E INTENTE NUEVAMENTE.<br>SI EL PROBLEMA PERSISTE GUARDE EXTERNAMENTE LOS CAMPOS CON ERROR Y COMUNIQUESE CON SOPORTE TECNICO.</label></i>
                  <i><label style="font-family: Helvetica; display:none;" class="status_error"></label></i>
              </td>      
          </tr>
          <tr>
              <td align="center">  
                  <TABLE id="idTableBotonNotificaAlert" width="50%" border="1">
                      <TR>
                          <TD align="center">
                            <i><input style="font-family: Helvetica;" id="alertRepBoton" type="button" class="small button blue" value="VUELVA A INTENTAR" title="BT86E" onclick="ocultar('divNotificacionAlertFolioPlantilla')" ></i>
                          </TD>
                      </TR>
                  </TABLE>
              </td>        	               
          </tr>            
      </table>
      <br/>
  </div>   
</div>

<div id="divContenedorParalelo" style="display: none; top: -1px; left: 1150px; width: 350px;" class="contenedorParalelo">
  <div id="divParalelo" class="Paralelo">
    <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divContenedorParalelo')" />
  </div>
  <table width="100%" border="1">
    <tr class="titulosListaEspera">
      <td style="width: 100%;" colspan="2">
        SIGNOS VITALES
      </td>
    </tr>
    <tr class="estiloImput">
      <td style="width: 33%;">
        Peso(KG): <label id="lblPesoParalelo"></label>
      </td>
      <td style="width: 33%;">
        Talla(CM): <label id="lblTallaParalelo"></label>
      </td>
    </tr>
    <tr class="estiloImput">
      <td style="width: 33%;">
        Tension Arterial Sistolica: <label id="lblTasParalelo"></label>
      </td>
      <td style="width: 33%;">
        Tension Arterial Diastolica: <label id="lblTadParalelo"></label>
      </td>
    </tr>
    <tr class="estiloImput">
      <td style="width: 33%;">
          TFGe: <label id="lblTfgParalelo"></label>
      </td>
      <td style="width: 33%;">
          Estadio: <label id="lblEstadioParalelo"></label>
      </td>      
  </tr>
    <tr class="titulosListaEspera">
      <td style="width: 100%;" colspan="2">
        MOTIVO CONSULTA
      </td>
    </tr>
    <tr class="estiloImput">
      <td style="width: 100%;" colspan="2">
        <textarea id="txtMotivoConsultaParalelo" style="width: 100%; height: 50px;"></textarea>
      </td>
    </tr>
    <tr class="titulosListaEspera">
      <td style="width: 100%;" colspan="2">
        ENFERMEDAD ACTUAL
      </td>
    </tr>
    <tr class="estiloImput">
      <td style="width: 100%;" colspan="2">
        <textarea id="txtEnfermedadActualParalelo" style="width: 100%; height: 280px;"></textarea>
      </td>
    </tr>
    <tr class="titulos">
      <td colspan="2">
        <table id="listDiagnosticosParalelo"></table>
      </td>
    </tr>
    <tr class="titulosListaEspera">
      <td style="width: 100%;" colspan="2">
        ANALISIS Y PLAN
      </td>
    </tr>
    <tr class="estiloImput">
      <td style="width: 100%;" colspan="2">
        <textarea id="txtAnalisisPlanParalelo" style="width: 100%; height: 280px;"></textarea>
      </td>
    </tr>
    <tr class="titulos">
      <td colspan="2">
        <table id="listProcedimientosParalelo"></table>
      </td>
    </tr>
    <tr class="titulos">
      <td colspan="2">
        <table id="listMedicamentosParalelo"></table>
      </td>
    </tr>
    <tr class="titulos">
      <td colspan="2">
        <table id="listParaleroLaboratorios"></table>
      </td>
    </tr>
  </table>
</div>

<div id="divInfoPacienteVentana" style="display: none; top: 450px; left:10px; width: 150px; position: fixed;">
  <table style="background-color: #ffffff;">
    <tr>
      <td>
        <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divInfoPacienteVentana')" />
      </td>
    </tr>
    <tr>
      <td>
        <label id="lblCedulaPacienteVentanita" class="" style="color:white;"></label>
      </td>
    </tr>
    <tr>
      <td>
        <label id="lblNombrePacienteVentanita" class="" style="color:white;"></label>
      </td>
    </tr>
    <tr>
      <td>
        <label id="lblEdadPacienteVentanita" class="" style="color:white;"></label><label class="" style="color:white;"> Años</label>
      </td>
    </tr>
    <tr>
      <td>
        <label id="lblCreatininaVentanita" class="" style="color:white;"></label>
      </td>
    </tr>
    <tr>
      <td>
        <label id="lblTFGVentanita" class="" style="color:white;"></label>
      </td>
    </tr>
    <tr>
      <td>
        <label id="lblEstadioVentanita" class="" style="color:white;"></label>
      </td>      
    </tr>
    <tr>
      <td>
        <label id="lblDiagnosticosVentanita" class="" style="color:white;"></label>
      </td>      
    </tr>
  </table>
</div>

<div id="divFirmarFolio" title="MENSAJE" style="z-index: 3000; top: 0px; left: 0px; position: fixed; visibility: visible; display: none;" class="cssjas" align="center">
    <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
    </div> 
    <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%; ">     
        <table id="tablaNotificacionesAlert" style="border: 1px solid #000;" width="300px" height="100px" bgcolor="#FFFFFF" align="center">
            <tr>
                <td bgcolor="#7CD9FC" background="../utilidades/imagenes/menu/six_0_.gif" align="center">
                    <b id="tittleFirmarFolio"></b>
                    <img id="imgCerrarAlerta" style="padding-right: 5px;" src="../utilidades/imagenes/acciones/cerrar2.png" onclick="$('#divFirmarFolio').hide()" width="16" height="16" align="right">

                </td>      
            </tr>	   
            <tr>
                <td>
                    <i><label style="font-family: Helvetica;" id="lblFirmarFolio"></label></i>
                </td>      
            </tr>
            <tr>
                <td align="center">  
                    <input class="small button blue" type="button"  onClick="$('#divFirmarFolio').hide()" value="SALIR">
                    <input class="small button blue" type="button" id="btnFirmarFolioAlerta" onClick="verificarNotificacionAntesDeGuardar('verificarDiagnosticoHipertension');
                                $('#divFirmarFolio').hide()" value="CONTINUAR">
                </td>
            </tr>            
        </table>
    </div>   
</div>

<div  id="loaderHcClonar" align="center" title="MENSAJE" style="z-index:3000; top:0px; left:0px; position:fixed; display:none;" >
  <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
  </div> 
  <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%;">     
      <table bgcolor="#FFFFFF" align="center"  width="300px" height="100px" id="tablaNotificacionesAlert" style="border: 1px solid #000;" >
          <tr>
              <td bgcolor="#7CD9FC" align="center" background="/clinica/utilidades/imagenes/menu/six_0_.gif" >
                  <b>ALERTA</b>
              </td>      
          </tr>	   
          <tr>
              <td>
                <i><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="100px" height="100px"></i>
              </td>
              <td>
                  <i><label style="font-family: Helvetica;" id="lblTotNotifVentanillaAlert">Por favor, espere unos segundos hasta completar la acci&oacute;n al folio</label></i>
              </td>      
          </tr>
          <tr>
              <td align="center">  
                  <TABLE id="idTableBotonNotificaAlert" width="50%" border="1">
                  </TABLE>
              </td>        	               
          </tr>            
      </table>
      <br/>
  </div>   
</div>