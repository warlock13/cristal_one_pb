<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<div id="loader_datos_generales_terapia" style="display: flex; justify-content: center; align-items: center; height: 150px; background-color: white;" hidden>
	<table>
	  <tr>
		<td align="center"><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="60px" height="60px"></td>
	  </tr>
	  <tr>
		<td><label style="font-size: small; font-style: italic;">Cargando datos. Por favor espere...</label></td>
	  </tr>
	</table>
</div>

<div id="divMotivoEnfermedad">
	<table width="100%" cellpadding="0" cellspacing="0" align="center">
		<tr class="titulos">
			<td>Motivo de la Consulta:</td>
			<td>Cronologia de la Patologia</td>
			<td style="display: none;">Paraclinicos</td>			
		</tr>
		<tr class="estiloImput">
			<td width="30%">
				<textarea rows="5" id="txtMotivoConsulta" size="4000" maxlength="4000" style="width:95%; height: 100%;"
					 onkeydown="onKeyDownHandler(event,this.id)" onblur="v28(this.value,this.id); modificarCRUD('motivoConsultaTep'); "> </textarea>
			</td>
			<td width="30%">
				<textarea rows="5" id="txtCronologia" size="4000" maxlength="4000" style="width:95%; height: 100%;"
					 onkeydown="onKeyDownHandler(event,this.id)" onblur="v28(this.value,this.id); modificarCRUD('cronologiaTep'); "> </textarea>
			</td>
			<td width="30%" style="display: none;">
				<textarea rows="5" id="txtParaclinico" size="4000" maxlength="4000" style="width:95%"
					 onkeydown="onKeyDownHandler(event,this.id)" onblur="v28(this.value,this.id); modificarCRUD('paraclinicoTep'); "> </textarea>				
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<hr>
			</td>
		</tr>
		<tr style="background-color: whitesmoke;">
			<td colspan="3">
				<table width="100%">
					<tr class="titulos">
						<th width="30%">Diagn&oacute;stico M&eacute;dico</th>
						<th width="30%">M&eacute;dico Remitente</th>
						<th width="30%">Estado de Conciencia</th> 
					</tr>
					<tr class="estiloImput">
						<td>
							<input type="text" value="" id="txtIdDxIngreso" style="width: 90%;" readonly>
							<img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
								onclick="traerVentanitaFuncionesHc(this.id, 'actualizarDxIngresoHC', 'divParaVentanita','txtIdDxIngreso','5')"
								src="/clinica/utilidades/imagenes/acciones/buscar.png">
						</td>
						<td>
							<input type="text"  value="" id="txtMedicoRemitente"  style="width:90%"  size="200"  maxlength="200"   onblur="modificarCRUD('medRemiTep')" onkeypress="return validarKey(event,this.id)" >							
						</td>
						<td>
							<select id="cmbEstadoConciencia" style="width: 95%;"
								onblur="modificarCRUD('estadoConcienciaTep')">
								<option value=""></option>
								<option value="ALERTA">ALERTA</option>
								<option value="SOMNOLIENTO">SOMNOLIENTO</option>
								<option value="CONCIENTE">CONCIENTE</option>
								<option value="CONFUSO">CONFUSO</option>
								<option value="ESTUPOROSO">ESTUPOROSO</option>
								<option value="COMA">COMA</option>
								<option value="VEGETATIVO">VEGETATIVO</option>
								<option value="NO APLICA">NO APLICA</option>                                    
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<!--      <input type="hidden"  id="txtVistaPrevia" value="NO" width="70px" /> -->
<input type="hidden" id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden" id="txtTipoDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblTituloDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblIdAdmisionVistaPrevia" value="" />
<input type="hidden" id="lblEstadoDocumentoVistaPrevia" value="" />