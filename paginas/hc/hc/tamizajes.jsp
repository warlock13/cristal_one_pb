<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="1300" id="idTamizajes" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div align="center" id="tituloForma">
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="TAMIZAJES" />
                </jsp:include>
            </div>
        </td>
    </tr>

    <tr>
        <td valign="top">
            <table width="100%" class="fondoTabla" cellpadding="0" cellspacing="0">                
                <tr class="titulos">
                    <td colspan="8" width="100%">
                            <table width="100%" >                  
                                <tr  class="estiloImput">
                                    <caption class="camposRepInp"> <label id="lblIdPaciente"></label>  </caption>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2">Paciente <input  type="text" id="txtIdBusPaciente" title="Ingrese Identificacion o Nombre del paciente" style="width:80%"   size="40"     />
                                                    <img width="18px" height="18px" align="middle" title="Seleccione Buscar " id="idLupitaVentanitaPacienC" onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                </td><div id="divParaVentanita"></div> 
                                                <td><input name="btn_BUSCAR PACIENTE" title="Permite buscar Pacientes Registrados" 
                                                    type="button" class="small button blue" value="BUSCAR"  
                                                    onclick=" buscarInformacionBasicaPaciente(); setTimeout('cargarInformacionContactoPaciente()', 200)"/></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <input type="checkbox" id="sw_nuevo_paciente" onchange="elementosNuevoPacienteEncuesta()"> Nuevo paciente
                                    </td>
                                    <td colspan="2" id="elementosNuevoPaciente" hidden>
                                        <table width="100%">
                                            <tr>
                                                <th>Tipo de documento</th>
                                                <th>Numero de documento</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbTipoIdUrgencias" width="90%">
                                                        <option value=""></option>
                                                            <%     
                                                            resultaux.clear();
                                                            ComboVO cmbM; 
                                                            resultaux=(ArrayList)beanAdmin.combo.cargar(105);	
                                                            for(int k=0;k<resultaux.size();k++){ 
                                                                    cmbM=(ComboVO)resultaux.get(k);
                                                            %>
                            <option value="<%= cmbM.getId()%>" title="<%= cmbM.getTitle()%>">
                                <%=cmbM.getId() + " " + cmbM.getDescripcion()%></option>
                            <%}%>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="number" width="90%" id="txtIdentificacionUrgencias" onkeypress="javascript:return soloTelefono(event);">
                                                </td>
                                                <td>
                                                    <input type="button" class="small button blue" value="CREAR" onclick="nuevoPacienteEncuesta()">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                 </tr>
                                 <tr>
                                    <td colspan="6"><hr></td>
                                </tr>
                                 <tr class="titulos">   
                                    <td width="20%">Tipo Id</td>
                                    <td width="20%">Identificacion</td>
                                    <td width="17%">Apellidos</td>  
                                    <td width="17%">Nombres</td>                      
                                    <td width="17%">Fecha Nacimiento</td>   
                                    <td width="17%">Edad</td>                                            
                                </tr>            
                                <tr class="estiloImput">
                                    <td>                   
                                            <select size="1" id="cmbTipoId" style="width:70%"  >
                                            <option value=""></option>
                                                <%     resultaux.clear();
                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(105);    
                                                    ComboVO cmb5; 
                                                    for(int k=0;k<resultaux.size();k++){ 
                                                        cmb5=(ComboVO)resultaux.get(k);
                                                %>
                            <option value="<%= cmb5.getId()%>" title="<%= cmb5.getTitle()%>">
                                <%= cmb5.getId()+"  "+cmb5.getDescripcion()%></option>
                            <%}%>                       
                                        </select>
                                    </td> 
                                    <td><input  type="text" id="txtIdentificacion"  style="width:95%"   size="60" maxlength="60"    onkeypress="javascript:return soloTelefono(event);"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtApellido1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />
                                        <input type="text" id="txtApellido2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />
                                    </td> 
                                    <td>
                                        <input type="text" id="txtNombre1" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"   style="width:45%" />
                                        <input type="text" id="txtNombre2" size="30" maxlength="30" onkeypress="javascript:return teclearsoloan(event);"  style="width:45%" />  
                                    </td>   
                                    <td><input id="txtFechaNac" maxlength="10" type="text"  size="10" value=""  maxlength="10"  /> 
                                        
                                    </td>                           
                                    <td> 
                                        <label id="lblEdad" style="font-size: 13PX;" hidden>  </label> 
                                        <label id="lblEdadCompleta" style="font-size: 13PX;">  </label> 
                                    </td> 

                                    
                                </tr>
                            <tr class="titulos">
                                <td width="20%">Municipio</td> 
                                <td width="20%">Direcci&oacute;n</td>   
                                <td width="17%">Acompañante</td>                    
                                <td width="17%">Celular1</td>
                                <td width="17%">Celular2</td>
                                <td width="17%">Sexo</td>
                                
                            </tr>                            
                            <tr class="estiloImput">
                                <td><input type="text" id="txtMunicipio" size="20"  onkeyup="llamarAutocomIdDescripcionConDato('txtMunicipio',310)" style="width:70%"    /></td> 
                                <td><input type="text" id="txtDireccionRes" size="60" maxlength="60"  style="width:95%"  onkeypress="javascript:return teclearsoloan(event);"  /></td>   
                                <td><input type="text" id="txtNomAcompanante"  size="100" maxlength="100"  style="width:95%"  onkeypress="javascript:return teclearExcluirCaracter(event);"   /> </td>                    
                                <td>
                                    <input type="text" id="txtCelular1" size="30"  style="width:95%" maxlength="10"  onKeyPress="javascript:return teclearsoloDigitos(event);"  />
                                </td>
                                <td>
                                    
                                    <input type="text" id="txtCelular2" size="30" maxlength="10" style="width:95%"   onKeyPress="javascript:return teclearsoloDigitos(event);"  />                   
                                </td>

                                <td>
                                    <select size="1" id="cmbSexo" maxlength="30" style="width:95%"  >                                            
                                        <option value=""></option>
                                        <%  resultaux.clear();
                                        resultaux=(ArrayList)beanAdmin.combo.cargar(110); 
                                        ComboVO cmbS1; 
                                        for(int k=0;k<resultaux.size();k++){ 
                                            cmbS1=(ComboVO)resultaux.get(k);
                                      
                                        %> 
                                        <option value="<%= cmbS1.getId()%>" title="<%= cmbS1.getTitle()%>"><%=cmbS1.getDescripcion()%>
                                        </option>
                                        <%}%>
                                    </select>
                               
                            </td>
                              
                            
                                                                     
                            </tr>
                            <tr class="titulos">
                                <td>Email</td>     
                                <td>Telefono</td>                                        
                            </tr>   
                            <tr class="estiloImput">
                                <td><input type="text" id="txtEmail" size="50"  maxlength="50" onkeypress="javascript:return email(event);" style="width:70%"  /> </td>     
                                <td>   
                                    <input type="text" id="txtTelefonos" size="30" maxlength="7" style="width:95%"   onKeyPress="javascript:return teclearsoloDigitos(event);"  />                    
                                </td>
                                
                            </tr> 
                            <tr>
                                <td><hr></td>
                                <% if(beanSession.usuario.preguntarMenu("CITB01") ){%>    
                                <td>
                                    <input type="button" id="CITB01" title="CITB01" onclick="modificarCRUD('modificarPacienteEncuesta')" class="small button blue" value="MODIFICAR PACIENTE"/>
                                </td>
                                <%}%>                                 
                            </tr> 
                            <tr class="titulos"  >
                                <td colspan="6" valing= "top">
                                    <table width="100%"  align="center" > 
                                        <tr class="titulos" >
                                            <td  width="100%"  valing= "top" > 
                                                <div id="divDepositoContactosPaciente"></div>     
                                            </td>
                                        </tr>   
                                    </table> 
                                </td>
                            </tr>               
                        </table>      
                        <label id="lblTipoDocumento">EVMD</label>                   
                    </td>                    
                </tr>                
            </table>            
    </tr>        
</table>    

<div id="divValidaciones" hidden>   
    <table id="listGrillaFormulasE" class="scroll"></table>    
    <table id="listGrillaValoresValidacionesE" class="scroll"></table>    
    <table id="listGrillaValidacionesE" class="scroll"></table>
</div>