<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.*,java.text.*,java.sql.*" %>
<%@ page import="Sgh.Utilidades.*" %>
<%@ page import="Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import="Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>


<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<% beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList(); %>

<table width="100%" cellpadding="0" cellspacing="0" align="center" style="padding-left: 2px;padding-right: 2px;">

    <tr class="titulosListaEspera">
        <td>Tipo</td>
        <td>Admisiones</td>
        <td>Estado</td>
        <td>Trazabilidad</td>
        <td></td>
    </tr>

    <tr class="estiloImput">
        <td>
            <select size="1" id="cmbClaseProcedimiento" style="width:50%"
                    onchange="buscarHistoria('listOrdenes')">
                <option value=""></option>
                <% resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(8);
                    ComboVO cmbCla;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbCla = (ComboVO) resultaux.get(k);%>
                <option value="<%= cmbCla.getId()%>">
                    <%=cmbCla.getId() + " " + cmbCla.getDescripcion()%>
                </option>
                <%}%>
            </select>
        </td>
        <td>
            <select id="cmbAdmisionesOrdenes" style="width:50%"
                    onchange="buscarHistoria('listOrdenes')">
                <option value="0">Admision Actual</option>
                <option value="1">Admisiones Anteriores</option>
            </select>
        </td>

        <td>
            <select size="1" id="cmbEstadoOrdenes" style="width:50%"
                    onchange="buscarHistoria('listOrdenes')">
                <option value=""></option>
                <% resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(1);
                    ComboVO cmbEst;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbEst = (ComboVO) resultaux.get(k);%>
                <option value="<%= cmbEst.getId()%>">
                    <%=cmbEst.getId() + " " + cmbEst.getDescripcion()%>
                </option>
                <%}%>
            </select>
        </td>
        <td>
            <select id="cmbTrazabilidad" style="width:50%"
                    onchange="buscarHistoria('listOrdenes')">
                <option value=""></option>
                <option value="S">Con Trazabilidad</option>
                <option value="N">Sin Trazabilidad</option>
            </select>
        </td>
        <td>
            <input type="button" class="small button blue" value="BUSCAR"
                   onclick="buscarHistoria('listOrdenes')" />
        </td>
    </tr>

    <tr>
        <td colspan="5" style="padding-left: 0px;">
            <table width="100%" id="idTablaProcedimientosDetalle" align="center" style="padding-left: 0px;">
                <tr class="titulos">
                    <td>
                        <table id="listOrdenes" class="scroll" cellpadding="0" cellspacing="0"
                               align="center"></table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>

<div id="tabsPrincipalOrdenes" style="width:auto; height: auto; ">
    <ul>
        <li>
            <a href="#divOrdenesSubidas"
               onclick="TabActivoConductaTratamiento('divOrdenesSubidas')">
                <center>DETALLE DE LOS RESULTADOS</center>
            </a>
        </li>
        <!--li>
<a href="#divInterpretacion" onclick="TabActivoConductaTratamiento('divInterpretacion')">
<center>INTERPRETACIONES HISTORICAS</center>
</a>
</li-->
        <li>
            <a href="#divNuevaInterpretacion"
               onclick="TabActivoConductaTratamiento('divNuevaInterpretacion')">
                <center>HACER UNA INTERPRETACION</center>
            </a>
        </li>

        <li id="conteResultadosExternos">
            <a href="#divResultadosExternos"
               onclick="TabActivoConductaTratamiento('divResultadosExternos')">
               <center>INGRESAR RESULTADOS EXTERNOS CR&Oacute;NICOS</center>
            </a>
        </li>

        <li id="conteIngresoResultadosExternos">
            <a href="#divIngresoResultadosExternos"
               onclick="TabActivoConductaTratamiento('divIngresoResultadosExternos')">
                <center>INGRESO RESULTADOS EXTERNOS</center>
            </a>
        </li>
        <li>
            <a href="#divGlucometrias"
               onclick="TabActivoConductaTratamiento('divGlucometrias')">
                <center>GLUCOMETRIAS ENFERMERIA</center>
            </a>
        </li>
        <li>
            <a href="#divAfinamientos"
               onclick="TabActivoConductaTratamiento('divAfinamientos')">
                <center>AFINAMIENTOS ENFERMERIA</center>
            </a>
        </li>
    </ul>

    <div id="divOrdenesSubidas" >
        <label style="display:none;" id="lblIdPlanEvolucionDetalleProgramacion"></label>
        <table width="100%" style="background-color: #E8E8E8;">
            <tr>
                <td class="titulos">
                    <!-- <table id="listOrdenesArchivo" ></table> -->
                    <table id="listAnalitosLaboratorios" ></table>
                </td>
            </tr>
        </table>        
    </div> 

    <div id="divNuevaInterpretacion">
        <table width="100%" style="background-color: #E8E8E8;">
            <tr class="titulosListaEspera">
                <td width="5%">ID ORDEN</td>
                <td width="8%">FECHA ORDENADO</td>
                <td width="10%">PROFESIONAL ORDENA</td>
                <td width="14%">EMPRESA QUE REALIZA EL EXAMEN</td>
                <td width="8%">ESTADO</td>
            </tr>
            <tr class="estiloImput">
                <td><label id="lblIdOrden"></label></td>
                <td><label id="lblFechaOrden"></label></td>
                <td><label id="lblProfesionalOrden"></label></td>
                <td><label id="lblEmpresaOrden"></label></td>
                <td><label id="lblIdEstadoOrden"></label>-<label id="lblNomEstadoOrden"></label>
                </td>

            </tr>

            <tr class="estiloImput">
                <td colspan="6"><textarea id="txtInterpretacion"
                                          style="width:95%;font-size: 14px;" maxlength="5000" 
                                          rows="6" onkeypress="return validarKey(event, this.id)"
                                          onblur="v28(this.value, this.id);"></textarea></td>
            </tr>

            <tr class="estiloImput">
                <td colspan="6">
                    <input id="btnAdicionarInterpretacion" type="button"
                           class="small button blue" value="INTERPRETAR"
                           onclick="modificarCRUD('interpretarProcedimiento')" />
                    <input id="btnModificarInterpretacion" type="button"
                           class="small button blue" value="MODIFICAR"
                           onclick="modificarCRUD('modificarInterpretacionProcedimiento')" />
                </td>
            </tr>

            <tr>
                <td class="titulos" colspan="6">
                    <table id="listInterpretaciones"></table>
                </td>
            </tr>
        </table>
    </div>


    <div id="divResultadosExternos">
        <div id="divParaVentanitaResultadosExternos"></div>        
        <table width="100%">
            <tr class="titulosListaEspera">
                <td>ESPECIALIDAD</td>
                <td></td>
            </tr>
            <tr class="estiloImput">
                <td><input id="txtIdProcedimientoResultado"
                           onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimientoResultado', 646)"
                           style="width:90%">
                    <img width="18px" height="18px" align="middle"
                         id="idLupitaVentanitaResultadosExternos"
                         onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdProcedimientoResultado', '30')"
                         src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td>
                    <input type="button" class="small button blue" value="ADICIONAR"
                           onclick="verificarNotificacionAntesDeGuardar('verificarResultadosExternos')" />
                </td>
            </tr>
            <tr>
                <td class="titulos" colspan="2">
                    <table id="listaProcedimientoResultadosExternos"></table>
                </td>
            </tr>
            <tr>
                <td class="titulos" colspan="2">
                    <table id="listaAnalitosResultadosExternos"></table>
                </td>
            </tr>

        </table>
    </div>

    <!-- ******************** -->
    <div id="divResultadosExternosVIH" hidden="true">
       <!-- <div id="divParaVentanitaResultadosExternos"></div>  -->      
        <table width="100%">
            <tr class="titulosListaEspera">
                <td>ESPECIALIDAD</td>
                <td></td>
            </tr>
            <tr class="estiloImput">
                <td><input id="txtIdProcedimientoResultado"
                           onkeypress="llamarAutocomIdDescripcionConDato('txtIdProcedimientoResultado', 646)"
                           style="width:90%">
                    <img width="18px" height="18px" align="middle"
                         id="idLupitaVentanitaResultadosExternos"
                         onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdProcedimientoResultado', '30')"
                         src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td>
                    <input type="button" class="small button blue" value="ADICIONAR"
                           onclick="verificarNotificacionAntesDeGuardar('verificarResultadosExternos')" />
                </td>
            </tr>
            <tr>
                <td class="titulos" colspan="2">
                    <table id="listaProcedimientoResultadosExternos"></table>
                </td>
            </tr>
            <tr>
                <td class="titulos" colspan="2">
                    <table id="listaAnalitosResultadosExternos"></table>
                </td>
            </tr>

        </table>
    </div>
    <!-- ******************** -->

    <div id="divIngresoResultadosExternos">
        <table width="100%">
            <tr class="titulosListaEspera">
                <td>Ingreso otros resultados Externos</td>
            </tr>
        </table>
        <table width="100%" height="120" class="fondoTabla">
            <tr class="titulosListaEspera">
                <td>
                    PROCEDIMIENTO
                </td>

            </tr>
            <tr class="estiloImputListaEspera">
                <td>
                    <input id="txtIdProcedimientoIngresoResultadoExterno" onkeypress="llenarElementosAutoCompletarKey(this.id, 221, this.value)" style="width:90%" >
                    <img title="VEN32" id="idLupitaVentanitaProcmto" onclick="traerVentanitaProcedimientos(this.id, 'lblIdAdministradora', 'divParaVentanitaCondicion1', 'txtIdProcedimientoIngresoResultadoExterno', '25')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle">
                </td>

            </tr>
            <tr>
                <td colspan="4" align="center">
                    <input id="btnIngresoOtrosResultadosEx" type="button" class="small button blue" value="AGREGAR" onclick="modificarCRUD('ingresoResultadoExterno')" >
                </td>
            </tr>
        </table>
        <table id="ingresoOtroResultadoExterno" class="datagrid">
            <tr class="titulos">
                <td>
                    <table id="tablaOtrosResultadosExternosIngresados">
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div id="divGlucometrias">
        
        
        <table id="tablaGlucometriaAfinamientos" class="datagrid">
            <tr class="titulos">
                <td>
                    <table id="glucometriasEnfe">

                    </table>
                </td>
            </tr>

        </table>
        <table width="100%">
            <tr class="estiloImput">
                <td style="padding: 0px 0px 0px 10px;">
                    <input type="button" class="small button blue"
                           value="CREAR REGISTRO DE GLUCOMETRIA"
                           onclick="comprobarGlucometria()"/>
                    <!-- <input type="button" class="small button blue" id="pruebaPeticiones" onclick="buscarHC('crearGlucomentriasEnfe')"> -->
                </td>
            </tr>
        </table>
        <table id="creacionGlucometrias">
            <tr class="titulos">
                <td style="padding: 0px 0px 0px 10px;">
                    <table id="mostrarGlucomentriasEnfe"></table>
                </td>
            </tr>
            <tr>
                <td style="display: flex;justify-content: space-between;">
                    <input type="button" class="small button blue" id="crearNuevoRegistroGlucometrias"
                           value="A&ntilde;adir un nuevo registro" onclick="modificarCRUD('crearNuevoRegistroGlucometrias')" />
                    <input type="button" class="small button blue" id="finalizarRegistroGlucometrias"
                           value="Finalizar toma de registros"  onclick="modificarCRUD('finalizarRegistroGlucometrias')"  />
                    <label for="" id="txtIdRegistroGlucometria" style="display:none"></label>
                </td>

            </tr>
        </table>
    </div>
    <div id="divAfinamientos">
        <table id="tablaGlucometriaAfinamientos" class="datagrid">
            <tr class="titulos">
                <td style="padding: 0px 0px 0px 10px;">
                    <table id="afinamientosEnfe">

                    </table>
                </td>
            </tr>

        </table>
        <table width="100%">
            <tr class="estiloImput">
                <td style="padding: 0px 0px 0px 10px;">
                    <input type="button" class="small button blue"
                           value="CREAR REGISTRO DE AFINAMIENTOS"
                           onclick="comprobarAfinamiento()"/>
                    <!-- <input type="button" class="small button blue" id="pruebaPeticiones" onclick="buscarHC('crearGlucomentriasEnfe')"> -->
                </td>
            </tr>
        </table>
        <table id="creacionAfinamientos">
            <tr class="titulos">
                <td style="padding: 0px 0 0px 10px;">
                    <table id="mostrarAfinamientosEnfe"></table>
                </td>
            </tr>
            <tr>
                <td style="display: flex;justify-content: space-between;">
                    <input type="button" class="small button blue" id="crearNuevoRegistroAfinamientos"
                           value="A&ntilde;adir un nuevo registro" onclick="modificarCRUD('crearNuevoRegistroAfinamientos')" />
                    <input type="button" class="small button blue" id="finalizarRegistroGlucometrias"
                           value="Finalizar toma de registros"  onclick="modificarCRUD('finalizarRegistroAfinamientos')"  />
                    <label for="" id="txtIdRegistroAfinamientos" style="display:none"></label>

                </td>
            </tr>
        </table>
    </div>
</div>

<input type="hidden" id="txtIdProcedimientoProgramacion" />
<input type="hidden" id="txtIdResultadoProgramacion" />
<input type="hidden" id="txtEliminarLaboratorio" />

<div id="divVentanitaResultadoPlantilla"
     style="display:none; z-index:1007; top:1px; left:211px;">
    <div class="transParencia"
         style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <div id="ventanitaHija"
         style="z-index:1009; position:fixed; top:200px; left:160px; width:70%">
        <table width="1000" height="90" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right"
                                        value="CERRAR" onclick="ocultar('divVentanitaResultadoPlantilla')" />
                </td>
                <td>&nbsp;</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right"
                                         value="CERRAR" onclick="ocultar('divVentanitaResultadoPlantilla')" />
                </td>
            </tr>

            <tr>
                <td colspan="3" class="titulos">
                    <table id="listResultadoPlantilla"></table>
                    <div id="pagerPlantilla"></div>
                </td>
            </tr>
        </table>


    </div>
</div>
<!-- VENTANITA QUE PERMITE EDITAR LOS REGISTROS DE GLUCOMETRIA SE ACTIVA CUANDO SE SELECCIONA UN REGISTRO DE LA TABLA -->
<div id="divEditarGlucometrias" style="display:none; z-index:1007; top:1px; left:211px;">
    <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <label id="txtIdRegistroGlucometriaModificar" style="display:none"></label>
    <div id="ventanitaHija" style="z-index:1009; position:fixed; top:200px; width:70%">                           
        <table width="1000" height="90" class="fondoTabla">
            <tr class="titulosListaEspera">
                <td>
                    FECHA TOMA
                </td>
                <td>
                    HORA TOMA
                </td>
                <td>
                    PREPRANDIAL
                </td>
                <td>
                    POSPANDRIAL
                </td>
                <td>
                    OBSERVACIONES
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td>
                    <input type="date" id="txtFechaTomaGlucometria">
                </td>
                <td>
                    <input type="time" id="txtHoraTomaGlucometria">
                </td>
                <td>
                    <input type="text" id="txtPrepandrialGlucometria">
                </td>
                <td>
                    <input type="text" id="txtPosGlucometria">
                </td>
                <td>
                    <textarea id="txtObservacionesGlucometria"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <input type="button" class="small button blue" value="AGREGAR" onclick="modificarCRUD('guardarRegistroGlucometriaModificar')">
                    <input type="button" class="small button blue" value="CANCELAR" onclick="ocultar('divEditarGlucometrias')">
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- VENTANITA QUE PERMITE EDITAR LOS REGISTROS DE AFINAMIENTOS SE ACTIVA CUANDO SE SELECCIONA UN REGISTRO DE LA TABLA -->
<div id="divEditarAfinamientos" style="display:none; z-index:1007; top:1px; left:211px;">
    <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
    </div>
    <label id="txtIdRegistroAfinamientosModificar" style="display:none"></label>
    <div id="ventanitaHija" style="z-index:1009; position:fixed; top:200px; width:70%">                           
        <table width="1000" height="90" class="fondoTabla">
            <tr class="titulosListaEspera">
                <td>
                    FECHA TOMA
                </td>
                <td>
                    TOMA DE LA MAÑANA
                </td>
                <td>
                    TOMA DE LA TARDE
                </td>
                <td>
                    OBSERVACIONES
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td>
                    <input type="date" id="txtFechaTomaAfinamiento">
                </td>
                <td>
                    <input type="text" id="txtTomaManianaAfinamiento">
                </td>
                <td>
                    <input type="text" id="txtTomaTardeAfinamiento">
                </td>
                <td>
                    <textarea id="txtObservacionesAfinamientos"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <input type="button" class="small button blue" value="AGREGAR" onclick="modificarCRUD('guardarRegistroAfinamientoModificar')">
                    <input type="button" class="small button blue" value="CANCELAR" onclick="ocultar('divEditarAfinamientos')">
                </td>
            </tr>
        </table>
    </div>
</div>

<!--VENTANITA DE INGRESAR RESULTADOS EXTERNOS-->

<div id="divParaIngresarResultadosExternos" style="display:none; z-index:1007; top:1px; left:211px;">
    <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4); height: 3000px;">
    </div>
    <div id="ventanitaHija" style="z-index:1009; position:fixed; top:200px; left:300px; width:90%">                           
        <table class="fondoTabla" width="1000" height="90">
            <tr class="titulosListaEspera">
                <td>
                    ANALITO                                                                                                
                </td>
                <td>
                    RESULTADO
                </td>
                <td>
                    FECHA RESULTADO
                </td>
                <td>
                    UNIDADES
                </td>
                <td>
                    MIN - MAX
                </td>
            </tr>
            <tr class="estiloImputListaEspera" style="font-size: medium;">
                <td>
                    <input type="hidden" id="txtTipoResultado"/>
                    <input type="hidden" id="txtIdProgramacion"/>
                    <input type="hidden" id="txtIdOrden"/>
                    <input type="hidden" id="txtIdPlantilla"/>
                    <input type="hidden" id="txtIdLa01"/>
                    <input type="hidden" id="txtModificar"/>
                    <label id="lblAnalito"></label>
                </td>
                <td>
                    <input id="txtResultadoNumericoAnalito" style="width: 90%; height: auto; display: none;" onPaste="return onPaste()" onkeypress="return checkDecimal(event, this);">
                    <textarea id="txtResultadoAnalito" style="width: 90%; height: auto; display: none;"></textarea>
                </td>
                <td>                    
                    <input type="date" value="<%=LocalDate.now()%>" id="txtFechaResultadoAnalito"
                           onclick="validarFechasPlantillas(this.id, 1);"
                           >
                </td>
                <td>
                    <label id="lblUnidades"></label>
                </td>
                <td>
                    <label id="lblMinAnalito"></label>-<label id="lblMaxAnalito"></label>
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td colspan="5" align="center">
                    <input type="button" style="display: none;" class="small button blue" value="ADICIONAR" id="btnAdicionarResultado" onclick="verificarNotificacionAntesDeGuardar('verificarDuplicadosRExternos')">
                    <input type="button" style="display: none;" class="small button blue" value="MODIFICAR" id="btnModificarResultado" onclick="verificarNotificacionAntesDeGuardar('verificarDuplicadosRExternos')">
                    <input type="button" style="display: none;" class="small button blue" value="ELIMINAR" id="btnEliminarResultado" onclick="modificarCRUD('eliminarLaboratorioExterno')">
                    <input type="button" class="small button blue" value="SALIR" onclick="ocultar('divParaIngresarResultadosExternos');">
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="divEditarResultadosExternos" style="display:none; z-index:1007; top:1px; left:211px;">
    <div class="transParencia" style="z-index:1008; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);"></div>
    <label id="txtIdRegistroResultadoExterno" style="display:none"></label>
    <label id="txtIdEstadoFolio" style="display:none"></label>
    <label id="txtIdFolioResultadoExterno" style="display:none"></label>
    <div id="ventanitaHija" style="z-index:1009; position:fixed; top:60%; left:20%; width:70%">                           
        <table width="1000" height="90" class="fondoTabla">
            <tr class="titulosListaEspera">
                <td>
                    NOMBRE
                </td>
                <td>
                    RESULTADO
                </td>
                <td>
                    UNIDADES
                </td>
                <td>
                    FECHA
                </td>
            </tr>
            <tr class="estiloImputListaEspera">
                <td>
                    <input type="text" style="width:90%" id="txtNombreResultadoExterno" readonly>
                </td>
                <td>
                    <textarea style="width:90%" id="txtResultadoInterpretacionExterno"></textarea>
                </td>
                <td>
                    <input type="text" style="width:90%" id="txtUnidadesResultadoExterno">
                </td>
                <td>
                    <input type="date" style="width:90%" id="txtFechaResultadoExterno" onclick="validarFechasPlantillas(this.id, 1)" onblur="validarFechaEscrita(valorAtributo('txtFechaResultadoExterno'), 'txtFechaResultadoExterno', 1);">
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <input type="button" class="small button blue" value="AGREGAR" onclick="modificarCRUD('guardarRegistroIngresoResultadoExterno')">
                    <input type="button" class="small button blue" value="CANCELAR" onclick="ocultar('divEditarResultadosExternos')">
                    <input id='btnEliminarOtroExamenEx' type="button" class="small button blue" value="ELIMINAR REGISTRO" onclick="modificarCRUD('eliminarRegistroLaboratorioExterno')">
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="divParaVentanitaCondicion1"></div>
