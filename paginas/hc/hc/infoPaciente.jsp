<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->

<%  beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<table width="100%"  cellpadding="0" cellspacing="0"  align="center">
    <tr hidden>
        <td colspan="6">
            <table id="listInfoPacienteEnHc" class="scroll" ></table>
        </td>
    </tr>
    <tr style="width: 100%; height: 10px;">

    </tr>
    <tr class="titulosListaEspera">
        <td width="10%">GRUPO SANGUINEO<label style="color: red;">*</label></td>
        <td width="20%">ORIENTACION SEXUAL</td>      
        <td width="20%">DISCAPACIDAD<label style="color: red;">*</label></td>
        <td width="20%">HECHOS VICTIMIZANTES<label style="color: red;">*</label></td>
        <td width="20%">GRUPO POBLACIONAL<label style="color: red;">*</label></td>


    </tr>
    <tr class="estiloImputListaEspera">
        <td>
            <select id="cmbIdGrupoSanguineo" style="width: 95%;"  >                                         
                <option value=""></option>
                <% resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(595);
                    ComboVO cmbGS;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbGS = (ComboVO) resultaux.get(k);
                %>
                <option value="<%=cmbGS.getId()%>" title=" "><%= cmbGS.getDescripcion()%></option>
                <%}%> 
            </select>
        </td>
        <td>
            <select id="cmbIdOrientacionSexual" style="width: 95%;"  >       
                <% resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(952);
                    ComboVO cmbOS;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbOS = (ComboVO) resultaux.get(k);
                %>
                <option value="<%=cmbOS.getId()%>" ><%= cmbOS.getDescripcion()%></option> 
                <%}%> 
            </select>
        </td>
        <td>
            <select id="cmbIdDiscapacidad" style="width: 95%;"  >
                <option value=""></option>
                <% resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(953);
                    ComboVO cmbDIS;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbDIS = (ComboVO) resultaux.get(k);
                %>
                <option value="<%=cmbDIS.getId()%>" ><%= cmbDIS.getDescripcion()%></option> 
                <%}%> 
            </select>
        </td>
        <td>
            <select id="cmbIdHechosVic" style="width: 95%;"  >
                <option value=""></option>
                <% resultaux.clear();
                    resultaux = (ArrayList) beanAdmin.combo.cargar(954);
                    ComboVO cmbHV;
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbHV = (ComboVO) resultaux.get(k);
                %>
                <option value="<%=cmbHV.getId()%>" ><%= cmbHV.getDescripcion()%></option> 
                <%}%> 
            </select>
        </td>
        <td>
            <select style="width: 95%;" id="cmbGrupoPobla">
                <option value=""></option>
                <%
                    resultaux.clear();
                    ComboVO cmbGPobla;
                    resultaux = (ArrayList) beanAdmin.combo.cargar(955);
                    for (int k = 0; k < resultaux.size(); k++) {
                        cmbGPobla = (ComboVO) resultaux.get(k);
                %>
                <option value="<%= cmbGPobla.getId()%>" ><%=cmbGPobla.getDescripcion()%></option>
                <%}%>
            </select>
        </td>


    </tr>
    <tr class="titulosListaEspera">
        <td width="15%">PROGRAMA SOCIAL<label style="color: red;">*</label></td>
        <td>CELULAR PACIENTE</td>
        <td>NOMBRE ACOMPAÑANTE</td>
        <td>PARENTESCO</td>
        <td>CELULAR ACOMPAÑANTE</td>
    </tr>
    <tr>

        <td>
    <center>
        <select id="cmbIdProgramaSocial" style="width: 95%;"  >
            <option value=""></option>
            <% resultaux.clear();
                resultaux = (ArrayList) beanAdmin.combo.cargar(956);
                ComboVO cmbPSOCI;
                for (int k = 0; k < resultaux.size(); k++) {
                    cmbPSOCI = (ComboVO) resultaux.get(k);
            %>
            <option value="<%=cmbPSOCI.getId()%>" ><%= cmbPSOCI.getDescripcion()%></option> 
            <%}%> 
        </select>
    </center>
</td>

<td>
<center>
    <input type="text" id="txtCelularPaciente" onKeyPress="javascript:return teclearsoloDigitos(event);">
</center>
</td>

<td>
<center>
    <input style="width: 95%;" type="text" id="txtNombreAcompanante" >
</center>
</td>

<td>
<center>
    <select id="cmbIdParentesco" style="width: 95%;"  >
        <option value=""></option>
        <% resultaux.clear();
            resultaux = (ArrayList) beanAdmin.combo.cargar(957);
            ComboVO cmbPAREN;
            for (int k = 0; k < resultaux.size(); k++) {
                cmbPAREN = (ComboVO) resultaux.get(k);
        %>
        <option value="<%=cmbPAREN.getId()%>" ><%= cmbPAREN.getDescripcion()%></option> 
        <%}%> 
    </select>
</center>
</td>

<td>
<center>
    <input type="text" id="txtCelularAcompanante" onKeyPress="javascript:return teclearsoloDigitos(event);">
</center>
</td>
</tr> 

<tr class="titulosListaEspera">	
    <td>EMAIL</td> 
    <td>ETNIA</td>             
    <td>NIVEL ESCOLARIDAD<label style="color: red;">*</label></td>  
    <td>OCUPACION<label style="color: red;">*</label></td> 
    <td>ESTRATO<label style="color: red;">*</label></td> 
<tr>  
<tr class="estiloImputListaEspera">
    <td><input type="text" id="txtEmail" size="60"  maxlength="60"  style="width:99%"  /> </td>                 
    <td>
        <select id="cmbIdEtnia" style="width:70%"  >	                                        
            <% resultaux.clear();
                resultaux = (ArrayList) beanAdmin.combo.cargar(2);
                ComboVO cmbEt;
                for (int k = 0; k < resultaux.size(); k++) {
                    cmbEt = (ComboVO) resultaux.get(k);
            %>
            <option value="<%= cmbEt.getId()%>" title="<%= cmbEt.getTitle()%>"><%= cmbEt.getDescripcion()%></option>
            <%}%>	
        </select>
    </td>             
    <td>
        <select id="cmbIdNivelEscolaridad" style="width:50%"  >	                                        
            <option value=""></option>
            <% resultaux.clear();
                resultaux = (ArrayList) beanAdmin.combo.cargar(4);
                ComboVO cmbES;
                for (int k = 0; k < resultaux.size(); k++) {
                    cmbES = (ComboVO) resultaux.get(k);
            %>
            <option value="<%= cmbES.getId()%>" title="<%= cmbES.getTitle()%>"><%= cmbES.getDescripcion()%></option>
            <%}%>	
        </select>                         
    </td>  
    <td>
        <input id="txtIdOcupacion" type="text"  style="width:90%" onkeypress="llamarAutocomIdDescripcionConDato('txtIdOcupacion', 8)" maxlength="60" size="60">  
        <img width="18px" height="18px" align="middle" src="/clinica/utilidades/imagenes/acciones/buscar.png" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdOcupacion', '6')" title="VEN31">                                                 
    </td>
    <td>
        <select  id="cmbIdEstrato" style="width:50%"  >	                                        
            <option value=""></option>
            <option value="1">1 - Bajo</option>
            <option value="2">2 - Medio-Bajo</option>   
            <option value="3">3 - Medio</option>   
            <option value="4">4 - Medio-Alto</option>   
            <option value="5">5 - Alto</option>   
            <option value="6">6 - Sin Dato</option>   
            <option value="7">7 - No Aplica</option>
            <option value="8">Sin Estrato</option>                            
        </select>                         
    </td>                                          
<tr>
<tr class="titulosListaEspera">
    <td >MUNICIPIO<label style="color: red;">*</label></td>
    <td >COMUNA/CORREGIMIENTO</td>
    <td >BARRIO/VEREDA</td>
    <td >DIRECCION</td>
    <td >POBLACION CLAVE<label style="color: red;">*</label></td>
</tr>
<tr class="estiloImputListaEspera">
    <td>
        <input id="txtMunicipio" type="text"  style="width:70%" onkeypress="llamarAutocomIdDescripcionConDato('txtMunicipio', 9022)" maxlength="60" size="60">        
        <img width="18px" height="18px" align="middle" title="VEN71" id="idLupitaVentanitaMuni" onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtMunicipio', '1')" src="/clinica/utilidades/imagenes/acciones/buscar.png">               
        <div id="divParaVentanita"></div> 
    </td>
    <td><select id="cmbIdLocalidadInfoP" style="width:80%" 
                onfocus="cargarDepDesdeMunicipioGrupo('txtMunicipio', 'cmbIdLocalidadInfoP');" onchange=" limpiaAtributo('cmbIdBarrioInfoP', 0);">	                                        
            <option value=""></option>
        </select>	 	                   
    </td> 
    <td><select size="1" id="cmbIdBarrioInfoP" style="width:80%"     
                onfocus="cargarDepDesdeLocalidadGrupo('cmbIdLocalidadInfoP', 'cmbIdBarrioInfoP')">	                                        
            <option value=""></option>                                  
        </select>	                   
    </td>
    <td><input type="text" id="txtDireccionInfoP"/></td>
    <td><select size="1" id="cmbIdPoblacionClave" style="width:80%">                      
        <option value=""></option>
        <% resultaux.clear();
                resultaux = (ArrayList) beanAdmin.combo.cargar(3730);
                ComboVO cmbPC;
                for (int k = 0; k < resultaux.size(); k++) {
                    cmbPC = (ComboVO) resultaux.get(k);
        %>
        <option value="<%= cmbPC.getId()%>" title="<%= cmbPC.getTitle()%>"><%= cmbPC.getDescripcion()%></option>
        <%}%>                            
        </select>	                   
    </td>
    


</tr>

<tr class="titulosListaEspera">
    <td >SEXO<label style="color: red;">*</label></td>
    <td >INTERSEXUAL</td>

</tr>
<tr>
    <td> <select size="1" id="cmbSexo" style="width:95%" >
        <option value=""></option>
            <%  resultaux.clear();
                resultaux=(ArrayList)beanAdmin.combo.cargar(110);	
                ComboVO cmbS; 
                for(int k=0;k<resultaux.size();k++){ 
                  cmbS=(ComboVO)resultaux.get(k);
            %>
        <option value="<%= cmbS.getId()%>" title="<%= cmbS.getTitle()%>"><%=cmbS.getDescripcion()%></option>
        <%}%>						
   </select>
 </td>
 <td> <select size="1" id="cmbIntersexual" style="width:95%" >
    <option value=""></option>
        <%  resultaux.clear();
            resultaux=(ArrayList)beanAdmin.combo.cargar(10322);	
            ComboVO cmbI; 
            for(int k=0;k<resultaux.size();k++){ 
              cmbI=(ComboVO)resultaux.get(k);
        %>
    <option value="<%= cmbI.getId()%>" title="<%= cmbI.getTitle()%>"><%=cmbI.getDescripcion()%></option>
    <%}%>						
</select>
</td>
</tr>


<tr class="estiloImput overs">
    <td colspan="6">
        <input style="width: 15%; font-size: 11px;" type="button"  value="Modificar" class="small button blue" onclick="validarDatosAntesDeGuardarPacienteHC()">     

    </td>
</tr>

<tr>
    <td colspan="6"><hr></td>
</tr>
</table>




