<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "java.time.LocalDate" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<!--
<label id="lblNomAdministradora2" style="font-size:11px; color:#F00" disabled="disabled"></label>
<img src="/clinica/utilidades/imagenes/acciones/search.png" title="IMGp88-cargarProcedimientosHistoricos"
     onclick="cargarProcedimientosHistoricos()" width="14px" height="14px"/>
-->

<div id="tabsPrincipalConductaYTratamiento" style="width:98%; height:auto">
    <div id="divParaVentanitaCondicion1VentanitaProcmto"></div>
    <ul>
        <% if(beanSession.usuario.preguntarMenu("CONDUC")){%>
        <li id="idproc">
            <a href="#divProcedimiento" onclick="TabActivoConductaTratamiento('divProcedimiento')">
                <center>PROCEDIMIENTOS/AYUDAS DIAGNOSTICAS</center>
            </a>
        </li>
        <% if(beanSession.usuario.preguntarMenu("HIS0223")){%>
        <li id="idSolicitudes">
            <a href="#divSolicitudes" onclick="TabActivoConductaTratamiento('divSolicitudes')">
                <center>SOLICITUDES</center>
            </a>
        </li>
        <%}%>  
        <!--<li id="idindi">
            <a href="#divIndicacionMedica" onclick="TabActivoConductaTratamiento('divIndicacionMedica')">
                <center>INDICACIONES</center>
            </a>
        </li>
            li>
            <a href="#divAyudasDiagnosticas" onclick="TabActivoConductaTratamiento('divAyudasDiagnosticas')">
                <center>AYUDAS DIAGNOSTICAS</center>
            </a>
        </li>
        <li>
            <a href="#divTerapias" onclick="TabActivoConductaTratamiento('divTerapias')">
                <center>TERAPIAS INTERCONSULTA</center>
            </a>
        </li>
        <li>
            <a href="#divLaboratorioClinico" onclick="TabActivoConductaTratamiento('divLaboratorioClinico')">
                <center>LABORATORIO</center>
            </a>
        </li-->

        <li id="idmed">
            <a href="#divMedicacion" onclick="TabActivoConductaTratamiento('divMedicacion')">
                <center>MEDICAMENTOS / INSUMOS</center>
            </a>
        </li>
        <li id="idconmed">
            <a href="#divConciliacionMedicamento" onclick="TabActivoConductaTratamiento('divConciliacionMedicamento')">
                <center>CONCILIACION MEDICAMENTO</center>
            </a>
        </li>
        <li id="idremision">
            <a href="#divRemision" onclick="TabActivoConductaTratamiento('divRemision')">
                <center>REMISION</center>
            </a>
        </li>
        <!--<li id="idrefcon">
            <a href="#divReferenciaYContrareferencia" onclick="TabActivoConductaTratamiento('listRemision')">
                <center>REFERENCIA</center>
            </a>
        </li>-->
        <!-- <li>
            <a href="#divControl" onclick="TabActivoConductaTratamiento('divControl')">
                <center>CITAS</center>
            </a>
        </li>-->
        <li id="idIncap">
            <a href="#divIncapacidad" onclick="TabActivoConductaTratamiento('listIncapacidad'); buscarParametros('listIncapacidad');">
                <center>INCAPACIDAD</center>
            </a>
        </li>
        <!--<li id="idcons">
            <a  href="#divConsentimientos" onclick="TabActivoConductaTratamiento('divConsentimientos')">
                <center>CONSENTIMIENTOS</center>
            </a>
        </li>
        <li id="idadmpac">
            <a href="#divAdministracionPaciente" onclick="TabActivoConductaTratamiento('divAdministracionPaciente')">
                <center>GESTI&Oacute;N ADMINISTRATIVA</center>
            </a>
        </li>-->
        <%}%>
        <li id="idedpac">
            <a href="#divEducacionPaciente" onclick="TabActivoConductaTratamiento('divEducacionPaciente'); buscarParametros('listGrillaEducacionPac');">
                <center>RECOMENDACIONES</center>
            </a>
        </li> 
        <% if(beanSession.usuario.preguntarMenu("CONDUC")){%>
        <li id="idimpr">
            <a href="#divImpresion" onclick="TabActivoConductaTratamiento('divImpresion')">
                <center>IMPRESI&Oacute;N</center>
            </a>
        </li>
        <!--li id="idPlanTratamientoNut" style="display: none;">
            <a href="#divPlanTratmientoNut" onclick="TabActivoConductaTratamiento('divPlanTratmientoNut');">
                <center>PLAN TRATAMIENTO</center>
            </a>
        </li>-->
        <%}%>        
    </ul>

    <% if(beanSession.usuario.preguntarMenu("CONDUC")){%>
<div id="divProcedimiento">
    <table width="100%" style="background-color: #E8E8E8;"> 
        <tr class="titulosListaEspera">
            <td>LAB INTERFAZ</td>
            <td width="35%">PROCEDIMIENTOS/AYUDAS DIAGNOSTICAS</td>
            <td width="5%">CANTIDAD</td>
            <td>SITIO</td>
            <td width="15%">GRADO NECESIDAD</td>
            <td width="20%">INDICACI&Oacute;N</td>
            <td width="25%">DIAGNOSTICO ASOCIADO</td>
            <td width="10%"></td>
        </tr>
        <tr class="estiloImput">
            <td>
                <input type="checkbox" id="chkInterfazLab">
            </td>
            <td>
                <input id="txtIdProcedimiento"
                       onkeypress="llenarValidarCheck(this.id, this.value)"
                       style="width:90%" >
                       
                <img width="18px" height="18px" align="middle" title="VEN32"
                     id="idLupitaVentanitaProcmto"
                     onclick="llenarValidarCheckVentanita(this.id)"
                     src="/clinica/utilidades/imagenes/acciones/buscar.png">
            </td>
            <td>
                <!--<select id="cmbCantidad" style="width:90%">
                    <option value=""></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                </select> -->
                <input type="text" value="1" id="cmbCantidad" style="width:90%" onKeyPress="javascript:return teclearsoloDigitos(event);"  maxlength="3" >
              
            </td>
            <td>
                <select size="1" id="cmbSitio" style="width:95%" title="GD48" >  
                    <option value="">[ SELECCIONE ]</option>
                         <%     resultaux.clear();
                                resultaux=(ArrayList)beanAdmin.combo.cargar(120);  
                                ComboVO cmbSQ; 
                                for(int k=0;k<resultaux.size();k++){ 
                                      cmbSQ=(ComboVO)resultaux.get(k);
                         %>
                      <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>"><%= cmbSQ.getDescripcion()%></option>
                                <%}%>                                
                </select> 
            </td>
            <td>
                <select id="cmbNecesidad" style="width:90%" >
                    <%resultaux.clear();
                        resultaux = (ArrayList) beanAdmin.combo.cargar(113);
                        ComboVO cmbPro;
                        for (int k = 0; k < resultaux.size(); k++) {
                            cmbPro = (ComboVO) resultaux.get(k);%>
                    <option value="<%= cmbPro.getId()%>" title="<%= cmbPro.getTitle()%>"><%=cmbPro.getDescripcion()%></option><%}%>           
                </select> 
            </td>
            <td class="titulos">
                <textarea id="txtIndicacion" style="width:95%" maxlength="5000" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)" ></textarea>
            </td>
            <td>
                <select id="cmbIdDxRelacionadoP" style="width:80%" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                    <option value=""></option>
                </select>
            </td>
            <td>
                <!--
                *  se realiza el cambio del nombre de los botones para evitar duplicidand en el manejo de los ids
                *  @author JUAN CARLOS JIMENEZ <juankjibe@gmail.com>
                *  @returns {void}
                */  
                -->
                <input id="btnAdicionarProcedimiento" type="button" class="small button blue" value="ADICIONAR" onclick="verificarNotificacionAntesDeGuardar('verificarLabNarinoClinizad')" />
                <input id="btnCargarProcedimientosAnteriores" type="button" class="small button blue"  value="CARGAR ANTERIORES" title="btnCargarProcedimientosAnteriores:: Permite Cargar Procedimientos de Anteriores Atenciones" onclick="cargarVentana('procedimientos')" />                
                <input id="btnAdicionarPaquetes" type="button" class="small button blue" value="PAQUETES" onclick="cargarVentana('paqueteProcedimientos')" />                
            </td>
            </tr>
            <tr class="titulosListaEspera">
                <td colspan="2" align="right">
                    FILTRO PROCEDIMIENTOS/AYUDAS DIAGNOSTICAS
                </td>
                <td colspan="2" align="left">
                    <select id="cmbClaseFiltro" onchange="buscarHC('listGrillaCargarProcedimientosFiltro', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp');" >
                        <option value="">[TODOS]</option>
                        <%resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(8);
                            ComboVO cmbProc;
                            for (int k = 0; k < resultaux.size(); k++) {
                                cmbProc = (ComboVO) resultaux.get(k);%>
                        <option value="<%= cmbProc.getId()%>" title="<%= cmbProc.getTitle()%>"><%=cmbProc.getDescripcion()%></option><%}%>
                    </select>
                </td>
            </tr>

            <tr class="titulos">
                <td colspan="8">
                    <table id="listaProcedimientoConductaTratamiento"></table>
                    <div id="pager1"></div> 
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <input type="button" class="small button blue" value="IMPRIMIR" onclick="imprimirProcedimientosMedicamentos('procedimientos')" />
                </td>
                <td colspan="6" align="center">
                    <input type="button" class="small button blue" value="IMPRIMIR ANEXO 3" onclick="imprimirProcedimientosMedicamentos('anexo_3')" />
                </td>
            </tr>
         
    </table>
</div>
<% if(beanSession.usuario.preguntarMenu("HIS0223")){%>
<div id="divSolicitudes">        
    <table width="100%">
        <tr class="titulos">
            <td colspan="6">
                <table id="listaProcedimientoConductaTratamientoS" class="scroll"></table>                 
            </td>
        </tr>
        <tr class="titulos">                                        
            <td style="width: 90%;">
              <center>Justificaci&oacute;n</center>
              <textarea type="text" id="txtJustificacionFolio" rows="2" maxlength="4000" style="width:90%; height: 150px;"
                > </textarea>
                <input style="display: none;" id="txtIdProcedimiento"/>
            </td>                                        
            <td>
                <center>Esperar Procedimiento</center>
              <select id="cmbEsperarProcAutoriza" size="1" style="width:60%" >
                <option value="0">0 Dias</option>
                <option value="10">10 dias</option>
                <option value="20">20 dias</option>
                <option value="30">30 dias (1 mes)</option>
                <option value="60">60 dias (2 meses)</option>
                <option value="90">90 dias (3 meses)</option>
                <option value="120">120 dias(4 meses)</option>
                <option value="150">150 dias(5 meses)</option>
                <option value="180">180 dias(6 meses)</option>
                <option value="210">210 dias(7 meses)</option>
                <option value="240">240 dias(8 meses)</option>
                <option value="270">270 dias(9 meses)</option>
                <option value="300">300 dias(10 meses)</option>
                <option value="330">330 dias(11 meses)</option>
                <option value="365">365 dias(12 meses)</option>
              </select>
            </td>                                                                    
          </tr>
          <tr class="titulos">
              <td colspan="2">
                    <input id="idBtnSolicitAutoriza" title="AUT34" type="button" class="small button blue"
                    value="Generar Solicitud" onClick="modificarCRUD('enviaSolicitud')" />
              </td>                                          
          </tr>                                       
    </table>
</div>
<%}%> 
    <!-- MEdicamentos  -->

    <!--<div id="divIndicacionMedica">
        <table width="100%" cellpadding="0" cellspacing="0" align="center" style="background-color: #E8E8E8;">
            <tr class="estiloImput">
                <td class="titulos" style="width: 15%;">
                    INDICACION
                </td>
                <td style="width: 75%;">
                    <textarea id="txtIndicacionMedica" style="width:95%" maxlength="5000" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea>                    
                </td>
                <td style="width: 10%;">
                    <input id="btnIndicacion" type="button" class="small button blue" value="ADICIONAR" title="btn_adm784" onclick="modificarCRUD('crearIndicacionMedica')" />
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="3">
                    <table id="listIndicaciones"></table>
                </td>
            </tr>
        </table>
    </div>-->

    <div id="divMedicacion">
        <table width="100%" cellpadding="0" cellspacing="0" align="center" style="background-color: #E8E8E8;">
            <tr class="titulosCentrados">
                <td></td>
                <td></td>
                <td>
                    MEDICAMENTOS
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr class="titulosListaEspera">
                <td width="30%">MEDICAMENTO </td>
                <td width="20%">VIA</td>
                <td width="20%">FORMA FARMACEUTICA</td>
                <td width="25%">DOSIS</td>
                <td width="25%">MEDIDA</td>
                <!-- <td width="5%"> -->
                    <!--input id="btnProcedimiento" type="button" class="small button blue" value="Activar" title="btn_adm784" onclick="mostrar('divVentanitaActivarMedicacion')" /-->
                <!-- </td> -->
            </tr>
            <tr class="estiloImput">
                <td align="center">
                    <input type="text" id="txtIdArticulo" style="width:90%" oninput="llenarElementosAutoCompletarKey(this.id, 214, decodeURI(this.value))" onblur="limpiarCombosMedicacionHC()" maxlength="20" >
                    <img width="18px" height="18px" align="middle" title="VEN32"
                     id="idLupitaVentanitaProcmto"
                     onclick="traerVentanita(this.id,'','divParaVentanita', 'txtIdArticulo', '43')"
                     src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td align="center">
                    <select id="cmbIdVia" style="width:90%" >	 
                       <option value=""></option>
                        <% resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(2060);   
                          ComboVO cmb2;
                         for(int k=0;k<resultaux.size();k++){
                         cmb2=(ComboVO)resultaux.get(k);
                         %>
                   <option value="<%= cmb2.getId()%>" title="<%= cmb2.getTitle()%>"><%= cmb2.getDescripcion()%></option>
                      <%}%>    
                            
                    </select>
                </td>
                <td align="center">
                    <select size="1" id="cmbUnidad" style="width:85%" >
                     <option value=""></option>
                        <% resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(2059);   
                          ComboVO cmb11;
                         for(int k=0;k<resultaux.size();k++){
                         cmb11=(ComboVO)resultaux.get(k);
                         %>
                   <option value="<%= cmb11.getId()%>" title="<%= cmb11.getTitle()%>"><%= cmb11.getDescripcion()%></option>
                      <%}%>    
                    </select>
                </td>
                <td align="center">
                    <input id="txtCant" title="Cantidad Dosis" style="width: 50%;" maxlength="3"  onkeypress="javascript:return teclearsoloDigitos(event);" onchange="habilitarMedida();"/> <!-- onchange="calcularCantidadMed(); "/> -->
                </td>
                <td align="center">
                    <select size="1" id="cmbMedida" style="width:85%" >
                        <option value=""></option>
                           <% resultaux.clear();
                             resultaux=(ArrayList)beanAdmin.combo.cargar(10385);   
                             ComboVO cmb12;
                            for(int k=0;k<resultaux.size();k++){
                            cmb11=(ComboVO)resultaux.get(k);
                            %>
                      <option value="<%= cmb11.getId()%>" title="<%= cmb11.getTitle()%>"><%= cmb11.getDescripcion()%></option>
                         <%}%>    
                    </select>
                    <!-- <input id="txtMedida" title="Medida" style="width: 50%;" disabled=""/> -->
                </td>
                <!-- <td >&nbsp;</td> -->
            </tr>
            <tr class="titulosListaEspera">

                <td>FRECUENCIA (HORAS)</td>
                <td>DURACION TRATAMIENTO (DIAS)</td>
                <td>CANTIDAD</td>
                <td>OBSERVACIONES</td>
                <td>
                    
                        <input id="btn073" class="small button blue" value="CARGAR ANTERIORES" title="BTN073::Cargar Permite Cargar Medicamentos e Insumos de Anteriores Atenciones" onclick="cargarVentana('medicamentos')" />
                    
              
                        <input id="btnProcedimiento" type="button" class="small button blue" value="ADICIONAR" title="btn_adm784" onclick="verificarNotificacionAntesDeGuardar('ValidarFrecuenciaCantidad');" />
                    
                   
                  
                </td>
            </tr>
            <tr class="estiloImput">

                <td align="center">
                    <input id="txtFrecuencia" title="Frecuencia en Horas" style="width: 50%;" maxlength="4"  onkeypress="javascript:return teclearsoloDigitos(event);" /><!-- onchange="calcularCantidadMed();"/> -->                    
                    <select id="cmbIdFrecuencia">
                        <option value=""></option>
                        <option value="HORA">HORA</option>
                        <option value="DIA">DÍAS</option>
                        <option value="MES">MES</option>
                    </select>
                </td>
                <td align="center">
                    <input id="txtDias" title="Dias del Tratamiento" style="width: 50%;" maxlength="3"  onkeypress="javascript:return teclearsoloDigitos(event);"/><!-- onchange="calcularCantidadMed();"/> --> 
                    <select id="cmbIdDuracionTratamiento">
                        <option value=""></option>
                        <option value="HORA">HORA</option>
                        <option value="DIA">DÍAS</option>
                        <option value="MES">MES</option>
                    </select>
                </td>
                <td align="center">
                    <input type="number" id="txtCantidad" style="width:60%" onkeypress="javascript:return soloNumeros(event);" onblur="v28(this.value, this.id);" />
                   
                    <select size="1" id="cmbUnidadFarmaceutica" style="width:0%;visibility: hidden;" >
                        <option value="Unidad">Unidad</option>  
                        <option value="">[Unidad_farmaceutica]</option>                     
                        <%     resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(42);
                            ComboVO cmbUnFarm;
                            for (int k = 0; k < resultaux.size(); k++) {
                                cmbUnFarm = (ComboVO) resultaux.get(k);
                        %>
                        <option value="<%= cmbUnFarm.getId()%>" title="<%= cmbUnFarm.getTitle()%>">
                            <%= cmbUnFarm.getDescripcion()%></option>
                            <%}%>

                    </select>
                </td>
                <td align="center">
                    <input type="text" value="" id="txtIndicaciones" style="width:98%" size="20" onblur="v28(this.value, this.id);"  />
                </td>
                <td></td>
            </tr>
            <tr>
                <td> </td>
            </tr>
            <tr class="titulosCentrados">
                <td></td>
                <td></td>
                <td>
                    INSUMOS
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr class="titulosListaEspera">
                <td width="30%">ARTICULO </td>
                <td width="30%">CANTIDAD</td>
                <td width="20%">OBSERVACIONES</td>
                <td></td>
                <td width="20%">
                    <input id="btn073" class="small button blue" value="CARGAR ANTERIORES"
                      title="BTN073::Cargar Permite Cargar Medicamentos e Insumos de Anteriores Atenciones"
                      onclick="cargarVentana('medicamentos')" />
  
                  </td>
              </tr>
              <tr class="estiloImput">
                <td  width="30%" align="center">
                  <input type="text" id="txtIdArticuloInsumo" style="width:90%"
                    oninput="llenarElementosAutoCompletarKey(this.id, 10386, decodeURI(this.value))"
                    onblur="limpiarCombosMedicacionHC()" maxlength="20">
                  <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaProcmto"
                    onclick="traerVentanita(this.id,'','divParaVentanita', 'txtIdArticuloInsumo', '62')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                </td>
                <td width="30%" align="center">
                  <input type="number" id="txtCantidadInsumos" style="width:60%"
                    onkeypress="javascript:return soloNumeros(event);" onblur="v28(this.value, this.id);"/>

                  <select size="1" id="cmbUnidadInsumos" style="width:0%;visibility: hidden;">
                    <option value="Unidad">Unidad</option>
                    <option value="">[Unidad_farmaceutica]</option>
                    <% resultaux.clear(); resultaux=(ArrayList) beanAdmin.combo.cargar(42); ComboVO
                      cmbUnFarm11; for (int k=0; k < resultaux.size(); k++) { cmbUnFarm11=(ComboVO)
                      resultaux.get(k); %>
                      <option value="<%= cmbUnFarm11.getId()%>" title="<%= cmbUnFarm11.getTitle()%>">
                        <%= cmbUnFarm11.getDescripcion()%>
                      </option>
                      <%}%>

                  </select>
                </td>
                <td width="20%" align="center">
                  <input type="text" value="" id="txtIndicacionesInsumos" style="width:98%" size="20"
                    onblur="v28(this.value, this.id);" />
                </td>
                <td></td>                
                <td width="20%">
                    <input id="btnProcedimiento" type="button" class="small button blue" value="ADICIONAR"
                      title="btn_adm785"
                      onclick="verificarNotificacionAntesDeGuardar('ValidarCantidadInsumos');" />
                  </td>
              </tr>

            <!-- grilla -->
            <tr class="titulos">
                <td colspan="8">
                    <table id="listMedicacion" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td>
            </tr>
            <td colspan="8" align="center">
                <input type="button" class="small button blue" value="IMPRIMIR" onclick="imprimirProcedimientosMedicamentos('medicamentos')" />
            </td>
        </table>
    </div>

    <div id="divConciliacionMedicamento">
        <table width="100%">
            <tr class="titulos">
                <td width="50%">Antecedentes Farmacologicos</td>
                <td width="50%">Medicamentos Ordenados</td>
            </tr>
            <tr class="titulos">
                <td>
                    <table id="listAntFarmacologicos" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td>
                <td>
                    <table id="listMedicacionOrdenada" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td>
            </tr>
        </table>

        <table width="100%">
            <tr class="titulos">
                <td width="30%" valign="top">EXISTE INTERACCION MEDICAMENTOSA?
                </td>
                <td width="60%">DESCRIPCION
                </td>
                <td width="10%">&nbsp;
                </td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <select id="cmbExiste" style="width:20%" onchange="selectConciliacionMedicamentosa()" >
                        <option value=""></option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                    </select>
                </td>
                <td>
                    <textarea id="txtConciliacionDescripcion" style="width:95%"  onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"> </textarea>
                </td>
                <td>
                    <input id="btnProcedimiento" type="button" title="BT66R" class="small button blue" value="Guardar" onclick="modificarCRUD('guardaConciliacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>


    <div id="divIncapacidad">
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td>
                    <input type="hidden" id="txtIdClaseLaboratorio" value="4" />    
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Tipo: </td> 
                            <td width="20%">Fecha Inicio:</td> 
                            <td width="10%" id="diasIncapacidad">Dias De Incapacidad:</td> 
                            <td width="20%" id="finalIncapacidad">Fecha Final:</td> 
                                                                                                 
                        </tr>       
                        <tr class="estiloImput"> 
                            <td>
                                <select name="" id="cmbIdTipo" onchange="bloquearCampos()">
                                    <option value="1">INCAPACIDAD</option>
                                    <option value="2">CONSTANCIA</option>
                                    <option value="3">CERTIFICADO</option>
                                </select>
                            </td>
                            <td>
                                <input id="txtFechaIncaInicio" type="text"  maxlength="10" size="10" onchange="calcularFechas()">
                            </td>    
                            <td id="diasIncapacidad">
                                <input id="txtDiasIncapacidad"  type="text"  maxlength="10" size="10" onkeypress='validarNumeros(event)' onchange="calcularFechas()" >
                            </td>                                  
                            <td id="finalIncapacidad">
                              <input id="txtFechaIncaFin" type="datetime"  maxlength="10" size="10"  readonly>
                                <!-- <input type="text" id="txtFechaIncaFin2" disabled> -->
                            </td> 
                           
                            
                        </tr>                                                                            
                    </table>  
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="60%">Motivo:</td>                                                                     
                        </tr>       
                        <tr class="estiloImput">   
                            <td>
                            <textarea id="txtMotivo" rows="5" style="width:90%"  maxlength="8000"   onkeypress="return validarKey(event,this.id)" ></textarea>    
                            </td>   
                        </tr>                         
                    </table>                    
                    <!-- <table width="100%">
                        <tr class="titulos"> 
                            <td width="60%">Comentarios y/o Observaciones:</td>                                                                     
                        </tr>       
                        <tr class="estiloImput">   
                            <td>
                            <textarea id="txtObservaciones" rows="5" style="width:90%"  maxlength="8000"   onkeypress="return validarKey(event,this.id)" ></textarea>
                            </td>   
                        </tr>                         
                    </table>  -->
                    <table width="100%" align="center">
                        <tr class="estiloImput"> 
                            <td>
                                <input id="btnIncapacidad" align="center" type="button" class="small button blue" title="adicion322" value="CREAR"  onclick="modificarCRUD('agregarIncapacidad');"  />  
                                <input id="btnLimpiar" align="center" type="button" class="small button blue" title="adicion35562" value="LIMPIAR"  onclick="limpiarDivEditarJuan('limpiarCamposIncapacidad');"  />                     
                            </td>   
                        </tr>                         
                    </table> 
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listIncapacidad" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>                                                              

                </td>   
            </tr>   
        </table>  
        <div id="divVentanitaIncapacidad"  style=" display:none;  left:211px; z-index:999">

        <div class="transParencia" style="z-index:1000; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                </div>  

        <div   style="z-index:1001; position:fixed; top:200px; left:400px; width:60%">  

            <table width="100%"  border="1"  class="fondoTabla">
                <tr class="estiloImput" >
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaIncapacidad')" /></td>  
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaIncapacidad')" /></td>  
                </tr>     
            
                <tr class="titulos" >
                    <td width="15%" >Id</td>  
                    <td width="15%" >Numero dias</td> 
                    <td width="30%" >Motivo Consulta</td>           
                    <td width="15%" >Fecha Inicio</td> 
                    <td width="55%">Diagnostico Asociado</td>
                    <!-- <td width="60%" >Fecha Fin</td>                           -->

                </tr>           
                <tr class="estiloImput" >
                    <td ><label id="lblIdIncap"></label></td> 
                    <td ><label id="lblNumerodias"></label></td>            
                    <td ><label id="lblMotivo"></label><label ></label></td>
                    <td ><label id="lblFechaI"></label></td> 
             
                    <td width="80%">
                                        
                        <select id="cmbIdDxRelacionadoIncapacidad" style="width:80%" onchange="ValidacionesAIEPI('');
                        actualizarDiagnostico(this.value);"  >
                        <option value=""></option>
                    </select>
               

                </td>
                     <td ><label id="lblIdEvolucionIncapacidad" hidden = 'true'></label></td>  

                </tr> 
                <tr>  
                    <td colspan="2" align="center">
                        <input id="btnEliminarIncapacidad" type="button" class="small button blue" title="B742" value="Elimina Registro" onclick="modificarCRUD('eliminarIncapacidad', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />  
                    </td>     
                    <td colspan="2" align="center">
                        <!-- <input id="btnProcedimientoImprimir"  type="button" class="small button blue" title="B743" value="Imprime" onclick="imprimirReferencia()"  /> -->
                        <input id="btnProcedimientoImprimirInca"  type="button" class="small button blue" title="B743" value="Imprime" onclick="impresionIncapacidad();"  />
                    </td>                    
                </tr>                
            </table> 
        </div>
        </div>
    </div>

    <div id="divRemision">
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td>                    
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Remitido desde:</td>
                            <td width="80%"></td>                                                                                                
                        </tr>       
                        <tr class="estiloImput"> 
                            <td>
                                <input id="txtRemitidoDesde" type="checkbox" onchange="habilitarRemitido();"  checked>
                            </td>
                            <td>                                                            
                                <select style="width: 90%;" id="cmbIpsRemisora">
                                    
                                    <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(3506);
                                    ComboVO cmbIps;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbIps = (ComboVO) resultaux.get(k);%>
                                    <option value="<%= cmbIps.getId()%>" title="<%= cmbIps.getTitle()%>"><%=cmbIps.getDescripcion()%></option><%}%>                                    
                                </select>
                            </td>                            
                        </tr>                                                                            
                    </table>  
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="40%">Medico que Remite:</td>
                            <td width="60%">Especialidad de la cual es Remitido:</td>
                        </tr>       
                        <tr class="estiloImput">   
                            <td>
                                <input id="txtMedicoRemite" type="text" style="width: 70%;">                            
                            </td>
                            <td>
                                <select style="width: 90%;" id="cmbEspecialidadRemite">
                                    <option id="cmbEspecialidadRemite_opcion" value=""></option>
                                    <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(123);
                                    ComboVO cmbProEspR;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                    cmbProEspR = (ComboVO) resultaux.get(k);%>
                                    <option value="<%= cmbProEspR.getId()%>" title="<%= cmbProEspR.getTitle()%>"><%=cmbProEspR.getDescripcion()%></option><%}%>                                    
                                </select>
                            </td>   
                        </tr>                         
                    </table>                    
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="30%">Fecha:</td>
                            <td width="20%">Hora:</td>
                            <td width="50%">Diagnostico por el Cual es Remitido:</td>                                                                   
                        </tr>       
                        <tr class="estiloImput">   
                            <td>
                                <input id="txtFechaRemite" type="date" style="width: 70%;" value="<%=LocalDate.now()%>">                            
                            </td>
                            <td>
                                <input id="txtHoraRemite" type="time" style="width: 70%;" onchange="document.getElementById('txtHoraRemitida').value=this.value">                            
                            </td>   
                            <!-- <td>
                                <input id="txtDXRemite" type="text" style="width: 70%;" oninput="llenarElementosAutoCompletarKey(this.id, 218, this.value,valorAtributo('lblEdadPaciente'),valorAtributo('lblGeneroPaciente'))">
                                <img title="VER52" id="idLupitaVentanita" onclick="traerVentanitaDx(this.id,'','divParaVentanita','txtDXRemite','48')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle">
                            </td>   -->
                            <td>
                                <select id="txtDXRemite" style="width:80%" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');"  onchange = "asignaAtributo('txtDXRemision', document.getElementById(this.id).options[document.getElementById(this.id).selectedIndex].text,1)">
                                    <option value=""></option>
                                </select> 
                            </td>     
                        </tr>                         
                    </table>
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Remitido a:</td>
                            <td width="80%"></td>                                                                                                
                        </tr>       
                        <tr class="estiloImput"> 
                            <td>
                                <input id="txtRemitidoa" type="checkbox" onchange="habilitarRemitir()" checked>
                            </td>
                            <td>                                
                                <select style="width: 90%;" id="cmbIpsRemitida">
                                    <option value=""></option>
                                    <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(3506);
                                    ComboVO cmbIpsR;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbIpsR = (ComboVO) resultaux.get(k);%>
                                    <option value="<%= cmbIpsR.getId()%>" title="<%= cmbIpsR.getTitle()%>"><%=cmbIpsR.getDescripcion()%></option><%}%>                                    
                                </select>
                            </td>                            
                        </tr>                                                                            
                    </table>  
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="40%">Medico que Remite:</td>
                            <td width="60%">Especialidad a la cual se Remite:</td>
                        </tr>       
                        <tr class="estiloImput">   
                            <td>
                                <label style="display: none;" id="txtIdMedicoRemiteR"></label>
                                <input id="txtMedicoRemiteR" type="text" style="width: 70%;">                            
                            </td>
                            <td>
                                <select style="width: 90%;" id="cmbEspecialidadRemitida">
                                    <option value=""></option>
                                    <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(123);
                                    ComboVO cmbProEsp;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                    cmbProEsp = (ComboVO) resultaux.get(k);%>
                                    <option value="<%= cmbProEsp.getId()%>" title="<%= cmbProEsp.getTitle()%>"><%=cmbProEsp.getDescripcion()%></option><%}%>
                                </select>
                            </td>   
                        </tr>                         
                    </table>                    
                    <table width="100%">
                        <tr class="titulos"> 
                            <td width="20%">Fecha:</td>
                            <td width="10%">Hora:</td>
                            <td width="50%">Diagnostico por el Cual se Remite:</td>
                            <td width="20%">Prioridad</td>                                                                   
                        </tr>       
                        <tr class="estiloImput">   
                            <td>
                                <input id="txtFechaRemitida" type="text"  style="width: 70%;">                            
                            </td>
                            <td>
                                <input id="txtHoraRemitida" type="time" style="width: 70%;">                            
                            </td>   
                            <td>
                                <input id="txtDXRemision" type="text" style="width: 70%;" oninput="llenarElementosAutoCompletarKey(this.id, 218, this.value,valorAtributo('lblEdadPaciente'),valorAtributo('lblGeneroPaciente'))"  disabled>
                                <!--<img title="VER52" id="idLupitaVentanita" onclick="traerVentanitaDx(this.id,'','divParaVentanita','txtDXRemision','48')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle">-->
                            </td>
                            <!--<td>
                                <select id="txtDXRemision" style="width:80%" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                                    <option value=""></option>
                                </select>
                            </td>-->
                            <td>
                                <select id="cmbPrioridadRemision">
                                    <option value="1">Urgente</option>
                                    <option value="2">Prioritario</option>
                                    <option value="3">Ambulatorio</option>
                                </select>
                            </td>      
                        </tr>                                                              
                    </table> 
                    <table width="100%">                                
                        <tr class="titulos">
                            <td width="20%">ContraReferencia:</td>
                            <td width="20%">Remisi&oacute;n Pertinente:</td>
                            <td width="20%">Fecha:</td>
                            <td width="10%">Hora:</td>
                            <td width="40%">Diagnostico ContraRemision:</td>                                                                                              
                        </tr>       
                        <tr class="estiloImput">   
                            <td>
                                <input id="txtContra" type="checkbox" onchange="habilitarContra()">
                            </td>
                            <td>
                                <input id="txtPertinente" type="checkbox" disabled>
                            </td>
                            <td>
                                <input id="txtFechaContra" type="date"  style="width: 70%;" disabled>                            
                            </td>
                            <td>
                                <input id="txtHoraContra" type="time" style="width: 70%;" disabled>                            
                            </td>   
                            <td>
                                <input id="txtDxContra" type="text" style="width: 70%;" disabled oninput="llenarElementosAutoCompletarKey(this.id, 218, this.value,valorAtributo('lblEdadPaciente'),valorAtributo('lblGeneroPaciente'))">
                                <img title="VER52" id="idLupitaVentanita" onclick="traerVentanitaDx(this.id,'','divParaVentanita','txtDxContra','48')" src="/clinica/utilidades/imagenes/acciones/buscar.png" width="18px" height="18px" align="middle">                                
                            </td>                                  
                        </tr>
                        <tr class="titulos"> 
                            <td colspan="6">
                                Motivo Remision
                            </td>
                        </tr>
                        <tr>
                            <td class="estiloImput" colspan="6">
                                <textarea id = "txtMotivoRemision" style="width: 721px; height: 86px;"></textarea>
                            </td>
                        </tr>
                    </table>                   
                    <table width="100%" align="center">
                        <tr class="estiloImput"> 
                            <td>
                                <input id="btnRemite" align="center" type="button" class="small button blue" title="adicion322" value="ADICIONAR"  onclick="modificarCRUD('agregarRemision');"  />  
                                <input id="btnEliminar" align="center" type="button" class="small button blue" title="adicion35562" value="ELIMINAR"  onclick="modificarCRUD('eliminarRemision');"  />                     
                            </td>   
                        </tr>                         
                    </table> 
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listRemisiones" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>                                                              

                </td>   
            </tr>   
        </table>          
    </div>

    <!--<div id="divReferenciaYContrareferencia">
        <table width="100%">
            <tr class="titulos" >
                <td colspan="1">&nbsp;Tipo de Evento: </td>
                <td colspan="1" align="left">
                    <select size="1" id="cmbTipoEvento" style="width:60%"   >	                                        
                        <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(300);	
                               ComboVO cmb32; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb32=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>"><%= cmb32.getDescripcion()%></option>
                        <%}%>						
                    </select>           
                </td> 
                <td colspan="2" align="left">Servicio que solicita :
                    <select size="1" id="cmbServicioSolicita" style="width:60%" title="32"  >	  
                        <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(13);	
                               ComboVO cmb36; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb36=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmb36.getId()%>" title="<%= cmb36.getTitle()%>"><%= cmb36.getDescripcion()%></option>
                        <%}%>						
                    </select>      
                </td>            
            </tr>      
            <tr class="titulos" >
                <td width="30%" colspan="2">Servicio para el cual se solicita</td>
                <td width="25%">Prioridad</td> 
                <td width="20%">Nivel</td>                                                        
            </tr>
            <tr class="estiloImput">
                <td width="50%" colspan="2">
                    <select id="cmbServicioParaCual" style="width:70%"  >	  
                        <option value=""></option>
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(301);	
                               ComboVO cmb4; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb4=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmb4.getId()%>" title="<%= cmb4.getTitle()%>"><%= cmb4.getDescripcion()%></option>
                        <%}%> 
                    </select>                   
                </td>
                <td>  
                    <select id="cmbPrioridad" style="width:70%" title="32"  >	  
                        <option value="URGENTE">URGENTE</option>
                        <option value="PRIORITARIO">PRIORITARIO</option>                    
                        <option value="AMBULATORIO">AMBULATORIO</option>                                        
                    </select>
                </td>    
                <td>
                    <select size="1" id="cmbNivelRemis" style="width:80%" title="32"  >	  
                        <option value="I">NIVEL I</option>
                        <option value="II">NIVEL II</option>                    
                        <option value="III">NIVEL III</option>                                        
                        <option value="IV">NIVEL IV</option>                                                        
                    </select>
                </td>                              
            </tr>                
        
            <tr class="titulos" >
                <td colspan="4">DATOS DE LA PERSONA RESPONSABLE DEL PACIENTE</td>     
            </tr> 
            <tr class="estiloImput" >
                <td colspan="4">
                    <TABLE width="100%">
                        <TR>
                            <TD>Tipo Doc</TD><TD>Documento</TD><TD>Primer apellido</TD><TD>Segundo apellido</TD><TD>Primer Nombre</TD><TD>Segundo Nombre</TD>
                        </TR>
                        <TR class="estiloImput">
                            <TD>
                                <select size="1" id="cmbTipoDocResponsable" style="width:99%" title="32"  >	  
                                    <option value="CC">Cedula de Ciudadania</option>
                                    <option value="CE">Cedula de Extranjeria</option>
                                    <option value="MS">Menor sin Identificar</option>
                                    <option value="PA">Pasaporte</option>
                                    <option value="RC">Registro Civil</option>
                                    <option value="TI">Tarjeta de identidad</option>
                                    <option value="CD">Carnet Diplomatico</option>
                                    <option value="NV">Certificado nacido vivo</option>                           
                                    <option value="AS">Adulto sin Identificar</option>                                                              
                                </select>
                            </TD>                
                            <TD><input type="text" id="txtDocResponsable" size="100"  maxlength="100" style="width:50%"/></TD>                                  
                            <TD><input type="text" id="txtPrimerApeResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                 
                            <TD><input type="text" id="txtSegundoApeResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                    
                            <TD><input type="text" id="txtPrimerNomResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                 
                            <TD><input type="text" id="txtSegundoNomResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                 
                        </TR>
                        <TR>
                            <TD>Telefono</TD><TD colspan="3">Direccion</TD><TD>Departamento</TD><TD>Municipio</TD>
                        </TR>
                        <TR class="estiloImput">
                            <TD><input type="text" id="txtTelefonoResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                     
                            <TD colspan="3"><input type="text" id="txtDireccionResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                                      
                            <TD><input type="text" id="txtDeparResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                                                       
                            <TD><input type="text" id="txtMunicResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                                                       
                        </TR>                              
                    </TABLE>           
                </td>             
            </tr>        
        
        
            <tr class="titulos" >
                <td colspan="3">Informacion clinica relevante</td>         
                <td colspan="1">Profesional realiza</td>                    
            </tr> 
            <tr class="estiloImput" >
                <td colspan="3">
                    <textarea rows="5" id="txt_InformacionRemision" maxlength="8000" style="width:95%"  onkeypress="return validarKey(event, this.id)"> </textarea> 
                </td>
                <td valign="top">
                    <select size="1" id="cmbIdProfesionalRemision" style="width:80%" title="26"   >	                                        
                        <option value=""></option>
                        <% resultaux.clear();
                           resultaux=(ArrayList)beanAdmin.combo.cargar(26);	
                           ComboVO cmbE; 
                           for(int k=0;k<resultaux.size();k++){ 
                                 cmbE=(ComboVO)resultaux.get(k);
                        %>
                        <option value="<%= cmbE.getId()%>" title="<%= cmbE.getTitle()%>"><%= cmbE.getDescripcion()%></option>
                        <%}%>                                 
                    </select> 
                    <p>.</p><p></p><p></p><p></p>                                
                    
                </td>        
            </tr>  
        
            <tr class="titulos" >
                <td colspan="4">Lista del chequeo para el traslado y/o referencia del paciente</td>
            </tr>
        
            <tr class="estiloImput">
                <td width="90%" class="estiloImputDer" colspan="3">VERIFICACIÓN DE PACIENTE CORRECTO:</td> 
                <td width="10%" class="estiloImputIzq2">
                    <select size="1" id="cmbVerificacionPaciente" style="width:40%;" title="32" onblur=" "   >	  
                        <option value=""></option>
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>
                        <option value="NA">NA</option>
                    </select>
                </td>
            </tr> 	
        
            <tr class="estiloImput">
                <td width="90%" class="estiloImputDer" colspan="3">FORMATO DILIGENCIADO DE REFERENCIA Y CONTRAREFERENCIA</td> 
                <td width="10%" class="estiloImputIzq2">
                    <select size="1" id="cmbFormatoDiligenciado" style="width:40%;" title="32" onblur=" "   >	  
                        <option value=""></option>
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>
                        <option value="NA">NA</option>
                    </select>
                </td>
            </tr> 
        
        
            <tr class="estiloImput">
                <td width="90%" class="estiloImputDer" colspan="3">RESUMEN DE HISTORIA CLINICA</td> 
                <td width="10%" class="estiloImputIzq2">
                    <select size="1" id="cmbResumenHc" style="width:40%;" title="32" onblur=" "   >	  
                        <option value=""></option>
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>
                        <option value="NA">NA</option>
                    </select>
                </td>
            </tr> 
        
            <tr class="estiloImput">
                <td width="90%" class="estiloImputDer" colspan="3">DOCUMENTO DE IDENTIDAD Y/O CARNET DE AFILIACIÓN</td> 
                <td width="10%" class="estiloImputIzq2">
                    <select size="1" id="cmbDocumentoIdentidad" style="width:40%;" title="32" onblur=" "   >	  
                        <option value=""></option>
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>
                        <option value="NA">NA</option>
                    </select>
                </td>
            </tr> 
        
            <tr class="estiloImput">
                <td width="90%" class="estiloImputDer" colspan="3">RESULTADOS DE IMÁGENES DIAGNOSTICAS (SI APLICA)</td> 
                <td width="10%" class="estiloImputIzq2">
                    <select size="1" id="cmbImagenDiagnostica" style="width:40%;" title="32" onblur=" "   >	  
                        <option value=""></option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <option value="NA">NA</option>
                    </select>
                </td>
            </tr> 
        
            <tr class="estiloImput">
                <td width="90%" class="estiloImputDer" colspan="3">PACIENTE LLEVA MANILLA DE IDENTIFICACION LEGIBLE (UNICAMENTE APLICA A PACIENTE CON REFERENCIA AL SERVICIO DE URGENCIAS)</td> 
                <td width="10%" class="estiloImputIzq2">
                    <select size="1" id="cmbPacienteManilla" style="width:40%;" title="32" onblur=" "   >	  
                        <option value=""></option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <option value="NA">NA</option>
                    </select>
                </td>
            </tr> 
        
            <tr>
                <td colspan="4" align="center">
                    <input id="btnProcedimiento_NO"  type="button" class="small button blue" title="BTN8D1" value="CREAR"  onclick="modificarCRUD('listRemision', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"/>
                </td>
            </tr>
        
            <tr class="titulos">
                <td colspan="4">                                    
                    <table id="listRemision" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                </td>
            </tr>
        </table>   -->
        
        
        
         
        <!-- cambia aqui y debe cambiar en referencia.jsp-->
        <!--<div id="divVentanitaRemision"  style=" display:none;  left:211px; z-index:999">
        
         <div class="transParencia" style="z-index:1000; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                </div>  
        
        <div   style="z-index:1001; position:fixed; top:200px; left:400px; width:50%">  
        
            <table width="100%"  border="1"  class="fondoTabla">
                <tr class="estiloImput" >
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaRemision')" /></td>  
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaRemision')" /></td>  
                </tr>     
                <tr class="titulos" >
                    <td width="15%" >Id</td>  
                    <td width="15%" >Fecha Evento</td> 
                    <td width="10%" >Tipo</td>           
                    <td width="60%" >Servicio que solicita</td>                          
                </tr>           
                <tr class="estiloImput" >
                    <td ><label id="lblIdRemi"></label></td> 
                    <td ><label id="lblFechaEvento"></label></td>            
                    <td ><label id="lblIdTipoRemi"></label>-<label id="lblNomTipoRemi"></label></td>
                    <td ><label id="lblNombreElementoRemi"></label></td> 
                </tr> 
                <tr>  
                    <td colspan="2" align="center">
                        <input id="btnEliminarMed" type="button" class="small button blue" title="B742" value="Elimina Registro" onclick="modificarCRUD('eliminarRemision', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />  
                    </td>     
                    <td colspan="2" align="center">-->
                        <!-- <input id="btnProcedimientoImprimir"  type="button" class="small button blue" title="B743" value="Imprime" onclick="imprimirReferencia()"  /> -->
                        <!--<input id="btnProcedimientoImprimir"  type="button" class="small button blue" title="B743" value="Imprime" onclick="impresionReferencia();"  />
                    </td>                    
                </tr>                
            </table> 
        </div>
        </div>  -->   
        
          
        
        <!-- cambia aqui y debe cambiar en referencia.jsp-->
        <!--<div id="divVentanitaLaboratorio"  style="position:absolute; display:none; background-color:#CCC; top:310px; left:150px; width:900px; height:200px; z-index:999">
            <table width="100%"  border="1"  class="fondoTabla">
                <tr class="estiloImput" >
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaLaboratorio')" /></td>  
                    <td>&nbsp;</td>
                    <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaLaboratorio')" /></td>  
                </tr>     
                <tr class="titulos" >
                    <td width="15%" >NOMBRE</td>  
                </tr>           
                <tr class="estiloImput" >
                    <td><input type="text" id="txtNombreElemento" size="100"  maxlength="100" style="width:50%"/></td> 
                </tr> 
                <tr>  
                    <td colspan="2" align="center">
                        <input id="btnCrearMed" type="button" class="small button blue" title="B742" value="CREAR" onclick="modificarRT('crearLaboratorio');" />  
                    </td>     
                </tr>                
            </table> 
        </div>  
    </div>-->

    <!--<div id="divConsentimientos">
        <table width="100%">
            <tr class="titulosListaEspera">
                <td align="CENTER">
                    Tipo
                    <select style="width:40%" id="cmbTipoConsentimiento">
                        <option value=""></option>
                        <%     resultaux.clear();
                            resultaux = (ArrayList) beanAdmin.combo.cargar(151);
                            ComboVO opt;
                            for (int k = 0; k < resultaux.size(); k++) {
                                opt = (ComboVO) resultaux.get(k);
                        %>
                        <option value="<%= opt.getId()%>" title="<%= opt.getTitle()%>">
                            <%= opt.getDescripcion()%></option>
                            <%}%>
                    </select>
                </td>
                <td>
                    <input type="button" class="small button blue" id="btnCrearConsentimiento" value="CREAR" onclick="modificarCRUD('crearConsentimiento')" />
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="2">
                    <table id="listConsentimientosPaciente"></table>
                </td>
            </tr>
        </table>        
        <table width="100%" style="background-color: #E8E8E8;">
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px" width="30%">
                    1. Diagn&oacutesticos y sus complicaciones sobre la salud del paciente:
                </td>
                <td>
                    <textarea type="text" id="txtTexto1" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    2. Descripci&oacuten simple del procedimiento que se plantea, sus objetivos y sus beneficios:
                </td>
                <td>
                    <textarea type="text" id="txtTexto2" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    3. Riesgos del procedimiento, molestias, efectos adversos, y posibles complicaciones:
                </td>
                <td>
                    <textarea type="text" id="txtTexto3" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    4. Especificaci&oacuten de procedimientos alternativos, sus riesgos y beneficios frente al procedimiento planteado:
                </td>
                <td>
                    <textarea type="text" id="txtTexto4" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')"></textarea>
                </td>
            </tr>
            <tr>
                <td class="estiloImputDer" align="left" style="padding:10px">
                    5. Las consecuencias previsibles de NO realizar el procedimiento propuesto o sus alternativas:
                </td>
                <td>
                    <textarea type="text" id="txtTexto5" rows="3" style="width:95%" onblur="modificarCRUD('guardarInfoConsentimiento')" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
            </tr>
        </table>
        <div align="CENTER">
            <input type="button" class="small button blue" value="GUARDAR" onclick="modificarCRUD('guardarInfoConsentimientoFinal')" />
        </div>        
    </div>-->

    <!--<div id="divAdministracionPaciente">
        <table width="100%" style="background-color: #E8E8E8;">
            <tr class="titulosListaEspera">
                <td width="15%">Ordenes de Servicios</td>
                <td width="15%">Tipo de Alta</td>
                <td width="20%">Observaciones</td>
                <td width="5%"></td>
                <td width="10%" align="left">Ver Ordenes Hist&oacute;ricas</td>
            </tr>
            <tr class="estiloImput">
                <td>
                    <select id="cmbOrdenenesAdmisionURG" style="width: 95%;" onchange="reaccionAEvento(this.id);">
                        <option></option>
                    </select>
                </td>
                <td>
                    <select id="cmbTipoAlta" style="width: 95%;" onchange="reaccionAEvento(this.id)">
                    </select>
                </td>
                <td>
                    <textarea id="txtObservacionOrdenURG" placeholder="Observaciones" style="width: 95%;" onkeypress="return validarKey(event,this.id)"></textarea>
                </td>
                <td align="left">
                    <input type="button" class="small button blue" value="ENVIAR" onclick="modificarCRUD('enviarOrdenMedicaURG')"/>
                </td>
                <td> 
                    <label>
                        <div class="onoffswitch">
                            <input type="checkbox" checked class="onoffswitch-checkbox" id="sw_historicos" onchange="buscarHC('listOrdenesPacienteURG', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp')">
                            <label class="onoffswitch-label" for="sw_historicos">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </label>

                </td>
            </tr>
            <tr id="trInfoMuerte" hidden>
                <td colspan="5">
                    <table width="100%">
                        <tr class="titulosListaEspera">
                            <td width="50%">Causa B&aacute;sica de Muerte</td>
                            <td width="50%">Fecha y Hora de Muerte</td>
                        </tr>
                        <tr class="estiloImput">
                            <td>
                                <input type="text" id="txtIdDxMuerte" style="width: 90%;" readonly>
                                <img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
                                     onclick="traerVentanita(this.id, '', 'divParaVentanita', 'txtIdDxMuerte', '5')"
                                     src="/clinica/utilidades/imagenes/acciones/buscar.png">
                            </td>
                            <td>
                                <table width="100%">
                                    <tr class="titulos">
                                        <td width="15%">Fecha y Hora Auto.</td>
                                        <td width="35%">Fecha</td>
                                        <td width="35%">Hora</td>
                                    </tr>
                                    <tr>
                                        <td align="CENTER">
                                            <label>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" class="onoffswitch-checkbox" id="sw_fecha_actual" onchange="reaccionAEvento(this.id)">
                                                    <label class="onoffswitch-label" for="sw_fecha_actual">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </label>
                                        </td>
                                        <td align="center">
                                            <input type="text" id="txtFechaMuerte">
                                        </td>
                                        <td align="center">
                                            <input type="number" min="1" max="12" id="txtHoraMuerte" style="width: 30%;">
                                            <input type="number" min="0" max="59" id="txtMinutoMuerte" style="width: 30%;">
                                            <select id="cmbPeriodoMuerte" style="width: 20%;">
                                                <option value=""></option>
                                                <option value="AM">AM</option>
                                                <option value="PM">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="titulos">
                <td colspan="5">
                    <table id="listOrdenesPacienteURG" width="100%"></table>
                </td>
            </tr>
        </table>
    </div>-->

    <div id="divImpresion">
        <table width="100%">
            <tr class="titulosListaEspera">
                <td colspan="9" >OPCIONES PARA IMPRIMIR PLAN DE TRATAMIENTO</td>
            </tr>
            <tr class="estiloImput">
                <td colspan="2" align="right">
                </td>
                <td colspan="1" align="center">
                    <select id="txt_MD_PRO" style="width:50%;" title="32"  onchange="mostrarx(this.value);">	  
                        <option value="formula_medica">Medicamentos</option>
                        <option value="formula_procedimientos">Procedimientos diagnosticos y terapeuticos</option>                                    
                    </select>
                </td>
                <td colspan="1" align="right">Clase:
                    <select size="1" id="txt_ES_POS" style="width:40%;" title="32" >                        
                        <option value="POS">PBS</option>
                        <option value="NOPOS">NO PBS</option>                                    
                    </select>
                </td>
                
                <td colspan="1" align="right">
                    <input type="button" class="small button blue" value="Imprimir" title="btn_iju" onclick="imprimirPDFAnexos(valorAtributo('txt_MD_PRO'))"  /> 
                </td>
                <!--td colspan="1" align="center">
                    <input id="btnFormulaOftalmica_" type="button" class="small button blue" value="Imp Formula Oft." title="btn_gfo" onClick="formulaPDFHC();"  />
                </td-->
                <td colspan="1" align="right">
                    <!--<input id="btnProcedimientoImprimir_" title="btn_u48p" type="button" class="small button blue" value="Imprime Anexo 3 para Procedimientos POS" onclick="imprimirPDFAnexos('anexo3')"  />-->
                </td>
                <td colspan="1" align="right">
                    <!--<input id="btnImprimirConstancia" type="button" class="small button blue" value="Imprimir Constancia" title="btn_impC" onclick="impresionConstancia()"  />-->
                </td>
                <td colspan="1" align="right">
                    <!-- <input id="btnLinkMipress" type="button" class="small button blue" value="MIPRESS" title="btn_MIPRESS" onclick="window.location.href='http://google.com'" target="_blank"  /> -->
                    <input id="btnLinkMipress" type="button" class="small button blue" value="MIPRESS" title="btn_MIPRESS" onclick="
                    window.open('https://mipres.sispro.gov.co/MIPRESNOPBS/Login.aspx',
                    '_blank' // <- This is what makes it open in a new window.
                    );
                    "   />
                </td>

            </tr>
        </table>
        <script>
            function mostrarx(id) {
                if(id=="Constancia"){
                $("#ImprimirC").show();
               // $("#btnProcedimientoImprimir").hide();
               document.getElementById('#ImprimirC').style.visibility = "visible";
                }
            }
        </script>
    </div>

    

    <!--<div id="divControl" style="width:99%; height:1100px">
        <table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td height="1100px">
                    <div id="divParaPaginaListaEspera" style="height:1100px"></div>
                </td>
            </tr>
        </table>
    </div>-->


    <div id="divVentanitaMedicamentosNoPos" style="display:none; z-index:2055; top:1px; left:190px;">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"> </div>
        <div style="z-index:2057; position:absolute; top:50px; left:15px; width:100%">
            <table width="100%" class="fondoTabla" align="center">
                <tr class="estiloImput">
                    <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaMedicamentosNoPos')" /></td>
                    <td>&nbsp;</td>
                    <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onClick="ocultar('divVentanitaMedicamentosNoPos')" /></td>
                </tr>
                <tr>
                    <td width="100%" colspan="3">
                        <div id="divParaPaginaNoPos"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="divVentanitaEditProcedimientos" style="position:absolute; display:none; top:0px; left:0px; width:600px; height:90px; z-index:2055">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"></div>
        <div style="z-index:2057; position:absolute; top:100px; left:200px; height:auto; width:800px">

            <table width="100%" class="fondoTabla">

                <tr class="estiloImput">
                    <td colspan="2" align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditProcedimientos')" /></td>
                    <td colspan="2" align="right"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditProcedimientos')" /></td>
                <tr>

                <tr class="titulosListaEspera">
                    <td width="10%">ID: <label id="lblIdProcedimientoEditar"></label></td>
                    <td width="15%">CUPS: <label id="lblCupsProcedimientoEditar"></label></td>
                    <td width="45%"> <label id="lblProcedimientoEditar"></label> </td>
                    <td width="30%">CLASE: <label id="lblProcedimientoClaseEditar"></label></td>
                </tr>


                <tr class="titulosListaEspera">
                    <td>CANTIDAD</td>
                    <td>GRADO NECESIDAD</td>
                    <td>INDICACI&Oacute;N</td>
                    <td>DIAGNOSTICO ASOCIADO</td>
                </tr>

                <tr class="estiloImput">
                    <td>
                        <select id="cmbCantidadProcedimientoEditar" style="width:90%">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                        </select>
                    </td>

                        <td>
                            <select id="cmbNecesidadProcedimientoEditar" style="width:90%" >
                                <option value=""></option>
                                <%resultaux.clear();
                                    resultaux = (ArrayList) beanAdmin.combo.cargar(113);
                                    ComboVO cmbProE;
                                    for (int k = 0; k < resultaux.size(); k++) {
                                        cmbProE = (ComboVO) resultaux.get(k);%>
                                <option value="<%= cmbProE.getId()%>" title="<%= cmbProE.getTitle()%>"><%=cmbProE.getDescripcion()%></option><%}%>           
                            </select> 
                        </td>

                    <td>
                        <textarea id="txtIndicacionProcedimientoEditar" style="width:90%" maxlength="5000"  onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea>
                    </td>
                    <td>
                        <select id="cmbProcedimientoDiagnotiscoEditar" style="width:95%; overflow-y: auto;"  onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');">
                            <option value=""></option>
                        </select>

                    </td>

                </tr>

                <tr>
                    <td colspan="4" align="center">
                        <input id="btnTratamientoEditar" type="button" class="small button blue" value="EDITAR" onclick="modificarCRUD('listProcedimientosDetalleEditar');" />
                        <input type="button" onclick="modificarCRUD('listProcedimientosDetalleEliminar');" value="ELIMINAR" title="ELIMI002" class="small button blue" id="btnProcedimientoEliminar">
                    </td>
                </tr>

            </table>

        </div>
    </div>


   

    <div id="divVentanitaProcedimientosHistoricos" style="position:absolute; display:none; background-color:#E2E1A5; top:1px; left:5px; width:900px; height:180px; z-index:999">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"> </div>
        <div style="z-index:2057; position:absolute; top:300px; left:50px; height:1000; width:100%">
            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaProcedimientosHistoricos')" /></td>
                    <td align="center">HISTORICOS DE ORDENES DE PLAN DE TRATAMIENTO</td>
                    <td colspan="1" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaProcedimientosHistoricos')" /></td>
                <tr>
                <tr class="titulos">
                    <td colspan="3">
                        <table id="listProcedimientosHistoricos" class="scroll" align="center"></table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="divVentanitaMedicacion" style="display:none; top:350px; left:350px; z-index:999">
        <div class="transParencia" style="z-index:2000; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2001; position:fixed; top:200px; left:400px; width:45%">
            <table width="100%" border="1" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaMedicacion')" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="1" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaMedicacion')" /></td>
                <tr>
                <tr class="titulos ">
                    <td width="10%">ID Orden</td>
                    <td width="10%" >ID Evolucion</td>
                    <td width="10%" >ID Elemento</td>
                    <td width="10%" >Clase</td>
                    <td width="60%">NOMBRE</td>
                <tr>
                <tr class="estiloImput">
                    <td><label id="lblId"></label></td>
                    <td><label id="lblIdEvolucion"></label></td>
                    <td><label id="lblIdElemento"></label></td>
                    <td><label id="lblClaseElemento"></label></td>
                    <td><label id="lblNombreElemento"></label></td>
                <tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="4" align="center">

                        <input id="btnProcedimiento" type="button" class="small button blue" value="Elimina" onclick="modificarCRUD('eliminarMedicacion', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />

                        <input id="btnModificarMedicamento" title="UD75" type="button" class="small button blue" value="Editar" onclick="ocultar('divVentanitaMedicacion');mostrar('divVentanitaEditMedicamentos');"  />

                        <input id="btnProcedimientoImprimir" title="UJ74" type="button" class="small button blue" value="Imprime justificaci&oacute;n" onclick="imprimirAnexoNoPos()"  />

                <tr>
            </table>
        </div>
    </div>



    <div id="divVentanitaActivarMedicacion" style="display:none; top:300px; left:200px; z-index:999">
        <div class="transParencia" style="z-index:2000; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2001; position:fixed; top:150px; left:300px; width:45%">

            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../suministros/articulo/articuloOrdenMedica.jsp" flush="true">
                    <jsp:param name="titulo" value="ADMISIONES" />
                </jsp:include>
            </div>

        </div>
    </div>



    <div id="divVentanitaEditMedicamentos" style="display:none;  top:350px; left:200px; z-index:999">
        <div class="transParencia" style="z-index:2056; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2057; position:fixed; top:200px; left:400px; width:60%">

            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left">
                        <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditMedicamentos')" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="1" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditMedicamentos')" /></td>
                <tr>
                <tr class="titulos">
                    <td align="center"><b>Id Orden: </b><label id="lblIdEditar"></label> </td>
                    <td align="center"><b>Id Articulo: </b><label id="lblIdElementoEditar"></label> </td>

                    <td colspan="2">
                        <b>Articulo: </b><label id="lblNombreElementoEditar"></label></td>
                </tr>
                <tr>
                    <td colspan="4"><br/></td>
                </tr>
                <tr align="center">
                    <td width="20%">Via</td>
                    <td width="20%">Forma Farmaceutica</td>
                    <td width="20%">Dosis</td>
                    <td width="20%">Medida</td>
                    <td width="20%">Frecuencia(Horas)</td>
                </tr>

                <tr>

                    <td align="center">
                        <select tabindex = "1" id="cmbIdViaEditar" style="width:90%" >	 
                            <option value=""></option>
                            <% resultaux.clear();
                          resultaux=(ArrayList)beanAdmin.combo.cargar(2060);   
                          ComboVO cmbViaEdit;
                         for(int k=0;k<resultaux.size();k++){
                         cmbViaEdit=(ComboVO)resultaux.get(k);
                         %>
                   <option value="<%= cmbViaEdit.getId()%>" title="<%= cmbViaEdit.getTitle()%>"><%= cmbViaEdit.getDescripcion()%></option>
                      <%}%>    
                        </select>
                    </td>
                    <td align="center">
                        <select tabindex = "2" size="1" id="cmbUnidadEditar" style="width:85%" onFocus="comboFormaFarmaceuticaEditar()" >                            
                            <option value=""></option>
                        </select>
                    </td>
                    <td align="center">
                        <input tabindex = "3" id="cmbCantEditar" title="Cantidad Dosis" style="width: 50%;" maxlength="3"  onkeypress="javascript:return teclearsoloDigitos(event);" /> <!-- onchange="calcularCantidadMedEditar()" /> -->
                    </td>
                    <td align="center">
                        <select size="1" id="cmbMedidaEditar" style="width:85%" >
                            <option value=""></option>
                               <% resultaux.clear();
                                 resultaux=(ArrayList)beanAdmin.combo.cargar(10385);   
                                 ComboVO cmb13;
                                for(int k=0;k<resultaux.size();k++){
                                cmb11=(ComboVO)resultaux.get(k);
                                %>
                          <option value="<%= cmb11.getId()%>" title="<%= cmb11.getTitle()%>"><%= cmb11.getDescripcion()%></option>
                             <%}%>    
                        </select>
                        <!-- <input tabindex = "3" id="cmbMedidaEditar" title="Cantidad_Medida" style="width: 50%;" maxlength="3"  onkeypress="javascript:return teclearsoloDigitos(event);" /> --><!-- onchange="calcularCantidadMedEditar()" /> --> 
                    </td>
                    <td align="center" >
                        <input tabindex = "4" id="cmbFrecuenciaEditar" title="Frecuencia en Horas" style="width: 50%;" maxlength="4"  onkeypress="javascript:return teclearsoloDigitos(event);"  /> <!-- onchange="calcularCantidadMedEditar()" /> -->
                        <select id="cmbIdFrecuenciaSelect">
                            <option value=""></option>
                            <option value="HORA">HORA</option>
                            <option value="DIA">DÍAS</option>
                            <option value="MES">MES</option>
                        </select>
                    </td>

                </tr>


                <tr align="center">
                    <td>Duracion tratamiento (Dias)</td>
                    <td>Cantidad</td>
                    <td colspan="2">Indicaciones</td>
                </tr>

                <tr>

                    <td align="center">
                        <input tabindex = "5"type="text" id="cmbDiasEditar" style="width:50%" maxlength="3"  onkeypress="javascript:return teclearsoloDigitos(event);" /> <!-- onchange="calcularCantidadMedEditar()" /> -->
                        <select id="cmbDiasEditarSelect">
                            <option value=""></option>
                            <option value="HORA">HORA</option>
                            <option value="DIA">DÍAS</option>
                            <option value="MES">MES</option>
                        </select>
                    </td>
                    <td align="center">
                        <input tabindex = "6" type="text" value="" id="txtCantidadEditar" style="width:20%" size="20" onkeypress="javascript:return soloNumeros(event)" onblur="v28(this.value, this.id);" />

                        <select tabindex = "7" size="1" id="cmbUnidadFarmaceuticaEditar" style="width:70%" >
                            <option value="">[Unidad_farmaceutica]</option>                     
                            <%     resultaux.clear();
                                resultaux = (ArrayList) beanAdmin.combo.cargar(42);
                                ComboVO cmbUnFarm2;
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmbUnFarm2 = (ComboVO) resultaux.get(k);
                            %>
                            <option value="<%= cmbUnFarm2.getId()%>" title="<%= cmbUnFarm2.getTitle()%>">
                                <%= cmbUnFarm2.getDescripcion()%></option>
                                <%}%>

                        </select>
                    </td>
                    <td colspan="2" align="center">
                        <input tabindex = "8" type="text" value="" id="txtIndicacionesEditar" style="width:80%" size="20" onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)" />
                    </td>

                </tr>

                <tr>
                    <td colspan="4"><br/></td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <input id="btnTratamientoEditar" type="button" class="small button blue" title="bot174" value="Editar" onclick="if (document.getElementById('lblClaseElemento').textContent.trim() === 'IN') {
                            verificarNotificacionAntesDeGuardar('ValidarCantidadEditarInsumos');
                        } else {
                            verificarNotificacionAntesDeGuardar('ValidarFrecuenciaCantidadEditar');
                        }"  />

                    </td>
                </tr>

            </table>

        </div>
    </div>
    <%}%>

    <div id="divEducacionPaciente">
        <table width="100%"  cellpadding="0" cellspacing="0"  align="center">
            <tr>
                <td>
                    <!-- <input type="hidden" id="txtIdClaseLaboratorio" value="4" />     -->
                    <table width="100%">
                             
                        <tr class="estiloImput">   
                            <td>
                            <textarea id="txtObservacionesEducacion" rows="9" style="width:90%; border:1px solid #4297d7;"  maxlength="18000"   onblur="v28(this.value,this.id);" onkeypress="return validarKey(event,this.id)" ></textarea>
                            </td>   
                        </tr>                         
                    </table> 
                    <table width="100%" align="center">
                        <tr class="estiloImput"> 
                            <td>
                                <input id="btnEducacionPac" align="center" type="button" class="small button blue" title="Permite adjuntar las recomendaciones en la historia clinica" value="Crear"  onclick="modificarCRUD('agregarEducacionPaciente');"  />  
                                <input id="btnLimpiar" align="center" type="button" class="small button blue" title="limpia la caja de texto a ingresar" value="Limpiar"  onclick="limpiaAtributo('txtObservacionesEducacion', 0);"  />                     
                            </td>   
                        </tr>                         
                    </table> 
                    <table  width="100%" border="1" cellpadding="1" cellspacing="1" align="center" >                          
                        <tr class="titulos">
                            <td>                                    
                                <table id="listGrillaEducacionPac" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
                            </td>
                        </tr>
                    </table>                                                              

                </td>   
            </tr>   
        </table> 

    </div>
    
    <div id="divCargarMedicamentos" style="display:none;  top:350px; left:200px; z-index:999">
        <div class="transParencia" style="position: fixed; top:0px;z-index:2056; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2057; position:fixed; top:200px; width:60%">
            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left" style="width: 25%;">
                        <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="cerrarVentana('medicamentos')" /></td>
                    <td style="width: 25%;">&nbsp;</td>
                    <td style="width: 25%;">&nbsp;</td>
                    <td colspan="1" align="right" style="width: 25%;"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="cerrarVentana('medicamentos')" /></td>
                <tr>
                <tr class="titulos">
                        <td colspan="1" align="left">
                            Tipo de Historia Clinica                                                        
                        </td>                    
                        <td class="estiloImput">
                            <select id="cmbHcPacienteMedicamentos" onfocus="cargarFoliosPaciente(this.id, 'lblIdPaciente','medicamentos');">
                            </select>
                        </td>
                        <td class="estiloImput">
                            <input type="button" class="small button blue" value="BUSCAR" onclick="buscarHC('listMedicamentosPaciente')">
                        </td>
                </tr>
                <tr class="titulos" colspan="4">
                    <td colspan="4">
                        <table id="listMedicamentosPaciente" class="scroll"></table>
                    </td>
                </tr>
                <tr class="titulos" colspan="4">
                    <td colspan="4">
                        <table id="listGrillaCargarMedicamentos" class="scroll"></table>
                    </td>
                </tr>
                <tr>
                    <td class="estiloImput" colspan="4">
                        <input type="button" class="small button blue" value="ADICIONAR" onclick="agregarMedicamentos()">
                    </td>

                </tr>
                
            </table>

        </div>
    </div>

    <div id="divCargarProcedimientos" style="display:none;  top:350px; left:200px; z-index:999">
        <div class="transParencia" style="position: fixed; top:0px; z-index:2056; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2057; position:fixed; top:200px; width:60%">
            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left" style="width: 25%;">
                        <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="cerrarVentana('procedimientos')" /></td>
                    <td style="width: 25%;">&nbsp;</td>
                    <td style="width: 25%;">&nbsp;</td>
                    <td colspan="1" align="right" style="width: 25%;"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="cerrarVentana('procedimientos')" /></td>
                <tr>
                <tr class="titulos">
                    <td colspan="1" align="left">
                        Tipo de Historia Clinica                                                
                    </td>                    
                    <td>
                        <select id="cmbHcPacienteProcedimentos" onfocus="cargarFoliosPaciente(this.id, 'lblIdPaciente','procedimiento');">
                        </select>
                    </td>
                    <td>
                        <input type="button" class="small button blue" value="BUSCAR" onclick="buscarHC('listProcedimientosPaciente')">
                    </td>
                </tr>
                <tr class="titulos" colspan="4">
                    <td colspan="4">
                        <table id="listProcedimientosPaciente" class="scroll" colspan="4"></table>
                    </td>
                </tr>    
                <tr class="titulos" colspan="4">
                    <td colspan="4">
                        <table id="listGrillaCargarProcedimientos" class="scroll" colspan="4"></table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table width="50%">

                            <tr class="titulosListaEspera">
                                <td>
                                    <div >      
                                        DIAGNOSTICO ASOCIADO: 
                                    </div>
                
                                    </td>
                                   
                                    
                                    <td width="50%">
                                        
                                            <select id="cmbIdDxRelacionadoCarga" style="width:80%" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                                            <option value=""></option>
                                        </select>
                                   
                
                                    </td>
                                <td class="estiloImput">
                                    <input type="button" class="small button blue" value="ADICIONAR" onclick="agregarProcedimientos()">
                                </td>
    
                            </tr>
                        </table>
                    </td>
                    
                    

                </tr>            
            </table>

        </div>
    </div>

    <div id="divCargarPaqProcedimientos" style="display:none;  top:350px; left:200px; z-index:999">
        <div class="transParencia" style="position: fixed; top:0px; z-index:2056; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>
        <div style="z-index:2057; position:fixed; top:200px;  width:60%">
            <table width="100%" class="fondoTabla">
                <tr class="estiloImput">
                    <td colspan="1" align="left" style="width: 25%;">
                        <input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divCargarPaqProcedimientos')" /></td>
                    <td style="width: 25%;">&nbsp;</td>
                    <td style="width: 25%;">&nbsp;</td>
                    <td colspan="1" align="right" style="width: 25%;"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divCargarPaqProcedimientos')" /></td>
                <tr>
                <tr class="titulos">
                    <td colspan="1" align="left">
                        PAQUETE
                    </td>                    
                    <td>
                        <select id="cmbPaquetes">
                            <option value=""></option>
                            <%resultaux.clear();
                                resultaux = (ArrayList) beanAdmin.combo.cargar(3000);
                                ComboVO cmbPq;
                                for (int k = 0; k < resultaux.size(); k++) {
                                    cmbPq = (ComboVO) resultaux.get(k);%>
                            <option value="<%= cmbPq.getId()%>" title="<%= cmbPq.getTitle()%>"><%=cmbPq.getDescripcion()%></option><%}%>      
                        </select>
                    </td>
                    <td>
                        <input type="button" class="small button blue" value="BUSCAR" onclick="buscarHC('listPaqueteProcedimientos')">
                    </td>
                </tr>
                <tr class="titulos" colspan="4">
                    <td colspan="4">
                        <table id="listPaqueteProcedimientos" class="scroll" colspan="4"></table>
                    </td>
                </tr>                
                <tr>
                    <td colspan="4">
                        <table width="50%">

                            <tr class="titulosListaEspera">
                                <td>
                                    <div >      
                                        DIAGNOSTICO ASOCIADO: 
                                    </div>
                
                                    </td>
                                   
                                    
                                    <td width="50%">
                                        
                                            <select id="cmbIdDxRelacionadoProcedimientos" style="width:80%" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                                            <option value=""></option>
                                        </select>
                                   
                
                                    </td>
                                <td class="estiloImput">
                                    <input type="button" class="small button blue" value="ADICIONAR" onclick="agregarPaqProcedimientos()">
                                </td>
    
                            </tr>
                        </table>
                    </td>
                    
                    

                   <!--  <td>
                    <div class="titulosListaEspera">      
                        DIAGNOSTICO ASOCIADO: 
                    </div>

                    </td>
                   
                    
                    <td>
                        
                        <div class="titulosListaEspera">      
                            <select id="cmbIdDxRelacionadoProcedimientos" style="width:80%" onfocus="cargarDiagnosticoRelacionado(this.id, 'lblIdDocumento');" >
                            <option value=""></option>
                        </select></div>
                   

                    </td>
                    <td class="estiloImput" colspan="4">
                        <input type="button" class="small button blue" value="ADICIONAR" onclick="agregarPaqProcedimientos()">
                    </td> -->

                </tr>            
            </table>

        </div>
    </div>

    <div id="divVentanitaEditIndicaciones" style="position:absolute; display:none; top:0px; left:0px; width:600px; height:90px; z-index:2055">
        <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60"></div>
        <div style="z-index:2057; position:absolute; top:100px; left:200px; height:auto; width:800px">

            <table width="100%" class="fondoTabla">

                <tr class="estiloImput">
                    <td colspan="2" align="left"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditIndicaciones')" /></td>
                    <td colspan="2" align="right"><input type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaEditIndicaciones')" /></td>
                <tr>                

                <tr class="titulosListaEspera">                    
                    <td colspan="4">INDICACION</td>
                </tr>

                <tr class="estiloImput">                    
                    <td colspan="4">
                        <textarea id="txtDescripcionIndicacionEditar" style="width:90%" maxlength="5000"  onblur="v28(this.value, this.id);" onkeypress="return validarKey(event, this.id)"></textarea>
                    </td>                    
                </tr>

                <tr>
                    <td colspan="4" align="center">
                        <input id="btnIndicacionesEditar" type="button" class="small button blue" value="EDITAR" onclick="modificarCRUD('editarIndicacionMedica');" />
                        <input type="button" onclick="modificarCRUD('eliminarIndicacionMedica');" value="ELIMINAR" title="ELIMI002" class="small button blue" id="btnIndicacionesEliminar">
                        <input id="btnIndicacionesImprimir" type="button" class="small button blue" value="IMPRIMIR" onclick="imprimirIndicacion()" />
                    </td>
                </tr>

            </table>
            <label id="lblIdIndicacionMedEditar" style="display: none;"></label>
            <label id="lblProfesionalCreaIndicacion" style="display: none;"></label>
        </div>
    </div>

    
</div>



</div>
