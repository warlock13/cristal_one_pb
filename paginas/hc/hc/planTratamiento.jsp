<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<!--
<label id="lblNomAdministradora2" style="font-size:11px; color:#F00" disabled="disabled"></label>
<img src="/clinica/utilidades/imagenes/acciones/search.png" title="IMGp88-cargarProcedimientosHistoricos"
     onclick="cargarProcedimientosHistoricos()" width="14px" height="14px"/>
-->
<div id="tabsPrincipalPT" style="width:98%; height:auto">
    <div id="divParaVentanitaCondicion1"></div>
    <ul>        
        <li id="idPlanTratamientoNut" style="display: none;">
            <a href="#divPlanTratmientoNut" onclick="TabActivoConductaTratamiento('divPlanTratmientoNut');">
                <center>PLAN TRATAMIENTO</center>
            </a>
        </li>        
    </ul>
    
    <div id="divPlanTratmientoNut">
        <table width="100%" cellpadding="0" cellspacing="0" align="center" class="fondoTabla">
            <tr>
              <td colspan="4">
                <table width="100%">
                  <tr class="titulos">
                    <td width="40%">Diagnostico Para Tratamiento</td>                    
                  </tr>
                  <tr class="estiloImput">
                    <td><input type="text" id="txtDx" style="width:70%">
                      <img width="18px" height="18px" align="middle" title="VER52" id="idLupitaVentanitaMuni"
                        onclick="traerVentanitaPT(this.id,'','divParaVentanita','txtDx','34')"
                        src="/clinica/utilidades/imagenes/acciones/buscar.png">
                    </td>                                         
                </tr>                                
                <tr class="titulos">
                    <td><label>Tipo de Diagnostico &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <select size="1" id="cmbDxTipoPT" style="width:30%"  title="53">
                            <option value=""></option>
                            <%     resultaux.clear();
                                             resultaux=(ArrayList)beanAdmin.combo.cargar(53);	
                                             ComboVO cmb11;
                                             for(int k=0;k<resultaux.size();k++){ 
                                                   cmb11=(ComboVO)resultaux.get(k);
                                      %>
                            <option value="<%= cmb11.getId()%>" title="<%= cmb11.getTitle()%>"><%=cmb11.getDescripcion()%></option>
                            <%}%>  
                        </select>                    
                    </td>                        
                </tr>
                <tr class="titulos">
                    <td>
                        Objetivo General
                    </td>
                </tr>
                <tr class="estiloImput">
                    <td>
                        <textarea id="txtObjGeneral" type="text" style="width: 70%;"> </textarea>
                        <input id="txtIdObjGeneral" hidden>                       
                    </td>
                </tr>
                <tr class="titulos">
                    <td>
                        Objetivos Especificos
                    </td>
                </tr>                        
                <tr>
                    <table  width="100%" align="center" id="Especificos" >                          
                        <tr class="titulos">
                          <td style="width: 50%;">                                    
                              <table id="listEspecificos" class="scroll" align="center"></table>                                                            
                          </td>
                          <td  style="width: 50%;">
                            <textarea id="txtEspe" style="width:90%; height: 140px; "></textarea>
                          </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" value="AGREGAR PLAN" onclick="agregarPlanT(obtenerListaPlan('listEspecificos'));agregarEspe(obtenerListaEspe('listEspecificos'));"  />
                            </td>
                        </tr>
                      </table>                      
                </tr>                
                <tr class="titulos">
                    <td>
                        Plan de Tratamiento
                    </td>                    
                </tr>
                <tr class="estiloImput">
                    <td>
                        <textarea id="txtPlanTr" style="width: 1050px; height: 100px;"></textarea>                        
                    </td>
                </tr>
                <tr class="estiloImput">
                    <td>
                        <input name="btn_CREAR" title="btn_ad75" type="button" class="small button blue" id="btnPlan" value="GUARDAR PLAN" onclick="modificarCRUD('planTratamiento')"  />
                    </td>
                </tr>                                                                
            </table>                                                                                   
            </td>   
          </tr>   
          </table>
    </div>
</div>
