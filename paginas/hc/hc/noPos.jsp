     
     <table width="100%" height="90"  cellpadding="0" cellspacing="0" class="fondoTabla" >
       <tr class="titulosIzquierda" >
           <td colspan="3">MEDICAMENTOS NO POS</td>               
       </tr>       
       <tr class="estiloImput">  
           <td colspan="3">                                                
              <table width="100%" >
                <tr class="titulos">
                  <td colspan="3">RESUMEN DE LA ENFERMEDAD ACTUAL</td>
				  <td colspan="1">TRATAMIENTO</td>                  
                </tr>                                  
                <tr class="estiloImput" >
                  <td colspan="3">
                  <textarea type="text" id="txt_ResumenHC"  size="4000"  maxlength="4000" style="width:95%"  > </textarea>
                  </td>
                  <td colspan="1">
                     <select id="txt_Tratamiento" style="width:30%" title="32" >	  
                        <option value="AMBULATORIO">AMBULATORIO</option>
                        <option value="HOSPITALARIO">HOSPITALARIO</option>
                        <option value="URGENTE">URGENTE</option>                                                                                                                        
                     </select>                  
                  </td>
                </tr>  
                <tr class="titulos">
                  <td width="50%" colspan="2">*Medicamento del POS-S utilizados(1) (Nombre generico)
                  </td>
                  <td width="50%" colspan="2">Medicamento del POS-S utilizados(2)  (Nombre generico)
                  </td>                  
                </tr>                  
                <tr class="estiloImput" >
                  <td colspan="2">
                  <input type="text" id="txt_MedicamentoDelPOS1" style="width:90%;" maxlength="500"/>
                  </td>
                  <td colspan="2">
                  <input type="text" id="txt_MedicamentoDelPOS2" style="width:90%;"  maxlength="500"/>
                  </td>                  
                </tr>  

 
                <tr class="titulos">
                  <td width="50%" colspan="2">*Presentacion y concentracion (1)
                  </td>
                  <td width="50%" colspan="2">Presentacion y concentracion (2)
                  </td>                  
                </tr>                  
                <tr class="estiloImput" >
                  <td colspan="2">
                  <input type="text" id="txt_PresentacionConcentracion1" style="width:90%;" maxlength="100"/>
                  </td>
                  <td colspan="2">
                  <input type="text" id="txt_PresentacionConcentracion2" style="width:90%;" maxlength="100"/>
                  </td>                  
                </tr>

  
                <tr class="titulos">
                  <td width="50%" colspan="2">*Dosis, frecuencia y duraci&oacute;n tratamiento (1)
                  </td>
                  <td width="50%" colspan="2">Dosis, frecuencia y duraci&oacute;n tratamiento (2)
                  </td>                  
                </tr>                  
                <tr class="estiloImput" >
                  <td colspan="2">
                  <input type="text" id="txt_DosisFrecuencia1" style="width:90%;"  maxlength="100"/>
                  </td>
                  <td colspan="2">
                  <input type="text" id="txt_DosisFrecuencia2" style="width:90%;"  maxlength="100"/>
                  </td>                  
                </tr>                
                
                
                              
                <tr class="titulos">
                  <td width="50%" colspan="4">INDICACION TERAPEUTICA</td>                                    
                </tr> 
                <tr class="estiloImput">
                  <td colspan="4"><input type="text" id="txt_IndicacionTerapeutica" style="width:90%;"  maxlength="4000"/></td>
                </tr>  
                
                <tr class="titulos">
                  <td width="100%" colspan="4">Cual es la razon para no formular alguno de los  medicamentos homologos o alternativos en el listado del POS-S
                  </td>
                </tr>    
                              
                <tr class="estiloImput" >
                  <td colspan="4">
                  1- El medicamento solicitado no se encuentra dentro del manual de POS-S y no cuenta con un homologo o alternativo
                     <select size="1" id="txt_pregunta1" style="width:5%" title="32"  >	  
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>                        
                     </select>                  
                  </td>
                </tr>  
                <tr class="estiloImput" >
                  <td colspan="4">
                  2- Existe el riesgo inminente para la vida o salud del paciente.
                     <select size="1" id="txt_pregunta2" style="width:5%" title="32"  >	  
                        <option value="SI" selected="selected">SI</option>
                        <option value="NO">NO</option>                        
                     </select>                  
                  </td>
                </tr>
                <tr class="estiloImput" >
                  <td colspan="4">
                  3-El paciente present&oacute; reaccion adversa ante el uso del medicamento POS-S.
                     <select size="1" id="txt_pregunta3" style="width:5%" title="32" >	  
                        <option value="SI">SI</option>
                        <option value="NO" selected="selected">NO</option>                        
                     </select>                  
                  </td>
                </tr>   
                <tr class="estiloImput" >
                  <td colspan="4">
                  4- Esta contraindicado el uso del medicamento POS-S para este paciente en particular.
                     <select size="1" id="txt_pregunta4" style="width:5%" title="32"  >	  
                        <option value="SI">SI</option>
                        <option value="NO" selected="selected">NO</option>                        
                     </select>                  
                  </td>
                </tr>  
                <tr class="estiloImput" >
                  <td colspan="4">
                  5- otro: 
                  <input type="text" id="txt_preguntaTexto5" style="width:90%;"  maxlength="500"/>
                  </td>
                </tr>                                            
                       
                                      
                <tr class="titulos">
                  <td width="100%" colspan="4">Explique
                  </td>
                </tr> 
                
                <tr class="estiloImput" >
                  <td colspan="4">
                  <textarea type="text" id="txt_ExpliquePos"  maxlength="4000" style="width:95%"  > </textarea>
                  </td>
                </tr>  
              </table>
           </td>  
       </tr> 
       <tr class="estiloImput">  
           <td colspan="3">              
           <input id="btnProcedimiento"  type="button" title="BYY5" class="small button blue" value="Adicionar El medicamento" onclick="modificarCRUD('listMedicacionConNoPOS','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  />              
           </td>
       </tr>
      </td>               
    </table>