<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();
        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.US);
    java.util.Date date = cal.getTime();
    java.text.DateFormat formateadorHora = java.text.DateFormat.getTimeInstance();
    SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd'/'MM'/'yyyy");
%>



<table width="100%">
    <tr class="titulos" >
        <td colspan="1">&nbsp;Tipo de Evento: </td>
        <td colspan="1" align="left">
            <select size="1" id="cmbTipoEvento" style="width:60%"   >	                                        
                <option value=""></option>
                <%     resultaux.clear();
                       resultaux=(ArrayList)beanAdmin.combo.cargar(300);	
                       ComboVO cmb32; 
                       for(int k=0;k<resultaux.size();k++){ 
                             cmb32=(ComboVO)resultaux.get(k);
                %>
                <option value="<%= cmb32.getId()%>" title="<%= cmb32.getTitle()%>"><%= cmb32.getDescripcion()%></option>
                <%}%>						
            </select>           
        </td> 
        <td colspan="2" align="left">Servicio que solicita :
            <select size="1" id="cmbServicioSolicita" style="width:60%" title="32"  >	  
                <option value=""></option>
                <%     resultaux.clear();
                       resultaux=(ArrayList)beanAdmin.combo.cargar(13);	
                       ComboVO cmb36; 
                       for(int k=0;k<resultaux.size();k++){ 
                             cmb36=(ComboVO)resultaux.get(k);
                %>
                <option value="<%= cmb36.getId()%>" title="<%= cmb36.getTitle()%>"><%= cmb36.getDescripcion()%></option>
                <%}%>						
            </select>      
        </td>            
    </tr>      
    <tr class="titulos" >
        <td width="30%" colspan="2">Servicio para el cual se solicita</td>
        <td width="25%">Prioridad</td> 
        <td width="20%">Nivel</td>                                                        
    </tr>
    <tr class="estiloImput">
        <td width="50%" colspan="2">
            <select id="cmbServicioParaCual" style="width:70%"  >	  
                <option value=""></option>
                <%     resultaux.clear();
                       resultaux=(ArrayList)beanAdmin.combo.cargar(301);	
                       ComboVO cmb4; 
                       for(int k=0;k<resultaux.size();k++){ 
                             cmb4=(ComboVO)resultaux.get(k);
                %>
                <option value="<%= cmb4.getId()%>" title="<%= cmb4.getTitle()%>"><%= cmb4.getDescripcion()%></option>
                <%}%> 
            </select>                   
        </td>
        <td>  
            <select id="cmbPrioridad" style="width:70%" title="32"  >	  
                <option value="URGENTE">URGENTE</option>
                <option value="PRIORITARIO">PRIORITARIO</option>                    
                <option value="AMBULATORIO">AMBULATORIO</option>                                        
            </select>
        </td>    
        <td>
            <select size="1" id="cmbNivelRemis" style="width:80%" title="32"  >	  
                <option value="I">NIVEL I</option>
                <option value="II">NIVEL II</option>                    
                <option value="III">NIVEL III</option>                                        
                <option value="IV">NIVEL IV</option>                                                        
            </select>
        </td>                              
    </tr>                

    <tr class="titulos" >
        <td colspan="4">DATOS DE LA PERSONA RESPONSABLE DEL PACIENTE</td>     
    </tr> 
    <tr class="estiloImput" >
        <td colspan="4">
            <TABLE width="100%">
                <TR>
                    <TD>Tipo Doc</TD><TD>Documento</TD><TD>Primer apellido</TD><TD>Segundo apellido</TD><TD>Primer Nombre</TD><TD>Segundo Nombre</TD>
                </TR>
                <TR class="estiloImput">
                    <TD>
                        <select size="1" id="cmbTipoDocResponsable" style="width:99%" title="32"  >	  
                            <option value="CC">Cedula de Ciudadania</option>
                            <option value="CE">Cedula de Extranjeria</option>
                            <option value="MS">Menor sin Identificar</option>
                            <option value="PA">Pasaporte</option>
                            <option value="RC">Registro Civil</option>
                            <option value="TI">Tarjeta de identidad</option>
                            <option value="CD">Carnet Diplomatico</option>
                            <option value="NV">Certificado nacido vivo</option>                           
                            <option value="AS">Adulto sin Identificar</option>                                                              
                        </select>
                    </TD>                
                    <TD><input type="text" id="txtDocResponsable" size="100"  maxlength="100" style="width:50%" onkeypress="javascript:return soloTelefono(event)"/></TD>                                  
                    <TD><input type="text" id="txtPrimerApeResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                 
                    <TD><input type="text" id="txtSegundoApeResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                    
                    <TD><input type="text" id="txtPrimerNomResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                 
                    <TD><input type="text" id="txtSegundoNomResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                 
                </TR>
                <TR>
                    <TD>Telefono</TD><TD colspan="3">Direccion</TD><TD>Departamento</TD><TD>Municipio</TD>
                </TR>
                <TR class="estiloImput">
                    <TD><input type="text" id="txtTelefonoResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                     
                    <TD colspan="3"><input type="text" id="txtDireccionResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                                      
                    <TD><input type="text" id="txtDeparResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                                                       
                    <TD><input type="text" id="txtMunicResponsable" size="100"  maxlength="100" style="width:90%"/></TD>                                                                                                                       
                </TR>                              
            </TABLE>           
        </td>             
    </tr>        


    <tr class="titulos" >
        <td colspan="3">Informacion clinica relevante</td>         
        <td colspan="1">Profesional realiza</td>                    
    </tr> 
    <tr class="estiloImput" >
        <td colspan="3">
            <textarea rows="5" id="txt_InformacionRemision" maxlength="8000" style="width:95%"  onkeypress="return validarKey(event, this.id)"> </textarea> 
        </td>
        <td valign="top">
            <select size="1" id="cmbIdProfesionalRemision" style="width:80%" title="26"   >	                                        
                <option value=""></option>
                <% resultaux.clear();
                   resultaux=(ArrayList)beanAdmin.combo.cargar(26);	
                   ComboVO cmbE; 
                   for(int k=0;k<resultaux.size();k++){ 
                         cmbE=(ComboVO)resultaux.get(k);
                %>
                <option value="<%= cmbE.getId()%>" title="<%= cmbE.getTitle()%>"><%= cmbE.getDescripcion()%></option>
                <%}%>                                 
            </select> 
            <p>.</p><p></p><p></p><p></p>                                
            
        </td>        
    </tr>  

    <tr class="titulos" >
    	<td colspan="4">Lista del chequeo para el traslado y/o referencia del paciente</td>
    </tr>

    <tr class="estiloImput">
        <td width="90%" class="estiloImputDer" colspan="3">VERIFICACIÓN DE PACIENTE CORRECTO:</td> 
        <td width="10%" class="estiloImputIzq2">
            <select size="1" id="cmbVerificacionPaciente" style="width:40%;" title="32" onblur=" "   >	  
                <option value=""></option>
                <option value="SI" selected="selected">SI</option>
                <option value="NO">NO</option>
                <option value="NA">NA</option>
            </select>
        </td>
    </tr> 	

    <tr class="estiloImput">
        <td width="90%" class="estiloImputDer" colspan="3">FORMATO DILIGENCIADO DE REFERENCIA Y CONTRAREFERENCIA</td> 
        <td width="10%" class="estiloImputIzq2">
            <select size="1" id="cmbFormatoDiligenciado" style="width:40%;" title="32" onblur=" "   >	  
                <option value=""></option>
                <option value="SI" selected="selected">SI</option>
                <option value="NO">NO</option>
                <option value="NA">NA</option>
            </select>
        </td>
    </tr> 


    <tr class="estiloImput">
        <td width="90%" class="estiloImputDer" colspan="3">RESUMEN DE HISTORIA CLINICA</td> 
        <td width="10%" class="estiloImputIzq2">
            <select size="1" id="cmbResumenHc" style="width:40%;" title="32" onblur=" "   >	  
                <option value=""></option>
                <option value="SI" selected="selected">SI</option>
                <option value="NO">NO</option>
                <option value="NA">NA</option>
            </select>
        </td>
    </tr> 

    <tr class="estiloImput">
        <td width="90%" class="estiloImputDer" colspan="3">DOCUMENTO DE IDENTIDAD Y/O CARNET DE AFILIACIÓN</td> 
        <td width="10%" class="estiloImputIzq2">
            <select size="1" id="cmbDocumentoIdentidad" style="width:40%;" title="32" onblur=" "   >	  
                <option value=""></option>
                <option value="SI" selected="selected">SI</option>
                <option value="NO">NO</option>
                <option value="NA">NA</option>
            </select>
        </td>
    </tr> 

    <tr class="estiloImput">
        <td width="90%" class="estiloImputDer" colspan="3">RESULTADOS DE IMÁGENES DIAGNOSTICAS (SI APLICA)</td> 
        <td width="10%" class="estiloImputIzq2">
            <select size="1" id="cmbImagenDiagnostica" style="width:40%;" title="32" onblur=" "   >	  
                <option value=""></option>
                <option value="SI">SI</option>
                <option value="NO">NO</option>
                <option value="NA">NA</option>
            </select>
        </td>
    </tr> 

    <tr class="estiloImput">
        <td width="90%" class="estiloImputDer" colspan="3">PACIENTE LLEVA MANILLA DE IDENTIFICACION LEGIBLE (UNICAMENTE APLICA A PACIENTE CON REFERENCIA AL SERVICIO DE URGENCIAS)</td> 
        <td width="10%" class="estiloImputIzq2">
            <select size="1" id="cmbPacienteManilla" style="width:40%;" title="32" onblur=" "   >	  
                <option value=""></option>
                <option value="SI">SI</option>
                <option value="NO">NO</option>
                <option value="NA">NA</option>
            </select>
        </td>
    </tr> 

    <tr>
        <td colspan="4" align="center">
            <input id="btnProcedimiento_NO"  type="button" class="small button blue" title="BTN8D1" value="CREAR"  onclick="modificarCRUD('listRemision', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"/>
        </td>
    </tr>

    <tr class="titulos">
        <td colspan="4">                                    
            <table id="listRemision" class="scroll" cellpadding="0" cellspacing="0" align="center"></table>
        </td>
    </tr>
</table>   



 
<!-- cambia aqui y debe cambiar en referencia.jsp-->
<div id="divVentanitaRemision"  style=" display:none;  left:211px; z-index:999">

 <div class="transParencia" style="z-index:1000; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
        </div>  

<div   style="z-index:1001; position:fixed; top:200px; left:400px; width:50%">  

    <table width="100%"  border="1"  class="fondoTabla">
        <tr class="estiloImput" >
            <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaRemision')" /></td>  
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaRemision')" /></td>  
        </tr>     
        <tr class="titulos" >
            <td width="15%" >Id</td>  
            <td width="15%" >Fecha Evento</td> 
            <td width="10%" >Tipo</td>           
            <td width="60%" >Servicio que solicita</td>                          
        </tr>           
        <tr class="estiloImput" >
            <td ><label id="lblIdRemi"></label></td> 
            <td ><label id="lblFechaEvento"></label></td>            
            <td ><label id="lblIdTipoRemi"></label>-<label id="lblNomTipoRemi"></label></td>
            <td ><label id="lblNombreElementoRemi"></label></td> 
        </tr> 
        <tr>  
            <td colspan="2" align="center">
                <input id="btnEliminarMed" type="button" class="small button blue" title="B742" value="Elimina Registro" onclick="modificarCRUD('eliminarRemision', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />  
            </td>     
            <td colspan="2" align="center">
                <!-- <input id="btnProcedimientoImprimir"  type="button" class="small button blue" title="B743" value="Imprime" onclick="imprimirReferencia()"  /> -->
                <input id="btnProcedimientoImprimir"  type="button" class="small button blue" title="B743" value="Imprime" onclick="impresionReferencia();"  />
            </td>                    
        </tr>                
    </table> 
</div>
</div>     

  

<!-- cambia aqui y debe cambiar en referencia.jsp-->
<div id="divVentanitaLaboratorio"  style="position:absolute; display:none; background-color:#CCC; top:310px; left:150px; width:900px; height:200px; z-index:999">
    <table width="100%"  border="1"  class="fondoTabla">
        <tr class="estiloImput" >
            <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaLaboratorio')" /></td>  
            <td>&nbsp;</td>
            <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaLaboratorio')" /></td>  
        </tr>     
        <tr class="titulos" >
            <td width="15%" >NOMBRE</td>  
        </tr>           
        <tr class="estiloImput" >
            <td><input type="text" id="txtNombreElemento" size="100"  maxlength="100" style="width:50%"/></td> 
        </tr> 
        <tr>  
            <td colspan="2" align="center">
                <input id="btnCrearMed" type="button" class="small button blue" title="B742" value="CREAR" onclick="modificarRT('crearLaboratorio');" />  
            </td>     
        </tr>                
    </table> 
</div>          



