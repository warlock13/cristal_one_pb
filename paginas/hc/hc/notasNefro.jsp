<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>
<%@ page import = "Sgh.Utilidades.Sesion" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" /> <!-- instanciar bean de session -->


<%  beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
           <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
               <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="NOTAS DE SEGUIMIENTO NEFROPROTECCION" />
                </jsp:include>
            </div>
           <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
       <td>
           <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
               <tr>
                   <td>
                        <div style="overflow:auto;height:700px; width:100%" id="divContenido">
                           <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulos" align="center">
                                    <td width="30%">PACIENTE</td>                                   
                                    <td width="70%">&nbsp;</td>                                    
                                </tr>
                                <tr class="estiloImput">                                                                   
                                    <td> 
                                        <input type="text" size="70"  maxlength="70" style="width:90%"  id="txtIdBusPaciente"  />                                         
                                        <img  width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanitaPaciente(this.id,'','divParaVentanita','txtIdBusPaciente','27')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
                                        <div id="divParaVentanita"></div>
                                    </td>                                   
                                    <td>
                                        <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue" value="BUSCAR" onclick="buscarHC('listNotasNefro')" />
                                        <input name="btn_BUSCAR" title="Crear Nota Seguimiento NefroProteccion" type="button" class="small button blue" value="CREAR"  onclick="modificarCRUD('crearNotaNefroproteccion')"/>
                                    </td>
                                </tr>
                                <tr class="titulos">
                                    <td colspan="2">
                                        <table id="listNotasNefro"></table>
                                    </td>
                                </tr>
                                <tr class="titulos">
                                    <td>
                                        <label id="lblIdEvolucion" style="color: red;"></label>
                                        <label id="lblIdProfesional" style="display: none;"></label>
                                    </td>
                                    <td>
                                        <label id="lblIdEstado" style="color: red;"></label>-<label id="lblEstado"></label>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="tabsnotasSegumiento" style="width:98%; height:auto">
                                            <ul>                                    
                                                <li>
                                                    <a href="#divNotas" onclick="">
                                                        <center>NOTA DE SEGUIMIENTO</center>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#divMedicamentos" onclick="">
                                                        <center>ENTREGA DE MEDICAMENTOS</center>
                                                    </a>                                        
                                                </li>                                                
                                                <li>
                                                    <a href="#divNovedades" onclick="">
                                                        <center>NOVEDADES</center>
                                                    </a>                                        
                                                </li>
                                            </ul>
                            
                                            <div id="divNotas">
                                                <table width="100%">
                                                    <tr class="titulos">
                                                        <td>PROFESION</td>
                                                        <td class="estiloImput">
                                                            <select id="cmbIdProfesion" onchange="modificarCRUD('guardarNota')" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <% resultaux.clear();
                                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(3504);   
                                                                    ComboVO cmb33;
                                                                    for(int k=0;k<resultaux.size();k++){
                                                                        cmb33=(ComboVO)resultaux.get(k);
                                                                    %>
                                                                    <option value="<%= cmb33.getId()%>" title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%></option>
                                                                <%}%>  
                                                            </select>
                                                        </td>                                                        
                                                    </tr>
                                                    <tr class="estiloImput">
                                                        <td style="width:20%" class="titulos">NOTA DE SEGUMIENTO DE NEFROPROTECCION</td>
                                                        <td width="80%">
                                                            <textarea disabled id="txtContenido" onblur="modificarCRUD('guardarNota')" style="width: 90%; height: 200px; font-size: 15px; border:solid 1px black;"></textarea>
                                                        </td>
                                                    </tr>                                                                                      
                                                </table>
                                            </div>
                                            <div id="divMedicamentos">
                                                <table width="100%">
                                                    <tr class="titulos">
                                                        <td>
                                                            TIPO DE ENTREGA
                                                        </td>
                                                        <td>
                                                            FECHA ENTREGA
                                                        </td>
                                                    </tr>
                                                    <tr class="estiloImput">
                                                        <td>
                                                            <select id="cmbIdEntrega" onchange="modificarCRUD('guardarFechaEntrega')" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <% resultaux.clear();
                                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(3235);   
                                                                    ComboVO cmb34;
                                                                    for(int k=0;k<resultaux.size();k++){
                                                                        cmb34=(ComboVO)resultaux.get(k);
                                                                    %>
                                                                    <option value="<%= cmb34.getId()%>" title="<%= cmb34.getTitle()%>"><%= cmb34.getDescripcion()%></option>
                                                                <%}%> 
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="date" id="txtFechaEntrega" onblur="modificarCRUD('guardarFechaEntrega')" disabled>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>                                            
                                            <div id="divNovedades">
                                                <table width="100%">
                                                    <tr class="titulos">
                                                        <td>
                                                            TIPO DE NOVEDAD
                                                        </td>
                                                        <td>
                                                            FECHA NOVEDAD
                                                        </td>
                                                    </tr>
                                                    <tr class="estiloImput">
                                                        <td>
                                                            <select id="cmbIdNovedad" onchange="modificarCRUD('guardarNovedadPaciente')" disabled>
                                                                <option value="">SELECCIONE</option> 
                                                                <% resultaux.clear();
                                                                    resultaux=(ArrayList)beanAdmin.combo.cargar(3570);   
                                                                    ComboVO cmbNovedad;
                                                                    for(int k=0;k<resultaux.size();k++){
                                                                        cmbNovedad=(ComboVO)resultaux.get(k);
                                                                    %>
                                                                    <option value="<%= cmbNovedad.getId()%>" title="<%= cmbNovedad.getTitle()%>"><%= cmbNovedad.getDescripcion()%></option>
                                                                <%}%>                                                                
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="date" id="txtFechaNovedad" onblur="modificarCRUD('guardarNovedadPaciente')" disabled>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>                                                                                                                                                                                                                  
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">                                                                                
                                        <input name="btn_BUSCAR" title="Firmar Notas de Seguimiento NefroProteccion" type="button" class="small button blue" value="FIRMAR"  onclick="modificarCRUD('firmarNota')"/>
                                    </td>                                    
                                </tr>                                
                            </table>                         
                        </div>                                                
                    </td>
                </tr>
            </table>            
       </td>
    </tr>    
</table>
