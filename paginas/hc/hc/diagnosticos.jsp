<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
     
    ArrayList resultaux=new ArrayList();
%>
<table width="100%" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td colspan="4" style="padding-left: 0px;">
      <table width="100%">
        <tr class="titulosListaEspera">
          <td width="40%">Diagnostico</td>
          <td width="12%">Tipo</td>
          <td width="10%">clase</td>
          <td width="10%">sitio</td>
          <td width="25%">Observaci&oacute;nes</td>
          <td width="6%"><img src="/clinica/utilidades/imagenes/acciones/lupa_roja.gif" onclick="cargarDxHistoricos()"
              width="14px" height="14px" /></td>
             
        </tr>
        <tr class="estiloImputListaEspera">
          <td><input type="text" id="txtIdDx" style="width:85%" oninput="llenarElementosAutoCompletarKey(this.id, 218, this.value,valorAtributo('lblEdadPaciente'),valorAtributo('lblGeneroPaciente'))">
            <img width="18px" height="18px" align="middle" title="VER52" id="idLupitaVentanitaMuni"
              onclick="traerVentanitaDx(this.id,'','divParaVentanita','txtIdDx','48')"
              src="/clinica/utilidades/imagenes/acciones/buscar.png">
            <img  width="18px" height="18px" align="middle" title="VER52" id="idLupitaVentanitaMuni" 
              src="/clinica/utilidades/imagenes/acciones/lupa_roja.gif" onclick="cargarDxFrecuentes()"
          </td>
          <td>
            <select size="1" id="cmbDxTipo" style="width:90%"  title="53">              
              <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(53);	
                               ComboVO cmb11;
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmb11=(ComboVO)resultaux.get(k);
                        %>
              <option value="<%= cmb11.getId()%>" title="<%= cmb11.getTitle()%>"><%=cmb11.getDescripcion()%></option>
              <%}%>  
                   </select>                       
              </td>
              <td>
                   <select size="1" id="cmbDxClase" style="width:90%" title="52" >	                                                              
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(52);	
                               ComboVO cm;
                               for(int k=0;k<resultaux.size();k++){ 
                                     cm=(ComboVO)resultaux.get(k);
                        %>
              <option value="<%= cm.getId()%>" title="<%= cm.getTitle()%>"><%=cm.getDescripcion()%></option>
              <%}%>    
                   </select>                                         
              </td>
              <td>
                   <select id="cmbIdSitioQuirurgicoDx" style="width:98%"  >                     
                        <%     resultaux.clear();
                               resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                               ComboVO cmbSQ; 
                               for(int k=0;k<resultaux.size();k++){ 
                                     cmbSQ=(ComboVO)resultaux.get(k);
                        %>
              <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>"><%= cmbSQ.getDescripcion()%></option>
              <%}%>
                   </select>                            
              </td>
              <td>
              	<!-- <input type="text"  placeholder="Coloque el estado y grado enfermedad" value="" id="txtDxObservacion"  style="width:90%"  size="200"  maxlength="200"  >             -->
                <textarea id="txtDxObservacion"   onkeypress="return validarKey(event,this.id)" style="width:90%"  maxlength="2000" ></textarea>
              </td>  
              <td><input id="btnProcedimiento_" title="DIA41" type="button" class="small button blue" value="Adici&oacute;n" onclick="modificarCRUD('listDiagnosticos','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"  /></td>
           </tr>                                                                            
      </table> 
                    
      
      <table  width="100%" align="center" id="idTablaDx" >                          
        <tr class="titulos">
          <td>                                    
                <table id="listDiagnosticos" class="scroll" align="center"></table>
          </td>
        </tr>
      </table>                    
  </td>   
</tr>   
</table>     


<div id="divVentanitaDx"  style=" background-color:#E2E1A5; top:310px; width:1190px; height:90px; z-index:999">
        <table width="100%"  border="1"  class="fondoTabla">
           
           <tr class="estiloImput" >
               <td colspan="7" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaDx')" /></td>  
           <tr>     
           <tr class="titulos" >
               <td width="15%" >ID </td>  
               <td width="15%" >ID DIAGNOSTICO</td>                 
               <td colspan="5">NOMBRE DIAGNOSTICO</td>           
           <tr>           
           <tr class="estiloImput" >
               <td ><label id="lblId"></label></td>            
               <td ><label id="lblIdElemento"></label></td>
               <td colspan="5"><label id="lblNombreElemento"></label></td> 
           <tr>
           <tr>
              <td colspan="2">
                  <input type="text"  id="txtIdDxVentanita" style="width:90%" readonly>
                  <img width="18px" height="18px" align="middle" title="VER52" id="idEditarDx" onclick="traerVentanitaDx(this.id,'','divParaVentanita','txtIdDxVentanita','48')" src="/clinica/utilidades/imagenes/acciones/buscar.png">
               </td>
               <td>
                  <select size="1" id="cmbDxTipoVentanita" style="width:90%"  title="53">
                    <option value=""></option>
                    <%     resultaux.clear();
                                     resultaux=(ArrayList)beanAdmin.combo.cargar(53);	
                                     for(int k=0;k<resultaux.size();k++){ 
                                           cmb11=(ComboVO)resultaux.get(k);
                              %>
              <option value="<%= cmb11.getId()%>" title="<%= cmb11.getTitle()%>"><%=cmb11.getDescripcion()%></option>
              <%}%>  
                         </select>                       
                    </td>
                    <td>
                         <select size="1" id="cmbDxClaseVentanita" style="width:90%" title="52" >	                                        
                            <option value=""></option>
                              <%     resultaux.clear();
                                     resultaux=(ArrayList)beanAdmin.combo.cargar(52);	
                                     for(int k=0;k<resultaux.size();k++){ 
                                           cm=(ComboVO)resultaux.get(k);
                              %>
              <option value="<%= cm.getId()%>" title="<%= cm.getTitle()%>"><%=cm.getDescripcion()%></option>
              <%}%>    
                         </select>                                         
                    </td>
                    <td>
                         <select id="cmbIdSitioQuirurgicoDxVentanita" style="width:98%"  >
                           <option value=""></option>
                              <%     resultaux.clear();
                                     resultaux=(ArrayList)beanAdmin.combo.cargar(120);	
                                     for(int k=0;k<resultaux.size();k++){ 
                                           cmbSQ=(ComboVO)resultaux.get(k);
                              %>
              <option value="<%= cmbSQ.getId()%>" title="<%= cmbSQ.getTitle()%>"><%= cmbSQ.getDescripcion()%></option>
              <%}%>
                         </select>                            
                    </td>
                    <td>
                      <!-- <input type="text"  placeholder="Coloque el estado y grado enfermedad" value="" id="txtDxObservacion"  style="width:90%"  size="200"  maxlength="200"  >             -->
                      <textarea id="txtDxObservacionVentanita" style="width:90%"  maxlength="2000" onkeypress="return validarKey(event,this.id)"></textarea>
                    </td>
               <td>
                  <div id="divBotonGuardar" style="width:80%; display:block" > 
                    <input type="button" class="small button blue" value="MODIFICAR DIAGNOSTICO" onclick="modificarCRUD('modificarDx', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   title="btn412" />  
                  </div>
               </td>   
           <tr>  
             <tr>
                <td align="CENTER" colspan="7">
                    <div id="divBotonGuardar" style="width:80%; display:block" > 
                   <input id="btnEliminarDx" type="button" class="small button blue" value="ELIMINAR" onclick="modificarCRUD('eliminarDxListado', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');"   title="btn412" />  
                  </div>
                 </td>  
             </tr>              
        </table> 
        <input type="hidden" id="txtIdTrans">
   </div>            

<div id="divVentanitaDxHistorico"  style="position:fixed; display:none;  top:1px; width:600px; height:400px; z-index:999">
     <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">  </div>  
      <div  style="z-index:2057; position:absolute; top:300px; left:50px; height:1000; width:100%">      
          <table width="100%"  class="fondoTabla">           
             <tr class="estiloImput" >
                 <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaDxHistorico')" /></td>  
                 <td align="center">HISTORICOS DE DIAGNOSTICOS</td>
                 <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaDxHistorico')" /></td>  
             <tr>     
            <tr class="titulos">
              <td colspan="3">                                      
                  <table id="listDiagnosticosHistorico" class="scroll" align="center"></table>
              </td>
            </tr>                
          </table> 
  </div>     
</div>    



<div id="divVentanitaDxFrecuente"  style="position:fixed; display:none;  top:1px; width:600px; height:400px; z-index:999">
  <div class="transParencia" style="z-index:2056; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">  </div>  
   <div  style="z-index:2057; position:absolute; top:300px; left:50px; height:1000; width:100%">      
       <table width="100%"  class="fondoTabla">           
          <tr class="estiloImput" >
              <td colspan="1" align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaDxFrecuente')" /></td>  
              <td align="center">DIAGNOSTICOS FRECUENTES</td>
              <td colspan="1" align="right" ><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divVentanitaDxFrecuente')" /></td>  
          <tr>     
         <tr class="titulos">
           <td colspan="3">                                      
               <table id="listDiagnosticosFrecuente" class="scroll" align="center"></table>
           </td>
         </tr>                
       </table> 
</div>     
</div>    