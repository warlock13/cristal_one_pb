function buscarSolicitudesFarmacia(arg) {
    idArticulo = '';
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
    switch (arg) {
        case 'listGrillaSolicitudFarmacia':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=493&parametros=";
            add_valores_a_mandar(document.getElementById('lblIdAdmisionAgen').textContent);

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['TOT', 'ID', 'ID ADMISION', 'ID ARTICULO', 'NOMBRE GENERICO', 'CANTIDAD', 'OBSERVACION', 'ID ESTADO', 'ID ELABORO', 'ELABORO', 'FECHA ELABORO', 'ID SOLICITUD TIPO', 'ID TRANS DEV'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_ADMISION', index: 'ID_ADMISION', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', hidden: true },
                    { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 25) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 10) },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 10) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', width: anchoP(ancho, 3) },
                    { name: 'ID_ELABORO', index: 'ID_ELABORO', hidden: true },
                    { name: 'ELABORO', index: 'ELABORO', width: anchoP(ancho, 15) },
                    { name: 'FECHA_ELABORO', index: 'FECHA_ELABORO', width: anchoP(ancho, 15) },
                    { name: 'id_solicitudes_tipo', index: 'id_solicitudes_tipo', width: anchoP(ancho, 5) },
                    { name: 'id_transaccion_devo', index: 'id_transaccion_devo', width: anchoP(ancho, 5) },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdSolicitud', datosRow.ID, 1);
                    asignaAtributo('txtIdArticuloSolicitudes', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtCantidadSolicitud', datosRow.CANTIDAD, 0);
                    asignaAtributo('txtObservacionSolicitud', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblSolicitudTipo', datosRow.id_solicitudes_tipo, 0);
                    asignaAtributo('lblIdTransaccionDevo', datosRow.id_transaccion_devo, 0);


                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;
    }
}