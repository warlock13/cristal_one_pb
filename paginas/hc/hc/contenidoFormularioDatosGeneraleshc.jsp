<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<style type="text/css">
	.loader-ajax{
		background-color: transparent;
		position:absolute;
		float: none;
		z-index: 2;
		width:100%; 
		height:12px; 
		display: none;
	}

	.button_ok{
		background-color: transparent;
		position:absolute;
		float: none;
		z-index: 2;
		width:100%; 
		height:12px; 
	}

	.cancel_error{
		background-color: transparent;
		position:absolute;
		float: none;
		z-index: 2;
		width:100%; 
		height:12px; 
		display: none;
	}
</style> 

<div id="loader_datos_generales" style="display: flex; justify-content: center; align-items: center; height: 150px; background-color: white;" hidden>
	<table>
	  <tr>
		<td align="center"><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="60px" height="60px"></td>
	  </tr>
	  <tr>
		<td><label style="font-size: small; font-style: italic;">Cargando datos. Por favor espere...</label></td>
	  </tr>
	</table>
</div>

<div id="divMotivoEnfermedad" hidden style="padding-left: 2px;padding-right: 2px;">
	<table width="100%" cellpadding="0" cellspacing="0" align="center">
		<tr class="titulosListaEspera">
			<td>Motivo de la Consulta:</td>
			<td>Enfermedad actual / Ananmesis</td>
			<td></td>
		</tr>
		<tr class="estiloImputListaEspera">
			<td width="30%">
				<textarea rows="5" id="txtMotivoConsulta" size="4000" maxlength="4000" style="width:95%; height: 100%;"
					 onkeypress="return validarKey(event, this.id)" onkeydown="onKeyDownHandler(event,this.id)" oninput="this.value = this.value.toUpperCase();" onblur="  modificarCRUD('motivoConsultaS', '', this.id); "></textarea>
					<img id='txtMotivoConsulta-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px">
					<img onclick="modificarCRUD('motivoConsultaS', '', this.id)" id='txtMotivoConsulta-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
					<img onclick="modificarCRUD('motivoConsultaS', '', this.id)" id='txtMotivoConsulta-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
			</td>
			<td width="30%">
				<textarea rows="5" id="txtEnfermedadActual" style="width:95%; height: 100%;"
					  onkeypress="return validarKey(event, this.id)"  onkeydown="onKeyDownHandler(event,this.id)" oninput="this.value = this.value.toUpperCase();" onblur="modificarCRUD('enfermedadActualS', '', this.id); "> </textarea>
					<img id='txtEnfermedadActual-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px">
					<img onclick="modificarCRUD('enfermedadActualS', '', this.id)" id='txtEnfermedadActual-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
					<img onclick="modificarCRUD('enfermedadActualS', '', this.id)" id='txtEnfermedadActual-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
			</td>
			<td width="30%">
				<table width="100%" align="center">
					<tr class="titulos" style="display:none;">
						<td width="20%" align="right">Medico remite:</td>
						<td width="80%" align="left">
							<input type="text" size="1" id="input_med" style="width:90%" title="medico que remite"
							onblur="modificarCRUD('motivoEnfermedad','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', this.id)"
							>
						</td>
					</tr>

					<tr>
						<td colspan="2">
							<table>
								<tr class="titulosListaEspera">
									<td width="60%" align="right">s&iacute;ntomatico piel</td>
									<td width="40%" align="left">
										<select size="1" id="cmb_SintomaticoPiel" style="width:90%" title="32"
											onblur="modificarCRUD('sintomaticoPielS','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', this.id)"
											>
											<option value="NO">NO</option>
											<option value="SI">SI</option>
										</select><img id='cmb_SintomaticoPiel-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px"><img onclick="modificarCRUD('sintomaticoPielS', '', this.id)" id='cmb_SintomaticoPiel-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px"><img onclick="modificarCRUD('sintomaticoPielS', '', this.id)" id='cmb_SintomaticoPiel-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
									</td>
								</tr>
								<tr class="titulosListaEspera">
									<td align="right">s&iacute;ntomatico respiratorio</td>
									<td align="left">
										<select size="1" id="cmb_SintomaticoRespira" style="width:90%" title="32"
											onblur="modificarCRUD('sintomaticoRespiraS','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', this.id)"
											>
											<option value="NO">NO</option>
											<option value="SI">SI</option>
										</select><img id='cmb_SintomaticoRespira-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px"><img onclick="modificarCRUD('sintomaticoRespiraS', '', this.id)" id='cmb_SintomaticoRespira-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px"><img onclick="modificarCRUD('sintomaticoRespiraS', '', this.id)" id='cmb_SintomaticoRespira-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
									</td>
								</tr>
								<tr class="titulosListaEspera">
									<td align="right">s&iacute;ntomatico Febril</td>
									<td align="left">
										<select size="1" id="cmb_SintomaticoFebril" style="width:90%" title="32"
											onblur="modificarCRUD('sintomaticoFebrilS','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', this.id)"
											>
											<option value="NO">NO</option>
											<option value="SI">SI</option>
										</select><img id='cmb_SintomaticoFebril-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px"><img onclick="modificarCRUD('sintomaticoFebrilS', '', this.id)" id='cmb_SintomaticoFebril-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px"><img onclick="modificarCRUD('sintomaticoFebrilS', '', this.id)" id='cmb_SintomaticoFebril-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
									</td>
								</tr>
								<tr class="titulosListaEspera">
									<td align="right">s&iacute;ntomatico Sistema Nervio Periferico</td>
									<td align="left">
										<select size="1" id="cmb_SintomaticoSnp" style="width:90%" title="32"
											onblur="modificarCRUD('modalidadSNPS','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', this.id)"
											>
											<option value="NO">NO</option>
											<option value="SI">SI</option>
										</select><img id='cmb_SintomaticoSnp-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px"><img onclick="modificarCRUD('modalidadSNPS', '', this.id)" id='cmb_SintomaticoSnp-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px"><img onclick="modificarCRUD('modalidadSNPS', '', this.id)" id='cmb_SintomaticoSnp-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
									</td>
								</tr>
								<tr class="titulosListaEspera">
									<td align="right">Modalidad Consulta</td>
									<td align="left">
										<select size="1" id="cmbModalidadConsulta" style="width:90%"  title="53" onblur="modificarCRUD('modalidadConsultaS','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp', this.id)">
											<%     resultaux.clear();
															resultaux=(ArrayList)beanAdmin.combo.cargar(3529);	
															ComboVO cmb11;
															for(int k=0;k<resultaux.size();k++){ 
																cmb11=(ComboVO)resultaux.get(k);
													%>
											<option value="<%= cmb11.getId()%>" title="<%= cmb11.getTitle()%>"><%=cmb11.getDescripcion()%></option>
											<%}%>  
										</select><img id='cmbModalidadConsulta-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px"><img onclick="modificarCRUD('modalidadConsultaS', '', this.id)" id='cmbModalidadConsulta-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px"><img onclick="modificarCRUD('modalidadConsultaS', '', this.id)" id='cmbModalidadConsulta-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
									</td>
								</tr>
						</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="background-color: whitesmoke;">
			<td colspan="3">
				<table width="100%">
					<tr class="titulosListaEspera">
						<!-- <th width="30%">Impresion Diagnostica</th> -->
						<th width="30%">Causa Externa</th>
						<th width="30%">Finalidad</th> 
					</tr>
					<tr class="estiloImputListaEspera">
<!-- 						<td>
							<input type="text" value="" id="txtIdDxIngreso" style="width: 90%;" onblur="if (valorAtributoIdAutoCompletar(this.id) != ''){modificarCRUD('actualizarDxIngresoHC');} " oninput="llenarElementosAutoCompletarKey(this.id, 218, this.value,valorAtributo('lblEdadPaciente'),valorAtributo('lblGeneroPaciente'))">
							<img width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaDxIngrT"
								onclick="traerVentanitaFuncionesHc(this.id, 'actualizarDxIngresoHC', 'divParaVentanita','txtIdDxIngreso','5')"
								src="/clinica/utilidades/imagenes/acciones/buscar.png">
							
							<img id='txtIdDxIngreso-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px" align="middle">
							<img onclick="modificarCRUD('actualizarDxIngresoHC', '', this.id)" id='txtIdDxIngreso-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
							<img onclick="modificarCRUD('actualizarDxIngresoHC', '', this.id)" id='txtIdDxIngreso-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">

						</td> -->
						<td>
							<select id="cmbCausaExterna" style="width: 95%;"
								onchange="modificarCRUD('actualizarCausaExternaHc')"
								onfocus="comboDependiente(this.id,'lblIdDocumento','3616')">						
							</select>
							
							<img id='cmbCausaExterna-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px">
							<img onclick="modificarCRUD('actualizarCausaExternaHc', '', this.id)" id='cmbCausaExterna-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
							<img onclick="modificarCRUD('actualizarCausaExternaHc', '', this.id)" id='cmbCausaExterna-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">
							

						</td>
						<td>
							<select id="cmbFinalidadConsulta" style="width: 95%;"
								onchange="modificarCRUD('actualizarFinalidadHc')"
								onfocus="comboDependiente(this.id,'lblIdDocumento','3617')">								
							</select>

							<img id='cmbFinalidadConsulta-ok' src="/clinica/utilidades/imagenes/acciones/button_ok.png" alt="" width="12px" height="12px">
							<img onclick="modificarCRUD('actualizarFinalidadHc', '', this.id)" id='cmbFinalidadConsulta-loader' style = 'display:none;' src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="12px" height="12px">
							<img onclick="modificarCRUD('actualizarFinalidadHc', '', this.id)" id='cmbFinalidadConsulta-error' style = 'display:none;' src="/clinica/utilidades/imagenes/acciones/button_cancel.png" alt="" width="12px" height="12px">

						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
<!--      <input type="hidden"  id="txtVistaPrevia" value="NO" width="70px" /> -->
<input type="hidden" id="txtIdDocumentoVistaPrevia" value="" />
<input type="hidden" id="txtTipoDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblTituloDocumentoVistaPrevia" value="" />
<input type="hidden" id="lblIdAdmisionVistaPrevia" value="" />
<input type="hidden" id="lblEstadoDocumentoVistaPrevia" value="" />
