<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.*,java.text.*,java.sql.*" %>
<%@ page import="Sgh.Utilidades.*" %>
<%@ page import="Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import="Clinica.Presentacion.*" %>
<%@ page import="Sgh.Utilidades.Sesion" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session"/>
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session"/>
<!-- instanciar bean de session -->


<% beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="NOTAS DE SEGUIMIENTO VIH"/>
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:700px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                <tr class="titulos" align="center">
                                    <td width="30%">PACIENTE</td>
                                    <td width="70%">&nbsp;</td>
                                </tr>
                                <tr class="estiloImput">
                                    <td>
                                        <input type="text" size="70" maxlength="70" style="width:90%"
                                               id="txtIdBusPaciente" />
                                        <img width="18px" height="18px" align="middle" title="VEN52"
                                             id="idLupitaVentanitaIdenT"
                                             onclick="traerVentanitaPaciente(this.id,'','divParaVentanita','txtIdBusPaciente','27')"
                                             src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                        <div id="divParaVentanita"></div>
                                    </td>
                                    <td>
                                        <input name="btn_BUSCAR" title="PTTT1" type="button" class="small button blue"
                                               value="BUSCAR" onclick="buscarHC('listNotasVih')"/>
                                        <input name="btn_BUSCAR" title="Crear Nota Seguimiento NefroProteccion"
                                               type="button" class="small button blue" value="CREAR"
                                               onclick="modificarCRUD('crearNotaVih')"/>
                                    </td>
                                </tr>
                                <tr class="titulos">
                                    <td colspan="2">
                                        <table id="listNotasVih"></table>
                                    </td>
                                </tr>
                                <tr class="titulos">
                                    <td>
                                        <label id="lblIdEvolucion" style="color: red;"></label>
                                        <label id="lblIdProfesional" style="display: none;"></label>
                                    </td>
                                    <td>
                                        <label id="lblIdEstado" style="color: red;"></label>-<label
                                            id="lblEstado"></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="tabsnotasSegumiento" style="width:98%; height:auto">
                                            <ul>
                                                <li>
                                                    <a href="#divNotas" onclick="">
                                                        <center>NOTA DE SEGUIMIENTO</center>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#divNovedades" onclick="">
                                                        <center>NOVEDADES</center>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#divPoblacionClave" onclick="">
                                                        <center>POBLACION CLAVE VIH</center>
                                                    </a>
                                                </li>
                                            </ul>

                                            <div id="divNotas">
                                                <table width="100%">
                                                    <tr class="titulos">
                                                        <td>PROFESION</td>
                                                        <td class="estiloImput">
                                                            <select id="cmbIdProfesion"
                                                                    onchange="modificarNotasCRUD('ntsv', 'c1', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <% resultaux.clear();
                                                                    resultaux = (ArrayList) beanAdmin.combo.cargar(3504);
                                                                    ComboVO cmb33;
                                                                    for (int k = 0; k < resultaux.size(); k++) {
                                                                        cmb33 = (ComboVO) resultaux.get(k);
                                                                %>
                                                                <option value="<%= cmb33.getId()%>"
                                                                        title="<%= cmb33.getTitle()%>"><%= cmb33.getDescripcion()%>
                                                                </option>
                                                                <%}%>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="estiloImput">
                                                        <td style="width:20%" class="titulos">NOTA DE SEGUMIENTO DE VIH</td>
                                                        <td width="80%">
                                                            <textarea disabled id="txtContenido"
                                                                      onblur="modificarNotasCRUD('ntsv', 'c2', this.value)"
                                                                      style="width: 90%; height: 200px; font-size: 15px; border:solid 1px black;"></textarea>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divPoblacionClave">
                                                <table width="90%">
                                                    <tr class="titulos">
                                                        <td>1. PERSONAS TRABAJADORAS SEXUALES</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc1"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c0', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >2. MUJERES TRANSGÉNERO</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc2"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c1', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >3. HOMBRES TRANSGÉNERO</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc3"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c2', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >4. HOMBRES QUE TIENEN RELACIONES SEXUALES CON OTROS HOMBRES</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc4"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c3', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >5. CONSUMIDORES DE SUSTANCIAS PSICOACTIVAS POR VÍA DIFERENTE A LA INYECTADA</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc5"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c4', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >6. PERSONAS QUE SE INYECTAN DROGAS</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc6"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c5', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >7. HABITANTES DE CALLE</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc7"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c6', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >8. POBLACIÓN PRIVADA DE LA LIBERTAD</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc8"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c7', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="60%" >9. NO PERTENECE A NINGUNA DE ESTAS POBLACIONES</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbc9"
                                                            onchange="modificarNotasCRUD('pcvi2', 'c8', this.value)" disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <option value="SI">SI</option>
                                                                <option value="NO">NO</option>
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divNovedades">
                                                <table width="100%">
                                                    <tr class="titulos">
                                                        <td width="50%" >V97- NOVEDADES</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbv97" style="width: 50%;"
                                                                    onchange="modificarNotasCRUD('nvvih', 'c1', this.value)"
                                                                    disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <% resultaux.clear();
                                                                    resultaux = (ArrayList) beanAdmin.combo.cargar(3669);
                                                                    ComboVO cmbNovedad;
                                                                    for (int k = 0; k < resultaux.size(); k++) {
                                                                        cmbNovedad = (ComboVO) resultaux.get(k);
                                                                %>
                                                                <option value="<%= cmbNovedad.getId()%>"
                                                                        title="<%= cmbNovedad.getTitle()%>"><%= cmbNovedad.getDescripcion()%>
                                                                </option>
                                                                <%}%>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="50%">V97.1- FECHA DE DESAFILIACION DE LA ENTIDAD</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <input type="date" id="txtv97-1" style="width: 25%;"
                                                                   onblur="modificarNotasCRUD('nvvih', 'c2', this.value)"
                                                                   disabled>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="50%">V97.2- ENTIDAD A LA CUAL SE TRASLADO EL USUARIO CON VIH DESAFILIADO</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <input type="number" id="txtv97-2" style="width: 25%;"
                                                                   onblur="modificarNotasCRUD('nvvih', 'c3', this.value)"
                                                                   disabled>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="50%">V97.3- FECHA DE MUERTE</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <input type="date" id="txtv97-3" style="width: 25%;"
                                                                   onblur="modificarNotasCRUD('nvvih', 'c4', this.value)"
                                                                   disabled>
                                                        </td>
                                                    </tr>
                                                    <tr class="titulos">
                                                        <td width="50%" >V97.4- CAUSA DE MUERTE</td>
                                                        <td class="estiloImput" style="text-align: left;">
                                                            <select id="cmbv97-4" style="width: 30%;"
                                                                    onchange="modificarNotasCRUD('nvvih', 'c5', this.value)"
                                                                    disabled>
                                                                <option value="">SELECCIONE</option>
                                                                <% resultaux.clear();
                                                                    resultaux = (ArrayList) beanAdmin.combo.cargar(3703);
                                                                    ComboVO cmbCausaMuerte;
                                                                    for (int k = 0; k < resultaux.size(); k++) {
                                                                        cmbCausaMuerte = (ComboVO) resultaux.get(k);
                                                                %>
                                                                <option value="<%= cmbCausaMuerte.getId()%>"
                                                                        title="<%= cmbCausaMuerte.getTitle()%>"><%= cmbCausaMuerte.getDescripcion()%>
                                                                </option>
                                                                <%}%>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <input name="btn_BUSCAR" title="Firmar Notas de Seguimiento NefroProteccion"
                                               type="button" class="small button blue" value="FIRMAR"
                                               onclick="modificarCRUD('firmarNota')"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
