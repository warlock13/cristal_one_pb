function modificarCRUDSolicitudesFarmacia(arg, pag) {
    if (pag == "undefined")
        pag = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";

    pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
    paginaActual = arg;
    switch (arg) {
        case "crearSolicitudHG":

            if(document.getElementById('lblIdAdmisionAgen').textContent === ''){
                alert('seleccione un paciente con Admision');
                return;
            }
            if (valorAtributoIdAutoCompletar("txtIdArticuloSolicitudes") === '') {
                alert('Seleccione un articulo');
                return
            }
            if (valorAtributo("txtCantidadSolicitud") === '') {
                alert('Digite la cantidad del producto');
                return
            }
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2284&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdAdmisionAgen"));
            add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdArticuloSolicitudes"));
            add_valores_a_mandar(valorAtributo("txtCantidadSolicitud"));
            add_valores_a_mandar(valorAtributo("txtObservacionSolicitud"));
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdArticuloSolicitudes"));
            add_valores_a_mandar(valorAtributo("cmbIdTipoServicio"));
            add_valores_a_mandar(document.getElementById('lblIdSede').textContent);
            ajaxModificar();

            break;

        case "modificaSolicitudHG":
            if (valorAtributoIdAutoCompletar("txtIdArticuloSolicitudes") === '') {
                alert('Seleccione un articulo');
                return
            }
            if (valorAtributo("txtCantidadSolicitud") === '') {
                alert('Digite la cantidad del producto');
                return
            }
            valores_a_mandar = "accion=" + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=491&parametros=";

            add_valores_a_mandar(
                valorAtributoIdAutoCompletar("txtIdArticuloSolicitudes")
            );
            add_valores_a_mandar(valorAtributo("txtCantidadSolicitud"));
            add_valores_a_mandar(valorAtributo("txtObservacionSolicitud"));
            add_valores_a_mandar(IdSesion());

            add_valores_a_mandar(valorAtributo("lblIdSolicitud"));
            ajaxModificar();
            break;

        case "eliminaSolicitudHG":
            if (valorAtributoIdAutoCompletar("txtIdArticuloSolicitudes") === '') {
                alert('Seleccione un articulo');
                return
            }
            if(valorAtributo('lblSolicitudTipo') === 'DEVOLUCION'){
                alert('No puedes realizar esta accion, si desea retirar la solicitud de devolucion, por favor use CANCELAR DEVOLUCION');
                return;

            }
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=492&parametros=";

                add_valores_a_mandar(valorAtributo("lblIdSolicitud"));
                ajaxModificar();
            }
            break;

        case 'cancelarDevolucionHG':
            if (valorAtributoIdAutoCompletar("txtIdArticuloSolicitudes") === '') {
                alert('Seleccione un articulo');
                return
            }
            if(valorAtributo('lblSolicitudTipo') === 'CONSUMO'){
                alert('No puedes realizar esta accion, el producto es de CONSUMO, si desea retirar la solicitud por favor usar ELIMINAR');
                return;

            }
            if (confirm('Esta seguro que desea cancelar la devolucion del producto')) {
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2305&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTransaccionDevo'));
                add_valores_a_mandar(valorAtributo('lblIdTransaccionDevo'));
                ajaxModificar();
            } else {
                return
            }
            break;

    }
}

function respuestaModificarCRUDSolicitudesFarmacia(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'crearSolicitudHG':
                alert('Pedido creado Correctamente !!!');
                limpiarModificarCRUDSolicitudesFarmacia('limpiarSolicitudFarmaciaHG');
                setTimeout(() => {
                    crearTablasGastos('listGrillaSolicitudFarmacia');
                }, 100);
                break;
            case 'modificaSolicitudHG':
                alert('Pedido modificado Correctamente');
                limpiarModificarCRUDSolicitudesFarmacia('limpiarSolicitudFarmaciaHG');
                setTimeout(() => {
                    crearTablasGastos('listGrillaSolicitudFarmacia');
                }, 100);
                break;

            case 'eliminaSolicitudHG':
                alert('Pedido Eliminado Correctamente');
                limpiarModificarCRUDSolicitudesFarmacia('limpiarSolicitudFarmaciaHG');
                setTimeout(() => {
                    crearTablasGastos('listGrillaSolicitudFarmacia');
                }, 100);
                break;
            case 'cancelarDevolucionHG':
                alert('Pedido devuelto a despachos del paciente');
                setTimeout(() => {
                    crearTablasGastos('listGrillaSolicitudFarmacia');
                }, 100);
                document.getElementById('txtIdArticuloSolicitudes').value = '';
                document.getElementById('txtCantidadSolicitud').value = '';
                setTimeout(() => {
                    crearTablasGastos('listHojaGastosDespachos');
                }, 150);
                break;
        }
    }
}

function limpiarModificarCRUDSolicitudesFarmacia(arg) {
    switch (arg) {
        case 'limpiarSolicitudFarmaciaHG':
            asignaAtributo('txtIdArticuloSolicitudes', '', 0);
            asignaAtributo('txtCantidadSolicitud', '', 0);
            asignaAtributo('txtObservacionSolicitud', '', 0);
            break;
    }
}