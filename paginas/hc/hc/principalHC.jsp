<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" pageEncoding="UTF-8" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session   contentType="text/html; charset=iso-8859-1"-->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->

<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>

<div id="loader" hidden>
    <img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="20px" height="20px"> <label style="font-size: small; font-style: italic;">Cargando elementos</label>
</div>
<table width="1200px" id='desplazarhc' class="desDerHC" align="center" border="0" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma"> 
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="HISTORIA CLINICA" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
                <tr>
                    <td>
                        <div style="overflow:auto;height:3px; width:100%" id="divContenido">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                        </div><!-- div contenido-->

                        <div id="divEditar" style="display:block; width:100%">
                            <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                <jsp:param name="titulo" value="LISTADO DE PACIENTES"  />
                                <jsp:param name="idDiv" value="divBuscar" />
                                <jsp:param name="pagina" value="buscar.jsp" />
                                <jsp:param name="display" value="block" /> 
                                <jsp:param name="funciones" value="ocultarTodoTipoDocumentosYAcordiones();contenedorBuscarPaciente()" /> 
                            </jsp:include>
                        </div>

                        <div id="divAcordionInfoPaciente" style="display:BLOCK">
                            <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                <jsp:param name="titulo" value="Informacion Paciente" />
                                <jsp:param name="idDiv" value="divInfoPaciente" />
                                <jsp:param name="pagina" value="contenidoInfoPaciente.jsp" />
                                <jsp:param name="display" value="NONE" />
                                <jsp:param name="funciones" value="cargarInfoPaciente()" />
                            </jsp:include>
                        </div>

                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor:pointer; margin-left: 2px; padding-right: 4px;">
                            <tr class="titulosListaEspera">
                                <td width="5%"><b>PDF</b></td>
                                <td width="5%">ID</td>
                                <td width="30%">PACIENTE</td>
                                <td width="10%">EDAD</td>
                                <td width="10%">FECHA NACIMIENTO</td>
                                <td width="10%">EPS</td>
                                <td width="10%">ESTADIO</td>
                                <td width="20%"><img id="imgInfoAdmision" src="/clinica/utilidades/imagenes/icons/icons/link.svg" width="15px" height="15px" onclick="mostrarVentanaInfoAdmision()"> ADMISION: <label style="font-size:9px"></label>
                                    <label id="lblGuardandoDoc" style="size:1PX"></label>
                                </td>
                                <td></td>
                            </tr>
                            <tr onclick="if (document.getElementById('divInfoPaciente').style.display == 'none') {
                                        mostrar('divInfoPaciente');
                                        document.getElementById('img+divInfoPaciente').src = '/clinica/utilidades/imagenes/acciones/build.png';
                                        cargarInfoPaciente()


                                    } else {
                                        ocultar('divInfoPaciente');
                                        document.getElementById('img+divInfoPaciente').src = '/clinica/utilidades/imagenes/acciones/buildup.png';
                                    }" 
                                class="estiloImputListaEspera">
                                <td>
                                    <input type="hidden" id="txtSexoPaciente">
                                    <input type="hidden" id="txtGestante">
                                    <div id="divPdfIdentificacion"></div>
                                </td>
                                <td>
                                    <label id="lblIdentificacionPaciente" style="size:1PX"></label>
                                </td>
                                <td>
                                    <label id="lblNombrePaciente"></label>
                                </td>
                                <td style="display:none;"><label id="lblEdadPaciente"></label>&nbsp;Años
                                    <!--<img src="/clinica/utilidades/imagenes/acciones/carpeta.png" width="25px" height="25px" onClick="buscarInformacionPaciente();mostrar('divFichaPaciente')"  title="Información Administrativa del paciente" />-->
                                </td>
                                <td>
                                    <label id="txtEdadCompleta"></label>
                                </td>
                                <td>
                                    <label id="lblFechaNacimiento"></label>
                                </td>
                                <td><label style="font-size:11px; color:#F00" id="lblIdAdministradora"></label>-<label
                                        style="font-size:11px; color:#F00" id="lblNomAdministradora"></label>
                                </td>
                                <td><label style="font-size:11px; color:#F00" id="lblEstadio"></label></td>
                                <td><label id="lblIdTipoAdmision" style="size:1PX"></label>-<label id="lblTipoAdmision"
                                                                                                   style="size:1PX"></label></td>
                                <td>
                                    <!-- Campo que se oculto en HC en informacion paciente porque salia un input muy notorio -->
                                    <input type="text" id="txtVrca" style="width:20%; visibility: hidden;" disabled="disabled"/>
                                    <input type="hidden" id="txtIdServicioAdmision">
                                </td>
                            </tr>
                        </table>
                        <div id="divAsociarCita" style="display:none; z-index:2000; top:100px; left:50px;">
                            <div class="transParencia" style="z-index:2003; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.4);">
                            </div>  
                            <div style="z-index:2004; position:fixed; top:200px;left:100; width:70%">  
                                <table width="90%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
                                    <tr class="estiloImput">
                                        <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divAsociarCita')" /></td>              
                                        <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" onclick="ocultar('divAsociarCita')" /></td>  
                                    </tr>
                                    <tr class="titulosCentrados" >
                                        <td width="100%" colspan="2">Asociar citas a la admision actual</td>
                                    <tr>
                                    <tr>
                                        <td colspan="2"><hr></td>
                                    </tr>
                                    <tr class="titulos">
                                        <td colspan="2">ID ADMSION ACTUAL:<label id="lblIdAdmisionAsociarCita" style="color: red;"></label> / ID FACTURA:<label id="lblIdFacturaAsociarCita"></label>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table width="100%">
                                                <tr class="titulosCentrados">
                                                    <td>CITAS PENDIENTES</td>
                                                    <td>CITAS CON ADMISION</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" id="citasDisponiblesAsociar"></table>
                                                    </td>
                                                    <td>
                                                        <table width="100%" id="citasAdmision"></table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="CENTER">
                                            <input type="button" class="small button blue" value="ASOCIAR CITAS SELECCIONADAS" onclick="verificarNotificacionAntesDeGuardar('asociarCitasAdmision')" style="width: 40%;"/>                         
                                        </td>
                                        <td>
                                            <input type="button" class="small button blue" value="VER FORMATO DE FIRMAS" onclick="imprimirPDFFormatoFirmas()" style="width: 40%;"/>                         
                                        </td>
                                    </tr>
                                </table>  
                            </div>  
                        </div>

                        <div id="divAcordionHistoricos" title="divAcordionHistoricos" style="display:BLOCK">
                            <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                <jsp:param name="titulo" value="HISTORICOS ATENCION" /> 
                                <jsp:param name="idDiv" value="divDocumentosHistoricos" />
                                <jsp:param name="pagina" value="documentosHistoricos.jsp" /> 
                                <jsp:param name="funciones" value="buscarHC('listDocumentosHistoricos', '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                                <jsp:param name="display" value="block" />
                            </jsp:include>
                        </div>

                        <div id="divPadreDeFolios" style="display:BLOCK">
                            <div id="divMedidasSignosVitales" style="display: none;">
                                <table width="100%">
                                    <tr class="titulosListaEspera">
                                        <td width="9%">PESO(KG) <label style="color: red;">*</label></td>
                                        <td width="9%">TALLA(CM) <label style="color: red;">*</label></td>
                                        <td width="9%">IMC</td>                    
                                        <td width="9%">PRESION CAPILAR</td> 
                                        <td width="9%">FRECUENCIA RESPIRATORIA</td>    
                                        <td width="9%">TENSION ARTERIAL SISTOLICA <label style="color: red;">*</label></td>
                                        <td width="9%">TENSION ARTERIAL DIASTOLICA <label style="color: red;">*</label></td>
                                        <td width="9%">TENSION ARTERIAL MEDIA</td>
                                        <td width="9%">TEMPERATURA</td>
                                        <td width="9%">SATURACION OXIGENO</td>
                                        <td width="9%">FRECUENCIA CARDIACA</td>                                                                     
                                    </tr>
                                    <tr class="estiloImput">
                                        <td width="9%"><input id="txtc1" style="width: 80%;" type="number" step="0.01" min="0" max="200" onkeyup="calcularIMC()" onblur="guardarDatosPlantilla('daan', 'c1', this.value);"></td>
                                        <td width="9%"><input id="txtc2" style="width: 80%;" type="number" min="0" max="250" onkeyup="calcularIMC()" onkeydown="noDecimal(event)" onblur="guardarDatosPlantilla('daan', 'c2', this.value);"></td>
                                        <td width="9%"><input id="txtc3" style="width: 80%;" type="number" step="0.01" min="0" max="200" disabled></td>                    
                                        <td width="9%"><input id="txtc8" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c8', this.value);"></td> 
                                        <td width="9%"><input id="txtc11" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c11', this.value);"></td>
                                        <td width="9%"><input id="txtc4" style="width: 80%;" type="number" min="0" max="300" onblur="guardarDatosPlantilla('daan', 'c4', this.value); calcularTAM();"></td>
                                        <td width="9%"><input id="txtc5" style="width: 80%;" type="number" min="0" max="300" onblur="guardarDatosPlantilla('daan', 'c5', this.value); calcularTAM();"></td>
                                        <td width="9%"><input id="txtc14" style="width: 80%;" type="number" step="0.01" min="0" max="200" disabled></td>
                                        <td width="9%"><input id="txtc7" style="width: 80%;" type="number" step="0.01" min="0" max="40" onblur="guardarDatosPlantilla('daan', 'c7', this.value);"></td>
                                        <td width="9%"><input id="txtc13" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c13', this.value);"></td>
                                        <td width="9%"><input id="txtc6" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c6', this.value);"></td>
                                    </tr>                
                                </table>
                            </div>

                            <div id="divMedidasSignosVitalesVIH" style="display: none;">
                                <table width="100%">
                                    <tr class="titulosListaEspera">
                                        <td width="9%">PESO(KG) <label style="color: red;">*</label></td>
                                        <td width="9%">TALLA(CM) <label style="color: red;">*</label></td>
                                        <td width="9%">IMC</td>                    
                                        <td width="9%">PRESION CAPILAR</td> 
                                        <td width="9%">FRECUENCIA RESPIRATORIA</td>    
                                        <td width="9%">TENSION ARTERIAL SISTOLICA <label style="color: red;">*</label></td>
                                        <td width="9%">TENSION ARTERIAL DIASTOLICA <label style="color: red;">*</label></td>
                                        <td width="9%">TENSION ARTERIAL MEDIA</td>
                                        <td width="9%">TEMPERATURA</td>
                                        <td width="9%">SATURACION OXIGENO</td>
                                        <td width="9%">PULSO</td>
                                        <td width="9%">RIESGO CARDIOVASCULAR <label style="color: red;">*</label></td>                                                                     
                                    </tr>
                                    <tr class="estiloImput">
                                        <td width="9%"><input id="txtc1" style="width: 80%;" type="number" step="0.01" min="2" max="200" onkeyup="calcularIMCDivVih()" onkeydown="maxDecimal( event, this.value,1)"  onblur="validarFormatoPeso(this);  "></td>
                                        <td width="9%"><input id="txtc2" style="width: 80%;" type="number" min="35" max="250" onkeyup="calcularIMCDivVih()" onkeydown="noDecimal(event)" onblur="guardarDatosPlantilla('daan', 'c2', this.value);"></td>
                                        <td width="9%"><input id="txtc3" style="width: 80%;" type="number" step="0.01" min="0" max="200" disabled></td>                    
                                        <td width="9%"><input id="txtc8" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c8', this.value);"></td> 
                                        <td width="9%"><input id="txtc11" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c11', this.value);"></td>
                                        <td width="9%"><input id="txtc4" style="width: 80%;" type="number" min="0" max="300" onkeyup="calcularTAM()" onblur="guardarDatosPlantilla('daan', 'c4', this.value);"></td>
                                        <td width="9%"><input id="txtc5" style="width: 80%;" type="number" min="0" max="300" onkeyup="calcularTAM()" onblur="guardarDatosPlantilla('daan', 'c5', this.value);"></td>
                                        <td width="9%"><input id="txtc14" style="width: 80%;" type="number" step="0.01" min="0" max="200" disabled></td>
                                        <td width="9%"><input id="txtc7" style="width: 80%;" type="number" step="0.01" min="0" max="40" onblur="guardarDatosPlantilla('daan', 'c7', this.value);"></td>
                                        <td width="9%"><input id="txtc13" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c13', this.value);"></td>
                                        <td width="9%"><input id="txtc6" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c6', this.value);"></td>
                                        <td width="9%"><input id="txtc1Rc" style="width: 80%;" type="number" step="0.01" min="0" max="200" disabled></td>
                                    </tr>                
                                </table>
                            </div>

                            <div id="divMedidasSignosVitalesVIHBebe" style="display: none;">
                                <table width="100%">
                                    <tr class="titulosListaEspera">
                                        <td width="9%">PESO(KG) <label style="color: red;">*</label></td>
                                        <td width="9%">TALLA(CM) <label style="color: red;">*</label></td>
                                        <td width="9%">IMC</td>                    
                                        <td width="9%">PRESION CAPILAR</td> 
                                        <td width="9%">FRECUENCIA RESPIRATORIA</td>    
                                        <td width="9%">TEMPERATURA</td>
                                        <td width="9%">SATURACION OXIGENO</td>
                                        <td width="9%">PULSO</td>                                                            
                                    </tr>
                                    <tr class="estiloImput">
                                        <td width="9%"><input id="txtc1" style="width: 80%;" type="number" step="0.01" min="0" max="200" onkeyup="calcularIMC()" onkeydown="maxDecimal( event, this.value,1)"  onblur="validarFormatoPeso(this);"></td>
                                        <td width="9%"><input id="txtc2" style="width: 80%;" type="number" min="0" max="250" onkeyup="calcularIMC()" onkeydown="noDecimal(event)" onblur="guardarDatosPlantilla('daan', 'c2', this.value);"></td>
                                        <td width="9%"><input id="txtc3" style="width: 80%;" type="number" step="0.01" min="0" max="200" disabled></td>                    
                                        <td width="9%"><input id="txtc8" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c8', this.value);"></td> 
                                        <td width="9%"><input id="txtc11" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c11', this.value);"></td>                                        
                                        <td width="9%"><input id="txtc7" style="width: 80%;" type="number" step="0.01" min="0" max="40" onblur="guardarDatosPlantilla('daan', 'c7', this.value);"></td>
                                        <td width="9%"><input id="txtc13" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c13', this.value);"></td>
                                        <td width="9%"><input id="txtc6" style="width: 80%;" type="number" step="0.01" min="0" max="200" onblur="guardarDatosPlantilla('daan', 'c6', this.value);"></td>
                                    </tr>                
                                </table>
                            </div>

                            <div id="divFormularioDatosGeneraleshc"  title="divFormularioDatosGeneraleshc"  style="display:none">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="DATOS GENERALES HISTORIA CLINICA" />
                                    <jsp:param name="idDiv" value="divDatosGeneraleshc" />
                                    <jsp:param name="pagina" value="contenidoFormularioDatosGeneraleshc.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="traerInfoGeneralHc();" />
                                </jsp:include>
                            </div>

                            <div id="divFormularioDatosGeneralesTerapia"  title="divFormularioDatosGeneralesTerapia"  style="display:none">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="DATOS GENERALES HISTORIA CLINICA" />
                                    <jsp:param name="idDiv" value="divDatosGeneralesTerapia" />
                                    <jsp:param name="pagina" value="contenidoFormularioDatosGeneralesTerapia.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="traerInfoGeneralHcTerapia()" />
                                </jsp:include>
                            </div>

                            <div id="divFormularioDatosGeneralesVIH"  title="divFormularioDatosGeneralesVIH"  style="display:none">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="DATOS GENERALES HISTORIA CLINICA" />
                                    <jsp:param name="idDiv" value="divDatosGeneralesVIH" />
                                    <jsp:param name="pagina" value="contenidoFormularioDatosGeneralesVIH.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="traerInfoGeneralHcVIH()" />
                                </jsp:include>
                            </div>


                            <div id="divAcordionAntecedentes" title="divAcordionAntecedentes"  style="display:none"> 
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Antecedentes" />
                                    <jsp:param name="idDiv" value="divAntecedentes" />
                                    <jsp:param name="pagina" value="antecedentes.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones"
                                               value="tabsContenidoEncuesta('desdeAntecedentes')" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionFolioEspecialidad" style="display:BLOCK" 
                                 onclick="validarTabsGeneroEspecialidad(divAcordionFolioEspecialidad);">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="OTROS ANTECEDENTES" />
                                    <jsp:param name="idDiv" value="divFolioEspecialidad" />
                                    <jsp:param name="pagina" value="../../encuesta/contenedorFolioEspecialidad.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="tabsContenidoEncuesta('folioEspecialidad')" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionRevisionPorSistemas" style="display:none">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="REVISION POR SISTEMAS"/>
                                    <jsp:param name="idDiv" value="divFolioPlantillaR"/>
                                    <jsp:param name="pagina" value="../../encuesta/contenedorFolioPlantillaR.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="tabsContenidoEncuesta('revisionSistemas')" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionExamenFisico" style="display:none;">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Mediciones y signos vitales" />
                                    <jsp:param name="idDiv" value="divExamenFisico" />
                                    <jsp:param name="pagina" value="examenFisico.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones"
                                               value="buscarHC('listSignosVitales' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                                </jsp:include>
                            </div>   
                            
                            <div id="divAcordionFolioPlantilla" style="display:none"
                                 onclick="validarTabsEvaluacionGeneral();">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="EVALUACION GENERAL"/>
                                    <jsp:param name="idDiv" value="divFolioPlantilla"/>
                                    <jsp:param name="pagina" value="../../encuesta/contenedorFolioPlantilla.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="tabsContenidoEncuesta('desdeFolio')" />
                                </jsp:include>
                            </div>   

                            <div id="divAcordionDiagnosticos"  style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Diagn&oacute;sticos"/>
                                    <jsp:param name="idDiv" value="divDiagnosticos"/>
                                    <jsp:param name="pagina" value="diagnosticos.jsp"/>
                                    <jsp:param name="display" value="NONE"/>
                                    <jsp:param name="funciones"
                                               value="buscarHC('listDiagnosticos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                                </jsp:include>
                            </div>

                            <div id="divEnfActual" style="display:none;">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="TAMIZAJES Y ENCUESTAS" />
                                    <jsp:param name="idDiv" value="divEncuesta" />
                                    <jsp:param name="pagina" value="../../encuesta/contenedorEncuesta.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="contenedorEncuesta()"/>  
                                </jsp:include>
                            </div>

                            <div id="divAcordionProcedimientos" onclick="validacionTabsFolios();" style="display:BLOCK" >
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Conducta y tratamiento" />
                                    <jsp:param name="idDiv" value="divProcedimientos" />
                                    <jsp:param name="pagina" value="contenidoPlanTratamiento.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="cargarTablePlanTratamiento();" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionOrdenes" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Resultados a procedimientos" />
                                    <jsp:param name="idDiv" value="divOrdenes" />
                                    <jsp:param name="pagina" value="contenidoOrdenes.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="cargarTableOrdenes()" /> 
                                </jsp:include>
                            </div>


                            <div id="divAcordionLabNefro" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Laboratorios Nefroproteccion" />
                                    <jsp:param name="idDiv" value="divLabNefro" />
                                    <jsp:param name="pagina" value="contenidoLaboratoriosNefro.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="buscarHC('listLabActual'); setTimeout(() => {buscarHC('listLabAnteriores')}, 300);" />
                                </jsp:include>
                            </div>


                            <div id="divAcordionLabVIH" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Laboratorios VIH" />
                                    <jsp:param name="idDiv" value="divLabVIH" />
                                    <jsp:param name="pagina" value="contenidoLaboratoriosVIH.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="buscarHC('listLabActualVIH'); setTimeout(() => {buscarHC('listLabAnterioresVIH')}, 300);" />
                                </jsp:include>
                            </div>
                            <div id="divAcordionLabCronicos" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Laboratorios Cronicos" />
                                    <jsp:param name="idDiv" value="divLabCronicos" />
                                    <jsp:param name="pagina" value="contenidoLaboratoriosCronicos.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="buscarHC('listLabActualCronicos'); setTimeout(() => {buscarHC('listLabAnterioresCronicos')}, 300);" />
                                </jsp:include>
                            </div>


                            
                            <div id="divAcordionFolioPlantilla" style="display:none"
                                 onclick="validarTabsEvaluacionGeneral();">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="EVALUACION GENERAL"/>
                                    <jsp:param name="idDiv" value="divFolioPlantilla"/>
                                    <jsp:param name="pagina" value="../../encuesta/contenedorFolioPlantilla.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="tabsContenidoEncuesta('desdeFolio')" />
                                </jsp:include>
                            </div>                           

                            <div id="divAcordionEncuesta" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="ENCUESTAS Y TAMIZAJES" />
                                    <jsp:param name="idDiv" value="divEncuesta" />
                                    <jsp:param name="pagina" value="../../encuesta/contenedorEncuesta.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="contenedorEncuesta()" /> 
                                </jsp:include>
                            </div>

                            <div id="divAcordionAnalisisPlan" style="display:none">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="ANALISIS Y PLAN"/>
                                    <jsp:param name="idDiv" value="divFolioPlantillaAyP"/>
                                    <jsp:param name="pagina" value="../../encuesta/contenedorFolioPlantillaAyP.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="tabsContenidoEncuesta('analisisyplan')" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionPlanT" onclick="validacionTabsFolios();" style="display:BLOCK" >
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Plan de Tratamiento" />
                                    <jsp:param name="idDiv" value="divPlanTratamiento" />
                                    <jsp:param name="pagina" value="contenidoPlanTratamientoT.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="cargarTablePlanTratamientoT();" />
                                </jsp:include>
                            </div>



                            <div id="divFactoresDeRiesgo" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="AGENDAMIENTO" />
                                    <jsp:param name="idDiv" value="divFactoresRiesgo" />
                                    <jsp:param name="pagina" value="contenidoFactoresDeRiesgo.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value=" " />
                                </jsp:include>
                            </div>

                            <div id="divAcordionHojaRuta" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="HOJA DE RUTA" />
                                    <jsp:param name="idDiv" value="divHojaRuta" />
                                    <jsp:param name="pagina" value="../../planAtencion/contenedorHojaRuta.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="contenedorRutaPacienteHC()" />
                                </jsp:include>
                            </div>                  

                            <div id="divAcordionAgendamientoHC" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="AGENDAMIENTO" />
                                    <jsp:param name="idDiv" value="divAgendamiento" />
                                    <jsp:param name="pagina" value="agendamientohc.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="" />
                                </jsp:include> 
                            </div>

                            <!-- adm de medicamentos -->
                            <div id="divAcordionAdmMedicamentos" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Administracion de Medicamentos" />
                                    <jsp:param name="idDiv" value="divAdmMedicamentos" />
                                    <jsp:param name="pagina" value="administracionMedicamentos.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones"
                                               value="buscarHC('listAdministracionMedicacion' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                                </jsp:include>
                            </div>

                            <!-- HOJA DE GASTOS -->
                            <div id="divAcordionHojaGastos" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Hoja de Gastos" />
                                    <jsp:param name="idDiv" value="divHojaGastos" />
                                    <jsp:param name="pagina" value="contenidoHojaDeGastos.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="cargarTableHojaDeGastos()" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionAdjuntos" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Archivos adjuntos" />
                                    <jsp:param name="idDiv" value="divArchivosAdjuntos" />
                                    <jsp:param name="pagina" value="../../archivo/contenidoAdjuntos.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="acordionArchivosAdjuntos()" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionContenidos" style="display:BLOCK">
                                <jsp:include page="divsAcordeonFormulario.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="" />
                                    <jsp:param name="idDiv" value="divContenidos" />
                                    <jsp:param name="pagina" value="../../contenidos.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="contenidoDoc()" />
                                </jsp:include>
                            </div>                  

                            <div id="divAcordionAntecedentesFarmacologicos" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Antecedentes Farmacologicos" />
                                    <jsp:param name="idDiv" value="divAntecedentesFarmacologicos" />
                                    <jsp:param name="pagina" value="antecedentesFarmacologicos.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones"
                                               value="buscarHC('listAntecedentesFarmacologicos' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionExamenFisicoDescripcion" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Examen Fisico" />
                                    <jsp:param name="idDiv" value="divExamenFisicoDescripcion" />
                                    <jsp:param name="pagina" value="examenFisicoDescripcion.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="guardarYtraerDatoAlListado('consultaExamenFisico')" />

                                </jsp:include>
                            </div>

                            <div id="divAcordionProcedimientosCir" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Procedimientos Programados" />
                                    <jsp:param name="idDiv" value="divProcedimientosCir" />
                                    <jsp:param name="pagina" value="contenidoProcedimientosProgramacion.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="cargarTableProcedimientosProgramados()" />
                                </jsp:include>
                            </div>

                            <!-- MONITORIZACION HEMODINAMICA DE HEMODIALISIS AGUDA -->
                            <div id="divAcordionSignosVitales" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Signos Vitales" />
                                    <jsp:param name="idDiv" value="divSignosVitales" />
                                    <jsp:param name="pagina" value="signosVitales.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones"
                                               value="buscarHC('listMonitorizacion' , '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp' )" />
                                </jsp:include>
                            </div>
                            <!-- FIN  -->

                            <div id="divAcordionOdonto" style="display:BLOCK">
                                <jsp:include page="divsAcordeonFormulario.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="ODONTOGRAMA Y SEGUIMIENTO" />
                                    <jsp:param name="idDiv" value="divOdonto" />
                                    <jsp:param name="pagina" value="../saludOral/contenidoAcordionesOdontograma.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="cargarAcordionesOdontograma()" />
                                </jsp:include>
                            </div>

                            <div id="divAcordionIndicaciones" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Otras Indicaciones " />
                                    <jsp:param name="idDiv" value="divIndicaciones" />
                                    <jsp:param name="pagina" value="indicaciones.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                </jsp:include>
                            </div>

                            <div id="divVideollamada" style="display:NONE">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Videollamada " />
                                    <jsp:param name="idDiv" value="divVideo" />
                                    <jsp:param name="pagina" value="videollamada.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                </jsp:include>
                            </div>
                            <div id="divAdministrativa" style="display:BLOCK">
                                <jsp:include page="divsAcordeon.jsp" flush="FALSE">
                                    <jsp:param name="titulo" value="Administrativa" />
                                    <jsp:param name="idDiv" value="divAdministrativos" />
                                    <jsp:param name="pagina" value="contenidoAdministrativa.jsp" />
                                    <jsp:param name="display" value="NONE" />
                                    <jsp:param name="funciones" value="cargarAdministrativo()" />
                                </jsp:include>
                            </div>

                            <table width="100%">
                                <tr class="estiloImputListaEspera">
                                    <td align="center">
                                        <input type="button" class="small button blue" title="BT7QR" value="VISTA PREVIA"
                                               onClick="imprimirHCPdf()" />            
                                        <input class="small button blue" type="button" id="btnFirmarFolio" 
                                               onClick="firmarFolio();" value="FIRMAR">
                                    </td>
                                </tr>
                            </table>

                            <% if (beanSession.usuario.preguntarMenu("b611D") || beanSession.usuario.preguntarMenu("HC")) {%>
                            <table width="100%">
                                <tr class="titulosListaEspera">
                                    <td>ACCIONES AL FOLIO</td>
                                    <td style="display: none;">MOTIVOS APERTURA</td>
                                    <td>OBSERVACIONES</td>
                                    <td></td>
                                </tr>
                                <tr class="estiloImputListaEspera">
                                    <td>
                                        <select id="cmbAccionFolio" style="width:70%" title="32" onchange="mostrarMotivosAbrir()">      
                                            
                                            <%     resultaux.clear();
                                            resultaux=(ArrayList)beanAdmin.combo.cargar(10399);	
                                            ComboVO cmbAV; 
                                            for(int k=0;k<resultaux.size();k++){ 
                                                  cmbAV=(ComboVO)resultaux.get(k);
                                             %>
                                            <option value="<%= cmbAV.getId()%>" title="<%= cmbAV.getTitle()%>"><%= cmbAV.getDescripcion()%></option>
                                            <%}%>  

                                            <!--<option value="0">ABRIR</option>-->                    
                                            <!-- option value="3">CREAR NOTA ACLARATORIA</option>
                                            <option value="4">CREAR EVOLUCION NOTA DE ENFERMERIA</option>
                                            <option value="14">CREAR NOTA EVOLUCION POSTCONSULTA</option>
                                            <option value="11">CREAR NOTA EVOLUCION NEFROPROTECCION</option>
                                            <option value="16">CREAR NOTA SEGUIMIENTO VIH</option>
                                            <option value="8">CREAR FORMATO DE ENMIENDA</option>
                                            <option value="9">CREAR INFORME FINAL DE TERAPIAS</option>
                                            <option value="2" title="PERMITE CLONAR LA HISTORIA CLINICA">CLONAR HISTORIA CLINICA</option>
                                            <option value="1">FIRMAR NOTA ACLARATORIA/FORMATO DE ENMIENDA</option>
                                            <option value="5">ELIMINAR FOLIO EN ESTADO BORRADOR</option>
                                            <option value="10">AUDITAR FOLIO DE MEDICINA GENERAL - EXPERTO B24X</option>
                                            <option value="17">CREAR FOLIO PRECONSULTA - B24X</option>
                                            <option value="18">CREAR NOTA DE SEGUIMIENTO</option> -->
                                            <!--<option value="7">ANULAR</option>-->
                                        </select>
                                    </td>
                                    <td style="display: none;">
                                        <select style="width:70%" id="cmbMotivosAbrir">
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMotivoAccion" size="100" maxlength="100" style="width:90%" />
                                    </td>
                                    <td>
                                        <button id="btnVideo" onclick="teleconsulta()"></button>
                                        <input id="btnabrirDireMedica" type="button" class="small button blue"
                                               value="EJECUTAR ACCION" title="BT00I- DESDE DIRECCION MEDICA"
                                               onClick="verificarNotificacionAntesDeGuardar('accionesAlEstadoFolio')" />
                                    </td>
                                </tr>
                            </table>
                            <%}%>
                        </div><!-- divPadreDeFolios-->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <label style="font-size:10px" id="lblIdEspecialidad"></label>
            <label style="font-size:10px" id="lblNombreEspecialidad"></label>&nbsp;&nbsp;&nbsp;
            <label style="font-size:10px" id="lblTipoDocumento"></label>
            <label style="font-size:10px" id="lblDescripcionTipoDocumento"></label>&nbsp;&nbsp;&nbsp;
            <label style="font-size:10px" id="lblGeneroPaciente" hidden></label>
            <label style="font-size:10px" id="lblEdadPaciente" hidden></label>
            <label style="font-size:10px" id="lblMesesEdadPaciente" ></label>
            <H6 align="right">
                Id Auxiliar=<label style="font-size:9px" id="lblIdAuxiliar">98400407</label>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </H6>
        </td>
    </tr>
</table>
<div class="imgLogo">
    <img src="/clinica/utilidades/imagenes/menu/CRYSTALlogo.png" width="100px" height="100px" alt="">
</div>

<!-- <div id="divValidaciones" hidden> -->
<div id="divValidaciones" hidden>
    <table id="listGrillaFormulas" class="scroll"></table>
    <table id="listGrillaFormulasE" class="scroll"></table>
    <table id="listGrillaValoresValidaciones" class="scroll"></table>
    <table id="listGrillaValoresValidacionesE" class="scroll"></table>
    <table id="listGrillaValidaciones" class="scroll"></table>
    <table id="listGrillaValidacionesE" class="scroll"></table>
</div>

<div id="divFichaPaciente"
     style="position:absolute; display:none; background-color:#E2E1A5; top:210px; left:200px; width:800px; height:100px; z-index:999">
    <table width="100%" border="1" class="fondoTabla">
        <tr class="estiloImput">
            <td colspan="3" align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                                 onClick="ocultar('divFichaPaciente')" /></td>
        <tr>
        <tr class="titulos">
            <td width="10%" colspan="3">
                <jsp:include page="divsSinAcordeon.jsp" flush="FALSE">
                    <jsp:param name="titulo" value="Informacion Administrativa Paciente" />
                    <jsp:param name="pagina" value="../fichaPaciente.jsp" />
                    <jsp:param name="display" value="block" />
                </jsp:include>
            </td>
        </tr>
    </table>
    <H6 align="right">
        <input type="hidden" id="txtIdUsuario" value="<%=beanSession.usuario.getIdentificacion()%>" />
        <input type="hidden" id="txtLogin" value="<%=beanSession.usuario.getLogin()%>" />
        <input type="hidden" id="txtPrincipalHC" value="OK" />
        IdPaciente=<label id="lblIdPaciente" style="size:1PX"></label>
    </H6>
</div>

</body>


<div id="divVentanitaPdf" style="display:none; z-index:2050; top:1px; left:200px; position: absolute; width: 100%;">
    <div id="idDivTransparencia" class="transParencia"
         style="z-index:2054; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60; width: 100%;">
    </div>
    <div id="idDivTransparencia2" style="z-index:2055; position:absolute; top:5px; left:15px;">
        <table id="idTablaVentanitaPDF" width="90%" height="90" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                        onClick="ocultar('divVentanitaPdf')" /></td>
                <td>&nbsp;</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR"
                                         onClick="ocultar('divVentanitaPdf')" /></td>
            </tr>
            <tr class="estiloImput">
                <td colspan="3">
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
                                <div id="divParaPaginaVistaPrevia"></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>

<input type="hidden" id="txtIdBusPaciente" value="" />
<input type="hidden" id="txtIdProfesionalElaboro" value="" />


<div id="divAuxiliar" style="display:none; z-index:2000; top:111px; left:-211px;">
    <div class="transParencia" style="z-index:2001; background:#999; filter:alpha(opacity=5);float:left;">
    </div>
    <div style="z-index:2002; position:absolute; top:200px; left:60px; width:95%">
        <table width="70%" height="90" cellpadding="0" cellspacing="0" class="fondoTabla">
            <tr class="estiloImput">
                <td align="left"><input name="btn_cerrar" type="button" align="right" value="CERRAR" disabled="disabled"
                                        onClick="ocultar('divAuxiliar')" /></td>
                <td>&nbsp</td>
                <td>&nbsp</td>
                <td align="right"><input name="btn_cerrar" type="button" align="right" value="CERRAR" disabled="disabled"
                                         onClick="ocultar('divAuxiliar')" /></td>
            <tr>
            <tr>
                <td class="titulos" colspan="4">DIGITE CONTRASEÑA DEL AUXILIAR DE CONSULTORIO</td>
            </tr>
            <tr>
                <td colspan="4">
            <center>
                <input id="txtContrasenaAdm" value="CRISTAL" type="password" size="32" maxlength="30"
                       onChange="verificarContrasenaAxiliar(this.value)"  />
            </center>
            </td>
            </tr>
        </table>
    </div>
</div>


<div id="divParaVentanita"></div>
<input type="hidden" id="txt_banderaOpcionCirugia" value="NO" />




<div id="divVentanitaODONTOGRAMA" style="display:none; z-index:9997; top:-51px; left:50px; width: 1200px">

    <div id="ventanitaHija" style="z-index:9999; position:absolute; top:0px; left:60px; width:1200px;">
        <table width="1200px" align="center" class="fondoTablaAmarillo">
            <tr>
                <td width="100%" align="center" colspan="2">

                    <div id="divAcordionTratamientoPorDiente" style="display:BLOCK">
                        <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
                            <jsp:param name="titulo" value="Descripcion del Tratamiento" />
                            <jsp:param name="idDiv" value="divTratamientoPorDiente" />
                            <jsp:param name="pagina" value="../saludOral/contenidoTratamientoPorDiente.jsp" />
                            <jsp:param name="display" value="BLOCK" />
                            <jsp:param name="funciones" value="cargarTratamientoPorDiente()" />
                        </jsp:include>
                    </div>
                    <div id="divAcordionTratamientoSeguimiento" style="display:BLOCK">
                        <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
                            <jsp:param name="titulo" value="Seguimiento" />
                            <jsp:param name="idDiv" value="divTratamientoSeguimiento" />
                            <jsp:param name="pagina" value="../saludOral/contenidoOdontograma.jsp" />
                            <jsp:param name="display" value="NONE" />
                            <jsp:param name="funciones" value="ocultar('divControlDePlaca');cargarOdontograma()" />
                        </jsp:include>
                    </div>
                    <div id="divAcordionTratamientoInicial" style="display:BLOCK">
                        <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
                            <jsp:param name="titulo" value="Odontograma inicial" />
                            <jsp:param name="idDiv" value="divTratamientoInicial" />
                            <jsp:param name="pagina" value="../saludOral/contenidoOdontogramaInicial.jsp" />
                            <jsp:param name="display" value="NONE" />
                            <jsp:param name="funciones" value="cargarOdontogramaInicial()" />
                        </jsp:include>
                    </div>
                    <div id="divAcordionIndiceDePlaca" style="display:BLOCK">
                        <jsp:include page="divsAcordeonHg.jsp" flush="FALSE">
                            <jsp:param name="titulo" value="Indice De Placa" />
                            <jsp:param name="idDiv" value="divControlDePlaca" />
                            <jsp:param name="pagina" value="../saludOral/contenidoControlDePlaca.jsp" />
                            <jsp:param name="display" value="NONE" />
                            <jsp:param name="funciones" value="cargarControlDePlaca()" />
                        </jsp:include>
                    </div>

                </td>
                <td align="right" valign="top">
                    <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" title="Cerrar"
                         onclick="javascript:ocultar('divVentanitaODONTOGRAMA')" width="15" height="15">
                </td>
            </tr>
        </table>

    </div> 
</div>

<div  id="divNotificacionAlertFolioPlantilla" align="center" title="MENSAJE" style="z-index:3000; top:0px; left:0px; position:fixed; display:none;" >
    <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
    </div> 
    <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%;">     
        <table bgcolor="#FFFFFF" align="center"  width="300px" height="100px" id="tablaNotificacionesAlert" style="border: 1px solid #000;" >
            <tr>
                <td bgcolor="#7CD9FC" align="center" background="/clinica/utilidades/imagenes/menu/six_0_.gif" >
                    <img align="left" height="20" width="20" src="/clinica/utilidades/imagenes/acciones/alertaIntermitente.gif"/>
                    <b>ALERTA</b>
                </td>      
            </tr>	   
            <tr>
                <td>
                    <i><label style="font-family: Helvetica;" id="lblTotNotifVentanillaAlert">HA OCURRIDO UN PROBLEMA AL INTENTAR GUARDAR LA INFORMACI&Oacute;N.<br><br>POR FAVOR VERIFIQUE SU CONEXI&Oacute;N A INTERNET E INTENTE NUEVAMENTE.<br>SI EL PROBLEMA PERSISTE GUARDE EXTERNAMENTE LOS CAMPOS CON ERROR Y COMUNIQUESE CON SOPORTE TECNICO.</label></i>
                    <i><label style="font-family: Helvetica; display:none;" class="status_error"></label></i>
                </td>      
            </tr>
            <tr>
                <td align="center">  
                    <TABLE id="idTableBotonNotificaAlert" width="50%" border="1">
                        <TR>
                            <TD align="center">
                                <i><input style="font-family: Helvetica;" id="alertRepBoton" type="button" class="small button blue" value="VUELVA A INTENTAR" title="BT86E" onclick="ocultar('divNotificacionAlertFolioPlantilla')" ></i>
                            </TD>
                        </TR>
                    </TABLE>
                </td>        	               
            </tr>            
        </table>
        <br/>
    </div>   
</div>


<div id="divContenedorParalelo" style="display: none;" class="contenedorParalelo">
    <div id="divParalelo" class="Paralelo">
        <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divContenedorParalelo')" />
    </div>
    <table width="100%"  border="1"> 
        <tr class="titulosListaEspera">
            <td style="width: 100%;" colspan="2">
                SIGNOS VITALES
            </td>
        </tr>          
        <tr class="estiloImput">
            <td style="width: 33%;">
                Peso(KG): <label id="lblPesoParalelo"></label>
            </td>
            <td style="width: 33%;">
                Talla(CM): <label id="lblTallaParalelo"></label>
            </td>      
        </tr>
        <tr class="estiloImput">
            <td style="width: 33%;">
                Tension Arterial Sistolica: <label id="lblTasParalelo"></label>
            </td>
            <td style="width: 33%;">
                Tension Arterial Diastolica: <label id="lblTadParalelo"></label>
            </td>      
        </tr>
        <tr class="estiloImput CRNC_paralelo">
            <td style="width: 33%;">
                TFGe: <label id="lblTfgParalelo"></label>
            </td>
            <td style="width: 33%;">
                Estadio: <label id="lblEstadioParalelo"></label>
            </td>      
        </tr>
        <tr class="titulosListaEspera" >
            <td style="width: 100%;" colspan="2">
                MOTIVO CONSULTA
            </td>        
        </tr>
        <tr class="estiloImput">
            <td style="width: 100%;" colspan="2">
                <textarea id="txtMotivoConsultaParalelo" style="width: 100%; height: 50px;"></textarea>
            </td>
        </tr>
        <tr class="titulosListaEspera" >
            <td style="width: 100%;" colspan="2">
                ENFERMEDAD ACTUAL
            </td>        
        </tr>
        <tr class="estiloImput">
            <td style="width: 100%;" colspan="2">
                <textarea id="txtEnfermedadActualParalelo" style="width: 100%; height: 280px;"></textarea>
            </td>
        </tr>
        <tr class="titulos">
            <td colspan="2">
                <table id="listDiagnosticosParalelo"></table>
            </td>
        </tr>
        <tr class="titulosListaEspera" >
            <td style="width: 100%;" colspan="2">
                ANALISIS Y PLAN
            </td>        
        </tr>
        <tr class="estiloImput">
            <td style="width: 100%;" colspan="2">
                <textarea id="txtAnalisisPlanParalelo" style="width: 100%; height: 280px;"></textarea>
            </td>
        </tr>
        <tr class="titulos">
            <td colspan="2">
                <table id="listProcedimientosParalelo"></table>
            </td>
        </tr>
        <tr class="titulos">
            <td colspan="2">
                <table id="listMedicamentosParalelo"></table>
            </td>
        </tr>    
        <tr class="titulos">
            <td colspan="2">
                <table id="listParaleroLaboratorios"></table>
            </td>
        </tr>
    </table>
</div>

<div class="bubble" id="bubble" onclick="maximizarVentanita('divInfoPacienteVentana')">
    <i class="fa-solid fa-address-card"></i>
</div>

<div id="divInfoPacienteVentana" style="display: none; top: 75px; right: 3%; width: 170px; position: fixed; white-space: normal;" onclick="moverTarjeta()">
    <div class="card-container">
  
        <div class="interno-card" id="miTarjeta">
          <div class="class-card barbarian">
            <div class="class-card__image class-card__image--barbarian">
                <!-- <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divInfoPacienteVentana')" /> -->
                <label onclick="ocultar('divInfoPacienteVentana')" class="cerrarInfoPaciente">
                    <i class="fas fa-times-circle"></i>
                </label>
                <label onclick="minimizarVentanita('divInfoPacienteVentana')" class="minimizarInfoPaciente">
                    <i aria-hidden="true" class="fas fa-chevron-circle-down" ></i>
                </label>
              <i class="fas fa-hospital-user fasCard"></i>
            </div>
            <div class="class-card__level class-card__level--barbarian">NOMBRE PACIENTE:</div>
            <div class="class-card__unit-name"><label id="lblNombrePacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label></div>
            <div class="grRh">
                <label for="">RH:</label>
                <label for="" id="lblRH"></label>
            </div>
            <div class="class-card__unit-description">
                <label id="lblEdadPacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label><label class="" style="color:white;"></label>
            </div>
            <div class="parentCard">

                <div id="divDatosGestantes" style="display: flex; padding-top: 5%; margin-left: 13%; gap:5px;">
                    <label style="color:rgb(231, 58, 58);"> <b>Gestante</b> </label>

                    <div class="onoffswitch2" >
                        <input type="checkbox" name="chkgestante" class="onoffswitch-checkbox2" id="chkgestante"  onclick="valorSwitchsn(this.id, this.checked);guardarGestanteVentanita(document.getElementById('lblIdPaciente').innerHTML , this.value,'ventana principal'); ">  
                        <label class="onoffswitch-label2" for="chkgestante">
                            <span  class="onoffswitch-inner2" id="chkgestante-span1"></span>
                            <span  class="onoffswitch-switch2" id="chkgestante-span2"></span>
                        </label>
                    </div>

                </div>

                <div>
                    <!-- <h5>Creatinina</h5> -->
                    <p><label id="lblCreatininaVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
                </div>
                <div>
                    <!-- <h5>TFG Actual</h5> -->
                    <p><label id="lblTFGVentanita" class="" style="color:rgb(0, 0, 0);"></label></p>
                </div>
                <div>
                    <!-- <h5>GRADO</h5> -->
                    <p><label id="lblEstadioVentanita" class="" style="color:rgb(1, 1, 1);"></label></p>
                </div>
            </div>      
          </div> <!-- end class-card barbarian-->
        </div> <!-- end wrapper -->
      </div> <!-- end container -->
</div>
<!-- <div id="divInfoPacienteVentana" style="display: none; top: 75px; right: 4%; width: 150px; position: fixed;">
    <table style="background-color: #ffffff;">
        <tr>
            <td>
                <img id="imgCerrar" src="/clinica/utilidades/imagenes/acciones/cerrarVentana.png" width="15" height="15" title="Cerrar" style="cursor:pointer;" onclick="ocultar('divInfoPacienteVentana')" />
            </td>
        </tr>
        <tr>
            <td>
                <label id="lblCedulaPacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label id="lblNombrePacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label id="lblEdadPacienteVentanita" class="" style="color:rgb(0, 0, 0);"></label><label class="" style="color:white;"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label id="lblCreatininaVentanita" class="" style="color:rgb(0, 0, 0);"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label id="lblTFGVentanita" class="" style="color:rgb(0, 0, 0);"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label id="lblEstadioVentanita" class="" style="color:rgb(1, 1, 1);"></label>
            </td>      
        </tr>
        <tr>
            <td>
                <label id="lblDiagnosticosVentanita" class="" style="color:rgb(0, 0, 0);"></label>
            </td>      
        </tr>
    </table>
</div> -->

<div  id="accionesAlEstadoFolio" align="center" title="MENSAJE" style="z-index:3000; top:0px; left:0px; position:fixed; display:none;" >
    <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
    </div> 
    <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%;">     
        <table bgcolor="#FFFFFF" align="center"  width="300px" height="100px" id="tablaNotificacionesAlert" style="border: 1px solid #000;" >
            <tr>
                <td bgcolor="#7CD9FC" align="center" background="/clinica/utilidades/imagenes/menu/six_0_.gif" >
                    <b>ALERTA</b>
                </td>      
            </tr>	   
            <tr>
                <td>
                    <i><img src="/clinica/utilidades/imagenes/ajax-loader.gif" alt="" width="100px" height="100px"></i>
                </td>
                <td>
                    <i><label style="font-family: Helvetica;" id="lblTotNotifVentanillaAlert">Por favor, espere unos segundos hasta completar la acci&oacute;n al folio</label></i>
                </td>      
            </tr>
            <tr>
                <td align="center">  
                    <TABLE id="idTableBotonNotificaAlert" width="50%" border="1">
                    </TABLE>
                </td>        	               
            </tr>            
        </table>
        <br/>
    </div>   
</div>


<div id="divFirmarFolio" title="MENSAJE" style="z-index: 3000; top: 0px; left: 0px; position: fixed; visibility: visible; display: none;" class="cssjas" align="center">
    <div class="transParencia" style="z-index:3001; background-color: rgba(0,0,0,0.4);">
    </div> 
    <div style="z-index:3002; position:absolute; top:250px; left:600px; width:100%; ">     
        <table id="tablaNotificacionesAlert" style="border: 1px solid #000;" width="300px" height="100px" bgcolor="#FFFFFF" align="center">
            <tr>
                <td bgcolor="#7CD9FC" background="../utilidades/imagenes/menu/six_0_.gif" align="center">
                    <b id="tittleFirmarFolio"></b>
                    <img id="imgCerrarAlerta" style="padding-right: 5px;" src="../utilidades/imagenes/acciones/cerrar2.png" onclick="$('#divFirmarFolio').hide()" width="16" height="16" align="right">

                </td>      
            </tr>	   
            <tr>
                <td>
                    <i><label style="font-family: Helvetica;" id="lblFirmarFolio"></label></i>
                </td>      
            </tr>
            <tr>
                <td align="center">  
                    <input class="small button blue" type="button"  onClick="$('#divFirmarFolio').hide()" value="SALIR">
                    <input class="small button blue" type="button" id="btnFirmarFolioAlerta" onClick="verificarNotificacionAntesDeGuardar('verificarDiagnosticoHipertension');
                            $('#divFirmarFolio').hide()" value="CONTINUAR">
                </td>
            </tr>            
        </table>
    </div>   
</div>

<div id="divValidacionesVih"  style="display: none; top: 30%; left:75%; width: 590px; position: fixed;">
    <table>
        <tr>

            <td> 
                <div>
                    <input class="small button blue" type="button" onClick="validacionesVih('', '');"
                           value="GENERAR VALIDACION">
                </div>
                <div>

                    <table id="listValidacionesVih" class="scroll" ></table> 
                </div>
            </td>

        </tr>
        <tr>
            <td>
                <div  id="divDescripcionRegla" style="width: 500px;   height: auto;
                      word-wrap: break-word;  background: #99bdce;   ">

                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divValidacionesVariables" style="width: 500px; ">
                    <table id="listVariablesVih" class="scroll" > </table> 
                </div>
            </td>
        </tr>
    </table>
</div>

<input type="hidden" id="txtServicioAuditoria">
<input type="hidden" id="txtDxRemision">