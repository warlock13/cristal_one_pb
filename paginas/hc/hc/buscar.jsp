<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" pageEncoding="UTF-8" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn());
    ArrayList resultaux = new ArrayList();
%>
<!--******************************** inicio de la tabla con los datos de busqueda ***************************-->
<div id="divBuscar" style="display:block; height:400px; margin-left: 1px;">
    <table width="99.7%" cellpadding="0" cellspacing="0" align="center">
        <tr class="titulosListaEspera" align="center">
            <td width="15%">SERVICIO</td>
            <td width="10%">TIPO ID</td>
            <td width="10%">DOCUMENTO</td>
            <td width="20%">PACIENTE</td>
            <td width="10%">FECHA AGENDA INICIO</td>
            <td width="10%">FEHA AGENDA FIN</td>
            <td width="5%"></td>
        </tr>
        <tr class="estiloImputListaEspera">
            <td>
                <select size="1" id="cmbIdTipoServicio" style="width:90%" title="76" 
                        onchange="activarODesactivarElemServicio()">
                    <% resultaux.clear();
                        resultaux = (ArrayList) beanAdmin.combo.cargar(13);
                        ComboVO cmbt;
                        for (int k = 0; k < resultaux.size(); k++) {
                            cmbt = (ComboVO) resultaux.get(k);
                    %>
                    <option value="<%= cmbt.getId()%>" title="<%= cmbt.getTitle()%>"><%= cmbt.getDescripcion()%></option>
                    <%}%>                    
                </select>     
            </td>

            <td >
                <select size="1" id="cmbTipoId" style="width:90%"  >
                    <option value=""></option>
                    <%     resultaux.clear();
                        resultaux = (ArrayList) beanAdmin.combo.cargar(105);
                        ComboVO cmb105;
                        for (int k = 0; k < resultaux.size(); k++) {
                            cmb105 = (ComboVO) resultaux.get(k);
                    %>
                    <option value="<%= cmb105.getId()%>" title="<%= cmb105.getTitle()%>">
                        <%= cmb105.getId() + "  " + cmb105.getDescripcion()%></option>
                        <%}%>						
                </select>
            </td> 
            <td  >
                <input  type="text" id="txtIdentificacion" style="width: 90%;"  onkeypress="javascript:return soloNumeroIdentificacionHc(event);"/>	         
            </td>
            <td> 
                <input type="text" size="70"  maxlength="70" style="width:90%"  id="txtIdBusPaciente"   onkeyup="event.key == 'Enter'?contenedorBuscarPaciente():'';"/>   
                <input id="btnActCie" type="hidden" class="small button blue" value="a" title="BNT_ACT_CIE" onClick="modificarCRUD('activarCIE', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"/>                                             
                <input id="btnDesactCie" type="hidden" class="small button blue" value="d" title="BNT_DSCT_CIE" onClick="modificarCRUD('desactivarCIE', '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')" disabled="disabled"/>                                             
                <img  width="18px" height="18px" align="middle" title="VEN52" id="idLupitaVentanitaIdenT" onclick="traerVentanitaPaciente(this.id, '', 'divParaVentanita', 'txtIdBusPaciente', '27')" src="/clinica/utilidades/imagenes/acciones/buscar.png"> 
            </td> 
            <td>
                <input id="txtFechaAgenda" type="text" onchange="contenedorBuscarPaciente()" style="width: 90%;">
            </td>     
            <td>
                <input id="txtFechaAgendaFin" type="text" onchange="contenedorBuscarPaciente()" style="width: 90%;">
            </td>                       
            <td>                     
                <input id="imgBuscarPaciente"  type="button" class="small button blue"  value="B U S C A R" title="BT86R" onclick="contenedorBuscarPaciente()"  />
            </td>
        </tr>
    </table>


    <table id="idElemAYD" style="display:none"  width="800PX">
        <tr>
            <td width="30%" >Tipo folio
                <select size="1" id="cmbTipoFolio" style="width:60%" title="76"  onfocus="comboTiposDeFolios()" >
                    <option value=""></option>                                                                                          
                </select>
            </td>                         
            <td width="30%" >Estado:
                <select size="1" id="cmbIdEstadoFolio" style="width:60%" title="76"  onchange="cambiarEstadoFolioAyudaDiagnostica()" >
                    <option value=""></option>                                                                                          
                    <option value="7">Cancelado Finalizado</option>                                                            
                    <option value="2">Tomado</option>
                    <option value="11">Para direccion medica</option>                                  
                    <option value="3">Enviado</option>      
                    <option value="5">Enviado urgente</option>                                                
                    <option value="4">Interpretado</option> 
                    <option value="6">Interpretado Urgente</option> 
                    <option value="1">Finalizado</option>                                   
                    <option value="8">Finalizado Urgente</option>
                    <option value="9">Finalizado Alerta</option>
                </select>
            </td>  
            <td width="20%" >fecha Desde:
                <input id="txtFechaDesdeFolio"  type="text"  size="10"  maxlength="10"  />
            </td>  
            <td width="20%" >fecha hasta:
                <input id="txtFechaHastaFolio"  type="text"  size="10"  maxlength="10"  />
            </td>  
            </td>  
    </table>

    <div id="idElemHOSDOM" style="width: 800px;" hidden>                                              
        <table width="100%" cellpadding="0" cellspacing="0" border="1">
            <tr class="titulos">
                <td width="30%">DEPARTAMENTO</td> 
                <td width="30%">MUNICIPIO</td>
                <td width="30%">COMUNA / LOCALIDAD</td>                                        
            </tr>     
            <tr class="estiloImput" >
                <td>
                    <select id="cmbIdDepartamento" style="width:80%" 
                            onfocus="cargarDepDesdeServicio('cmbIdTipoServicio', 'cmbIdDepartamento')"
                            onchange="limpiaAtributo('cmbIdMunicipio', 0); limpiaAtributo('cmbIdLocalidad', 0);">	                                        
                        <option value=""></option>
                    </select>	                   
                </td>   
                <td>
                    <select size="1" id="cmbIdMunicipio" style="width:80%"  
                            onfocus="cargarMunicipioDesdeDepartamento('cmbIdDepartamento', 'cmbIdTipoServicio', 'cmbIdMunicipio')"
                            onchange="limpiaAtributo('cmbIdLocalidad', 0);">	                                        
                        <option value=""></option>                                  
                    </select>	                   
                </td>  
                <td>
                    <select id="cmbIdLocalidad" style="width:80%" 
                            onfocus="cargarLocalidad('cmbIdDepartamento', 'cmbIdMunicipio', 'cmbIdTipoServicio', 'cmbIdLocalidad')">
                        <option value=""></option>
                    </select>	                   
                </td>            
            </tr>                            
        </table>
    </div>

    <table id="idElemHOS" style="display:none" width="800PX">
        <tr>
            <td width="30%">                          
                <table width="100%" cellpadding="0" cellspacing="0" border="1">
                    <tr class="titulos">
                        <td width="30%">PISO/AREA</td> 
                        <td width="30%">HABITACION</td>
                        <td width="30%">ESTADO CAMA</td>                                        
                    </tr>     
                    <tr class="estiloImput" >
                        <td><select id="cmbIdArea" style="width:80%"   onfocus="cargarAreaDesdeServicio('cmbIdTipoServicio', 'cmbIdArea')" onchange="asignaAtributo('cmbIdHabitacion', '', 0)" >	                                        
                                <option value=""></option>		
                            </select>	                   
                        </td>   
                        <td><select size="1" id="cmbIdHabitacion" style="width:80%"  onfocus="cargarHabitacionDesdeArea('cmbIdArea', 'cmbIdHabitacion')"   >	                                        
                                <option value=""></option>                                  
                            </select>	                   
                        </td>  
                        <td>
                            <select id="cmbIdEstadoCama" style="width:80%" 
                                    onchange="asignaAtributo('cmbIdHabitacion', '', 0)">
                                <option value=""></option>
                            </select>	                   
                        </td>            
                    </tr>                            
                </table>
            </td> 
        </tr>  
    </table>     

    <table width="50%" id="idElementoCEX" hidden>
        <tr class="titulosListaEspera">
            <td width="50%">ESPECIALIDAD</td>
            <td width="50%">PROFESIONAL</td>
        </tr>
        <tr class="estiloImputListaEspera">
            <td>
                <select id="cmbIdEspecialidadCEX" style="width:90%"
                  onfocus="cargarComboGRALCondicion2('', '', this.id, 220, valorAtributo('cmbIdTipoServicio'), IdSede())"
                  onchange="contenedorBuscarPaciente()">
                    <option value="">[ SELECCIONE ]</option>					
                </select>
            </td>
            <td>
                <select id="cmbIdProfesionalCEX" style="width:90%"
                  onfocus="cargarComboGRALCondicion2('', '', this.id, 197, valorAtributo('cmbIdEspecialidadCEX'), IdSede());"
                  onchange="contenedorBuscarPaciente()">
                    <option value="">[ SELECCIONE ]</option>					
                </select>
            </td>
        </tr>
    </table>  

    <table width="20%" id="idElementosVIH" hidden>
        <tr class="titulosListaEspera">
            <td width="50%">ESTADO AUDITORIA</td>
        </tr>
        <tr class="estiloImputListaEspera">
            <td>
                <select id="cmbIdEstadoAuditoria" style="width:90%" onchange="buscarHistoria('ordenesAVH')">
                    <option value="NO">SIN AUDITAR</option>		
                    <option value="SI">AUDITADO</option>				
                </select>
            </td>
        </tr>
    </table>      

    <table width="100%">
        <tr class="titulos" align="center"> 
            <td colspan ="7"> 
                <div id="divParaContenedorBuscarPaciente" ></div>      
            </td>
        </tr>    
    </table>
</div>   

<!--********************** fin de la tabla con los datos del formulario ***********************************************-->