<table align="center" border=0 cellspacing=1 cellpadding=1>
	<table align="center">
		<tr class="estiloImputIzq2">
			<td style="border: 2px solid #e8e8e8;">
				NOMBRE PRODUCTO
			</td>
			<td style="border: 2px solid #e8e8e8;">
				<strong><label id="lblNombreArticulo"></label></strong>
			</td>
		</tr>

		<tr class="estiloImputIzq2">
			<td style="border: 2px solid #e8e8e8;">
				DOSIS ADMINISTRADAS
			</td>
			<td style="border: 2px solid #e8e8e8;">
				<strong><label id="lblCantidadDosis"></label>/<label id="lblTotal"></label></strong>
			</td>
		</tr>

		<tr class="estiloImputIzq2">
			<td style="border: 2px solid #e8e8e8;">CANTIDAD RESTANTE</td>
			<td style="border: 2px solid #e8e8e8;"><input class="estImputAdminLargo" type="text" id="txtCantidadRestante"
					style="width:100%" />
			</td>
		</tr>
	</table>
	<table align="center">
		<tr>
			<div>
				<td>
					<table id="listHojaGastosDespachos" width="50%" class="scroll">
					</table>
				</td>
			</div>

			<div>
				<table align="center" border=0 cellspacing=1 cellpadding=1>
					<tr>
						<td class="estiloImput">
							<input title="Adicionar Cantidad" type="image" alt="Adicionar Cantidad"
								height="24px" src="/clinica/utilidades/imagenes/anadir.png" class="small button blue"
								onclick="modificarCRUDDespachoPedidos('enviarProductosHojaConsumo')" />
							<input title="Ultima Cantidad" type="image" alt="Ultima Cantidad" height="24px"
								src="/clinica/utilidades/imagenes/okay.png" class="small button blue"
								onclick="modificarCRUDDespachoPedidos('anadirUltimoConsumo')" />
							<input title="Regresar Cantidad A Solicitudes" type="image" alt="Regresar Cantidad"
							 height="24px" src="/clinica/utilidades/imagenes/retornar.png"
								class="small button blue" onclick="modificarCRUDDespachoPedidos('devolverABodega')" />
						</td>
					</tr>
				</table>
			</div>
		</tr>
	</table>
</table>

<label id="lblIdArticuloTransaccion" hidden></label>
<label id="lblIdTransaccion" hidden></label>
<label id="lblSwPresentacion" hidden></label>
<label id="lblLoteDespacho" hidden></label>
<label id="lblCantidadDespacho" hidden></label>
<label id="lblDisponibleDevolucion" hidden></label>