<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" />
<!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->
<% 	beanAdmin.setCn(beanSession.getCn()); 
    ArrayList resultaux=new ArrayList();
%>

<table width="100%" cellpadding="0" cellspacing="0" align="center" style="margin-left: 2px; padding-right: 4px;">	
	<tr class="estiloImput overs">
		<td colspan="6">
			<center>Tipo de folio a crear: 		  
				<select id="cmbTipoDocumento" style="width:60%" onFocus="comboCreacionDedocumentos()"
					title="80">
				</select>
				<input type="text" id="txtIdEsdadoDocumento" style="width:5%" disabled="disabled" />&nbsp;		
				<input id="btnCrearFolio" type="button" class="button blue" value="CREAR FOLIO"
					title="TT65" onClick="cargabotonloader(this); verificarNotificacionAntesDeGuardar('crearFolio')" />
			<!-- 	<input id="btnProcedimiento_a" type="button" class="button blue" value="Historicos Nefroproteccion"
				title="SE MUESTRA LA INFORMACION DE LA BASE NOMINAL" onClick="visorHistoricosCronicos()" />	 -->					
			</center>
		</td>
	</tr>
</table>
<br>
<table width="100%">
	<tr class="titulosListaEspera overs">		
		<td width="10%">
			Cita Actual: <label style="color: red;" id="lblIdCita"></label>
		</td>
		<td width="15%">
			Admisi&oacute;n Actual: <label style="color: red;" id="lblIdAdmisionAgen"></label>
		</td>
		<td width="20%">N&uacute;mero de evoluci&oacute;n seleccionada: <label id="lblIdDocumento"></label></td>
		<td width="15%">
			Admisi&oacute;n Folio: <label id="lblIdAdmision"></label>
		</td>
		<td width="20%">
			Cita Folio: <label id="lblIdCitaFolio"></label>
		</td>
		<td width="20%" align="LEFT">Estado de evoluci&oacute;n: <label id="lblNomEstadoDocumento" style="size:1PX"></label>
		  <label class="parpadea text"  style="color:#F00;font-weight:bold" id="lblSemeforoRiesgo"></label>
		</td>			
	</tr>
</table>
<br>
<table width="100%" cellpadding="0" cellspacing="0" align="center" style="padding-left: 1px;padding-right: 1px;">
	<tr class="titulosListaEspera">
		<td style="width: 33%;">
			Especialidad
		</td>
		<td style="width: 33%;">
			Profesional
		</td>
		<td style="width: 34%;"> 
			Tipo Folio
		</td>
	</tr>
	<tr class="estiloImputListaEspera">
		<td>
			<input type="text" id="txtEspecialidadHistoricos" style="width:90%" onblur="buscarHC('listDocumentosHistoricos')" onkeypress="llamarAutocomIdDescripcionConDato('txtEspecialidadHistoricos', 1417); checkKeyHistoricos(event);" >
			<img onclick="limpiaAtributo('txtEspecialidadHistoricos'); buscarHC('listDocumentosHistoricos')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">
		</td>	
		<td>
			<input type="text" id="txtIdProfesionalHistoricos" style="width:90%" onblur="buscarHC('listDocumentosHistoricos')" onkeypress="llamarAutocomIdDescripcionConDato('txtIdProfesionalHistoricos', 1416); checkKeyHistoricos(event);" >    
			<img onclick="limpiaAtributo('txtIdProfesionalHistoricos'); buscarHC('listDocumentosHistoricos')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">            
		</td>	
		<td >
			<select id="cmbTipoFolioFiltro" style="width:90%" 
                    onfocus="cargarComboGRALCondicion1('cmbPadre', '1', 'cmbTipoFolioFiltro', 3225, valorAtributo('lblIdPaciente'));" onchange="buscarHC('listDocumentosHistoricos')">	                                        
                    <option value=''>[ TODOS ]</option>
            </select>
		</td> 
	</tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" align="center" style="padding-left: 1px;padding-right: 1px;">
	<tr class="titulosListaEspera">
		<td style="width: 10%;">Cita</td>
		<td style="width: 10%;"> Admision</td>
		<!-- <td style="width: 15%;">Numero de Admision</td> -->
		<td style="width: 10%;">Fecha Desde</td>
		<td style="width: 15%;">Fecha Hasta</td>
		<td style="width: 15%;">Estado</td>
		<td style="width: 15%;">Numero de evolucion</td>
		<td style="width: 10%;"></td>
	</tr>
	<tr class="estiloImputListaEspera">	
		<td>
			<select id="cmbCitaHistoricos" style="width:90%" onchange="buscarHC('listDocumentosHistoricos')">
				<option value="">[ TODO ]</option>
				<option value="1">Cita Actual</option>
			</select>
		</td>
		<td >
			<select id="cmbAdmisionHistoricos" style="width:90%" 
                    onfocus="cargarComboGRALCondicion1('cmbPadre', '1', 'cmbAdmisionHistoricos', 3224, valorAtributo('lblIdPaciente'));" onchange="buscarHC('listDocumentosHistoricos')">                                       
            </select>	
		</td>
		<!-- <td>
			<input type="number" id="txtIdNumeroAdmision" style="width:85%" onblur="buscarHC('listDocumentosHistoricos')" onkeypress="checkKeyHistoricos(event);" >    
			<img onclick="limpiaAtributo('txtIdNumeroAdmision'); buscarHC('listDocumentosHistoricos')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">            
		</td> -->
		<td>
			<input id="txtFechaDesdeHistoricos" type="date" onchange="buscarHC('listDocumentosHistoricos')" style="width: 90%;">
		</td>
		<td>
			<input id="txtFechaHastaHistoricos" type="date" onchange="buscarHC('listDocumentosHistoricos')" style="width: 90%;">
		</td>
		<td > 
			<select id="cmbEstadoFolioHistoricos" style="width:90%" onchange="buscarHC('listDocumentosHistoricos')">
				<option value="">[ TODO ]</option>
				<option value="0">Borrador</option>
				<option value="1">Firmado</option>
			</select>
		</td>
		<td>
			<input type="number" id="txtIdNumeroEvolucion" style="width:80%" onblur="buscarHC('listDocumentosHistoricos')" onkeypress="checkKeyHistoricos(event);" >    
			<img onclick="limpiaAtributo('txtIdNumeroEvolucion'); buscarHC('listDocumentosHistoricos')" src="/clinica/utilidades/imagenes/acciones/button_cancel.png" width="15px" height="15px" align="middle">            
		</td>	
		<td>
            <input type="button" style ="width:90%" class="small button blue" value="LIMPIAR" onclick="limpiaAtributo('txtEspecialidadHistoricos'); limpiaAtributo('txtIdProfesionalHistoricos');
			limpiaAtributo('cmbTipoFolioFiltro'); limpiaAtributo('cmbCitaHistoricos'); limpiaAtributo('cmbAdmisionHistoricos'); limpiaAtributo('txtIdNumeroEvolucion');
			limpiaAtributo('txtIdNumeroAdmision'); limpiaAtributo('txtFechaDesdeHistoricos'); limpiaAtributo('txtFechaHastaHistoricos'); 
			limpiaAtributo('cmbEstadoFolioHistoricos'); buscarHC('listDocumentosHistoricos') "/>                                     
        </td> 
	</tr>
	
	<!--tr class="estiloImput overs">
		<td width="100%" >
		  <input id="btnProcedimiento" type="button" class="small button blue" value="ATENDIDO" title="ATENDIDO DOCUMENTO BTN9765" onClick="modificarCRUD('pacienteAtendido','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')"/> -->
		  <!--input id="btnProcedimiento_a" type="button" class="small button blue" value="CREAR FOLIO"
			title="TT65" onClick="verificarNotificacionAntesDeGuardar('crearFolio')" />
		  <input id="btnProcedimientoR" type="button" class="small button blue" value="LISTAR FOLIOS"
			title="VERCONTENIDO DEL FOLIO" onClick="cargarRefrescaDocClinico()" />
		  <input id="btnProcedimiento_a" type="button" class="small button blue" value="ESPERA" title="TT65"
			onClick="modificarCRUD('pacienteDilatando','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')" />
		  <input id="btnProcedimiento_a" type="button" class="small button blue" value="PRECONSULTA"
			title="TT65"
			onClick="modificarCRUD('pacientePreconsulta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')" />
		  <input id="btnProcedimiento_a" type="button" class="small button blue" value="CONSULTA"
			title="TT65"
			onClick="modificarCRUD('pacienteConsulta','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp')" />
		  <input id="btnProcedimiento" type="button" class="small button blue" value="FIN" title="BF65"
			onClick="modificarCRUD('pacienteAtendido')" />
		  <input id="btnAudit" type="button" class="small button blue" value="CORRECCION" title="BF35"
			onClick="modificarCRUD('folioCorregidoAuditoria')" />
		  <input type="hidden" id="txtIdProfesionalElaboro" /-->
		  <!-- <img src="sss" onclick="modificarCRUD('reiniciarVariables')" width="15" height="15"> >

		</td>
	</tr-->   

	<tr class="estiloImput">
		
	</tr>
	

	<tr>
		<td colspan="7" style="padding-left: 0px;">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center" >
				<tr class="titulos">
					<td style="width:99%">
						<div style="width: 100%; height: 150px; overflow-y: scroll;">							
								<table id="listDocumentosHistoricos" class="scroll" cellpadding="0" cellspacing="0" align="center">								
									<tr>
										<th>

										</th>
									</tr>
								</table>																									
						</div>													
					</td>
				</tr>
			</table>
		</td>
	</tr>

</table>

<input id="txtHistoricos" hidden value="HC"/>
