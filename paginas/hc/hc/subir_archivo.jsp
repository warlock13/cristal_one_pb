<%@ page contentType="application/json"%>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.*" %>
<%@ page import="java.io.*" %>
 
<%
    try {
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory); 

        String ruta_archivo = "";
        String nombre_archivo = "";
        
        List items = upload.parseRequest(request);

        // Captura de variables
        for (Object item : items) {
            FileItem uploaded = (FileItem) item;
            String key = uploaded.getFieldName();
            switch(key)
            {
                case "ruta_archivo":
                    ruta_archivo = uploaded.getString();
                    break; 
                
                case "nombre_archivo":
                    nombre_archivo = uploaded.getString();
                    break; 
            }
        }

        // Escritura de archivo
        for (Object item : items) {
            FileItem uploaded = (FileItem) item;
            if (!uploaded.isFormField()) {
                File fichero = new File(ruta_archivo, nombre_archivo);
                if(fichero.exists()){
                    fichero.delete();
                }
                uploaded.write(fichero);
                %> {"respuesta":<%= fichero.exists() %>} <%
            } 
        }
    }
    catch(Exception e) {
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        %> {"respuesta":<%= false %>} <%
    }
%>