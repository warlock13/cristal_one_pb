function crearGrillaOrdenCompra(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
    switch (arg) {
        case 'listGrillaOrdenesCompra':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2242&parametros=";
            add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc')); 
            add_valores_a_mandar(valorAtributo('txtFechaHastaDoc')); 
            add_valores_a_mandar(valorAtributo('cmbIdEstado')); 

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'ID',
                    'ID DOCUMENTO',
                    'OBSERVACION',
                    'ID TERCERO',
                    'FECHA CREA',
                    'FECHA DOCUMENTO',
                    'FECHA CIERRE',
                    'USUARIO CREA',
                    'USUARIO CIERRA',
                    'ESTADO',
                    'ESTADO EN INVENTARIO',
                    'NOMBRE TERCERO',
                    'CLASE DE ORDEN',
                    'LUGAR DE ENTREGA',
                    'TIEMPO DE ENTREGA', 
                    'FORMA DE PAGO'
                ],
                colModel: [{
                        name: 'ID',
                        index: 'ID',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'ID_DOCUMENTO',
                        index: 'ID_DOCUMENTO',
                        width: anchoP(ancho, 5),
                        
                    },
                    {
                        name: 'OBSERVACION',
                        index: 'OBSERVACION',
                        width: anchoP(ancho, 5),
                        hidden:true

                    },
                    {
                        name: 'ID_TERCERO',
                        index: 'ID_TERCERO',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'FECHA_CREA',
                        index: 'FECHA_CREA',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'FECHA_DOCUMENTO',
                        index: 'FECHA_DOCUMENTO',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'FECHA_FACTURADO',
                        index: 'FECHA_FACTURADO',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'USUARIO_CREA',
                        index: 'USUARIO_CREA',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'USUARIO_FACTURA',
                        index: 'USUARIO_FACTURA',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'ESTADO',
                        index: 'ESTADO',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'ESTADO_INVENTARIO',
                        index: 'ESTADO_INVENTARIO',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'NOMBRE_TERCERO',
                        index: 'NOMBRE_TERCERO',
                        width: anchoP(ancho, 5),
                        hidden: false

                    },
                    {
                        name: 'ID_CLASEO',
                        index: 'ID_CLASEO',
                        width: anchoP(ancho, 5),
                        hidden: false

                    }
                    ,
                    {
                        name: 'LUGAR_ENTREGA',
                        index: 'LUGAR_ENTREGA',
                        width: anchoP(ancho, 5),
                        hidden: true

                    }
                    ,
                    {
                        name: 'TIEMPO_ENTREGA',
                        index: 'TIEMPO_ENTREGA',
                        width: anchoP(ancho, 5),
                        hidden: true

                    }
                    ,
                    {
                        name: 'FORMA_PAGO',
                        index: 'FORMA_PAGO',
                        width: anchoP(ancho, 5),
                        hidden: true

                    }
                ],
                height: 175,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#'+arg).getRowData(rowid);
                    estad = 0;
                    if(datosRow.ESTADO === '0'){
                        var estado = 'BORRADOR'
                    }else{
                        var estado = 'CERRADO'
                    }

                    asignaAtributo('txtLugarEntrega', datosRow.LUGAR_ENTREGA, 0);
                    asignaAtributo('txtTimepoEntrega', datosRow.TIEMPO_ENTREGA, 0);
                    asignaAtributo('txtFormaPago', datosRow.FORMA_PAGO, 0);

                    asignaAtributo('lblIdDoc', datosRow.ID_DOCUMENTO, 1);
                    asignaAtributo('lblIdEstado', estado, 1);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    document.getElementById('txtIdClase').value = datosRow.ID_CLASEO;
                    OrdenCompraAsincrono('crearGrillaOrdenCompra','listArticulosOrdenCompra');
                    OrdenCompraAsincrono('limpiarOrdenCompra','limpiarArticuloOrdenCompra');
                }

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;

            case 'listArticulosOrdenCompra':
                ancho = 1100;
                alto = 80;
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=2266&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
    
                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',
    
                    colNames: [
                        'ID',
                        'ID ARTICULOOC',
                        'ID ARTICULO',
                        'NOMBRE COMERCIAL',
                        'ID ORDEN COMPRA',
                        'CANTIDAD',
                        'VALOR UNITARIO',
                        'DESCUENTO',
                        'IVA', 
                        'SUBTOTAL',
                        'VALOR TOTAL',
                        'DESCRIPCION'
                        
                    ],
                    colModel: [{
                            name: 'ID',
                            index: 'ID',
                            width: anchoP(ancho, 5),
    
                        },
                        {
                            name:  'ID_ARTICULOOC',
                            index: 'ID_ARTICULOOC',
                            width: anchoP(ancho, 5),
                            hidden: true
    
                        },
                        {
                            name: 'ID_ARTICULO',
                            index: 'ID_ARTICULO',
                            width: anchoP(ancho, 5),
    
                        },
                        {
                            name: 'NOMBRE_COMERCIAL',
                            index: 'NOMBRE_COMERCIAL',
                            width: anchoP(ancho, 5),
    
                        },
                        {
                            name:  'ID_ORDEN_COMPRA',
                            index: 'ID_ORDEN_COMPRA',
                            width: anchoP(ancho, 5),
                            hidden: true
    
                        },
                        {
                            name: 'CANTIDAD',
                            index: 'CANTIDAD',
                            width: anchoP(ancho, 5),
    
                        },
                        {
                            name: 'VALOR_UNITARIO',
                            index: 'VALOR_UNITARIO',
                            width: anchoP(ancho, 5),
    
                        },
                        {
                            name: 'DESCUENTO',
                            index: 'DESCUENTO',
                            width: anchoP(ancho, 5),
                            hidden: true
    
                        },
                        {
                            name: 'IVA',
                            index: 'IVA',
                            width: anchoP(ancho, 5),
                            hidden: true
    
                        },
                        //=========================
                        {
                            name: 'SUBTOTAL',
                            index: 'SUBTOTAL',
                            width: anchoP(ancho, 5),
                            hidden: true
    
                        },
                        {
                            name: 'VALOR_PRODUCTO',
                            index: 'VALOR_PRODUCTO',
                            width: anchoP(ancho, 5),
    
                        },
                        {
                            name: 'DESCRIPCION',
                            index: 'DESCRIPCION',
                            width: anchoP(ancho, 5)
    
                        },
                    ],
                    height: 160,
                    width: ancho + 40,
                    onSelectRow: function (rowid) {
                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#'+arg).getRowData(rowid);
                        estad = 0;
                        asignaAtributo('lblIdTransaccion', datosRow.ID_ARTICULOOC, 1);
                        asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO +'-'+ datosRow.NOMBRE_COMERCIAL , 1);
                        asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);
                        asignaAtributo('txtValorU', datosRow.VALOR_UNITARIO, 0);
                        asignaAtributo('lblSubtotal', datosRow.SUBTOTAL, 0);
                        (datosRow.DESCUENTO === '0' || datosRow.DESCUENTO === '' || datosRow.DESCUENTO == null) ?asignaAtributo('lblDescuento', '0', 0) : asignaAtributo('lblDescuento', datosRow.DESCUENTO, 0); 
                        (datosRow.DESCUENTO === '0' || datosRow.DESCUENTO === '' || datosRow.DESCUENTO == null) ?asignaAtributo('txtDescuento', '0', 0) : asignaAtributo('txtDescuento', datosRow.DESCUENTO, 0); 
                        asignaAtributo('lblImpuesto', datosRow.IVA + '%', 0);
                        document.getElementById('txtIdArticulo').disabled = false;
                        asignaAtributo('lblTotal', datosRow.VALOR_PRODUCTO, 0);
                        asignaAtributo('txtObsArt', datosRow.DESCRIPCION, 0);
                        asignaAtributo('cmbIVA',datosRow.IVA,0);
                    }
    
                });
                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                    url: valores_a_mandar
                }).trigger('reloadGrid');
                break;
    }
}