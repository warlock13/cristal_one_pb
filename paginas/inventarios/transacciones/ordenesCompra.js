function GuardarIdArticulo() {
    var identificadores = [];
}


async function OrdenCompraAsincrono(proceso, arg) {
    const result = await cargarProcesoOrdenCompra(proceso, arg);
    console.info(result)
}

function cargarProcesoOrdenCompra(proceso, arg) {
    return new Promise((resolve, reject) => {
        if (proceso === 'crearGrillaOrdenCompra') {
            crearGrillaOrdenCompra(arg);
            resolve(proceso + ' ' + arg + ' Exito');
            reject(proceso + ' ' + arg + ' Fallo')
        }
        if (proceso === 'limpiarOrdenCompra') {
            limpiarOrdenCompra(arg);
            resolve(proceso + ' ' + arg + ' Exito');
            reject(proceso + ' ' + arg + ' Fallo');
        }

    });
}

function valorTotalArticulo() {
    if (valorAtributo('txtDescuento') === '') {
        asignaAtributo('txtDescuento', '0', 0);
    }
    subtotal = parseInt(valorAtributo('txtCantidad')) * parseFloat(valorAtributo('txtValorU'));
    subtotal_descuento = parseFloat(subtotal) - (parseFloat(valorAtributo('txtDescuento')) * parseInt(valorAtributo('txtCantidad')));
    total = (subtotal_descuento * parseInt(document.getElementById('cmbIVA').value) / 100) + subtotal_descuento;
    asignaAtributo('lblSubtotal', subtotal_descuento, 0);
    asignaAtributo('lblDescuento', valorAtributo('txtDescuento'), 0);
    document.getElementById('lblTotal').textContent = total;
    document.getElementById('lblImpuesto').textContent = document.getElementById('cmbIVA').value;
}

function activarArticulo(arg) {
    valorTotalArticulo();
    if (arg === '') {
        asignaAtributo('txtIdArticulo', 'SELECCIONE LA CLASE DE LA ORDEN', 1);
        document.getElementById('txtIdArticulo').disabled = true;
        document.getElementById('idArticulo').style.display = 'none';

    }
    if (arg === 'OC') {
        document.getElementById('txtIdArticulo').disabled = false;
        document.getElementById('idArticulo').style.display = '';
        asignaAtributo('txtIdArticulo', '', 0);
    }
    if (arg === 'OSI') {
        document.getElementById('txtIdArticulo').disabled = false;
        document.getElementById('idArticulo').style.display = 'none';
        asignaAtributo('txtIdArticulo', '39679-SERVICIO CON IVA', 1);
    }
    if (arg === 'OSSI') {
        document.getElementById('txtIdArticulo').disabled = false;
        document.getElementById('idArticulo').style.display = 'none';
        asignaAtributo('txtIdArticulo', '39680-SERVICIO SIN IVA', 1);
    }

}

function modificarCRUDOrdenCompra(arg) {
    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {

        case 'imprimirOrdenDeCompra':
            var dimension = 'width=1150,height=752,scrollbars=NO,statusbar=NO,left=150,top=90';
            var idDoc = document.getElementById('lblIdDoc').innerHTML;
            if (idDoc != '') {
                var idReporte = "DOC_FARMACIA";
                var nuevaURL = "ireports/inventario/generaInventario.jsp?reporte=" + idReporte + "&idDoc=" + idDoc;
                window.open(nuevaURL, '', dimension);
            } else { alert('Seleccione un producto o servicio el cual tenga el encabezado FACTURADO (F)'); }
            break;

        case 'cerrarOrdenDeCompra':
            if (valorAtributo('lblIdDoc') === '') {
                alert('NO HA SELECCIONADO UNA ORDEN DE COMPRA O DE SERVICIO');
                return
            }
            if (valorAtributo('lblIdEstado') === 'CERRADO') {
                alert('LA ORDEN DE COMPRA O SERVICIO HA SIDO CERRADA PREVIAMENTE!!!');
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2262&parametros=";
            //now()
            add_valores_a_mandar(IdSesion());
            add_valores_a_mandar('1');
            add_valores_a_mandar(valorAtributo("lblIdDoc"));
            ajaxModificar();
            break;

        case 'crearOrdenCompra':
            if (verificarCamposOrdenCompra(arg)) {
                if (valorAtributo("txtIdClase") == '') {
                    alert('SELECCIONE LA CLASE DE ORDEN');
                    return false;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2241&parametros=";
                add_valores_a_mandar(valorAtributo("txtObservacion"));
                add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdTercero"));
                add_valores_a_mandar(valorAtributo("txtFechaDocumento"));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(document.getElementById('txtIdClase').value);
                add_valores_a_mandar(valorAtributo('txtLugarEntrega'));
                add_valores_a_mandar(valorAtributo('txtTimepoEntrega'));
                add_valores_a_mandar(valorAtributo('txtFormaPago'));
                ajaxModificar();
            }
            break;

        case 'modificarOrdenCompra':
            if (valorAtributo("lblIdDoc") == '') {
                alert('SELECCCIONE UNA ORDEN DE COMPRA O DE SERVICIO');
                return;
            }
            if(valorAtributo('lblIdEstado') === 'CERRADO'){
                alert('NO SE PUEDE ACTUALIZAR, LA ORDEN DE COMPRA/SERVICIO YA HA SIDO CERRADA'); 
                return;
            }
            if (verificarCamposOrdenCompra('crearOrdenCompra')) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2245&parametros=";
                add_valores_a_mandar(valorAtributo("txtObservacion"));
                add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdTercero"));
                add_valores_a_mandar(valorAtributo("txtFechaDocumento"));
                add_valores_a_mandar(document.getElementById('txtIdClase').value);
                add_valores_a_mandar(valorAtributo('txtLugarEntrega'));
                add_valores_a_mandar(valorAtributo('txtTimepoEntrega'));
                add_valores_a_mandar(valorAtributo('txtFormaPago'));
                add_valores_a_mandar(valorAtributo("lblIdDoc"));
                ajaxModificar();
            }
            break;

        case 'adicionarArticuloOrdenCompra':
            let text = $('td[aria-describedby="listArticulosOrdenCompra_ID_ARTICULO"]');
            for (var i = 0; i < text.length; i++) {
                if (text[i].title.trim() === valorAtributoIdAutoCompletar("txtIdArticulo").trim()) {
                    alert('ESTE PRODUCTO O SERVICIO YA HA SIDO AGREGADO ANTERIORMENTE');
                    return
                }
            }
            if (valorAtributo('lblIdEstado') === 'CERRADO') {
                alert("NO PUEDE MODIFICAR EL PRODUCTO O SERVICIO, LA ORDEN DE COMPRA O SERVICIO SE ECUENTRA EN ESTADO CERRADO");
                return false;
            }
            if (verificarCamposOrdenCompra(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2243&parametros=";
                //default
                add_valores_a_mandar(valorAtributo("lblIdDoc"));
                add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdArticulo"));
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("lblTotal"));
                add_valores_a_mandar(valorAtributo("lblSubtotal"));
                add_valores_a_mandar(valorAtributo("lblDescuento"));
                add_valores_a_mandar(valorAtributo("cmbIVA"));
                add_valores_a_mandar(valorAtributo("txtObsArt"));
                add_valores_a_mandar(valorAtributo("txtValorU"));
                ajaxModificar();
            }
            break;

        case 'modificarArticuloOrdenCompra':
            if (valorAtributo('lblIdTransaccion') === '') {
                alert('SELECCIONE UN PRODUCTO O SERVICIO');
                return
            }
            if (verificarCamposOrdenCompra(arg)) {
                if (valorAtributo('lblIdEstado') === 'CERRADO') {
                    alert("NO PUEDE MODIFICAR EL PRODUCTO O SERVICIO, LA ORDEN DE COMPRA O SERVICIO SE ECUENTRA EN ESTADO CERRADO");
                    return false;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2249&parametros=";
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(valorAtributo("lblTotal"));
                add_valores_a_mandar(valorAtributo("lblSubtotal"));
                add_valores_a_mandar(valorAtributo("lblDescuento"));
                add_valores_a_mandar(valorAtributo("lblImpuesto").replace('%', ''));
                add_valores_a_mandar(valorAtributo("txtObsArt"));
                add_valores_a_mandar(valorAtributo("txtValorU"));
                add_valores_a_mandar(valorAtributo("lblIdTransaccion"));

                ajaxModificar();
            }
            break;

        case 'eliminarArticuloOrdenCompra':
            if (valorAtributo('lblIdEstado') === 'CERRADO') {
                alert("NO PUEDE MODIFICAR EL PRODUCTO O SERVICIO, LA ORDEN DE COMPRA O SERVICIO SE ECUENTRA EN ESTADO CERRADO");
                return false;
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2251&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdTransaccion"));
            ajaxModificar();
            break;
    }
}

function respuestaModificarCRUDOrdenCompra(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'crearOrdenCompra':
                alert('Orden de compra creada Exitosamente');
                limpiarOrdenCompra('limpiarOrdenCompra');
                crearGrillaOrdenCompra('listGrillaOrdenesCompra');
                break;
            case 'adicionarArticuloOrdenCompra':
                alert('Articulo adicionado correctamente a la orden de compra');
                limpiarOrdenCompra('limpiarArticuloOrdenCompra');
                crearGrillaOrdenCompra('listArticulosOrdenCompra');
                break;

            case 'modificarOrdenCompra':
                alert('Orden de compra modificada Exitosamente');
                setTimeout(() => {
                    crearGrillaOrdenCompra('listGrillaOrdenesCompra');
                }, 100);
                setTimeout(() => {
                    limpiarOrdenCompra('limpiarOrdenCompra');
                }, 200);
                break;

            case 'modificarArticuloOrdenCompra':
                alert('Articulo o servicio actualizado exitosamente');
                crearGrillaOrdenCompra('listArticulosOrdenCompra');
                break;

            case 'eliminarArticuloOrdenCompra':
                alert('Articulo o servicio eliminado exitosamente');
                limpiarOrdenCompra('limpiarArticuloOrdenCompra');
                crearGrillaOrdenCompra('listArticulosOrdenCompra');
                break;

            case 'cerrarOrdenDeCompra':
                alert('Orden de compra cerrada Exitosamente !!!')
                setTimeout(() => {
                    limpiarOrdenCompra('limpiarOrdenCompra');
                }, 100);
                setTimeout(() => {
                    crearGrillaOrdenCompra('listGrillaOrdenesCompra');
                }, 200);
                break;
        }
    }
}

function verificarCamposOrdenCompra(arg) {
    switch (arg) {
        case 'crearOrdenCompra':
            if (valorAtributoIdAutoCompletar("txtIdTercero") == '') {
                alert('SELECCIONE UN TERCERO');
                return false;
            }
            if (valorAtributo("txtFechaDocumento") == '') {
                alert('SELECCIONE FECHA DOCUMENTO');
                return false;
            }
            break;
        case 'adicionarArticuloOrdenCompra':
            if (valorAtributo('txtIdArticulo') == '') {
                alert('SELECCIONE UN PRODUCTO O SERVICIO');
                return false;
            };
            if (valorAtributo('txtCantidad') == '') {
                alert('DIGITE LA CANTIDAD DE UNIDADES DEL ARTICULO');
                return false
            };
            if (valorAtributo('txtValorU') == '') {
                alert('DIGITE EL VALOR UNITARIO DEL ARTICULO');
                return false
            };
            break;
    }
    return true;
}

function limpiarOrdenCompra(arg) {
    switch (arg) {
        case 'limpiarOrdenCompra':
            document.getElementById("lblIdDoc").textContent = '';
            document.getElementById("lblIdEstado").textContent = '';
            document.getElementById("txtObservacion").value = '';
            document.getElementById("txtIdTercero").value = '';
            document.getElementById("txtFechaDocumento").value = '';

            document.getElementById("txtLugarEntrega").value = '';
            document.getElementById("txtTimepoEntrega").value = '';
            document.getElementById("txtFormaPago").value = '';
            document.getElementById("txtIdClase").value = '';
            break;

        case 'limpiarArticuloOrdenCompra':
            document.getElementById("txtIdArticulo").value = '';
            document.getElementById("txtIdArticulo").disabled = false;
            document.getElementById("txtCantidad").value = '';
            document.getElementById("txtValorU").value = '';
            document.getElementById("lblIdTransaccion").textContent = '';
            document.getElementById("lblTotal").textContent = '';
            document.getElementById("txtObsArt").value = '';
            document.getElementById("lblSubtotal").textContent = '';
            document.getElementById("lblDescuento").textContent = '';
            document.getElementById("txtDescuento").value = '';
            document.getElementById("lblImpuesto").textContent = '';
            document.getElementById("txtObsArt").value = '';
            break;
    }

}