<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <!-- AQUI COMIENZA EL TITULO -->
      <div align="center" id="tituloForma">
        <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
        <jsp:include page="../../titulo.jsp" flush="true">
          <jsp:param name="titulo" value="Ordenes de Compra" />
        </jsp:include>
      </div>
      <!-- AQUI TERMINA EL TITULO -->
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
        <tr>
          <td>
        <tr>
          <td>
            <div id="divEditar" style="display:block; width:100%">

              <table width="100%" cellpadding="0" cellspacing="0" align="center">

                <tr>
                  <td>
                    <table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0" class="fondoTabla">
                      <tr>
                        <td class="tdTitulo">
                          <p class="pTitulo">ORDEN DE COMPRA</p>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
               

                <tr>
                  <td>
                    <table width="100%">
                      <tr class="estiloImputIzq2">
                        <td>ID ORDEN DE COMPRA
                        </td>
                        <td>
                          <strong><label id="lblIdDoc"></label></strong>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          ESTADO DE LA ORDEN: &nbsp; <strong><label id="lblIdEstado"></label></strong>
                        </td>
                      </tr>
                      <tr class="estiloImputIzq2">
                        <td>CLASE DE ORDEN</td>
                        <td colspan="2">
                          <select style="height: 20px;" id="txtIdClase">
                            <option value="" onclick=""></option>
                            <option value="OS" onclick="">ORDEN DE SERVICIO</option>
                            </option>
                            <option value="OC" onclick="">ORDEN DE COMPRA</option>
                          </select>
                        </td>
                      </tr>
                      <tr class="estiloImputIzq2">
                        <td>TERCERO</td>
                        <td colspan="2">
                          <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" size="110" maxlength="210" id="txtIdTercero"
                            style="width:92%"  />
                          <img width="18px" height="18px" align="middle" title="VEN32" id="idLupitaVentanitaMuni"
                            onclick="traerVentanita(this.id,'','divParaVentanita','txtIdTercero','36')"
                            src="/clinica/utilidades/imagenes/acciones/buscar.png">
                            <a href="javascript:cargarMenu('facturacion/terceros/principalTerceros.jsp', '4-0-0', 'principalTerceros', 'principalTerceros', 's', 's', 's', 's', 's', 's');">
                              <img width="18px" height="18px" align="middle" title="VEN32" src="/clinica/utilidades/imagenes/acciones/anadir.png">
                            </a>
                          <div id="divParaVentanita" style="top:1500PX"></div>
                        </td>
                      </tr>

                      <tr class="estiloImputIzq2">
                        <td>OBSERVACION&nbsp;&nbsp;
                        </td>
                        <td>
                          <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtObservacion"
                            style="width:92%; height: 60px;"  />
                        </td>
                      </tr>

                      <tr class="estiloImputIzq2">
                        <td>LUGAR DE ENTREGA</td>
                        <td colspan="2">
                          <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" size="110" maxlength="210" id="txtLugarEntrega"
                            style="width:92%"  />
                          <div id="divParaVentanita" style="top:1500PX"></div>
                        </td>
                      </tr>
                      <tr class="estiloImputIzq2">
                        <td>TIEMPO DE ENTREGA</td>
                        <td colspan="2">
                          <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" size="110" maxlength="210" id="txtTimepoEntrega"
                            style="width:92%"  />
                          <div id="divParaVentanita" style="top:1500PX"></div>
                        </td>
                      </tr>
                      <tr class="estiloImputIzq2">
                        <td>FORMA DE PAGO</td>
                        <td colspan="2">
                          <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" size="110" maxlength="210" id="txtFormaPago"
                            style="width:92%"  />
                          <div id="divParaVentanita" style="top:1500PX"></div>
                        </td>
                      </tr>
                </tr>
                <tr class="estiloImputIzq2">
                  <td>FECHA DOCUMENTO</td>
                  <td >  
                    <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtFechaDocumento" style="width:10%"  />
                  </td>
                </tr>

                <tr class="estiloImputIzq2">
                  <td>BUSQUEDA&nbsp;&nbsp;
                  <td>
                    <table width="100%">
                      <tr class="estiloImputIzq2">
                        <td width="40%">FECHA DESDE:
                          <input oninput="handleInput(event)" class="estImputAdminLargo" id="txtFechaDesdeDoc" type="text" 
                            maxlength="10" size="10">
                        </td>
                        <td width="40%">FECHA HASTA:
                          <input oninput="handleInput(event)" class="estImputAdminLargo" id="txtFechaHastaDoc" type="text" 
                            maxlength="10" size="10">
                        </td>
                        <td width="20%">
                          ESTADO:
                          <select style="height: 20px;" class="estImputAdminLargo" size="1" id="cmbIdEstado" style="width:50%" >
                            <option value=""></option>
                            <%     resultaux.clear();
                                              resultaux=(ArrayList)beanAdmin.combo.cargar(135);	
                                              ComboVO cmbEst; 
                                              for(int k=0;k<resultaux.size();k++){ 
                                                    cmbEst=(ComboVO)resultaux.get(k);
                                       %>
                            <option value="<%= cmbEst.getId()%>" title="<%= cmbEst.getTitle()%>">
                              <%=cmbEst.getDescripcion()%></option>
                            <%} %>
                          </select>
                        </td>
                      </tr>
                    </table>
                  </td>

                </tr>

                <tr>
                  <td class="br-f-gris">

                  </td>
                </tr>

                <tr>
                  <td colspan="4" align="center">
                    <input oninput="handleInput(event)" id="btn_limpia" title="btn_lp78e" class="small button blue" type="button" value="LIMPIAR"
                      onclick="limpiarOrdenCompra('limpiarOrdenCompra')">
                    <!--                 <input oninput="handleInput(event)" class="estImputAdminLargo" id="btn_crea"   title="btn_B70"  class="small button blue" type="button" value="Crear"  onclick="modificarCRUD('crearDocumento','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" > -->
                    <input oninput="handleInput(event)" id="btnProcedimiento_a" title="BG67" type="button" class="small button blue" value="NUEVO"
                      onclick="modificarCRUDOrdenCompra('crearOrdenCompra')" />
                    <input oninput="handleInput(event)" name="btn_modifica" title="btn_b75" type="button" class="small button blue" value="MODIFICAR"
                      onclick="modificarCRUDOrdenCompra('modificarOrdenCompra');" />
                    <input oninput="handleInput(event)" name="btn_cierra" title="BTRR5" type="button" class="small button blue" value="CERRAR"
                      onclick="confirm('DESPUES DE CERRAR EL DOCUMENTO SERA IMPOSIBLE MODIFICARLO, ESTA SEGURO ?') ? modificarCRUDOrdenCompra('cerrarOrdenDeCompra') : ''" />
                    <input oninput="handleInput(event)" name="btn_cierra" title="BTRim" type="button" class="small button blue" value="IMPRIMIR"
                      onclick="modificarCRUDOrdenCompra('imprimirOrdenDeCompra');" />
                    <input oninput="handleInput(event)" name="btn_cierra" title="BTTY7" type="button" class="small button blue" value="BUSCAR"
                      onclick="crearGrillaOrdenCompra('listGrillaOrdenesCompra')" />
                  </td>
                </tr>
                <tr>
                  <td class="br-f-gris">

                  </td>
                </tr>
              </table>
              <div id="divContenido" style="height: 200px; width: 100%">
                <table id="listGrillaOrdenesCompra" class="scroll"></table>
              </div>
          </td>
        </tr>
      </table>

      <table width="100%" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td>
            <table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0" class="fondoTabla">
              <tr>
                <td class="tdTitulo">
                  <p class="pTitulo">TRANSACCIONES ORDEN DE COMPRA</p>
                </td>
              </tr>
            </table>
          </td>
        </tr>

       


        <tr>
          <td>
            <table width="100%">
              <tr class="estiloImputIzq2">
                <td width="20%">ID PRODUCTO ORDEN DE COMPRA:&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td width="80%">&nbsp;&nbsp;&nbsp;&nbsp;<label id="lblIdTransaccion"></label></label></td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>PRODUCTO</td>
                <td colspan="2">
                  <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" size="110" maxlength="210" id="txtIdArticulo"
                    style="width:92%"  />
                  <img  width="18px" height="18px" align="middle" title="VEN32" id="idArticulo"
                    onclick="traerVentanita(this.id,'','divParaVentanita','txtIdArticulo','37')"
                    src="/clinica/utilidades/imagenes/acciones/buscar.png">
                  <div id="divParaVentanita" style="top:1500PX"></div>
                </td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>OBSERVACION</td>
                <td colspan="2">
                  <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" size="110" maxlength="210" id="txtObsArt"
                    style="width:92%"  />
                  <div id="divParaVentanita" style="top:1500PX"></div>
                </td>
              </tr>
              <!-- <tr class="estiloImputIzq2">
                <td>LOTE</td>
                <td>
                  <TABLE width="100%">
                    <TR>
                      <TD width="25%">
                        <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtLote" style="width:112px"  />
                      </TD>
                      <TD width="25%">
                        PRODUCTO SIN LOTE<input oninput="handleInput(event)" class="estImputAdminLargo" id="chkLote" onchange="cambiarLote()"
                          type="checkbox" />
                      </TD>
                      <TD width="25%">
                        <div class="optica" style="display:none;"> TIPO-MEDIDA:</div>
                      </TD>
                      <TD width="25%" aling="left">
                        <div class="optica" style="display:none;">
                          <select style="height: 20px;" class="estImputAdminLargo" id="cmbMedida">
                            <option value="">[ Seleccionar ] </option>
                            <option value="TR90"> TR90 </option>
                            <option value="ACETATO"> ACETATO </option>
                            <option value="METALICO"> METALICO </option>
                            <option value="PLASTICO"> PLASTICO </option>
                            <option value="TITANIO"> TITANIO </option>
                          </select>
                        </div>
                      </TD>
                    </TR>
                  </TABLE>
                </td>
              </tr> -->
              <!-- <tr class="estiloImputIzq2">
                <td>FECHA DE VENCIMIENTO</td>
                <td>
                  <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtFechaVencimiento" style="width:112px"
                     />
                </td>
              </tr> -->
              <tr class="estiloImputIzq2">
                <td>CANTIDAD</td>
                <td><input oninput="handleInput(event)" class="estImputAdminLargo" type="number" id="txtCantidad" style="width:12%" 
                    onkeyup="valorTotalArticulo()" />
                </td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>VALOR UNITARIO</td>
                <td><input oninput="handleInput(event)" class="estImputAdminLargo" type="number" id="txtValorU" style="width:12%" 
                    onkeyup="valorTotalArticulo()" /></td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>DESCUENTO x PRODUCTO</td>
                <td><input class="estImputAdminLargo" type="number" id="txtDescuento" style="width:12%" 
                    onblur="valorTotalArticulo()" value="0"/></td>
              </tr>
              <tr class="estiloImputIzq2">
                <td>IMPUESTO</td>
                <td>
                  <select style="height: 20px;" name="cmbIVA" id="cmbIVA">
                    <option value="0" onclick="valorTotalArticulo()">0%</option>
                    <option value="19" onclick="valorTotalArticulo()">19%</option>
                  </select>
                </td>
              </tr>
          </td>
        </tr>
        <tr class="estiloImputIzq2">
          <td><strong>SUBTOTAL</strong></td>
          <td><label id="lblSubtotal">0</label></td>
        </tr>
        <tr class="estiloImputIzq2">
          <td><strong>DESCUENTO</strong></td>
          <td><label id="lblDescuento">0</label></td>
        </tr>
        <tr class="estiloImputIzq2">
          <td><strong>IMPUESTO</strong></td>
          <td width="30%"><label id="lblImpuesto">0</label></td>
        </tr>
        <tr class="estiloImputIzq2">
          <td><strong>TOTAL PRODUCTO</strong></td>
          <td><label id="lblTotal">0</label></td>
        </tr>
        <!-- <tr class="estiloImputIzq2">
          <td>PRECIO DE VENTA</td>
          <td><input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtPrecioVenta"
              onKeyPress="javascript:return soloNumeros(event)" style="width:12%"  /></td>
        </tr> -->
        <!-- <tr class="estiloImputIzq2">
          <td>SERIAL
          </td>
          <td><input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtSerial" style="width:12%" 
              onchange="registrarSerial();" onpaste="this.onchange();" />
          </td>
        </tr> -->
        <tr>
          <td class="br-f-gris">

          </td>
        </tr>
        <tr>
          <td colspan="4" align="center">
            <!-- <% if(beanSession.usuario.preguntarMenu("DES") ){%>
              <input oninput="handleInput(event)" type="button" class="small button blue"
                onClick="mostrar('divVentanitaDescuento'); buscarSuministrosBodegas('listDescuentosArticulo'); "
                value="DESCUENTO PRODUCTO" title="BTdescuento">
              <%}%> -->
            <!--<input oninput="handleInput(event)" class="estImputAdminLargo" name="btn_cierra" title="BR79-GENERA CODIGO BARRAS DOCUMENTO" type="button" class="small button blue" value="GENERA CB DOCUMENTO" onclick="impresionBarCode();"   /> -->
            <!--<input oninput="handleInput(event)" class="estImputAdminLargo" name="btn_cierra" title="BR74-GENERA CODIGO BARRAS TRANSACCION" type="button" class="small button blue" value="GENERA CB TRANSACCION" onclick="impresionBarCodeTr();"   />  -->
            <!--<input oninput="handleInput(event)" class="estImputAdminLargo" name="btn_modifica" title="BR75-GENERA CODIGO BARRAS AUTOMATICO" type="button" class="small button blue" value="GENERA CB AUTOMATICO" onclick="modificarCRUD('creaSerialesTransaccion')"  />  -->
            <input oninput="handleInput(event)" id="btn_limpia" title="BR75" type="button" class="small button blue" value="LIMPIAR"
              onclick="limpiarOrdenCompra('limpiarArticuloOrdenCompra')">
            <input oninput="handleInput(event)" id="btn_crea" title="BR76" type="button" class="small button blue" value="CREAR"
              onclick="modificarCRUDOrdenCompra('adicionarArticuloOrdenCompra')">
            <input oninput="handleInput(event)" name="btn_modifica" title="BR77" type="button" class="small button blue" value="MODIFICAR"
              onclick="modificarCRUDOrdenCompra('modificarArticuloOrdenCompra')" />
            <input oninput="handleInput(event)" name="btn_elimina" title="BR78" type="button" class="small button blue" value="ELIMINAR"
              onclick="modificarCRUDOrdenCompra('eliminarArticuloOrdenCompra');" />
          </td>
        </tr>
        <tr>
          <td class="br-f-gris">

          </td>
        </tr>
      </table>
      <div id="divContenido">
        <table id="listArticulosOrdenCompra" class="scroll"></table>
      </div>
    </td>
  </tr>
</table>

<input oninput="handleInput(event)" class="estImputAdminLargo" type="hidden" id="txtIdUsuarioSesion"
  value="<%=beanSession.usuario.getIdentificacion()%>" />


<input oninput="handleInput(event)" class="estImputAdminLargo" type="hidden" id="txtIdLote1" value="" />
<input oninput="handleInput(event)" class="estImputAdminLargo" type="hidden" id="txtIdTipoBodega" value="" />
<input oninput="handleInput(event)" class="estImputAdminLargo" type="hidden" id="txtIdLoteBan" value="NO" />
<input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtRespuestaSerial" value="" style="display: none;" />


<div id="divVentanitaDescuento" style="display:none; z-index:3050; top:700px; left:100px;">
  <div id="idDivTransparencia_1" class="transParencia"
    style="z-index:3051; filter:alpha(opacity=60);float:left;-moz-opacity:.60;opacity:.60">
  </div>
  <div id="idDivTransparencia21" style="z-index:3052; position:absolute; top:400px; left:-5px; width:100%">
    <table id="idTablaVentanitaIntercambio" width="93%" height="100px" class="fondoTabla">
      <tr class="estiloImput">
        <td align="left"><input oninput="handleInput(event)" class="estImputAdminLargo" name="btn_cerrar" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaDescuento')" /></td>
        <td colspan="2">
          <label id="lblIdArtDescuento"></label>-
          <label id="lblArtDescuento"></label> -
          <label id="lblSerialDescuento"></label>
        </td>
        <td align="right"><input oninput="handleInput(event)" name="btn_cerrar" type="button" align="right" value="CERRAR"
            onClick="ocultar('divVentanitaDescuento')" /></td>
      </tr>
      <tr class="estiloImput">
        <td colspan="4" align="center">
          <table id="listDescuentosArticulo" class="scroll"></table>
        </td>
      </tr>
      <tr class="estiloImput">
        <td colspan="4">
          <table width="100%">
            <tr class="titulos">
              <td align="right">Descuento:</td>
              <td>
                <input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtDescuentoArt"
                  onKeyPress="javascript:return soloNumeros(event)" style="width:20%"  />
              </td>
            </tr>
            <tr class="estiloImput">
              <td><input oninput="handleInput(event)" id="idBtnSolicitAutoriza" type="button" class="small button blue" value="Crear"
                  onClick="modificarCRUD('crearDescuento')" /></td>
              <td><input oninput="handleInput(event)" id="idBtnSolicitAutoriza" type="button" class="small button blue" value="Eliminar"
                  onClick="modificarCRUD('eliminarDescuento')" /></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>


<input oninput="handleInput(event)" class="estImputAdminLargo" type="text" id="txtIdDocumento" style="display: none;">