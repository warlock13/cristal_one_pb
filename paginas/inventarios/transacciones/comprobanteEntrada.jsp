<%@ page session="true" contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1100px" align="center" style="background: #e8e8e8" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <!-- AQUI COMIENZA EL TITULO -->
            <div align="center" id="tituloForma">
                <!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
                <jsp:include page="../../titulo.jsp" flush="true">
                    <jsp:param name="titulo" value="Comprobante de Entrega" />
                </jsp:include>
            </div>
            <!-- AQUI TERMINA EL TITULO -->
        </td>
    </tr>
    <tr>
        <td style="border: 2px solid #e8e8e8;">
            <table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla"
                style="border-bottom-style:none;border:0px;">
                <tr>
                    <td style="border: 2px solid #e8e8e8;">
                        <div style="overflow:auto;height:230px; width:100%">
                            <!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
                            <div id="divBuscar" style="display:block">
                                <table width="100%" cellpadding="0" cellspacing="0" align="center">
                                    <tr class="titulos" align="center">
                                        <td style="border: 2px solid #e8e8e8;" width="20%">CODIGO DE ORDEN DE
                                            COMPRA/SERVICIO</td>
                                        <td style="border: 2px solid #e8e8e8;" width="20%">ID PROVEEDOR</td>
                                        <td style="border: 2px solid #e8e8e8;" width="15%">FECHA DESDE</td>
                                        <td style="border: 2px solid #e8e8e8;" width="15%">FECHA HASTA</td>
                                        <td style="border: 2px solid #e8e8e8;" width="10%">&nbsp;</td>
                                    </tr>
                                    <tr class="estiloImput">
                                        <td style="border: 2px solid #e8e8e8;" colspan="1"><input
                                                class="estImputAdminLargo" type="text" id="txtIdOC" style="width:90%"
                                                 /></td>
                                        <td style="border: 2px solid #e8e8e8;" colspan="1"><input
                                                class="estImputAdminLargo" type="text" id="txtIdProveedor"
                                                style="width:90%"  /></td>
                                        <td style="border: 2px solid #e8e8e8;" colspan="1"><input
                                                class="estImputAdminLargo" type="text" id="txtFechaDesdeDoc"
                                                style="width:90%"  /></td>
                                        <td style="border: 2px solid #e8e8e8;" colspan="1"><input
                                                class="estImputAdminLargo" type="text" id="txtFechaHastaDoc"
                                                style="width:90%"  /></td>
                                        <td style="border: 2px solid #e8e8e8;">
                                            <input title="btn_bt754" type="button" class="small button blue"
                                                value="BUSCAR"
                                                onclick="crearGrillaComprobanteEntrada('listOrdenesCompra')" />
                                        </td>
                                    </tr>
                                    <div id="divContenido">
                                        <table id="listOrdenesCompra" class="scroll"></table>
                                    </div>
                                    <tr>
                                        <td style="border: 2px solid #e8e8e8;">
                                            <table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0"
                                                class="fondoTabla">
                                                <tr>
                                                    <td style="border: 2px solid #e8e8e8;" class="tdTitulo">
                                                        <p class="pTitulo">PRODUCTOS O SERVICIOS DE LA ORDEN DE SERVICIO
                                                            O COMPRA</p>
                                                    </td>
                                                </tr>
                                                <div id="divContenido">
                                                    <table id="listArticulosOrdenCompra" class="scroll"></table>
                                                </div>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: 2px solid #e8e8e8;">
                                            <table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0"
                                                class="fondoTabla">
                                                <tr>
                                                    <td style="border: 2px solid #e8e8e8;" class="tdTitulo">
                                                        <p class="pTitulo">CANTIDADES Y DESCRIPCION DE PRODUCTOS
                                                            RECIBIDOS</p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" align="center">
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;" width="20%">BODEGA</td>
                                                    <td style="border: 2px solid #e8e8e8;" width="80%">
                                                        <label hidden id="lblIdEncargado">
                                                        </label>
                                                        <select class="estImputAdminLargo" id="cmbIdBodega"
                                                            style="width:30%" 
                                                            onfocus="asignarFuncionario();comboDependiente('cmbIdBodega','lblIdEncargado','2093');">
                                                        </select>

                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">NUMERO FACTURA</td>
                                                    <td style="border: 2px solid #e8e8e8;"><input
                                                            class="estImputAdminLargo" type="text" id="txtFactura"
                                                            style="width:12%" />
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">FECHA DE FACTURA</td>
                                                    <td style="border: 2px solid #e8e8e8;"><input
                                                            class="estImputAdminLargo" type="text" id="txtFechaFactura"
                                                            style="width:12%" />
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">PRODUCTO</td>
                                                    <td style="border: 2px solid #e8e8e8;" colspan="2">
                                                        <input class="estImputAdminLargo" type="text" size="110"
                                                            maxlength="210" id="txtIdArticulo" style="width:92%"
                                                             />
                                                        <img width="18px" height="18px" align="middle" title="VEN32"
                                                            id="idLupitaVentanitaMuni"
                                                            onclick="traerVentanita(this.id,'','divParaVentanita','txtIdArticulo','37')"
                                                            src="/clinica/utilidades/imagenes/acciones/buscar.png">
                                                        <div id="divParaVentanita" style="top:1500PX"></div>
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">LOTE</td>
                                                    <td style="border: 2px solid #e8e8e8;">
                                                        <TABLE width="100%">
                                                            <TR>
                                                                <td width="25%">
                                                                    <input class="estImputAdminLargo" type="text"
                                                                        id="txtLote" style="width:112px"
                                                                         />
                                                                </TD>
                                                                <td width="25%">
                                                                    ARTICULO SIN LOTE<input class="estImputAdminLargo"
                                                                        id="chkLote" onchange="cambiarLote()"
                                                                        type="checkbox" />
                                                                </TD>
                                                            </TR>
                                                        </TABLE>
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">FECHA DE VENCIMIENTO</td>
                                                    <td style="border: 2px solid #e8e8e8;">
                                                        <input class="estImputAdminLargo" type="text"
                                                            id="txtFechaVencimiento" style="width:12%"  />
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;" width="20%">
                                                        CANTIDAD&nbsp;&nbsp;
                                                    </td>
                                                    <td style="border: 2px solid #e8e8e8;" colspan="3">
                                                        <input class="estImputAdminLargo" type="text" id="txtCantidad"
                                                            style="width:12%;"  />
                                                    </td>
                                                </tr>
                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">VALOR UNITARIO (SIN IVA)</td>
                                                    <td style="border: 2px solid #e8e8e8;"><input
                                                            class="estImputAdminLargo" type="text" id="txtValorU"
                                                            style="width:12%"
                                                            onKeyPress="javascript:return soloNumeros(event)"
                                                            
                                                            onkeyup="calcularImpuesto('lblValorImpuesto')"
                                                            onblur="guardarYtraerDatoAlListado('verificaValorInventario');" />
                                                    </td>
                                                </tr>

                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">DESCUENTO</td>
                                                    <td style="border: 2px solid #e8e8e8;"><input
                                                            class="estImputAdminLargo" type="text" id="txtDescuento"
                                                            style="width:12%" />
                                                    </td>
                                                </tr>

                                                <tr class="estiloImputIzq2">
                                                    <td style="border: 2px solid #e8e8e8;">IVA</td>
                                                    <td style="border: 2px solid #e8e8e8;">
                                                        <select class="estImputAdminLargo" id="cmbIva" style="width:12%"
                                                            
                                                            onblur="calcularImpuesto('lblValorImpuesto')">
                                                            <option value=""></option>
                                                            <%     resultaux.clear();
                                                                           resultaux=(ArrayList)beanAdmin.combo.cargar(520);	
                                                                           ComboVO cmbIvaArticulo; 
                                                                           for(int k=0;k<resultaux.size();k++){ 
                                                                                 cmbIvaArticulo=(ComboVO)resultaux.get(k);
                                                                    %>
                                                            <option value="<%= cmbIvaArticulo.getId()%>"
                                                                title="<%= cmbIvaArticulo.getTitle()%>">
                                                                <%=cmbIvaArticulo.getDescripcion()%></option>
                                                            <%} %>
                                                        </select> %
                                                    </td>
                                                </tr>
                                            </table>
                                    <tr>
                                        <td class="estiloImput">
                                            <input title="btn_bt754" type="button" class="small button blue"
                                                value="AÑADIR PRODUCTO Y CANTIDAD"
                                                onclick="modificarCRUDOrdenEntrega('asignarAtributos')" />
                                            <input title="btn_bt754" type="button" class="small button blue"
                                                value="REVERTIR ACCION"
                                                onclick="modificarCRUDOrdenEntrega('revertirTransaccionOrdenCompra')" />
                                            <input title="btn_bt754" type="button" class="small button blue"
                                                value="CONFIRMAR ENTREGA"
                                                onclick="modificarCRUDOrdenEntrega('RelacionarTransConFac')" />
                                            <input title="btn_bt754" type="button" class="small button blue"
                                                value="IMPRIMIR FACTURA" onclick="impresionFacturaOrdenDeCompra()" />
                                            <input title="btn_bt754" type="button" class="small button blue"
                                                value="IMPRIMIR TODAS LAS FACTURAS"
                                                onclick="impresionTodasFacturaOrdenDeCompra()" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: 2px solid #e8e8e8;">
                                            <div id="divContenido">
                                                <table id="listTransaccionesOrdenEntrada" class="scroll"></table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>

<label hidden id="txtIdOC"></label>
<label hidden id="txtIdArticuloOC"></label>
<label hidden id="lblObservacion"></label>
<label hidden id="lblIdTercero"></label>
<label hidden id="lblObservacionOrden"></label>
<label hidden id="lblClase"></label>
<label hidden id="lblCantidad_piv"></label>
<label hidden id="lblUltimaCantidad"></label>
<label hidden id="lblTotalEntregado"></label>
<label hidden id="lblArticuloCodigo"></label>


<label hidden id="lblCantidadTransaccionRevertir"></label>
<label hidden id="lblIdArticuloBorrarTrans"></label>
<label hidden id="lblIdArticuloArevertir"></label>
<label hidden id="lblSwFactura"></label>
<label hidden id="lblIdDoc"></label>
<label hidden id="lblInventario"></label>