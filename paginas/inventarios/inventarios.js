// JavaScript para la programacion de los botones

function asignarFuncionario() {
	if (valorAtributo('lblIdEncargado') == '') {
		asignaAtributo('lblIdEncargado', IdSesion(), 0);
	}
	setTimeout(() => {

	}, 1000);
}

function cargarPacientes() {
	//cargarComboGRAL('cmbIdAdmision', 'xxx', 'cmbIdAdmision', 511 ) 
	fecDesde = valorAtributo('txtFechaDesdeDoc');
	fecHasta = valorAtributo('txtFechaHastaDoc') + ' 23:59:59';

	if (valorAtributo('cmbIdGastos') == 'S') {
		cargarComboGRALCondicion2('cmbIdAdmision', 'xxx', 'cmbIdAdmision', 550, fecDesde, fecHasta)
	} else {
		cargarComboGRALCondicion2('cmbIdAdmision', 'xxx', 'cmbIdAdmision', 511, fecDesde, fecHasta)
	}
}

function cargarPacientesDevolucion() {
	fecDesde = valorAtributo('txtFechaDesdeDoc');
	fecHasta = valorAtributo('txtFechaHastaDoc') + ' 23:59:59';
	switch (valorAtributo('cmbIdTipoDevolucion')) {
		case 'CON_ADMISION':
			cargarComboGRALCondicion2('cmbIdAdmision', 'xxx', 'cmbIdAdmision', 512, fecDesde, fecHasta)
			break;
		case 'SIN_ADMISION':
			cargarComboGRALCondicion2('cmbIdAdmision', 'xxx', 'cmbIdAdmision', 527, fecDesde, fecHasta)
			break;
		case 'OTROS':
			cargarComboGRALCondicion2('cmbIdAdmision', 'xxx', 'cmbIdAdmision', 537, fecDesde, fecHasta)
			break;
	}
}

function cargarListSolicitud() {

	if (valorAtributo('cmbIdGastos') == 'S') {
		asignaAtributo('cmbIdTipoDocumento', '34', 0);
		buscarSuministros('listGrillaDespachoSGastos');
	} else {
		asignaAtributo('cmbIdTipoDocumento', '22', 0);
		buscarSuministros('listGrillaDespachoSolicitudes');
	}
}

function cargarSolicitudPaciente() {
	asignaAtributo('lblIdAdmisionAgen', valorAtributo('cmbIdAdmision'), 0);
	setTimeout("buscarSuministros('listGrillaDespachoSolicitudes')", 500);

}

function cargarDevolucionPaciente() {
	asignaAtributo('lblIdAdmisionAgen', valorAtributo('cmbIdAdmision'), 0);
	buscarSuministros('listGrillaDevoluciones');
}


/** funciones para la accion de busqueda*/





function agregarListaArticuloTemporal() {



	if (verificarCamposGuardar('agregarListaArticuloTemporal')) {
		/*var ids = jQuery("#listGrillaViaArticulo").getDataIDs();			  
							  
				  for(var i=0;i<ids.length;i++){ 
					  var c = ids[i];
					  var datosRow = jQuery("#listGrillaViaArticulo").getRowData(i); 
	
					  console.log(i+ ' - '+datosRow.VIA+' - '+c)
					  if(c == valorAtributo('cmbViaArticulo')  ){ 
							console.log('YA EXISTE');
					  }
				  }	
		*/
		jQuery("#listGrillaViaArticulo").jqGrid({
			datatype: "local",
			height: 120,
			width: 890,
			colNames: ['ID', 'VIA'],
			colModel: [
				{ name: 'id', index: 'id', width: 20 },
				{ name: 'via', index: 'via', width: 160 },
			], onSelectRow: function (rowid) {
				jQuery("#listGrillaViaArticulo").delRowData(rowid);
			}
		});
		var row = {
			id: valorAtributo('cmbViaArticulo'),
			via: valorAtributoCombo('cmbViaArticulo')
		};

		jQuery("#listGrillaViaArticulo").addRowData(valorAtributo('cmbViaArticulo'), row);
	}

}


function buscarSuministros(arg) {
	idArticulo = '';
	pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
	pagina = pag;
	ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
	//    alto= ($('#drag'+ventanaActual.num).find("#divContenido").width())*92/100;	
	switch (arg) {

		case 'listGrillaTransaccionSalida':
			//ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width())-50;
			ancho = 1000;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=96&parametros=";
			/*add_valores_a_mandar( valorAtributo('txtCodDiagnosticoBus')  );	
			add_valores_a_mandar( valorAtributo('txtNomDiagnosticoBus')  );				 */
			add_valores_a_mandar(valorAtributo('lblIdDoc'));
			$('#drag' + ventanaActual.num).find("#listGrillaTransaccionDocumento").jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['Tot', 'ID', 'ID_DOCUMENTO', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'CANTIDAD', 'VALOR_UNITARIO', 'IVA %', 'VALORIMPUESTO', 'NATURALEZA', 'LOTE', 'Fecha_vencimiento', 'SERIAL', 'PRECIO_VENTA', 'EXITO', 'DESC', 'TIPO'
				],
				colModel: [
					{ name: 'contador', index: 'contador', width: anchoP(ancho, 3) },
					{ name: 'ID', index: 'ID', hidden: true },
					{ name: 'ID_DOCUMENTO', index: 'ID_DOCUMENTO', hidden: true },
					{ name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
					{ name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 30) },
					{ name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 8) },
					{ name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', width: anchoP(ancho, 8) },
					{ name: 'IVA', index: 'IVA', hidden: true },
					{ name: 'VALOR_IMPUESTO', index: 'VALOR_IMPUESTO', hidden: true },
					{ name: 'ID_NATURALEZA', index: 'ID_NATURALEZA', hidden: true },
					{ name: 'LOTE', index: 'LOTE', width: anchoP(ancho, 8) },
					{ name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: anchoP(ancho, 8) },
					{ name: 'SERIAL', index: 'SERIAL', width: anchoP(ancho, 8) },
					{ name: 'PRECIO_VENTA', index: 'PRECIO_VENTA', width: anchoP(ancho, 4) },
					{ name: 'EXITO', index: 'EXITO', width: anchoP(ancho, 5) },
					{ name: 'porcent_descuento', index: 'porcent_descuento', width: anchoP(ancho, 5) },
					{ name: 'medida', index: 'medida', width: anchoP(ancho, 5) },
				],

				//  pager: jQuery('#pagerGrilla'), 
				height: 80,
				width: ancho,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaTransaccionDocumento').getRowData(rowid);
					estad = 0;
					idArticulo = datosRow.ID_ARTICULO;
					asignaAtributo('lblIdTransaccion', datosRow.ID, 1);
					asignaAtributo('lblMedida', datosRow.medida, 1);
					asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);

					//console.log("Entro a seleccionar transaccion")


					//if(valorAtributo('txtIdTipoBodega')== '4'){
					asignaAtributo('txtSerial', datosRow.SERIAL, 0);
					asignaAtributo('lblValorUnitario', datosRow.VALOR_UNITARIO, 0);
					asignaAtributo('txtVlrUnitarioAnterior', datosRow.VALOR_UNITARIO, 0);
					asignaAtributo('lblIva', datosRow.IVA, 0);
					asignaAtributo('lblValorImpuesto', datosRow.VALOR_IMPUESTO, 0);
					asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);
					asignaAtributo('cmbIdDescuento', '0', 0);
					//}

					//$("#txtIdArticulo").focus();
				},
			});
			$('#drag' + ventanaActual.num).find("#listGrillaTransaccionDocumento").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

			break;

		case 'listGrillaDocumentosBodegaProgramacion':
			/* ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width())-50; */
			ancho = 1250;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=483&parametros=";
			add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['Tot', 'ID', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA_DOCUMENTO', 'ID_TERCERO',
					'NOMBRE_TERCERO', 'NATURALEZA', 'OBSERVACION', 'ID_ESTADO', 'ESTADO'
				],
				colModel: [
					{ name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
					{ name: 'ID', index: 'ID', hidden: true },
					{ name: 'ID_DOCUMENTO_TIPO', index: 'ID_DOCUMENTO_TIPO', hidden: true },
					{ name: 'NOMBRE_DOCUMENTO_TIPO', index: 'NOMBRE_DOCUMENTO_TIPO', width: anchoP(ancho, 25) },
					{ name: 'NUMERO', index: 'NUMERO', width: anchoP(ancho, 8) },
					{ name: 'FECHA_DOCUMENTO', index: 'FECHA_DOCUMENTO', width: anchoP(ancho, 8) },
					{ name: 'ID_TERCERO', index: 'ID_TERCERO', hidden: true },
					{ name: 'NOMBRE_TERCERO', index: 'NOMBRE_TERCERO', width: anchoP(ancho, 8) },
					{ name: 'NATURALEZA', index: 'NATURALEZA', width: anchoP(ancho, 6) },
					{ name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 20) },
					{ name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
					{ name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 6) },

				],

				//  pager: jQuery('#pagerGrilla'), 
				height: 200,
				width: ancho + 40,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('lblIdDoc', datosRow.ID, 1);
					asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 0);
					asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
					asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
					asignaAtributo('lblNaturaleza', datosRow.NATURALEZA, 0);
					asignaAtributo('txtNumero', datosRow.NUMERO, 0);
					asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
					asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);

					/* cargar transacciones del documento */
					limpiarDivEditarJuan('limpCamposTransaccion')
					if (valorAtributo('txtNaturaleza') == 'I') {
						buscarSuministros('listGrillaTransaccionDocumento');
					} else {
						buscarSuministros('listGrillaTransaccionSalida');
					}

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

			break;
		case 'listGrillaElemento3':

			// limpiarDivEditarJuan(arg); 
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=43&parametros=";
			add_valores_a_mandar(valorAtributo('txtCodBus'));
			add_valores_a_mandar(valorAtributo('txtNomBus'));
			add_valores_a_mandar(valorAtributo('cmbIdClaseBus'));
			add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['Tot', 'ID', 'NOMBRE', 'NOMBRE_COMERCIAL', 'MARCA', 'REFERENCIA', 'ID_FABRICANTE', 'NOMBRE_FABRICANTE', 'ID_GRUPO', 'ID_CLASE', 'ID_SUBCLASE', 'COD_ARTICULO', 'DESCRIPCION',
					'PRESENTACION', 'ID_UNIDAD', 'VALOR_UN', 'UNIDAD', 'PORC_IVA', 'PORC_IVA_VENTA', 'CODIGO_INVIMA', 'VENCIMIENTO_INVIMA', 'CODIGO_ATC', 'SW_FECHA_VENCIMIENTO', 'SW_PROVEEDOR_UNICO', 'ID_ANATOFARMACOLOGICO',
					'ID_PRINCIPIO_ACTIVO', 'ID_FARMACOLOGICA', 'CONCENTRACION', 'FACTOR_CONVERSION', 'A_CLASE', 'CONCENTRACION', 'MED_FORM_FARMACE', 'CLASE', 'POS', 'VIGENTE', 'CLASE_RIESGO', 'VIDA_UTIL', 'atc'
				],
				colModel: [
					{ name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
					{ name: 'ID', index: 'ID', width: anchoP(ancho, 2) },
					{ name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 25) },
					{ name: 'NOMBRE_COMERCIAL', index: 'NOMBRE_COMERCIAL', hidden: true },
					{ name: 'marca', index: 'marca', width: anchoP(ancho, 6) },
					{ name: 'DESCRIPCION_ABREVIADA', index: 'DESCRIPCION_ABREVIADA', width: anchoP(ancho, 6) },
					{ name: 'ID_FABRICANTE', index: 'ID_FABRICANTE', hidden: true },
					{ name: 'NOMBRE_FABRICANTE', index: 'NOMBRE_FABRICANTE', hidden: true },
					{ name: 'ID_GRUPO', index: 'ID_GRUPO', hidden: true },
					{ name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
					{ name: 'ID_SUBTIPO', index: 'ID_SUBTIPO', hidden: true },
					{ name: 'COD_ARTICULO', index: 'COD_ARTICULO', hidden: true },
					{ name: 'DESCRIPCION', index: 'DESCRIPCION', hidden: true },
					{ name: 'PRESENTACION', index: 'PRESENTACION', width: anchoP(ancho, 5) },
					{ name: 'ID_UNIDAD', index: 'ID_UNIDAD', hidden: true },
					{ name: 'VALOR_UNIDAD', index: 'VALOR_UNIDAD', width: anchoP(ancho, 4) },
					{ name: 'ML', index: 'ML', width: anchoP(ancho, 3) },
					{ name: 'PORC_IVA', index: 'PORC_IVA', hidden: true },
					{ name: 'PORC_IVA_VENTA', index: 'PORC_IVA_VENTA', hidden: true },
					{ name: 'CODIGO_INVIMA', index: 'CODIGO_INVIMA', hidden: true },
					{ name: 'VENCIMIENTO_INVIMA', index: 'VENCIMIENTO_INVIMA', hidden: true },
					{ name: 'CODIGO_ATC', index: 'CODIGO_ATC', hidden: true },
					{ name: 'SW_FECHA_VENCIMIENTO', index: 'SW_FECHA_VENCIMIENTO', hidden: true },
					{ name: 'SW_PROVEEDOR_UNICO', index: 'SW_PROVEEDOR_UNICO', hidden: true },

					{ name: 'ID_ANATOFARMACOLOGICO', index: 'ID_ANATOFARMACOLOGICO', hidden: true },
					{ name: 'ID_PRINCIPIO_ACTIVO', index: 'ID_PRINCIPIO_ACTIVO', hidden: true },
					{ name: 'ID_FARMACOLOGICA', index: 'ID_FARMACOLOGICA', hidden: true },
					{ name: 'CONCENTRACION', index: 'CONCENTRACION', hidden: true },
					{ name: 'FACTOR_CONVERSION', index: 'FACTOR_CONVERSION', hidden: true },
					{ name: 'A_CLASE', index: 'A_CLASE', hidden: true },
					{ name: 'A_CONCENTRACION', index: 'A_CONCENTRACION', width: anchoP(ancho, 5) },
					{ name: 'ID_FORMA_FARMACEUTICA', index: 'ID_FORMA_FARMACEUTICA', hidden: true },
					{ name: 'CLASE', index: 'CLASE', width: anchoP(ancho, 5) },
					{ name: 'POS', index: 'POS', width: anchoP(ancho, 3) },
					{ name: 'VIGENTE', index: 'VIGENTE', width: anchoP(ancho, 2) },
					{ name: 'CLASE_RIESGO', index: 'CLASE_RIESGO', hidden: true },
					{ name: 'VIDA_UTIL', index: 'VIDA_UTIL', hidden: true },
					{ name: 'atc', index: 'atc', hidden: true },

				],

				//  pager: jQuery('#pagerGrilla'), 
				height: 250,
				width: ancho + 40,
				onSelectRow: function (rowid) {
				
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaElemento3').getRowData(rowid);
					estad = 0;
					asignaAtributo('txtIdArticulo', datosRow.ID, 1);
					asignaAtributo('txtNombreArticulo', datosRow.NOMBRE, 0);
					asignaAtributo('lblNomArticulo', datosRow.NOMBRE, 0);

					asignaAtributo('cmbIdGrupo', datosRow.ID_GRUPO, 0);
					asignaAtributo('cmbIdTipo', datosRow.ID_TIPO, 0);
					asignaAtributo('cmbIdSubTipo', datosRow.ID_SUBTIPO, 0);
					asignaAtributo('txtCodArticulo', datosRow.COD_ARTICULO, 0);
					asignaAtributo('txtDescripcionCompleta', datosRow.DESCRIPCION, 0);
					asignaAtributo('txtDescripcionAbreviada', datosRow.DESCRIPCION_ABREVIADA, 0);
					asignaAtributo('txtIdFabricante', concatenarCodigoNombre(datosRow.ID_FABRICANTE, datosRow.NOMBRE_FABRICANTE), 0)
					asignaAtributo('cmbIdUnidad', datosRow.ID_UNIDAD, 0);
					asignaAtributo('txtValorUnidad', datosRow.VALOR_UNIDAD, 0);
					asignaAtributo('cmbML', datosRow.ML, 0);
					asignaAtributo('txtIVA', datosRow.PORC_IVA, 0);
					asignaAtributo('cmbIVAVenta', datosRow.PORC_IVA_VENTA, 0);
					asignaAtributo('txtInvima', datosRow.CODIGO_INVIMA, 0);
					asignaAtributo('txtVigInvima', datosRow.VENCIMIENTO_INVIMA, 0);
					//asignaAtributo('txtATC', datosRow.CODIGO_ATC, 0);
					asignaAtributo('txtATC', datosRow.CUM, 0);
					asignaAtributo('cmbIdFechaVencimiento', datosRow.SW_FECHA_VENCIMIENTO, 0);
					asignaAtributo('cmbIdProveedorUnico', datosRow.SW_PROVEEDOR_UNICO, 0);
					asignaAtributo('cmbPOS', datosRow.POS, 0);
					asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);

					asignaAtributo('cmbIdClaseRiesgo', datosRow.CLASE_RIESGO, 0);
					asignaAtributo('cmbIdVidaUtil', datosRow.VIDA_UTIL, 0);
					asignaAtributo('txtMarca', datosRow.marca, 0);

					asignaAtributo('cmbAnatomofarmacologico', datosRow.ID_ANATOFARMACOLOGICO, 0);
					asignaAtributo('cmbPrincipioActivo', datosRow.ID_PRINCIPIO_ACTIVO, 0);
					asignaAtributo('cmbFormaFarmacologica', datosRow.ID_FARMACOLOGICA, 0);
					asignaAtributo('txtoncentracion', datosRow.CONCENTRACION, 0);
					asignaAtributo('txtFactorConv', datosRow.FACTOR_CONVERSION, 0);
					id_clase = trim(datosRow.A_CLASE);
					//alert (id_clase);
					asignaAtributo('cmbIdClase', id_clase, 0);
					//asignaAtributo('cmbIdClase2',id_clase,0);
					asignaAtributo('cmbPresentacion', datosRow.PRESENTACION, 0);
					asignaAtributo('txtConcentracion', datosRow.A_CONCENTRACION, 0);
					asignaAtributo('cmbIdUnidadAdministracion', datosRow.ID_FORMA_FARMACEUTICA, 0);
					asignaAtributo('txtNombreComercial', datosRow.NOMBRE_COMERCIAL, 0);
					//asignaAtributo('txtCodAtc', datosRow.atc, 0);
					asignaAtributo('txtCodAtc', CODIGO_ATC, 0);

					buscarSuministros('listGrillaViaArticulo');

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
			break;

		case 'listGrillaViaArticulo':
			ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
			valores_a_mandar = pag;
			valores_a_mandar = valores_a_mandar + "?idQuery=523&parametros=";
			add_valores_a_mandar(valorAtributo('txtIdArticulo'));

			$('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
				url: valores_a_mandar,
				datatype: 'xml',
				mtype: 'GET',

				colNames: ['Tot', 'ID', 'VIA'
				],
				colModel: [
					{ name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
					{ name: 'ID', index: 'ID', hidden: true },
					{ name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 52) },

				],

				//  pager: jQuery('#pagerGrilla'), 
				height: 200,
				width: ancho + 40,
				onSelectRow: function (rowid) {
					//			 limpiarDivEditarJuan(arg); 
					var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
					estad = 0;
					asignaAtributo('cmbViaArticulo', datosRow.ID, 0);
					asignaAtributo('txtIdViaArticulo', datosRow.ID, 0);

				},
			});
			$('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

			break;
	}//fin switch
}
var topY = 100;

function abrirVentanaConteoItem(codItem, NomItem) {
	mostrar('divSublistadoConteo')
	$('#drag' + ventanaActual.num).find('#lblCodItem').html(codItem)
	$('#drag' + ventanaActual.num).find('#lblNombreItem').html(NomItem)

	$('#drag' + ventanaActual.num).find('#txtValorConteo').val('');
	$('#drag' + ventanaActual.num).find('#txtValorConteo').focus();
	document.getElementById('divSublistadoConteo').style.top = topY;
}

function handleInput(e) {
	var ss = e.target.selectionStart;
	var se = e.target.selectionEnd;
	e.target.value = e.target.value.toUpperCase();
	e.target.selectionStart = ss;
	e.target.selectionEnd = se;
}


function guardarValorConteoItems() {
	ocultar('divSublistadoConteo')
	varajaxInit = crearAjax();
	varajaxInit.open("POST", '/clinica/paginas/accionesXml/guardar_xml.jsp', true);
	varajaxInit.onreadystatechange = respuestaGuardarValorConteoItemsAjax;
	varajaxInit.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

	valores_a_mandar = "accion=valorConteoItem";
	valores_a_mandar = valores_a_mandar + "&codItem=" + $('#drag' + ventanaActual.num).find('#lblCodItem').html();
	valores_a_mandar = valores_a_mandar + "&ValorConteo=" + $('#drag' + ventanaActual.num).find('#txtValorConteo').val();
	varajaxInit.send(valores_a_mandar);
}
function respuestaGuardarValorConteoItemsAjax() {

	if (varajaxInit.readyState == 4) {
		if (varajaxInit.status == 200) {
			raiz = varajaxInit.responseXML.documentElement;
			if (raiz.getElementsByTagName('raiz') != null) {

				if (raiz.getElementsByTagName('respuesta')[0].firstChild.data) {
					buscarSuministros('inventario', '/clinica/paginas/accionesXml/buscarSuministros_xml.jsp')
				}
				else {
					alert("Caramba no se pudo actualizar la tarea");
				}

			}
		} else {
			swAlertError('LA ACCION NO SE PUDO REALIZAR. POR FAVOR INTENTE NUEVAMENTE');
		}
	}
	if (varajaxInit.readyState == 1) {
		//abrirVentana(260, 100);
		//VentanaModal.setSombra(true);
	}
}



function guardarSuministros(arg, pag) {  //alert(arg+'--'+pag);  // un nuevo boton adicionar   
	pagina = pag;
	paginaActual = arg;

	switch (arg) {

		case 'adminItem':
			if (verificarCamposGuardar(arg)) {
				if ($('#drag' + ventanaActual.num).find('#lblCodItem').html() == "") {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&NomItem=" + $('#drag' + ventanaActual.num).find('#txtNomItem').val();
					valores_a_mandar = valores_a_mandar + "&UnidMed=" + $('#drag' + ventanaActual.num).find('#cmbUnidMed').val();
					valores_a_mandar = valores_a_mandar + "&Observa=" + $('#drag' + ventanaActual.num).find('#txtObserva').val();
					valores_a_mandar = valores_a_mandar + "&Servicio=" + $('#drag' + ventanaActual.num).find('#cmbServicio').val();
					valores_a_mandar = valores_a_mandar + "&GrupoItem=" + $('#drag' + ventanaActual.num).find('#cmbGrupoItem').val();
					valores_a_mandar = valores_a_mandar + "&Clase=" + $('#drag' + ventanaActual.num).find('#cmbClase').val();
					valores_a_mandar = valores_a_mandar + "&Estado=" + $('#drag' + ventanaActual.num).find('#cmbEstado').val();

					ajaxGuardar();
				}
				else alert('El Item ya existe, pruebe con MODIFICAR');
			}
			break;

	}

}

function respuestaGuardarSuministros(arg, xmlraiz) {

	switch (arg) {
		case 'adminItem':
			alert("!Item guardado Exitosamente");
			limpiarDivEditarJuan(arg)
			break;
	}
}


function modificarSuministros(arg, pag) {// PARA UPDATE

	pagina = pag;
	paginaActual = arg;
	switch (arg) {

		case 'adminItem':
			if (verificarCamposGuardar(arg)) {
				if ($('#drag' + ventanaActual.num).find('#lblCodItem').html() != "") {
					valores_a_mandar = 'accion=' + arg;
					valores_a_mandar = valores_a_mandar + "&CodItem=" + $('#drag' + ventanaActual.num).find('#lblCodItem').html();
					valores_a_mandar = valores_a_mandar + "&NomItem=" + $('#drag' + ventanaActual.num).find('#txtNomItem').val();
					valores_a_mandar = valores_a_mandar + "&UnidMed=" + $('#drag' + ventanaActual.num).find('#cmbUnidMed').val();
					valores_a_mandar = valores_a_mandar + "&Observa=" + $('#drag' + ventanaActual.num).find('#txtObserva').val();
					valores_a_mandar = valores_a_mandar + "&Servicio=" + $('#drag' + ventanaActual.num).find('#cmbServicio').val();
					valores_a_mandar = valores_a_mandar + "&GrupoItem=" + $('#drag' + ventanaActual.num).find('#cmbGrupoItem').val();
					valores_a_mandar = valores_a_mandar + "&Clase=" + $('#drag' + ventanaActual.num).find('#cmbClase').val();
					valores_a_mandar = valores_a_mandar + "&Estado=" + $('#drag' + ventanaActual.num).find('#cmbEstado').val();
					ajaxModificar();

				} else alert('No se puede modificar porque a�n no ha sido creado');
			}
			break;
	}
}

function respuestaModificarSuministros(arg, xmlraiz) {

	switch (arg) {
		case 'adminItem':
			alert("Modificacion Item Exitosa !");
			break;

	}
}

function cambiarLote() {

	if ($('#drag' + ventanaActual.num).find('#chkLote').attr('checked')) {
		asignaAtributo('txtLote', 'SIN_LOTE')
		asignaAtributo('txtFechaVencimiento', '01/12/2020')
	} else {
		asignaAtributo('txtLote', '', 0)
		asignaAtributo('txtFechaVencimiento', '', 0)
	}

}


function probarExiste(idElemento) {
	if (document.getElementById(idElemento) == undefined || document.getElementById(idElemento) == null) {
		return false;
	}
	else {
		return true;
	}
}










