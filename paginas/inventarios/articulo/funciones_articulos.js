function modificarCRUDArticulo(arg, pag) {
    if (pag == "undefined")
        pag = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";

    pagina = "/clinica/paginas/accionesXml/modificarCRUD_xml.jsp";
    paginaActual = arg;
    switch (arg) {

        case 'modificaTransaccionBodega':
            if (verificarCamposGuardar(arg)) {

                switch (valorAtributo('lblNaturaleza')) {
                    case "I":
                        var vlrUnitario = valorAtributo('txtValorU');
                        var lote = valorAtributo('txtLote');
                        var precioVenta = valorAtributo('txtPrecioVenta');
                        var fechaDeVencimiento = valorAtributo('txtFechaVencimiento');
                        var iva = valorAtributo('cmbIva');
                        var serial = valorAtributo('txtSerial');
                        var medida = valorAtributo('cmbMedida');
                        break;
                    case "S":
                        if(valorAtributo('lblIdEstado') === '1'){
                            alert('No se modificar el producto el documento ya esta cerrado');
                            return
                        }
                        var vlrUnitario = valorAtributo('lblValorUnitario');
                        var lote = valorAtributo('cmbIdLote');
                        var iva = valorAtributo('lblIva');
                        var serial = valorAtributo('txtSerial');
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo('txtFV');
                        var medida = valorAtributo('lblMedida');
                        break;
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=88&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
                add_valores_a_mandar(valorAtributo('txtCantidad'));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo('lblValorImpuesto'));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(medida);

                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
            break;

        case 'eliminaTransaccionBodegaSalida':
            if (verificarCamposGuardar(arg)) {
                if(valorAtributo('lblIdEstado') === '1'){
                    alert('No se puedo eliminar el documento ya esta cerrado');
                    return
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=90&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdTransaccion'));

                ajaxModificar();
            }
        break;

        case "crearTransaccion":
            if (valorAtributo("lblValorImpuesto") == "") {
                if (valorAtributo("lblNaturaleza") == "I") {
                    calcularImpuesto("lblValorImpuesto");
                } else {
                    calcularImpuestoSalida();
                }
            }
            if (verificarCamposGuardar(arg)) {
                switch (valorAtributo("lblNaturaleza")) {
                    case "I":
                        var vlrUnitario = valorAtributo("txtValorU");
                        var iva = valorAtributo("cmbIva");
                        var lote = valorAtributo("txtLote");
                        var serial = valorAtributo("txtSerial");
                        var precioVenta = valorAtributo("txtPrecioVenta");
                        var fechaDeVencimiento = valorAtributo("txtFechaVencimiento");
                        var medida = valorAtributo("cmbMedida");
                        var valorPromedio = 0;
                        break;
                    case "S":
                        var rows = jQuery("#listGrillaTransaccionDocumento").jqGrid('getDataIDs');
                        for (var i = 0; i < rows.length; i++) {
                            var row = jQuery("#listGrillaTransaccionDocumento").getRowData(rows[i]);
                            if(valorAtributoIdAutoCompletar('txtIdArticulo').trim() === row.ID_ARTICULO && valorAtributo('cmbIdLote').trim() === row.LOTE ){
                                alert('Ya has agregado este producto, no puedes volver agregar el producto con el mismo lote');
                                return;
                            }
                        }
                        var vlrUnitario = valorAtributo("lblValorUnitario");
                        var iva = valorAtributo("lblIva");
                        var lote = valorAtributo("cmbIdLote");
                        var serial = valorAtributo("txtSerial");
                        var precioVenta = 0;
                        var fechaDeVencimiento = valorAtributo("txtFV");
                        var valorPromedio = vlrUnitario;
                        if (valorAtributo("cmbIdBodega") == 1) var medida = "";
                        //valorAtributo('lblMedida');
                        else var medida = valorAtributo("lblMedida");
                        break;
                }
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=89&parametros=";
                add_valores_a_mandar(valorAtributo("lblIdDoc"));
                add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdArticulo"));
                add_valores_a_mandar(valorAtributo("txtCantidad"));
                add_valores_a_mandar(vlrUnitario);
                add_valores_a_mandar(iva);
                add_valores_a_mandar(valorAtributo("lblValorImpuesto"));
                add_valores_a_mandar(valorAtributo("lblNaturaleza"));
                add_valores_a_mandar(lote);
                add_valores_a_mandar(serial);
                add_valores_a_mandar(precioVenta);
                add_valores_a_mandar(fechaDeVencimiento);
                add_valores_a_mandar(valorPromedio);
                add_valores_a_mandar(medida);
                ajaxModificar();
            }
            break;


        case "crearViaArticuloP":
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=524&parametros=";

                add_valores_a_mandar(valorAtributo("cmbViaArticulo"));
                add_valores_a_mandar(valorAtributo("txtIdArticulo"));
                ajaxModificar();
            }
            break;

        case "eliminarViaArticuloP":
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = "accion=" + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=525&parametros=";

                add_valores_a_mandar(valorAtributo("txtIdViaArticulo"));
                add_valores_a_mandar(valorAtributo("txtIdArticulo"));

                ajaxModificar();
            }
            break;

        case "crearArticuloP":
            switch (valorAtributo("cmbIdClase")) {
                case "MD":
                    //funcionesAdicionales.js crearMedicamento
                    if (verificarCamposGuardar("crearMedicamento")) {
                        valores_a_mandar = "accion=" + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=49&parametros=";
                        add_valores_a_mandar(valorAtributo("txtNombreArticulo"));
                        add_valores_a_mandar(valorAtributo("txtNombreComercial"));
                        add_valores_a_mandar(valorAtributo("cmbIdClase"));
                        add_valores_a_mandar(valorAtributo("cmbIdGrupo"));
                        add_valores_a_mandar(valorAtributo("cmbPresentacion"));
                        add_valores_a_mandar(valorAtributo("txtConcentracion"));
                        add_valores_a_mandar(valorAtributo("txtCodArticulo"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionCompleta"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionAbreviada"));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdFabricante"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidad"));
                        add_valores_a_mandar(valorAtributo("txtValorUnidad"));
                        add_valores_a_mandar(valorAtributo("cmbML")); //
                        add_valores_a_mandar(valorAtributo("txtIVA"));
                        add_valores_a_mandar(valorAtributo("cmbIVAVenta"));
                        add_valores_a_mandar(valorAtributo("txtInvima"));
                        add_valores_a_mandar(valorAtributo("txtVigInvima"));
                        add_valores_a_mandar(valorAtributo("txtATC"));
                        add_valores_a_mandar(valorAtributo("txtATC"));
                        add_valores_a_mandar(valorAtributo("cmbIdFechaVencimiento"));
                        add_valores_a_mandar(valorAtributo("cmbIdProveedorUnico"));
                        add_valores_a_mandar(valorAtributo("cmbPOS"));
                        add_valores_a_mandar(valorAtributo("cmbVigente"));
                        add_valores_a_mandar(valorAtributo("cmbIdClaseRiesgo"));
                        add_valores_a_mandar(valorAtributo("cmbIdVidaUtil"));
                        add_valores_a_mandar(valorAtributo("txtMarca"));
                        add_valores_a_mandar(valorAtributo("txtCodAtc")); /**/
                        /* MEDICAMENTO */
                        add_valores_a_mandar(valorAtributo("cmbAnatomofarmacologico"));
                        add_valores_a_mandar(valorAtributo("cmbPrincipioActivo"));
                        add_valores_a_mandar(valorAtributo("cmbFormaFarmacologica"));
                        add_valores_a_mandar(valorAtributo("txtFactorConv"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidadAdministracion"));
                        /*FORMA FARMACEUTICA*/
                        add_valores_a_mandar(valorAtributo("cmbIdUnidadAdministracion"));

                        ajaxModificar();
                    }
                    break;
                case "DM":
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = "accion=" + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=496&parametros=";
                        add_valores_a_mandar(valorAtributo("txtNombreArticulo"));
                        add_valores_a_mandar(valorAtributo("txtNombreComercial"));
                        add_valores_a_mandar(valorAtributo("cmbIdClase"));
                        add_valores_a_mandar(valorAtributo("cmbIdGrupo"));
                        add_valores_a_mandar(valorAtributo("cmbPresentacion"));
                        add_valores_a_mandar(valorAtributo("txtConcentracion"));
                        add_valores_a_mandar(valorAtributo("txtCodArticulo"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionCompleta"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionAbreviada"));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdFabricante"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidad"));
                        add_valores_a_mandar(valorAtributo("txtValorUnidad"));
                        add_valores_a_mandar(valorAtributo("cmbML"));
                        add_valores_a_mandar(valorAtributo("txtIVA"));
                        add_valores_a_mandar(valorAtributo("cmbIVAVenta"));
                        add_valores_a_mandar(valorAtributo("txtInvima"));
                        add_valores_a_mandar(valorAtributo("txtVigInvima"));
                        add_valores_a_mandar(valorAtributo("txtATC"));
                        add_valores_a_mandar(valorAtributo("cmbIdFechaVencimiento"));
                        add_valores_a_mandar(valorAtributo("cmbIdProveedorUnico"));
                        add_valores_a_mandar(valorAtributo("cmbPOS"));
                        add_valores_a_mandar(valorAtributo("cmbVigente"));
                        add_valores_a_mandar(valorAtributo("cmbIdClaseRiesgo"));
                        add_valores_a_mandar(valorAtributo("cmbIdVidaUtil"));
                        add_valores_a_mandar(valorAtributo("txtMarca"));
                        add_valores_a_mandar(valorAtributo("txtCodAtc")); /**/
                        /* MEDICAMENTO */
                        add_valores_a_mandar(valorAtributo("txtFactorConv"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidadAdministracion"));
                        /*FORMA FARMACEUTICA*/
                        add_valores_a_mandar(valorAtributo("cmbIdUnidadAdministracion"));

                        ajaxModificar();
                    }
                    break;

                default:
                    
                        if(valorAtributo('txtNombreArticulo') === ''){
                            alert('El nombre del articulo es obligatorio');
                            return 
                        }
                        if(valorAtributo('cmbIdGrupo') === ''){
                            alert('Seleccione el Grupo al que pertenece el articulo');
                            return
                        }
                        if(valorAtributo('txtDescripcionCompleta') === ''){
                            alert('La descripcion completa es obligatoria'); 
                            return
                        }
                        if(valorAtributo('txtIVA') === ''){
                            alert('El valor del IVA es obligatorio si no aplica digite 0(CERO)');
                            return
                        }
                        if(valorAtributo('cmbIVAVenta') === ''){
                            alert('El campo valor de venta es obligatorio');
                            return

                        }
                        if(valorAtributo('cmbIdClaseRiesgo') === ''){
                            alert('El campo Riesgo es obligatorio si no aplica seleccione NA'); 
                            return
                        }
                        if(valorAtributo('cmbIdFechaVencimiento') === ''){
                            alert('Tipo de despacho es obligatorio '); 
                            return

                        }
                        if(valorAtributo('cmbIdProveedorUnico') === ''){
                            alert('Tipo de proveedor es obligatorio');
                            return

                        }
                        if(valorAtributo('cmbPOS') === ''){
                            alert('Producto POS  es obligatorio');
                            return
                        }
                        if(valorAtributo('cmbVigente') === ''){
                            alert('Vigente es obligatorio'); 
                            return
                        }
                        if(valorAtributo('cmbIdClase') === ''){
                            alert('El valor clase es obligatorio'); 
                            return
                        }
                        valores_a_mandar = "accion=" + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=2224&parametros=";
                        add_valores_a_mandar(valorAtributo("txtNombreArticulo"));
                        add_valores_a_mandar(valorAtributo("txtNombreComercial"));
                        add_valores_a_mandar(valorAtributo("cmbIdClase"));
                        add_valores_a_mandar(valorAtributo("cmbIdGrupo"));
                        add_valores_a_mandar(valorAtributo("cmbPresentacion"));
                        add_valores_a_mandar(valorAtributo("txtConcentracion"));
                        add_valores_a_mandar(valorAtributo("txtCodArticulo"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionCompleta"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionAbreviada"));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdFabricante"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidad"));
                        add_valores_a_mandar(valorAtributo("txtValorUnidad"));
                        add_valores_a_mandar(valorAtributo("cmbML"));
                        add_valores_a_mandar(valorAtributo("txtIVA"));
                        add_valores_a_mandar(valorAtributo("cmbIVAVenta"));
                        add_valores_a_mandar(valorAtributo("txtInvima"));
                        add_valores_a_mandar(valorAtributo("txtVigInvima"));
                        add_valores_a_mandar(valorAtributo("txtATC"));
                        add_valores_a_mandar(valorAtributo("cmbIdFechaVencimiento"));
                        add_valores_a_mandar(valorAtributo("cmbIdProveedorUnico"));
                        add_valores_a_mandar(valorAtributo("cmbPOS"));
                        add_valores_a_mandar(valorAtributo("cmbVigente"));
                        add_valores_a_mandar(valorAtributo("cmbIdClaseRiesgo"));
                        add_valores_a_mandar(valorAtributo("cmbIdVidaUtil"));
                        add_valores_a_mandar(valorAtributo("txtMarca"));
                        add_valores_a_mandar(valorAtributo("txtCodAtc")); /**/
                        ajaxModificar();
                    break;
            }
            break;
        case "modificaArticuloP":
            switch (valorAtributo("cmbIdClase")) {
                case "MD":
                    if (verificarCamposGuardar("modificaMedicamento")) {
                        valores_a_mandar = "accion=" + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=48&parametros=";
                        add_valores_a_mandar(valorAtributo("txtNombreArticulo"));
                        add_valores_a_mandar(valorAtributo("txtNombreComercial"));
                        add_valores_a_mandar(valorAtributo("cmbIdClase"));
                        add_valores_a_mandar(valorAtributo("cmbIdGrupo"));
                        add_valores_a_mandar(valorAtributo("cmbPresentacion"));
                        add_valores_a_mandar(valorAtributo("txtConcentracion"));
                        add_valores_a_mandar(valorAtributo("txtCodArticulo"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionCompleta"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionAbreviada"));
                        add_valores_a_mandar(valorAtributoIdAutoCompletar("txtIdFabricante"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidad"));
                        add_valores_a_mandar(valorAtributo("txtValorUnidad"));
                        add_valores_a_mandar(valorAtributo("cmbML"));
                        add_valores_a_mandar(valorAtributo("txtIVA"));
                        add_valores_a_mandar(valorAtributo("cmbIVAVenta"));
                        add_valores_a_mandar(valorAtributo("txtInvima"));
                        add_valores_a_mandar(valorAtributo("txtVigInvima"));
                        add_valores_a_mandar(valorAtributo("txtATC"));
                        add_valores_a_mandar(valorAtributo("txtATC"));
                        add_valores_a_mandar(valorAtributo("cmbIdFechaVencimiento"));
                        add_valores_a_mandar(valorAtributo("cmbIdProveedorUnico"));
                        add_valores_a_mandar(valorAtributo("cmbPOS"));
                        add_valores_a_mandar(valorAtributo("cmbVigente"));
                        add_valores_a_mandar(valorAtributo("cmbIdClaseRiesgo"));
                        add_valores_a_mandar(valorAtributo("cmbIdVidaUtil"));
                        add_valores_a_mandar(valorAtributo("txtMarca"));
                        add_valores_a_mandar(valorAtributo("txtCodAtc"));
                        add_valores_a_mandar(valorAtributo("txtIdArticulo"));
                        add_valores_a_mandar(valorAtributo("cmbAnatomofarmacologico"));
                        add_valores_a_mandar(valorAtributo("cmbPrincipioActivo"));
                        add_valores_a_mandar(valorAtributo("cmbFormaFarmacologica"));
                        add_valores_a_mandar(valorAtributo("txtFactorConv"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidadAdministracion"));
                        add_valores_a_mandar(valorAtributo("txtIdArticulo"));
                        ajaxModificar();
                    }
                    break;
                case "DM":
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = "accion=" + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=498&parametros=";
                        add_valores_a_mandar(valorAtributo("txtNombreArticulo"));
                        add_valores_a_mandar(valorAtributo("txtNombreComercial"));
                        add_valores_a_mandar(valorAtributo("cmbIdClase"));
                        add_valores_a_mandar(valorAtributo("cmbIdGrupo"));
                        add_valores_a_mandar(valorAtributo("cmbPresentacion"));
                        add_valores_a_mandar(valorAtributo("txtConcentracion"));
                        add_valores_a_mandar(valorAtributo("txtCodArticulo"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionCompleta"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionAbreviada"));
                        add_valores_a_mandar(
                            valorAtributoIdAutoCompletar("txtIdFabricante")
                        );
                        add_valores_a_mandar(valorAtributo("cmbIdUnidad"));
                        add_valores_a_mandar(valorAtributo("txtValorUnidad"));
                        add_valores_a_mandar(valorAtributo("cmbML"));
                        add_valores_a_mandar(valorAtributo("txtIVA"));
                        add_valores_a_mandar(valorAtributo("cmbIVAVenta"));
                        add_valores_a_mandar(valorAtributo("txtInvima"));
                        add_valores_a_mandar(valorAtributo("txtVigInvima"));
                        add_valores_a_mandar(valorAtributo("txtATC"));
                        add_valores_a_mandar(valorAtributo("cmbIdFechaVencimiento"));
                        add_valores_a_mandar(valorAtributo("cmbIdProveedorUnico"));
                        add_valores_a_mandar(valorAtributo("cmbPOS"));
                        add_valores_a_mandar(valorAtributo("cmbVigente"));
                        add_valores_a_mandar(valorAtributo("cmbIdClaseRiesgo"));
                        add_valores_a_mandar(valorAtributo("cmbIdVidaUtil"));
                        add_valores_a_mandar(valorAtributo("txtMarca"));
                        add_valores_a_mandar(valorAtributo("txtCodAtc"));
                        add_valores_a_mandar(valorAtributo("txtIdArticulo"));
                        add_valores_a_mandar(valorAtributo("txtFactorConv"));
                        add_valores_a_mandar(valorAtributo("cmbIdUnidadAdministracion"));
                        add_valores_a_mandar(valorAtributo("txtIdArticulo"));
                        ajaxModificar();
                    }
                    break;
                default:
                    if (verificarCamposGuardar(arg)) {
                        valores_a_mandar = "accion=" + arg;
                        valores_a_mandar = valores_a_mandar + "&idQuery=45&parametros=";
                        add_valores_a_mandar(valorAtributo("txtNombreArticulo"));
                        add_valores_a_mandar(valorAtributo("txtNombreComercial"));
                        add_valores_a_mandar(valorAtributo("cmbIdClase"));
                        add_valores_a_mandar(valorAtributo("cmbIdGrupo"));
                        add_valores_a_mandar(valorAtributo("cmbPresentacion"));
                        add_valores_a_mandar(valorAtributo("txtConcentracion"));

                        add_valores_a_mandar(valorAtributo("txtCodArticulo"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionCompleta"));
                        add_valores_a_mandar(valorAtributo("txtDescripcionAbreviada"));
                        add_valores_a_mandar(
                            valorAtributoIdAutoCompletar("txtIdFabricante")
                        );
                        add_valores_a_mandar(valorAtributo("cmbIdUnidad"));
                        add_valores_a_mandar(valorAtributo("txtValorUnidad"));
                        add_valores_a_mandar(valorAtributo("cmbML"));
                        add_valores_a_mandar(valorAtributo("txtIVA"));
                        add_valores_a_mandar(valorAtributo("cmbIVAVenta"));
                        add_valores_a_mandar(valorAtributo("txtInvima"));
                        add_valores_a_mandar(valorAtributo("txtVigInvima"));
                        add_valores_a_mandar(valorAtributo("cmbIdFechaVencimiento"));
                        add_valores_a_mandar(valorAtributo("cmbIdProveedorUnico"));
                        add_valores_a_mandar(valorAtributo("cmbPOS"));
                        add_valores_a_mandar(valorAtributo("cmbVigente"));
                        add_valores_a_mandar(valorAtributo("cmbIdClaseRiesgo"));
                        add_valores_a_mandar(valorAtributo("cmbIdVidaUtil"));
                        add_valores_a_mandar(valorAtributo("txtMarca"));
                        add_valores_a_mandar(valorAtributo("txtIdArticulo"));

                        ajaxModificar();
                    }
                    break;
            }
            break;
    }
}


function buscarSuministrosInventarios(arg) {
    idArticulo = '';
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;

    /* if(document.getElementById('cmbIdClaseBus').value !== 'MD'){
        document.getElementById('datosMedicamento').style.display = "none";
    }else{
        document.getElementById('datosMedicamento').style.display = '';
    }*/
    switch (arg) {
        case 'listaPanelMedicamentos':
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=779&parametros=";
            add_valores_a_mandar(valorAtributo('cmbEstado'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdPaciente'));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdPaciente'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeP'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaP'));
            add_valores_a_mandar(valorAtributo('cmbSede'));
            add_valores_a_mandar(valorAtributo('cmbSede'));

            $('#' + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'FORMULA', 'FOLIO', 'FECHA', 'IDENTIFICACION', 'PACIENTE', 'MEDICO', 'ID_ESTADO',
                    'ESTADO', 'OBSERVACION'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    hidden: true
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: 6
                },
                {
                    name: 'FOLIO',
                    index: 'FOLIO',
                    width: 20
                },
                {
                    name: 'FECHA',
                    index: 'FECHA',
                    width: 10
                },
                {
                    name: 'IDENTIFICACION',
                    index: 'IDENTIFICACION',
                    width: 10
                },
                {
                    name: 'PACIENTE',
                    index: 'PACIENTE',
                    width: 20
                },
                {
                    name: 'MEDICO',
                    index: 'MEDICO',
                    width: 15
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: 12
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    width: 25
                }
                ],
                height: 300,
                width: 1140,
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdEvolucion', datosRow.ID, 0);
                    buscarSuministros('listaDetallePanelMedicamentos');
                }
            });
            $("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');


            break;


        case 'listaDetallePanelMedicamentos':

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=828&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdEvolucion"));

            $('#' + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'ID', 'ID_EVOLUCION', 'POS', 'ID ARTICULO', 'ID SF', 'NOMBRE',
                    'DESCRIPCION', 'VIA', 'FORMA FARMACEUTICA', 'CANTIDAD', 'UNIDAD', 'INDICACION', 'ID_ESTADO_SF', 'ESTADO'
                ],
                colModel: [{
                    name: 'C',
                    index: 'C',
                    hidden: true
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_EVOLUCION',
                    index: 'ID_EVOLUCION',
                    hidden: true
                },
                {
                    name: 'POS',
                    index: 'POS',
                    width: 4
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    hidden: true
                },
                {
                    name: 'ID_SF',
                    index: 'ID_SF',
                    width: 4
                },
                {
                    name: 'NOMBRE',
                    index: 'NOMBRE',
                    width: 15
                },
                {
                    name: 'DESCRIPCION',
                    index: 'DESCRIPCION',
                    width: 15
                },
                {
                    name: 'VIA',
                    index: 'VIA',
                    width: 8
                },
                {
                    name: 'FORMA_FARMACEUTICA',
                    index: 'FORMA_FARMACEUTICA',
                    width: 13
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: 6
                },
                {
                    name: 'UNIDAD',
                    index: 'UNIDAD',
                    width: 8
                },
                {
                    name: 'INDICACION',
                    index: 'INDICACION',
                    width: 18
                },
                {
                    name: 'ID_ESTADO_SF',
                    index: 'ID_ESTADO_SF',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: 12
                }
                ],
                height: 100,
                width: 1130,
                caption: "MEDICAMENTOS ORDENADOS",
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdMedicacion', datosRow.ID, 0);
                    asignaAtributo('lblIdArticulo', datosRow.ID_ARTICULO, 0);
                    buscarSuministros('listaEntregaPanelMedicamentos');
                }
            });
            $("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;


        case 'listaEntregaPanelMedicamentos':

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=829&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdMedicacion"));

            $('#' + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['C', 'CONSECUTIVO', 'ID_ARTICULO', 'ID SF', 'NOMBRE', 'DESCRIPCION', 'CANTIDAD', 'WEB SERVICES', 'FECHA ENTREGA'],
                colModel: [{
                    name: 'C',
                    index: 'C',
                    hidden: true
                },
                {
                    name: 'CONSECUTIVO',
                    index: 'CONSECUTIVO',
                    width: 6
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    hidden: true
                },
                {
                    name: 'ID_SF',
                    index: 'ID_SF',
                    width: 3
                },
                {
                    name: 'NOMBRE',
                    index: 'NOMBRE',
                    width: 20
                },
                {
                    name: 'DESCRIPCION',
                    index: 'DESCRIPCION',
                    width: 25
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: 5
                },
                {
                    name: 'WEB_SERVICES',
                    index: 'WEB_SERVICES',
                    width: 8
                },
                {
                    name: 'FECHA ENTREGA',
                    index: 'FECHA ENTREGA',
                    width: 10
                }
                ],
                height: 100,
                width: 1130,
                caption: "MEDICAMENTOS ENTREGADOS",
                onSelectRow: function (rowid) {
                    var datosRow = $('#' + arg).getRowData(rowid);
                }
            });
            $("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;


        case "listaMedicaciones":

            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=690&parametros=";
            add_valores_a_mandar(valorAtributo("lblIdEvolucion"));

            $('#' + arg).jqGrid({
                url: valores_a_mandar,
                datatype: "xml",
                mtype: "GET",
                colNames: [
                    "contador", "ID", "ID_EVOLUCION", "POS", "ID_ARTICULO", "Articulo", "id_via", "Via", "id_forma", "Forma", "Dosis", "id_frecuencia", "Frecuencia", "Duracion Tratamiento", "id_cantidad", "unidad", "Cantidad", "Indicacion", "Fecha_elaboro",
                ],
                colModel: [{
                    name: "contador",
                    index: "contador",
                    hidden: true
                },
                {
                    name: "ID",
                    index: "ID",
                    hidden: true
                },
                {
                    name: "ID_EVOLUCION",
                    index: "ID_EVOLUCION",
                    hidden: true
                },
                {
                    name: "POS",
                    index: "POS",
                    width: anchoP(ancho, 5)
                },
                {
                    name: "ID_ARTICULO",
                    index: "ID_ARTICULO",
                    hidden: true
                },
                {
                    name: "NOMBRE_ARTICULO",
                    index: "NOMBRE_ARTICULO",
                    width: anchoP(ancho, 30)
                },
                {
                    name: "ID_VIA",
                    index: "ID_VIA",
                    hidden: true
                },
                {
                    name: "NOMBRE_VIA",
                    index: "NOMBRE_VIA",
                    width: anchoP(ancho, 10)
                },
                {
                    name: "ID_FORMA",
                    index: "ID_FORMA",
                    hidden: true
                },
                {
                    name: "NOMBRE_FORMA",
                    index: "NOMBRE_FORMA",
                    width: anchoP(ancho, 10)
                },
                {
                    name: "DOSIS",
                    index: "DOSIS",
                    width: anchoP(ancho, 8)
                },
                {
                    name: "ID_FRECUENCIA",
                    index: "ID_FRECUENCIA",
                    hidden: true
                },
                {
                    name: "FRECUENCIA",
                    index: "FRECUENCIA",
                    width: anchoP(ancho, 12)
                },
                {
                    name: "DIAS",
                    index: "DIAS",
                    width: anchoP(ancho, 8)
                },
                {
                    name: "ID_CANTIDAD",
                    index: "ID_CANTIDAD",
                    hidden: true
                },
                {
                    name: "UNIDAD",
                    index: "UNIDAD",
                    hidden: true
                },
                {
                    name: "CANTIDAD",
                    index: "CANTIDAD",
                    width: anchoP(ancho, 8)
                },
                {
                    name: "INDICACION",
                    index: "INDICACION",
                    width: anchoP(ancho, 20)
                },
                {
                    name: "FECHA_ELABORO",
                    index: "FECHA_ELABORO",
                    hidden: true
                },
                ],
                onSelectRow: function (rowid) {
                    var datosRow = jQuery("#drag" + ventanaActual.num)
                        .find("#" + arg)
                        .getRowData(rowid);

                },
                height: 100,
                width: 1140,
            });
            $('#' + arg).setGridParam({
                url: valores_a_mandar
            })
                .trigger("reloadGrid");
            break;




        case 'listDescuentosArticulo':
            asignaAtributo('lblIdArtDescuento', valorAtributoIdAutoCompletar('txtIdArticulo'), 1);
            asignaAtributo('lblArtDescuento', $('#drag' + ventanaActual.num).find('#txtIdArticulo').val().trim(), 1);

            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=647&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
            add_valores_a_mandar(valorAtributo('txtSerial'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'SERIAL', 'PORCENTAJE'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 30)
                },
                {
                    name: 'SERIAL',
                    index: 'SERIAL',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'porcent_descuento',
                    index: 'porcent_descuento',
                    width: anchoP(ancho, 8)
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 60,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdArtDescuento', datosRow.ID_ARTICULO, 1);
                    asignaAtributo('lblSerialDescuento', datosRow.SERIAL, 1);
                    asignaAtributo('txtDescuentoArt', datosRow.porcent_descuento, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;

        case 'listGrillaDevolucionHG':
            ancho = 980;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=515&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'CANTIDAD', 'VALOR_UNITARIO', 'IVA %', 'VALORIMPUESTO', 'NATURALEZA',
                    'LOTE', 'SERIAL', 'PRECIO_VENTA', 'EXITO', 'FACTOR_CONVERSION', 'Consumo', 'FORMA_FARMACEUTICA', 'FECHA_ELAB', 'EXISTENCIA_BODEGA'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_DOCUMENTO',
                    index: 'ID_DOCUMENTO',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 30)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 4)
                },
                {
                    name: 'VALOR_UNITARIO',
                    index: 'VALOR_UNITARIO',
                    hidden: true
                },
                {
                    name: 'IVA',
                    index: 'IVA',
                    hidden: true
                },
                {
                    name: 'VALOR_IMPUESTO',
                    index: 'VALOR_IMPUESTO',
                    hidden: true
                },
                {
                    name: 'ID_NATURALEZA',
                    index: 'ID_NATURALEZA',
                    hidden: true
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'SERIAL',
                    index: 'SERIAL',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'PRECIO_VENTA',
                    index: 'PRECIO_VENTA',
                    hidden: true
                },
                {
                    name: 'EXITO',
                    index: 'EXITO',
                    hidden: true
                },
                {
                    name: 'FACTOR_CONVERSION',
                    index: 'FACTOR_CONVERSION',
                    hidden: true
                },
                {
                    name: 'CONSUM',
                    index: 'CONSUM',
                    hidden: true
                },
                {
                    name: 'FORMA_FARMACEUTICA',
                    index: 'FORMA_FARMACEUTICA',
                    hidden: true
                },
                {
                    name: 'FECHA_DOC',
                    index: 'FECHA_DOC',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'EXISTENCIA',
                    index: 'EXISTENCIA',
                    width: anchoP(ancho, 4)
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;

                    asignaAtributo('lblIdTransaccionDev', datosRow.ID, 0);
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);

                    asignaAtributo('lblValorUnitario', datosRow.VALOR_UNITARIO, 0);

                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);

                    asignaAtributo('lblValorImpuesto', datosRow.VALOR_IMPUESTO, 0);
                    asignaAtributo('txtSerial', datosRow.SERIAL, 0);



                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaKardex': //por lotes
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=530&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodegaBus'));
            add_valores_a_mandar(valorAtributo('cmbIdBodegaBus'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ART', 'existencia', 'valor_unitario', 'valor_total', 'id_bodega', 'lote', 'Fecha_vencimiento'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'nombre_generico',
                    index: 'nombre_generico',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'existencia',
                    index: 'existencia',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'valor_unitario',
                    index: 'valor_unitario',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'valor_total',
                    index: 'valor_total',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'id_bodega',
                    index: 'id_bodega',
                    hidden: true
                },
                {
                    name: 'lote',
                    index: 'lote',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'fecha_vencimiento',
                    index: 'fecha_vencimiento',
                    width: anchoP(ancho, 10)
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 510,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdArticulo', datosRow.ID, 1);
                    asignaAtributo('lblArticulo', datosRow.nombre_generico, 1);
                    asignaAtributo('txtExistenciasBodega', datosRow.existencia, 0);
                    asignaAtributo('txtValorUnitario', datosRow.valor_unitario, 0);
                    asignaAtributo('txtIdBodega', datosRow.id_bodega, 1);
                    asignaAtributo('txtLote', datosRow.lote, 1);


                    //buscarSuministros('listGrillaTransaccionDocumentoHistorico');				
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaKardexArticulo':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=577&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodegaBus'));
            //   add_valores_a_mandar( valorAtributo('cmbIdBodegaBus')  );				 			 

            $('#drag' + ventanaActual.num).find("#listGrillaKardex").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ART', 'existencia', 'valor_unitario', 'valor_total', 'id_bodega', 'lote', 'Fecha_vencimiento'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'nombre_generico',
                    index: 'nombre_generico',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'existencia',
                    index: 'existencia',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'valor_unitario',
                    index: 'valor_unitario',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'valor_total',
                    index: 'valor_total',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'id_bodega',
                    index: 'id_bodega',
                    hidden: true
                },
                {
                    name: 'lote',
                    index: 'lote',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'fecha_vencimiento',
                    index: 'fecha_vencimiento',
                    width: anchoP(ancho, 10)
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 500,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdArticulo', datosRow.ID, 1);
                    asignaAtributo('lblArticulo', datosRow.nombre_generico, 1);
                    asignaAtributo('txtExistenciasBodega', datosRow.existencia, 0);
                    asignaAtributo('txtValorUnitario', datosRow.valor_unitario, 0);
                    asignaAtributo('txtIdBodega', datosRow.id_bodega, 1);
                    asignaAtributo('txtLote', datosRow.lote, 1);


                    //buscarSuministros('listGrillaTransaccionDocumentoHistorico');				
                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaKardex").setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaDocumentosHistoricos':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=508&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodegaBus'));
            add_valores_a_mandar(valorAtributo('cmbIdNaturalezaBus'));
            add_valores_a_mandar(valorAtributo('cmbTipoBus'));

            add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc') + ' 00:00:00');
            add_valores_a_mandar(valorAtributo('txtFechaHastaDoc') + ' 23:59:59');
            add_valores_a_mandar(valorAtributo('cmbIdEstado'));
            add_valores_a_mandar(valorAtributo('txtNumeroDoc'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_NATURALEZA', 'NATURALEZA', 'TIPO DOCUMENTO', 'NUMERO', 'USUARIO_CREA',
                    'FECHA CREA', 'USUARIO FINALIZA', 'FECHA CIERRE', 'NOMBRE_TERCERO', 'OBSERVACION', 'ESTADO', 'ID_ESTADO', 'ID_BODEGA'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 4)
                },
                {
                    name: 'ID_NATURALEZA',
                    index: 'ID_NATURALEZA',
                    hidden: true
                },
                {
                    name: 'NATURALEZA',
                    index: 'NATURALEZA',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'NOMBRE_DOCUMENTO_TIPO',
                    index: 'NOMBRE_DOCUMENTO_TIPO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'NUMERO',
                    index: 'NUMERO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'NomPersonal',
                    index: 'NomPersonal',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'FECHA_CREA',
                    index: 'FECHA_CREA',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'PCIERRA',
                    index: 'PCIERRA',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'FECHA_CIERRE',
                    index: 'FECHA_CIERRE',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'NOMBRE_TERCERO',
                    index: 'NOMBRE_TERCERO',
                    hidden: true
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ID_BODEGA',
                    index: 'ID_BODEGA',
                    hidden: true
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 240,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDoc', datosRow.ID, 1);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 1);
                    asignaAtributo('lblNaturaleza', datosRow.ID_NATURALEZA, 1);
                    asignaAtributo('lblIdBodega', datosRow.ID_BODEGA, 1);

                    buscarSuministros('listGrillaTransaccionDocumentoHistorico');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaTransaccionDocumentoHistorico':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=509&parametros=";
            /*add_valores_a_mandar( valorAtributo('txtCodDiagnosticoBus')  );	
            add_valores_a_mandar( valorAtributo('txtNomDiagnosticoBus')  );				 */
            add_valores_a_mandar(valorAtributo('lblIdDoc'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'CANTIDAD', 'VALOR_UNITARIO', 'IVA %', 'VALOR_IMPUESTO', 'NATURALEZA', 'LOTE', 'SERIAL', 'FECHA_VENCIMIENTO', 'PRECIO_VENTA', 'VLR_PROMEDIO', 'EXISTENCIA'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 30)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'VALOR_UNITARIO',
                    index: 'VALOR_UNITARIO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'IVA',
                    index: 'IVA',
                    hidden: true
                },
                {
                    name: 'VALOR_IMPUESTO',
                    index: 'VALOR_IMPUESTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'ID_NATURALEZA',
                    index: 'ID_NATURALEZA',
                    hidden: true
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'SERIAL',
                    index: 'SERIAL',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'FECHA_VENCIMIENTO',
                    index: 'FECHA_VENCIMIENTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'PRECIO_VENTA',
                    index: 'PRECIO_VENTA',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'valor_unitario_promedio',
                    index: 'valor_unitario_promedio',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'EXISTENCIA',
                    index: 'EXISTENCIA',
                    width: anchoP(ancho, 8)
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 200,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdTr', datosRow.ID, 0);
                    asignaAtributo('lblArt', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtTrCantidadAnt', datosRow.CANTIDAD, 0);
                    asignaAtributo('txtTrCantidad', datosRow.CANTIDAD, 0);
                    asignaAtributo('txtVlrTr', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('txtLoteTr', datosRow.LOTE, 0);
                    asignaAtributo('txtFechaTr', datosRow.FECHA_VENCIMIENTO, 0);
                    asignaAtributo('cmbVlrIva', datosRow.IVA, 0);
                    asignaAtributo('lblVlrImpuesto', datosRow.VALOR_IMPUESTO, 0);
                    asignaAtributo('lblExistencia', datosRow.EXISTENCIA, 0);
                    asignaAtributo('txtLoteAnt', datosRow.LOTE, 0);
                    mostrar('divVentanitaModTransaccion');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaDevoluciones':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            if (valorAtributo('cmbIdTipoDevolucion') == 'CON_ADMISION') {
                idQuery = 501;
            } else {
                idQuery = 535;
            }

            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQuery + "&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_ADMISION', 'ID_ARTICULO', 'NOMBRE', 'CANTIDAD', 'ID_NOVEDAD', 'ID_USUARIO', 'ELABORO', 'FECHA_ELABORO', 'VALOR_UNITARIO', 'iva', 'Lote', 'Serial', 'Fecha_vencimiento'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_ADMISION',
                    index: 'ID_ADMISION',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'ID_NOVEDAD',
                    index: 'ID_NOVEDAD',
                    hidden: true
                },
                {
                    name: 'ID_USUARIO',
                    index: 'ID_USUARIO',
                    hidden: true
                },
                {
                    name: 'NomPersonal',
                    index: 'NomPersonal',
                    width: anchoP(ancho, 15)
                },
                {
                    name: 'FECHA_ELABORO',
                    index: 'FECHA_ELABORO',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'VALOR_UNITARIO',
                    index: 'VALOR_UNITARIO',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'IVA',
                    index: 'IVA',
                    hidden: true
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'serial',
                    index: 'serial',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'FECHA_VENCIMIENTO',
                    index: 'FECHA_VENCIMIENTO',
                    hidden: true
                },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 180,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    // idArticulo=datosRow.ID_ARTICULO;	
                    // traerVentanitaArticulo('txtIdArticulo','11');
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 1);
                    asignaAtributo('lblIdAdmision', datosRow.ID_ADMISION, 0);
                    asignaAtributo('lblIdDevolucion', datosRow.ID, 0);
                    asignaAtributo('lblValorUnitario', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('lblIva', '0', 0);
                    asignaAtributo('lblIdLote', datosRow.LOTE, 0);
                    asignaAtributo('txtFV', datosRow.FECHA_VENCIMIENTO, 0);
                    asignaAtributo('txtSerial', datosRow.serial, 0);
                    /* asignaAtributo('cmbIdLote',datosRow.LOTE,0);	 */
                    $("#txtIdArticulo").focus();
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;
        case 'listGrillaDespachoSolicitudes':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            if (valorAtributo('cmbIdGastos') == 'S') {
                qry = 637;
            } else {
                qry = 494;
            }

            valores_a_mandar = valores_a_mandar + "?idQuery=" + qry + "&parametros=";

            add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_ADMISION', 'ID_ARTICULO', 'NOMBRE_GENERICO', 'CANTIDAD', 'OBSERVACION', 'ID_ESTADO', 'ID_ELABORO', 'ELABORO', 'FECHA_ELABORO'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_ADMISION',
                    index: 'ID_ADMISION',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ID_ELABORO',
                    index: 'ID_ELABORO',
                    hidden: true
                },
                {
                    name: 'ELABORO',
                    index: 'ELABORO',
                    width: anchoP(ancho, 15)
                },
                {
                    name: 'FECHA_ELABORO',
                    index: 'FECHA_ELABORO',
                    width: anchoP(ancho, 15)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    idArticulo = datosRow.ID_ARTICULO;
                    traerVentanitaArticulo('txtIdArticulo', '11');
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);
                    asignaAtributo('lblIdAdmision', datosRow.ID_ADMISION, 0);
                    asignaAtributo('lblIdSolicitud', datosRow.ID, 0);
                    $("#txtIdArticulo").focus();

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaDespachoSGastos':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=610&parametros=";
            //add_valores_a_mandar( valorAtributo('lblIdAdmisionAgen')  );	

            $('#drag' + ventanaActual.num).find("#listGrillaDespachoSolicitudes").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'id_admision', 'ID_ARTICULO', 'NOMBRE_GENERICO', 'CANTIDAD', 'OBSERVACION', 'ID_ESTADO', 'ID_ELABORO', 'ELABORO', 'FECHA_ELABORO'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'id_admision',
                    index: 'id_admision',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ID_ELABORO',
                    index: 'ID_ELABORO',
                    hidden: true
                },
                {
                    name: 'ELABORO',
                    index: 'ELABORO',
                    width: anchoP(ancho, 15)
                },
                {
                    name: 'FECHA_ELABORO',
                    index: 'FECHA_ELABORO',
                    width: anchoP(ancho, 15)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaDespachoSolicitudes').getRowData(rowid);
                    estad = 0;
                    idArticulo = datosRow.ID_ARTICULO;
                    traerVentanitaArticulo('txtIdArticulo', '11');
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);
                    asignaAtributo('lblIdAdmision', datosRow.ID_ADMISION, 0);
                    asignaAtributo('lblIdSolicitud', datosRow.ID, 0);
                    $("#txtIdArticulo").focus();

                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaDespachoSolicitudes").setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaPlantillaCanasta':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=446&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodPlantilla'));
            add_valores_a_mandar(valorAtributo('txtNomPlantilla'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            $('#drag' + ventanaActual.num).find("#listGrillaPlantillaCanasta").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'NOMBRE', 'CONTENIDO', 'ID_TIPO', 'TIPO', 'VIGENTE'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: 5
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: 5
                },
                {
                    name: 'NOMBRE',
                    index: 'NOMBRE',
                    width: 20
                },
                {
                    name: 'CONTENIDO',
                    index: 'CONTENIDO',
                    width: 30
                },
                {
                    name: 'ID_TIPO',
                    index: 'ID_TIPO',
                    hidden: 5
                },
                {
                    name: 'TIPO',
                    index: 'TIPO',
                    width: 5
                },
                {
                    name: 'VIGENTE',
                    index: 'VIGENTE',
                    hidden: true,
                    width: 5
                }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPlantillaCanasta').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblCodigoCanasta', datosRow.ID, 1);
                    asignaAtributo('txtNombreCanasta', datosRow.NOMBRE, 0);
                    asignaAtributo('txtContenidoCanasta', datosRow.CONTENIDO, 0);
                    asignaAtributo('cmbTipoCanasta', datosRow.ID_TIPO, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);

                    buscarSuministros('listGrillaCanastaDetalle');

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;

        case 'listGrillaCanastaDetalle':

            ancho = ($('#drag' + ventanaActual.num).find("#listGrillaCanastaDetalle").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=449&parametros=";
            add_valores_a_mandar(valorAtributo('lblCodigoCanasta'));

            $('#drag' + ventanaActual.num).find("#listGrillaCanastaDetalle").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_CANASTA', 'ID_ARTICULO', 'NOMBRE_GENERICO', 'CANTIDAD', 'UNIDAD'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_CANASTA',
                    index: 'ID_CANASTA',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 70)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'ABREVIATURA',
                    index: 'ABREVIATURA',
                    width: anchoP(ancho, 5)
                }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDetalle', datosRow.ID, 1);
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('cmbCantidad', datosRow.CANTIDAD, 0);
                    asignaAtributo('lbl_Unidad', datosRow.ABREVIATURA, 0);

                    if (valorAtributo('lblIdDocumento') != '') {
                        asignaAtributo('lblIdPlantillaCan', datosRow.ID, 0);
                        asignaAtributo('lblIdCanasta', datosRow.ID_CANASTA, 0);
                        asignaAtributo('lblIdArticulo', datosRow.ID_ARTICULO, 0);
                        asignaAtributo('lblNombreArticulo', datosRow.NOMBRE_GENERICO, 0);
                        //asignaAtributo('lblCantidad',datosRow.CANTIDAD,0);
                        asignaAtributo('cmbCantConsumo', datosRow.CANTIDAD, 0);

                        mostrar('divVentanitaTraerArticuloCanasta');

                    } else alert('Debe seleccionar un documento clinico');

                },
            });

            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;
        case 'listGrillaCanastaDetalleProgramacion':
            if (valorAtributo('lblIdDoc') != '') {
                /* ancho= ($('#drag'+ventanaActual.num).find("#"+arg).width())-50; */
                ancho = 1000;
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=484&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('lblCodigoCanasta'));


                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',

                    colNames: ['Tot',
                        'ID',
                        'ID_CANASTA',
                        'ID_ARTICULO',
                        'NOMBRE_GENERICO',
                        'CANTIDAD',
                        'EXISTENCIA_BODEGA',
                        'UNIDAD',
                        'INCLUIDO'
                    ],
                    colModel: [{
                        name: 'contador',
                        index: 'contador',
                        hidden: true
                    },
                    {
                        name: 'ID',
                        index: 'ID',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'ID_CANASTA',
                        index: 'ID_CANASTA',
                        hidden: true
                    },
                    {
                        name: 'ID_ARTICULO',
                        index: 'ID_ARTICULO',
                        width: anchoP(ancho, 4)
                    },
                    {
                        name: 'NOMBRE_GENERICO',
                        index: 'NOMBRE_GENERICO',
                        width: anchoP(ancho, 60)
                    },
                    {
                        name: 'CANTIDAD',
                        index: 'CANTIDAD',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'existencia',
                        index: 'existencia',
                        width: anchoP(ancho, 6)
                    },
                    {
                        name: 'ABREVIATURA',
                        index: 'ABREVIATURA',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'INCLUIDO',
                        index: 'INCLUIDO',
                        width: anchoP(ancho, 5)
                    },
                    ],

                    //  pager: jQuery('#pagerGrilla'), 
                    height: 250,
                    width: ancho + 40,
                    onSelectRow: function (rowid) {

                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        estad = 0;
                        //	asignaAtributo('lblIdDetalle',datosRow.ID,1);
                        idArticulo = datosRow.ID_ARTICULO;
                        traerVentanitaArticulo('txtIdArticulo', '11');
                        asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                        asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);

                        $("#txtIdArticulo").focus();
                    },
                });

                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                    url: valores_a_mandar
                }).trigger('reloadGrid');
            } else alert('DEBE ANTES CREAR O SELECCIONAR DOCUMENTO INVENTARIO')
            break;

        case 'listGrillaCanastaTrasladoBodega':
            if (valorAtributo('lblIdDoc') != '') {
                /* ancho= ($('#drag'+ventanaActual.num).find("#"+arg).width())-50; */
                ancho = 1000;
                valores_a_mandar = pag;
                valores_a_mandar = valores_a_mandar + "?idQuery=484&parametros=";
                add_valores_a_mandar(valorAtributo('lblIdDoc'));
                add_valores_a_mandar(valorAtributo('lblCodigoCanasta'));

                $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                    url: valores_a_mandar,
                    datatype: 'xml',
                    mtype: 'GET',

                    colNames: ['Tot', 'ID', 'ID_CANASTA', 'ID_ARTICULO', 'NOMBRE_GENERICO', 'CANTIDAD', 'EXISTENCIA_BODEGA', 'UNIDAD', 'INCLUIDO'],
                    colModel: [{
                        name: 'contador',
                        index: 'contador',
                        hidden: true
                    },
                    {
                        name: 'ID',
                        index: 'ID',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'ID_CANASTA',
                        index: 'ID_CANASTA',
                        hidden: true
                    },
                    {
                        name: 'ID_ARTICULO',
                        index: 'ID_ARTICULO',
                        width: anchoP(ancho, 4)
                    },
                    {
                        name: 'NOMBRE_GENERICO',
                        index: 'NOMBRE_GENERICO',
                        width: anchoP(ancho, 60)
                    },
                    {
                        name: 'CANTIDAD',
                        index: 'CANTIDAD',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'existencia',
                        index: 'existencia',
                        width: anchoP(ancho, 6)
                    },
                    {
                        name: 'ABREVIATURA',
                        index: 'ABREVIATURA',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'INCLUIDO',
                        index: 'INCLUIDO',
                        width: anchoP(ancho, 5)
                    },
                    ],

                    //  pager: jQuery('#pagerGrilla'), 
                    height: 250,
                    width: ancho + 40,
                    onSelectRow: function (rowid) {

                        var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                        estad = 0;
                        //	asignaAtributo('lblIdDetalle',datosRow.ID,1);
                        idArticulo = datosRow.ID_ARTICULO;
                        traerVentanitaArticulo('txtIdArticulo', '11');
                        asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                        asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);

                        $("#txtIdArticulo").focus();
                    },
                });

                $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                    url: valores_a_mandar
                }).trigger('reloadGrid');
            } else alert('DEBE ANTES CREAR O SELECCIONAR UN DOCUMENTO')
            break;

        case 'listGrillaElemento3':
            if(valorAtributo('txtNomBus') === '' && valorAtributo('txtCodBus') === ''){
                alert('Por favor filter por nombre(palabra clave, letra etc...) o codigo del articulo'); 
                return
            }
            jQuery("#listGrillaElemento3").jqGrid("clearGridData");
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=43&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodBus'));
            add_valores_a_mandar(valorAtributo('txtNomBus'));
            add_valores_a_mandar(valorAtributo('cmbIdClaseBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'TOT', 'ID', 'NOMBRE', 'NOMBRE COMERCIAL', 'MARCA', 'REFERENCIA', 'ID FABRICANTE', 'NOMBRE FABRICANTE', 'ID GRUPO', 'ID CLASE', 'ID SUBCLASE', 'COD ARTICULO', 'DESCRIPCION',
                    'PRESENTACION', 'ID UNIDAD', 'VALOR UN', 'UNIDAD', 'PORC IVA', 'PORC IVA VENTA', 'CODIGO INVIMA', 'VENCIMIENTO INVIMA', 'CODIGO ATC', 'SW FECHA VENCIMIENTO', 'SW PROVEEDOR UNICO', 'ID ANATOFARMACOLOGICO',
                    'ID PRINCIPIO ACTIVO', 'ID FARMACOLOGICA', 'CONCENTRACION', 'FACTOR CONVERSION', 'A CLASE', 'CONCENTRACION', 'MED FORM FARMACE', 'CLASE', 'POS', 'VIGENTE', 'CLASE RIESGO', 'VIDA UTIL', 'CUM'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2) + 5
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 2) + 10
                },
                {
                    name: 'NOMBRE',
                    index: 'NOMBRE',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'NOMBRE_COMERCIAL',
                    index: 'NOMBRE_COMERCIAL',
                    hidden: true
                },
                {
                    name: 'marca',
                    index: 'marca',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'DESCRIPCION_ABREVIADA',
                    index: 'DESCRIPCION_ABREVIADA',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'ID_FABRICANTE',
                    index: 'ID_FABRICANTE',
                    hidden: true
                },
                {
                    name: 'NOMBRE_FABRICANTE',
                    index: 'NOMBRE_FABRICANTE',
                    hidden: true
                },
                {
                    name: 'ID_GRUPO',
                    index: 'ID_GRUPO',
                    hidden: true
                },
                {
                    name: 'ID_TIPO',
                    index: 'ID_TIPO',
                    hidden: true
                },
                {
                    name: 'ID_SUBTIPO',
                    index: 'ID_SUBTIPO',
                    hidden: true
                },
                {
                    name: 'COD_ARTICULO',
                    index: 'COD_ARTICULO',
                    hidden: true
                },
                {
                    name: 'DESCRIPCION',
                    index: 'DESCRIPCION',
                    hidden: true
                },
                {
                    name: 'PRESENTACION',
                    index: 'PRESENTACION',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'ID_UNIDAD',
                    index: 'ID_UNIDAD',
                    hidden: true
                },
                {
                    name: 'VALOR_UNIDAD',
                    index: 'VALOR_UNIDAD',
                    width: anchoP(ancho, 4)
                },
                {
                    name: 'ML',
                    index: 'ML',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'PORC_IVA',
                    index: 'PORC_IVA',
                    hidden: true
                },
                {
                    name: 'PORC_IVA_VENTA',
                    index: 'PORC_IVA_VENTA',
                    hidden: true
                },
                {
                    name: 'CODIGO_INVIMA',
                    index: 'CODIGO_INVIMA',
                    hidden: true
                },
                {
                    name: 'VENCIMIENTO_INVIMA',
                    index: 'VENCIMIENTO_INVIMA',
                    hidden: true
                },
                {
                    name: 'CODIGO_ATC',
                    index: 'CODIGO_ATC',
                    hidden: true
                },
                {
                    name: 'SW_FECHA_VENCIMIENTO',
                    index: 'SW_FECHA_VENCIMIENTO',
                    hidden: true
                },
                {
                    name: 'SW_PROVEEDOR_UNICO',
                    index: 'SW_PROVEEDOR_UNICO',
                    hidden: true
                },

                {
                    name: 'ID_ANATOFARMACOLOGICO',
                    index: 'ID_ANATOFARMACOLOGICO',
                    hidden: true
                },
                {
                    name: 'ID_PRINCIPIO_ACTIVO',
                    index: 'ID_PRINCIPIO_ACTIVO',
                    hidden: true
                },
                {
                    name: 'ID_FARMACOLOGICA',
                    index: 'ID_FARMACOLOGICA',
                    hidden: true
                },
                {
                    name: 'CONCENTRACION',
                    index: 'CONCENTRACION',
                    hidden: true
                },
                {
                    name: 'FACTOR_CONVERSION',
                    index: 'FACTOR_CONVERSION',
                    hidden: true
                },
                {
                    name: 'A_CLASE',
                    index: 'A_CLASE',
                    hidden: true
                },
                {
                    name: 'A_CONCENTRACION',
                    index: 'A_CONCENTRACION',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'ID_FORMA_FARMACEUTICA',
                    index: 'ID_FORMA_FARMACEUTICA',
                    hidden: true
                },
                {
                    name: 'CLASE',
                    index: 'CLASE',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'POS',
                    index: 'POS',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'VIGENTE',
                    index: 'VIGENTE',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'CLASE_RIESGO',
                    index: 'CLASE_RIESGO',
                    hidden: true
                },
                {
                    name: 'VIDA_UTIL',
                    index: 'VIDA_UTIL',
                    hidden: true
                },
                {
                    name: 'CUM',
                    index: 'CUM',
                    hidden: true
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaElemento3').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtConcentracion', datosRow.A_CONCENTRACION, 0);
                    asignaAtributo('txtIdArticulo', datosRow.ID, 1);
                    asignaAtributo('txtNombreArticulo', datosRow.NOMBRE, 0);
                    asignaAtributo('lblNomArticulo', datosRow.NOMBRE, 0);
                    asignaAtributo('cmbIdGrupo', datosRow.ID_GRUPO, 0);
                    asignaAtributo('cmbIdTipo', datosRow.ID_TIPO, 0);
                    asignaAtributo('cmbIdSubTipo', datosRow.ID_SUBTIPO, 0);
                    asignaAtributo('txtCodArticulo', datosRow.COD_ARTICULO, 0);
                    asignaAtributo('txtDescripcionCompleta', datosRow.DESCRIPCION, 0);
                    asignaAtributo('txtDescripcionAbreviada', datosRow.DESCRIPCION_ABREVIADA, 0);
                    asignaAtributo('txtIdFabricante', concatenarCodigoNombre(datosRow.ID_FABRICANTE, datosRow.NOMBRE_FABRICANTE), 0)
                    asignaAtributo('cmbIdUnidad', datosRow.ID_UNIDAD, 0);
                    asignaAtributo('txtValorUnidad', datosRow.VALOR_UNIDAD, 0);
                    asignaAtributo('cmbML', datosRow.ML, 0);
                    asignaAtributo('txtIVA', datosRow.PORC_IVA, 0);
                    asignaAtributo('cmbIVAVenta', datosRow.PORC_IVA_VENTA, 0);
                    asignaAtributo('txtInvima', datosRow.CODIGO_INVIMA, 0);
                    asignaAtributo('txtVigInvima', datosRow.VENCIMIENTO_INVIMA, 0);
                    //asignaAtributo('txtATC', datosRow.CODIGO_ATC, 0);  
                    asignaAtributo('txtATC', datosRow.CUM, 0);
                    asignaAtributo('cmbIdFechaVencimiento', datosRow.SW_FECHA_VENCIMIENTO, 0);
                    asignaAtributo('cmbIdProveedorUnico', datosRow.SW_PROVEEDOR_UNICO, 0);
                    asignaAtributo('cmbPOS', datosRow.POS, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);
                    asignaAtributo('cmbIdClaseRiesgo', datosRow.CLASE_RIESGO, 0);
                    asignaAtributo('cmbIdVidaUtil', datosRow.VIDA_UTIL, 0);
                    asignaAtributo('txtMarca', datosRow.marca, 0);
                    asignaAtributo('cmbAnatomofarmacologico', datosRow.ID_ANATOFARMACOLOGICO, 0);
                    asignaAtributo('cmbPrincipioActivo', datosRow.ID_PRINCIPIO_ACTIVO, 0);
                    asignaAtributo('cmbFormaFarmacologica', datosRow.ID_FARMACOLOGICA, 0);
                    asignaAtributo('txtoncentracion', datosRow.CONCENTRACION, 0);
                    asignaAtributo('txtFactorConv', datosRow.FACTOR_CONVERSION, 0);
                    //asignaAtributo('txtCodAtc', datosRow.atc, 0); 
                    asignaAtributo('txtCodAtc', datosRow.CODIGO_ATC, 0);
                    id_clase = trim(datosRow.A_CLASE);
                    asignaAtributo('cmbIdClase', id_clase, 0);
                    asignaAtributo('cmbPresentacion', datosRow.PRESENTACION, 0);
                    asignaAtributo('txtConcentracion', datosRow.A_CONCENTRACION, 0);
                    asignaAtributo('txtNombreComercial', datosRow.NOMBRE_COMERCIAL, 0);
                    asignaAtributo('cmbIdUnidadAdministracion', datosRow.ID_FORMA_FARMACEUTICA, 0);
                    buscarSuministrosInventarios('listGrillaViaArticulo');

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;

        case 'listGrillaViaArticulo':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=523&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdArticulo'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'VIA'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'NOMBRE',
                    index: 'NOMBRE',
                    width: anchoP(ancho, 52)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 200,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('cmbViaArticulo', datosRow.ID, 0);
                    asignaAtributo('txtIdViaArticulo', datosRow.ID, 0);

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;

        case 'listGrillaBodega':
            /*		   ancho = ($('#drag'+ventanaActual.num).find("#divDocuInv").width());	   		 
                       alto = ($('#drag'+ventanaActual.num).find("#divDocuInv").height()); */
            //ancho = 900; 		   
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=80&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodBodegaBus'));
            add_valores_a_mandar(valorAtributo('txtNomBodegaBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            //add_valores_a_mandar( IdSesion()  );			   

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'DESCRIPCION', 'ID_CENTRO_COSTO', 'CENTRO DE COSTO', 'ID_RESPONSABLE', 'RESPONSABLE', 'ESTADO', 'SW_RESTITUCION', 'AUTORIZACION_RECIBIR_COMPRAS',
                    'SW_RESTRICCION_STOCK', 'ID_BODEGA_TIPO', 'TIPO BODEGA', 'SW_CONSIGNACION', 'SW_APROVECHAMIENTO', 'SW_AFECTA_COSTO'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'B_DESCRIPCION',
                    index: 'B_DESCRIPCION',
                    width: anchoP(ancho, 35)
                },
                //{name:'ID_EMPRESA', index:'ID_EMPRESA', hidden:true }, 	
                {
                    name: 'ID_CENTRO_COSTO',
                    index: 'ID_CENTRO_COSTO',
                    hidden: true
                },
                {
                    name: 'CENTRO_COSTO',
                    index: 'CENTRO_COSTO',
                    width: anchoP(ancho, 20)
                },
                {
                    name: 'ID_RESPONSABLE',
                    index: 'ID_RESPONSABLE',
                    hidden: true
                },
                {
                    name: 'RESPONSABLE',
                    index: 'RESPONSABLE',
                    width: anchoP(ancho, 20)
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    hidden: true
                },
                {
                    name: 'SW_RESTITUCION',
                    index: 'SW_RESTITUCION',
                    hidden: true
                },
                {
                    name: 'AUTORIZACION_RECIBIR_COMPRAS',
                    index: 'AUTORIZACION_RECIBIR_COMPRAS',
                    hidden: true
                },
                {
                    name: 'SW_RESTRICCION_STOCK',
                    index: 'SW_RESTRICCION_STOCK',
                    hidden: true
                },
                {
                    name: 'ID_BODEGA_TIPO',
                    index: 'ID_BODEGA_TIPO',
                    hidden: true
                },
                {
                    name: 'BODEGA_TIPO',
                    index: 'BODEGA_TIPO',
                    width: anchoP(ancho, 15)
                },
                {
                    name: 'SW_CONSIGNACION',
                    index: 'SW_CONSIGNACION',
                    hidden: true
                },
                {
                    name: 'SW_APROVECHAMIENTO',
                    index: 'SW_APROVECHAMIENTO',
                    hidden: true
                },
                {
                    name: 'SW_AFECTA_COSTO',
                    index: 'SW_AFECTA_COSTO',
                    hidden: true
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: alto,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtId', datosRow.ID, 1);
                    asignaAtributo('txtDescripcion', datosRow.B_DESCRIPCION, 0);
                    //asignaAtributo('lblDescripcion',datosRow.B_DESCRIPCION,0);
                    //asignaAtributo('cmbIdEmpresa',datosRow.ID_EMPRESA,0);			  
                    asignaAtributo('cmbIdCentroCosto', datosRow.ID_CENTRO_COSTO, 0);
                    asignaAtributo('cmbIdIdResponsable', datosRow.ID_RESPONSABLE, 0);
                    asignaAtributo('cmbEstado', datosRow.ESTADO, 0);
                    asignaAtributo('cmbSWRestitucion', datosRow.SW_RESTITUCION, 0);
                    asignaAtributo('txtAutorizacion', datosRow.AUTORIZACION_RECIBIR_COMPRAS, 0);
                    asignaAtributo('cmbSWRestriccionStock', datosRow.SW_RESTRICCION_STOCK, 0);
                    asignaAtributo('cmbIdBodegaTipo', datosRow.ID_BODEGA_TIPO, 0);
                    asignaAtributo('cmbSWConsignacion', datosRow.SW_CONSIGNACION, 0);
                    asignaAtributo('cmbSWAprovechamiento', datosRow.SW_APROVECHAMIENTO, 0);
                    asignaAtributo('cmbSWAfectaCosto', datosRow.SW_AFECTA_COSTO, 0);
                    asignaAtributo('txtIdTipoBodega', datosRow.ID_BODEGA_TIPO, 0);
                    buscarSuministros('listBodegaSede')
                    setTimeout(() => {
                        buscarSuministros('listUsuarioBodega')
                    }, 300);

                    /* cargar documentos de la bodega */
                    if (valorAtributo('txtIdTipoBodega') === '4') {
                        $(".optica").show();
                    } else {
                        $(".optica").hide();
                    }

                    limpiarDivEditarJuan('limpCamposDocumento');
                    buscarSuministros('listGrillaDocumentosBodega');
                    if (valorAtributo('txtNaturaleza') == 'I') {
                        setTimeout("buscarSuministros('listGrillaTransaccionDocumento')", 500);
                    } else {
                        setTimeout("buscarSuministros('listGrillaTransaccionSalida')", 500);
                    }

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;

        case 'listBodegaSede':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=999" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_BODEGA', 'BODEGA', 'ID_SEDE', 'SEDE'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    hidden: true
                },
                {
                    name: 'ID_BODEGA',
                    index: 'ID_BODEGA',
                    hidden: true
                },
                {
                    name: 'BODEGA',
                    index: 'BODEGA',
                    hidden: true
                },
                {
                    name: 'ID_SEDE',
                    index: 'ID_SEDE',
                    hidden: true
                },
                {
                    name: 'SEDE',
                    index: 'SEDE',
                    width: anchoP(ancho, 20)
                },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdSede', datosRow.ID_SEDE + "-" + datosRow.SEDE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;

        case 'listUsuarioBodega':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1003" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_PERSONAL', 'ID_BODEGA', 'NOMBRE', 'ID_PROFESION', 'PROFESION'],
                colModel: [{
                    name: 'TOTAL',
                    index: 'TOTAL',
                    hidden: true
                },
                {
                    name: 'ID_PERSONAL',
                    index: 'ID_PERSONAL',
                    hidden: true
                },
                {
                    name: 'ID_BODEGA',
                    index: 'ID_BODEGA',
                    hidden: true
                },
                {
                    name: 'NOMBRE',
                    index: 'NOMBRE',
                    width: anchoP(ancho, 20)
                },
                {
                    name: 'ID_PROFESION',
                    index: 'ID_PROFESION',
                    hidden: true
                },
                {
                    name: 'PROFESION',
                    index: 'PROFESION',
                    width: anchoP(ancho, 20)
                },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdGrupo', datosRow.ID_PERSONAL, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;


        case 'listGrillaDocumentoDevolucion':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=99&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            add_valores_a_mandar('I');

            add_valores_a_mandar(valorAtributo('txtFechaDesdeDev') + ' 00:00:00');
            add_valores_a_mandar(valorAtributo('txtFechaHastaDev') + ' 23:59:59');
            add_valores_a_mandar(1);
            add_valores_a_mandar(valorAtributo('cmbIdTipoDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA_CREA', 'ID_TERCERO',
                    'TERCERO', 'NATURALEZA', 'OBSERVACION', 'ID_ESTADO', 'ESTADO'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID_DOCUMENTO_TIPO',
                    index: 'ID_DOCUMENTO_TIPO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_DOCUMENTO_TIPO',
                    index: 'NOMBRE_DOCUMENTO_TIPO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'NUMERO',
                    index: 'NUMERO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'FECHA_DOCUMENTO',
                    index: 'FECHA_DOCUMENTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'ID_TERCERO',
                    index: 'ID_TERCERO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_TERCERO',
                    index: 'NOMBRE_TERCERO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'NATURALEZA',
                    index: 'NATURALEZA',
                    hidden: true
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    width: anchoP(ancho, 20)
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: anchoP(ancho, 6)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDocDev', datosRow.ID, 1);
                    /* asignaAtributo('cmbIdTipoDocumento',datosRow.ID_DOCUMENTO_TIPO,0);
                    asignaAtributo('txtObservacion',datosRow.OBSERVACION,0);			  
                    asignaAtributo('lblIdEstado',datosRow.ID_ESTADO,0);			  						  
                    asignaAtributo('lblNaturaleza',datosRow.NATURALEZA,0);			  						  
                    asignaAtributo('txtNumero',datosRow.NUMERO,0);			  						  
                    asignaAtributo('txtFechaDocumento',datosRow.FECHA_DOCUMENTO,0);			  						  
                    asignaAtributo('txtIdTercero',datosRow.ID_TERCERO+'-'+datosRow.NOMBRE_TERCERO,0);	 */

                    /* cargar transacciones del documento */
                    limpiarDivEditarJuan('limpCamposTransaccion')
                    buscarSuministros('listGrillaTransaccionDocumentoDevolucion');

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaTransaccionDocumentoDevolucion':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=87&parametros=";

            add_valores_a_mandar(valorAtributo('lblIdDocDev'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'CANTIDAD', 'VALOR_UNITARIO', 'IVA %', 'VALOR_IMPUESTO', 'NATURALEZA', 'LOTE', 'SERIAL', 'FECHA_VENCIMIENTO', 'PRECIO_VENTA', 'EXITO', 'TIPO'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_DOCUMENTO',
                    index: 'ID_DOCUMENTO',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 30)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'VALOR_UNITARIO',
                    index: 'VALOR_UNITARIO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'IVA',
                    index: 'IVA',
                    hidden: true
                },
                {
                    name: 'VALOR_IMPUESTO',
                    index: 'VALOR_IMPUESTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'ID_NATURALEZA',
                    index: 'ID_NATURALEZA',
                    hidden: true
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'SERIAL',
                    index: 'SERIAL',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'FECHA_VENCIMIENTO',
                    index: 'FECHA_VENCIMIENTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'PRECIO_VENTA',
                    index: 'PRECIO_VENTA',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'EXITO',
                    index: 'EXITO',
                    hidden: true
                },
                {
                    name: 'medida',
                    index: 'medida',
                    hidden: true
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 150,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    /* asignaAtributo('lblIdTransaccion',datosRow.ID,1);	 */
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('lblIdLote', datosRow.LOTE, 0);
                    asignaAtributo('txtSerial', datosRow.SERIAL, 0);
                    asignaAtributo('txtCantidadTr', datosRow.CANTIDAD, 0);
                    asignaAtributo('lblValorUnitario', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('cmbMedida', datosRow.medida, 0);
                    $("#txtIdArticulo").focus();
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;

        case 'listGrillaTransaccionDocumento':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=87&parametros=";
            /*add_valores_a_mandar( valorAtributo('txtCodDiagnosticoBus')  );	
            add_valores_a_mandar( valorAtributo('txtNomDiagnosticoBus')  );				 */
            add_valores_a_mandar(valorAtributo('lblIdDoc'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'Id documento', 'Id Articulo', 'Nombre Articulo', 'Cantidad', 'Valor unitario', 'Iva %', 'Valor Impuesto', 'Naturaleza', 'Lote', 'Serial', 'Fecha Vencimiento', 'Precio venta', 'Exito', 'TIPO','id_bodega'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_DOCUMENTO',
                    index: 'ID_DOCUMENTO',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 30)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'VALOR_UNITARIO',
                    index: 'VALOR_UNITARIO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'IVA',
                    index: 'IVA',
                    hidden: true
                },
                {
                    name: 'VALOR_IMPUESTO',
                    index: 'VALOR_IMPUESTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'ID_NATURALEZA',
                    index: 'ID_NATURALEZA',
                    hidden: true
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'SERIAL',
                    index: 'SERIAL',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'FECHA_VENCIMIENTO',
                    index: 'FECHA_VENCIMIENTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'PRECIO_VENTA',
                    index: 'PRECIO_VENTA',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'EXITO',
                    index: 'EXITO',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'medida',
                    index: 'medida',
                    width: anchoP(ancho, 5)
                },
                { name: 'id_bodega', index: 'id_bodega', width: anchoP(ancho, 5),hidden:true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 900,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdTransaccion', datosRow.ID, 1);
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtLote', datosRow.LOTE, 0);
                    asignaAtributo('txtFechaVencimiento', datosRow.FECHA_VENCIMIENTO, 0);
                    asignaAtributo('txtSerial', datosRow.SERIAL, 0);
                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);

                    asignaAtributo('txtValorU', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('cmbIva', datosRow.IVA, 0);

                    asignaAtributo('txtPrecioVenta', datosRow.PRECIO_VENTA, 0);
                    asignaAtributo('cmbMedida', datosRow.medida, 0);

                    $("#txtIdArticulo").focus();
                    calcularImpuesto('lblValorImpuesto')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaTransaccionSalida':
            //ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width())-50;
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=96&parametros=";
            /*add_valores_a_mandar( valorAtributo('txtCodDiagnosticoBus')  );	
            add_valores_a_mandar( valorAtributo('txtNomDiagnosticoBus')  );				 */
            add_valores_a_mandar(valorAtributo('lblIdDoc'));
            $('#drag' + ventanaActual.num).find("#listGrillaTransaccionDocumento").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot',
                    'ID',
                    'ID DOCUMENTO',
                    'ID ARTICULO',
                    'NOMBRE ARTICULO',
                    'CANTIDAD',
                    'VALOR UNITARIO',
                    'IVA %',
                    'VALORIMPUESTO',
                    'NATURALEZA',
                    'LOTE',
                    'FECHA VENCIMIENTO',
                    'SERIAL',
                    'PRECIO VENTA',
                    'EXITO',
                    'DESC',
                    'TIPO',
                    'id_bodega'
                ],
                colModel: [{
                    name: '#',
                    index: '#',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_DOCUMENTO',
                    index: 'ID_DOCUMENTO',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 30)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'VALOR_UNITARIO',
                    index: 'VALOR_UNITARIO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'IVA',
                    index: 'IVA',
                    hidden: true
                },
                {
                    name: 'VALOR_IMPUESTO',
                    index: 'VALOR_IMPUESTO',
                    hidden: true
                },
                {
                    name: 'ID_NATURALEZA',
                    index: 'ID_NATURALEZA',
                    hidden: true
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'fecha_vencimiento',
                    index: 'fecha_vencimiento',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'SERIAL',
                    index: 'SERIAL',
                    width: anchoP(ancho, 8),
                    hidden: true
                },
                {
                    name: 'PRECIO_VENTA',
                    index: 'PRECIO_VENTA',
                    width: anchoP(ancho, 4),
                    hidden: true
                },
                {
                    name: 'EXITO',
                    index: 'EXITO',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'porcent_descuento',
                    index: 'porcent_descuento',
                    width: anchoP(ancho, 5),
                    hidden:true
                },
                {
                    name: 'medida',
                    index: 'medida',
                    width: anchoP(ancho, 5),
                    hidden:true
                },
                {
                    name: 'id_bodega',
                    index: 'id_bodega',
                    width: anchoP(ancho, 5),
                    hidden: true
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 80,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaTransaccionDocumento').getRowData(rowid);
                    estad = 0;
                    idArticulo = datosRow.ID_ARTICULO;
                    asignaAtributo('lblIdTransaccion', datosRow.ID, 1);
                    asignaAtributo('lblMedida', datosRow.medida, 1);
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);

                    //console.log("Entro a seleccionar transaccion")

                    asignaAtributo('LOTE', datosRow.LOTE, 1)
                    //if(valorAtributo('txtIdTipoBodega')== '4'){
                    asignaAtributo('txtSerial', datosRow.SERIAL, 0);
                    asignaAtributo('lblValorUnitario', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('txtVlrUnitarioAnterior', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('lblIva', datosRow.IVA, 0);
                    asignaAtributo('lblValorImpuesto', datosRow.VALOR_IMPUESTO, 0);
                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);
                    asignaAtributo('cmbIdDescuento', '0', 0);
                    //}

                    //$("#txtIdArticulo").focus();
                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaTransaccionDocumento").setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;

        case 'listGrillaDocumentosBodegaProgramacion':
            /* ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width())-50; */
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=483&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAgendaDetalle'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA_DOCUMENTO', 'ID_TERCERO',
                    'NOMBRE_TERCERO', 'NATURALEZA', 'OBSERVACION', 'ID_ESTADO', 'ESTADO'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_DOCUMENTO_TIPO',
                    index: 'ID_DOCUMENTO_TIPO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_DOCUMENTO_TIPO',
                    index: 'NOMBRE_DOCUMENTO_TIPO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'NUMERO',
                    index: 'NUMERO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'FECHA_DOCUMENTO',
                    index: 'FECHA_DOCUMENTO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'ID_TERCERO',
                    index: 'ID_TERCERO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_TERCERO',
                    index: 'NOMBRE_TERCERO',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'NATURALEZA',
                    index: 'NATURALEZA',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    width: anchoP(ancho, 20)
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: anchoP(ancho, 6)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 200,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDoc', datosRow.ID, 1);
                    asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
                    asignaAtributo('lblNaturaleza', datosRow.NATURALEZA, 0);
                    asignaAtributo('txtNumero', datosRow.NUMERO, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);

                    /* cargar transacciones del documento */
                    limpiarDivEditarJuan('limpCamposTransaccion')
                    if (valorAtributo('txtNaturaleza') == 'I') {
                        buscarSuministros('listGrillaTransaccionDocumento');
                    } else {
                        buscarSuministros('listGrillaTransaccionSalida');
                    }

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;

        case 'listGrillaDocumentosTrasladoBodega':
            if(valorAtributo('cmbIdBodega') === ''){
                alert('Seleccione una bodega de origen para realizar la busqueda de documentos');
                return;
            }
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=495&parametros=";
            add_valores_a_mandar(valorAtributo('txtFechaDocumentoDesde'));
            add_valores_a_mandar(valorAtributo('txtFechaDocumentoHasta'));
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'ID_TERCERO',
                    'BODEGA', 'NATURALEZA', 'OBSERVACION', 'ID_ESTADO', 'FECHA_CREA', 'FECHA_CIERRE', 'ESTADO'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID_DOCUMENTO_TIPO',
                    index: 'ID_DOCUMENTO_TIPO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_DOCUMENTO_TIPO',
                    index: 'NOMBRE_DOCUMENTO_TIPO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'NUMERO',
                    index: 'NUMERO',
                    hidden: true
                },
                {
                    name: 'ID_TERCERO',
                    index: 'ID_TERCERO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_TERCERO',
                    index: 'NOMBRE_TERCERO',
                    width: anchoP(ancho, 15)
                },
                {
                    name: 'NATURALEZA',
                    index: 'NATURALEZA',
                    hidden: true
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    width: anchoP(ancho, 20)
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'FECHA_CREA',
                    index: 'FECHA_CREA',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'FECHA_DOCUMENTO',
                    index: 'FECHA_DOCUMENTO',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: anchoP(ancho, 6)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 200,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDoc', datosRow.ID, 1);
                    asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
                    asignaAtributo('lblNaturaleza', datosRow.NATURALEZA, 0);
                    asignaAtributo('txtNumero', datosRow.NUMERO, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);
                    asignaAtributoCombo2('cmbIdBodega', datosRow.ID_TERCERO, datosRow.NOMBRE_TERCERO);

                    limpiarDivEditarJuan('limpCamposTransaccion')

                    buscarSuministrosInventarios('listGrillaTransaccionSalida');
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaSolicitudGastos':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=605&parametros=";
            add_valores_a_mandar(IdSesion());

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_ARTICULO', 'NOMBRE_GENERICO', 'CANTIDAD', 'OBSERVACION', 'ID_ESTADO', 'ID_ELABORO', 'ELABORO', 'FECHA_ELABORO'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    width: anchoP(ancho, 10)
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'ID_ELABORO',
                    index: 'ID_ELABORO',
                    hidden: true
                },
                {
                    name: 'ELABORO',
                    index: 'ELABORO',
                    width: anchoP(ancho, 15)
                },
                {
                    name: 'FECHA_ELABORO',
                    index: 'FECHA_ELABORO',
                    width: anchoP(ancho, 15)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdSGasto', datosRow.ID, 1);
                    asignaAtributo('txtIdArticuloSGastos', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtCantidadSGasto', datosRow.CANTIDAD, 0);
                    asignaAtributo('txtObservacionSGasto', datosRow.OBSERVACION, 0);


                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
            case 'listGrillaSolicitudFarmacia': 
            ancho= 1000;
            valores_a_mandar=pag;
            valores_a_mandar=valores_a_mandar+"?idQuery=493&parametros=";
            add_valores_a_mandar( valorAtributo('lblIdAdmision')  );				 			 
 
            $('#drag'+ventanaActual.num).find("#"+arg).jqGrid({ 
            url:valores_a_mandar, 	
            datatype: 'xml', 
            mtype: 'GET', 
            
            colNames:[ 'TOT','ID','ID ADMISION','ID ARTICULO','NOMBRE GENERICO','CANTIDAD','OBSERVACION','ID ESTADO','ID ELABORO','ELABORO','FECHA ELABORO', 'ID SOLICITUD TIPO', 'ID TRANS DEV'
             ],
            colModel :[ 
              {name:'contador', index:'contador',  width: anchoP(ancho,2)},  
              {name:'ID', index:'ID', hidden:true },			   
              {name:'ID_ADMISION', index:'ID_ADMISION', hidden:true }, 	
              {name:'ID_ARTICULO', index:'ID_ARTICULO', hidden:true  }, 					 		 
              {name:'NOMBRE_GENERICO', index:'NOMBRE_GENERICO', width: anchoP(ancho,25)}, 					 		 
              {name:'CANTIDAD', index:'CANTIDAD', width: anchoP(ancho,10)}, 	
              {name:'OBSERVACION', index:'OBSERVACION', width: anchoP(ancho,10)}, 				 
              {name:'ID_ESTADO', index:'ID_ESTADO', width: anchoP(ancho,3)}, 					 		 
              {name:'ID_ELABORO', index:'ID_ELABORO', hidden:true}, 					 		 
              {name:'ELABORO', index:'ELABORO', width: anchoP(ancho,15)}, 					 		 
              {name:'FECHA_ELABORO', index:'FECHA_ELABORO',width: anchoP(ancho,15)},
              {name:'id_solicitudes_tipo', index:'id_solicitudes_tipo',width: anchoP(ancho,5)}, 
              {name:'id_transaccion_devo', index:'id_transaccion_devo',width: anchoP(ancho,5)}, 	 						 				
                                                            
              ], 
            
          //  pager: jQuery('#pagerGrilla'), 
            height: 170, 
            width: ancho,			
            onSelectRow: function(rowid){ 
     //			 limpiarDivEditarJuan(arg); 
            var datosRow = jQuery('#drag'+ventanaActual.num).find('#'+arg).getRowData(rowid);  
             estad=0;
             asignaAtributo('lblIdSolicitud',datosRow.ID,1);	
             asignaAtributo('txtIdArticuloSolicitudes',datosRow.ID_ARTICULO+'-'+datosRow.NOMBRE_GENERICO,0);
             asignaAtributo('txtCantidadSolicitud',datosRow.CANTIDAD,0);			  
             asignaAtributo('txtObservacionSolicitud',datosRow.OBSERVACION,0);	
             asignaAtributo('lblSolicitudTipo',datosRow.id_solicitudes_tipo,0);	
             asignaAtributo('lblIdTransaccionDevo',datosRow.id_transaccion_devo,0);			  						  
 
                
             },
        });  
       $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');
       
     break;

        case 'listGrillaDocumentosDevolucion':
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=504&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdAdmisionAgen'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA_DOCUMENTO', 'ID_TERCERO',
                    'NOMBRE_TERCERO', 'NATURALEZA', 'OBSERVACION', 'ID_ESTADO', 'ESTADO'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_DOCUMENTO_TIPO',
                    index: 'ID_DOCUMENTO_TIPO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_DOCUMENTO_TIPO',
                    index: 'NOMBRE_DOCUMENTO_TIPO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'NUMERO',
                    index: 'NUMERO',
                    hidden: true
                },
                {
                    name: 'FECHA_DOCUMENTO',
                    index: 'FECHA_DOCUMENTO',
                    hidden: true
                },
                {
                    name: 'ID_TERCERO',
                    index: 'ID_TERCERO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_TERCERO',
                    index: 'NOMBRE_TERCERO',
                    hidden: true
                },
                {
                    name: 'NATURALEZA',
                    index: 'NATURALEZA',
                    hidden: true
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    hidden: true
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: anchoP(ancho, 6)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 200,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDoc', datosRow.ID, 1);
                    asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
                    asignaAtributo('lblNaturaleza', datosRow.NATURALEZA, 0);
                    asignaAtributo('txtNumero', datosRow.NUMERO, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);

                    /* cargar transacciones del documento */
                    limpiarDivEditarJuan('limpCamposTransaccion')
                    if (valorAtributo('txtNaturaleza') == 'I') {
                        buscarSuministros('listGrillaTransaccionDocumento');
                    } else {
                        buscarSuministros('listGrillaTransaccionSalida');
                    }

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaElementosBodegaConsumo':
            ancho = 980;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=507&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'NOMBRE', 'PRECIO_VENTA', 'PORC_IVA_VENTA', 'LOTE', 'EXISTENCIAS'],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'NOMBRE',
                    index: 'NOMBRE',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'PRECIO_VENTA',
                    index: 'PRECIO_VENTA',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'PORC_IVA_VENTA',
                    index: 'PORC_IVA_VENTA',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 2)
                },
                {
                    name: 'EXISTENCIAS',
                    index: 'EXISTENCIAS',
                    width: anchoP(ancho, 2)
                },

                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 170,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtIdArticulo', datosRow.ID + '-' + datosRow.NOMBRE, 0);
                    asignaAtributo('lblValorUnitario', datosRow.PRECIO_VENTA, 0);
                    asignaAtributo('lblIva', datosRow.PORC_IVA_VENTA, 0);
                    asignaAtributo('cmbIdLote', datosRow.LOTE, 0);
                    asignaAtributo('lblExistencias', datosRow.EXISTENCIAS, 0);
                    asignaAtributo('txtCantidad', datosRow.EXISTENCIAS, 0);


                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaArticulosBodegaConsumo':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=515&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'CANTIDAD DESPACHO', 'VALOR_UNITARIO', 'IVA %', 'VALORIMPUESTO', 'NATURALEZA',
                    'LOTE', 'SERIAL', 'PRECIO_VENTA', 'EXITO', 'FACTOR_CONVERSION', 'CONSUMO', 'FORMA_FARMACEUTICA', 'FECHA DESPACHO', 'EXISTENCIA BODEGA'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    hidden: true
                },
                {
                    name: 'ID',
                    index: 'ID',
                    hidden: true
                },
                {
                    name: 'ID_DOCUMENTO',
                    index: 'ID_DOCUMENTO',
                    hidden: true
                },
                {
                    name: 'ID_ARTICULO',
                    index: 'ID_ARTICULO',
                    width: anchoP(ancho, 3)
                },
                {
                    name: 'NOMBRE_GENERICO',
                    index: 'NOMBRE_GENERICO',
                    width: anchoP(ancho, 30)
                },
                {
                    name: 'CANTIDAD',
                    index: 'CANTIDAD',
                    width: anchoP(ancho, 4)
                },
                {
                    name: 'VALOR_UNITARIO',
                    index: 'VALOR_UNITARIO',
                    hidden: true
                },
                {
                    name: 'IVA',
                    index: 'IVA',
                    hidden: true
                },
                {
                    name: 'VALOR_IMPUESTO',
                    index: 'VALOR_IMPUESTO',
                    hidden: true
                },
                {
                    name: 'ID_NATURALEZA',
                    index: 'ID_NATURALEZA',
                    hidden: true
                },
                {
                    name: 'LOTE',
                    index: 'LOTE',
                    width: anchoP(ancho, 7)
                },
                {
                    name: 'SERIAL',
                    index: 'SERIAL',
                    width: anchoP(ancho, 5)
                },
                {
                    name: 'PRECIO_VENTA',
                    index: 'PRECIO_VENTA',
                    hidden: true
                },
                {
                    name: 'EXITO',
                    index: 'EXITO',
                    hidden: true
                },
                {
                    name: 'FACTOR_CONVERSION',
                    index: 'FACTOR_CONVERSION',
                    hidden: true
                },
                {
                    name: 'CONSUM',
                    index: 'CONSUM',
                    hidden: true
                },
                {
                    name: 'FORMA_FARMACEUTICA',
                    index: 'FORMA_FARMACEUTICA',
                    hidden: true
                },
                {
                    name: 'FECHA_DOC',
                    index: 'FECHA_DOC',
                    width: anchoP(ancho, 8)
                },
                {
                    name: 'EXISTENCIA',
                    index: 'EXISTENCIA',
                    width: anchoP(ancho, 8)
                },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 400,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    limpiaAtributo('lblCostoHG', '', 0);
                    limpiaAtributo('txtConsumo', '', 0);
                    asignaAtributo('lblIdTransaccion', datosRow.ID, 0);
                    asignaAtributo('lblIdArticulo', datosRow.ID_ARTICULO, 0);
                    asignaAtributo('lblLoteArticulo', datosRow.LOTE, 0);
                    asignaAtributo('lblNombreArticulo', datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('lblCantDespacho', datosRow.CANTIDAD, 0);
                    if (valorAtributo('band_ultimo_consumo') == '1') {

                        asignaAtributo('cmbCantConsumo', '1', 0);
                    } else {
                        asignaAtributo('cmbCantConsumo', datosRow.CANTIDAD, 0);
                    }
                    asignaAtributo('txtCostoHG', datosRow.VALOR_UNITARIO, 0);
                    /* asignaAtributo('txtConsumo',datosRow.FACTOR_CONVERSION,0); */
                    asignaAtributo('lblCantidadConsumo', datosRow.FACTOR_CONVERSION, 0);
                    asignaAtributo('lblIdUnidad', datosRow.FORMA_FARMACEUTICA, 0);
                    asignaAtributo('txtExistenciasLote', datosRow.EXISTENCIA, 0);
                    asignaAtributo('lblCantNovedad', '0', 0);
                    if (datosRow.CONSUM != '') asignaAtributo('lblCantAplicada', datosRow.CONSUM, 0);
                    else asignaAtributo('lblCantAplicada', '0', 0);

                    asignaAtributo('lblinfoConsumo', datosRow.FACTOR_CONVERSION, 0);
                    asignaAtributo('cmbNovedad', '1', 0);
                    asignaAtributo('txtBanGrilla', '2', 0);

                    /* if(valorAtributo('lblCantAplicada')>= valorAtributo('lblCantidadConsumo')) habilitar('btnTraerArticulo',0);
                    else */
                    if (valorAtributo('lblIdUnidad') != 'Unidad') $('#divAplicacion').show();
                    else $('#divAplicacion').hide();
                    habilitar('btnTraerArticulo', 1);

                    habilitar('btnTraerUltimoArticulo', 1);
                    habilitar('cmbNovedad', 0);


                    mostrar('divVentanitaTraerArticulo');

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;

        case 'listGrillaDocumentosBodegaConsumo':
            //limpiarDivEditarJuan(arg); 	
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=486&parametros=";
            add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaDoc'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID DOC', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA_DOCUMENTO', 'ID_TERCERO',
                    'NOMBRE_TERCERO', 'NATURALEZA', 'OBSERVACION', 'ID_ESTADO', 'ESTADO'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    hidden: true
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'ID_DOCUMENTO_TIPO',
                    index: 'ID_DOCUMENTO_TIPO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_DOCUMENTO_TIPO',
                    index: 'NOMBRE_DOCUMENTO_TIPO',
                    width: anchoP(ancho, 25)
                },
                {
                    name: 'NUMERO',
                    index: 'NUMERO',
                    hidden: true
                },
                {
                    name: 'FECHA_DOCUMENTO',
                    index: 'FECHA_DOCUMENTO',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'ID_TERCERO',
                    index: 'ID_TERCERO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_TERCERO',
                    index: 'NOMBRE_TERCERO',
                    hidden: true
                },
                {
                    name: 'NATURALEZA',
                    index: 'NATURALEZA',
                    hidden: true
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    hidden: true
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: anchoP(ancho, 6)
                },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 60,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDoc', datosRow.ID, 1);
                    asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
                    //asignaAtributo('lblNaturaleza',datosRow.NATURALEZA,0);			  						  
                    asignaAtributo('txtNumero', datosRow.NUMERO, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);

                    /* cargar transacciones del documento */
                    limpiarDivEditarJuan('limpCamposTransaccion')
                    if (valorAtributo('txtNaturaleza') == 'I') {
                        buscarSuministros('listGrillaTransaccionDocumento');
                    } else {
                        buscarSuministros('listGrillaTransaccionSalida');
                    }
                    /* cargar elementos de bodega de consumo en hoja de gasto */
                    if (valorAtributo('cmbIdTipoDocumento') == '21') {
                        if (valorAtributo('cmbIdBodega') != '') {
                            setTimeout("buscarSuministros('listGrillaElementosBodegaConsumo')", 400);
                        }
                    }

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;
        case 'listGrillaDocumentosBodegaConsumoInv':
            /*DEVOLUCIONES*/
            //limpiarDivEditarJuan(arg); 	
            ancho = 1000;
            valores_a_mandar = pag;
            if (valorAtributo('cmbIdTipoDevolucion') == "SIN_ADMISION" || valorAtributo('cmbIdTipoDevolucion') == "OTROS") {
                var idQ = '583';
            } else {
                var idQ = '539';
            }
            valores_a_mandar = valores_a_mandar + "?idQuery=" + idQ + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc'));
            add_valores_a_mandar(valorAtributo('txtFechaHastaDoc'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoDocumento'));

            /*  add_valores_a_mandar( valorAtributo('txtFechaDesdeDoc')  );				 			 
              add_valores_a_mandar( valorAtributo('txtFechaHastaDoc')  );				 			 		   
              add_valores_a_mandar( valorAtributo('cmbIdTipoDocumento')  );	
             */
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID DOC', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA_DOCUMENTO', 'id_pac', 'ID',
                    'PACIENTE', 'NATURALEZA', 'OBSERVACION', 'ID_ESTADO', 'ESTADO'
                ],
                colModel: [{
                    name: 'contador',
                    index: 'contador',
                    hidden: true
                },
                {
                    name: 'ID',
                    index: 'ID',
                    width: anchoP(ancho, 4)
                },
                {
                    name: 'ID_DOCUMENTO_TIPO',
                    index: 'ID_DOCUMENTO_TIPO',
                    hidden: true
                },
                {
                    name: 'NOMBRE_DOCUMENTO_TIPO',
                    index: 'NOMBRE_DOCUMENTO_TIPO',
                    width: anchoP(ancho, 12)
                },
                {
                    name: 'NUMERO',
                    index: 'NUMERO',
                    hidden: true
                },
                {
                    name: 'FECHA_DOCUMENTO',
                    index: 'FECHA_DOCUMENTO',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'id_pac',
                    index: 'id_pac',
                    hidden: true
                },
                {
                    name: 'ID_TERCERO',
                    index: 'ID_TERCERO',
                    width: anchoP(ancho, 6)
                },
                {
                    name: 'NOMBRE_TERCERO',
                    index: 'NOMBRE_TERCERO',
                    width: anchoP(ancho, 15)
                },
                {
                    name: 'NATURALEZA',
                    index: 'NATURALEZA',
                    hidden: true
                },
                {
                    name: 'OBSERVACION',
                    index: 'OBSERVACION',
                    hidden: true
                },
                {
                    name: 'ID_ESTADO',
                    index: 'ID_ESTADO',
                    hidden: true
                },
                {
                    name: 'ESTADO',
                    index: 'ESTADO',
                    width: anchoP(ancho, 6)
                },
                ],
                //  pager: jQuery('#pagerGrilla'), 
                height: 100,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDoc', datosRow.ID, 1);
                    asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
                    //asignaAtributo('lblNaturaleza',datosRow.NATURALEZA,0);			  						  
                    asignaAtributo('txtNumero', datosRow.NUMERO, 0);
                    asignaAtributo('lblid_paciente', datosRow.id_pac, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);

                    /* cargar transacciones del documento */
                    limpiarDivEditarJuan('limpCamposTransaccion')
                    if (valorAtributo('txtNaturaleza') == 'I') {
                        buscarSuministros('listGrillaTransaccionDocumento');
                    } else {
                        buscarSuministros('listGrillaTransaccionSalida');
                    }
                    /* cargar elementos de bodega de consumo en hoja de gasto */
                    if (valorAtributo('cmbIdTipoDocumento') == '21') {
                        if (valorAtributo('cmbIdBodega') != '') {
                            setTimeout("buscarSuministros('listGrillaElementosBodegaConsumo')", 400);
                        }
                    }

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');

            break;


    } //fin switch
}
var topY = 100;

function abrirVentanaConteoItem(codItem, NomItem) {
    mostrar('divSublistadoConteo')
    $('#drag' + ventanaActual.num).find('#lblCodItem').html(codItem)
    $('#drag' + ventanaActual.num).find('#lblNombreItem').html(NomItem)

    $('#drag' + ventanaActual.num).find('#txtValorConteo').val('');
    $('#drag' + ventanaActual.num).find('#txtValorConteo').focus();

    document.getElementById('divSublistadoConteo').style.top = topY;
}

function respuestaModificarCRUDArticulo(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'crearViaArticuloP':
                alert('Via agregada correctamente al articulo');
                buscarSuministrosInventarios('listGrillaElemento3');
                break;

            case 'eliminarViaArticuloP':
                alert('Via Eliminada correctamente del articulo');
                buscarSuministrosInventarios('listGrillaElemento3');
                break;

            case 'crearArticuloP':
                alert('Articulo Creado de manera correcta');
                buscarSuministrosInventarios('listGrillaElemento3');
                limpiarArticuloP();
                break;

            case 'modificaArticuloP':
                alert('Modificacion del articulo fue Correcta');
                buscarSuministrosInventarios('listGrillaElemento3');
                limpiarArticuloP();
                break;
            case 'eliminaTransaccionBodegaSalida':
                alert('Eliminado satisfactoriamente');
                buscarSuministrosBodegas('listGrillaTransaccionSalida');
            break;

            case 'modificaTransaccionBodega':
                alert('Modificado satisfactoriamente');
                buscarSuministrosBodegas('listGrillaTransaccionSalida');
                break;
        }
    }

    return;

}


function limpiarArticuloP() {
    limpiaAtributo('txtIdArticulo', 1);
    limpiaAtributo('txtNombreArticulo', 0);
    limpiaAtributo('txtNombreComercial', 0);
    limpiaAtributo('cmbIdGrupo', 0);
    limpiaAtributo('cmbIdClase', 0);
    limpiaAtributo('cmbPresentacion', 0);
    limpiaAtributo('txtConcentracion', 0);
    limpiaAtributo('txtCodArticulo', 0);
    limpiaAtributo('txtDescripcionCompleta', 0);
    limpiaAtributo('txtDescripcionAbreviada', 0);
    limpiaAtributo('txtIdFabricante', 0);
    limpiaAtributo('cmbIdUnidad', 0);
    limpiaAtributo('txtValorUnidad', 0);
    limpiaAtributo('cmbML', 0);
    limpiaAtributo('txtIVA', 0);
    limpiaAtributo('cmbIVAVenta', 0);
    limpiaAtributo('txtInvima', 0);
    limpiaAtributo('txtBarras', 0);
    limpiaAtributo('txtMarca', 0);
    limpiaAtributo('txtATC', 0);
    limpiaAtributo('cmbIdClaseRiesgo', 0);
    limpiaAtributo('txtCodAtc', 0); /**/
    limpiaAtributo('cmbIdFechaVencimiento', 0);
    limpiaAtributo('cmbIdProveedorUnico', 0);
    limpiaAtributo('cmbPOS', 0);
    limpiaAtributo('cmbVigente', 0);
    limpiaAtributo('cmbAnatomofarmacologico', 0);
    limpiaAtributo('cmbPrincipioActivo', 0);
    limpiaAtributo('cmbFormaFarmacologica', 0);
    limpiaAtributo('txtFactorConv', 0);
    limpiaAtributo('cmbViaArticulo', 0);
    limpiaAtributo('txtIdViaArticulo', 0);
    limpiaAtributo('lblNomArticulo', 0);
    limpiaAtributo('cmbIdUnidadAdministracion', 0);
    // limpiaAtributo('txtCodBus',0); 
    // limpiaAtributo('txtNomBus',0);
    // limpiaAtributo('cmbIdClaseBus',0);
}