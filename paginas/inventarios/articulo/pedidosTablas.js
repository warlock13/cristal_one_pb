function listaOrdenesPedido(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
    switch (arg) {
        case 'listGrillaOrdenesDePedido':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2270&parametros=";
            add_valores_a_mandar(valorAtributo('txtFechaDesde') + ' 00:00:00');
            add_valores_a_mandar(valorAtributo('txtFechaHasta') + ' 23:59:59');
            add_valores_a_mandar(valorAtributo('cmbIdEstado'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'TOTAL',
                    'ID PEDIDO',
                    'ID TERCERO',
                    'TERCERO',
                    'OBSERVACION',
                    'ID SEDE',
                    'NOMBRE SEDE',
                    'FECHA CREA',
                    'FECHA CIERRE',
                    'ESTADO',
                    'ID ORDEN DE COMPRA',
                    'ID USUARIO CREA',
                    'NOMBRE USUARIO CREA',
                    'ID USUARIO CIERRA',
                    'NOMBRE USUARIO CIERRA',
                    'SW RECIBIDA',
                    'OBSERVACION RECIBIDA'

                ],
                colModel: [{
                        name: 'tot',
                        index: 'tot',
                        width: anchoP(ancho, 5)
                    }, {
                        name: 'id',
                        index: 'id',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_tercero',
                        index: 'id_tercero',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'nombre_tercero',
                        index: 'nombre_tercero',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'observacion',
                        index: 'observacion',
                        width: anchoP(ancho, 5),
                        hidden:true
                    },
                    {
                        name: 'id_sede',
                        index: 'id_sede',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'nombre',
                        index: 'nombre',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'fecha_crea',
                        index: 'fecha_crea',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'fecha_cierre',
                        index: 'fecha_cierre',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'sw_estado',
                        index: 'sw_estado',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_oc',
                        index: 'id_oc',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_usuario_crea',
                        index: 'id_usuario_crea',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'nompersonal',
                        index: 'nompersonal',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_usuario_cierra',
                        index: 'id_usuario_cierra',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'nompersonalC',
                        index: 'nompersonalC',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'sw_recepcion',
                        index: 'sw_recepcion',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'observacionReci',
                        index: 'observacionReci',
                        width: anchoP(ancho, 5),
                        hidden: true
                    }
                ],
                height: 160,
                width: ancho + 40,
                onSelectRow: function(rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdOrdenPedido', datosRow.id, 1);
                    if (datosRow.sw_estado.trim() === '0') {
                        asignaAtributo('lblIdEstado', 'BORRADOR', 1);
                    }
                    if (datosRow.sw_estado.trim() === '1') {
                        asignaAtributo('lblIdEstado', 'CERRADO', 1);
                    }
                    var estado;
                    asignaAtributo('lblSwRecibidoPedidos',datosRow.sw_recepcion,0);
                    if(datosRow.sw_recepcion.trim() === 'P'){
                        estado = 'PENDIENTE';
                    }
                    if(datosRow.sw_recepcion.trim() === 'R'){
                        estado = 'RECIBIDO';
                    }
                    if(datosRow.sw_recepcion.trim() === 'C'){
                        estado = 'CANCELADO';
                    }
                    asignaAtributo('txtObservacion', datosRow.observacion, 0);
                    document.getElementById('txtObservacionRece').value =  'SU ORDEN DE PEDIDO SE ENCUENTRA EN ESTADO: ' + estado+ '\nOBSERVACION DE LA PERSONA QUIEN RECIBIO: ' + datosRow.observacionReci;
                    listaOrdenesPedido('listArticulosOrdenPedido');  
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;


        case 'listGrillaOrdenesDePedidoRecepcion':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2278&parametros=";
            add_valores_a_mandar(valorAtributo('txtFechaDesde') + ' 00:00:00');
            add_valores_a_mandar(valorAtributo('txtFechaHasta') + ' 23:59:59');

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'TOTAL',
                    'ID PEDIDO',
                    'ID TERCERO',
                    'NOMBRE TERCERO',
                    'OBSERVACION',
                    'ID SEDE',
                    'NOMBRE SEDE',
                    'FECHA CREA',
                    'FECHA CIERRE',
                    'ESTADO',
                    'ID ORDEN DE COMPRA',
                    'ID USUARIO CREA',
                    'NOMBRE USUARIO CREA',
                    'ID USUARIO CIERRA',
                    'NOMBRE USUARIO CIERRA',
                    'SW RECIBIDA',
                    'OBSERVACION RECIBIDA',
                    'SERVICIO'

                ],
                colModel: [{
                        name: 'tot',
                        index: 'tot',
                        width: anchoP(ancho, 5)
                    }, {
                        name: 'id',
                        index: 'id',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_tercero',
                        index: 'id_tercero',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'nombre_tercero',
                        index: 'nombre_tercero',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'observacion',
                        index: 'observacion',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_sede',
                        index: 'id_sede',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'nombre',
                        index: 'nombre',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'fecha_crea',
                        index: 'fecha_crea',
                        width: anchoP(ancho, 3)
                    },
                    {
                        name: 'fecha_cierre',
                        index: 'fecha_cierre',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'sw_estado',
                        index: 'sw_estado',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_oc',
                        index: 'id_oc',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_usuario_crea',
                        index: 'id_usuario_crea',
                        width: anchoP(ancho, 5),
                        hidden:true
                    },
                    {
                        name: 'nompersonal',
                        index: 'nompersonal',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_usuario_cierra',
                        index: 'id_usuario_cierra',
                        width: anchoP(ancho, 5),
                        hidden:true
                    },
                    {
                        name: 'nompersonalC',
                        index: 'nompersonalC',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'sw_recepcion',
                        index: 'sw_recepcion',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'observacionReci',
                        index: 'observacionReci',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'servicio',
                        index: 'servicio',
                        width: anchoP(ancho, 5)
                    },
                ],
                height: 160,
                width: ancho + 40,
                onSelectRow: function(rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdOrdenPedido', datosRow.id, 1);
                    if (datosRow.sw_estado.trim() === '0') {
                        asignaAtributo('lblIdEstado', 'BORRADOR', 1);
                    }
                    if (datosRow.sw_estado.trim() === '1') {
                        asignaAtributo('lblIdEstado', 'CERRADO', 1);
                    }

                    if(datosRow.sw_recepcion.trim() === 'P'){
                        asignaAtributo('lblSwRecibido',datosRow.sw_recepcion,1);
                        asignaAtributo('cmbEstadoObservacion',datosRow.sw_recepcion,0); 
                        asignaAtributo('txtObservacionRecepecionOrden',datosRow.observacionReci,0);
                    }else{
                        asignaAtributo('lblSwRecibido',datosRow.sw_recepcion,1);
                        asignaAtributo('cmbEstadoObservacion',datosRow.sw_recepcion,1); 
                        asignaAtributo('txtObservacionRecepecionOrden',datosRow.observacionReci,1);
                    }
                    listaOrdenesPedido('listArticulosOrdenPedidoRecepcion');
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;

        case 'listArticulosOrdenPedidoRecepcion':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2276&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdOrdenPedido'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'TOTAL',
                    'ID TRANSACCION',
                    'ID ARTICULO',
                    'NOMBRE COMERCIAL',
                    'OBSERVACION',
                    'CANTIDAD',
                    'ID ORDEN PEDIDO'

                ],
                colModel: [{
                        name: 'tot',
                        index: 'tot',
                        width: anchoP(ancho, 5)
                    }, {
                        name: 'id',
                        index: 'id',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_articulo',
                        index: 'id_articulo',
                        width: anchoP(ancho, 5),
                    },
                    {
                        name: 'nombre_comercial',
                        index: 'nombre_comercial',
                        width: anchoP(ancho, 5),
                    },
                    {
                        name: 'observacion',
                        index: 'observacion',
                        width: anchoP(ancho, 5),
                    },
                    {
                        name: 'cantidad',
                        index: 'cantidad',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_orden_pedido',
                        index: 'id_orden_pedido',
                        width: anchoP(ancho, 5),
                        hidden: true
                    }
                ],
                height: 160,
                width: ancho + 40,
                caption: "",
                onSelectRow: function(rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdTransaccion', datosRow.id, 1);
                    asignaAtributo('txtIdArticulo', datosRow.id_articulo + '-' + datosRow.nombre_comercial, 1);
                    asignaAtributo('txtObsArt', datosRow.observacion, 1);
                    asignaAtributo('txtCantidad', datosRow.cantidad, 1);
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            $(".ui-jqgrid-titlebar").hide();
            break;

        case 'listArticulosOrdenPedido':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2276&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdOrdenPedido'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'TOTAL',
                    'ID TRANSACCION',
                    'ID ARTIUCLO',
                    'NOMBRE COMERCIAL',
                    'OBSERVACION',
                    'CANTIDAD',
                    'ID ORDEN PEDIDO'

                ],
                colModel: [{
                        name: 'tot',
                        index: 'tot',
                        width: anchoP(ancho, 5)
                    }, {
                        name: 'id',
                        index: 'id',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_articulo',
                        index: 'id_articulo',
                        width: anchoP(ancho, 5),
                    },
                    {
                        name: 'nombre_comercial',
                        index: 'nombre_comercial',
                        width: anchoP(ancho, 5),
                    },
                    {
                        name: 'observacion',
                        index: 'observacion',
                        width: anchoP(ancho, 5),
                    },
                    {
                        name: 'cantidad',
                        index: 'cantidad',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_orden_pedido',
                        index: 'id_orden_pedido',
                        width: anchoP(ancho, 5),
                        hidden: true
                    }
                ],
                height: 160,
                width: ancho + 40,
                onSelectRow: function(rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdTransaccion', datosRow.id, 0);
                    asignaAtributo('txtIdArticulo', datosRow.id_articulo + '-' + datosRow.nombre_comercial, 0);
                    asignaAtributo('txtObsArt', datosRow.observacion, 0);
                    asignaAtributo('txtCantidad', datosRow.cantidad, 0);
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;
    }
}