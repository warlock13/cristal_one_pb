function limpiarRecepcion() {
    document.getElementById('txtTamLote').value = '';
    document.getElementById('txtTamMuestra').value = '';
    document.getElementById('txtTamProductosCriticos').value = '';
    document.getElementById('txtTamProductosDefMayores').value = '';
    document.getElementById('txtProductosDefectosMenores').value = '';
    document.getElementById('txtCantidadProductosRechazados').value = '';
    document.getElementById('txtCondicionesAlmacenamiento').value = '';
    document.getElementById('txtTempCadenaFrio').value = '';
    document.getElementById('txtCodigoInspeccion').value = '';
}


function modificarRecepcionTecnica(arg) {
    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {
        case 'crearRecepcionTecnica':
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2310&parametros=";
            add_valores_a_mandar(localStorage.getItem("lblIdArticuloRecepcion"));
            add_valores_a_mandar(valorAtributo('txtTamLote'));
            add_valores_a_mandar(valorAtributo('txtTamMuestra'));
            add_valores_a_mandar(valorAtributo('txtTamProductosCriticos'));
            add_valores_a_mandar(valorAtributo('txtTamProductosDefMayores'));
            add_valores_a_mandar(valorAtributo('txtProductosDefectosMenores'));
            add_valores_a_mandar(valorAtributo('txtCantidadProductosRechazados'));
            add_valores_a_mandar(valorAtributo('txtCondicionesAlmacenamiento'));
            add_valores_a_mandar(valorAtributo('txtTempCadenaFrio'));
            add_valores_a_mandar(valorAtributo('txtCodigoInspeccion'));
            ajaxModificar();

            break;
        
            case 'modificacionRecepcionTecnica':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2411&parametros=";
                add_valores_a_mandar(valorAtributo('txtTamLote'));
                add_valores_a_mandar(valorAtributo('txtTamMuestra'));
                add_valores_a_mandar(valorAtributo('txtTamProductosCriticos'));
                add_valores_a_mandar(valorAtributo('txtTamProductosDefMayores'));
                add_valores_a_mandar(valorAtributo('txtProductosDefectosMenores'));
                add_valores_a_mandar(valorAtributo('txtCantidadProductosRechazados'));
                add_valores_a_mandar(valorAtributo('txtCondicionesAlmacenamiento'));
                add_valores_a_mandar(valorAtributo('txtTempCadenaFrio'));
                add_valores_a_mandar(valorAtributo('txtCodigoInspeccion'));
                add_valores_a_mandar(localStorage.getItem("lblIdArticuloRecepcion"));
                ajaxModificar();
            break;

            case 'actualizarRemision':
                if(valorAtributo('txtIdFacturaRemision') === ''){
                    alert('El numero de factura no puede estar vacio');
                    return
                }
                if(valorAtributo('txtFechaLegalizacion') === ''){
                    alert('La fecha de factura no puede estar vacia');
                    return
                }
                valores_a_mandar = 'accion=' + 'actualizarRemision';
                valores_a_mandar = valores_a_mandar + "&idQuery=2315&parametros=";
                add_valores_a_mandar(valorAtributo('txtIdFacturaRemision').trim());
                add_valores_a_mandar(valorAtributo('txtFechaLegalizacion').trim());
                add_valores_a_mandar(valorAtributo('lblIdDocumentoLegalizacion').trim());
                ajaxModificar();
            break;

            // case 'modificarActualizacionDeRemision':
            //     if(valorAtributo('txtIdFacturaRemision') === ''){
            //         alert('El numero de factura no puede estar vacio');
            //         return
            //     }
            //     if(valorAtributo('txtFechaLegalizacion') === ''){
            //         alert('La fecha de factura no puede estar vacia');
            //         return
            //     } 
            //     valores_a_mandar = 'accion=' + 'actualizarRemision';
            //     valores_a_mandar = valores_a_mandar + "&idQuery=2315&parametros=";
            //     add_valores_a_mandar(valorAtributo('txtIdFacturaRemision').trim());
            //     add_valores_a_mandar(valorAtributo('txtFechaLegalizacion').trim());
            //     add_valores_a_mandar(valorAtributo('lblIdDocumentoLegalizacion').trim());
            //     ajaxModificar();
            // break;
    }
}


function limpiarLegalizacionRemision(){
    asignaAtributo('txtIdFacturaRemision','',0);
    asignaAtributo('txtFechaLegalizacion','',0);
}


function respuestaModificarRecepcionTecnica(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'crearRecepcionTecnica': 
                alert('Recepcion tecnica Creada Exitosamente !!!');
            break;

            case 'modificacionRecepcionTecnica':
                alert('Modificacion Exitosa !!!');
                break;
            case 'actualizarRemision':
                alert('Datos de la remision actualizados de manera correcta');
                break;

            // case 'modificarActualizacionDeRemision':
            //     alert('Datos de la actulizacion de remision  modificados de manera correcta');
            //     break;
        }
    }
}