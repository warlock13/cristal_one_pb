function savePdf() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    source = $('#customers')[0];
    specialElementHandlers = {
        '#bypassme': function (element, renderer) {
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 60,
        width: 700
    };

    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        pdf.save('Test.pdf');
    }, margins);
}


function imprimirOrdenPedidoRecibida(arg, filename, align,title,desc,aligndesc){
    //El parametro que viene de tableID no se esta tomando en cuenta, pero deberia ser gview_listCasosPQRD
    var table = document.getElementById('gview_'+arg);
    var headers = document.querySelector('table[aria-labelledby="gbox_'+arg+'"'+']')
    var body = document.querySelector('table[id="'+arg+'"]')
    headers.setAttribute("border", "1");
    body.setAttribute("border", "1");

    //Adicionando Titulo
    var caption = document.createElement('h3');
    caption.setAttribute('id','title');
    caption.setAttribute('align',align); 
    caption.style.cssText = 'display:none;font-family:Arial Unicode MS';
    var t = document.createTextNode(title);
    caption.appendChild(t);
    headers.appendChild(caption);

    var descripcion = document.createElement('p');
    descripcion.setAttribute('id','desc');
    descripcion.setAttribute('align',aligndesc); 
    descripcion.style.cssText = 'display:none;font-family: Arial Unicode MS;';
    var texto = document.createTextNode(desc);
    descripcion.appendChild(texto);
    headers.appendChild(descripcion);
    //Adicionando Descripcion del reporte 
    //borra los encabezados que estan en hidden:true en jqgrid
    //jQuery(table).find('th[style*="display: none"]').remove();
    //borra las columnas que estan en hidden:true en jqgrid
    //var todo_td  = jQuery(table).find('td:hidden').remove();
    //jQuery(table).find('#load_listCasosPQRD').remove();
    jQuery(table).find('.hidden').remove();
    jQuery(table).find("img").remove();
    jQuery(table).find(".blank").remove();

    table = table.innerHTML;
    var dataType = 'application/vnd.ms-excel;charset=utf-8';
    var blob = new Blob(['\ufeff', table], {
        type: dataType
    });
    saveAs(blob, filename);
    setTimeout(function(){listaOrdenesPedido('listArticulosOrdenPedidoRecepcion');},1000);
    let obj1 = document.getElementById('title'); 
    obj1.remove();
    let obj = document.getElementById('desc'); 
    obj.remove();
}


function modificarCRUDPedidos(arg) {
    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {


        case 'agregarInfoRecibidoOrden':
            if(valorAtributo('lblIdOrdenPedido') === ''){
                alert('Debe seleccionar una orden de pedido'); 
                return
            }
            if(valorAtributo('lblSwRecibido') !== 'P'){
                alert('Ya  se ha realizado una recepcion para este pedido, no puedes modificar'); 
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2282&parametros=";
            add_valores_a_mandar(valorAtributo('txtObservacionRecepecionOrden'));
            add_valores_a_mandar(valorAtributo('cmbEstadoObservacion')); 
            add_valores_a_mandar(valorAtributo('lblIdOrdenPedido'));
            ajaxModificar();  
        break; 

        case 'crearPedido':			
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2269&parametros=";
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(document.getElementById('lblIdSede').textContent); 
            add_valores_a_mandar(IdSesion());
            ajaxModificar();  
            break; 

        case 'modificarPedido':
            if(valorAtributo('lblIdEstado') === 'CERRADO'){
                alert('No puede modifcar la orden de pedido interna puesto que ya ha sido cerrada !'); 
                return
            }
            if(valorAtributo('lblIdOrdenPedido') === ''){
                alert('Por favor selecciona una Orden de pedido interna'); 
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2271&parametros=";
            add_valores_a_mandar(valorAtributo('txtObservacion'));
            add_valores_a_mandar(valorAtributo('lblIdOrdenPedido'));
            ajaxModificar();  
            break; 
        
        case 'cerrarPedido':
            if(valorAtributo('lblIdEstado') === 'CERRADO'){
                alert('No puede modifcar la orden de pedido interna puesto que ha sido cerrada anteriormente'); 
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2272&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdOrdenPedido'));
            ajaxModificar();  
            break; 

        
        case 'adicionarArticuloOrdenPedido':
            if(valorAtributo('lblIdEstado') === 'CERRADO'){
                alert('No puede modifcar la orden de pedido interna puesto que ya ha sido cerrada !'); 
                return
            }
            if(valorAtributo('lblIdOrdenPedido') === ''){
                alert('Seleccione una Orden de pedido'); 
                return
            }
            if(valorAtributo('txtIdArticulo') == ''){
                alert('Seleccione un producto'); 
                return
            }
            if(valorAtributo('txtCantidad') === ''){
                alert('La cantidad del articulo es obligatoria');
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2273&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
            add_valores_a_mandar(valorAtributo('txtObsArt'));
            add_valores_a_mandar(valorAtributo('txtCantidad')); 
            add_valores_a_mandar(valorAtributo('lblIdOrdenPedido'));
            ajaxModificar();
        break; 
        
        case 'modificarArticuloOrdenPedido':
            if(valorAtributo('lblIdEstado') === 'CERRADO'){
                alert('No puede modifcar la orden de pedido interna puesto que ya ha sido cerrada !'); 
                return
            }
            if(valorAtributo('lblIdOrdenPedido') === ''){
                alert('Seleccione una Orden de pedido'); 
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2274&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
            add_valores_a_mandar(valorAtributo('txtObsArt'));
            add_valores_a_mandar(valorAtributo('txtCantidad')); 
            add_valores_a_mandar(valorAtributo('lblIdOrdenPedido'));
            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
            ajaxModificar();
        break;

        case 'eliminarArticuloOrdenPedido':
            if(valorAtributo('lblIdEstado') === 'CERRADO'){
                alert('No puede modifcar la orden de pedido interna puesto que ya ha sido cerrada !'); 
                return
            }
            if(valorAtributo('lblIdOrdenPedido') === ''){
                alert('Seleccione una Orden de pedido'); 
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2277&parametros=";
            add_valores_a_mandar(valorAtributo('lblIdTransaccion'));
            ajaxModificar();
            break;
    }
}


function respuestaModificarCRUDPedidos(arg, xmlraiz){
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'crearPedido':
                alert('Pedido creado Correctamente !!!'); 
                setTimeout(() => {
                    listaOrdenesPedido('listGrillaOrdenesDePedido');  
                }, 100); 
            break; 
            case 'modificarPedido':
                alert('Pedido Modificado correctamente !!!'); 
                setTimeout(() => {
                    listaOrdenesPedido('listGrillaOrdenesDePedido');  
                }, 100); 
                limpiarCamposPedido('limpiarPedido');
            break; 
            case 'cerrarPedido': 
                alert('Orden de Pedido Cerrada Exitosamente'); 
                limpiarCamposPedido('limpiarPedido');
                setTimeout(() => {
                    listaOrdenesPedido('listGrillaOrdenesDePedido');  
                }, 100); 
            break;

            case 'adicionarArticuloOrdenPedido':
                alert('Producto adicionado correctamente !!!');
                setTimeout(() => {
                    listaOrdenesPedido('listArticulosOrdenPedido');
                }, 100);
                setTimeout(() => {
                    limpiarCamposPedido('limpiarArticuloOrdenPedido')
                }, 150);
                
            break;

            case 'modificarArticuloOrdenPedido':
                alert('Producto modificado correctamente !!!');
                setTimeout(() => {
                    listaOrdenesPedido('listArticulosOrdenPedido');
                }, 100);
                setTimeout(() => {
                    limpiarCamposPedido('limpiarArticuloOrdenPedido')
                }, 150);
            break; 
            case 'eliminarArticuloOrdenPedido':
                alert('Producto eliminado correctamente !!!');
                setTimeout(() => {
                    listaOrdenesPedido('listArticulosOrdenPedido');
                }, 100);
                setTimeout(() => {
                    limpiarCamposPedido('limpiarArticuloOrdenPedido')
                }, 150);
            break;

            case 'agregarInfoRecibidoOrden':
                alert('Informacion actualizada correctamente !!!'); 
                setTimeout(() => {
                    listaOrdenesPedido('listGrillaOrdenesDePedidoRecepcion');
                }, 100);
                setTimeout(() => {
                    limpiarCamposPedido('agregarInfoRecibidoOrden')
                }, 150);
            break; 
        }
    }
}

function limpiarCamposPedido(arg){
    switch (arg) {
        case 'limpiarPedido':
            asignaAtributo('lblIdOrdenPedido','',0);
            asignaAtributo('lblIdEstado','',0);
            asignaAtributo('txtObservacion','',0);
            break;

        case 'limpiarArticuloOrdenPedido':
            asignaAtributo('lblIdTransaccion','',0); 
            asignaAtributo('txtIdArticulo','',0); 
            asignaAtributo('txtObsArt','',0);
            asignaAtributo('txtCantidad','',0)
            break;
        case 'agregarInfoRecibidoOrden':
            asignaAtributo('txtObservacionRecepecionOrden','',0);
            asignaAtributo('cmbEstadoObservacion','',0);
        break; 
    }
}