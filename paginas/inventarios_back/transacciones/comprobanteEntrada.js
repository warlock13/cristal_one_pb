function impresionFacturaOrdenDeCompra() {
	var dimension = 'width=1150,height=752,scrollbars=NO,statusbar=NO,left=150,top=90';
	var idDoc = document.getElementById('lblIdDoc').innerHTML;
	if (idDoc != '') {
		var idReporte = "DOC_FARMACIA";
		var nuevaURL = "ireports/inventario/generaInventario.jsp?reporte=" + idReporte + "&idDoc=" + idDoc;
		window.open(nuevaURL, '', dimension);
	} else { alert('Seleccione un producto o servicio el cual tenga el encabezado FACTURADO (F)'); }
}

function impresionTodasFacturaOrdenDeCompra(){
    var dimension = 'width=1150,height=752,scrollbars=NO,statusbar=NO,left=150,top=90';
    const unique = (value, index, self) => {
        return self.indexOf(value) === index
    }
    let documentos = []
    let valores = $('td[aria-describedby="listTransaccionesOrdenEntrada_id_fac_entrada"]');
    for(let i=0; i<valores.length; i++){
        documentos.push(valores[i].title.trim());
    }
    let documentosUnicos = documentos.filter(unique);
    for(let i=0; i<documentosUnicos.length; i++){
        var idDoc = documentosUnicos[i];
        if (idDoc != '') {
            var idReporte = "DOC_FARMACIA";
            var nuevaURL = "ireports/inventario/generaInventario.jsp?reporte=" + idReporte + "&idDoc=" + idDoc;
            window.open(nuevaURL, '', dimension);
        } else { alert('Seleccione un producto o servicio el cual tenga el encabezado FACTURADO (F)'); }
    }
    return
}


async function compobanteEntradaAsincrono(proceso,arg){
    const result = await cargarProcesoAsincrono(proceso,arg);
    console.info(result)
}

function cargarProcesoAsincrono(proceso,arg){
    return new Promise((resolve,reject) => {
        if(proceso === 'crearGrillaComprobanteEntrada'){
            setTimeout(() => {
                crearGrillaComprobanteEntrada(arg);
            }, 100);
        } 
    });
}

function modificarCRUDOrdenEntrega(arg){
    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {
        case 'RelacionarTransConFac':
            if(valorAtributo('lblInventario') === 'F'){
                alert('No puede realizar mas acciones en la orden. La orden ha sido cargada completamente al inventario'); 
                return
            }
            if(valorAtributo('txtIdOC') === ''){
                alert('Debe seleccionar una orden de compra o servicio'); 
                return 
            }
            if(valorAtributo('lblClase') === 'OS'){
                if(valorAtributo('cmbIdBodega') !== ''){
                    alert('El campo bodega debe ser vacio'); 
                    return 
                }
                if(valorAtributo('txtFactura') === ''){
                    alert('Digite numero de Factura');
                    return
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2283&parametros=";
                add_valores_a_mandar('5');
                add_valores_a_mandar(valorAtributo('lblIdTercero'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('txtIdOC'));
                if(valorAtributo('lblClase') === 'OS'){
                    console.log('entrando');
                    add_valores_a_mandar('0');
                    add_valores_a_mandar(valorAtributo('txtFactura'));
                    add_valores_a_mandar(valorAtributo('txtIdOC'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('txtIdOC'));
                }else{
                    add_valores_a_mandar(valorAtributo('cmbIdBodega'));
                    add_valores_a_mandar(valorAtributo('txtFactura'));
                    add_valores_a_mandar(valorAtributo('txtIdOC'));
                    add_valores_a_mandar(IdSesion());
                    add_valores_a_mandar(valorAtributo('txtIdOC'));
                }
                ajaxModificar();
            }else{
                if(valorAtributo('cmbIdBodega') === ''){
                    alert('Seleccione una bodega'); 
                    return 
                }
                if(valorAtributo('txtFactura') === ''){
                    alert('Digite numero de Factura');
                    return
                }
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2265&parametros=";
                add_valores_a_mandar('5');
                add_valores_a_mandar(valorAtributo('lblIdTercero'));
                add_valores_a_mandar(IdSesion());
                add_valores_a_mandar(valorAtributo('txtIdOC'));
                add_valores_a_mandar(valorAtributo('cmbIdBodega'));
                add_valores_a_mandar(valorAtributo('txtFactura'));
                //********************************************** */
                add_valores_a_mandar(valorAtributo('txtIdOC'));
                add_valores_a_mandar(valorAtributo('txtIdOC'));
                ///********************************************* */
                add_valores_a_mandar(IdSesion());
                //********************************************* */
                add_valores_a_mandar(valorAtributo('txtIdOC'));
                add_valores_a_mandar(valorAtributo('txtIdOC'));
                add_valores_a_mandar(valorAtributo('txtIdOC'));
                ajaxModificar();
            }
            break; 



        case 'OrdenCompraOrdenEntrega':
            if(verificarCamposOrdenCompra(arg)){
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2241&parametros=";
                add_valores_a_mandar(valorAtributo("txtObservacion"));
                ajaxModificar();
            }
        break;

        case 'revertirTransaccionOrdenCompra': 
            if(valorAtributo('lblInventario') === 'F'){
                alert('No puede realizar mas acciones en la orden. La orden ha sido cargada completamente al inventario'); 
                return
            }
            if(valorAtributo('lblSwFactura') === 'F'){
                alert('NO PUEDE REVERTIR LA ACCION LOS ARTICULOS SE ENCUENTRAN EN INVENTARIO (F)');
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2264&parametros=";
            // lblIdTransaccion
            add_valores_a_mandar(document.getElementById('lblIdArticuloBorrarTrans').textContent)
            add_valores_a_mandar(document.getElementById('lblIdArticuloBorrarTrans').textContent);

            add_valores_a_mandar(document.getElementById('lblCantidadTransaccionRevertir').textContent);
            add_valores_a_mandar(document.getElementById('lblIdArticuloArevertir').textContent);

            
            ajaxModificar();
        break;

        case 'asignarAtributos':
            if(valorAtributo('lblInventario') === 'F'){
                alert('No puede realizar mas acciones en la orden. La orden ha sido cargada completamente al inventario'); 
                return
            }
            if(document.getElementById('txtIdArticuloOC').textContent === ''){
                alert('POR FAVOR SELECCIONE UN PRODUCTO');
                return
            }
            if(parseInt(valorAtributo("txtCantidad")) > parseInt(valorAtributo("lblCantidad_piv"))){
                alert('NO PUEDE SELECCIONAR UNA CANTIDAD MAYOR A LA CANTIDAD TOTAL');
                return
            } 
            if(parseInt(valorAtributo('lblTotalEntregado')) == parseInt(valorAtributo('lblCantidad_piv'))){
                alert('DE ESTE ARTICULO NO PUEDES REALIZAR MAS ENTRADAS');
                return
            }

            if((parseInt(valorAtributo('lblTotalEntregado')) + parseInt(valorAtributo('txtCantidad'))) > parseInt(valorAtributo("lblCantidad_piv"))){
                alert('LOS ARTICULOS QUE DESEA ENTRAR SUPERAN LA CANTIDAD TOTAL DE LA ORDEN DE COMPRA'); 
                return
            }
            valores_a_mandar = 'accion=' + arg;
            valores_a_mandar = valores_a_mandar + "&idQuery=2256&parametros=";
            add_valores_a_mandar(valorAtributo("txtCantidad"));
            add_valores_a_mandar( parseInt(valorAtributo("lblTotalEntregado"))+parseInt(valorAtributo('txtCantidad')));
            add_valores_a_mandar(document.getElementById('txtIdArticuloOC').textContent);

            add_valores_a_mandar(valorAtributo("txtIdOC"));
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
            add_valores_a_mandar(document.getElementById('txtIdArticuloOC').textContent);
            add_valores_a_mandar(valorAtributo("txtLote"));
            add_valores_a_mandar(valorAtributo("txtFechaVencimiento")); 
            add_valores_a_mandar(valorAtributo("txtCantidad")); 
            add_valores_a_mandar(valorAtributo("txtValorU"));
            add_valores_a_mandar(valorAtributo("txtDescuento")); 
            add_valores_a_mandar(valorAtributo("cmbIva"));
            if(valorAtributo("cmbIdBodega") === '' && valorAtributo('lblClase') === 'OS'){
                add_valores_a_mandar('0'); 
            }else{
                add_valores_a_mandar(valorAtributo("cmbIdBodega"));
            }
            ajaxModificar();
        break;

        case 'EnviarProductosADocumento':
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=2256&parametros=";
                add_valores_a_mandar('5');
                add_valores_a_mandar('txtIdOC'); 
                add_valores_a_mandar(valorAtributo('lblObservacionOrden'));
                add_valores_a_mandar(valorAtributo('lblIdTercero'));
                add_valores_a_mandar(IdSesion())
                add_valores_a_mandar(IdSesion())
                add_valores_a_mandar('0');
                add_valores_a_mandar(valorAtributo('lblClase'));
                ajaxModificar();
        break; 
    }
}

function respuestaModificarCRUDOrdenEntrega(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'asignarAtributos': 
                alert('VALORES ASIGNADOS DE MANERA CORRECTA');
                limpiarAtributoAsignar(arg);
                setTimeout(() => {
                    crearGrillaComprobanteEntrada('listArticulosOrdenCompra'); 
                }, 100);
                setTimeout(() => {
                    crearGrillaComprobanteEntrada('listTransaccionesOrdenEntrada'); 
                }, 200);
                
            break;
            case 'EnviarProductosADocumento':
                alert('TODOS LOS PRODUCTOS HAN SIDO ENVIADOS A INVENTARIO'); 
                crearGrillaComprobanteEntrada('listArticulosOrdenCompra');
            break; 
            
            case 'RelacionarTransConFac':
                setTimeout(() => {
                    crearGrillaComprobanteEntrada('listOrdenesCompra'); 
                }, 50);

                setTimeout(() => {
                    crearGrillaComprobanteEntrada('listTransaccionesOrdenEntrada'); 
                }, 100);
                setTimeout(() => {
                    document.getElementById('cmbIdBodega'). value = ''; 
                }, 150); 

                setTimeout(() => {
                    document.getElementById('txtFactura').value = ''; 
                }, 200);
                alert('TODOS LOS DATOS PENDIENTES FUERON MIGRADOS A LA FACTURA DE ENTRADA');

                
                break;
            case 'revertirTransaccionOrdenCompra': 
                alert('TRANSACCION REMOVIDA EXITOSAMENTE !!! '); 
                setTimeout(() => {
                    crearGrillaComprobanteEntrada('listArticulosOrdenCompra'); 
                }, 100);
                
                setTimeout(() => {
                    crearGrillaComprobanteEntrada('listTransaccionesOrdenEntrada'); 
                }, 200);
            break; 
        }
    }
}

function limpiarAtributoAsignar(arg){
    switch (arg) {
        case 'asignarAtributos':
            document.getElementById('txtIdArticuloOC').textContent = '';
            document.getElementById('txtCantidad').value = '';
            document.getElementById('txtLote').value = '';
            document.getElementById('txtFechaVencimiento').value = '';
            document.getElementById('txtValorU').value = '';
            document.getElementById('txtDescuento').value = '';
            document.getElementById('cmbIva').value = '';
            document.getElementById('txtIdArticulo').value = '';
        break;
    }

}
