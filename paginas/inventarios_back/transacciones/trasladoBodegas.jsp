﻿<%@ page session="true" contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import = "java.util.*,java.text.*,java.sql.*"%>
<%@ page import = "Sgh.Utilidades.*" %>
<%@ page import = "Clinica.AdminSeguridad.ControlAdmin" %>
<%@ page import = "Clinica.Presentacion.*" %>

<jsp:useBean id="beanSession" class="Sgh.Utilidades.Sesion" scope="session" /> <!-- instanciar bean de session -->
<jsp:useBean id="beanAdmin" class="Clinica.AdminSeguridad.ControlAdmin" scope="session" />
<!-- instanciar bean de session -->


<% 	beanAdmin.setCn(beanSession.getCn()); 
   ArrayList resultaux=new ArrayList();
%>

<table width="1050px" align="center" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td>
			<!-- AQUI COMIENZA EL TITULO -->
			<div align="center" id="tituloForma">
				<!-- el id debe ser tituloForma para poder realizar Drag and Drop desde la barra de titulo -->
				<jsp:include page="../../titulo.jsp" flush="true">
					<jsp:param name="titulo" value="Traslado entre Bodegas" />
				</jsp:include>
			</div>
			<!-- AQUI TERMINA EL TITULO -->
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" class="fondoTabla">
				<tr>
					<td>

						<div style="overflow:auto;height:390px; width:100%" id="divContenido">
							<!-- en height se ubica el maximo valor de la ventana antes de que salgan los scroll -->
							<div id="divBuscar" style="display:block">
								<table width="100%" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<table width="100%">
												<tr class="titulos">
													<td width="22%">BODEGA ORIGEN</td>
													<td width="15%">BODEGA DESTINO</td>
													<td>ID DOCUMENTO</td>
													<td>ESTADO</td>
													<td>TIPO</td>
													<td>FECHA DESDE</td>
													<td>FECHA HASTA</td>
												</tr>
												<tr class="estiloImput">
													<td>
														<select class="estImputAdminLargo" size="1" id="cmbIdBodega"
															style="width:90%" 
															onfocus="comboBodegasDespachos(this.id,'509')"
															onblur="cargarHG(this.value)"
															onchange="cargarHG(this.value)">
															<!--<select class="estImputAdminLargo" size="1" id="cmbIdBodega" style="width:90%"  > -->
															<option value=""></option>
															<option value="1">FARMACIA</option>
														</select>
													</td>
													<td>
														<select class="estImputAdminLargo" size="1"
															id="cmbIdBodegaDestino" style="width:90%" 
															onfocus="comboBodegasDespachos(this.id,'509')"
															onblur="cargarCanasta(this.value)"
															onchange="cargarCanasta(this.value)">
															<option value=""></option>
														</select>
													</td>
													<td><label id="lblIdDoc"></label> </td>
													<td><label id="lblIdEstado"></label></td>
													<td><select class="estImputAdminLargo" id="cmbIdTipoDocumento"
															style="width:90%" >
															<option value="19">TRANSACCIONES ENTRE BODEGA</option>
														</select>
														<input class="estImputAdminLargo" type="hidden"
															id="txtNaturaleza" value="T" />
													</td>
													<td>
														<input class="estImputAdminLargo" type="text"
															id="txtFechaDocumentoDesde" style="width:100px"
															title="Fecha de creacion del documento" />
													</td>
													<td>
														<input class="estImputAdminLargo" type="text"
															id="txtFechaDocumentoHasta" style="width:100px"
															title="Fecha de creacion del documento" />
													</td>
												</tr>
												<tr class="titulos">
													<td colspan="7" align="center">
														<input class="small button blue" value="NUEVO DOCUMENTO"
															title="BG31" type="button"
															onclick="guardarYtraerDatoAlListado('crearDocInvBodega')">
														<input class="small button blue" value="CERRAR TRASLADO"
															title="BT935" type="button"
															onclick="guardarYtraerDatoAlListado('verificaTransaccionesInventarioTrasladoBodega')">
														<input class="small button blue" value="BUSCAR" title="BT93Y5"
															type="button"
															onclick="buscarSuministrosInventarios('listGrillaDocumentosTrasladoBodega')">
														<input name="btn_cierra" title="BTRim" type="button"
															class="small button blue" value="IMPRI DOCUMENTO"
															onclick="impresionDocFarmacia();" />
													</td>
												</tr>
											</table>
											<table id="listGrillaDocumentosTrasladoBodega" class="scroll"></table>
										</td>
									</tr>
								</table>
							</div>
						</div><!-- div contenido-->

						<div id="divEditar" style="display:block; width:100%">
							<table width="100%" cellpadding="0" cellspacing="0" align="center">
								<tr>
									<td>
										<table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0"
											class="fondoTabla">
											<tr>
												<td class="tdTitulo">
													<p class="pTitulo">TRANSACCIONES</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr></tr>
								<tr>
									<td>
										<table width="100%">
											<tr class="estiloImputIzq2">
												<td width="20%">ID TRANSACCION: &nbsp;&nbsp;&nbsp;&nbsp;<label
														id="lblIdTransaccion"></label></td>
												<td width="80%">NATURALEZA: &nbsp;&nbsp;&nbsp;&nbsp;<label
														id="lblNaturaleza">S</label></td>
											</tr>

											<tr class="estiloImputIzq2">
												<td width="20%">ARTICULO: </td>
												<td width="80%" colspan="3">
													<input class="estImputAdminLargo" type="text" id="txtIdArticulo"
														style="width:50%"  />
													<img width="18px" height="18px" align="middle"
														src="/clinica/utilidades/imagenes/acciones/buscar.png"
														onclick="traerVentanitaArticulo('txtIdArticulo','11')"
														title="VEN234">

													<input hidden class="estImputAdminLargo" type="text"
														id="txtIdBarCode" value=""
														onchange="traerElementosCodigoBarras(this.value);"
														onpaste="this.onchange();" />
													<div id="divParaVentanitaArticulo"></div>
												</td>
											</tr>
											<tr class="estiloImputIzq2">
												<td>LOTE: </td>
												<td>
													<select class="estImputAdminLargo" id="cmbIdLote" style="width:20%"
														 onfocus="cargarLote()"
														onblur="traerExistenciaLote()">
														<option value=""></option>
													</select>
													<input class="estImputAdminLargo" type="hidden" id="txtFV" />
													<label id="lblMedida"><label>
												</td>
											</tr>

											<tr class="estiloImputIzq2">
												<td>EXISTENCIAS: </td>
												<td><label id="lblExistencias">0</label></td>
											</tr>

											<tr class="estiloImputIzq2">
												<td>CANTIDAD</td>
												<td><input class="estImputAdminLargo" type="text" id="txtCantidad"
														style="width:20%" 
														onKeyPress="javascript:return soloNumeros(event)"
														onkeyup="calcularImpuestoSalida()" /></td>
											</tr>

											<tr class="estiloImputIzq2">
												<td>SERIAL :</td>
												<td><input class="estImputAdminLargo" type="text" id="txtSerial"
														style="width:20%"  /></td>
											</tr>

											<tr class="estiloImputIzq2">
												<td>VALOR UNITARIO:</td>
												<td><label style="width:20%" id="lblValorUnitario">0</label></td>


											</tr>
											<tr class="estiloImputIzq2">
												<td>IVA VENTA:</td>
												<td><label id="lblIva">0</label>
												</td>
											</tr>
											<tr class="estiloImputIzq2">
												<td>IMPUESTO IVA $:</td>
												<td> <label id="lblValorImpuesto"></label>
												</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>

											<tr class="estiloImputIzq2">
												<td>SUBTOTAL $:</td>
												<td colspan="3"><label id="lblSubTotal">0</label></td>
											</tr>
											<tr class="estiloImputIzq2">
												<td>TOTAL $:</td>
												<td colspan="3"> <label id="lblTotal">0</label></td>
											</tr>

											<tr>
												<td colspan="4" align="center">
													<input id="btn_limpia" title="BTP01" class="small button blue"
														type="button" value="LIMPIAR"
														onclick="limpiarDivEditarJuan('limpCamposTransaccion')">
													<input id="btn_crea" title="BTL02" class="small button blue"
														type="button" value="CREAR"
														onclick="insertarTransaccionDevolucion()">
													<input name="btn_modifica" title="BTP02" type="button"
														class="small button blue" value="MODIFICAR"
														onclick="modificarCRUD('modificaTransaccionTraslado');" />
													<input name="btn_elimina" title="BTP03" type="button"
														class="small button blue" value="ELIMINAR"
														onclick="eliminarTransaccionDevolucion();" />
												</td>
											</tr>
										</table>
										<table id="listGrillaTransaccionDocumento" class="scroll"></table>
									</td>
								</tr>
							</table>

							<div id="listaDevolucionesHG">
								<table width="100%" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0"
												class="fondoTabla">
												<tr>
													<td class="tdTitulo">
														<p class="pTitulo">BODEGAS DE CONSUMO</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
									<tr>
										<td>
											<table id="listGrillaDevolucionHG" class="scroll"></table>
										</td>

									</tr>

								</table>
							</div>
							<br />

							<div id="listaCanastaTraslado">

								<table width="100%" cellpadding="0" cellspacing="0" align="center">

									<tr>
										<td>
											<table width="100%" cellpadding="0" class="tablaTitulo" cellspacing="0"
												class="fondoTabla">
												<tr>
													<td class="tdTitulo">
														<p class="pTitulo">CANASTA</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
									<tr class="titulos">
										<td width="5%">&nbsp;</td>
										<td width="20%" align="right">CANASTA</td>
										<td align="center">
											<label id="lblCodigoCanasta"></label>
											<select class="estImputAdminLargo" size="1" id="cmbCanasta"
												style="width:48%" 
												onchange="asignaAtributo('lblCodigoCanasta', this.value, 0); buscarSuministros('listGrillaCanastaTrasladoBodega');">
												<option value=""></option>
												<%     resultaux.clear();
					  resultaux=(ArrayList)beanAdmin.combo.cargar(560);	
					  ComboVO cmb45ca;
					  for(int k=0;k<resultaux.size();k++){ 
							cmb45ca=(ComboVO)resultaux.get(k);
			   %>
												<option value="<%= cmb45ca.getId()%>" title="<%= cmb45ca.getTitle()%>">
													<%=cmb45ca.getDescripcion()%></option>
												<%}%>
		   
		  </select>  
			 
	  </td>
	  <td>
		 <input type="button" value="PROGRAMAR CANASTA" title="BTC01" onclick="modificarCRUD('crearCanastaTrasladoBodega','/clinica/paginas/accionesXml/modificarCRUD_xml.jsp');" />
	 </td>			 
	</tr>
	
	<tr class="titulos">  
	   <tr>
		 <td colspan="4">
		   <table id="listGrillaCanastaTrasladoBodega" width="100%" class="scroll"></table>          
		 </td>   
	   </tr>
	</tr> 
</table>   

</div>


</div><!-- divEditar--> 
	 


	   </td>
	  </tr>
	 </table>

  </td>
</tr> 

</table>








<label id="lblIdTransaccionDev" style="font-size:8px;" ></label>
<input class="estImputAdminLargo" type="hidden" id="txtIdUsuarioSesion" value ="<%=beanSession.usuario.getIdentificacion()%>"
												/>
												<input class="estImputAdminLargo" type="hidden"
													id="txtBanderaDevolucion" value="NO" />

												<input class="estImputAdminLargo" type="hidden" id="txtIdLote1"
													value="" />
												<input class="estImputAdminLargo" type="hidden" id="txtIdLoteBan"
													value="NO" />
												<input class="estImputAdminLargo" type="hidden" id="txtValorFlete"
													style="width:80%" value="0"  />
												<label id="lblTraidosCodBarras" style="font-size:9px"></label>
												<input class="estImputAdminLargo" type="hidden"
													id="txtIdArticulo_dos" />
												<input class="estImputAdminLargo" type="hidden" id="txtIdLote_dos" />
												<input class="estImputAdminLargo" type="hidden"
													id="txtValorUnitario_dos" />
												<input class="estImputAdminLargo" type="hidden" id="txtFV_dos" />
												<input class="estImputAdminLargo" type="hidden" id="txtVenCodBarras"
													value="1" />
												<input class="estImputAdminLargo" type="hidden" id="txtIdDocumento">