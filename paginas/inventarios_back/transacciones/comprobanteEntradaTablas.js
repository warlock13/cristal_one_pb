function crearGrillaComprobanteEntrada(arg) {
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
    switch (arg) {
        case 'listOrdenesCompra':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2255&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdOC'));
            add_valores_a_mandar(valorAtributo('txtIdProveedor'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc')+' 00:00:00');
            add_valores_a_mandar(valorAtributo('txtFechaHastaDoc')+' 23:59:59');

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'TOTAL',
                    'ID ORDEN',
                    'OBSERVACION',
                    'ID TERCERO',
                    'FECHA CREA',
                    'FECHA DOCUMENTO',
                    'FECHA CIERRE',
                    'USUARIO CREA',
                    'USUARIO CIERRA',
                    'ID ESTADO',
                    'ESTADO FINAL',
                    'ID CLASE',
                    'LUGAR ENTREGA',
                    'TIEMPO ENTREGA',
                    'FORMA DE PAGO'

                ],
                colModel: [{
                        name: 'total',
                        index: 'total',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id',
                        index: 'id',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'observacion',
                        index: 'observacion',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_tercero',
                        index: 'id_tercero',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'fecha_crea',
                        index: 'fecha_crea',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'fecha_documento',
                        index: 'fecha_documento',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'fecha_cierre',
                        index: 'fecha_cierre',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_usuario_crea',
                        index: 'id_usuario_crea',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_usuario_cierra',
                        index: 'id_usuario_cierra',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'id_estado',
                        index: 'id_estado',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'sw_inventario',
                        index: 'sw_inventario',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'id_clase',
                        index: 'id_clase',
                        width: anchoP(ancho, 5)
                    },
                    {
                        name: 'lugar_entrega',
                        index: 'lugar_entrega',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'tiempo_entrega',
                        index: 'tiempo_entrega',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                    {
                        name: 'forma_pago',
                        index: 'forma_pago',
                        width: anchoP(ancho, 5),
                        hidden: true
                    },
                ],
                height: 160,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdOC', datosRow.id, 0)
                    asignaAtributo('lblIdTercero', datosRow.id_tercero,0);
                    asignaAtributo('lblObservacionOrden', datosRow.observacion);
                    asignaAtributo('lblClase', datosRow.id_clase,0);
                    asignaAtributo('lblInventario',datosRow.sw_inventario);
                    setTimeout(() => {
                        compobanteEntradaAsincrono('crearGrillaComprobanteEntrada','listArticulosOrdenCompra'); 
                    }, 100);

                    setTimeout(() => {
                        compobanteEntradaAsincrono('crearGrillaComprobanteEntrada','listTransaccionesOrdenEntrada');
                    }, 200);
                }
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;

        case 'listTransaccionesOrdenEntrada': 
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2263&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdOC'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'ID',
                    'ID TRANSACCION',
                    'ID ORDEN',
                    'ID ARTICULO',
                    'ID ARTICULO ORDEN DE COMPRA',
                    'ID FACTURA ENTRADA',
                    'LOTE',
                    'FECHA VENCIMIENTO',
                    'CANTIDAD',
                    'VALOR UNITARIO',
                    'DESCUENTO',
                    'IVA',
                    'ID BODEGA',
                    'FACTURADO'
                ],
                colModel: [{
                        name: 'ID',
                        index: 'ID',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'id_trans',
                        index: 'id_trans',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'id_orden',
                        index: 'id_orden',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'id_articulo',
                        index: 'id_articulo',
                        width: anchoP(ancho, 5)
                        

                    },
                    {
                        name: 'id_articulo_oc',
                        index: 'id_articulo_oc',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'id_fac_entrada',
                        index: 'id_fac_entrada',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'lote',
                        index: 'lote',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'fecha_vencimiento',
                        index: 'fecha_vencimiento',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'cantidad',
                        index: 'cantidad',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'valor_unitario',
                        index: 'valor_unitario',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'descuento',
                        index: 'descuento',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'iva',
                        index: 'iva',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'id_bodega',
                        index: 'id_bodega',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'sw_fac',
                        index: 'sw_fac',
                        width: anchoP(ancho, 5),

                    }
                ],
                height: 160,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdArticuloBorrarTrans', datosRow.id_trans, 0);
                    asignaAtributo('lblCantidadTransaccionRevertir',datosRow.cantidad,0);
                    asignaAtributo('lblIdArticuloArevertir',datosRow.id_articulo_oc,0);
                    asignaAtributo('lblSwFactura',datosRow.sw_fac,0);
                    asignaAtributo('lblIdDoc',datosRow.id_fac_entrada,0);
                    
                }

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
        break;

        case 'listArticulosOrdenCompra':
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=2244&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdOC'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'ID',
                    'ID ARTICULOOC',
                    'ID ARTICULO',
                    'NOMBRE COMERCIAL',
                    'ID ORDEN COMPRA',
                    'CANTIDAD',
                    'VALOR TOTAL',
                    'SUBTOTAL',
                    'DESCUENTO',
                    'IVA',
                    'DESCRIPCION',
                    'ULT. CANT ENTREGADA',
                    'FECHA VENCIMIENTO',
                    'LOTE',
                    'TOT. ENT',
                    'VALOR UNITARIO'

                ],
                colModel: [{
                        name: 'ID',
                        index: 'ID',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'ID_ARTICULOOC',
                        index: 'ID_ARTICULOOC',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'ID_ARTICULO',
                        index: 'ID_ARTICULO',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'NOMBRE_COMERCIAL',
                        index: 'NOMBRE_COMERCIAL',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'ID_ORDEN_COMPRA',
                        index: 'ID_ORDEN_COMPRA',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'CANTIDAD',
                        index: 'CANTIDAD',
                        width: anchoP(ancho, 5),

                    },
                    {
                        name: 'VALOR_PRODUCTO',
                        index: 'VALOR_PRODUCTO',
                        width: anchoP(ancho, 5),

                    },
                    //=========================
                    {
                        name: 'SUBTOTAL',
                        index: 'SUBTOTAL',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'DESCUENTO',
                        index: 'DESCUENTO',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'IVA',
                        index: 'IVA',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'DESCRIPCION',
                        index: 'DESCRIPCION',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },
                    {
                        name: 'CANTIDAD_PIV',
                        index: 'CANTIDAD_PIV',
                        width: anchoP(ancho, 5)

                    },
                    {
                        name: 'FECHA_VENCIMIENTO',
                        index: 'FECHA_VENCIMIENTO',
                        width: anchoP(ancho, 5),
                        hidden:true

                    },
                    {
                        name: 'LOTE',
                        index: 'LOTE',
                        width: anchoP(ancho, 5),
                        hidden: true

                    },{
                        name: 'TOTAL_ENT',
                        index: 'TOTAL_ENT',
                        width: anchoP(ancho, 5)
                    },{
                        name: 'VALOR_UNITATIO',
                        index: 'VALOR_UNITARIO',
                        width: anchoP(ancho, 5),
                        hidden: true
                    }
                ],
                height: 160,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    if(datosRow.TOTAL_ENT === ''){
                        datosRow.TOTAL_ENT = 0;
                    }

                    limpiarAtributoAsignar('asignarAtributos');

                    //Asignacion de atributos
                    asignaAtributo('cmbIva',datosRow.IVA);
                    asignaAtributo('txtValorU',datosRow.VALOR_UNITATIO,1)
                    asignaAtributo('txtCantidad', parseInt(datosRow.CANTIDAD) - parseInt(datosRow.TOTAL_ENT), 0);
                    asignaAtributo('txtDescuento',datosRow.DESCUENTO);

                    //Logica de cantidades
                    document.getElementById('lblCantidad_piv').textContent = datosRow.CANTIDAD;
                    document.getElementById('lblUltimaCantidad').textContent = datosRow.CANTIDAD_PIV;
                    document.getElementById('txtIdArticuloOC').textContent = datosRow.ID_ARTICULOOC;
                    asignaAtributo('lblObservacion', datosRow.DESCRIPCION,0); 
                    asignaAtributo('lblTotalEntregado', datosRow.TOTAL_ENT,0);
                    asignaAtributo('lblArticuloCodigo', datosRow.ID_ARTICULO,0);
                    asignaAtributo('txtIdArticulo',datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_COMERCIAL,0);
                }

            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({
                url: valores_a_mandar
            }).trigger('reloadGrid');
            break;
    }
}