function modificarCRUDInventario(arg, pag) {

    if (pag == 'undefined')
        pag = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';

    pagina = '/clinica/paginas/accionesXml/modificarCRUD_xml.jsp';
    paginaActual = arg;
    switch (arg) {
        case 'crearBodegaInventario':
            if (verificarCamposGuardar(arg)) {
                valores_a_mandar = 'accion=' + arg;
                valores_a_mandar = valores_a_mandar + "&idQuery=82&parametros=";
                add_valores_a_mandar(valorAtributo('txtDescripcion'));
                add_valores_a_mandar(document.getElementById('lblIdEmpresa').textContent);
                add_valores_a_mandar(document.getElementById('lblIdSede').textContent);
                add_valores_a_mandar(valorAtributo('cmbIdCentroCosto'));
                add_valores_a_mandar(valorAtributo('cmbIdIdResponsable'));
                add_valores_a_mandar(valorAtributo('cmbEstado'));
                add_valores_a_mandar(valorAtributo('cmbSWRestriccionStock'));
                add_valores_a_mandar(valorAtributo('cmbIdBodegaTipo'));
                ajaxModificar();
            }
            break;
    }
}

function respuestaModificarCRUDInventario(arg, xmlraiz) {
    if (xmlraiz.getElementsByTagName("respuesta")[0].firstChild.data == "true") {
        switch (arg) {
            case 'crearBodegaInventario':
                alert('Bodega Creada de manera Exitosa');
                buscarSuministrosBodegas('listGrillaBodega')
                limpiarDivEditarJuan('limpCamposBodega');
                break;
        }
    }
}


function buscarSuministrosBodegas(arg) {
    idArticulo = '';
    pag = '/clinica/paginas/accionesXml/buscarGrilla_xml.jsp';
    pagina = pag;
    ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) * 92 / 100;
    //    alto= ($('#drag'+ventanaActual.num).find("#divContenido").width())*92/100;	
    switch (arg) {
        case 'listGrillaBodega':
            /*		   ancho = ($('#drag'+ventanaActual.num).find("#divDocuInv").width());	   		 
                       alto = ($('#drag'+ventanaActual.num).find("#divDocuInv").height()); */
            //ancho = 900; 		   
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=80&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('txtNomDiagnosticoBus'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            //add_valores_a_mandar( IdSesion()  );			   

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: [
                    'ID',
                    'ID BODEGA',
                    'DESCRIPCION',
                    'CENTRO DE COSTO',
                    'NOMBRE FUNCIONARIO RESPONSABLE',
                    'ESTADO',
                    'RESTRICCION STOCK',
                    'TIPO BODEGA',
                    'ID CENTRO COSTO',
                    'ID RESPONSABLE',
                    'ID BODEGA'
                ],
                colModel: [
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 5), hidden: true },
                    { name: 'ID_BODEGA', index: 'ID_BODEGA' },
                    { name: 'DESCRIPCION', index: 'DESCRIPCION', width: anchoP(ancho, 35) },
                    { name: 'CENTRO_COSTO', index: 'CENTRO_COSTO', width: anchoP(ancho, 35) },
                    { name: 'RESPONSABLE', index: 'RESPONSABLE', width: anchoP(ancho, 35) },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 35), hidden: true },
                    { name: 'RESTRICCION_STOCK', index: 'RESTRICCION_STOCK', width: anchoP(ancho, 35), hidden: true },
                    { name: 'TIPO_BODEGA', index: 'TIPO_BODEGA', width: anchoP(ancho, 35) },
                    { name: 'ID_CC', index: 'ID_CC', width: anchoP(ancho, 35), hidden: true },
                    { name: 'ID_RESPONSABLE', index: 'ID_RESPONSABLE', width: anchoP(ancho, 35), hidden: true },
                    { name: 'ID_BODEGA_no', index: 'ID_BODEGA_no', width: anchoP(ancho, 35), hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: alto,
                width: ancho,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    if (probarExiste('listBodegaSede') === true && probarExiste('listUsuarioBodega') === true) {
                        document.getElementById('listBodegaSede').style.display = '';
                        document.getElementById('listUsuarioBodega').style.display = '';
                    }

                    asignaAtributo('txtId', datosRow.ID_BODEGA, 1);
                    asignaAtributo('txtDescripcion', datosRow.DESCRIPCION, 0);
                    asignaAtributo('cmbIdCentroCosto', datosRow.ID_CC, 0);
                    asignaAtributo('cmbIdIdResponsable', datosRow.ID_RESPONSABLE, 0);
                    asignaAtributo('cmbEstado', datosRow.ESTADO, 0);
                    asignaAtributo('cmbSWRestriccionStock', datosRow.RESTRICCION_STOCK, 0);
                    asignaAtributo('cmbIdBodegaTipo', datosRow.ID_BODEGA, 0);

                    /* Grillas de bodega */
                    setTimeout(() => {
                        buscarSuministrosBodegas('listBodegaSede');
                    }, 300);
                    setTimeout(() => {
                        buscarSuministrosBodegas('listUsuarioBodega')
                    }, 300);

                    if (probarExiste('cmbIdBodega') == true) {
                        document.getElementById('cmbIdBodega').value = datosRow.ID_BODEGA;
                    }



                    if (valorAtributo('txtIdTipoBodega') === '4') {
                        $(".optica").show();
                    } else {
                        $(".optica").hide();
                    }

                    //limpiarDivEditarJuan('limpCamposDocumento');
                    buscarSuministrosBodegas('listGrillaDocumentosBodega');
                    if (valorAtributo('txtNaturaleza') == 'I') {
                            setTimeout("buscarSuministrosBodegas('listGrillaTransaccionDocumento')", 500);
                    } else {
                        setTimeout("buscarSuministrosBodegas('listGrillaTransaccionSalida')", 500);
                    }

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;


        case 'listGrillaPlantillaCanasta':

            // limpiarDivEditarJuan(arg); 
            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=446&parametros=";
            add_valores_a_mandar(valorAtributo('txtCodPlantilla'));
            add_valores_a_mandar(valorAtributo('txtNomPlantilla'));
            add_valores_a_mandar(valorAtributo('cmbVigenteBus'));
            $('#drag' + ventanaActual.num).find("#listGrillaPlantillaCanasta").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'NOMBRE', 'CONTENIDO', 'ID_TIPO', 'TIPO', 'VIGENTE'],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 3) },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 3) },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 75) },
                    { name: 'CONTENIDO', index: 'CONTENIDO', width: anchoP(ancho, 1) },
                    { name: 'ID_TIPO', index: 'ID_TIPO', hidden: true },
                    { name: 'TIPO', index: 'TIPO', width: anchoP(ancho, 10) },
                    { name: 'VIGENTE', index: 'VIGENTE', hidden: true }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 250,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaPlantillaCanasta').getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblCodigoCanasta', datosRow.ID, 1);
                    asignaAtributo('txtNombreCanasta', datosRow.NOMBRE, 0);
                    asignaAtributo('txtContenidoCanasta', datosRow.CONTENIDO, 0);
                    asignaAtributo('cmbTipoCanasta', datosRow.ID_TIPO, 0);
                    asignaAtributo('cmbVigente', datosRow.VIGENTE, 0);

                    buscarSuministrosBodegas('listGrillaCanastaDetalle');

                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;


        case 'listGrillaCanastaDetalle':
                    
            ancho= ($('#drag'+ventanaActual.num).find("#listGrillaCanastaDetalle").width())-50;
            valores_a_mandar=pag;
            valores_a_mandar=valores_a_mandar+"?idQuery=449&parametros=";
            add_valores_a_mandar( valorAtributo('lblCodigoCanasta')  );			   
            
            $('#drag'+ventanaActual.num).find("#listGrillaCanastaDetalle").jqGrid({ 
            url:valores_a_mandar, 	
            datatype: 'xml', 
            mtype: 'GET', 
            
            colNames:[ 'Tot','ID','ID_CANASTA','ID_ARTICULO','NOMBRE_GENERICO','CANTIDAD','UNIDAD'],
            colModel :[ 
                {name:'contador', index:'contador',  width: anchoP(ancho,5)},  
                {name:'ID', index:'ID', hidden:true},			   
                {name:'ID_CANASTA', index:'ID_CANASTA', hidden:true}, 
                {name:'ID_ARTICULO', index:'ID_ARTICULO', width: anchoP(ancho,5)}, 
                {name:'NOMBRE_GENERICO', index:'NOMBRE_GENERICO', width: anchoP(ancho,70)},			   			   			   			   
                {name:'CANTIDAD', index:'CANTIDAD', width: anchoP(ancho,10)},
                {name:'ABREVIATURA', index:'ABREVIATURA', width: anchoP(ancho,5)}
                ], 
            
            //  pager: jQuery('#pagerGrilla'), 
            height: 250, 
            width: ancho+40,			
            onSelectRow: function(rowid){ 
        
            var datosRow = jQuery('#drag'+ventanaActual.num).find('#'+arg).getRowData(rowid);  
                estad=0;
                    asignaAtributo('lblIdDetalle',datosRow.ID,1);	
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO+'-'+datosRow.NOMBRE_GENERICO,0);
                    asignaAtributo('cmbCantidad',datosRow.CANTIDAD,0);			  
                    asignaAtributo('lbl_Unidad',datosRow.ABREVIATURA,0);			  						   
                    
                    if(valorAtributo('lblIdDocumento')!=''){
                        asignaAtributo('lblIdPlantillaCan',datosRow.ID,0);	 
                        asignaAtributo('lblIdCanasta',datosRow.ID_CANASTA,0);	 
                        asignaAtributo('lblIdArticulo',datosRow.ID_ARTICULO,0);	 
                        asignaAtributo('lblNombreArticulo',datosRow.NOMBRE_GENERICO,0);	 
                        //asignaAtributo('lblCantidad',datosRow.CANTIDAD,0);
                        asignaAtributo('cmbCantConsumo',datosRow.CANTIDAD,0);
                        
                        mostrar('divVentanitaTraerArticuloCanasta');
                            
                    }
                    //else alert('Debe seleccionar un documento clinico');
                                                    
                },
        });  
        
        $('#drag'+ventanaActual.num).find("#"+arg).setGridParam({url:valores_a_mandar}).trigger('reloadGrid');			 
        break;

        case 'listGrillaDocumentosBodegaOrdenesCompra':
            ancho = ($('#drag' + ventanaActual.num).find("#divDocuInv").width());
            alto = ($('#drag' + ventanaActual.num).find("#divDocuInv").height() - 40);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=84&parametros=";
            add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            add_valores_a_mandar(valorAtributo('txtNaturaleza'));

            add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc') + ' 00:00:00');
            add_valores_a_mandar(valorAtributo('txtFechaHastaDoc') + ' 23:59:59');
            add_valores_a_mandar(valorAtributo('cmbIdEstado'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA CREA', 'FECHA DOCUMENTO', 'ID_TERCERO',
                    'TERCERO', 'NATURALEZA', 'OBSERVACION', 'VLR FLETE', 'ID_ESTADO', 'ESTADO', 'ID_FACTURA', 'ID_PACIENTE', 'FACTURA'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 3) },
                    { name: 'ID_DOCUMENTO_TIPO', index: 'ID_DOCUMENTO_TIPO', hidden: true },
                    { name: 'NOMBRE_DOCUMENTO_TIPO', index: 'NOMBRE_DOCUMENTO_TIPO', width: anchoP(ancho, 25) },
                    { name: 'NUMERO', index: 'NUMERO', width: anchoP(ancho, 5) },
                    { name: 'FECHA_CREA', index: 'FECHA_CREA', width: anchoP(ancho, 8) },
                    { name: 'FECHA_DOCUMENTO', index: 'FECHA_DOCUMENTO', width: anchoP(ancho, 11) },
                    { name: 'ID_TERCERO', index: 'ID_TERCERO', hidden: true },
                    { name: 'NOMBRE_TERCERO', index: 'NOMBRE_TERCERO', width: anchoP(ancho, 8) },
                    { name: 'NATURALEZA', index: 'NATURALEZA', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 20) },
                    { name: 'valor_flete', index: 'valor_flete', width: anchoP(ancho, 5) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 6) },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'ESTADO_FACTURA', index: 'ESTADO_FACTURA', width: anchoP(ancho, 6) }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: alto,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg);
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('txtIdDocumento', datosRow.ID, 0);
                    asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 0);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
                    asignaAtributo('lblNaturaleza', datosRow.NATURALEZA, 0);
                    asignaAtributo('txtNumero', datosRow.NUMERO, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);


                    $("#txtIdBarCode").focus();
                    /* cargar transacciones del documento */
                    limpiarDivEditarJuan('limpCamposTransaccion')
                    if (valorAtributo('txtNaturaleza') == 'I') {
                        asignaAtributo('txtValorFlete', datosRow.valor_flete, 0);
                        buscarSuministrosBodegas('listGrillaTransaccionDocumento');
                    } else {
                        buscarSuministrosBodegas('listGrillaTransaccionSalida');
                    }

                    if (probarExiste('limpiarFacturaOptica')) {
                        limpiarDivEditarJuan('limpiarFacturaOptica');
                    }


                    if (datosRow.ID_FACTURA != '') {

                        console.log('Factura: ' + datosRow.ID_FACTURA)

                        asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                        asignaAtributo('txtIdBusPaciente', datosRow.ID_PACIENTE + '-', 0)
                        setTimeout("buscarInformacionBasicaPaciente()", 600);
                        guardarYtraerDatoAlListado('consultarValoresCuenta')
                        setTimeout("buscarFacturacion('listArticulosDeFactura')", 200)
                    }
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;     

        case 'listGrillaDocumentosBodega':
            ancho = ($('#drag' + ventanaActual.num).find("#divDocuInv").width());
            alto = ($('#drag' + ventanaActual.num).find("#divDocuInv").height() - 40);
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=84&parametros=";
            if(valorAtributo('cmbIdBodega') === ''){
                add_valores_a_mandar('999');
            }else{
                add_valores_a_mandar(valorAtributo('cmbIdBodega'));
            }
            add_valores_a_mandar(valorAtributo('txtNaturaleza'));
            add_valores_a_mandar(valorAtributo('txtFechaDesdeDoc') + ' 00:00:00');
            add_valores_a_mandar(valorAtributo('txtFechaHastaDoc') + ' 23:59:59');
            add_valores_a_mandar(valorAtributo('cmbIdEstado'));
            add_valores_a_mandar(valorAtributo('cmbIdTipoDocumento'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO_TIPO', 'TIPO DOCUMENTO', 'NUMERO', 'FECHA CREA', 'FECHA DOCUMENTO', 'ID_TERCERO',
                    'TERCERO', 'NATURALEZA', 'OBSERVACION', 'VLR FLETE', 'ID_ESTADO', 'ESTADO', 'ID_FACTURA', 'ID_PACIENTE', 'FACTURA'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID', index: 'ID', width: anchoP(ancho, 3) },
                    { name: 'ID_DOCUMENTO_TIPO', index: 'ID_DOCUMENTO_TIPO', hidden: true },
                    { name: 'NOMBRE_DOCUMENTO_TIPO', index: 'NOMBRE_DOCUMENTO_TIPO', width: anchoP(ancho, 25) },
                    { name: 'NUMERO', index: 'NUMERO', width: anchoP(ancho, 5) },
                    { name: 'FECHA_CREA', index: 'FECHA_CREA', width: anchoP(ancho, 8) },
                    { name: 'FECHA_DOCUMENTO', index: 'FECHA_DOCUMENTO', width: anchoP(ancho, 11) },
                    { name: 'ID_TERCERO', index: 'ID_TERCERO', hidden: true },
                    { name: 'NOMBRE_TERCERO', index: 'NOMBRE_TERCERO', width: anchoP(ancho, 8) },
                    { name: 'NATURALEZA', index: 'NATURALEZA', hidden: true },
                    { name: 'OBSERVACION', index: 'OBSERVACION', width: anchoP(ancho, 20) },
                    { name: 'valor_flete', index: 'valor_flete', width: anchoP(ancho, 5) },
                    { name: 'ID_ESTADO', index: 'ID_ESTADO', hidden: true },
                    { name: 'ESTADO', index: 'ESTADO', width: anchoP(ancho, 6) },
                    { name: 'ID_FACTURA', index: 'ID_FACTURA', hidden: true },
                    { name: 'ID_PACIENTE', index: 'ID_PACIENTE', hidden: true },
                    { name: 'ESTADO_FACTURA', index: 'ESTADO_FACTURA', width: anchoP(ancho, 6) }
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: alto,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg);
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdDoc', datosRow.ID, 0);
                    document.getElementById('txtIdDocumento').value = valorAtributo('lblIdDoc'); 
                    asignaAtributo('cmbIdTipoDocumento', datosRow.ID_DOCUMENTO_TIPO, 1);
                    asignaAtributo('txtObservacion', datosRow.OBSERVACION, 0);
                    asignaAtributo('lblIdEstado', datosRow.ID_ESTADO, 0);
                    asignaAtributo('lblNaturaleza', datosRow.NATURALEZA, 0);
                    asignaAtributo('txtNumero', datosRow.NUMERO, 0);
                    asignaAtributo('txtFechaDocumento', datosRow.FECHA_DOCUMENTO, 0);
                    asignaAtributo('txtIdTercero', datosRow.ID_TERCERO + '-' + datosRow.NOMBRE_TERCERO, 0);


                    $("#txtIdBarCode").focus();
                    /* cargar transacciones del documento */
                    limpiarDivEditarJuan('limpCamposTransaccion')
                    if (valorAtributo('txtNaturaleza') == 'I') {
                        asignaAtributo('txtValorFlete', datosRow.valor_flete, 0);
                        buscarSuministrosBodegas('listGrillaTransaccionDocumento');
                    } else {
                        buscarSuministrosBodegas('listGrillaTransaccionSalida');
                    }

                    if (probarExiste('limpiarFacturaOptica')) {
                        limpiarDivEditarJuan('limpiarFacturaOptica');
                    }


                    if (datosRow.ID_FACTURA != '') {

                        console.log('Factura: ' + datosRow.ID_FACTURA)

                        asignaAtributo('lblIdFactura', datosRow.ID_FACTURA, 0)
                        asignaAtributo('txtIdBusPaciente', datosRow.ID_PACIENTE + '-', 0)
                        setTimeout("buscarInformacionBasicaPaciente()", 600);
                        guardarYtraerDatoAlListado('consultarValoresCuenta')
                        setTimeout("buscarFacturacion('listArticulosDeFactura')", 200)
                    }
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listGrillaTransaccionSalida':
            //ancho= ($('#drag'+ventanaActual.num).find("#divContenido").width())-50;
            ancho = 1100;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=96&parametros=";
            /*add_valores_a_mandar( valorAtributo('txtCodDiagnosticoBus')  );	
            add_valores_a_mandar( valorAtributo('txtNomDiagnosticoBus')  );				 */
            add_valores_a_mandar(valorAtributo('lblIdDoc'));
            $('#drag' + ventanaActual.num).find("#listGrillaTransaccionDocumento").jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'ID_DOCUMENTO', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'CANTIDAD', 'VALOR_UNITARIO', 'IVA %', 'VALORIMPUESTO', 'NATURALEZA', 'LOTE', 'Fecha_vencimiento', 'SERIAL', 'PRECIO_VENTA', 'EXITO', 'DESC', 'TIPO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 3) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_DOCUMENTO', index: 'ID_DOCUMENTO', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
                    { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 30) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 8) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', width: anchoP(ancho, 8) },
                    { name: 'IVA', index: 'IVA', hidden: true },
                    { name: 'VALOR_IMPUESTO', index: 'VALOR_IMPUESTO', hidden: true },
                    { name: 'ID_NATURALEZA', index: 'ID_NATURALEZA', hidden: true },
                    { name: 'LOTE', index: 'LOTE', width: anchoP(ancho, 8) },
                    { name: 'fecha_vencimiento', index: 'fecha_vencimiento', width: anchoP(ancho, 8) },
                    { name: 'SERIAL', index: 'SERIAL', width: anchoP(ancho, 8) },
                    { name: 'PRECIO_VENTA', index: 'PRECIO_VENTA', width: anchoP(ancho, 4) },
                    { name: 'EXITO', index: 'EXITO', width: anchoP(ancho, 5) },
                    { name: 'porcent_descuento', index: 'porcent_descuento', width: anchoP(ancho, 5) },
                    { name: 'medida', index: 'medida', width: anchoP(ancho, 5) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 80,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#listGrillaTransaccionDocumento').getRowData(rowid);
                    estad = 0;
                    idArticulo = datosRow.ID_ARTICULO;
                    asignaAtributo('lblIdTransaccion', datosRow.ID, 1);
                    asignaAtributo('lblMedida', datosRow.medida, 1);
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);

                    //if(valorAtributo('txtIdTipoBodega')== '4'){
                    asignaAtributo('txtSerial', datosRow.SERIAL, 0);
                    asignaAtributo('lblValorUnitario', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('txtVlrUnitarioAnterior', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('lblIva', datosRow.IVA, 0);
                    asignaAtributo('lblValorImpuesto', datosRow.VALOR_IMPUESTO, 0);
                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);
                    asignaAtributo('cmbIdDescuento', '0', 0);
                    //}

                    //$("#txtIdArticulo").focus();
                },
            });
            $('#drag' + ventanaActual.num).find("#listGrillaTransaccionDocumento").setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listDescuentosArticulo':
            asignaAtributo('lblIdArtDescuento', valorAtributoIdAutoCompletar('txtIdArticulo'), 1);
            asignaAtributo('lblArtDescuento', $('#drag' + ventanaActual.num).find('#txtIdArticulo').val().trim(), 1);

            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=647&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
            add_valores_a_mandar(valorAtributo('txtSerial'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'SERIAL', 'PORCENTAJE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', hidden: true },
                    { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 30) },
                    { name: 'SERIAL', index: 'SERIAL', width: anchoP(ancho, 8) },
                    { name: 'porcent_descuento', index: 'porcent_descuento', width: anchoP(ancho, 8) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 60,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdArtDescuento', datosRow.ID_ARTICULO, 1);
                    asignaAtributo('lblSerialDescuento', datosRow.SERIAL, 1);
                    asignaAtributo('txtDescuentoArt', datosRow.porcent_descuento, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listDescuentosArticulo':
            asignaAtributo('lblIdArtDescuento', valorAtributoIdAutoCompletar('txtIdArticulo'), 1);
            asignaAtributo('lblArtDescuento', $('#drag' + ventanaActual.num).find('#txtIdArticulo').val().trim(), 1);

            ancho = ($('#drag' + ventanaActual.num).find("#divContenido").width()) - 50;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=647&parametros=";
            add_valores_a_mandar(valorAtributoIdAutoCompletar('txtIdArticulo'));
            add_valores_a_mandar(valorAtributo('txtSerial'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID_ARTICULO', 'NOMBRE ARTICULO', 'SERIAL', 'PORCENTAJE'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 2) },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', hidden: true },
                    { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 30) },
                    { name: 'SERIAL', index: 'SERIAL', width: anchoP(ancho, 8) },
                    { name: 'porcent_descuento', index: 'porcent_descuento', width: anchoP(ancho, 8) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 60,
                width: ancho,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdArtDescuento', datosRow.ID_ARTICULO, 1);
                    asignaAtributo('lblSerialDescuento', datosRow.SERIAL, 1);
                    asignaAtributo('txtDescuentoArt', datosRow.porcent_descuento, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');

            break;

        case 'listGrillaTransaccionDocumento':
            ancho = 1250;
            alto = 80;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=87&parametros=";
            add_valores_a_mandar(valorAtributo('txtIdDocumento'));
            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',

                colNames: ['Tot', 'ID', 'Id documento', 'Id Articulo', 'Nombre Articulo', 'Cantidad', 'Valor unitario', 'Iva %', 'Valor Impuesto', 'Naturaleza', 'Lote', 'Serial', 'Fecha Vencimiento', 'Precio venta', 'Exito', 'TIPO'
                ],
                colModel: [
                    { name: 'contador', index: 'contador', width: anchoP(ancho, 3) },
                    { name: 'ID', index: 'ID', hidden: true },
                    { name: 'ID_DOCUMENTO', index: 'ID_DOCUMENTO', hidden: true },
                    { name: 'ID_ARTICULO', index: 'ID_ARTICULO', width: anchoP(ancho, 3) },
                    { name: 'NOMBRE_GENERICO', index: 'NOMBRE_GENERICO', width: anchoP(ancho, 30) },
                    { name: 'CANTIDAD', index: 'CANTIDAD', width: anchoP(ancho, 8) },
                    { name: 'VALOR_UNITARIO', index: 'VALOR_UNITARIO', width: anchoP(ancho, 8) },
                    { name: 'IVA', index: 'IVA', hidden: true },
                    { name: 'VALOR_IMPUESTO', index: 'VALOR_IMPUESTO', width: anchoP(ancho, 8) },
                    { name: 'ID_NATURALEZA', index: 'ID_NATURALEZA', hidden: true },
                    { name: 'LOTE', index: 'LOTE', width: anchoP(ancho, 8) },
                    { name: 'SERIAL', index: 'SERIAL', width: anchoP(ancho, 8) },
                    { name: 'FECHA_VENCIMIENTO', index: 'FECHA_VENCIMIENTO', width: anchoP(ancho, 8) },
                    { name: 'PRECIO_VENTA', index: 'PRECIO_VENTA', width: anchoP(ancho, 8) },
                    { name: 'EXITO', index: 'EXITO', width: anchoP(ancho, 5) },
                    { name: 'medida', index: 'medida', width: anchoP(ancho, 5) },
                ],

                //  pager: jQuery('#pagerGrilla'), 
                height: 160,
                width: ancho + 40,
                onSelectRow: function (rowid) {
                    //			 limpiarDivEditarJuan(arg); 

                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    estad = 0;
                    asignaAtributo('lblIdTransaccion', datosRow.ID, 1);
                    asignaAtributo('txtIdArticulo', datosRow.ID_ARTICULO + '-' + datosRow.NOMBRE_GENERICO, 0);
                    asignaAtributo('txtLote', datosRow.LOTE, 0);
                    asignaAtributo('txtFechaVencimiento', datosRow.FECHA_VENCIMIENTO, 0);
                    asignaAtributo('txtSerial', datosRow.SERIAL, 0);
                    asignaAtributo('txtCantidad', datosRow.CANTIDAD, 0);

                    asignaAtributo('txtValorU', datosRow.VALOR_UNITARIO, 0);
                    asignaAtributo('cmbIva', datosRow.IVA, 0);

                    asignaAtributo('txtPrecioVenta', datosRow.PRECIO_VENTA, 0);
                    asignaAtributo('cmbMedida', datosRow.medida, 0);

                    $("#txtIdArticulo").focus();
                    calcularImpuesto('lblValorImpuesto')
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listBodegaSede':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=999" + "&parametros=";
            console.log(valorAtributo('txtId'));
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['contador', 'ID_BODEGA', 'BODEGA', 'ID_SEDE', 'SEDE'],
                colModel: [
                    { name: 'contador', index: 'contador', hidden: true },
                    { name: 'ID_BODEGA', index: 'ID_BODEGA', hidden: true },
                    { name: 'BODEGA', index: 'BODEGA', hidden: true },
                    { name: 'ID_SEDE', index: 'ID_SEDE', hidden: true },
                    { name: 'SEDE', index: 'SEDE', width: anchoP(ancho, 20) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('txtIdSede', datosRow.ID_SEDE + "-" + datosRow.SEDE, 0)
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;

        case 'listUsuarioBodega':
            ancho = 1000;
            valores_a_mandar = pag;
            valores_a_mandar = valores_a_mandar + "?idQuery=1003" + "&parametros=";
            add_valores_a_mandar(valorAtributo('txtId'));

            $('#drag' + ventanaActual.num).find("#" + arg).jqGrid({
                url: valores_a_mandar,
                datatype: 'xml',
                mtype: 'GET',
                colNames: ['TOTAL', 'ID_PERSONAL', 'ID_BODEGA', 'NOMBRE', 'ID_PROFESION', 'PROFESION'],
                colModel: [
                    { name: 'TOTAL', index: 'TOTAL', hidden: true },
                    { name: 'ID_PERSONAL', index: 'ID_PERSONAL', hidden: true },
                    { name: 'ID_BODEGA', index: 'ID_BODEGA', hidden: true },
                    { name: 'NOMBRE', index: 'NOMBRE', width: anchoP(ancho, 20) },
                    { name: 'ID_PROFESION', index: 'ID_PROFESION', hidden: true },
                    { name: 'PROFESION', index: 'PROFESION', width: anchoP(ancho, 20) },
                ],
                height: 100,
                width: 1040,
                onSelectRow: function (rowid) {
                    var datosRow = jQuery('#drag' + ventanaActual.num).find('#' + arg).getRowData(rowid);
                    asignaAtributo('lblIdGrupo', datosRow.ID_PERSONAL, 0);
                },
            });
            $('#drag' + ventanaActual.num).find("#" + arg).setGridParam({ url: valores_a_mandar }).trigger('reloadGrid');
            break;
    }
}