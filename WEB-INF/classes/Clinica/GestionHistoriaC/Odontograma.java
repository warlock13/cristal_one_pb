package Clinica.GestionHistoriaC;

import Clinica.Presentacion.OdontogramaVO;
import Sgh.AdminSeguridad.Usuario;
import Sgh.Utilidades.Conexion;
import Sgh.Utilidades.Constantes;
import Sgh.Utilidades.LoggableStatement;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Odontograma
{

   private String tipoIdPaciente;
   private String idPaciente;

   private int idDiente;
   private String v;
   private String m;
   private String l;
   private String d;
   private String o;
   private String bis;
   private String bii;
   private String sellante;
   private String sellanteXhacer;
   private String protesis;
   private String protesisXhacer;
   private String corona;
   private String coronaXhacer;
   private String endodoncia;
   private String endodonciaXhacer;
   private String exodonciaIndi;
   private String extraido;
   private String ausente;
   private String erupcion;

   private String idDienteElegTratam;
   private String idTratamDiente;
   private String tratamDiente;
   private int id_cita;
   private int numTrataHistorico;
   private String idTipoTratamiento;
   private int idEvolucion;
   private int id_estado_diente;
   private String descri_tratam_ejecut;

   private int cantDientesPlaca, superficiesPorDiente;
   private String observacionPlaca;
   private String observacionCops;
   private int cariados, obturados, perdidos, sanos, perdidosXCaries, presentes, cavitacional;

   private String[] arrayDientes;
   private Conexion cn;
   private StringBuffer sql = new StringBuffer();
   private int tamaño_tiene_trata=0;
      /*
     public void recibirOdontograma(String[] arreglDientes){
          this.arrayDientes=arreglDientes;
     }
               */

          //Metodos CRUD para funciones en BD
 public Object cargarOdontograma(){
        ArrayList<OdontogramaVO> resultado=new ArrayList<OdontogramaVO>();
        OdontogramaVO parametro;
        try{
        if(cn.isEstado()){
            sql.delete(0,sql.length());
            sql.append("SELECT d.id_diente, d.v, d.m, d.l, d.d, d.o, d.bis, d.bii, d.id_tratamiento  ");
            sql.append("FROM   odontologia.dentadura d INNER JOIN hc.paciente p on p.id=d.id_paciente ");
            sql.append("where d.id_paciente = ?::integer ");
            sql.append("and num_trata_historico = ? ");
            sql.append("and id_tipo_tratamiento = ? ");
            sql.append("order by id_diente");

            this.cn.prepareStatementSEL(Constantes.PS1,sql);
            cn.ps1.setString(1, this.idPaciente);
            cn.ps1.setInt(2,this.numTrataHistorico);
            cn.ps1.setString(3, this.idTipoTratamiento);

            System.out.println("ODONTOGRAMA="+((LoggableStatement)cn.ps1).getQueryString());
           if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  parametro = new OdontogramaVO();
//                System.out.println("id_diente= "+cn.rs1.getInt("id_diente"));
                  parametro.setIdDiente(cn.rs1.getInt("id_diente"));
                  parametro.setV(cn.rs1.getString("v") );
                  parametro.setM(cn.rs1.getString("m") );
                  parametro.setL(cn.rs1.getString("l") );
                  parametro.setD(cn.rs1.getString("d") );
                  parametro.setO(cn.rs1.getString("o") );
                  parametro.setBIS(cn.rs1.getString("bis") );
                  parametro.setBII(cn.rs1.getString("bii") );
                  parametro.setIdTratamDiente(cn.rs1.getString("id_tratamiento") );
                  resultado.add(parametro);
               }
                 cn.cerrarPS(Constantes.PS1);
             }


          }
       }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-cargarOdontograma-- --> SQLException --> "+e.getMessage());
        //   e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-cargarOdontograma--  --> Exception --> " + e.getMessage());
          // e.printStackTrace();
       }
        tamaño_tiene_trata=resultado.size();
        return  resultado;
    }

 public Object cargarOdontogramaPlaca(){
        ArrayList<OdontogramaVO> resultado=new ArrayList<OdontogramaVO>();
        OdontogramaVO parametro;
        try{
        if(cn.isEstado()){
            sql.delete(0,sql.length());
            sql.append("SELECT d.id_diente, d.v, d.m, d.l, d.d, d.o, d.bis, d.bii, d.id_tratamiento  ");
            sql.append("FROM   odontologia.dentadura d ");
            sql.append("where d.id_paciente = ?::integer ");
            sql.append("and num_trata_historico = ? ");
            sql.append("and id_tipo_tratamiento = ? ");
            sql.append("and id_evolucion = ?::integer ");
            sql.append("order by id_diente");

            this.cn.prepareStatementSEL(Constantes.PS1,sql);
            cn.ps1.setString(1, this.idPaciente);
            cn.ps1.setInt(2,this.numTrataHistorico);
            cn.ps1.setString(3, this.idTipoTratamiento);
            cn.ps1.setInt(4, this.idEvolucion);

            System.out.println("ODONTOPLACA="+((LoggableStatement)cn.ps1).getQueryString());
           if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  parametro = new OdontogramaVO();
//                System.out.println("id_diente= "+cn.rs1.getInt("id_diente"));
                  parametro.setIdDiente(cn.rs1.getInt("id_diente"));
                  parametro.setV(cn.rs1.getString("v") );
                  parametro.setM(cn.rs1.getString("m") );
                  parametro.setL(cn.rs1.getString("l") );
                  parametro.setD(cn.rs1.getString("d") );
                  parametro.setO(cn.rs1.getString("o") );
                  parametro.setBIS(cn.rs1.getString("bis") );
                  parametro.setBII(cn.rs1.getString("bii") );
                  parametro.setIdTratamDiente(cn.rs1.getString("id_tratamiento") );
                  resultado.add(parametro);
               }
                 cn.cerrarPS(Constantes.PS1);
             }


          }
       }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-cargarOdontogramaplaca-- --> SQLException --> "+e.getMessage());
        //   e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-cargarOdontogramaplaca--  --> Exception --> " + e.getMessage());
          // e.printStackTrace();
       }
        tamaño_tiene_trata=resultado.size();
        return  resultado;
    }


    public Object cargarOdontogramaIni(){
        ArrayList<OdontogramaVO> resultado=new ArrayList<OdontogramaVO>();
        OdontogramaVO parametro;
        try{
        if(cn.isEstado()){

            sql.delete(0,sql.length());
            sql.append("  SELECT  ");
            sql.append("    historia.diente_inicio.id_diente, ");
            sql.append("    historia.diente_inicio.id_paciente,  ");
            sql.append("    historia.diente_inicio.o, ");
            sql.append("    historia.diente_inicio.d, ");
            sql.append("    historia.diente_inicio.l, ");
            sql.append("    historia.diente_inicio.m, ");
            sql.append("    historia.diente_inicio.v, ");
            sql.append("    case when  adm.tratamiento_odontogram.id_tratam_odonto is null then '' else  adm.tratamiento_odontogram.id_tratam_odonto end as id_tratamiento, ");
            sql.append("    case when  adm.tratamiento_odontogram.des_tratam is null then '' else  adm.tratamiento_odontogram.des_tratam end as des_tratam ");
            sql.append("  FROM ");
            sql.append("   historia.diente_inicio ");
            sql.append("   left JOIN historia.diente_inicio_tratamiento ON (historia.diente_inicio.tipo_id=historia.diente_inicio_tratamiento.tipo_id) ");
            sql.append("    AND (historia.diente_inicio.id_paciente=historia.diente_inicio_tratamiento.id_paciente) ");
            sql.append("    AND (historia.diente_inicio.id_diente=historia.diente_inicio_tratamiento.id_diente) ");
            sql.append("   left JOIN adm.tratamiento_odontogram ON (historia.diente_inicio_tratamiento.id_tratamiento=adm.tratamiento_odontogram.id_tratam_odonto) ");
            sql.append("   WHERE ");
            sql.append("  historia.diente_inicio.tipo_id = ? AND ");
            sql.append("  historia.diente_inicio.id_paciente =? ");

            this.cn.prepareStatementSEL(Constantes.PS1,sql);
            cn.ps1.setString(1, this.tipoIdPaciente);
            cn.ps1.setString(2, this.idPaciente);

           // System.out.println(((LoggableStatement)cn.ps1).getQueryString());
           if(cn.selectSQL(Constantes.PS1)){
               while(cn.rs1.next()) {
                  parametro = new OdontogramaVO();
                  parametro.setIdDiente(cn.rs1.getInt("id_diente"));
                  parametro.setV(cn.rs1.getString("v") );
                  parametro.setM(cn.rs1.getString("m") );
                  parametro.setL(cn.rs1.getString("l") );
                  parametro.setD(cn.rs1.getString("d") );
                  parametro.setO(cn.rs1.getString("o") );
                  parametro.setIdTratamDiente(cn.rs1.getString("id_tratamiento") );
                  parametro.setTratamDiente(cn.rs1.getString("des_tratam") );
                  resultado.add(parametro);
               }
                 cn.cerrarPS(Constantes.PS1);
             }


          }
       }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-cargarOdontogramaIni-- --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-cargarOdontogramaIni--  --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }
        tamaño_tiene_trata=resultado.size();
        return  resultado;
    }


    public boolean consultaPlaca() {

        boolean placa = false;
        System.out.println("--------------placa-------------");

        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT  cantidad_dientes,  superficies_por_diente,  observaciones FROM   odontologia.placa  where id_evolucion = ? ");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, idEvolucion);

                System.out.println("SQL placa = " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        cantDientesPlaca = cn.rs2.getInt("cantidad_dientes");
                        superficiesPorDiente = cn.rs2.getInt("superficies_por_diente");
                        observacionPlaca = cn.rs2.getString("observaciones");
                        placa = true;
                        cn.cerrarPS(Constantes.PS2);
                        break;
                    }
                }

            }
        } catch (SQLException e) {
            System.out.println("Error --> odontologia.java-- function consultaPlaca --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> odontologia.java-- function consultaPlaca --> Exception --> " + e.getMessage());
        }
        return placa;
    }

    public boolean consultaCOPS() {

        boolean cops = false;
        System.out.println("--------------cops-------------");

        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append(" SELECT  cariados,  obturados,  perdidos, sanos, perdidos_caries, observaciones, presentes, cavitacional FROM odontologia.cops where id_evolucion = ? ");
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setInt(1, idEvolucion);

                System.out.println("SQL cops = " + ((LoggableStatement) cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        cariados = cn.rs2.getInt("cariados");
                        obturados = cn.rs2.getInt("obturados");
                        perdidos = cn.rs2.getInt("perdidos");
                        sanos = cn.rs2.getInt("sanos");
                        perdidosXCaries = cn.rs2.getInt("perdidos_caries");
                        observacionCops = cn.rs2.getString("observaciones");
                        presentes = cn.rs2.getInt("presentes");
                        cavitacional = cn.rs2.getInt("cavitacional");
                        cops = true;
                        cn.cerrarPS(Constantes.PS2);
                        break;
                    }
                }

            }
        } catch (SQLException e) {
            System.out.println("Error --> odontologia.java-- function consultaCOPS --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> odontologia.java-- function consultaCOPS --> Exception --> " + e.getMessage());
        }
        return cops;
    }


   /*
   guarda el odontograma de historicos en la tabla    historia.diente_historico
   */
   public boolean guardarOdontogramaInicioTratamientoHistorico(String[] dientes ){
       try{

               String [] temp = null;
               int contDient=0;

               for(int j=11;j<=85;j++){	  //85
		         if( (j>=11&&j<=18)|| (j>=41&&j<=48)|| (j>=21&&j<=28) || (j>=31&&j<=38) || (j>=51&&j<=55)  || (j>=81&&j<=85) || (j>=61&&j<=65) || (j>=71&&j<=75)   ){

                     // System.out.println(" -tipo id: "+this.tipoIdPaciente+" -idPacient: "+this.idPaciente+" -idDiente: "+dientes[contDient]);
                      temp = dientes[contDient].split("_");


                          sql.delete(0,sql.length());
                          sql.append("insert into historia.diente_historico(tipo_id,id_paciente,v,m,l,d,o,id_diente,id_cita,id_cita_inicio) VALUES(?,?,?,?,?,?,?,?,?,?); ");
                          cn.prepareStatementIDU(Constantes.PS2,sql);
                          cn.ps2.setString(1, this.tipoIdPaciente);
                          cn.ps2.setString(2, this.idPaciente);
                          cn.ps2.setString(3, temp[0]);
                          cn.ps2.setString(4, temp[1]);
                          cn.ps2.setString(5, temp[2]);
                          cn.ps2.setString(6, temp[3]);
                          cn.ps2.setString(7, temp[4]);
                          cn.ps2.setInt(8, j);
                          cn.ps2.setInt(9, this.id_cita);
                          cn.ps2.setInt(10, this.id_cita);
                          cn.iduSQL(Constantes.PS2);


                          if(!temp[5].equals("")){     // sellante
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "1");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[6].equals("")){     // sellanteXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "12");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[7].equals("") ){   // protesis
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "2");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                            // System.out.println(((LoggableStatement)cn.ps2).getQueryString());
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[8].equals("")){     // protesisXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "3");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[9].equals("")){    //  corona
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "4");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[10].equals("")){    // coronaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "5");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[11].equals("")){    // endodoncia
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "6");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[12].equals("")){      //endodonciaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "7");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[13].equals("")){      //exodonciaIndi
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "8");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[14].equals("")){      //extraido
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "9");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[15].equals("")){      //ausente
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "10");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[16].equals("")){      //erupcion
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "11");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                    contDient++;
                   }
                }
        }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-guardarOdontogramaInicioTratamientoHistorico-- --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-guardarOdontogramaInicioTratamientoHistorico--  --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }
      return cn.exito;
   }
   /*
     este metodo  inserta los registro odontograma de cada diente del paciente en la tabla historia.diente_inicio
     lugar donde inicia el tratamiento que en la ventana se lo ve como el check de odont inicial
   */
   public boolean guardarOdontogramaInicioTratamiento(String[] dientes ){
       try{

               String [] temp = null;
               int contDient=0;

               for(int j=11;j<=85;j++){	  //85
		         if( (j>=11&&j<=18)|| (j>=41&&j<=48)|| (j>=21&&j<=28) || (j>=31&&j<=38) || (j>=51&&j<=55)  || (j>=81&&j<=85) || (j>=61&&j<=65) || (j>=71&&j<=75)   ){

                     // System.out.println(" -tipo id: "+this.tipoIdPaciente+" -idPacient: "+this.idPaciente+" -idDiente: "+dientes[contDient]);
                      temp = dientes[contDient].split("_");


                          sql.delete(0,sql.length());
                          sql.append("insert into historia.diente_inicio(tipo_id,id_paciente,v,m,l,d,o,id_diente,id_cita) VALUES(?,?,?,?,?,?,?,?,?); ");
                          cn.prepareStatementIDU(Constantes.PS2,sql);
                          cn.ps2.setString(1, this.tipoIdPaciente);
                          cn.ps2.setString(2, this.idPaciente);
                          cn.ps2.setString(3, temp[0]);
                          cn.ps2.setString(4, temp[1]);
                          cn.ps2.setString(5, temp[2]);
                          cn.ps2.setString(6, temp[3]);
                          cn.ps2.setString(7, temp[4]);
                          cn.ps2.setInt(8, j);
                          cn.ps2.setInt(9, this.id_cita);

                          cn.iduSQL(Constantes.PS2);


                          if(!temp[5].equals("")){     // sellante
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "1");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[6].equals("")){     // sellanteXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "12");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);

                          }
                          if(!temp[7].equals("") ){   // protesis
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "2");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             //System.out.println(((LoggableStatement)cn.ps2).getQueryString());
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[8].equals("")){     // protesisXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "3");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[9].equals("")){    //  corona
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "4");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[10].equals("")){    // coronaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "5");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[11].equals("")){    // endodoncia
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "6");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[12].equals("")){      //endodonciaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "7");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[13].equals("")){      //exodonciaIndi
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "8");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[14].equals("")){      //extraido
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "9");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[15].equals("")){      //ausente
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "10");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[16].equals("")){      //erupcion
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_inicio_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente, id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "11");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5, this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                    contDient++;
                   }
                }
        }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-guardarOdontogramaInicioTratamiento-- --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-guardarOdontogramaInicioTratamiento--  --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }
      return cn.exito;
   }

/*
es llamado  que se necesita guardar el odontograma de un paciente
en la tabla de tratamiento inicial, historia.diente, historia_diente_inicio y historia.diente historico
*/
  public boolean recibirParaGuardarDentadura(String[] dientes ){
      try{

         if(cn.isEstado()){

              cn.iniciatransaccion();
              String [] temp = null;
              int contDient=0, j=0;

                 if(tamaño_tiene_trata==0){    // si no existen registros del odontograma entonces se inicia un nuevo tratamiento y se guarda odontograma inicial
                    guardarOdontogramaInicioTratamiento(dientes);            //para guardar   historia.diente_inicio
                    guardarOdontogramaInicioTratamientoHistorico(dientes);   //para guardar   historia.diente_inicio_historico
                 }


              sql.delete(0,sql.length());
              sql.append("delete from historia.diente_tratamiento where tipo_id=? AND id_paciente=?; ");      //se borran los registros para volver a ingresar nuevos
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);


              sql.delete(0,sql.length());
              sql.append("delete from historia.diente where tipo_id=? AND id_paciente=?; ");                 //se borran los registros para volver a ingresar nuevos
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);



                     // System.out.println(" -tipo id: "+this.tipoIdPaciente+" -idPacient: "+this.idPaciente+" -idDiente: "+dientes[contDient]);
                      temp = dientes[contDient].split("_");


                          sql.delete(0,sql.length());
                          sql.append("insert into historia.diente(tipo_id,id_paciente,v,m,l,d,o,id_diente,id_cita) VALUES(?,?,?,?,?,?,?,?,?); ");
                          cn.prepareStatementIDU(Constantes.PS2,sql);
                          cn.ps2.setString(1, this.tipoIdPaciente);
                          cn.ps2.setString(2, this.idPaciente);
                          cn.ps2.setString(3, temp[0]);
                          cn.ps2.setString(4, temp[1]);
                          cn.ps2.setString(5, temp[2]);
                          cn.ps2.setString(6, temp[3]);
                          cn.ps2.setString(7, temp[4]);
                          cn.ps2.setInt(8, j);
                          cn.ps2.setInt(9,this.id_cita);
                          cn.iduSQL(Constantes.PS2);

                          if(!temp[5].equals("")){     // sellante
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "1");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[6].equals("")){     // sellanteXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "12");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[7].equals("") ){   // protesis
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "2");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                            // System.out.println(((LoggableStatement)cn.ps2).getQueryString());
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[8].equals("")){     // protesisXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "3");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[9].equals("")){    //  corona
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "4");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[10].equals("")){    // coronaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "5");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[11].equals("")){    // endodoncia
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "6");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[12].equals("")){      //endodonciaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "7");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[13].equals("")){      //exodonciaIndi
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "8");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[14].equals("")){      //extraido
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "9");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[15].equals("")){      //ausente
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "10");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[16].equals("")){      //erupcion
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "11");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }


                    contDient++;
               cn.fintransaccion();
           }

               }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-recibirParaGuardarOdontograma-- --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-recibirParaGuardarOdontograma--  --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }

      return cn.exito;
   }


   public boolean recibirParaGuardarOdontograma(String[] dientes ){
      try{

         if(cn.isEstado()){

              cn.iniciatransaccion();
              String [] temp = null;
              int contDient=0;

                 if(tamaño_tiene_trata==0){    // si no existen registros del odontograma entonces se inicia un nuevo tratamiento y se guarda odontograma inicial
                    guardarOdontogramaInicioTratamiento(dientes);            //para guardar   historia.diente_inicio
                    guardarOdontogramaInicioTratamientoHistorico(dientes);   //para guardar   historia.diente_inicio_historico
                 }


              sql.delete(0,sql.length());
              sql.append("delete from historia.diente_tratamiento where tipo_id=? AND id_paciente=?; ");      //se borran los registros para volver a ingresar nuevos
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);


              sql.delete(0,sql.length());
              sql.append("delete from historia.diente where tipo_id=? AND id_paciente=?; ");                 //se borran los registros para volver a ingresar nuevos
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);



               for(int j=11;j<=85;j++){	  //85
		         if( (j>=11&&j<=18)|| (j>=41&&j<=48)|| (j>=21&&j<=28) || (j>=31&&j<=38) || (j>=51&&j<=55)  || (j>=81&&j<=85) || (j>=61&&j<=65) || (j>=71&&j<=75)   ){

                     // System.out.println(" -tipo id: "+this.tipoIdPaciente+" -idPacient: "+this.idPaciente+" -idDiente: "+dientes[contDient]);
                      temp = dientes[contDient].split("_");


                          sql.delete(0,sql.length());
                          sql.append("insert into historia.diente(tipo_id,id_paciente,v,m,l,d,o,id_diente,id_cita) VALUES(?,?,?,?,?,?,?,?,?); ");
                          cn.prepareStatementIDU(Constantes.PS2,sql);
                          cn.ps2.setString(1, this.tipoIdPaciente);
                          cn.ps2.setString(2, this.idPaciente);
                          cn.ps2.setString(3, temp[0]);
                          cn.ps2.setString(4, temp[1]);
                          cn.ps2.setString(5, temp[2]);
                          cn.ps2.setString(6, temp[3]);
                          cn.ps2.setString(7, temp[4]);
                          cn.ps2.setInt(8, j);
                          cn.ps2.setInt(9,this.id_cita);
                          cn.iduSQL(Constantes.PS2);

                          if(!temp[5].equals("")){     // sellante
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "1");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[6].equals("")){     // sellanteXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "12");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[7].equals("") ){   // protesis
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "2");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                            // System.out.println(((LoggableStatement)cn.ps2).getQueryString());
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[8].equals("")){     // protesisXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "3");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[9].equals("")){    //  corona
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "4");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[10].equals("")){    // coronaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "5");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[11].equals("")){    // endodoncia
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "6");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[12].equals("")){      //endodonciaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "7");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[13].equals("")){      //exodonciaIndi
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "8");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[14].equals("")){      //extraido
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "9");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[15].equals("")){      //ausente
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "10");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[16].equals("")){      //erupcion
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento(id_diente,id_tratamiento,tipo_id,id_paciente,id_cita) VALUES(?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "11");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }


                    contDient++;
                   }
                }
               cn.fintransaccion();
           }

               }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-recibirParaGuardarOdontograma-- --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-recibirParaGuardarOdontograma--  --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }

      return cn.exito;
   }

    /*para finalizar un tratamiento borra de dientes y tratamientos   y dientes_inicial y dientes_tratamietno_inicial
      y guarda en el historico de diente y tratamiento diente
    */
   public boolean recibirParaGuardarOdontogramaFinHistorico(String[] dientes ){
      try{
      int citaInicioTratam=0;

         if(cn.isEstado()){

              cn.iniciatransaccion();
              String [] temp = null;
              int contDient=0;

                 if(tamaño_tiene_trata==0){    // si no existen registros del odontograma entonces se inicia un nuevo tratamiento y se guarda odontograma inicial
                  //  guardarOdontogramaInicioTratamiento(dientes);
                   // guardarOdontogramaInicioTratamientoHistorico(dientes);
                 }

              sql.delete(0,sql.length());
              sql.append("delete from historia.diente_tratamiento where tipo_id=? AND id_paciente=?; ");
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);


              sql.delete(0,sql.length());
              sql.append("delete from historia.diente where tipo_id=? AND id_paciente=?; ");
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);


              sql.delete(0,sql.length());
              sql.append("delete from historia.diente_inicio_tratamiento where tipo_id=? AND id_paciente=?; ");
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);


              sql.delete(0,sql.length());
              sql.append("delete from historia.diente_inicio where tipo_id=? AND id_paciente=?; ");
              cn.prepareStatementIDU(Constantes.PS2,sql);
              cn.ps2.setString(1, this.tipoIdPaciente);
              cn.ps2.setString(2, this.idPaciente);
              cn.iduSQL(Constantes.PS2);



            sql.delete(0,sql.length());
            sql.append(" SELECT ");
            sql.append(" max(historia.diente_historico.id_cita) as citaInicioTrat   ");
            sql.append(" FROM  ");
            sql.append("   historia.diente_historico ");
            sql.append(" where  ");
            sql.append("   historia.diente_historico.tipo_id like ? and  ");
            sql.append("   historia.diente_historico.id_paciente like ? ");

            this.cn.prepareStatementSEL(Constantes.PS2,sql);
            cn.ps2.setString(1,this.tipoIdPaciente);
            cn.ps2.setString(2,this.idPaciente);

            if(cn.selectSQL(Constantes.PS2)){
               while(cn.rs2.next()) {
                  citaInicioTratam = cn.rs2.getInt("citaInicioTrat");   // para saber con que cita fue que inicio el tratamiento y luego guardan como termino
               }
                 cn.cerrarPS(Constantes.PS2);
             }
             System.out.println("citaInicioTratam= "+citaInicioTratam);

               for(int j=11;j<=85;j++){	  //85
		         if( (j>=11&&j<=18)|| (j>=41&&j<=48)|| (j>=21&&j<=28) || (j>=31&&j<=38) || (j>=51&&j<=55)  || (j>=81&&j<=85) || (j>=61&&j<=65) || (j>=71&&j<=75)   ){

                     // System.out.println(" -tipo id: "+this.tipoIdPaciente+" -idPacient: "+this.idPaciente+" -idDiente: "+dientes[contDient]);
                      temp = dientes[contDient].split("_");


                          sql.delete(0,sql.length());
                          sql.append("insert into historia.diente_historico(tipo_id,id_paciente,v,m,l,d,o,id_diente,tipo_histor,id_cita,id_cita_inicio) VALUES(?,?,?,?,?,?,?,?,?,?,?); ");
                          cn.prepareStatementIDU(Constantes.PS2,sql);
                          cn.ps2.setString(1, this.tipoIdPaciente);
                          cn.ps2.setString(2, this.idPaciente);
                          cn.ps2.setString(3, temp[0]);
                          cn.ps2.setString(4, temp[1]);
                          cn.ps2.setString(5, temp[2]);
                          cn.ps2.setString(6, temp[3]);
                          cn.ps2.setString(7, temp[4]);
                          cn.ps2.setInt(8, j);
                          cn.ps2.setInt(9,1);
                          cn.ps2.setInt(10,this.id_cita);
                          cn.ps2.setInt(11,citaInicioTratam);
                          cn.iduSQL(Constantes.PS2);

                          if(!temp[5].equals("")){     // sellante
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "1");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[6].equals("")){     // sellanteXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1,j);
                             cn.ps2.setString(2, "12");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[7].equals("") ){   // protesis
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "2");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                            // System.out.println(((LoggableStatement)cn.ps2).getQueryString());
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[8].equals("")){     // protesisXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "3");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[9].equals("")){    //  corona
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);  ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "4");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[10].equals("")){    // coronaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "5");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[11].equals("")){    // endodoncia
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "6");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[12].equals("")){      //endodonciaXhacer
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "7");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }

                          if(!temp[13].equals("")){      //exodonciaIndi
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "8");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[14].equals("")){      //extraido
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "9");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[15].equals("")){      //ausente
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "10");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }
                          if(!temp[16].equals("")){      //erupcion
                             sql.delete(0,sql.length());
                             sql.append("insert into historia.diente_tratamiento_historico(id_diente,id_tratamiento,tipo_id,id_paciente,tipo_histor,id_cita) VALUES(?,?,?,?,?,?);   ");
                             cn.prepareStatementIDU(Constantes.PS2,sql);
                             cn.ps2.setInt(1, j);
                             cn.ps2.setString(2, "11");
                             cn.ps2.setString(3, this.tipoIdPaciente);
                             cn.ps2.setString(4, this.idPaciente);
                             cn.ps2.setInt(5,1);
                             cn.ps2.setInt(6,this.id_cita);
                             cn.iduSQL(Constantes.PS2);
                          }


                    contDient++;
                   }
                }
               cn.fintransaccion();
           }

               }catch(SQLException e){
           System.out.println("Error --> clase  -->odontograma-recibirParaGuardarOdontogramaFinHistorico-- --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           System.out.println("Error --> clase  -->odontograma-recibirParaGuardarOdontogramaFinHistorico--  --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }

      return cn.exito;
   }

  //contar
    public int contar(){      // tienn que estar los mismos parametros de busqueda que en el Select de buscPag
     String var="";
     int cuantos=0;
     try{
        if(cn.isEstado()){
            sql.delete(0,sql.length());

            /*sql.append("SELECT ");
            sql.append("  historia.citas_medicas.fecha_hora_atencion, ");
            sql.append("  hdt.id_diente, ");
            sql.append("  hdt.id_estado AS id_estado_trata, ");
            sql.append("  adm.tratamiento_odontogram.des_tratam AS des_estado_trata, ");
            sql.append("  adm.profesi_medicos.nombres ||' '||  adm.profesi_medicos.apellidos as profesional , ");
            sql.append("  hdt.id_tratamiento, ");
            sql.append("  hdt.descr_trata_exe ");
            sql.append("FROM ");
            sql.append(" historia.diente_tratamiento_exe as hdt ");
            sql.append(" INNER JOIN historia.citas_medicas ON (hdt.id_paciente=historia.citas_medicas.id_paciente) ");
            sql.append("  AND (hdt.id_cita=historia.citas_medicas.id_cita) ");
            sql.append(" INNER JOIN adm.tratamiento_odontogram ON (hdt.id_estado=adm.tratamiento_odontogram.id_tratam_odonto) ");
            sql.append(" INNER JOIN adm.profesi_medicos ON (historia.citas_medicas.cod_tipodoc_medico=adm.profesi_medicos.cod_tipo_doc) ");
            sql.append("  AND (historia.citas_medicas.id_medico=adm.profesi_medicos.no_doc) ");
            sql.append("WHERE ");
            sql.append("  historia.citas_medicas.id_cita = ? ");    */


            sql.append(" SELECT ");
            sql.append(" count(citas_med.id_cita) as total ");
            sql.append(" FROM ");
            sql.append("  adm.profesi_medicos prof_med ");
            sql.append("  INNER JOIN historia.citas_medicas citas_med ON (prof_med.cod_tipo_doc=citas_med.cod_tipodoc_medico) ");
            sql.append("   AND (prof_med.no_doc=citas_med.id_medico) ");
            sql.append("  INNER JOIN historia.diente_tratamiento_exe trata_exec ON (citas_med.id_cita=trata_exec.id_cita) ");
            sql.append("  INNER JOIN adm.tratamiento_odontogram trata_odont ON (trata_exec.id_estado=trata_odont.id_tratam_odonto) ");
            sql.append("  INNER JOIN adm.tratamiento_odontogram ON (trata_exec.id_tratamiento=adm.tratamiento_odontogram.id_tratam_odonto) ");
            sql.append(" WHERE ");
            sql.append("  citas_med.cod_tiposerv = '2' AND ");
            sql.append("  trata_exec.tipo_id_pacie = ? AND ");
            sql.append("  trata_exec.id_paciente = ?  ");

            this.cn.prepareStatementSEL(Constantes.PS2,sql);
            cn.ps2.setString(1,this.tipoIdPaciente);
            cn.ps2.setString(2,this.idPaciente);
           // System.out.println(((LoggableStatement)cn.ps2).getQueryString());

            if(cn.selectSQL(Constantes.PS2)){
               while(cn.rs2.next()) {
                  cuantos = (cn.rs2.getInt("total"));
               }
                 cn.cerrarPS(Constantes.PS2);
             }
          }
            System.out.println("----------------------------------1----------------------------------------");

       }catch(SQLException e){
           //System.out.println("Error --> clase CentroSalud --> function contar --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           //System.out.println("Error --> clase Color --> function contar --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }
       return cuantos;
   }//Fin funcion contar()



      /* buscar paginado.
    Atentcion. postgresSQL 8.2 no soporta la sintaxis LIMIT n,m por eso se usa OFF*/

    public Object buscarPag(int sidx, String sord, int start, int limit){
     ArrayList<OdontogramaVO> resultado=new ArrayList<OdontogramaVO>();
     String var="";
     OdontogramaVO parametro;
     sord=sord.toUpperCase();
     var=(sord.equals("ASC"))?"ASC":"DESC";
     try{
        if(cn.isEstado()){
            sql.delete(0,sql.length());

            System.out.println("-----------------------------------2---------------------------------------");
            sql.append("SELECT  ");
            sql.append("  citas_med.fecha_hora_atencion, ");
            sql.append("  trata_exec.id_diente, ");
            sql.append("  trata_exec.id_estado, ");
            sql.append("  trata_odont.des_tratam AS des_estado, ");
            sql.append("    adm.tratamiento_odontogram.id_tratam_odonto, ");
            sql.append("    adm.tratamiento_odontogram.des_tratam, ");
            sql.append("  trata_exec.descr_trata_exe AS observa_trata_execut, ");
            sql.append("  prof_med.cod_tipo_doc AS tip_id_prof, ");
            sql.append("  prof_med.no_doc AS id_prof, ");
            sql.append("  prof_med.nombres||' ' || prof_med.apellidos AS profesional ");

            sql.append("FROM ");
            sql.append(" adm.profesi_medicos prof_med ");
            sql.append(" INNER JOIN historia.citas_medicas citas_med ON (prof_med.cod_tipo_doc=citas_med.cod_tipodoc_medico) ");
            sql.append("  AND (prof_med.no_doc=citas_med.id_medico) ");
            sql.append(" INNER JOIN historia.diente_tratamiento_exe trata_exec ON (citas_med.id_cita=trata_exec.id_cita) ");
            sql.append(" INNER JOIN adm.tratamiento_odontogram trata_odont ON (trata_exec.id_estado=trata_odont.id_tratam_odonto) ");
            sql.append(" INNER JOIN adm.tratamiento_odontogram ON (trata_exec.id_tratamiento=adm.tratamiento_odontogram.id_tratam_odonto) ");
            sql.append("WHERE ");
            sql.append("  citas_med.cod_tiposerv = '2' AND ");
            sql.append("  trata_exec.tipo_id_pacie = ? AND ");
            sql.append("  trata_exec.id_paciente = ?  ");
//            sql.append("ORDER BY citas_med.id_cita ");
//            sql.append("ORDER BY ");
//            sql.append(OrderByInt.getColumnNameFromIndex(sql, sidx));     // tipo_id = sidx=1 id_paciente = sidx=2
            sql.append(" "+var+" ");//sord
            sql.append("LIMIT ? OFFSET ? ");      // limit=cuantos offser=desde
            this.cn.prepareStatementSEL(Constantes.PS3,sql);
            cn.ps3.setString(1,this.tipoIdPaciente);
            cn.ps3.setString(2,this.idPaciente);
            cn.ps3.setInt(3, limit );
            cn.ps3.setInt(4, start );


          //  System.out.println(((LoggableStatement)cn.ps3).getQueryString());

            if(cn.selectSQL(Constantes.PS3)){
               while(cn.rs3.next()) {
                  parametro = new OdontogramaVO();
                     parametro.setFecha_hora_atencion(cn.rs3.getString("fecha_hora_atencion"));
                     parametro.setIdDiente(cn.rs3.getInt("id_diente"));
                     parametro.setId_estado_trata(cn.rs3.getString("id_estado"));      // id estado... como lo encontro el diente el odontologo
                     parametro.setDesc_estado_trata(cn.rs3.getString("des_estado"));
                     parametro.setIdTratamDiente(cn.rs3.getString("id_tratam_odonto"));  // id del tratamiento que trabajo el odontologo
                     parametro.setTratamDiente(cn.rs3.getString("des_tratam"));          // nombre del tratamiento que trabajo el odontologo
                     parametro.setObserTratamDiente(cn.rs3.getString("observa_trata_execut"));  // que observaciones le ha puesto tratamiento que trabajo el odontologo
                     parametro.setTipo_id_profes(cn.rs3.getString("tip_id_prof"));
                     parametro.setId_profes(cn.rs3.getString("id_prof"));
                     parametro.setNombreProfesional(cn.rs3.getString("profesional"));
                  resultado.add(parametro);
               }
                 cn.cerrarPS(Constantes.PS3);
             }
          }
            System.out.println("-----------------------------------2---------------------------------------");
       }catch(SQLException e){
           //System.out.println("Error --> clase Color --> function buscarPag --> SQLException --> "+e.getMessage());
           e.printStackTrace();
       }catch(Exception e){
           //System.out.println("Error --> clase Color --> function buscarPag --> Exception --> " + e.getMessage());
           e.printStackTrace();
       }
       return resultado;
   }//Fin funcion buscarPag()



   public Odontograma()
   {
          this.tipoIdPaciente="";
          this.idPaciente="";
           this.idDiente=0;
           this.v="";
           this.m="";
           this.l="";
           this.d="";
           this.o="";
           this.bis="";
           this.bii="";           
           this.sellante="";
           this.sellanteXhacer="";
           this.protesis="";
           this.protesisXhacer="";
           this.corona="";
           this.coronaXhacer="";
           this.endodoncia="";
           this.endodonciaXhacer="";
           this.exodonciaIndi="";
           this.extraido="";
           this.ausente="";
           this.erupcion="";
           this.idTratamDiente="";
           this.tratamDiente="";

           this.id_cita=0;
   }


    public java.lang.String getTipoIdPaciente() {
        return tipoIdPaciente;
    }

    public void setTipoIdPaciente(java.lang.String value) {
        tipoIdPaciente = value;
    }

    public java.lang.String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(java.lang.String value) {
        idPaciente = value;
    }

    public int getIdDiente() {
        return idDiente;
    }

    public void setIdDiente(int value) {
        idDiente = value;
    }

    public java.lang.String getV() {
        return v;
    }

    public void setV(java.lang.String value) {
        v = value;
    }

    public java.lang.String getM() {
        return m;
    }

    public void setM(java.lang.String value) {
        m = value;
    }

    public java.lang.String getL() {
        return l;
    }

    public void setL(java.lang.String value) {
        l = value;
    }

    public java.lang.String getD() {
        return d;
    }

    public void setD(java.lang.String value) {
        d = value;
    }

    public java.lang.String getO() {
        return o;
    }

    public void setO(java.lang.String value) {
        o = value;
    }

    public java.lang.String getBIS() {
        return bis;
    }

    public void setBIS(java.lang.String value) {
        bis = value;
    }

    public java.lang.String getBII() {
        return bii;
    }

    public void setBII(java.lang.String value) {
        bii = value;
    }

    public java.lang.String getSellante() {
        return sellante;
    }

    public void setSellante(java.lang.String value) {
        sellante = value;
    }

    public java.lang.String getSellanteXhacer() {
        return sellanteXhacer;
    }

    public void setSellanteXhacer(java.lang.String value) {
        sellanteXhacer = value;
    }

    public java.lang.String getProtesis() {
        return protesis;
    }

    public void setProtesis(java.lang.String value) {
        protesis = value;
    }

    public java.lang.String getProtesisXhacer() {
        return protesisXhacer;
    }

    public void setProtesisXhacer(java.lang.String value) {
        protesisXhacer = value;
    }

    public java.lang.String getCorona() {
        return corona;
    }

    public void setCorona(java.lang.String value) {
        corona = value;
    }

    public java.lang.String getCoronaXhacer() {
        return coronaXhacer;
    }

    public void setCoronaXhacer(java.lang.String value) {
        coronaXhacer = value;
    }

    public java.lang.String getEndodoncia() {
        return endodoncia;
    }

    public void setEndodoncia(java.lang.String value) {
        endodoncia = value;
    }

    public java.lang.String getEndodonciaXhacer() {
        return endodonciaXhacer;
    }

    public void setEndodonciaXhacer(java.lang.String value) {
        endodonciaXhacer = value;
    }

    public java.lang.String getExodonciaIndi() {
        return exodonciaIndi;
    }

    public void setExodonciaIndi(java.lang.String value) {
        exodonciaIndi = value;
    }

    public java.lang.String getExtraido() {
        return extraido;
    }

    public void setExtraido(java.lang.String value) {
        extraido = value;
    }

    public java.lang.String getAusente() {
        return ausente;
    }

    public void setAusente(java.lang.String value) {
        ausente = value;
    }

    public java.lang.String getErupcion() {
        return erupcion;
    }

    public void setErupcion(java.lang.String value) {
        erupcion = value;
    }

    public java.lang.String getIdDienteElegTratam() {
        return idDienteElegTratam;
    }

    public void setIdDienteElegTratam(java.lang.String value) {
        idDienteElegTratam = value;
    }

    public java.lang.String getIdTratamDiente() {
        return idTratamDiente;
    }

    public void setIdTratamDiente(java.lang.String value) {
        idTratamDiente = value;
    }

    public java.lang.String getTratamDiente() {
        return tratamDiente;
    }

    public void setTratamDiente(java.lang.String value) {
        tratamDiente = value;
    }

    public int getId_cita() {
        return id_cita;
    }

    public void setId_cita(int value) {
        id_cita = value;
    }

    public int getId_estado_diente() {
        return id_estado_diente;
    }

    public void setId_estado_diente(int value) {
        id_estado_diente = value;
    }

    public java.lang.String getDescri_tratam_ejecut() {
        return descri_tratam_ejecut;
    }

    public void setDescri_tratam_ejecut(java.lang.String value) {
        descri_tratam_ejecut = value;
    }

    public java.lang.String[] getArrayDientes() {
        return arrayDientes;
    }

    public void setArrayDientes(java.lang.String[] value) {
        arrayDientes = value;
    }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public java.lang.StringBuffer getSql() {
        return sql;
    }

    public void setSql(java.lang.StringBuffer value) {
        sql = value;
    }

    public int getTamaño_tiene_trata() {
        return tamaño_tiene_trata;
    }

    public void setTamaño_tiene_trata(int value) {
        tamaño_tiene_trata = value;
    }
    public int getNumTrataHistorico() {
        return numTrataHistorico;
    }

    public void setNumTrataHistorico(int value) {
        numTrataHistorico = value;
    }

    public java.lang.String getIdTipoTratamiento() {
        return idTipoTratamiento;
    }

    public void setIdTipoTratamiento(java.lang.String value) {
        idTipoTratamiento = value;
    }

    public int getIdEvolucion() {
        return idEvolucion;
    }

    public void setIdEvolucion(int value) {
        idEvolucion = value;
    }
    public int getCantDientesPlaca() {
        return cantDientesPlaca;
    }

    public void setCantDientesPlaca(int value) {
        cantDientesPlaca = value;
    }

    public int getSuperficiesPorDiente() {
        return superficiesPorDiente;
    }

    public void setSuperficiesPorDiente(int value) {
        superficiesPorDiente = value;
    }

    public java.lang.String getObservacionPlaca() {
        return observacionPlaca;
    }

    public void setObservacionPlaca(java.lang.String value) {
        observacionPlaca = value;
    }

    public int getcariados() {
        return cariados;
    }

    public void setcariados(int value) {
        cariados = value;
    }
    public int getobturados() {
        return obturados;
    }

    public void setobturados(int value) {
        obturados = value;
    }
    public int getperdidos() {
        return perdidos;
    }

    public void setperdidos(int value) {
        perdidos = value;
    }
    public int getsanos() {
        return sanos;
    }

    public void setsanos(int value) {
        sanos = value;
    }

    public int getperdidosXCaries() {
        return perdidosXCaries;
    }

    public void setperdidosXCaries(int value) {
        perdidosXCaries = value;
    }
    public java.lang.String getobservacionCops() {
        return observacionCops;
    }

    public void setobservacionCops(java.lang.String value) {
        observacionCops = value;
    }
    public int getpresentes() {
        return presentes;
    }

    public void setpresentes(int value) {
        presentes = value;
    }
    public int getcavitacional() {
        return cavitacional;
    }

    public void setcavitacional(int value) {
        cavitacional = value;
    }

}
