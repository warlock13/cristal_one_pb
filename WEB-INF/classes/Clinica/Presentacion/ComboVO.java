/**
  last: jas
* **/
package Clinica.Presentacion;

public class ComboVO
{

   private String id;
   private String descripcion;
   private String title;
   private String otro;

   private String orden, jerarquia, idPlantilla, referencia, resultado, observacion, tipoEntrada, valorPorDefecto, idQueryCombo,  var1, var2, var3, var4, var5;


   public ComboVO()
   {

   }
    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String value) {
        id = value;
    }

    public java.lang.String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(java.lang.String value) {
        descripcion = value;
    }

    public java.lang.String getTitle() {
        return title;
    }

    public void setTitle(java.lang.String value) {
        title = value;
    }

    public java.lang.String getOtro() {
        return otro;
    }

    public void setOtro(java.lang.String value) {

        otro = value;

    }
    public java.lang.String getVar1() {
        return var1;
    }

    public void setVar1(java.lang.String value) {
        var1 = value;
    }

    public java.lang.String getVar2() {
        return var2;
    }

    public void setVar2(java.lang.String value) {
        var2 = value;
    }

    public java.lang.String getVar3() {
        return var3;
    }

    public void setVar3(java.lang.String value) {
        var3 = value;
    }

    public java.lang.String getVar4() {
        return var4;
    }

    public void setVar4(java.lang.String value) {
        var4 = value;
    }

    public java.lang.String getVar5() {
        return var5;
    }

    public void setVar5(java.lang.String value) {
        var5 = value;
    }



    public java.lang.String getValorPorDefecto() {
        return valorPorDefecto;
    }

    public void setValorPorDefecto(java.lang.String value) {
        valorPorDefecto = value;
    }


    public java.lang.String getReferencia() {
        return referencia;
    }

    public void setReferencia(java.lang.String value) {
        referencia = value;
    }

    public java.lang.String getResultado() {
        return resultado;
    }

    public void setResultado(java.lang.String value) {
        resultado = value;
    }

    public java.lang.String getObservacion() {
        return observacion;
    }

    public void setObservacion(java.lang.String value) {
        observacion = value;
    }
    public java.lang.String getOrden() {
        return orden;
    }

    public void setOrden(java.lang.String value) {
        orden = value;
    }

    public java.lang.String getIdPlantilla() {
        return idPlantilla;
    }

    public void setIdPlantilla(java.lang.String value) {
        idPlantilla = value;
    }


    public java.lang.String getTipoEntrada() {
        return tipoEntrada.trim();
    }

    public void setTipoEntrada(java.lang.String value) {
        tipoEntrada = value.trim();
    }

    public java.lang.String getJerarquia() {
        return jerarquia;
    }

    public void setJerarquia(java.lang.String value) {
        jerarquia = value;
    }
    public java.lang.String getIdQueryCombo() {
        return idQueryCombo;
    }

    public void setIdQueryCombo(java.lang.String value) {
        idQueryCombo = value;
    }

}









