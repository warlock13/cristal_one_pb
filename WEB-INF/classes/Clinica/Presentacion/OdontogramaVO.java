package Clinica.Presentacion;

public class OdontogramaVO {
   private String tipoIdPaciente;
   private String idPaciente;
   private int idDiente;
   private String v;
   private String m;
   private String l;
   private String d;
   private String o;
   private String bis;
   private String bii;
   private String sellante;
   private String protesis;
   private String protesisXhacer;
   private String corona;
   private String coronaXhacer;
   private String endodoncia;
   private String endodonciaXhacer;
   private String exodonciaIndi;
   private String extraido;
   private String ausente;
   private String fecha_hora_atencion;
   private String erupcion;

   private String idTratamDiente;
   private String tratamDiente;
   private String obserTratamDiente;
   private int id_cita;
   private String id_estado_trata;
   private String desc_estado_trata;
   private String tipo_id_profes;
   private String id_profes;
   private String nombreProfesional;


  public OdontogramaVO(){

    tipoIdPaciente="";
    idPaciente="";
    v="";
    m="";
    l="";
    d="";
    o="";
    bis="";
    bii="";
    sellante="";
    protesis="";
    protesisXhacer="";
    corona="";
    coronaXhacer="";
    endodoncia="";
    endodonciaXhacer="";
    exodonciaIndi="";
    extraido="";
    ausente="";
    erupcion="";
    idTratamDiente="";
    tratamDiente="";
    obserTratamDiente="";
    id_cita=0;

    tipo_id_profes="";
    id_profes="";


  }


    public java.lang.String getTipoIdPaciente() {
        return tipoIdPaciente;
    }

    public void setTipoIdPaciente(java.lang.String value) {
        tipoIdPaciente = value;
    }

    public java.lang.String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(java.lang.String value) {
        idPaciente = value;
    }

    public java.lang.String getV() {
        return v;
    }

    public void setV(java.lang.String value) {
        v = value;
    }

    public java.lang.String getM() {
        return m;
    }

    public void setM(java.lang.String value) {
        m = value;
    }

    public java.lang.String getL() {
        return l;
    }

    public void setL(java.lang.String value) {
        l = value;
    }

    public java.lang.String getD() {
        return d;
    }

    public void setD(java.lang.String value) {
        d = value;
    }

    public java.lang.String getO() {
        return o;
    }

    public void setO(java.lang.String value) {
        o = value;
    }

    public java.lang.String getBIS() {
        return bis;
    }

    public void setBIS(java.lang.String value) {
        bis = value;
    }

    public java.lang.String getBII() {
        return bii;
    }

    public void setBII(java.lang.String value) {
        bii = value;
    }

    public java.lang.String getSellante() {
        return sellante;
    }

    public void setSellante(java.lang.String value) {
        sellante = value;
    }

    public java.lang.String getProtesis() {
        return protesis;
    }

    public void setProtesis(java.lang.String value) {
        protesis = value;
    }

    public java.lang.String getProtesisXhacer() {
        return protesisXhacer;
    }

    public void setProtesisXhacer(java.lang.String value) {
        protesisXhacer = value;
    }

    public java.lang.String getCorona() {
        return corona;
    }

    public void setCorona(java.lang.String value) {
        corona = value;
    }

    public java.lang.String getCoronaXhacer() {
        return coronaXhacer;
    }

    public void setCoronaXhacer(java.lang.String value) {
        coronaXhacer = value;
    }

    public java.lang.String getEndodoncia() {
        return endodoncia;
    }

    public void setEndodoncia(java.lang.String value) {
        endodoncia = value;
    }

    public java.lang.String getEndodonciaXhacer() {
        return endodonciaXhacer;
    }

    public void setEndodonciaXhacer(java.lang.String value) {
        endodonciaXhacer = value;
    }

    public java.lang.String getExodonciaIndi() {
        return exodonciaIndi;
    }

    public void setExodonciaIndi(java.lang.String value) {
        exodonciaIndi = value;
    }

    public java.lang.String getExtraido() {
        return extraido;
    }

    public void setExtraido(java.lang.String value) {
        extraido = value;
    }

    public java.lang.String getAusente() {
        return ausente;
    }

    public void setAusente(java.lang.String value) {
        ausente = value;
    }

    public java.lang.String getErupcion() {
        return erupcion;
    }

    public void setErupcion(java.lang.String value) {
        erupcion = value;
    }

    public int getIdDiente() {
        return idDiente;
    }

    public void setIdDiente(int value) {
        idDiente = value;
    }



    public java.lang.String getIdTratamDiente() {
        return idTratamDiente;
    }

    public void setIdTratamDiente(java.lang.String value) {
        idTratamDiente = value;
    }

    public int getId_cita() {
        return id_cita;
    }

    public void setId_cita(int value) {
        id_cita = value;
    }

    public java.lang.String getFecha_hora_atencion() {
        return fecha_hora_atencion;
    }

    public void setFecha_hora_atencion(java.lang.String value) {
        fecha_hora_atencion = value;
    }

    public java.lang.String getId_estado_trata() {
        return id_estado_trata;
    }

    public void setId_estado_trata(java.lang.String value) {
        id_estado_trata = value;
    }

    public java.lang.String getDesc_estado_trata() {
        return desc_estado_trata;
    }

    public void setDesc_estado_trata(java.lang.String value) {
        desc_estado_trata = value;
    }

    public java.lang.String getNombreProfesional() {
        return nombreProfesional;
    }

    public void setNombreProfesional(java.lang.String value) {
        nombreProfesional = value;
    }

    public java.lang.String getTipo_id_profes() {
        return tipo_id_profes;
    }

    public void setTipo_id_profes(java.lang.String value) {
        tipo_id_profes = value;
    }

    public java.lang.String getId_profes() {
        return id_profes;
    }

    public void setId_profes(java.lang.String value) {
        id_profes = value;
    }

    public java.lang.String getObserTratamDiente() {
        return obserTratamDiente;
    }

    public void setObserTratamDiente(java.lang.String value) {
        obserTratamDiente = value;
    }

    public java.lang.String getTratamDiente() {
        return tratamDiente;
    }

    public void setTratamDiente(java.lang.String value) {
        tratamDiente = value;
    }
}








