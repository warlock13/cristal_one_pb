/**
*@(#)Clase Conexion.java version 1.02 2015/12/09
*Copyright(c) 2015 Firmas Digital.
* JAS
*/
package Clinica.Presentacion.HistoriaClinica;

public class FacturacionVO {
   private Integer id;
   private String tipoId;
   private String Identificacion;
   private String Nombre1;
   private String Nombre2;
   private String Apellido1;
   private String Apellido2;
   private String idMunicipio;
   private String nomMunicipio;
   private String fechaNacimiento;
   private String edad;
   private String direccion;
   private String telefono;
   private String celular1;
   private String celular2;
   private String idEtnia;
   private String idNivelEscolar;
   private String idOcupacion;
   private String nomOcupacion;
   private String idEstrato;
   private String email;

   private String acompanante;
   private String idEstadoCivil;
   private String sexo;

   private String var1, var2, var3, var4, var5, var6, var7, var8;


   public FacturacionVO(){
     id =0;
     tipoId="";
     Identificacion="";
     Nombre1="";
     Nombre2="";
     Apellido1="";
     Apellido2="";
     idMunicipio="";
     nomMunicipio="";
     fechaNacimiento="";
     edad="";
     direccion="";
     telefono="";
     celular1="";
     celular2="";
     idEtnia="";
     idNivelEscolar="";
     idOcupacion="";
     nomOcupacion="";
     idEstrato="";

     email="";
     acompanante="";
     idEstadoCivil="";
     sexo="";
     var1=""; var2=""; var3=""; var4=""; var5=""; var6=""; var7=""; var8="";
    }


    public java.lang.Integer getId() {
        return id;
    }

    public void setId(java.lang.Integer value) {
        id = value;
    }

    public java.lang.String getTipoId() {
        return tipoId;
    }

    public void setTipoId(java.lang.String value) {
        tipoId = value;
    }

    public java.lang.String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(java.lang.String value) {
        Identificacion = value;
    }

    public java.lang.String getNombre1() {
        return Nombre1;
    }

    public void setNombre1(java.lang.String value) {
        Nombre1 = value;
    }

    public java.lang.String getNombre2() {
        return Nombre2;
    }

    public void setNombre2(java.lang.String value) {
        Nombre2 = value;
    }

    public java.lang.String getApellido1() {
        return Apellido1;
    }

    public void setApellido1(java.lang.String value) {
        Apellido1 = value;
    }

    public java.lang.String getApellido2() {
        return Apellido2;
    }

    public void setApellido2(java.lang.String value) {
        Apellido2 = value;
    }

    public java.lang.String getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(java.lang.String value) {
        idMunicipio = value;
    }

    public java.lang.String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(java.lang.String value) {
        fechaNacimiento = value;
    }

    public java.lang.String getDireccion() {
        return direccion;
    }

    public void setDireccion(java.lang.String value) {
        direccion = value;
    }

    public java.lang.String getTelefono() {
        return telefono;
    }

    public void setTelefono(java.lang.String value) {
        telefono = value;
    }

    public java.lang.String getEmail() {
        return email;
    }

    public void setEmail(java.lang.String value) {
        email = value;
    }

    public java.lang.String getIdOcupacion() {
        return idOcupacion;
    }

    public void setIdOcupacion(java.lang.String value) {
        idOcupacion = value;
    }

    public java.lang.String getAcompanante() {
        return acompanante;
    }

    public void setAcompanante(java.lang.String value) {
        acompanante = value;
    }

    public java.lang.String getIdEstadoCivil() {
        return idEstadoCivil;
    }

    public void setIdEstadoCivil(java.lang.String value) {
        idEstadoCivil = value;
    }

    public java.lang.String getEdad() {
        return edad;
    }

    public void setEdad(java.lang.String value) {
        edad = value;
    }

    public java.lang.String getNomMunicipio() {
        return nomMunicipio;
    }

    public void setNomMunicipio(java.lang.String value) {
        nomMunicipio = value;
    }

    public java.lang.String getSexo() {
        return sexo;
    }

    public void setSexo(java.lang.String value) {
        sexo = value;
    }

    public java.lang.String getCelular1() {
        return celular1;
    }

    public void setCelular1(java.lang.String value) {
        celular1 = value;
    }

    public java.lang.String getCelular2() {
        return celular2;
    }

    public void setCelular2(java.lang.String value) {
        celular2 = value;
    }

    public java.lang.String getIdEtnia() {
        return idEtnia;
    }

    public void setIdEtnia(java.lang.String value) {
        idEtnia = value;
    }

    public java.lang.String getIdNivelEscolar() {
        return idNivelEscolar;
    }

    public void setIdNivelEscolar(java.lang.String value) {
        idNivelEscolar = value;
    }

    public java.lang.String getIdEstrato() {
        return idEstrato;
    }

    public void setIdEstrato(java.lang.String value) {
        idEstrato = value;
    }

    public java.lang.String getVar1() {
        return var1;
    }

    public void setVar1(java.lang.String value) {
        var1 = value;
    }

    public java.lang.String getVar2() {
        return var2;
    }

    public void setVar2(java.lang.String value) {
        var2 = value;
    }

    public java.lang.String getVar3() {
        return var3;
    }

    public void setVar3(java.lang.String value) {
        var3 = value;
    }

    public java.lang.String getVar4() {
        return var4;
    }

    public void setVar4(java.lang.String value) {
        var4 = value;
    }

    public java.lang.String getVar5() {
        return var5;
    }

    public void setVar5(java.lang.String value) {
        var5 = value;
    }

    public java.lang.String getVar6() {
        return var6;
    }

    public void setVar6(java.lang.String value) {
        var6 = value;
    }

    public java.lang.String getVar7() {
        return var7;
    }

    public void setVar7(java.lang.String value) {
        var7 = value;
    }

    public java.lang.String getVar8() {
        return var8;
    }

    public void setVar8(java.lang.String value) {
        var8 = value;
    }
    public java.lang.String getNomOcupacion() {
        return nomOcupacion;
    }

    public void setNomOcupacion(java.lang.String value) {
        nomOcupacion = value;
    }

 }


