/* Decompiler 155ms, total 937ms, lines 293 */
package Clinica.Presentacion.HistoriaClinica;

public class PacienteVO {
   private Integer id = 0;
   private String tipoId = "";
   private String Identificacion = "";
   private String Nombre1 = "";
   private String Nombre2 = "";
   private String Apellido1 = "";
   private String Apellido2 = "";
   private String idMunicipio = "";
   private String nomMunicipio = "";
   private String fechaNacimiento = "";
   private String edad = "";
   private String direccion = "";
   private String telefono = "";
   private String celular1 = "";
   private String celular2 = "";
   private String idEtnia = "";
   private String idNivelEscolar = "";
   private String idOcupacion = "";
   private String nomOcupacion = "";
   private String idEstrato = "";
   private String email = "";
    private String id_tipo_regimen;
    private String tipo_regimen;
   private String acompanante = "";
   private String idEstadoCivil = "";
   private String sexo = "";
   private String var1 = "";
   private String var2 = "";
   private String var3 = "";
   private String var4 = "";
   private String var5 = "";
   private String var6 = "";
   private String var7 = "";
   private String var8 = "";

   public Integer getId() {
      return this.id;
   }

   public void setId(Integer var1x) {
      this.id = var1x;
   }

   public String getTipoId() {
      return this.tipoId;
   }

   public void setTipoId(String var1x) {
      this.tipoId = var1x;
   }

   public String getIdentificacion() {
      return this.Identificacion;
   }

   public void setIdentificacion(String var1x) {
      this.Identificacion = var1x;
   }

   public String getNombre1() {
      return this.Nombre1;
   }

   public void setNombre1(String var1x) {
      this.Nombre1 = var1x;
   }

   public String getNombre2() {
      return this.Nombre2;
   }

   public void setNombre2(String var1x) {
      this.Nombre2 = var1x;
   }

   public String getApellido1() {
      return this.Apellido1;
   }

   public void setApellido1(String var1x) {
      this.Apellido1 = var1x;
   }

   public String getApellido2() {
      return this.Apellido2;
   }

   public void setApellido2(String var1x) {
      this.Apellido2 = var1x;
   }

   public String getIdMunicipio() {
      return this.idMunicipio;
   }

   public void setIdMunicipio(String var1x) {
      this.idMunicipio = var1x;
   }

   public String getFechaNacimiento() {
      return this.fechaNacimiento;
   }

   public void setFechaNacimiento(String var1x) {
      this.fechaNacimiento = var1x;
   }

   public String getDireccion() {
      return this.direccion;
   }

   public void setDireccion(String var1x) {
      this.direccion = var1x;
   }

   public String getTelefono() {
      return this.telefono;
   }

   public void setTelefono(String var1x) {
      this.telefono = var1x;
   }

   public String getEmail() {
      return this.email;
   }

   public void setEmail(String var1x) {
      this.email = var1x;
   }

   public String getIdRegimen() {
        return this.id_tipo_regimen;
    }
    
    public void setIdRegimen(final String id_tipo_regimen) {
        this.id_tipo_regimen = id_tipo_regimen;
    }

    public String getTipoRegimen() {
        return this.tipo_regimen;
    }
    
    public void setTipoRegimen(final String tipo_regimen) {
        this.tipo_regimen = tipo_regimen;
    }

   public String getIdOcupacion() {
      return this.idOcupacion;
   }

   public void setIdOcupacion(String var1x) {
      this.idOcupacion = var1x;
   }

   public String getAcompanante() {
      return this.acompanante;
   }

   public void setAcompanante(String var1x) {
      this.acompanante = var1x;
   }

   public String getIdEstadoCivil() {
      return this.idEstadoCivil;
   }

   public void setIdEstadoCivil(String var1x) {
      this.idEstadoCivil = var1x;
   }

   public String getEdad() {
      return this.edad;
   }

   public void setEdad(String var1x) {
      this.edad = var1x;
   }

   public String getNomMunicipio() {
      return this.nomMunicipio;
   }

   public void setNomMunicipio(String var1x) {
      this.nomMunicipio = var1x;
   }

   public String getSexo() {
      return this.sexo;
   }

   public void setSexo(String var1x) {
      this.sexo = var1x;
   }

   public String getCelular1() {
      return this.celular1;
   }

   public void setCelular1(String var1x) {
      this.celular1 = var1x;
   }

   public String getCelular2() {
      return this.celular2;
   }

   public void setCelular2(String var1x) {
      this.celular2 = var1x;
   }

   public String getIdEtnia() {
      return this.idEtnia;
   }

   public void setIdEtnia(String var1x) {
      this.idEtnia = var1x;
   }

   public String getIdNivelEscolar() {
      return this.idNivelEscolar;
   }

   public void setIdNivelEscolar(String var1x) {
      this.idNivelEscolar = var1x;
   }

   public String getIdEstrato() {
      return this.idEstrato;
   }

   public void setIdEstrato(String var1x) {
      this.idEstrato = var1x;
   }

   public String getVar1() {
      return this.var1;
   }

   public void setVar1(String var1x) {
      this.var1 = var1x;
   }

   public String getVar2() {
      return this.var2;
   }

   public void setVar2(String var1x) {
      this.var2 = var1x;
   }

   public String getVar3() {
      return this.var3;
   }

   public void setVar3(String var1x) {
      this.var3 = var1x;
   }

   public String getVar4() {
      return this.var4;
   }

   public void setVar4(String var1x) {
      this.var4 = var1x;
   }

   public String getVar5() {
      return this.var5;
   }

   public void setVar5(String var1x) {
      this.var5 = var1x;
   }

   public String getVar6() {
      return this.var6;
   }

   public void setVar6(String var1x) {
      this.var6 = var1x;
   }

   public String getVar7() {
      return this.var7;
   }

   public void setVar7(String var1x) {
      this.var7 = var1x;
   }

   public String getVar8() {
      return this.var8;
   }

   public void setVar8(String var1x) {
      this.var8 = var1x;
   }

   public String getNomOcupacion() {
      return this.nomOcupacion;
   }

   public void setNomOcupacion(String var1x) {
      this.nomOcupacion = var1x;
   }
}
