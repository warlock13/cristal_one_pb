package Clinica.AdminSeguridad;

import Sgh.Utilidades.*;
import Clinica.Presentacion.*;

import java.io.*;
import java.text.*;
import java.util.*;
import java.lang.*;
import java.sql.*;
import java.lang.String;
import java.sql.*;

/**
 * **
 * por: jas
 */
public class Notificacion {

    private String idUsuarioSesion;
    private String id;
    private String descripcion;
    private String idEmisor;
    private String nomEmisor;
    private String tipo;
    private String estado;
    private String total;
    private Integer parametroA;
    private String parametro1;
    private String parametro2;
    private String parametro3;
    private String parametro4;
    private String parametro5;
    private String parametro6;

    private Conexion cn;
    private StringBuffer sql = new StringBuffer();

    public Notificacion() {
        idUsuarioSesion = "";
        id = "";
        descripcion = "";
        idEmisor = "";
        nomEmisor = "";
        tipo = "";
        estado = "";
        total = "";
        parametroA = 0;
        parametro1 = "";
        parametro2 = "";
        parametro3 = "";
        parametro4 = "";
        parametro5 = "";
        parametro6 = "";
    }

    public String notificacionEscrita() {
        this.total = "0";
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("\nSELECT count(id) total ");
                sql2.append("\nFROM notificaciones.notificacion_escrita ne  ");
                sql2.append("\nINNER JOIN notificaciones.notificacion_personal_destino npd on npd.id_notificacion = ne.id  ");
                sql2.append("\nWHERE npd.id_personal_destino = ? and npd.id_estado = 'S'  ");

                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                cn.ps2.setString(1, this.getIdUsuarioSesion());
                // System.out.println(((LoggableStatement)cn.ps2).getQueryString());
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        this.total = cn.rs2.getString("total");
                        //   System.out.println("---------------------------------this.total------------------------------------"+this.total);
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
                System.out.println("total = " + this.total);
            }
        } catch (SQLException e) {            //  System.out.println("Error --> Notificacion.java-- notificacionEscrita --> SQLException --> "+e.getMessage());
        } catch (Exception e) {               //   System.out.println("Error --> Notificacion.java-- notificacionEscrita--> Exception --> " + e.getMessage());
        }
        return this.total;
    }

    public String notificacionAlert(int idQuery) {
        System.out.println("Not Alert - Parametro1: " + parametro1);
        System.out.println("Not Alert - Parametro2: " + parametro2);
        System.out.println("Not Alert - Parametro3: " + parametro3);
        System.out.println("Not Alert - Parametro4: " + parametro4);
        System.out.println("Not Alert - Parametro5: " + parametro5);
        System.out.println("Not Alert - Parametro6: " + parametro6);
        System.out.println("Not Alert - IdQuery: " + idQuery);
        
        this.total = "";
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQueryNotificacion(idQuery));
                
                cn.ps2.setString(1, parametro1);
                if (!parametro2.equals("")) cn.ps2.setString(2, parametro2);
                if (!parametro3.equals("")) cn.ps2.setString(3, parametro3);
                if (!parametro4.equals("")) cn.ps2.setString(4, parametro4);
                if (!parametro5.equals("")) cn.ps2.setString(5, parametro5);
                if (!parametro6.equals("")) cn.ps2.setString(6, parametro6);
                

                System.out.println(((LoggableStatement) cn.ps2).getQueryString());


                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        this.total = "1";
                        if (cn.rs2.getString("mensaje") == null) {
                            this.descripcion = "";
                        } else {
                            this.descripcion = cn.rs2.getString("mensaje");
                        }

                    }
                }
                cn.cerrarPS(Constantes.PS2);
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->NOTIFICACION-- function notificacionAlert --> SQLException --> " + e.getMessage());

        } catch (Exception e) {
            System.out.println("Error --> clase  -->NOTIFICACION-- function notificacionAlert --> Exception --> " + e.getMessage());
        }
        return this.total;
    }
    
    public String escrita2(int idQuery) {
        System.out.println("notificacion Alert PARAMETRO1== " + this.getParametro1());
        System.out.println("notificacion Alert PARAMETRO2== " + this.getParametro2());
        System.out.println("notificacion Alert PARAMETRO3== " + this.getParametro3());
        System.out.println("notificacion Alert idQuery== " + idQuery);
        this.total = "";
        try {
            if (cn.isEstado()) {
                this.cn.prepareStatementSEL(Constantes.PS2, cn.traerElQueryNotificacion(idQuery));
                cn.ps2.setString(1, this.getParametro1());
                if (!this.getParametro2().equals("")) {
                    cn.ps2.setString(2, this.getParametro2());
                }
                if (!this.getParametro3().equals("")) {
                    cn.ps2.setString(3, this.getParametro3());
                }

                System.out.println(((LoggableStatement) cn.ps2).getQueryString());


                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        this.total = "1";
                        if (cn.rs2.getString("mensaje") == null) {
                            this.descripcion = "";
                        } else {
                            this.descripcion = cn.rs2.getString("mensaje");
                        }

                    }
                }
                cn.cerrarPS(Constantes.PS2);
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->NOTIFICACION-- function escrita2 --> SQLException --> " + e.getMessage());

        } catch (Exception e) {
            System.out.println("Error --> clase  -->NOTIFICACION-- function escrita2 --> Exception --> " + e.getMessage());
        }
        return this.total;
    }
    /* para cargar diccionario */
    public Object cargarDiccionario(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));

                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargar: =" + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }

                    /* System.out.println("FFF  "+nomColumnas.get(0) );
                       System.out.println("FFF  "+nomColumnas.get(1) );
                       System.out.println("FFF  "+nomColumnas.get(2) );  */
                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0))).trim());
                        parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargar --> SQLException --> " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargar --> Exception --> " + e.getMessage());
            // e.printStackTrace();
        }
        return lista;
    }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        if (value == null) {
            id = "%";
        } else {
            id = value.toUpperCase();
        }
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String value) {
        if (value == null) {
            descripcion = "%";
        } else {
            descripcion = value.toUpperCase();
        }

    }

    public java.lang.Integer getParametroA() {
        return parametroA;
    }

    public void setParametroA(java.lang.Integer value) {
        parametroA = value;
    }

    public java.lang.String getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(java.lang.String value) {
        idEmisor = value;
    }

    public java.lang.String getTipo() {
        return tipo;
    }

    public void setTipo(java.lang.String value) {
        tipo = value;
    }

    public java.lang.String getEstado() {
        return estado;
    }

    public void setEstado(java.lang.String value) {
        estado = value;
    }

    public java.lang.String getIdUsuarioSesion() {
        return idUsuarioSesion;
    }

    public void setIdUsuarioSesion(java.lang.String value) {
        idUsuarioSesion = value;
    }

    public java.lang.String getNomEmisor() {
        return nomEmisor;
    }

    public void setNomEmisor(java.lang.String value) {
        nomEmisor = value;
    }

    public java.lang.String getTotal() {
        return total;
    }

    public void setTotal(java.lang.String value) {
        total = value;
    }

    public java.lang.String getParametro1() {
        return parametro1;
    }

    public void setParametro1(java.lang.String value) {
        parametro1 = value;
    }

    public java.lang.String getParametro2() {
        return parametro2;
    }

    public void setParametro2(java.lang.String value) {
        parametro2 = value;
    }


    public String getParametro3() {
        return parametro3;
    }

    public void setParametro3(String value) {
        parametro3 = value;
    }

    public String getParametro4() {
        return parametro4;
    }

    public void setParametro4(String value) {
        parametro4 = value;
    }

    public String getParametro5() {
        return parametro5;
    }

    public void setParametro5(String value) {
        parametro5 = value;
    }

    public String getParametro6() {
        return parametro6;
    }

    public void setParametro6(String value) {
        parametro6 = value;
    }

    

    
}