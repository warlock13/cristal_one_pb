package Clinica.AdminSeguridad;

import Sgh.Utilidades.*;
import Clinica.Presentacion.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import java.lang.*;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.io.File;
import java.lang.String;
import java.sql.*;
//import Clinica.Utilidades.*;        constantes de sgh.utlidades

/****
 * por:
 */

public class Grilla {
    private int contador;
    private String id;
    private String nombre;
    private String idProceso;
    private String idTipo;
    private String condicion1;
    private String condicion2;
    private String condicion3;
    private String condicion4;
    private String condicion5;
    private String condicion6;
    public int numColumnas;

    private String parametros;

    public Constantes constantes;

    // public Fecha fecha;
    private Conexion cn;

    private StringBuffer sql = new StringBuffer();
    private StringBuffer sql2 = new StringBuffer();

    public Grilla() {
        contador = 0;
        id = "";
        nombre = "";
        idProceso = "";
        idTipo = "";
        numColumnas = 0;
        parametros = "";
        condicion1 = "";
        condicion2 = "";
    }

    public boolean modificarCRUD() {
        cn.exito = true;
        String[] temp = null;
        temp = parametros.split("_-");

        try {
            if (cn.isEstado()) {
                cn.iniciatransaccion();
                cn.prepareStatementIDU(Constantes.PS1, cn.traerElQuery(Integer.parseInt(this.id)));

                for (int i = 1; i < temp.length; i++) {
                    System.out.println("LENGTH= " + temp.length + "---------------------Temp" + i + "=" + temp[i]);
                    if (temp[i].equals("x.X.x"))
                        cn.ps1.setString(i, "");
                    else
                        cn.ps1.setString(i, temp[i]);
                }
                System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                cn.iduSQL(Constantes.PS1);
                cn.fintransaccion();
            }
        } catch (SQLException e) {
            System.out.println("1. CODIGO_POSTGRES" + e.getSQLState());
            System.out.println(e.getMessage() + " en metodo modificarCRUD() de grilla.java");
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " en metodo modificarCRUD() de grilla.java");
            return false;
        }
        return true;
    }

    public Object buscarDatosDelReporte() {
        ArrayList<GrillaVO> resultado = new ArrayList<GrillaVO>();
        ArrayList nomColumnas = new ArrayList();
        nomColumnas.clear();
        String[] temp = null;
        int totalParametrosQuery = 0;

        temp = parametros.split("_-");

        GrillaVO parametro;
        try {
            if (cn.isEstado()) {
                /*
                 * String palabra = "" + cn.traerElQuery(Integer.parseInt(this.id));
                 * 
                 * for(int i=0;i<palabra.length();++i){ if (palabra.charAt(i)=='?')
                 * totalParametrosQuery++; }
                 * System.out.println("*********totalParametrosQuery*******" +
                 * totalParametrosQuery);
                 */

                sql.delete(0, sql.length());
                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQuery(Integer.parseInt(this.id)));
                // cn.ps1.setString(1,temp[1]);
                // cn.ps1.setString(2,temp[2]);

                // System.out.println("----CANT PARAMETROS ENVIADOS= "+(temp.length-1));

                for (int i = 1; i < temp.length; i++) {
                    // System.out.println("-----------------PARAMETRO " + i+"= " + temp[i]);
                    if (temp[i].equals("x.X.x")) /* si llega x.X.x es porque esta vacio */
                        cn.ps1.setString(i, "%");
                    else
                        cn.ps1.setString(i, temp[i]);
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData(); /** PARA SACAR EL NUMERO DE COLUMNAS **/
                    this.numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < this.numColumnas + 1; i++) { /** PARA SACAR EL NOMBRE DE LAS COLUMNAS **/
                        nomColumnas.add(rsmd.getColumnName(i));
                    }
                    /**/
                    if (cn.imprimirConsola)
                        System.out.println(((LoggableStatement) cn.ps1).getQueryString());

                    while (cn.rs1.next()) {

                        parametro = new GrillaVO();
                        if (this.numColumnas >= 1) {
                            parametro.setC1(cn.rs1.getString(String.valueOf(nomColumnas
                                    .get(0)))); /** CADA NOMBRE DE COLUMNAS, SE INGRESARA EN CADA C1, C2.. **/
                        }
                        if (this.numColumnas >= 2) {
                            parametro.setC2(cn.rs1.getString(String.valueOf(nomColumnas.get(1))));
                        }
                        if (this.numColumnas >= 3) {
                            parametro.setC3(cn.rs1.getString(String.valueOf(nomColumnas.get(2))));
                        }
                        if (this.numColumnas >= 4) {
                            parametro.setC4(cn.rs1.getString(String.valueOf(nomColumnas.get(3))));
                        }
                        if (this.numColumnas >= 5) {
                            parametro.setC5(cn.rs1.getString(String.valueOf(nomColumnas.get(4))));
                        }
                        if (this.numColumnas >= 6) {
                            parametro.setC6(cn.rs1.getString(String.valueOf(nomColumnas.get(5))));
                        }
                        if (this.numColumnas >= 7) {
                            parametro.setC7(cn.rs1.getString(String.valueOf(nomColumnas.get(6))));
                        }
                        if (this.numColumnas >= 8) {
                            parametro.setC8(cn.rs1.getString(String.valueOf(nomColumnas.get(7))));
                        }
                        if (this.numColumnas >= 9) {
                            parametro.setC9(cn.rs1.getString(String.valueOf(nomColumnas.get(8))));
                        }
                        if (this.numColumnas >= 10) {
                            parametro.setC10(cn.rs1.getString(String.valueOf(nomColumnas.get(9))));
                        }
                        if (this.numColumnas >= 11) {
                            parametro.setC11(cn.rs1.getString(String.valueOf(nomColumnas.get(10))));
                        }
                        if (this.numColumnas >= 12) {
                            parametro.setC12(cn.rs1.getString(String.valueOf(nomColumnas.get(11))));
                        }
                        if (this.numColumnas >= 13) {
                            parametro.setC13(cn.rs1.getString(String.valueOf(nomColumnas.get(12))));
                        }
                        if (this.numColumnas >= 14) {
                            parametro.setC14(cn.rs1.getString(String.valueOf(nomColumnas.get(13))));
                        }
                        if (this.numColumnas >= 15) {
                            parametro.setC15(cn.rs1.getString(String.valueOf(nomColumnas.get(14))));
                        }
                        if (this.numColumnas >= 16) {
                            parametro.setC16(cn.rs1.getString(String.valueOf(nomColumnas.get(15))));
                        }
                        if (this.numColumnas >= 17) {
                            parametro.setC17(cn.rs1.getString(String.valueOf(nomColumnas.get(16))));
                        }
                        if (this.numColumnas >= 18) {
                            parametro.setC18(cn.rs1.getString(String.valueOf(nomColumnas.get(17))));
                        }
                        if (this.numColumnas >= 19) {
                            parametro.setC19(cn.rs1.getString(String.valueOf(nomColumnas.get(18))));
                        }
                        if (this.numColumnas >= 20) {
                            parametro.setC20(cn.rs1.getString(String.valueOf(nomColumnas.get(19))));
                        }
                        if (this.numColumnas >= 21) {
                            parametro.setC21(cn.rs1.getString(String.valueOf(nomColumnas.get(20))));
                        }
                        if (this.numColumnas >= 22) {
                            parametro.setC22(cn.rs1.getString(String.valueOf(nomColumnas.get(21))));
                        }
                        if (this.numColumnas >= 23) {
                            parametro.setC23(cn.rs1.getString(String.valueOf(nomColumnas.get(22))));
                        }
                        if (this.numColumnas >= 24) {
                            parametro.setC24(cn.rs1.getString(String.valueOf(nomColumnas.get(23))));
                        }
                        if (this.numColumnas >= 25) {
                            parametro.setC25(cn.rs1.getString(String.valueOf(nomColumnas.get(24))));
                        }
                        if (this.numColumnas >= 26) {
                            parametro.setC26(cn.rs1.getString(String.valueOf(nomColumnas.get(25))));
                        }
                        if (this.numColumnas >= 27) {
                            parametro.setC27(cn.rs1.getString(String.valueOf(nomColumnas.get(26))));
                        }
                        if (this.numColumnas >= 28) {
                            parametro.setC28(cn.rs1.getString(String.valueOf(nomColumnas.get(27))));
                        }
                        if (this.numColumnas >= 29) {
                            parametro.setC29(cn.rs1.getString(String.valueOf(nomColumnas.get(28))));
                        }
                        if (this.numColumnas >= 30) {
                            parametro.setC30(cn.rs1.getString(String.valueOf(nomColumnas.get(29))));
                        }

                        if (this.numColumnas >= 31) {
                            parametro.setC31(cn.rs1.getString(String.valueOf(nomColumnas.get(30))));
                        }
                        if (this.numColumnas >= 32) {
                            parametro.setC32(cn.rs1.getString(String.valueOf(nomColumnas.get(31))));
                        }
                        if (this.numColumnas >= 33) {
                            parametro.setC33(cn.rs1.getString(String.valueOf(nomColumnas.get(32))));
                        }
                        if (this.numColumnas >= 34) {
                            parametro.setC34(cn.rs1.getString(String.valueOf(nomColumnas.get(33))));
                        }
                        if (this.numColumnas >= 35) {
                            parametro.setC35(cn.rs1.getString(String.valueOf(nomColumnas.get(34))));
                        }
                        if (this.numColumnas >= 36) {
                            parametro.setC36(cn.rs1.getString(String.valueOf(nomColumnas.get(35))));
                        }
                        if (this.numColumnas >= 37) {
                            parametro.setC37(cn.rs1.getString(String.valueOf(nomColumnas.get(36))));
                        }
                        if (this.numColumnas >= 38) {
                            parametro.setC38(cn.rs1.getString(String.valueOf(nomColumnas.get(37))));
                        }
                        if (this.numColumnas >= 39) {
                            parametro.setC39(cn.rs1.getString(String.valueOf(nomColumnas.get(38))));
                        }
                        if (this.numColumnas >= 40) {
                            parametro.setC40(cn.rs1.getString(String.valueOf(nomColumnas.get(39))));
                        }
                        if (this.numColumnas >= 41) {
                            parametro.setC41(cn.rs1.getString(String.valueOf(nomColumnas.get(40))));
                        }
                        if (this.numColumnas >= 42) {
                            parametro.setC42(cn.rs1.getString(String.valueOf(nomColumnas.get(41))));
                        }
                        if (this.numColumnas >= 43) {
                            parametro.setC43(cn.rs1.getString(String.valueOf(nomColumnas.get(42))));
                        }
                        if (this.numColumnas >= 44) {
                            parametro.setC44(cn.rs1.getString(String.valueOf(nomColumnas.get(43))));
                        }
                        if (this.numColumnas >= 45) {
                            parametro.setC45(cn.rs1.getString(String.valueOf(nomColumnas.get(44))));
                        }
                        if (this.numColumnas >= 46) {
                            parametro.setC46(cn.rs1.getString(String.valueOf(nomColumnas.get(45))));
                        }
                        if (this.numColumnas >= 47) {
                            parametro.setC47(cn.rs1.getString(String.valueOf(nomColumnas.get(46))));
                        }
                        if (this.numColumnas >= 48) {
                            parametro.setC48(cn.rs1.getString(String.valueOf(nomColumnas.get(47))));
                        }
                        if (this.numColumnas >= 49) {
                            parametro.setC49(cn.rs1.getString(String.valueOf(nomColumnas.get(48))));
                        }
                        if (this.numColumnas >= 50) {
                            parametro.setC50(cn.rs1.getString(String.valueOf(nomColumnas.get(49))));
                        }
                        if (this.numColumnas >= 51) {
                            parametro.setC51(cn.rs1.getString(String.valueOf(nomColumnas.get(50))));
                        }
                        if (this.numColumnas >= 52) {
                            parametro.setC51(cn.rs1.getString(String.valueOf(nomColumnas.get(51))));
                        }
                        if (this.numColumnas >= 53) {
                            parametro.setC52(cn.rs1.getString(String.valueOf(nomColumnas.get(52))));
                        }
                        if (this.numColumnas >= 54) {
                            parametro.setC53(cn.rs1.getString(String.valueOf(nomColumnas.get(53))));
                        }
                        if (this.numColumnas >= 55) {
                            parametro.setC54(cn.rs1.getString(String.valueOf(nomColumnas.get(54))));
                        }
                        if (this.numColumnas >= 56) {
                            parametro.setC55(cn.rs1.getString(String.valueOf(nomColumnas.get(55))));
                        }
                        if (this.numColumnas >= 57) {
                            parametro.setC56(cn.rs1.getString(String.valueOf(nomColumnas.get(56))));
                        }
                        if (this.numColumnas >= 58) {
                            parametro.setC57(cn.rs1.getString(String.valueOf(nomColumnas.get(57))));
                        }
                        if (this.numColumnas >= 59) {
                            parametro.setC58(cn.rs1.getString(String.valueOf(nomColumnas.get(58))));
                        }
                        if (this.numColumnas >= 60) {
                            parametro.setC59(cn.rs1.getString(String.valueOf(nomColumnas.get(59))));
                        }
                        resultado.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Grilla --> function buscarDatosDelReporte --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Grilla --> function buscarDatosDelReporte --> Exception --> " + e.getMessage());
        }
        return resultado;
    }

    public Object buscarGrillaVentana() {
        ArrayList<GrillaVO> resultado = new ArrayList<GrillaVO>();
        ArrayList nomColumnas = new ArrayList();
        nomColumnas.clear();
        String[] temp = null;
        int totalParametrosQuery = 0;

        temp = parametros.split("_-");

        GrillaVO parametro;
        try {
            if (cn.isEstado()) {

                sql.delete(0, sql.length());
                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryVentana(Integer.parseInt(this.id)));

                for (int i = 1; i < temp.length; i++) {
                    if (temp[i].equals("x.X.x")) /* si llega x.X.x es porque esta vacio */
                        cn.ps1.setString(i, "%");
                    else
                        cn.ps1.setString(i, temp[i]);
                }

                // if(!this.condicion1.equals(""))
                // cn.ps1.setString(3,this.condicion1);

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData(); /** PARA SACAR EL NUMERO DE COLUMNAS **/
                    this.numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < this.numColumnas + 1; i++) { /** PARA SACAR EL NOMBRE DE LAS COLUMNAS **/
                        nomColumnas.add(rsmd.getColumnName(i));
                    }
                    /**/
                    if (cn.imprimirConsola)
                        System.out.println(((LoggableStatement) cn.ps1).getQueryString());

                    while (cn.rs1.next()) {

                        parametro = new GrillaVO();
                        if (this.numColumnas >= 1) {
                            parametro.setC1(cn.rs1.getString(String.valueOf(nomColumnas
                                    .get(0)))); /** CADA NOMBRE DE COLUMNAS, SE INGRESARA EN CADA C1, C2.. **/
                        }
                        if (this.numColumnas >= 2) {
                            parametro.setC2(cn.rs1.getString(String.valueOf(nomColumnas.get(1))));
                        }
                        if (this.numColumnas >= 3) {
                            parametro.setC3(cn.rs1.getString(String.valueOf(nomColumnas.get(2))));
                        }
                        if (this.numColumnas >= 4) {
                            parametro.setC4(cn.rs1.getString(String.valueOf(nomColumnas.get(3))));
                        }
                        if (this.numColumnas >= 5) {
                            parametro.setC5(cn.rs1.getString(String.valueOf(nomColumnas.get(4))));
                        }
                        if (this.numColumnas >= 6) {
                            parametro.setC6(cn.rs1.getString(String.valueOf(nomColumnas.get(5))));
                        }
                        if (this.numColumnas >= 7) {
                            parametro.setC7(cn.rs1.getString(String.valueOf(nomColumnas.get(6))));
                        }
                        if (this.numColumnas >= 8) {
                            parametro.setC8(cn.rs1.getString(String.valueOf(nomColumnas.get(7))));
                        }
                        if (this.numColumnas >= 9) {
                            parametro.setC9(cn.rs1.getString(String.valueOf(nomColumnas.get(8))));
                        }
                        if (this.numColumnas >= 10) {
                            parametro.setC10(cn.rs1.getString(String.valueOf(nomColumnas.get(9))));
                        }

                        resultado.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase Grilla --> function buscarDatosDelReporte --> SQLException --> " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            System.out.println(
                    "Error --> clase Grilla --> function buscarDatosDelReporte --> Exception --> " + e.getMessage());
            // e.printStackTrace();
        }
        return resultado;
    }

    public int maximoValorIdTabla(String nombreTabla) {
        int valor = 0;
        StringBuffer sql2 = new StringBuffer();
        try {
            if (cn.isEstado()) {
                sql2.delete(0, sql2.length());
                sql2.append("select MAX(id)+1 as maximo from " + nombreTabla);
                this.cn.prepareStatementSEL(Constantes.PS2, sql2);
                if (cn.selectSQL(Constantes.PS2)) {
                    while (cn.rs2.next()) {
                        valor = cn.rs2.getInt("maximo");
                        cn.cerrarPS(Constantes.PS2);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(
                    "Error --> clase  -->Grilla-- function maximoValor --> SQLException --> " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase  -->Grilla-- function maximoValor --> Exception --> " + e.getMessage());
            e.printStackTrace();
        }
        return valor;
    }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int value) {
        contador = value;
    }

    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String value) {
        id = value;
    }

    public java.lang.String getNombre() {
        return nombre;
    }

    public void setNombre(java.lang.String value) {
        nombre = value;
    }

    public java.lang.String getIdProceso() {
        return idProceso;
    }

    public void setIdProceso(java.lang.String value) {
        idProceso = value;
    }

    public java.lang.String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(java.lang.String value) {
        idTipo = value;
    }

    public int getNumColumnas() {
        return numColumnas;
    }

    public void setNumColumnas(int value) {
        numColumnas = value;
    }

    public java.lang.String getParametros() {
        return parametros;
    }

    public void setParametros(java.lang.String value) {
        parametros = value;
    }

    public java.lang.String getCondicion1() {
        return condicion1;
    }

    public void setCondicion1(java.lang.String value) {
        condicion1 = value;
    }

    public java.lang.String getCondicion2() {
        return condicion2;
    }

    public void setCondicion2(java.lang.String value) {
        condicion2 = value;
    }

    public java.lang.String getCondicion3() {
        return condicion3;
    }

    public void setCondicion3(java.lang.String value) {
        condicion3 = value;
    }
}