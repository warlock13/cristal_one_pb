package Clinica.AdminSeguridad;

import Clinica.Presentacion.ComboVO;
import Sgh.Utilidades.Conexion;
import Sgh.Utilidades.Constantes;
import Sgh.Utilidades.LoggableStatement;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @(#)Clase Conexion.java version 2.0 2019/11/01 Copyright(c) 2019 Firmas
 * Digitales.
 */
public class Combo {

    private String id;
    private String descripcion;
    private String title;
    private String otro;
    private String condicion1;
    private String condicion2;
    private String condicion3;
    private String condicion4;
    private String condicion5;
    private String condicion6;

    public ComboVO comboVO;

    public Constantes constantes;

    //public Fecha fecha;
    private Conexion cn;

    private StringBuffer sql = new StringBuffer();

    public Combo() {
        id = "";
        descripcion = "";
        title = "";
        otro = "";
        condicion1 = "";
        condicion2 = "";
        condicion3 = "";
        condicion4 = "";
        condicion5 = "";
        condicion6 = "";

        comboVO = new ComboVO();
    }

    public Object cargar(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));

                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargar:: =" + idQuery);
                    //  System.out.println(((LoggableStatement)cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }

                    /* System.out.println("FFF  "+nomColumnas.get(0) );
                       System.out.println("FFF  "+nomColumnas.get(1) );
                       System.out.println("FFF  "+nomColumnas.get(2) );  */
                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0))).trim());
                        parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargar --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargar --> Exception --> " + e.getMessage());
        }
        return lista;
    }

    public Object cargarCodicion1(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, this.condicion1);

                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargarCodicion1 =" + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }

                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                        parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargar --> SQLException --> " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargar --> Exception --> " + e.getMessage());
            // e.printStackTrace();
        }
        return lista;
    }

    public Object cargarPlantillaFolio(int idQuery, String condicion) {

        //  System.out.println("xxxxxxxxxxxxidQueryxxxxx "+idQuery);
        //  System.out.println("xxxxxxxxxxxxcondicion1xxxxx "+this.condicion1);
        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, condicion);

                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargarCodicion1 =" + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {
                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }
                    /* System.out.println("FFF  "+nomColumnas.get(0) );
                       System.out.println("FFF  "+nomColumnas.get(1) );
                       System.out.println("FFF  "+nomColumnas.get(2) );  */

                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        parametro.setOrden(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()).trim());
                        parametro.setIdPlantilla(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()).trim());
                        parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()).trim());
                        parametro.setResultado(cn.rs1.getString(String.valueOf(nomColumnas.get(3)).trim()).trim());
                        parametro.setTipoEntrada(cn.rs1.getString(String.valueOf(nomColumnas.get(4)).trim()).trim());
                        parametro.setJerarquia(cn.rs1.getString(String.valueOf(nomColumnas.get(5)).trim()).trim());
                        parametro.setIdQueryCombo(cn.rs1.getString(String.valueOf(nomColumnas.get(6)).trim()).trim());
                        parametro.setVar1(cn.rs1.getString(String.valueOf(nomColumnas.get(7)).trim()).trim());
                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargarDatosEdiablesCondicion1 --> SQLException --> " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargarDatosEdiablesCondicion1 --> Exception --> " + e.getMessage());
            // e.printStackTrace();
        }
        return lista;
    }

    public Object cargarCodicion2(int idQuery) {
        System.out.println("idQueeeeeeeeeeeee 1 ");

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {
                System.out.println("idQueeeeeeeeeeeee 2 ");

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, this.condicion1);
                cn.ps1.setString(2, this.condicion2);

                //   if(cn.imprimirConsola){                        System.out.println("idQueeeeeeeeeeeee 3 ");
                System.out.println("idQuery cargarCodicion2 =" + idQuery);
                System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                //    }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }

                    /* System.out.println("FFF  "+nomColumnas.get(0) );
                       System.out.println("FFF  "+nomColumnas.get(1) );
                       System.out.println("FFF  "+nomColumnas.get(2) );  */
                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                        parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion2 --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion2 --> Exception --> " + e.getMessage());
        }
        return lista;
    }

    public Object cargarCodicionUno(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, this.condicion1);

                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargarCodicion1 =" + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }

                    /* System.out.println("FFF  "+nomColumnas.get(0) );
                       System.out.println("FFF  "+nomColumnas.get(1) );
                       System.out.println("FFF  "+nomColumnas.get(2) );  */
                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        if (numColumnas == 1) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                        }

                        if (numColumnas == 2) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        }
                        if (numColumnas == 3) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                            parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        }

                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion2 --> SQLException --> " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion2 --> Exception --> " + e.getMessage());
            // e.printStackTrace();
        }
        return lista;
    }

    public Object cargarCodicionDos(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, this.condicion1);
                cn.ps1.setString(2, this.condicion2);

                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargarCodicionDos =" + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }

                    /* System.out.println("FFF  "+nomColumnas.get(0) );
                       System.out.println("FFF  "+nomColumnas.get(1) );
                       System.out.println("FFF  "+nomColumnas.get(2) );  */
                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        if (numColumnas == 1) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                        }

                        if (numColumnas == 2) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                          parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        }
                        if (numColumnas == 3) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                            parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        }

                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion2 --> SQLException --> " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion2 --> Exception --> " + e.getMessage());
            // e.printStackTrace();
        }
        return lista;
    }

    public Object cargarCodicionTres(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                this.cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, this.condicion1);
                cn.ps1.setString(2, this.condicion2);
                cn.ps1.setString(3, this.condicion3);

                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargarCodicion3 =" + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                    /**
                     * PARA SACAR EL NUMERO DE COLUMNAS*
                     */
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        /**
                         * PARA SACAR EL NOMBRE DE LAS COLUMNAS*
                         */
                        nomColumnas.add(rsmd.getColumnName(i));
                        // System.out.println("Error  "+rsmd.getColumnName(i));
                    }

                    /* System.out.println("FFF  "+nomColumnas.get(0) );
                       System.out.println("FFF  "+nomColumnas.get(1) );
                       System.out.println("FFF  "+nomColumnas.get(2) );  */
                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        if (numColumnas == 1) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                        }

                        if (numColumnas == 2) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        }
                        if (numColumnas == 3) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                            parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        }

                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion3 --> SQLException --> " + e.getMessage());
            // e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargarCodicion3 --> Exception --> " + e.getMessage());
            // e.printStackTrace();
        }
        return lista;
    }
    
    
    public Object cargarCondicionCuatro(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, condicion1);
                cn.ps1.setString(2, condicion2);
                cn.ps1.setString(3, condicion3);                
                cn.ps1.setString(4, condicion4);


                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargarCondicion4: " + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                   
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        nomColumnas.add(rsmd.getColumnName(i));
                    }

                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        if (numColumnas == 1) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                        }

                        if (numColumnas == 2) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        }
                        if (numColumnas == 3) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                            parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        }

                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargarCondicion4 --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargarCondicion4 --> Exception --> " + e.getMessage());
        }
        return lista;
    }

public Object cargarCondicionCinco(int idQuery) {

        ArrayList lista = new ArrayList();
        ArrayList nomColumnas = new ArrayList();
        ComboVO parametro;
        int numColumnas;
        try {
            if (cn.isEstado()) {

                cn.prepareStatementSEL(Constantes.PS1, cn.traerElQueryCombo(idQuery));
                cn.ps1.setString(1, condicion1);
                cn.ps1.setString(2, condicion2);
                cn.ps1.setString(3, condicion3);                
                cn.ps1.setString(4, condicion4);
                cn.ps1.setString(5, condicion5);


                if (cn.imprimirConsola) {
                    System.out.println("idQuery cargarCondicion5: " + idQuery);
                    System.out.println(((LoggableStatement) cn.ps1).getQueryString());
                }

                if (cn.selectSQL(Constantes.PS1)) {

                    ResultSetMetaData rsmd = cn.rs1.getMetaData();
                   
                    numColumnas = rsmd.getColumnCount();

                    for (int i = 1; i < numColumnas + 1; i++) {
                        nomColumnas.add(rsmd.getColumnName(i));
                    }

                    while (cn.rs1.next()) {
                        parametro = new ComboVO();
                        if (numColumnas == 1) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                        }

                        if (numColumnas == 2) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                        }
                        if (numColumnas == 3) {
                            parametro.setId(cn.rs1.getString(String.valueOf(nomColumnas.get(0)).trim()));
                            parametro.setDescripcion(cn.rs1.getString(String.valueOf(nomColumnas.get(1)).trim()));
                            parametro.setTitle(cn.rs1.getString(String.valueOf(nomColumnas.get(2)).trim()));
                        }

                        lista.add(parametro);
                    }
                    cn.cerrarPS(Constantes.PS1);
                }
            }

        } catch (SQLException e) {
            System.out.println("Error --> clase  -->combo-- function cargarCondicion4 --> SQLException --> " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error --> clase  -->combo-- function cargarCondicion4 --> Exception --> " + e.getMessage());
        }
        return lista;
    }

    public Sgh.Utilidades.Conexion getCn() {
        return cn;
    }

    public void setCn(Sgh.Utilidades.Conexion value) {
        cn = value;
    }

    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String value) {
        id = value;
    }

    public java.lang.String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(java.lang.String value) {
        descripcion = value;
    }

    public java.lang.String getTitle() {
        return title;
    }

    public void setTitle(java.lang.String value) {
        title = value;
    }

    public java.lang.String getCondicion1() {
        return condicion1;
    }

    public void setCondicion1(java.lang.String value) {
        condicion1 = value;
    }

    public java.lang.String getCondicion2() {
        return condicion2;
    }

    public void setCondicion2(java.lang.String value) {
        condicion2 = value;
    }

    public java.lang.String getCondicion3() {
        return condicion3;
    }

    public void setCondicion3(java.lang.String value) {
        condicion3 = value;
    }

    public java.lang.String getCondicion4() {
        return condicion4;
    }

    public void setCondicion4(java.lang.String value) {
        condicion4 = value;
    }

    public java.lang.String getCondicion5() {
        return condicion5;
    }

    public void setCondicion5(java.lang.String value) {
        condicion5 = value;
    }

    public java.lang.String getCondicion6() {
        return condicion6;
    }

    public void setCondicion6(java.lang.String value) {
        condicion6 = value;
    }
}
