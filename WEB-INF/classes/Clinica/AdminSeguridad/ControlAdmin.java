/*
last: jas
 */

package Clinica.AdminSeguridad;

import Sgh.Utilidades.*;
//import Clinica.AdminSeguridad.Suministros.*;
//import Clinica.gestionarNoConformidades.*;
//import Clinica.Indicadores.*;
import java.util.*;

public class ControlAdmin {
   private Conexion cn;
   public Usuario usuario;
   /*
    * public Pais pais; public Depto depto; public Municipio municipio; public
    * Proceso proceso; public ClaseEA claseEA; public TipoPlan tipoPlan; public
    * FactorEspina factorEspina; public CriterioFactor criterioFactor; public
    * UbicacionArea ubicacionArea; public NoConformidad noConformidad; public
    * IndicadorEventos indicador; public EstadoEvento estadoEvento; public
    * Calendario calendario; public TipoPaciente tipoPaciente; public TipoReporte
    * tipoReporte;
    */
   public Combo combo;
   public Grilla grilla;
   public Autocomplete autocomplete;
   public Notificacion notificacion;

   // evento adverso
   /*
    * public BarreraDefensa barreraDefensa; public AccionInsegura accionInsegura;
    * public OrigenFactorContribut origenFactorContribut; public
    * OrganizacionCultura organizacionCultura; public Ambito ambito; public
    * TipoNivelResponsabilidad tipoNivelResponsabilidad;
    */

   // plan de mejora
   // public EstadoTarea estadoTarea;

   // citas medicas de mejora
   // public TipoAgendaCita tipoAgendaCita;

   // plan de suministros
   /*
    * public Servicio servicio; public GrupoItem grupoItem; public ClaseItem
    * claseItem; public Clinica.AdminSeguridad.Suministros.UnidadMedida
    * unidadMedida;
    */

   private String page;
   private String rows;
   private String sidx;
   private String sord;

   private String sinertrans_user;

   public ControlAdmin() {
      /*
       * proceso = new Proceso(); ubicacionArea = new UbicacionArea(); noConformidad =
       * new NoConformidad(); indicador = new IndicadorEventos(); calendario = new
       * Calendario(); barreraDefensa = new BarreraDefensa(); accionInsegura = new
       * AccionInsegura(); origenFactorContribut = new OrigenFactorContribut();
       * organizacionCultura = new OrganizacionCultura(); tipoPlan = new TipoPlan();
       * factorEspina = new FactorEspina(); criterioFactor = new CriterioFactor();
       * claseEA = new ClaseEA();
       * 
       * estadoTarea = new EstadoTarea(); ambito = new Ambito();
       * tipoNivelResponsabilidad = new TipoNivelResponsabilidad();
       * 
       * servicio= new Servicio(); grupoItem= new GrupoItem(); claseItem= new
       * ClaseItem(); unidadMedida= new
       * Clinica.AdminSeguridad.Suministros.UnidadMedida(); estadoEvento= new
       * EstadoEvento();
       * 
       * // citas tipoAgendaCita = new TipoAgendaCita(); tipoPaciente = new
       * TipoPaciente(); citasEstado = new CitasEstado(); tipoReporte = new
       * TipoReporte(); listaEsperaEstado = new ListaEsperaEstado();
       */
      usuario = new Usuario();
      autocomplete = new Autocomplete();
      notificacion = new Notificacion();

      combo = new Combo();
      grilla = new Grilla();

   }

   // Metodos traducidos por el controlador a las clases especificas

   // Metodos traducidos por el controlador a las clases especificas

   /*
    * 
    * public Object cargarDeptos(){ return depto.cargarDeptos(); }
    * 
    * public Object cargarDeptos1(){ return depto.cargarDeptos1(); }
    * 
    * public Object cargarDeptosCol(){ return depto.cargarDeptosCol(); } public
    * Object cargarTodosDeptos(){ return depto.cargarTodosDeptos(); } //cargar los
    * municipios de colombia de un depto especifico public Object
    * cargarMunicipios(){ return municipio.cargarMunicipios(); }
    * 
    * public Object cargarMunicipios1(){ return municipio.cargarMunicipios1(); }
    * public Object CargarProceso(){ return proceso.CargarProceso(); }
    * 
    * 
    * 
    * public Object CargarTipoPlan(String val){ return
    * tipoPlan.CargarTipoPlan(val); } public Object CargarFactorEspina(){ return
    * factorEspina.CargarFactorEspina(); }
    * 
    * 
    * public Object CargarProcesoAcreditacion(){ return
    * proceso.CargarProcesoAcreditacion(); } public Object CargarProgramas(){
    * return programa.CargarProgramas(); }
    * 
    * 
    * public Object cargarUbicacionArea(){ return
    * ubicacionArea.CargarUbicacionArea(); } public Object
    * cargarPersonaDestinoEvento(){ return
    * noConformidad.cargarPersonaDestinoEvento(); } public Object
    * cargarJefeDeProceso(){ return noConformidad.cargarJefeDeProceso(); }
    * 
    * 
    * public Object cargarRolDestinoEvento(){ return
    * noConformidad.cargarRolDestinoEvento(); }
    * 
    * public Object cargarSubTipoEvento(String tipoEvent){ return
    * noConformidad.cargarSubTipoEvento(tipoEvent); } public Object
    * cargarEstadoEvento(String tipoEvent){ return
    * noConformidad.cargarEstadoEvento(tipoEvent); }
    * 
    * 
    * public Object cargarNoConformidad(){ return
    * noConformidad.cargarNoConformidad(); }
    * 
    * public Object cargarCriterioFactor(){ System.out.println("001"); return
    * criterioFactor.CargarCriterioFactor(); }
    * 
    * 
    * public Object cargarNoConformidadTodas(){ return
    * noConformidad.cargarNoConformidadTodas(); }
    * 
    * /*cargar evento adversos
    */
   /*
    * public Object CargarBarreraDefensa(){ return
    * barreraDefensa.CargarBarreraDefensa(); }
    * 
    * public Object CargarAmbito(){ return ambito.CargarAmbito(); }
    * 
    * 
    * 
    * public Object CargarNivelResponsabilidad(){ return
    * tipoNivelResponsabilidad.CargarNivelResponsabilidad(); }
    * 
    * 
    * 
    * 
    * public Object CargarAccionInsegura(){ return
    * accionInsegura.CargarAccionInsegura(); }
    * 
    * public Object CargarOrigenFactorContribut(){ return
    * origenFactorContribut.CargarOrigenFactorContribut(); }
    * 
    * public Object CargarOrganizacionCultura(){ return
    * organizacionCultura.CargarOrganizacionCultura(); }
    * 
    * public Object cargarEstadoTarea(){ return estadoTarea.cargarEstadoTarea(); }
    */

   // cargar los bariios de nari�o de un municiopi especifico

   /*
    * public boolean cambiarContrasena(String nuevac){ boolean resultado; //
    * resultado= usuario.cambiarContrasena(nuevac); return resultado; }
    */

   public int cargarDatosDelUsuario() {
      return usuario.cargarDatosDelUsuario();
   }

   /****
    *
    * Metodos propios del cotrolador
    *
    ***/
   public boolean asignaGuardar(String opcion) {
      int op = 0;
      boolean respuesta = false;
      if (opcion.equals("usuario"))
         op = 1;
      if (opcion.equals("reportarNoConformidad"))
         op = 201;
      if (opcion.equals("administrarNoConformidad"))
         op = 202;

      switch (op) {
         case 1:
            respuesta = usuario.crearCuentaUsuario();
            break;
      }
      return respuesta;
   }

   public boolean asignaModificar(String opcion) {
      int op = 0;
      boolean respuesta = false;
      if (opcion.equals("usuario"))
         op = 1;
      if (opcion.equals("reportarNoConformidad"))
         op = 201;
      if (opcion.equals("administrarNoConformidad"))
         op = 202;

      switch (op) {
         case 1:
            respuesta = usuario.modificarCuentaUsuario();
            break;

      }
      return respuesta;
   }

   /* Metodos Paginacion */

   // OJO construir try catch para la conversion a int para esta seccion completa..
   private int totalPages;
   private int count;
   private int start; // para int offset
   private int pagina;// para int page
   private int limit;// para int rows

   /*
    * devuelve el numero total de paginas calculadas para el numero de registros
    * contados... y para la pagina de peticion, calcula el valor 'start' donde
    * comenzara el set. ahora, le paso el totalcount, pero podria tomarlo de
    * this.count, y guardar el tp en this.totalPages.
    */
   public int totalPages(int totalcount) {
      // System.out.println(" ControlAdmin.totalPages ");
      int tp = 0;
      // System.out.println("aqui 1:"+opcion);
      this.pagina = Integer.parseInt(this.page);// para int
      this.limit = Integer.parseInt(this.rows);// para int
      this.start = 0;

      if (totalcount > 0)
         tp = (int) Math.ceil((1.0 * totalcount) / limit);// asegurar el decimal.
      else
         tp = 0;
      //
      if (this.pagina > tp)
         this.pagina = tp;
      if (this.pagina <= 0)
         this.pagina = 1;// adicionado por mi, pa q avance. revisarlo.
      //
      this.start = this.limit * this.pagina - this.limit;

      return tp;
   }

   public int asignaContar(String opcion) {
      int op = 0;
      ArrayList respuesta = new ArrayList();
      if (opcion.equals("cargos"))
         op = 7;
      if (opcion.equals("tiposIdentificaciones"))
         op = 8;
      if (opcion.equals("reportarNoConformidad"))
         op = 201;
      switch (op) {
         case 8:
            // System.out.println("aqui 2:"+opcion);
            // op=(int)tipoid.contar(); //cuantos registros con los parametros del select
            break;
         case 201:
            // System.out.println("aqui 2:"+opcion);
            // op=(int)tipoid.contar(); //cuantos registros con los parametros del select
            break;

      }
      return op;
   }

   public Object asignaBuscarPag(String opcion) {
      System.out.println(" sidx ++++++++++++++++++--" + sidx + "---" + opcion);
      int page, limit, sidx, start;
      // validar el indice de ordenamiento, la direcc de orden,
      if (this.sidx == null)
         sidx = 1;
      else if (this.sidx.equals(""))
         sidx = 1;
      else {
         try {
            sidx = Integer.parseInt(this.sidx);
         } catch (Exception e) {
            sidx = 1;
            System.out.println(" Se paso como Indice de Ordenacion algo que NO es Int !!! ");
         }
         sidx = Integer.parseInt(this.sidx);
      }
      page = this.pagina;
      limit = this.limit;
      start = this.start;
      sord = this.sord;// sort order. String sord
      //
      int op = 0;
      ArrayList respuesta = new ArrayList();
      // System.out.println("aqui 1:"+opcion);
      if (opcion.equals("tiposIdentificaciones"))
         op = 8;
      if (opcion.equals("reportarNoConformidad"))
         op = 201;
      if (opcion.equals("administrarNoConformidad"))
         op = 202;
      if (opcion.equals("listPlanAccion"))
         op = 204;
      if (opcion.equals("administrarNoConformidadAsociar"))
         op = 205;
      if (opcion.equals("listAsociarEventos"))
         op = 206;
      if (opcion.equals("listPersonasPlanAccion"))
         op = 207;
      if (opcion.equals("listPlanAccionSeguimiento"))
         op = 208;
      if (opcion.equals("listPlanAccionSeguimientoMeta"))
         op = 209;
      if (opcion.equals("listadoCalendario"))
         op = 211;
      if (opcion.equals("listadoCitas"))
         op = 212;

      switch (op) {
         /*
          * case 201: respuesta=(ArrayList)noConformidad.buscarPag(sidx, sord, start,
          * limit); break; case 202:
          * respuesta=(ArrayList)noConformidad.buscarPagAdmin(sidx, sord, start, limit);
          * break; case 204:
          * respuesta=(ArrayList)noConformidad.buscarPagListPlanAccion(sidx, sord, start,
          * limit); break; case 205:
          * respuesta=(ArrayList)noConformidad.buscarPagAdminNoAsociados(sidx, sord,
          * start, limit); break; case 206:
          * respuesta=(ArrayList)noConformidad.buscarPagAdminlistAsociarEventos(sidx,
          * sord, start, limit); break; case 207:
          * respuesta=(ArrayList)noConformidad.buscarPagAdminPersonaslistAsociarEventos(
          * sidx, sord, start, limit); break; case 208:
          * respuesta=(ArrayList)noConformidad.buscarPagListPlanAccionParaSeguimiento(
          * sidx, sord, start, limit); break; case 209:
          * respuesta=(ArrayList)noConformidad.buscarPagAdminMetasSeguimiento(sidx, sord,
          * start, limit); break; case 211:
          * respuesta=(ArrayList)calendario.buscarCalendario(); break; case 212:
          * respuesta=(ArrayList)calendario.buscarCalendario(); break;
          * 
          */

      }
      return respuesta;
   }

   /*
    * public Object asignaBuscar(String opcion){
    * //System.out.println(" ControlAdmin.asigna buscar "); int op=0; ArrayList
    * respuesta=new ArrayList(); if(opcion.equals("ubicaciones"))op=1;
    * //ubicaciones
    * 
    * switch (op){ case 1: //System.out.println("aqui 2:"+opcion); //
    * respuesta=(ArrayList)agencia.buscarAgencia(); break; } return respuesta;
    * 
    * }
    */

   public boolean asignaEliminar(String opcion) {
      int op = 0;
      boolean respuesta = false;
      if (opcion.indexOf("ubicaciones") != -1)
         op = 1;

      switch (op) {
         case 1:
            int op2 = 0;
            if (opcion.indexOf("Pais") != -1)
               op2 = 1;
            if (opcion.indexOf("Depto") != -1)
               op2 = 2;
            if (opcion.indexOf("Municipio") != -1)
               op2 = 3;
            if (opcion.indexOf("Barrio") != -1)
               op2 = 4;
            if (opcion.indexOf("tiposIdentificaciones") != -1)
               op2 = 8;
            switch (op2) {
               case 1:
                  // respuesta=pais.eliminarPais();
                  break;
               case 8:
                  // respuesta=tipoid.eliminarTipoId();
                  break;
            }
            break;

      }
      return respuesta;
   }

   public Sgh.Utilidades.Conexion getCn() {
      return cn;
   }

   public void setCn(Sgh.Utilidades.Conexion value) {
      /*
       * milo: todo lo que recibe cn aqui debe inicializar en el constructor
       */
      cn = value;

      usuario.setCn(this.cn);
      autocomplete.setCn(this.cn);
      notificacion.setCn(this.cn);
      combo.setCn(this.cn);
      grilla.setCn(this.cn);

   }

   /* metodo diccionario */

   /* fin */
   public java.lang.String getPage() {
      return page;
   }

   public void setPage(java.lang.String value) {
      page = value;
   }

   public java.lang.String getRows() {
      return rows;
   }

   public void setRows(java.lang.String value) {
      rows = value;
   }

   public java.lang.String getSidx() {
      return sidx;
   }

   public void setSidx(java.lang.String value) {
      sidx = value;
   }

   public java.lang.String getSord() {
      return sord;
   }

   public void setSord(java.lang.String value) {
      sord = value;
   }

   public int getTotalPages() {
      return totalPages;
   }

   public void setTotalPages(int value) {
      totalPages = value;
   }

   public int getCount() {
      return count;
   }

   public void setCount(int value) {
      count = value;
   }

   public int getStart() {
      return start;
   }

   public void setStart(int value) {
      start = value;
   }

   public int getPagina() {
      return pagina;
   }

   public void setPagina(int value) {
      pagina = value;
   }

   public int getLimit() {
      return limit;
   }

   public void setLimit(int value) {
      limit = value;
   }
}