package WebServicesLaboratorio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "paciente")
@XmlAccessorType(XmlAccessType.FIELD)
public class PacienteLaboratorio {

    @XmlElement(name = "codigoAfiliacion")
    private String codigoAfiliacion;

    @XmlElement(name = "codigoRegimen")
    private String codigoRegimen;

    @XmlElement(name = "tipoDocumento")
    private String tipoDocumento;

    @XmlElement(name = "documento")
    private String documento;

    @XmlElement(name = "nombre1")
    private String nombre1;

    @XmlElement(name = "nombre2")
    private String nombre2;

    @XmlElement(name = "apellido1")
    private String apellido1;

    @XmlElement(name = "apellido2")
    private String apellido2;

    @XmlElement(name = "sexo")
    private String sexo;

    @XmlElement(name = "fechaNacimiento")
    private String fechaNacimiento;

    @XmlElement(name = "enEmbarazo")
    private String enEmbarazo;

    @XmlElement(name = "codigoMunicipio")
    private String codigoMunicipio;

    @XmlElement(name = "codigoZona")
    private String codigoZona;

    @XmlElement(name = "direccion")
    private String direccion;

    @XmlElement(name = "telefono")
    private String telefono;

    @XmlElement(name = "correo")
    private String correo;

    @XmlElement(name = "caracteristicasMedicas ")
    private String caracteristicasMedicas;

    public PacienteLaboratorio() {
        caracteristicasMedicas = "";
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getEnEmbarazo() {
        return enEmbarazo;
    }

    public void setEnEmbarazo(String enEmbarazo) {
        this.enEmbarazo = enEmbarazo;
    }

    public String getCodigoAfiliacion() {
        return codigoAfiliacion;
    }

    public void setCodigoAfiliacion(String codigoAfiliacion) {
        this.codigoAfiliacion = codigoAfiliacion;
    }

    public String getCodigoRegimen() {
        return codigoRegimen;
    }

    public void setCodigoRegimen(String codigoRegimen) {
        this.codigoRegimen = codigoRegimen;
    }

    public String getCodigoZona() {
        return codigoZona;
    }

    public void setCodigoZona(String codigoZona) {
        this.codigoZona = codigoZona;
    }

    public String getCaracteristicasMedicas() {
        return caracteristicasMedicas;
    }

    public void setCaracteristicasMedicas(String caracteristicasMedicas) {
        this.caracteristicasMedicas = caracteristicasMedicas;
    }

    @Override
    public String toString() {
        return "PacienteLaboratorio{" + "codigoAfiliacion=" + codigoAfiliacion + ", codigoRegimen=" + codigoRegimen + ", tipoDocumento=" + tipoDocumento + ", documento=" + documento + ", nombre1=" + nombre1 + ", nombre2=" + nombre2 + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", sexo=" + sexo + ", fechaNacimiento=" + fechaNacimiento + ", enEmbarazo=" + enEmbarazo + ", codigoMunicipio=" + codigoMunicipio + ", codigoZona=" + codigoZona + ", direccion=" + direccion + ", telefono=" + telefono + ", correo=" + correo + ", caracteristicasMedicas=" + caracteristicasMedicas + '}';
    }

}
