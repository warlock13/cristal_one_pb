
package WebServicesLaboratorio.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "envioOrdenesTomadas", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "envioOrdenesTomadas", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
public class EnvioOrdenesTomadas {

    @XmlElement(name = "orden", namespace = "", required = true)
    private List<WebServicesLaboratorio.OrdenTomada> orden;

    /**
     * 
     * @return
     *     returns List<OrdenTomada>
     */
    public List<WebServicesLaboratorio.OrdenTomada> getOrden() {
        return this.orden;
    }

    /**
     * 
     * @param orden
     *     the value for the orden property
     */
    public void setOrden(List<WebServicesLaboratorio.OrdenTomada> orden) {
        this.orden = orden;
    }

}
