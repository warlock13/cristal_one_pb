
package WebServicesLaboratorio.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "consultaOrdenes", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaOrdenes", namespace = "http://cristalweb.emssanar.org.co/clinica/WebServiceLaboratorio", propOrder = {
    "fechaDesde",
    "fechaHasta"
})
public class ConsultaOrdenes {

    @XmlElement(name = "fechaDesde", namespace = "", required = true)
    private String fechaDesde;
    @XmlElement(name = "fechaHasta", namespace = "", required = true)
    private String fechaHasta;

    /**
     * 
     * @return
     *     returns String
     */
    public String getFechaDesde() {
        return this.fechaDesde;
    }

    /**
     * 
     * @param fechaDesde
     *     the value for the fechaDesde property
     */
    public void setFechaDesde(String fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getFechaHasta() {
        return this.fechaHasta;
    }

    /**
     * 
     * @param fechaHasta
     *     the value for the fechaHasta property
     */
    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

}
