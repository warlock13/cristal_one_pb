package WebServicesLaboratorio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ordenTomada")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrdenTomada {

    @XmlElement(name = "numero", required = true)
    private String numero;

    public OrdenTomada() {

    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "OrdenTomada{" + "numero=" + numero + '}';
    }

}
