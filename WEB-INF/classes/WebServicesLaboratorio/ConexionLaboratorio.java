package WebServicesLaboratorio;

import Sgh.Utilidades.Des;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class ConexionLaboratorio {

    private HikariConfig config;
    private HikariDataSource ds;

    public ConexionLaboratorio() {

        config = new HikariConfig();
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        //config.setAutoCommit(false);

        Properties props = new Properties();
        Des des = new Des();

        props.setProperty("dataSourceClassName", "org.postgresql.ds.PGSimpleDataSource");
        props.setProperty("dataSource.user", "postgres");
        props.setProperty("dataSource.password", des.desencriptar("OnXs4nAx9UIvMgxz+58uzoIH6l0+GaX9"));
        props.setProperty("dataSource.databaseName", "clinica_desarrollo");
        props.setProperty("dataSource.serverName", "localhost");        
        //props.setProperty("dataSource.serverName", "190.60.242.160");

        props.setProperty("autoCommit", "false");

        props.put("dataSource.logWriter", new PrintWriter(System.out));

        config = new HikariConfig(props);
        ds = new HikariDataSource(config);

    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public void cerrarConexion() throws SQLException {
        ds.getConnection().close();
        ds.close();
    }

    public String consultarQuery(Integer idQuery, PreparedStatement ps, ResultSet rs) {

        String swQuery = "";

        String query = "SELECT query\n"
                + "FROM sw.query \n"
                + "WHERE id = ?";

        try {
            ps = getConnection().prepareStatement(query);
            ps.setInt(1, idQuery);
            rs = ps.executeQuery();

            while (rs.next()) {
                swQuery = rs.getString("query");
            }
        } catch (SQLException e) {
            System.out.println("Error consultar query: " + e.getMessage());
        }

        return swQuery;
    }

}
