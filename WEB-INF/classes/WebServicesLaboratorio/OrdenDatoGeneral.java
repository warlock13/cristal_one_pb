package WebServicesLaboratorio;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "datosGenerales")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrdenDatoGeneral {

    @XmlElement(name = "historiaClinica")
    private String historiaClinica;

    @XmlElement(name = "fechaOrdenado")
    private String fechaOrdenado;

    @XmlElement(name = "documentoMedico")
    private String documentoMedico;

    @XmlElement(name = "nombreMedico")
    private String nombreMedico;

    @XmlElement(name = "codigoCosto")
    private String codigoCosto;

    @XmlElement(name = "nombreCosto")
    private String nombreCosto;

    @XmlElement(name = "codigoSede")
    private String codigoSede;

    @XmlElement(name = "codigoCliente")
    private String codigoCliente;

    @XmlElement(name = "nombreCliente")
    private String nombreCliente;

    @XmlElement(name = "cama")
    private String cama;

    @XmlElement(name = "codigoProgramaSalud")
    private String codigoProgramaSalud;

    @XmlElement(name = "nombreProgramaSalud")
    private String nombreProgramaSalud;

    @XmlElement(name = "observaciones")
    private String observaciones;
    
    @XmlElement(name = "codigoPlanFacturacion")
    private String codigoPlanFacturacion;

    @XmlElement(name = "nombrePlanFacturacion")
    private String nombrePlanFacturacion;
    
    @XmlElement(name = "ipEquipo")
    private String ipEquipo;

    public OrdenDatoGeneral() {
        cama = "NA";
        codigoProgramaSalud = "NA";
        nombreProgramaSalud = "No Aplica";
        observaciones = "";
    }

    public String getCodigoSede() {
        return codigoSede;
    }

    public void setCodigoSede(String codigoSede) {
        this.codigoSede = codigoSede;
    }

    public String getHistoriaClinica() {
        return historiaClinica;
    }

    public void setHistoriaClinica(String historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

    public String getDocumentoMedico() {
        return documentoMedico;
    }

    public void setDocumentoMedico(String documentoMedico) {
        this.documentoMedico = documentoMedico;
    }

    public String getNombreMedico() {
        return nombreMedico;
    }

    public void setNombreMedico(String nombreMedico) {
        this.nombreMedico = nombreMedico;
    }

    public String getFechaOrdenado() {
        return fechaOrdenado;
    }

    public void setFechaOrdenado(String fechaOrdenado) {
        this.fechaOrdenado = fechaOrdenado;
    }

    public String getCodigoCosto() {
        return codigoCosto;
    }

    public void setCodigoCosto(String codigoCosto) {
        this.codigoCosto = codigoCosto;
    }

    public String getNombreCosto() {
        return nombreCosto;
    }

    public void setNombreCosto(String nombreCosto) {
        this.nombreCosto = nombreCosto;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getCama() {
        return cama;
    }

    public void setCama(String cama) {
        this.cama = cama;
    }

    public String getCodigoProgramaSalud() {
        return codigoProgramaSalud;
    }

    public void setCodigoProgramaSalud(String codigoProgramaSalud) {
        this.codigoProgramaSalud = codigoProgramaSalud;
    }

    public String getNombreProgramaSalud() {
        return nombreProgramaSalud;
    }

    public void setNombreProgramaSalud(String nombreProgramaSalud) {
        this.nombreProgramaSalud = nombreProgramaSalud;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getCodigoPlanFacturacion() {
        return codigoPlanFacturacion;
    }

    public void setCodigoPlanFacturacion(String codigoPlanFacturacion) {
        this.codigoPlanFacturacion = codigoPlanFacturacion;
    }

    public String getNombrePlanFacturacion() {
        return nombrePlanFacturacion;
    }

    public void setNombrePlanFacturacion(String nombrePlanFacturacion) {
        this.nombrePlanFacturacion = nombrePlanFacturacion;
    }

    public String getIpEquipo() {
        return ipEquipo;
    }

    public void setIpEquipo(String ipEquipo) {
        this.ipEquipo = ipEquipo;
    }

    
    
    
}
