/**
 *@(#)Clase Conexion.java version 1.02 2015/12/09
 *Copyright(c) 2015 Firmas Digital.
 * JAS
 */
package Sgh.Utilidades;

import Sgh.AdminSeguridad.*;
import Clinica.Utilidades.Constantes;
import java.io.*;
import java.text.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import Clinica.Presentacion.*;

public class ConexionArchivo {

    //Fundonar
    private final String servidorIpPublica = "190.60.242.160";


    //private final String servidorIpPublica = "181.48.155.147";

    //Cer
    //private final String servidorIpPublica = "190.65.79.34";

    //AN
   // private final String servidorIpPublica = "190.147.27.96";

    public Usuario usuario;
    public Constantes constantes;
    private StringBuffer sql = new StringBuffer();
    private Connection cn;
    public PreparedStatement ps1;
    public PreparedStatement ps2;
    public PreparedStatement ps3;
    public PreparedStatement ps4;
    public PreparedStatement ps5;
    public PreparedStatement psQ;
    public ResultSet rs1 = null;
    public ResultSet rs2 = null;
    public ResultSet rs3 = null;
    public ResultSet rs4 = null;
    public ResultSet rs5 = null;
    public ResultSet rsQ = null;
    boolean estado = false;
    public boolean errorTrans, llegoConexion;
    public boolean exito = true;
    private Mensajes msg; // no usar new() jamas.    s
    public boolean queryTabla;

    ArrayList<ConexionVO> resultadoQuery = new ArrayList<ConexionVO>();
    ArrayList<ConexionVO> resultadoQueryCombo = new ArrayList<ConexionVO>();
    ArrayList<ConexionVO> resultadoQueryNotificacion = new ArrayList<ConexionVO>();
    public ConexionVO parametroQ, parametroQCombo, parametroQNotificacion;

    String rutaRaiz = "";

    String url = "";
    String user = "";
    String pwd = "";
    String drv = "";

    public boolean imprimirConsola = false;


    /* FUNDONAR*/
//    String servidor = "190.147.111.78";
   // String servidor = "192.168.0.160";
    String servidor = "localhost";

    String motor = "postgres";
//      String nombreBd = "clinica_pruebas";
     String nombreBd = "clinica_desarrollo";
    //String nombreBd = "clinica";

    public ConexionArchivo(Connection cn) {
        this.cn = cn;
        llegoConexion = true;
        estado = true;
    }

    public ConexionArchivo() {

        llegoConexion = false;
        Des des = new Des();
        //pwd = des.desencriptar("8LXwTohLHTomdY5zIvRc0UTrukuPLS87ggfqXT4Zpf0=");
        pwd = des.desencriptar("/gZIeaZpUUCgOaugfWYiuRdBC+iqhX1M");
        //pwd = des.desencriptar("/DLvNN8rTjkAUzPPtNsGph8UgFOn6AH6");
        initdb();
        queryTabla = true;
        System.out.println("Conexion Archivo MOTOR \n archivo: " + motor + "\n SERVIDOR    : " + servidor + "\n nombreBd    : " + nombreBd + "\n IMP.CONSOLA : " + imprimirConsola + "\n");
    }

    private void initdb() {
        if (motor.equals("postgres")) {
            url = "jdbc:postgresql://" + servidor + "/" + nombreBd;
            user = "postgres";
            drv = "org.postgresql.Driver";
        }
        try {
            Class.forName(drv);
            estado = true;
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
            estado = false;
        }

        try {
            if (motor.equals("postgres")) {
                cn = DriverManager.getConnection(url, user, pwd);
            }
            estado = true;
        } catch (SQLException ex) {
            System.out.println(" " + ex.getMessage());
            estado = false;
        }
        this.rs1 = null;
        this.rs2 = null;
        this.rs3 = null;
        this.rs4 = null;
        this.rs5 = null;
        this.ps1 = null;
        this.ps2 = null;
        this.ps3 = null;
        this.ps4 = null;
        this.ps5 = null;
        this.rsQ = null;
    }

    public void cerrarPS(int numPS) {
        try {
            cerrarRS(numPS);
            switch (numPS) {
                case 1:
                    if (this.ps1 != null) {
                        this.ps1.close();
                        this.ps1 = null;
                    }
                    break;
                case 2:
                    if (this.ps2 != null) {
                        this.ps2.close();
                        this.ps2 = null;
                    }
                    break;
                case 3:
                    if (this.ps3 != null) {
                        this.ps3.close();
                        this.ps3 = null;
                    }
                    break;
                case 4:
                    if (this.ps4 != null) {
                        this.ps4.close();
                        this.ps4 = null;
                    }
                    break;
                case 5:
                    if (this.ps5 != null) {
                        this.ps5.close();
                        this.ps5 = null;
                    }
                    break;
                case 6:
                    if (this.psQ != null) {
                        this.psQ.close();
                        this.psQ = null;
                    }
                    break;

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Error cerrando  PS" + numPS);
        }
    }

    /* self */
    public void cerrarPS(StatementResultSet srs) {
        try {/*
            cerrarRS(numPS);
            switch(numPS){
                case 1: if(this.ps1!=null){
                            this.ps1.close();
                            this.ps1=null;
                        }
                        break;
                case 2: if(this.ps2!=null){
                            this.ps2.close();
                            this.ps2=null;
                        }
                        break;
            } */
            //
            cerrarRS(srs.getRs());
            if (srs.ps != null) {
                srs.ps.close();
                srs.ps = null;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Error cerrando  PS self" + srs);
        }
    }

    /**
     * M�todo para cerrar los resultSet
     *
     * @param numRS N�mero del resultSet que se quiere cerrar
     */
    private void cerrarRS(int numRS) {
        try {
            switch (numRS) {
                case 1:
                    if (this.rs1 != null) {
                        this.rs1.close();
                        this.rs1 = null;
                    }
                    break;
                case 2:
                    if (this.rs2 != null) {
                        this.rs2.close();
                        this.rs2 = null;
                    }
                    break;
                case 3:
                    if (this.rs3 != null) {
                        this.rs3.close();
                        this.rs3 = null;
                    }
                    break;
                case 4:
                    if (this.rs4 != null) {
                        this.rs4.close();
                        this.rs4 = null;
                    }
                    break;
                case 5:
                    if (this.rs5 != null) {
                        this.rs5.close();
                        this.rs5 = null;
                    }
                    break;
                case 6:
                    if (this.rsQ != null) {
                        this.rsQ.close();
                        this.rsQ = null;
                    }
                    break;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Error en clase conexion cerrando  RS" + numRS);
        }

    }

    /* self */
    private void cerrarRS(ResultSet rs) {
        try {/*
            switch(numRS){
                case 1: if(this.rs1!=null){
                            this.rs1.close();
                            this.rs1=null;
                        }
                        break;
                case 5: if(this.rs5!=null){
                            this.rs5.close();
                            this.rs5=null;
                        }
                        break;
            }*/
            //
            if (rs != null) {
                rs.close();
                rs = null;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Error en clase conexion cerrando  RS self" + rs);
        }

    }

    /**
     * M�todo que realiza un Rollback(deshacer lo hecho en una transaccion no
     * satisfactoria)
     */
    public void rollback() {
        try {
            cn.rollback();
        } catch (SQLException e) {
            System.out.println("error en bd.rollback() clase conexion " + e.getMessage());
        }
    }

    /**
     * M�todo destroy de la clase conexion llama al m�todo encargado de cerrar
     * los elementos utilizados para las ejecuciones con BD
     */
    public void destroy() {
        if (!llegoConexion) {
            destroydb();
        } else {
            System.out.println("No se cierra la conexion porque es de sesion.");
        }
    }

    /**
     * M�todo que cierra los PreparedStatement, ResultSet y la conexion con la
     * BD
     */
    private void destroydb() {
        try {
            this.cerrarRS(1);
            this.cerrarRS(2);
            this.cerrarRS(3);
            this.cerrarRS(4);
            this.cerrarRS(5);
            this.cerrarPS(1);
            this.cerrarPS(2);
            this.cerrarPS(3);
            this.cerrarPS(4);
            this.cerrarPS(5);
            cn.close();
        } catch (SQLException ex) {
        }
    }

    /**
     * M�todo que retorna el estado de la conexion con la BD
     */
    public boolean isEstado() {
        return estado;
    }

    /**
     * m�todo mutador que permite cambiar la variable de estado de la conexion
     * de la BD
     */
    public void setEstado(boolean value) {
        estado = value;
    }

    /**
     * Adrian --- Para IReports m�todo que retorna la variable de conexion
     */
    public Connection getConexion() {
        return this.cn;
    }

    /**
     * establecer o retornar msg, con el mensaje ultimo. *
     */
    /*  public SinergiaBasic.Utilidades.Mensajes getMsg() {
        return msg;
    }

    public void setMsg(SinergiaBasic.Utilidades.Mensajes value) {
        msg = value;
    }   */
    public java.lang.String getMotor() {
        return motor;
    }

    public void setMotor(java.lang.String value) {
        motor = value;
    }

    public java.lang.String getServidor() {
        return servidor;
    }

    public void setServidor(java.lang.String value) {
        servidor = value;
    }

    public java.lang.String getNombreBd() {
        return nombreBd;
    }

    public void setNombreBd(java.lang.String value) {
        nombreBd = value;
    }

    public java.lang.String getUser() {
        return user;
    }

    public void setUser(java.lang.String value) {
        user = value;
    }

    public java.lang.String getPwd() {
        return pwd;
    }

    public void setPwd(java.lang.String value) {
        pwd = value;
    }

    public boolean iduSQL(int numPS) {

        boolean rta = true;
        try {
            switch (numPS) {
                case 1:
                    if (this.ps1.executeUpdate() == 0) {
                        exito = false;
                    }
                    /* else{
                       escribirBitacoraIDU(((LoggableStatement)this.ps1).getQueryString());
                     */
                    if (imprimirConsola) {
                        System.out.println("Query Consola= " + ((LoggableStatement) this.ps1).getQueryString());
                    }

                    /* }*/
                    break;
                case 2:
                    if (this.ps2.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 3:
                    if (this.ps3.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 4:
                    if (this.ps4.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 5:
                    if (this.ps5.executeUpdate() == 0) {
                        exito = false;
                    } else {
                        escribirBitacoraIDU(((LoggableStatement) this.ps1).getQueryString());
                    }
                    break;
                case 6:              // esta opcion no escribe bitacora porque se volveria un ciclo infinito
                    if (this.psQ.executeUpdate() == 0) {
                        exito = false;
                    }
                    break;
            }
        } catch (SQLException e) {
            switch (numPS) {
                case 1:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps1).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 2:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps2).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 3:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps3).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 4:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps4).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 5:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.ps5).getQueryString() + "::exception " + e.getMessage());
                    break;
                case 6:
                    System.out.println("Error al ejecutar:  " + ((LoggableStatement) this.psQ).getQueryString() + "::exception " + e.getMessage());
                    break;

            }
            e.printStackTrace();
            this.rollback();
            errorTrans = true;
            exito = false;
            rta = false;
            //msg//
            msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
            msg.SIS_ERROR = e.getErrorCode();
            System.out.println("SQLEX msg:" + msg.SIS_MENSAJE + " " + msg.SIS_ERROR + ".");
        }

        return rta;
    }

    /* self */
    public boolean iduSQL(StatementResultSet srs) {
        boolean rta = true;
        try {/*
			switch(numPS){
                 case 1:
                    if(this.ps1.executeUpdate()==0)
                        exito=false;
                    break;
                 case 2:
                    if(this.ps2.executeUpdate()==0)
                        exito=false;
                    break;
                 case 3:
                    if(this.ps3.executeUpdate()==0)
                        exito=false;
                    break;
                 case 4:
                    if(this.ps4.executeUpdate()==0)
                        exito=false;
                    break;
                 case 5:
                    if(this.ps5.executeUpdate()==0)
                        exito=false;
                    break;
            }*/
            if (srs.ps.executeUpdate() == 0) {
                exito = false;
            }
            // ESTE MENSAJE INDICA QUE SE EJECUTO CORRECTAMENTE LA INSTRUCCION, PERO NO SE ACTUALIZO NINGUN REGISTRO.
            //msg.SIS_MENSAJE=" INFO(1.1): No se actualizo ningun registro";}
        } catch (SQLException e) {
            /*
			switch(numPS){
                case 1:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps1).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 2:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps2).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 3:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps3).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 4:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps4).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 5:   System.out.println("Error al ejecutar:  " +  ((LoggableStatement)this.ps5).getQueryString() + "::exception " +e.getMessage());
                          break;
             }*/
            //
            System.out.println("Error al ejecutar SELF IDU:  " + ((LoggableStatement) srs.ps).getQueryString() + "::exception " + e.getMessage());
            e.printStackTrace();
            this.rollback();
            errorTrans = true;
            exito = false;
            rta = false;
            //msg//
            msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
            msg.SIS_ERROR = e.getErrorCode();
            System.out.println("SQLEX msg:" + msg.SIS_MENSAJE + " " + msg.SIS_ERROR + ".");
        }
        return rta;
    }

    public void prepareStatementIDU(int numPS, StringBuffer sql) throws SQLException {
        this.cerrarPS(numPS);
        switch (numPS) {
            case 1:
                this.ps1 = new LoggableStatement(this.cn, sql.toString());
                break;
            case 2:
                this.ps2 = new LoggableStatement(this.cn, sql.toString());
                break;
            case 3:
                this.ps3 = new LoggableStatement(this.cn, sql.toString());
                break;
            case 4:
                this.ps4 = new LoggableStatement(this.cn, sql.toString());
                break;
            case 5:
                this.ps5 = new LoggableStatement(this.cn, sql.toString());
                break;
            case 6:
                this.psQ = new LoggableStatement(this.cn, sql.toString());
                break;
        }

    }

    /**/
    public void prepareStatementIDU(StatementResultSet srs, StringBuffer sql) throws SQLException {
        this.cerrarPS(srs);
        /*
           switch(numPS){
               case 1: this.ps1=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 2: this.ps2=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 3: this.ps3=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 4: this.ps4=new LoggableStatement(this.cn,sql.toString());
                       break;
               case 5: this.ps5=new LoggableStatement(this.cn,sql.toString());
                       break;
           } */
        srs.ps = new LoggableStatement(this.cn, sql.toString());
    }

    public boolean escribirBitacoraIDU(String cadena) {
        boolean rta = true;
        /*
      try{
             sql.delete(0,sql.length());
             sql.append("INSERT INTO sw_bitacora( id_persona,  query) ");
             sql.append("VALUES (?,?) ");
             this.prepareStatementIDU(6,sql);

             this.psQ.setString(1,usuario.getIdentificacion());
             this.psQ.setString(2,cadena);
             this.iduSQL(6);


       }catch(SQLException e){
           System.out.println("Error --> clase Conexion --> function escribirBitacoraIDU --> SQLException --> "+e.getMessage());
       }catch(Exception e){
          System.out.println("Error --> clase Conexion --> function escribirBitacoraIDU --> Exception --> " + e.getMessage());
       }
         */
        return rta;
    }

    public void prepareStatementSEL(int numPS, StringBuffer sql) throws SQLException {

        this.cerrarPS(numPS);
        switch (numPS) {
            case 1:
                this.ps1 = new LoggableStatement(this.cn, sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

                break;
            case 2:
                this.ps2 = new LoggableStatement(this.cn, sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

                break;
            case 3:
                this.ps3 = new LoggableStatement(this.cn, sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

                break;
            case 4:
                this.ps4 = new LoggableStatement(this.cn, sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

                break;
            case 5:
                this.ps5 = new LoggableStatement(this.cn, sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

                break;
            case 6:
                this.psQ = new LoggableStatement(this.cn, sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

                break;

        }
    }

    /* self */
    public void prepareStatementSEL(StatementResultSet srs, StringBuffer sql) throws SQLException {
        this.cerrarPS(srs);
        /*
         switch(numPS){
               case 1: this.ps1=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 2: this.ps2=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 3: this.ps3=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 4: this.ps4=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
               case 5: this.ps5=new LoggableStatement(this.cn,sql.toString(),ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
                       break;
          }*/
        srs.ps = new LoggableStatement(this.cn, sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

    }

    public boolean selectSQL(int numPS) {
        boolean rta = false;
        try {
            cerrarRS(numPS);
            switch (numPS) {
                case 1:
                    //  System.out.println("XXX=  " +  ((LoggableStatement)this.ps1).getQueryString());
                    this.rs1 = this.ps1.executeQuery();
                    rta = true;
                    break;
                case 2:
                    //  System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                    this.rs2 = this.ps2.executeQuery();
                    rta = true;
                    break;
                case 3:
                    //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                    this.rs3 = this.ps3.executeQuery();
                    rta = true;
                    break;
                case 4:
                    //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                    this.rs4 = this.ps4.executeQuery();
                    rta = true;
                    break;
                case 5:
                    //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                    this.rs5 = this.ps5.executeQuery();
                    rta = true;
                    break;
                case 6:
                    //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                    this.rsQ = this.psQ.executeQuery();
                    rta = true;
                    break;

            }
        } catch (SQLException e) {
            System.out.println("--------------------------------------------------------------------------------------------");
            switch (numPS) {
                case 1:
                    System.out.println("Error al ejecutar select A:\n  " + ((LoggableStatement) this.ps1).getQueryString() + "\n\n::exception " + e.getMessage());
                    break;
                case 2:
                    System.out.println("Error al ejecutar select B:\n  " + ((LoggableStatement) this.ps2).getQueryString() + "\n::exception " + e.getMessage());
                    break;
                case 3:
                    System.out.println("Error al ejecutar select C:\n  " + ((LoggableStatement) this.ps3).getQueryString() + "\n::exception " + e.getMessage());
                    break;
                case 4:
                    System.out.println("Error al ejecutar select D:\n  " + ((LoggableStatement) this.ps4).getQueryString() + "\n::exception " + e.getMessage());
                    break;
                case 5:
                    System.out.println("Error al ejecutar select E:\n  " + ((LoggableStatement) this.ps5).getQueryString() + "\n::exception " + e.getMessage());
                    break;
                case 6:
                    System.out.println("Error al ejecutar select F:\n  " + ((LoggableStatement) this.psQ).getQueryString() + "\n::exception " + e.getMessage());
                    break;

            }
            System.out.println("--------------------------------------------------------------------------------------------");
            errorTrans = true;
            exito = false;
            // this.rollback();
            //msg//
            msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
            msg.SIS_ERROR = e.getErrorCode();
            System.out.println("SQLEX msg:" + msg.SIS_MENSAJE + " " + msg.SIS_ERROR + ".");
        }
        return rta;
    }


    /* self */
    public boolean selectSQL(StatementResultSet srs) {
        boolean rta = false;
        try {
            //cerrarRS(numPS);
            cerrarRS(srs.getRs());
            /*
            switch(numPS){
                case 1:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps1).getQueryString());
                        this.rs1=this.ps1.executeQuery();
                        rta=true;
                        break;
                case 2:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs2=this.ps2.executeQuery();
                        rta=true;
                        break;
                case 3:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs3=this.ps3.executeQuery();
                        rta=true;
                        break;
                case 4:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs4=this.ps4.executeQuery();
                        rta=true;
                        break;
                case 5:
                        //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
                        this.rs5=this.ps5.executeQuery();
                        rta=true;
                        break;

            } */
            //System.out.println("SELECT A EJECUTAR:  " +  ((LoggableStatement)this.ps2).getQueryString());
            srs.rs = srs.ps.executeQuery();
            rta = true;

        } catch (SQLException e) {
            /*
			switch(numPS){
                case 1:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps1).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 2:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps2).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 3:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps3).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 4:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps4).getQueryString() + "::exception " +e.getMessage());
                          break;
                case 5:   System.out.println("Error al ejecutar select:  " +  ((LoggableStatement)this.ps5).getQueryString() + "::exception " +e.getMessage());
                          break;
            } */
            System.out.println("Error al ejecutar SELF select:  " + ((LoggableStatement) srs.ps).getQueryString() + "::exception " + e.getMessage());
            errorTrans = true;
            exito = false;
            this.rollback();
            //msg//
            msg.SIS_MENSAJE = msg.obtenerMensajeParaUsuario(e.getMessage());
            msg.SIS_ERROR = e.getErrorCode();
            System.out.println("SQLEX msg:" + msg.SIS_MENSAJE + " " + msg.SIS_ERROR + ".");
        }
        return rta;
    }

    public String getServidorIpPublica() {
        return servidorIpPublica;
    }



}
