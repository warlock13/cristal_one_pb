package Sgh.Utilidades;

import Sgh.AdminSeguridad.Usuario;

/**
 * @(#)Clase Sesion.java version 2.0 2019/11/01 Copyright(c) 2019 Firmas
 * Digitales.
 */
public class Sesion {

    public Conexion cn;
    public Usuario usuario;

    public Sesion() {
        cn = new Conexion();
        usuario = new Usuario();
        usuario.setCn(this.cn);

    }

    public Conexion getCn() {
        return cn;
    }

    public void setCn(Conexion value) {
        cn = value;
    }
}
