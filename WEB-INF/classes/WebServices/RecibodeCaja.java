package WebServices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para RecibodeCaja complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="RecibodeCaja">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoRecibo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Cedula" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Valor" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecibodeCaja", propOrder = {
    "prefijo",
    "tipoRecibo",
    "noRecibo",
    "cedula",
    "valor",
    "prefijoFactura",
    "noFactura"
})
public class RecibodeCaja {

    @XmlElement(name = "Prefijo", required = true)
    protected String prefijo;
    @XmlElement(name = "TipoRecibo", required = true)
    protected Integer tipoRecibo;
    @XmlElement(name = "NoRecibo", required = true)
    protected String noRecibo;
    @XmlElement(name = "Cedula", required = true)
    protected String cedula;
    @XmlElement(name = "Valor", required = true)
    protected float valor;
    @XmlElement(name = "Prefijofac", required = false)
    protected String prefijoFactura;
    @XmlElement(name = "NoFactura", required = false)
    protected String noFactura;

    public RecibodeCaja() {
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Integer getTipoRecibo() {
        return tipoRecibo;
    }

    public void setTipoRecibo(Integer tipoRecibo) {
        this.tipoRecibo = tipoRecibo;
    }

    public String getNoRecibo() {
        return noRecibo;
    }

    public void setNoRecibo(String noRecibo) {
        this.noRecibo = noRecibo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getPrefijoFactura() {
        return prefijoFactura;
    }

    public void setPrefijoFactura(String prefijoFactura) {
        this.prefijoFactura = prefijoFactura;
    }

    public String getNoFactura() {
        return noFactura;
    }

    public void setNoFactura(String noFactura) {
        this.noFactura = noFactura;
    }

    @Override
    public String toString() {
        return "\nRecibodeCaja{prefijo=" + prefijo + ", noRecibo=" + noRecibo + ", cedula=" + cedula + ", valor=" + valor + ", prefijoFactura=" + prefijoFactura + ", noFactura="+ noFactura + "}";
    }

}