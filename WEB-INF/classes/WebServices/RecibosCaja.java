package WebServices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para Recibos complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Recibos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Recibos" type="{InsercionFacturacion}RecibodeCaja" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Recibos", propOrder = {
    "recibos"
})
public class RecibosCaja {

    @XmlElement(name = "Recibos", required = true, nillable = true)
    protected List<RecibodeCaja> recibos;

    public RecibosCaja() {
        recibos = new ArrayList<RecibodeCaja>();
    }

    public void agregarRecibo(RecibodeCaja recibo) {
        recibos.add(recibo);
    }

    public void limpiarRecibos() {
        recibos.clear();
    }

    public List<RecibodeCaja> getRecibos() {
        return recibos;
    }

    public void setRecibos(List<RecibodeCaja> recibos) {
        this.recibos = recibos;
    }

    @Override
    public String toString() {
        return "\nRecibos{" + "recibos=" + recibos + '}';
    }

}
