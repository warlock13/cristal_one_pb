package WebServices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para Recibo complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Recibo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoRecibo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Recibo", propOrder = { "tipoRecibo", "noRecibo", "cedula", "valor", "noFactura" })
public class Recibo {

    @XmlElement(name = "TipoRecibo", required = true)
    protected String tipoRecibo;
    @XmlElement(name = "NoRecibo", required = true)
    protected String noRecibo;
    @XmlElement(name = "Cedula", required = true)
    protected String cedula;
    @XmlElement(name = "Valor", required = true)
    protected String valor;
    @XmlElement(name = "NoFactura", required = false)
    protected String noFactura;

    public Recibo() {
    }

    public String getTipoRecibo() {
        return tipoRecibo;
    }

    public void setTipoRecibo(String tipoRecibo) {
        this.tipoRecibo = tipoRecibo;
    }

    public String getNoRecibo() {
        return noRecibo;
    }

    public void setNoRecibo(String noRecibo) {
        this.noRecibo = noRecibo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getNoFactura() {
        return noFactura;
    }

    public void setNoFactura(String noFactura) {
        this.noFactura = noFactura;
    }

    @Override
    public String toString() {
        return "\nRecibo{" + "tipoRecibo=" + tipoRecibo  + " noRecibo=" + noRecibo + " cedula=" + cedula  + " valor=" + valor  + " noFactura=" + noFactura + "}";
    }

}
