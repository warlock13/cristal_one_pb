package WebServices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para Datos_Terceros complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Datos_Terceros">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tercero" type="{InsercionFacturacion}Tercero" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Datos_Terceros", propOrder = {
    "tercero"
})
public class DatosTerceros {

    @XmlElement(name = "Tercero", required = true, nillable = true)
    protected List<Tercero> tercero;

    public DatosTerceros() {
        tercero = new ArrayList<Tercero>();
    }

    public void agregarTercero(Tercero t) {
        tercero.add(t);
    }

    public void limpiarTerceros() {
        tercero.clear();
    }

    public List<Tercero> getTerceros() {
        return tercero;
    }

    public void setTerceros(List<Tercero> t) {
        tercero = t;
    }

    @Override
    public String toString() {
        return "\nDatosTerceros{" + "terceros=" + tercero + '}';
    }

}