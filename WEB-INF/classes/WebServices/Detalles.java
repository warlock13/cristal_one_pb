package WebServices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para Detalles complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Detalles">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DetalleFactura" type="{InsercionFacturacion}DetalleFactura" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Detalles", propOrder = {
    "detalleFactura"
})
public class Detalles {

    @XmlElement(name = "DetalleFactura", required = true, nillable = true)
    protected List<DetalleFactura> detalleFactura;

    public Detalles() {
        detalleFactura = new ArrayList<DetalleFactura>();
    }

    public void agregarDetalleFactura(DetalleFactura detalle) {
        detalleFactura.add(detalle);
    }

    public void limpiarDetallesFactura() {
        detalleFactura.clear();
    }

    public List<DetalleFactura> getDetallesFactura() {
        return detalleFactura;
    }

    public void setDetallesFactura(List<DetalleFactura> detallesFactura) {
        this.detalleFactura = detallesFactura;
    }

    @Override
    public String toString() {
        return "\nDetalles{" + "detallesFactura=" + detalleFactura + '}';
    }

}