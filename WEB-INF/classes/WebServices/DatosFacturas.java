package WebServices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para Datos_facturas complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Datos_facturas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FacturaVenta" type="{InsercionFacturacion}FacturaVenta" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Datos_facturas", propOrder = {
    "facturaVenta"
})
public class DatosFacturas {

    @XmlElement(name = "FacturaVenta", required = true, nillable = true)
    protected List<FacturaVenta> facturaVenta;

    public DatosFacturas() {
        facturaVenta = new ArrayList<FacturaVenta>();
    }

    public void agregarFacturaVenta(FacturaVenta factura) {
        facturaVenta.add(factura);
    }

    public void limpiarFacturaVentas() {
        facturaVenta.clear();
    }

    public List<FacturaVenta> getFacturaVentas() {
        return facturaVenta;
    }

    public void setFacturaVentas(List<FacturaVenta> facturaVenta) {
        this.facturaVenta = facturaVenta;
    }

    @Override
    public String toString() {
        return "\nDatosFacturas{" + "facturaVentas=" + facturaVenta + '}';
    }

}