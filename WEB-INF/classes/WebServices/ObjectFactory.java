package WebServices;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the t1 package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package: t1
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DatosFacturas }
     *
     * @return
     */
    public DatosFacturas createDatosFacturas() {
        return new DatosFacturas();
    }

    /**
     * Create an instance of {@link RecibodeCaja }
     *
     * @return
     */
    public RecibodeCaja createRecibodeCaja() {
        return new RecibodeCaja();
    }

    /**
     * Create an instance of {@link Error }
     *
     * @return
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link DatosTerceros }
     *
     * @return
     */
    public DatosTerceros createDatosTerceros() {
        return new DatosTerceros();
    }

    /**
     * Create an instance of {@link Recibo }
     *
     * @return
     */
    public Recibo createRecibo() {
        return new Recibo();
    }

    /**
     * Create an instance of {@link RecibosCaja }
     *
     * @return
     */
    public Recibos createRecibos() {
        return new Recibos();
    }

    /**
     * Create an instance of {@link FacturaVenta }
     *
     * @return
     */
    public FacturaVenta createFacturaVenta() {
        return new FacturaVenta();
    }

    /**
     * Create an instance of {@link Resultado }
     *
     * @return
     */
    public Resultado createResultado() {
        return new Resultado();
    }

    /**
     * Create an instance of {@link InfoFacturacion }
     *
     * @return
     */
    public InfoFacturacion createInfoFacturacion() {
        return new InfoFacturacion();
    }

    /**
     * Create an instance of {@link DetalleFactura }
     *
     * @return
     */
    public DetalleFactura createDetalleFactura() {
        return new DetalleFactura();
    }

    /**
     * Create an instance of {@link InfoFACResult }
     *
     * @return
     */
    public InfoFACResult createInfoFACResult() {
        return new InfoFACResult();
    }

    /**
     * Create an instance of {@link Terc RecibodeCaja()ero }
     *
     * @return
     */
    public Tercero createTercero() {
        return new Tercero();
    }

    /**
     * Create an instance of {@link AnulaFacturas }
     *
     * @return
     */
    public AnulaFacturas createAnulaFacturas() {
        return new AnulaFacturas();
    }

    /**
     * Create an instance of {@link Detalles }
     *
     * @return
     */
    public Detalles createDetalles() {
        return new Detalles();
    }

    /**
     * Create an instance of {@link AnulaFactura }
     *
     * @return
     */
    public AnulaFactura createAnulaFactura() {
        return new AnulaFactura();
    }

}
