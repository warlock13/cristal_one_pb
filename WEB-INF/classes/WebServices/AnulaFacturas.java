package WebServices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para AnulaFacturas complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="AnulaFacturas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AnulaFacturas" type="{InsercionFacturacion}AnulaFactura" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnulaFacturas", propOrder = {
    "anulaFacturas"
})
public class AnulaFacturas {

    @XmlElement(name = "AnulaFacturas", required = true, nillable = true)
    protected List<AnulaFactura> anulaFacturas;

    public AnulaFacturas() {
        anulaFacturas = new ArrayList<AnulaFactura>();
    }

    public void agregarAnulaFactura(AnulaFactura anulaFactura) {
        anulaFacturas.add(anulaFactura);
    }

    public void limpiarAnulaFacturas() {
        anulaFacturas.clear();
    }

    public void setAnulaFacturas(List<AnulaFactura> anulaFacturas) {
        this.anulaFacturas = anulaFacturas;
    }

    public List<AnulaFactura> getAnulaFacturas() {
        return anulaFacturas;
    }

    @Override
    public String toString() {
        return "\nAnulaFacturas{" + "anulaFacturas=" + anulaFacturas + '}';
    }

}