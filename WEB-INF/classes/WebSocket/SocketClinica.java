
package WebSocket;

import java.io.StringReader;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ApplicationScoped
 @ServerEndpoint("/turnos")
public class SocketClinica {

    @Inject
    private PacienteSessionHandler pacienteSession;

    @OnOpen
    public void onOpen(Session session) {

        pacienteSession.agregarSesion(session);
    }

    @OnClose
    public void onClose(Session session) {

        pacienteSession.eliminarSesion(session);
    }

    @OnMessage
    public void onMessage(String mensaje,Session session) {

        try {
            JsonReader reader = Json.createReader(new StringReader(mensaje));
            JsonObject mensajeJson = reader.readObject();

            String id = mensajeJson.getString("id");


                   String accion = mensajeJson.getString("accion");

            if(accion.equals(PacienteSessionHandler.AGREGAR)){
                agregarPaciente(mensajeJson);

            }else if(accion.equals(PacienteSessionHandler.ELIMINAR)){
                pacienteSession.eliminarPaciente(id);

            }else if(accion.equals(PacienteSessionHandler.LLAMAR)){
                pacienteSession.llamarPaciente(id);

            }else if(accion.equals(PacienteSessionHandler.MODIFICAR)){
                pacienteSession.modificarPaciente(id);
            }



        }catch(Exception e){
            System.out.println("Error en Formato de Json: "+e.getMessage()+"\n"+mensaje);
        }
    }

    private void agregarPaciente(JsonObject mensaje){

        PacienteSocket paciente = new PacienteSocket();
        paciente.setId(mensaje.getString("id"));
        paciente.setNombre(mensaje.getString("nombre"));
        paciente.setConsultorio(mensaje.getString("consultorio"));
        paciente.setEstado(mensaje.getString("estado"));
        paciente.setTurno(mensaje.getInt("turno"));

        pacienteSession.agregarPaciente(paciente);

    }

    @OnError
    public void onError(Throwable t) {

        System.out.println("Error en SocketClinica: "+t.getMessage());
    }

}